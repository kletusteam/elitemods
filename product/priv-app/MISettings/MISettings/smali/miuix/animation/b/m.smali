.class public Lmiuix/animation/b/m;
.super Lmiuix/animation/b/b;

# interfaces
.implements Lmiuix/animation/i;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/animation/b/m$a;
    }
.end annotation


# static fields
.field private static b:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap<",
            "Landroid/view/View;",
            "Lmiuix/animation/b/m$a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:F

.field private d:Lmiuix/animation/a/a;

.field private e:Lmiuix/animation/a/a;

.field private f:Lmiuix/animation/a/a;

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lmiuix/animation/i$b;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lmiuix/animation/i$b;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lmiuix/animation/i$a;

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:[I

.field private o:F

.field private p:I

.field private q:I

.field private r:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljava/lang/String;

.field private v:Lmiuix/animation/e/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lmiuix/animation/b/m;->b:Ljava/util/WeakHashMap;

    return-void
.end method

.method public varargs constructor <init>([Lmiuix/animation/e;)V
    .locals 5

    invoke-direct {p0, p1}, Lmiuix/animation/b/b;-><init>([Lmiuix/animation/e;)V

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lmiuix/animation/b/m;->c:F

    new-instance v0, Lmiuix/animation/a/a;

    invoke-direct {v0}, Lmiuix/animation/a/a;-><init>()V

    const/4 v1, 0x2

    new-array v2, v1, [F

    fill-array-data v2, :array_0

    const/4 v3, -0x2

    invoke-static {v3, v2}, Lmiuix/animation/h/c;->c(I[F)Lmiuix/animation/h/c$a;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmiuix/animation/a/a;->a(Lmiuix/animation/h/c$a;)Lmiuix/animation/a/a;

    iput-object v0, p0, Lmiuix/animation/b/m;->d:Lmiuix/animation/a/a;

    new-instance v0, Lmiuix/animation/a/a;

    invoke-direct {v0}, Lmiuix/animation/a/a;-><init>()V

    iput-object v0, p0, Lmiuix/animation/b/m;->e:Lmiuix/animation/a/a;

    new-instance v0, Lmiuix/animation/a/a;

    invoke-direct {v0}, Lmiuix/animation/a/a;-><init>()V

    iput-object v0, p0, Lmiuix/animation/b/m;->f:Lmiuix/animation/a/a;

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lmiuix/animation/b/m;->g:Ljava/util/Map;

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lmiuix/animation/b/m;->h:Ljava/util/Map;

    sget-object v0, Lmiuix/animation/i$a;->a:Lmiuix/animation/i$a;

    iput-object v0, p0, Lmiuix/animation/b/m;->i:Lmiuix/animation/i$a;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/animation/b/m;->j:Z

    iput-boolean v0, p0, Lmiuix/animation/b/m;->l:Z

    new-array v2, v1, [I

    iput-object v2, p0, Lmiuix/animation/b/m;->n:[I

    const/4 v2, 0x0

    iput v2, p0, Lmiuix/animation/b/m;->o:F

    iput v0, p0, Lmiuix/animation/b/m;->p:I

    iput v0, p0, Lmiuix/animation/b/m;->q:I

    const-string v2, "MOVE"

    iput-object v2, p0, Lmiuix/animation/b/m;->u:Ljava/lang/String;

    new-instance v2, Lmiuix/animation/b/k;

    invoke-direct {v2, p0}, Lmiuix/animation/b/k;-><init>(Lmiuix/animation/b/m;)V

    iput-object v2, p0, Lmiuix/animation/b/m;->v:Lmiuix/animation/e/b;

    array-length v2, p1

    if-lez v2, :cond_0

    aget-object p1, p1, v0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-direct {p0, p1}, Lmiuix/animation/b/m;->a(Lmiuix/animation/e;)V

    iget-object p1, p0, Lmiuix/animation/b/m;->i:Lmiuix/animation/i$a;

    invoke-direct {p0, p1}, Lmiuix/animation/b/m;->b(Lmiuix/animation/i$a;)V

    iget-object p1, p0, Lmiuix/animation/b/m;->e:Lmiuix/animation/a/a;

    new-array v2, v1, [F

    fill-array-data v2, :array_1

    invoke-static {v3, v2}, Lmiuix/animation/h/c;->c(I[F)Lmiuix/animation/h/c$a;

    move-result-object v2

    invoke-virtual {p1, v2}, Lmiuix/animation/a/a;->a(Lmiuix/animation/h/c$a;)Lmiuix/animation/a/a;

    iget-object p1, p0, Lmiuix/animation/b/m;->e:Lmiuix/animation/a/a;

    const/4 v2, 0x1

    new-array v2, v2, [Lmiuix/animation/e/b;

    iget-object v4, p0, Lmiuix/animation/b/m;->v:Lmiuix/animation/e/b;

    aput-object v4, v2, v0

    invoke-virtual {p1, v2}, Lmiuix/animation/a/a;->a([Lmiuix/animation/e/b;)Lmiuix/animation/a/a;

    iget-object p1, p0, Lmiuix/animation/b/m;->f:Lmiuix/animation/a/a;

    new-array v0, v1, [F

    fill-array-data v0, :array_2

    invoke-virtual {p1, v3, v0}, Lmiuix/animation/a/a;->a(I[F)Lmiuix/animation/a/a;

    sget-object v0, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    const-wide/16 v2, -0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_3

    invoke-virtual {p1, v0, v2, v3, v1}, Lmiuix/animation/a/a;->a(Lmiuix/animation/g/b;J[F)Lmiuix/animation/a/a;

    return-void

    :array_0
    .array-data 4
        0x3f666666    # 0.9f
        0x3ecccccd    # 0.4f
    .end array-data

    :array_1
    .array-data 4
        0x3f7d70a4    # 0.99f
        0x3f19999a    # 0.6f
    .end array-data

    :array_2
    .array-data 4
        0x3f7d70a4    # 0.99f
        0x3ecccccd    # 0.4f
    .end array-data

    :array_3
    .array-data 4
        0x3f666666    # 0.9f
        0x3e4ccccd    # 0.2f
    .end array-data
.end method

.method private a(FFF)F
    .locals 0

    sub-float/2addr p1, p2

    sub-float/2addr p3, p2

    div-float/2addr p1, p3

    return p1
.end method

.method private a(Ljava/lang/ref/WeakReference;)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    :cond_0
    return-object p1
.end method

.method private static a(Landroid/view/View;F)V
    .locals 5

    const/4 v0, 0x1

    :try_start_0
    new-array v1, v0, [Ljava/lang/Class;

    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object v2, v1, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v2, "android.view.View"

    :try_start_1
    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const-string v4, "setFeedbackRadius"

    invoke-virtual {v2, v4, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {v1, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "setFeedbackRadius failed , e:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, ""

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 7

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result p2

    iget-object v1, p0, Lmiuix/animation/b/m;->n:[I

    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    iget-object v1, p0, Lmiuix/animation/b/m;->n:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v3, v4

    add-float/2addr v1, v3

    iget-object v3, p0, Lmiuix/animation/b/m;->n:[I

    const/4 v5, 0x1

    aget v3, v3, v5

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v4

    add-float/2addr v3, v6

    sub-float/2addr v0, v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    sub-float/2addr p2, v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    int-to-float p1, p1

    div-float/2addr p2, p1

    const/high16 p1, 0x3f800000    # 1.0f

    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const/high16 v1, -0x40800000    # -1.0f

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {p1, p2}, Ljava/lang/Math;->min(FF)F

    move-result p2

    invoke-static {v1, p2}, Ljava/lang/Math;->max(FF)F

    move-result p2

    iget v1, p0, Lmiuix/animation/b/m;->c:F

    const v3, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v4, v1, v3

    if-nez v4, :cond_0

    move v1, p1

    :cond_0
    mul-float/2addr v0, v1

    iget v1, p0, Lmiuix/animation/b/m;->c:F

    cmpl-float v3, v1, v3

    if-nez v3, :cond_1

    goto :goto_0

    :cond_1
    move p1, v1

    :goto_0
    mul-float/2addr p2, p1

    iget-object p1, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    iget-object v1, p0, Lmiuix/animation/b/m;->u:Ljava/lang/String;

    invoke-interface {p1, v1}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object p1

    sget-object v1, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    float-to-double v3, v0

    invoke-virtual {p1, v1, v3, v4}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v0, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    float-to-double v3, p2

    invoke-virtual {p1, v0, v3, v4}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    iget-object p2, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    new-array v0, v5, [Lmiuix/animation/a/a;

    iget-object v1, p0, Lmiuix/animation/b/m;->d:Lmiuix/animation/a/a;

    aput-object v1, v0, v2

    invoke-interface {p2, p1, v0}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    return-void
.end method

.method private varargs a(Landroid/view/View;Landroid/view/MotionEvent;[Lmiuix/animation/a/a;)V
    .locals 2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 p1, 0x9

    if-eq v0, p1, :cond_1

    const/16 p1, 0xa

    if-eq v0, p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0, p2, p3}, Lmiuix/animation/b/m;->d(Landroid/view/MotionEvent;[Lmiuix/animation/a/a;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p2, p3}, Lmiuix/animation/b/m;->c(Landroid/view/MotionEvent;[Lmiuix/animation/a/a;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lmiuix/animation/b/m;->b(Landroid/view/View;Landroid/view/MotionEvent;[Lmiuix/animation/a/a;)V

    :goto_0
    return-void
.end method

.method private static a(Landroid/view/View;Z)V
    .locals 5

    const/4 v0, 0x1

    :try_start_0
    new-array v1, v0, [Ljava/lang/Class;

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object v2, v1, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v2, "android.view.View"

    :try_start_1
    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const-string v4, "setMagicView"

    invoke-virtual {v2, v4, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {v1, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "setMagicView failed , e:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, ""

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method static synthetic a(Lmiuix/animation/b/m;Landroid/view/View;Landroid/view/MotionEvent;[Lmiuix/animation/a/a;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lmiuix/animation/b/m;->a(Landroid/view/View;Landroid/view/MotionEvent;[Lmiuix/animation/a/a;)V

    return-void
.end method

.method private a(Lmiuix/animation/e;)V
    .locals 6

    instance-of v0, p1, Lmiuix/animation/ViewTarget;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lmiuix/animation/ViewTarget;

    invoke-virtual {v0}, Lmiuix/animation/ViewTarget;->e()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    sget-object v1, Lmiuix/animation/g/A;->n:Lmiuix/animation/g/A;

    invoke-virtual {p1, v1}, Lmiuix/animation/e;->a(Lmiuix/animation/g/b;)F

    move-result v1

    sget-object v2, Lmiuix/animation/g/A;->m:Lmiuix/animation/g/A;

    invoke-virtual {p1, v2}, Lmiuix/animation/e;->a(Lmiuix/animation/g/b;)F

    move-result p1

    invoke-static {v1, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    const/high16 v1, 0x41400000    # 12.0f

    add-float/2addr v1, p1

    div-float/2addr v1, p1

    const p1, 0x3f933333    # 1.15f

    invoke-static {v1, p1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    iput v1, p0, Lmiuix/animation/b/m;->p:I

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iput v0, p0, Lmiuix/animation/b/m;->q:I

    iget v0, p0, Lmiuix/animation/b/m;->p:I

    add-int/lit8 v0, v0, -0x28

    iget v1, p0, Lmiuix/animation/b/m;->q:I

    add-int/lit8 v1, v1, -0x28

    int-to-float v0, v0

    const/high16 v2, 0x43b40000    # 360.0f

    const/4 v3, 0x0

    invoke-direct {p0, v0, v3, v2}, Lmiuix/animation/b/m;->a(FFF)F

    move-result v0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v4, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const/high16 v5, 0x41700000    # 15.0f

    invoke-direct {p0, v0, v5, v3}, Lmiuix/animation/b/m;->b(FFF)F

    move-result v0

    invoke-static {v5, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    int-to-float v1, v1

    invoke-direct {p0, v1, v3, v2}, Lmiuix/animation/b/m;->a(FFF)F

    move-result v1

    invoke-static {v4, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-direct {p0, v1, v5, v3}, Lmiuix/animation/b/m;->b(FFF)F

    move-result v1

    invoke-static {v5, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    cmpl-float p1, p1, v4

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v3

    :goto_1
    iput v3, p0, Lmiuix/animation/b/m;->c:F

    iget p1, p0, Lmiuix/animation/b/m;->p:I

    iget v0, p0, Lmiuix/animation/b/m;->q:I

    if-ne p1, v0, :cond_2

    const/16 v1, 0x64

    if-ge p1, v1, :cond_2

    if-ge v0, v1, :cond_2

    int-to-float p1, p1

    const/high16 v0, 0x3f000000    # 0.5f

    mul-float/2addr p1, v0

    float-to-int p1, p1

    int-to-float p1, p1

    invoke-virtual {p0, p1}, Lmiuix/animation/b/m;->b(F)Lmiuix/animation/i;

    goto :goto_2

    :cond_2
    const/high16 p1, 0x42100000    # 36.0f

    invoke-virtual {p0, p1}, Lmiuix/animation/b/m;->b(F)Lmiuix/animation/i;

    :cond_3
    :goto_2
    return-void
.end method

.method private varargs a(Z[Lmiuix/animation/a/a;)V
    .locals 5

    iput-boolean p1, p0, Lmiuix/animation/b/m;->j:Z

    const/4 p1, 0x1

    iput-boolean p1, p0, Lmiuix/animation/b/m;->m:Z

    iget-object v0, p0, Lmiuix/animation/b/m;->i:Lmiuix/animation/i$a;

    sget-object v1, Lmiuix/animation/i$a;->c:Lmiuix/animation/i$a;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lmiuix/animation/b/m;->r:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    invoke-static {v0, p1}, Lmiuix/animation/b/m;->a(Landroid/view/View;Z)V

    invoke-static {v0, p1}, Lmiuix/animation/b/m;->c(Landroid/view/View;Z)V

    :cond_1
    invoke-virtual {p0}, Lmiuix/animation/b/m;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, p1}, Lmiuix/animation/b/m;->a(Z)V

    invoke-virtual {p0, p1}, Lmiuix/animation/b/m;->b(Z)V

    :cond_2
    iget v0, p0, Lmiuix/animation/b/m;->o:F

    invoke-virtual {p0, v0}, Lmiuix/animation/b/m;->b(F)Lmiuix/animation/i;

    invoke-direct {p0}, Lmiuix/animation/b/m;->n()V

    invoke-direct {p0, p2}, Lmiuix/animation/b/m;->h([Lmiuix/animation/a/a;)[Lmiuix/animation/a/a;

    move-result-object p2

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v1, Lmiuix/animation/i$b;->a:Lmiuix/animation/i$b;

    invoke-interface {v0, v1}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v0

    sget-object v1, Lmiuix/animation/i$b;->a:Lmiuix/animation/i$b;

    invoke-direct {p0, v1}, Lmiuix/animation/b/m;->a(Lmiuix/animation/i$b;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    invoke-interface {v1}, Lmiuix/animation/b/w;->getTarget()Lmiuix/animation/e;

    move-result-object v1

    sget-object v2, Lmiuix/animation/g/A;->n:Lmiuix/animation/g/A;

    invoke-virtual {v1, v2}, Lmiuix/animation/e;->a(Lmiuix/animation/g/b;)F

    move-result v2

    sget-object v3, Lmiuix/animation/g/A;->m:Lmiuix/animation/g/A;

    invoke-virtual {v1, v3}, Lmiuix/animation/e;->a(Lmiuix/animation/g/b;)F

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    const/high16 v2, 0x41400000    # 12.0f

    add-float/2addr v2, v1

    div-float/2addr v2, v1

    const v1, 0x3f933333    # 1.15f

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    sget-object v2, Lmiuix/animation/g/A;->e:Lmiuix/animation/g/A;

    float-to-double v3, v1

    invoke-virtual {v0, v2, v3, v4}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v1, Lmiuix/animation/g/A;->f:Lmiuix/animation/g/A;

    invoke-virtual {v0, v1, v3, v4}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    :cond_3
    iget-object v1, p0, Lmiuix/animation/b/m;->t:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_4

    new-array p1, p1, [Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    aput-object v1, p1, v2

    invoke-static {p1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object p1

    sget-object v1, Lmiuix/animation/g/A;->e:Lmiuix/animation/g/A;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {p1, v1, v2}, Lmiuix/animation/k;->a(Lmiuix/animation/g/b;F)Lmiuix/animation/k;

    sget-object v1, Lmiuix/animation/g/A;->f:Lmiuix/animation/g/A;

    invoke-interface {p1, v1, v2}, Lmiuix/animation/k;->a(Lmiuix/animation/g/b;F)Lmiuix/animation/k;

    invoke-interface {p1, p2}, Lmiuix/animation/k;->a([Lmiuix/animation/a/a;)Lmiuix/animation/k;

    :cond_4
    iget-object p1, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    invoke-interface {p1, v0, p2}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    return-void
.end method

.method private a(Landroid/view/View;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b/m;->r:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-ne v0, p1, :cond_1

    const/4 p1, 0x0

    return p1

    :cond_1
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmiuix/animation/b/m;->r:Ljava/lang/ref/WeakReference;

    const/4 p1, 0x1

    return p1
.end method

.method static a(Landroid/view/View;[ILandroid/view/MotionEvent;)Z
    .locals 5

    const/4 v0, 0x1

    if-eqz p0, :cond_1

    invoke-virtual {p0, p1}, Landroid/view/View;->getLocationOnScreen([I)V

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result p2

    float-to-int p2, p2

    const/4 v2, 0x0

    aget v3, p1, v2

    if-lt v1, v3, :cond_0

    aget v3, p1, v2

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    if-gt v1, v3, :cond_0

    aget v1, p1, v0

    if-lt p2, v1, :cond_0

    aget p1, p1, v0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result p0

    add-int/2addr p1, p0

    if-gt p2, p1, :cond_0

    goto :goto_0

    :cond_0
    move v0, v2

    :cond_1
    :goto_0
    return v0
.end method

.method private a(Lmiuix/animation/i$b;)Z
    .locals 2

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, Lmiuix/animation/b/m;->g:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private b(FFF)F
    .locals 0

    sub-float/2addr p3, p2

    mul-float/2addr p3, p1

    add-float/2addr p2, p3

    return p2
.end method

.method private varargs b(I[Lmiuix/animation/a/a;)V
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    :cond_1
    const/4 p1, 0x0

    invoke-direct {p0, p1, p2}, Lmiuix/animation/b/m;->a(Z[Lmiuix/animation/a/a;)V

    goto :goto_1

    :cond_2
    :goto_0
    invoke-virtual {p0, p2}, Lmiuix/animation/b/m;->f([Lmiuix/animation/a/a;)V

    :cond_3
    :goto_1
    return-void
.end method

.method private varargs b(Landroid/view/View;Landroid/view/MotionEvent;[Lmiuix/animation/a/a;)V
    .locals 0

    iget-boolean p3, p0, Lmiuix/animation/b/m;->m:Z

    if-eqz p3, :cond_0

    if-eqz p1, :cond_0

    sget-object p3, Lmiuix/animation/i$b;->a:Lmiuix/animation/i$b;

    invoke-direct {p0, p3}, Lmiuix/animation/b/m;->b(Lmiuix/animation/i$b;)Z

    move-result p3

    if-eqz p3, :cond_0

    iget-boolean p3, p0, Lmiuix/animation/b/m;->j:Z

    if-eqz p3, :cond_0

    invoke-direct {p0, p1, p2}, Lmiuix/animation/b/m;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    :cond_0
    return-void
.end method

.method private static b(Landroid/view/View;Z)V
    .locals 5

    const/4 v0, 0x1

    :try_start_0
    new-array v1, v0, [Ljava/lang/Class;

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object v2, v1, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v2, "android.view.View"

    :try_start_1
    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const-string v4, "setPointerHide"

    invoke-virtual {v2, v4, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {v1, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "setPointerHide failed , e:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, ""

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method private b(Lmiuix/animation/i$a;)V
    .locals 2

    sget-object v0, Lmiuix/animation/b/l;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_5

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lmiuix/animation/b/m;->i:Lmiuix/animation/i$a;

    sget-object v1, Lmiuix/animation/i$a;->a:Lmiuix/animation/i$a;

    if-eq v0, v1, :cond_1

    sget-object v1, Lmiuix/animation/i$a;->b:Lmiuix/animation/i$a;

    if-ne v0, v1, :cond_2

    :cond_1
    invoke-virtual {p0}, Lmiuix/animation/b/m;->e()Lmiuix/animation/i;

    :cond_2
    invoke-direct {p0}, Lmiuix/animation/b/m;->l()V

    invoke-direct {p0}, Lmiuix/animation/b/m;->m()V

    invoke-direct {p0}, Lmiuix/animation/b/m;->k()V

    iput-object p1, p0, Lmiuix/animation/b/m;->i:Lmiuix/animation/i$a;

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lmiuix/animation/b/m;->i:Lmiuix/animation/i$a;

    sget-object v1, Lmiuix/animation/i$a;->c:Lmiuix/animation/i$a;

    if-ne v0, v1, :cond_4

    invoke-direct {p0}, Lmiuix/animation/b/m;->g()V

    :cond_4
    invoke-direct {p0}, Lmiuix/animation/b/m;->n()V

    invoke-direct {p0}, Lmiuix/animation/b/m;->l()V

    invoke-direct {p0}, Lmiuix/animation/b/m;->m()V

    iput-object p1, p0, Lmiuix/animation/b/m;->i:Lmiuix/animation/i$a;

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lmiuix/animation/b/m;->i:Lmiuix/animation/i$a;

    sget-object v1, Lmiuix/animation/i$a;->b:Lmiuix/animation/i$a;

    if-ne v0, v1, :cond_6

    invoke-direct {p0}, Lmiuix/animation/b/m;->h()V

    invoke-direct {p0}, Lmiuix/animation/b/m;->i()V

    goto :goto_0

    :cond_6
    sget-object v1, Lmiuix/animation/i$a;->c:Lmiuix/animation/i$a;

    if-ne v0, v1, :cond_7

    invoke-direct {p0}, Lmiuix/animation/b/m;->h()V

    invoke-direct {p0}, Lmiuix/animation/b/m;->i()V

    invoke-direct {p0}, Lmiuix/animation/b/m;->g()V

    :cond_7
    :goto_0
    invoke-direct {p0}, Lmiuix/animation/b/m;->n()V

    iput-object p1, p0, Lmiuix/animation/b/m;->i:Lmiuix/animation/i$a;

    :goto_1
    return-void
.end method

.method private b(Lmiuix/animation/i$b;)Z
    .locals 2

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, Lmiuix/animation/b/m;->h:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private varargs c(Landroid/view/MotionEvent;[Lmiuix/animation/a/a;)V
    .locals 2

    invoke-static {}, Lmiuix/animation/h/g;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onEventEnter, touchEnter"

    invoke-static {v1, v0}, Lmiuix/animation/h/g;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p0, p1, p2}, Lmiuix/animation/b/m;->a(Landroid/view/MotionEvent;[Lmiuix/animation/a/a;)V

    return-void
.end method

.method private static c(Landroid/view/View;Z)V
    .locals 5

    const/4 v0, 0x1

    :try_start_0
    new-array v1, v0, [Ljava/lang/Class;

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object v2, v1, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v2, "android.view.View"

    :try_start_1
    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const-string v4, "setWrapped"

    invoke-virtual {v2, v4, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {v1, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "setWrapped failed , e:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, ""

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method private varargs c(Landroid/view/View;[Lmiuix/animation/a/a;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lmiuix/animation/b/m;->d(Landroid/view/View;[Lmiuix/animation/a/a;)V

    invoke-direct {p0, p1}, Lmiuix/animation/b/m;->a(Landroid/view/View;)Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-static {}, Lmiuix/animation/h/g;->c()Z

    move-result p2

    if-eqz p2, :cond_0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "handleViewHover for "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Object;

    invoke-static {p1, p2}, Lmiuix/animation/h/g;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private varargs d(Landroid/view/MotionEvent;[Lmiuix/animation/a/a;)V
    .locals 2

    iget-boolean v0, p0, Lmiuix/animation/b/m;->m:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lmiuix/animation/h/g;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onEventExit, touchExit"

    invoke-static {v1, v0}, Lmiuix/animation/h/g;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p0, p1, p2}, Lmiuix/animation/b/m;->b(Landroid/view/MotionEvent;[Lmiuix/animation/a/a;)V

    invoke-direct {p0}, Lmiuix/animation/b/m;->j()V

    :cond_1
    return-void
.end method

.method private varargs d(Landroid/view/View;[Lmiuix/animation/a/a;)V
    .locals 2

    sget-object v0, Lmiuix/animation/b/m;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/animation/b/m$a;

    if-nez v0, :cond_0

    new-instance v0, Lmiuix/animation/b/m$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmiuix/animation/b/m$a;-><init>(Lmiuix/animation/b/k;)V

    sget-object v1, Lmiuix/animation/b/m;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    invoke-virtual {v0, p0, p2}, Lmiuix/animation/b/m$a;->a(Lmiuix/animation/b/m;[Lmiuix/animation/a/a;)V

    return-void
.end method

.method private g()V
    .locals 0

    return-void
.end method

.method private h()V
    .locals 2

    sget-object v0, Lmiuix/animation/i$b;->a:Lmiuix/animation/i$b;

    invoke-direct {p0, v0}, Lmiuix/animation/b/m;->a(Lmiuix/animation/i$b;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v1, Lmiuix/animation/i$b;->a:Lmiuix/animation/i$b;

    invoke-interface {v0, v1}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v0

    sget-object v1, Lmiuix/animation/g/A;->e:Lmiuix/animation/g/A;

    invoke-virtual {v0, v1}, Lmiuix/animation/b/a;->e(Ljava/lang/Object;)Lmiuix/animation/b/a;

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v1, Lmiuix/animation/i$b;->a:Lmiuix/animation/i$b;

    invoke-interface {v0, v1}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v0

    sget-object v1, Lmiuix/animation/g/A;->f:Lmiuix/animation/g/A;

    invoke-virtual {v0, v1}, Lmiuix/animation/b/a;->e(Ljava/lang/Object;)Lmiuix/animation/b/a;

    :cond_0
    sget-object v0, Lmiuix/animation/i$b;->b:Lmiuix/animation/i$b;

    invoke-direct {p0, v0}, Lmiuix/animation/b/m;->a(Lmiuix/animation/i$b;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v1, Lmiuix/animation/i$b;->b:Lmiuix/animation/i$b;

    invoke-interface {v0, v1}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v0

    sget-object v1, Lmiuix/animation/g/A;->e:Lmiuix/animation/g/A;

    invoke-virtual {v0, v1}, Lmiuix/animation/b/a;->e(Ljava/lang/Object;)Lmiuix/animation/b/a;

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v1, Lmiuix/animation/i$b;->b:Lmiuix/animation/i$b;

    invoke-interface {v0, v1}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v0

    sget-object v1, Lmiuix/animation/g/A;->f:Lmiuix/animation/g/A;

    invoke-virtual {v0, v1}, Lmiuix/animation/b/a;->e(Ljava/lang/Object;)Lmiuix/animation/b/a;

    :cond_1
    iget-object v0, p0, Lmiuix/animation/b/m;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method private varargs h([Lmiuix/animation/a/a;)[Lmiuix/animation/a/a;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lmiuix/animation/a/a;

    iget-object v1, p0, Lmiuix/animation/b/m;->e:Lmiuix/animation/a/a;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Lmiuix/animation/h/a;->a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lmiuix/animation/a/a;

    return-object p1
.end method

.method private i()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/animation/b/m;->j:Z

    sget-object v0, Lmiuix/animation/i$b;->a:Lmiuix/animation/i$b;

    invoke-direct {p0, v0}, Lmiuix/animation/b/m;->b(Lmiuix/animation/i$b;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v1, Lmiuix/animation/i$b;->a:Lmiuix/animation/i$b;

    invoke-interface {v0, v1}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v0

    sget-object v1, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    invoke-virtual {v0, v1}, Lmiuix/animation/b/a;->e(Ljava/lang/Object;)Lmiuix/animation/b/a;

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v1, Lmiuix/animation/i$b;->a:Lmiuix/animation/i$b;

    invoke-interface {v0, v1}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v0

    sget-object v1, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    invoke-virtual {v0, v1}, Lmiuix/animation/b/a;->e(Ljava/lang/Object;)Lmiuix/animation/b/a;

    :cond_0
    sget-object v0, Lmiuix/animation/i$b;->b:Lmiuix/animation/i$b;

    invoke-direct {p0, v0}, Lmiuix/animation/b/m;->b(Lmiuix/animation/i$b;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v1, Lmiuix/animation/i$b;->b:Lmiuix/animation/i$b;

    invoke-interface {v0, v1}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v0

    sget-object v1, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    invoke-virtual {v0, v1}, Lmiuix/animation/b/a;->e(Ljava/lang/Object;)Lmiuix/animation/b/a;

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v1, Lmiuix/animation/i$b;->b:Lmiuix/animation/i$b;

    invoke-interface {v0, v1}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v0

    sget-object v1, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    invoke-virtual {v0, v1}, Lmiuix/animation/b/a;->e(Ljava/lang/Object;)Lmiuix/animation/b/a;

    :cond_1
    iget-object v0, p0, Lmiuix/animation/b/m;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method private varargs i([Lmiuix/animation/a/a;)[Lmiuix/animation/a/a;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lmiuix/animation/a/a;

    iget-object v1, p0, Lmiuix/animation/b/m;->f:Lmiuix/animation/a/a;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Lmiuix/animation/h/a;->a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lmiuix/animation/a/a;

    return-object p1
.end method

.method private j()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/animation/b/m;->m:Z

    return-void
.end method

.method private k()V
    .locals 0

    return-void
.end method

.method private l()V
    .locals 4

    iget-object v0, p0, Lmiuix/animation/b/m;->g:Ljava/util/Map;

    sget-object v1, Lmiuix/animation/i$b;->a:Lmiuix/animation/i$b;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmiuix/animation/b/m;->g:Ljava/util/Map;

    sget-object v1, Lmiuix/animation/i$b;->b:Lmiuix/animation/i$b;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v1, Lmiuix/animation/i$b;->b:Lmiuix/animation/i$b;

    invoke-interface {v0, v1}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v0

    sget-object v1, Lmiuix/animation/g/A;->e:Lmiuix/animation/g/A;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v1, v2, v3}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v1, Lmiuix/animation/g/A;->f:Lmiuix/animation/g/A;

    invoke-virtual {v0, v1, v2, v3}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    return-void
.end method

.method private m()V
    .locals 4

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-boolean v0, p0, Lmiuix/animation/b/m;->j:Z

    iget-object v0, p0, Lmiuix/animation/b/m;->h:Ljava/util/Map;

    sget-object v2, Lmiuix/animation/i$b;->a:Lmiuix/animation/i$b;

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmiuix/animation/b/m;->h:Ljava/util/Map;

    sget-object v2, Lmiuix/animation/i$b;->b:Lmiuix/animation/i$b;

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v1, Lmiuix/animation/i$b;->b:Lmiuix/animation/i$b;

    invoke-interface {v0, v1}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v0

    sget-object v1, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v1, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    invoke-virtual {v0, v1, v2, v3}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    return-void
.end method

.method private n()V
    .locals 5

    iget-boolean v0, p0, Lmiuix/animation/b/m;->k:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lmiuix/animation/b/m;->l:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/16 v0, 0x14

    const/4 v1, 0x0

    invoke-static {v0, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    iget-object v1, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    invoke-interface {v1}, Lmiuix/animation/b/w;->getTarget()Lmiuix/animation/e;

    move-result-object v1

    invoke-virtual {v1}, Lmiuix/animation/e;->e()Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Landroid/view/View;

    if-eqz v2, :cond_1

    check-cast v1, Landroid/view/View;

    sget v0, Ld/f/a;->miuix_folme_color_touch_tint:I

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :cond_1
    sget-object v1, Lmiuix/animation/g/C;->a:Lmiuix/animation/g/C$b;

    iget-object v2, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v3, Lmiuix/animation/i$b;->a:Lmiuix/animation/i$b;

    invoke-interface {v2, v3}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v2

    int-to-double v3, v0

    invoke-virtual {v2, v1, v3, v4}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v2, Lmiuix/animation/i$b;->b:Lmiuix/animation/i$b;

    invoke-interface {v0, v2}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    :cond_2
    :goto_0
    return-void
.end method


# virtual methods
.method public a(FFFF)Lmiuix/animation/i;
    .locals 1

    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr p1, v0

    float-to-int p1, p1

    mul-float/2addr p2, v0

    float-to-int p2, p2

    mul-float/2addr p3, v0

    float-to-int p3, p3

    mul-float/2addr p4, v0

    float-to-int p4, p4

    invoke-static {p1, p2, p3, p4}, Landroid/graphics/Color;->argb(IIII)I

    move-result p1

    invoke-virtual {p0, p1}, Lmiuix/animation/b/m;->setTint(I)Lmiuix/animation/i;

    return-object p0
.end method

.method public a(Lmiuix/animation/i$a;)Lmiuix/animation/i;
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/animation/b/m;->b(Lmiuix/animation/i$a;)V

    return-object p0
.end method

.method public a()V
    .locals 2

    invoke-super {p0}, Lmiuix/animation/b/b;->a()V

    iget-object v0, p0, Lmiuix/animation/b/m;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lmiuix/animation/b/m;->r:Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lmiuix/animation/b/m;->a(Ljava/lang/ref/WeakReference;)Landroid/view/View;

    iput-object v1, p0, Lmiuix/animation/b/m;->r:Ljava/lang/ref/WeakReference;

    :cond_0
    iget-object v0, p0, Lmiuix/animation/b/m;->s:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    invoke-direct {p0, v0}, Lmiuix/animation/b/m;->a(Ljava/lang/ref/WeakReference;)Landroid/view/View;

    iput-object v1, p0, Lmiuix/animation/b/m;->s:Ljava/lang/ref/WeakReference;

    :cond_1
    iget-object v0, p0, Lmiuix/animation/b/m;->t:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    invoke-direct {p0, v0}, Lmiuix/animation/b/m;->a(Ljava/lang/ref/WeakReference;)Landroid/view/View;

    iput-object v1, p0, Lmiuix/animation/b/m;->t:Ljava/lang/ref/WeakReference;

    :cond_2
    return-void
.end method

.method public a(F)V
    .locals 2

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    invoke-interface {v0}, Lmiuix/animation/b/w;->getTarget()Lmiuix/animation/e;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/animation/e;->e()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lmiuix/animation/b/m;->a(Landroid/view/View;F)V

    :cond_0
    return-void
.end method

.method public varargs a(Landroid/view/MotionEvent;[Lmiuix/animation/a/a;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result p1

    invoke-direct {p0, p1, p2}, Lmiuix/animation/b/m;->b(I[Lmiuix/animation/a/a;)V

    return-void
.end method

.method public a(Z)V
    .locals 2

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    invoke-interface {v0}, Lmiuix/animation/b/w;->getTarget()Lmiuix/animation/e;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/animation/e;->e()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lmiuix/animation/b/m;->a(Landroid/view/View;Z)V

    :cond_0
    return-void
.end method

.method public b(F)Lmiuix/animation/i;
    .locals 2

    iput p1, p0, Lmiuix/animation/b/m;->o:F

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    invoke-interface {v0}, Lmiuix/animation/b/w;->getTarget()Lmiuix/animation/e;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/animation/e;->e()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/view/View;

    sget v1, Lmiuix/animation/p;->miuix_animation_tag_view_hover_corners:I

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    :cond_0
    return-object p0
.end method

.method public b(FFFF)Lmiuix/animation/i;
    .locals 1

    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr p1, v0

    float-to-int p1, p1

    mul-float/2addr p2, v0

    float-to-int p2, p2

    mul-float/2addr p3, v0

    float-to-int p3, p3

    mul-float/2addr p4, v0

    float-to-int p4, p4

    invoke-static {p1, p2, p3, p4}, Landroid/graphics/Color;->argb(IIII)I

    move-result p1

    invoke-virtual {p0, p1}, Lmiuix/animation/b/m;->b(I)Lmiuix/animation/i;

    return-object p0
.end method

.method public b(I)Lmiuix/animation/i;
    .locals 4

    sget-object v0, Lmiuix/animation/g/C;->b:Lmiuix/animation/g/C$a;

    iget-object v1, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v2, Lmiuix/animation/i$b;->a:Lmiuix/animation/i$b;

    invoke-interface {v1, v2}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v1

    int-to-double v2, p1

    invoke-virtual {v1, v0, v2, v3}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    iget-object p1, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v1, Lmiuix/animation/i$b;->b:Lmiuix/animation/i$b;

    invoke-interface {p1, v1}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object p1

    iget-object v1, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    invoke-interface {v1}, Lmiuix/animation/b/w;->getTarget()Lmiuix/animation/e;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-static {v1, v0, v2, v3}, Lmiuix/animation/d/l;->b(Lmiuix/animation/e;Lmiuix/animation/g/b;D)D

    move-result-wide v1

    double-to-int v1, v1

    int-to-double v1, v1

    invoke-virtual {p1, v0, v1, v2}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    return-object p0
.end method

.method public varargs b(Landroid/view/MotionEvent;[Lmiuix/animation/a/a;)V
    .locals 3

    iget-object v0, p0, Lmiuix/animation/b/m;->t:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/animation/b/m;->r:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lmiuix/animation/b/m;->n:[I

    invoke-static {v0, v1, p1}, Lmiuix/animation/b/m;->a(Landroid/view/View;[ILandroid/view/MotionEvent;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    new-array v0, p1, [Landroid/view/View;

    iget-object v1, p0, Lmiuix/animation/b/m;->t:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/h;->c()Lmiuix/animation/i;

    move-result-object v0

    new-array p1, p1, [Lmiuix/animation/a/a;

    iget-object v1, p0, Lmiuix/animation/b/m;->e:Lmiuix/animation/a/a;

    aput-object v1, p1, v2

    invoke-interface {v0, p1}, Lmiuix/animation/i;->f([Lmiuix/animation/a/a;)V

    :cond_0
    sget-object p1, Lmiuix/animation/i$b;->b:Lmiuix/animation/i$b;

    invoke-direct {p0, p1}, Lmiuix/animation/b/m;->b(Lmiuix/animation/i$b;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-boolean p1, p0, Lmiuix/animation/b/m;->j:Z

    if-eqz p1, :cond_1

    iget-object p1, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v0, Lmiuix/animation/i$b;->b:Lmiuix/animation/i$b;

    invoke-interface {p1, v0}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object p1

    sget-object v0, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    const-wide/16 v1, 0x0

    invoke-virtual {p1, v0, v1, v2}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v0, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    invoke-virtual {p1, v0, v1, v2}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    :cond_1
    invoke-virtual {p0, p2}, Lmiuix/animation/b/m;->c([Lmiuix/animation/a/a;)V

    return-void
.end method

.method public varargs b(Landroid/view/View;[Lmiuix/animation/a/a;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmiuix/animation/b/m;->c(Landroid/view/View;[Lmiuix/animation/a/a;)V

    return-void
.end method

.method public b(Z)V
    .locals 2

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    invoke-interface {v0}, Lmiuix/animation/b/w;->getTarget()Lmiuix/animation/e;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/animation/e;->e()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lmiuix/animation/b/m;->b(Landroid/view/View;Z)V

    :cond_0
    return-void
.end method

.method public varargs c([Lmiuix/animation/a/a;)V
    .locals 2

    invoke-direct {p0, p1}, Lmiuix/animation/b/m;->i([Lmiuix/animation/a/a;)[Lmiuix/animation/a/a;

    move-result-object p1

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v1, Lmiuix/animation/i$b;->b:Lmiuix/animation/i$b;

    invoke-interface {v0, v1}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    return-void
.end method

.method public e()Lmiuix/animation/i;
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/animation/b/m;->l:Z

    sget-object v0, Lmiuix/animation/g/C;->a:Lmiuix/animation/g/C$b;

    iget-object v1, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v2, Lmiuix/animation/i$b;->a:Lmiuix/animation/i$b;

    invoke-interface {v1, v2}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmiuix/animation/b/a;->e(Ljava/lang/Object;)Lmiuix/animation/b/a;

    iget-object v1, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v2, Lmiuix/animation/i$b;->b:Lmiuix/animation/i$b;

    invoke-interface {v1, v2}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmiuix/animation/b/a;->e(Ljava/lang/Object;)Lmiuix/animation/b/a;

    return-object p0
.end method

.method public varargs f([Lmiuix/animation/a/a;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lmiuix/animation/b/m;->a(Z[Lmiuix/animation/a/a;)V

    return-void
.end method

.method public f()Z
    .locals 2

    iget v0, p0, Lmiuix/animation/b/m;->p:I

    const/16 v1, 0x64

    if-ge v0, v1, :cond_1

    iget v0, p0, Lmiuix/animation/b/m;->q:I

    if-ge v0, v1, :cond_1

    iget-boolean v0, p0, Lmiuix/animation/b/m;->j:Z

    if-eqz v0, :cond_0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/animation/b/m;->i:Lmiuix/animation/i$a;

    sget-object v1, Lmiuix/animation/i$a;->b:Lmiuix/animation/i$a;

    if-eq v0, v1, :cond_0

    sget-object v1, Lmiuix/animation/i$a;->c:Lmiuix/animation/i$a;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setTint(I)Lmiuix/animation/i;
    .locals 4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/animation/b/m;->k:Z

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lmiuix/animation/b/m;->l:Z

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v1, Lmiuix/animation/i$b;->a:Lmiuix/animation/i$b;

    invoke-interface {v0, v1}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v0

    sget-object v1, Lmiuix/animation/g/C;->a:Lmiuix/animation/g/C$b;

    int-to-double v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    return-object p0
.end method
