.class final Lmiuix/animation/b/t$d;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/animation/b/t;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "d"
.end annotation


# instance fields
.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lmiuix/animation/b/t;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lmiuix/animation/b/p;)V
    .locals 0

    invoke-direct {p0}, Lmiuix/animation/b/t$d;-><init>()V

    return-void
.end method


# virtual methods
.method a(Lmiuix/animation/b/t;)V
    .locals 3

    goto/32 :goto_1

    nop

    :goto_0
    int-to-long v1, p1

    goto/32 :goto_4

    nop

    :goto_1
    iget-object v0, p1, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    goto/32 :goto_b

    nop

    :goto_2
    return-void

    :goto_3
    instance-of v1, v0, Lmiuix/animation/ViewTarget;

    goto/32 :goto_7

    nop

    :goto_4
    invoke-virtual {v0, p0, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_5
    goto/32 :goto_2

    nop

    :goto_6
    check-cast v0, Lmiuix/animation/ViewTarget;

    goto/32 :goto_c

    nop

    :goto_7
    if-nez v1, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_6

    nop

    :goto_8
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result p1

    goto/32 :goto_0

    nop

    :goto_9
    new-instance v1, Ljava/lang/ref/WeakReference;

    goto/32 :goto_e

    nop

    :goto_a
    if-nez v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_9

    nop

    :goto_b
    invoke-interface {v0}, Lmiuix/animation/b/w;->getTarget()Lmiuix/animation/e;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_c
    invoke-virtual {v0}, Lmiuix/animation/ViewTarget;->e()Landroid/view/View;

    move-result-object v0

    goto/32 :goto_a

    nop

    :goto_d
    iput-object v1, p0, Lmiuix/animation/b/t$d;->a:Ljava/lang/ref/WeakReference;

    goto/32 :goto_8

    nop

    :goto_e
    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    goto/32 :goto_d

    nop
.end method

.method b(Lmiuix/animation/b/t;)V
    .locals 1

    goto/32 :goto_9

    nop

    :goto_0
    if-nez p1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_5

    nop

    :goto_1
    instance-of v0, p1, Lmiuix/animation/ViewTarget;

    goto/32 :goto_7

    nop

    :goto_2
    return-void

    :goto_3
    check-cast p1, Lmiuix/animation/ViewTarget;

    goto/32 :goto_4

    nop

    :goto_4
    invoke-virtual {p1}, Lmiuix/animation/ViewTarget;->e()Landroid/view/View;

    move-result-object p1

    goto/32 :goto_0

    nop

    :goto_5
    invoke-virtual {p1, p0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    :goto_6
    goto/32 :goto_2

    nop

    :goto_7
    if-nez v0, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_3

    nop

    :goto_8
    invoke-interface {p1}, Lmiuix/animation/b/w;->getTarget()Lmiuix/animation/e;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_9
    iget-object p1, p1, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    goto/32 :goto_8

    nop
.end method

.method public run()V
    .locals 3

    iget-object v0, p0, Lmiuix/animation/b/t$d;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/animation/b/t;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    invoke-interface {v1}, Lmiuix/animation/b/w;->getTarget()Lmiuix/animation/e;

    move-result-object v1

    instance-of v2, v1, Lmiuix/animation/ViewTarget;

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lmiuix/animation/e;->e()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_0

    invoke-static {v0}, Lmiuix/animation/b/t;->b(Lmiuix/animation/b/t;)Landroid/view/View$OnLongClickListener;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->performLongClick()Z

    invoke-static {v0, v1}, Lmiuix/animation/b/t;->b(Lmiuix/animation/b/t;Landroid/view/View;)V

    :cond_0
    return-void
.end method
