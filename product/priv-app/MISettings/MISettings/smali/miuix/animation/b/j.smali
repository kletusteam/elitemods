.class public Lmiuix/animation/b/j;
.super Lmiuix/animation/b/b;

# interfaces
.implements Lmiuix/animation/n;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/animation/b/j$a;
    }
.end annotation


# instance fields
.field private b:Lmiuix/animation/c/a;

.field private c:I

.field private d:Lmiuix/animation/a/a;

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x0

    new-array v1, v0, [Lmiuix/animation/e;

    invoke-direct {p0, v1}, Lmiuix/animation/b/b;-><init>([Lmiuix/animation/e;)V

    new-instance v1, Lmiuix/animation/a/a;

    invoke-direct {v1}, Lmiuix/animation/a/a;-><init>()V

    iput-object v1, p0, Lmiuix/animation/b/j;->d:Lmiuix/animation/a/a;

    iget-object v1, p0, Lmiuix/animation/b/j;->d:Lmiuix/animation/a/a;

    const/4 v2, 0x3

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v2}, Lmiuix/animation/h/c;->c(I[F)Lmiuix/animation/h/c$a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lmiuix/animation/a/a;->a(Lmiuix/animation/h/c$a;)Lmiuix/animation/a/a;

    return-void

    nop

    :array_0
    .array-data 4
        0x43af0000    # 350.0f
        0x3f666666    # 0.9f
        0x3f5c28f6    # 0.86f
    .end array-data
.end method


# virtual methods
.method public a()V
    .locals 1

    invoke-super {p0}, Lmiuix/animation/b/b;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    iput-object v0, p0, Lmiuix/animation/b/j;->b:Lmiuix/animation/c/a;

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/animation/b/j;->c:I

    return-void
.end method

.method public varargs b(I[Lmiuix/animation/a/a;)Lmiuix/animation/n;
    .locals 4

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    if-eqz v0, :cond_2

    iget-boolean v1, p0, Lmiuix/animation/b/j;->e:Z

    const/4 v2, 0x1

    if-nez v1, :cond_0

    iput-boolean v2, p0, Lmiuix/animation/b/j;->e:Z

    sget-object v1, Lmiuix/animation/b/j$a;->a:Lmiuix/animation/b/j$a;

    invoke-interface {v0, v1}, Lmiuix/animation/k;->b(Ljava/lang/Object;)Lmiuix/animation/k;

    :cond_0
    new-array v0, v2, [Lmiuix/animation/a/a;

    const/4 v1, 0x0

    iget-object v2, p0, Lmiuix/animation/b/j;->d:Lmiuix/animation/a/a;

    aput-object v2, v0, v1

    invoke-static {p2, v0}, Lmiuix/animation/h/a;->a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [Lmiuix/animation/a/a;

    iget v0, p0, Lmiuix/animation/b/j;->c:I

    if-ne v0, p1, :cond_1

    iget-object p1, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v0, Lmiuix/animation/b/j$a;->a:Lmiuix/animation/b/j$a;

    invoke-interface {p1, v0, p2}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v1, Lmiuix/animation/b/j$a;->b:Lmiuix/animation/b/j$a;

    invoke-interface {v0, v1}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v0

    iget-object v1, p0, Lmiuix/animation/b/j;->b:Lmiuix/animation/c/a;

    int-to-double v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    iget-object p1, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v0, Lmiuix/animation/b/j$a;->b:Lmiuix/animation/b/j$a;

    invoke-interface {p1, v0, p2}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    :cond_2
    :goto_0
    return-object p0
.end method
