.class Lmiuix/animation/b/A;
.super Ljava/lang/Object;


# static fields
.field static final a:Lmiuix/animation/g/f;

.field static final b:Lmiuix/animation/g/e;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lmiuix/animation/g/f;

    const-string v1, "defaultProperty"

    invoke-direct {v0, v1}, Lmiuix/animation/g/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiuix/animation/b/A;->a:Lmiuix/animation/g/f;

    new-instance v0, Lmiuix/animation/g/e;

    const-string v1, "defaultIntProperty"

    invoke-direct {v0, v1}, Lmiuix/animation/g/e;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiuix/animation/b/A;->b:Lmiuix/animation/g/e;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/Object;Z)F
    .locals 0

    if-eqz p2, :cond_0

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-float p1, p1

    goto :goto_0

    :cond_0
    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    :goto_0
    return p1
.end method

.method private varargs a(Lmiuix/animation/e;Lmiuix/animation/b/a;Lmiuix/animation/a/b;Ljava/lang/Object;Ljava/lang/Object;I[Ljava/lang/Object;)I
    .locals 6

    invoke-direct {p0, p3, p4}, Lmiuix/animation/b/A;->b(Lmiuix/animation/a/b;Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_1

    invoke-direct {p0, p1, p4, p5}, Lmiuix/animation/b/A;->a(Lmiuix/animation/e;Ljava/lang/Object;Ljava/lang/Object;)Lmiuix/animation/g/b;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-direct {p0, v3}, Lmiuix/animation/b/A;->a(Lmiuix/animation/g/b;)Z

    move-result p3

    if-eqz p3, :cond_0

    goto :goto_0

    :cond_0
    add-int/lit8 p6, p6, 0x1

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p6

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lmiuix/animation/b/A;->a(Lmiuix/animation/e;Lmiuix/animation/b/a;Lmiuix/animation/g/b;I[Ljava/lang/Object;)I

    move-result p1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    if-lez p1, :cond_2

    add-int/2addr p6, p1

    goto :goto_2

    :cond_2
    add-int/lit8 p6, p6, 0x1

    :goto_2
    return p6
.end method

.method private varargs a(Lmiuix/animation/e;Lmiuix/animation/b/a;Lmiuix/animation/g/b;I[Ljava/lang/Object;)I
    .locals 2

    const/4 v0, 0x1

    if-eqz p3, :cond_0

    invoke-direct {p0, p4, p5}, Lmiuix/animation/b/A;->a(I[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p2, p3, v1}, Lmiuix/animation/b/A;->a(Lmiuix/animation/b/a;Lmiuix/animation/g/b;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    add-int/2addr p4, v0

    invoke-direct {p0, p1, p3, p4, p5}, Lmiuix/animation/b/A;->a(Lmiuix/animation/e;Lmiuix/animation/g/b;I[Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0
.end method

.method private varargs a(I[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    array-length v0, p2

    if-ge p1, v0, :cond_0

    aget-object p1, p2, p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private a(Lmiuix/animation/e;Ljava/lang/Object;Ljava/lang/Object;)Lmiuix/animation/g/b;
    .locals 2

    instance-of v0, p2, Lmiuix/animation/g/b;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v1, p2

    check-cast v1, Lmiuix/animation/g/b;

    goto :goto_0

    :cond_0
    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_2

    instance-of v0, p1, Lmiuix/animation/r;

    if-eqz v0, :cond_2

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    :cond_1
    check-cast p1, Lmiuix/animation/r;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p1, p2, v1}, Lmiuix/animation/r;->a(Ljava/lang/String;Ljava/lang/Class;)Lmiuix/animation/g/b;

    move-result-object v1

    goto :goto_0

    :cond_2
    instance-of p1, p2, Ljava/lang/Float;

    if-eqz p1, :cond_3

    sget-object v1, Lmiuix/animation/b/A;->a:Lmiuix/animation/g/f;

    :cond_3
    :goto_0
    return-object v1
.end method

.method private a(Lmiuix/animation/a/a;Ljava/lang/Object;)V
    .locals 2

    instance-of v0, p2, Lmiuix/animation/e/b;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Lmiuix/animation/e/b;

    const/4 v1, 0x0

    check-cast p2, Lmiuix/animation/e/b;

    aput-object p2, v0, v1

    invoke-virtual {p1, v0}, Lmiuix/animation/a/a;->a([Lmiuix/animation/e/b;)Lmiuix/animation/a/a;

    goto :goto_0

    :cond_0
    instance-of v0, p2, Lmiuix/animation/h/c$a;

    if-eqz v0, :cond_1

    check-cast p2, Lmiuix/animation/h/c$a;

    invoke-virtual {p1, p2}, Lmiuix/animation/a/a;->a(Lmiuix/animation/h/c$a;)Lmiuix/animation/a/a;

    :cond_1
    :goto_0
    return-void
.end method

.method private a(Lmiuix/animation/a/b;Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p2, Lmiuix/animation/a/a;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    check-cast p2, Lmiuix/animation/a/a;

    new-array v0, v1, [Z

    invoke-virtual {p1, p2, v0}, Lmiuix/animation/a/b;->a(Lmiuix/animation/a/a;[Z)V

    const/4 p1, 0x1

    return p1

    :cond_0
    instance-of v0, p2, Lmiuix/animation/a/b;

    if-eqz v0, :cond_1

    check-cast p2, Lmiuix/animation/a/b;

    new-array v0, v1, [Z

    invoke-virtual {p1, p2, v0}, Lmiuix/animation/a/b;->a(Lmiuix/animation/a/b;[Z)V

    :cond_1
    return v1
.end method

.method private a(Lmiuix/animation/b/a;Lmiuix/animation/g/b;Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p3, Ljava/lang/Integer;

    if-nez v0, :cond_1

    instance-of v1, p3, Ljava/lang/Float;

    if-nez v1, :cond_1

    instance-of v1, p3, Ljava/lang/Double;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    instance-of v1, p2, Lmiuix/animation/g/c;

    if-eqz v1, :cond_2

    invoke-direct {p0, p3, v0}, Lmiuix/animation/b/A;->b(Ljava/lang/Object;Z)I

    move-result p3

    int-to-double v0, p3

    invoke-virtual {p1, p2, v0, v1}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    goto :goto_1

    :cond_2
    invoke-direct {p0, p3, v0}, Lmiuix/animation/b/A;->a(Ljava/lang/Object;Z)F

    move-result p3

    float-to-double v0, p3

    invoke-virtual {p1, p2, v0, v1}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    :goto_1
    const/4 p1, 0x1

    return p1
.end method

.method private varargs a(Lmiuix/animation/e;Lmiuix/animation/g/b;I[Ljava/lang/Object;)Z
    .locals 2

    array-length v0, p4

    const/4 v1, 0x0

    if-lt p3, v0, :cond_0

    return v1

    :cond_0
    aget-object p3, p4, p3

    instance-of p4, p3, Ljava/lang/Float;

    if-eqz p4, :cond_1

    check-cast p3, Ljava/lang/Float;

    invoke-virtual {p3}, Ljava/lang/Float;->floatValue()F

    move-result p3

    float-to-double p3, p3

    invoke-virtual {p1, p2, p3, p4}, Lmiuix/animation/e;->a(Lmiuix/animation/g/b;D)V

    const/4 p1, 0x1

    return p1

    :cond_1
    return v1
.end method

.method private a(Lmiuix/animation/g/b;)Z
    .locals 1

    sget-object v0, Lmiuix/animation/b/A;->a:Lmiuix/animation/g/f;

    if-eq p1, v0, :cond_1

    sget-object v0, Lmiuix/animation/b/A;->b:Lmiuix/animation/g/e;

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private b(Ljava/lang/Object;Z)I
    .locals 0

    if-eqz p2, :cond_0

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_0

    :cond_0
    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    float-to-int p1, p1

    :goto_0
    return p1
.end method

.method private b(Lmiuix/animation/a/b;Ljava/lang/Object;)Z
    .locals 6

    instance-of v0, p2, Lmiuix/animation/e/b;

    const/4 v1, 0x1

    if-nez v0, :cond_5

    instance-of v0, p2, Lmiuix/animation/h/c$a;

    if-eqz v0, :cond_0

    goto :goto_3

    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p2}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v0

    const/4 v2, 0x0

    move v3, v2

    move v4, v3

    :goto_0
    if-ge v3, v0, :cond_3

    invoke-static {p2, v3}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v5

    invoke-direct {p0, p1, v5}, Lmiuix/animation/b/A;->a(Lmiuix/animation/a/b;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    if-eqz v4, :cond_1

    goto :goto_1

    :cond_1
    move v4, v2

    goto :goto_2

    :cond_2
    :goto_1
    move v4, v1

    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    return v4

    :cond_4
    invoke-direct {p0, p1, p2}, Lmiuix/animation/b/A;->a(Lmiuix/animation/a/b;Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_5
    :goto_3
    invoke-virtual {p1}, Lmiuix/animation/a/b;->b()Lmiuix/animation/a/a;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lmiuix/animation/b/A;->a(Lmiuix/animation/a/a;Ljava/lang/Object;)V

    return v1
.end method


# virtual methods
.method varargs a(Lmiuix/animation/e;Lmiuix/animation/b/a;Lmiuix/animation/a/b;[Ljava/lang/Object;)V
    .locals 9

    goto/32 :goto_b

    nop

    :goto_0
    aget-object v0, p4, v0

    goto/32 :goto_23

    nop

    :goto_1
    const/4 v1, 0x0

    :goto_2
    goto/32 :goto_24

    nop

    :goto_3
    if-eqz v0, :cond_0

    goto/32 :goto_21

    :cond_0
    goto/32 :goto_20

    nop

    :goto_4
    add-int/lit8 v0, v7, 0x1

    goto/32 :goto_8

    nop

    :goto_5
    return-void

    :goto_6
    move-object v2, p1

    goto/32 :goto_1c

    nop

    :goto_7
    move-object v4, p3

    goto/32 :goto_22

    nop

    :goto_8
    array-length v1, p4

    goto/32 :goto_1a

    nop

    :goto_9
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_a
    goto/32 :goto_c

    nop

    :goto_b
    array-length v0, p4

    goto/32 :goto_3

    nop

    :goto_c
    move v7, v0

    :goto_d
    goto/32 :goto_15

    nop

    :goto_e
    instance-of v1, v5, Ljava/lang/String;

    goto/32 :goto_19

    nop

    :goto_f
    const/4 v0, 0x0

    goto/32 :goto_0

    nop

    :goto_10
    if-lt v7, v0, :cond_1

    goto/32 :goto_14

    :cond_1
    goto/32 :goto_18

    nop

    :goto_11
    instance-of v1, v6, Ljava/lang/String;

    goto/32 :goto_1b

    nop

    :goto_12
    move-object v1, p0

    goto/32 :goto_6

    nop

    :goto_13
    goto :goto_d

    :goto_14
    goto/32 :goto_5

    nop

    :goto_15
    array-length v0, p4

    goto/32 :goto_10

    nop

    :goto_16
    goto :goto_a

    :goto_17
    goto/32 :goto_12

    nop

    :goto_18
    aget-object v5, p4, v7

    goto/32 :goto_4

    nop

    :goto_19
    if-nez v1, :cond_2

    goto/32 :goto_17

    :cond_2
    goto/32 :goto_11

    nop

    :goto_1a
    if-lt v0, v1, :cond_3

    goto/32 :goto_1f

    :cond_3
    goto/32 :goto_1d

    nop

    :goto_1b
    if-nez v1, :cond_4

    goto/32 :goto_17

    :cond_4
    goto/32 :goto_16

    nop

    :goto_1c
    move-object v3, p2

    goto/32 :goto_7

    nop

    :goto_1d
    aget-object v1, p4, v0

    goto/32 :goto_1e

    nop

    :goto_1e
    goto/16 :goto_2

    :goto_1f
    goto/32 :goto_1

    nop

    :goto_20
    return-void

    :goto_21
    goto/32 :goto_f

    nop

    :goto_22
    move-object v8, p4

    goto/32 :goto_25

    nop

    :goto_23
    invoke-virtual {p2}, Lmiuix/animation/b/a;->c()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_24
    move-object v6, v1

    goto/32 :goto_e

    nop

    :goto_25
    invoke-direct/range {v1 .. v8}, Lmiuix/animation/b/A;->a(Lmiuix/animation/e;Lmiuix/animation/b/a;Lmiuix/animation/a/b;Ljava/lang/Object;Ljava/lang/Object;I[Ljava/lang/Object;)I

    move-result v7

    goto/32 :goto_13

    nop
.end method
