.class public Lmiuix/animation/b/o;
.super Ljava/lang/Object;

# interfaces
.implements Lmiuix/animation/b/w;


# instance fields
.field a:Lmiuix/animation/e;

.field b:Lmiuix/animation/b/B;

.field c:Lmiuix/animation/a/b;

.field private d:Z


# direct methods
.method constructor <init>(Lmiuix/animation/e;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmiuix/animation/b/B;

    invoke-direct {v0}, Lmiuix/animation/b/B;-><init>()V

    iput-object v0, p0, Lmiuix/animation/b/o;->b:Lmiuix/animation/b/B;

    new-instance v0, Lmiuix/animation/a/b;

    invoke-direct {v0}, Lmiuix/animation/a/b;-><init>()V

    iput-object v0, p0, Lmiuix/animation/b/o;->c:Lmiuix/animation/a/b;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/animation/b/o;->d:Z

    iput-object p1, p0, Lmiuix/animation/b/o;->a:Lmiuix/animation/e;

    return-void
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;Lmiuix/animation/a/b;)Lmiuix/animation/k;
    .locals 3

    iget-boolean v0, p0, Lmiuix/animation/b/o;->d:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/animation/b/o;->b:Lmiuix/animation/b/B;

    invoke-virtual {v0, p2}, Lmiuix/animation/b/B;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lmiuix/animation/b/o;->b(Ljava/lang/Object;)Lmiuix/animation/k;

    :cond_0
    invoke-virtual {p0, p2}, Lmiuix/animation/b/o;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v0

    iget-object v1, p0, Lmiuix/animation/b/o;->b:Lmiuix/animation/b/B;

    invoke-virtual {v1, v0, p3}, Lmiuix/animation/b/B;->a(Lmiuix/animation/b/a;Lmiuix/animation/a/b;)V

    invoke-static {}, Lmiuix/animation/d/h;->e()Lmiuix/animation/d/h;

    move-result-object v1

    iget-object v2, p0, Lmiuix/animation/b/o;->a:Lmiuix/animation/e;

    invoke-virtual {p0, p1}, Lmiuix/animation/b/o;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object p1

    invoke-virtual {p0, p2}, Lmiuix/animation/b/o;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object p2

    invoke-virtual {v1, v2, p1, p2, p3}, Lmiuix/animation/d/h;->a(Lmiuix/animation/e;Lmiuix/animation/b/a;Lmiuix/animation/b/a;Lmiuix/animation/a/b;)V

    iget-object p1, p0, Lmiuix/animation/b/o;->b:Lmiuix/animation/b/B;

    invoke-virtual {p1, v0}, Lmiuix/animation/b/B;->b(Lmiuix/animation/b/a;)V

    invoke-virtual {p3}, Lmiuix/animation/a/b;->a()V

    :cond_1
    return-object p0
.end method

.method private a(Ljava/lang/Object;Lmiuix/animation/a/b;)Lmiuix/animation/k;
    .locals 2

    iget-object v0, p0, Lmiuix/animation/b/o;->a:Lmiuix/animation/e;

    if-nez v0, :cond_0

    return-object p0

    :cond_0
    instance-of v1, p1, Ljava/lang/Integer;

    if-nez v1, :cond_2

    instance-of v1, p1, Ljava/lang/Float;

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    new-instance v1, Lmiuix/animation/b/n;

    invoke-direct {v1, p0, p1, p2}, Lmiuix/animation/b/n;-><init>(Lmiuix/animation/b/o;Ljava/lang/Object;Lmiuix/animation/a/b;)V

    invoke-virtual {v0, v1}, Lmiuix/animation/e;->a(Ljava/lang/Runnable;)V

    return-object p0

    :cond_2
    :goto_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    invoke-virtual {p0, v0}, Lmiuix/animation/b/o;->d([Ljava/lang/Object;)Lmiuix/animation/k;

    return-object p0
.end method

.method private c()Lmiuix/animation/a/b;
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b/o;->c:Lmiuix/animation/a/b;

    return-object v0
.end method


# virtual methods
.method public a(J)Lmiuix/animation/k;
    .locals 1

    invoke-virtual {p0}, Lmiuix/animation/b/o;->getTarget()Lmiuix/animation/e;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lmiuix/animation/e;->b(J)V

    return-object p0
.end method

.method public a(Ljava/lang/Object;)Lmiuix/animation/k;
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b/o;->b:Lmiuix/animation/b/B;

    invoke-virtual {v0, p1}, Lmiuix/animation/b/B;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    return-object p0
.end method

.method public varargs a(Ljava/lang/Object;Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;
    .locals 6

    invoke-direct {p0}, Lmiuix/animation/b/o;->c()Lmiuix/animation/a/b;

    move-result-object v0

    array-length v1, p3

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_0

    aget-object v4, p3, v3

    new-array v5, v2, [Z

    invoke-virtual {v0, v4, v5}, Lmiuix/animation/a/b;->a(Lmiuix/animation/a/a;[Z)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1, p2, v0}, Lmiuix/animation/b/o;->a(Ljava/lang/Object;Ljava/lang/Object;Lmiuix/animation/a/b;)Lmiuix/animation/k;

    return-object p0
.end method

.method public varargs a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;
    .locals 3

    instance-of v0, p1, Lmiuix/animation/b/a;

    if-nez v0, :cond_2

    iget-object v0, p0, Lmiuix/animation/b/o;->b:Lmiuix/animation/b/B;

    invoke-virtual {v0, p1}, Lmiuix/animation/b/B;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-static {p1}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v0

    array-length v2, p2

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length p1, p2

    invoke-static {p2, v1, v2, v0, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {p0, v2}, Lmiuix/animation/b/o;->c([Ljava/lang/Object;)Lmiuix/animation/k;

    return-object p0

    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    invoke-virtual {p0, v0}, Lmiuix/animation/b/o;->c([Ljava/lang/Object;)Lmiuix/animation/k;

    return-object p0

    :cond_2
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Lmiuix/animation/b/o;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object p1

    invoke-virtual {p0, v0, p1, p2}, Lmiuix/animation/b/o;->a(Ljava/lang/Object;Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    return-object p0
.end method

.method public a(Ljava/lang/String;I)Lmiuix/animation/k;
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b/o;->b:Lmiuix/animation/b/B;

    invoke-virtual {v0, p1, p2}, Lmiuix/animation/b/B;->a(Ljava/lang/String;I)V

    return-object p0
.end method

.method public a(Lmiuix/animation/g/b;F)Lmiuix/animation/k;
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b/o;->b:Lmiuix/animation/b/B;

    invoke-virtual {v0, p1, p2}, Lmiuix/animation/b/B;->a(Lmiuix/animation/g/b;F)V

    return-object p0
.end method

.method public varargs a([Lmiuix/animation/a/a;)Lmiuix/animation/k;
    .locals 1

    invoke-virtual {p0}, Lmiuix/animation/b/o;->b()Lmiuix/animation/b/a;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lmiuix/animation/b/o;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    return-object p0
.end method

.method public a()V
    .locals 0

    invoke-virtual {p0}, Lmiuix/animation/b/o;->cancel()V

    return-void
.end method

.method public a(Lmiuix/animation/b/a;)V
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b/o;->b:Lmiuix/animation/b/B;

    invoke-virtual {v0, p1}, Lmiuix/animation/b/B;->a(Lmiuix/animation/b/a;)V

    return-void
.end method

.method public varargs a([Ljava/lang/Object;)V
    .locals 3

    array-length v0, p1

    if-lez v0, :cond_1

    const/4 v0, 0x0

    aget-object v1, p1, v0

    instance-of v1, v1, Lmiuix/animation/g/b;

    if-eqz v1, :cond_0

    array-length v1, p1

    new-array v1, v1, [Lmiuix/animation/g/b;

    array-length v2, p1

    invoke-static {p1, v0, v1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {}, Lmiuix/animation/d/h;->e()Lmiuix/animation/d/h;

    move-result-object p1

    iget-object v0, p0, Lmiuix/animation/b/o;->a:Lmiuix/animation/e;

    invoke-virtual {p1, v0, v1}, Lmiuix/animation/d/h;->b(Lmiuix/animation/e;[Lmiuix/animation/g/b;)V

    goto :goto_0

    :cond_0
    array-length v1, p1

    new-array v1, v1, [Ljava/lang/String;

    array-length v2, p1

    invoke-static {p1, v0, v1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {}, Lmiuix/animation/d/h;->e()Lmiuix/animation/d/h;

    move-result-object p1

    iget-object v0, p0, Lmiuix/animation/b/o;->a:Lmiuix/animation/e;

    invoke-virtual {p1, v0, v1}, Lmiuix/animation/d/h;->a(Lmiuix/animation/e;[Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public varargs a([Lmiuix/animation/g/b;)V
    .locals 2

    invoke-static {}, Lmiuix/animation/d/h;->e()Lmiuix/animation/d/h;

    move-result-object v0

    iget-object v1, p0, Lmiuix/animation/b/o;->a:Lmiuix/animation/e;

    invoke-virtual {v0, v1, p1}, Lmiuix/animation/d/h;->a(Lmiuix/animation/e;[Lmiuix/animation/g/b;)V

    return-void
.end method

.method public varargs b([Ljava/lang/Object;)J
    .locals 4

    invoke-virtual {p0}, Lmiuix/animation/b/o;->getTarget()Lmiuix/animation/e;

    move-result-object v0

    invoke-direct {p0}, Lmiuix/animation/b/o;->c()Lmiuix/animation/a/b;

    move-result-object v1

    iget-object v2, p0, Lmiuix/animation/b/o;->b:Lmiuix/animation/b/B;

    invoke-virtual {v2, v0, v1, p1}, Lmiuix/animation/b/B;->b(Lmiuix/animation/e;Lmiuix/animation/a/b;[Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object p1

    const/4 v2, 0x0

    invoke-static {v0, v2, p1, v1}, Lmiuix/animation/d/o;->a(Lmiuix/animation/e;Lmiuix/animation/b/a;Lmiuix/animation/b/a;Lmiuix/animation/a/b;)J

    move-result-wide v2

    iget-object v0, p0, Lmiuix/animation/b/o;->b:Lmiuix/animation/b/B;

    invoke-virtual {v0, p1}, Lmiuix/animation/b/B;->b(Lmiuix/animation/b/a;)V

    invoke-virtual {v1}, Lmiuix/animation/a/b;->a()V

    return-wide v2
.end method

.method public b()Lmiuix/animation/b/a;
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b/o;->b:Lmiuix/animation/b/B;

    invoke-virtual {v0}, Lmiuix/animation/b/B;->a()Lmiuix/animation/b/a;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;)Lmiuix/animation/k;
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lmiuix/animation/a/a;

    invoke-virtual {p0, p1, v0}, Lmiuix/animation/b/o;->b(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    return-object p0
.end method

.method public varargs b(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;
    .locals 0

    invoke-static {p2}, Lmiuix/animation/a/b;->a([Lmiuix/animation/a/a;)Lmiuix/animation/a/b;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lmiuix/animation/b/o;->a(Ljava/lang/Object;Lmiuix/animation/a/b;)Lmiuix/animation/k;

    return-object p0
.end method

.method public c(Ljava/lang/Object;)Lmiuix/animation/b/a;
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b/o;->b:Lmiuix/animation/b/B;

    invoke-virtual {v0, p1}, Lmiuix/animation/b/B;->a(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object p1

    return-object p1
.end method

.method public varargs c([Ljava/lang/Object;)Lmiuix/animation/k;
    .locals 3

    iget-object v0, p0, Lmiuix/animation/b/o;->b:Lmiuix/animation/b/B;

    invoke-virtual {p0}, Lmiuix/animation/b/o;->getTarget()Lmiuix/animation/e;

    move-result-object v1

    invoke-direct {p0}, Lmiuix/animation/b/o;->c()Lmiuix/animation/a/b;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Lmiuix/animation/b/B;->b(Lmiuix/animation/e;Lmiuix/animation/a/b;[Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Lmiuix/animation/a/a;

    const/4 v1, 0x0

    invoke-virtual {p0, v1, p1, v0}, Lmiuix/animation/b/o;->a(Ljava/lang/Object;Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    return-object p0
.end method

.method public cancel()V
    .locals 3

    invoke-static {}, Lmiuix/animation/d/h;->e()Lmiuix/animation/d/h;

    move-result-object v0

    iget-object v1, p0, Lmiuix/animation/b/o;->a:Lmiuix/animation/e;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lmiuix/animation/d/h;->a(Lmiuix/animation/e;[Lmiuix/animation/g/b;)V

    return-void
.end method

.method public varargs d([Ljava/lang/Object;)Lmiuix/animation/k;
    .locals 3

    invoke-direct {p0}, Lmiuix/animation/b/o;->c()Lmiuix/animation/a/b;

    move-result-object v0

    iget-object v1, p0, Lmiuix/animation/b/o;->b:Lmiuix/animation/b/B;

    invoke-virtual {p0}, Lmiuix/animation/b/o;->getTarget()Lmiuix/animation/e;

    move-result-object v2

    invoke-virtual {v1, v2, v0, p1}, Lmiuix/animation/b/B;->a(Lmiuix/animation/e;Lmiuix/animation/a/b;[Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object p1

    invoke-direct {p0, p1, v0}, Lmiuix/animation/b/o;->a(Ljava/lang/Object;Lmiuix/animation/a/b;)Lmiuix/animation/k;

    return-object p0
.end method

.method public getTarget()Lmiuix/animation/e;
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b/o;->a:Lmiuix/animation/e;

    return-object v0
.end method
