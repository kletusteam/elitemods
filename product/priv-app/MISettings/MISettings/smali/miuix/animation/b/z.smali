.class public Lmiuix/animation/b/z;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lmiuix/animation/h/l$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiuix/animation/h/l$a<",
            "Lmiuix/animation/b/w;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmiuix/animation/b/y;

    invoke-direct {v0}, Lmiuix/animation/b/y;-><init>()V

    sput-object v0, Lmiuix/animation/b/z;->a:Lmiuix/animation/h/l$a;

    return-void
.end method

.method public static varargs a([Lmiuix/animation/e;)Lmiuix/animation/b/w;
    .locals 4

    if-eqz p0, :cond_3

    array-length v0, p0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    new-instance v0, Lmiuix/animation/b/o;

    aget-object p0, p0, v1

    invoke-direct {v0, p0}, Lmiuix/animation/b/o;-><init>(Lmiuix/animation/e;)V

    return-object v0

    :cond_1
    array-length v0, p0

    new-array v0, v0, [Lmiuix/animation/b/o;

    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_2

    new-instance v2, Lmiuix/animation/b/o;

    aget-object v3, p0, v1

    invoke-direct {v2, v3}, Lmiuix/animation/b/o;-><init>(Lmiuix/animation/e;)V

    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const-class p0, Lmiuix/animation/b/w;

    sget-object v1, Lmiuix/animation/b/z;->a:Lmiuix/animation/h/l$a;

    invoke-static {p0, v1, v0}, Lmiuix/animation/h/l;->a(Ljava/lang/Class;Lmiuix/animation/h/l$a;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lmiuix/animation/b/w;

    return-object p0

    :cond_3
    :goto_1
    const/4 p0, 0x0

    return-object p0
.end method
