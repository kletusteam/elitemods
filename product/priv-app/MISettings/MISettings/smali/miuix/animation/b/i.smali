.class public Lmiuix/animation/b/i;
.super Lmiuix/animation/b/b;

# interfaces
.implements Lmiuix/animation/f;


# instance fields
.field private b:J

.field private c:I

.field private d:Lmiuix/animation/a/a;

.field private e:Lmiuix/animation/a/a;

.field private f:Lmiuix/animation/a/a;

.field private g:Ljava/lang/Runnable;

.field h:I


# direct methods
.method public varargs constructor <init>([Lmiuix/animation/e;)V
    .locals 5

    invoke-direct {p0, p1}, Lmiuix/animation/b/b;-><init>([Lmiuix/animation/e;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmiuix/animation/b/i;->b:J

    const/4 p1, 0x1

    iput p1, p0, Lmiuix/animation/b/i;->c:I

    new-instance v0, Lmiuix/animation/a/a;

    invoke-direct {v0}, Lmiuix/animation/a/a;-><init>()V

    new-array v1, p1, [F

    const/4 v2, 0x0

    const/high16 v3, 0x44160000    # 600.0f

    aput v3, v1, v2

    const/4 v3, 0x6

    invoke-virtual {v0, v3, v1}, Lmiuix/animation/a/a;->a(I[F)Lmiuix/animation/a/a;

    iput-object v0, p0, Lmiuix/animation/b/i;->d:Lmiuix/animation/a/a;

    new-instance v0, Lmiuix/animation/a/a;

    invoke-direct {v0}, Lmiuix/animation/a/a;-><init>()V

    new-array v1, p1, [F

    const/high16 v4, 0x43c80000    # 400.0f

    aput v4, v1, v2

    const/16 v4, 0x10

    invoke-virtual {v0, v4, v1}, Lmiuix/animation/a/a;->a(I[F)Lmiuix/animation/a/a;

    iput-object v0, p0, Lmiuix/animation/b/i;->e:Lmiuix/animation/a/a;

    new-instance v0, Lmiuix/animation/a/a;

    invoke-direct {v0}, Lmiuix/animation/a/a;-><init>()V

    new-array v1, p1, [F

    const/high16 v4, 0x42c80000    # 100.0f

    aput v4, v1, v2

    invoke-virtual {v0, v3, v1}, Lmiuix/animation/a/a;->a(I[F)Lmiuix/animation/a/a;

    iput-object v0, p0, Lmiuix/animation/b/i;->f:Lmiuix/animation/a/a;

    new-instance v0, Lmiuix/animation/b/c;

    invoke-direct {v0, p0}, Lmiuix/animation/b/c;-><init>(Lmiuix/animation/b/i;)V

    iput-object v0, p0, Lmiuix/animation/b/i;->g:Ljava/lang/Runnable;

    iput v2, p0, Lmiuix/animation/b/i;->h:I

    invoke-direct {p0}, Lmiuix/animation/b/i;->e()V

    iget-object v0, p0, Lmiuix/animation/b/i;->e:Lmiuix/animation/a/a;

    new-array v1, p1, [Lmiuix/animation/e/b;

    new-instance v3, Lmiuix/animation/b/f;

    invoke-direct {v3, p0}, Lmiuix/animation/b/f;-><init>(Lmiuix/animation/b/i;)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lmiuix/animation/a/a;->a([Lmiuix/animation/e/b;)Lmiuix/animation/a/a;

    iget-object v0, p0, Lmiuix/animation/b/i;->d:Lmiuix/animation/a/a;

    new-array v1, p1, [Lmiuix/animation/e/b;

    new-instance v3, Lmiuix/animation/b/g;

    invoke-direct {v3, p0}, Lmiuix/animation/b/g;-><init>(Lmiuix/animation/b/i;)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lmiuix/animation/a/a;->a([Lmiuix/animation/e/b;)Lmiuix/animation/a/a;

    iget-object v0, p0, Lmiuix/animation/b/i;->f:Lmiuix/animation/a/a;

    new-array p1, p1, [Lmiuix/animation/e/b;

    new-instance v1, Lmiuix/animation/b/h;

    invoke-direct {v1, p0}, Lmiuix/animation/b/h;-><init>(Lmiuix/animation/b/i;)V

    aput-object v1, p1, v2

    invoke-virtual {v0, p1}, Lmiuix/animation/a/a;->a([Lmiuix/animation/e/b;)Lmiuix/animation/a/a;

    return-void
.end method

.method static synthetic a(Lmiuix/animation/b/i;)Lmiuix/animation/a/a;
    .locals 0

    iget-object p0, p0, Lmiuix/animation/b/i;->d:Lmiuix/animation/a/a;

    return-object p0
.end method

.method static synthetic b(Lmiuix/animation/b/i;)Lmiuix/animation/a/a;
    .locals 0

    iget-object p0, p0, Lmiuix/animation/b/i;->e:Lmiuix/animation/a/a;

    return-object p0
.end method

.method static synthetic c(Lmiuix/animation/b/i;)I
    .locals 0

    iget p0, p0, Lmiuix/animation/b/i;->c:I

    return p0
.end method

.method private e()V
    .locals 5

    const/4 v0, 0x0

    const/16 v1, 0x14

    invoke-static {v1, v0, v0, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    iget-object v1, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    invoke-interface {v1}, Lmiuix/animation/b/w;->getTarget()Lmiuix/animation/e;

    move-result-object v1

    invoke-virtual {v1}, Lmiuix/animation/e;->e()Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Landroid/view/View;

    if-eqz v2, :cond_0

    check-cast v1, Landroid/view/View;

    sget v0, Ld/f/a;->miuix_folme_color_blink_tint:I

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :cond_0
    sget-object v1, Lmiuix/animation/g/C;->a:Lmiuix/animation/g/C$b;

    iget-object v2, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v3, Lmiuix/animation/f$a;->a:Lmiuix/animation/f$a;

    invoke-interface {v2, v3}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v2

    int-to-double v3, v0

    invoke-virtual {v2, v1, v3, v4}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v2, Lmiuix/animation/f$a;->b:Lmiuix/animation/f$a;

    invoke-interface {v0, v2}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    return-void
.end method


# virtual methods
.method public a(Lmiuix/animation/a/a;)Lmiuix/animation/f;
    .locals 3

    iput-object p1, p0, Lmiuix/animation/b/i;->d:Lmiuix/animation/a/a;

    iget-object p1, p0, Lmiuix/animation/b/i;->d:Lmiuix/animation/a/a;

    const/4 v0, 0x1

    new-array v0, v0, [Lmiuix/animation/e/b;

    new-instance v1, Lmiuix/animation/b/d;

    invoke-direct {v1, p0}, Lmiuix/animation/b/d;-><init>(Lmiuix/animation/b/i;)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p1, v0}, Lmiuix/animation/a/a;->a([Lmiuix/animation/e/b;)Lmiuix/animation/a/a;

    return-object p0
.end method

.method public varargs a(I[Lmiuix/animation/a/a;)V
    .locals 2

    iput p1, p0, Lmiuix/animation/b/i;->c:I

    array-length p1, p2

    const/4 v0, 0x0

    if-lez p1, :cond_0

    aget-object p1, p2, v0

    invoke-virtual {p0, p1}, Lmiuix/animation/b/i;->a(Lmiuix/animation/a/a;)Lmiuix/animation/f;

    array-length p1, p2

    const/4 v1, 0x1

    if-le p1, v1, :cond_0

    aget-object p1, p2, v1

    invoke-virtual {p0, p1}, Lmiuix/animation/b/i;->b(Lmiuix/animation/a/a;)Lmiuix/animation/f;

    :cond_0
    new-array p1, v0, [Lmiuix/animation/a/a;

    invoke-virtual {p0, p1}, Lmiuix/animation/b/i;->h([Lmiuix/animation/a/a;)V

    return-void
.end method

.method public b(Lmiuix/animation/a/a;)Lmiuix/animation/f;
    .locals 3

    iput-object p1, p0, Lmiuix/animation/b/i;->e:Lmiuix/animation/a/a;

    iget-object p1, p0, Lmiuix/animation/b/i;->e:Lmiuix/animation/a/a;

    const/4 v0, 0x1

    new-array v0, v0, [Lmiuix/animation/e/b;

    new-instance v1, Lmiuix/animation/b/e;

    invoke-direct {v1, p0}, Lmiuix/animation/b/e;-><init>(Lmiuix/animation/b/i;)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p1, v0}, Lmiuix/animation/a/a;->a([Lmiuix/animation/e/b;)Lmiuix/animation/a/a;

    return-object p0
.end method

.method public c()V
    .locals 5

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    invoke-interface {v0}, Lmiuix/animation/b/w;->getTarget()Lmiuix/animation/e;

    move-result-object v0

    iget-object v0, v0, Lmiuix/animation/e;->b:Lmiuix/animation/d/s;

    iget-object v1, p0, Lmiuix/animation/b/i;->g:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v1, Lmiuix/animation/f$a;->b:Lmiuix/animation/f$a;

    invoke-interface {v0, v1}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lmiuix/animation/a/a;

    iget-object v3, p0, Lmiuix/animation/b/i;->f:Lmiuix/animation/a/a;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-interface {v0, v1, v2}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    return-void
.end method

.method public varargs h([Lmiuix/animation/a/a;)V
    .locals 3

    array-length v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {p0, v0}, Lmiuix/animation/b/i;->a(Lmiuix/animation/a/a;)Lmiuix/animation/f;

    array-length v0, p1

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    aget-object p1, p1, v1

    invoke-virtual {p0, p1}, Lmiuix/animation/b/i;->b(Lmiuix/animation/a/a;)Lmiuix/animation/f;

    :cond_0
    iget-object p1, p0, Lmiuix/animation/b/i;->g:Ljava/lang/Runnable;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    invoke-interface {p1}, Lmiuix/animation/b/w;->getTarget()Lmiuix/animation/e;

    move-result-object p1

    iget-object p1, p1, Lmiuix/animation/e;->b:Lmiuix/animation/d/s;

    iget-object v0, p0, Lmiuix/animation/b/i;->g:Ljava/lang/Runnable;

    iget v1, p0, Lmiuix/animation/b/i;->h:I

    if-nez v1, :cond_1

    const-wide/16 v1, 0x0

    goto :goto_0

    :cond_1
    iget-wide v1, p0, Lmiuix/animation/b/i;->b:J

    :goto_0
    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_2
    return-void
.end method
