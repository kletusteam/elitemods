.class public Lmiuix/animation/b/t;
.super Lmiuix/animation/b/b;

# interfaces
.implements Lmiuix/animation/m;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/animation/b/t$d;,
        Lmiuix/animation/b/t$b;,
        Lmiuix/animation/b/t$a;,
        Lmiuix/animation/b/t$c;
    }
.end annotation


# static fields
.field private static b:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap<",
            "Landroid/view/View;",
            "Lmiuix/animation/b/t$b;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:Lmiuix/animation/b/j;

.field private d:I

.field private e:I

.field private f:Landroid/view/View$OnClickListener;

.field private g:Landroid/view/View$OnLongClickListener;

.field private h:I

.field private i:F

.field private j:F

.field private k:Z

.field private l:Z

.field private m:[I

.field private n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lmiuix/animation/m$b;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private q:F

.field private r:Lmiuix/animation/a/a;

.field private s:Lmiuix/animation/a/a;

.field private t:Z

.field private u:Z

.field private v:Lmiuix/animation/e/b;

.field private w:Lmiuix/animation/b/t$d;

.field private x:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lmiuix/animation/b/t;->b:Ljava/util/WeakHashMap;

    return-void
.end method

.method public varargs constructor <init>([Lmiuix/animation/e;)V
    .locals 6

    invoke-direct {p0, p1}, Lmiuix/animation/b/b;-><init>([Lmiuix/animation/e;)V

    const/4 v0, 0x2

    new-array v1, v0, [I

    iput-object v1, p0, Lmiuix/animation/b/t;->m:[I

    new-instance v1, Landroid/util/ArrayMap;

    invoke-direct {v1}, Landroid/util/ArrayMap;-><init>()V

    iput-object v1, p0, Lmiuix/animation/b/t;->n:Ljava/util/Map;

    new-instance v1, Lmiuix/animation/a/a;

    invoke-direct {v1}, Lmiuix/animation/a/a;-><init>()V

    iput-object v1, p0, Lmiuix/animation/b/t;->r:Lmiuix/animation/a/a;

    new-instance v1, Lmiuix/animation/a/a;

    invoke-direct {v1}, Lmiuix/animation/a/a;-><init>()V

    iput-object v1, p0, Lmiuix/animation/b/t;->s:Lmiuix/animation/a/a;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmiuix/animation/b/t;->u:Z

    new-instance v2, Lmiuix/animation/b/p;

    invoke-direct {v2, p0}, Lmiuix/animation/b/p;-><init>(Lmiuix/animation/b/t;)V

    iput-object v2, p0, Lmiuix/animation/b/t;->v:Lmiuix/animation/e/b;

    array-length v2, p1

    if-lez v2, :cond_0

    aget-object p1, p1, v1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-direct {p0, p1}, Lmiuix/animation/b/t;->a(Lmiuix/animation/e;)V

    sget-object p1, Lmiuix/animation/g/A;->e:Lmiuix/animation/g/A;

    sget-object v2, Lmiuix/animation/g/A;->f:Lmiuix/animation/g/A;

    iget-object v3, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v4, Lmiuix/animation/m$b;->a:Lmiuix/animation/m$b;

    invoke-interface {v3, v4}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v3

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v3, p1, v4, v5}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    invoke-virtual {v3, v2, v4, v5}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    invoke-direct {p0}, Lmiuix/animation/b/t;->f()V

    iget-object p1, p0, Lmiuix/animation/b/t;->r:Lmiuix/animation/a/a;

    new-array v2, v0, [F

    fill-array-data v2, :array_0

    const/4 v3, -0x2

    invoke-static {v3, v2}, Lmiuix/animation/h/c;->c(I[F)Lmiuix/animation/h/c$a;

    move-result-object v2

    invoke-virtual {p1, v2}, Lmiuix/animation/a/a;->a(Lmiuix/animation/h/c$a;)Lmiuix/animation/a/a;

    iget-object p1, p0, Lmiuix/animation/b/t;->r:Lmiuix/animation/a/a;

    const/4 v2, 0x1

    new-array v2, v2, [Lmiuix/animation/e/b;

    iget-object v4, p0, Lmiuix/animation/b/t;->v:Lmiuix/animation/e/b;

    aput-object v4, v2, v1

    invoke-virtual {p1, v2}, Lmiuix/animation/a/a;->a([Lmiuix/animation/e/b;)Lmiuix/animation/a/a;

    iget-object p1, p0, Lmiuix/animation/b/t;->s:Lmiuix/animation/a/a;

    new-array v1, v0, [F

    fill-array-data v1, :array_1

    invoke-virtual {p1, v3, v1}, Lmiuix/animation/a/a;->a(I[F)Lmiuix/animation/a/a;

    sget-object v1, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    const-wide/16 v2, -0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_2

    invoke-virtual {p1, v1, v2, v3, v0}, Lmiuix/animation/a/a;->a(Lmiuix/animation/g/b;J[F)Lmiuix/animation/a/a;

    return-void

    :array_0
    .array-data 4
        0x3f7d70a4    # 0.99f
        0x3e19999a    # 0.15f
    .end array-data

    :array_1
    .array-data 4
        0x3f7d70a4    # 0.99f
        0x3e99999a    # 0.3f
    .end array-data

    :array_2
    .array-data 4
        0x3f666666    # 0.9f
        0x3e4ccccd    # 0.2f
    .end array-data
.end method

.method private a(Ljava/lang/ref/WeakReference;)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_0
    return-object p1
.end method

.method private a(Landroid/view/View;)Lmiuix/animation/b/t$c;
    .locals 4

    new-instance v0, Lmiuix/animation/b/t$c;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmiuix/animation/b/t$c;-><init>(Lmiuix/animation/b/p;)V

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_2

    instance-of v3, v2, Landroid/widget/AbsListView;

    if-eqz v3, :cond_0

    move-object v1, v2

    check-cast v1, Landroid/widget/AbsListView;

    goto :goto_1

    :cond_0
    instance-of v3, v2, Landroid/view/View;

    if-eqz v3, :cond_1

    move-object p1, v2

    check-cast p1, Landroid/view/View;

    :cond_1
    invoke-interface {v2}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    goto :goto_0

    :cond_2
    :goto_1
    if-eqz v1, :cond_3

    new-instance v2, Ljava/lang/ref/WeakReference;

    iget-object v3, v0, Lmiuix/animation/b/t$c;->a:Landroid/widget/AbsListView;

    invoke-direct {v2, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, p0, Lmiuix/animation/b/t;->p:Ljava/lang/ref/WeakReference;

    iput-object v1, v0, Lmiuix/animation/b/t$c;->a:Landroid/widget/AbsListView;

    iput-object p1, v0, Lmiuix/animation/b/t$c;->b:Landroid/view/View;

    :cond_3
    return-object v0
.end method

.method public static a(Landroid/widget/AbsListView;)Lmiuix/animation/b/x;
    .locals 1

    sget v0, Ld/f/b;->miuix_animation_tag_touch_listener:I

    invoke-virtual {p0, v0}, Landroid/widget/AbsListView;->getTag(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lmiuix/animation/b/x;

    return-object p0
.end method

.method private varargs a([Lmiuix/animation/m$b;)Lmiuix/animation/m$b;
    .locals 1

    array-length v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x0

    aget-object p1, p1, v0

    goto :goto_0

    :cond_0
    sget-object p1, Lmiuix/animation/m$b;->b:Lmiuix/animation/m$b;

    :goto_0
    return-object p1
.end method

.method private varargs a(Landroid/view/MotionEvent;Landroid/view/View;[Lmiuix/animation/a/a;)V
    .locals 1

    iget-boolean v0, p0, Lmiuix/animation/b/t;->l:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/animation/b/t;->m:[I

    invoke-static {p2, v0, p1}, Lmiuix/animation/b/t;->a(Landroid/view/View;[ILandroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p3}, Lmiuix/animation/b/t;->g([Lmiuix/animation/a/a;)V

    invoke-direct {p0}, Lmiuix/animation/b/t;->e()V

    goto :goto_0

    :cond_0
    iget-object p3, p0, Lmiuix/animation/b/t;->w:Lmiuix/animation/b/t$d;

    if-eqz p3, :cond_1

    invoke-direct {p0, p2, p1}, Lmiuix/animation/b/t;->b(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lmiuix/animation/b/t;->w:Lmiuix/animation/b/t$d;

    invoke-virtual {p1, p0}, Lmiuix/animation/b/t$d;->b(Lmiuix/animation/b/t;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private a(Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V
    .locals 3

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    invoke-interface {v0}, Lmiuix/animation/b/w;->getTarget()Lmiuix/animation/e;

    move-result-object v0

    instance-of v1, v0, Lmiuix/animation/ViewTarget;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    check-cast v0, Lmiuix/animation/ViewTarget;

    invoke-virtual {v0}, Lmiuix/animation/ViewTarget;->e()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v2

    :goto_0
    if-nez v0, :cond_1

    return-void

    :cond_1
    iget-object v1, p0, Lmiuix/animation/b/t;->f:Landroid/view/View$OnClickListener;

    if-eqz v1, :cond_2

    if-nez p1, :cond_2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_2
    if-eqz p1, :cond_3

    new-instance v1, Lmiuix/animation/b/r;

    invoke-direct {v1, p0}, Lmiuix/animation/b/r;-><init>(Lmiuix/animation/b/t;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    :goto_1
    iput-object p1, p0, Lmiuix/animation/b/t;->f:Landroid/view/View$OnClickListener;

    iget-object p1, p0, Lmiuix/animation/b/t;->g:Landroid/view/View$OnLongClickListener;

    if-eqz p1, :cond_4

    if-nez p2, :cond_4

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto :goto_2

    :cond_4
    if-eqz p2, :cond_5

    new-instance p1, Lmiuix/animation/b/s;

    invoke-direct {p1, p0}, Lmiuix/animation/b/s;-><init>(Lmiuix/animation/b/t;)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_5
    :goto_2
    iput-object p2, p0, Lmiuix/animation/b/t;->g:Landroid/view/View$OnLongClickListener;

    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 2

    iget-boolean v0, p0, Lmiuix/animation/b/t;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/animation/b/t;->f:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget v0, p0, Lmiuix/animation/b/t;->h:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    invoke-interface {v0}, Lmiuix/animation/b/w;->getTarget()Lmiuix/animation/e;

    move-result-object v0

    instance-of v1, v0, Lmiuix/animation/ViewTarget;

    if-eqz v1, :cond_0

    invoke-direct {p0, p1, p2}, Lmiuix/animation/b/t;->b(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result p1

    if-eqz p1, :cond_0

    check-cast v0, Lmiuix/animation/ViewTarget;

    invoke-virtual {v0}, Lmiuix/animation/ViewTarget;->e()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->performClick()Z

    invoke-direct {p0, p1}, Lmiuix/animation/b/t;->b(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method private varargs a(Landroid/view/View;Landroid/view/MotionEvent;[Lmiuix/animation/a/a;)V
    .locals 2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0, p2, p1, p3}, Lmiuix/animation/b/t;->a(Landroid/view/MotionEvent;Landroid/view/View;[Lmiuix/animation/a/a;)V

    goto :goto_1

    :cond_1
    invoke-direct {p0, p1, p2}, Lmiuix/animation/b/t;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    :goto_0
    invoke-direct {p0, p3}, Lmiuix/animation/b/t;->k([Lmiuix/animation/a/a;)V

    goto :goto_1

    :cond_2
    invoke-direct {p0, p2}, Lmiuix/animation/b/t;->b(Landroid/view/MotionEvent;)V

    invoke-direct {p0, p3}, Lmiuix/animation/b/t;->j([Lmiuix/animation/a/a;)V

    :goto_1
    return-void
.end method

.method private varargs a(Landroid/view/View;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Z[Lmiuix/animation/a/a;)V
    .locals 6

    invoke-direct {p0, p2, p3}, Lmiuix/animation/b/t;->a(Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V

    invoke-direct {p0, p1, p5}, Lmiuix/animation/b/t;->c(Landroid/view/View;[Lmiuix/animation/a/a;)V

    invoke-direct {p0, p1}, Lmiuix/animation/b/t;->d(Landroid/view/View;)Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-static {}, Lmiuix/animation/h/g;->c()Z

    move-result p2

    if-eqz p2, :cond_0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "handleViewTouch for "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x0

    new-array p3, p3, [Ljava/lang/Object;

    invoke-static {p2, p3}, Lmiuix/animation/h/g;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->isClickable()Z

    move-result v5

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/view/View;->setClickable(Z)V

    new-instance p2, Lmiuix/animation/b/q;

    move-object v0, p2

    move-object v1, p0

    move v2, p4

    move-object v3, p1

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lmiuix/animation/b/q;-><init>(Lmiuix/animation/b/t;ZLandroid/view/View;[Lmiuix/animation/a/a;Z)V

    invoke-static {p1, p2}, Lmiuix/animation/h/a;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    :cond_1
    return-void
.end method

.method private a(Landroid/view/View;Z)V
    .locals 0

    invoke-virtual {p1, p2}, Landroid/view/View;->setClickable(Z)V

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method private varargs a(Landroid/widget/AbsListView;Landroid/view/View;Z[Lmiuix/animation/a/a;)V
    .locals 2

    invoke-static {p1}, Lmiuix/animation/b/t;->a(Landroid/widget/AbsListView;)Lmiuix/animation/b/x;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lmiuix/animation/b/x;

    invoke-direct {v0, p1}, Lmiuix/animation/b/x;-><init>(Landroid/widget/AbsListView;)V

    sget v1, Ld/f/b;->miuix_animation_tag_touch_listener:I

    invoke-virtual {p1, v1, v0}, Landroid/widget/AbsListView;->setTag(ILjava/lang/Object;)V

    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_1
    new-instance p1, Lmiuix/animation/b/t$a;

    invoke-direct {p1, p0, p4}, Lmiuix/animation/b/t$a;-><init>(Lmiuix/animation/b/t;[Lmiuix/animation/a/a;)V

    invoke-virtual {v0, p2, p1}, Lmiuix/animation/b/x;->a(Landroid/view/View;Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method static synthetic a(Lmiuix/animation/b/t;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/animation/b/t;->b(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lmiuix/animation/b/t;Landroid/view/View;Landroid/view/MotionEvent;[Lmiuix/animation/a/a;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lmiuix/animation/b/t;->a(Landroid/view/View;Landroid/view/MotionEvent;[Lmiuix/animation/a/a;)V

    return-void
.end method

.method static synthetic a(Lmiuix/animation/b/t;Landroid/view/View;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmiuix/animation/b/t;->a(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic a(Lmiuix/animation/b/t;[Lmiuix/animation/a/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/animation/b/t;->k([Lmiuix/animation/a/a;)V

    return-void
.end method

.method private a(Lmiuix/animation/e;)V
    .locals 2

    instance-of v0, p1, Lmiuix/animation/ViewTarget;

    if-eqz v0, :cond_0

    check-cast p1, Lmiuix/animation/ViewTarget;

    invoke-virtual {p1}, Lmiuix/animation/ViewTarget;->e()Landroid/view/View;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    invoke-static {v0, v1, p1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result p1

    iput p1, p0, Lmiuix/animation/b/t;->q:F

    :cond_1
    return-void
.end method

.method static a(Landroid/view/View;[ILandroid/view/MotionEvent;)Z
    .locals 5

    const/4 v0, 0x1

    if-eqz p0, :cond_1

    invoke-virtual {p0, p1}, Landroid/view/View;->getLocationOnScreen([I)V

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result p2

    float-to-int p2, p2

    const/4 v2, 0x0

    aget v3, p1, v2

    if-lt v1, v3, :cond_0

    aget v3, p1, v2

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    if-gt v1, v3, :cond_0

    aget v1, p1, v0

    if-lt p2, v1, :cond_0

    aget p1, p1, v0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result p0

    add-int/2addr p1, p0

    if-gt p2, p1, :cond_0

    goto :goto_0

    :cond_0
    move v0, v2

    :cond_1
    :goto_0
    return v0
.end method

.method static synthetic a(Lmiuix/animation/b/t;)Z
    .locals 0

    iget-boolean p0, p0, Lmiuix/animation/b/t;->x:Z

    return p0
.end method

.method static synthetic a(Lmiuix/animation/b/t;Landroid/view/View;Z[Lmiuix/animation/a/a;)Z
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lmiuix/animation/b/t;->b(Landroid/view/View;Z[Lmiuix/animation/a/a;)Z

    move-result p0

    return p0
.end method

.method private a(Lmiuix/animation/m$b;)Z
    .locals 2

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, Lmiuix/animation/b/t;->n:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method static synthetic b(Lmiuix/animation/b/t;)Landroid/view/View$OnLongClickListener;
    .locals 0

    iget-object p0, p0, Lmiuix/animation/b/t;->g:Landroid/view/View$OnLongClickListener;

    return-object p0
.end method

.method private b(F)V
    .locals 2

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    invoke-interface {v0}, Lmiuix/animation/b/w;->getTarget()Lmiuix/animation/e;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/animation/e;->e()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/view/View;

    sget v1, Lmiuix/animation/p;->miuix_animation_tag_view_hover_corners:I

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private b(Landroid/view/MotionEvent;)V
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b/t;->f:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/animation/b/t;->g:Landroid/view/View$OnLongClickListener;

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    iput v0, p0, Lmiuix/animation/b/t;->h:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Lmiuix/animation/b/t;->i:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result p1

    iput p1, p0, Lmiuix/animation/b/t;->j:F

    const/4 p1, 0x0

    iput-boolean p1, p0, Lmiuix/animation/b/t;->k:Z

    iput-boolean p1, p0, Lmiuix/animation/b/t;->x:Z

    invoke-direct {p0}, Lmiuix/animation/b/t;->g()V

    :cond_1
    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 1

    iget-boolean v0, p0, Lmiuix/animation/b/t;->k:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lmiuix/animation/b/t;->x:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/animation/b/t;->k:Z

    iget-object v0, p0, Lmiuix/animation/b/t;->f:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method static synthetic b(Lmiuix/animation/b/t;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/animation/b/t;->c(Landroid/view/View;)V

    return-void
.end method

.method private b(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result p2

    iget v1, p0, Lmiuix/animation/b/t;->i:F

    iget v2, p0, Lmiuix/animation/b/t;->j:F

    invoke-static {v1, v2, v0, p2}, Lmiuix/animation/h/a;->a(FFFF)D

    move-result-wide v0

    invoke-static {p1}, Lmiuix/animation/h/a;->a(Landroid/view/View;)F

    move-result p1

    float-to-double p1, p1

    cmpg-double p1, v0, p1

    if-gez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private varargs b(Landroid/view/View;Z[Lmiuix/animation/a/a;)Z
    .locals 4

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    invoke-interface {v0}, Lmiuix/animation/b/w;->getTarget()Lmiuix/animation/e;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lmiuix/animation/b/t;->a(Landroid/view/View;)Lmiuix/animation/b/t$c;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, v0, Lmiuix/animation/b/t$c;->a:Landroid/widget/AbsListView;

    if-eqz v2, :cond_1

    invoke-static {}, Lmiuix/animation/h/g;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleListViewTouch for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v1}, Lmiuix/animation/h/g;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-object v0, v0, Lmiuix/animation/b/t$c;->a:Landroid/widget/AbsListView;

    invoke-direct {p0, v0, p1, p2, p3}, Lmiuix/animation/b/t;->a(Landroid/widget/AbsListView;Landroid/view/View;Z[Lmiuix/animation/a/a;)V

    const/4 p1, 0x1

    return p1

    :cond_1
    return v1
.end method

.method private c(Landroid/view/View;)V
    .locals 1

    iget-boolean v0, p0, Lmiuix/animation/b/t;->x:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/animation/b/t;->x:Z

    iget-object v0, p0, Lmiuix/animation/b/t;->g:Landroid/view/View$OnLongClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnLongClickListener;->onLongClick(Landroid/view/View;)Z

    :cond_0
    return-void
.end method

.method private varargs c(Landroid/view/View;[Lmiuix/animation/a/a;)V
    .locals 2

    sget-object v0, Lmiuix/animation/b/t;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/animation/b/t$b;

    if-nez v0, :cond_0

    new-instance v0, Lmiuix/animation/b/t$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmiuix/animation/b/t$b;-><init>(Lmiuix/animation/b/p;)V

    sget-object v1, Lmiuix/animation/b/t;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v0, p0, p2}, Lmiuix/animation/b/t$b;->a(Lmiuix/animation/b/t;[Lmiuix/animation/a/a;)V

    return-void
.end method

.method private d(Landroid/view/View;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b/t;->o:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-ne v0, p1, :cond_1

    const/4 p1, 0x0

    return p1

    :cond_1
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmiuix/animation/b/t;->o:Ljava/lang/ref/WeakReference;

    const/4 p1, 0x1

    return p1
.end method

.method private e()V
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b/t;->w:Lmiuix/animation/b/t$d;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lmiuix/animation/b/t$d;->b(Lmiuix/animation/b/t;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/animation/b/t;->l:Z

    iput v0, p0, Lmiuix/animation/b/t;->h:I

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/animation/b/t;->i:F

    iput v0, p0, Lmiuix/animation/b/t;->j:F

    return-void
.end method

.method private f()V
    .locals 5

    iget-boolean v0, p0, Lmiuix/animation/b/t;->t:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lmiuix/animation/b/t;->u:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/16 v0, 0x14

    const/4 v1, 0x0

    invoke-static {v0, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    iget-object v1, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    invoke-interface {v1}, Lmiuix/animation/b/w;->getTarget()Lmiuix/animation/e;

    move-result-object v1

    invoke-virtual {v1}, Lmiuix/animation/e;->e()Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Landroid/view/View;

    if-eqz v2, :cond_1

    check-cast v1, Landroid/view/View;

    sget v0, Ld/f/a;->miuix_folme_color_touch_tint:I

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :cond_1
    sget-object v1, Lmiuix/animation/g/C;->a:Lmiuix/animation/g/C$b;

    iget-object v2, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v3, Lmiuix/animation/m$b;->b:Lmiuix/animation/m$b;

    invoke-interface {v2, v3}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v2

    int-to-double v3, v0

    invoke-virtual {v2, v1, v3, v4}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v2, Lmiuix/animation/m$b;->a:Lmiuix/animation/m$b;

    invoke-interface {v0, v2}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    :cond_2
    :goto_0
    return-void
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lmiuix/animation/b/t;->g:Landroid/view/View$OnLongClickListener;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lmiuix/animation/b/t;->w:Lmiuix/animation/b/t$d;

    if-nez v0, :cond_1

    new-instance v0, Lmiuix/animation/b/t$d;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmiuix/animation/b/t$d;-><init>(Lmiuix/animation/b/p;)V

    iput-object v0, p0, Lmiuix/animation/b/t;->w:Lmiuix/animation/b/t$d;

    :cond_1
    iget-object v0, p0, Lmiuix/animation/b/t;->w:Lmiuix/animation/b/t$d;

    invoke-virtual {v0, p0}, Lmiuix/animation/b/t$d;->a(Lmiuix/animation/b/t;)V

    return-void
.end method

.method private varargs h([Lmiuix/animation/a/a;)[Lmiuix/animation/a/a;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lmiuix/animation/a/a;

    iget-object v1, p0, Lmiuix/animation/b/t;->r:Lmiuix/animation/a/a;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Lmiuix/animation/h/a;->a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lmiuix/animation/a/a;

    return-object p1
.end method

.method private varargs i([Lmiuix/animation/a/a;)[Lmiuix/animation/a/a;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lmiuix/animation/a/a;

    iget-object v1, p0, Lmiuix/animation/b/t;->s:Lmiuix/animation/a/a;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Lmiuix/animation/h/a;->a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lmiuix/animation/a/a;

    return-object p1
.end method

.method private varargs j([Lmiuix/animation/a/a;)V
    .locals 2

    invoke-static {}, Lmiuix/animation/h/g;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onEventDown, touchDown"

    invoke-static {v1, v0}, Lmiuix/animation/h/g;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/animation/b/t;->l:Z

    invoke-virtual {p0, p1}, Lmiuix/animation/b/t;->d([Lmiuix/animation/a/a;)V

    return-void
.end method

.method private varargs k([Lmiuix/animation/a/a;)V
    .locals 2

    iget-boolean v0, p0, Lmiuix/animation/b/t;->l:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lmiuix/animation/h/g;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onEventUp, touchUp"

    invoke-static {v1, v0}, Lmiuix/animation/h/g;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p0, p1}, Lmiuix/animation/b/t;->g([Lmiuix/animation/a/a;)V

    invoke-direct {p0}, Lmiuix/animation/b/t;->e()V

    :cond_1
    return-void
.end method


# virtual methods
.method public a(FFFF)Lmiuix/animation/m;
    .locals 1

    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr p1, v0

    float-to-int p1, p1

    mul-float/2addr p2, v0

    float-to-int p2, p2

    mul-float/2addr p3, v0

    float-to-int p3, p3

    mul-float/2addr p4, v0

    float-to-int p4, p4

    invoke-static {p1, p2, p3, p4}, Landroid/graphics/Color;->argb(IIII)I

    move-result p1

    invoke-virtual {p0, p1}, Lmiuix/animation/b/t;->setTint(I)Lmiuix/animation/m;

    return-object p0
.end method

.method public varargs a(F[Lmiuix/animation/m$b;)Lmiuix/animation/m;
    .locals 3

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    invoke-direct {p0, p2}, Lmiuix/animation/b/t;->a([Lmiuix/animation/m$b;)Lmiuix/animation/m$b;

    move-result-object p2

    invoke-interface {v0, p2}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object p2

    sget-object v0, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    float-to-double v1, p1

    invoke-virtual {p2, v0, v1, v2}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    return-object p0
.end method

.method public a(I)Lmiuix/animation/m;
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b/t;->r:Lmiuix/animation/a/a;

    invoke-virtual {v0, p1}, Lmiuix/animation/a/a;->a(I)Lmiuix/animation/a/a;

    iget-object v0, p0, Lmiuix/animation/b/t;->s:Lmiuix/animation/a/a;

    invoke-virtual {v0, p1}, Lmiuix/animation/a/a;->a(I)Lmiuix/animation/a/a;

    return-object p0
.end method

.method public a()V
    .locals 3

    invoke-super {p0}, Lmiuix/animation/b/b;->a()V

    iget-object v0, p0, Lmiuix/animation/b/t;->c:Lmiuix/animation/b/j;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/animation/b/j;->a()V

    :cond_0
    iget-object v0, p0, Lmiuix/animation/b/t;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lmiuix/animation/b/t;->o:Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-direct {p0, v0}, Lmiuix/animation/b/t;->a(Ljava/lang/ref/WeakReference;)Landroid/view/View;

    iput-object v1, p0, Lmiuix/animation/b/t;->o:Ljava/lang/ref/WeakReference;

    :cond_1
    iget-object v0, p0, Lmiuix/animation/b/t;->p:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_3

    invoke-direct {p0, v0}, Lmiuix/animation/b/t;->a(Ljava/lang/ref/WeakReference;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    sget v2, Ld/f/b;->miuix_animation_tag_touch_listener:I

    invoke-virtual {v0, v2, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    :cond_2
    iput-object v1, p0, Lmiuix/animation/b/t;->p:Ljava/lang/ref/WeakReference;

    :cond_3
    invoke-direct {p0}, Lmiuix/animation/b/t;->e()V

    return-void
.end method

.method public a(Landroid/view/MotionEvent;)V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Lmiuix/animation/a/a;

    const/4 v1, 0x0

    invoke-direct {p0, v1, p1, v0}, Lmiuix/animation/b/t;->a(Landroid/view/View;Landroid/view/MotionEvent;[Lmiuix/animation/a/a;)V

    return-void
.end method

.method public varargs a(Landroid/view/View;Z[Lmiuix/animation/a/a;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lmiuix/animation/b/t;->a(Landroid/view/View;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Z[Lmiuix/animation/a/a;)V

    return-void
.end method

.method public varargs a(Landroid/view/View;[Lmiuix/animation/a/a;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lmiuix/animation/b/t;->a(Landroid/view/View;Z[Lmiuix/animation/a/a;)V

    return-void
.end method

.method public a(Lmiuix/animation/b/j;)V
    .locals 0

    iput-object p1, p0, Lmiuix/animation/b/t;->c:Lmiuix/animation/b/j;

    return-void
.end method

.method public b(FFFF)Lmiuix/animation/m;
    .locals 1

    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr p1, v0

    float-to-int p1, p1

    mul-float/2addr p2, v0

    float-to-int p2, p2

    mul-float/2addr p3, v0

    float-to-int p3, p3

    mul-float/2addr p4, v0

    float-to-int p4, p4

    invoke-static {p1, p2, p3, p4}, Landroid/graphics/Color;->argb(IIII)I

    move-result p1

    invoke-virtual {p0, p1}, Lmiuix/animation/b/t;->b(I)Lmiuix/animation/m;

    return-object p0
.end method

.method public varargs b(F[Lmiuix/animation/m$b;)Lmiuix/animation/m;
    .locals 3

    invoke-direct {p0, p2}, Lmiuix/animation/b/t;->a([Lmiuix/animation/m$b;)Lmiuix/animation/m$b;

    move-result-object p2

    iget-object v0, p0, Lmiuix/animation/b/t;->n:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    invoke-interface {v0, p2}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object p2

    sget-object v0, Lmiuix/animation/g/A;->e:Lmiuix/animation/g/A;

    float-to-double v1, p1

    invoke-virtual {p2, v0, v1, v2}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object p1, Lmiuix/animation/g/A;->f:Lmiuix/animation/g/A;

    invoke-virtual {p2, p1, v1, v2}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    return-object p0
.end method

.method public b(I)Lmiuix/animation/m;
    .locals 4

    sget-object v0, Lmiuix/animation/g/C;->b:Lmiuix/animation/g/C$a;

    iget-object v1, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v2, Lmiuix/animation/m$b;->b:Lmiuix/animation/m$b;

    invoke-interface {v1, v2}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v1

    int-to-double v2, p1

    invoke-virtual {v1, v0, v2, v3}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    iget-object p1, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v1, Lmiuix/animation/m$b;->a:Lmiuix/animation/m$b;

    invoke-interface {p1, v1}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object p1

    iget-object v1, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    invoke-interface {v1}, Lmiuix/animation/b/w;->getTarget()Lmiuix/animation/e;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-static {v1, v0, v2, v3}, Lmiuix/animation/d/l;->b(Lmiuix/animation/e;Lmiuix/animation/g/b;D)D

    move-result-wide v1

    double-to-int v1, v1

    int-to-double v1, v1

    invoke-virtual {p1, v0, v1, v2}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    return-object p0
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v1, Lmiuix/animation/m$b;->a:Lmiuix/animation/m$b;

    invoke-interface {v0, v1}, Lmiuix/animation/k;->b(Ljava/lang/Object;)Lmiuix/animation/k;

    return-void
.end method

.method public varargs d([Lmiuix/animation/a/a;)V
    .locals 5

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lmiuix/animation/b/t;->b(F)V

    invoke-direct {p0}, Lmiuix/animation/b/t;->f()V

    invoke-direct {p0, p1}, Lmiuix/animation/b/t;->h([Lmiuix/animation/a/a;)[Lmiuix/animation/a/a;

    move-result-object p1

    iget-object v0, p0, Lmiuix/animation/b/t;->c:Lmiuix/animation/b/j;

    if-eqz v0, :cond_0

    iget v1, p0, Lmiuix/animation/b/t;->e:I

    invoke-virtual {v0, v1, p1}, Lmiuix/animation/b/j;->b(I[Lmiuix/animation/a/a;)Lmiuix/animation/n;

    :cond_0
    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v1, Lmiuix/animation/m$b;->b:Lmiuix/animation/m$b;

    invoke-interface {v0, v1}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v0

    sget-object v1, Lmiuix/animation/m$b;->b:Lmiuix/animation/m$b;

    invoke-direct {p0, v1}, Lmiuix/animation/b/t;->a(Lmiuix/animation/m$b;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    invoke-interface {v1}, Lmiuix/animation/b/w;->getTarget()Lmiuix/animation/e;

    move-result-object v1

    sget-object v2, Lmiuix/animation/g/A;->n:Lmiuix/animation/g/A;

    invoke-virtual {v1, v2}, Lmiuix/animation/e;->a(Lmiuix/animation/g/b;)F

    move-result v2

    sget-object v3, Lmiuix/animation/g/A;->m:Lmiuix/animation/g/A;

    invoke-virtual {v1, v3}, Lmiuix/animation/e;->a(Lmiuix/animation/g/b;)F

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iget v2, p0, Lmiuix/animation/b/t;->q:F

    sub-float v2, v1, v2

    div-float/2addr v2, v1

    const v1, 0x3f666666    # 0.9f

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    sget-object v2, Lmiuix/animation/g/A;->e:Lmiuix/animation/g/A;

    float-to-double v3, v1

    invoke-virtual {v0, v2, v3, v4}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v1, Lmiuix/animation/g/A;->f:Lmiuix/animation/g/A;

    invoke-virtual {v0, v1, v3, v4}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    :cond_1
    iget-object v1, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    invoke-interface {v1, v0, p1}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    return-void
.end method

.method public varargs g([Lmiuix/animation/a/a;)V
    .locals 2

    invoke-direct {p0, p1}, Lmiuix/animation/b/t;->i([Lmiuix/animation/a/a;)[Lmiuix/animation/a/a;

    move-result-object p1

    iget-object v0, p0, Lmiuix/animation/b/t;->c:Lmiuix/animation/b/j;

    if-eqz v0, :cond_0

    iget v1, p0, Lmiuix/animation/b/t;->d:I

    invoke-virtual {v0, v1, p1}, Lmiuix/animation/b/j;->b(I[Lmiuix/animation/a/a;)Lmiuix/animation/n;

    :cond_0
    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v1, Lmiuix/animation/m$b;->a:Lmiuix/animation/m$b;

    invoke-interface {v0, v1}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    return-void
.end method

.method public setTint(I)Lmiuix/animation/m;
    .locals 4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/animation/b/t;->t:Z

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lmiuix/animation/b/t;->u:Z

    iget-object v0, p0, Lmiuix/animation/b/b;->a:Lmiuix/animation/b/w;

    sget-object v1, Lmiuix/animation/m$b;->b:Lmiuix/animation/m$b;

    invoke-interface {v0, v1}, Lmiuix/animation/b/w;->c(Ljava/lang/Object;)Lmiuix/animation/b/a;

    move-result-object v0

    sget-object v1, Lmiuix/animation/g/C;->a:Lmiuix/animation/g/C$b;

    int-to-double v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    return-object p0
.end method
