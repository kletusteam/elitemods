.class public Lmiuix/animation/a/a;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lmiuix/animation/h/c$a;


# instance fields
.field public b:J

.field public c:J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public d:F

.field public e:Lmiuix/animation/h/c$a;

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lmiuix/animation/a/c;",
            ">;"
        }
    .end annotation
.end field

.field public g:I

.field public h:Ljava/lang/Object;

.field public i:J

.field public final j:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Lmiuix/animation/e/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    const/4 v1, -0x2

    invoke-static {v1, v0}, Lmiuix/animation/h/c;->c(I[F)Lmiuix/animation/h/c$a;

    move-result-object v0

    sput-object v0, Lmiuix/animation/a/a;->a:Lmiuix/animation/h/c$a;

    return-void

    :array_0
    .array-data 4
        0x3f59999a    # 0.85f
        0x3e99999a    # 0.3f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lmiuix/animation/a/a;-><init>(Z)V

    return-void
.end method

.method public constructor <init>(Lmiuix/animation/a/a;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lmiuix/animation/a/a;-><init>(Z)V

    invoke-virtual {p0, p1}, Lmiuix/animation/a/a;->b(Lmiuix/animation/a/a;)V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lmiuix/animation/a/a;->d:F

    const/4 v0, -0x1

    iput v0, p0, Lmiuix/animation/a/a;->g:I

    if-nez p1, :cond_0

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lmiuix/animation/a/a;->f:Ljava/util/Map;

    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lmiuix/animation/a/a;->j:Ljava/util/HashSet;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    iput-object p1, p0, Lmiuix/animation/a/a;->f:Ljava/util/Map;

    iput-object p1, p0, Lmiuix/animation/a/a;->j:Ljava/util/HashSet;

    :goto_0
    return-void
.end method

.method private a(Ljava/lang/String;Z)Lmiuix/animation/a/c;
    .locals 1

    iget-object v0, p0, Lmiuix/animation/a/a;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/animation/a/c;

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    new-instance v0, Lmiuix/animation/a/c;

    invoke-direct {v0}, Lmiuix/animation/a/c;-><init>()V

    iget-object p2, p0, Lmiuix/animation/a/a;->f:Ljava/util/Map;

    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method private a(Lmiuix/animation/g/b;Z)Lmiuix/animation/a/c;
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-virtual {p1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lmiuix/animation/a/a;->a(Ljava/lang/String;Z)Lmiuix/animation/a/c;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public a(F)Lmiuix/animation/a/a;
    .locals 0

    iput p1, p0, Lmiuix/animation/a/a;->d:F

    return-object p0
.end method

.method public a(I)Lmiuix/animation/a/a;
    .locals 0

    iput p1, p0, Lmiuix/animation/a/a;->g:I

    return-object p0
.end method

.method public varargs a(I[F)Lmiuix/animation/a/a;
    .locals 0

    invoke-static {p1, p2}, Lmiuix/animation/h/c;->c(I[F)Lmiuix/animation/h/c$a;

    move-result-object p1

    iput-object p1, p0, Lmiuix/animation/a/a;->e:Lmiuix/animation/h/c$a;

    return-object p0
.end method

.method public a(J)Lmiuix/animation/a/a;
    .locals 0

    iput-wide p1, p0, Lmiuix/animation/a/a;->b:J

    return-object p0
.end method

.method public varargs a(Lmiuix/animation/g/b;J[F)Lmiuix/animation/a/a;
    .locals 6

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v3, p2

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lmiuix/animation/a/a;->a(Lmiuix/animation/g/b;Lmiuix/animation/h/c$a;J[F)Lmiuix/animation/a/a;

    move-result-object p1

    return-object p1
.end method

.method public a(Lmiuix/animation/g/b;Lmiuix/animation/a/c;)Lmiuix/animation/a/a;
    .locals 1

    if-eqz p2, :cond_0

    iget-object v0, p0, Lmiuix/animation/a/a;->f:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lmiuix/animation/a/a;->f:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-object p0
.end method

.method public varargs a(Lmiuix/animation/g/b;Lmiuix/animation/h/c$a;J[F)Lmiuix/animation/a/a;
    .locals 7

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lmiuix/animation/a/a;->a(Lmiuix/animation/g/b;Z)Lmiuix/animation/a/c;

    move-result-object v2

    move-object v1, p0

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Lmiuix/animation/a/a;->a(Lmiuix/animation/a/c;Lmiuix/animation/h/c$a;J[F)V

    return-object p0
.end method

.method public a(Lmiuix/animation/h/c$a;)Lmiuix/animation/a/a;
    .locals 0

    iput-object p1, p0, Lmiuix/animation/a/a;->e:Lmiuix/animation/h/c$a;

    return-object p0
.end method

.method public varargs a([Lmiuix/animation/e/b;)Lmiuix/animation/a/a;
    .locals 1

    iget-object v0, p0, Lmiuix/animation/a/a;->j:Ljava/util/HashSet;

    invoke-static {v0, p1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lmiuix/animation/a/c;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiuix/animation/a/a;->a(Ljava/lang/String;Z)Lmiuix/animation/a/c;

    move-result-object p1

    return-object p1
.end method

.method public a()V
    .locals 4

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmiuix/animation/a/a;->b:J

    const/4 v2, 0x0

    iput-object v2, p0, Lmiuix/animation/a/a;->e:Lmiuix/animation/h/c$a;

    iget-object v3, p0, Lmiuix/animation/a/a;->j:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->clear()V

    iput-object v2, p0, Lmiuix/animation/a/a;->h:Ljava/lang/Object;

    iput-wide v0, p0, Lmiuix/animation/a/a;->i:J

    const v2, 0x7f7fffff    # Float.MAX_VALUE

    iput v2, p0, Lmiuix/animation/a/a;->d:F

    iput-wide v0, p0, Lmiuix/animation/a/a;->c:J

    const/4 v0, -0x1

    iput v0, p0, Lmiuix/animation/a/a;->g:I

    iget-object v0, p0, Lmiuix/animation/a/a;->f:Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :cond_0
    return-void
.end method

.method public a(Lmiuix/animation/a/a;)V
    .locals 1

    iget-object v0, p0, Lmiuix/animation/a/a;->f:Ljava/util/Map;

    iget-object p1, p1, Lmiuix/animation/a/a;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    return-void
.end method

.method varargs a(Lmiuix/animation/a/c;Lmiuix/animation/h/c$a;J[F)V
    .locals 2

    goto/32 :goto_9

    nop

    :goto_0
    cmp-long p2, p3, v0

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    if-gtz p2, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_7

    nop

    :goto_3
    const-wide/16 v0, 0x0

    goto/32 :goto_0

    nop

    :goto_4
    const/4 p2, 0x0

    goto/32 :goto_c

    nop

    :goto_5
    array-length p2, p5

    goto/32 :goto_6

    nop

    :goto_6
    if-gtz p2, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_4

    nop

    :goto_7
    invoke-virtual {p1, p3, p4}, Lmiuix/animation/a/a;->a(J)Lmiuix/animation/a/a;

    :goto_8
    goto/32 :goto_5

    nop

    :goto_9
    if-nez p2, :cond_2

    goto/32 :goto_e

    :cond_2
    goto/32 :goto_d

    nop

    :goto_a
    invoke-virtual {p1, p2}, Lmiuix/animation/a/a;->a(F)Lmiuix/animation/a/a;

    :goto_b
    goto/32 :goto_1

    nop

    :goto_c
    aget p2, p5, p2

    goto/32 :goto_a

    nop

    :goto_d
    invoke-virtual {p1, p2}, Lmiuix/animation/a/a;->a(Lmiuix/animation/h/c$a;)Lmiuix/animation/a/a;

    :goto_e
    goto/32 :goto_3

    nop
.end method

.method public b(J)Lmiuix/animation/a/a;
    .locals 0

    iput-wide p1, p0, Lmiuix/animation/a/a;->c:J

    return-object p0
.end method

.method public varargs b([Lmiuix/animation/e/b;)Lmiuix/animation/a/a;
    .locals 1

    array-length v0, p1

    if-nez v0, :cond_0

    iget-object p1, p0, Lmiuix/animation/a/a;->j:Ljava/util/HashSet;

    invoke-virtual {p1}, Ljava/util/HashSet;->clear()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/animation/a/a;->j:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->removeAll(Ljava/util/Collection;)Z

    :goto_0
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lmiuix/animation/a/c;
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lmiuix/animation/a/a;->a(Ljava/lang/String;Z)Lmiuix/animation/a/c;

    move-result-object p1

    return-object p1
.end method

.method public b(Lmiuix/animation/a/a;)V
    .locals 2

    if-eqz p1, :cond_0

    if-eq p1, p0, :cond_0

    iget-wide v0, p1, Lmiuix/animation/a/a;->b:J

    iput-wide v0, p0, Lmiuix/animation/a/a;->b:J

    iget-object v0, p1, Lmiuix/animation/a/a;->e:Lmiuix/animation/h/c$a;

    iput-object v0, p0, Lmiuix/animation/a/a;->e:Lmiuix/animation/h/c$a;

    iget-object v0, p0, Lmiuix/animation/a/a;->j:Ljava/util/HashSet;

    iget-object v1, p1, Lmiuix/animation/a/a;->j:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p1, Lmiuix/animation/a/a;->h:Ljava/lang/Object;

    iput-object v0, p0, Lmiuix/animation/a/a;->h:Ljava/lang/Object;

    iget-wide v0, p1, Lmiuix/animation/a/a;->i:J

    iput-wide v0, p0, Lmiuix/animation/a/a;->i:J

    iget v0, p1, Lmiuix/animation/a/a;->d:F

    iput v0, p0, Lmiuix/animation/a/a;->d:F

    iget-wide v0, p1, Lmiuix/animation/a/a;->c:J

    iput-wide v0, p0, Lmiuix/animation/a/a;->c:J

    iget v0, p1, Lmiuix/animation/a/a;->g:I

    iput v0, p0, Lmiuix/animation/a/a;->g:I

    iget-object v0, p0, Lmiuix/animation/a/a;->f:Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lmiuix/animation/a/a;->f:Ljava/util/Map;

    iget-object p1, p1, Lmiuix/animation/a/a;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AnimConfig{delay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lmiuix/animation/a/a;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", minDuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lmiuix/animation/a/a;->c:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", ease="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lmiuix/animation/a/a;->e:Lmiuix/animation/h/c$a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", fromSpeed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lmiuix/animation/a/a;->d:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", tintMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lmiuix/animation/a/a;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", tag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lmiuix/animation/a/a;->h:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", flags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lmiuix/animation/a/a;->i:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", listeners="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lmiuix/animation/a/a;->j:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", specialNameMap = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lmiuix/animation/a/a;->f:Ljava/util/Map;

    const-string v2, "    "

    invoke-static {v1, v2}, Lmiuix/animation/h/a;->a(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
