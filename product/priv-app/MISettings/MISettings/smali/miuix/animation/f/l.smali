.class public final Lmiuix/animation/f/l;
.super Ljava/lang/Object;

# interfaces
.implements Lmiuix/animation/f/g;


# instance fields
.field a:D

.field b:D

.field private c:Z

.field private d:D

.field private e:D

.field private f:D

.field private g:D

.field private h:D

.field private i:D

.field private final j:Lmiuix/animation/f/e$a;


# direct methods
.method public constructor <init>(F)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide v0, 0x4097700000000000L    # 1500.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    iput-wide v0, p0, Lmiuix/animation/f/l;->a:D

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    iput-wide v0, p0, Lmiuix/animation/f/l;->b:D

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/animation/f/l;->c:Z

    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lmiuix/animation/f/l;->i:D

    new-instance v0, Lmiuix/animation/f/e$a;

    invoke-direct {v0}, Lmiuix/animation/f/e$a;-><init>()V

    iput-object v0, p0, Lmiuix/animation/f/l;->j:Lmiuix/animation/f/e$a;

    float-to-double v0, p1

    iput-wide v0, p0, Lmiuix/animation/f/l;->i:D

    return-void
.end method

.method private b()V
    .locals 8

    iget-boolean v0, p0, Lmiuix/animation/f/l;->c:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-wide v0, p0, Lmiuix/animation/f/l;->i:D

    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_3

    iget-wide v0, p0, Lmiuix/animation/f/l;->b:D

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v4, v0, v2

    if-lez v4, :cond_1

    neg-double v4, v0

    iget-wide v6, p0, Lmiuix/animation/f/l;->a:D

    mul-double/2addr v4, v6

    mul-double/2addr v0, v0

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    mul-double/2addr v6, v0

    add-double/2addr v4, v6

    iput-wide v4, p0, Lmiuix/animation/f/l;->f:D

    iget-wide v0, p0, Lmiuix/animation/f/l;->b:D

    neg-double v4, v0

    iget-wide v6, p0, Lmiuix/animation/f/l;->a:D

    mul-double/2addr v4, v6

    mul-double/2addr v0, v0

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    mul-double/2addr v6, v0

    sub-double/2addr v4, v6

    iput-wide v4, p0, Lmiuix/animation/f/l;->g:D

    goto :goto_0

    :cond_1
    const-wide/16 v4, 0x0

    cmpl-double v4, v0, v4

    if-ltz v4, :cond_2

    cmpg-double v4, v0, v2

    if-gez v4, :cond_2

    iget-wide v4, p0, Lmiuix/animation/f/l;->a:D

    mul-double/2addr v0, v0

    sub-double/2addr v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    mul-double/2addr v4, v0

    iput-wide v4, p0, Lmiuix/animation/f/l;->h:D

    :cond_2
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/animation/f/l;->c:Z

    return-void

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Error: Final position of the spring must be set before the miuix.animation starts"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a()F
    .locals 2

    iget-wide v0, p0, Lmiuix/animation/f/l;->i:D

    double-to-float v0, v0

    return v0
.end method

.method a(DDJ)Lmiuix/animation/f/e$a;
    .locals 16

    goto/32 :goto_8

    nop

    :goto_0
    iget-wide v12, v0, Lmiuix/animation/f/l;->g:D

    goto/32 :goto_27

    nop

    :goto_1
    sub-double v7, v7, p3

    goto/32 :goto_2a

    nop

    :goto_2
    iget-wide v12, v0, Lmiuix/animation/f/l;->a:D

    goto/32 :goto_43

    nop

    :goto_3
    add-double/2addr v12, v14

    goto/32 :goto_59

    nop

    :goto_4
    div-double/2addr v7, v12

    goto/32 :goto_50

    nop

    :goto_5
    mul-double v7, v5, v3

    goto/32 :goto_42

    nop

    :goto_6
    cmpl-double v9, v5, v7

    goto/32 :goto_7

    nop

    :goto_7
    if-eqz v9, :cond_0

    goto/32 :goto_7d

    :cond_0
    goto/32 :goto_1f

    nop

    :goto_8
    move-object/from16 v0, p0

    goto/32 :goto_2f

    nop

    :goto_9
    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v1

    goto/32 :goto_d

    nop

    :goto_a
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    goto/32 :goto_74

    nop

    :goto_b
    invoke-static {v10, v11, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    goto/32 :goto_7a

    nop

    :goto_c
    iget-wide v5, v0, Lmiuix/animation/f/l;->g:D

    goto/32 :goto_45

    nop

    :goto_d
    mul-double/2addr v7, v1

    goto/32 :goto_1d

    nop

    :goto_e
    iget-wide v12, v0, Lmiuix/animation/f/l;->a:D

    goto/32 :goto_4f

    nop

    :goto_f
    mul-double/2addr v3, v12

    goto/32 :goto_e

    nop

    :goto_10
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    goto/32 :goto_76

    nop

    :goto_11
    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    goto/32 :goto_21

    nop

    :goto_12
    mul-double/2addr v5, v3

    goto/32 :goto_35

    nop

    :goto_13
    iget-wide v12, v0, Lmiuix/animation/f/l;->h:D

    goto/32 :goto_4

    nop

    :goto_14
    iget-wide v12, v0, Lmiuix/animation/f/l;->f:D

    goto/32 :goto_73

    nop

    :goto_15
    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    goto/32 :goto_57

    nop

    :goto_16
    move-wide/from16 v1, p5

    goto/32 :goto_58

    nop

    :goto_17
    mul-double/2addr v5, v12

    goto/32 :goto_6b

    nop

    :goto_18
    invoke-static {v11, v12, v9, v10}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v9

    goto/32 :goto_3b

    nop

    :goto_19
    mul-double/2addr v7, v14

    goto/32 :goto_68

    nop

    :goto_1a
    invoke-static {v11, v12}, Ljava/lang/Math;->sin(D)D

    move-result-wide v3

    goto/32 :goto_12

    nop

    :goto_1b
    mul-double/2addr v14, v1

    goto/32 :goto_15

    nop

    :goto_1c
    add-double v7, v14, v9

    goto/32 :goto_4a

    nop

    :goto_1d
    add-double/2addr v5, v7

    goto/32 :goto_6a

    nop

    :goto_1e
    add-double/2addr v7, v3

    goto/32 :goto_7c

    nop

    :goto_1f
    iget-wide v5, v0, Lmiuix/animation/f/l;->a:D

    goto/32 :goto_5

    nop

    :goto_20
    iget-wide v12, v0, Lmiuix/animation/f/l;->h:D

    goto/32 :goto_28

    nop

    :goto_21
    mul-double/2addr v12, v3

    goto/32 :goto_67

    nop

    :goto_22
    div-double/2addr v3, v12

    goto/32 :goto_2e

    nop

    :goto_23
    mul-double/2addr v12, v3

    goto/32 :goto_49

    nop

    :goto_24
    mul-double/2addr v14, v3

    goto/32 :goto_38

    nop

    :goto_25
    mul-double/2addr v12, v1

    goto/32 :goto_10

    nop

    :goto_26
    sub-double v12, v5, v12

    goto/32 :goto_22

    nop

    :goto_27
    mul-double/2addr v7, v12

    goto/32 :goto_25

    nop

    :goto_28
    mul-double/2addr v12, v1

    goto/32 :goto_11

    nop

    :goto_29
    mul-double v12, v7, v1

    goto/32 :goto_70

    nop

    :goto_2a
    iget-wide v12, v0, Lmiuix/animation/f/l;->f:D

    goto/32 :goto_52

    nop

    :goto_2b
    mul-double/2addr v12, v1

    goto/32 :goto_a

    nop

    :goto_2c
    mul-double/2addr v14, v10

    goto/32 :goto_3f

    nop

    :goto_2d
    const-wide v3, 0x408f400000000000L    # 1000.0

    goto/32 :goto_34

    nop

    :goto_2e
    mul-double/2addr v5, v1

    goto/32 :goto_6c

    nop

    :goto_2f
    invoke-direct/range {p0 .. p0}, Lmiuix/animation/f/l;->b()V

    goto/32 :goto_16

    nop

    :goto_30
    move-wide/from16 p1, v5

    goto/32 :goto_5f

    nop

    :goto_31
    mul-double/2addr v12, v1

    goto/32 :goto_75

    nop

    :goto_32
    cmpl-double v9, v5, v7

    goto/32 :goto_66

    nop

    :goto_33
    mul-double/2addr v12, v1

    goto/32 :goto_77

    nop

    :goto_34
    div-double/2addr v1, v3

    goto/32 :goto_62

    nop

    :goto_35
    iget-wide v3, v0, Lmiuix/animation/f/l;->h:D

    goto/32 :goto_5a

    nop

    :goto_36
    mul-double/2addr v5, v3

    goto/32 :goto_5e

    nop

    :goto_37
    add-double/2addr v7, v3

    goto/32 :goto_3c

    nop

    :goto_38
    add-double v14, v14, p3

    goto/32 :goto_19

    nop

    :goto_39
    sub-double v3, v3, p3

    goto/32 :goto_26

    nop

    :goto_3a
    mul-double/2addr v5, v7

    goto/32 :goto_79

    nop

    :goto_3b
    iget-wide v11, v0, Lmiuix/animation/f/l;->h:D

    goto/32 :goto_30

    nop

    :goto_3c
    goto/16 :goto_4b

    :goto_3d
    goto/32 :goto_6

    nop

    :goto_3e
    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    goto/32 :goto_32

    nop

    :goto_3f
    neg-double v9, v10

    goto/32 :goto_46

    nop

    :goto_40
    mul-double/2addr v3, v1

    goto/32 :goto_9

    nop

    :goto_41
    const-wide v11, 0x4005bf0a8b145769L    # Math.E

    goto/32 :goto_18

    nop

    :goto_42
    add-double v7, p3, v7

    goto/32 :goto_29

    nop

    :goto_43
    neg-double v12, v12

    goto/32 :goto_78

    nop

    :goto_44
    mul-double v14, v5, v12

    goto/32 :goto_24

    nop

    :goto_45
    mul-double v7, v5, v3

    goto/32 :goto_1

    nop

    :goto_46
    mul-double/2addr v9, v12

    goto/32 :goto_60

    nop

    :goto_47
    iget-object v1, v0, Lmiuix/animation/f/l;->j:Lmiuix/animation/f/e$a;

    goto/32 :goto_72

    nop

    :goto_48
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    goto/32 :goto_f

    nop

    :goto_49
    add-double/2addr v5, v12

    goto/32 :goto_0

    nop

    :goto_4a
    move-wide/from16 v5, p1

    :goto_4b
    goto/32 :goto_47

    nop

    :goto_4c
    double-to-float v2, v7

    goto/32 :goto_4e

    nop

    :goto_4d
    mul-double/2addr v7, v1

    goto/32 :goto_1e

    nop

    :goto_4e
    iput v2, v1, Lmiuix/animation/f/e$a;->b:F

    goto/32 :goto_7b

    nop

    :goto_4f
    neg-double v14, v12

    goto/32 :goto_5b

    nop

    :goto_50
    iget-wide v12, v0, Lmiuix/animation/f/l;->a:D

    goto/32 :goto_44

    nop

    :goto_51
    mul-double/2addr v3, v5

    goto/32 :goto_39

    nop

    :goto_52
    sub-double v14, v5, v12

    goto/32 :goto_6e

    nop

    :goto_53
    mul-double/2addr v5, v1

    goto/32 :goto_b

    nop

    :goto_54
    iget-wide v5, v0, Lmiuix/animation/f/l;->b:D

    goto/32 :goto_3e

    nop

    :goto_55
    if-gtz v9, :cond_1

    goto/32 :goto_3d

    :cond_1
    goto/32 :goto_c

    nop

    :goto_56
    sub-double v7, v3, v7

    goto/32 :goto_51

    nop

    :goto_57
    mul-double/2addr v14, v7

    goto/32 :goto_3

    nop

    :goto_58
    long-to-double v1, v1

    goto/32 :goto_2d

    nop

    :goto_59
    mul-double/2addr v5, v12

    goto/32 :goto_65

    nop

    :goto_5a
    mul-double/2addr v7, v3

    goto/32 :goto_40

    nop

    :goto_5b
    mul-double/2addr v3, v14

    goto/32 :goto_64

    nop

    :goto_5c
    mul-double/2addr v14, v5

    goto/32 :goto_61

    nop

    :goto_5d
    sub-double v3, p1, v3

    goto/32 :goto_54

    nop

    :goto_5e
    mul-double/2addr v11, v1

    goto/32 :goto_1a

    nop

    :goto_5f
    neg-double v5, v11

    goto/32 :goto_36

    nop

    :goto_60
    mul-double/2addr v9, v1

    goto/32 :goto_41

    nop

    :goto_61
    iget-wide v10, v0, Lmiuix/animation/f/l;->b:D

    goto/32 :goto_2c

    nop

    :goto_62
    iget-wide v3, v0, Lmiuix/animation/f/l;->i:D

    goto/32 :goto_5d

    nop

    :goto_63
    invoke-static {v10, v11, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    goto/32 :goto_20

    nop

    :goto_64
    neg-double v12, v12

    goto/32 :goto_33

    nop

    :goto_65
    iget-wide v12, v0, Lmiuix/animation/f/l;->a:D

    goto/32 :goto_6f

    nop

    :goto_66
    const-wide v10, 0x4005bf0a8b145769L    # Math.E

    goto/32 :goto_55

    nop

    :goto_67
    iget-wide v14, v0, Lmiuix/animation/f/l;->h:D

    goto/32 :goto_1b

    nop

    :goto_68
    neg-double v5, v5

    goto/32 :goto_17

    nop

    :goto_69
    iput v2, v1, Lmiuix/animation/f/e$a;->a:F

    goto/32 :goto_4c

    nop

    :goto_6a
    mul-double/2addr v9, v5

    goto/32 :goto_1c

    nop

    :goto_6b
    mul-double/2addr v5, v1

    goto/32 :goto_63

    nop

    :goto_6c
    invoke-static {v10, v11, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    goto/32 :goto_3a

    nop

    :goto_6d
    neg-double v5, v5

    goto/32 :goto_53

    nop

    :goto_6e
    div-double/2addr v7, v14

    goto/32 :goto_56

    nop

    :goto_6f
    neg-double v14, v12

    goto/32 :goto_5c

    nop

    :goto_70
    add-double/2addr v3, v12

    goto/32 :goto_6d

    nop

    :goto_71
    add-double/2addr v5, v2

    goto/32 :goto_7e

    nop

    :goto_72
    iget-wide v2, v0, Lmiuix/animation/f/l;->i:D

    goto/32 :goto_71

    nop

    :goto_73
    mul-double/2addr v3, v12

    goto/32 :goto_2b

    nop

    :goto_74
    mul-double/2addr v3, v1

    goto/32 :goto_37

    nop

    :goto_75
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    goto/32 :goto_23

    nop

    :goto_76
    mul-double/2addr v7, v12

    goto/32 :goto_14

    nop

    :goto_77
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    goto/32 :goto_4d

    nop

    :goto_78
    mul-double/2addr v12, v1

    goto/32 :goto_48

    nop

    :goto_79
    iget-wide v12, v0, Lmiuix/animation/f/l;->f:D

    goto/32 :goto_31

    nop

    :goto_7a
    mul-double/2addr v5, v3

    goto/32 :goto_2

    nop

    :goto_7b
    return-object v1

    :goto_7c
    goto/16 :goto_4b

    :goto_7d
    goto/32 :goto_13

    nop

    :goto_7e
    double-to-float v2, v5

    goto/32 :goto_69

    nop
.end method

.method public a(F)Lmiuix/animation/f/l;
    .locals 2

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_0

    float-to-double v0, p1

    iput-wide v0, p0, Lmiuix/animation/f/l;->b:D

    const/4 p1, 0x0

    iput-boolean p1, p0, Lmiuix/animation/f/l;->c:Z

    return-object p0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Damping ratio must be non-negative"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method a(D)V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    invoke-static {p1, p2}, Ljava/lang/Math;->abs(D)D

    move-result-wide p1

    goto/32 :goto_1

    nop

    :goto_1
    iput-wide p1, p0, Lmiuix/animation/f/l;->d:D

    goto/32 :goto_3

    nop

    :goto_2
    const-wide v0, 0x404f400000000000L    # 62.5

    goto/32 :goto_6

    nop

    :goto_3
    iget-wide p1, p0, Lmiuix/animation/f/l;->d:D

    goto/32 :goto_2

    nop

    :goto_4
    return-void

    :goto_5
    iput-wide p1, p0, Lmiuix/animation/f/l;->e:D

    goto/32 :goto_4

    nop

    :goto_6
    mul-double/2addr p1, v0

    goto/32 :goto_5

    nop
.end method

.method public a(FF)Z
    .locals 4

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result p2

    float-to-double v0, p2

    iget-wide v2, p0, Lmiuix/animation/f/l;->e:D

    cmpg-double p2, v0, v2

    if-gez p2, :cond_0

    invoke-virtual {p0}, Lmiuix/animation/f/l;->a()F

    move-result p2

    sub-float/2addr p1, p2

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    float-to-double p1, p1

    iget-wide v0, p0, Lmiuix/animation/f/l;->d:D

    cmpg-double p1, p1, v0

    if-gez p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public b(F)Lmiuix/animation/f/l;
    .locals 2

    float-to-double v0, p1

    iput-wide v0, p0, Lmiuix/animation/f/l;->i:D

    return-object p0
.end method

.method public c(F)Lmiuix/animation/f/l;
    .locals 2

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-lez v0, :cond_0

    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    iput-wide v0, p0, Lmiuix/animation/f/l;->a:D

    const/4 p1, 0x0

    iput-boolean p1, p0, Lmiuix/animation/f/l;->c:Z

    return-object p0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Spring stiffness constant must be positive."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
