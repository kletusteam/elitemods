.class Lmiuix/animation/f/b$d;
.super Lmiuix/animation/f/b$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/animation/f/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "d"
.end annotation


# instance fields
.field private final b:Ljava/lang/Runnable;

.field private final c:Landroid/os/Handler;

.field private d:J


# direct methods
.method constructor <init>(Lmiuix/animation/f/b$a;)V
    .locals 2

    invoke-direct {p0, p1}, Lmiuix/animation/f/b$c;-><init>(Lmiuix/animation/f/b$a;)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lmiuix/animation/f/b$d;->d:J

    new-instance p1, Lmiuix/animation/f/c;

    invoke-direct {p1, p0}, Lmiuix/animation/f/c;-><init>(Lmiuix/animation/f/b$d;)V

    iput-object p1, p0, Lmiuix/animation/f/b$d;->b:Ljava/lang/Runnable;

    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p1, p0, Lmiuix/animation/f/b$d;->c:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lmiuix/animation/f/b$d;J)J
    .locals 0

    iput-wide p1, p0, Lmiuix/animation/f/b$d;->d:J

    return-wide p1
.end method


# virtual methods
.method a()Z
    .locals 2

    goto/32 :goto_7

    nop

    :goto_0
    const/4 v0, 0x0

    :goto_1
    goto/32 :goto_6

    nop

    :goto_2
    const/4 v0, 0x1

    goto/32 :goto_9

    nop

    :goto_3
    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_4
    iget-object v1, p0, Lmiuix/animation/f/b$d;->c:Landroid/os/Handler;

    goto/32 :goto_3

    nop

    :goto_5
    if-eq v0, v1, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_2

    nop

    :goto_6
    return v0

    :goto_7
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_8
    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    goto/32 :goto_5

    nop

    :goto_9
    goto :goto_1

    :goto_a
    goto/32 :goto_0

    nop
.end method

.method b()V
    .locals 4

    goto/32 :goto_3

    nop

    :goto_0
    const-wide/16 v2, 0xa

    goto/32 :goto_1

    nop

    :goto_1
    sub-long/2addr v2, v0

    goto/32 :goto_9

    nop

    :goto_2
    sub-long/2addr v0, v2

    goto/32 :goto_0

    nop

    :goto_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    goto/32 :goto_7

    nop

    :goto_4
    iget-object v2, p0, Lmiuix/animation/f/b$d;->c:Landroid/os/Handler;

    goto/32 :goto_8

    nop

    :goto_5
    return-void

    :goto_6
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto/32 :goto_4

    nop

    :goto_7
    iget-wide v2, p0, Lmiuix/animation/f/b$d;->d:J

    goto/32 :goto_2

    nop

    :goto_8
    iget-object v3, p0, Lmiuix/animation/f/b$d;->b:Ljava/lang/Runnable;

    goto/32 :goto_a

    nop

    :goto_9
    const-wide/16 v0, 0x0

    goto/32 :goto_6

    nop

    :goto_a
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/32 :goto_5

    nop
.end method
