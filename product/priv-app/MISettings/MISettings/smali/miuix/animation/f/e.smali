.class public abstract Lmiuix/animation/f/e;
.super Ljava/lang/Object;

# interfaces
.implements Lmiuix/animation/f/b$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/animation/f/e$c;,
        Lmiuix/animation/f/e$b;,
        Lmiuix/animation/f/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lmiuix/animation/f/e<",
        "TT;>;>",
        "Ljava/lang/Object;",
        "Lmiuix/animation/f/b$b;"
    }
.end annotation


# instance fields
.field a:F

.field b:F

.field c:Z

.field final d:Ljava/lang/Object;

.field final e:Lmiuix/animation/g/b;

.field f:Z

.field g:F

.field h:F

.field private i:J

.field private j:F

.field private k:J

.field private final l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lmiuix/animation/f/e$b;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lmiuix/animation/f/e$c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Object;Lmiuix/animation/g/b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">(TK;",
            "Lmiuix/animation/g/b<",
            "TK;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/animation/f/e;->a:F

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lmiuix/animation/f/e;->b:F

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmiuix/animation/f/e;->c:Z

    iput-boolean v1, p0, Lmiuix/animation/f/e;->f:Z

    iput v0, p0, Lmiuix/animation/f/e;->g:F

    iget v0, p0, Lmiuix/animation/f/e;->g:F

    neg-float v0, v0

    iput v0, p0, Lmiuix/animation/f/e;->h:F

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmiuix/animation/f/e;->i:J

    iput-wide v0, p0, Lmiuix/animation/f/e;->k:J

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/animation/f/e;->l:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/animation/f/e;->m:Ljava/util/ArrayList;

    iput-object p1, p0, Lmiuix/animation/f/e;->d:Ljava/lang/Object;

    iput-object p2, p0, Lmiuix/animation/f/e;->e:Lmiuix/animation/g/b;

    iget-object p1, p0, Lmiuix/animation/f/e;->e:Lmiuix/animation/g/b;

    sget-object p2, Lmiuix/animation/g/A;->g:Lmiuix/animation/g/A;

    if-eq p1, p2, :cond_4

    sget-object p2, Lmiuix/animation/g/A;->h:Lmiuix/animation/g/A;

    if-eq p1, p2, :cond_4

    sget-object p2, Lmiuix/animation/g/A;->i:Lmiuix/animation/g/A;

    if-ne p1, p2, :cond_0

    goto :goto_1

    :cond_0
    sget-object p2, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    if-ne p1, p2, :cond_1

    const/high16 p1, 0x3b800000    # 0.00390625f

    iput p1, p0, Lmiuix/animation/f/e;->j:F

    goto :goto_2

    :cond_1
    sget-object p2, Lmiuix/animation/g/A;->e:Lmiuix/animation/g/A;

    if-eq p1, p2, :cond_3

    sget-object p2, Lmiuix/animation/g/A;->f:Lmiuix/animation/g/A;

    if-ne p1, p2, :cond_2

    goto :goto_0

    :cond_2
    const/high16 p1, 0x3f800000    # 1.0f

    iput p1, p0, Lmiuix/animation/f/e;->j:F

    goto :goto_2

    :cond_3
    :goto_0
    const p1, 0x3b03126f    # 0.002f

    iput p1, p0, Lmiuix/animation/f/e;->j:F

    goto :goto_2

    :cond_4
    :goto_1
    const p1, 0x3dcccccd    # 0.1f

    iput p1, p0, Lmiuix/animation/f/e;->j:F

    :goto_2
    return-void
.end method

.method private static a(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/ArrayList<",
            "TT;>;)V"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(Z)V
    .locals 4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/animation/f/e;->f:Z

    invoke-static {}, Lmiuix/animation/f/b;->a()Lmiuix/animation/f/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lmiuix/animation/f/b;->a(Lmiuix/animation/f/b$b;)V

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lmiuix/animation/f/e;->i:J

    iput-boolean v0, p0, Lmiuix/animation/f/e;->c:Z

    :goto_0
    iget-object v1, p0, Lmiuix/animation/f/e;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lmiuix/animation/f/e;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmiuix/animation/f/e;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/animation/f/e$b;

    iget v2, p0, Lmiuix/animation/f/e;->b:F

    iget v3, p0, Lmiuix/animation/f/e;->a:F

    invoke-interface {v1, p0, p1, v2, v3}, Lmiuix/animation/f/e$b;->a(Lmiuix/animation/f/e;ZFF)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lmiuix/animation/f/e;->l:Ljava/util/ArrayList;

    invoke-static {p1}, Lmiuix/animation/f/e;->a(Ljava/util/ArrayList;)V

    return-void
.end method

.method private f()F
    .locals 2

    iget-object v0, p0, Lmiuix/animation/f/e;->e:Lmiuix/animation/g/b;

    iget-object v1, p0, Lmiuix/animation/f/e;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lmiuix/animation/g/b;->b(Ljava/lang/Object;)F

    move-result v0

    return v0
.end method

.method private g()V
    .locals 3

    iget-boolean v0, p0, Lmiuix/animation/f/e;->f:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/animation/f/e;->f:Z

    iget-boolean v0, p0, Lmiuix/animation/f/e;->c:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmiuix/animation/f/e;->f()F

    move-result v0

    iput v0, p0, Lmiuix/animation/f/e;->b:F

    :cond_0
    iget v0, p0, Lmiuix/animation/f/e;->b:F

    iget v1, p0, Lmiuix/animation/f/e;->g:F

    cmpl-float v1, v0, v1

    if-gtz v1, :cond_1

    iget v1, p0, Lmiuix/animation/f/e;->h:F

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_1

    invoke-static {}, Lmiuix/animation/f/b;->a()Lmiuix/animation/f/b;

    move-result-object v0

    iget-wide v1, p0, Lmiuix/animation/f/e;->k:J

    invoke-virtual {v0, p0, v1, v2}, Lmiuix/animation/f/b;->a(Lmiuix/animation/f/b$b;J)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Starting value need to be in between min value and max value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_0
    return-void
.end method


# virtual methods
.method public a(F)Lmiuix/animation/f/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-lez v0, :cond_0

    iput p1, p0, Lmiuix/animation/f/e;->j:F

    const/high16 v0, 0x3f400000    # 0.75f

    mul-float/2addr p1, v0

    invoke-virtual {p0, p1}, Lmiuix/animation/f/e;->e(F)V

    return-object p0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Minimum visible change must be positive."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(Lmiuix/animation/f/e$b;)Lmiuix/animation/f/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiuix/animation/f/e$b;",
            ")TT;"
        }
    .end annotation

    iget-object v0, p0, Lmiuix/animation/f/e;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/animation/f/e;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0
.end method

.method public a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiuix/animation/f/e$c;",
            ")TT;"
        }
    .end annotation

    invoke-virtual {p0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lmiuix/animation/f/e;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/animation/f/e;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0

    :cond_1
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Error: Update listeners must be added beforethe miuix.animation."

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a()V
    .locals 2

    invoke-virtual {p0}, Lmiuix/animation/f/e;->b()Lmiuix/animation/f/b;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/animation/f/b;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lmiuix/animation/f/e;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lmiuix/animation/f/e;->a(Z)V

    :cond_0
    return-void

    :cond_1
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Animations may only be canceled from the same thread as the animation handler"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(J)Z
    .locals 4

    iget-wide v0, p0, Lmiuix/animation/f/e;->i:J

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    const/4 v3, 0x0

    if-nez v2, :cond_0

    iput-wide p1, p0, Lmiuix/animation/f/e;->i:J

    iget p1, p0, Lmiuix/animation/f/e;->b:F

    invoke-virtual {p0, p1}, Lmiuix/animation/f/e;->b(F)V

    return v3

    :cond_0
    sub-long v0, p1, v0

    iput-wide p1, p0, Lmiuix/animation/f/e;->i:J

    invoke-virtual {p0, v0, v1}, Lmiuix/animation/f/e;->c(J)Z

    move-result p1

    iget p2, p0, Lmiuix/animation/f/e;->b:F

    iget v0, p0, Lmiuix/animation/f/e;->g:F

    invoke-static {p2, v0}, Ljava/lang/Math;->min(FF)F

    move-result p2

    iput p2, p0, Lmiuix/animation/f/e;->b:F

    iget p2, p0, Lmiuix/animation/f/e;->b:F

    iget v0, p0, Lmiuix/animation/f/e;->h:F

    invoke-static {p2, v0}, Ljava/lang/Math;->max(FF)F

    move-result p2

    iput p2, p0, Lmiuix/animation/f/e;->b:F

    iget p2, p0, Lmiuix/animation/f/e;->b:F

    invoke-virtual {p0, p2}, Lmiuix/animation/f/e;->b(F)V

    if-eqz p1, :cond_1

    invoke-direct {p0, v3}, Lmiuix/animation/f/e;->a(Z)V

    :cond_1
    return p1
.end method

.method public b()Lmiuix/animation/f/b;
    .locals 1

    invoke-static {}, Lmiuix/animation/f/b;->a()Lmiuix/animation/f/b;

    move-result-object v0

    return-object v0
.end method

.method b(F)V
    .locals 3

    goto/32 :goto_8

    nop

    :goto_0
    goto :goto_e

    :goto_1
    goto/32 :goto_9

    nop

    :goto_2
    add-int/lit8 p1, p1, 0x1

    goto/32 :goto_0

    nop

    :goto_3
    invoke-interface {v0, p0, v1, v2}, Lmiuix/animation/f/e$c;->a(Lmiuix/animation/f/e;FF)V

    :goto_4
    goto/32 :goto_2

    nop

    :goto_5
    if-lt p1, v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_11

    nop

    :goto_6
    return-void

    :goto_7
    iget v1, p0, Lmiuix/animation/f/e;->b:F

    goto/32 :goto_15

    nop

    :goto_8
    iget-object v0, p0, Lmiuix/animation/f/e;->e:Lmiuix/animation/g/b;

    goto/32 :goto_17

    nop

    :goto_9
    iget-object p1, p0, Lmiuix/animation/f/e;->m:Ljava/util/ArrayList;

    goto/32 :goto_14

    nop

    :goto_a
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_5

    nop

    :goto_b
    iget-object v0, p0, Lmiuix/animation/f/e;->m:Ljava/util/ArrayList;

    goto/32 :goto_12

    nop

    :goto_c
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_f

    nop

    :goto_d
    const/4 p1, 0x0

    :goto_e
    goto/32 :goto_13

    nop

    :goto_f
    if-nez v0, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_b

    nop

    :goto_10
    check-cast v0, Lmiuix/animation/f/e$c;

    goto/32 :goto_7

    nop

    :goto_11
    iget-object v0, p0, Lmiuix/animation/f/e;->m:Ljava/util/ArrayList;

    goto/32 :goto_c

    nop

    :goto_12
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_10

    nop

    :goto_13
    iget-object v0, p0, Lmiuix/animation/f/e;->m:Ljava/util/ArrayList;

    goto/32 :goto_a

    nop

    :goto_14
    invoke-static {p1}, Lmiuix/animation/f/e;->a(Ljava/util/ArrayList;)V

    goto/32 :goto_6

    nop

    :goto_15
    iget v2, p0, Lmiuix/animation/f/e;->a:F

    goto/32 :goto_3

    nop

    :goto_16
    invoke-virtual {v0, v1, p1}, Lmiuix/animation/g/b;->a(Ljava/lang/Object;F)V

    goto/32 :goto_d

    nop

    :goto_17
    iget-object v1, p0, Lmiuix/animation/f/e;->d:Ljava/lang/Object;

    goto/32 :goto_16

    nop
.end method

.method public b(J)V
    .locals 3

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-gez v2, :cond_0

    move-wide p1, v0

    :cond_0
    iput-wide p1, p0, Lmiuix/animation/f/e;->k:J

    return-void
.end method

.method c()F
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    mul-float/2addr v0, v1

    goto/32 :goto_3

    nop

    :goto_1
    const/high16 v1, 0x3f400000    # 0.75f

    goto/32 :goto_0

    nop

    :goto_2
    iget v0, p0, Lmiuix/animation/f/e;->j:F

    goto/32 :goto_1

    nop

    :goto_3
    return v0
.end method

.method public c(F)Lmiuix/animation/f/e;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    iput p1, p0, Lmiuix/animation/f/e;->b:F

    const/4 p1, 0x1

    iput-boolean p1, p0, Lmiuix/animation/f/e;->c:Z

    return-object p0
.end method

.method abstract c(J)Z
.end method

.method public d(F)Lmiuix/animation/f/e;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    iput p1, p0, Lmiuix/animation/f/e;->a:F

    return-object p0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/animation/f/e;->f:Z

    return v0
.end method

.method public e()V
    .locals 2

    invoke-virtual {p0}, Lmiuix/animation/f/e;->b()Lmiuix/animation/f/b;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/animation/f/b;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lmiuix/animation/f/e;->f:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmiuix/animation/f/e;->g()V

    :cond_0
    return-void

    :cond_1
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Animations may only be started on the same thread as the animation handler"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method abstract e(F)V
.end method
