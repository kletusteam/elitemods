.class public final Lmiuix/animation/f/j;
.super Lmiuix/animation/f/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiuix/animation/f/e<",
        "Lmiuix/animation/f/j;",
        ">;"
    }
.end annotation


# instance fields
.field private n:Lmiuix/animation/f/l;

.field private o:F

.field private p:Z


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">(TK;",
            "Lmiuix/animation/g/b<",
            "TK;>;F)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lmiuix/animation/f/e;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lmiuix/animation/f/j;->n:Lmiuix/animation/f/l;

    const p1, 0x7f7fffff    # Float.MAX_VALUE

    iput p1, p0, Lmiuix/animation/f/j;->o:F

    const/4 p1, 0x0

    iput-boolean p1, p0, Lmiuix/animation/f/j;->p:Z

    new-instance p1, Lmiuix/animation/f/l;

    invoke-direct {p1, p3}, Lmiuix/animation/f/l;-><init>(F)V

    iput-object p1, p0, Lmiuix/animation/f/j;->n:Lmiuix/animation/f/l;

    return-void
.end method

.method private i()V
    .locals 4

    iget-object v0, p0, Lmiuix/animation/f/j;->n:Lmiuix/animation/f/l;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lmiuix/animation/f/l;->a()F

    move-result v0

    float-to-double v0, v0

    iget v2, p0, Lmiuix/animation/f/e;->g:F

    float-to-double v2, v2

    cmpl-double v2, v0, v2

    if-gtz v2, :cond_1

    iget v2, p0, Lmiuix/animation/f/e;->h:F

    float-to-double v2, v2

    cmpg-double v0, v0, v2

    if-ltz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Final position of the spring cannot be less than the min value."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Final position of the spring cannot be greater than the max value."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Incomplete SpringAnimation: Either final position or a spring force needs to be set."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method a(FF)Z
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {v0, p1, p2}, Lmiuix/animation/f/l;->a(FF)Z

    move-result p1

    goto/32 :goto_1

    nop

    :goto_1
    return p1

    :goto_2
    iget-object v0, p0, Lmiuix/animation/f/j;->n:Lmiuix/animation/f/l;

    goto/32 :goto_0

    nop
.end method

.method c(J)Z
    .locals 20

    goto/32 :goto_33

    nop

    :goto_0
    iget v1, v0, Lmiuix/animation/f/e;->b:F

    goto/32 :goto_1d

    nop

    :goto_1
    invoke-static {v1, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    goto/32 :goto_e

    nop

    :goto_2
    cmpl-float v1, v1, v5

    goto/32 :goto_2a

    nop

    :goto_3
    invoke-virtual/range {v13 .. v19}, Lmiuix/animation/f/l;->a(DDJ)Lmiuix/animation/f/e$a;

    move-result-object v1

    goto/32 :goto_40

    nop

    :goto_4
    move-wide/from16 v18, p1

    goto/32 :goto_3

    nop

    :goto_5
    iget v1, v0, Lmiuix/animation/f/e;->b:F

    goto/32 :goto_20

    nop

    :goto_6
    invoke-virtual {v1}, Lmiuix/animation/f/l;->a()F

    move-result v1

    goto/32 :goto_3b

    nop

    :goto_7
    float-to-double v14, v5

    goto/32 :goto_21

    nop

    :goto_8
    iput v1, v0, Lmiuix/animation/f/e;->a:F

    :goto_9
    goto/32 :goto_1f

    nop

    :goto_a
    invoke-virtual/range {v6 .. v12}, Lmiuix/animation/f/l;->a(DDJ)Lmiuix/animation/f/e$a;

    move-result-object v1

    goto/32 :goto_19

    nop

    :goto_b
    const v5, 0x7f7fffff    # Float.MAX_VALUE

    goto/32 :goto_28

    nop

    :goto_c
    float-to-double v5, v1

    goto/32 :goto_29

    nop

    :goto_d
    invoke-virtual {v6, v7}, Lmiuix/animation/f/l;->b(F)Lmiuix/animation/f/l;

    goto/32 :goto_25

    nop

    :goto_e
    iput v1, v0, Lmiuix/animation/f/e;->b:F

    goto/32 :goto_15

    nop

    :goto_f
    iget v5, v1, Lmiuix/animation/f/e$a;->a:F

    goto/32 :goto_11

    nop

    :goto_10
    iget v1, v1, Lmiuix/animation/f/e$a;->b:F

    goto/32 :goto_8

    nop

    :goto_11
    iput v5, v0, Lmiuix/animation/f/e;->b:F

    goto/32 :goto_34

    nop

    :goto_12
    return v2

    :goto_13
    goto/32 :goto_39

    nop

    :goto_14
    iget-object v6, v0, Lmiuix/animation/f/j;->n:Lmiuix/animation/f/l;

    goto/32 :goto_36

    nop

    :goto_15
    iget v1, v0, Lmiuix/animation/f/e;->b:F

    goto/32 :goto_49

    nop

    :goto_16
    goto :goto_9

    :goto_17
    goto/32 :goto_31

    nop

    :goto_18
    iget-object v6, v0, Lmiuix/animation/f/j;->n:Lmiuix/animation/f/l;

    goto/32 :goto_0

    nop

    :goto_19
    iget-object v6, v0, Lmiuix/animation/f/j;->n:Lmiuix/animation/f/l;

    goto/32 :goto_42

    nop

    :goto_1a
    iget v1, v0, Lmiuix/animation/f/e;->a:F

    goto/32 :goto_41

    nop

    :goto_1b
    invoke-virtual {v0, v1, v5}, Lmiuix/animation/f/j;->a(FF)Z

    move-result v1

    goto/32 :goto_52

    nop

    :goto_1c
    iput v4, v0, Lmiuix/animation/f/e;->a:F

    goto/32 :goto_2e

    nop

    :goto_1d
    float-to-double v7, v1

    goto/32 :goto_1a

    nop

    :goto_1e
    if-nez v6, :cond_0

    goto/32 :goto_45

    :cond_0
    goto/32 :goto_14

    nop

    :goto_1f
    iget v1, v0, Lmiuix/animation/f/e;->b:F

    goto/32 :goto_30

    nop

    :goto_20
    iget v5, v0, Lmiuix/animation/f/e;->a:F

    goto/32 :goto_1b

    nop

    :goto_21
    iget v1, v1, Lmiuix/animation/f/e$a;->b:F

    goto/32 :goto_c

    nop

    :goto_22
    iget v1, v0, Lmiuix/animation/f/e;->a:F

    goto/32 :goto_2f

    nop

    :goto_23
    iget-object v1, v0, Lmiuix/animation/f/j;->n:Lmiuix/animation/f/l;

    goto/32 :goto_3a

    nop

    :goto_24
    iput v4, v0, Lmiuix/animation/f/e;->a:F

    goto/32 :goto_46

    nop

    :goto_25
    iput v5, v0, Lmiuix/animation/f/j;->o:F

    goto/32 :goto_26

    nop

    :goto_26
    iget-object v13, v0, Lmiuix/animation/f/j;->n:Lmiuix/animation/f/l;

    goto/32 :goto_4b

    nop

    :goto_27
    const-wide/16 v11, 0x2

    goto/32 :goto_3d

    nop

    :goto_28
    if-nez v1, :cond_1

    goto/32 :goto_13

    :cond_1
    goto/32 :goto_3f

    nop

    :goto_29
    move-wide/from16 v16, v5

    goto/32 :goto_38

    nop

    :goto_2a
    if-nez v1, :cond_2

    goto/32 :goto_17

    :cond_2
    goto/32 :goto_23

    nop

    :goto_2b
    iput v5, v0, Lmiuix/animation/f/e;->b:F

    goto/32 :goto_10

    nop

    :goto_2c
    float-to-double v14, v1

    goto/32 :goto_22

    nop

    :goto_2d
    iput v1, v0, Lmiuix/animation/f/e;->b:F

    goto/32 :goto_5

    nop

    :goto_2e
    iput-boolean v3, v0, Lmiuix/animation/f/j;->p:Z

    goto/32 :goto_12

    nop

    :goto_2f
    float-to-double v5, v1

    goto/32 :goto_50

    nop

    :goto_30
    iget v5, v0, Lmiuix/animation/f/e;->h:F

    goto/32 :goto_1

    nop

    :goto_31
    iget-object v13, v0, Lmiuix/animation/f/j;->n:Lmiuix/animation/f/l;

    goto/32 :goto_4d

    nop

    :goto_32
    iget-object v1, v0, Lmiuix/animation/f/j;->n:Lmiuix/animation/f/l;

    goto/32 :goto_6

    nop

    :goto_33
    move-object/from16 v0, p0

    goto/32 :goto_3e

    nop

    :goto_34
    iget v1, v1, Lmiuix/animation/f/e$a;->b:F

    goto/32 :goto_37

    nop

    :goto_35
    invoke-virtual {v1}, Lmiuix/animation/f/l;->a()F

    move-result v1

    goto/32 :goto_53

    nop

    :goto_36
    invoke-virtual {v6, v1}, Lmiuix/animation/f/l;->b(F)Lmiuix/animation/f/l;

    goto/32 :goto_44

    nop

    :goto_37
    iput v1, v0, Lmiuix/animation/f/e;->a:F

    goto/32 :goto_16

    nop

    :goto_38
    invoke-virtual/range {v13 .. v19}, Lmiuix/animation/f/l;->a(DDJ)Lmiuix/animation/f/e$a;

    move-result-object v1

    goto/32 :goto_f

    nop

    :goto_39
    iget v1, v0, Lmiuix/animation/f/j;->o:F

    goto/32 :goto_2

    nop

    :goto_3a
    invoke-virtual {v1}, Lmiuix/animation/f/l;->a()F

    goto/32 :goto_18

    nop

    :goto_3b
    iput v1, v0, Lmiuix/animation/f/e;->b:F

    goto/32 :goto_24

    nop

    :goto_3c
    invoke-static {v1, v5}, Ljava/lang/Math;->min(FF)F

    move-result v1

    goto/32 :goto_2d

    nop

    :goto_3d
    div-long v18, p1, v11

    goto/32 :goto_4a

    nop

    :goto_3e
    iget-boolean v1, v0, Lmiuix/animation/f/j;->p:Z

    goto/32 :goto_4f

    nop

    :goto_3f
    iget v1, v0, Lmiuix/animation/f/j;->o:F

    goto/32 :goto_43

    nop

    :goto_40
    iget v5, v1, Lmiuix/animation/f/e$a;->a:F

    goto/32 :goto_2b

    nop

    :goto_41
    float-to-double v9, v1

    goto/32 :goto_27

    nop

    :goto_42
    iget v7, v0, Lmiuix/animation/f/j;->o:F

    goto/32 :goto_d

    nop

    :goto_43
    cmpl-float v6, v1, v5

    goto/32 :goto_1e

    nop

    :goto_44
    iput v5, v0, Lmiuix/animation/f/j;->o:F

    :goto_45
    goto/32 :goto_4e

    nop

    :goto_46
    return v2

    :goto_47
    goto/32 :goto_4c

    nop

    :goto_48
    const/4 v3, 0x0

    goto/32 :goto_51

    nop

    :goto_49
    iget v5, v0, Lmiuix/animation/f/e;->g:F

    goto/32 :goto_3c

    nop

    :goto_4a
    move-wide/from16 v11, v18

    goto/32 :goto_a

    nop

    :goto_4b
    iget v5, v1, Lmiuix/animation/f/e$a;->a:F

    goto/32 :goto_7

    nop

    :goto_4c
    return v3

    :goto_4d
    iget v1, v0, Lmiuix/animation/f/e;->b:F

    goto/32 :goto_2c

    nop

    :goto_4e
    iget-object v1, v0, Lmiuix/animation/f/j;->n:Lmiuix/animation/f/l;

    goto/32 :goto_35

    nop

    :goto_4f
    const/4 v2, 0x1

    goto/32 :goto_48

    nop

    :goto_50
    move-wide/from16 v16, v5

    goto/32 :goto_4

    nop

    :goto_51
    const/4 v4, 0x0

    goto/32 :goto_b

    nop

    :goto_52
    if-nez v1, :cond_3

    goto/32 :goto_47

    :cond_3
    goto/32 :goto_32

    nop

    :goto_53
    iput v1, v0, Lmiuix/animation/f/e;->b:F

    goto/32 :goto_1c

    nop
.end method

.method public e()V
    .locals 3

    invoke-direct {p0}, Lmiuix/animation/f/j;->i()V

    iget-object v0, p0, Lmiuix/animation/f/j;->n:Lmiuix/animation/f/l;

    invoke-virtual {p0}, Lmiuix/animation/f/e;->c()F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Lmiuix/animation/f/l;->a(D)V

    invoke-super {p0}, Lmiuix/animation/f/e;->e()V

    return-void
.end method

.method e(F)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-void
.end method

.method public f()Z
    .locals 4

    iget-object v0, p0, Lmiuix/animation/f/j;->n:Lmiuix/animation/f/l;

    iget-wide v0, v0, Lmiuix/animation/f/l;->b:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public g()Lmiuix/animation/f/l;
    .locals 1

    iget-object v0, p0, Lmiuix/animation/f/j;->n:Lmiuix/animation/f/l;

    return-object v0
.end method

.method public h()V
    .locals 2

    invoke-virtual {p0}, Lmiuix/animation/f/j;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lmiuix/animation/f/e;->b()Lmiuix/animation/f/b;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/animation/f/b;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lmiuix/animation/f/e;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/animation/f/j;->p:Z

    :cond_0
    return-void

    :cond_1
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Animations may only be started on the same thread as the animation handler"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Spring animations can only come to an end when there is damping"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
