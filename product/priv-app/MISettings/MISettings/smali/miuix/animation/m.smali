.class public interface abstract Lmiuix/animation/m;
.super Ljava/lang/Object;

# interfaces
.implements Lmiuix/animation/j;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/animation/m$a;,
        Lmiuix/animation/m$b;
    }
.end annotation


# virtual methods
.method public abstract a(FFFF)Lmiuix/animation/m;
.end method

.method public varargs abstract a(F[Lmiuix/animation/m$b;)Lmiuix/animation/m;
.end method

.method public abstract a(I)Lmiuix/animation/m;
    .param p1    # I
        .annotation build Landroidx/annotation/IntRange;
            from = -0x1L
            to = 0x3L
        .end annotation
    .end param
.end method

.method public abstract a(Landroid/view/MotionEvent;)V
.end method

.method public varargs abstract a(Landroid/view/View;[Lmiuix/animation/a/a;)V
.end method

.method public abstract b(FFFF)Lmiuix/animation/m;
.end method

.method public varargs abstract b(F[Lmiuix/animation/m$b;)Lmiuix/animation/m;
.end method

.method public abstract d()V
.end method

.method public varargs abstract d([Lmiuix/animation/a/a;)V
.end method

.method public varargs abstract g([Lmiuix/animation/a/a;)V
.end method

.method public abstract setTint(I)Lmiuix/animation/m;
.end method
