.class public Lmiuix/animation/d;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/animation/d$a;
    }
.end annotation


# static fields
.field private static a:Landroid/os/Looper;

.field private static b:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Lmiuix/animation/e;",
            "Lmiuix/animation/d$a;",
            ">;"
        }
    .end annotation
.end field

.field private static d:F

.field private static e:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lmiuix/animation/a;

    invoke-direct {v0}, Lmiuix/animation/a;-><init>()V

    invoke-static {v0}, Lmiuix/animation/d/x;->a(Ljava/lang/Runnable;)V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    sput-object v0, Lmiuix/animation/d;->a:Landroid/os/Looper;

    sget-object v0, Lmiuix/animation/d;->a:Landroid/os/Looper;

    invoke-static {v0}, Lmiuix/animation/d;->a(Landroid/os/Looper;)V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lmiuix/animation/d;->b:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lmiuix/animation/d;->c:Ljava/util/concurrent/ConcurrentHashMap;

    const/high16 v0, 0x41480000    # 12.5f

    sput v0, Lmiuix/animation/d;->d:F

    return-void
.end method

.method private static a([Landroid/view/View;[Lmiuix/animation/e;)Lmiuix/animation/d$a;
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x0

    move-object v3, v0

    move v2, v1

    :goto_0
    array-length v4, p0

    if-ge v1, v4, :cond_2

    aget-object v4, p0, v1

    sget-object v5, Lmiuix/animation/ViewTarget;->k:Lmiuix/animation/l;

    invoke-static {v4, v5}, Lmiuix/animation/d;->a(Ljava/lang/Object;Lmiuix/animation/l;)Lmiuix/animation/e;

    move-result-object v4

    aput-object v4, p1, v1

    sget-object v4, Lmiuix/animation/d;->c:Ljava/util/concurrent/ConcurrentHashMap;

    aget-object v5, p1, v1

    invoke-virtual {v4, v5}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiuix/animation/d$a;

    if-nez v3, :cond_0

    move-object v3, v4

    goto :goto_1

    :cond_0
    if-eq v3, v4, :cond_1

    const/4 v2, 0x1

    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    if-eqz v2, :cond_3

    goto :goto_2

    :cond_3
    move-object v0, v3

    :goto_2
    return-object v0
.end method

.method public static a(Ljava/lang/Object;Lmiuix/animation/l;)Lmiuix/animation/e;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lmiuix/animation/l<",
            "TT;>;)",
            "Lmiuix/animation/e;"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    instance-of v1, p0, Lmiuix/animation/e;

    if-eqz v1, :cond_1

    check-cast p0, Lmiuix/animation/e;

    return-object p0

    :cond_1
    sget-object v1, Lmiuix/animation/d;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/animation/e;

    invoke-virtual {v2}, Lmiuix/animation/e;->e()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v3, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    return-object v2

    :cond_3
    if-eqz p1, :cond_4

    invoke-interface {p1, p0}, Lmiuix/animation/l;->a(Ljava/lang/Object;)Lmiuix/animation/e;

    move-result-object p0

    if-eqz p0, :cond_4

    invoke-static {p0}, Lmiuix/animation/d;->a(Lmiuix/animation/e;)Lmiuix/animation/h;

    return-object p0

    :cond_4
    return-object v0
.end method

.method public static a(Lmiuix/animation/e;)Lmiuix/animation/h;
    .locals 3

    sget-object v0, Lmiuix/animation/d;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/animation/d$a;

    if-nez v0, :cond_0

    new-instance v0, Lmiuix/animation/d$a;

    const/4 v1, 0x1

    new-array v1, v1, [Lmiuix/animation/e;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lmiuix/animation/d$a;-><init>([Lmiuix/animation/e;Lmiuix/animation/a;)V

    sget-object v1, Lmiuix/animation/d;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lmiuix/animation/d$a;

    if-eqz p0, :cond_0

    move-object v0, p0

    :cond_0
    return-object v0
.end method

.method public static varargs a([Landroid/view/View;)Lmiuix/animation/h;
    .locals 5

    array-length v0, p0

    if-eqz v0, :cond_3

    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    aget-object p0, p0, v1

    sget-object v0, Lmiuix/animation/ViewTarget;->k:Lmiuix/animation/l;

    invoke-static {p0, v0}, Lmiuix/animation/d;->a(Ljava/lang/Object;Lmiuix/animation/l;)Lmiuix/animation/e;

    move-result-object p0

    invoke-static {p0}, Lmiuix/animation/d;->a(Lmiuix/animation/e;)Lmiuix/animation/h;

    move-result-object p0

    return-object p0

    :cond_0
    array-length v0, p0

    new-array v0, v0, [Lmiuix/animation/e;

    invoke-static {p0, v0}, Lmiuix/animation/d;->a([Landroid/view/View;[Lmiuix/animation/e;)Lmiuix/animation/d$a;

    move-result-object p0

    if-nez p0, :cond_2

    new-instance p0, Lmiuix/animation/d$a;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lmiuix/animation/d$a;-><init>([Lmiuix/animation/e;Lmiuix/animation/a;)V

    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    sget-object v4, Lmiuix/animation/d;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, v3, p0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiuix/animation/d$a;

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lmiuix/animation/d$a;->f()V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-object p0

    :cond_3
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "useAt can not be applied to empty views array"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method static synthetic a()V
    .locals 0

    invoke-static {}, Lmiuix/animation/d;->i()V

    return-void
.end method

.method private static a(I)V
    .locals 1

    sget-object v0, Lmiuix/animation/d;->e:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lmiuix/animation/d;->e:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    return-void
.end method

.method private static a(Landroid/os/Looper;)V
    .locals 1

    new-instance v0, Lmiuix/animation/b;

    invoke-direct {v0, p0}, Lmiuix/animation/b;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lmiuix/animation/d;->e:Landroid/os/Handler;

    return-void
.end method

.method private static a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lmiuix/animation/d;->a(Ljava/lang/Object;Lmiuix/animation/l;)Lmiuix/animation/e;

    move-result-object p0

    invoke-static {p0}, Lmiuix/animation/d;->b(Lmiuix/animation/e;)V

    return-void
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Runnable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Ljava/lang/Runnable;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lmiuix/animation/d;->a(Ljava/lang/Object;Lmiuix/animation/l;)Lmiuix/animation/e;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Lmiuix/animation/e;->b(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Ljava/util/List;)V
    .locals 0

    invoke-static {p0}, Lmiuix/animation/d;->b(Ljava/util/List;)V

    return-void
.end method

.method static synthetic a(Z)V
    .locals 0

    invoke-static {p0}, Lmiuix/animation/d;->b(Z)V

    return-void
.end method

.method public static varargs a([Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/SafeVarargs;
    .end annotation

    invoke-static {p0}, Lmiuix/animation/h/a;->a([Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p0, Lmiuix/animation/d;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/animation/e;

    invoke-static {v0}, Lmiuix/animation/d;->b(Lmiuix/animation/e;)V

    goto :goto_0

    :cond_0
    array-length v0, p0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_1

    aget-object v2, p0, v1

    invoke-static {v2}, Lmiuix/animation/d;->a(Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method static synthetic b()V
    .locals 0

    invoke-static {}, Lmiuix/animation/d;->h()V

    return-void
.end method

.method private static b(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lmiuix/animation/e;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/animation/e;

    invoke-virtual {v0}, Lmiuix/animation/e;->f()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    const/4 v2, 0x0

    new-array v3, v2, [Lmiuix/animation/g/b;

    invoke-virtual {v1, v3}, Lmiuix/animation/d/d;->a([Lmiuix/animation/g/b;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    invoke-virtual {v1}, Lmiuix/animation/d/d;->c()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lmiuix/animation/e;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [Lmiuix/animation/e;

    aput-object v0, v1, v2

    invoke-static {v1}, Lmiuix/animation/d;->a([Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private static b(Lmiuix/animation/e;)V
    .locals 2

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lmiuix/animation/e;->a()V

    sget-object v0, Lmiuix/animation/d;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/animation/d$a;

    iget-object v1, p0, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    invoke-virtual {v1}, Lmiuix/animation/d/d;->a()V

    invoke-virtual {p0}, Lmiuix/animation/e;->d()Lmiuix/animation/e/a;

    move-result-object p0

    invoke-virtual {p0}, Lmiuix/animation/e/a;->a()V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/animation/d$a;->f()V

    :cond_0
    return-void
.end method

.method private static b(Z)V
    .locals 4

    const/4 v0, 0x1

    invoke-static {v0}, Lmiuix/animation/d;->a(I)V

    if-eqz p0, :cond_0

    invoke-static {}, Lmiuix/animation/h/g;->c()Z

    move-result p0

    if-eqz p0, :cond_0

    sget-object p0, Lmiuix/animation/d;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/animation/e;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exist target:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lmiuix/animation/e;->e()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, " , target isValid : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lmiuix/animation/e;->f()Z

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lmiuix/animation/h/g;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    sget-object p0, Lmiuix/animation/d;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result p0

    if-lez p0, :cond_1

    sget-object p0, Lmiuix/animation/d;->e:Landroid/os/Handler;

    const-wide/16 v1, 0x4e20

    invoke-virtual {p0, v0, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    :cond_1
    invoke-static {v0}, Lmiuix/animation/d;->a(I)V

    :goto_1
    return-void
.end method

.method public static varargs b([Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)V"
        }
    .end annotation

    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    aget-object v2, p0, v1

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lmiuix/animation/d;->a(Ljava/lang/Object;Lmiuix/animation/l;)Lmiuix/animation/e;

    move-result-object v2

    if-nez v2, :cond_0

    goto :goto_1

    :cond_0
    sget-object v3, Lmiuix/animation/d;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/animation/d$a;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lmiuix/animation/d$a;->g()V

    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method static synthetic c()Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1

    sget-object v0, Lmiuix/animation/d;->c:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method public static varargs c([Ljava/lang/Object;)Lmiuix/animation/k;
    .locals 2

    array-length v0, p0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    aget-object p0, p0, v0

    sget-object v0, Lmiuix/animation/r;->k:Lmiuix/animation/l;

    invoke-static {p0, v0}, Lmiuix/animation/d;->a(Ljava/lang/Object;Lmiuix/animation/l;)Lmiuix/animation/e;

    move-result-object p0

    invoke-static {p0}, Lmiuix/animation/d;->a(Lmiuix/animation/e;)Lmiuix/animation/h;

    move-result-object p0

    goto :goto_0

    :cond_0
    new-instance p0, Lmiuix/animation/r;

    invoke-direct {p0}, Lmiuix/animation/r;-><init>()V

    const-wide/16 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lmiuix/animation/e;->b(J)V

    invoke-static {p0}, Lmiuix/animation/d;->a(Lmiuix/animation/e;)Lmiuix/animation/h;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object p0

    return-object p0
.end method

.method static synthetic d()Landroid/os/Handler;
    .locals 1

    sget-object v0, Lmiuix/animation/d;->e:Landroid/os/Handler;

    return-object v0
.end method

.method public static e()Landroid/os/Looper;
    .locals 1

    sget-object v0, Lmiuix/animation/d;->a:Landroid/os/Looper;

    return-object v0
.end method

.method public static f()Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lmiuix/animation/e;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lmiuix/animation/h/g;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lmiuix/animation/d;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    move v2, v1

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiuix/animation/e;

    invoke-virtual {v3}, Lmiuix/animation/e;->f()Z

    move-result v3

    if-nez v3, :cond_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "current sImplMap total : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Lmiuix/animation/d;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "  , target invalid count :  "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lmiuix/animation/h/g;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    sget-object v0, Lmiuix/animation/d;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static g()F
    .locals 1

    sget-object v0, Lmiuix/animation/d;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method private static h()V
    .locals 6

    sget-object v0, Lmiuix/animation/d;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/animation/e;

    invoke-virtual {v1}, Lmiuix/animation/e;->f()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    const-wide/16 v4, 0x1

    invoke-virtual {v1, v4, v5}, Lmiuix/animation/e;->a(J)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    new-array v4, v3, [Lmiuix/animation/g/b;

    invoke-virtual {v2, v4}, Lmiuix/animation/d/d;->a([Lmiuix/animation/g/b;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v1, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    invoke-virtual {v2}, Lmiuix/animation/d/d;->c()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lmiuix/animation/e;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    const/4 v2, 0x1

    new-array v2, v2, [Lmiuix/animation/e;

    aput-object v1, v2, v3

    invoke-static {v2}, Lmiuix/animation/d;->a([Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private static i()V
    .locals 4

    sget-object v0, Lmiuix/animation/d;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    if-lez v0, :cond_0

    sget-object v0, Lmiuix/animation/d;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x400

    rem-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Lmiuix/animation/c;

    invoke-direct {v0}, Lmiuix/animation/c;-><init>()V

    invoke-static {v0}, Lmiuix/animation/d/x;->a(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method
