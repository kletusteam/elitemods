.class public Lmiuix/animation/d/h;
.super Ljava/lang/Object;

# interfaces
.implements Lmiuix/animation/f/b$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/animation/d/h$a;
    }
.end annotation


# static fields
.field private static final a:Landroid/os/HandlerThread;

.field public static final b:Lmiuix/animation/d/q;

.field static c:Landroid/os/Handler;


# instance fields
.field private volatile d:J

.field private e:J

.field private f:[J

.field private g:I

.field private volatile h:Z

.field private i:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "AnimRunnerThread"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiuix/animation/d/h;->a:Landroid/os/HandlerThread;

    invoke-static {}, Lmiuix/animation/d;->e()Landroid/os/Looper;

    move-result-object v0

    invoke-static {v0}, Lmiuix/animation/d/h;->a(Landroid/os/Looper;)V

    sget-object v0, Lmiuix/animation/d/h;->a:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Lmiuix/animation/d/q;

    sget-object v1, Lmiuix/animation/d/h;->a:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lmiuix/animation/d/q;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lmiuix/animation/d/h;->b:Lmiuix/animation/d/q;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x10

    iput-wide v0, p0, Lmiuix/animation/d/h;->d:J

    const/4 v0, 0x5

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    iput-object v0, p0, Lmiuix/animation/d/h;->f:[J

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/animation/d/h;->g:I

    return-void

    nop

    :array_0
    .array-data 8
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method synthetic constructor <init>(Lmiuix/animation/d/f;)V
    .locals 0

    invoke-direct {p0}, Lmiuix/animation/d/h;-><init>()V

    return-void
.end method

.method private a([J)J
    .locals 9

    array-length v0, p1

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    move v4, v1

    move-wide v5, v2

    :goto_0
    if-ge v1, v0, :cond_1

    aget-wide v7, p1, v1

    add-long/2addr v5, v7

    cmp-long v7, v7, v2

    if-lez v7, :cond_0

    add-int/lit8 v4, v4, 0x1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    if-lez v4, :cond_2

    int-to-long v0, v4

    div-long v2, v5, v0

    :cond_2
    return-wide v2
.end method

.method static synthetic a()V
    .locals 0

    invoke-static {}, Lmiuix/animation/d/h;->h()V

    return-void
.end method

.method public static a(Landroid/os/Looper;)V
    .locals 1

    new-instance v0, Lmiuix/animation/d/f;

    invoke-direct {v0, p0}, Lmiuix/animation/d/f;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lmiuix/animation/d/h;->c:Landroid/os/Handler;

    return-void
.end method

.method private static a(Ljava/util/Collection;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lmiuix/animation/e;",
            ">;Z)V"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lmiuix/animation/d/h;->b:Lmiuix/animation/d/q;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/animation/e;

    iget-object v1, v0, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    const/4 v2, 0x0

    new-array v3, v2, [Lmiuix/animation/g/b;

    invoke-virtual {v1, v3}, Lmiuix/animation/d/d;->a([Lmiuix/animation/g/b;)Z

    move-result v1

    iget-object v3, v0, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    invoke-virtual {v3}, Lmiuix/animation/d/d;->c()Z

    move-result v3

    invoke-virtual {v0}, Lmiuix/animation/e;->g()Z

    move-result v4

    if-eqz v1, :cond_2

    iget-object v0, v0, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    invoke-virtual {v0, p1}, Lmiuix/animation/d/d;->a(Z)V

    goto :goto_0

    :cond_2
    if-nez v3, :cond_1

    if-nez v1, :cond_1

    const-wide/16 v5, 0x1

    invoke-virtual {v0, v5, v6}, Lmiuix/animation/e;->a(J)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v4, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Lmiuix/animation/e;

    aput-object v0, v1, v2

    invoke-static {v1}, Lmiuix/animation/d;->a([Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method private b(J)J
    .locals 5

    iget-object v0, p0, Lmiuix/animation/d/h;->f:[J

    invoke-direct {p0, v0}, Lmiuix/animation/d/h;->a([J)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    move-wide p1, v0

    :cond_0
    cmp-long v0, p1, v2

    const-wide/16 v1, 0x10

    if-eqz v0, :cond_1

    cmp-long v0, p1, v1

    if-lez v0, :cond_2

    :cond_1
    move-wide p1, v1

    :cond_2
    long-to-float p1, p1

    iget p2, p0, Lmiuix/animation/d/h;->i:F

    div-float/2addr p1, p2

    float-to-double p1, p1

    invoke-static {p1, p2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide p1

    double-to-long p1, p1

    return-wide p1
.end method

.method static synthetic b()V
    .locals 0

    invoke-static {}, Lmiuix/animation/d/h;->g()V

    return-void
.end method

.method private c(J)V
    .locals 5

    iget-wide v0, p0, Lmiuix/animation/d/h;->e:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iput-wide p1, p0, Lmiuix/animation/d/h;->e:J

    goto :goto_0

    :cond_0
    sub-long v2, p1, v0

    iput-wide p1, p0, Lmiuix/animation/d/h;->e:J

    :goto_0
    iget p1, p0, Lmiuix/animation/d/h;->g:I

    rem-int/lit8 p2, p1, 0x5

    iget-object v0, p0, Lmiuix/animation/d/h;->f:[J

    aput-wide v2, v0, p2

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lmiuix/animation/d/h;->g:I

    invoke-direct {p0, v2, v3}, Lmiuix/animation/d/h;->b(J)J

    move-result-wide p1

    iput-wide p1, p0, Lmiuix/animation/d/h;->d:J

    return-void
.end method

.method public static e()Lmiuix/animation/d/h;
    .locals 1

    sget-object v0, Lmiuix/animation/d/h$a;->a:Lmiuix/animation/d/h;

    return-object v0
.end method

.method private static g()V
    .locals 4

    invoke-static {}, Lmiuix/animation/d/h;->e()Lmiuix/animation/d/h;

    move-result-object v0

    iget-boolean v1, v0, Lmiuix/animation/d/h;->h:Z

    if-nez v1, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lmiuix/animation/h/g;->c()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    new-array v1, v2, [Ljava/lang/Object;

    const-string v3, "AnimRunner.endAnimation"

    invoke-static {v3, v1}, Lmiuix/animation/h/g;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    iput-boolean v2, v0, Lmiuix/animation/d/h;->h:Z

    invoke-static {}, Lmiuix/animation/f/b;->a()Lmiuix/animation/f/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmiuix/animation/f/b;->a(Lmiuix/animation/f/b$b;)V

    return-void
.end method

.method private static h()V
    .locals 4

    invoke-static {}, Lmiuix/animation/d/h;->e()Lmiuix/animation/d/h;

    move-result-object v0

    iget-boolean v1, v0, Lmiuix/animation/d/h;->h:Z

    if-eqz v1, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lmiuix/animation/h/g;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "AnimRunner.start"

    invoke-static {v2, v1}, Lmiuix/animation/h/g;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    invoke-static {}, Lmiuix/animation/d;->g()F

    move-result v1

    iput v1, v0, Lmiuix/animation/d/h;->i:F

    const/4 v1, 0x1

    iput-boolean v1, v0, Lmiuix/animation/d/h;->h:Z

    invoke-static {}, Lmiuix/animation/f/b;->a()Lmiuix/animation/f/b;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lmiuix/animation/f/b;->a(Lmiuix/animation/f/b$b;J)V

    return-void
.end method


# virtual methods
.method public a(Lmiuix/animation/d/y;)V
    .locals 2

    iget-object v0, p1, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    new-instance v1, Lmiuix/animation/d/g;

    invoke-direct {v1, p0, p1}, Lmiuix/animation/d/g;-><init>(Lmiuix/animation/d/h;Lmiuix/animation/d/y;)V

    invoke-virtual {v0, v1}, Lmiuix/animation/e;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(Lmiuix/animation/e;Lmiuix/animation/b/a;Lmiuix/animation/b/a;Lmiuix/animation/a/b;)V
    .locals 1

    new-instance v0, Lmiuix/animation/d/y;

    invoke-direct {v0, p1, p2, p3, p4}, Lmiuix/animation/d/y;-><init>(Lmiuix/animation/e;Lmiuix/animation/b/a;Lmiuix/animation/b/a;Lmiuix/animation/a/b;)V

    invoke-virtual {p0, v0}, Lmiuix/animation/d/h;->a(Lmiuix/animation/d/y;)V

    return-void
.end method

.method public varargs a(Lmiuix/animation/e;[Ljava/lang/String;)V
    .locals 4

    invoke-static {p2}, Lmiuix/animation/h/a;->a([Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x3

    if-eqz v0, :cond_0

    iget-object v0, p1, Lmiuix/animation/e;->b:Lmiuix/animation/d/s;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    sget-object v0, Lmiuix/animation/d/h;->b:Lmiuix/animation/d/q;

    new-instance v2, Lmiuix/animation/d/e;

    const/4 v3, 0x0

    invoke-direct {v2, p1, v1, p2, v3}, Lmiuix/animation/d/e;-><init>(Lmiuix/animation/e;B[Ljava/lang/String;[Lmiuix/animation/g/b;)V

    invoke-virtual {v0, v2}, Lmiuix/animation/d/q;->a(Lmiuix/animation/d/e;)V

    return-void
.end method

.method public varargs a(Lmiuix/animation/e;[Lmiuix/animation/g/b;)V
    .locals 4

    sget-object v0, Lmiuix/animation/d/h;->b:Lmiuix/animation/d/q;

    new-instance v1, Lmiuix/animation/d/e;

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-direct {v1, p1, v2, v3, p2}, Lmiuix/animation/d/e;-><init>(Lmiuix/animation/e;B[Ljava/lang/String;[Lmiuix/animation/g/b;)V

    invoke-virtual {v0, v1}, Lmiuix/animation/d/q;->a(Lmiuix/animation/d/e;)V

    return-void
.end method

.method public a(J)Z
    .locals 5

    invoke-direct {p0, p1, p2}, Lmiuix/animation/d/h;->c(J)V

    iget-boolean p1, p0, Lmiuix/animation/d/h;->h:Z

    if-eqz p1, :cond_6

    invoke-static {}, Lmiuix/animation/d;->f()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v0, 0x0

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/animation/e;

    iget-object v3, v2, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    new-array v4, v0, [Lmiuix/animation/g/b;

    invoke-virtual {v3, v4}, Lmiuix/animation/d/d;->a([Lmiuix/animation/g/b;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v2, v2, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    invoke-virtual {v2}, Lmiuix/animation/d/d;->b()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0

    :cond_1
    const/16 p2, 0x1f4

    if-le v1, p2, :cond_2

    const/4 v0, 0x1

    :cond_2
    if-nez v0, :cond_3

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p2

    if-gtz p2, :cond_4

    :cond_3
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p2

    if-nez p2, :cond_5

    :cond_4
    invoke-static {p1, v0}, Lmiuix/animation/d/h;->a(Ljava/util/Collection;Z)V

    :cond_5
    sget-object p2, Lmiuix/animation/d/h;->b:Lmiuix/animation/d/q;

    invoke-virtual {p2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object p2

    const/4 v1, 0x3

    iput v1, p2, Landroid/os/Message;->what:I

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p2, Landroid/os/Message;->obj:Ljava/lang/Object;

    sget-object v1, Lmiuix/animation/d/h;->b:Lmiuix/animation/d/q;

    invoke-virtual {v1, p2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    if-eqz v0, :cond_6

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p2

    if-lez p2, :cond_6

    invoke-static {p1, v0}, Lmiuix/animation/d/h;->a(Ljava/util/Collection;Z)V

    :cond_6
    iget-boolean p1, p0, Lmiuix/animation/d/h;->h:Z

    return p1
.end method

.method public varargs b(Lmiuix/animation/e;[Lmiuix/animation/g/b;)V
    .locals 4

    invoke-static {p2}, Lmiuix/animation/h/a;->a([Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x3

    if-eqz v0, :cond_0

    iget-object v0, p1, Lmiuix/animation/e;->b:Lmiuix/animation/d/s;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    sget-object v0, Lmiuix/animation/d/h;->b:Lmiuix/animation/d/q;

    new-instance v2, Lmiuix/animation/d/e;

    const/4 v3, 0x0

    invoke-direct {v2, p1, v1, v3, p2}, Lmiuix/animation/d/e;-><init>(Lmiuix/animation/e;B[Ljava/lang/String;[Lmiuix/animation/g/b;)V

    invoke-virtual {v0, v2}, Lmiuix/animation/d/q;->a(Lmiuix/animation/d/e;)V

    return-void
.end method

.method c()V
    .locals 2

    goto/32 :goto_9

    nop

    :goto_0
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_1
    goto/32 :goto_3

    nop

    :goto_2
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    goto/32 :goto_7

    nop

    :goto_3
    return-void

    :goto_4
    invoke-static {}, Lmiuix/animation/d/h;->g()V

    goto/32 :goto_5

    nop

    :goto_5
    goto :goto_1

    :goto_6
    goto/32 :goto_8

    nop

    :goto_7
    if-eq v0, v1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_4

    nop

    :goto_8
    sget-object v0, Lmiuix/animation/d/h;->c:Landroid/os/Handler;

    goto/32 :goto_a

    nop

    :goto_9
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_a
    const/4 v1, 0x1

    goto/32 :goto_0

    nop
.end method

.method public d()J
    .locals 2

    iget-wide v0, p0, Lmiuix/animation/d/h;->d:J

    return-wide v0
.end method

.method f()V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_1
    return-void

    :goto_2
    sget-object v0, Lmiuix/animation/d/h;->c:Landroid/os/Handler;

    goto/32 :goto_8

    nop

    :goto_3
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    goto/32 :goto_5

    nop

    :goto_4
    invoke-static {}, Lmiuix/animation/d/h;->h()V

    goto/32 :goto_6

    nop

    :goto_5
    if-eq v0, v1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_4

    nop

    :goto_6
    goto :goto_a

    :goto_7
    goto/32 :goto_2

    nop

    :goto_8
    const/4 v1, 0x0

    goto/32 :goto_9

    nop

    :goto_9
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_a
    goto/32 :goto_1

    nop
.end method
