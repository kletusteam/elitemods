.class public Lmiuix/animation/d/m;
.super Ljava/lang/Object;


# instance fields
.field a:Lmiuix/animation/e/a;

.field b:Lmiuix/animation/e/a;

.field c:Lmiuix/animation/e;

.field private d:Lmiuix/animation/a/a;


# direct methods
.method public constructor <init>(Lmiuix/animation/e;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmiuix/animation/a/a;

    invoke-direct {v0}, Lmiuix/animation/a/a;-><init>()V

    iput-object v0, p0, Lmiuix/animation/d/m;->d:Lmiuix/animation/a/a;

    iput-object p1, p0, Lmiuix/animation/d/m;->c:Lmiuix/animation/e;

    new-instance v0, Lmiuix/animation/e/a;

    invoke-direct {v0, p1}, Lmiuix/animation/e/a;-><init>(Lmiuix/animation/e;)V

    iput-object v0, p0, Lmiuix/animation/d/m;->a:Lmiuix/animation/e/a;

    new-instance v0, Lmiuix/animation/e/a;

    invoke-direct {v0, p1}, Lmiuix/animation/e/a;-><init>(Lmiuix/animation/e;)V

    iput-object v0, p0, Lmiuix/animation/d/m;->b:Lmiuix/animation/e/a;

    return-void
.end method


# virtual methods
.method public a()Lmiuix/animation/e/a;
    .locals 1

    iget-object v0, p0, Lmiuix/animation/d/m;->b:Lmiuix/animation/e/a;

    return-object v0
.end method

.method public a(Lmiuix/animation/b/a;Lmiuix/animation/a/b;)V
    .locals 2

    if-nez p2, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Lmiuix/animation/b/a;->c()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lmiuix/animation/d/m;->d:Lmiuix/animation/a/a;

    invoke-virtual {p1}, Lmiuix/animation/b/a;->b()Lmiuix/animation/a/a;

    move-result-object p1

    invoke-virtual {v1, p1}, Lmiuix/animation/a/a;->b(Lmiuix/animation/a/a;)V

    iget-object p1, p0, Lmiuix/animation/d/m;->d:Lmiuix/animation/a/a;

    invoke-virtual {p2, p1}, Lmiuix/animation/a/b;->a(Lmiuix/animation/a/a;)V

    iget-object p1, p0, Lmiuix/animation/d/m;->a:Lmiuix/animation/e/a;

    iget-object p2, p0, Lmiuix/animation/d/m;->d:Lmiuix/animation/a/a;

    invoke-virtual {p1, v0, p2}, Lmiuix/animation/e/a;->a(Ljava/lang/Object;Lmiuix/animation/a/a;)Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lmiuix/animation/d/m;->d:Lmiuix/animation/a/a;

    invoke-virtual {p1}, Lmiuix/animation/a/a;->a()V

    return-void

    :cond_1
    iget-object p1, p0, Lmiuix/animation/d/m;->a:Lmiuix/animation/e/a;

    invoke-virtual {p1, v0, v0}, Lmiuix/animation/e/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object p1, p0, Lmiuix/animation/d/m;->c:Lmiuix/animation/e;

    iget-object p1, p1, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    iget-object p1, p1, Lmiuix/animation/d/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object p1

    iget-object p2, p0, Lmiuix/animation/d/m;->a:Lmiuix/animation/e/a;

    invoke-virtual {p2, v0, v0, p1}, Lmiuix/animation/e/a;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V

    iget-object p2, p0, Lmiuix/animation/d/m;->a:Lmiuix/animation/e/a;

    invoke-virtual {p2, v0, v0, p1}, Lmiuix/animation/e/a;->c(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V

    iget-object p2, p0, Lmiuix/animation/d/m;->a:Lmiuix/animation/e/a;

    invoke-virtual {p2, v0, v0, p1}, Lmiuix/animation/e/a;->b(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V

    iget-object p1, p0, Lmiuix/animation/d/m;->a:Lmiuix/animation/e/a;

    invoke-virtual {p1, v0, v0}, Lmiuix/animation/e/a;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object p1, p0, Lmiuix/animation/d/m;->a:Lmiuix/animation/e/a;

    invoke-virtual {p1, v0}, Lmiuix/animation/e/a;->a(Ljava/lang/Object;)V

    iget-object p1, p0, Lmiuix/animation/d/m;->d:Lmiuix/animation/a/a;

    invoke-virtual {p1}, Lmiuix/animation/a/a;->a()V

    return-void
.end method
