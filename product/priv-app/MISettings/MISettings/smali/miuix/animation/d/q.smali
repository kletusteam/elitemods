.class Lmiuix/animation/d/q;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/animation/d/q$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lmiuix/animation/e;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lmiuix/animation/e;",
            "Lmiuix/animation/d/e;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lmiuix/animation/e;",
            "Lmiuix/animation/d/y;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiuix/animation/d/k;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiuix/animation/e;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiuix/animation/d/y;",
            ">;"
        }
    .end annotation
.end field

.field private g:Z

.field private h:J

.field private i:J

.field private j:I

.field private k:Z

.field private l:Z

.field private final m:[I


# direct methods
.method public constructor <init>(Landroid/os/Looper;)V
    .locals 2
    .param p1    # Landroid/os/Looper;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lmiuix/animation/d/q;->a:Ljava/util/Set;

    new-instance p1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {p1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object p1, p0, Lmiuix/animation/d/q;->b:Ljava/util/Map;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lmiuix/animation/d/q;->c:Ljava/util/Map;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lmiuix/animation/d/q;->d:Ljava/util/List;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lmiuix/animation/d/q;->e:Ljava/util/List;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lmiuix/animation/d/q;->f:Ljava/util/List;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmiuix/animation/d/q;->h:J

    iput-wide v0, p0, Lmiuix/animation/d/q;->i:J

    const/4 p1, 0x0

    iput p1, p0, Lmiuix/animation/d/q;->j:I

    const/4 p1, 0x2

    new-array p1, p1, [I

    iput-object p1, p0, Lmiuix/animation/d/q;->m:[I

    return-void
.end method

.method private a()V
    .locals 4

    iget-object v0, p0, Lmiuix/animation/d/q;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/animation/d/y;

    iget-object v2, p0, Lmiuix/animation/d/q;->a:Ljava/util/Set;

    iget-object v3, v1, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v2, v1, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    iget-object v2, v2, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    invoke-virtual {v2, v1}, Lmiuix/animation/d/d;->a(Lmiuix/animation/d/y;)V

    invoke-virtual {v1}, Lmiuix/animation/h/e;->a()Lmiuix/animation/h/e;

    move-result-object v1

    check-cast v1, Lmiuix/animation/d/y;

    if-nez v1, :cond_0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmiuix/animation/d/q;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-boolean v0, p0, Lmiuix/animation/d/q;->l:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/animation/d/q;->l:Z

    invoke-static {}, Lmiuix/animation/d/h;->e()Lmiuix/animation/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/animation/d/h;->f()V

    :cond_2
    return-void
.end method

.method private a(JJZ)V
    .locals 8

    iget-object v0, p0, Lmiuix/animation/d/q;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmiuix/animation/d/q;->d()V

    return-void

    :cond_0
    iput-wide p1, p0, Lmiuix/animation/d/q;->h:J

    invoke-static {}, Lmiuix/animation/d/h;->e()Lmiuix/animation/d/h;

    move-result-object p1

    invoke-virtual {p1}, Lmiuix/animation/d/h;->d()J

    move-result-wide p1

    iget v0, p0, Lmiuix/animation/d/q;->j:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const-wide/16 v2, 0x2

    mul-long/2addr v2, p1

    cmp-long v0, p3, v2

    if-lez v0, :cond_1

    goto :goto_0

    :cond_1
    move-wide p1, p3

    :goto_0
    iget-wide p3, p0, Lmiuix/animation/d/q;->i:J

    add-long/2addr p3, p1

    iput-wide p3, p0, Lmiuix/animation/d/q;->i:J

    iget p3, p0, Lmiuix/animation/d/q;->j:I

    add-int/2addr p3, v1

    iput p3, p0, Lmiuix/animation/d/q;->j:I

    invoke-direct {p0}, Lmiuix/animation/d/q;->c()I

    move-result p3

    iget-object p4, p0, Lmiuix/animation/d/q;->m:[I

    invoke-static {p3, p4}, Lmiuix/animation/d/x;->a(I[I)V

    iget-object p3, p0, Lmiuix/animation/d/q;->m:[I

    const/4 p4, 0x0

    aget p4, p3, p4

    aget p3, p3, v1

    iget-object v0, p0, Lmiuix/animation/d/q;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/animation/e;

    iget-object v2, v2, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    iget-object v3, p0, Lmiuix/animation/d/q;->f:Ljava/util/List;

    invoke-virtual {v2, v3}, Lmiuix/animation/d/d;->a(Ljava/util/List;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lmiuix/animation/d/q;->f:Ljava/util/List;

    invoke-direct {p0, v0, p3, p4}, Lmiuix/animation/d/q;->a(Ljava/util/List;II)V

    iget-object p3, p0, Lmiuix/animation/d/q;->d:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result p3

    xor-int/2addr p3, v1

    iput-boolean p3, p0, Lmiuix/animation/d/q;->g:Z

    sget-object p3, Lmiuix/animation/d/k;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object p4, p0, Lmiuix/animation/d/q;->d:Ljava/util/List;

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result p4

    invoke-virtual {p3, p4}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    iget-object p3, p0, Lmiuix/animation/d/q;->d:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_2
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_3

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    move-object v2, p4

    check-cast v2, Lmiuix/animation/d/k;

    iget-wide v3, p0, Lmiuix/animation/d/q;->i:J

    move-wide v5, p1

    move v7, p5

    invoke-virtual/range {v2 .. v7}, Lmiuix/animation/d/k;->a(JJZ)V

    goto :goto_2

    :cond_3
    iget-object p1, p0, Lmiuix/animation/d/q;->f:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    iget-object p1, p0, Lmiuix/animation/d/q;->d:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    return-void
.end method

.method private a(Ljava/util/List;II)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lmiuix/animation/d/y;",
            ">;II)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/animation/d/y;

    iget-object v0, v0, Lmiuix/animation/d/y;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/animation/d/k;

    invoke-direct {p0}, Lmiuix/animation/d/q;->b()Lmiuix/animation/d/k;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v3, p0, Lmiuix/animation/d/q;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v3, p3, :cond_1

    invoke-virtual {v2}, Lmiuix/animation/d/k;->c()I

    move-result v3

    invoke-virtual {v1}, Lmiuix/animation/d/k;->b()I

    move-result v4

    add-int/2addr v3, v4

    if-le v3, p2, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v2, v1}, Lmiuix/animation/h/e;->a(Lmiuix/animation/h/e;)V

    goto :goto_0

    :cond_2
    :goto_1
    iget-object v2, p0, Lmiuix/animation/d/q;->d:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    return-void
.end method

.method private static a(Lmiuix/animation/d/k;Lmiuix/animation/d/j;Lmiuix/animation/e/c;Lmiuix/animation/d/e;)V
    .locals 5

    iget-object v0, p2, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iget-byte v0, v0, Lmiuix/animation/d/c;->a:B

    invoke-static {v0}, Lmiuix/animation/d/k;->a(B)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-byte v1, p3, Lmiuix/animation/d/e;->b:B

    if-eqz v1, :cond_4

    iget-object v1, p3, Lmiuix/animation/d/e;->c:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v2, p2, Lmiuix/animation/e/c;->a:Lmiuix/animation/g/b;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_0
    iget-object v1, p2, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iget-byte v1, v1, Lmiuix/animation/d/c;->a:B

    invoke-static {v1}, Lmiuix/animation/d/k;->a(B)Z

    move-result v1

    if-eqz v1, :cond_4

    iget v1, p3, Lmiuix/animation/d/e;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p3, Lmiuix/animation/d/e;->e:I

    iget-byte v1, p3, Lmiuix/animation/d/e;->b:B

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    iget-object v1, p2, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iget-wide v1, v1, Lmiuix/animation/d/c;->h:D

    const-wide v3, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v1, v1, v3

    if-eqz v1, :cond_1

    iget-object v1, p2, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iget-wide v2, v1, Lmiuix/animation/d/c;->h:D

    iput-wide v2, v1, Lmiuix/animation/d/c;->i:D

    :cond_1
    iget-object v1, p0, Lmiuix/animation/d/k;->c:Lmiuix/animation/d/j;

    iget v2, v1, Lmiuix/animation/d/j;->f:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lmiuix/animation/d/j;->f:I

    iget v1, p1, Lmiuix/animation/d/j;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p1, Lmiuix/animation/d/j;->f:I

    goto :goto_0

    :cond_2
    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lmiuix/animation/d/k;->c:Lmiuix/animation/d/j;

    iget v2, v1, Lmiuix/animation/d/j;->e:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lmiuix/animation/d/j;->e:I

    iget v1, p1, Lmiuix/animation/d/j;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p1, Lmiuix/animation/d/j;->e:I

    :cond_3
    :goto_0
    iget-byte p3, p3, Lmiuix/animation/d/e;->b:B

    invoke-virtual {p2, p3}, Lmiuix/animation/e/c;->a(B)V

    invoke-static {p0, p1, p2, v0}, Lmiuix/animation/d/y;->a(Lmiuix/animation/d/k;Lmiuix/animation/d/j;Lmiuix/animation/e/c;B)V

    :cond_4
    return-void
.end method

.method private a(Lmiuix/animation/d/q$a;)V
    .locals 6

    iget-object v0, p1, Lmiuix/animation/d/q$a;->a:Lmiuix/animation/e;

    instance-of v0, v0, Lmiuix/animation/ViewTarget;

    iget-object v1, p1, Lmiuix/animation/d/q$a;->b:Lmiuix/animation/b/a;

    invoke-virtual {v1}, Lmiuix/animation/b/a;->d()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p1, Lmiuix/animation/d/q$a;->b:Lmiuix/animation/b/a;

    invoke-virtual {v3, v2}, Lmiuix/animation/b/a;->c(Ljava/lang/Object;)Lmiuix/animation/g/b;

    move-result-object v2

    iget-object v3, p1, Lmiuix/animation/d/q$a;->a:Lmiuix/animation/e;

    iget-object v3, v3, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    iget-object v3, v3, Lmiuix/animation/d/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiuix/animation/e/c;

    if-nez v3, :cond_1

    goto :goto_0

    :cond_1
    iget-object v4, p1, Lmiuix/animation/d/q$a;->b:Lmiuix/animation/b/a;

    iget-object v5, p1, Lmiuix/animation/d/q$a;->a:Lmiuix/animation/e;

    invoke-virtual {v4, v5, v2}, Lmiuix/animation/b/a;->a(Lmiuix/animation/e;Lmiuix/animation/g/b;)D

    move-result-wide v4

    if-eqz v3, :cond_2

    iget-object v2, v3, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iput-wide v4, v2, Lmiuix/animation/d/c;->j:D

    if-nez v0, :cond_0

    iget-object v2, p1, Lmiuix/animation/d/q$a;->a:Lmiuix/animation/e;

    invoke-virtual {v3, v2}, Lmiuix/animation/e/c;->a(Lmiuix/animation/e;)V

    goto :goto_0

    :cond_2
    instance-of v3, v2, Lmiuix/animation/g/c;

    if-eqz v3, :cond_3

    iget-object v3, p1, Lmiuix/animation/d/q$a;->a:Lmiuix/animation/e;

    check-cast v2, Lmiuix/animation/g/c;

    double-to-int v4, v4

    invoke-virtual {v3, v2, v4}, Lmiuix/animation/e;->a(Lmiuix/animation/g/c;I)V

    goto :goto_0

    :cond_3
    iget-object v3, p1, Lmiuix/animation/d/q$a;->a:Lmiuix/animation/e;

    double-to-float v4, v4

    invoke-virtual {v3, v2, v4}, Lmiuix/animation/e;->a(Lmiuix/animation/g/b;F)V

    goto :goto_0

    :cond_4
    iget-object v0, p1, Lmiuix/animation/d/q$a;->a:Lmiuix/animation/e;

    const/4 v1, 0x0

    new-array v1, v1, [Lmiuix/animation/g/b;

    invoke-virtual {v0, v1}, Lmiuix/animation/e;->a([Lmiuix/animation/g/b;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object p1, p1, Lmiuix/animation/d/q$a;->a:Lmiuix/animation/e;

    iget-object p1, p1, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    iget-object p1, p1, Lmiuix/animation/d/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    :cond_5
    return-void
.end method

.method private static a(Lmiuix/animation/d/y;Lmiuix/animation/d/e;Lmiuix/animation/d/j;)V
    .locals 8

    iget-object v0, p0, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    iget-object v0, v0, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    iget-object v0, v0, Lmiuix/animation/d/d;->b:Ljava/util/Set;

    iget-object v1, p0, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    iget-object v1, p0, Lmiuix/animation/d/y;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/animation/d/k;

    iget-object v3, p0, Lmiuix/animation/d/y;->l:Ljava/util/List;

    iget v4, v2, Lmiuix/animation/d/k;->d:I

    invoke-virtual {v2}, Lmiuix/animation/d/k;->b()I

    move-result v5

    add-int/2addr v5, v4

    :goto_0
    if-ge v4, v5, :cond_0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lmiuix/animation/e/c;

    if-eqz v6, :cond_1

    invoke-static {v2, p2, v6}, Lmiuix/animation/d/q;->a(Lmiuix/animation/d/k;Lmiuix/animation/d/j;Lmiuix/animation/e/c;)Z

    move-result v7

    if-nez v7, :cond_1

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    invoke-static {v2, p2, v6, p1}, Lmiuix/animation/d/q;->a(Lmiuix/animation/d/k;Lmiuix/animation/d/j;Lmiuix/animation/e/c;Lmiuix/animation/d/e;)V

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    if-nez v0, :cond_3

    iget-object p1, p0, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    iget-object p1, p1, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    iget-object p1, p1, Lmiuix/animation/d/d;->b:Ljava/util/Set;

    iget-object v0, p0, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-virtual {p2}, Lmiuix/animation/d/j;->a()Z

    move-result p1

    if-eqz p1, :cond_4

    iget p1, p2, Lmiuix/animation/d/j;->d:I

    if-lez p1, :cond_4

    iget-object p1, p0, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    iget-object p1, p1, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    iget-object p1, p1, Lmiuix/animation/d/d;->c:Ljava/util/Set;

    iget-object p2, p0, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    invoke-interface {p1, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    sget-object p1, Lmiuix/animation/d/y;->b:Ljava/util/Map;

    iget p2, p0, Lmiuix/animation/d/y;->d:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p1, p2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    iget-object p1, p1, Lmiuix/animation/e;->b:Lmiuix/animation/d/s;

    iget p0, p0, Lmiuix/animation/d/y;->d:I

    const/4 p2, 0x0

    invoke-virtual {p1, p2, p0, p2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object p0

    invoke-virtual {p0}, Landroid/os/Message;->sendToTarget()V

    :cond_4
    return-void
.end method

.method private a(Lmiuix/animation/e;Lmiuix/animation/h/e;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lmiuix/animation/h/e;",
            ">(",
            "Lmiuix/animation/e;",
            "TT;",
            "Ljava/util/Map<",
            "Lmiuix/animation/e;",
            "TT;>;)V"
        }
    .end annotation

    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/animation/h/e;

    if-nez v0, :cond_0

    invoke-interface {p3, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p2}, Lmiuix/animation/h/e;->a(Lmiuix/animation/h/e;)V

    :goto_0
    return-void
.end method

.method private static a(Lmiuix/animation/d/k;Lmiuix/animation/d/j;Lmiuix/animation/e/c;)Z
    .locals 3

    invoke-static {p2}, Lmiuix/animation/d/l;->a(Lmiuix/animation/e/c;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    iget-object v0, p2, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iget-byte v0, v0, Lmiuix/animation/d/c;->a:B

    invoke-static {v0}, Lmiuix/animation/d/k;->a(B)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/animation/d/k;->c:Lmiuix/animation/d/j;

    iget v2, v0, Lmiuix/animation/d/j;->e:I

    add-int/2addr v2, v1

    iput v2, v0, Lmiuix/animation/d/j;->e:I

    iget v0, p1, Lmiuix/animation/d/j;->e:I

    add-int/2addr v0, v1

    iput v0, p1, Lmiuix/animation/d/j;->e:I

    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Lmiuix/animation/e/c;->a(B)V

    iget-object v0, p2, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iget-byte v0, v0, Lmiuix/animation/d/c;->a:B

    invoke-static {p0, p1, p2, v0}, Lmiuix/animation/d/y;->a(Lmiuix/animation/d/k;Lmiuix/animation/d/j;Lmiuix/animation/e/c;B)V

    :cond_1
    return v1
.end method

.method private a(Lmiuix/animation/d/y;)Z
    .locals 2

    iget-object v0, p0, Lmiuix/animation/d/q;->c:Ljava/util/Map;

    iget-object v1, p1, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/animation/d/y;

    :goto_0
    if-eqz v0, :cond_1

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    iget-object v0, v0, Lmiuix/animation/h/e;->a:Lmiuix/animation/h/e;

    check-cast v0, Lmiuix/animation/d/y;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method private a(Lmiuix/animation/e;)Z
    .locals 2

    iget-object p1, p1, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    iget-object p1, p1, Lmiuix/animation/d/d;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lmiuix/animation/d/y;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    iget-object v1, p0, Lmiuix/animation/d/q;->c:Ljava/util/Map;

    invoke-direct {p0, v0, p1, v1}, Lmiuix/animation/d/q;->a(Lmiuix/animation/e;Lmiuix/animation/h/e;Ljava/util/Map;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method private a(Lmiuix/animation/e;Ljava/util/List;)Z
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiuix/animation/e;",
            "Ljava/util/List<",
            "Lmiuix/animation/d/y;",
            ">;)Z"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget-object v2, v1, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    move-object/from16 v3, p2

    invoke-virtual {v2, v3}, Lmiuix/animation/d/d;->a(Ljava/util/List;)V

    iget-object v2, v0, Lmiuix/animation/d/q;->b:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/animation/d/e;

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    const/4 v5, 0x0

    move v6, v5

    move v7, v6

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    const/4 v9, 0x1

    if-eqz v8, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lmiuix/animation/d/y;

    invoke-direct {v0, v8}, Lmiuix/animation/d/q;->a(Lmiuix/animation/d/y;)Z

    move-result v10

    if-eqz v10, :cond_0

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_0
    if-eqz v2, :cond_1

    iget-wide v11, v8, Lmiuix/animation/d/y;->k:J

    iget-wide v13, v2, Lmiuix/animation/d/e;->d:J

    cmp-long v11, v11, v13

    if-lez v11, :cond_1

    add-int/lit8 v6, v6, 0x1

    move v11, v6

    const/4 v6, 0x0

    goto :goto_1

    :cond_1
    move v11, v6

    move-object v6, v2

    :goto_1
    invoke-virtual {v8}, Lmiuix/animation/d/y;->c()Lmiuix/animation/d/j;

    move-result-object v12

    invoke-virtual {v12}, Lmiuix/animation/d/j;->b()Z

    move-result v13

    if-eqz v13, :cond_2

    invoke-static {v8, v6, v12}, Lmiuix/animation/d/q;->a(Lmiuix/animation/d/y;Lmiuix/animation/d/e;Lmiuix/animation/d/j;)V

    :cond_2
    invoke-static {}, Lmiuix/animation/h/g;->c()Z

    move-result v13

    if-eqz v13, :cond_4

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "---- updateAnim, target = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x6

    new-array v14, v14, [Ljava/lang/Object;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "key = "

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v10, v8, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v14, v5

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "useOp = "

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v14, v9

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "info.startTime = "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v9, v8, Lmiuix/animation/d/y;->k:J

    invoke-virtual {v6, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x2

    aput-object v6, v14, v9

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "opInfo.time = "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v2, :cond_3

    iget-wide v9, v2, Lmiuix/animation/d/e;->d:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    goto :goto_2

    :cond_3
    const/4 v10, 0x0

    :goto_2
    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x3

    aput-object v6, v14, v9

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "stats.isRunning = "

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Lmiuix/animation/d/j;->a()Z

    move-result v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/16 v16, 0x4

    aput-object v6, v14, v16

    const/4 v6, 0x5

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "stats = "

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v14, v6

    invoke-static {v13, v14}, Lmiuix/animation/h/g;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    :cond_4
    const/4 v9, 0x3

    const/16 v16, 0x4

    :goto_3
    invoke-virtual {v12}, Lmiuix/animation/d/j;->a()Z

    move-result v6

    if-nez v6, :cond_6

    iget-object v6, v1, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    iget v10, v12, Lmiuix/animation/d/j;->e:I

    iget v12, v12, Lmiuix/animation/d/j;->f:I

    if-le v10, v12, :cond_5

    move/from16 v9, v16

    :cond_5
    const/4 v10, 0x2

    invoke-virtual {v6, v8, v10, v9}, Lmiuix/animation/d/d;->a(Lmiuix/animation/d/y;II)V

    goto :goto_4

    :cond_6
    add-int/lit8 v7, v7, 0x1

    :goto_4
    move v6, v11

    goto/16 :goto_0

    :cond_7
    if-eqz v2, :cond_9

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v4

    if-eq v6, v4, :cond_8

    invoke-virtual {v2}, Lmiuix/animation/d/e;->a()Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_8
    iget-object v2, v0, Lmiuix/animation/d/q;->b:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->clear()V

    if-lez v7, :cond_a

    move v5, v9

    :cond_a
    return v5
.end method

.method private b()Lmiuix/animation/d/k;
    .locals 5

    iget-object v0, p0, Lmiuix/animation/d/q;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const v2, 0x7fffffff

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiuix/animation/d/k;

    invoke-virtual {v3}, Lmiuix/animation/d/k;->c()I

    move-result v4

    if-ge v4, v2, :cond_0

    move-object v1, v3

    move v2, v4

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method private c()I
    .locals 3

    iget-object v0, p0, Lmiuix/animation/d/q;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/animation/e;

    iget-object v2, v2, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    invoke-virtual {v2}, Lmiuix/animation/d/d;->b()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0

    :cond_0
    return v1
.end method

.method private d()V
    .locals 5

    iget-boolean v0, p0, Lmiuix/animation/d/q;->k:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lmiuix/animation/h/g;->c()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "total time = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v3, p0, Lmiuix/animation/d/q;->i:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "frame count = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lmiuix/animation/d/q;->j:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    const-string v2, "RunnerHandler.stopAnimRunner"

    invoke-static {v2, v0}, Lmiuix/animation/h/g;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iput-boolean v1, p0, Lmiuix/animation/d/q;->k:Z

    iput-boolean v1, p0, Lmiuix/animation/d/q;->l:Z

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lmiuix/animation/d/q;->i:J

    iput v1, p0, Lmiuix/animation/d/q;->j:I

    invoke-static {}, Lmiuix/animation/d/h;->e()Lmiuix/animation/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/animation/d/h;->c()V

    :cond_1
    return-void
.end method

.method private e()V
    .locals 5

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/animation/d/q;->g:Z

    iget-object v1, p0, Lmiuix/animation/d/q;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/animation/e;

    iget-object v4, p0, Lmiuix/animation/d/q;->f:Ljava/util/List;

    invoke-direct {p0, v2, v4}, Lmiuix/animation/d/q;->a(Lmiuix/animation/e;Ljava/util/List;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-direct {p0, v2}, Lmiuix/animation/d/q;->a(Lmiuix/animation/e;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_1

    :cond_0
    iget-object v3, p0, Lmiuix/animation/d/q;->e:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    :goto_1
    move v0, v3

    :goto_2
    iget-object v2, p0, Lmiuix/animation/d/q;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lmiuix/animation/d/q;->a:Ljava/util/Set;

    iget-object v2, p0, Lmiuix/animation/d/q;->e:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lmiuix/animation/d/q;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v1, p0, Lmiuix/animation/d/q;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-direct {p0}, Lmiuix/animation/d/q;->a()V

    move v0, v3

    :cond_3
    if-nez v0, :cond_4

    invoke-direct {p0}, Lmiuix/animation/d/q;->d()V

    :cond_4
    return-void
.end method


# virtual methods
.method public a(Lmiuix/animation/d/e;)V
    .locals 2

    iget-object v0, p1, Lmiuix/animation/d/e;->a:Lmiuix/animation/e;

    const/4 v1, 0x0

    new-array v1, v1, [Lmiuix/animation/g/b;

    invoke-virtual {v0, v1}, Lmiuix/animation/e;->a([Lmiuix/animation/g/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p1, Lmiuix/animation/d/e;->d:J

    iget-object v0, p0, Lmiuix/animation/d/q;->b:Ljava/util/Map;

    iget-object v1, p1, Lmiuix/animation/d/e;->a:Lmiuix/animation/e;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public a(Lmiuix/animation/e;Lmiuix/animation/b/a;)V
    .locals 2

    new-instance v0, Lmiuix/animation/d/q$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmiuix/animation/d/q$a;-><init>(Lmiuix/animation/d/p;)V

    iput-object p1, v0, Lmiuix/animation/d/q$a;->a:Lmiuix/animation/e;

    iget-boolean p1, p2, Lmiuix/animation/b/a;->d:Z

    if-eqz p1, :cond_0

    new-instance p1, Lmiuix/animation/b/a;

    invoke-direct {p1}, Lmiuix/animation/b/a;-><init>()V

    iput-object p1, v0, Lmiuix/animation/d/q$a;->b:Lmiuix/animation/b/a;

    iget-object p1, v0, Lmiuix/animation/d/q$a;->b:Lmiuix/animation/b/a;

    invoke-virtual {p1, p2}, Lmiuix/animation/b/a;->a(Lmiuix/animation/b/a;)V

    goto :goto_0

    :cond_0
    iput-object p2, v0, Lmiuix/animation/d/q$a;->b:Lmiuix/animation/b/a;

    :goto_0
    const/4 p1, 0x4

    invoke-virtual {p0, p1, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1    # Landroid/os/Message;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_5

    const/4 v2, 0x2

    if-eq v0, v2, :cond_4

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    goto/16 :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/animation/d/q;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    invoke-direct {p0}, Lmiuix/animation/d/q;->d()V

    goto :goto_0

    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lmiuix/animation/d/q$a;

    invoke-direct {p0, v0}, Lmiuix/animation/d/q;->a(Lmiuix/animation/d/q$a;)V

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lmiuix/animation/d/q;->l:Z

    if-eqz v0, :cond_6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {}, Lmiuix/animation/d/h;->e()Lmiuix/animation/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/animation/d/h;->d()J

    move-result-wide v5

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    iget-boolean v0, p0, Lmiuix/animation/d/q;->k:Z

    if-nez v0, :cond_3

    iput-boolean v1, p0, Lmiuix/animation/d/q;->k:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmiuix/animation/d/q;->i:J

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/animation/d/q;->j:I

    move-object v2, p0

    invoke-direct/range {v2 .. v7}, Lmiuix/animation/d/q;->a(JJZ)V

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lmiuix/animation/d/q;->g:Z

    if-nez v0, :cond_6

    iget-wide v0, p0, Lmiuix/animation/d/q;->h:J

    sub-long v5, v3, v0

    move-object v2, p0

    invoke-direct/range {v2 .. v7}, Lmiuix/animation/d/q;->a(JJZ)V

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lmiuix/animation/d/q;->e()V

    goto :goto_0

    :cond_5
    sget-object v0, Lmiuix/animation/d/y;->b:Ljava/util/Map;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/animation/d/y;

    if-eqz v0, :cond_6

    iget-object v1, v0, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    iget-object v2, p0, Lmiuix/animation/d/q;->c:Ljava/util/Map;

    invoke-direct {p0, v1, v0, v2}, Lmiuix/animation/d/q;->a(Lmiuix/animation/e;Lmiuix/animation/h/e;Ljava/util/Map;)V

    iget-boolean v0, p0, Lmiuix/animation/d/q;->g:Z

    if-nez v0, :cond_6

    invoke-direct {p0}, Lmiuix/animation/d/q;->a()V

    :cond_6
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    return-void
.end method
