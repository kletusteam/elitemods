.class public final Lmiuix/animation/d/s;
.super Landroid/os/Handler;


# instance fields
.field private final a:Lmiuix/animation/e;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiuix/animation/d/y;",
            ">;"
        }
    .end annotation
.end field

.field public final c:J


# direct methods
.method public constructor <init>(Lmiuix/animation/e;)V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/animation/d/s;->b:Ljava/util/List;

    iput-object p1, p0, Lmiuix/animation/d/s;->a:Lmiuix/animation/e;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    iput-wide v0, p0, Lmiuix/animation/d/s;->c:J

    return-void
.end method

.method private static a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lmiuix/animation/e/c;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/animation/e/c;

    iget-object v3, v2, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iget-wide v3, v3, Lmiuix/animation/d/c;->i:D

    invoke-static {v3, v4}, Lmiuix/animation/d/l;->a(D)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    invoke-interface {p0, v0}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    :cond_2
    return-void
.end method

.method private a(Lmiuix/animation/d/y;)V
    .locals 4

    invoke-static {}, Lmiuix/animation/h/g;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<<< onReplaced, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lmiuix/animation/d/s;->a:Lmiuix/animation/e;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", info.key = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lmiuix/animation/h/g;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p1}, Lmiuix/animation/d/y;->b()I

    move-result v0

    const/16 v1, 0xfa0

    if-gt v0, v1, :cond_1

    iget-object v0, p0, Lmiuix/animation/d/s;->a:Lmiuix/animation/e;

    invoke-virtual {v0}, Lmiuix/animation/e;->d()Lmiuix/animation/e/a;

    move-result-object v0

    iget-object v1, p1, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    iget-object v2, p1, Lmiuix/animation/d/y;->f:Ljava/lang/Object;

    iget-object v3, p1, Lmiuix/animation/d/y;->l:Ljava/util/List;

    invoke-virtual {v0, v1, v2, v3}, Lmiuix/animation/e/a;->b(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V

    :cond_1
    iget-object v0, p0, Lmiuix/animation/d/s;->a:Lmiuix/animation/e;

    invoke-virtual {v0}, Lmiuix/animation/e;->d()Lmiuix/animation/e/a;

    move-result-object v0

    iget-object v1, p1, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    iget-object v2, p1, Lmiuix/animation/d/y;->f:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lmiuix/animation/e/a;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lmiuix/animation/d/s;->a:Lmiuix/animation/e;

    invoke-virtual {v0}, Lmiuix/animation/e;->d()Lmiuix/animation/e/a;

    move-result-object v0

    iget-object v1, p1, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lmiuix/animation/e/a;->a(Ljava/lang/Object;)V

    iget-object v0, p1, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    iget-object v0, v0, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    iget-object p1, p1, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Lmiuix/animation/d/d;->a(Ljava/lang/Object;)V

    return-void
.end method

.method private a(Lmiuix/animation/d/y;I)V
    .locals 3

    invoke-static {}, Lmiuix/animation/h/g;->c()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<<< onEnd, "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lmiuix/animation/d/s;->a:Lmiuix/animation/e;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", info.key = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lmiuix/animation/h/g;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-direct {p0, v1, p1}, Lmiuix/animation/d/s;->a(ZLmiuix/animation/d/y;)V

    invoke-static {p1, v1}, Lmiuix/animation/d/s;->a(Lmiuix/animation/d/y;Z)V

    const/4 v0, 0x4

    if-ne p2, v0, :cond_1

    iget-object p2, p1, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    invoke-virtual {p2}, Lmiuix/animation/e;->d()Lmiuix/animation/e/a;

    move-result-object p2

    iget-object v0, p1, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    iget-object v1, p1, Lmiuix/animation/d/y;->f:Ljava/lang/Object;

    invoke-virtual {p2, v0, v1}, Lmiuix/animation/e/a;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object p2, p1, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    invoke-virtual {p2}, Lmiuix/animation/e;->d()Lmiuix/animation/e/a;

    move-result-object p2

    iget-object v0, p1, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    iget-object v1, p1, Lmiuix/animation/d/y;->f:Ljava/lang/Object;

    invoke-virtual {p2, v0, v1}, Lmiuix/animation/e/a;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    iget-object p2, p1, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    invoke-virtual {p2}, Lmiuix/animation/e;->d()Lmiuix/animation/e/a;

    move-result-object p2

    iget-object v0, p1, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    invoke-virtual {p2, v0}, Lmiuix/animation/e/a;->a(Ljava/lang/Object;)V

    iget-object p2, p1, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    iget-object p2, p2, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    iget-object p1, p1, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    invoke-virtual {p2, p1}, Lmiuix/animation/d/d;->a(Ljava/lang/Object;)V

    return-void
.end method

.method private static a(Lmiuix/animation/d/y;Z)V
    .locals 4

    invoke-virtual {p0}, Lmiuix/animation/d/y;->b()I

    move-result v0

    const/16 v1, 0xfa0

    if-le v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lmiuix/animation/d/y;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/animation/e/c;

    iget-object v2, v1, Lmiuix/animation/e/c;->a:Lmiuix/animation/g/b;

    sget-object v3, Lmiuix/animation/g/C;->a:Lmiuix/animation/g/C$b;

    if-eq v2, v3, :cond_1

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_2

    iget-object v2, p0, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    invoke-static {v2, v1}, Lmiuix/animation/styles/a;->b(Lmiuix/animation/e;Lmiuix/animation/e/c;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    invoke-static {v2, v1}, Lmiuix/animation/styles/a;->a(Lmiuix/animation/e;Lmiuix/animation/e/c;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method private static a(Lmiuix/animation/e;Ljava/lang/Object;Ljava/lang/Object;Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiuix/animation/e;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/util/List<",
            "Lmiuix/animation/e/c;",
            ">;Z)V"
        }
    .end annotation

    if-eqz p4, :cond_0

    instance-of p4, p0, Lmiuix/animation/ViewTarget;

    if-eqz p4, :cond_1

    :cond_0
    invoke-static {p0, p3}, Lmiuix/animation/d/s;->a(Lmiuix/animation/e;Ljava/util/List;)V

    :cond_1
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p4

    const v0, 0x9c40

    if-le p4, v0, :cond_2

    invoke-virtual {p0}, Lmiuix/animation/e;->d()Lmiuix/animation/e/a;

    move-result-object p0

    invoke-virtual {p0, p1, p2}, Lmiuix/animation/e/a;->d(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lmiuix/animation/e;->d()Lmiuix/animation/e/a;

    move-result-object p4

    invoke-virtual {p4, p1, p2, p3}, Lmiuix/animation/e/a;->b(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V

    invoke-virtual {p0}, Lmiuix/animation/e;->d()Lmiuix/animation/e/a;

    move-result-object p0

    invoke-virtual {p0, p1, p2, p3}, Lmiuix/animation/e/a;->c(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V

    :goto_0
    return-void
.end method

.method private static a(Lmiuix/animation/e;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiuix/animation/e;",
            "Ljava/util/List<",
            "Lmiuix/animation/e/c;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/animation/e/c;

    iget-object v1, v0, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iget-wide v1, v1, Lmiuix/animation/d/c;->i:D

    invoke-static {v1, v2}, Lmiuix/animation/d/l;->a(D)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p0}, Lmiuix/animation/e/c;->a(Lmiuix/animation/e;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(ZLmiuix/animation/d/y;)V
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p2, Lmiuix/animation/d/y;->l:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lmiuix/animation/d/s;->a(Ljava/util/List;)V

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p2, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    iget-object v2, p2, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    iget-object p2, p2, Lmiuix/animation/d/y;->f:Ljava/lang/Object;

    invoke-static {v1, v2, p2, v0, p1}, Lmiuix/animation/d/s;->a(Lmiuix/animation/e;Ljava/lang/Object;Ljava/lang/Object;Ljava/util/List;Z)V

    :cond_0
    return-void
.end method

.method private b(Lmiuix/animation/d/y;)V
    .locals 4

    invoke-static {}, Lmiuix/animation/h/g;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ">>> onStart, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lmiuix/animation/d/s;->a:Lmiuix/animation/e;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", info.key = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lmiuix/animation/h/g;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p1, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    invoke-virtual {v0}, Lmiuix/animation/e;->d()Lmiuix/animation/e/a;

    move-result-object v0

    iget-object v1, p1, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    iget-object v2, p1, Lmiuix/animation/d/y;->h:Lmiuix/animation/a/a;

    invoke-virtual {v0, v1, v2}, Lmiuix/animation/e/a;->a(Ljava/lang/Object;Lmiuix/animation/a/a;)Z

    iget-object v0, p1, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    invoke-virtual {v0}, Lmiuix/animation/e;->d()Lmiuix/animation/e/a;

    move-result-object v0

    iget-object v1, p1, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    iget-object v2, p1, Lmiuix/animation/d/y;->f:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lmiuix/animation/e/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p1, Lmiuix/animation/d/y;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/16 v2, 0xfa0

    if-gt v1, v2, :cond_1

    iget-object v1, p1, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    invoke-virtual {v1}, Lmiuix/animation/e;->d()Lmiuix/animation/e/a;

    move-result-object v1

    iget-object v2, p1, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    iget-object v3, p1, Lmiuix/animation/d/y;->f:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3, v0}, Lmiuix/animation/e/a;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V

    :cond_1
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lmiuix/animation/d/s;->a(Lmiuix/animation/d/y;Z)V

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {p0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    new-instance v0, Lmiuix/animation/d/r;

    invoke-direct {v0, p0, p1}, Lmiuix/animation/d/r;-><init>(Lmiuix/animation/d/s;Z)V

    invoke-virtual {p0, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lmiuix/animation/d/s;->b(Z)V

    :goto_0
    return-void
.end method

.method public a()Z
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {p0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public b(Z)V
    .locals 2

    iget-object v0, p0, Lmiuix/animation/d/s;->a:Lmiuix/animation/e;

    iget-object v0, v0, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    iget-object v1, p0, Lmiuix/animation/d/s;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, Lmiuix/animation/d/d;->a(Ljava/util/List;)V

    iget-object v0, p0, Lmiuix/animation/d/s;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/animation/d/y;

    invoke-direct {p0, p1, v1}, Lmiuix/animation/d/s;->a(ZLmiuix/animation/d/y;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lmiuix/animation/d/s;->b:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget v0, p1, Landroid/os/Message;->what:I

    if-eqz v0, :cond_5

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_4

    goto/16 :goto_0

    :cond_0
    sget-object v0, Lmiuix/animation/d/y;->b:Ljava/util/Map;

    iget p1, p1, Landroid/os/Message;->arg1:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lmiuix/animation/d/y;

    if-eqz p1, :cond_6

    iget-object v0, p0, Lmiuix/animation/d/s;->a:Lmiuix/animation/e;

    invoke-virtual {v0}, Lmiuix/animation/e;->d()Lmiuix/animation/e/a;

    move-result-object v0

    iget-object v1, p1, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lmiuix/animation/e/a;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lmiuix/animation/d/s;->a:Lmiuix/animation/e;

    invoke-virtual {v0}, Lmiuix/animation/e;->d()Lmiuix/animation/e/a;

    move-result-object v0

    iget-object v1, p1, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    iget-object p1, p1, Lmiuix/animation/d/y;->h:Lmiuix/animation/a/a;

    invoke-virtual {v0, v1, p1}, Lmiuix/animation/e/a;->a(Ljava/lang/Object;Lmiuix/animation/a/a;)Z

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lmiuix/animation/d/s;->a:Lmiuix/animation/e;

    iget-object p1, p1, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    iget-object p1, p1, Lmiuix/animation/d/d;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    goto :goto_0

    :cond_2
    sget-object v0, Lmiuix/animation/d/y;->b:Ljava/util/Map;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/animation/d/y;

    if-nez v0, :cond_3

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lmiuix/animation/d/y;

    :cond_3
    if-eqz v0, :cond_4

    iget v1, p1, Landroid/os/Message;->arg2:I

    invoke-direct {p0, v0, v1}, Lmiuix/animation/d/s;->a(Lmiuix/animation/d/y;I)V

    :cond_4
    sget-object v0, Lmiuix/animation/d/y;->b:Ljava/util/Map;

    iget p1, p1, Landroid/os/Message;->arg1:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lmiuix/animation/d/y;

    if-eqz p1, :cond_6

    invoke-direct {p0, p1}, Lmiuix/animation/d/s;->a(Lmiuix/animation/d/y;)V

    goto :goto_0

    :cond_5
    sget-object v0, Lmiuix/animation/d/y;->b:Ljava/util/Map;

    iget p1, p1, Landroid/os/Message;->arg1:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lmiuix/animation/d/y;

    if-eqz p1, :cond_6

    invoke-direct {p0, p1}, Lmiuix/animation/d/s;->b(Lmiuix/animation/d/y;)V

    :cond_6
    :goto_0
    return-void
.end method
