.class public Lmiuix/animation/d/u;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/animation/d/u$b;,
        Lmiuix/animation/d/u$a;
    }
.end annotation


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lmiuix/animation/g/b;",
            "Lmiuix/animation/d/u$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lmiuix/animation/d/u;->a:Ljava/util/Map;

    return-void
.end method

.method private a(Lmiuix/animation/g/b;)Lmiuix/animation/d/u$a;
    .locals 2

    iget-object v0, p0, Lmiuix/animation/d/u;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/animation/d/u$a;

    if-nez v0, :cond_0

    new-instance v0, Lmiuix/animation/d/u$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmiuix/animation/d/u$a;-><init>(Lmiuix/animation/d/t;)V

    iget-object v1, p0, Lmiuix/animation/d/u;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method


# virtual methods
.method public a(Lmiuix/animation/e;Lmiuix/animation/g/b;D)V
    .locals 4

    invoke-direct {p0, p2}, Lmiuix/animation/d/u;->a(Lmiuix/animation/g/b;)Lmiuix/animation/d/u$a;

    move-result-object v0

    iget-object v1, v0, Lmiuix/animation/d/u$a;->a:Lmiuix/animation/h/n;

    const/4 v2, 0x1

    new-array v2, v2, [D

    const/4 v3, 0x0

    aput-wide p3, v2, v3

    invoke-virtual {v1, v2}, Lmiuix/animation/h/n;->a([D)V

    iget-object p3, v0, Lmiuix/animation/d/u$a;->a:Lmiuix/animation/h/n;

    invoke-virtual {p3, v3}, Lmiuix/animation/h/n;->a(I)F

    move-result p3

    const/4 p4, 0x0

    cmpl-float p4, p3, p4

    if-eqz p4, :cond_0

    iget-object p4, v0, Lmiuix/animation/d/u$a;->b:Lmiuix/animation/d/u$b;

    invoke-virtual {p4, p1, p2}, Lmiuix/animation/d/u$b;->a(Lmiuix/animation/e;Lmiuix/animation/g/b;)V

    float-to-double p3, p3

    invoke-virtual {p1, p2, p3, p4}, Lmiuix/animation/e;->a(Lmiuix/animation/g/b;D)V

    :cond_0
    return-void
.end method
