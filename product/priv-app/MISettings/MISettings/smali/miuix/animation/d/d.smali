.class public Lmiuix/animation/d/d;
.super Ljava/lang/Object;

# interfaces
.implements Lmiuix/animation/d/y$a;


# instance fields
.field a:Lmiuix/animation/e;

.field final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final d:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Lmiuix/animation/g/b;",
            "Lmiuix/animation/e/c;",
            ">;"
        }
    .end annotation
.end field

.field final e:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Object;",
            "Lmiuix/animation/d/y;",
            ">;"
        }
    .end annotation
.end field

.field final f:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue<",
            "Lmiuix/animation/d/y;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiuix/animation/e/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmiuix/animation/d/d;->b:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmiuix/animation/d/d;->c:Ljava/util/Set;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lmiuix/animation/d/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lmiuix/animation/d/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lmiuix/animation/d/d;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-void
.end method

.method private varargs a(Lmiuix/animation/d/y;[Lmiuix/animation/g/b;)Z
    .locals 4

    array-length v0, p2

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_1

    aget-object v3, p2, v2

    invoke-virtual {p1, v3}, Lmiuix/animation/d/y;->a(Lmiuix/animation/g/b;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method private b(Lmiuix/animation/b/a;Lmiuix/animation/a/b;)V
    .locals 7

    invoke-virtual {p1}, Lmiuix/animation/b/a;->d()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Lmiuix/animation/b/a;->d(Ljava/lang/Object;)Lmiuix/animation/g/b;

    move-result-object v1

    iget-object v2, p0, Lmiuix/animation/d/d;->a:Lmiuix/animation/e;

    invoke-virtual {p1, v2, v1}, Lmiuix/animation/b/a;->a(Lmiuix/animation/e;Lmiuix/animation/g/b;)D

    move-result-wide v2

    iget-object v4, p0, Lmiuix/animation/d/d;->a:Lmiuix/animation/e;

    iget-object v4, v4, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    iget-object v4, v4, Lmiuix/animation/d/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiuix/animation/e/c;

    if-eqz v4, :cond_0

    iget-object v4, v4, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iput-wide v2, v4, Lmiuix/animation/d/c;->j:D

    :cond_0
    instance-of v4, v1, Lmiuix/animation/g/c;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lmiuix/animation/d/d;->a:Lmiuix/animation/e;

    move-object v5, v1

    check-cast v5, Lmiuix/animation/g/c;

    double-to-int v6, v2

    invoke-virtual {v4, v5, v6}, Lmiuix/animation/e;->a(Lmiuix/animation/g/c;I)V

    goto :goto_1

    :cond_1
    iget-object v4, p0, Lmiuix/animation/d/d;->a:Lmiuix/animation/e;

    double-to-float v5, v2

    invoke-virtual {v4, v1, v5}, Lmiuix/animation/e;->a(Lmiuix/animation/g/b;F)V

    :goto_1
    iget-object v4, p0, Lmiuix/animation/d/d;->a:Lmiuix/animation/e;

    invoke-virtual {v4, v1, v2, v3}, Lmiuix/animation/e;->b(Lmiuix/animation/g/b;D)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lmiuix/animation/d/d;->a:Lmiuix/animation/e;

    invoke-virtual {v0, p1, p2}, Lmiuix/animation/e;->a(Lmiuix/animation/b/a;Lmiuix/animation/a/b;)V

    return-void
.end method

.method private c(Lmiuix/animation/d/y;)Z
    .locals 4

    iget-object v0, p1, Lmiuix/animation/d/y;->j:Lmiuix/animation/b/a;

    iget-wide v0, v0, Lmiuix/animation/b/a;->e:J

    const-wide/16 v2, 0x1

    invoke-static {v0, v1, v2, v3}, Lmiuix/animation/h/a;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/animation/d/d;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method private d()V
    .locals 3

    iget-object v0, p0, Lmiuix/animation/d/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/animation/d/y;

    sget-object v2, Lmiuix/animation/d/y;->b:Ljava/util/Map;

    iget v1, v1, Lmiuix/animation/d/y;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/animation/d/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    return-void
.end method

.method private d(Lmiuix/animation/d/y;)V
    .locals 6

    iget-object v0, p0, Lmiuix/animation/d/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/animation/d/y;

    if-ne v1, p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v2, v1, Lmiuix/animation/d/y;->l:Ljava/util/List;

    iget-object v3, p0, Lmiuix/animation/d/d;->g:Ljava/util/List;

    if-nez v3, :cond_1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lmiuix/animation/d/d;->g:Ljava/util/List;

    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiuix/animation/e/c;

    iget-object v4, p1, Lmiuix/animation/d/y;->j:Lmiuix/animation/b/a;

    iget-object v5, v3, Lmiuix/animation/e/c;->a:Lmiuix/animation/g/b;

    invoke-virtual {v4, v5}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lmiuix/animation/d/d;->g:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lmiuix/animation/d/d;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    const/4 v3, 0x4

    invoke-virtual {p0, v1, v2, v3}, Lmiuix/animation/d/d;->a(Lmiuix/animation/d/y;II)V

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lmiuix/animation/d/d;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget-object v3, v1, Lmiuix/animation/d/y;->l:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-eq v2, v3, :cond_5

    iget-object v2, p0, Lmiuix/animation/d/d;->g:Ljava/util/List;

    iput-object v2, v1, Lmiuix/animation/d/y;->l:Ljava/util/List;

    const/4 v2, 0x0

    iput-object v2, p0, Lmiuix/animation/d/d;->g:Ljava/util/List;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lmiuix/animation/d/y;->a(Z)V

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lmiuix/animation/d/d;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    goto :goto_0

    :cond_6
    return-void
.end method


# virtual methods
.method public a(Lmiuix/animation/g/b;)Lmiuix/animation/e/c;
    .locals 2

    iget-object v0, p0, Lmiuix/animation/d/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/animation/e/c;

    if-nez v0, :cond_0

    new-instance v0, Lmiuix/animation/e/c;

    invoke-direct {v0, p1}, Lmiuix/animation/e/c;-><init>(Lmiuix/animation/g/b;)V

    iget-object v1, p0, Lmiuix/animation/d/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lmiuix/animation/e/c;

    if-eqz p1, :cond_0

    move-object v0, p1

    :cond_0
    return-object v0
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lmiuix/animation/d/d;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lmiuix/animation/d/d;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lmiuix/animation/d/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    invoke-direct {p0}, Lmiuix/animation/d/d;->d()V

    iget-object v0, p0, Lmiuix/animation/d/d;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    return-void
.end method

.method a(Ljava/lang/Object;)V
    .locals 1

    goto/32 :goto_6

    nop

    :goto_0
    new-array p1, p1, [Lmiuix/animation/g/b;

    goto/32 :goto_3

    nop

    :goto_1
    return-void

    :goto_2
    iget-object p1, p0, Lmiuix/animation/d/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    goto/32 :goto_8

    nop

    :goto_3
    invoke-virtual {p0, p1}, Lmiuix/animation/d/d;->a([Lmiuix/animation/g/b;)Z

    move-result p1

    goto/32 :goto_7

    nop

    :goto_4
    const/4 p1, 0x0

    goto/32 :goto_0

    nop

    :goto_5
    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_4

    nop

    :goto_6
    iget-object v0, p0, Lmiuix/animation/d/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    goto/32 :goto_5

    nop

    :goto_7
    if-eqz p1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_2

    nop

    :goto_8
    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    :goto_9
    goto/32 :goto_1

    nop
.end method

.method a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lmiuix/animation/d/y;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_e

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_d

    nop

    :goto_1
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    goto/32 :goto_a

    nop

    :goto_2
    iget-object v2, v1, Lmiuix/animation/d/y;->l:Ljava/util/List;

    goto/32 :goto_c

    nop

    :goto_3
    if-eqz v2, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_10

    nop

    :goto_4
    goto :goto_b

    :goto_5
    goto/32 :goto_7

    nop

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_7
    return-void

    :goto_8
    check-cast v1, Lmiuix/animation/d/y;

    goto/32 :goto_2

    nop

    :goto_9
    iget-object v2, v1, Lmiuix/animation/d/y;->l:Ljava/util/List;

    goto/32 :goto_f

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_b
    goto/32 :goto_0

    nop

    :goto_c
    if-nez v2, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_9

    nop

    :goto_d
    if-nez v1, :cond_2

    goto/32 :goto_5

    :cond_2
    goto/32 :goto_6

    nop

    :goto_e
    iget-object v0, p0, Lmiuix/animation/d/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    goto/32 :goto_1

    nop

    :goto_f
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    goto/32 :goto_3

    nop

    :goto_10
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_4

    nop
.end method

.method public a(Lmiuix/animation/b/a;Lmiuix/animation/a/b;)V
    .locals 5

    invoke-static {}, Lmiuix/animation/h/g;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setTo, target = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lmiuix/animation/d/d;->a:Lmiuix/animation/e;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "to = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lmiuix/animation/h/g;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p1}, Lmiuix/animation/b/a;->d()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/16 v1, 0x96

    if-le v0, v1, :cond_1

    sget-object p2, Lmiuix/animation/d/h;->b:Lmiuix/animation/d/q;

    iget-object v0, p0, Lmiuix/animation/d/d;->a:Lmiuix/animation/e;

    invoke-virtual {p2, v0, p1}, Lmiuix/animation/d/q;->a(Lmiuix/animation/e;Lmiuix/animation/b/a;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, p2}, Lmiuix/animation/d/d;->b(Lmiuix/animation/b/a;Lmiuix/animation/a/b;)V

    :goto_0
    return-void
.end method

.method a(Lmiuix/animation/d/y;)V
    .locals 4

    goto/32 :goto_3

    nop

    :goto_0
    iget v2, p1, Lmiuix/animation/d/y;->d:I

    goto/32 :goto_5

    nop

    :goto_1
    iget-object v0, v0, Lmiuix/animation/d/d;->b:Ljava/util/Set;

    goto/32 :goto_f

    nop

    :goto_2
    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_1d

    nop

    :goto_3
    iget-object v0, p0, Lmiuix/animation/d/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    goto/32 :goto_d

    nop

    :goto_4
    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    goto/32 :goto_14

    nop

    :goto_5
    const/4 v3, 0x0

    goto/32 :goto_10

    nop

    :goto_6
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_1c

    nop

    :goto_7
    if-nez v0, :cond_0

    goto/32 :goto_13

    :cond_0
    goto/32 :goto_17

    nop

    :goto_8
    iget-object v0, p1, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    goto/32 :goto_18

    nop

    :goto_9
    return-void

    :goto_a
    iget-object v1, v1, Lmiuix/animation/a/a;->j:Ljava/util/HashSet;

    goto/32 :goto_4

    nop

    :goto_b
    invoke-virtual {v0, v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_1b

    nop

    :goto_c
    invoke-direct {p0, p1}, Lmiuix/animation/d/d;->d(Lmiuix/animation/d/y;)V

    goto/32 :goto_15

    nop

    :goto_d
    iget-object v1, p1, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    goto/32 :goto_b

    nop

    :goto_e
    iget v1, p1, Lmiuix/animation/d/y;->d:I

    goto/32 :goto_6

    nop

    :goto_f
    iget-object v1, p1, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    goto/32 :goto_2

    nop

    :goto_10
    invoke-virtual {v0, v1, v2, v3, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    goto/32 :goto_12

    nop

    :goto_11
    const/4 v0, 0x1

    goto/32 :goto_1a

    nop

    :goto_12
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    :goto_13
    goto/32 :goto_9

    nop

    :goto_14
    if-eqz v1, :cond_1

    goto/32 :goto_13

    :cond_1
    goto/32 :goto_7

    nop

    :goto_15
    iget-object v0, p1, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    goto/32 :goto_16

    nop

    :goto_16
    iget-object v0, v0, Lmiuix/animation/e;->c:Lmiuix/animation/d/d;

    goto/32 :goto_1

    nop

    :goto_17
    sget-object v0, Lmiuix/animation/d/y;->b:Ljava/util/Map;

    goto/32 :goto_e

    nop

    :goto_18
    iget-object v0, v0, Lmiuix/animation/e;->b:Lmiuix/animation/d/s;

    goto/32 :goto_19

    nop

    :goto_19
    const/4 v1, 0x4

    goto/32 :goto_0

    nop

    :goto_1a
    invoke-virtual {p1, v0}, Lmiuix/animation/d/y;->a(Z)V

    goto/32 :goto_c

    nop

    :goto_1b
    invoke-virtual {p1, p0}, Lmiuix/animation/d/y;->a(Lmiuix/animation/d/y$a;)V

    goto/32 :goto_11

    nop

    :goto_1c
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_8

    nop

    :goto_1d
    iget-object v1, p1, Lmiuix/animation/d/y;->h:Lmiuix/animation/a/a;

    goto/32 :goto_a

    nop
.end method

.method a(Lmiuix/animation/d/y;II)V
    .locals 2

    goto/32 :goto_b

    nop

    :goto_0
    iget-object p1, p1, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    goto/32 :goto_5

    nop

    :goto_1
    invoke-virtual {v0, p2, v1, p3, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    goto/32 :goto_13

    nop

    :goto_2
    iget v1, p1, Lmiuix/animation/d/y;->d:I

    goto/32 :goto_7

    nop

    :goto_3
    goto :goto_6

    :goto_4
    goto/32 :goto_0

    nop

    :goto_5
    invoke-virtual {p0, p1}, Lmiuix/animation/d/d;->a(Ljava/lang/Object;)V

    :goto_6
    goto/32 :goto_11

    nop

    :goto_7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_a

    nop

    :goto_8
    iget-object v0, p0, Lmiuix/animation/d/d;->a:Lmiuix/animation/e;

    goto/32 :goto_9

    nop

    :goto_9
    iget-object v0, v0, Lmiuix/animation/e;->b:Lmiuix/animation/d/s;

    goto/32 :goto_e

    nop

    :goto_a
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_8

    nop

    :goto_b
    iget-object v0, p0, Lmiuix/animation/d/d;->b:Ljava/util/Set;

    goto/32 :goto_12

    nop

    :goto_c
    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_14

    nop

    :goto_d
    iget-object v1, p1, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    goto/32 :goto_f

    nop

    :goto_e
    iget v1, p1, Lmiuix/animation/d/y;->d:I

    goto/32 :goto_1

    nop

    :goto_f
    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto/32 :goto_15

    nop

    :goto_10
    iget-object v0, p0, Lmiuix/animation/d/d;->c:Ljava/util/Set;

    goto/32 :goto_d

    nop

    :goto_11
    return-void

    :goto_12
    iget-object v1, p1, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    goto/32 :goto_c

    nop

    :goto_13
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    goto/32 :goto_3

    nop

    :goto_14
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_10

    nop

    :goto_15
    sget-object v0, Lmiuix/animation/d/y;->b:Ljava/util/Map;

    goto/32 :goto_2

    nop
.end method

.method public a(Lmiuix/animation/e;)V
    .locals 0

    iput-object p1, p0, Lmiuix/animation/d/d;->a:Lmiuix/animation/e;

    return-void
.end method

.method public a(Lmiuix/animation/g/b;F)V
    .locals 2

    invoke-virtual {p0, p1}, Lmiuix/animation/d/d;->a(Lmiuix/animation/g/b;)Lmiuix/animation/e/c;

    move-result-object p1

    float-to-double v0, p2

    iput-wide v0, p1, Lmiuix/animation/e/c;->c:D

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/animation/d/d;->a:Lmiuix/animation/e;

    iget-object v0, v0, Lmiuix/animation/e;->b:Lmiuix/animation/d/s;

    invoke-virtual {v0, p1}, Lmiuix/animation/d/s;->a(Z)V

    return-void
.end method

.method public varargs a([Lmiuix/animation/g/b;)Z
    .locals 3

    invoke-static {p1}, Lmiuix/animation/h/a;->a([Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/animation/d/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/animation/d/d;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return v1

    :cond_1
    iget-object v0, p0, Lmiuix/animation/d/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/animation/d/y;

    invoke-direct {p0, v2, p1}, Lmiuix/animation/d/d;->a(Lmiuix/animation/d/y;[Lmiuix/animation/g/b;)Z

    move-result v2

    if-eqz v2, :cond_2

    return v1

    :cond_3
    const/4 p1, 0x0

    return p1
.end method

.method public b()I
    .locals 3

    iget-object v0, p0, Lmiuix/animation/d/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/animation/d/y;

    invoke-virtual {v2}, Lmiuix/animation/d/y;->b()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0

    :cond_0
    return v1
.end method

.method public b(Lmiuix/animation/d/y;)V
    .locals 3

    invoke-direct {p0, p1}, Lmiuix/animation/d/d;->c(Lmiuix/animation/d/y;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ".startAnim, pendState"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-array v0, v1, [Ljava/lang/Object;

    invoke-static {p1, v0}, Lmiuix/animation/h/g;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    sget-object v0, Lmiuix/animation/d/y;->b:Ljava/util/Map;

    iget v2, p1, Lmiuix/animation/d/y;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lmiuix/animation/d/h;->b:Lmiuix/animation/d/q;

    const/4 v2, 0x1

    iget p1, p1, Lmiuix/animation/d/y;->d:I

    invoke-virtual {v0, v2, p1, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public c()Z
    .locals 2

    sget-object v0, Lmiuix/animation/d/h;->b:Lmiuix/animation/d/q;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
