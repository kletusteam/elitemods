.class public Lmiuix/animation/d/b;
.super Ljava/lang/Object;


# instance fields
.field public a:Lmiuix/animation/g/b;

.field public b:D

.field public c:I

.field public d:Z

.field public e:B

.field public f:Lmiuix/animation/h/c$a;

.field public g:J

.field public h:J

.field public i:J

.field public j:I

.field public k:D

.field public l:D

.field public m:D

.field public n:D

.field o:Z

.field p:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lmiuix/animation/d/b;->j:I

    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lmiuix/animation/d/b;->l:D

    iput-wide v0, p0, Lmiuix/animation/d/b;->m:D

    iput-wide v0, p0, Lmiuix/animation/d/b;->n:D

    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    iput-object v0, p0, Lmiuix/animation/d/b;->a:Lmiuix/animation/g/b;

    goto/32 :goto_3

    nop

    :goto_2
    return-void

    :goto_3
    iput-object v0, p0, Lmiuix/animation/d/b;->f:Lmiuix/animation/h/c$a;

    goto/32 :goto_2

    nop
.end method

.method public a(B)V
    .locals 1

    iput-byte p1, p0, Lmiuix/animation/d/b;->e:B

    if-eqz p1, :cond_1

    const/4 v0, 0x2

    if-le p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    iput-boolean p1, p0, Lmiuix/animation/d/b;->d:Z

    return-void
.end method

.method a(Lmiuix/animation/e/c;)V
    .locals 3

    goto/32 :goto_f

    nop

    :goto_0
    iget-byte v1, p0, Lmiuix/animation/d/b;->e:B

    goto/32 :goto_1a

    nop

    :goto_1
    iput v0, p1, Lmiuix/animation/e/c;->d:I

    goto/32 :goto_18

    nop

    :goto_2
    iput-wide v1, v0, Lmiuix/animation/d/c;->c:J

    goto/32 :goto_c

    nop

    :goto_3
    iput-wide v1, v0, Lmiuix/animation/d/c;->d:J

    goto/32 :goto_a

    nop

    :goto_4
    iput-wide v1, v0, Lmiuix/animation/d/c;->e:D

    goto/32 :goto_16

    nop

    :goto_5
    iput-boolean v1, p1, Lmiuix/animation/e/c;->e:Z

    goto/32 :goto_13

    nop

    :goto_6
    iget-boolean v1, p0, Lmiuix/animation/d/b;->d:Z

    goto/32 :goto_5

    nop

    :goto_7
    iget-wide v1, p0, Lmiuix/animation/d/b;->h:J

    goto/32 :goto_2

    nop

    :goto_8
    iget-wide v1, p0, Lmiuix/animation/d/b;->g:J

    goto/32 :goto_10

    nop

    :goto_9
    iput-wide v1, v0, Lmiuix/animation/d/c;->i:D

    goto/32 :goto_15

    nop

    :goto_a
    iget-wide v1, p0, Lmiuix/animation/d/b;->k:D

    goto/32 :goto_4

    nop

    :goto_b
    iget v1, p0, Lmiuix/animation/d/b;->j:I

    goto/32 :goto_17

    nop

    :goto_c
    iget-wide v1, p0, Lmiuix/animation/d/b;->i:J

    goto/32 :goto_3

    nop

    :goto_d
    iput-wide v1, v0, Lmiuix/animation/d/c;->h:D

    goto/32 :goto_6

    nop

    :goto_e
    iget-wide v1, p0, Lmiuix/animation/d/b;->m:D

    goto/32 :goto_d

    nop

    :goto_f
    iget v0, p0, Lmiuix/animation/d/b;->c:I

    goto/32 :goto_1

    nop

    :goto_10
    iput-wide v1, v0, Lmiuix/animation/d/c;->b:J

    goto/32 :goto_b

    nop

    :goto_11
    iput-boolean p1, v0, Lmiuix/animation/d/c;->k:Z

    goto/32 :goto_1c

    nop

    :goto_12
    iput-wide v1, p1, Lmiuix/animation/e/c;->c:D

    goto/32 :goto_19

    nop

    :goto_13
    iget-wide v1, p0, Lmiuix/animation/d/b;->n:D

    goto/32 :goto_9

    nop

    :goto_14
    return-void

    :goto_15
    iget-wide v1, p0, Lmiuix/animation/d/b;->b:D

    goto/32 :goto_12

    nop

    :goto_16
    iget-wide v1, p0, Lmiuix/animation/d/b;->l:D

    goto/32 :goto_1b

    nop

    :goto_17
    iput v1, v0, Lmiuix/animation/d/c;->f:I

    goto/32 :goto_7

    nop

    :goto_18
    iget-object v0, p1, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    goto/32 :goto_0

    nop

    :goto_19
    iget-boolean p1, p0, Lmiuix/animation/d/b;->o:Z

    goto/32 :goto_11

    nop

    :goto_1a
    iput-byte v1, v0, Lmiuix/animation/d/c;->a:B

    goto/32 :goto_8

    nop

    :goto_1b
    iput-wide v1, v0, Lmiuix/animation/d/c;->g:D

    goto/32 :goto_e

    nop

    :goto_1c
    invoke-virtual {p0}, Lmiuix/animation/d/b;->a()V

    goto/32 :goto_14

    nop
.end method

.method a(Lmiuix/animation/e/c;Lmiuix/animation/a/a;Lmiuix/animation/a/c;)V
    .locals 2

    goto/32 :goto_c

    nop

    :goto_0
    iput-wide v0, p0, Lmiuix/animation/d/b;->k:D

    goto/32 :goto_15

    nop

    :goto_1
    iget-object v0, p1, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    goto/32 :goto_1b

    nop

    :goto_2
    iput-object v0, p0, Lmiuix/animation/d/b;->a:Lmiuix/animation/g/b;

    goto/32 :goto_12

    nop

    :goto_3
    iget-object v0, p1, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    goto/32 :goto_d

    nop

    :goto_4
    iget-object v0, p1, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    goto/32 :goto_20

    nop

    :goto_5
    invoke-static {p2, p3}, Lmiuix/animation/d/a;->a(Lmiuix/animation/a/a;Lmiuix/animation/a/c;)J

    move-result-wide p1

    goto/32 :goto_1e

    nop

    :goto_6
    invoke-static {p2, p3}, Lmiuix/animation/d/a;->b(Lmiuix/animation/a/a;Lmiuix/animation/a/c;)Lmiuix/animation/h/c$a;

    move-result-object p1

    goto/32 :goto_1f

    nop

    :goto_7
    iget v0, p1, Lmiuix/animation/e/c;->d:I

    goto/32 :goto_14

    nop

    :goto_8
    iput p1, p0, Lmiuix/animation/d/b;->j:I

    goto/32 :goto_6

    nop

    :goto_9
    iget-wide v0, v0, Lmiuix/animation/d/c;->h:D

    goto/32 :goto_e

    nop

    :goto_a
    iget-boolean p1, p1, Lmiuix/animation/d/c;->k:Z

    goto/32 :goto_10

    nop

    :goto_b
    iget-object p1, p1, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    goto/32 :goto_a

    nop

    :goto_c
    iget-object v0, p1, Lmiuix/animation/e/c;->a:Lmiuix/animation/g/b;

    goto/32 :goto_2

    nop

    :goto_d
    iget-wide v0, v0, Lmiuix/animation/d/c;->e:D

    goto/32 :goto_0

    nop

    :goto_e
    iput-wide v0, p0, Lmiuix/animation/d/b;->m:D

    goto/32 :goto_21

    nop

    :goto_f
    iget-wide v0, v0, Lmiuix/animation/d/c;->g:D

    goto/32 :goto_13

    nop

    :goto_10
    iput-boolean p1, p0, Lmiuix/animation/d/b;->o:Z

    goto/32 :goto_17

    nop

    :goto_11
    iput-wide v0, p0, Lmiuix/animation/d/b;->n:D

    goto/32 :goto_23

    nop

    :goto_12
    iget-wide v0, p1, Lmiuix/animation/e/c;->c:D

    goto/32 :goto_1a

    nop

    :goto_13
    iput-wide v0, p0, Lmiuix/animation/d/b;->l:D

    goto/32 :goto_16

    nop

    :goto_14
    iput v0, p0, Lmiuix/animation/d/b;->c:I

    goto/32 :goto_22

    nop

    :goto_15
    iget-object v0, p1, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    goto/32 :goto_f

    nop

    :goto_16
    iget-object v0, p1, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    goto/32 :goto_9

    nop

    :goto_17
    invoke-static {p2, p3}, Lmiuix/animation/d/a;->d(Lmiuix/animation/a/a;Lmiuix/animation/a/c;)I

    move-result p1

    goto/32 :goto_8

    nop

    :goto_18
    iput-boolean v0, p0, Lmiuix/animation/d/b;->d:Z

    goto/32 :goto_b

    nop

    :goto_19
    iput-wide v0, p0, Lmiuix/animation/d/b;->h:J

    goto/32 :goto_4

    nop

    :goto_1a
    iput-wide v0, p0, Lmiuix/animation/d/b;->b:D

    goto/32 :goto_7

    nop

    :goto_1b
    iget-wide v0, v0, Lmiuix/animation/d/c;->c:J

    goto/32 :goto_19

    nop

    :goto_1c
    iget-byte v0, v0, Lmiuix/animation/d/c;->a:B

    goto/32 :goto_26

    nop

    :goto_1d
    iget-wide v0, v0, Lmiuix/animation/d/c;->i:D

    goto/32 :goto_11

    nop

    :goto_1e
    iput-wide p1, p0, Lmiuix/animation/d/b;->g:J

    goto/32 :goto_25

    nop

    :goto_1f
    iput-object p1, p0, Lmiuix/animation/d/b;->f:Lmiuix/animation/h/c$a;

    goto/32 :goto_5

    nop

    :goto_20
    iget-wide v0, v0, Lmiuix/animation/d/c;->d:J

    goto/32 :goto_24

    nop

    :goto_21
    iget-object v0, p1, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    goto/32 :goto_1d

    nop

    :goto_22
    iget-object v0, p1, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    goto/32 :goto_1c

    nop

    :goto_23
    iget-boolean v0, p1, Lmiuix/animation/e/c;->e:Z

    goto/32 :goto_18

    nop

    :goto_24
    iput-wide v0, p0, Lmiuix/animation/d/b;->i:J

    goto/32 :goto_3

    nop

    :goto_25
    return-void

    :goto_26
    iput-byte v0, p0, Lmiuix/animation/d/b;->e:B

    goto/32 :goto_1

    nop
.end method

.method b()V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    iput-boolean v0, p0, Lmiuix/animation/d/b;->o:Z

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    iput v0, p0, Lmiuix/animation/d/b;->c:I

    goto/32 :goto_0

    nop

    :goto_3
    const/4 v0, 0x0

    goto/32 :goto_4

    nop

    :goto_4
    iput-boolean v0, p0, Lmiuix/animation/d/b;->d:Z

    goto/32 :goto_2

    nop
.end method
