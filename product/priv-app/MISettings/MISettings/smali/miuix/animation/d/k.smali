.class public Lmiuix/animation/d/k;
.super Lmiuix/animation/h/e;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiuix/animation/h/e<",
        "Lmiuix/animation/d/k;",
        ">;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# static fields
.field public static final b:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public final c:Lmiuix/animation/d/j;

.field public volatile d:I

.field public volatile e:Lmiuix/animation/d/y;

.field public volatile f:J

.field public volatile g:J

.field public volatile h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lmiuix/animation/d/k;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiuix/animation/h/e;-><init>()V

    new-instance v0, Lmiuix/animation/d/j;

    invoke-direct {v0}, Lmiuix/animation/d/j;-><init>()V

    iput-object v0, p0, Lmiuix/animation/d/k;->c:Lmiuix/animation/d/j;

    return-void
.end method

.method public static a(B)Z
    .locals 2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v1, 0x2

    if-ne p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0
.end method


# virtual methods
.method public a(II)V
    .locals 1

    iget-object v0, p0, Lmiuix/animation/d/k;->c:Lmiuix/animation/d/j;

    invoke-virtual {v0}, Lmiuix/animation/d/j;->clear()V

    iget-object v0, p0, Lmiuix/animation/d/k;->c:Lmiuix/animation/d/j;

    iput p2, v0, Lmiuix/animation/d/j;->g:I

    iput p1, p0, Lmiuix/animation/d/k;->d:I

    return-void
.end method

.method public a(JJZ)V
    .locals 0

    iput-wide p1, p0, Lmiuix/animation/d/k;->f:J

    iput-wide p3, p0, Lmiuix/animation/d/k;->g:J

    iput-boolean p5, p0, Lmiuix/animation/d/k;->h:Z

    invoke-static {p0}, Lmiuix/animation/d/x;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lmiuix/animation/d/k;->c:Lmiuix/animation/d/j;

    iget v0, v0, Lmiuix/animation/d/j;->g:I

    return v0
.end method

.method public c()I
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    move-object v0, p0

    :goto_0
    if-eqz v0, :cond_0

    iget-object v2, v0, Lmiuix/animation/d/k;->c:Lmiuix/animation/d/j;

    iget v2, v2, Lmiuix/animation/d/j;->g:I

    add-int/2addr v1, v2

    iget-object v0, v0, Lmiuix/animation/h/e;->a:Lmiuix/animation/h/e;

    check-cast v0, Lmiuix/animation/d/k;

    goto :goto_0

    :cond_0
    return v1
.end method

.method d()V
    .locals 6

    goto/32 :goto_20

    nop

    :goto_0
    goto/16 :goto_34

    :goto_1
    goto/32 :goto_28

    nop

    :goto_2
    iget-object v2, p0, Lmiuix/animation/d/k;->c:Lmiuix/animation/d/j;

    goto/32 :goto_30

    nop

    :goto_3
    goto/16 :goto_3c

    :goto_4
    goto/32 :goto_3a

    nop

    :goto_5
    iput v3, v2, Lmiuix/animation/d/j;->f:I

    goto/32 :goto_3b

    nop

    :goto_6
    if-lt v0, v1, :cond_0

    goto/32 :goto_f

    :cond_0
    goto/32 :goto_11

    nop

    :goto_7
    iget v2, v2, Lmiuix/animation/d/j;->g:I

    goto/32 :goto_8

    nop

    :goto_8
    add-int/2addr v1, v2

    :goto_9
    goto/32 :goto_6

    nop

    :goto_a
    goto/16 :goto_34

    :goto_b
    goto/32 :goto_32

    nop

    :goto_c
    goto/16 :goto_34

    :goto_d
    goto/32 :goto_19

    nop

    :goto_e
    goto :goto_9

    :goto_f
    goto/32 :goto_1c

    nop

    :goto_10
    if-ne v2, v3, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_3e

    nop

    :goto_11
    iget-object v2, p0, Lmiuix/animation/d/k;->e:Lmiuix/animation/d/y;

    goto/32 :goto_3f

    nop

    :goto_12
    iget-object v3, v2, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    goto/32 :goto_2f

    nop

    :goto_13
    goto/16 :goto_34

    :goto_14
    goto/32 :goto_35

    nop

    :goto_15
    iput v3, v2, Lmiuix/animation/d/j;->e:I

    goto/32 :goto_c

    nop

    :goto_16
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_1e

    nop

    :goto_17
    add-int/2addr v3, v4

    goto/32 :goto_15

    nop

    :goto_18
    if-eqz v2, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_a

    nop

    :goto_19
    iget-object v2, p0, Lmiuix/animation/d/k;->c:Lmiuix/animation/d/j;

    goto/32 :goto_27

    nop

    :goto_1a
    const/4 v3, 0x3

    goto/32 :goto_24

    nop

    :goto_1b
    add-int/2addr v3, v4

    goto/32 :goto_21

    nop

    :goto_1c
    return-void

    :goto_1d
    iget v5, v3, Lmiuix/animation/d/j;->b:I

    goto/32 :goto_22

    nop

    :goto_1e
    check-cast v2, Lmiuix/animation/e/c;

    goto/32 :goto_18

    nop

    :goto_1f
    iget v3, v2, Lmiuix/animation/d/j;->c:I

    goto/32 :goto_1b

    nop

    :goto_20
    iget v0, p0, Lmiuix/animation/d/k;->d:I

    goto/32 :goto_29

    nop

    :goto_21
    iput v3, v2, Lmiuix/animation/d/j;->c:I

    goto/32 :goto_0

    nop

    :goto_22
    add-int/2addr v5, v4

    goto/32 :goto_2c

    nop

    :goto_23
    iget-object v2, p0, Lmiuix/animation/d/k;->c:Lmiuix/animation/d/j;

    goto/32 :goto_7

    nop

    :goto_24
    if-ne v2, v3, :cond_3

    goto/32 :goto_d

    :cond_3
    goto/32 :goto_36

    nop

    :goto_25
    add-int/2addr v3, v4

    goto/32 :goto_33

    nop

    :goto_26
    iget-byte v3, v3, Lmiuix/animation/d/c;->a:B

    goto/32 :goto_2d

    nop

    :goto_27
    iget v3, v2, Lmiuix/animation/d/j;->f:I

    goto/32 :goto_37

    nop

    :goto_28
    iget-object v2, p0, Lmiuix/animation/d/k;->c:Lmiuix/animation/d/j;

    goto/32 :goto_31

    nop

    :goto_29
    iget v1, p0, Lmiuix/animation/d/k;->d:I

    goto/32 :goto_23

    nop

    :goto_2a
    if-ne v2, v3, :cond_4

    goto/32 :goto_14

    :cond_4
    goto/32 :goto_13

    nop

    :goto_2b
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_e

    nop

    :goto_2c
    iput v5, v3, Lmiuix/animation/d/j;->b:I

    goto/32 :goto_2e

    nop

    :goto_2d
    const/4 v4, 0x1

    goto/32 :goto_38

    nop

    :goto_2e
    iget-object v2, v2, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    goto/32 :goto_39

    nop

    :goto_2f
    iget-byte v3, v3, Lmiuix/animation/d/c;->a:B

    goto/32 :goto_3d

    nop

    :goto_30
    iget v3, v2, Lmiuix/animation/d/j;->a:I

    goto/32 :goto_25

    nop

    :goto_31
    iget v3, v2, Lmiuix/animation/d/j;->e:I

    goto/32 :goto_17

    nop

    :goto_32
    iget-object v3, v2, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    goto/32 :goto_26

    nop

    :goto_33
    iput v3, v2, Lmiuix/animation/d/j;->a:I

    :goto_34
    goto/32 :goto_2b

    nop

    :goto_35
    iget-object v2, p0, Lmiuix/animation/d/k;->c:Lmiuix/animation/d/j;

    goto/32 :goto_1f

    nop

    :goto_36
    const/4 v3, 0x4

    goto/32 :goto_10

    nop

    :goto_37
    add-int/2addr v3, v4

    goto/32 :goto_5

    nop

    :goto_38
    if-nez v3, :cond_5

    goto/32 :goto_3c

    :cond_5
    goto/32 :goto_12

    nop

    :goto_39
    iget-byte v2, v2, Lmiuix/animation/d/c;->a:B

    goto/32 :goto_1a

    nop

    :goto_3a
    iget-object v3, p0, Lmiuix/animation/d/k;->c:Lmiuix/animation/d/j;

    goto/32 :goto_1d

    nop

    :goto_3b
    goto :goto_34

    :goto_3c
    goto/32 :goto_2

    nop

    :goto_3d
    if-eq v3, v4, :cond_6

    goto/32 :goto_4

    :cond_6
    goto/32 :goto_3

    nop

    :goto_3e
    const/4 v3, 0x5

    goto/32 :goto_2a

    nop

    :goto_3f
    iget-object v2, v2, Lmiuix/animation/d/y;->l:Ljava/util/List;

    goto/32 :goto_16

    nop
.end method

.method public run()V
    .locals 7

    :try_start_0
    iget-wide v1, p0, Lmiuix/animation/d/k;->f:J

    iget-wide v3, p0, Lmiuix/animation/d/k;->g:J

    const/4 v5, 0x1

    iget-boolean v6, p0, Lmiuix/animation/d/k;->h:Z

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lmiuix/animation/d/i;->a(Lmiuix/animation/d/k;JJZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "miuix_anim"

    const-string v2, "doAnimationFrame failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    sget-object v0, Lmiuix/animation/d/k;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lmiuix/animation/d/h;->b:Lmiuix/animation/d/q;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method
