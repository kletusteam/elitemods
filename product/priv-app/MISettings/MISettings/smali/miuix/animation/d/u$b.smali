.class Lmiuix/animation/d/u$b;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/animation/d/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lmiuix/animation/e;",
            ">;"
        }
    .end annotation
.end field

.field b:Lmiuix/animation/g/b;

.field c:Lmiuix/animation/d/u$a;


# direct methods
.method constructor <init>(Lmiuix/animation/d/u$a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiuix/animation/d/u$b;->c:Lmiuix/animation/d/u$a;

    return-void
.end method


# virtual methods
.method a(Lmiuix/animation/e;Lmiuix/animation/g/b;)V
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    iput-object v0, p0, Lmiuix/animation/d/u$b;->a:Ljava/lang/ref/WeakReference;

    :goto_1
    goto/32 :goto_2

    nop

    :goto_2
    iput-object p2, p0, Lmiuix/animation/d/u$b;->b:Lmiuix/animation/g/b;

    goto/32 :goto_e

    nop

    :goto_3
    iget-object v0, p0, Lmiuix/animation/d/u$b;->a:Ljava/lang/ref/WeakReference;

    goto/32 :goto_a

    nop

    :goto_4
    const-wide/16 v0, 0x258

    goto/32 :goto_f

    nop

    :goto_5
    iget-object v0, p1, Lmiuix/animation/e;->b:Lmiuix/animation/d/s;

    goto/32 :goto_9

    nop

    :goto_6
    if-ne v0, p1, :cond_0

    goto/32 :goto_1

    :cond_0
    :goto_7
    goto/32 :goto_b

    nop

    :goto_8
    return-void

    :goto_9
    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto/32 :goto_3

    nop

    :goto_a
    if-nez v0, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_c

    nop

    :goto_b
    new-instance v0, Ljava/lang/ref/WeakReference;

    goto/32 :goto_d

    nop

    :goto_c
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_d
    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    goto/32 :goto_0

    nop

    :goto_e
    iget-object p1, p1, Lmiuix/animation/e;->b:Lmiuix/animation/d/s;

    goto/32 :goto_4

    nop

    :goto_f
    invoke-virtual {p1, p0, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/32 :goto_8

    nop
.end method

.method public run()V
    .locals 4

    iget-object v0, p0, Lmiuix/animation/d/u$b;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/animation/e;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Lmiuix/animation/g/b;

    const/4 v2, 0x0

    iget-object v3, p0, Lmiuix/animation/d/u$b;->b:Lmiuix/animation/g/b;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lmiuix/animation/e;->a([Lmiuix/animation/g/b;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lmiuix/animation/d/u$b;->b:Lmiuix/animation/g/b;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lmiuix/animation/e;->a(Lmiuix/animation/g/b;D)V

    :cond_0
    iget-object v0, p0, Lmiuix/animation/d/u$b;->c:Lmiuix/animation/d/u$a;

    iget-object v0, v0, Lmiuix/animation/d/u$a;->a:Lmiuix/animation/h/n;

    invoke-virtual {v0}, Lmiuix/animation/h/n;->a()V

    :cond_1
    return-void
.end method
