.class public Lmiuix/animation/d/o;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lmiuix/animation/d/y$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmiuix/animation/d/n;

    invoke-direct {v0}, Lmiuix/animation/d/n;-><init>()V

    sput-object v0, Lmiuix/animation/d/o;->a:Lmiuix/animation/d/y$a;

    return-void
.end method

.method public static a(Lmiuix/animation/e;Lmiuix/animation/b/a;Lmiuix/animation/b/a;Lmiuix/animation/a/b;)J
    .locals 9

    new-instance v0, Lmiuix/animation/d/y;

    invoke-direct {v0, p0, p1, p2, p3}, Lmiuix/animation/d/y;-><init>(Lmiuix/animation/e;Lmiuix/animation/b/a;Lmiuix/animation/b/a;Lmiuix/animation/a/b;)V

    sget-object p0, Lmiuix/animation/d/o;->a:Lmiuix/animation/d/y$a;

    invoke-virtual {v0, p0}, Lmiuix/animation/d/y;->a(Lmiuix/animation/d/y$a;)V

    const/4 p0, 0x1

    invoke-virtual {v0, p0}, Lmiuix/animation/d/y;->a(Z)V

    invoke-static {}, Lmiuix/animation/d/h;->e()Lmiuix/animation/d/h;

    move-result-object p0

    invoke-virtual {p0}, Lmiuix/animation/d/h;->d()J

    move-result-wide p0

    move-wide p2, p0

    :goto_0
    iget-object v1, v0, Lmiuix/animation/d/y;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/animation/d/k;

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-wide v2, p2

    move-wide v4, p0

    invoke-static/range {v1 .. v7}, Lmiuix/animation/d/i;->a(Lmiuix/animation/d/k;JJZZ)V

    goto :goto_1

    :cond_0
    invoke-virtual {v0}, Lmiuix/animation/d/y;->c()Lmiuix/animation/d/j;

    move-result-object v1

    invoke-virtual {v1}, Lmiuix/animation/d/j;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    add-long/2addr p2, p0

    goto :goto_0

    :cond_1
    return-wide p2
.end method
