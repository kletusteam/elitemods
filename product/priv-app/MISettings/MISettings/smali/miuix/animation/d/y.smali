.class Lmiuix/animation/d/y;
.super Lmiuix/animation/h/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/animation/d/y$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiuix/animation/h/e<",
        "Lmiuix/animation/d/y;",
        ">;"
    }
.end annotation


# static fields
.field public static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lmiuix/animation/d/y;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public final d:I

.field public final e:Lmiuix/animation/e;

.field public final f:Ljava/lang/Object;

.field public volatile g:Ljava/lang/Object;

.field public volatile h:Lmiuix/animation/a/a;

.field public volatile i:Lmiuix/animation/b/a;

.field public volatile j:Lmiuix/animation/b/a;

.field public volatile k:J

.field public volatile l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiuix/animation/e/c;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiuix/animation/d/k;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lmiuix/animation/d/j;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lmiuix/animation/d/y;->b:Ljava/util/Map;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lmiuix/animation/d/y;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Lmiuix/animation/e;Lmiuix/animation/b/a;Lmiuix/animation/b/a;Lmiuix/animation/a/b;)V
    .locals 1

    invoke-direct {p0}, Lmiuix/animation/h/e;-><init>()V

    sget-object v0, Lmiuix/animation/d/y;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    iput v0, p0, Lmiuix/animation/d/y;->d:I

    new-instance v0, Lmiuix/animation/a/a;

    invoke-direct {v0}, Lmiuix/animation/a/a;-><init>()V

    iput-object v0, p0, Lmiuix/animation/d/y;->h:Lmiuix/animation/a/a;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/animation/d/y;->m:Ljava/util/List;

    new-instance v0, Lmiuix/animation/d/j;

    invoke-direct {v0}, Lmiuix/animation/d/j;-><init>()V

    iput-object v0, p0, Lmiuix/animation/d/y;->n:Lmiuix/animation/d/j;

    iput-object p1, p0, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    invoke-direct {p0, p2}, Lmiuix/animation/d/y;->a(Lmiuix/animation/b/a;)Lmiuix/animation/b/a;

    move-result-object p2

    iput-object p2, p0, Lmiuix/animation/d/y;->i:Lmiuix/animation/b/a;

    invoke-direct {p0, p3}, Lmiuix/animation/d/y;->a(Lmiuix/animation/b/a;)Lmiuix/animation/b/a;

    move-result-object p2

    iput-object p2, p0, Lmiuix/animation/d/y;->j:Lmiuix/animation/b/a;

    iget-object p2, p0, Lmiuix/animation/d/y;->j:Lmiuix/animation/b/a;

    invoke-virtual {p2}, Lmiuix/animation/b/a;->c()Ljava/lang/Object;

    move-result-object p2

    iput-object p2, p0, Lmiuix/animation/d/y;->f:Ljava/lang/Object;

    iget-boolean p2, p3, Lmiuix/animation/b/a;->d:Z

    if-eqz p2, :cond_0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lmiuix/animation/d/y;->f:Ljava/lang/Object;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    iget v0, p0, Lmiuix/animation/d/y;->d:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lmiuix/animation/d/y;->f:Ljava/lang/Object;

    iput-object p2, p0, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    :goto_0
    const/4 p2, 0x0

    iput-object p2, p0, Lmiuix/animation/d/y;->l:Ljava/util/List;

    invoke-direct {p0}, Lmiuix/animation/d/y;->d()V

    iget-object p2, p0, Lmiuix/animation/d/y;->h:Lmiuix/animation/a/a;

    invoke-virtual {p3}, Lmiuix/animation/b/a;->b()Lmiuix/animation/a/a;

    move-result-object p3

    invoke-virtual {p2, p3}, Lmiuix/animation/a/a;->b(Lmiuix/animation/a/a;)V

    if-eqz p4, :cond_1

    iget-object p2, p0, Lmiuix/animation/d/y;->h:Lmiuix/animation/a/a;

    invoke-virtual {p4, p2}, Lmiuix/animation/a/b;->a(Lmiuix/animation/a/a;)V

    :cond_1
    invoke-virtual {p1}, Lmiuix/animation/e;->d()Lmiuix/animation/e/a;

    move-result-object p1

    iget-object p2, p0, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    iget-object p3, p0, Lmiuix/animation/d/y;->h:Lmiuix/animation/a/a;

    invoke-virtual {p1, p2, p3}, Lmiuix/animation/e/a;->a(Ljava/lang/Object;Lmiuix/animation/a/a;)Z

    return-void
.end method

.method private a(Lmiuix/animation/b/a;)Lmiuix/animation/b/a;
    .locals 1

    if-eqz p1, :cond_0

    iget-boolean v0, p1, Lmiuix/animation/b/a;->d:Z

    if-eqz v0, :cond_0

    new-instance v0, Lmiuix/animation/b/a;

    invoke-direct {v0}, Lmiuix/animation/b/a;-><init>()V

    invoke-virtual {v0, p1}, Lmiuix/animation/b/a;->a(Lmiuix/animation/b/a;)V

    return-object v0

    :cond_0
    return-object p1
.end method

.method static a(Lmiuix/animation/d/k;Lmiuix/animation/d/j;Lmiuix/animation/e/c;B)V
    .locals 3

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    if-ne p3, v0, :cond_0

    iget-object p2, p2, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iget-wide p2, p2, Lmiuix/animation/d/c;->b:J

    const-wide/16 v1, 0x0

    cmp-long p2, p2, v1

    if-lez p2, :cond_0

    iget-object p0, p0, Lmiuix/animation/d/k;->c:Lmiuix/animation/d/j;

    iget p2, p0, Lmiuix/animation/d/j;->a:I

    if-lez p2, :cond_0

    sub-int/2addr p2, v0

    iput p2, p0, Lmiuix/animation/d/j;->a:I

    iget p0, p1, Lmiuix/animation/d/j;->a:I

    sub-int/2addr p0, v0

    iput p0, p1, Lmiuix/animation/d/j;->a:I

    :cond_0
    return-void
.end method

.method private d()V
    .locals 5

    iget-object v0, p0, Lmiuix/animation/d/y;->i:Lmiuix/animation/b/a;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lmiuix/animation/d/y;->j:Lmiuix/animation/b/a;

    invoke-virtual {v0}, Lmiuix/animation/b/a;->d()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lmiuix/animation/d/y;->j:Lmiuix/animation/b/a;

    invoke-virtual {v2, v1}, Lmiuix/animation/b/a;->d(Ljava/lang/Object;)Lmiuix/animation/g/b;

    move-result-object v1

    instance-of v2, v1, Lmiuix/animation/g/a;

    if-nez v2, :cond_2

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    const-wide v3, 0x7fefffffffffffffL    # Double.MAX_VALUE

    invoke-static {v2, v1, v3, v4}, Lmiuix/animation/d/l;->b(Lmiuix/animation/e;Lmiuix/animation/g/b;D)D

    move-result-wide v2

    invoke-static {v2, v3}, Lmiuix/animation/d/l;->a(D)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lmiuix/animation/d/y;->i:Lmiuix/animation/b/a;

    iget-object v3, p0, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    invoke-virtual {v2, v3, v1}, Lmiuix/animation/b/a;->a(Lmiuix/animation/e;Lmiuix/animation/g/b;)D

    move-result-wide v2

    invoke-static {v2, v3}, Lmiuix/animation/d/l;->a(D)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    check-cast v1, Lmiuix/animation/g/a;

    double-to-int v2, v2

    invoke-virtual {v4, v1, v2}, Lmiuix/animation/e;->a(Lmiuix/animation/g/c;I)V

    goto :goto_0

    :cond_4
    return-void
.end method


# virtual methods
.method public a(Lmiuix/animation/d/y$a;)V
    .locals 12

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lmiuix/animation/d/y;->k:J

    iget-object v0, p0, Lmiuix/animation/d/y;->i:Lmiuix/animation/b/a;

    iget-object v1, p0, Lmiuix/animation/d/y;->j:Lmiuix/animation/b/a;

    invoke-static {}, Lmiuix/animation/h/g;->c()Z

    move-result v2

    const/4 v3, 0x0

    const-string v4, "-- doSetup, target = "

    if-eqz v2, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v6, ", key = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v6, ", f = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v6, ", t = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v6, "\nconfig = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lmiuix/animation/d/y;->h:Lmiuix/animation/a/a;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lmiuix/animation/h/g;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1}, Lmiuix/animation/b/a;->d()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v1, v7}, Lmiuix/animation/b/a;->c(Ljava/lang/Object;)Lmiuix/animation/g/b;

    move-result-object v7

    invoke-interface {p1, v7}, Lmiuix/animation/d/y$a;->a(Lmiuix/animation/g/b;)Lmiuix/animation/e/c;

    move-result-object v8

    if-nez v8, :cond_2

    goto :goto_0

    :cond_2
    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v9, v8, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iget-object v10, p0, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    invoke-virtual {v1, v10, v7}, Lmiuix/animation/b/a;->a(Lmiuix/animation/e;Lmiuix/animation/g/b;)D

    move-result-wide v10

    iput-wide v10, v9, Lmiuix/animation/d/c;->h:D

    if-eqz v0, :cond_3

    iget-object v9, v8, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iget-object v10, p0, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    invoke-virtual {v0, v10, v7}, Lmiuix/animation/b/a;->a(Lmiuix/animation/e;Lmiuix/animation/g/b;)D

    move-result-wide v10

    iput-wide v10, v9, Lmiuix/animation/d/c;->g:D

    goto :goto_1

    :cond_3
    iget-object v9, v8, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iget-wide v9, v9, Lmiuix/animation/d/c;->g:D

    iget-object v11, p0, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    invoke-static {v11, v7, v9, v10}, Lmiuix/animation/d/l;->b(Lmiuix/animation/e;Lmiuix/animation/g/b;D)D

    move-result-wide v9

    invoke-static {v9, v10}, Lmiuix/animation/d/l;->a(D)Z

    move-result v11

    if-nez v11, :cond_4

    iget-object v11, v8, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iput-wide v9, v11, Lmiuix/animation/d/c;->g:D

    :cond_4
    :goto_1
    invoke-static {v8}, Lmiuix/animation/d/l;->a(Lmiuix/animation/e/c;)Z

    if-eqz v2, :cond_1

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v10, p0, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v10, ", property = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ", startValue = "

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v8, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iget-wide v10, v7, Lmiuix/animation/d/c;->g:D

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v7, ", targetValue = "

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v8, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iget-wide v10, v7, Lmiuix/animation/d/c;->h:D

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v7, ", value = "

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v8, Lmiuix/animation/e/c;->f:Lmiuix/animation/d/c;

    iget-wide v7, v7, Lmiuix/animation/d/c;->i:D

    invoke-virtual {v9, v7, v8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-array v8, v3, [Ljava/lang/Object;

    invoke-static {v7, v8}, Lmiuix/animation/h/g;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_5
    iput-object v5, p0, Lmiuix/animation/d/y;->l:Ljava/util/List;

    return-void
.end method

.method public a(Z)V
    .locals 6

    iget-object v0, p0, Lmiuix/animation/d/y;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    div-int/lit16 v1, v0, 0xfa0

    const/4 v2, 0x1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    iget-object v3, p0, Lmiuix/animation/d/y;->m:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v1, :cond_0

    iget-object v3, p0, Lmiuix/animation/d/y;->m:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v3, v1, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    goto :goto_1

    :cond_0
    iget-object v3, p0, Lmiuix/animation/d/y;->m:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    :goto_0
    if-ge v3, v1, :cond_1

    iget-object v4, p0, Lmiuix/animation/d/y;->m:Ljava/util/List;

    new-instance v5, Lmiuix/animation/d/k;

    invoke-direct {v5}, Lmiuix/animation/d/k;-><init>()V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    const/4 v1, 0x0

    iget-object v3, p0, Lmiuix/animation/d/y;->m:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiuix/animation/d/k;

    iput-object p0, v4, Lmiuix/animation/d/k;->e:Lmiuix/animation/d/y;

    add-int v5, v1, v2

    if-le v5, v0, :cond_2

    sub-int v5, v0, v1

    goto :goto_3

    :cond_2
    move v5, v2

    :goto_3
    invoke-virtual {v4, v1, v5}, Lmiuix/animation/d/k;->a(II)V

    if-eqz p1, :cond_3

    iget-object v4, v4, Lmiuix/animation/d/k;->c:Lmiuix/animation/d/j;

    iput v5, v4, Lmiuix/animation/d/j;->a:I

    goto :goto_4

    :cond_3
    invoke-virtual {v4}, Lmiuix/animation/d/k;->d()V

    :goto_4
    add-int/2addr v1, v5

    goto :goto_2

    :cond_4
    return-void
.end method

.method public a(Lmiuix/animation/g/b;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/animation/d/y;->j:Lmiuix/animation/b/a;

    invoke-virtual {v0, p1}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lmiuix/animation/d/y;->j:Lmiuix/animation/b/a;

    invoke-virtual {v0}, Lmiuix/animation/b/a;->d()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method

.method public c()Lmiuix/animation/d/j;
    .locals 3

    iget-object v0, p0, Lmiuix/animation/d/y;->n:Lmiuix/animation/d/j;

    invoke-virtual {v0}, Lmiuix/animation/d/j;->clear()V

    iget-object v0, p0, Lmiuix/animation/d/y;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/animation/d/k;

    iget-object v2, p0, Lmiuix/animation/d/y;->n:Lmiuix/animation/d/j;

    iget-object v1, v1, Lmiuix/animation/d/k;->c:Lmiuix/animation/d/j;

    invoke-virtual {v2, v1}, Lmiuix/animation/d/j;->a(Lmiuix/animation/d/j;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/animation/d/y;->n:Lmiuix/animation/d/j;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TransitionInfo{target = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lmiuix/animation/d/y;->e:Lmiuix/animation/e;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lmiuix/animation/e;->e()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", key = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lmiuix/animation/d/y;->g:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", propSize = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lmiuix/animation/d/y;->j:Lmiuix/animation/b/a;

    invoke-virtual {v1}, Lmiuix/animation/b/a;->d()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", next = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lmiuix/animation/h/e;->a:Lmiuix/animation/h/e;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
