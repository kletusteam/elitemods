.class Lmiuix/animation/h/j$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/animation/h/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field final a:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final b:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Object;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field final c:Ljava/lang/Runnable;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lmiuix/animation/h/j$a;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lmiuix/animation/h/j$a;->b:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v0, Lmiuix/animation/h/i;

    invoke-direct {v0, p0}, Lmiuix/animation/h/i;-><init>(Lmiuix/animation/h/j$a;)V

    iput-object v0, p0, Lmiuix/animation/h/j$a;->c:Ljava/lang/Runnable;

    return-void
.end method

.method synthetic constructor <init>(Lmiuix/animation/h/h;)V
    .locals 0

    invoke-direct {p0}, Lmiuix/animation/h/j$a;-><init>()V

    return-void
.end method


# virtual methods
.method varargs a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;[",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    goto/32 :goto_9

    nop

    :goto_0
    invoke-static {p1, p2}, Lmiuix/animation/h/j;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_1
    goto/32 :goto_3

    nop

    :goto_2
    iget-object p1, p0, Lmiuix/animation/h/j$a;->b:Ljava/util/concurrent/ConcurrentHashMap;

    goto/32 :goto_7

    nop

    :goto_3
    return-object v0

    :goto_4
    goto :goto_1

    :goto_5
    goto/32 :goto_a

    nop

    :goto_6
    if-nez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_2

    nop

    :goto_7
    invoke-virtual {p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_4

    nop

    :goto_8
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_9
    iget-object v0, p0, Lmiuix/animation/h/j$a;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    goto/32 :goto_8

    nop

    :goto_a
    if-nez p1, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_0

    nop
.end method

.method a()V
    .locals 2

    :goto_0
    goto/32 :goto_c

    nop

    :goto_1
    goto :goto_5

    :goto_2
    goto/32 :goto_7

    nop

    :goto_3
    iget-object v0, p0, Lmiuix/animation/h/j$a;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    goto/32 :goto_8

    nop

    :goto_4
    goto :goto_0

    :goto_5
    goto/32 :goto_d

    nop

    :goto_6
    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_4

    nop

    :goto_7
    iget-object v1, p0, Lmiuix/animation/h/j$a;->b:Ljava/util/concurrent/ConcurrentHashMap;

    goto/32 :goto_6

    nop

    :goto_8
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_e

    nop

    :goto_9
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v0

    goto/32 :goto_b

    nop

    :goto_a
    if-gt v0, v1, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_3

    nop

    :goto_b
    const/16 v1, 0xa

    goto/32 :goto_a

    nop

    :goto_c
    iget-object v0, p0, Lmiuix/animation/h/j$a;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    goto/32 :goto_9

    nop

    :goto_d
    return-void

    :goto_e
    if-eqz v0, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_1

    nop
.end method

.method a(Ljava/lang/Object;)V
    .locals 3

    goto/32 :goto_10

    nop

    :goto_0
    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_1
    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    goto/32 :goto_3

    nop

    :goto_2
    const/4 v1, 0x1

    goto/32 :goto_12

    nop

    :goto_3
    invoke-static {}, Lmiuix/animation/h/j;->a()Landroid/os/Handler;

    move-result-object p1

    goto/32 :goto_6

    nop

    :goto_4
    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_5
    goto/32 :goto_9

    nop

    :goto_6
    iget-object v0, p0, Lmiuix/animation/h/j$a;->c:Ljava/lang/Runnable;

    goto/32 :goto_15

    nop

    :goto_7
    if-nez v0, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_c

    nop

    :goto_8
    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result p1

    goto/32 :goto_13

    nop

    :goto_9
    return-void

    :goto_a
    const-wide/16 v1, 0x1388

    goto/32 :goto_4

    nop

    :goto_b
    invoke-static {}, Lmiuix/animation/h/j;->a()Landroid/os/Handler;

    move-result-object p1

    goto/32 :goto_e

    nop

    :goto_c
    return-void

    :goto_d
    goto/32 :goto_f

    nop

    :goto_e
    iget-object v0, p0, Lmiuix/animation/h/j$a;->c:Ljava/lang/Runnable;

    goto/32 :goto_a

    nop

    :goto_f
    iget-object v0, p0, Lmiuix/animation/h/j$a;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    goto/32 :goto_1

    nop

    :goto_10
    iget-object v0, p0, Lmiuix/animation/h/j$a;->b:Ljava/util/concurrent/ConcurrentHashMap;

    goto/32 :goto_2

    nop

    :goto_11
    iget-object p1, p0, Lmiuix/animation/h/j$a;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    goto/32 :goto_8

    nop

    :goto_12
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_13
    const/16 v0, 0xa

    goto/32 :goto_14

    nop

    :goto_14
    if-gt p1, v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_b

    nop

    :goto_15
    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto/32 :goto_11

    nop
.end method
