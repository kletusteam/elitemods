.class public Lmiuix/animation/h/c;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/animation/h/c$c;,
        Lmiuix/animation/h/c$b;,
        Lmiuix/animation/h/c$a;
    }
.end annotation


# static fields
.field static final a:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Integer;",
            "Landroid/animation/TimeInterpolator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lmiuix/animation/h/c;->a:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method static varargs a(I[F)Landroid/animation/TimeInterpolator;
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    :pswitch_0
    new-instance p0, Lmiuix/view/a/b;

    invoke-direct {p0}, Lmiuix/view/a/b;-><init>()V

    return-object p0

    :pswitch_1
    new-instance p0, Lmiuix/view/a/c;

    invoke-direct {p0}, Lmiuix/view/a/c;-><init>()V

    return-object p0

    :pswitch_2
    new-instance p0, Lmiuix/view/a/a;

    invoke-direct {p0}, Lmiuix/view/a/a;-><init>()V

    return-object p0

    :pswitch_3
    new-instance p0, Landroid/view/animation/BounceInterpolator;

    invoke-direct {p0}, Landroid/view/animation/BounceInterpolator;-><init>()V

    return-object p0

    :pswitch_4
    new-instance p0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {p0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    return-object p0

    :pswitch_5
    new-instance p0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {p0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    return-object p0

    :pswitch_6
    new-instance p0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {p0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    return-object p0

    :pswitch_7
    new-instance p0, Lmiuix/view/a/h;

    invoke-direct {p0}, Lmiuix/view/a/h;-><init>()V

    return-object p0

    :pswitch_8
    new-instance p0, Lmiuix/view/a/i;

    invoke-direct {p0}, Lmiuix/view/a/i;-><init>()V

    return-object p0

    :pswitch_9
    new-instance p0, Lmiuix/view/a/g;

    invoke-direct {p0}, Lmiuix/view/a/g;-><init>()V

    return-object p0

    :pswitch_a
    new-instance p0, Lmiuix/view/a/s;

    invoke-direct {p0}, Lmiuix/view/a/s;-><init>()V

    return-object p0

    :pswitch_b
    new-instance p0, Lmiuix/view/a/t;

    invoke-direct {p0}, Lmiuix/view/a/t;-><init>()V

    return-object p0

    :pswitch_c
    new-instance p0, Lmiuix/view/a/r;

    invoke-direct {p0}, Lmiuix/view/a/r;-><init>()V

    return-object p0

    :pswitch_d
    new-instance p0, Lmiuix/view/a/p;

    invoke-direct {p0}, Lmiuix/view/a/p;-><init>()V

    return-object p0

    :pswitch_e
    new-instance p0, Lmiuix/view/a/q;

    invoke-direct {p0}, Lmiuix/view/a/q;-><init>()V

    return-object p0

    :pswitch_f
    new-instance p0, Lmiuix/view/a/o;

    invoke-direct {p0}, Lmiuix/view/a/o;-><init>()V

    return-object p0

    :pswitch_10
    new-instance p0, Lmiuix/view/a/n;

    invoke-direct {p0}, Lmiuix/view/a/n;-><init>()V

    return-object p0

    :pswitch_11
    new-instance p0, Lmiuix/view/a/l;

    invoke-direct {p0}, Lmiuix/view/a/l;-><init>()V

    return-object p0

    :pswitch_12
    new-instance p0, Lmiuix/view/a/m;

    invoke-direct {p0}, Lmiuix/view/a/m;-><init>()V

    return-object p0

    :pswitch_13
    new-instance p0, Lmiuix/view/a/e;

    invoke-direct {p0}, Lmiuix/view/a/e;-><init>()V

    return-object p0

    :pswitch_14
    new-instance p0, Lmiuix/view/a/f;

    invoke-direct {p0}, Lmiuix/view/a/f;-><init>()V

    return-object p0

    :pswitch_15
    new-instance p0, Lmiuix/view/a/d;

    invoke-direct {p0}, Lmiuix/view/a/d;-><init>()V

    return-object p0

    :pswitch_16
    new-instance p0, Lmiuix/view/a/k;

    invoke-direct {p0}, Lmiuix/view/a/k;-><init>()V

    return-object p0

    :pswitch_17
    new-instance p0, Lmiuix/view/a/l;

    invoke-direct {p0}, Lmiuix/view/a/l;-><init>()V

    return-object p0

    :pswitch_18
    new-instance p0, Lmiuix/view/a/j;

    invoke-direct {p0}, Lmiuix/view/a/j;-><init>()V

    return-object p0

    :pswitch_19
    new-instance p0, Lmiuix/animation/h/c$c;

    invoke-direct {p0}, Lmiuix/animation/h/c$c;-><init>()V

    const/4 v0, 0x0

    aget v0, p1, v0

    invoke-virtual {p0, v0}, Lmiuix/animation/h/c$c;->a(F)Lmiuix/animation/h/c$c;

    const/4 v0, 0x1

    aget p1, p1, v0

    invoke-virtual {p0, p1}, Lmiuix/animation/h/c$c;->b(F)Lmiuix/animation/h/c$c;

    return-object p0

    :pswitch_1a
    new-instance p0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {p0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1a
        :pswitch_19
        :pswitch_1a
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Lmiuix/animation/h/c$b;)Landroid/animation/TimeInterpolator;
    .locals 2

    if-eqz p0, :cond_1

    sget-object v0, Lmiuix/animation/h/c;->a:Ljava/util/concurrent/ConcurrentHashMap;

    iget v1, p0, Lmiuix/animation/h/c$a;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/TimeInterpolator;

    if-nez v0, :cond_0

    iget v0, p0, Lmiuix/animation/h/c$a;->a:I

    iget-object v1, p0, Lmiuix/animation/h/c$a;->b:[F

    invoke-static {v0, v1}, Lmiuix/animation/h/c;->a(I[F)Landroid/animation/TimeInterpolator;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lmiuix/animation/h/c;->a:Ljava/util/concurrent/ConcurrentHashMap;

    iget p0, p0, Lmiuix/animation/h/c$a;->a:I

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v1, p0, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static a(I)Z
    .locals 1

    const/4 v0, -0x1

    if-ge p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static varargs b(I[F)Landroid/animation/TimeInterpolator;
    .locals 0

    invoke-static {p0, p1}, Lmiuix/animation/h/c;->d(I[F)Lmiuix/animation/h/c$b;

    move-result-object p0

    invoke-static {p0}, Lmiuix/animation/h/c;->a(Lmiuix/animation/h/c$b;)Landroid/animation/TimeInterpolator;

    move-result-object p0

    return-object p0
.end method

.method public static varargs c(I[F)Lmiuix/animation/h/c$a;
    .locals 3

    const/4 v0, -0x1

    if-lt p0, v0, :cond_2

    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-le v0, v2, :cond_0

    array-length v0, p1

    invoke-static {p1, v2, v0}, Ljava/util/Arrays;->copyOfRange([FII)[F

    move-result-object v0

    goto :goto_0

    :cond_0
    new-array v0, v1, [F

    :goto_0
    invoke-static {p0, v0}, Lmiuix/animation/h/c;->d(I[F)Lmiuix/animation/h/c$b;

    move-result-object p0

    array-length v0, p1

    if-lez v0, :cond_1

    aget p1, p1, v1

    float-to-int p1, p1

    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, Lmiuix/animation/h/c$b;->a(J)Lmiuix/animation/h/c$b;

    :cond_1
    return-object p0

    :cond_2
    new-instance v0, Lmiuix/animation/h/c$a;

    invoke-direct {v0, p0, p1}, Lmiuix/animation/h/c$a;-><init>(I[F)V

    return-object v0
.end method

.method private static varargs d(I[F)Lmiuix/animation/h/c$b;
    .locals 1

    new-instance v0, Lmiuix/animation/h/c$b;

    invoke-direct {v0, p0, p1}, Lmiuix/animation/h/c$b;-><init>(I[F)V

    return-object v0
.end method
