.class public Lmiuix/animation/styles/b;
.super Ljava/lang/Object;


# static fields
.field static final a:Lmiuix/animation/f/m;

.field static final b:Lmiuix/animation/f/a;

.field static final c:Lmiuix/animation/f/h;

.field static d:Lmiuix/animation/f/f;

.field static final e:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Lmiuix/animation/f/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmiuix/animation/f/m;

    invoke-direct {v0}, Lmiuix/animation/f/m;-><init>()V

    sput-object v0, Lmiuix/animation/styles/b;->a:Lmiuix/animation/f/m;

    new-instance v0, Lmiuix/animation/f/a;

    invoke-direct {v0}, Lmiuix/animation/f/a;-><init>()V

    sput-object v0, Lmiuix/animation/styles/b;->b:Lmiuix/animation/f/a;

    new-instance v0, Lmiuix/animation/f/h;

    invoke-direct {v0}, Lmiuix/animation/f/h;-><init>()V

    sput-object v0, Lmiuix/animation/styles/b;->c:Lmiuix/animation/f/h;

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lmiuix/animation/styles/b;->e:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public static a(I)Lmiuix/animation/f/i;
    .locals 1

    const/4 v0, -0x4

    if-eq p0, v0, :cond_2

    const/4 v0, -0x3

    if-eq p0, v0, :cond_1

    const/4 v0, -0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    sget-object p0, Lmiuix/animation/styles/b;->a:Lmiuix/animation/f/m;

    return-object p0

    :cond_1
    sget-object p0, Lmiuix/animation/styles/b;->b:Lmiuix/animation/f/a;

    return-object p0

    :cond_2
    sget-object p0, Lmiuix/animation/styles/b;->c:Lmiuix/animation/f/h;

    return-object p0
.end method

.method private static a(Lmiuix/animation/d/b;D)V
    .locals 13

    iget-wide v1, p0, Lmiuix/animation/d/b;->b:D

    iget-object v0, p0, Lmiuix/animation/d/b;->f:Lmiuix/animation/h/c$a;

    iget v0, v0, Lmiuix/animation/h/c$a;->a:I

    invoke-static {v0}, Lmiuix/animation/styles/b;->a(I)Lmiuix/animation/f/i;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v3, v0, Lmiuix/animation/f/m;

    if-eqz v3, :cond_0

    iget-wide v3, p0, Lmiuix/animation/d/b;->m:D

    invoke-static {v3, v4}, Lmiuix/animation/d/l;->a(D)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lmiuix/animation/d/b;->f:Lmiuix/animation/h/c$a;

    iget-object v3, v3, Lmiuix/animation/h/c$a;->c:[D

    const/4 v4, 0x0

    aget-wide v5, v3, v4

    const/4 v7, 0x1

    aget-wide v8, v3, v7

    const/4 v3, 0x2

    new-array v10, v3, [D

    iget-wide v11, p0, Lmiuix/animation/d/b;->m:D

    aput-wide v11, v10, v4

    iget-wide v3, p0, Lmiuix/animation/d/b;->n:D

    aput-wide v3, v10, v7

    move-wide v3, v5

    move-wide v5, v8

    move-wide v7, p1

    move-object v9, v10

    invoke-interface/range {v0 .. v9}, Lmiuix/animation/f/i;->a(DDDD[D)D

    move-result-wide v0

    iget-wide v2, p0, Lmiuix/animation/d/b;->n:D

    iget-wide v4, p0, Lmiuix/animation/d/b;->b:D

    add-double/2addr v4, v0

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v4, v6

    mul-double/2addr v4, p1

    add-double/2addr v2, v4

    iput-wide v2, p0, Lmiuix/animation/d/b;->n:D

    iput-wide v0, p0, Lmiuix/animation/d/b;->b:D

    goto :goto_1

    :cond_1
    :goto_0
    iget-wide p1, p0, Lmiuix/animation/d/b;->m:D

    iput-wide p1, p0, Lmiuix/animation/d/b;->n:D

    const-wide/16 p1, 0x0

    iput-wide p1, p0, Lmiuix/animation/d/b;->b:D

    :goto_1
    return-void
.end method

.method private static a(Lmiuix/animation/d/b;J)V
    .locals 4

    iget-object v0, p0, Lmiuix/animation/d/b;->f:Lmiuix/animation/h/c$a;

    check-cast v0, Lmiuix/animation/h/c$b;

    invoke-static {v0}, Lmiuix/animation/h/c;->a(Lmiuix/animation/h/c$b;)Landroid/animation/TimeInterpolator;

    move-result-object v1

    iget-wide v2, v0, Lmiuix/animation/h/c$b;->d:J

    cmp-long v0, p1, v2

    if-gez v0, :cond_0

    long-to-float p1, p1

    long-to-float p2, v2

    div-float/2addr p1, p2

    invoke-interface {v1, p1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result p1

    float-to-double p1, p1

    iput-wide p1, p0, Lmiuix/animation/d/b;->k:D

    iget-wide p1, p0, Lmiuix/animation/d/b;->k:D

    iput-wide p1, p0, Lmiuix/animation/d/b;->n:D

    goto :goto_0

    :cond_0
    const/4 p1, 0x3

    invoke-virtual {p0, p1}, Lmiuix/animation/d/b;->a(B)V

    const-wide/high16 p1, 0x3ff0000000000000L    # 1.0

    iput-wide p1, p0, Lmiuix/animation/d/b;->k:D

    iget-wide p1, p0, Lmiuix/animation/d/b;->k:D

    iput-wide p1, p0, Lmiuix/animation/d/b;->n:D

    :goto_0
    return-void
.end method

.method public static a(Lmiuix/animation/e;Lmiuix/animation/d/b;JJJ)V
    .locals 8

    iget-wide v2, p1, Lmiuix/animation/d/b;->i:J

    sub-long v2, p2, v2

    iget-object v0, p1, Lmiuix/animation/d/b;->f:Lmiuix/animation/h/c$a;

    iget v0, v0, Lmiuix/animation/h/c$a;->a:I

    invoke-static {v0}, Lmiuix/animation/h/c;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p0

    move-object v1, p1

    move-wide v4, p4

    move-wide v6, p6

    invoke-static/range {v0 .. v7}, Lmiuix/animation/styles/b;->b(Lmiuix/animation/e;Lmiuix/animation/d/b;JJJ)V

    goto :goto_0

    :cond_0
    invoke-static {p1, v2, v3}, Lmiuix/animation/styles/b;->a(Lmiuix/animation/d/b;J)V

    :goto_0
    return-void
.end method

.method private static a(Lmiuix/animation/d/b;)Z
    .locals 1

    iget-object p0, p0, Lmiuix/animation/d/b;->f:Lmiuix/animation/h/c$a;

    iget p0, p0, Lmiuix/animation/h/c$a;->a:I

    const/4 v0, -0x2

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static a(Lmiuix/animation/f/f;Lmiuix/animation/g/b;IDDJ)Z
    .locals 6

    move-object v0, p0

    move v1, p2

    move-wide v2, p3

    move-wide v4, p5

    invoke-virtual/range {v0 .. v5}, Lmiuix/animation/f/f;->a(IDD)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    const/4 p2, 0x0

    if-eqz p0, :cond_1

    const-wide/16 p3, 0x2710

    cmp-long p3, p7, p3

    if-lez p3, :cond_1

    invoke-static {}, Lmiuix/animation/h/g;->c()Z

    move-result p0

    if-eqz p0, :cond_0

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "animation for "

    invoke-virtual {p0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " stopped for running time too long, totalTime = "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p7, p8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-array p1, p2, [Ljava/lang/Object;

    invoke-static {p0, p1}, Lmiuix/animation/h/g;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    move p0, p2

    :cond_1
    return p0
.end method

.method private static b(Lmiuix/animation/d/b;)V
    .locals 2

    invoke-static {p0}, Lmiuix/animation/styles/b;->a(Lmiuix/animation/d/b;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-wide v0, p0, Lmiuix/animation/d/b;->m:D

    iput-wide v0, p0, Lmiuix/animation/d/b;->n:D

    return-void
.end method

.method private static b(Lmiuix/animation/e;Lmiuix/animation/d/b;JJJ)V
    .locals 14

    move-object v0, p1

    move-wide/from16 v1, p4

    move-wide/from16 v3, p6

    cmp-long v5, v1, v3

    if-lez v5, :cond_0

    long-to-float v1, v1

    long-to-float v2, v3

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    :goto_0
    long-to-double v2, v3

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    sget-object v4, Lmiuix/animation/styles/b;->e:Ljava/lang/ThreadLocal;

    const-class v5, Lmiuix/animation/f/f;

    invoke-static {v4, v5}, Lmiuix/animation/h/a;->a(Ljava/lang/ThreadLocal;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiuix/animation/f/f;

    sput-object v4, Lmiuix/animation/styles/b;->d:Lmiuix/animation/f/f;

    sget-object v4, Lmiuix/animation/styles/b;->d:Lmiuix/animation/f/f;

    iget-object v5, v0, Lmiuix/animation/d/b;->a:Lmiuix/animation/g/b;

    iget-wide v6, v0, Lmiuix/animation/d/b;->m:D

    move-object v8, p0

    invoke-virtual {v4, p0, v5, v6, v7}, Lmiuix/animation/f/f;->a(Lmiuix/animation/e;Lmiuix/animation/g/b;D)V

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v1, :cond_2

    invoke-static {p1, v2, v3}, Lmiuix/animation/styles/b;->a(Lmiuix/animation/d/b;D)V

    sget-object v5, Lmiuix/animation/styles/b;->d:Lmiuix/animation/f/f;

    iget-object v6, v0, Lmiuix/animation/d/b;->a:Lmiuix/animation/g/b;

    iget-object v7, v0, Lmiuix/animation/d/b;->f:Lmiuix/animation/h/c$a;

    iget v7, v7, Lmiuix/animation/h/c$a;->a:I

    iget-wide v8, v0, Lmiuix/animation/d/b;->n:D

    iget-wide v10, v0, Lmiuix/animation/d/b;->b:D

    move-wide/from16 v12, p2

    invoke-static/range {v5 .. v13}, Lmiuix/animation/styles/b;->a(Lmiuix/animation/f/f;Lmiuix/animation/g/b;IDDJ)Z

    move-result v5

    if-nez v5, :cond_1

    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Lmiuix/animation/d/b;->a(B)V

    invoke-static {p1}, Lmiuix/animation/styles/b;->b(Lmiuix/animation/d/b;)V

    goto :goto_2

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_2
    :goto_2
    return-void
.end method
