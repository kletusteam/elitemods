.class Lmiuix/popupwidget/widget/ListPopupWindow$a;
.super Landroid/widget/ListView;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/popupwidget/widget/ListPopupWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private a:Z

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 2

    const/4 v0, 0x0

    const v1, 0x101006d

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean p2, p0, Lmiuix/popupwidget/widget/ListPopupWindow$a;->b:Z

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/widget/ListView;->setCacheColorHint(I)V

    return-void
.end method

.method static synthetic a(Lmiuix/popupwidget/widget/ListPopupWindow$a;Z)Z
    .locals 0

    iput-boolean p1, p0, Lmiuix/popupwidget/widget/ListPopupWindow$a;->a:Z

    return p1
.end method


# virtual methods
.method final a(IIIII)I
    .locals 10

    goto/32 :goto_d

    nop

    :goto_0
    if-ge v4, p4, :cond_0

    goto/32 :goto_1e

    :cond_0
    goto/32 :goto_3f

    nop

    :goto_1
    if-lt p2, v1, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_20

    nop

    :goto_2
    if-gtz v8, :cond_2

    goto/32 :goto_1a

    :cond_2
    goto/32 :goto_1f

    nop

    :goto_3
    invoke-static {p3, p3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    :goto_4
    goto/32 :goto_1b

    nop

    :goto_5
    if-nez v1, :cond_3

    goto/32 :goto_17

    :cond_3
    goto/32 :goto_16

    nop

    :goto_6
    goto/16 :goto_2e

    :goto_7
    goto/32 :goto_24

    nop

    :goto_8
    move v0, p3

    :goto_9
    goto/32 :goto_18

    nop

    :goto_a
    if-ne v8, v5, :cond_4

    goto/32 :goto_26

    :cond_4
    goto/32 :goto_36

    nop

    :goto_b
    move v4, p2

    goto/32 :goto_3d

    nop

    :goto_c
    invoke-virtual {p0}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v0

    goto/32 :goto_27

    nop

    :goto_d
    invoke-virtual {p0}, Landroid/widget/ListView;->getListPaddingTop()I

    move-result p2

    goto/32 :goto_29

    nop

    :goto_e
    move v7, v5

    goto/32 :goto_2d

    nop

    :goto_f
    add-int/2addr p2, p3

    goto/32 :goto_1c

    nop

    :goto_10
    add-int/2addr v4, v0

    :goto_11
    goto/32 :goto_21

    nop

    :goto_12
    if-ge p2, p5, :cond_5

    goto/32 :goto_14

    :cond_5
    goto/32 :goto_13

    nop

    :goto_13
    move v7, v4

    :goto_14
    goto/32 :goto_2f

    nop

    :goto_15
    move v5, p2

    goto/32 :goto_e

    nop

    :goto_16
    goto :goto_9

    :goto_17
    goto/32 :goto_8

    nop

    :goto_18
    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    goto/32 :goto_3e

    nop

    :goto_19
    goto :goto_4

    :goto_1a
    goto/32 :goto_3

    nop

    :goto_1b
    invoke-virtual {v6, p1, v8}, Landroid/view/View;->measure(II)V

    goto/32 :goto_39

    nop

    :goto_1c
    const/4 p3, 0x0

    goto/32 :goto_38

    nop

    :goto_1d
    return p4

    :goto_1e
    goto/32 :goto_3b

    nop

    :goto_1f
    const/high16 v9, 0x40000000    # 2.0f

    goto/32 :goto_37

    nop

    :goto_20
    invoke-interface {v2, p2}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v8

    goto/32 :goto_a

    nop

    :goto_21
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    goto/32 :goto_28

    nop

    :goto_22
    invoke-interface {v2, p2, v6, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    goto/32 :goto_30

    nop

    :goto_23
    if-ne v4, p4, :cond_6

    goto/32 :goto_35

    :cond_6
    goto/32 :goto_34

    nop

    :goto_24
    return v4

    :goto_25
    move v5, v8

    :goto_26
    goto/32 :goto_22

    nop

    :goto_27
    invoke-virtual {p0}, Landroid/widget/ListView;->getDivider()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto/32 :goto_3a

    nop

    :goto_28
    add-int/2addr v4, v8

    goto/32 :goto_0

    nop

    :goto_29
    invoke-virtual {p0}, Landroid/widget/ListView;->getListPaddingBottom()I

    move-result p3

    goto/32 :goto_c

    nop

    :goto_2a
    if-eqz v2, :cond_7

    goto/32 :goto_2c

    :cond_7
    goto/32 :goto_33

    nop

    :goto_2b
    return p2

    :goto_2c
    goto/32 :goto_f

    nop

    :goto_2d
    move-object v6, v3

    :goto_2e
    goto/32 :goto_1

    nop

    :goto_2f
    add-int/lit8 p2, p2, 0x1

    goto/32 :goto_6

    nop

    :goto_30
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    goto/32 :goto_31

    nop

    :goto_31
    iget v8, v8, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto/32 :goto_2

    nop

    :goto_32
    if-gtz v7, :cond_8

    goto/32 :goto_35

    :cond_8
    goto/32 :goto_23

    nop

    :goto_33
    add-int/2addr p2, p3

    goto/32 :goto_2b

    nop

    :goto_34
    move p4, v7

    :goto_35
    goto/32 :goto_1d

    nop

    :goto_36
    move-object v6, v3

    goto/32 :goto_25

    nop

    :goto_37
    invoke-static {v8, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    goto/32 :goto_19

    nop

    :goto_38
    if-gtz v0, :cond_9

    goto/32 :goto_17

    :cond_9
    goto/32 :goto_5

    nop

    :goto_39
    if-gtz p2, :cond_a

    goto/32 :goto_11

    :cond_a
    goto/32 :goto_10

    nop

    :goto_3a
    invoke-virtual {p0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    goto/32 :goto_2a

    nop

    :goto_3b
    if-gez p5, :cond_b

    goto/32 :goto_14

    :cond_b
    goto/32 :goto_12

    nop

    :goto_3c
    if-gt p2, p5, :cond_c

    goto/32 :goto_35

    :cond_c
    goto/32 :goto_32

    nop

    :goto_3d
    move p2, p3

    goto/32 :goto_15

    nop

    :goto_3e
    const/4 v3, 0x0

    goto/32 :goto_b

    nop

    :goto_3f
    if-gez p5, :cond_d

    goto/32 :goto_35

    :cond_d
    goto/32 :goto_3c

    nop
.end method

.method public hasFocus()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/popupwidget/widget/ListPopupWindow$a;->b:Z

    if-nez v0, :cond_1

    invoke-super {p0}, Landroid/widget/ListView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public hasWindowFocus()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/popupwidget/widget/ListPopupWindow$a;->b:Z

    if-nez v0, :cond_1

    invoke-super {p0}, Landroid/widget/ListView;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isFocused()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/popupwidget/widget/ListPopupWindow$a;->b:Z

    if-nez v0, :cond_1

    invoke-super {p0}, Landroid/widget/ListView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isInTouchMode()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/popupwidget/widget/ListPopupWindow$a;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lmiuix/popupwidget/widget/ListPopupWindow$a;->a:Z

    if-nez v0, :cond_1

    :cond_0
    invoke-super {p0}, Landroid/widget/ListView;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
