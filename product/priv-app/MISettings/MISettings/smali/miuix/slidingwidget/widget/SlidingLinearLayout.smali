.class public Lmiuix/slidingwidget/widget/SlidingLinearLayout;
.super Landroid/widget/LinearLayout;


# instance fields
.field private final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Landroid/view/View;",
            "Landroid/util/Pair<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Landroid/view/View;",
            "Landroid/util/Pair<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiuix/slidingwidget/widget/SlidingLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lmiuix/slidingwidget/widget/SlidingLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lmiuix/slidingwidget/widget/SlidingLinearLayout;->a:Ljava/util/HashMap;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lmiuix/slidingwidget/widget/SlidingLinearLayout;->b:Ljava/util/HashMap;

    const/4 p1, 0x4

    new-array p1, p1, [I

    iput-object p1, p0, Lmiuix/slidingwidget/widget/SlidingLinearLayout;->c:[I

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 17
    .annotation build Landroidx/annotation/RequiresApi;
        api = 0x18
    .end annotation

    move-object/from16 v0, p0

    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    if-eqz p1, :cond_d

    invoke-virtual/range {p0 .. p0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Landroid/widget/LinearLayout;->getOrientation()I

    move-result v2

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-ne v2, v5, :cond_1

    iget-object v2, v0, Lmiuix/slidingwidget/widget/SlidingLinearLayout;->c:[I

    aget v2, v2, v5

    sub-int v2, v2, p3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    iget-object v7, v0, Lmiuix/slidingwidget/widget/SlidingLinearLayout;->c:[I

    aget v7, v7, v3

    sub-int v7, v7, p5

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v7

    if-le v2, v7, :cond_0

    :goto_0
    move v2, v5

    goto :goto_1

    :cond_0
    move v2, v6

    goto :goto_1

    :cond_1
    iget-object v2, v0, Lmiuix/slidingwidget/widget/SlidingLinearLayout;->c:[I

    aget v2, v2, v6

    sub-int v2, v2, p2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    iget-object v7, v0, Lmiuix/slidingwidget/widget/SlidingLinearLayout;->c:[I

    aget v7, v7, v4

    sub-int v7, v7, p4

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v7

    if-le v2, v7, :cond_0

    goto :goto_0

    :goto_1
    move v7, v6

    :goto_2
    if-ge v7, v1, :cond_c

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    iget-object v9, v0, Lmiuix/slidingwidget/widget/SlidingLinearLayout;->a:Ljava/util/HashMap;

    const-string v10, "end"

    const-string v11, "start"

    const/16 v12, 0x8

    const-wide/16 v13, 0x0

    if-eqz v9, :cond_6

    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    move-result v9

    if-lez v9, :cond_6

    iget-object v9, v0, Lmiuix/slidingwidget/widget/SlidingLinearLayout;->a:Ljava/util/HashMap;

    invoke-virtual {v9, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/util/Pair;

    if-eqz v9, :cond_5

    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v15

    if-eq v15, v12, :cond_5

    invoke-virtual {v8}, Landroid/view/View;->getX()F

    move-result v15

    iget-object v3, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    cmpl-float v3, v15, v3

    if-nez v3, :cond_2

    invoke-virtual {v8}, Landroid/view/View;->getY()F

    move-result v3

    iget-object v15, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v15, Ljava/lang/Float;

    invoke-virtual {v15}, Ljava/lang/Float;->floatValue()F

    move-result v15

    cmpl-float v3, v3, v15

    if-eqz v3, :cond_3

    :cond_2
    if-nez v2, :cond_3

    iget-object v3, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-virtual {v8}, Landroid/view/View;->getX()F

    move-result v15

    sub-float v15, v3, v15

    iget-object v3, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-virtual {v8}, Landroid/view/View;->getY()F

    move-result v9

    sub-float/2addr v3, v9

    goto :goto_3

    :cond_3
    invoke-virtual {v8}, Landroid/view/View;->getX()F

    move-result v3

    iget-object v15, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v15, Ljava/lang/Float;

    invoke-virtual {v15}, Ljava/lang/Float;->floatValue()F

    move-result v15

    cmpl-float v3, v3, v15

    if-nez v3, :cond_4

    invoke-virtual {v8}, Landroid/view/View;->getY()F

    move-result v3

    iget-object v9, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v9, Ljava/lang/Float;

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v9

    cmpl-float v3, v3, v9

    if-nez v3, :cond_4

    if-eqz v2, :cond_4

    iget-object v3, v0, Lmiuix/slidingwidget/widget/SlidingLinearLayout;->c:[I

    aget v9, v3, v6

    sub-int v9, v9, p2

    int-to-float v15, v9

    aget v3, v3, v5

    sub-int v3, v3, p3

    int-to-float v3, v3

    goto :goto_3

    :cond_4
    const/4 v3, 0x0

    const/4 v15, 0x0

    :goto_3
    new-instance v9, Lmiuix/animation/b/a;

    invoke-direct {v9, v11}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    sget-object v4, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    move/from16 v16, v7

    float-to-double v6, v15

    invoke-virtual {v9, v4, v6, v7}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v4, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    float-to-double v6, v3

    invoke-virtual {v9, v4, v6, v7}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    new-instance v3, Lmiuix/animation/b/a;

    invoke-direct {v3, v10}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    sget-object v4, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    invoke-virtual {v3, v4, v13, v14}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v4, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    invoke-virtual {v3, v4, v13, v14}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    new-array v4, v5, [Landroid/view/View;

    const/4 v6, 0x0

    aput-object v8, v4, v6

    invoke-static {v4}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v4

    invoke-interface {v4}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v4

    invoke-interface {v4, v9}, Lmiuix/animation/k;->b(Ljava/lang/Object;)Lmiuix/animation/k;

    new-array v7, v6, [Lmiuix/animation/a/a;

    invoke-interface {v4, v9, v3, v7}, Lmiuix/animation/k;->a(Ljava/lang/Object;Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    goto :goto_4

    :cond_5
    move/from16 v16, v7

    :goto_4
    iget-object v3, v0, Lmiuix/slidingwidget/widget/SlidingLinearLayout;->a:Ljava/util/HashMap;

    invoke-virtual {v3, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    :cond_6
    move/from16 v16, v7

    :goto_5
    iget-object v3, v0, Lmiuix/slidingwidget/widget/SlidingLinearLayout;->b:Ljava/util/HashMap;

    if-eqz v3, :cond_b

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    if-lez v3, :cond_b

    iget-object v3, v0, Lmiuix/slidingwidget/widget/SlidingLinearLayout;->b:Ljava/util/HashMap;

    invoke-virtual {v3, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    if-eqz v3, :cond_a

    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-eq v4, v12, :cond_a

    invoke-virtual {v8}, Landroid/view/View;->getX()F

    move-result v4

    iget-object v6, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    cmpl-float v4, v4, v6

    if-nez v4, :cond_7

    invoke-virtual {v8}, Landroid/view/View;->getY()F

    move-result v4

    iget-object v6, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    cmpl-float v4, v4, v6

    if-eqz v4, :cond_8

    :cond_7
    if-nez v2, :cond_8

    iget-object v4, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v8}, Landroid/view/View;->getX()F

    move-result v6

    sub-float v15, v4, v6

    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-virtual {v8}, Landroid/view/View;->getY()F

    move-result v4

    sub-float/2addr v3, v4

    goto :goto_6

    :cond_8
    invoke-virtual {v8}, Landroid/view/View;->getX()F

    move-result v4

    iget-object v6, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    cmpl-float v4, v4, v6

    if-nez v4, :cond_9

    invoke-virtual {v8}, Landroid/view/View;->getY()F

    move-result v4

    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    cmpl-float v3, v4, v3

    if-nez v3, :cond_9

    if-eqz v2, :cond_9

    iget-object v3, v0, Lmiuix/slidingwidget/widget/SlidingLinearLayout;->c:[I

    const/4 v4, 0x0

    aget v6, v3, v4

    sub-int v6, v6, p2

    int-to-float v15, v6

    aget v3, v3, v5

    sub-int v3, v3, p3

    int-to-float v3, v3

    goto :goto_6

    :cond_9
    const/4 v3, 0x0

    const/4 v15, 0x0

    :goto_6
    new-instance v4, Lmiuix/animation/b/a;

    invoke-direct {v4, v11}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    sget-object v6, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    float-to-double v11, v15

    invoke-virtual {v4, v6, v11, v12}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v6, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    float-to-double v11, v3

    invoke-virtual {v4, v6, v11, v12}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    new-instance v3, Lmiuix/animation/b/a;

    invoke-direct {v3, v10}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    sget-object v6, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    invoke-virtual {v3, v6, v13, v14}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v6, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    invoke-virtual {v3, v6, v13, v14}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    new-array v6, v5, [Landroid/view/View;

    const/4 v7, 0x0

    aput-object v8, v6, v7

    invoke-static {v6}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v6

    invoke-interface {v6}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v6

    invoke-interface {v6, v4}, Lmiuix/animation/k;->b(Ljava/lang/Object;)Lmiuix/animation/k;

    new-array v9, v7, [Lmiuix/animation/a/a;

    invoke-interface {v6, v4, v3, v9}, Lmiuix/animation/k;->a(Ljava/lang/Object;Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    :cond_a
    iget-object v3, v0, Lmiuix/slidingwidget/widget/SlidingLinearLayout;->b:Ljava/util/HashMap;

    invoke-virtual {v3, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_b
    add-int/lit8 v7, v16, 0x1

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v6, 0x0

    goto/16 :goto_2

    :cond_c
    iget-object v1, v0, Lmiuix/slidingwidget/widget/SlidingLinearLayout;->a:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    iget-object v1, v0, Lmiuix/slidingwidget/widget/SlidingLinearLayout;->b:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    iget-object v1, v0, Lmiuix/slidingwidget/widget/SlidingLinearLayout;->c:[I

    const/4 v2, 0x0

    aput p2, v1, v2

    aput p3, v1, v5

    const/4 v2, 0x2

    aput p4, v1, v2

    const/4 v2, 0x3

    aput p5, v1, v2

    :cond_d
    return-void
.end method
