.class public Lmiuix/slidingwidget/widget/j;
.super Ljava/lang/Object;


# static fields
.field private static final a:[I


# instance fields
.field private A:Z

.field private B:Lmiuix/animation/f/j;

.field private C:Lmiuix/animation/f/j;

.field private D:Lmiuix/animation/f/j;

.field private E:Lmiuix/animation/f/j;

.field private F:Lmiuix/animation/f/j;

.field private G:Lmiuix/animation/f/j;

.field private H:Lmiuix/animation/f/j;

.field private I:Lmiuix/animation/f/j;

.field private J:Lmiuix/animation/f/j;

.field private K:Lmiuix/animation/f/j;

.field private L:Lmiuix/animation/f/j;

.field private M:F

.field private N:F

.field private O:F

.field private P:F

.field private Q:F

.field private R:Landroid/graphics/drawable/Drawable;

.field private S:Landroid/graphics/drawable/Drawable;

.field private T:Z

.field private U:I

.field private V:I

.field private W:Z

.field private X:F

.field private Y:Lmiuix/animation/g/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiuix/animation/g/b<",
            "Landroid/widget/CompoundButton;",
            ">;"
        }
    .end annotation
.end field

.field private Z:Lmiuix/animation/f/e$c;

.field private aa:Lmiuix/animation/g/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiuix/animation/g/b<",
            "Landroid/widget/CompoundButton;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/graphics/drawable/Drawable;

.field private ba:Lmiuix/animation/g/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiuix/animation/g/b<",
            "Landroid/widget/CompoundButton;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private ca:Lmiuix/animation/g/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiuix/animation/g/b<",
            "Landroid/widget/CompoundButton;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/graphics/drawable/Drawable;

.field private da:Lmiuix/animation/g/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiuix/animation/g/b<",
            "Landroid/widget/CompoundButton;",
            ">;"
        }
    .end annotation
.end field

.field private e:I

.field private ea:I

.field private f:I

.field private fa:I

.field private g:I

.field private ga:I

.field private h:I

.field private ha:F

.field private i:I

.field private ia:[F

.field private j:I

.field private ja:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:Z

.field private p:Z

.field private q:I

.field private r:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private s:Landroid/graphics/Rect;

.field private t:Landroid/graphics/drawable/StateListDrawable;

.field private u:Z

.field private v:Lmiuix/animation/g/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiuix/animation/g/b<",
            "Landroid/widget/CompoundButton;",
            ">;"
        }
    .end annotation
.end field

.field private w:Landroid/graphics/drawable/Drawable;

.field private x:Landroid/graphics/drawable/Drawable;

.field private y:Landroid/graphics/drawable/Drawable;

.field private z:Landroid/widget/CompoundButton;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    sput-object v0, Lmiuix/slidingwidget/widget/j;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/widget/CompoundButton;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lmiuix/slidingwidget/widget/j;->s:Landroid/graphics/Rect;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/slidingwidget/widget/j;->u:Z

    new-instance v1, Lmiuix/slidingwidget/widget/b;

    const-string v2, "SliderOffset"

    invoke-direct {v1, p0, v2}, Lmiuix/slidingwidget/widget/b;-><init>(Lmiuix/slidingwidget/widget/j;Ljava/lang/String;)V

    iput-object v1, p0, Lmiuix/slidingwidget/widget/j;->v:Lmiuix/animation/g/b;

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lmiuix/slidingwidget/widget/j;->M:F

    const/4 v2, 0x0

    iput v2, p0, Lmiuix/slidingwidget/widget/j;->N:F

    const v3, 0x3dcccccd    # 0.1f

    iput v3, p0, Lmiuix/slidingwidget/widget/j;->O:F

    iput v1, p0, Lmiuix/slidingwidget/widget/j;->P:F

    iput v2, p0, Lmiuix/slidingwidget/widget/j;->Q:F

    iput-boolean v0, p0, Lmiuix/slidingwidget/widget/j;->T:Z

    const/4 v3, -0x1

    iput v3, p0, Lmiuix/slidingwidget/widget/j;->U:I

    iput v3, p0, Lmiuix/slidingwidget/widget/j;->V:I

    iput-boolean v0, p0, Lmiuix/slidingwidget/widget/j;->W:Z

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lmiuix/slidingwidget/widget/j;->X:F

    new-instance v0, Lmiuix/slidingwidget/widget/c;

    const-string v3, "SliderScale"

    invoke-direct {v0, p0, v3}, Lmiuix/slidingwidget/widget/c;-><init>(Lmiuix/slidingwidget/widget/j;Ljava/lang/String;)V

    iput-object v0, p0, Lmiuix/slidingwidget/widget/j;->Y:Lmiuix/animation/g/b;

    new-instance v0, Lmiuix/slidingwidget/widget/a;

    invoke-direct {v0, p0}, Lmiuix/slidingwidget/widget/a;-><init>(Lmiuix/slidingwidget/widget/j;)V

    iput-object v0, p0, Lmiuix/slidingwidget/widget/j;->Z:Lmiuix/animation/f/e$c;

    new-instance v0, Lmiuix/slidingwidget/widget/d;

    const-string v3, "SliderShadowAlpha"

    invoke-direct {v0, p0, v3}, Lmiuix/slidingwidget/widget/d;-><init>(Lmiuix/slidingwidget/widget/j;Ljava/lang/String;)V

    iput-object v0, p0, Lmiuix/slidingwidget/widget/j;->aa:Lmiuix/animation/g/b;

    new-instance v0, Lmiuix/slidingwidget/widget/e;

    const-string v3, "StrokeAlpha"

    invoke-direct {v0, p0, v3}, Lmiuix/slidingwidget/widget/e;-><init>(Lmiuix/slidingwidget/widget/j;Ljava/lang/String;)V

    iput-object v0, p0, Lmiuix/slidingwidget/widget/j;->ba:Lmiuix/animation/g/b;

    new-instance v0, Lmiuix/slidingwidget/widget/f;

    const-string v3, "MaskCheckedSlideBarAlpha"

    invoke-direct {v0, p0, v3}, Lmiuix/slidingwidget/widget/f;-><init>(Lmiuix/slidingwidget/widget/j;Ljava/lang/String;)V

    iput-object v0, p0, Lmiuix/slidingwidget/widget/j;->ca:Lmiuix/animation/g/b;

    new-instance v0, Lmiuix/slidingwidget/widget/g;

    const-string v3, "MaskUnCheckedSlideBarAlpha"

    invoke-direct {v0, p0, v3}, Lmiuix/slidingwidget/widget/g;-><init>(Lmiuix/slidingwidget/widget/j;Ljava/lang/String;)V

    iput-object v0, p0, Lmiuix/slidingwidget/widget/j;->da:Lmiuix/animation/g/b;

    iput v1, p0, Lmiuix/slidingwidget/widget/j;->ha:F

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lmiuix/slidingwidget/widget/j;->ia:[F

    iput-object p1, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p1

    iput-boolean p1, p0, Lmiuix/slidingwidget/widget/j;->A:Z

    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p1

    if-nez p1, :cond_0

    iput v2, p0, Lmiuix/slidingwidget/widget/j;->P:F

    :cond_0
    return-void

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method static synthetic a(Lmiuix/slidingwidget/widget/j;)F
    .locals 0

    iget p0, p0, Lmiuix/slidingwidget/widget/j;->M:F

    return p0
.end method

.method static synthetic a(Lmiuix/slidingwidget/widget/j;F)F
    .locals 0

    iput p1, p0, Lmiuix/slidingwidget/widget/j;->M:F

    return p1
.end method

.method private a(Landroid/graphics/Canvas;F)V
    .locals 3

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->P:F

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, v0

    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr v1, v0

    mul-float/2addr v1, p2

    float-to-int v1, v1

    if-lez v1, :cond_0

    iget-object v2, p0, Lmiuix/slidingwidget/widget/j;->x:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v1, p0, Lmiuix/slidingwidget/widget/j;->x:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    iget v1, p0, Lmiuix/slidingwidget/widget/j;->Q:F

    mul-float/2addr v1, v0

    mul-float/2addr v1, p2

    float-to-int v1, v1

    if-lez v1, :cond_1

    iget-object v2, p0, Lmiuix/slidingwidget/widget/j;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v1, p0, Lmiuix/slidingwidget/widget/j;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_1
    iget v1, p0, Lmiuix/slidingwidget/widget/j;->P:F

    mul-float/2addr v1, v0

    mul-float/2addr v1, p2

    float-to-int p2, v1

    if-lez p2, :cond_2

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object p2, p0, Lmiuix/slidingwidget/widget/j;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_2
    return-void
.end method

.method private a(Landroid/graphics/Canvas;II)V
    .locals 6

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->N:F

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lmiuix/slidingwidget/widget/j;->R:Landroid/graphics/drawable/Drawable;

    instance-of v2, v1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Lmiuix/slidingwidget/widget/j;->R:Landroid/graphics/drawable/Drawable;

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lmiuix/slidingwidget/widget/j;->R:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    :goto_0
    iget-object v3, p0, Lmiuix/slidingwidget/widget/j;->R:Landroid/graphics/drawable/Drawable;

    div-int/lit8 v1, v1, 0x2

    sub-int v4, p2, v1

    div-int/lit8 v2, v2, 0x2

    sub-int v5, p3, v2

    add-int/2addr p2, v1

    add-int/2addr p3, v2

    invoke-virtual {v3, v4, v5, p2, p3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object p2, p0, Lmiuix/slidingwidget/widget/j;->R:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object p2, p0, Lmiuix/slidingwidget/widget/j;->R:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method private a(Landroid/graphics/Canvas;IIIIF)V
    .locals 3

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->S:Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lmiuix/slidingwidget/widget/j;->O:F

    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    mul-float/2addr v1, p6

    float-to-int p6, v1

    invoke-virtual {v0, p6}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object p6, p0, Lmiuix/slidingwidget/widget/j;->S:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p6, p2, p3, p4, p5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object p2, p0, Lmiuix/slidingwidget/widget/j;->S:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method private a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    iput-object p1, p0, Lmiuix/slidingwidget/widget/j;->w:Landroid/graphics/drawable/Drawable;

    iput-object p2, p0, Lmiuix/slidingwidget/widget/j;->x:Landroid/graphics/drawable/Drawable;

    iput-object p3, p0, Lmiuix/slidingwidget/widget/j;->y:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method private a(ZILjava/lang/Runnable;)V
    .locals 3

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->L:Lmiuix/animation/f/j;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->L:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->a()V

    :cond_0
    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-eq p1, v0, :cond_1

    return-void

    :cond_1
    new-instance v0, Lmiuix/animation/f/j;

    iget-object v1, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    iget-object v2, p0, Lmiuix/slidingwidget/widget/j;->v:Lmiuix/animation/g/b;

    int-to-float p2, p2

    invoke-direct {v0, v1, v2, p2}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/slidingwidget/widget/j;->L:Lmiuix/animation/f/j;

    iget-object p2, p0, Lmiuix/slidingwidget/widget/j;->L:Lmiuix/animation/f/j;

    invoke-virtual {p2}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object p2

    const v0, 0x4476bd71

    invoke-virtual {p2, v0}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object p2, p0, Lmiuix/slidingwidget/widget/j;->L:Lmiuix/animation/f/j;

    invoke-virtual {p2}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object p2

    const v0, 0x3f333333    # 0.7f

    invoke-virtual {p2, v0}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object p2, p0, Lmiuix/slidingwidget/widget/j;->L:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->Z:Lmiuix/animation/f/e$c;

    invoke-virtual {p2, v0}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    iget-object p2, p0, Lmiuix/slidingwidget/widget/j;->L:Lmiuix/animation/f/j;

    new-instance v0, Lmiuix/slidingwidget/widget/h;

    invoke-direct {v0, p0, p3}, Lmiuix/slidingwidget/widget/h;-><init>(Lmiuix/slidingwidget/widget/j;Ljava/lang/Runnable;)V

    invoke-virtual {p2, v0}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$b;)Lmiuix/animation/f/e;

    iget-object p2, p0, Lmiuix/slidingwidget/widget/j;->L:Lmiuix/animation/f/j;

    invoke-virtual {p2}, Lmiuix/animation/f/j;->e()V

    if-eqz p1, :cond_3

    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->H:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->d()Z

    move-result p1

    if-nez p1, :cond_2

    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->H:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/j;->e()V

    :cond_2
    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->I:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->d()Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->I:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->a()V

    goto :goto_0

    :cond_3
    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->I:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->d()Z

    move-result p1

    if-nez p1, :cond_4

    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->I:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/j;->e()V

    :cond_4
    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->H:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->d()Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->H:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->a()V

    :cond_5
    :goto_0
    return-void
.end method

.method static synthetic a(Lmiuix/slidingwidget/widget/j;Z)Z
    .locals 0

    iput-boolean p1, p0, Lmiuix/slidingwidget/widget/j;->A:Z

    return p1
.end method

.method private a(Landroid/view/View;Landroid/view/MotionEvent;)[F
    .locals 8

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result p2

    const/4 v1, 0x2

    new-array v2, v1, [I

    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    const/4 v3, 0x0

    aget v4, v2, v3

    int-to-float v4, v4

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x3f000000    # 0.5f

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x1

    aget v2, v2, v5

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v6

    add-float/2addr v2, v7

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v6

    const/4 v7, 0x0

    if-nez v6, :cond_0

    move v0, v7

    goto :goto_0

    :cond_0
    sub-float/2addr v0, v4

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v0, v4

    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    if-nez v4, :cond_1

    goto :goto_1

    :cond_1
    sub-float/2addr p2, v2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    int-to-float p1, p1

    div-float v7, p2, p1

    :goto_1
    const/high16 p1, 0x3f800000    # 1.0f

    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    move-result p2

    const/high16 v0, -0x40800000    # -1.0f

    invoke-static {v0, p2}, Ljava/lang/Math;->max(FF)F

    move-result p2

    invoke-static {p1, v7}, Ljava/lang/Math;->min(FF)F

    move-result p1

    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->ja:I

    int-to-float v2, v0

    mul-float/2addr p2, v2

    int-to-float v0, v0

    mul-float/2addr p1, v0

    new-array v0, v1, [F

    aput p2, v0, v3

    aput p1, v0, v5

    return-object v0
.end method

.method static synthetic b(Lmiuix/slidingwidget/widget/j;)F
    .locals 0

    iget p0, p0, Lmiuix/slidingwidget/widget/j;->N:F

    return p0
.end method

.method static synthetic b(Lmiuix/slidingwidget/widget/j;F)F
    .locals 0

    iput p1, p0, Lmiuix/slidingwidget/widget/j;->N:F

    return p1
.end method

.method private b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 5

    new-instance v0, Lmiuix/smooth/SmoothContainerDrawable;

    invoke-direct {v0}, Lmiuix/smooth/SmoothContainerDrawable;-><init>()V

    iget-object v1, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->getLayerType()I

    move-result v1

    invoke-virtual {v0, v1}, Lmiuix/smooth/SmoothContainerDrawable;->a(I)V

    iget v1, p0, Lmiuix/slidingwidget/widget/j;->ea:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lmiuix/smooth/SmoothContainerDrawable;->a(F)V

    invoke-virtual {v0, p1}, Lmiuix/smooth/SmoothContainerDrawable;->a(Landroid/graphics/drawable/Drawable;)V

    new-instance p1, Landroid/graphics/Rect;

    iget v1, p0, Lmiuix/slidingwidget/widget/j;->ga:I

    iget v2, p0, Lmiuix/slidingwidget/widget/j;->fa:I

    iget v3, p0, Lmiuix/slidingwidget/widget/j;->f:I

    sub-int/2addr v3, v1

    iget v4, p0, Lmiuix/slidingwidget/widget/j;->g:I

    sub-int/2addr v4, v2

    invoke-direct {p1, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    return-object v0
.end method

.method private b(Landroid/graphics/Canvas;)V
    .locals 0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method private b(Landroid/graphics/Canvas;II)V
    .locals 1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->M:F

    int-to-float p2, p2

    int-to-float p3, p3

    invoke-virtual {p1, v0, v0, p2, p3}, Landroid/graphics/Canvas;->scale(FFFF)V

    return-void
.end method

.method private b(Z)V
    .locals 2

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    invoke-direct {p0, p1}, Lmiuix/slidingwidget/widget/j;->d(Z)V

    invoke-virtual {p0}, Lmiuix/slidingwidget/widget/j;->j()V

    :cond_0
    if-eqz p1, :cond_1

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->k:I

    goto :goto_0

    :cond_1
    iget v0, p0, Lmiuix/slidingwidget/widget/j;->j:I

    :goto_0
    new-instance v1, Lmiuix/slidingwidget/widget/i;

    invoke-direct {v1, p0}, Lmiuix/slidingwidget/widget/i;-><init>(Lmiuix/slidingwidget/widget/j;)V

    invoke-direct {p0, p1, v0, v1}, Lmiuix/slidingwidget/widget/j;->a(ZILjava/lang/Runnable;)V

    return-void
.end method

.method static synthetic c(Lmiuix/slidingwidget/widget/j;)F
    .locals 0

    iget p0, p0, Lmiuix/slidingwidget/widget/j;->O:F

    return p0
.end method

.method static synthetic c(Lmiuix/slidingwidget/widget/j;F)F
    .locals 0

    iput p1, p0, Lmiuix/slidingwidget/widget/j;->O:F

    return p1
.end method

.method private c(I)V
    .locals 3

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-static {v0}, Landroidx/appcompat/widget/Fa;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    neg-int p1, p1

    :cond_0
    iget v0, p0, Lmiuix/slidingwidget/widget/j;->l:I

    add-int/2addr v0, p1

    iput v0, p0, Lmiuix/slidingwidget/widget/j;->l:I

    iget p1, p0, Lmiuix/slidingwidget/widget/j;->l:I

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->j:I

    if-ge p1, v0, :cond_1

    iput v0, p0, Lmiuix/slidingwidget/widget/j;->l:I

    goto :goto_0

    :cond_1
    iget v0, p0, Lmiuix/slidingwidget/widget/j;->k:I

    if-le p1, v0, :cond_2

    iput v0, p0, Lmiuix/slidingwidget/widget/j;->l:I

    :cond_2
    :goto_0
    iget p1, p0, Lmiuix/slidingwidget/widget/j;->l:I

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->j:I

    if-eq p1, v0, :cond_4

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->k:I

    if-ne p1, v0, :cond_3

    goto :goto_1

    :cond_3
    const/4 p1, 0x0

    goto :goto_2

    :cond_4
    :goto_1
    const/4 p1, 0x1

    :goto_2
    if-eqz p1, :cond_5

    iget-boolean v0, p0, Lmiuix/slidingwidget/widget/j;->u:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    sget v1, Lmiuix/view/d;->F:I

    sget v2, Lmiuix/view/d;->i:I

    invoke-static {v0, v1, v2}, Lmiuix/view/HapticCompat;->a(Landroid/view/View;II)Z

    :cond_5
    iput-boolean p1, p0, Lmiuix/slidingwidget/widget/j;->u:Z

    iget p1, p0, Lmiuix/slidingwidget/widget/j;->l:I

    invoke-virtual {p0, p1}, Lmiuix/slidingwidget/widget/j;->b(I)V

    return-void
.end method

.method private c(Z)V
    .locals 1

    iget-boolean v0, p0, Lmiuix/slidingwidget/widget/j;->A:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->I:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->I:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->a()V

    :cond_0
    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->H:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p1, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lmiuix/slidingwidget/widget/j;->P:F

    :cond_1
    iget-boolean v0, p0, Lmiuix/slidingwidget/widget/j;->A:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->H:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->H:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->a()V

    :cond_2
    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->I:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-nez v0, :cond_3

    if-eqz p1, :cond_3

    const/4 p1, 0x0

    iput p1, p0, Lmiuix/slidingwidget/widget/j;->P:F

    :cond_3
    return-void
.end method

.method static synthetic d(Lmiuix/slidingwidget/widget/j;)F
    .locals 0

    iget p0, p0, Lmiuix/slidingwidget/widget/j;->P:F

    return p0
.end method

.method static synthetic d(Lmiuix/slidingwidget/widget/j;F)F
    .locals 0

    iput p1, p0, Lmiuix/slidingwidget/widget/j;->P:F

    return p1
.end method

.method private d(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->L:Lmiuix/animation/f/j;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    iget-boolean v0, p0, Lmiuix/slidingwidget/widget/j;->A:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->k:I

    goto :goto_0

    :cond_1
    iget v0, p0, Lmiuix/slidingwidget/widget/j;->j:I

    :goto_0
    iput v0, p0, Lmiuix/slidingwidget/widget/j;->l:I

    iget-boolean v0, p0, Lmiuix/slidingwidget/widget/j;->A:Z

    if-eqz v0, :cond_2

    const/16 v0, 0xff

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    iput v0, p0, Lmiuix/slidingwidget/widget/j;->c:I

    :cond_3
    invoke-direct {p0}, Lmiuix/slidingwidget/widget/j;->q()V

    invoke-direct {p0, p1}, Lmiuix/slidingwidget/widget/j;->c(Z)V

    return-void
.end method

.method static synthetic e(Lmiuix/slidingwidget/widget/j;)F
    .locals 0

    iget p0, p0, Lmiuix/slidingwidget/widget/j;->Q:F

    return p0
.end method

.method static synthetic e(Lmiuix/slidingwidget/widget/j;F)F
    .locals 0

    iput p1, p0, Lmiuix/slidingwidget/widget/j;->Q:F

    return p1
.end method

.method static synthetic f(Lmiuix/slidingwidget/widget/j;)I
    .locals 0

    iget p0, p0, Lmiuix/slidingwidget/widget/j;->l:I

    return p0
.end method

.method static synthetic g(Lmiuix/slidingwidget/widget/j;)I
    .locals 0

    iget p0, p0, Lmiuix/slidingwidget/widget/j;->k:I

    return p0
.end method

.method private m()V
    .locals 3

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lmiuix/slidingwidget/widget/j;->b(Z)V

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    sget v1, Lmiuix/view/d;->F:I

    sget v2, Lmiuix/view/d;->i:I

    invoke-static {v0, v1, v2}, Lmiuix/view/HapticCompat;->a(Landroid/view/View;II)Z

    return-void
.end method

.method private n()Landroid/graphics/drawable/StateListDrawable;
    .locals 4

    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    iget v1, p0, Lmiuix/slidingwidget/widget/j;->f:I

    iget v2, p0, Lmiuix/slidingwidget/widget/j;->g:I

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->setBounds(IIII)V

    iget-object v1, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/StateListDrawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    return-object v0
.end method

.method private o()V
    .locals 1

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->C:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->C:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->a()V

    :cond_0
    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->B:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->B:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->e()V

    :cond_1
    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->D:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->D:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->e()V

    :cond_2
    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->K:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->K:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->a()V

    :cond_3
    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->J:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->J:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->e()V

    :cond_4
    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->F:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->F:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->e()V

    :cond_5
    return-void
.end method

.method private p()V
    .locals 1

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->B:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->B:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->a()V

    :cond_0
    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->C:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->C:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->e()V

    :cond_1
    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->D:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->D:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->a()V

    :cond_2
    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->E:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->E:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->e()V

    :cond_3
    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->F:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->F:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->a()V

    :cond_4
    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->J:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->J:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->a()V

    :cond_5
    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->K:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->K:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->e()V

    :cond_6
    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->G:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->G:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->e()V

    :cond_7
    return-void
.end method

.method private q()V
    .locals 1

    iget-boolean v0, p0, Lmiuix/slidingwidget/widget/j;->T:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->U:I

    iput v0, p0, Lmiuix/slidingwidget/widget/j;->l:I

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->V:I

    iput v0, p0, Lmiuix/slidingwidget/widget/j;->c:I

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->X:F

    iput v0, p0, Lmiuix/slidingwidget/widget/j;->P:F

    iget-boolean v0, p0, Lmiuix/slidingwidget/widget/j;->W:Z

    iput-boolean v0, p0, Lmiuix/slidingwidget/widget/j;->A:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/slidingwidget/widget/j;->T:Z

    const/4 v0, -0x1

    iput v0, p0, Lmiuix/slidingwidget/widget/j;->U:I

    iput v0, p0, Lmiuix/slidingwidget/widget/j;->V:I

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lmiuix/slidingwidget/widget/j;->X:F

    :cond_0
    return-void
.end method

.method private r()V
    .locals 1

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->l:I

    iput v0, p0, Lmiuix/slidingwidget/widget/j;->U:I

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->c:I

    iput v0, p0, Lmiuix/slidingwidget/widget/j;->V:I

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->P:F

    iput v0, p0, Lmiuix/slidingwidget/widget/j;->X:F

    iget-boolean v0, p0, Lmiuix/slidingwidget/widget/j;->A:Z

    iput-boolean v0, p0, Lmiuix/slidingwidget/widget/j;->W:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/slidingwidget/widget/j;->T:Z

    return-void
.end method


# virtual methods
.method public a()F
    .locals 1

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->ha:F

    return v0
.end method

.method public a(F)V
    .locals 0

    iput p1, p0, Lmiuix/slidingwidget/widget/j;->ha:F

    return-void
.end method

.method public a(I)V
    .locals 2

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->w:Landroid/graphics/drawable/Drawable;

    instance-of v1, v0, Lmiuix/smooth/SmoothContainerDrawable;

    if-eqz v1, :cond_0

    check-cast v0, Lmiuix/smooth/SmoothContainerDrawable;

    invoke-virtual {v0, p1}, Lmiuix/smooth/SmoothContainerDrawable;->a(I)V

    :cond_0
    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->x:Landroid/graphics/drawable/Drawable;

    instance-of v1, v0, Lmiuix/smooth/SmoothContainerDrawable;

    if-eqz v1, :cond_1

    check-cast v0, Lmiuix/smooth/SmoothContainerDrawable;

    invoke-virtual {v0, p1}, Lmiuix/smooth/SmoothContainerDrawable;->a(I)V

    :cond_1
    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->y:Landroid/graphics/drawable/Drawable;

    instance-of v1, v0, Lmiuix/smooth/SmoothContainerDrawable;

    if-eqz v1, :cond_2

    check-cast v0, Lmiuix/smooth/SmoothContainerDrawable;

    invoke-virtual {v0, p1}, Lmiuix/smooth/SmoothContainerDrawable;->a(I)V

    :cond_2
    return-void
.end method

.method public a(Landroid/content/Context;Landroid/content/res/TypedArray;)V
    .locals 5

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Ld/s/c;->miuix_appcompat_sliding_button_frame_corner_radius:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lmiuix/slidingwidget/widget/j;->ea:I

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Ld/s/c;->miuix_appcompat_sliding_button_mask_vertical_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lmiuix/slidingwidget/widget/j;->fa:I

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Ld/s/c;->miuix_appcompat_sliding_button_mask_horizontal_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lmiuix/slidingwidget/widget/j;->ga:I

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setDrawingCacheEnabled(Z)V

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lmiuix/slidingwidget/widget/j;->q:I

    sget v0, Ld/s/f;->SlidingButton_sliderOn:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lmiuix/slidingwidget/widget/j;->b:Landroid/graphics/drawable/Drawable;

    sget v0, Ld/s/f;->SlidingButton_sliderOff:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lmiuix/slidingwidget/widget/j;->d:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    sget v2, Ld/s/f;->SlidingButton_android_background:I

    invoke-virtual {p2, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/CompoundButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const-string v0, "#FF0D84FF"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-lt v2, v3, :cond_0

    sget v0, Ld/s/b;->miuix_appcompat_sliding_button_bar_on_light:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getColor(I)I

    move-result v0

    :cond_0
    sget p1, Ld/s/f;->SlidingButton_slidingBarColor:I

    invoke-virtual {p2, p1, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p1

    iput p1, p0, Lmiuix/slidingwidget/widget/j;->e:I

    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Ld/s/c;->miuix_appcompat_sliding_button_frame_vertical_padding:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Ld/s/c;->miuix_appcompat_sliding_button_frame_horizontal_padding:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v2, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Ld/s/c;->miuix_appcompat_sliding_button_height:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget-object v3, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {v3}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Ld/s/c;->miuix_appcompat_sliding_button_width:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v3

    iput v0, p0, Lmiuix/slidingwidget/widget/j;->f:I

    mul-int/lit8 p1, p1, 0x2

    add-int/2addr p1, v2

    iput p1, p0, Lmiuix/slidingwidget/widget/j;->g:I

    iget p1, p0, Lmiuix/slidingwidget/widget/j;->f:I

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    iput p1, p0, Lmiuix/slidingwidget/widget/j;->h:I

    iget p1, p0, Lmiuix/slidingwidget/widget/j;->g:I

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    iput p1, p0, Lmiuix/slidingwidget/widget/j;->i:I

    iput v1, p0, Lmiuix/slidingwidget/widget/j;->j:I

    iget p1, p0, Lmiuix/slidingwidget/widget/j;->f:I

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->h:I

    sub-int/2addr p1, v0

    iput p1, p0, Lmiuix/slidingwidget/widget/j;->k:I

    iget p1, p0, Lmiuix/slidingwidget/widget/j;->j:I

    iput p1, p0, Lmiuix/slidingwidget/widget/j;->l:I

    new-instance p1, Landroid/util/TypedValue;

    invoke-direct {p1}, Landroid/util/TypedValue;-><init>()V

    sget v0, Ld/s/f;->SlidingButton_barOff:I

    invoke-virtual {p2, v0, p1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    sget v1, Ld/s/f;->SlidingButton_barOn:I

    invoke-virtual {p2, v1, v0}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    sget v1, Ld/s/f;->SlidingButton_barOff:I

    invoke-virtual {p2, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sget v2, Ld/s/f;->SlidingButton_barOn:I

    invoke-virtual {p2, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    iget v2, p1, Landroid/util/TypedValue;->type:I

    iget v3, v0, Landroid/util/TypedValue;->type:I

    if-ne v2, v3, :cond_1

    iget v2, p1, Landroid/util/TypedValue;->data:I

    iget v3, v0, Landroid/util/TypedValue;->data:I

    if-ne v2, v3, :cond_1

    iget p1, p1, Landroid/util/TypedValue;->resourceId:I

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    if-ne p1, v0, :cond_1

    move-object p2, v1

    :cond_1
    if-eqz p2, :cond_3

    if-eqz v1, :cond_3

    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x15

    if-lt p1, v0, :cond_2

    iget p1, p0, Lmiuix/slidingwidget/widget/j;->e:I

    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    :cond_2
    invoke-direct {p0, p2}, Lmiuix/slidingwidget/widget/j;->b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-direct {p0, v1}, Lmiuix/slidingwidget/widget/j;->b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, p2}, Lmiuix/slidingwidget/widget/j;->b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    invoke-direct {p0, p1, v0, p2}, Lmiuix/slidingwidget/widget/j;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-direct {p0}, Lmiuix/slidingwidget/widget/j;->n()Landroid/graphics/drawable/StateListDrawable;

    move-result-object p1

    iput-object p1, p0, Lmiuix/slidingwidget/widget/j;->t:Landroid/graphics/drawable/StateListDrawable;

    :cond_3
    invoke-virtual {p0}, Lmiuix/slidingwidget/widget/j;->l()V

    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_4

    iget p1, p0, Lmiuix/slidingwidget/widget/j;->k:I

    invoke-virtual {p0, p1}, Lmiuix/slidingwidget/widget/j;->b(I)V

    :cond_4
    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Ld/s/c;->miuix_appcompat_sliding_button_slider_max_offset:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    iput p1, p0, Lmiuix/slidingwidget/widget/j;->ja:I

    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 10

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xff

    goto :goto_0

    :cond_0
    const/16 v0, 0x7f

    :goto_0
    int-to-float v0, v0

    iget v1, p0, Lmiuix/slidingwidget/widget/j;->ha:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    int-to-float v1, v0

    const/high16 v2, 0x437f0000    # 255.0f

    div-float v9, v1, v2

    invoke-direct {p0, p1, v9}, Lmiuix/slidingwidget/widget/j;->a(Landroid/graphics/Canvas;F)V

    iget-object v1, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-static {v1}, Landroidx/appcompat/widget/Fa;->a(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v2, p0, Lmiuix/slidingwidget/widget/j;->f:I

    iget v3, p0, Lmiuix/slidingwidget/widget/j;->l:I

    sub-int/2addr v2, v3

    iget v3, p0, Lmiuix/slidingwidget/widget/j;->h:I

    sub-int/2addr v2, v3

    goto :goto_1

    :cond_1
    iget v2, p0, Lmiuix/slidingwidget/widget/j;->l:I

    :goto_1
    iget-object v3, p0, Lmiuix/slidingwidget/widget/j;->ia:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    float-to-int v3, v3

    add-int v5, v2, v3

    if-eqz v1, :cond_2

    iget v1, p0, Lmiuix/slidingwidget/widget/j;->f:I

    iget v2, p0, Lmiuix/slidingwidget/widget/j;->l:I

    sub-int/2addr v1, v2

    goto :goto_2

    :cond_2
    iget v1, p0, Lmiuix/slidingwidget/widget/j;->h:I

    iget v2, p0, Lmiuix/slidingwidget/widget/j;->l:I

    add-int/2addr v1, v2

    :goto_2
    iget-object v2, p0, Lmiuix/slidingwidget/widget/j;->ia:[F

    aget v3, v2, v4

    float-to-int v3, v3

    add-int v7, v1, v3

    iget v1, p0, Lmiuix/slidingwidget/widget/j;->g:I

    iget v3, p0, Lmiuix/slidingwidget/widget/j;->i:I

    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    const/4 v4, 0x1

    aget v2, v2, v4

    float-to-int v2, v2

    add-int v6, v1, v2

    add-int v8, v6, v3

    add-int v1, v7, v5

    div-int/lit8 v1, v1, 0x2

    add-int v2, v8, v6

    div-int/lit8 v2, v2, 0x2

    invoke-direct {p0, p1, v1, v2}, Lmiuix/slidingwidget/widget/j;->a(Landroid/graphics/Canvas;II)V

    invoke-direct {p0, p1, v1, v2}, Lmiuix/slidingwidget/widget/j;->b(Landroid/graphics/Canvas;II)V

    iget-boolean v1, p0, Lmiuix/slidingwidget/widget/j;->A:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lmiuix/slidingwidget/widget/j;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_3

    :cond_3
    iget-object v1, p0, Lmiuix/slidingwidget/widget/j;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :goto_3
    move-object v3, p0

    move-object v4, p1

    invoke-direct/range {v3 .. v9}, Lmiuix/slidingwidget/widget/j;->a(Landroid/graphics/Canvas;IIIIF)V

    invoke-direct {p0, p1}, Lmiuix/slidingwidget/widget/j;->b(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public a(Landroid/view/MotionEvent;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x7

    if-eq v0, v1, :cond_4

    const/16 p1, 0x9

    if-eq v0, p1, :cond_2

    const/16 p1, 0xa

    if-eq v0, p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->ia:[F

    const/4 v0, 0x0

    const/4 v1, 0x0

    aput v1, p1, v0

    const/4 v0, 0x1

    aput v1, p1, v0

    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->B:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->d()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->B:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->a()V

    :cond_1
    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->C:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/j;->e()V

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->C:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->d()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->C:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->a()V

    :cond_3
    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->B:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/j;->e()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-direct {p0, v0, p1}, Lmiuix/slidingwidget/widget/j;->a(Landroid/view/View;Landroid/view/MotionEvent;)[F

    move-result-object p1

    iput-object p1, p0, Lmiuix/slidingwidget/widget/j;->ia:[F

    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->invalidate()V

    :goto_0
    return-void
.end method

.method public a(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 0

    iput-object p1, p0, Lmiuix/slidingwidget/widget/j;->r:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    return-void
.end method

.method public synthetic a(Lmiuix/animation/f/e;FF)V
    .locals 0

    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->invalidate()V

    return-void
.end method

.method public a(Z)V
    .locals 1

    invoke-direct {p0}, Lmiuix/slidingwidget/widget/j;->r()V

    iput-boolean p1, p0, Lmiuix/slidingwidget/widget/j;->A:Z

    if-eqz p1, :cond_0

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->k:I

    goto :goto_0

    :cond_0
    iget v0, p0, Lmiuix/slidingwidget/widget/j;->j:I

    :goto_0
    iput v0, p0, Lmiuix/slidingwidget/widget/j;->l:I

    if-eqz p1, :cond_1

    const/16 v0, 0xff

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    iput v0, p0, Lmiuix/slidingwidget/widget/j;->c:I

    if-eqz p1, :cond_2

    const/high16 p1, 0x3f800000    # 1.0f

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    :goto_2
    iput p1, p0, Lmiuix/slidingwidget/widget/j;->P:F

    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->L:Lmiuix/animation/f/j;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lmiuix/animation/f/e;->d()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->L:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->a()V

    :cond_3
    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->I:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->d()Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->I:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->a()V

    :cond_4
    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->H:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->d()Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->H:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->a()V

    :cond_5
    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->invalidate()V

    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->t:Landroid/graphics/drawable/StateListDrawable;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->g:I

    return v0
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lmiuix/slidingwidget/widget/j;->l:I

    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->invalidate()V

    return-void
.end method

.method public b(Landroid/view/MotionEvent;)V
    .locals 7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    float-to-int p1, p1

    iget-object v2, p0, Lmiuix/slidingwidget/widget/j;->s:Landroid/graphics/Rect;

    iget-object v3, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-static {v3}, Landroidx/appcompat/widget/Fa;->a(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget v4, p0, Lmiuix/slidingwidget/widget/j;->f:I

    iget v5, p0, Lmiuix/slidingwidget/widget/j;->l:I

    sub-int/2addr v4, v5

    iget v5, p0, Lmiuix/slidingwidget/widget/j;->h:I

    sub-int/2addr v4, v5

    goto :goto_0

    :cond_0
    iget v4, p0, Lmiuix/slidingwidget/widget/j;->l:I

    :goto_0
    if-eqz v3, :cond_1

    iget v3, p0, Lmiuix/slidingwidget/widget/j;->f:I

    iget v5, p0, Lmiuix/slidingwidget/widget/j;->l:I

    sub-int/2addr v3, v5

    goto :goto_1

    :cond_1
    iget v3, p0, Lmiuix/slidingwidget/widget/j;->l:I

    iget v5, p0, Lmiuix/slidingwidget/widget/j;->h:I

    add-int/2addr v3, v5

    :goto_1
    iget v5, p0, Lmiuix/slidingwidget/widget/j;->g:I

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v6, v3, v5}, Landroid/graphics/Rect;->set(IIII)V

    const/4 v3, 0x1

    if-eqz v0, :cond_b

    const/4 v4, 0x2

    if-eq v0, v3, :cond_6

    if-eq v0, v4, :cond_5

    const/4 p1, 0x3

    if-eq v0, p1, :cond_2

    goto/16 :goto_7

    :cond_2
    invoke-direct {p0}, Lmiuix/slidingwidget/widget/j;->p()V

    iget-boolean p1, p0, Lmiuix/slidingwidget/widget/j;->o:Z

    if-eqz p1, :cond_4

    iget p1, p0, Lmiuix/slidingwidget/widget/j;->l:I

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->k:I

    div-int/2addr v0, v4

    if-lt p1, v0, :cond_3

    goto :goto_2

    :cond_3
    move v3, v6

    :goto_2
    iput-boolean v3, p0, Lmiuix/slidingwidget/widget/j;->A:Z

    iget-boolean p1, p0, Lmiuix/slidingwidget/widget/j;->A:Z

    invoke-direct {p0, p1}, Lmiuix/slidingwidget/widget/j;->b(Z)V

    :cond_4
    iput-boolean v6, p0, Lmiuix/slidingwidget/widget/j;->o:Z

    iput-boolean v6, p0, Lmiuix/slidingwidget/widget/j;->p:Z

    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {p1, v6}, Landroid/widget/CompoundButton;->setPressed(Z)V

    goto/16 :goto_7

    :cond_5
    iget-boolean p1, p0, Lmiuix/slidingwidget/widget/j;->o:Z

    if-eqz p1, :cond_f

    iget p1, p0, Lmiuix/slidingwidget/widget/j;->m:I

    sub-int p1, v1, p1

    invoke-direct {p0, p1}, Lmiuix/slidingwidget/widget/j;->c(I)V

    iput v1, p0, Lmiuix/slidingwidget/widget/j;->m:I

    iget p1, p0, Lmiuix/slidingwidget/widget/j;->n:I

    sub-int/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result p1

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->q:I

    if-lt p1, v0, :cond_f

    iput-boolean v3, p0, Lmiuix/slidingwidget/widget/j;->p:Z

    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    invoke-interface {p1, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto/16 :goto_7

    :cond_6
    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v6}, Landroid/widget/CompoundButton;->playSoundEffect(I)V

    invoke-direct {p0}, Lmiuix/slidingwidget/widget/j;->p()V

    iget-boolean v0, p0, Lmiuix/slidingwidget/widget/j;->o:Z

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Lmiuix/slidingwidget/widget/j;->p:Z

    if-nez v0, :cond_7

    invoke-direct {p0}, Lmiuix/slidingwidget/widget/j;->m()V

    goto :goto_4

    :cond_7
    iget v0, p0, Lmiuix/slidingwidget/widget/j;->l:I

    iget v5, p0, Lmiuix/slidingwidget/widget/j;->k:I

    div-int/2addr v5, v4

    if-lt v0, v5, :cond_8

    goto :goto_3

    :cond_8
    move v3, v6

    :goto_3
    iput-boolean v3, p0, Lmiuix/slidingwidget/widget/j;->A:Z

    iget-boolean v0, p0, Lmiuix/slidingwidget/widget/j;->A:Z

    invoke-direct {p0, v0}, Lmiuix/slidingwidget/widget/j;->b(Z)V

    invoke-virtual {v2, v1, p1}, Landroid/graphics/Rect;->contains(II)Z

    move-result p1

    if-eqz p1, :cond_a

    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    sget v0, Lmiuix/view/d;->F:I

    sget v1, Lmiuix/view/d;->i:I

    invoke-static {p1, v0, v1}, Lmiuix/view/HapticCompat;->a(Landroid/view/View;II)Z

    goto :goto_4

    :cond_9
    invoke-direct {p0}, Lmiuix/slidingwidget/widget/j;->m()V

    :cond_a
    :goto_4
    iput-boolean v6, p0, Lmiuix/slidingwidget/widget/j;->o:Z

    iput-boolean v6, p0, Lmiuix/slidingwidget/widget/j;->p:Z

    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {p1, v6}, Landroid/widget/CompoundButton;->setPressed(Z)V

    goto :goto_7

    :cond_b
    invoke-virtual {v2, v1, p1}, Landroid/graphics/Rect;->contains(II)Z

    move-result p1

    if-eqz p1, :cond_e

    iput-boolean v3, p0, Lmiuix/slidingwidget/widget/j;->o:Z

    iget-object p1, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {p1, v3}, Landroid/widget/CompoundButton;->setPressed(Z)V

    invoke-direct {p0}, Lmiuix/slidingwidget/widget/j;->o()V

    iget p1, p0, Lmiuix/slidingwidget/widget/j;->l:I

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->j:I

    if-le p1, v0, :cond_d

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->k:I

    if-lt p1, v0, :cond_c

    goto :goto_5

    :cond_c
    move v3, v6

    :cond_d
    :goto_5
    iput-boolean v3, p0, Lmiuix/slidingwidget/widget/j;->u:Z

    goto :goto_6

    :cond_e
    iput-boolean v6, p0, Lmiuix/slidingwidget/widget/j;->o:Z

    :goto_6
    iput v1, p0, Lmiuix/slidingwidget/widget/j;->m:I

    iput v1, p0, Lmiuix/slidingwidget/widget/j;->n:I

    iput-boolean v6, p0, Lmiuix/slidingwidget/widget/j;->p:Z

    :cond_f
    :goto_7
    return-void
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->f:I

    return v0
.end method

.method public d()Landroid/graphics/drawable/StateListDrawable;
    .locals 1

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->t:Landroid/graphics/drawable/StateListDrawable;

    return-object v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lmiuix/slidingwidget/widget/j;->l:I

    return v0
.end method

.method public f()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->b:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public g()V
    .locals 9

    new-instance v0, Lmiuix/animation/f/j;

    iget-object v1, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    iget-object v2, p0, Lmiuix/slidingwidget/widget/j;->Y:Lmiuix/animation/g/b;

    const v3, 0x3fce147b    # 1.61f

    invoke-direct {v0, v1, v2, v3}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/slidingwidget/widget/j;->B:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->B:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    const v1, 0x4476bd71

    invoke-virtual {v0, v1}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->B:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    const v2, 0x3f19999a    # 0.6f

    invoke-virtual {v0, v2}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->B:Lmiuix/animation/f/j;

    const v3, 0x3b03126f    # 0.002f

    invoke-virtual {v0, v3}, Lmiuix/animation/f/e;->a(F)Lmiuix/animation/f/e;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->B:Lmiuix/animation/f/j;

    iget-object v4, p0, Lmiuix/slidingwidget/widget/j;->Z:Lmiuix/animation/f/e$c;

    invoke-virtual {v0, v4}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    new-instance v0, Lmiuix/animation/f/j;

    iget-object v4, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    iget-object v5, p0, Lmiuix/slidingwidget/widget/j;->Y:Lmiuix/animation/g/b;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-direct {v0, v4, v5, v6}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/slidingwidget/widget/j;->C:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->C:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->C:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->C:Lmiuix/animation/f/j;

    invoke-virtual {v0, v3}, Lmiuix/animation/f/e;->a(F)Lmiuix/animation/f/e;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->C:Lmiuix/animation/f/j;

    iget-object v2, p0, Lmiuix/slidingwidget/widget/j;->Z:Lmiuix/animation/f/e$c;

    invoke-virtual {v0, v2}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    new-instance v0, Lmiuix/animation/f/j;

    iget-object v2, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    iget-object v3, p0, Lmiuix/slidingwidget/widget/j;->aa:Lmiuix/animation/g/b;

    invoke-direct {v0, v2, v3, v6}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/slidingwidget/widget/j;->D:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->D:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->D:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    const v2, 0x3f7d70a4    # 0.99f

    invoke-virtual {v0, v2}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->D:Lmiuix/animation/f/j;

    const/high16 v3, 0x3b800000    # 0.00390625f

    invoke-virtual {v0, v3}, Lmiuix/animation/f/e;->a(F)Lmiuix/animation/f/e;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->D:Lmiuix/animation/f/j;

    iget-object v4, p0, Lmiuix/slidingwidget/widget/j;->Z:Lmiuix/animation/f/e$c;

    invoke-virtual {v0, v4}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    new-instance v0, Lmiuix/animation/f/j;

    iget-object v4, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    iget-object v5, p0, Lmiuix/slidingwidget/widget/j;->aa:Lmiuix/animation/g/b;

    const/4 v7, 0x0

    invoke-direct {v0, v4, v5, v7}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/slidingwidget/widget/j;->E:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->E:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->E:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->E:Lmiuix/animation/f/j;

    invoke-virtual {v0, v3}, Lmiuix/animation/f/e;->a(F)Lmiuix/animation/f/e;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->E:Lmiuix/animation/f/j;

    iget-object v4, p0, Lmiuix/slidingwidget/widget/j;->Z:Lmiuix/animation/f/e$c;

    invoke-virtual {v0, v4}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    new-instance v0, Lmiuix/animation/f/j;

    iget-object v4, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    iget-object v5, p0, Lmiuix/slidingwidget/widget/j;->ba:Lmiuix/animation/g/b;

    const v8, 0x3e19999a    # 0.15f

    invoke-direct {v0, v4, v5, v8}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/slidingwidget/widget/j;->F:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->F:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->F:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->F:Lmiuix/animation/f/j;

    invoke-virtual {v0, v3}, Lmiuix/animation/f/e;->a(F)Lmiuix/animation/f/e;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->F:Lmiuix/animation/f/j;

    iget-object v4, p0, Lmiuix/slidingwidget/widget/j;->Z:Lmiuix/animation/f/e$c;

    invoke-virtual {v0, v4}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    new-instance v0, Lmiuix/animation/f/j;

    iget-object v4, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    iget-object v5, p0, Lmiuix/slidingwidget/widget/j;->ba:Lmiuix/animation/g/b;

    const v8, 0x3dcccccd    # 0.1f

    invoke-direct {v0, v4, v5, v8}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/slidingwidget/widget/j;->G:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->G:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->G:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->G:Lmiuix/animation/f/j;

    invoke-virtual {v0, v3}, Lmiuix/animation/f/e;->a(F)Lmiuix/animation/f/e;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->G:Lmiuix/animation/f/j;

    iget-object v4, p0, Lmiuix/slidingwidget/widget/j;->Z:Lmiuix/animation/f/e$c;

    invoke-virtual {v0, v4}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    new-instance v0, Lmiuix/animation/f/j;

    iget-object v4, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    iget-object v5, p0, Lmiuix/slidingwidget/widget/j;->ca:Lmiuix/animation/g/b;

    invoke-direct {v0, v4, v5, v6}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/slidingwidget/widget/j;->H:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->H:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    const v4, 0x43db51ec

    invoke-virtual {v0, v4}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->H:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->H:Lmiuix/animation/f/j;

    invoke-virtual {v0, v3}, Lmiuix/animation/f/e;->a(F)Lmiuix/animation/f/e;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->H:Lmiuix/animation/f/j;

    iget-object v4, p0, Lmiuix/slidingwidget/widget/j;->Z:Lmiuix/animation/f/e$c;

    invoke-virtual {v0, v4}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    new-instance v0, Lmiuix/animation/f/j;

    iget-object v4, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    iget-object v5, p0, Lmiuix/slidingwidget/widget/j;->ca:Lmiuix/animation/g/b;

    invoke-direct {v0, v4, v5, v7}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/slidingwidget/widget/j;->I:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->I:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->I:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->I:Lmiuix/animation/f/j;

    invoke-virtual {v0, v3}, Lmiuix/animation/f/e;->a(F)Lmiuix/animation/f/e;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->I:Lmiuix/animation/f/j;

    iget-object v4, p0, Lmiuix/slidingwidget/widget/j;->Z:Lmiuix/animation/f/e$c;

    invoke-virtual {v0, v4}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    new-instance v0, Lmiuix/animation/f/j;

    iget-object v4, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    iget-object v5, p0, Lmiuix/slidingwidget/widget/j;->da:Lmiuix/animation/g/b;

    const v6, 0x3d4ccccd    # 0.05f

    invoke-direct {v0, v4, v5, v6}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/slidingwidget/widget/j;->J:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->J:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->J:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->J:Lmiuix/animation/f/j;

    invoke-virtual {v0, v3}, Lmiuix/animation/f/e;->a(F)Lmiuix/animation/f/e;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->J:Lmiuix/animation/f/j;

    iget-object v4, p0, Lmiuix/slidingwidget/widget/j;->Z:Lmiuix/animation/f/e$c;

    invoke-virtual {v0, v4}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    new-instance v0, Lmiuix/animation/f/j;

    iget-object v4, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    iget-object v5, p0, Lmiuix/slidingwidget/widget/j;->da:Lmiuix/animation/g/b;

    invoke-direct {v0, v4, v5, v7}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/slidingwidget/widget/j;->K:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->K:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->K:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->K:Lmiuix/animation/f/j;

    invoke-virtual {v0, v3}, Lmiuix/animation/f/e;->a(F)Lmiuix/animation/f/e;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->K:Lmiuix/animation/f/j;

    iget-object v1, p0, Lmiuix/slidingwidget/widget/j;->Z:Lmiuix/animation/f/e$c;

    invoke-virtual {v0, v1}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    return-void
.end method

.method public h()V
    .locals 2

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Ld/s/d;->miuix_appcompat_sliding_btn_slider_shadow:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lmiuix/slidingwidget/widget/j;->R:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Ld/s/d;->miuix_appcompat_sliding_btn_slider_stroke_light:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lmiuix/slidingwidget/widget/j;->S:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public i()V
    .locals 1

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->t:Landroid/graphics/drawable/StateListDrawable;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable;->jumpToCurrentState()V

    :cond_0
    return-void
.end method

.method public j()V
    .locals 3

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->r:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    iget-object v1, p0, Lmiuix/slidingwidget/widget/j;->r:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    iget-object v2, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-interface {v1, v2, v0}, Landroid/widget/CompoundButton$OnCheckedChangeListener;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    :cond_0
    return-void
.end method

.method public k()V
    .locals 2

    iget-object v0, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    :cond_0
    return-void
.end method

.method public l()V
    .locals 2

    invoke-virtual {p0}, Lmiuix/slidingwidget/widget/j;->f()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmiuix/slidingwidget/widget/j;->f()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    invoke-virtual {p0}, Lmiuix/slidingwidget/widget/j;->d()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    iget-object v1, p0, Lmiuix/slidingwidget/widget/j;->z:Landroid/widget/CompoundButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    :cond_0
    return-void
.end method
