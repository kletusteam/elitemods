.class public Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;
.super Lmiuix/androidbasewidget/internal/view/SeekBarGradientDrawable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable$a;
    }
.end annotation


# static fields
.field private static d:Landroid/graphics/drawable/Drawable;


# instance fields
.field private e:Lmiuix/animation/f/j;

.field private f:Lmiuix/animation/f/j;

.field private g:Lmiuix/animation/f/j;

.field private h:Lmiuix/animation/f/j;

.field private i:F

.field private j:F

.field private k:Lmiuix/animation/g/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiuix/animation/g/b<",
            "Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lmiuix/animation/f/e$c;

.field private m:Lmiuix/animation/g/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiuix/animation/g/b<",
            "Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lmiuix/androidbasewidget/internal/view/SeekBarGradientDrawable;-><init>()V

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->i:F

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->j:F

    new-instance v0, Lmiuix/androidbasewidget/internal/view/c;

    const-string v1, "ShadowAlpha"

    invoke-direct {v0, p0, v1}, Lmiuix/androidbasewidget/internal/view/c;-><init>(Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;Ljava/lang/String;)V

    iput-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->k:Lmiuix/animation/g/b;

    new-instance v0, Lmiuix/androidbasewidget/internal/view/a;

    invoke-direct {v0, p0}, Lmiuix/androidbasewidget/internal/view/a;-><init>(Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;)V

    iput-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->l:Lmiuix/animation/f/e$c;

    new-instance v0, Lmiuix/androidbasewidget/internal/view/d;

    const-string v1, "Scale"

    invoke-direct {v0, p0, v1}, Lmiuix/androidbasewidget/internal/view/d;-><init>(Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;Ljava/lang/String;)V

    iput-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->m:Lmiuix/animation/g/b;

    invoke-direct {p0}, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->f()V

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;Lmiuix/androidbasewidget/internal/view/SeekBarGradientDrawable$a;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lmiuix/androidbasewidget/internal/view/SeekBarGradientDrawable;-><init>(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;Lmiuix/androidbasewidget/internal/view/SeekBarGradientDrawable$a;)V

    const/high16 p2, 0x3f800000    # 1.0f

    iput p2, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->i:F

    const/4 p2, 0x0

    iput p2, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->j:F

    new-instance p2, Lmiuix/androidbasewidget/internal/view/c;

    const-string p3, "ShadowAlpha"

    invoke-direct {p2, p0, p3}, Lmiuix/androidbasewidget/internal/view/c;-><init>(Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;Ljava/lang/String;)V

    iput-object p2, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->k:Lmiuix/animation/g/b;

    new-instance p2, Lmiuix/androidbasewidget/internal/view/a;

    invoke-direct {p2, p0}, Lmiuix/androidbasewidget/internal/view/a;-><init>(Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;)V

    iput-object p2, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->l:Lmiuix/animation/f/e$c;

    new-instance p2, Lmiuix/androidbasewidget/internal/view/d;

    const-string p3, "Scale"

    invoke-direct {p2, p0, p3}, Lmiuix/androidbasewidget/internal/view/d;-><init>(Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;Ljava/lang/String;)V

    iput-object p2, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->m:Lmiuix/animation/g/b;

    invoke-direct {p0}, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->f()V

    if-eqz p1, :cond_0

    sget-object p2, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->d:Landroid/graphics/drawable/Drawable;

    if-nez p2, :cond_0

    sget p2, Ld/a/d;->miuix_appcompat_sliding_btn_slider_shadow:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    sput-object p1, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->d:Landroid/graphics/drawable/Drawable;

    :cond_0
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 7

    invoke-virtual {p0}, Landroid/graphics/drawable/GradientDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    sget-object v1, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    sget-object v2, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {p0}, Lmiuix/androidbasewidget/internal/view/SeekBarGradientDrawable;->getIntrinsicWidth()I

    move-result v3

    sub-int/2addr v1, v3

    invoke-virtual {p0}, Lmiuix/androidbasewidget/internal/view/SeekBarGradientDrawable;->getIntrinsicHeight()I

    move-result v3

    sub-int/2addr v2, v3

    sget-object v3, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->d:Landroid/graphics/drawable/Drawable;

    iget v4, v0, Landroid/graphics/Rect;->left:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v4, v1

    iget v5, v0, Landroid/graphics/Rect;->top:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v5, v2

    iget v6, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v6, v1

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v2

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    sget-object v0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->d:Landroid/graphics/drawable/Drawable;

    const/high16 v1, 0x437f0000    # 255.0f

    iget v2, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->j:F

    mul-float/2addr v2, v1

    float-to-int v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    sget-object v0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method private f()V
    .locals 6

    new-instance v0, Lmiuix/animation/f/j;

    iget-object v1, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->m:Lmiuix/animation/g/b;

    const v2, 0x404c28f6    # 3.19f

    invoke-direct {v0, p0, v1, v2}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->e:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->e:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    const v1, 0x4476bd71

    invoke-virtual {v0, v1}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->e:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    const v2, 0x3f333333    # 0.7f

    invoke-virtual {v0, v2}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->e:Lmiuix/animation/f/j;

    const v2, 0x3b03126f    # 0.002f

    invoke-virtual {v0, v2}, Lmiuix/animation/f/e;->a(F)Lmiuix/animation/f/e;

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->e:Lmiuix/animation/f/j;

    iget-object v3, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->l:Lmiuix/animation/f/e$c;

    invoke-virtual {v0, v3}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    new-instance v0, Lmiuix/animation/f/j;

    iget-object v3, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->m:Lmiuix/animation/g/b;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v0, p0, v3, v4}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->f:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->f:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->f:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    const v3, 0x3f4ccccd    # 0.8f

    invoke-virtual {v0, v3}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->f:Lmiuix/animation/f/j;

    invoke-virtual {v0, v2}, Lmiuix/animation/f/e;->a(F)Lmiuix/animation/f/e;

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->f:Lmiuix/animation/f/j;

    iget-object v2, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->l:Lmiuix/animation/f/e$c;

    invoke-virtual {v0, v2}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    new-instance v0, Lmiuix/animation/f/j;

    iget-object v2, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->k:Lmiuix/animation/g/b;

    invoke-direct {v0, p0, v2, v4}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->g:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->g:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->g:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    const v2, 0x3f7d70a4    # 0.99f

    invoke-virtual {v0, v2}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->g:Lmiuix/animation/f/j;

    const/high16 v3, 0x3b800000    # 0.00390625f

    invoke-virtual {v0, v3}, Lmiuix/animation/f/e;->a(F)Lmiuix/animation/f/e;

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->g:Lmiuix/animation/f/j;

    iget-object v4, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->l:Lmiuix/animation/f/e$c;

    invoke-virtual {v0, v4}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    new-instance v0, Lmiuix/animation/f/j;

    iget-object v4, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->k:Lmiuix/animation/g/b;

    const/4 v5, 0x0

    invoke-direct {v0, p0, v4, v5}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->h:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->h:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->h:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->h:Lmiuix/animation/f/j;

    invoke-virtual {v0, v3}, Lmiuix/animation/f/e;->a(F)Lmiuix/animation/f/e;

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->h:Lmiuix/animation/f/j;

    iget-object v1, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->l:Lmiuix/animation/f/e$c;

    invoke-virtual {v0, v1}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    return-void
.end method


# virtual methods
.method protected a()Lmiuix/androidbasewidget/internal/view/SeekBarGradientDrawable$a;
    .locals 1

    new-instance v0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable$a;

    invoke-direct {v0}, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable$a;-><init>()V

    return-object v0
.end method

.method public a(F)V
    .locals 0

    iput p1, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->i:F

    return-void
.end method

.method public synthetic a(Lmiuix/animation/f/e;FF)V
    .locals 0

    invoke-virtual {p0}, Landroid/graphics/drawable/GradientDrawable;->invalidateSelf()V

    return-void
.end method

.method protected b()V
    .locals 1

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->f:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->f:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->a()V

    :cond_0
    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->e:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->e:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->e()V

    :cond_1
    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->h:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->h:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->a()V

    :cond_2
    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->g:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->g:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->e()V

    :cond_3
    return-void
.end method

.method public b(F)V
    .locals 0

    iput p1, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->j:F

    return-void
.end method

.method protected c()V
    .locals 1

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->e:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->e:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->a()V

    :cond_0
    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->f:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->f:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->e()V

    :cond_1
    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->g:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->g:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->a()V

    :cond_2
    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->h:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/e;->d()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->h:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->e()V

    :cond_3
    return-void
.end method

.method public d()F
    .locals 1

    iget v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->i:F

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 3

    invoke-virtual {p0}, Landroid/graphics/drawable/GradientDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget v2, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    iget v2, v0, Landroid/graphics/Rect;->top:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v0

    div-int/lit8 v2, v2, 0x2

    invoke-direct {p0, p1}, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->a(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->i:F

    int-to-float v1, v1

    int-to-float v2, v2

    invoke-virtual {p1, v0, v0, v1, v2}, Landroid/graphics/Canvas;->scale(FFFF)V

    invoke-super {p0, p1}, Landroid/graphics/drawable/GradientDrawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method public e()F
    .locals 1

    iget v0, p0, Lmiuix/androidbasewidget/internal/view/SeekBaThumbShapeDrawable;->j:F

    return v0
.end method
