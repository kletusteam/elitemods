.class Lmiuix/autodensity/a;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/ComponentCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiuix/autodensity/b;->a(Landroid/app/Application;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/app/Application;


# direct methods
.method constructor <init>(Landroid/app/Application;)V
    .locals 0

    iput-object p1, p0, Lmiuix/autodensity/a;->a:Landroid/app/Application;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    invoke-static {}, Lmiuix/autodensity/e;->a()Lmiuix/autodensity/e;

    move-result-object v0

    iget-object v1, p0, Lmiuix/autodensity/a;->a:Landroid/app/Application;

    invoke-virtual {v0, v1, p1}, Lmiuix/autodensity/e;->a(Landroid/content/Context;Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lmiuix/autodensity/a;->a:Landroid/app/Application;

    invoke-static {v0}, Lmiuix/autodensity/b;->a(Landroid/app/Application;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/autodensity/a;->a:Landroid/app/Application;

    invoke-static {v0}, Lmiuix/autodensity/f;->a(Landroid/content/Context;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-le v0, v1, :cond_0

    invoke-static {}, Lmiuix/autodensity/e;->a()Lmiuix/autodensity/e;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/autodensity/e;->b()Lmiuix/autodensity/d;

    move-result-object v0

    iget v0, v0, Lmiuix/autodensity/d;->b:I

    iput v0, p1, Landroid/content/res/Configuration;->densityDpi:I

    :cond_0
    return-void
.end method

.method public onLowMemory()V
    .locals 0

    return-void
.end method
