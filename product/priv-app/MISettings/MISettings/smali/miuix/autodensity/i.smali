.class public Lmiuix/autodensity/i;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static c:F

.field private static d:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    const-string v1, "ro.miui.density.primaryscale"

    invoke-static {v1, v0}, Lmiuix/core/util/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lmiuix/autodensity/i;->a:Ljava/lang/String;

    const-string v1, "ro.miui.density.secondaryscale"

    invoke-static {v1, v0}, Lmiuix/core/util/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmiuix/autodensity/i;->b:Ljava/lang/String;

    const/4 v0, 0x0

    sput v0, Lmiuix/autodensity/i;->c:F

    sput v0, Lmiuix/autodensity/i;->d:F

    sget-object v1, Lmiuix/autodensity/i;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lmiuix/autodensity/i;->a:Ljava/lang/String;

    invoke-static {v1}, Lmiuix/autodensity/i;->a(Ljava/lang/String;)F

    move-result v1

    sput v1, Lmiuix/autodensity/i;->c:F

    :cond_0
    sget-object v1, Lmiuix/autodensity/i;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lmiuix/autodensity/i;->b:Ljava/lang/String;

    invoke-static {v1}, Lmiuix/autodensity/i;->a(Ljava/lang/String;)F

    move-result v1

    sput v1, Lmiuix/autodensity/i;->d:F

    :cond_1
    sget v1, Lmiuix/autodensity/i;->d:F

    cmpl-float v0, v1, v0

    if-nez v0, :cond_2

    sget v0, Lmiuix/autodensity/i;->c:F

    sput v0, Lmiuix/autodensity/i;->d:F

    :cond_2
    return-void
.end method

.method static a(Landroid/content/Context;)F
    .locals 2

    sget v0, Lmiuix/autodensity/i;->c:F

    invoke-static {}, Ld/h/a/e;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Ld/h/a/e;->a(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_0

    sget v0, Lmiuix/autodensity/i;->d:F

    :cond_0
    return v0
.end method

.method private static a(Ljava/lang/String;)F
    .locals 2

    :try_start_0
    invoke-static {p0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result p0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    const-string v0, "AutoDensity"

    const-string v1, "catch error: sku scale is not a number"

    invoke-static {v0, v1, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static a()Z
    .locals 2

    sget v0, Lmiuix/autodensity/i;->c:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
