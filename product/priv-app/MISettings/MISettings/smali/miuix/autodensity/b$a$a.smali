.class Lmiuix/autodensity/b$a$a;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/hardware/display/DisplayManager$DisplayListener;
.implements Landroid/content/ComponentCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/autodensity/b$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lmiuix/autodensity/b$a;


# direct methods
.method constructor <init>(Lmiuix/autodensity/b$a;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lmiuix/autodensity/b$a$a;->b:Lmiuix/autodensity/b$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    iput-object p1, p0, Lmiuix/autodensity/b$a$a;->a:Ljava/lang/ref/WeakReference;

    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lmiuix/autodensity/b$a$a;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method private a(Landroid/app/Activity;)V
    .locals 2

    :try_start_0
    const-class v0, Landroid/app/Activity;

    const-string v1, "mCurrentConfig"

    invoke-static {v0, p1, v1}, Ld/q/a;->a(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Configuration;

    invoke-static {}, Lmiuix/autodensity/e;->a()Lmiuix/autodensity/e;

    move-result-object v1

    invoke-virtual {v1}, Lmiuix/autodensity/e;->b()Lmiuix/autodensity/d;

    move-result-object v1

    iget v1, v1, Lmiuix/autodensity/d;->b:I

    iput v1, v0, Landroid/content/res/Configuration;->densityDpi:I

    const-class v0, Landroid/app/Activity;

    const-string v1, "mActivityInfo"

    invoke-static {v0, p1, v1}, Ld/q/a;->a(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ActivityInfo;

    iget v1, v0, Landroid/content/pm/ActivityInfo;->configChanges:I

    and-int/lit16 v1, v1, 0x1000

    if-nez v1, :cond_0

    iget v1, v0, Landroid/content/pm/ActivityInfo;->configChanges:I

    or-int/lit16 v1, v1, 0x1000

    iput v1, v0, Landroid/content/pm/ActivityInfo;->configChanges:I

    iget-object v0, p0, Lmiuix/autodensity/b$a$a;->b:Lmiuix/autodensity/b$a;

    invoke-static {v0, p1}, Lmiuix/autodensity/b$a;->a(Lmiuix/autodensity/b$a;Landroid/app/Activity;)Landroid/app/Fragment;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Lmiuix/autodensity/ConfigurationChangeFragment;

    invoke-virtual {p1}, Lmiuix/autodensity/ConfigurationChangeFragment;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method private b(Landroid/app/Activity;)V
    .locals 3

    :try_start_0
    const-class v0, Landroid/app/Activity;

    const-string v1, "mCurrentConfig"

    const/4 v2, 0x0

    invoke-static {v0, p1, v1, v2}, Ld/q/a;->a(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    :goto_1
    goto/32 :goto_3

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_0

    nop

    :goto_3
    return-void

    :goto_4
    iget-object v0, p0, Lmiuix/autodensity/b$a$a;->a:Ljava/lang/ref/WeakReference;

    goto/32 :goto_2

    nop
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    iget-object p1, p0, Lmiuix/autodensity/b$a$a;->a:Ljava/lang/ref/WeakReference;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_3

    invoke-static {p1}, Lmiuix/autodensity/f;->a(Landroid/content/Context;)V

    invoke-static {p1}, Lmiuix/core/util/screenutils/b;->b(Landroid/content/Context;)I

    move-result v0

    invoke-static {v0}, Lmiuix/core/util/screenutils/b;->a(I)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v0}, Lmiuix/core/util/screenutils/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-le v0, v1, :cond_3

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1f

    if-gt v0, v1, :cond_2

    invoke-direct {p0, p1}, Lmiuix/autodensity/b$a$a;->b(Landroid/app/Activity;)V

    goto :goto_1

    :cond_2
    invoke-direct {p0, p1}, Lmiuix/autodensity/b$a$a;->a(Landroid/app/Activity;)V

    :cond_3
    :goto_1
    return-void
.end method

.method public onDisplayAdded(I)V
    .locals 0

    return-void
.end method

.method public onDisplayChanged(I)V
    .locals 2

    iget-object p1, p0, Lmiuix/autodensity/b$a$a;->a:Ljava/lang/ref/WeakReference;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onDisplayChanged activity: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lmiuix/autodensity/c;->a(Ljava/lang/String;)V

    if-eqz p1, :cond_1

    invoke-static {p1}, Lmiuix/autodensity/f;->a(Landroid/content/Context;)V

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lmiuix/autodensity/b$a$a;->b:Lmiuix/autodensity/b$a;

    invoke-static {p1, p0}, Lmiuix/autodensity/b$a;->a(Lmiuix/autodensity/b$a;Lmiuix/autodensity/b$a$a;)V

    :goto_1
    return-void
.end method

.method public onDisplayRemoved(I)V
    .locals 0

    return-void
.end method

.method public onLowMemory()V
    .locals 0

    return-void
.end method
