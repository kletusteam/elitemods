.class public Lmiuix/autodensity/b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/autodensity/b$a;
    }
.end annotation


# static fields
.field private static a:Z = true


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/app/Application;Z)V
    .locals 0

    sput-boolean p1, Lmiuix/autodensity/b;->a:Z

    invoke-static {}, Lmiuix/autodensity/c;->b()V

    invoke-static {}, Lmiuix/autodensity/e;->a()Lmiuix/autodensity/e;

    move-result-object p1

    invoke-virtual {p1, p0}, Lmiuix/autodensity/e;->a(Landroid/content/Context;)V

    invoke-static {p0}, Lmiuix/autodensity/b;->d(Landroid/app/Application;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-static {p0}, Lmiuix/autodensity/f;->a(Landroid/content/Context;)V

    :cond_0
    new-instance p1, Lmiuix/autodensity/b$a;

    invoke-direct {p1}, Lmiuix/autodensity/b$a;-><init>()V

    invoke-virtual {p0, p1}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    new-instance p1, Lmiuix/autodensity/a;

    invoke-direct {p1, p0}, Lmiuix/autodensity/a;-><init>(Landroid/app/Application;)V

    invoke-virtual {p0, p1}, Landroid/app/Application;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    return-void
.end method

.method public static a()Z
    .locals 1

    sget-boolean v0, Lmiuix/autodensity/b;->a:Z

    return v0
.end method

.method static synthetic a(Landroid/app/Application;)Z
    .locals 0

    invoke-static {p0}, Lmiuix/autodensity/b;->d(Landroid/app/Application;)Z

    move-result p0

    return p0
.end method

.method public static b(Landroid/app/Application;)V
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lmiuix/autodensity/b;->a(Landroid/app/Application;Z)V

    return-void
.end method

.method static c(Landroid/app/Application;)V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1d

    if-ne v0, v1, :cond_0

    invoke-static {p0}, Lmiuix/autodensity/b;->d(Landroid/app/Application;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lmiuix/autodensity/f;->a(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method private static d(Landroid/app/Application;)Z
    .locals 1

    instance-of v0, p0, Lmiuix/autodensity/g;

    if-eqz v0, :cond_0

    check-cast p0, Lmiuix/autodensity/g;

    invoke-interface {p0}, Lmiuix/autodensity/g;->a()Z

    move-result p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x1

    :goto_0
    return p0
.end method
