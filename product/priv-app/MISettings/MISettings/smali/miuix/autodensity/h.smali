.class public Lmiuix/autodensity/h;
.super Ljava/lang/Object;


# static fields
.field private static a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lmiuix/autodensity/h;->b()Z

    move-result v0

    sput-boolean v0, Lmiuix/autodensity/h;->a:Z

    return-void
.end method

.method public static a()Z
    .locals 1

    sget-boolean v0, Lmiuix/autodensity/h;->a:Z

    return v0
.end method

.method private static b()Z
    .locals 7

    sget-object v0, Landroid/os/Build;->TAGS:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    const-string v3, "test-keys"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    const-string v3, "/system/bin/su"

    const-string v4, "/system/xbin/su"

    filled-new-array {v3, v4}, [Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    :goto_1
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    move v0, v2

    goto :goto_2

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    :goto_2
    if-eqz v0, :cond_3

    const-string v1, "Current device is rooted"

    invoke-static {v1}, Lmiuix/autodensity/c;->a(Ljava/lang/String;)V

    :cond_3
    return v0
.end method
