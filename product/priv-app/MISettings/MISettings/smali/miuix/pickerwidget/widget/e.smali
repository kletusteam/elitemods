.class Lmiuix/pickerwidget/widget/e;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiuix/pickerwidget/widget/NumberPicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lmiuix/pickerwidget/widget/NumberPicker;


# direct methods
.method constructor <init>(Lmiuix/pickerwidget/widget/NumberPicker;)V
    .locals 0

    iput-object p1, p0, Lmiuix/pickerwidget/widget/e;->a:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result p1

    const/4 p2, 0x7

    const/4 v0, 0x0

    if-eq p1, p2, :cond_2

    const/16 p2, 0x9

    if-eq p1, p2, :cond_1

    const/16 p2, 0xa

    if-eq p1, p2, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lmiuix/pickerwidget/widget/e;->a:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-static {p1, v0}, Lmiuix/pickerwidget/widget/NumberPicker;->a(Lmiuix/pickerwidget/widget/NumberPicker;Z)Z

    iget-object p1, p0, Lmiuix/pickerwidget/widget/e;->a:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-static {p1}, Lmiuix/pickerwidget/widget/NumberPicker;->h(Lmiuix/pickerwidget/widget/NumberPicker;)Lmiuix/animation/i;

    move-result-object p1

    sget-object p2, Lmiuix/animation/i$a;->a:Lmiuix/animation/i$a;

    invoke-interface {p1, p2}, Lmiuix/animation/i;->a(Lmiuix/animation/i$a;)Lmiuix/animation/i;

    new-array p2, v0, [Lmiuix/animation/a/a;

    invoke-interface {p1, p2}, Lmiuix/animation/i;->c([Lmiuix/animation/a/a;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lmiuix/pickerwidget/widget/e;->a:Lmiuix/pickerwidget/widget/NumberPicker;

    const/4 p2, 0x1

    invoke-static {p1, p2}, Lmiuix/pickerwidget/widget/NumberPicker;->a(Lmiuix/pickerwidget/widget/NumberPicker;Z)Z

    iget-object p1, p0, Lmiuix/pickerwidget/widget/e;->a:Lmiuix/pickerwidget/widget/NumberPicker;

    invoke-static {p1}, Lmiuix/pickerwidget/widget/NumberPicker;->h(Lmiuix/pickerwidget/widget/NumberPicker;)Lmiuix/animation/i;

    move-result-object p1

    sget-object p2, Lmiuix/animation/i$a;->a:Lmiuix/animation/i$a;

    invoke-interface {p1, p2}, Lmiuix/animation/i;->a(Lmiuix/animation/i$a;)Lmiuix/animation/i;

    new-array p2, v0, [Lmiuix/animation/a/a;

    invoke-interface {p1, p2}, Lmiuix/animation/i;->f([Lmiuix/animation/a/a;)V

    :cond_2
    :goto_0
    return v0
.end method
