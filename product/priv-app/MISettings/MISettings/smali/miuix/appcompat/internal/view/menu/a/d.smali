.class public Lmiuix/appcompat/internal/view/menu/a/d;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/PopupWindow$OnDismissListener;


# instance fields
.field private a:Lmiuix/appcompat/internal/view/menu/i;

.field private b:Lmiuix/appcompat/internal/view/menu/m$a;

.field private c:Lmiuix/appcompat/internal/view/menu/a/c;


# direct methods
.method public constructor <init>(Lmiuix/appcompat/internal/view/menu/i;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/a/d;->a:Lmiuix/appcompat/internal/view/menu/i;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a/d;->c:Lmiuix/appcompat/internal/view/menu/a/c;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmiuix/appcompat/internal/view/menu/a/c;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/a/d;->c:Lmiuix/appcompat/internal/view/menu/a/c;

    :cond_0
    return-void
.end method

.method public a(Landroid/os/IBinder;Landroid/view/View;FF)V
    .locals 2

    new-instance p1, Lmiuix/appcompat/internal/view/menu/a/h;

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a/d;->a:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/i;->d()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/a/d;->a:Lmiuix/appcompat/internal/view/menu/i;

    invoke-direct {p1, v0, v1, p0}, Lmiuix/appcompat/internal/view/menu/a/h;-><init>(Landroid/content/Context;Lmiuix/appcompat/internal/view/menu/i;Landroid/widget/PopupWindow$OnDismissListener;)V

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/a/d;->c:Lmiuix/appcompat/internal/view/menu/a/c;

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/a/d;->c:Lmiuix/appcompat/internal/view/menu/a/c;

    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-interface {p1, p2, v0, p3, p4}, Lmiuix/appcompat/internal/view/menu/a/c;->a(Landroid/view/View;Landroid/view/ViewGroup;FF)V

    return-void
.end method

.method public a(Lmiuix/appcompat/internal/view/menu/m$a;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/a/d;->b:Lmiuix/appcompat/internal/view/menu/m$a;

    return-void
.end method

.method public onDismiss()V
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a/d;->b:Lmiuix/appcompat/internal/view/menu/m$a;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/a/d;->a:Lmiuix/appcompat/internal/view/menu/i;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lmiuix/appcompat/internal/view/menu/m$a;->a(Lmiuix/appcompat/internal/view/menu/i;Z)V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a/d;->a:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/i;->b()V

    return-void
.end method
