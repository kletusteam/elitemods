.class public Lmiuix/appcompat/internal/view/menu/i;
.super Lcom/android/internal/view/menu/MenuBuilder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/internal/view/menu/i$b;,
        Lmiuix/appcompat/internal/view/menu/i$a;
    }
.end annotation


# static fields
.field private static final sCategoryToOrder:[I


# instance fields
.field private mActionItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lmiuix/appcompat/internal/view/menu/k;",
            ">;"
        }
    .end annotation
.end field

.field private mCallback:Lmiuix/appcompat/internal/view/menu/i$a;

.field private final mContext:Landroid/content/Context;

.field private mCurrentMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

.field private mDefaultShowAsAction:I

.field private mExpandedItem:Lmiuix/appcompat/internal/view/menu/k;

.field mHeaderIcon:Landroid/graphics/drawable/Drawable;

.field mHeaderTitle:Ljava/lang/CharSequence;

.field mHeaderView:Landroid/view/View;

.field private mIsActionItemsStale:Z

.field private mIsClosing:Z

.field private mIsVisibleItemsStale:Z

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lmiuix/appcompat/internal/view/menu/k;",
            ">;"
        }
    .end annotation
.end field

.field private mItemsChangedWhileDispatchPrevented:Z

.field private mNonActionItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lmiuix/appcompat/internal/view/menu/k;",
            ">;"
        }
    .end annotation
.end field

.field private mOptionalIconsVisible:Z

.field private mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Ljava/lang/ref/WeakReference<",
            "Lmiuix/appcompat/internal/view/menu/m;",
            ">;>;"
        }
    .end annotation
.end field

.field private mPreventDispatchingItemsChanged:Z

.field private mQwertyMode:Z

.field private final mResources:Landroid/content/res/Resources;

.field private mShortcutsVisible:Z

.field private mTempShortcutItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lmiuix/appcompat/internal/view/menu/k;",
            ">;"
        }
    .end annotation
.end field

.field private mVisibleItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lmiuix/appcompat/internal/view/menu/k;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lmiuix/appcompat/internal/view/menu/i;->sCategoryToOrder:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x4
        0x5
        0x3
        0x2
        0x0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Lcom/android/internal/view/menu/MenuBuilder;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mDefaultShowAsAction:I

    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mPreventDispatchingItemsChanged:Z

    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mItemsChangedWhileDispatchPrevented:Z

    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mOptionalIconsVisible:Z

    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mIsClosing:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mTempShortcutItemList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/i;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/i;->mResources:Landroid/content/res/Resources;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/i;->mItems:Ljava/util/ArrayList;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/i;->mVisibleItems:Ljava/util/ArrayList;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lmiuix/appcompat/internal/view/menu/i;->mIsVisibleItemsStale:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mActionItems:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mNonActionItems:Ljava/util/ArrayList;

    iput-boolean p1, p0, Lmiuix/appcompat/internal/view/menu/i;->mIsActionItemsStale:Z

    invoke-direct {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->d(Z)V

    return-void
.end method

.method private static a(Ljava/util/ArrayList;I)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lmiuix/appcompat/internal/view/menu/k;",
            ">;I)I"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/view/menu/k;

    invoke-virtual {v1}, Lmiuix/appcompat/internal/view/menu/k;->b()I

    move-result v1

    if-gt v1, p1, :cond_0

    add-int/lit8 v0, v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return p0
.end method

.method private a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 10

    invoke-static {p3}, Lmiuix/appcompat/internal/view/menu/i;->f(I)I

    move-result v8

    new-instance v9, Lmiuix/appcompat/internal/view/menu/k;

    iget v7, p0, Lmiuix/appcompat/internal/view/menu/i;->mDefaultShowAsAction:I

    move-object v0, v9

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, v8

    move-object v6, p4

    invoke-direct/range {v0 .. v7}, Lmiuix/appcompat/internal/view/menu/k;-><init>(Lmiuix/appcompat/internal/view/menu/i;IIIILjava/lang/CharSequence;I)V

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/i;->mCurrentMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    if-eqz p1, :cond_0

    invoke-virtual {v9, p1}, Lmiuix/appcompat/internal/view/menu/k;->a(Landroid/view/ContextMenu$ContextMenuInfo;)V

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/i;->mItems:Ljava/util/ArrayList;

    invoke-static {p1, v8}, Lmiuix/appcompat/internal/view/menu/i;->a(Ljava/util/ArrayList;I)I

    move-result p2

    invoke-virtual {p1, p2, v9}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    return-object v9
.end method

.method private a(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/i;->k()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p5, :cond_0

    iput-object p5, p0, Lmiuix/appcompat/internal/view/menu/i;->mHeaderView:Landroid/view/View;

    iput-object v1, p0, Lmiuix/appcompat/internal/view/menu/i;->mHeaderTitle:Ljava/lang/CharSequence;

    iput-object v1, p0, Lmiuix/appcompat/internal/view/menu/i;->mHeaderIcon:Landroid/graphics/drawable/Drawable;

    goto :goto_2

    :cond_0
    if-lez p1, :cond_1

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/i;->mHeaderTitle:Ljava/lang/CharSequence;

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    iput-object p2, p0, Lmiuix/appcompat/internal/view/menu/i;->mHeaderTitle:Ljava/lang/CharSequence;

    :cond_2
    :goto_0
    if-lez p3, :cond_3

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/i;->mHeaderIcon:Landroid/graphics/drawable/Drawable;

    goto :goto_1

    :cond_3
    if-eqz p4, :cond_4

    iput-object p4, p0, Lmiuix/appcompat/internal/view/menu/i;->mHeaderIcon:Landroid/graphics/drawable/Drawable;

    :cond_4
    :goto_1
    iput-object v1, p0, Lmiuix/appcompat/internal/view/menu/i;->mHeaderView:Landroid/view/View;

    :goto_2
    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    return-void
.end method

.method private a(IZ)V
    .locals 1

    if-ltz p1, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    if-eqz p2, :cond_1

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    :cond_1
    :goto_0
    return-void
.end method

.method private a(Lmiuix/appcompat/internal/view/menu/o;)Z
    .locals 4

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiuix/appcompat/internal/view/menu/m;

    if-nez v3, :cond_2

    iget-object v3, p0, Lmiuix/appcompat/internal/view/menu/i;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    if-nez v1, :cond_1

    invoke-interface {v3, p1}, Lmiuix/appcompat/internal/view/menu/m;->a(Lmiuix/appcompat/internal/view/menu/o;)Z

    move-result v1

    goto :goto_0

    :cond_3
    return v1
.end method

.method private c(Z)V
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/i;->q()V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/appcompat/internal/view/menu/m;

    if-nez v2, :cond_1

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/i;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v2, p1}, Lmiuix/appcompat/internal/view/menu/m;->a(Z)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/i;->p()V

    return-void
.end method

.method private d(Z)V
    .locals 2

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/i;->mResources:Landroid/content/res/Resources;

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    iget p1, p1, Landroid/content/res/Configuration;->keyboard:I

    if-eq p1, v0, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/i;->mResources:Landroid/content/res/Resources;

    sget v1, Ld/b/c;->abc_config_showMenuShortcutsWhenKeyboardPresent:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mShortcutsVisible:Z

    return-void
.end method

.method private static f(I)I
    .locals 3

    const/high16 v0, -0x10000

    and-int/2addr v0, p0

    shr-int/lit8 v0, v0, 0x10

    if-ltz v0, :cond_0

    sget-object v1, Lmiuix/appcompat/internal/view/menu/i;->sCategoryToOrder:[I

    array-length v2, v1

    if-ge v0, v2, :cond_0

    aget v0, v1, v0

    shl-int/lit8 v0, v0, 0x10

    const v1, 0xffff

    and-int/2addr p0, v1

    or-int/2addr p0, v0

    return p0

    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "order does not contain a valid category."

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public a(I)I
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lmiuix/appcompat/internal/view/menu/i;->a(II)I

    move-result p1

    return p1
.end method

.method public a(II)I
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/i;->size()I

    move-result v0

    if-gez p2, :cond_0

    const/4 p2, 0x0

    :cond_0
    :goto_0
    if-ge p2, v0, :cond_2

    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/i;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/view/menu/k;

    invoke-virtual {v1}, Lmiuix/appcompat/internal/view/menu/k;->getGroupId()I

    move-result v1

    if-ne v1, p1, :cond_1

    return p2

    :cond_1
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, -0x1

    return p1
.end method

.method protected a(Landroid/graphics/drawable/Drawable;)Lmiuix/appcompat/internal/view/menu/i;
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lmiuix/appcompat/internal/view/menu/i;->a(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    return-object p0
.end method

.method protected a(Landroid/view/View;)Lmiuix/appcompat/internal/view/menu/i;
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lmiuix/appcompat/internal/view/menu/i;->a(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    return-object p0
.end method

.method protected a(Ljava/lang/CharSequence;)Lmiuix/appcompat/internal/view/menu/i;
    .locals 6

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lmiuix/appcompat/internal/view/menu/i;->a(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    return-object p0
.end method

.method a(ILandroid/view/KeyEvent;)Lmiuix/appcompat/internal/view/menu/k;
    .locals 9

    goto/32 :goto_2c

    nop

    :goto_0
    if-eq v6, v7, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_21

    nop

    :goto_1
    check-cast p1, Lmiuix/appcompat/internal/view/menu/k;

    goto/32 :goto_c

    nop

    :goto_2
    aget-char v7, v7, v5

    goto/32 :goto_13

    nop

    :goto_3
    const/4 v8, 0x2

    goto/32 :goto_33

    nop

    :goto_4
    return-object v4

    :goto_5
    goto/32 :goto_2b

    nop

    :goto_6
    const/16 v7, 0x8

    goto/32 :goto_0

    nop

    :goto_7
    if-nez v4, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_20

    nop

    :goto_8
    invoke-virtual {v4}, Lmiuix/appcompat/internal/view/menu/k;->getAlphabeticShortcut()C

    move-result v6

    goto/32 :goto_a

    nop

    :goto_9
    if-eq v6, v7, :cond_2

    goto/32 :goto_2e

    :cond_2
    goto/32 :goto_28

    nop

    :goto_a
    goto/16 :goto_23

    :goto_b
    goto/32 :goto_22

    nop

    :goto_c
    return-object p1

    :goto_d
    goto/32 :goto_19

    nop

    :goto_e
    if-nez p2, :cond_3

    goto/32 :goto_b

    :cond_3
    goto/32 :goto_8

    nop

    :goto_f
    check-cast v4, Lmiuix/appcompat/internal/view/menu/k;

    goto/32 :goto_e

    nop

    :goto_10
    if-eq p2, v4, :cond_4

    goto/32 :goto_d

    :cond_4
    goto/32 :goto_32

    nop

    :goto_11
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_12
    goto/32 :goto_2f

    nop

    :goto_13
    if-eq v6, v7, :cond_5

    goto/32 :goto_1e

    :cond_5
    goto/32 :goto_16

    nop

    :goto_14
    const/4 v4, 0x1

    goto/32 :goto_24

    nop

    :goto_15
    new-instance v3, Landroid/view/KeyCharacterMap$KeyData;

    goto/32 :goto_17

    nop

    :goto_16
    and-int/lit8 v7, v1, 0x2

    goto/32 :goto_1d

    nop

    :goto_17
    invoke-direct {v3}, Landroid/view/KeyCharacterMap$KeyData;-><init>()V

    goto/32 :goto_34

    nop

    :goto_18
    iget-object v7, v3, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    goto/32 :goto_2

    nop

    :goto_19
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/i;->n()Z

    move-result p2

    goto/32 :goto_11

    nop

    :goto_1a
    if-nez p2, :cond_6

    goto/32 :goto_12

    :cond_6
    goto/32 :goto_6

    nop

    :goto_1b
    iget-object v7, v3, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    goto/32 :goto_3

    nop

    :goto_1c
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v1

    goto/32 :goto_15

    nop

    :goto_1d
    if-nez v7, :cond_7

    goto/32 :goto_36

    :cond_7
    :goto_1e
    goto/32 :goto_1b

    nop

    :goto_1f
    invoke-virtual {p0, v0, p1, p2}, Lmiuix/appcompat/internal/view/menu/i;->a(Ljava/util/List;ILandroid/view/KeyEvent;)V

    goto/32 :goto_26

    nop

    :goto_20
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_f

    nop

    :goto_21
    const/16 v6, 0x43

    goto/32 :goto_35

    nop

    :goto_22
    invoke-virtual {v4}, Lmiuix/appcompat/internal/view/menu/k;->getNumericShortcut()C

    move-result v6

    :goto_23
    goto/32 :goto_18

    nop

    :goto_24
    const/4 v5, 0x0

    goto/32 :goto_10

    nop

    :goto_25
    if-nez v1, :cond_8

    goto/32 :goto_31

    :cond_8
    goto/32 :goto_30

    nop

    :goto_26
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    goto/32 :goto_29

    nop

    :goto_27
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p2

    goto/32 :goto_14

    nop

    :goto_28
    and-int/lit8 v7, v1, 0x2

    goto/32 :goto_2d

    nop

    :goto_29
    const/4 v2, 0x0

    goto/32 :goto_25

    nop

    :goto_2a
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto/32 :goto_1f

    nop

    :goto_2b
    return-object v2

    :goto_2c
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mTempShortcutItemList:Ljava/util/ArrayList;

    goto/32 :goto_2a

    nop

    :goto_2d
    if-eqz v7, :cond_9

    goto/32 :goto_36

    :cond_9
    :goto_2e
    goto/32 :goto_1a

    nop

    :goto_2f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    goto/32 :goto_7

    nop

    :goto_30
    return-object v2

    :goto_31
    goto/32 :goto_1c

    nop

    :goto_32
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_33
    aget-char v7, v7, v8

    goto/32 :goto_9

    nop

    :goto_34
    invoke-virtual {p2, v3}, Landroid/view/KeyEvent;->getKeyData(Landroid/view/KeyCharacterMap$KeyData;)Z

    goto/32 :goto_27

    nop

    :goto_35
    if-eq p1, v6, :cond_a

    goto/32 :goto_12

    :cond_a
    :goto_36
    goto/32 :goto_4

    nop
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mCallback:Lmiuix/appcompat/internal/view/menu/i$a;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Lmiuix/appcompat/internal/view/menu/i$a;->b(Lmiuix/appcompat/internal/view/menu/i;)V

    :cond_0
    return-void
.end method

.method a(Landroid/view/MenuItem;)V
    .locals 4

    goto/32 :goto_18

    nop

    :goto_0
    if-eqz v3, :cond_0

    goto/32 :goto_f

    :cond_0
    goto/32 :goto_e

    nop

    :goto_1
    const/4 v3, 0x0

    :goto_2
    goto/32 :goto_19

    nop

    :goto_3
    check-cast v2, Lmiuix/appcompat/internal/view/menu/k;

    goto/32 :goto_9

    nop

    :goto_4
    goto :goto_7

    :goto_5
    goto/32 :goto_15

    nop

    :goto_6
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    goto/32 :goto_8

    nop

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    goto/32 :goto_1b

    nop

    :goto_9
    invoke-virtual {v2}, Lmiuix/appcompat/internal/view/menu/k;->getGroupId()I

    move-result v3

    goto/32 :goto_12

    nop

    :goto_a
    invoke-virtual {v2}, Lmiuix/appcompat/internal/view/menu/k;->isCheckable()Z

    move-result v3

    goto/32 :goto_1a

    nop

    :goto_b
    if-eq v2, p1, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_c

    nop

    :goto_c
    const/4 v3, 0x1

    goto/32 :goto_10

    nop

    :goto_d
    invoke-virtual {v2}, Lmiuix/appcompat/internal/view/menu/k;->h()Z

    move-result v3

    goto/32 :goto_0

    nop

    :goto_e
    goto :goto_7

    :goto_f
    goto/32 :goto_a

    nop

    :goto_10
    goto :goto_2

    :goto_11
    goto/32 :goto_1

    nop

    :goto_12
    if-eq v3, v0, :cond_2

    goto/32 :goto_7

    :cond_2
    goto/32 :goto_d

    nop

    :goto_13
    goto :goto_7

    :goto_14
    goto/32 :goto_b

    nop

    :goto_15
    return-void

    :goto_16
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_3

    nop

    :goto_17
    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/i;->mItems:Ljava/util/ArrayList;

    goto/32 :goto_6

    nop

    :goto_18
    invoke-interface {p1}, Landroid/view/MenuItem;->getGroupId()I

    move-result v0

    goto/32 :goto_17

    nop

    :goto_19
    invoke-virtual {v2, v3}, Lmiuix/appcompat/internal/view/menu/k;->b(Z)V

    goto/32 :goto_4

    nop

    :goto_1a
    if-eqz v3, :cond_3

    goto/32 :goto_14

    :cond_3
    goto/32 :goto_13

    nop

    :goto_1b
    if-nez v2, :cond_4

    goto/32 :goto_5

    :cond_4
    goto/32 :goto_16

    nop
.end method

.method a(Ljava/util/List;ILandroid/view/KeyEvent;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lmiuix/appcompat/internal/view/menu/k;",
            ">;I",
            "Landroid/view/KeyEvent;",
            ")V"
        }
    .end annotation

    goto/32 :goto_29

    nop

    :goto_0
    if-eq v6, v7, :cond_0

    goto/32 :goto_11

    :cond_0
    goto/32 :goto_16

    nop

    :goto_1
    if-eqz v7, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_2f

    nop

    :goto_2
    if-nez v6, :cond_2

    goto/32 :goto_11

    :cond_2
    goto/32 :goto_31

    nop

    :goto_3
    aget-char v7, v7, v8

    goto/32 :goto_d

    nop

    :goto_4
    iget-object v3, p0, Lmiuix/appcompat/internal/view/menu/i;->mItems:Ljava/util/ArrayList;

    goto/32 :goto_10

    nop

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    goto/32 :goto_b

    nop

    :goto_6
    aget-char v8, v7, v8

    goto/32 :goto_28

    nop

    :goto_7
    const/16 v4, 0x43

    goto/32 :goto_22

    nop

    :goto_8
    goto/16 :goto_2d

    :goto_9
    goto/32 :goto_2c

    nop

    :goto_a
    check-cast v5, Lmiuix/appcompat/internal/view/menu/k;

    goto/32 :goto_20

    nop

    :goto_b
    if-nez v5, :cond_3

    goto/32 :goto_1d

    :cond_3
    goto/32 :goto_21

    nop

    :goto_c
    iget-object v7, v2, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    goto/32 :goto_30

    nop

    :goto_d
    if-ne v6, v7, :cond_4

    goto/32 :goto_17

    :cond_4
    goto/32 :goto_12

    nop

    :goto_e
    return-void

    :goto_f
    invoke-virtual {v5}, Lmiuix/appcompat/internal/view/menu/k;->isEnabled()Z

    move-result v6

    goto/32 :goto_2

    nop

    :goto_10
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_11
    goto/32 :goto_5

    nop

    :goto_12
    if-nez v0, :cond_5

    goto/32 :goto_11

    :cond_5
    goto/32 :goto_27

    nop

    :goto_13
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v1

    goto/32 :goto_1b

    nop

    :goto_14
    check-cast v6, Lmiuix/appcompat/internal/view/menu/i;

    goto/32 :goto_2a

    nop

    :goto_15
    and-int/lit8 v7, v1, 0x5

    goto/32 :goto_1

    nop

    :goto_16
    if-eq p2, v4, :cond_6

    goto/32 :goto_11

    :cond_6
    :goto_17
    goto/32 :goto_f

    nop

    :goto_18
    invoke-virtual {v5}, Lmiuix/appcompat/internal/view/menu/k;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v6

    goto/32 :goto_14

    nop

    :goto_19
    return-void

    :goto_1a
    goto/32 :goto_4

    nop

    :goto_1b
    new-instance v2, Landroid/view/KeyCharacterMap$KeyData;

    goto/32 :goto_23

    nop

    :goto_1c
    goto :goto_11

    :goto_1d
    goto/32 :goto_e

    nop

    :goto_1e
    invoke-virtual {v5}, Lmiuix/appcompat/internal/view/menu/k;->getAlphabeticShortcut()C

    move-result v6

    goto/32 :goto_8

    nop

    :goto_1f
    if-nez v0, :cond_7

    goto/32 :goto_9

    :cond_7
    goto/32 :goto_1e

    nop

    :goto_20
    invoke-virtual {v5}, Lmiuix/appcompat/internal/view/menu/k;->hasSubMenu()Z

    move-result v6

    goto/32 :goto_26

    nop

    :goto_21
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    goto/32 :goto_a

    nop

    :goto_22
    if-eqz v3, :cond_8

    goto/32 :goto_1a

    :cond_8
    goto/32 :goto_24

    nop

    :goto_23
    invoke-direct {v2}, Landroid/view/KeyCharacterMap$KeyData;-><init>()V

    goto/32 :goto_25

    nop

    :goto_24
    if-ne p2, v4, :cond_9

    goto/32 :goto_1a

    :cond_9
    goto/32 :goto_19

    nop

    :goto_25
    invoke-virtual {p3, v2}, Landroid/view/KeyEvent;->getKeyData(Landroid/view/KeyCharacterMap$KeyData;)Z

    move-result v3

    goto/32 :goto_7

    nop

    :goto_26
    if-nez v6, :cond_a

    goto/32 :goto_2b

    :cond_a
    goto/32 :goto_18

    nop

    :goto_27
    const/16 v7, 0x8

    goto/32 :goto_0

    nop

    :goto_28
    if-ne v6, v8, :cond_b

    goto/32 :goto_17

    :cond_b
    goto/32 :goto_2e

    nop

    :goto_29
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/i;->n()Z

    move-result v0

    goto/32 :goto_13

    nop

    :goto_2a
    invoke-virtual {v6, p1, p2, p3}, Lmiuix/appcompat/internal/view/menu/i;->a(Ljava/util/List;ILandroid/view/KeyEvent;)V

    :goto_2b
    goto/32 :goto_1f

    nop

    :goto_2c
    invoke-virtual {v5}, Lmiuix/appcompat/internal/view/menu/k;->getNumericShortcut()C

    move-result v6

    :goto_2d
    goto/32 :goto_15

    nop

    :goto_2e
    const/4 v8, 0x2

    goto/32 :goto_3

    nop

    :goto_2f
    if-nez v6, :cond_c

    goto/32 :goto_11

    :cond_c
    goto/32 :goto_c

    nop

    :goto_30
    const/4 v8, 0x0

    goto/32 :goto_6

    nop

    :goto_31
    invoke-interface {p1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_1c

    nop
.end method

.method public a(Lmiuix/appcompat/internal/view/menu/i$a;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/i;->mCallback:Lmiuix/appcompat/internal/view/menu/i$a;

    return-void
.end method

.method public a(Lmiuix/appcompat/internal/view/menu/m;)V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mContext:Landroid/content/Context;

    invoke-interface {p1, v0, p0}, Lmiuix/appcompat/internal/view/menu/m;->a(Landroid/content/Context;Lmiuix/appcompat/internal/view/menu/i;)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lmiuix/appcompat/internal/view/menu/i;->mIsActionItemsStale:Z

    return-void
.end method

.method final a(Z)V
    .locals 3

    goto/32 :goto_9

    nop

    :goto_0
    invoke-interface {v2, p0, p1}, Lmiuix/appcompat/internal/view/menu/m;->a(Lmiuix/appcompat/internal/view/menu/i;Z)V

    goto/32 :goto_4

    nop

    :goto_1
    const/4 p1, 0x0

    goto/32 :goto_16

    nop

    :goto_2
    return-void

    :goto_3
    goto/32 :goto_7

    nop

    :goto_4
    goto :goto_15

    :goto_5
    goto/32 :goto_1

    nop

    :goto_6
    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/i;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    goto/32 :goto_19

    nop

    :goto_7
    const/4 v0, 0x1

    goto/32 :goto_13

    nop

    :goto_8
    if-nez v1, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_b

    nop

    :goto_9
    iget-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mIsClosing:Z

    goto/32 :goto_e

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_8

    nop

    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_d

    nop

    :goto_c
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    goto/32 :goto_14

    nop

    :goto_d
    check-cast v1, Ljava/lang/ref/WeakReference;

    goto/32 :goto_17

    nop

    :goto_e
    if-nez v0, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_2

    nop

    :goto_f
    goto :goto_15

    :goto_10
    goto/32 :goto_0

    nop

    :goto_11
    return-void

    :goto_12
    if-eqz v2, :cond_2

    goto/32 :goto_10

    :cond_2
    goto/32 :goto_6

    nop

    :goto_13
    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mIsClosing:Z

    goto/32 :goto_c

    nop

    :goto_14
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_15
    goto/32 :goto_a

    nop

    :goto_16
    iput-boolean p1, p0, Lmiuix/appcompat/internal/view/menu/i;->mIsClosing:Z

    goto/32 :goto_11

    nop

    :goto_17
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_18

    nop

    :goto_18
    check-cast v2, Lmiuix/appcompat/internal/view/menu/m;

    goto/32 :goto_12

    nop

    :goto_19
    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto/32 :goto_f

    nop
.end method

.method public a(Landroid/view/MenuItem;I)Z
    .locals 6

    check-cast p1, Lmiuix/appcompat/internal/view/menu/k;

    const/4 v0, 0x0

    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/k;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_3

    :cond_0
    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/k;->f()Z

    move-result v1

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/k;->a()Landroid/view/ActionProvider;

    move-result-object v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/view/ActionProvider;->hasSubMenu()Z

    move-result v4

    if-eqz v4, :cond_1

    move v4, v3

    goto :goto_0

    :cond_1
    move v4, v0

    :goto_0
    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/k;->e()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/k;->expandActionView()Z

    move-result p1

    or-int/2addr v1, p1

    if-eqz v1, :cond_7

    invoke-virtual {p0, v3}, Lmiuix/appcompat/internal/view/menu/i;->a(Z)V

    goto :goto_2

    :cond_2
    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/k;->hasSubMenu()Z

    move-result v5

    if-nez v5, :cond_4

    if-eqz v4, :cond_3

    goto :goto_1

    :cond_3
    and-int/lit8 p1, p2, 0x1

    if-nez p1, :cond_7

    invoke-virtual {p0, v3}, Lmiuix/appcompat/internal/view/menu/i;->a(Z)V

    goto :goto_2

    :cond_4
    :goto_1
    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/view/menu/i;->a(Z)V

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/k;->hasSubMenu()Z

    move-result p2

    if-nez p2, :cond_5

    new-instance p2, Lmiuix/appcompat/internal/view/menu/o;

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/i;->d()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0, p0, p1}, Lmiuix/appcompat/internal/view/menu/o;-><init>(Landroid/content/Context;Lmiuix/appcompat/internal/view/menu/i;Lmiuix/appcompat/internal/view/menu/k;)V

    invoke-virtual {p1, p2}, Lmiuix/appcompat/internal/view/menu/k;->a(Lmiuix/appcompat/internal/view/menu/o;)V

    :cond_5
    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/k;->getSubMenu()Landroid/view/SubMenu;

    move-result-object p1

    check-cast p1, Lmiuix/appcompat/internal/view/menu/o;

    if-eqz v4, :cond_6

    invoke-virtual {v2, p1}, Landroid/view/ActionProvider;->onPrepareSubMenu(Landroid/view/SubMenu;)V

    :cond_6
    invoke-direct {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->a(Lmiuix/appcompat/internal/view/menu/o;)Z

    move-result p1

    or-int/2addr v1, p1

    if-nez v1, :cond_7

    invoke-virtual {p0, v3}, Lmiuix/appcompat/internal/view/menu/i;->a(Z)V

    :cond_7
    :goto_2
    return v1

    :cond_8
    :goto_3
    return v0
.end method

.method a(Lmiuix/appcompat/internal/view/menu/i;Landroid/view/MenuItem;)Z
    .locals 1

    goto/32 :goto_6

    nop

    :goto_0
    invoke-interface {v0, p1, p2}, Lmiuix/appcompat/internal/view/menu/i$a;->a(Lmiuix/appcompat/internal/view/menu/i;Landroid/view/MenuItem;)Z

    move-result p1

    goto/32 :goto_7

    nop

    :goto_1
    const/4 p1, 0x0

    :goto_2
    goto/32 :goto_4

    nop

    :goto_3
    const/4 p1, 0x1

    goto/32 :goto_8

    nop

    :goto_4
    return p1

    :goto_5
    if-nez v0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_0

    nop

    :goto_6
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mCallback:Lmiuix/appcompat/internal/view/menu/i$a;

    goto/32 :goto_5

    nop

    :goto_7
    if-nez p1, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_3

    nop

    :goto_8
    goto :goto_2

    :goto_9
    goto/32 :goto_1

    nop
.end method

.method public a(Lmiuix/appcompat/internal/view/menu/k;)Z
    .locals 4

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_4

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mExpandedItem:Lmiuix/appcompat/internal/view/menu/k;

    if-eq v0, p1, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/i;->q()V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiuix/appcompat/internal/view/menu/m;

    if-nez v3, :cond_2

    iget-object v3, p0, Lmiuix/appcompat/internal/view/menu/i;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-interface {v3, p0, p1}, Lmiuix/appcompat/internal/view/menu/m;->b(Lmiuix/appcompat/internal/view/menu/i;Lmiuix/appcompat/internal/view/menu/k;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_3
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/i;->p()V

    if-eqz v1, :cond_4

    const/4 p1, 0x0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/i;->mExpandedItem:Lmiuix/appcompat/internal/view/menu/k;

    :cond_4
    :goto_1
    return v1
.end method

.method public add(I)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0, v0, p1}, Lmiuix/appcompat/internal/view/menu/i;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object p1

    return-object p1
.end method

.method public add(IIII)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p4

    invoke-direct {p0, p1, p2, p3, p4}, Lmiuix/appcompat/internal/view/menu/i;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object p1

    return-object p1
.end method

.method public add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lmiuix/appcompat/internal/view/menu/i;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object p1

    return-object p1
.end method

.method public add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0, v0, p1}, Lmiuix/appcompat/internal/view/menu/i;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object p1

    return-object p1
.end method

.method public addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .locals 7

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p4, p5, p6, v1}, Landroid/content/pm/PackageManager;->queryIntentActivityOptions(Landroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I)Ljava/util/List;

    move-result-object p4

    if-eqz p4, :cond_0

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v2

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_0
    and-int/lit8 p7, p7, 0x1

    if-nez p7, :cond_1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->removeGroup(I)V

    :cond_1
    :goto_1
    if-ge v1, v2, :cond_4

    invoke-interface {p4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p7

    check-cast p7, Landroid/content/pm/ResolveInfo;

    new-instance v3, Landroid/content/Intent;

    iget v4, p7, Landroid/content/pm/ResolveInfo;->specificIndex:I

    if-gez v4, :cond_2

    move-object v4, p6

    goto :goto_2

    :cond_2
    aget-object v4, p5, v4

    :goto_2
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    new-instance v4, Landroid/content/ComponentName;

    iget-object v5, p7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v6, p7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-virtual {p7, v0}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {p0, p1, p2, p3, v4}, Lmiuix/appcompat/internal/view/menu/i;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v4

    invoke-virtual {p7, v0}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v3}, Landroid/view/MenuItem;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;

    move-result-object v3

    if-eqz p8, :cond_3

    iget p7, p7, Landroid/content/pm/ResolveInfo;->specificIndex:I

    if-ltz p7, :cond_3

    aput-object v3, p8, p7

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    return v2
.end method

.method public addSubMenu(I)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, v0, p1}, Lmiuix/appcompat/internal/view/menu/i;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object p1

    return-object p1
.end method

.method public addSubMenu(IIII)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lmiuix/appcompat/internal/view/menu/i;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object p1

    return-object p1
.end method

.method public addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lmiuix/appcompat/internal/view/menu/i;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object p1

    check-cast p1, Lmiuix/appcompat/internal/view/menu/k;

    new-instance p2, Lmiuix/appcompat/internal/view/menu/o;

    iget-object p3, p0, Lmiuix/appcompat/internal/view/menu/i;->mContext:Landroid/content/Context;

    invoke-direct {p2, p3, p0, p1}, Lmiuix/appcompat/internal/view/menu/o;-><init>(Landroid/content/Context;Lmiuix/appcompat/internal/view/menu/i;Lmiuix/appcompat/internal/view/menu/k;)V

    invoke-virtual {p1, p2}, Lmiuix/appcompat/internal/view/menu/k;->a(Lmiuix/appcompat/internal/view/menu/o;)V

    return-object p2
.end method

.method public addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, v0, p1}, Lmiuix/appcompat/internal/view/menu/i;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object p1

    return-object p1
.end method

.method public b(I)I
    .locals 3

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/i;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/i;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/appcompat/internal/view/menu/k;

    invoke-virtual {v2}, Lmiuix/appcompat/internal/view/menu/k;->getItemId()I

    move-result v2

    if-ne v2, p1, :cond_0

    return v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    return p1
.end method

.method public b()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mPreventDispatchingItemsChanged:Z

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/i;->clear()V

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/i;->clearHeader()V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmiuix/appcompat/internal/view/menu/i;->mPreventDispatchingItemsChanged:Z

    iput-boolean v1, p0, Lmiuix/appcompat/internal/view/menu/i;->mItemsChangedWhileDispatchPrevented:Z

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    return-void
.end method

.method public b(Lmiuix/appcompat/internal/view/menu/m;)V
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/appcompat/internal/view/menu/m;

    if-eqz v2, :cond_1

    if-ne v2, p1, :cond_0

    :cond_1
    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/i;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-void
.end method

.method b(Z)V
    .locals 2

    goto/32 :goto_4

    nop

    :goto_0
    const/4 v1, 0x1

    goto/32 :goto_8

    nop

    :goto_1
    return-void

    :goto_2
    goto :goto_a

    :goto_3
    goto/32 :goto_9

    nop

    :goto_4
    iget-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mPreventDispatchingItemsChanged:Z

    goto/32 :goto_0

    nop

    :goto_5
    iput-boolean v1, p0, Lmiuix/appcompat/internal/view/menu/i;->mIsActionItemsStale:Z

    :goto_6
    goto/32 :goto_c

    nop

    :goto_7
    iput-boolean v1, p0, Lmiuix/appcompat/internal/view/menu/i;->mIsVisibleItemsStale:Z

    goto/32 :goto_5

    nop

    :goto_8
    if-eqz v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_b

    nop

    :goto_9
    iput-boolean v1, p0, Lmiuix/appcompat/internal/view/menu/i;->mItemsChangedWhileDispatchPrevented:Z

    :goto_a
    goto/32 :goto_1

    nop

    :goto_b
    if-nez p1, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_7

    nop

    :goto_c
    invoke-direct {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->c(Z)V

    goto/32 :goto_2

    nop
.end method

.method public b(Lmiuix/appcompat/internal/view/menu/k;)Z
    .locals 4

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/i;->q()V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiuix/appcompat/internal/view/menu/m;

    if-nez v3, :cond_2

    iget-object v3, p0, Lmiuix/appcompat/internal/view/menu/i;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-interface {v3, p0, p1}, Lmiuix/appcompat/internal/view/menu/m;->a(Lmiuix/appcompat/internal/view/menu/i;Lmiuix/appcompat/internal/view/menu/k;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_3
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/i;->p()V

    if-eqz v1, :cond_4

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/i;->mExpandedItem:Lmiuix/appcompat/internal/view/menu/k;

    :cond_4
    return v1
.end method

.method public c(I)Lmiuix/appcompat/internal/view/menu/i;
    .locals 0

    iput p1, p0, Lmiuix/appcompat/internal/view/menu/i;->mDefaultShowAsAction:I

    return-object p0
.end method

.method public c()V
    .locals 5

    iget-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mIsActionItemsStale:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiuix/appcompat/internal/view/menu/m;

    if-nez v4, :cond_1

    iget-object v4, p0, Lmiuix/appcompat/internal/view/menu/i;->mPresenters:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4, v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v4}, Lmiuix/appcompat/internal/view/menu/m;->a()Z

    move-result v3

    or-int/2addr v2, v3

    goto :goto_0

    :cond_2
    if-eqz v2, :cond_4

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mActionItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mNonActionItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/i;->m()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/appcompat/internal/view/menu/k;

    invoke-virtual {v2}, Lmiuix/appcompat/internal/view/menu/k;->g()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lmiuix/appcompat/internal/view/menu/i;->mActionItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lmiuix/appcompat/internal/view/menu/i;->mNonActionItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mActionItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mNonActionItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mNonActionItems:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/i;->m()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_5
    iput-boolean v1, p0, Lmiuix/appcompat/internal/view/menu/i;->mIsActionItemsStale:Z

    return-void
.end method

.method c(Lmiuix/appcompat/internal/view/menu/k;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    const/4 p1, 0x1

    goto/32 :goto_1

    nop

    :goto_1
    iput-boolean p1, p0, Lmiuix/appcompat/internal/view/menu/i;->mIsActionItemsStale:Z

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    goto/32 :goto_3

    nop

    :goto_3
    return-void
.end method

.method public clear()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mExpandedItem:Lmiuix/appcompat/internal/view/menu/k;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/view/menu/i;->a(Lmiuix/appcompat/internal/view/menu/k;)Z

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    return-void
.end method

.method public clearHeader()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mHeaderIcon:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mHeaderTitle:Ljava/lang/CharSequence;

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mHeaderView:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    return-void
.end method

.method public close()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/view/menu/i;->a(Z)V

    return-void
.end method

.method public d()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method protected d(I)Lmiuix/appcompat/internal/view/menu/i;
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move v3, p1

    invoke-direct/range {v0 .. v5}, Lmiuix/appcompat/internal/view/menu/i;->a(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    return-object p0
.end method

.method d(Lmiuix/appcompat/internal/view/menu/k;)V
    .locals 0

    goto/32 :goto_2

    nop

    :goto_0
    iput-boolean p1, p0, Lmiuix/appcompat/internal/view/menu/i;->mIsVisibleItemsStale:Z

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    goto/32 :goto_3

    nop

    :goto_2
    const/4 p1, 0x1

    goto/32 :goto_0

    nop

    :goto_3
    return-void
.end method

.method protected e(I)Lmiuix/appcompat/internal/view/menu/i;
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    invoke-direct/range {v0 .. v5}, Lmiuix/appcompat/internal/view/menu/i;->a(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    return-object p0
.end method

.method public e()Lmiuix/appcompat/internal/view/menu/k;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mExpandedItem:Lmiuix/appcompat/internal/view/menu/k;

    return-object v0
.end method

.method public f()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mHeaderIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public findItem(I)Landroid/view/MenuItem;
    .locals 4

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/i;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/i;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/appcompat/internal/view/menu/k;

    invoke-virtual {v2}, Lmiuix/appcompat/internal/view/menu/k;->getItemId()I

    move-result v3

    if-ne v3, p1, :cond_0

    return-object v2

    :cond_0
    invoke-virtual {v2}, Lmiuix/appcompat/internal/view/menu/k;->hasSubMenu()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lmiuix/appcompat/internal/view/menu/k;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v2

    invoke-interface {v2, p1}, Landroid/view/SubMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_1

    return-object v2

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method

.method public g()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mHeaderTitle:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getItem(I)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/MenuItem;

    return-object p1
.end method

.method public h()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mHeaderView:Landroid/view/View;

    return-object v0
.end method

.method public hasVisibleItems()Z
    .locals 4

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/i;->size()I

    move-result v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_1

    iget-object v3, p0, Lmiuix/appcompat/internal/view/menu/i;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiuix/appcompat/internal/view/menu/k;

    invoke-virtual {v3}, Lmiuix/appcompat/internal/view/menu/k;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method public i()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lmiuix/appcompat/internal/view/menu/k;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/i;->c()V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mNonActionItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method public isShortcutKey(ILandroid/view/KeyEvent;)Z
    .locals 0

    invoke-virtual {p0, p1, p2}, Lmiuix/appcompat/internal/view/menu/i;->a(ILandroid/view/KeyEvent;)Lmiuix/appcompat/internal/view/menu/k;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method j()Z
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    iget-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mOptionalIconsVisible:Z

    goto/32 :goto_0

    nop
.end method

.method k()Landroid/content/res/Resources;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mResources:Landroid/content/res/Resources;

    goto/32 :goto_0

    nop
.end method

.method public l()Lmiuix/appcompat/internal/view/menu/i;
    .locals 0

    return-object p0
.end method

.method public m()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lmiuix/appcompat/internal/view/menu/k;",
            ">;"
        }
    .end annotation

    iget-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mIsVisibleItemsStale:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mVisibleItems:Ljava/util/ArrayList;

    return-object v0

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mVisibleItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/view/menu/k;

    invoke-virtual {v1}, Lmiuix/appcompat/internal/view/menu/k;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/i;->mVisibleItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mIsVisibleItemsStale:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mIsActionItemsStale:Z

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mVisibleItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method n()Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mQwertyMode:Z

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method public o()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mShortcutsVisible:Z

    return v0
.end method

.method public p()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mPreventDispatchingItemsChanged:Z

    iget-boolean v1, p0, Lmiuix/appcompat/internal/view/menu/i;->mItemsChangedWhileDispatchPrevented:Z

    if-eqz v1, :cond_0

    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mItemsChangedWhileDispatchPrevented:Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    :cond_0
    return-void
.end method

.method public performIdentifierAction(II)Z
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->findItem(I)Landroid/view/MenuItem;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lmiuix/appcompat/internal/view/menu/i;->a(Landroid/view/MenuItem;I)Z

    move-result p1

    return p1
.end method

.method public performShortcut(ILandroid/view/KeyEvent;I)Z
    .locals 0

    invoke-virtual {p0, p1, p2}, Lmiuix/appcompat/internal/view/menu/i;->a(ILandroid/view/KeyEvent;)Lmiuix/appcompat/internal/view/menu/k;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1, p3}, Lmiuix/appcompat/internal/view/menu/i;->a(Landroid/view/MenuItem;I)Z

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    and-int/lit8 p2, p3, 0x2

    if-eqz p2, :cond_1

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, Lmiuix/appcompat/internal/view/menu/i;->a(Z)V

    :cond_1
    return p1
.end method

.method public q()V
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mPreventDispatchingItemsChanged:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mPreventDispatchingItemsChanged:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mItemsChangedWhileDispatchPrevented:Z

    :cond_0
    return-void
.end method

.method public removeGroup(I)V
    .locals 5

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->a(I)I

    move-result v0

    if-ltz v0, :cond_1

    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/i;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    sub-int/2addr v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    add-int/lit8 v4, v3, 0x1

    if-ge v3, v1, :cond_0

    iget-object v3, p0, Lmiuix/appcompat/internal/view/menu/i;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiuix/appcompat/internal/view/menu/k;

    invoke-virtual {v3}, Lmiuix/appcompat/internal/view/menu/k;->getGroupId()I

    move-result v3

    if-ne v3, p1, :cond_0

    invoke-direct {p0, v0, v2}, Lmiuix/appcompat/internal/view/menu/i;->a(IZ)V

    move v3, v4

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    :cond_1
    return-void
.end method

.method public removeItem(I)V
    .locals 1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->b(I)I

    move-result p1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lmiuix/appcompat/internal/view/menu/i;->a(IZ)V

    return-void
.end method

.method public setGroupCheckable(IZZ)V
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/view/menu/k;

    invoke-virtual {v1}, Lmiuix/appcompat/internal/view/menu/k;->getGroupId()I

    move-result v2

    if-ne v2, p1, :cond_0

    invoke-virtual {v1, p3}, Lmiuix/appcompat/internal/view/menu/k;->c(Z)V

    invoke-virtual {v1, p2}, Lmiuix/appcompat/internal/view/menu/k;->setCheckable(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setGroupEnabled(IZ)V
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/view/menu/k;

    invoke-virtual {v1}, Lmiuix/appcompat/internal/view/menu/k;->getGroupId()I

    move-result v2

    if-ne v2, p1, :cond_0

    invoke-virtual {v1, p2}, Lmiuix/appcompat/internal/view/menu/k;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setGroupVisible(IZ)V
    .locals 5

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/appcompat/internal/view/menu/k;

    invoke-virtual {v2}, Lmiuix/appcompat/internal/view/menu/k;->getGroupId()I

    move-result v4

    if-ne v4, p1, :cond_0

    invoke-virtual {v2, p2}, Lmiuix/appcompat/internal/view/menu/k;->e(Z)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v3

    goto :goto_0

    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {p0, v3}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    :cond_2
    return-void
.end method

.method public setQwertyMode(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/view/menu/i;->mQwertyMode:Z

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    return-void
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
