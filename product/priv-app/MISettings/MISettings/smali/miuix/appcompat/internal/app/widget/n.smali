.class Lmiuix/appcompat/internal/app/widget/n;
.super Lmiuix/animation/g/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->e(Z)Lmiuix/animation/f/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiuix/animation/g/b<",
        "Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic b:Lmiuix/appcompat/internal/view/menu/action/ActionMenuView;

.field final synthetic c:F

.field final synthetic d:I

.field final synthetic e:Z

.field final synthetic f:I

.field final synthetic g:I

.field final synthetic h:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;


# direct methods
.method constructor <init>(Lmiuix/appcompat/internal/app/widget/ActionBarContextView;Ljava/lang/String;Lmiuix/appcompat/internal/view/menu/action/ActionMenuView;FIZII)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/n;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    iput-object p3, p0, Lmiuix/appcompat/internal/app/widget/n;->b:Lmiuix/appcompat/internal/view/menu/action/ActionMenuView;

    iput p4, p0, Lmiuix/appcompat/internal/app/widget/n;->c:F

    iput p5, p0, Lmiuix/appcompat/internal/app/widget/n;->d:I

    iput-boolean p6, p0, Lmiuix/appcompat/internal/app/widget/n;->e:Z

    iput p7, p0, Lmiuix/appcompat/internal/app/widget/n;->f:I

    iput p8, p0, Lmiuix/appcompat/internal/app/widget/n;->g:I

    invoke-direct {p0, p2}, Lmiuix/animation/g/b;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;)F
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;F)V
    .locals 0

    check-cast p1, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {p0, p1, p2}, Lmiuix/appcompat/internal/app/widget/n;->a(Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;F)V

    return-void
.end method

.method public a(Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;F)V
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/n;->b:Lmiuix/appcompat/internal/view/menu/action/ActionMenuView;

    if-eqz v0, :cond_0

    iget v1, p0, Lmiuix/appcompat/internal/app/widget/n;->c:F

    iget v2, p0, Lmiuix/appcompat/internal/app/widget/n;->d:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    sub-float/2addr v1, p2

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setTranslationY(F)V

    :cond_0
    float-to-int v0, p2

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->a(I)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/n;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    invoke-static {p1}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->f(Lmiuix/appcompat/internal/app/widget/ActionBarContextView;)Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/n;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    iget-boolean p2, p0, Lmiuix/appcompat/internal/app/widget/n;->e:Z

    invoke-virtual {p1, p2}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->g(Z)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/n;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    const/4 p2, 0x1

    invoke-static {p1, p2}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->a(Lmiuix/appcompat/internal/app/widget/ActionBarContextView;Z)Z

    return-void

    :cond_1
    iget p1, p0, Lmiuix/appcompat/internal/app/widget/n;->f:I

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/n;->g:I

    if-ne p1, v0, :cond_2

    const/high16 p1, 0x3f800000    # 1.0f

    goto :goto_0

    :cond_2
    int-to-float v1, v0

    sub-float/2addr p2, v1

    sub-int/2addr p1, v0

    int-to-float p1, p1

    div-float p1, p2, p1

    :goto_0
    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/n;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/n;->e:Z

    invoke-virtual {p2, v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->b(ZF)V

    return-void
.end method

.method public bridge synthetic b(Ljava/lang/Object;)F
    .locals 0

    check-cast p1, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/n;->a(Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;)F

    move-result p1

    return p1
.end method
