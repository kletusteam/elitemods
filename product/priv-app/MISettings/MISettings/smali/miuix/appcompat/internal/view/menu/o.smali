.class public Lmiuix/appcompat/internal/view/menu/o;
.super Lmiuix/appcompat/internal/view/menu/i;

# interfaces
.implements Landroid/view/SubMenu;


# instance fields
.field private mItem:Lmiuix/appcompat/internal/view/menu/k;

.field private mParentMenu:Lmiuix/appcompat/internal/view/menu/i;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lmiuix/appcompat/internal/view/menu/i;Lmiuix/appcompat/internal/view/menu/k;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lmiuix/appcompat/internal/view/menu/o;->mParentMenu:Lmiuix/appcompat/internal/view/menu/i;

    iput-object p3, p0, Lmiuix/appcompat/internal/view/menu/o;->mItem:Lmiuix/appcompat/internal/view/menu/k;

    return-void
.end method


# virtual methods
.method public a(Lmiuix/appcompat/internal/view/menu/i$a;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/o;->mParentMenu:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/view/menu/i;->a(Lmiuix/appcompat/internal/view/menu/i$a;)V

    return-void
.end method

.method public a(Lmiuix/appcompat/internal/view/menu/i;Landroid/view/MenuItem;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Lmiuix/appcompat/internal/view/menu/i;->a(Lmiuix/appcompat/internal/view/menu/i;Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/o;->mParentMenu:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v0, p1, p2}, Lmiuix/appcompat/internal/view/menu/i;->a(Lmiuix/appcompat/internal/view/menu/i;Landroid/view/MenuItem;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public a(Lmiuix/appcompat/internal/view/menu/k;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/o;->mParentMenu:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/view/menu/i;->a(Lmiuix/appcompat/internal/view/menu/k;)Z

    move-result p1

    return p1
.end method

.method public b(Lmiuix/appcompat/internal/view/menu/k;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/o;->mParentMenu:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/view/menu/i;->b(Lmiuix/appcompat/internal/view/menu/k;)Z

    move-result p1

    return p1
.end method

.method public clearHeader()V
    .locals 0

    return-void
.end method

.method public getItem()Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/o;->mItem:Lmiuix/appcompat/internal/view/menu/k;

    return-object v0
.end method

.method public l()Lmiuix/appcompat/internal/view/menu/i;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/o;->mParentMenu:Lmiuix/appcompat/internal/view/menu/i;

    return-object v0
.end method

.method public n()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/o;->mParentMenu:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/i;->n()Z

    move-result v0

    return v0
.end method

.method public o()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/o;->mParentMenu:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/i;->o()Z

    move-result v0

    return v0
.end method

.method public r()Landroid/view/Menu;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/o;->mParentMenu:Lmiuix/appcompat/internal/view/menu/i;

    return-object v0
.end method

.method public setHeaderIcon(I)Landroid/view/SubMenu;
    .locals 1

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/i;->d()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->a(Landroid/graphics/drawable/Drawable;)Lmiuix/appcompat/internal/view/menu/i;

    return-object p0
.end method

.method public setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->a(Landroid/graphics/drawable/Drawable;)Lmiuix/appcompat/internal/view/menu/i;

    return-object p0
.end method

.method public setHeaderTitle(I)Landroid/view/SubMenu;
    .locals 1

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/i;->d()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->a(Ljava/lang/CharSequence;)Lmiuix/appcompat/internal/view/menu/i;

    return-object p0
.end method

.method public setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->a(Ljava/lang/CharSequence;)Lmiuix/appcompat/internal/view/menu/i;

    return-object p0
.end method

.method public setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->a(Landroid/view/View;)Lmiuix/appcompat/internal/view/menu/i;

    return-object p0
.end method

.method public setIcon(I)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/o;->mItem:Lmiuix/appcompat/internal/view/menu/k;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/view/menu/k;->setIcon(I)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/o;->mItem:Lmiuix/appcompat/internal/view/menu/k;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/view/menu/k;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setQwertyMode(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/o;->mParentMenu:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/view/menu/i;->setQwertyMode(Z)V

    return-void
.end method
