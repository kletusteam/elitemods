.class public Lmiuix/appcompat/internal/app/widget/u;
.super Lmiuix/appcompat/app/d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/internal/app/widget/u$b;,
        Lmiuix/appcompat/internal/app/widget/u$a;
    }
.end annotation


# static fields
.field private static a:Landroidx/appcompat/app/ActionBar$d;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

.field private G:Ld/b/b/c/b$a;

.field private H:Lmiuix/animation/k;

.field private I:Lmiuix/animation/k;

.field private J:I

.field private K:Z

.field private L:I

.field b:Landroid/view/ActionMode;

.field private c:Landroid/content/Context;

.field private d:Landroid/content/Context;

.field private e:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

.field private f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

.field private g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

.field private h:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

.field private i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

.field private j:Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

.field private k:Landroid/view/View;

.field private l:Landroid/view/View$OnClickListener;

.field private m:Lmiuix/appcompat/internal/app/widget/J;

.field private n:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

.field private o:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

.field private p:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

.field private q:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

.field private r:Lmiuix/appcompat/internal/app/widget/K;

.field private s:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lmiuix/appcompat/internal/app/widget/u$a;",
            ">;"
        }
    .end annotation
.end field

.field private t:Lmiuix/appcompat/internal/app/widget/u$a;

.field private u:Landroidx/fragment/app/FragmentManager;

.field private v:I

.field private w:Z

.field private x:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroidx/appcompat/app/ActionBar$a;",
            ">;"
        }
    .end annotation
.end field

.field private y:I

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmiuix/appcompat/internal/app/widget/q;

    invoke-direct {v0}, Lmiuix/appcompat/internal/app/widget/q;-><init>()V

    sput-object v0, Lmiuix/appcompat/internal/app/widget/u;->a:Landroidx/appcompat/app/ActionBar$d;

    return-void
.end method

.method public constructor <init>(Landroidx/fragment/app/Fragment;)V
    .locals 1

    invoke-direct {p0}, Lmiuix/appcompat/app/d;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->s:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/u;->v:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->x:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/u;->z:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/u;->D:Z

    new-instance v0, Lmiuix/appcompat/internal/app/widget/r;

    invoke-direct {v0, p0}, Lmiuix/appcompat/internal/app/widget/r;-><init>(Lmiuix/appcompat/internal/app/widget/u;)V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->G:Ld/b/b/c/b$a;

    move-object v0, p1

    check-cast v0, Lmiuix/appcompat/app/q;

    invoke-interface {v0}, Lmiuix/appcompat/app/q;->b()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->c:Landroid/content/Context;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->u:Landroidx/fragment/app/FragmentManager;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/u;->a(Landroid/view/ViewGroup;)V

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setWindowTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public constructor <init>(Lmiuix/appcompat/app/AppCompatActivity;Landroid/view/ViewGroup;)V
    .locals 1

    invoke-direct {p0}, Lmiuix/appcompat/app/d;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->s:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/u;->v:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->x:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/u;->z:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/u;->D:Z

    new-instance v0, Lmiuix/appcompat/internal/app/widget/r;

    invoke-direct {v0, p0}, Lmiuix/appcompat/internal/app/widget/r;-><init>(Lmiuix/appcompat/internal/app/widget/u;)V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->G:Ld/b/b/c/b$a;

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->c:Landroid/content/Context;

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->u:Landroidx/fragment/app/FragmentManager;

    invoke-virtual {p0, p2}, Lmiuix/appcompat/internal/app/widget/u;->a(Landroid/view/ViewGroup;)V

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p1}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p2, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setWindowTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private E()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->t:Lmiuix/appcompat/internal/app/widget/u$a;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/u;->a(Landroidx/appcompat/app/ActionBar$c;)V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->n:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->a()V

    :cond_1
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->o:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->a()V

    :cond_2
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->p:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->a()V

    :cond_3
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->q:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->a()V

    :cond_4
    const/4 v0, -0x1

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/u;->v:I

    return-void
.end method

.method private F()V
    .locals 5

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->n:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->k()V

    return-void

    :cond_0
    new-instance v0, Lmiuix/appcompat/internal/app/widget/CollapseTabContainer;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/u;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lmiuix/appcompat/internal/app/widget/CollapseTabContainer;-><init>(Landroid/content/Context;)V

    new-instance v1, Lmiuix/appcompat/internal/app/widget/ExpandTabContainer;

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->c:Landroid/content/Context;

    invoke-direct {v1, v2}, Lmiuix/appcompat/internal/app/widget/ExpandTabContainer;-><init>(Landroid/content/Context;)V

    new-instance v2, Lmiuix/appcompat/internal/app/widget/SecondaryCollapseTabContainer;

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/u;->c:Landroid/content/Context;

    invoke-direct {v2, v3}, Lmiuix/appcompat/internal/app/widget/SecondaryCollapseTabContainer;-><init>(Landroid/content/Context;)V

    new-instance v3, Lmiuix/appcompat/internal/app/widget/SecondaryExpandTabContainer;

    iget-object v4, p0, Lmiuix/appcompat/internal/app/widget/u;->c:Landroid/content/Context;

    invoke-direct {v3, v4}, Lmiuix/appcompat/internal/app/widget/SecondaryExpandTabContainer;-><init>(Landroid/content/Context;)V

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    invoke-virtual {v1, v4}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    invoke-virtual {v2, v4}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    invoke-virtual {v3, v4}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    iget-object v4, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v4, v0, v1, v2, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setEmbeddedTabView(Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;)V

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->setEmbeded(Z)V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->n:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/u;->o:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    iput-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->p:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    iput-object v3, p0, Lmiuix/appcompat/internal/app/widget/u;->q:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    return-void
.end method

.method private G()I
    .locals 6

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/u;->g()I

    move-result v0

    const v1, 0x8000

    and-int/2addr v0, v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v3

    :goto_0
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/u;->g()I

    move-result v4

    const/16 v5, 0x4000

    and-int/2addr v4, v5

    if-eqz v4, :cond_1

    goto :goto_1

    :cond_1
    move v2, v3

    :goto_1
    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    move v1, v3

    :goto_2
    if-eqz v2, :cond_3

    move v3, v5

    :cond_3
    or-int v0, v1, v3

    return v0
.end method

.method private H()I
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v0

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    instance-of v2, v1, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    if-eqz v2, :cond_0

    check-cast v1, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    invoke-virtual {v1}, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;->d()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;->getCollapsedHeight()I

    move-result v0

    :cond_0
    return v0
.end method

.method private I()V
    .locals 4

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->e:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/u;->F:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v0

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/u;->e:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v1

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/u;->F:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v1, v2, v3}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v1

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->F:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    invoke-virtual {v2, v0, v1}, Landroid/widget/FrameLayout;->measure(II)V

    return-void
.end method

.method private a(ZLjava/lang/String;Lmiuix/animation/b/a;)Lmiuix/animation/k;
    .locals 9

    invoke-direct {p0}, Lmiuix/appcompat/internal/app/widget/u;->H()I

    move-result v0

    const-wide/16 v1, 0x0

    const/4 v3, 0x2

    const/4 v4, -0x2

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-eqz p1, :cond_1

    new-instance p1, Lmiuix/animation/a/a;

    invoke-direct {p1}, Lmiuix/animation/a/a;-><init>()V

    new-array v0, v3, [F

    fill-array-data v0, :array_0

    invoke-static {v4, v0}, Lmiuix/animation/h/c;->c(I[F)Lmiuix/animation/h/c$a;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmiuix/animation/a/a;->a(Lmiuix/animation/h/c$a;)Lmiuix/animation/a/a;

    new-instance v0, Lmiuix/animation/b/a;

    invoke-direct {v0, p2}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    sget-object v3, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    invoke-virtual {v0, v3, v1, v2}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v1, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v1, v2, v3}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    new-array v1, v6, [Landroid/view/View;

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    aput-object v2, v1, v5

    invoke-static {v1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v1

    if-eqz p3, :cond_0

    invoke-virtual {p3, p2}, Lmiuix/animation/b/a;->f(Ljava/lang/Object;)V

    invoke-interface {v1, p3}, Lmiuix/animation/k;->b(Ljava/lang/Object;)Lmiuix/animation/k;

    :cond_0
    new-array p2, v6, [Lmiuix/animation/a/a;

    aput-object p1, p2, v5

    invoke-interface {v1, v0, p2}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    goto :goto_0

    :cond_1
    new-instance p1, Lmiuix/animation/a/a;

    invoke-direct {p1}, Lmiuix/animation/a/a;-><init>()V

    new-array v3, v3, [F

    fill-array-data v3, :array_1

    invoke-static {v4, v3}, Lmiuix/animation/h/c;->c(I[F)Lmiuix/animation/h/c$a;

    move-result-object v3

    invoke-virtual {p1, v3}, Lmiuix/animation/a/a;->a(Lmiuix/animation/h/c$a;)Lmiuix/animation/a/a;

    new-array v3, v6, [Lmiuix/animation/e/b;

    new-instance v4, Lmiuix/appcompat/internal/app/widget/u$b;

    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-direct {v4, v7, p0}, Lmiuix/appcompat/internal/app/widget/u$b;-><init>(Landroid/view/View;Lmiuix/appcompat/internal/app/widget/u;)V

    aput-object v4, v3, v5

    invoke-virtual {p1, v3}, Lmiuix/animation/a/a;->a([Lmiuix/animation/e/b;)Lmiuix/animation/a/a;

    new-instance v3, Lmiuix/animation/b/a;

    invoke-direct {v3, p2}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    sget-object v4, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    add-int/lit8 v0, v0, 0x64

    int-to-double v7, v0

    invoke-virtual {v3, v4, v7, v8}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v0, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    invoke-virtual {v3, v0, v1, v2}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    new-array v0, v6, [Landroid/view/View;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    aput-object v1, v0, v5

    invoke-static {v0}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v1

    if-eqz p3, :cond_2

    invoke-virtual {p3, p2}, Lmiuix/animation/b/a;->f(Ljava/lang/Object;)V

    invoke-interface {v1, p3}, Lmiuix/animation/k;->b(Ljava/lang/Object;)Lmiuix/animation/k;

    :cond_2
    new-array p2, v6, [Lmiuix/animation/a/a;

    aput-object p1, p2, v5

    invoke-interface {v1, v3, p2}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    :goto_0
    return-object v1

    nop

    :array_0
    .array-data 4
        0x3f666666    # 0.9f
        0x3e800000    # 0.25f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3eb33333    # 0.35f
    .end array-data
.end method

.method private a(ZLjava/lang/String;Lmiuix/animation/b/a;Lmiuix/animation/b/a;)Lmiuix/animation/k;
    .locals 9

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v0

    const-wide/16 v1, 0x0

    const/4 v3, 0x2

    const/4 v4, -0x2

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-eqz p1, :cond_2

    new-instance p1, Lmiuix/animation/a/a;

    invoke-direct {p1}, Lmiuix/animation/a/a;-><init>()V

    new-array v0, v3, [F

    fill-array-data v0, :array_0

    invoke-static {v4, v0}, Lmiuix/animation/h/c;->c(I[F)Lmiuix/animation/h/c$a;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmiuix/animation/a/a;->a(Lmiuix/animation/h/c$a;)Lmiuix/animation/a/a;

    if-nez p4, :cond_0

    new-instance p4, Lmiuix/animation/b/a;

    invoke-direct {p4, p2}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    sget-object v0, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    invoke-virtual {p4, v0, v1, v2}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v0, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    invoke-virtual {p4, v0, v1, v2}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    :cond_0
    new-array v0, v6, [Landroid/view/View;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/u;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    aput-object v1, v0, v5

    invoke-static {v0}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v0

    if-eqz p3, :cond_1

    invoke-virtual {p3, p2}, Lmiuix/animation/b/a;->f(Ljava/lang/Object;)V

    invoke-interface {v0, p3}, Lmiuix/animation/k;->b(Ljava/lang/Object;)Lmiuix/animation/k;

    :cond_1
    new-array p2, v6, [Lmiuix/animation/a/a;

    aput-object p1, p2, v5

    invoke-interface {v0, p4, p2}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    goto :goto_0

    :cond_2
    new-instance p1, Lmiuix/animation/a/a;

    invoke-direct {p1}, Lmiuix/animation/a/a;-><init>()V

    new-array v3, v3, [F

    fill-array-data v3, :array_1

    invoke-static {v4, v3}, Lmiuix/animation/h/c;->c(I[F)Lmiuix/animation/h/c$a;

    move-result-object v3

    invoke-virtual {p1, v3}, Lmiuix/animation/a/a;->a(Lmiuix/animation/h/c$a;)Lmiuix/animation/a/a;

    new-array v3, v6, [Lmiuix/animation/e/b;

    new-instance v4, Lmiuix/appcompat/internal/app/widget/u$b;

    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/u;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-direct {v4, v7, p0}, Lmiuix/appcompat/internal/app/widget/u$b;-><init>(Landroid/view/View;Lmiuix/appcompat/internal/app/widget/u;)V

    aput-object v4, v3, v5

    invoke-virtual {p1, v3}, Lmiuix/animation/a/a;->a([Lmiuix/animation/e/b;)Lmiuix/animation/a/a;

    if-nez p4, :cond_3

    new-instance p4, Lmiuix/animation/b/a;

    invoke-direct {p4, p2}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    sget-object v3, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    neg-int v0, v0

    add-int/lit8 v0, v0, -0x64

    int-to-double v7, v0

    invoke-virtual {p4, v3, v7, v8}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    sget-object v0, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    invoke-virtual {p4, v0, v1, v2}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    :cond_3
    new-array v0, v6, [Landroid/view/View;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/u;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    aput-object v1, v0, v5

    invoke-static {v0}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v0

    if-eqz p3, :cond_4

    invoke-virtual {p3, p2}, Lmiuix/animation/b/a;->f(Ljava/lang/Object;)V

    invoke-interface {v0, p3}, Lmiuix/animation/k;->b(Ljava/lang/Object;)Lmiuix/animation/k;

    :cond_4
    new-array p2, v6, [Lmiuix/animation/a/a;

    aput-object p1, p2, v5

    invoke-interface {v0, p4, p2}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    :goto_0
    return-object v0

    :array_0
    .array-data 4
        0x3f666666    # 0.9f
        0x3e800000    # 0.25f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3eb33333    # 0.35f
    .end array-data
.end method

.method private a(Landroidx/appcompat/app/ActionBar$c;I)V
    .locals 1

    check-cast p1, Lmiuix/appcompat/internal/app/widget/u$a;

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/u$a;->g()Landroidx/appcompat/app/ActionBar$d;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, p2}, Lmiuix/appcompat/internal/app/widget/u$a;->a(I)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->s:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    :goto_0
    add-int/lit8 p2, p2, 0x1

    if-ge p2, p1, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/u$a;

    invoke-virtual {v0, p2}, Lmiuix/appcompat/internal/app/widget/u$a;->a(I)V

    goto :goto_0

    :cond_0
    return-void

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Action Bar Tab must have a Callback"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private a(ZLmiuix/animation/b/a;)V
    .locals 6

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->H:Lmiuix/animation/k;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmiuix/animation/k;->b()Lmiuix/animation/b/a;

    move-result-object v0

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->H:Lmiuix/animation/k;

    invoke-interface {v2}, Lmiuix/animation/g;->cancel()V

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/u;->B()Z

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_2

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    move p1, v3

    goto :goto_2

    :cond_2
    :goto_1
    const/4 p1, 0x1

    :goto_2
    const/16 v2, 0x8

    const/4 v4, 0x0

    if-eqz p1, :cond_3

    const-string v5, "HideActionBar"

    invoke-direct {p0, v3, v5, v0, p2}, Lmiuix/appcompat/internal/app/widget/u;->a(ZLjava/lang/String;Lmiuix/animation/b/a;Lmiuix/animation/b/a;)Lmiuix/animation/k;

    move-result-object p2

    iput-object p2, p0, Lmiuix/appcompat/internal/app/widget/u;->H:Lmiuix/animation/k;

    goto :goto_3

    :cond_3
    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/u;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p2, v0}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/u;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p2, v4}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setAlpha(F)V

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/u;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p2, v2}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setVisibility(I)V

    :goto_3
    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz p2, :cond_6

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/u;->I:Lmiuix/animation/k;

    if-eqz p2, :cond_4

    invoke-interface {p2}, Lmiuix/animation/k;->b()Lmiuix/animation/b/a;

    move-result-object v1

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/u;->I:Lmiuix/animation/k;

    invoke-interface {p2}, Lmiuix/animation/g;->cancel()V

    :cond_4
    if-eqz p1, :cond_5

    const-string p1, "SpliterHide"

    invoke-direct {p0, v3, p1, v1}, Lmiuix/appcompat/internal/app/widget/u;->a(ZLjava/lang/String;Lmiuix/animation/b/a;)Lmiuix/animation/k;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->I:Lmiuix/animation/k;

    goto :goto_4

    :cond_5
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-direct {p0}, Lmiuix/appcompat/internal/app/widget/u;->H()I

    move-result p2

    int-to-float p2, p2

    invoke-virtual {p1, p2}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p1, v4}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setAlpha(F)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p1, v2}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setVisibility(I)V

    :goto_4
    invoke-direct {p0, v3}, Lmiuix/appcompat/internal/app/widget/u;->n(Z)V

    :cond_6
    return-void
.end method

.method static synthetic a(Lmiuix/appcompat/internal/app/widget/u;)Z
    .locals 0

    iget-boolean p0, p0, Lmiuix/appcompat/internal/app/widget/u;->D:Z

    return p0
.end method

.method private static a(ZZZ)Z
    .locals 1

    const/4 v0, 0x1

    if-eqz p2, :cond_0

    return v0

    :cond_0
    if-nez p0, :cond_2

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    return v0

    :cond_2
    :goto_0
    const/4 p0, 0x0

    return p0
.end method

.method static synthetic b(Lmiuix/appcompat/internal/app/widget/u;)Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/app/widget/u;->j:Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    return-object p0
.end method

.method private b(ZLmiuix/animation/b/a;)V
    .locals 7

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->H:Lmiuix/animation/k;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmiuix/animation/k;->b()Lmiuix/animation/b/a;

    move-result-object v0

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->H:Lmiuix/animation/k;

    invoke-interface {v2}, Lmiuix/animation/g;->cancel()V

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/u;->B()Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-nez v2, :cond_2

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    move p1, v3

    goto :goto_2

    :cond_2
    :goto_1
    move p1, v4

    :goto_2
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    iget-object v5, p0, Lmiuix/appcompat/internal/app/widget/u;->b:Landroid/view/ActionMode;

    instance-of v5, v5, Lmiuix/view/f;

    if-eqz v5, :cond_3

    const/16 v5, 0x8

    goto :goto_3

    :cond_3
    move v5, v3

    :goto_3
    invoke-virtual {v2, v5}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setVisibility(I)V

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    if-eqz p1, :cond_4

    const-string v6, "ShowActionBar"

    invoke-direct {p0, v4, v6, v0, p2}, Lmiuix/appcompat/internal/app/widget/u;->a(ZLjava/lang/String;Lmiuix/animation/b/a;Lmiuix/animation/b/a;)Lmiuix/animation/k;

    move-result-object p2

    iput-object p2, p0, Lmiuix/appcompat/internal/app/widget/u;->H:Lmiuix/animation/k;

    goto :goto_4

    :cond_4
    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/u;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p2, v5}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/u;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p2, v2}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setAlpha(F)V

    :goto_4
    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz p2, :cond_8

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/u;->I:Lmiuix/animation/k;

    if-eqz p2, :cond_5

    invoke-interface {p2}, Lmiuix/animation/k;->b()Lmiuix/animation/b/a;

    move-result-object v1

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/u;->I:Lmiuix/animation/k;

    invoke-interface {p2}, Lmiuix/animation/g;->cancel()V

    :cond_5
    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p2, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setVisibility(I)V

    if-eqz p1, :cond_6

    const-string p1, "SpliterShow"

    invoke-direct {p0, v4, p1, v1}, Lmiuix/appcompat/internal/app/widget/u;->a(ZLjava/lang/String;Lmiuix/animation/b/a;)Lmiuix/animation/k;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->I:Lmiuix/animation/k;

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->r()Z

    move-result p1

    if-eqz p1, :cond_7

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result p1

    if-lez p1, :cond_7

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p1, v3}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_7

    instance-of p2, p1, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    if-eqz p2, :cond_7

    move-object p2, p1

    check-cast p2, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    invoke-virtual {p2}, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;->d()Z

    move-result p2

    xor-int/2addr p2, v4

    if-eqz p2, :cond_7

    check-cast p1, Lmiuix/appcompat/internal/view/menu/action/ActionMenuView;

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->startLayoutAnimation()V

    goto :goto_5

    :cond_6
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p1, v5}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p1, v2}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setAlpha(F)V

    :cond_7
    :goto_5
    invoke-direct {p0, v4}, Lmiuix/appcompat/internal/app/widget/u;->n(Z)V

    :cond_8
    return-void
.end method

.method private c(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 2

    instance-of v0, p1, Lmiuix/view/f$a;

    if-eqz v0, :cond_0

    new-instance v0, Ld/b/b/c/d;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/u;->c:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Ld/b/b/c/d;-><init>(Landroid/content/Context;Landroid/view/ActionMode$Callback;)V

    goto :goto_0

    :cond_0
    new-instance v0, Ld/b/b/c/c;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/u;->c:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Ld/b/b/c/c;-><init>(Landroid/content/Context;Landroid/view/ActionMode$Callback;)V

    :goto_0
    return-object v0
.end method

.method static synthetic c(Lmiuix/appcompat/internal/app/widget/u;)Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/app/widget/u;->n:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    return-object p0
.end method

.method private c(ZLmiuix/animation/b/a;)V
    .locals 3

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/u;->A:Z

    iget-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/u;->B:Z

    iget-boolean v2, p0, Lmiuix/appcompat/internal/app/widget/u;->C:Z

    invoke-static {v0, v1, v2}, Lmiuix/appcompat/internal/app/widget/u;->a(ZZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/u;->D:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/u;->D:Z

    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/internal/app/widget/u;->b(ZLmiuix/animation/b/a;)V

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/u;->D:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/u;->D:Z

    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/internal/app/widget/u;->a(ZLmiuix/animation/b/a;)V

    :cond_1
    :goto_0
    return-void
.end method

.method static synthetic d(Lmiuix/appcompat/internal/app/widget/u;)Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/app/widget/u;->o:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    return-object p0
.end method

.method static synthetic e(Lmiuix/appcompat/internal/app/widget/u;)Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/app/widget/u;->p:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    return-object p0
.end method

.method private k(Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiuix/appcompat/internal/app/widget/u;->a(ZLmiuix/animation/b/a;)V

    return-void
.end method

.method private l(Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiuix/appcompat/internal/app/widget/u;->b(ZLmiuix/animation/b/a;)V

    return-void
.end method

.method private m(Z)V
    .locals 4

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setTabContainer(Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->n:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/u;->o:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->p:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/u;->q:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p1, v0, v1, v2, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setEmbeddedTabView(Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;)V

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/u;->u()I

    move-result p1

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x2

    if-ne p1, v2, :cond_0

    move p1, v0

    goto :goto_0

    :cond_0
    move p1, v1

    :goto_0
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->n:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    const/16 v3, 0x8

    if-eqz v2, :cond_2

    if-eqz p1, :cond_1

    invoke-virtual {v2, v1}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    goto :goto_1

    :cond_1
    invoke-virtual {v2, v3}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    :goto_1
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->n:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {v2, v0}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->setEmbeded(Z)V

    :cond_2
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->o:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v2, :cond_4

    if-eqz p1, :cond_3

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->q:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {v2, v1}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    goto :goto_2

    :cond_3
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->q:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {v2, v3}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    :goto_2
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->q:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {v2, v0}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->setEmbeded(Z)V

    :cond_4
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->p:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v2, :cond_6

    if-eqz p1, :cond_5

    invoke-virtual {v2, v1}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    goto :goto_3

    :cond_5
    invoke-virtual {v2, v3}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    :goto_3
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->p:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {v2, v0}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->setEmbeded(Z)V

    :cond_6
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->q:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v2, :cond_8

    if-eqz p1, :cond_7

    invoke-virtual {v2, v1}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    goto :goto_4

    :cond_7
    invoke-virtual {v2, v3}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    :goto_4
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->q:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->setEmbeded(Z)V

    :cond_8
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p1, v1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setCollapsable(Z)V

    return-void
.end method

.method private n(Z)V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->j:Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->j:Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->k:Landroid/view/View;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->e:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->l:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->a(Landroid/view/View$OnClickListener;)Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$b;

    move-result-object p1

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$b;->b()Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->e:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->a(Landroid/view/View$OnClickListener;)Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$b;

    move-result-object p1

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$b;->a()Landroid/animation/Animator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    :cond_1
    :goto_0
    return-void
.end method

.method private o(Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiuix/appcompat/internal/app/widget/u;->c(ZLmiuix/animation/b/a;)V

    return-void
.end method

.method static synthetic q()Landroidx/appcompat/app/ActionBar$d;
    .locals 1

    sget-object v0, Lmiuix/appcompat/internal/app/widget/u;->a:Landroidx/appcompat/app/ActionBar$d;

    return-object v0
.end method


# virtual methods
.method public A()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->g()Z

    move-result v0

    return v0
.end method

.method B()Z
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/u;->E:Z

    goto/32 :goto_0

    nop
.end method

.method public C()V
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/u;->z()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/u;->y()V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add tab directly in fragment view pager mode!\n Please using addFragmentTab()."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method D()V
    .locals 6

    goto/32 :goto_19

    nop

    :goto_0
    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/u;->C:Z

    goto/32 :goto_11

    nop

    :goto_1
    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/u;->r:Lmiuix/appcompat/internal/app/widget/K;

    goto/32 :goto_8

    nop

    :goto_2
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_26

    nop

    :goto_3
    move v0, v1

    :goto_4
    goto/32 :goto_f

    nop

    :goto_5
    check-cast v2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    goto/32 :goto_1c

    nop

    :goto_6
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_7
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_20

    nop

    :goto_8
    instance-of v3, v3, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    goto/32 :goto_1f

    nop

    :goto_9
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->r:Lmiuix/appcompat/internal/app/widget/K;

    goto/32 :goto_5

    nop

    :goto_a
    invoke-virtual {v2, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->setResizable(Z)V

    :goto_b
    goto/32 :goto_21

    nop

    :goto_c
    if-nez v4, :cond_0

    goto/32 :goto_28

    :cond_0
    goto/32 :goto_27

    nop

    :goto_d
    invoke-virtual {v2, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->setExpandState(I)V

    goto/32 :goto_9

    nop

    :goto_e
    if-nez v3, :cond_1

    goto/32 :goto_17

    :cond_1
    goto/32 :goto_2

    nop

    :goto_f
    invoke-virtual {v2, v3, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->a(ZZ)V

    :goto_10
    goto/32 :goto_24

    nop

    :goto_11
    const/4 v1, 0x0

    goto/32 :goto_18

    nop

    :goto_12
    invoke-virtual {p0, v1}, Lmiuix/appcompat/internal/app/widget/u;->g(Z)V

    goto/32 :goto_16

    nop

    :goto_13
    const v5, 0x8000

    goto/32 :goto_22

    nop

    :goto_14
    check-cast v2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    goto/32 :goto_2b

    nop

    :goto_15
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getImportantForAccessibility()I

    move-result v2

    goto/32 :goto_2a

    nop

    :goto_16
    goto :goto_b

    :goto_17
    goto/32 :goto_14

    nop

    :goto_18
    invoke-direct {p0, v1}, Lmiuix/appcompat/internal/app/widget/u;->o(Z)V

    goto/32 :goto_29

    nop

    :goto_19
    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/u;->C:Z

    goto/32 :goto_1e

    nop

    :goto_1a
    instance-of v3, v2, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    goto/32 :goto_e

    nop

    :goto_1b
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_1

    nop

    :goto_1c
    iget-boolean v3, p0, Lmiuix/appcompat/internal/app/widget/u;->K:Z

    goto/32 :goto_a

    nop

    :goto_1d
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->r:Lmiuix/appcompat/internal/app/widget/K;

    goto/32 :goto_1a

    nop

    :goto_1e
    if-eqz v0, :cond_2

    goto/32 :goto_10

    :cond_2
    goto/32 :goto_6

    nop

    :goto_1f
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/u;->g()I

    move-result v4

    goto/32 :goto_13

    nop

    :goto_20
    const/4 v3, 0x4

    goto/32 :goto_23

    nop

    :goto_21
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_15

    nop

    :goto_22
    and-int/2addr v4, v5

    goto/32 :goto_c

    nop

    :goto_23
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setImportantForAccessibility(I)V

    goto/32 :goto_1b

    nop

    :goto_24
    return-void

    :goto_25
    iput-boolean v2, p0, Lmiuix/appcompat/internal/app/widget/u;->K:Z

    goto/32 :goto_1d

    nop

    :goto_26
    invoke-virtual {v2, v1, v0, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setExpandState(IZZ)V

    goto/32 :goto_12

    nop

    :goto_27
    goto/16 :goto_4

    :goto_28
    goto/32 :goto_3

    nop

    :goto_29
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/u;->t()I

    move-result v2

    goto/32 :goto_2d

    nop

    :goto_2a
    iput v2, p0, Lmiuix/appcompat/internal/app/widget/u;->L:I

    goto/32 :goto_7

    nop

    :goto_2b
    iget v3, p0, Lmiuix/appcompat/internal/app/widget/u;->J:I

    goto/32 :goto_d

    nop

    :goto_2c
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/u;->A()Z

    move-result v2

    goto/32 :goto_25

    nop

    :goto_2d
    iput v2, p0, Lmiuix/appcompat/internal/app/widget/u;->J:I

    goto/32 :goto_2c

    nop
.end method

.method public a(Ljava/lang/String;Landroidx/appcompat/app/ActionBar$c;Ljava/lang/Class;Landroid/os/Bundle;Z)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroidx/appcompat/app/ActionBar$c;",
            "Ljava/lang/Class<",
            "+",
            "Landroidx/fragment/app/Fragment;",
            ">;",
            "Landroid/os/Bundle;",
            "Z)I"
        }
    .end annotation

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->m:Lmiuix/appcompat/internal/app/widget/J;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lmiuix/appcompat/internal/app/widget/J;->a(Ljava/lang/String;Landroidx/appcompat/app/ActionBar$c;Ljava/lang/Class;Landroid/os/Bundle;Z)I

    move-result p1

    return p1
.end method

.method public a(Landroid/view/ActionMode$Callback;)Lmiuix/appcompat/internal/app/widget/K;
    .locals 1

    instance-of p1, p1, Lmiuix/view/f$a;

    if-eqz p1, :cond_3

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->F:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/u;->r()Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->F:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->e:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->getBaseInnerInsets()Landroid/graphics/Rect;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->F:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget p1, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->setStatusBarPaddingTop(I)V

    :cond_1
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->e:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->F:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eq p1, v0, :cond_2

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->e:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->F:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    :cond_2
    invoke-direct {p0}, Lmiuix/appcompat/internal/app/widget/u;->I()V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->F:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->a(Lmiuix/view/a;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->F:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    goto :goto_0

    :cond_3
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    if-eqz p1, :cond_4

    :goto_0
    return-object p1

    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "not set windowSplitActionBar true in activity style!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(II)V
    .locals 4

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getDisplayOptions()I

    move-result v0

    and-int/lit8 v1, p2, 0x4

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    iput-boolean v2, p0, Lmiuix/appcompat/internal/app/widget/u;->w:Z

    :cond_0
    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    and-int v3, p1, p2

    not-int p2, p2

    and-int/2addr p2, v0

    or-int/2addr p2, v3

    invoke-virtual {v1, p2}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setDisplayOptions(I)V

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p2}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getDisplayOptions()I

    move-result p2

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    const v3, 0x8000

    and-int/2addr p2, v3

    if-eqz p2, :cond_1

    move p2, v2

    goto :goto_0

    :cond_1
    move p2, v1

    :goto_0
    invoke-virtual {v0, p2}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->a(Z)Z

    :cond_2
    and-int/lit16 p1, p1, 0x4000

    if-eqz p1, :cond_3

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz p1, :cond_3

    invoke-virtual {p1, v2}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->a(Z)Z

    goto :goto_1

    :cond_3
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz p1, :cond_4

    invoke-virtual {p1, v1}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->a(Z)Z

    :cond_4
    :goto_1
    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->c:Landroid/content/Context;

    invoke-static {v0}, Ld/b/b/c/a;->a(Landroid/content/Context;)Ld/b/b/c/a;

    move-result-object v0

    invoke-virtual {v0}, Ld/b/b/c/a;->f()Z

    move-result v0

    invoke-direct {p0, v0}, Lmiuix/appcompat/internal/app/widget/u;->m(Z)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->F:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->isAttachedToWindow()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->F:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/i;->d()Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/u;->J:I

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->y()V

    :cond_1
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setEndView(Landroid/view/View;)V

    return-void
.end method

.method protected a(Landroid/view/ViewGroup;)V
    .locals 4
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    if-nez p1, :cond_0

    return-void

    :cond_0
    move-object v0, p1

    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->e:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->e:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, p0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setActionBar(Lmiuix/appcompat/app/d;)V

    sget v0, Ld/b/g;->action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    sget v0, Ld/b/g;->action_context_bar:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    sget v0, Ld/b/g;->action_bar_container:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    sget v0, Ld/b/g;->split_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    sget v0, Ld/b/g;->content_mask:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->k:Landroid/view/View;

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->k:Landroid/view/View;

    if-eqz p1, :cond_1

    new-instance p1, Lmiuix/appcompat/internal/app/widget/s;

    invoke-direct {p1, p0}, Lmiuix/appcompat/internal/app/widget/s;-><init>(Lmiuix/appcompat/internal/app/widget/u;)V

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->l:Landroid/view/View$OnClickListener;

    :cond_1
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    if-nez p1, :cond_3

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    if-nez p1, :cond_3

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lmiuix/appcompat/internal/app/widget/u;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " can only be used with a compatible window decor layout"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_0
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->r()Z

    move-result p1

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/u;->y:I

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getDisplayOptions()I

    move-result p1

    and-int/lit8 p1, p1, 0x4

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_4

    move p1, v1

    goto :goto_1

    :cond_4
    move p1, v0

    :goto_1
    if-eqz p1, :cond_5

    iput-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/u;->w:Z

    :cond_5
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->c:Landroid/content/Context;

    invoke-static {v2}, Ld/b/b/c/a;->a(Landroid/content/Context;)Ld/b/b/c/a;

    move-result-object v2

    invoke-virtual {v2}, Ld/b/b/c/a;->a()Z

    move-result v3

    if-nez v3, :cond_6

    if-eqz p1, :cond_7

    :cond_6
    move v0, v1

    :cond_7
    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/u;->j(Z)V

    invoke-virtual {v2}, Ld/b/b/c/a;->f()Z

    move-result p1

    invoke-direct {p0, p1}, Lmiuix/appcompat/internal/app/widget/u;->m(Z)V

    return-void
.end method

.method public a(Landroidx/appcompat/app/ActionBar$c;)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lmiuix/appcompat/internal/app/widget/u;->b(Landroidx/appcompat/app/ActionBar$c;Z)V

    return-void
.end method

.method a(Landroidx/appcompat/app/ActionBar$c;Z)V
    .locals 1

    goto/32 :goto_d

    nop

    :goto_0
    invoke-virtual {v0, p1, p2}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->a(Landroidx/appcompat/app/ActionBar$c;Z)V

    goto/32 :goto_1

    nop

    :goto_1
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->q:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    goto/32 :goto_9

    nop

    :goto_2
    return-void

    :goto_3
    invoke-virtual {v0, p1, p2}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->a(Landroidx/appcompat/app/ActionBar$c;Z)V

    goto/32 :goto_6

    nop

    :goto_4
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->n:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    goto/32 :goto_8

    nop

    :goto_5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_c

    nop

    :goto_6
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->p:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    goto/32 :goto_0

    nop

    :goto_7
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->s:Ljava/util/ArrayList;

    goto/32 :goto_5

    nop

    :goto_8
    invoke-virtual {v0, p1, p2}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->a(Landroidx/appcompat/app/ActionBar$c;Z)V

    goto/32 :goto_e

    nop

    :goto_9
    invoke-virtual {v0, p1, p2}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->a(Landroidx/appcompat/app/ActionBar$c;Z)V

    goto/32 :goto_7

    nop

    :goto_a
    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/u;->a(Landroidx/appcompat/app/ActionBar$c;)V

    :goto_b
    goto/32 :goto_2

    nop

    :goto_c
    invoke-direct {p0, p1, v0}, Lmiuix/appcompat/internal/app/widget/u;->a(Landroidx/appcompat/app/ActionBar$c;I)V

    goto/32 :goto_f

    nop

    :goto_d
    invoke-direct {p0}, Lmiuix/appcompat/internal/app/widget/u;->F()V

    goto/32 :goto_4

    nop

    :goto_e
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->o:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    goto/32 :goto_3

    nop

    :goto_f
    if-nez p2, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_a

    nop
.end method

.method public a(Landroidx/fragment/app/FragmentActivity;Z)V
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/u;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/u;->C()V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/u;->f(I)V

    new-instance v0, Lmiuix/appcompat/internal/app/widget/J;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/u;->u:Landroidx/fragment/app/FragmentManager;

    invoke-virtual {p1}, Landroidx/activity/ComponentActivity;->getLifecycle()Landroidx/lifecycle/i;

    move-result-object p1

    invoke-direct {v0, p0, v1, p1, p2}, Lmiuix/appcompat/internal/app/widget/J;-><init>(Lmiuix/appcompat/internal/app/widget/u;Landroidx/fragment/app/FragmentManager;Landroidx/lifecycle/i;Z)V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->m:Lmiuix/appcompat/internal/app/widget/J;

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->n:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/u;->a(Lmiuix/appcompat/app/d$a;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->o:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/u;->a(Lmiuix/appcompat/app/d$a;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->p:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/u;->a(Lmiuix/appcompat/app/d$a;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->q:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/u;->a(Lmiuix/appcompat/app/d$a;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/u;->a(Lmiuix/appcompat/app/d$a;)V

    :cond_1
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public a(Lmiuix/animation/b/a;)V
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/u;->A:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/u;->A:Z

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lmiuix/appcompat/internal/app/widget/u;->c(ZLmiuix/animation/b/a;)V

    :cond_0
    return-void
.end method

.method public a(Lmiuix/appcompat/app/d$a;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->m:Lmiuix/appcompat/internal/app/widget/J;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/J;->a(Lmiuix/appcompat/app/d$a;)V

    return-void
.end method

.method public b(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->b:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    :cond_0
    invoke-direct {p0, p1}, Lmiuix/appcompat/internal/app/widget/u;->c(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/u;->r:Lmiuix/appcompat/internal/app/widget/K;

    instance-of v1, v1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    if-eqz v1, :cond_1

    instance-of v1, v0, Ld/b/b/c/d;

    if-nez v1, :cond_2

    :cond_1
    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/u;->r:Lmiuix/appcompat/internal/app/widget/K;

    instance-of v1, v1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    if-eqz v1, :cond_3

    instance-of v1, v0, Ld/b/b/c/c;

    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/u;->r:Lmiuix/appcompat/internal/app/widget/K;

    invoke-interface {v1}, Lmiuix/appcompat/internal/app/widget/K;->b()V

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/u;->r:Lmiuix/appcompat/internal/app/widget/K;

    invoke-interface {v1}, Lmiuix/appcompat/internal/app/widget/K;->a()V

    :cond_3
    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/u;->a(Landroid/view/ActionMode$Callback;)Lmiuix/appcompat/internal/app/widget/K;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->r:Lmiuix/appcompat/internal/app/widget/K;

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->r:Lmiuix/appcompat/internal/app/widget/K;

    if-eqz p1, :cond_7

    instance-of v1, v0, Ld/b/b/c/b;

    if-eqz v1, :cond_6

    move-object v1, v0

    check-cast v1, Ld/b/b/c/b;

    invoke-virtual {v1, p1}, Ld/b/b/c/b;->a(Lmiuix/appcompat/internal/app/widget/K;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->G:Ld/b/b/c/b$a;

    invoke-virtual {v1, p1}, Ld/b/b/c/b;->a(Ld/b/b/c/b$a;)V

    invoke-virtual {v1}, Ld/b/b/c/b;->b()Z

    move-result p1

    if-eqz p1, :cond_6

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->r:Lmiuix/appcompat/internal/app/widget/K;

    invoke-interface {p1, v0}, Lmiuix/appcompat/internal/app/widget/K;->a(Landroid/view/ActionMode;)V

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/u;->h(Z)V

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v1, :cond_4

    iget v2, p0, Lmiuix/appcompat/internal/app/widget/u;->y:I

    if-ne v2, p1, :cond_4

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setVisibility(I)V

    :cond_4
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->r:Lmiuix/appcompat/internal/app/widget/K;

    instance-of v1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    if-eqz v1, :cond_5

    check-cast p1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    const/16 v1, 0x20

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->sendAccessibilityEvent(I)V

    :cond_5
    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->b:Landroid/view/ActionMode;

    return-object v0

    :cond_6
    const/4 p1, 0x0

    return-object p1

    :cond_7
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "not set windowSplitActionBar true in activity style!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public b(I)V
    .locals 5

    and-int/lit8 v0, p1, 0x4

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/u;->w:Z

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setDisplayOptions(I)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getDisplayOptions()I

    move-result v0

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    const v4, 0x8000

    and-int/2addr v0, v4

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v3

    :goto_0
    invoke-virtual {v2, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->a(Z)Z

    :cond_2
    and-int/lit16 p1, p1, 0x4000

    if-eqz p1, :cond_3

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz p1, :cond_3

    invoke-virtual {p1, v1}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->a(Z)Z

    goto :goto_1

    :cond_3
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz p1, :cond_4

    invoke-virtual {p1, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->a(Z)Z

    :cond_4
    :goto_1
    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setStartView(Landroid/view/View;)V

    return-void
.end method

.method b(Landroidx/appcompat/app/ActionBar$c;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    const/4 v0, 0x1

    goto/32 :goto_3

    nop

    :goto_1
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/u;->w()I

    move-result v0

    goto/32 :goto_2

    nop

    :goto_2
    if-eqz v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_0

    nop

    :goto_3
    goto :goto_6

    :goto_4
    goto/32 :goto_5

    nop

    :goto_5
    const/4 v0, 0x0

    :goto_6
    goto/32 :goto_7

    nop

    :goto_7
    invoke-virtual {p0, p1, v0}, Lmiuix/appcompat/internal/app/widget/u;->a(Landroidx/appcompat/app/ActionBar$c;Z)V

    goto/32 :goto_8

    nop

    :goto_8
    return-void
.end method

.method public b(Landroidx/appcompat/app/ActionBar$c;Z)V
    .locals 4

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/u;->u()I

    move-result v0

    const/4 v1, -0x1

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroidx/appcompat/app/ActionBar$c;->d()I

    move-result v1

    :cond_0
    iput v1, p0, Lmiuix/appcompat/internal/app/widget/u;->v:I

    return-void

    :cond_1
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->u:Landroidx/fragment/app/FragmentManager;

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/qa;->d()Landroidx/fragment/app/qa;

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->t:Lmiuix/appcompat/internal/app/widget/u$a;

    if-ne v2, p1, :cond_2

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Lmiuix/appcompat/internal/app/widget/u$a;->g()Landroidx/appcompat/app/ActionBar$d;

    move-result-object p2

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/u;->t:Lmiuix/appcompat/internal/app/widget/u$a;

    invoke-interface {p2, v1, v0}, Landroidx/appcompat/app/ActionBar$d;->c(Landroidx/appcompat/app/ActionBar$c;Landroidx/fragment/app/qa;)V

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/u;->n:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p1}, Landroidx/appcompat/app/ActionBar$c;->d()I

    move-result v1

    invoke-virtual {p2, v1}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->a(I)V

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/u;->o:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p1}, Landroidx/appcompat/app/ActionBar$c;->d()I

    move-result v1

    invoke-virtual {p2, v1}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->a(I)V

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/u;->p:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p1}, Landroidx/appcompat/app/ActionBar$c;->d()I

    move-result v1

    invoke-virtual {p2, v1}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->a(I)V

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/u;->q:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p1}, Landroidx/appcompat/app/ActionBar$c;->d()I

    move-result p1

    invoke-virtual {p2, p1}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->a(I)V

    goto :goto_3

    :cond_2
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->n:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Landroidx/appcompat/app/ActionBar$c;->d()I

    move-result v3

    goto :goto_0

    :cond_3
    move v3, v1

    :goto_0
    invoke-virtual {v2, v3, p2}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->setTabSelected(IZ)V

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->o:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Landroidx/appcompat/app/ActionBar$c;->d()I

    move-result v3

    goto :goto_1

    :cond_4
    move v3, v1

    :goto_1
    invoke-virtual {v2, v3, p2}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->setTabSelected(IZ)V

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->p:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Landroidx/appcompat/app/ActionBar$c;->d()I

    move-result v3

    goto :goto_2

    :cond_5
    move v3, v1

    :goto_2
    invoke-virtual {v2, v3, p2}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->setTabSelected(IZ)V

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->q:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Landroidx/appcompat/app/ActionBar$c;->d()I

    move-result v1

    :cond_6
    invoke-virtual {v2, v1, p2}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->setTabSelected(IZ)V

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/u;->t:Lmiuix/appcompat/internal/app/widget/u$a;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lmiuix/appcompat/internal/app/widget/u$a;->g()Landroidx/appcompat/app/ActionBar$d;

    move-result-object v1

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->t:Lmiuix/appcompat/internal/app/widget/u$a;

    invoke-interface {v1, v2, v0}, Landroidx/appcompat/app/ActionBar$d;->b(Landroidx/appcompat/app/ActionBar$c;Landroidx/fragment/app/qa;)V

    :cond_7
    check-cast p1, Lmiuix/appcompat/internal/app/widget/u$a;

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->t:Lmiuix/appcompat/internal/app/widget/u$a;

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->t:Lmiuix/appcompat/internal/app/widget/u$a;

    if-eqz p1, :cond_8

    iput-boolean p2, p1, Lmiuix/appcompat/internal/app/widget/u$a;->h:Z

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/u$a;->g()Landroidx/appcompat/app/ActionBar$d;

    move-result-object p1

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/u;->t:Lmiuix/appcompat/internal/app/widget/u$a;

    invoke-interface {p1, p2, v0}, Landroidx/appcompat/app/ActionBar$d;->a(Landroidx/appcompat/app/ActionBar$c;Landroidx/fragment/app/qa;)V

    :cond_8
    :goto_3
    invoke-virtual {v0}, Landroidx/fragment/app/qa;->e()Z

    move-result p1

    if-nez p1, :cond_9

    invoke-virtual {v0}, Landroidx/fragment/app/qa;->a()I

    :cond_9
    return-void
.end method

.method public b(Lmiuix/animation/b/a;)V
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/u;->A:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/u;->A:Z

    invoke-direct {p0, v0, p1}, Lmiuix/appcompat/internal/app/widget/u;->c(ZLmiuix/animation/b/a;)V

    :cond_0
    return-void
.end method

.method public c(I)V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getNavigationMode()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/app/ActionBar$c;

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/u;->a(Landroidx/appcompat/app/ActionBar$c;)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "setSelectedNavigationIndex not valid for current navigation mode"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setDropdownSelectedPosition(I)V

    :goto_0
    return-void
.end method

.method public d(I)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->c:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/u;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public d(Z)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lmiuix/appcompat/internal/app/widget/u;->G()I

    move-result p1

    or-int/lit8 p1, p1, 0x4

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-direct {p0}, Lmiuix/appcompat/internal/app/widget/u;->G()I

    move-result v0

    or-int/lit8 v0, v0, 0x4

    invoke-virtual {p0, p1, v0}, Lmiuix/appcompat/internal/app/widget/u;->a(II)V

    return-void
.end method

.method public e(I)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/i;->setExpandStateByUser(I)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setExpandState(I)V

    return-void
.end method

.method public e(Z)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lmiuix/appcompat/internal/app/widget/u;->G()I

    move-result p1

    or-int/lit8 p1, p1, 0x8

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-direct {p0}, Lmiuix/appcompat/internal/app/widget/u;->G()I

    move-result v0

    or-int/lit8 v0, v0, 0x8

    invoke-virtual {p0, p1, v0}, Lmiuix/appcompat/internal/app/widget/u;->a(II)V

    return-void
.end method

.method public f(I)V
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getNavigationMode()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/u;->v()I

    move-result v0

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/u;->v:I

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/u;->a(Landroidx/appcompat/app/ActionBar$c;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->n:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->o:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {v0, v2}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->p:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {v0, v2}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->q:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {v0, v2}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setNavigationMode(I)V

    const/4 v0, 0x0

    if-eq p1, v1, :cond_1

    goto :goto_1

    :cond_1
    invoke-direct {p0}, Lmiuix/appcompat/internal/app/widget/u;->F()V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->n:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p1, v0}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->o:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p1, v0}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->p:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p1, v0}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->q:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p1, v0}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    iget p1, p0, Lmiuix/appcompat/internal/app/widget/u;->v:I

    const/4 v1, -0x1

    if-eq p1, v1, :cond_2

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/u;->c(I)V

    iput v1, p0, Lmiuix/appcompat/internal/app/widget/u;->v:I

    :cond_2
    :goto_1
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setCollapsable(Z)V

    return-void
.end method

.method public f(Z)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "RestrictedApi"
        }
    .end annotation

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/u;->E:Z

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/u;->k()Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-direct {p0, v0}, Lmiuix/appcompat/internal/app/widget/u;->l(Z)V

    goto :goto_0

    :cond_0
    invoke-direct {p0, v0}, Lmiuix/appcompat/internal/app/widget/u;->k(Z)V

    :cond_1
    :goto_0
    return-void
.end method

.method public g()I
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getDisplayOptions()I

    move-result v0

    return v0
.end method

.method public g(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setResizable(Z)V

    return-void
.end method

.method public h()Landroid/content/Context;
    .locals 4

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->d:Landroid/content/Context;

    if-nez v0, :cond_1

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/u;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x1010397

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    if-eqz v0, :cond_0

    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->c:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/u;->d:Landroid/content/Context;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->c:Landroid/content/Context;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->d:Landroid/content/Context;

    :cond_1
    :goto_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->d:Landroid/content/Context;

    return-object v0
.end method

.method h(Z)V
    .locals 2

    goto/32 :goto_1d

    nop

    :goto_0
    xor-int/lit8 v1, p1, 0x1

    goto/32 :goto_4

    nop

    :goto_1
    return-void

    :goto_2
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->o:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    goto/32 :goto_19

    nop

    :goto_3
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->n:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    goto/32 :goto_10

    nop

    :goto_4
    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setEnabled(Z)V

    goto/32 :goto_14

    nop

    :goto_5
    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->s()Z

    move-result v0

    goto/32 :goto_1b

    nop

    :goto_6
    invoke-virtual {v0, p1}, Landroid/widget/HorizontalScrollView;->setEnabled(Z)V

    :goto_7
    goto/32 :goto_1

    nop

    :goto_8
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->n:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    goto/32 :goto_f

    nop

    :goto_9
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/u;->D()V

    goto/32 :goto_a

    nop

    :goto_a
    goto :goto_17

    :goto_b
    goto/32 :goto_16

    nop

    :goto_c
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_5

    nop

    :goto_d
    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setEnabled(Z)V

    goto/32 :goto_2

    nop

    :goto_e
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_1c

    nop

    :goto_f
    xor-int/lit8 v1, p1, 0x1

    goto/32 :goto_d

    nop

    :goto_10
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_c

    nop

    :goto_11
    if-nez v0, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_8

    nop

    :goto_12
    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setEnabled(Z)V

    goto/32 :goto_13

    nop

    :goto_13
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->p:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    goto/32 :goto_0

    nop

    :goto_14
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->q:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    goto/32 :goto_1a

    nop

    :goto_15
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->r:Lmiuix/appcompat/internal/app/widget/K;

    goto/32 :goto_18

    nop

    :goto_16
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/u;->x()V

    :goto_17
    goto/32 :goto_15

    nop

    :goto_18
    invoke-interface {v0, p1}, Lmiuix/appcompat/internal/app/widget/K;->c(Z)V

    goto/32 :goto_3

    nop

    :goto_19
    xor-int/lit8 v1, p1, 0x1

    goto/32 :goto_12

    nop

    :goto_1a
    xor-int/lit8 p1, p1, 0x1

    goto/32 :goto_6

    nop

    :goto_1b
    if-eqz v0, :cond_2

    goto/32 :goto_7

    :cond_2
    goto/32 :goto_e

    nop

    :goto_1c
    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->q()Z

    move-result v0

    goto/32 :goto_11

    nop

    :goto_1d
    if-nez p1, :cond_3

    goto/32 :goto_b

    :cond_3
    goto/32 :goto_9

    nop
.end method

.method public i()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/u;->a(Lmiuix/animation/b/a;)V

    return-void
.end method

.method public i(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->f:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setIsMiuixFloating(Z)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->F:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->d(Z)V

    :cond_0
    return-void
.end method

.method public j(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setHomeButtonEnabled(Z)V

    return-void
.end method

.method public k()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/u;->D:Z

    return v0
.end method

.method public l()Landroidx/appcompat/app/ActionBar$c;
    .locals 1

    new-instance v0, Lmiuix/appcompat/internal/app/widget/u$a;

    invoke-direct {v0, p0}, Lmiuix/appcompat/internal/app/widget/u$a;-><init>(Lmiuix/appcompat/internal/app/widget/u;)V

    return-object v0
.end method

.method public o()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/u;->b(Lmiuix/animation/b/a;)V

    return-void
.end method

.method public p()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getStartView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public r()Lmiuix/appcompat/internal/app/widget/SearchActionModeView;
    .locals 4

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/u;->h()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Ld/b/i;->miuix_appcompat_search_action_mode_view:I

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/u;->e:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    new-instance v1, Lmiuix/appcompat/internal/app/widget/t;

    invoke-direct {v1, p0}, Lmiuix/appcompat/internal/app/widget/t;-><init>(Lmiuix/appcompat/internal/app/widget/u;)V

    invoke-virtual {v0, v1}, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->setOnBackClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method s()Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->e:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method public t()I
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getExpandState()I

    move-result v0

    return v0
.end method

.method public u()I
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getNavigationMode()I

    move-result v0

    return v0
.end method

.method public v()I
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getNavigationMode()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    const/4 v2, -0x1

    if-eq v0, v1, :cond_0

    return v2

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->t:Lmiuix/appcompat/internal/app/widget/u$a;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/u$a;->d()I

    move-result v2

    :cond_1
    return v2

    :cond_2
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getDropdownSelectedPosition()I

    move-result v0

    return v0
.end method

.method public w()I
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method x()V
    .locals 4

    goto/32 :goto_18

    nop

    :goto_0
    invoke-virtual {v0, v1, v3, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setExpandState(IZZ)V

    goto/32 :goto_1a

    nop

    :goto_1
    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->getExpandState()I

    move-result v0

    goto/32 :goto_2a

    nop

    :goto_2
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->r:Lmiuix/appcompat/internal/app/widget/K;

    goto/32 :goto_2b

    nop

    :goto_3
    iget v1, p0, Lmiuix/appcompat/internal/app/widget/u;->J:I

    goto/32 :goto_0

    nop

    :goto_4
    iget v1, p0, Lmiuix/appcompat/internal/app/widget/u;->L:I

    goto/32 :goto_6

    nop

    :goto_5
    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/u;->g(Z)V

    goto/32 :goto_1d

    nop

    :goto_6
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setImportantForAccessibility(I)V

    :goto_7
    goto/32 :goto_17

    nop

    :goto_8
    invoke-virtual {v1, v2}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->d(Z)V

    goto/32 :goto_21

    nop

    :goto_9
    invoke-virtual {v0, v1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setExpandState(I)V

    :goto_a
    goto/32 :goto_10

    nop

    :goto_b
    if-nez v2, :cond_0

    goto/32 :goto_f

    :cond_0
    goto/32 :goto_1f

    nop

    :goto_c
    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/u;->K:Z

    goto/32 :goto_5

    nop

    :goto_d
    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/u;->K:Z

    goto/32 :goto_22

    nop

    :goto_e
    goto :goto_13

    :goto_f
    goto/32 :goto_12

    nop

    :goto_10
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_4

    nop

    :goto_11
    and-int/2addr v2, v3

    goto/32 :goto_1e

    nop

    :goto_12
    move v2, v0

    :goto_13
    goto/32 :goto_8

    nop

    :goto_14
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->r:Lmiuix/appcompat/internal/app/widget/K;

    goto/32 :goto_24

    nop

    :goto_15
    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->g()Z

    move-result v0

    goto/32 :goto_2c

    nop

    :goto_16
    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_29

    nop

    :goto_17
    return-void

    :goto_18
    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/u;->C:Z

    goto/32 :goto_19

    nop

    :goto_19
    if-nez v0, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_27

    nop

    :goto_1a
    goto :goto_a

    :goto_1b
    goto/32 :goto_25

    nop

    :goto_1c
    iget v1, p0, Lmiuix/appcompat/internal/app/widget/u;->J:I

    goto/32 :goto_9

    nop

    :goto_1d
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_1c

    nop

    :goto_1e
    const/4 v3, 0x1

    goto/32 :goto_b

    nop

    :goto_1f
    move v2, v3

    goto/32 :goto_e

    nop

    :goto_20
    const v3, 0x8000

    goto/32 :goto_11

    nop

    :goto_21
    invoke-direct {p0, v0}, Lmiuix/appcompat/internal/app/widget/u;->o(Z)V

    goto/32 :goto_2

    nop

    :goto_22
    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/u;->g(Z)V

    goto/32 :goto_26

    nop

    :goto_23
    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/u;->C:Z

    goto/32 :goto_16

    nop

    :goto_24
    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    goto/32 :goto_1

    nop

    :goto_25
    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    goto/32 :goto_15

    nop

    :goto_26
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->g:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_3

    nop

    :goto_27
    const/4 v0, 0x0

    goto/32 :goto_23

    nop

    :goto_28
    if-nez v1, :cond_2

    goto/32 :goto_1b

    :cond_2
    goto/32 :goto_d

    nop

    :goto_29
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/u;->g()I

    move-result v2

    goto/32 :goto_20

    nop

    :goto_2a
    iput v0, p0, Lmiuix/appcompat/internal/app/widget/u;->J:I

    goto/32 :goto_c

    nop

    :goto_2b
    instance-of v1, v0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    goto/32 :goto_28

    nop

    :goto_2c
    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/u;->K:Z

    goto/32 :goto_14

    nop
.end method

.method y()V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-direct {p0}, Lmiuix/appcompat/internal/app/widget/u;->E()V

    goto/32 :goto_0

    nop
.end method

.method public z()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u;->m:Lmiuix/appcompat/internal/app/widget/J;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
