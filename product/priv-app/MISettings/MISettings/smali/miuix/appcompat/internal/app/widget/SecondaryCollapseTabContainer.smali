.class public Lmiuix/appcompat/internal/app/widget/SecondaryCollapseTabContainer;
.super Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/SecondaryCollapseTabContainer;->getTabContainerHeight()I

    move-result p1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->setContentHeight(I)V

    return-void
.end method


# virtual methods
.method b(Landroid/widget/TextView;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-static {p1}, Ld/b/b/b/e;->a(Landroid/widget/TextView;)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method getDefaultTabTextStyle()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    sget v0, Ld/b/b;->actionBarTabTextSecondaryStyle:I

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method getTabBarLayoutRes()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    sget v0, Ld/b/i;->miuix_appcompat_action_bar_tabbar_collapse_secondary:I

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method getTabContainerHeight()I
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    const/4 v0, -0x2

    goto/32 :goto_0

    nop
.end method

.method getTabViewLayoutRes()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    sget v0, Ld/b/i;->miuix_appcompat_action_bar_tab_secondary:I

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method getTabViewMarginHorizontal()I
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    goto/32 :goto_4

    nop

    :goto_1
    sget v1, Ld/b/e;->miuix_appcompat_action_bar_tab_secondary_margin:I

    goto/32 :goto_0

    nop

    :goto_2
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_3
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getContext()Landroid/content/Context;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_4
    return v0
.end method
