.class public Lmiuix/appcompat/internal/view/menu/g;
.super Lmiuix/internal/widget/h;

# interfaces
.implements Lmiuix/appcompat/internal/view/menu/d;


# instance fields
.field private A:Landroid/view/ViewGroup;

.field private x:Lmiuix/appcompat/app/f;

.field private y:Lmiuix/appcompat/internal/view/menu/c;

.field private z:Landroid/view/View;


# direct methods
.method public constructor <init>(Lmiuix/appcompat/app/f;Landroid/view/Menu;)V
    .locals 1

    invoke-virtual {p1}, Lmiuix/appcompat/app/f;->h()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lmiuix/internal/widget/h;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Lmiuix/appcompat/app/f;->h()Landroid/content/Context;

    move-result-object v0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/g;->x:Lmiuix/appcompat/app/f;

    new-instance p1, Lmiuix/appcompat/internal/view/menu/c;

    invoke-direct {p1, v0, p2}, Lmiuix/appcompat/internal/view/menu/c;-><init>(Landroid/content/Context;Landroid/view/Menu;)V

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/g;->y:Lmiuix/appcompat/internal/view/menu/c;

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/g;->y:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {p0, p1}, Lmiuix/internal/widget/h;->a(Landroid/widget/ListAdapter;)V

    new-instance p1, Lmiuix/appcompat/internal/view/menu/f;

    invoke-direct {p1, p0}, Lmiuix/appcompat/internal/view/menu/f;-><init>(Lmiuix/appcompat/internal/view/menu/g;)V

    invoke-virtual {p0, p1}, Lmiuix/internal/widget/h;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method static synthetic a(Lmiuix/appcompat/internal/view/menu/g;)Lmiuix/appcompat/internal/view/menu/c;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/view/menu/g;->y:Lmiuix/appcompat/internal/view/menu/c;

    return-object p0
.end method

.method static synthetic b(Lmiuix/appcompat/internal/view/menu/g;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/view/menu/g;->z:Landroid/view/View;

    return-object p0
.end method

.method static synthetic c(Lmiuix/appcompat/internal/view/menu/g;)Landroid/view/ViewGroup;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/view/menu/g;->A:Landroid/view/ViewGroup;

    return-object p0
.end method

.method static synthetic d(Lmiuix/appcompat/internal/view/menu/g;)Lmiuix/appcompat/app/f;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/view/menu/g;->x:Lmiuix/appcompat/app/f;

    return-object p0
.end method

.method private d(Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 4

    if-nez p2, :cond_0

    const-string p1, "ImmersionMenu"

    const-string p2, "ImmersionMenuPopupWindow offset can\'t be adjusted without parent"

    invoke-static {p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const/4 v0, 0x2

    new-array v1, v0, [I

    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->getLocationInWindow([I)V

    new-array v0, v0, [I

    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    const/4 v2, 0x1

    aget v3, v0, v2

    aget v2, v1, v2

    sub-int/2addr v3, v2

    invoke-virtual {p0}, Lmiuix/internal/widget/h;->h()I

    move-result v2

    sub-int/2addr v3, v2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v2, v3

    neg-int v2, v2

    invoke-virtual {p0, v2}, Lmiuix/internal/widget/h;->b(I)V

    invoke-static {p2}, Ld/h/a/i;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lmiuix/internal/widget/h;->g()I

    move-result p1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    aget v0, v0, v2

    aget v1, v1, v2

    sub-int/2addr v0, v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result p1

    add-int/2addr v0, p1

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getWidth()I

    move-result p1

    sub-int/2addr p1, v0

    invoke-virtual {p0}, Lmiuix/internal/widget/h;->g()I

    move-result p2

    sub-int/2addr p1, p2

    :goto_0
    invoke-virtual {p0, p1}, Lmiuix/internal/widget/h;->a(I)V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/Menu;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/g;->y:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/view/menu/c;->a(Landroid/view/Menu;)V

    return-void
.end method

.method public a(Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/g;->z:Landroid/view/View;

    iput-object p2, p0, Lmiuix/appcompat/internal/view/menu/g;->A:Landroid/view/ViewGroup;

    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/internal/view/menu/g;->d(Landroid/view/View;Landroid/view/ViewGroup;)V

    invoke-super {p0, p1, p2}, Lmiuix/internal/widget/h;->a(Landroid/view/View;Landroid/view/ViewGroup;)V

    return-void
.end method

.method public b(Z)V
    .locals 0

    invoke-virtual {p0}, Lmiuix/internal/widget/h;->dismiss()V

    return-void
.end method
