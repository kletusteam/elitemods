.class Lmiuix/appcompat/internal/app/widget/J$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/appcompat/internal/app/widget/J;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private a:I

.field private b:F

.field c:Z

.field d:Z

.field e:I

.field f:I


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/J$a;->a:I

    return-void
.end method

.method synthetic constructor <init>(Lmiuix/appcompat/internal/app/widget/H;)V
    .locals 0

    invoke-direct {p0}, Lmiuix/appcompat/internal/app/widget/J$a;-><init>()V

    return-void
.end method

.method private a()V
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/J$a;->f:I

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/J$a;->e:I

    const/4 v0, -0x1

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/J$a;->a:I

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/J$a;->b:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/J$a;->d:Z

    return-void
.end method

.method private b(IF)V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/J$a;->c:Z

    iget v1, p0, Lmiuix/appcompat/internal/app/widget/J$a;->b:F

    cmpl-float p2, p2, v1

    if-lez p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-eqz v0, :cond_1

    move p2, p1

    goto :goto_0

    :cond_1
    add-int/lit8 p2, p1, 0x1

    :goto_0
    iput p2, p0, Lmiuix/appcompat/internal/app/widget/J$a;->e:I

    if-eqz v0, :cond_2

    add-int/lit8 p1, p1, 0x1

    :cond_2
    iput p1, p0, Lmiuix/appcompat/internal/app/widget/J$a;->f:I

    return-void
.end method

.method private c(IF)V
    .locals 0

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/J$a;->a:I

    iput p2, p0, Lmiuix/appcompat/internal/app/widget/J$a;->b:F

    const/4 p1, 0x1

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/J$a;->c:Z

    const/4 p1, 0x0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/J$a;->d:Z

    return-void
.end method


# virtual methods
.method a(IF)V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    if-ne v0, p1, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_6

    nop

    :goto_1
    return-void

    :goto_2
    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/J$a;->c:Z

    goto/32 :goto_e

    nop

    :goto_3
    const v0, 0x38d1b717    # 1.0E-4f

    goto/32 :goto_8

    nop

    :goto_4
    goto :goto_b

    :goto_5
    goto/32 :goto_2

    nop

    :goto_6
    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/internal/app/widget/J$a;->c(IF)V

    goto/32 :goto_4

    nop

    :goto_7
    iget v0, p0, Lmiuix/appcompat/internal/app/widget/J$a;->a:I

    goto/32 :goto_0

    nop

    :goto_8
    cmpg-float v0, p2, v0

    goto/32 :goto_f

    nop

    :goto_9
    invoke-direct {p0}, Lmiuix/appcompat/internal/app/widget/J$a;->a()V

    goto/32 :goto_c

    nop

    :goto_a
    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/internal/app/widget/J$a;->b(IF)V

    :goto_b
    goto/32 :goto_1

    nop

    :goto_c
    goto :goto_b

    :goto_d
    goto/32 :goto_7

    nop

    :goto_e
    if-nez v0, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_a

    nop

    :goto_f
    if-ltz v0, :cond_2

    goto/32 :goto_d

    :cond_2
    goto/32 :goto_9

    nop
.end method
