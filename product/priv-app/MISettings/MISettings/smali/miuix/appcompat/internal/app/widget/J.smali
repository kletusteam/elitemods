.class public Lmiuix/appcompat/internal/app/widget/J;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/internal/app/widget/J$a;
    }
.end annotation


# instance fields
.field private a:Lmiuix/appcompat/internal/app/widget/u;

.field private b:Lmiuix/viewpager/widget/ViewPager;

.field private c:Lmiuix/appcompat/internal/app/widget/L;

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lmiuix/appcompat/app/d$a;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroidx/appcompat/app/ActionBar$d;


# direct methods
.method constructor <init>(Lmiuix/appcompat/internal/app/widget/u;Landroidx/fragment/app/FragmentManager;Landroidx/lifecycle/i;Z)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p3, Lmiuix/appcompat/internal/app/widget/H;

    invoke-direct {p3, p0}, Lmiuix/appcompat/internal/app/widget/H;-><init>(Lmiuix/appcompat/internal/app/widget/J;)V

    iput-object p3, p0, Lmiuix/appcompat/internal/app/widget/J;->e:Landroidx/appcompat/app/ActionBar$d;

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/J;->a:Lmiuix/appcompat/internal/app/widget/u;

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/J;->a:Lmiuix/appcompat/internal/app/widget/u;

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/u;->s()Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p3

    sget v0, Ld/b/g;->view_pager:I

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Lmiuix/viewpager/widget/ViewPager;

    if-eqz v1, :cond_0

    check-cast v0, Lmiuix/viewpager/widget/ViewPager;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/J;->b:Lmiuix/viewpager/widget/ViewPager;

    goto :goto_0

    :cond_0
    new-instance v0, Lmiuix/viewpager/widget/ViewPager;

    invoke-direct {v0, p3}, Lmiuix/viewpager/widget/ViewPager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/J;->b:Lmiuix/viewpager/widget/ViewPager;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/J;->b:Lmiuix/viewpager/widget/ViewPager;

    sget v1, Ld/b/g;->view_pager:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setId(I)V

    new-instance v0, Lmiuix/springback/view/SpringBackLayout;

    invoke-direct {v0, p3}, Lmiuix/springback/view/SpringBackLayout;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lmiuix/springback/view/SpringBackLayout;->setScrollOrientation(I)V

    new-instance v1, Landroidx/viewpager/widget/OriginalViewPager$LayoutParams;

    invoke-direct {v1}, Landroidx/viewpager/widget/OriginalViewPager$LayoutParams;-><init>()V

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/J;->b:Lmiuix/viewpager/widget/ViewPager;

    invoke-virtual {v0, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/J;->b:Lmiuix/viewpager/widget/ViewPager;

    invoke-virtual {v0, v1}, Lmiuix/springback/view/SpringBackLayout;->setTarget(Landroid/view/View;)V

    const v1, 0x1020002

    invoke-virtual {p1, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    new-instance p1, Lmiuix/appcompat/internal/app/widget/L;

    invoke-direct {p1, p3, p2}, Lmiuix/appcompat/internal/app/widget/L;-><init>(Landroid/content/Context;Landroidx/fragment/app/FragmentManager;)V

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/J;->c:Lmiuix/appcompat/internal/app/widget/L;

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/J;->b:Lmiuix/viewpager/widget/ViewPager;

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/J;->c:Lmiuix/appcompat/internal/app/widget/L;

    invoke-virtual {p1, p2}, Landroidx/viewpager/widget/OriginalViewPager;->setAdapter(Landroidx/viewpager/widget/f;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/J;->b:Lmiuix/viewpager/widget/ViewPager;

    new-instance p2, Lmiuix/appcompat/internal/app/widget/I;

    invoke-direct {p2, p0}, Lmiuix/appcompat/internal/app/widget/I;-><init>(Lmiuix/appcompat/internal/app/widget/J;)V

    invoke-virtual {p1, p2}, Landroidx/viewpager/widget/OriginalViewPager;->a(Landroidx/viewpager/widget/OriginalViewPager$d;)V

    if-eqz p4, :cond_1

    invoke-static {}, Ld/h/a/e;->a()Z

    move-result p1

    if-eqz p1, :cond_1

    new-instance p1, Lmiuix/appcompat/internal/app/widget/O;

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/J;->b:Lmiuix/viewpager/widget/ViewPager;

    iget-object p3, p0, Lmiuix/appcompat/internal/app/widget/J;->c:Lmiuix/appcompat/internal/app/widget/L;

    invoke-direct {p1, p2, p3}, Lmiuix/appcompat/internal/app/widget/O;-><init>(Lmiuix/viewpager/widget/ViewPager;Lmiuix/appcompat/internal/app/widget/L;)V

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/J;->a(Lmiuix/appcompat/app/d$a;)V

    :cond_1
    return-void
.end method

.method static synthetic a(Lmiuix/appcompat/internal/app/widget/J;)Lmiuix/appcompat/internal/app/widget/L;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/app/widget/J;->c:Lmiuix/appcompat/internal/app/widget/L;

    return-object p0
.end method

.method static synthetic b(Lmiuix/appcompat/internal/app/widget/J;)Lmiuix/viewpager/widget/ViewPager;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/app/widget/J;->b:Lmiuix/viewpager/widget/ViewPager;

    return-object p0
.end method

.method static synthetic c(Lmiuix/appcompat/internal/app/widget/J;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/app/widget/J;->d:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic d(Lmiuix/appcompat/internal/app/widget/J;)Lmiuix/appcompat/internal/app/widget/u;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/app/widget/J;->a:Lmiuix/appcompat/internal/app/widget/u;

    return-object p0
.end method


# virtual methods
.method a(Ljava/lang/String;Landroidx/appcompat/app/ActionBar$c;Ljava/lang/Class;Landroid/os/Bundle;Z)I
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroidx/appcompat/app/ActionBar$c;",
            "Ljava/lang/Class<",
            "+",
            "Landroidx/fragment/app/Fragment;",
            ">;",
            "Landroid/os/Bundle;",
            "Z)I"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/J;->e:Landroidx/appcompat/app/ActionBar$d;

    goto/32 :goto_4

    nop

    :goto_1
    move-object v0, p2

    goto/32 :goto_10

    nop

    :goto_2
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/J;->a:Lmiuix/appcompat/internal/app/widget/u;

    goto/32 :goto_5

    nop

    :goto_3
    invoke-virtual {p3}, Lmiuix/appcompat/internal/app/widget/L;->getCount()I

    move-result p3

    goto/32 :goto_d

    nop

    :goto_4
    invoke-virtual {v0, v1}, Lmiuix/appcompat/internal/app/widget/u$a;->a(Landroidx/appcompat/app/ActionBar$d;)Landroidx/appcompat/app/ActionBar$c;

    goto/32 :goto_2

    nop

    :goto_5
    invoke-virtual {v0, p2}, Lmiuix/appcompat/internal/app/widget/u;->b(Landroidx/appcompat/app/ActionBar$c;)V

    goto/32 :goto_8

    nop

    :goto_6
    if-nez p2, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_f

    nop

    :goto_7
    move-object v3, p3

    goto/32 :goto_11

    nop

    :goto_8
    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/J;->c:Lmiuix/appcompat/internal/app/widget/L;

    goto/32 :goto_b

    nop

    :goto_9
    invoke-virtual {p2}, Lmiuix/appcompat/internal/app/widget/L;->a()Z

    move-result p2

    goto/32 :goto_6

    nop

    :goto_a
    invoke-virtual/range {v1 .. v6}, Lmiuix/appcompat/internal/app/widget/L;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;Landroidx/appcompat/app/ActionBar$c;Z)I

    move-result p1

    goto/32 :goto_15

    nop

    :goto_b
    move-object v2, p1

    goto/32 :goto_7

    nop

    :goto_c
    iget-object p3, p0, Lmiuix/appcompat/internal/app/widget/J;->c:Lmiuix/appcompat/internal/app/widget/L;

    goto/32 :goto_3

    nop

    :goto_d
    add-int/lit8 p3, p3, -0x1

    goto/32 :goto_13

    nop

    :goto_e
    move v6, p5

    goto/32 :goto_a

    nop

    :goto_f
    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/J;->b:Lmiuix/viewpager/widget/ViewPager;

    goto/32 :goto_c

    nop

    :goto_10
    check-cast v0, Lmiuix/appcompat/internal/app/widget/u$a;

    goto/32 :goto_0

    nop

    :goto_11
    move-object v4, p4

    goto/32 :goto_16

    nop

    :goto_12
    return p1

    :goto_13
    invoke-virtual {p2, p3}, Landroidx/viewpager/widget/OriginalViewPager;->setCurrentItem(I)V

    :goto_14
    goto/32 :goto_12

    nop

    :goto_15
    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/J;->c:Lmiuix/appcompat/internal/app/widget/L;

    goto/32 :goto_9

    nop

    :goto_16
    move-object v5, p2

    goto/32 :goto_e

    nop
.end method

.method a(Lmiuix/appcompat/app/d$a;)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/J;->d:Ljava/util/ArrayList;

    goto/32 :goto_3

    nop

    :goto_2
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/J;->d:Ljava/util/ArrayList;

    goto/32 :goto_5

    nop

    :goto_3
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_0

    nop

    :goto_4
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_8

    nop

    :goto_5
    if-eqz v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_4

    nop

    :goto_6
    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/J;->d:Ljava/util/ArrayList;

    :goto_7
    goto/32 :goto_1

    nop

    :goto_8
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_6

    nop
.end method
