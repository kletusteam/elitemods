.class Lmiuix/appcompat/internal/view/menu/a/f;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiuix/appcompat/internal/view/menu/a/h;-><init>(Landroid/content/Context;Lmiuix/appcompat/internal/view/menu/i;Landroid/widget/PopupWindow$OnDismissListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lmiuix/appcompat/internal/view/menu/a/h;


# direct methods
.method constructor <init>(Lmiuix/appcompat/internal/view/menu/a/h;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/a/f;->a:Lmiuix/appcompat/internal/view/menu/a/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/a/f;->a:Lmiuix/appcompat/internal/view/menu/a/h;

    invoke-static {p1}, Lmiuix/appcompat/internal/view/menu/a/h;->a(Lmiuix/appcompat/internal/view/menu/a/h;)Lmiuix/appcompat/internal/view/menu/a/a;

    move-result-object p1

    invoke-virtual {p1, p3}, Lmiuix/internal/widget/j;->getItem(I)Landroid/view/MenuItem;

    move-result-object p1

    iget-object p2, p0, Lmiuix/appcompat/internal/view/menu/a/f;->a:Lmiuix/appcompat/internal/view/menu/a/h;

    invoke-static {p2}, Lmiuix/appcompat/internal/view/menu/a/h;->b(Lmiuix/appcompat/internal/view/menu/a/h;)Lmiuix/appcompat/internal/view/menu/i;

    move-result-object p2

    const/4 p3, 0x0

    invoke-virtual {p2, p1, p3}, Lmiuix/appcompat/internal/view/menu/i;->a(Landroid/view/MenuItem;I)Z

    invoke-interface {p1}, Landroid/view/MenuItem;->hasSubMenu()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object p1

    iget-object p2, p0, Lmiuix/appcompat/internal/view/menu/a/f;->a:Lmiuix/appcompat/internal/view/menu/a/h;

    new-instance p3, Lmiuix/appcompat/internal/view/menu/a/e;

    invoke-direct {p3, p0, p1}, Lmiuix/appcompat/internal/view/menu/a/e;-><init>(Lmiuix/appcompat/internal/view/menu/a/f;Landroid/view/SubMenu;)V

    invoke-virtual {p2, p3}, Lmiuix/internal/widget/h;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/a/f;->a:Lmiuix/appcompat/internal/view/menu/a/h;

    invoke-virtual {p1}, Lmiuix/internal/widget/h;->dismiss()V

    return-void
.end method
