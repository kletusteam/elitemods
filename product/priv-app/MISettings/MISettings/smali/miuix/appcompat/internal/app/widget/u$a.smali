.class public Lmiuix/appcompat/internal/app/widget/u$a;
.super Landroidx/appcompat/app/ActionBar$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/appcompat/internal/app/widget/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field private a:Landroidx/appcompat/app/ActionBar$d;

.field private b:Landroidx/appcompat/app/ActionBar$d;

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:Ljava/lang/CharSequence;

.field private e:Ljava/lang/CharSequence;

.field private f:I

.field private g:Landroid/view/View;

.field public h:Z

.field final synthetic i:Lmiuix/appcompat/internal/app/widget/u;


# direct methods
.method public constructor <init>(Lmiuix/appcompat/internal/app/widget/u;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/u$a;->i:Lmiuix/appcompat/internal/app/widget/u;

    invoke-direct {p0}, Landroidx/appcompat/app/ActionBar$c;-><init>()V

    const/4 p1, -0x1

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/u$a;->f:I

    const/4 p1, 0x1

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/u$a;->h:Z

    return-void
.end method

.method static synthetic a(Lmiuix/appcompat/internal/app/widget/u$a;)Landroidx/appcompat/app/ActionBar$d;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/app/widget/u$a;->b:Landroidx/appcompat/app/ActionBar$d;

    return-object p0
.end method

.method static synthetic b(Lmiuix/appcompat/internal/app/widget/u$a;)Landroidx/appcompat/app/ActionBar$d;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/app/widget/u$a;->a:Landroidx/appcompat/app/ActionBar$d;

    return-object p0
.end method


# virtual methods
.method public a(Landroidx/appcompat/app/ActionBar$d;)Landroidx/appcompat/app/ActionBar$c;
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/u$a;->b:Landroidx/appcompat/app/ActionBar$d;

    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)Landroidx/appcompat/app/ActionBar$c;
    .locals 1

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/u$a;->d:Ljava/lang/CharSequence;

    iget p1, p0, Lmiuix/appcompat/internal/app/widget/u$a;->f:I

    if-ltz p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u$a;->i:Lmiuix/appcompat/internal/app/widget/u;

    invoke-static {p1}, Lmiuix/appcompat/internal/app/widget/u;->c(Lmiuix/appcompat/internal/app/widget/u;)Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    move-result-object p1

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/u$a;->f:I

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->c(I)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u$a;->i:Lmiuix/appcompat/internal/app/widget/u;

    invoke-static {p1}, Lmiuix/appcompat/internal/app/widget/u;->d(Lmiuix/appcompat/internal/app/widget/u;)Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    move-result-object p1

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/u$a;->f:I

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->c(I)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u$a;->i:Lmiuix/appcompat/internal/app/widget/u;

    invoke-static {p1}, Lmiuix/appcompat/internal/app/widget/u;->e(Lmiuix/appcompat/internal/app/widget/u;)Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    move-result-object p1

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/u$a;->f:I

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->c(I)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/u$a;->i:Lmiuix/appcompat/internal/app/widget/u;

    invoke-static {p1}, Lmiuix/appcompat/internal/app/widget/u;->e(Lmiuix/appcompat/internal/app/widget/u;)Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    move-result-object p1

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/u$a;->f:I

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->c(I)V

    :cond_0
    return-object p0
.end method

.method public a()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u$a;->e:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/u$a;->f:I

    return-void
.end method

.method public b()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u$a;->g:Landroid/view/View;

    return-object v0
.end method

.method public c()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u$a;->c:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/u$a;->f:I

    return v0
.end method

.method public e()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u$a;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/u$a;->i:Lmiuix/appcompat/internal/app/widget/u;

    invoke-virtual {v0, p0}, Lmiuix/appcompat/internal/app/widget/u;->a(Landroidx/appcompat/app/ActionBar$c;)V

    return-void
.end method

.method public g()Landroidx/appcompat/app/ActionBar$d;
    .locals 1

    invoke-static {}, Lmiuix/appcompat/internal/app/widget/u;->q()Landroidx/appcompat/app/ActionBar$d;

    move-result-object v0

    return-object v0
.end method
