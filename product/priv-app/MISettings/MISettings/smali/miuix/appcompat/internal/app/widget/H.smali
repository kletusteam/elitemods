.class Lmiuix/appcompat/internal/app/widget/H;
.super Ljava/lang/Object;

# interfaces
.implements Landroidx/appcompat/app/ActionBar$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/appcompat/internal/app/widget/J;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lmiuix/appcompat/internal/app/widget/J;


# direct methods
.method constructor <init>(Lmiuix/appcompat/internal/app/widget/J;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/H;->a:Lmiuix/appcompat/internal/app/widget/J;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroidx/appcompat/app/ActionBar$c;Landroidx/fragment/app/qa;)V
    .locals 2

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/H;->a:Lmiuix/appcompat/internal/app/widget/J;

    invoke-static {p2}, Lmiuix/appcompat/internal/app/widget/J;->a(Lmiuix/appcompat/internal/app/widget/J;)Lmiuix/appcompat/internal/app/widget/L;

    move-result-object p2

    invoke-virtual {p2}, Lmiuix/appcompat/internal/app/widget/L;->getCount()I

    move-result p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_2

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/H;->a:Lmiuix/appcompat/internal/app/widget/J;

    invoke-static {v1}, Lmiuix/appcompat/internal/app/widget/J;->a(Lmiuix/appcompat/internal/app/widget/J;)Lmiuix/appcompat/internal/app/widget/L;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmiuix/appcompat/internal/app/widget/L;->a(I)Landroidx/appcompat/app/ActionBar$c;

    move-result-object v1

    if-ne v1, p1, :cond_1

    instance-of p2, p1, Lmiuix/appcompat/internal/app/widget/u$a;

    if-eqz p2, :cond_0

    check-cast p1, Lmiuix/appcompat/internal/app/widget/u$a;

    iget-boolean p1, p1, Lmiuix/appcompat/internal/app/widget/u$a;->h:Z

    goto :goto_1

    :cond_0
    const/4 p1, 0x1

    :goto_1
    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/H;->a:Lmiuix/appcompat/internal/app/widget/J;

    invoke-static {p2}, Lmiuix/appcompat/internal/app/widget/J;->b(Lmiuix/appcompat/internal/app/widget/J;)Lmiuix/viewpager/widget/ViewPager;

    move-result-object p2

    invoke-virtual {p2, v0, p1}, Landroidx/viewpager/widget/OriginalViewPager;->setCurrentItem(IZ)V

    goto :goto_2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    :goto_2
    return-void
.end method

.method public b(Landroidx/appcompat/app/ActionBar$c;Landroidx/fragment/app/qa;)V
    .locals 0

    return-void
.end method

.method public c(Landroidx/appcompat/app/ActionBar$c;Landroidx/fragment/app/qa;)V
    .locals 0

    return-void
.end method
