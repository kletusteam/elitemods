.class Lmiuix/appcompat/internal/app/widget/q;
.super Ljava/lang/Object;

# interfaces
.implements Landroidx/appcompat/app/ActionBar$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/appcompat/internal/app/widget/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroidx/appcompat/app/ActionBar$c;Landroidx/fragment/app/qa;)V
    .locals 2

    move-object v0, p1

    check-cast v0, Lmiuix/appcompat/internal/app/widget/u$a;

    invoke-static {v0}, Lmiuix/appcompat/internal/app/widget/u$a;->a(Lmiuix/appcompat/internal/app/widget/u$a;)Landroidx/appcompat/app/ActionBar$d;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lmiuix/appcompat/internal/app/widget/u$a;->a(Lmiuix/appcompat/internal/app/widget/u$a;)Landroidx/appcompat/app/ActionBar$d;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Landroidx/appcompat/app/ActionBar$d;->a(Landroidx/appcompat/app/ActionBar$c;Landroidx/fragment/app/qa;)V

    :cond_0
    invoke-static {v0}, Lmiuix/appcompat/internal/app/widget/u$a;->b(Lmiuix/appcompat/internal/app/widget/u$a;)Landroidx/appcompat/app/ActionBar$d;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {v0}, Lmiuix/appcompat/internal/app/widget/u$a;->b(Lmiuix/appcompat/internal/app/widget/u$a;)Landroidx/appcompat/app/ActionBar$d;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroidx/appcompat/app/ActionBar$d;->a(Landroidx/appcompat/app/ActionBar$c;Landroidx/fragment/app/qa;)V

    :cond_1
    return-void
.end method

.method public b(Landroidx/appcompat/app/ActionBar$c;Landroidx/fragment/app/qa;)V
    .locals 2

    move-object v0, p1

    check-cast v0, Lmiuix/appcompat/internal/app/widget/u$a;

    invoke-static {v0}, Lmiuix/appcompat/internal/app/widget/u$a;->a(Lmiuix/appcompat/internal/app/widget/u$a;)Landroidx/appcompat/app/ActionBar$d;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lmiuix/appcompat/internal/app/widget/u$a;->a(Lmiuix/appcompat/internal/app/widget/u$a;)Landroidx/appcompat/app/ActionBar$d;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Landroidx/appcompat/app/ActionBar$d;->b(Landroidx/appcompat/app/ActionBar$c;Landroidx/fragment/app/qa;)V

    :cond_0
    invoke-static {v0}, Lmiuix/appcompat/internal/app/widget/u$a;->b(Lmiuix/appcompat/internal/app/widget/u$a;)Landroidx/appcompat/app/ActionBar$d;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {v0}, Lmiuix/appcompat/internal/app/widget/u$a;->b(Lmiuix/appcompat/internal/app/widget/u$a;)Landroidx/appcompat/app/ActionBar$d;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroidx/appcompat/app/ActionBar$d;->b(Landroidx/appcompat/app/ActionBar$c;Landroidx/fragment/app/qa;)V

    :cond_1
    return-void
.end method

.method public c(Landroidx/appcompat/app/ActionBar$c;Landroidx/fragment/app/qa;)V
    .locals 2

    move-object v0, p1

    check-cast v0, Lmiuix/appcompat/internal/app/widget/u$a;

    invoke-static {v0}, Lmiuix/appcompat/internal/app/widget/u$a;->a(Lmiuix/appcompat/internal/app/widget/u$a;)Landroidx/appcompat/app/ActionBar$d;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lmiuix/appcompat/internal/app/widget/u$a;->a(Lmiuix/appcompat/internal/app/widget/u$a;)Landroidx/appcompat/app/ActionBar$d;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Landroidx/appcompat/app/ActionBar$d;->c(Landroidx/appcompat/app/ActionBar$c;Landroidx/fragment/app/qa;)V

    :cond_0
    invoke-static {v0}, Lmiuix/appcompat/internal/app/widget/u$a;->b(Lmiuix/appcompat/internal/app/widget/u$a;)Landroidx/appcompat/app/ActionBar$d;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {v0}, Lmiuix/appcompat/internal/app/widget/u$a;->b(Lmiuix/appcompat/internal/app/widget/u$a;)Landroidx/appcompat/app/ActionBar$d;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroidx/appcompat/app/ActionBar$d;->c(Landroidx/appcompat/app/ActionBar$c;Landroidx/fragment/app/qa;)V

    :cond_1
    return-void
.end method
