.class Lmiuix/appcompat/internal/app/widget/L;
.super Landroidx/viewpager/widget/f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/internal/app/widget/L$a;
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroidx/fragment/app/FragmentManager;

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lmiuix/appcompat/internal/app/widget/L$a;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroidx/fragment/app/qa;

.field private e:Landroidx/fragment/app/Fragment;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroidx/fragment/app/FragmentManager;)V
    .locals 1

    invoke-direct {p0}, Landroidx/viewpager/widget/f;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/L;->c:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/L;->d:Landroidx/fragment/app/qa;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/L;->e:Landroidx/fragment/app/Fragment;

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/L;->a:Landroid/content/Context;

    iput-object p2, p0, Lmiuix/appcompat/internal/app/widget/L;->b:Landroidx/fragment/app/FragmentManager;

    return-void
.end method


# virtual methods
.method a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;Landroidx/appcompat/app/ActionBar$c;Z)I
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "+",
            "Landroidx/fragment/app/Fragment;",
            ">;",
            "Landroid/os/Bundle;",
            "Landroidx/appcompat/app/ActionBar$c;",
            "Z)I"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    new-instance v9, Lmiuix/appcompat/internal/app/widget/L$a;

    goto/32 :goto_7

    nop

    :goto_1
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/L;->a()Z

    move-result v0

    goto/32 :goto_19

    nop

    :goto_2
    move-object v5, p2

    goto/32 :goto_f

    nop

    :goto_3
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/L;->c:Ljava/util/ArrayList;

    goto/32 :goto_20

    nop

    :goto_4
    invoke-direct/range {v1 .. v7}, Lmiuix/appcompat/internal/app/widget/L$a;-><init>(Lmiuix/appcompat/internal/app/widget/L;Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;Landroidx/appcompat/app/ActionBar$c;Z)V

    goto/32 :goto_5

    nop

    :goto_5
    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_6
    goto/32 :goto_e

    nop

    :goto_7
    move-object v2, v9

    goto/32 :goto_8

    nop

    :goto_8
    move-object v3, p0

    goto/32 :goto_1a

    nop

    :goto_9
    move-object v2, p0

    goto/32 :goto_15

    nop

    :goto_a
    move-object v5, p3

    goto/32 :goto_c

    nop

    :goto_b
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/L;->c:Ljava/util/ArrayList;

    goto/32 :goto_d

    nop

    :goto_c
    move-object v6, p4

    goto/32 :goto_16

    nop

    :goto_d
    const/4 v1, 0x0

    goto/32 :goto_0

    nop

    :goto_e
    invoke-virtual {p0}, Landroidx/viewpager/widget/f;->notifyDataSetChanged()V

    goto/32 :goto_3

    nop

    :goto_f
    move-object v6, p3

    goto/32 :goto_11

    nop

    :goto_10
    new-instance v8, Lmiuix/appcompat/internal/app/widget/L$a;

    goto/32 :goto_13

    nop

    :goto_11
    move-object v7, p4

    goto/32 :goto_1f

    nop

    :goto_12
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/L;->c:Ljava/util/ArrayList;

    goto/32 :goto_10

    nop

    :goto_13
    move-object v1, v8

    goto/32 :goto_9

    nop

    :goto_14
    move-object v4, p2

    goto/32 :goto_a

    nop

    :goto_15
    move-object v3, p1

    goto/32 :goto_14

    nop

    :goto_16
    move v7, p5

    goto/32 :goto_4

    nop

    :goto_17
    invoke-direct/range {v2 .. v8}, Lmiuix/appcompat/internal/app/widget/L$a;-><init>(Lmiuix/appcompat/internal/app/widget/L;Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;Landroidx/appcompat/app/ActionBar$c;Z)V

    goto/32 :goto_1c

    nop

    :goto_18
    return p1

    :goto_19
    if-nez v0, :cond_0

    goto/32 :goto_1e

    :cond_0
    goto/32 :goto_b

    nop

    :goto_1a
    move-object v4, p1

    goto/32 :goto_2

    nop

    :goto_1b
    add-int/lit8 p1, p1, -0x1

    goto/32 :goto_18

    nop

    :goto_1c
    invoke-virtual {v0, v1, v9}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto/32 :goto_1d

    nop

    :goto_1d
    goto :goto_6

    :goto_1e
    goto/32 :goto_12

    nop

    :goto_1f
    move v8, p5

    goto/32 :goto_17

    nop

    :goto_20
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    goto/32 :goto_1b

    nop
.end method

.method a(I)Landroidx/appcompat/app/ActionBar$c;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/L;->c:Ljava/util/ArrayList;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_3

    nop

    :goto_2
    return-object p1

    :goto_3
    check-cast p1, Lmiuix/appcompat/internal/app/widget/L$a;

    goto/32 :goto_4

    nop

    :goto_4
    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/L$a;->e:Landroidx/appcompat/app/ActionBar$c;

    goto/32 :goto_2

    nop
.end method

.method a(IZ)Landroidx/fragment/app/Fragment;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0, p1, p2, v0}, Lmiuix/appcompat/internal/app/widget/L;->a(IZZ)Landroidx/fragment/app/Fragment;

    move-result-object p1

    goto/32 :goto_2

    nop

    :goto_1
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_2
    return-object p1
.end method

.method a(IZZ)Landroidx/fragment/app/Fragment;
    .locals 2

    goto/32 :goto_13

    nop

    :goto_0
    iget-object v0, p1, Lmiuix/appcompat/internal/app/widget/L$a;->a:Ljava/lang/String;

    goto/32 :goto_1c

    nop

    :goto_1
    return-object v1

    :goto_2
    goto/32 :goto_12

    nop

    :goto_3
    iget-object p3, p1, Lmiuix/appcompat/internal/app/widget/L$a;->c:Landroidx/fragment/app/Fragment;

    goto/32 :goto_14

    nop

    :goto_4
    return-object p1

    :goto_5
    if-nez v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_6
    iput-object v1, p1, Lmiuix/appcompat/internal/app/widget/L$a;->b:Ljava/lang/Class;

    goto/32 :goto_1d

    nop

    :goto_7
    if-nez p3, :cond_1

    goto/32 :goto_20

    :cond_1
    goto/32 :goto_1f

    nop

    :goto_8
    if-nez p2, :cond_2

    goto/32 :goto_1e

    :cond_2
    goto/32 :goto_1a

    nop

    :goto_9
    iput-object p2, p1, Lmiuix/appcompat/internal/app/widget/L$a;->c:Landroidx/fragment/app/Fragment;

    goto/32 :goto_6

    nop

    :goto_a
    invoke-static {p3, p2, v0}, Landroidx/fragment/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroidx/fragment/app/Fragment;

    move-result-object p2

    goto/32 :goto_9

    nop

    :goto_b
    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_17

    nop

    :goto_c
    check-cast p1, Lmiuix/appcompat/internal/app/widget/L$a;

    goto/32 :goto_3

    nop

    :goto_d
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    goto/32 :goto_10

    nop

    :goto_e
    if-eqz p3, :cond_3

    goto/32 :goto_1e

    :cond_3
    goto/32 :goto_8

    nop

    :goto_f
    iget-object p3, p1, Lmiuix/appcompat/internal/app/widget/L$a;->c:Landroidx/fragment/app/Fragment;

    goto/32 :goto_e

    nop

    :goto_10
    const/4 v1, 0x0

    goto/32 :goto_5

    nop

    :goto_11
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_c

    nop

    :goto_12
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/L;->c:Ljava/util/ArrayList;

    goto/32 :goto_7

    nop

    :goto_13
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/L;->c:Ljava/util/ArrayList;

    goto/32 :goto_d

    nop

    :goto_14
    if-eqz p3, :cond_4

    goto/32 :goto_1e

    :cond_4
    goto/32 :goto_16

    nop

    :goto_15
    iput-object p3, p1, Lmiuix/appcompat/internal/app/widget/L$a;->c:Landroidx/fragment/app/Fragment;

    goto/32 :goto_f

    nop

    :goto_16
    iget-object p3, p0, Lmiuix/appcompat/internal/app/widget/L;->b:Landroidx/fragment/app/FragmentManager;

    goto/32 :goto_0

    nop

    :goto_17
    iget-object v0, p1, Lmiuix/appcompat/internal/app/widget/L$a;->d:Landroid/os/Bundle;

    goto/32 :goto_a

    nop

    :goto_18
    if-nez p2, :cond_5

    goto/32 :goto_1e

    :cond_5
    goto/32 :goto_1b

    nop

    :goto_19
    iget-object p1, p1, Lmiuix/appcompat/internal/app/widget/L$a;->c:Landroidx/fragment/app/Fragment;

    goto/32 :goto_4

    nop

    :goto_1a
    iget-object p2, p1, Lmiuix/appcompat/internal/app/widget/L$a;->b:Ljava/lang/Class;

    goto/32 :goto_18

    nop

    :goto_1b
    iget-object p3, p0, Lmiuix/appcompat/internal/app/widget/L;->a:Landroid/content/Context;

    goto/32 :goto_b

    nop

    :goto_1c
    invoke-virtual {p3, v0}, Landroidx/fragment/app/FragmentManager;->c(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object p3

    goto/32 :goto_15

    nop

    :goto_1d
    iput-object v1, p1, Lmiuix/appcompat/internal/app/widget/L$a;->d:Landroid/os/Bundle;

    :goto_1e
    goto/32 :goto_19

    nop

    :goto_1f
    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/L;->c(I)I

    move-result p1

    :goto_20
    goto/32 :goto_11

    nop
.end method

.method a()Z
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/L;->a:Landroid/content/Context;

    goto/32 :goto_8

    nop

    :goto_1
    goto :goto_7

    :goto_2
    goto/32 :goto_6

    nop

    :goto_3
    const/4 v1, 0x1

    goto/32 :goto_a

    nop

    :goto_4
    return v1

    :goto_5
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_6
    const/4 v1, 0x0

    :goto_7
    goto/32 :goto_4

    nop

    :goto_8
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_9
    invoke-virtual {v0}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v0

    goto/32 :goto_3

    nop

    :goto_a
    if-eq v0, v1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop
.end method

.method public b(I)Z
    .locals 1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/L;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/L;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lmiuix/appcompat/internal/app/widget/L$a;

    iget-boolean p1, p1, Lmiuix/appcompat/internal/app/widget/L$a;->f:Z

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method c(I)I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/L;->a()Z

    move-result v0

    goto/32 :goto_a

    nop

    :goto_1
    return v0

    :goto_2
    goto/32 :goto_4

    nop

    :goto_3
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/L;->c:Ljava/util/ArrayList;

    goto/32 :goto_7

    nop

    :goto_4
    const/4 p1, 0x0

    goto/32 :goto_c

    nop

    :goto_5
    return p1

    :goto_6
    goto/32 :goto_3

    nop

    :goto_7
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_b

    nop

    :goto_8
    sub-int/2addr v0, p1

    goto/32 :goto_1

    nop

    :goto_9
    if-gt v0, p1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_8

    nop

    :goto_a
    if-eqz v0, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_5

    nop

    :goto_b
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_9

    nop

    :goto_c
    return p1
.end method

.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/L;->d:Landroidx/fragment/app/qa;

    if-nez p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/L;->b:Landroidx/fragment/app/FragmentManager;

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/L;->d:Landroidx/fragment/app/qa;

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/L;->d:Landroidx/fragment/app/qa;

    check-cast p3, Landroidx/fragment/app/Fragment;

    invoke-virtual {p1, p3}, Landroidx/fragment/app/qa;->b(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    return-void
.end method

.method public finishUpdate(Landroid/view/ViewGroup;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/L;->d:Landroidx/fragment/app/qa;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroidx/fragment/app/qa;->b()I

    const/4 p1, 0x0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/L;->d:Landroidx/fragment/app/qa;

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/L;->b:Landroidx/fragment/app/FragmentManager;

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentManager;->p()Z

    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/L;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/L;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/L;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/appcompat/internal/app/widget/L$a;

    iget-object v2, v2, Lmiuix/appcompat/internal/app/widget/L$a;->c:Landroidx/fragment/app/Fragment;

    if-ne p1, v2, :cond_0

    return v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, -0x2

    return p1
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/L;->d:Landroidx/fragment/app/qa;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/L;->b:Landroidx/fragment/app/FragmentManager;

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->b()Landroidx/fragment/app/qa;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/L;->d:Landroidx/fragment/app/qa;

    :cond_0
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p2, v0, v1}, Lmiuix/appcompat/internal/app/widget/L;->a(IZZ)Landroidx/fragment/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/L;->d:Landroidx/fragment/app/qa;

    invoke-virtual {p1, v0}, Landroidx/fragment/app/qa;->a(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/qa;

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/L;->d:Landroidx/fragment/app/qa;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result p1

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/L;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lmiuix/appcompat/internal/app/widget/L$a;

    iget-object p2, p2, Lmiuix/appcompat/internal/app/widget/L$a;->a:Ljava/lang/String;

    invoke-virtual {v2, p1, v0, p2}, Landroidx/fragment/app/qa;->a(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/qa;

    :goto_0
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/L;->e:Landroidx/fragment/app/Fragment;

    if-eq v0, p1, :cond_2

    invoke-virtual {v0, v1}, Landroidx/fragment/app/Fragment;->setMenuVisibility(Z)V

    invoke-virtual {v0, v1}, Landroidx/fragment/app/Fragment;->setUserVisibleHint(Z)V

    :cond_2
    return-object v0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 0

    check-cast p2, Landroidx/fragment/app/Fragment;

    invoke-virtual {p2}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object p2

    if-ne p2, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    check-cast p3, Landroidx/fragment/app/Fragment;

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/L;->e:Landroidx/fragment/app/Fragment;

    if-eq p3, p1, :cond_2

    if-eqz p1, :cond_0

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroidx/fragment/app/Fragment;->setMenuVisibility(Z)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/L;->e:Landroidx/fragment/app/Fragment;

    invoke-virtual {p1, p2}, Landroidx/fragment/app/Fragment;->setUserVisibleHint(Z)V

    :cond_0
    if-eqz p3, :cond_1

    const/4 p1, 0x1

    invoke-virtual {p3, p1}, Landroidx/fragment/app/Fragment;->setMenuVisibility(Z)V

    invoke-virtual {p3, p1}, Landroidx/fragment/app/Fragment;->setUserVisibleHint(Z)V

    :cond_1
    iput-object p3, p0, Lmiuix/appcompat/internal/app/widget/L;->e:Landroidx/fragment/app/Fragment;

    :cond_2
    return-void
.end method

.method public startUpdate(Landroid/view/ViewGroup;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method
