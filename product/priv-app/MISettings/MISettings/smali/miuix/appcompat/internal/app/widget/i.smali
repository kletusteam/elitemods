.class abstract Lmiuix/appcompat/internal/app/widget/i;
.super Landroid/view/ViewGroup;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/internal/app/widget/i$a;
    }
.end annotation


# instance fields
.field protected a:Lmiuix/animation/a/a;

.field protected b:Lmiuix/animation/a/a;

.field protected c:Lmiuix/animation/a/a;

.field protected d:Lmiuix/animation/a/a;

.field protected e:Lmiuix/animation/a/a;

.field protected f:Lmiuix/appcompat/internal/view/menu/action/ActionMenuView;

.field protected g:Lmiuix/appcompat/internal/view/menu/action/ActionMenuPresenter;

.field protected h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

.field protected i:Z

.field protected j:Z

.field protected k:I

.field protected l:I

.field protected m:I

.field protected n:I

.field o:Lmiuix/appcompat/app/g;

.field p:I

.field q:I

.field private r:Z

.field protected s:Z

.field protected t:I

.field u:F

.field protected v:Lmiuix/animation/e/b;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiuix/appcompat/internal/app/widget/i;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lmiuix/appcompat/internal/app/widget/i;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p3, 0x1

    iput p3, p0, Lmiuix/appcompat/internal/app/widget/i;->p:I

    iput p3, p0, Lmiuix/appcompat/internal/app/widget/i;->q:I

    iput-boolean p3, p0, Lmiuix/appcompat/internal/app/widget/i;->r:Z

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/i;->u:F

    new-instance v0, Lmiuix/appcompat/internal/app/widget/f;

    invoke-direct {v0, p0}, Lmiuix/appcompat/internal/app/widget/f;-><init>(Lmiuix/appcompat/internal/app/widget/i;)V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/i;->v:Lmiuix/animation/e/b;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/i;->s:Z

    const/4 v1, -0x1

    iput v1, p0, Lmiuix/appcompat/internal/app/widget/i;->t:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ld/b/e;->miuix_appcompat_action_bar_title_collapse_padding_vertical:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lmiuix/appcompat/internal/app/widget/i;->m:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ld/b/e;->miuix_appcompat_action_bar_subtitle_collapse_padding_vertical:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lmiuix/appcompat/internal/app/widget/i;->n:I

    new-instance v1, Lmiuix/animation/a/a;

    invoke-direct {v1}, Lmiuix/animation/a/a;-><init>()V

    const/4 v2, 0x2

    new-array v3, v2, [F

    fill-array-data v3, :array_0

    const/4 v4, -0x2

    invoke-virtual {v1, v4, v3}, Lmiuix/animation/a/a;->a(I[F)Lmiuix/animation/a/a;

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/i;->a:Lmiuix/animation/a/a;

    new-instance v1, Lmiuix/animation/a/a;

    invoke-direct {v1}, Lmiuix/animation/a/a;-><init>()V

    new-array v3, v2, [F

    fill-array-data v3, :array_1

    invoke-virtual {v1, v4, v3}, Lmiuix/animation/a/a;->a(I[F)Lmiuix/animation/a/a;

    new-array v3, p3, [Lmiuix/animation/e/b;

    iget-object v5, p0, Lmiuix/appcompat/internal/app/widget/i;->v:Lmiuix/animation/e/b;

    aput-object v5, v3, v0

    invoke-virtual {v1, v3}, Lmiuix/animation/a/a;->a([Lmiuix/animation/e/b;)Lmiuix/animation/a/a;

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/i;->c:Lmiuix/animation/a/a;

    new-instance v1, Lmiuix/animation/a/a;

    invoke-direct {v1}, Lmiuix/animation/a/a;-><init>()V

    new-array v3, v2, [F

    fill-array-data v3, :array_2

    invoke-virtual {v1, v4, v3}, Lmiuix/animation/a/a;->a(I[F)Lmiuix/animation/a/a;

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/i;->b:Lmiuix/animation/a/a;

    new-instance v1, Lmiuix/animation/a/a;

    invoke-direct {v1}, Lmiuix/animation/a/a;-><init>()V

    new-array v3, v2, [F

    fill-array-data v3, :array_3

    invoke-virtual {v1, v4, v3}, Lmiuix/animation/a/a;->a(I[F)Lmiuix/animation/a/a;

    new-array v3, p3, [Lmiuix/animation/e/b;

    iget-object v5, p0, Lmiuix/appcompat/internal/app/widget/i;->v:Lmiuix/animation/e/b;

    aput-object v5, v3, v0

    invoke-virtual {v1, v3}, Lmiuix/animation/a/a;->a([Lmiuix/animation/e/b;)Lmiuix/animation/a/a;

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/i;->d:Lmiuix/animation/a/a;

    new-instance v1, Lmiuix/animation/a/a;

    invoke-direct {v1}, Lmiuix/animation/a/a;-><init>()V

    new-array v2, v2, [F

    fill-array-data v2, :array_4

    invoke-virtual {v1, v4, v2}, Lmiuix/animation/a/a;->a(I[F)Lmiuix/animation/a/a;

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/i;->e:Lmiuix/animation/a/a;

    sget-object v1, Ld/b/l;->ActionBar:[I

    const v2, 0x10102ce

    invoke-virtual {p1, p2, v1, v2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    sget p2, Ld/b/l;->ActionBar_expandState:I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    sget v1, Ld/b/l;->ActionBar_resizable:I

    invoke-virtual {p1, v1, p3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/i;->h()Z

    move-result p1

    if-eqz p1, :cond_0

    iget p1, p0, Lmiuix/appcompat/internal/app/widget/i;->t:I

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/i;->p:I

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/i;->q:I

    goto :goto_1

    :cond_0
    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/i;->d()Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    iput p3, p0, Lmiuix/appcompat/internal/app/widget/i;->p:I

    iput p3, p0, Lmiuix/appcompat/internal/app/widget/i;->q:I

    goto :goto_1

    :cond_2
    :goto_0
    iput v0, p0, Lmiuix/appcompat/internal/app/widget/i;->p:I

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/i;->q:I

    :goto_1
    iput-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/i;->r:Z

    return-void

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3e99999a    # 0.3f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3e99999a    # 0.3f
    .end array-data

    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x3e19999a    # 0.15f
    .end array-data

    :array_3
    .array-data 4
        0x3f800000    # 1.0f
        0x3e19999a    # 0.15f
    .end array-data

    :array_4
    .array-data 4
        0x3f800000    # 1.0f
        0x3f19999a    # 0.6f
    .end array-data
.end method

.method private setTitleMaxHeight(I)V
    .locals 0

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/i;->l:I

    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    return-void
.end method

.method private setTitleMinHeight(I)V
    .locals 0

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/i;->k:I

    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/view/View;III)I
    .locals 1

    const/high16 v0, -0x80000000

    invoke-static {p2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p1, v0, p3}, Landroid/view/View;->measure(II)V

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    sub-int/2addr p2, p1

    sub-int/2addr p2, p4

    const/4 p1, 0x0

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result p1

    return p1
.end method

.method protected a(Landroid/view/View;IIIZ)I
    .locals 8

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr p4, v1

    div-int/lit8 p4, p4, 0x2

    add-int/2addr p3, p4

    if-nez p5, :cond_0

    iget p3, p0, Lmiuix/appcompat/internal/app/widget/i;->k:I

    sub-int/2addr p3, v1

    div-int/lit8 p3, p3, 0x2

    :cond_0
    move v5, p3

    add-int v6, p2, v0

    add-int v7, v5, v1

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    invoke-static/range {v2 .. v7}, Ld/h/a/i;->a(Landroid/view/ViewGroup;Landroid/view/View;IIII)V

    return v0
.end method

.method protected a(II)V
    .locals 0

    return-void
.end method

.method protected b(Landroid/view/View;III)I
    .locals 6

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lmiuix/appcompat/internal/app/widget/i;->a(Landroid/view/View;IIIZ)I

    move-result p1

    return p1
.end method

.method protected abstract b(II)V
.end method

.method protected c(Landroid/view/View;III)I
    .locals 7

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p3

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p4

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/i;->k:I

    sub-int/2addr v0, p4

    div-int/lit8 v4, v0, 0x2

    sub-int v3, p2, p3

    add-int v6, v4, p4

    move-object v1, p0

    move-object v2, p1

    move v5, p2

    invoke-static/range {v1 .. v6}, Ld/h/a/i;->a(Landroid/view/ViewGroup;Landroid/view/View;IIII)V

    return p3
.end method

.method public c()Z
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/i;->g:Lmiuix/appcompat/internal/view/menu/action/ActionMenuPresenter;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Lmiuix/appcompat/internal/view/menu/action/ActionMenuPresenter;->c(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method protected d()Z
    .locals 2

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ld/h/a/e;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/i;->h()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public e()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/i;->g:Lmiuix/appcompat/internal/view/menu/action/ActionMenuPresenter;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/action/ActionMenuPresenter;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public f()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/i;->g:Lmiuix/appcompat/internal/view/menu/action/ActionMenuPresenter;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/action/ActionMenuPresenter;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public g()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/i;->r:Z

    return v0
.end method

.method getActionBarStyle()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    const v0, 0x10102ce

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method public getActionBarTransitionListener()Lmiuix/appcompat/app/g;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/i;->o:Lmiuix/appcompat/app/g;

    return-object v0
.end method

.method public getActionMenuView()Lmiuix/appcompat/internal/view/menu/action/ActionMenuView;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/i;->f:Lmiuix/appcompat/internal/view/menu/action/ActionMenuView;

    return-object v0
.end method

.method public getAnimatedVisibility()I
    .locals 1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    return v0
.end method

.method public getExpandState()I
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/i;->q:I

    return v0
.end method

.method public getMenuView()Lmiuix/appcompat/internal/view/menu/action/ActionMenuView;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/i;->f:Lmiuix/appcompat/internal/view/menu/action/ActionMenuView;

    return-object v0
.end method

.method public abstract getTitleView()Landroid/view/View;
.end method

.method protected h()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/i;->s:Z

    return v0
.end method

.method public i()V
    .locals 1

    new-instance v0, Lmiuix/appcompat/internal/app/widget/g;

    invoke-direct {v0, p0}, Lmiuix/appcompat/internal/app/widget/g;-><init>(Lmiuix/appcompat/internal/app/widget/i;)V

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public j()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/i;->g:Lmiuix/appcompat/internal/view/menu/action/ActionMenuPresenter;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/action/ActionMenuPresenter;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Ld/b/l;->ActionBar:[I

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/i;->getActionBarStyle()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Ld/b/l;->ActionBar_android_minHeight:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v1

    invoke-direct {p0, v1}, Lmiuix/appcompat/internal/app/widget/i;->setTitleMinHeight(I)V

    sget v1, Ld/b/l;->ActionBar_android_maxHeight:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v1

    invoke-direct {p0, v1}, Lmiuix/appcompat/internal/app/widget/i;->setTitleMaxHeight(I)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/i;->j:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Ld/b/c;->abc_split_action_bar_is_narrow:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/i;->setSplitActionBar(Z)V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/i;->g:Lmiuix/appcompat/internal/view/menu/action/ActionMenuPresenter;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/view/menu/action/ActionMenuPresenter;->a(Landroid/content/res/Configuration;)V

    :cond_1
    return-void
.end method

.method public setActionBarTransitionListener(Lmiuix/appcompat/app/g;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/i;->o:Lmiuix/appcompat/app/g;

    return-void
.end method

.method public setExpandState(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v0}, Lmiuix/appcompat/internal/app/widget/i;->setExpandState(IZZ)V

    return-void
.end method

.method public setExpandState(IZZ)V
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/i;->r:Z

    if-nez v0, :cond_0

    if-eqz p3, :cond_4

    :cond_0
    iget p3, p0, Lmiuix/appcompat/internal/app/widget/i;->p:I

    if-eq p3, p1, :cond_4

    if-eqz p2, :cond_1

    invoke-virtual {p0, p3, p1}, Lmiuix/appcompat/internal/app/widget/i;->a(II)V

    goto :goto_1

    :cond_1
    iput p1, p0, Lmiuix/appcompat/internal/app/widget/i;->p:I

    if-nez p1, :cond_2

    const/4 p2, 0x0

    iput p2, p0, Lmiuix/appcompat/internal/app/widget/i;->q:I

    goto :goto_0

    :cond_2
    const/4 p2, 0x1

    if-ne p1, p2, :cond_3

    iput p2, p0, Lmiuix/appcompat/internal/app/widget/i;->q:I

    :cond_3
    :goto_0
    invoke-virtual {p0, p3, p1}, Lmiuix/appcompat/internal/app/widget/i;->b(II)V

    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    :cond_4
    :goto_1
    return-void
.end method

.method protected setExpandStateByUser(I)V
    .locals 1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/i;->s:Z

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/i;->t:I

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/i;->s:Z

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/i;->t:I

    :goto_0
    return-void
.end method

.method public setResizable(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/i;->r:Z

    return-void
.end method

.method public setSplitActionBar(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/i;->i:Z

    return-void
.end method

.method public setSplitView(Lmiuix/appcompat/internal/app/widget/ActionBarContainer;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/i;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    return-void
.end method

.method public setSplitWhenNarrow(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/i;->j:Z

    return-void
.end method

.method public setVisibility(I)V
    .locals 1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_0
    return-void
.end method
