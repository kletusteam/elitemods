.class public final Lmiuix/appcompat/internal/view/menu/k;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/MenuItem;


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Ljava/lang/String;

.field private static c:Ljava/lang/String;

.field private static d:Ljava/lang/String;


# instance fields
.field private A:Landroid/view/ContextMenu$ContextMenuInfo;

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private i:Ljava/lang/CharSequence;

.field private j:Ljava/lang/CharSequence;

.field private k:Ljava/lang/CharSequence;

.field private l:Landroid/content/Intent;

.field private m:C

.field private n:C

.field private o:Landroid/graphics/drawable/Drawable;

.field private p:I

.field private q:Lmiuix/appcompat/internal/view/menu/i;

.field private r:Lmiuix/appcompat/internal/view/menu/o;

.field private s:Ljava/lang/Runnable;

.field private t:Landroid/view/MenuItem$OnMenuItemClickListener;

.field private u:I

.field private v:I

.field private w:Landroid/view/View;

.field private x:Landroid/view/ActionProvider;

.field private y:Landroid/view/MenuItem$OnActionExpandListener;

.field private z:Z


# direct methods
.method constructor <init>(Lmiuix/appcompat/internal/view/menu/i;IIIILjava/lang/CharSequence;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/appcompat/internal/view/menu/k;->p:I

    const/16 v1, 0x10

    iput v1, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    iput v0, p0, Lmiuix/appcompat/internal/view/menu/k;->v:I

    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/k;->z:Z

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    iput p3, p0, Lmiuix/appcompat/internal/view/menu/k;->e:I

    iput p2, p0, Lmiuix/appcompat/internal/view/menu/k;->f:I

    iput p4, p0, Lmiuix/appcompat/internal/view/menu/k;->g:I

    iput p5, p0, Lmiuix/appcompat/internal/view/menu/k;->h:I

    iput-object p6, p0, Lmiuix/appcompat/internal/view/menu/k;->i:Ljava/lang/CharSequence;

    iput p7, p0, Lmiuix/appcompat/internal/view/menu/k;->v:I

    return-void
.end method


# virtual methods
.method public a()Landroid/view/ActionProvider;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->x:Landroid/view/ActionProvider;

    return-object v0
.end method

.method a(Lmiuix/appcompat/internal/view/menu/n$a;)Ljava/lang/CharSequence;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    if-nez p1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_8

    nop

    :goto_1
    return-object p1

    :goto_2
    if-nez p1, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_7

    nop

    :goto_3
    goto :goto_6

    :goto_4
    goto/32 :goto_5

    nop

    :goto_5
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/k;->getTitle()Ljava/lang/CharSequence;

    move-result-object p1

    :goto_6
    goto/32 :goto_1

    nop

    :goto_7
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/k;->getTitleCondensed()Ljava/lang/CharSequence;

    move-result-object p1

    goto/32 :goto_3

    nop

    :goto_8
    invoke-interface {p1}, Lmiuix/appcompat/internal/view/menu/n$a;->a()Z

    move-result p1

    goto/32 :goto_2

    nop
.end method

.method a(Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->A:Landroid/view/ContextMenu$ContextMenuInfo;

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method a(Lmiuix/appcompat/internal/view/menu/o;)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/o;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    goto/32 :goto_3

    nop

    :goto_1
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/k;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_2
    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->r:Lmiuix/appcompat/internal/view/menu/o;

    goto/32 :goto_1

    nop

    :goto_3
    return-void
.end method

.method public a(Z)V
    .locals 1

    iput-boolean p1, p0, Lmiuix/appcompat/internal/view/menu/k;->z:Z

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    return-void
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/k;->h:I

    return v0
.end method

.method b(Z)V
    .locals 3

    goto/32 :goto_a

    nop

    :goto_0
    or-int/2addr p1, v1

    goto/32 :goto_10

    nop

    :goto_1
    and-int/lit8 v1, v0, -0x3

    goto/32 :goto_9

    nop

    :goto_2
    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    goto/32 :goto_7

    nop

    :goto_3
    move p1, v2

    :goto_4
    goto/32 :goto_0

    nop

    :goto_5
    const/4 p1, 0x2

    goto/32 :goto_c

    nop

    :goto_6
    if-nez p1, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_5

    nop

    :goto_7
    invoke-virtual {p1, v2}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    :goto_8
    goto/32 :goto_e

    nop

    :goto_9
    const/4 v2, 0x0

    goto/32 :goto_6

    nop

    :goto_a
    iget v0, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    goto/32 :goto_1

    nop

    :goto_b
    if-ne v0, p1, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_2

    nop

    :goto_c
    goto :goto_4

    :goto_d
    goto/32 :goto_3

    nop

    :goto_e
    return-void

    :goto_f
    iget p1, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    goto/32 :goto_b

    nop

    :goto_10
    iput p1, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    goto/32 :goto_f

    nop
.end method

.method c()C
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-char v0, p0, Lmiuix/appcompat/internal/view/menu/k;->n:C

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method public c(Z)V
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    and-int/lit8 v0, v0, -0x5

    if-eqz p1, :cond_0

    const/4 p1, 0x4

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    or-int/2addr p1, v0

    iput p1, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    return-void
.end method

.method public collapseActionView()Z
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/k;->v:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->w:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->y:Landroid/view/MenuItem$OnActionExpandListener;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Landroid/view/MenuItem$OnActionExpandListener;->onMenuItemActionCollapse(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v0, p0}, Lmiuix/appcompat/internal/view/menu/i;->a(Lmiuix/appcompat/internal/view/menu/k;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method d()Ljava/lang/String;
    .locals 3

    goto/32 :goto_1c

    nop

    :goto_0
    const/16 v2, 0xa

    goto/32 :goto_3

    nop

    :goto_1
    sget-object v0, Lmiuix/appcompat/internal/view/menu/k;->c:Ljava/lang/String;

    goto/32 :goto_4

    nop

    :goto_2
    sget-object v0, Lmiuix/appcompat/internal/view/menu/k;->d:Ljava/lang/String;

    goto/32 :goto_14

    nop

    :goto_3
    if-ne v0, v2, :cond_0

    goto/32 :goto_f

    :cond_0
    goto/32 :goto_18

    nop

    :goto_4
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    goto/32 :goto_12

    nop

    :goto_6
    sget-object v2, Lmiuix/appcompat/internal/view/menu/k;->a:Ljava/lang/String;

    goto/32 :goto_b

    nop

    :goto_7
    goto :goto_5

    :goto_8
    goto/32 :goto_2

    nop

    :goto_9
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_15

    nop

    :goto_a
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/32 :goto_7

    nop

    :goto_b
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    goto/32 :goto_11

    nop

    :goto_c
    if-ne v0, v2, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_a

    nop

    :goto_d
    if-ne v0, v2, :cond_2

    goto/32 :goto_16

    :cond_2
    goto/32 :goto_0

    nop

    :goto_e
    goto :goto_5

    :goto_f
    goto/32 :goto_17

    nop

    :goto_10
    if-eqz v0, :cond_3

    goto/32 :goto_1b

    :cond_3
    goto/32 :goto_13

    nop

    :goto_11
    const/16 v2, 0x8

    goto/32 :goto_d

    nop

    :goto_12
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_19

    nop

    :goto_13
    const-string v0, ""

    goto/32 :goto_1a

    nop

    :goto_14
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_e

    nop

    :goto_15
    goto :goto_5

    :goto_16
    goto/32 :goto_1

    nop

    :goto_17
    sget-object v0, Lmiuix/appcompat/internal/view/menu/k;->b:Ljava/lang/String;

    goto/32 :goto_9

    nop

    :goto_18
    const/16 v2, 0x20

    goto/32 :goto_c

    nop

    :goto_19
    return-object v0

    :goto_1a
    return-object v0

    :goto_1b
    goto/32 :goto_1d

    nop

    :goto_1c
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/k;->c()C

    move-result v0

    goto/32 :goto_10

    nop

    :goto_1d
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop
.end method

.method public d(Z)V
    .locals 0

    if-eqz p1, :cond_0

    iget p1, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    or-int/lit8 p1, p1, 0x20

    iput p1, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    goto :goto_0

    :cond_0
    iget p1, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    and-int/lit8 p1, p1, -0x21

    iput p1, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    :goto_0
    return-void
.end method

.method public e()Z
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/k;->v:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->w:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method e(Z)Z
    .locals 3

    goto/32 :goto_9

    nop

    :goto_0
    const/4 v2, 0x1

    :goto_1
    goto/32 :goto_6

    nop

    :goto_2
    if-nez p1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_8

    nop

    :goto_3
    goto :goto_b

    :goto_4
    goto/32 :goto_a

    nop

    :goto_5
    iput p1, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    goto/32 :goto_d

    nop

    :goto_6
    return v2

    :goto_7
    if-ne v0, p1, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_0

    nop

    :goto_8
    move p1, v2

    goto/32 :goto_3

    nop

    :goto_9
    iget v0, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    goto/32 :goto_e

    nop

    :goto_a
    const/16 p1, 0x8

    :goto_b
    goto/32 :goto_c

    nop

    :goto_c
    or-int/2addr p1, v1

    goto/32 :goto_5

    nop

    :goto_d
    iget p1, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    goto/32 :goto_7

    nop

    :goto_e
    and-int/lit8 v1, v0, -0x9

    goto/32 :goto_f

    nop

    :goto_f
    const/4 v2, 0x0

    goto/32 :goto_2

    nop
.end method

.method public expandActionView()Z
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/k;->v:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->w:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->y:Landroid/view/MenuItem$OnActionExpandListener;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Landroid/view/MenuItem$OnActionExpandListener;->onMenuItemActionExpand(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v0, p0}, Lmiuix/appcompat/internal/view/menu/i;->b(Lmiuix/appcompat/internal/view/menu/k;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public f()Z
    .locals 4

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Landroid/view/MenuItem$OnMenuItemClickListener;->onMenuItemClick(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/i;->l()Lmiuix/appcompat/internal/view/menu/i;

    move-result-object v2

    invoke-virtual {v0, v2, p0}, Lmiuix/appcompat/internal/view/menu/i;->a(Lmiuix/appcompat/internal/view/menu/i;Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_1
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->s:Ljava/lang/Runnable;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    return v1

    :cond_2
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->l:Landroid/content/Intent;

    if-eqz v0, :cond_3

    :try_start_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/i;->d()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/k;->l:Landroid/content/Intent;

    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    :catch_0
    move-exception v0

    const-string v2, "MenuItemImpl"

    const-string v3, "Can\'t find activity to handle intent; ignoring"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_3
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->x:Landroid/view/ActionProvider;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/view/ActionProvider;->onPerformDefaultAction()Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public g()Z
    .locals 2

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    const/16 v1, 0x20

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getActionProvider()Landroid/view/ActionProvider;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Implementation should use getSupportActionProvider!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getActionView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->w:Landroid/view/View;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->x:Landroid/view/ActionProvider;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p0}, Landroid/view/ActionProvider;->onCreateActionView(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->w:Landroid/view/View;

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->w:Landroid/view/View;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAlphabeticShortcut()C
    .locals 1

    iget-char v0, p0, Lmiuix/appcompat/internal/view/menu/k;->n:C

    return v0
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->k:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getGroupId()I
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/k;->f:I

    return v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->o:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    iget v0, p0, Lmiuix/appcompat/internal/view/menu/k;->p:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/i;->k()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lmiuix/appcompat/internal/view/menu/k;->p:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, p0, Lmiuix/appcompat/internal/view/menu/k;->p:I

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->o:Landroid/graphics/drawable/Drawable;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->l:Landroid/content/Intent;

    return-object v0
.end method

.method public getItemId()I
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/k;->e:I

    return v0
.end method

.method public getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->A:Landroid/view/ContextMenu$ContextMenuInfo;

    return-object v0
.end method

.method public getNumericShortcut()C
    .locals 1

    iget-char v0, p0, Lmiuix/appcompat/internal/view/menu/k;->m:C

    return v0
.end method

.method public getOrder()I
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/k;->g:I

    return v0
.end method

.method public getSubMenu()Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->r:Lmiuix/appcompat/internal/view/menu/o;

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->i:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTitleCondensed()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->j:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->i:Ljava/lang/CharSequence;

    :goto_0
    return-object v0
.end method

.method public h()Z
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasSubMenu()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->r:Lmiuix/appcompat/internal/view/menu/o;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public i()Z
    .locals 2

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/k;->v:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isActionViewExpanded()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/k;->z:Z

    return v0
.end method

.method public isCheckable()Z
    .locals 2

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isChecked()Z
    .locals 2

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    const/4 v1, 0x2

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isEnabled()Z
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isVisible()Z
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->x:Landroid/view/ActionProvider;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/ActionProvider;->overridesItemVisibility()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->x:Landroid/view/ActionProvider;

    invoke-virtual {v0}, Landroid/view/ActionProvider;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    return v1

    :cond_1
    iget v0, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_2

    goto :goto_1

    :cond_2
    move v1, v2

    :goto_1
    return v1
.end method

.method public j()Z
    .locals 2

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/k;->v:I

    const/4 v1, 0x2

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public k()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/i;->j()Z

    move-result v0

    return v0
.end method

.method l()Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/i;->o()Z

    move-result v0

    goto/32 :goto_a

    nop

    :goto_2
    const/4 v0, 0x1

    goto/32 :goto_4

    nop

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_2

    nop

    :goto_4
    goto :goto_9

    :goto_5
    goto/32 :goto_8

    nop

    :goto_6
    return v0

    :goto_7
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/k;->c()C

    move-result v0

    goto/32 :goto_3

    nop

    :goto_8
    const/4 v0, 0x0

    :goto_9
    goto/32 :goto_6

    nop

    :goto_a
    if-nez v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_7

    nop
.end method

.method public setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Implementation should use setSupportActionProvider!"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setActionView(I)Landroid/view/MenuItem;
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/i;->d()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    invoke-virtual {v1, p1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/k;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setActionView(Landroid/view/View;)Landroid/view/MenuItem;
    .locals 2

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->w:Landroid/view/View;

    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->x:Landroid/view/ActionProvider;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/k;->e:I

    if-lez v0, :cond_0

    invoke-virtual {p1, v0}, Landroid/view/View;->setId(I)V

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {p1, p0}, Lmiuix/appcompat/internal/view/menu/i;->c(Lmiuix/appcompat/internal/view/menu/k;)V

    return-object p0
.end method

.method public setAlphabeticShortcut(C)Landroid/view/MenuItem;
    .locals 1

    iget-char v0, p0, Lmiuix/appcompat/internal/view/menu/k;->n:C

    if-ne v0, p1, :cond_0

    return-object p0

    :cond_0
    invoke-static {p1}, Ljava/lang/Character;->toLowerCase(C)C

    move-result p1

    iput-char p1, p0, Lmiuix/appcompat/internal/view/menu/k;->n:C

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    return-object p0
.end method

.method public setCheckable(Z)Landroid/view/MenuItem;
    .locals 2

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    and-int/lit8 v1, v0, -0x2

    or-int/2addr p1, v1

    iput p1, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    iget p1, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    if-eq v0, p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    :cond_0
    return-object p0
.end method

.method public setChecked(Z)Landroid/view/MenuItem;
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {p1, p0}, Lmiuix/appcompat/internal/view/menu/i;->a(Landroid/view/MenuItem;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/k;->b(Z)V

    :goto_0
    return-object p0
.end method

.method public setContentDescription(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->k:Ljava/lang/CharSequence;

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    return-object p0
.end method

.method public setEnabled(Z)Landroid/view/MenuItem;
    .locals 1

    if-eqz p1, :cond_0

    iget p1, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    or-int/lit8 p1, p1, 0x10

    iput p1, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    goto :goto_0

    :cond_0
    iget p1, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    and-int/lit8 p1, p1, -0x11

    iput p1, p0, Lmiuix/appcompat/internal/view/menu/k;->u:I

    :goto_0
    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    return-object p0
.end method

.method public setIcon(I)Landroid/view/MenuItem;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->o:Landroid/graphics/drawable/Drawable;

    iput p1, p0, Lmiuix/appcompat/internal/view/menu/k;->p:I

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    return-object p0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/appcompat/internal/view/menu/k;->p:I

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->o:Landroid/graphics/drawable/Drawable;

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    return-object p0
.end method

.method public setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->l:Landroid/content/Intent;

    return-object p0
.end method

.method public setNumericShortcut(C)Landroid/view/MenuItem;
    .locals 1

    iget-char v0, p0, Lmiuix/appcompat/internal/view/menu/k;->m:C

    if-ne v0, p1, :cond_0

    return-object p0

    :cond_0
    iput-char p1, p0, Lmiuix/appcompat/internal/view/menu/k;->m:C

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    return-object p0
.end method

.method public setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Implementation should use setSupportOnActionExpandListener!"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    return-object p0
.end method

.method public setShortcut(CC)Landroid/view/MenuItem;
    .locals 0

    iput-char p1, p0, Lmiuix/appcompat/internal/view/menu/k;->m:C

    invoke-static {p2}, Ljava/lang/Character;->toLowerCase(C)C

    move-result p1

    iput-char p1, p0, Lmiuix/appcompat/internal/view/menu/k;->n:C

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    return-object p0
.end method

.method public setShowAsAction(I)V
    .locals 2

    and-int/lit8 v0, p1, 0x3

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    iput p1, p0, Lmiuix/appcompat/internal/view/menu/k;->v:I

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {p1, p0}, Lmiuix/appcompat/internal/view/menu/i;->c(Lmiuix/appcompat/internal/view/menu/k;)V

    return-void
.end method

.method public setShowAsActionFlags(I)Landroid/view/MenuItem;
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/k;->setShowAsAction(I)V

    return-object p0
.end method

.method public setTitle(I)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/i;->d()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/k;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 2

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->i:Ljava/lang/CharSequence;

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->r:Lmiuix/appcompat/internal/view/menu/o;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/view/menu/o;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    :cond_0
    return-object p0
.end method

.method public setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->j:Ljava/lang/CharSequence;

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    return-object p0
.end method

.method public setVisible(Z)Landroid/view/MenuItem;
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/k;->e(Z)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/k;->q:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {p1, p0}, Lmiuix/appcompat/internal/view/menu/i;->d(Lmiuix/appcompat/internal/view/menu/k;)V

    :cond_0
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/k;->i:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
