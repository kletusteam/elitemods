.class Lmiuix/appcompat/internal/view/menu/f;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiuix/appcompat/internal/view/menu/g;-><init>(Lmiuix/appcompat/app/f;Landroid/view/Menu;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lmiuix/appcompat/internal/view/menu/g;


# direct methods
.method constructor <init>(Lmiuix/appcompat/internal/view/menu/g;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/f;->a:Lmiuix/appcompat/internal/view/menu/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/f;->a:Lmiuix/appcompat/internal/view/menu/g;

    invoke-static {p1}, Lmiuix/appcompat/internal/view/menu/g;->a(Lmiuix/appcompat/internal/view/menu/g;)Lmiuix/appcompat/internal/view/menu/c;

    move-result-object p1

    invoke-virtual {p1, p3}, Lmiuix/appcompat/internal/view/menu/c;->getItem(I)Landroid/view/MenuItem;

    move-result-object p1

    invoke-interface {p1}, Landroid/view/MenuItem;->hasSubMenu()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object p1

    iget-object p2, p0, Lmiuix/appcompat/internal/view/menu/f;->a:Lmiuix/appcompat/internal/view/menu/g;

    new-instance p3, Lmiuix/appcompat/internal/view/menu/e;

    invoke-direct {p3, p0, p1}, Lmiuix/appcompat/internal/view/menu/e;-><init>(Lmiuix/appcompat/internal/view/menu/f;Landroid/view/SubMenu;)V

    invoke-virtual {p2, p3}, Lmiuix/internal/widget/h;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lmiuix/appcompat/internal/view/menu/f;->a:Lmiuix/appcompat/internal/view/menu/g;

    invoke-static {p2}, Lmiuix/appcompat/internal/view/menu/g;->d(Lmiuix/appcompat/internal/view/menu/g;)Lmiuix/appcompat/app/f;

    move-result-object p2

    const/4 p3, 0x0

    invoke-virtual {p2, p3, p1}, Lmiuix/appcompat/app/f;->a(ILandroid/view/MenuItem;)Z

    :goto_0
    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/f;->a:Lmiuix/appcompat/internal/view/menu/g;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lmiuix/appcompat/internal/view/menu/g;->b(Z)V

    return-void
.end method
