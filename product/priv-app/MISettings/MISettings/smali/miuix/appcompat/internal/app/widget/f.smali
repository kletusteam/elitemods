.class Lmiuix/appcompat/internal/app/widget/f;
.super Lmiuix/animation/e/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/appcompat/internal/app/widget/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lmiuix/appcompat/internal/app/widget/i;


# direct methods
.method constructor <init>(Lmiuix/appcompat/internal/app/widget/i;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/f;->a:Lmiuix/appcompat/internal/app/widget/i;

    invoke-direct {p0}, Lmiuix/animation/e/b;-><init>()V

    return-void
.end method


# virtual methods
.method public onBegin(Ljava/lang/Object;)V
    .locals 1

    invoke-super {p0, p1}, Lmiuix/animation/e/b;->onBegin(Ljava/lang/Object;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/f;->a:Lmiuix/appcompat/internal/app/widget/i;

    iget-object v0, v0, Lmiuix/appcompat/internal/app/widget/i;->o:Lmiuix/appcompat/app/g;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lmiuix/appcompat/app/g;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public onComplete(Ljava/lang/Object;)V
    .locals 1

    invoke-super {p0, p1}, Lmiuix/animation/e/b;->onComplete(Ljava/lang/Object;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/f;->a:Lmiuix/appcompat/internal/app/widget/i;

    iget-object v0, v0, Lmiuix/appcompat/internal/app/widget/i;->o:Lmiuix/appcompat/app/g;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lmiuix/appcompat/app/g;->b(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public onUpdate(Ljava/lang/Object;Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/Collection<",
            "Lmiuix/animation/e/c;",
            ">;)V"
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lmiuix/animation/e/b;->onUpdate(Ljava/lang/Object;Ljava/util/Collection;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/f;->a:Lmiuix/appcompat/internal/app/widget/i;

    iget-object v0, v0, Lmiuix/appcompat/internal/app/widget/i;->o:Lmiuix/appcompat/app/g;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lmiuix/appcompat/app/g;->a(Ljava/lang/Object;Ljava/util/Collection;)V

    :cond_0
    return-void
.end method
