.class final enum Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;

.field public static final enum b:Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;

.field public static final enum c:Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;

.field public static final enum d:Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;

.field private static final synthetic e:[Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;

    const/4 v1, 0x0

    const-string v2, "Collapsed"

    invoke-direct {v0, v2, v1}, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;->a:Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;

    new-instance v0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;

    const/4 v2, 0x1

    const-string v3, "Expanding"

    invoke-direct {v0, v3, v2}, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;->b:Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;

    new-instance v0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;

    const/4 v3, 0x2

    const-string v4, "Expanded"

    invoke-direct {v0, v4, v3}, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;->c:Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;

    new-instance v0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;

    const/4 v4, 0x3

    const-string v5, "Collapsing"

    invoke-direct {v0, v5, v4}, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;->d:Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;

    const/4 v0, 0x4

    new-array v0, v0, [Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;

    sget-object v5, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;->a:Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;

    aput-object v5, v0, v1

    sget-object v1, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;->b:Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;

    aput-object v1, v0, v2

    sget-object v1, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;->c:Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;

    aput-object v1, v0, v3

    sget-object v1, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;->d:Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;

    aput-object v1, v0, v4

    sput-object v0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;->e:[Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;
    .locals 1

    const-class v0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;

    return-object p0
.end method

.method public static values()[Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;
    .locals 1

    sget-object v0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;->e:[Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;

    invoke-virtual {v0}, [Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView$a;

    return-object v0
.end method
