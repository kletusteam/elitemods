.class public Lmiuix/appcompat/internal/view/menu/j;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Lmiuix/appcompat/internal/view/menu/m$a;


# instance fields
.field private a:Lmiuix/appcompat/internal/view/menu/i;

.field private b:Lmiuix/appcompat/app/j;

.field c:Lmiuix/appcompat/internal/view/menu/h;

.field private d:Lmiuix/appcompat/internal/view/menu/m$a;


# direct methods
.method public constructor <init>(Lmiuix/appcompat/internal/view/menu/i;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/j;->a:Lmiuix/appcompat/internal/view/menu/i;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/j;->b:Lmiuix/appcompat/app/j;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/app/j;->dismiss()V

    :cond_0
    return-void
.end method

.method public a(Landroid/os/IBinder;)V
    .locals 5

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/j;->a:Lmiuix/appcompat/internal/view/menu/i;

    new-instance v1, Lmiuix/appcompat/app/j$a;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/i;->d()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lmiuix/appcompat/app/j$a;-><init>(Landroid/content/Context;)V

    new-instance v2, Lmiuix/appcompat/internal/view/menu/h;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/i;->d()Landroid/content/Context;

    move-result-object v3

    sget v4, Ld/b/i;->miuix_appcompat_list_menu_item_layout:I

    invoke-direct {v2, v3, v4}, Lmiuix/appcompat/internal/view/menu/h;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lmiuix/appcompat/internal/view/menu/j;->c:Lmiuix/appcompat/internal/view/menu/h;

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/j;->c:Lmiuix/appcompat/internal/view/menu/h;

    invoke-virtual {v2, p0}, Lmiuix/appcompat/internal/view/menu/h;->a(Lmiuix/appcompat/internal/view/menu/m$a;)V

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/j;->a:Lmiuix/appcompat/internal/view/menu/i;

    iget-object v3, p0, Lmiuix/appcompat/internal/view/menu/j;->c:Lmiuix/appcompat/internal/view/menu/h;

    invoke-virtual {v2, v3}, Lmiuix/appcompat/internal/view/menu/i;->a(Lmiuix/appcompat/internal/view/menu/m;)V

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/j;->c:Lmiuix/appcompat/internal/view/menu/h;

    invoke-virtual {v2}, Lmiuix/appcompat/internal/view/menu/h;->b()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-virtual {v1, v2, p0}, Lmiuix/appcompat/app/j$a;->a(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/j$a;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/i;->h()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v2}, Lmiuix/appcompat/app/j$a;->a(Landroid/view/View;)Lmiuix/appcompat/app/j$a;

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/i;->f()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiuix/appcompat/app/j$a;->a(Landroid/graphics/drawable/Drawable;)Lmiuix/appcompat/app/j$a;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/i;->g()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Lmiuix/appcompat/app/j$a;->b(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/j$a;

    :goto_0
    invoke-virtual {v1, p0}, Lmiuix/appcompat/app/j$a;->a(Landroid/content/DialogInterface$OnKeyListener;)Lmiuix/appcompat/app/j$a;

    invoke-virtual {v1}, Lmiuix/appcompat/app/j$a;->a()Lmiuix/appcompat/app/j;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/j;->b:Lmiuix/appcompat/app/j;

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/j;->b:Lmiuix/appcompat/app/j;

    invoke-virtual {v0, p0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/j;->b:Lmiuix/appcompat/app/j;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    const/16 v1, 0x3eb

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    if-eqz p1, :cond_1

    iput-object p1, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    :cond_1
    iget p1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v1, 0x20000

    or-int/2addr p1, v1

    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/j;->b:Lmiuix/appcompat/app/j;

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public a(Lmiuix/appcompat/internal/view/menu/i;Z)V
    .locals 1

    if-nez p2, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/j;->a:Lmiuix/appcompat/internal/view/menu/i;

    if-ne p1, v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/j;->a()V

    :cond_1
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/j;->d:Lmiuix/appcompat/internal/view/menu/m$a;

    if-eqz v0, :cond_2

    invoke-interface {v0, p1, p2}, Lmiuix/appcompat/internal/view/menu/m$a;->a(Lmiuix/appcompat/internal/view/menu/i;Z)V

    :cond_2
    return-void
.end method

.method public a(Lmiuix/appcompat/internal/view/menu/m$a;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/j;->d:Lmiuix/appcompat/internal/view/menu/m$a;

    return-void
.end method

.method public a(Lmiuix/appcompat/internal/view/menu/i;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/j;->d:Lmiuix/appcompat/internal/view/menu/m$a;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lmiuix/appcompat/internal/view/menu/m$a;->a(Lmiuix/appcompat/internal/view/menu/i;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/j;->a:Lmiuix/appcompat/internal/view/menu/i;

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/j;->c:Lmiuix/appcompat/internal/view/menu/h;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/h;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p2}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lmiuix/appcompat/internal/view/menu/k;

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Lmiuix/appcompat/internal/view/menu/i;->a(Landroid/view/MenuItem;I)Z

    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/j;->c:Lmiuix/appcompat/internal/view/menu/h;

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/j;->a:Lmiuix/appcompat/internal/view/menu/i;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lmiuix/appcompat/internal/view/menu/h;->a(Lmiuix/appcompat/internal/view/menu/i;Z)V

    return-void
.end method

.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 2

    const/16 v0, 0x52

    if-eq p2, v0, :cond_0

    const/4 v0, 0x4

    if-ne p2, v0, :cond_2

    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_1

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/j;->b:Lmiuix/appcompat/app/j;

    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1, p3, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    return v1

    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/j;->b:Lmiuix/appcompat/app/j;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0, p3}, Landroid/view/KeyEvent$DispatcherState;->isTracking(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object p2, p0, Lmiuix/appcompat/internal/view/menu/j;->a:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {p2, v1}, Lmiuix/appcompat/internal/view/menu/i;->a(Z)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return v1

    :cond_2
    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/j;->a:Lmiuix/appcompat/internal/view/menu/i;

    const/4 v0, 0x0

    invoke-virtual {p1, p2, p3, v0}, Lmiuix/appcompat/internal/view/menu/i;->performShortcut(ILandroid/view/KeyEvent;I)Z

    move-result p1

    return p1
.end method
