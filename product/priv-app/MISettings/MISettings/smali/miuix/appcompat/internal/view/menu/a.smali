.class public abstract Lmiuix/appcompat/internal/view/menu/a;
.super Ljava/lang/Object;

# interfaces
.implements Lmiuix/appcompat/internal/view/menu/m;


# instance fields
.field protected a:Landroid/content/Context;

.field protected b:Landroid/content/Context;

.field protected c:Lmiuix/appcompat/internal/view/menu/i;

.field protected d:Landroid/view/LayoutInflater;

.field protected e:Landroid/view/LayoutInflater;

.field private f:Lmiuix/appcompat/internal/view/menu/m$a;

.field private g:I

.field private h:I

.field protected i:Lmiuix/appcompat/internal/view/menu/n;

.field private j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/a;->a:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/a;->d:Landroid/view/LayoutInflater;

    iput p2, p0, Lmiuix/appcompat/internal/view/menu/a;->g:I

    iput p3, p0, Lmiuix/appcompat/internal/view/menu/a;->h:I

    return-void
.end method

.method protected static a(Lmiuix/appcompat/internal/view/menu/i;IIIILjava/lang/CharSequence;I)Lmiuix/appcompat/internal/view/menu/k;
    .locals 9

    new-instance v8, Lmiuix/appcompat/internal/view/menu/k;

    move-object v0, v8

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lmiuix/appcompat/internal/view/menu/k;-><init>(Lmiuix/appcompat/internal/view/menu/i;IIIILjava/lang/CharSequence;I)V

    return-object v8
.end method

.method protected static a(Lmiuix/appcompat/internal/view/menu/i;Lmiuix/appcompat/internal/view/menu/i;Landroid/view/MenuItem;)Z
    .locals 0

    invoke-virtual {p0, p1, p2}, Lmiuix/appcompat/internal/view/menu/i;->a(Lmiuix/appcompat/internal/view/menu/i;Landroid/view/MenuItem;)Z

    move-result p0

    return p0
.end method

.method protected static b(Lmiuix/appcompat/internal/view/menu/i;Z)V
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->a(Z)V

    return-void
.end method

.method protected static c(Lmiuix/appcompat/internal/view/menu/i;Z)V
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->b(Z)V

    return-void
.end method


# virtual methods
.method public a(Lmiuix/appcompat/internal/view/menu/k;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    instance-of v0, p2, Lmiuix/appcompat/internal/view/menu/n$a;

    if-eqz v0, :cond_0

    check-cast p2, Lmiuix/appcompat/internal/view/menu/n$a;

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p3}, Lmiuix/appcompat/internal/view/menu/a;->a(Landroid/view/ViewGroup;)Lmiuix/appcompat/internal/view/menu/n$a;

    move-result-object p2

    :goto_0
    invoke-virtual {p0, p1, p2}, Lmiuix/appcompat/internal/view/menu/a;->a(Lmiuix/appcompat/internal/view/menu/k;Lmiuix/appcompat/internal/view/menu/n$a;)V

    check-cast p2, Landroid/view/View;

    return-object p2
.end method

.method public a(Landroid/view/ViewGroup;)Lmiuix/appcompat/internal/view/menu/n$a;
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a;->d:Landroid/view/LayoutInflater;

    iget v1, p0, Lmiuix/appcompat/internal/view/menu/a;->h:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lmiuix/appcompat/internal/view/menu/n$a;

    return-object p1
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lmiuix/appcompat/internal/view/menu/a;->j:I

    return-void
.end method

.method public a(Landroid/content/Context;Lmiuix/appcompat/internal/view/menu/i;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/a;->b:Landroid/content/Context;

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/a;->b:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/a;->e:Landroid/view/LayoutInflater;

    iput-object p2, p0, Lmiuix/appcompat/internal/view/menu/a;->c:Lmiuix/appcompat/internal/view/menu/i;

    return-void
.end method

.method protected a(Landroid/view/View;I)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/n;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    return-void
.end method

.method public a(Lmiuix/appcompat/internal/view/menu/i;Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a;->f:Lmiuix/appcompat/internal/view/menu/m$a;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lmiuix/appcompat/internal/view/menu/m$a;->a(Lmiuix/appcompat/internal/view/menu/i;Z)V

    :cond_0
    return-void
.end method

.method public abstract a(Lmiuix/appcompat/internal/view/menu/k;Lmiuix/appcompat/internal/view/menu/n$a;)V
.end method

.method public a(Lmiuix/appcompat/internal/view/menu/m$a;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/a;->f:Lmiuix/appcompat/internal/view/menu/m$a;

    return-void
.end method

.method public a(Z)V
    .locals 7

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/n;

    move-object v0, p1

    check-cast v0, Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {p1}, Lmiuix/appcompat/internal/view/menu/n;->a()Z

    move-result p1

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    move p1, v1

    :goto_0
    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/n;

    invoke-interface {v2}, Lmiuix/appcompat/internal/view/menu/n;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    add-int/lit8 p1, p1, 0x1

    :cond_2
    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/a;->c:Lmiuix/appcompat/internal/view/menu/i;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lmiuix/appcompat/internal/view/menu/i;->c()V

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/a;->c:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v2}, Lmiuix/appcompat/internal/view/menu/i;->m()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiuix/appcompat/internal/view/menu/k;

    invoke-virtual {p0, p1, v3}, Lmiuix/appcompat/internal/view/menu/a;->a(ILmiuix/appcompat/internal/view/menu/k;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    instance-of v5, v4, Lmiuix/appcompat/internal/view/menu/n$a;

    if-eqz v5, :cond_4

    move-object v5, v4

    check-cast v5, Lmiuix/appcompat/internal/view/menu/n$a;

    invoke-interface {v5}, Lmiuix/appcompat/internal/view/menu/n$a;->getItemData()Lmiuix/appcompat/internal/view/menu/k;

    move-result-object v5

    goto :goto_2

    :cond_4
    const/4 v5, 0x0

    :goto_2
    invoke-virtual {p0, v3, v4, v0}, Lmiuix/appcompat/internal/view/menu/a;->a(Lmiuix/appcompat/internal/view/menu/k;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    if-eq v3, v5, :cond_5

    invoke-virtual {v6, v1}, Landroid/view/View;->setPressed(Z)V

    :cond_5
    if-eq v6, v4, :cond_6

    invoke-virtual {p0, v6, p1}, Lmiuix/appcompat/internal/view/menu/a;->a(Landroid/view/View;I)V

    :cond_6
    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    :cond_7
    :goto_3
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge p1, v1, :cond_8

    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/n;

    invoke-interface {v1, p1}, Lmiuix/appcompat/internal/view/menu/n;->a(I)Z

    move-result v1

    if-nez v1, :cond_7

    add-int/lit8 p1, p1, 0x1

    goto :goto_3

    :cond_8
    return-void
.end method

.method public abstract a(ILmiuix/appcompat/internal/view/menu/k;)Z
.end method

.method public a(Lmiuix/appcompat/internal/view/menu/i;Lmiuix/appcompat/internal/view/menu/k;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public a(Lmiuix/appcompat/internal/view/menu/o;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a;->f:Lmiuix/appcompat/internal/view/menu/m$a;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lmiuix/appcompat/internal/view/menu/m$a;->a(Lmiuix/appcompat/internal/view/menu/i;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public b(Landroid/view/ViewGroup;)Lmiuix/appcompat/internal/view/menu/n;
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/n;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a;->d:Landroid/view/LayoutInflater;

    iget v1, p0, Lmiuix/appcompat/internal/view/menu/a;->g:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lmiuix/appcompat/internal/view/menu/n;

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/n;

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/n;

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a;->c:Lmiuix/appcompat/internal/view/menu/i;

    invoke-interface {p1, v0}, Lmiuix/appcompat/internal/view/menu/n;->a(Lmiuix/appcompat/internal/view/menu/i;)V

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/a;->a(Z)V

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/n;

    return-object p1
.end method

.method public b(Lmiuix/appcompat/internal/view/menu/i;Lmiuix/appcompat/internal/view/menu/k;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method
