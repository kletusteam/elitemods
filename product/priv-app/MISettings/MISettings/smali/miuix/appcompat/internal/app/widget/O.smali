.class Lmiuix/appcompat/internal/app/widget/O;
.super Ljava/lang/Object;

# interfaces
.implements Lmiuix/appcompat/app/d$a;


# instance fields
.field a:Landroid/graphics/Rect;

.field b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field c:I

.field d:Z

.field e:I

.field f:I

.field g:Landroid/view/ViewGroup;

.field h:Lmiuix/viewpager/widget/ViewPager;

.field i:Lmiuix/appcompat/internal/app/widget/L;


# direct methods
.method public constructor <init>(Lmiuix/viewpager/widget/ViewPager;Lmiuix/appcompat/internal/app/widget/L;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/O;->a:Landroid/graphics/Rect;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/O;->b:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/O;->c:I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/O;->d:Z

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/O;->e:I

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/O;->f:I

    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/O;->g:Landroid/view/ViewGroup;

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/O;->h:Lmiuix/viewpager/widget/ViewPager;

    iput-object p2, p0, Lmiuix/appcompat/internal/app/widget/O;->i:Lmiuix/appcompat/internal/app/widget/L;

    return-void
.end method


# virtual methods
.method a(IIIF)I
    .locals 1

    goto/32 :goto_f

    nop

    :goto_0
    mul-float/2addr p4, p4

    goto/32 :goto_17

    nop

    :goto_1
    cmpl-float p2, p1, p2

    goto/32 :goto_15

    nop

    :goto_2
    mul-int/2addr p1, p2

    goto/32 :goto_18

    nop

    :goto_3
    int-to-float p2, p2

    goto/32 :goto_6

    nop

    :goto_4
    float-to-int p1, p1

    goto/32 :goto_9

    nop

    :goto_5
    const v0, 0x3f666666    # 0.9f

    goto/32 :goto_10

    nop

    :goto_6
    mul-float/2addr p3, p2

    goto/32 :goto_e

    nop

    :goto_7
    move p1, p2

    :goto_8
    goto/32 :goto_0

    nop

    :goto_9
    goto :goto_14

    :goto_a
    goto/32 :goto_13

    nop

    :goto_b
    sub-float/2addr p3, p4

    goto/32 :goto_3

    nop

    :goto_c
    goto :goto_8

    :goto_d
    goto/32 :goto_7

    nop

    :goto_e
    add-float/2addr p1, p3

    goto/32 :goto_11

    nop

    :goto_f
    if-lt p1, p3, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_2

    nop

    :goto_10
    div-float/2addr p4, v0

    goto/32 :goto_b

    nop

    :goto_11
    const/4 p2, 0x0

    goto/32 :goto_1

    nop

    :goto_12
    const p3, 0x3dcccccd    # 0.1f

    goto/32 :goto_5

    nop

    :goto_13
    const/4 p1, 0x0

    :goto_14
    goto/32 :goto_16

    nop

    :goto_15
    if-gtz p2, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_4

    nop

    :goto_16
    return p1

    :goto_17
    int-to-float p1, p1

    goto/32 :goto_12

    nop

    :goto_18
    div-int/2addr p1, p3

    goto/32 :goto_c

    nop
.end method

.method public a(IFZZ)V
    .locals 7

    const/4 p3, 0x0

    cmpl-float p3, p2, p3

    const/4 p4, 0x1

    if-nez p3, :cond_0

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/O;->c:I

    iput-boolean p4, p0, Lmiuix/appcompat/internal/app/widget/O;->d:Z

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/O;->g:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/O;->a(Landroid/view/ViewGroup;)V

    :cond_0
    iget v0, p0, Lmiuix/appcompat/internal/app/widget/O;->e:I

    if-eq v0, p1, :cond_3

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/O;->c:I

    if-ge v0, p1, :cond_1

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/O;->c:I

    goto :goto_0

    :cond_1
    add-int/lit8 v1, p1, 0x1

    if-le v0, v1, :cond_2

    iput v1, p0, Lmiuix/appcompat/internal/app/widget/O;->c:I

    :cond_2
    :goto_0
    iput p1, p0, Lmiuix/appcompat/internal/app/widget/O;->e:I

    iput-boolean p4, p0, Lmiuix/appcompat/internal/app/widget/O;->d:Z

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/O;->g:Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/O;->a(Landroid/view/ViewGroup;)V

    :cond_3
    if-lez p3, :cond_8

    iget-boolean p3, p0, Lmiuix/appcompat/internal/app/widget/O;->d:Z

    const/4 v0, 0x0

    if-eqz p3, :cond_5

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/O;->d:Z

    iget p3, p0, Lmiuix/appcompat/internal/app/widget/O;->c:I

    if-ne p3, p1, :cond_4

    iget-object p3, p0, Lmiuix/appcompat/internal/app/widget/O;->i:Lmiuix/appcompat/internal/app/widget/L;

    invoke-virtual {p3}, Lmiuix/appcompat/internal/app/widget/L;->getCount()I

    move-result p3

    sub-int/2addr p3, p4

    if-ge p1, p3, :cond_4

    add-int/lit8 p3, p1, 0x1

    iput p3, p0, Lmiuix/appcompat/internal/app/widget/O;->f:I

    goto :goto_1

    :cond_4
    iput p1, p0, Lmiuix/appcompat/internal/app/widget/O;->f:I

    :goto_1
    iget-object p3, p0, Lmiuix/appcompat/internal/app/widget/O;->i:Lmiuix/appcompat/internal/app/widget/L;

    iget v1, p0, Lmiuix/appcompat/internal/app/widget/O;->f:I

    invoke-virtual {p3, v1, v0}, Lmiuix/appcompat/internal/app/widget/L;->a(IZ)Landroidx/fragment/app/Fragment;

    move-result-object p3

    const/4 v1, 0x0

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/O;->g:Landroid/view/ViewGroup;

    if-eqz p3, :cond_5

    invoke-virtual {p3}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {p3}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object p3

    const v1, 0x102000a

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    instance-of v1, p3, Landroid/view/ViewGroup;

    if-eqz v1, :cond_5

    check-cast p3, Landroid/view/ViewGroup;

    iput-object p3, p0, Lmiuix/appcompat/internal/app/widget/O;->g:Landroid/view/ViewGroup;

    :cond_5
    iget p3, p0, Lmiuix/appcompat/internal/app/widget/O;->f:I

    if-ne p3, p1, :cond_6

    const/high16 p3, 0x3f800000    # 1.0f

    sub-float p2, p3, p2

    :cond_6
    move v5, p2

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/O;->g:Landroid/view/ViewGroup;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v3

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/O;->g:Landroid/view/ViewGroup;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    iget p2, p0, Lmiuix/appcompat/internal/app/widget/O;->f:I

    if-eq p2, p1, :cond_7

    move v6, p4

    goto :goto_2

    :cond_7
    move v6, v0

    :goto_2
    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Lmiuix/appcompat/internal/app/widget/O;->a(Landroid/view/ViewGroup;IIFZ)V

    :cond_8
    return-void
.end method

.method a(Landroid/view/ViewGroup;)V
    .locals 2

    goto/32 :goto_8

    nop

    :goto_0
    goto :goto_4

    :goto_1
    goto/32 :goto_e

    nop

    :goto_2
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/O;->b:Ljava/util/ArrayList;

    goto/32 :goto_a

    nop

    :goto_3
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_4
    goto/32 :goto_9

    nop

    :goto_5
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_6
    invoke-virtual {p0, p1, v0}, Lmiuix/appcompat/internal/app/widget/O;->a(Landroid/view/ViewGroup;Ljava/util/ArrayList;)V

    goto/32 :goto_2

    nop

    :goto_7
    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    goto/32 :goto_0

    nop

    :goto_8
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/O;->b:Ljava/util/ArrayList;

    goto/32 :goto_6

    nop

    :goto_9
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    goto/32 :goto_c

    nop

    :goto_a
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    goto/32 :goto_d

    nop

    :goto_b
    check-cast v0, Landroid/view/View;

    goto/32 :goto_f

    nop

    :goto_c
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_5

    nop

    :goto_d
    if-eqz p1, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_10

    nop

    :goto_e
    return-void

    :goto_f
    const/4 v1, 0x0

    goto/32 :goto_7

    nop

    :goto_10
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/O;->b:Ljava/util/ArrayList;

    goto/32 :goto_3

    nop
.end method

.method a(Landroid/view/ViewGroup;IIFZ)V
    .locals 6

    goto/32 :goto_1

    nop

    :goto_0
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/O;->b:Ljava/util/ArrayList;

    goto/32 :goto_c

    nop

    :goto_1
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/O;->b:Ljava/util/ArrayList;

    goto/32 :goto_24

    nop

    :goto_2
    const v1, 0x7fffffff

    goto/32 :goto_7

    nop

    :goto_3
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result p1

    goto/32 :goto_2

    nop

    :goto_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    goto/32 :goto_13

    nop

    :goto_6
    int-to-float v4, v0

    goto/32 :goto_12

    nop

    :goto_7
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/O;->b:Ljava/util/ArrayList;

    goto/32 :goto_4

    nop

    :goto_8
    check-cast v3, Landroid/view/View;

    goto/32 :goto_a

    nop

    :goto_9
    move v1, v0

    goto/32 :goto_1d

    nop

    :goto_a
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v4

    goto/32 :goto_15

    nop

    :goto_b
    sub-int v1, v0, p1

    goto/32 :goto_10

    nop

    :goto_c
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    goto/32 :goto_19

    nop

    :goto_d
    goto :goto_17

    :goto_e
    goto/32 :goto_16

    nop

    :goto_f
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v0

    goto/32 :goto_b

    nop

    :goto_10
    invoke-virtual {p0, v1, p2, p3, p4}, Lmiuix/appcompat/internal/app/widget/O;->a(IIIF)I

    move-result v1

    goto/32 :goto_14

    nop

    :goto_11
    if-nez v3, :cond_0

    goto/32 :goto_23

    :cond_0
    goto/32 :goto_20

    nop

    :goto_12
    invoke-virtual {v3, v4}, Landroid/view/View;->setTranslationX(F)V

    goto/32 :goto_22

    nop

    :goto_13
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    goto/32 :goto_11

    nop

    :goto_14
    if-nez p5, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_d

    nop

    :goto_15
    if-ne v1, v4, :cond_2

    goto/32 :goto_1e

    :cond_2
    goto/32 :goto_f

    nop

    :goto_16
    neg-int v1, v1

    :goto_17
    goto/32 :goto_1c

    nop

    :goto_18
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/O;->b:Ljava/util/ArrayList;

    goto/32 :goto_1b

    nop

    :goto_19
    if-eqz p1, :cond_3

    goto/32 :goto_23

    :cond_3
    goto/32 :goto_18

    nop

    :goto_1a
    check-cast p1, Landroid/view/View;

    goto/32 :goto_3

    nop

    :goto_1b
    const/4 v0, 0x0

    goto/32 :goto_21

    nop

    :goto_1c
    move v5, v1

    goto/32 :goto_9

    nop

    :goto_1d
    move v0, v5

    :goto_1e
    goto/32 :goto_6

    nop

    :goto_1f
    return-void

    :goto_20
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_8

    nop

    :goto_21
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_1a

    nop

    :goto_22
    goto/16 :goto_5

    :goto_23
    goto/32 :goto_1f

    nop

    :goto_24
    invoke-virtual {p0, p1, v0}, Lmiuix/appcompat/internal/app/widget/O;->a(Landroid/view/ViewGroup;Ljava/util/ArrayList;)V

    goto/32 :goto_0

    nop
.end method

.method a(Landroid/view/ViewGroup;Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_17

    nop

    :goto_0
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    goto/32 :goto_13

    nop

    :goto_1
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    goto/32 :goto_0

    nop

    :goto_2
    if-gtz v3, :cond_0

    goto/32 :goto_8

    :cond_0
    :goto_3
    goto/32 :goto_7

    nop

    :goto_4
    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    goto/32 :goto_14

    nop

    :goto_5
    if-eq v3, v4, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_9

    nop

    :goto_6
    invoke-static {p1, v0}, Ld/h/a/i;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    goto/32 :goto_d

    nop

    :goto_7
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_8
    goto/32 :goto_e

    nop

    :goto_9
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v3

    goto/32 :goto_2

    nop

    :goto_a
    return-void

    :goto_b
    return-void

    :goto_c
    goto/32 :goto_12

    nop

    :goto_d
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/O;->a:Landroid/graphics/Rect;

    goto/32 :goto_4

    nop

    :goto_e
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_15

    nop

    :goto_f
    const/4 v1, 0x0

    :goto_10
    goto/32 :goto_11

    nop

    :goto_11
    if-lt v1, v0, :cond_2

    goto/32 :goto_16

    :cond_2
    goto/32 :goto_1

    nop

    :goto_12
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    goto/32 :goto_f

    nop

    :goto_13
    const/16 v4, 0x8

    goto/32 :goto_5

    nop

    :goto_14
    if-nez v0, :cond_3

    goto/32 :goto_c

    :cond_3
    goto/32 :goto_b

    nop

    :goto_15
    goto :goto_10

    :goto_16
    goto/32 :goto_a

    nop

    :goto_17
    invoke-virtual {p0, p2, p1}, Lmiuix/appcompat/internal/app/widget/O;->a(Ljava/util/ArrayList;Landroid/view/ViewGroup;)V

    goto/32 :goto_19

    nop

    :goto_18
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/O;->a:Landroid/graphics/Rect;

    goto/32 :goto_6

    nop

    :goto_19
    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    goto/32 :goto_18

    nop
.end method

.method a(Ljava/util/ArrayList;Landroid/view/ViewGroup;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;",
            "Landroid/view/ViewGroup;",
            ")V"
        }
    .end annotation

    goto/32 :goto_8

    nop

    :goto_0
    if-nez v1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_3

    nop

    :goto_1
    const/4 v2, 0x0

    goto/32 :goto_e

    nop

    :goto_2
    const/4 v2, -0x1

    goto/32 :goto_5

    nop

    :goto_3
    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationX(F)V

    goto/32 :goto_6

    nop

    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    goto/32 :goto_f

    nop

    :goto_5
    if-eq v1, v2, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_d

    nop

    :goto_6
    goto :goto_9

    :goto_7
    goto/32 :goto_b

    nop

    :goto_8
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_9
    goto/32 :goto_4

    nop

    :goto_a
    check-cast v0, Landroid/view/View;

    goto/32 :goto_c

    nop

    :goto_b
    return-void

    :goto_c
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v1

    goto/32 :goto_2

    nop

    :goto_d
    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v1

    goto/32 :goto_1

    nop

    :goto_e
    cmpl-float v1, v1, v2

    goto/32 :goto_0

    nop

    :goto_f
    if-nez v0, :cond_2

    goto/32 :goto_7

    :cond_2
    goto/32 :goto_10

    nop

    :goto_10
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_a

    nop
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0

    if-nez p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/O;->h:Lmiuix/viewpager/widget/ViewPager;

    invoke-virtual {p1}, Landroidx/viewpager/widget/OriginalViewPager;->getCurrentItem()I

    move-result p1

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/O;->c:I

    const/4 p1, 0x1

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/O;->d:Z

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/O;->g:Landroid/view/ViewGroup;

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/O;->a(Landroid/view/ViewGroup;)V

    :cond_0
    return-void
.end method

.method public onPageSelected(I)V
    .locals 0

    return-void
.end method
