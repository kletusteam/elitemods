.class public Lmiuix/appcompat/internal/view/menu/l;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/widget/PopupWindow$OnDismissListener;
.implements Lmiuix/appcompat/internal/view/menu/m;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/internal/view/menu/l$a;
    }
.end annotation


# static fields
.field private static final a:I


# instance fields
.field b:Z

.field private c:Landroid/content/Context;

.field private d:Landroid/view/LayoutInflater;

.field private e:Lmiuix/internal/widget/h;

.field private f:Lmiuix/appcompat/internal/view/menu/i;

.field private g:Landroid/view/View;

.field private h:Z

.field private i:Lmiuix/appcompat/internal/view/menu/l$a;

.field private j:Lmiuix/appcompat/internal/view/menu/m$a;

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget v0, Ld/b/i;->miuix_appcompat_popup_menu_item_layout:I

    sput v0, Lmiuix/appcompat/internal/view/menu/l;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lmiuix/appcompat/internal/view/menu/i;Landroid/view/View;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Lmiuix/appcompat/internal/view/menu/l;->a:I

    iput v0, p0, Lmiuix/appcompat/internal/view/menu/l;->k:I

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/l;->c:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/l;->d:Landroid/view/LayoutInflater;

    iput-object p2, p0, Lmiuix/appcompat/internal/view/menu/l;->f:Lmiuix/appcompat/internal/view/menu/i;

    iput-boolean p4, p0, Lmiuix/appcompat/internal/view/menu/l;->h:Z

    iput-object p3, p0, Lmiuix/appcompat/internal/view/menu/l;->g:Landroid/view/View;

    invoke-virtual {p2, p0}, Lmiuix/appcompat/internal/view/menu/i;->a(Lmiuix/appcompat/internal/view/menu/m;)V

    return-void
.end method

.method static synthetic a(Lmiuix/appcompat/internal/view/menu/l;)Z
    .locals 0

    iget-boolean p0, p0, Lmiuix/appcompat/internal/view/menu/l;->h:Z

    return p0
.end method

.method static synthetic b(Lmiuix/appcompat/internal/view/menu/l;)I
    .locals 0

    iget p0, p0, Lmiuix/appcompat/internal/view/menu/l;->k:I

    return p0
.end method

.method static synthetic c(Lmiuix/appcompat/internal/view/menu/l;)Landroid/view/LayoutInflater;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/view/menu/l;->d:Landroid/view/LayoutInflater;

    return-object p0
.end method

.method static synthetic d(Lmiuix/appcompat/internal/view/menu/l;)Lmiuix/appcompat/internal/view/menu/i;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/view/menu/l;->f:Lmiuix/appcompat/internal/view/menu/i;

    return-object p0
.end method


# virtual methods
.method public a(I)V
    .locals 0

    iput p1, p0, Lmiuix/appcompat/internal/view/menu/l;->k:I

    return-void
.end method

.method public a(Landroid/content/Context;Lmiuix/appcompat/internal/view/menu/i;)V
    .locals 0

    return-void
.end method

.method public a(Lmiuix/appcompat/internal/view/menu/i;Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/l;->f:Lmiuix/appcompat/internal/view/menu/i;

    if-eq p1, v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/view/menu/l;->b(Z)V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/l;->j:Lmiuix/appcompat/internal/view/menu/m$a;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1, p2}, Lmiuix/appcompat/internal/view/menu/m$a;->a(Lmiuix/appcompat/internal/view/menu/i;Z)V

    :cond_1
    return-void
.end method

.method public a(Lmiuix/appcompat/internal/view/menu/m$a;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/l;->j:Lmiuix/appcompat/internal/view/menu/m$a;

    return-void
.end method

.method public a(Z)V
    .locals 0

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/l;->i:Lmiuix/appcompat/internal/view/menu/l$a;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/l$a;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public a()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a(Lmiuix/appcompat/internal/view/menu/i;Lmiuix/appcompat/internal/view/menu/k;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public a(Lmiuix/appcompat/internal/view/menu/o;)Z
    .locals 7

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/i;->hasVisibleItems()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    new-instance v0, Lmiuix/appcompat/internal/view/menu/l;

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/l;->c:Landroid/content/Context;

    iget-object v3, p0, Lmiuix/appcompat/internal/view/menu/l;->g:Landroid/view/View;

    invoke-direct {v0, v2, p1, v3, v1}, Lmiuix/appcompat/internal/view/menu/l;-><init>(Landroid/content/Context;Lmiuix/appcompat/internal/view/menu/i;Landroid/view/View;Z)V

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/l;->j:Lmiuix/appcompat/internal/view/menu/m$a;

    invoke-virtual {v0, v2}, Lmiuix/appcompat/internal/view/menu/l;->a(Lmiuix/appcompat/internal/view/menu/m$a;)V

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/i;->size()I

    move-result v2

    move v3, v1

    :goto_0
    const/4 v4, 0x1

    if-ge v3, v2, :cond_1

    invoke-virtual {p1, v3}, Lmiuix/appcompat/internal/view/menu/i;->getItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/MenuItem;->isVisible()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    if-eqz v5, :cond_0

    move v2, v4

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v2, v1

    :goto_1
    invoke-virtual {v0, v2}, Lmiuix/appcompat/internal/view/menu/l;->c(Z)V

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/l;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/l;->j:Lmiuix/appcompat/internal/view/menu/m$a;

    if-eqz v0, :cond_2

    invoke-interface {v0, p1}, Lmiuix/appcompat/internal/view/menu/m$a;->a(Lmiuix/appcompat/internal/view/menu/i;)Z

    :cond_2
    return v4

    :cond_3
    return v1
.end method

.method public b(Z)V
    .locals 0

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/l;->isShowing()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/l;->e:Lmiuix/internal/widget/h;

    invoke-virtual {p1}, Lmiuix/internal/widget/h;->dismiss()V

    :cond_0
    return-void
.end method

.method public b()Z
    .locals 3

    new-instance v0, Lmiuix/internal/widget/h;

    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/l;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lmiuix/internal/widget/h;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/l;->e:Lmiuix/internal/widget/h;

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/l;->e:Lmiuix/internal/widget/h;

    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/l;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ld/b/e;->miuix_appcompat_menu_popup_max_height:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lmiuix/internal/widget/h;->f(I)V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/l;->e:Lmiuix/internal/widget/h;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiuix/internal/widget/h;->a(Z)V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/l;->e:Lmiuix/internal/widget/h;

    invoke-virtual {v0, p0}, Lmiuix/internal/widget/h;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/l;->e:Lmiuix/internal/widget/h;

    invoke-virtual {v0, p0}, Lmiuix/internal/widget/h;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-instance v0, Lmiuix/appcompat/internal/view/menu/l$a;

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/l;->f:Lmiuix/appcompat/internal/view/menu/i;

    invoke-direct {v0, p0, v2}, Lmiuix/appcompat/internal/view/menu/l$a;-><init>(Lmiuix/appcompat/internal/view/menu/l;Lmiuix/appcompat/internal/view/menu/i;)V

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/l;->i:Lmiuix/appcompat/internal/view/menu/l$a;

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/l;->e:Lmiuix/internal/widget/h;

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/l;->i:Lmiuix/appcompat/internal/view/menu/l$a;

    invoke-virtual {v0, v2}, Lmiuix/internal/widget/h;->a(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/l;->e:Lmiuix/internal/widget/h;

    invoke-virtual {v0}, Lmiuix/internal/widget/h;->g()I

    move-result v2

    neg-int v2, v2

    invoke-virtual {v0, v2}, Lmiuix/internal/widget/h;->a(I)V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/l;->e:Lmiuix/internal/widget/h;

    invoke-virtual {v0, v1}, Lmiuix/internal/widget/h;->b(I)V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/l;->e:Lmiuix/internal/widget/h;

    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/l;->g:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lmiuix/internal/widget/h;->a(Landroid/view/View;Landroid/view/ViewGroup;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/l;->e:Lmiuix/internal/widget/h;

    invoke-virtual {v0}, Lmiuix/internal/widget/h;->f()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    const/4 v0, 0x1

    return v0
.end method

.method public b(Lmiuix/appcompat/internal/view/menu/i;Lmiuix/appcompat/internal/view/menu/k;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public c(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/view/menu/l;->b:Z

    return-void
.end method

.method public isShowing()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/l;->e:Lmiuix/internal/widget/h;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onDismiss()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/l;->e:Lmiuix/internal/widget/h;

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/l;->f:Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/i;->close()V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/l;->i:Lmiuix/appcompat/internal/view/menu/l$a;

    invoke-static {p1}, Lmiuix/appcompat/internal/view/menu/l$a;->a(Lmiuix/appcompat/internal/view/menu/l$a;)Lmiuix/appcompat/internal/view/menu/i;

    move-result-object p2

    invoke-virtual {p1, p3}, Lmiuix/appcompat/internal/view/menu/l$a;->getItem(I)Lmiuix/appcompat/internal/view/menu/k;

    move-result-object p1

    const/4 p3, 0x0

    invoke-virtual {p2, p1, p3}, Lmiuix/appcompat/internal/view/menu/i;->a(Landroid/view/MenuItem;I)Z

    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result p1

    const/4 p3, 0x0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/16 p1, 0x52

    if-ne p2, p1, :cond_0

    invoke-virtual {p0, p3}, Lmiuix/appcompat/internal/view/menu/l;->b(Z)V

    return v0

    :cond_0
    return p3
.end method
