.class public Lmiuix/appcompat/internal/view/menu/a/b;
.super Lmiuix/appcompat/internal/view/menu/i;

# interfaces
.implements Landroid/view/ContextMenu;


# instance fields
.field mHelper:Lmiuix/appcompat/internal/view/menu/a/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Landroid/os/IBinder;FF)Lmiuix/appcompat/internal/view/menu/a/d;
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1, p0}, Landroid/view/View;->createContextMenu(Landroid/view/ContextMenu;)V

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/i;->m()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    const v0, 0xc351

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(II)I

    new-instance v0, Lmiuix/appcompat/internal/view/menu/a/d;

    invoke-direct {v0, p0}, Lmiuix/appcompat/internal/view/menu/a/d;-><init>(Lmiuix/appcompat/internal/view/menu/i;)V

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/a/b;->mHelper:Lmiuix/appcompat/internal/view/menu/a/d;

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a/b;->mHelper:Lmiuix/appcompat/internal/view/menu/a/d;

    invoke-virtual {v0, p2, p1, p3, p4}, Lmiuix/appcompat/internal/view/menu/a/d;->a(Landroid/os/IBinder;Landroid/view/View;FF)V

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/a/b;->mHelper:Lmiuix/appcompat/internal/view/menu/a/d;

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public a(Landroid/view/View;Landroid/os/IBinder;)Lmiuix/appcompat/internal/view/menu/j;
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1, p0}, Landroid/view/View;->createContextMenu(Landroid/view/ContextMenu;)V

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/i;->m()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-lez p1, :cond_1

    const p1, 0xc351

    const/4 v0, 0x1

    invoke-static {p1, v0}, Landroid/util/EventLog;->writeEvent(II)I

    new-instance p1, Lmiuix/appcompat/internal/view/menu/j;

    invoke-direct {p1, p0}, Lmiuix/appcompat/internal/view/menu/j;-><init>(Lmiuix/appcompat/internal/view/menu/i;)V

    invoke-virtual {p1, p2}, Lmiuix/appcompat/internal/view/menu/j;->a(Landroid/os/IBinder;)V

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public close()V
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/internal/view/menu/i;->close()V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a/b;->mHelper:Lmiuix/appcompat/internal/view/menu/a/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/a/d;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/a/b;->mHelper:Lmiuix/appcompat/internal/view/menu/a/d;

    :cond_0
    return-void
.end method

.method public setHeaderIcon(I)Landroid/view/ContextMenu;
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->d(I)Lmiuix/appcompat/internal/view/menu/i;

    move-object p1, p0

    check-cast p1, Landroid/view/ContextMenu;

    return-object p1
.end method

.method public setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/ContextMenu;
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->a(Landroid/graphics/drawable/Drawable;)Lmiuix/appcompat/internal/view/menu/i;

    move-object p1, p0

    check-cast p1, Landroid/view/ContextMenu;

    return-object p1
.end method

.method public setHeaderTitle(I)Landroid/view/ContextMenu;
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->e(I)Lmiuix/appcompat/internal/view/menu/i;

    move-object p1, p0

    check-cast p1, Landroid/view/ContextMenu;

    return-object p1
.end method

.method public setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->a(Ljava/lang/CharSequence;)Lmiuix/appcompat/internal/view/menu/i;

    move-object p1, p0

    check-cast p1, Landroid/view/ContextMenu;

    return-object p1
.end method

.method public setHeaderView(Landroid/view/View;)Landroid/view/ContextMenu;
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/view/menu/i;->a(Landroid/view/View;)Lmiuix/appcompat/internal/view/menu/i;

    move-object p1, p0

    check-cast p1, Landroid/view/ContextMenu;

    return-object p1
.end method
