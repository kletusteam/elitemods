.class Lmiuix/appcompat/internal/view/menu/e;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/PopupWindow$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiuix/appcompat/internal/view/menu/f;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/view/SubMenu;

.field final synthetic b:Lmiuix/appcompat/internal/view/menu/f;


# direct methods
.method constructor <init>(Lmiuix/appcompat/internal/view/menu/f;Landroid/view/SubMenu;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->b:Lmiuix/appcompat/internal/view/menu/f;

    iput-object p2, p0, Lmiuix/appcompat/internal/view/menu/e;->a:Landroid/view/SubMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss()V
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->b:Lmiuix/appcompat/internal/view/menu/f;

    iget-object v0, v0, Lmiuix/appcompat/internal/view/menu/f;->a:Lmiuix/appcompat/internal/view/menu/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiuix/internal/widget/h;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->b:Lmiuix/appcompat/internal/view/menu/f;

    iget-object v0, v0, Lmiuix/appcompat/internal/view/menu/f;->a:Lmiuix/appcompat/internal/view/menu/g;

    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/e;->a:Landroid/view/SubMenu;

    invoke-virtual {v0, v1}, Lmiuix/appcompat/internal/view/menu/g;->a(Landroid/view/Menu;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->b:Lmiuix/appcompat/internal/view/menu/f;

    iget-object v0, v0, Lmiuix/appcompat/internal/view/menu/f;->a:Lmiuix/appcompat/internal/view/menu/g;

    invoke-static {v0}, Lmiuix/appcompat/internal/view/menu/g;->b(Lmiuix/appcompat/internal/view/menu/g;)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/e;->b:Lmiuix/appcompat/internal/view/menu/f;

    iget-object v2, v2, Lmiuix/appcompat/internal/view/menu/f;->a:Lmiuix/appcompat/internal/view/menu/g;

    invoke-static {v2}, Lmiuix/appcompat/internal/view/menu/g;->c(Lmiuix/appcompat/internal/view/menu/g;)Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmiuix/internal/widget/h;->b(Landroid/view/View;Landroid/view/ViewGroup;)V

    return-void
.end method
