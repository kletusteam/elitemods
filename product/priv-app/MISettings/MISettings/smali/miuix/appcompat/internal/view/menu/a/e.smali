.class Lmiuix/appcompat/internal/view/menu/a/e;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/PopupWindow$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiuix/appcompat/internal/view/menu/a/f;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/view/SubMenu;

.field final synthetic b:Lmiuix/appcompat/internal/view/menu/a/f;


# direct methods
.method constructor <init>(Lmiuix/appcompat/internal/view/menu/a/f;Landroid/view/SubMenu;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/a/e;->b:Lmiuix/appcompat/internal/view/menu/a/f;

    iput-object p2, p0, Lmiuix/appcompat/internal/view/menu/a/e;->a:Landroid/view/SubMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss()V
    .locals 4

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a/e;->b:Lmiuix/appcompat/internal/view/menu/a/f;

    iget-object v0, v0, Lmiuix/appcompat/internal/view/menu/a/f;->a:Lmiuix/appcompat/internal/view/menu/a/h;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiuix/internal/widget/h;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a/e;->b:Lmiuix/appcompat/internal/view/menu/a/f;

    iget-object v0, v0, Lmiuix/appcompat/internal/view/menu/a/f;->a:Lmiuix/appcompat/internal/view/menu/a/h;

    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/a/e;->a:Landroid/view/SubMenu;

    invoke-virtual {v0, v1}, Lmiuix/appcompat/internal/view/menu/a/h;->a(Landroid/view/Menu;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a/e;->b:Lmiuix/appcompat/internal/view/menu/a/f;

    iget-object v0, v0, Lmiuix/appcompat/internal/view/menu/a/f;->a:Lmiuix/appcompat/internal/view/menu/a/h;

    invoke-static {v0}, Lmiuix/appcompat/internal/view/menu/a/h;->c(Lmiuix/appcompat/internal/view/menu/a/h;)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/a/e;->b:Lmiuix/appcompat/internal/view/menu/a/f;

    iget-object v2, v2, Lmiuix/appcompat/internal/view/menu/a/f;->a:Lmiuix/appcompat/internal/view/menu/a/h;

    invoke-static {v2}, Lmiuix/appcompat/internal/view/menu/a/h;->d(Lmiuix/appcompat/internal/view/menu/a/h;)F

    move-result v2

    iget-object v3, p0, Lmiuix/appcompat/internal/view/menu/a/e;->b:Lmiuix/appcompat/internal/view/menu/a/f;

    iget-object v3, v3, Lmiuix/appcompat/internal/view/menu/a/f;->a:Lmiuix/appcompat/internal/view/menu/a/h;

    invoke-static {v3}, Lmiuix/appcompat/internal/view/menu/a/h;->e(Lmiuix/appcompat/internal/view/menu/a/h;)F

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lmiuix/appcompat/internal/view/menu/a/h;->a(Lmiuix/appcompat/internal/view/menu/a/h;Landroid/view/View;FF)V

    return-void
.end method
