.class Lmiuix/appcompat/widget/Spinner$e;
.super Lmiuix/internal/widget/h;

# interfaces
.implements Lmiuix/appcompat/widget/Spinner$h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/appcompat/widget/Spinner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "e"
.end annotation


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:Landroid/view/View;

.field private F:I

.field private G:I

.field final synthetic H:Lmiuix/appcompat/widget/Spinner;

.field private x:Ljava/lang/CharSequence;

.field y:Landroid/widget/ListAdapter;

.field private final z:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Lmiuix/appcompat/widget/Spinner;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/widget/Spinner$e;->H:Lmiuix/appcompat/widget/Spinner;

    invoke-direct {p0, p2}, Lmiuix/internal/widget/h;-><init>(Landroid/content/Context;)V

    new-instance p3, Landroid/graphics/Rect;

    invoke-direct {p3}, Landroid/graphics/Rect;-><init>()V

    iput-object p3, p0, Lmiuix/appcompat/widget/Spinner$e;->z:Landroid/graphics/Rect;

    const/4 p3, -0x1

    iput p3, p0, Lmiuix/appcompat/widget/Spinner$e;->D:I

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget p4, Ld/b/e;->miuix_appcompat_spinner_magin_screen_horizontal:I

    invoke-virtual {p3, p4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p3

    iput p3, p0, Lmiuix/appcompat/widget/Spinner$e;->B:I

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget p4, Ld/b/e;->miuix_appcompat_spinner_max_height:I

    invoke-virtual {p3, p4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p3

    iput p3, p0, Lmiuix/appcompat/widget/Spinner$e;->F:I

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget p4, Ld/b/e;->miuix_appcompat_spinner_max_width:I

    invoke-virtual {p3, p4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p3

    iput p3, p0, Lmiuix/appcompat/widget/Spinner$e;->G:I

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget p3, Ld/b/e;->miuix_appcompat_spinner_magin_screen_vertical:I

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lmiuix/appcompat/widget/Spinner$e;->C:I

    const p2, 0x800033

    invoke-virtual {p0, p2}, Lmiuix/internal/widget/h;->e(I)V

    new-instance p2, Lmiuix/appcompat/widget/e;

    invoke-direct {p2, p0, p1}, Lmiuix/appcompat/widget/e;-><init>(Lmiuix/appcompat/widget/Spinner$e;Lmiuix/appcompat/widget/Spinner;)V

    invoke-virtual {p0, p2}, Lmiuix/internal/widget/h;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private a(IIII)I
    .locals 8

    invoke-virtual {p0}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v0

    iget v1, p0, Lmiuix/appcompat/widget/Spinner$e;->G:I

    if-le v0, v1, :cond_0

    invoke-virtual {p0, v1}, Landroid/widget/PopupWindow;->setWidth(I)V

    iget v0, p0, Lmiuix/appcompat/widget/Spinner$e;->G:I

    :cond_0
    add-int v1, p1, p2

    add-int v2, p3, p4

    add-int v3, p1, v0

    iget v4, p0, Lmiuix/appcompat/widget/Spinner$e;->B:I

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-gt v3, v2, :cond_1

    move v3, v4

    goto :goto_0

    :cond_1
    move v3, v5

    :goto_0
    sub-int v6, v1, v0

    iget v7, p0, Lmiuix/appcompat/widget/Spinner$e;->B:I

    mul-int/lit8 v7, v7, 0x2

    sub-int v7, v6, v7

    if-lt v7, p3, :cond_2

    goto :goto_1

    :cond_2
    move v4, v5

    :goto_1
    if-eqz v3, :cond_3

    iget p2, p0, Lmiuix/appcompat/widget/Spinner$e;->B:I

    add-int/2addr p1, p2

    add-int p4, p3, p2

    if-ge p1, p4, :cond_7

    add-int p1, p3, p2

    goto :goto_3

    :cond_3
    if-eqz v4, :cond_5

    iget p1, p0, Lmiuix/appcompat/widget/Spinner$e;->B:I

    sub-int p2, v6, p1

    add-int p3, p2, v0

    sub-int p4, v2, p1

    if-le p3, p4, :cond_4

    goto :goto_2

    :cond_4
    move p1, p2

    goto :goto_3

    :cond_5
    sub-int p1, v2, v1

    sub-int/2addr p4, p2

    div-int/lit8 p4, p4, 0x2

    if-lt p1, p4, :cond_6

    iget p1, p0, Lmiuix/appcompat/widget/Spinner$e;->B:I

    add-int/2addr p1, p3

    goto :goto_3

    :cond_6
    iget p1, p0, Lmiuix/appcompat/widget/Spinner$e;->B:I

    :goto_2
    sub-int/2addr v2, p1

    sub-int p1, v2, v0

    :cond_7
    :goto_3
    return p1
.end method

.method private a(II)V
    .locals 2

    invoke-virtual {p0}, Lmiuix/internal/widget/h;->f()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setTextDirection(I)V

    invoke-virtual {v0, p2}, Landroid/widget/ListView;->setTextAlignment(I)V

    iget-object p1, p0, Lmiuix/appcompat/widget/Spinner$e;->H:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {p1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setSelection(I)V

    invoke-virtual {v0, p1, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    return-void
.end method

.method private b(IIII)F
    .locals 5

    invoke-direct {p0}, Lmiuix/appcompat/widget/Spinner$e;->n()I

    move-result v0

    iget v1, p0, Lmiuix/appcompat/widget/Spinner$e;->F:I

    if-le v0, v1, :cond_0

    invoke-virtual {p0, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    iget v0, p0, Lmiuix/appcompat/widget/Spinner$e;->F:I

    :cond_0
    add-int v1, p1, p2

    add-int/2addr p4, p3

    add-int v2, p4, v0

    iget v3, p0, Lmiuix/appcompat/widget/Spinner$e;->C:I

    sub-int v4, v1, v3

    if-ge v2, v4, :cond_1

    int-to-float p2, p4

    add-int p3, p1, v3

    if-ge p4, p3, :cond_6

    add-int/2addr p1, v3

    int-to-float p2, p1

    goto :goto_1

    :cond_1
    sub-int v2, p3, v0

    add-int v4, p1, v3

    if-le v2, v4, :cond_2

    int-to-float p2, v2

    sub-int p1, v1, v3

    if-le p3, p1, :cond_6

    sub-int/2addr v1, v3

    sub-int/2addr v1, v0

    int-to-float p2, v1

    goto :goto_1

    :cond_2
    add-int v2, p1, v3

    if-ge p4, v2, :cond_3

    add-int/2addr p1, v3

    int-to-float p1, p1

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr p2, v3

    int-to-float p2, p2

    float-to-int p2, p2

    invoke-virtual {p0, p2}, Landroid/widget/PopupWindow;->setHeight(I)V

    :goto_0
    move p2, p1

    goto :goto_1

    :cond_3
    sub-int v2, v1, v3

    if-le p3, v2, :cond_4

    sub-int/2addr v1, v3

    sub-int/2addr v1, v0

    int-to-float p1, v1

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr p2, v3

    int-to-float p2, p2

    float-to-int p2, p2

    invoke-virtual {p0, p2}, Landroid/widget/PopupWindow;->setHeight(I)V

    goto :goto_0

    :cond_4
    div-int/lit8 p2, p2, 0x2

    if-ge p3, p2, :cond_5

    int-to-float p2, p4

    sub-int/2addr v1, v3

    sub-int/2addr v1, p4

    int-to-float p1, v1

    float-to-int p1, p1

    invoke-virtual {p0, p1}, Landroid/widget/PopupWindow;->setHeight(I)V

    goto :goto_1

    :cond_5
    sub-int p2, p3, v3

    sub-int/2addr p2, p1

    int-to-float p1, p2

    float-to-int p2, p1

    invoke-virtual {p0, p2}, Landroid/widget/PopupWindow;->setHeight(I)V

    int-to-float p2, p3

    sub-float/2addr p2, p1

    :cond_6
    :goto_1
    invoke-virtual {p0}, Lmiuix/internal/widget/h;->i()Z

    move-result p1

    invoke-virtual {p0, p1}, Lmiuix/internal/widget/h;->c(Z)V

    return p2
.end method

.method private f(Landroid/view/View;)V
    .locals 8

    const/4 v0, 0x2

    new-array v0, v0, [I

    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    const/4 v1, 0x0

    aget v2, v0, v1

    const/4 v3, 0x1

    aget v4, v0, v3

    iget-object v5, p0, Lmiuix/appcompat/widget/Spinner$e;->E:Landroid/view/View;

    if-nez v5, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v5

    :cond_0
    invoke-virtual {v5, v0}, Landroid/view/View;->getLocationInWindow([I)V

    aget v6, v0, v1

    aget v0, v0, v3

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v7

    invoke-direct {p0, v2, v3, v6, v7}, Lmiuix/appcompat/widget/Spinner$e;->a(IIII)I

    move-result v2

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    invoke-direct {p0, v0, v3, v4, v5}, Lmiuix/appcompat/widget/Spinner$e;->b(IIII)F

    move-result v0

    invoke-virtual {p0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v3

    if-nez v3, :cond_1

    float-to-int v0, v0

    invoke-virtual {p0, p1, v1, v2, v0}, Lmiuix/internal/widget/h;->showAtLocation(Landroid/view/View;III)V

    iget-object p1, p0, Lmiuix/internal/widget/h;->g:Landroid/widget/FrameLayout;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getRootView()Landroid/view/View;

    move-result-object p1

    invoke-static {p1}, Lmiuix/internal/widget/h;->a(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    float-to-int p1, v0

    invoke-virtual {p0}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/widget/PopupWindow;->getHeight()I

    move-result v1

    invoke-virtual {p0, v2, p1, v0, v1}, Landroid/widget/PopupWindow;->update(IIII)V

    :goto_0
    return-void
.end method

.method private m()V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner$e;->E:Landroid/view/View;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner$e;->H:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    instance-of v1, v1, Lmiuix/appcompat/app/p;

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/app/p;

    invoke-interface {v1}, Lmiuix/appcompat/app/p;->isInFloatingWindowMode()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    sget v1, Ld/b/g;->action_bar_overlay_layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiuix/appcompat/widget/Spinner$e;->e(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method private n()I
    .locals 7

    invoke-virtual {p0}, Lmiuix/internal/widget/h;->f()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmiuix/internal/widget/h;->f()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    move v2, v1

    move v3, v2

    :goto_0
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v4

    if-ge v2, v4, :cond_1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lmiuix/internal/widget/h;->f()Landroid/widget/ListView;

    move-result-object v5

    invoke-interface {v0, v2, v4, v5}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/view/View;->measure(II)V

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/internal/widget/h;->h:Landroid/view/View;

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/view/View;->measure(II)V

    iget-object v0, p0, Lmiuix/internal/widget/h;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/lit8 v3, v0, 0x0

    :cond_1
    return v3
.end method


# virtual methods
.method public a(IIFF)V
    .locals 1

    invoke-direct {p0}, Lmiuix/appcompat/widget/Spinner$e;->m()V

    invoke-virtual {p0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result p3

    invoke-virtual {p0}, Lmiuix/appcompat/widget/Spinner$e;->k()V

    const/4 p4, 0x2

    invoke-virtual {p0, p4}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    iget-object p4, p0, Lmiuix/appcompat/widget/Spinner$e;->H:Lmiuix/appcompat/widget/Spinner;

    const/4 v0, 0x0

    invoke-virtual {p0, p4, v0}, Lmiuix/internal/widget/h;->c(Landroid/view/View;Landroid/view/ViewGroup;)Z

    move-result p4

    if-eqz p4, :cond_0

    iget-object p4, p0, Lmiuix/appcompat/widget/Spinner$e;->H:Lmiuix/appcompat/widget/Spinner;

    invoke-direct {p0, p4}, Lmiuix/appcompat/widget/Spinner$e;->f(Landroid/view/View;)V

    :cond_0
    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/widget/Spinner$e;->a(II)V

    if-eqz p3, :cond_1

    return-void

    :cond_1
    new-instance p1, Lmiuix/appcompat/widget/f;

    invoke-direct {p1, p0}, Lmiuix/appcompat/widget/f;-><init>(Lmiuix/appcompat/widget/Spinner$e;)V

    invoke-virtual {p0, p1}, Lmiuix/internal/widget/h;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    return-void
.end method

.method public a(Landroid/widget/ListAdapter;)V
    .locals 0

    invoke-super {p0, p1}, Lmiuix/internal/widget/h;->a(Landroid/widget/ListAdapter;)V

    iput-object p1, p0, Lmiuix/appcompat/widget/Spinner$e;->y:Landroid/widget/ListAdapter;

    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/widget/Spinner$e;->x:Ljava/lang/CharSequence;

    return-void
.end method

.method public b()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner$e;->x:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public c(I)V
    .locals 0

    iput p1, p0, Lmiuix/appcompat/widget/Spinner$e;->A:I

    return-void
.end method

.method public d(I)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner$e;->H:Lmiuix/appcompat/widget/Spinner;

    iget v0, v0, Lmiuix/appcompat/widget/Spinner;->h:I

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner$e;->H:Lmiuix/appcompat/widget/Spinner;

    iget v0, v0, Lmiuix/appcompat/widget/Spinner;->g:I

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    invoke-super {p0, p1}, Lmiuix/internal/widget/h;->d(I)V

    return-void
.end method

.method public e(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/widget/Spinner$e;->E:Landroid/view/View;

    return-void
.end method

.method public g(I)V
    .locals 0

    iput p1, p0, Lmiuix/appcompat/widget/Spinner$e;->D:I

    return-void
.end method

.method k()V
    .locals 8

    goto/32 :goto_2e

    nop

    :goto_0
    add-int/2addr v0, v2

    goto/32 :goto_18

    nop

    :goto_1
    invoke-virtual {v0}, Landroid/widget/Spinner;->getPaddingLeft()I

    move-result v0

    goto/32 :goto_4f

    nop

    :goto_2
    iget v5, v4, Lmiuix/appcompat/widget/Spinner;->f:I

    goto/32 :goto_1f

    nop

    :goto_3
    iget-object v0, v0, Lmiuix/appcompat/widget/Spinner;->k:Landroid/graphics/Rect;

    goto/32 :goto_14

    nop

    :goto_4
    if-gt v4, v5, :cond_0

    goto/32 :goto_2d

    :cond_0
    goto/32 :goto_2c

    nop

    :goto_5
    goto :goto_19

    :goto_6
    goto/32 :goto_58

    nop

    :goto_7
    invoke-virtual {p0, v1}, Lmiuix/internal/widget/h;->a(I)V

    goto/32 :goto_49

    nop

    :goto_8
    goto/16 :goto_42

    :goto_9
    goto/32 :goto_4b

    nop

    :goto_a
    if-eq v5, v6, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_40

    nop

    :goto_b
    iget-object v0, v0, Lmiuix/appcompat/widget/Spinner;->k:Landroid/graphics/Rect;

    goto/32 :goto_3e

    nop

    :goto_c
    move v1, v0

    goto/32 :goto_34

    nop

    :goto_d
    sub-int v5, v3, v0

    goto/32 :goto_47

    nop

    :goto_e
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    goto/32 :goto_57

    nop

    :goto_f
    if-eq v5, v4, :cond_2

    goto/32 :goto_5c

    :cond_2
    goto/32 :goto_29

    nop

    :goto_10
    iget v5, p0, Lmiuix/appcompat/widget/Spinner$e;->B:I

    goto/32 :goto_5e

    nop

    :goto_11
    iget v6, v6, Landroid/graphics/Rect;->right:I

    goto/32 :goto_50

    nop

    :goto_12
    invoke-virtual {v3}, Landroid/widget/Spinner;->getWidth()I

    move-result v3

    goto/32 :goto_2a

    nop

    :goto_13
    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner$e;->H:Lmiuix/appcompat/widget/Spinner;

    goto/32 :goto_1

    nop

    :goto_14
    iget v0, v0, Landroid/graphics/Rect;->left:I

    goto/32 :goto_25

    nop

    :goto_15
    invoke-static {v0}, Landroidx/appcompat/widget/Fa;->a(Landroid/view/View;)Z

    move-result v0

    goto/32 :goto_33

    nop

    :goto_16
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    goto/32 :goto_36

    nop

    :goto_17
    add-int/2addr v1, v3

    goto/32 :goto_5

    nop

    :goto_18
    add-int/2addr v1, v0

    :goto_19
    goto/32 :goto_7

    nop

    :goto_1a
    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    goto/32 :goto_51

    nop

    :goto_1b
    iput v1, v0, Landroid/graphics/Rect;->left:I

    :goto_1c
    goto/32 :goto_13

    nop

    :goto_1d
    goto :goto_26

    :goto_1e
    goto/32 :goto_46

    nop

    :goto_1f
    const/4 v6, -0x2

    goto/32 :goto_a

    nop

    :goto_20
    sub-int/2addr v3, v2

    goto/32 :goto_22

    nop

    :goto_21
    sub-int/2addr v3, v0

    goto/32 :goto_53

    nop

    :goto_22
    invoke-virtual {p0}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v0

    goto/32 :goto_21

    nop

    :goto_23
    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner$e;->H:Lmiuix/appcompat/widget/Spinner;

    goto/32 :goto_b

    nop

    :goto_24
    iget-object v0, v0, Lmiuix/appcompat/widget/Spinner;->k:Landroid/graphics/Rect;

    goto/32 :goto_3f

    nop

    :goto_25
    neg-int v0, v0

    :goto_26
    goto/32 :goto_c

    nop

    :goto_27
    mul-int/lit8 v6, v6, 0x2

    goto/32 :goto_3d

    nop

    :goto_28
    iget v6, p0, Lmiuix/appcompat/widget/Spinner$e;->B:I

    goto/32 :goto_4a

    nop

    :goto_29
    sub-int v4, v3, v0

    goto/32 :goto_52

    nop

    :goto_2a
    iget-object v4, p0, Lmiuix/appcompat/widget/Spinner$e;->H:Lmiuix/appcompat/widget/Spinner;

    goto/32 :goto_2

    nop

    :goto_2b
    invoke-virtual {v4, v5, v6}, Lmiuix/appcompat/widget/Spinner;->a(Landroid/widget/SpinnerAdapter;Landroid/graphics/drawable/Drawable;)I

    move-result v4

    goto/32 :goto_5d

    nop

    :goto_2c
    move v4, v5

    :goto_2d
    goto/32 :goto_d

    nop

    :goto_2e
    invoke-virtual {p0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/32 :goto_37

    nop

    :goto_2f
    invoke-virtual {v2}, Landroid/widget/Spinner;->getPaddingRight()I

    move-result v2

    goto/32 :goto_32

    nop

    :goto_30
    sub-int/2addr v5, v7

    goto/32 :goto_11

    nop

    :goto_31
    iget v7, v6, Landroid/graphics/Rect;->left:I

    goto/32 :goto_30

    nop

    :goto_32
    iget-object v3, p0, Lmiuix/appcompat/widget/Spinner$e;->H:Lmiuix/appcompat/widget/Spinner;

    goto/32 :goto_12

    nop

    :goto_33
    if-nez v0, :cond_3

    goto/32 :goto_1e

    :cond_3
    goto/32 :goto_4e

    nop

    :goto_34
    goto :goto_1c

    :goto_35
    goto/32 :goto_23

    nop

    :goto_36
    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    goto/32 :goto_56

    nop

    :goto_37
    const/4 v1, 0x0

    goto/32 :goto_39

    nop

    :goto_38
    iget-object v6, p0, Lmiuix/appcompat/widget/Spinner$e;->H:Lmiuix/appcompat/widget/Spinner;

    goto/32 :goto_44

    nop

    :goto_39
    if-nez v0, :cond_4

    goto/32 :goto_35

    :cond_4
    goto/32 :goto_45

    nop

    :goto_3a
    invoke-static {v4}, Landroidx/appcompat/widget/Fa;->a(Landroid/view/View;)Z

    move-result v4

    goto/32 :goto_54

    nop

    :goto_3b
    iget-object v1, v1, Lmiuix/appcompat/widget/Spinner;->k:Landroid/graphics/Rect;

    goto/32 :goto_e

    nop

    :goto_3c
    sub-int/2addr v4, v5

    goto/32 :goto_43

    nop

    :goto_3d
    sub-int/2addr v5, v6

    goto/32 :goto_1a

    nop

    :goto_3e
    iput v1, v0, Landroid/graphics/Rect;->right:I

    goto/32 :goto_1b

    nop

    :goto_3f
    iget v0, v0, Landroid/graphics/Rect;->right:I

    goto/32 :goto_1d

    nop

    :goto_40
    iget-object v5, p0, Lmiuix/appcompat/widget/Spinner$e;->y:Landroid/widget/ListAdapter;

    goto/32 :goto_55

    nop

    :goto_41
    invoke-virtual {p0, v5}, Lmiuix/appcompat/widget/Spinner$e;->d(I)V

    :goto_42
    goto/32 :goto_5a

    nop

    :goto_43
    invoke-virtual {p0, v4}, Lmiuix/appcompat/widget/Spinner$e;->d(I)V

    goto/32 :goto_5b

    nop

    :goto_44
    iget-object v6, v6, Lmiuix/appcompat/widget/Spinner;->k:Landroid/graphics/Rect;

    goto/32 :goto_31

    nop

    :goto_45
    iget-object v1, p0, Lmiuix/appcompat/widget/Spinner$e;->H:Lmiuix/appcompat/widget/Spinner;

    goto/32 :goto_3b

    nop

    :goto_46
    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner$e;->H:Lmiuix/appcompat/widget/Spinner;

    goto/32 :goto_3

    nop

    :goto_47
    sub-int/2addr v5, v2

    goto/32 :goto_5f

    nop

    :goto_48
    sub-int/2addr v5, v6

    goto/32 :goto_4

    nop

    :goto_49
    return-void

    :goto_4a
    mul-int/lit8 v6, v6, 0x2

    goto/32 :goto_48

    nop

    :goto_4b
    const/4 v4, -0x1

    goto/32 :goto_f

    nop

    :goto_4c
    sub-int/2addr v3, v0

    goto/32 :goto_17

    nop

    :goto_4d
    invoke-virtual {v5}, Landroid/widget/Spinner;->getContext()Landroid/content/Context;

    move-result-object v5

    goto/32 :goto_16

    nop

    :goto_4e
    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner$e;->H:Lmiuix/appcompat/widget/Spinner;

    goto/32 :goto_24

    nop

    :goto_4f
    iget-object v2, p0, Lmiuix/appcompat/widget/Spinner$e;->H:Lmiuix/appcompat/widget/Spinner;

    goto/32 :goto_2f

    nop

    :goto_50
    sub-int/2addr v5, v6

    goto/32 :goto_28

    nop

    :goto_51
    invoke-virtual {p0, v4}, Lmiuix/appcompat/widget/Spinner$e;->d(I)V

    goto/32 :goto_8

    nop

    :goto_52
    sub-int/2addr v4, v2

    goto/32 :goto_10

    nop

    :goto_53
    invoke-virtual {p0}, Lmiuix/appcompat/widget/Spinner$e;->l()I

    move-result v0

    goto/32 :goto_4c

    nop

    :goto_54
    if-nez v4, :cond_5

    goto/32 :goto_6

    :cond_5
    goto/32 :goto_20

    nop

    :goto_55
    check-cast v5, Landroid/widget/SpinnerAdapter;

    goto/32 :goto_59

    nop

    :goto_56
    iget v5, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    goto/32 :goto_38

    nop

    :goto_57
    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner$e;->H:Lmiuix/appcompat/widget/Spinner;

    goto/32 :goto_15

    nop

    :goto_58
    invoke-virtual {p0}, Lmiuix/appcompat/widget/Spinner$e;->l()I

    move-result v2

    goto/32 :goto_0

    nop

    :goto_59
    invoke-virtual {p0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    goto/32 :goto_2b

    nop

    :goto_5a
    iget-object v4, p0, Lmiuix/appcompat/widget/Spinner$e;->H:Lmiuix/appcompat/widget/Spinner;

    goto/32 :goto_3a

    nop

    :goto_5b
    goto/16 :goto_42

    :goto_5c
    goto/32 :goto_41

    nop

    :goto_5d
    iget-object v5, p0, Lmiuix/appcompat/widget/Spinner$e;->H:Lmiuix/appcompat/widget/Spinner;

    goto/32 :goto_4d

    nop

    :goto_5e
    mul-int/lit8 v5, v5, 0x2

    goto/32 :goto_3c

    nop

    :goto_5f
    iget v6, p0, Lmiuix/appcompat/widget/Spinner$e;->B:I

    goto/32 :goto_27

    nop
.end method

.method public l()I
    .locals 1

    iget v0, p0, Lmiuix/appcompat/widget/Spinner$e;->A:I

    return v0
.end method
