.class Lmiuix/appcompat/widget/a/f;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiuix/appcompat/widget/a/g;->a(Landroid/view/View;Landroid/view/View;ZLmiuix/appcompat/app/j$c;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lmiuix/appcompat/app/j$c;

.field final synthetic c:Landroid/view/View$OnLayoutChangeListener;

.field final synthetic d:Lmiuix/appcompat/widget/a/g;


# direct methods
.method constructor <init>(Lmiuix/appcompat/widget/a/g;ZLmiuix/appcompat/app/j$c;Landroid/view/View$OnLayoutChangeListener;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/widget/a/f;->d:Lmiuix/appcompat/widget/a/g;

    iput-boolean p2, p0, Lmiuix/appcompat/widget/a/f;->a:Z

    iput-object p3, p0, Lmiuix/appcompat/widget/a/f;->b:Lmiuix/appcompat/app/j$c;

    iput-object p4, p0, Lmiuix/appcompat/widget/a/f;->c:Landroid/view/View$OnLayoutChangeListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 6

    invoke-virtual {p1, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    sub-int p2, p5, p3

    const/4 p3, 0x0

    invoke-static {p1, p2, p3}, Lmiuix/appcompat/widget/a/g;->a(Landroid/view/View;IZ)V

    iget-boolean p4, p0, Lmiuix/appcompat/widget/a/f;->a:Z

    new-instance p5, Lmiuix/appcompat/widget/a/g$c;

    iget-object v1, p0, Lmiuix/appcompat/widget/a/f;->d:Lmiuix/appcompat/widget/a/g;

    iget-object v2, p0, Lmiuix/appcompat/widget/a/f;->b:Lmiuix/appcompat/app/j$c;

    iget-object v3, p0, Lmiuix/appcompat/widget/a/f;->c:Landroid/view/View$OnLayoutChangeListener;

    const/4 v5, 0x0

    move-object v0, p5

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lmiuix/appcompat/widget/a/g$c;-><init>(Lmiuix/appcompat/widget/a/g;Lmiuix/appcompat/app/j$c;Landroid/view/View$OnLayoutChangeListener;Landroid/view/View;I)V

    new-instance p6, Lmiuix/appcompat/widget/a/g$d;

    iget-object p3, p0, Lmiuix/appcompat/widget/a/f;->d:Lmiuix/appcompat/widget/a/g;

    iget-boolean p7, p0, Lmiuix/appcompat/widget/a/f;->a:Z

    invoke-direct {p6, p3, p1, p7}, Lmiuix/appcompat/widget/a/g$d;-><init>(Lmiuix/appcompat/widget/a/g;Landroid/view/View;Z)V

    const/4 p3, 0x0

    invoke-static/range {p1 .. p6}, Lmiuix/appcompat/widget/a/g;->a(Landroid/view/View;IIZLmiuix/appcompat/widget/a/g$c;Lmiuix/appcompat/widget/a/g$d;)V

    return-void
.end method
