.class Lmiuix/appcompat/widget/a/e;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiuix/appcompat/widget/a/g;->a(Landroid/view/View;Landroid/view/View;ZLmiuix/appcompat/app/j$c;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Z

.field final synthetic c:Lmiuix/appcompat/app/j$c;

.field final synthetic d:Landroid/view/View$OnLayoutChangeListener;

.field final synthetic e:Lmiuix/appcompat/widget/a/g;


# direct methods
.method constructor <init>(Lmiuix/appcompat/widget/a/g;Landroid/view/View;ZLmiuix/appcompat/app/j$c;Landroid/view/View$OnLayoutChangeListener;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/widget/a/e;->e:Lmiuix/appcompat/widget/a/g;

    iput-object p2, p0, Lmiuix/appcompat/widget/a/e;->a:Landroid/view/View;

    iput-boolean p3, p0, Lmiuix/appcompat/widget/a/e;->b:Z

    iput-object p4, p0, Lmiuix/appcompat/widget/a/e;->c:Lmiuix/appcompat/app/j$c;

    iput-object p5, p0, Lmiuix/appcompat/widget/a/e;->d:Landroid/view/View$OnLayoutChangeListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 6

    invoke-virtual {p1, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    iget-object p2, p0, Lmiuix/appcompat/widget/a/e;->a:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result p4

    const/4 p2, 0x0

    invoke-static {p1, p4, p2}, Lmiuix/appcompat/widget/a/g;->a(Landroid/view/View;IZ)V

    iget-boolean p6, p0, Lmiuix/appcompat/widget/a/e;->b:Z

    new-instance p7, Lmiuix/appcompat/widget/a/g$c;

    iget-object v1, p0, Lmiuix/appcompat/widget/a/e;->e:Lmiuix/appcompat/widget/a/g;

    iget-object v2, p0, Lmiuix/appcompat/widget/a/e;->c:Lmiuix/appcompat/app/j$c;

    iget-object v3, p0, Lmiuix/appcompat/widget/a/e;->d:Landroid/view/View$OnLayoutChangeListener;

    const/4 v5, 0x0

    move-object v0, p7

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lmiuix/appcompat/widget/a/g$c;-><init>(Lmiuix/appcompat/widget/a/g;Lmiuix/appcompat/app/j$c;Landroid/view/View$OnLayoutChangeListener;Landroid/view/View;I)V

    new-instance p8, Lmiuix/appcompat/widget/a/g$d;

    iget-object p3, p0, Lmiuix/appcompat/widget/a/e;->e:Lmiuix/appcompat/widget/a/g;

    iget-boolean p5, p0, Lmiuix/appcompat/widget/a/e;->b:Z

    invoke-direct {p8, p3, p1, p5}, Lmiuix/appcompat/widget/a/g$d;-><init>(Lmiuix/appcompat/widget/a/g;Landroid/view/View;Z)V

    const/4 p5, 0x0

    move-object p3, p1

    invoke-static/range {p3 .. p8}, Lmiuix/appcompat/widget/a/g;->a(Landroid/view/View;IIZLmiuix/appcompat/widget/a/g$c;Lmiuix/appcompat/widget/a/g$d;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
