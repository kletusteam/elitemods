.class public Lmiuix/appcompat/widget/Spinner;
.super Landroid/widget/Spinner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/widget/Spinner$f;,
        Lmiuix/appcompat/widget/Spinner$g;,
        Lmiuix/appcompat/widget/Spinner$e;,
        Lmiuix/appcompat/widget/Spinner$a;,
        Lmiuix/appcompat/widget/Spinner$h;,
        Lmiuix/appcompat/widget/Spinner$c;,
        Lmiuix/appcompat/widget/Spinner$d;,
        Lmiuix/appcompat/widget/Spinner$b;,
        Lmiuix/appcompat/widget/Spinner$SavedState;
    }
.end annotation


# static fields
.field private static a:Ljava/lang/reflect/Field;


# instance fields
.field private final b:Landroid/content/Context;

.field private c:Landroid/widget/SpinnerAdapter;

.field private final d:Z

.field private e:Lmiuix/appcompat/widget/Spinner$h;

.field f:I

.field g:I

.field h:I

.field private i:F

.field private j:F

.field final k:Landroid/graphics/Rect;

.field private l:Lmiuix/appcompat/widget/Spinner$f;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    :try_start_0
    const-class v0, Landroid/widget/Spinner;

    const-string v1, "mForwardingListener"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    sput-object v0, Lmiuix/appcompat/widget/Spinner;->a:Ljava/lang/reflect/Field;

    sget-object v0, Lmiuix/appcompat/widget/Spinner;->a:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Spinner"

    const-string v2, "static initializer: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiuix/appcompat/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    sget v0, Ld/b/b;->miuiSpinnerStyle:I

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0, p2}, Lmiuix/appcompat/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Ld/b/b;->miuiSpinnerStyle:I

    invoke-direct {p0, p1, p2, v0}, Lmiuix/appcompat/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lmiuix/appcompat/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lmiuix/appcompat/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILandroid/content/res/Resources$Theme;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILandroid/content/res/Resources$Theme;)V
    .locals 5

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/widget/Spinner;->k:Landroid/graphics/Rect;

    sget-object v0, Ld/b/l;->Spinner:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    if-eqz p5, :cond_0

    new-instance v2, La/a/d/d;

    invoke-direct {v2, p1, p5}, La/a/d/d;-><init>(Landroid/content/Context;Landroid/content/res/Resources$Theme;)V

    iput-object v2, p0, Lmiuix/appcompat/widget/Spinner;->b:Landroid/content/Context;

    goto :goto_0

    :cond_0
    sget p5, Ld/b/l;->Spinner_popupTheme:I

    invoke-virtual {v0, p5, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p5

    if-eqz p5, :cond_1

    new-instance v2, La/a/d/d;

    invoke-direct {v2, p1, p5}, La/a/d/d;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lmiuix/appcompat/widget/Spinner;->b:Landroid/content/Context;

    goto :goto_0

    :cond_1
    iput-object p1, p0, Lmiuix/appcompat/widget/Spinner;->b:Landroid/content/Context;

    :goto_0
    const/4 p5, -0x1

    if-ne p4, p5, :cond_2

    sget p4, Ld/b/l;->Spinner_spinnerModeCompat:I

    invoke-virtual {v0, p4, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p4

    :cond_2
    const/4 p5, 0x0

    const/4 v2, 0x1

    if-eqz p4, :cond_5

    if-eq p4, v2, :cond_3

    goto :goto_2

    :cond_3
    new-instance p4, Lmiuix/appcompat/widget/Spinner$e;

    iget-object v3, p0, Lmiuix/appcompat/widget/Spinner;->b:Landroid/content/Context;

    invoke-direct {p4, p0, v3, p2, p3}, Lmiuix/appcompat/widget/Spinner$e;-><init>(Lmiuix/appcompat/widget/Spinner;Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iget-object v3, p0, Lmiuix/appcompat/widget/Spinner;->b:Landroid/content/Context;

    sget-object v4, Ld/b/l;->Spinner:[I

    invoke-virtual {v3, p2, v4, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    sget p3, Ld/b/l;->Spinner_android_dropDownWidth:I

    const/4 v3, -0x2

    invoke-virtual {p2, p3, v3}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result p3

    iput p3, p0, Lmiuix/appcompat/widget/Spinner;->f:I

    sget p3, Ld/b/l;->Spinner_dropDownMinWidth:I

    invoke-virtual {p2, p3, v3}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result p3

    iput p3, p0, Lmiuix/appcompat/widget/Spinner;->g:I

    sget p3, Ld/b/l;->Spinner_dropDownMaxWidth:I

    invoke-virtual {p2, p3, v3}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result p3

    iput p3, p0, Lmiuix/appcompat/widget/Spinner;->h:I

    sget p3, Ld/b/l;->Spinner_android_popupBackground:I

    invoke-virtual {p2, p3, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p3

    if-eqz p3, :cond_4

    invoke-virtual {p0, p3}, Lmiuix/appcompat/widget/Spinner;->setPopupBackgroundResource(I)V

    goto :goto_1

    :cond_4
    sget p3, Ld/b/l;->Spinner_android_popupBackground:I

    invoke-virtual {p2, p3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p3

    invoke-virtual {p4, p3}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_1
    sget p3, Ld/b/l;->Spinner_android_prompt:I

    invoke-virtual {v0, p3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p4, p3}, Lmiuix/appcompat/widget/Spinner$e;->a(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    iput-object p4, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    goto :goto_2

    :cond_5
    new-instance p2, Lmiuix/appcompat/widget/Spinner$a;

    invoke-direct {p2, p0, p5}, Lmiuix/appcompat/widget/Spinner$a;-><init>(Lmiuix/appcompat/widget/Spinner;Lmiuix/appcompat/widget/c;)V

    iput-object p2, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    iget-object p2, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    sget p3, Ld/b/l;->Spinner_android_prompt:I

    invoke-virtual {v0, p3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-interface {p2, p3}, Lmiuix/appcompat/widget/Spinner$h;->a(Ljava/lang/CharSequence;)V

    :goto_2
    invoke-direct {p0}, Lmiuix/appcompat/widget/Spinner;->f()V

    sget p2, Ld/b/l;->Spinner_android_entries:I

    invoke-virtual {v0, p2}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object p2

    if-eqz p2, :cond_6

    new-instance p3, Landroid/widget/ArrayAdapter;

    sget p4, Ld/b/i;->miuix_appcompat_simple_spinner_layout:I

    const v3, 0x1020014

    invoke-direct {p3, p1, p4, v3, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    sget p1, Ld/b/i;->miuix_appcompat_simple_spinner_dropdown_item:I

    invoke-virtual {p3, p1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {p0, p3}, Lmiuix/appcompat/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    :cond_6
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    iput-boolean v2, p0, Lmiuix/appcompat/widget/Spinner;->d:Z

    iget-object p1, p0, Lmiuix/appcompat/widget/Spinner;->c:Landroid/widget/SpinnerAdapter;

    if-eqz p1, :cond_7

    invoke-virtual {p0, p1}, Lmiuix/appcompat/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iput-object p5, p0, Lmiuix/appcompat/widget/Spinner;->c:Landroid/widget/SpinnerAdapter;

    :cond_7
    invoke-static {p0, v1}, Lmiuix/view/b;->a(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic a(Lmiuix/appcompat/widget/Spinner;)Lmiuix/appcompat/widget/Spinner$h;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    return-object p0
.end method

.method private b(Landroid/widget/SpinnerAdapter;Landroid/graphics/drawable/Drawable;)I
    .locals 5

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    invoke-interface {p1}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/widget/Spinner;->getMeasuredWidth()I

    move-result v2

    invoke-static {v2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {p0}, Landroid/widget/Spinner;->getMeasuredHeight()I

    move-result v3

    invoke-static {v3, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {p0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v4

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-interface {p1, v4, v1, p0}, Landroid/widget/SpinnerAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x2

    invoke-direct {v1, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    invoke-virtual {p1, v2, v3}, Landroid/view/View;->measure(II)V

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    if-eqz p2, :cond_2

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->k:Landroid/graphics/Rect;

    invoke-virtual {p2, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    iget-object p2, p0, Lmiuix/appcompat/widget/Spinner;->k:Landroid/graphics/Rect;

    iget v0, p2, Landroid/graphics/Rect;->left:I

    iget p2, p2, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, p2

    add-int/2addr p1, v0

    :cond_2
    return p1

    :cond_3
    :goto_0
    return v0
.end method

.method static synthetic b(Lmiuix/appcompat/widget/Spinner;)V
    .locals 0

    invoke-direct {p0}, Lmiuix/appcompat/widget/Spinner;->h()V

    return-void
.end method

.method static synthetic c(Lmiuix/appcompat/widget/Spinner;)V
    .locals 0

    invoke-direct {p0}, Lmiuix/appcompat/widget/Spinner;->j()V

    return-void
.end method

.method private d()V
    .locals 4

    invoke-virtual {p0}, Landroid/widget/Spinner;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v1, v0, [Landroid/view/View;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object v1

    invoke-interface {v1, v0}, Lmiuix/animation/m;->a(I)Lmiuix/animation/m;

    const/high16 v0, 0x3f800000    # 1.0f

    new-array v3, v2, [Lmiuix/animation/m$b;

    invoke-interface {v1, v0, v3}, Lmiuix/animation/m;->b(F[Lmiuix/animation/m$b;)Lmiuix/animation/m;

    new-array v0, v2, [Lmiuix/animation/a/a;

    invoke-interface {v1, v0}, Lmiuix/animation/m;->d([Lmiuix/animation/a/a;)V

    :cond_0
    return-void
.end method

.method private e()V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    instance-of v1, v0, Lmiuix/appcompat/widget/Spinner$e;

    if-eqz v1, :cond_0

    check-cast v0, Lmiuix/appcompat/widget/Spinner$e;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getHeight()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    check-cast v0, Lmiuix/appcompat/widget/Spinner$e;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    check-cast v0, Lmiuix/appcompat/widget/Spinner$e;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setWidth(I)V

    :cond_0
    return-void
.end method

.method private f()V
    .locals 3

    sget-object v0, Lmiuix/appcompat/widget/Spinner;->a:Ljava/lang/reflect/Field;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Spinner"

    const-string v2, "makeSupperForwardingListenerInvalid: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method private g()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->l:Lmiuix/appcompat/widget/Spinner$f;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmiuix/appcompat/widget/Spinner$f;->a()V

    :cond_0
    return-void
.end method

.method private h()V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {v0}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object v0

    new-array v1, v1, [Lmiuix/animation/a/a;

    invoke-interface {v0, v1}, Lmiuix/animation/m;->g([Lmiuix/animation/a/a;)V

    invoke-direct {p0}, Lmiuix/appcompat/widget/Spinner;->g()V

    return-void
.end method

.method private i()Z
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/widget/Spinner;->sendAccessibilityEvent(I)V

    const/4 v0, 0x0

    return v0
.end method

.method private j()V
    .locals 2

    sget v0, Lmiuix/view/d;->A:I

    sget v1, Lmiuix/view/d;->k:I

    invoke-static {p0, v0, v1}, Lmiuix/view/HapticCompat;->a(Landroid/view/View;II)Z

    return-void
.end method


# virtual methods
.method a(Landroid/widget/SpinnerAdapter;Landroid/graphics/drawable/Drawable;)I
    .locals 10

    goto/32 :goto_1c

    nop

    :goto_0
    move v0, v8

    :goto_1
    goto/32 :goto_21

    nop

    :goto_2
    invoke-virtual {v7, v1, v2}, Landroid/view/View;->measure(II)V

    goto/32 :goto_d

    nop

    :goto_3
    add-int/lit8 v5, v3, 0xf

    goto/32 :goto_4

    nop

    :goto_4
    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    goto/32 :goto_11

    nop

    :goto_5
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_f

    nop

    :goto_6
    move v6, v0

    goto/32 :goto_1f

    nop

    :goto_7
    rsub-int/lit8 v5, v5, 0xf

    goto/32 :goto_23

    nop

    :goto_8
    const/4 v5, 0x0

    goto/32 :goto_6

    nop

    :goto_9
    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    goto/32 :goto_25

    nop

    :goto_a
    add-int/2addr p2, p1

    goto/32 :goto_14

    nop

    :goto_b
    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    goto/32 :goto_18

    nop

    :goto_c
    if-ne v8, v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_28

    nop

    :goto_d
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    goto/32 :goto_26

    nop

    :goto_e
    iget-object p1, p0, Lmiuix/appcompat/widget/Spinner;->k:Landroid/graphics/Rect;

    goto/32 :goto_22

    nop

    :goto_f
    goto :goto_20

    :goto_10
    goto/32 :goto_32

    nop

    :goto_11
    sub-int v5, v4, v3

    goto/32 :goto_7

    nop

    :goto_12
    return v6

    :goto_13
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    goto/32 :goto_e

    nop

    :goto_14
    add-int/2addr v6, p2

    :goto_15
    goto/32 :goto_12

    nop

    :goto_16
    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    goto/32 :goto_8

    nop

    :goto_17
    if-lt v3, v4, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_2b

    nop

    :goto_18
    invoke-virtual {p0}, Landroid/widget/Spinner;->getMeasuredHeight()I

    move-result v2

    goto/32 :goto_24

    nop

    :goto_19
    new-instance v8, Landroid/view/ViewGroup$LayoutParams;

    goto/32 :goto_1b

    nop

    :goto_1a
    iget-object p1, p0, Lmiuix/appcompat/widget/Spinner;->k:Landroid/graphics/Rect;

    goto/32 :goto_13

    nop

    :goto_1b
    const/4 v9, -0x2

    goto/32 :goto_27

    nop

    :goto_1c
    const/4 v0, 0x0

    goto/32 :goto_2e

    nop

    :goto_1d
    iget p1, p1, Landroid/graphics/Rect;->right:I

    goto/32 :goto_a

    nop

    :goto_1e
    invoke-virtual {p0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v3

    goto/32 :goto_9

    nop

    :goto_1f
    move-object v7, v5

    :goto_20
    goto/32 :goto_17

    nop

    :goto_21
    invoke-interface {p1, v3, v7, p0}, Landroid/widget/SpinnerAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    goto/32 :goto_2f

    nop

    :goto_22
    iget p2, p1, Landroid/graphics/Rect;->left:I

    goto/32 :goto_1d

    nop

    :goto_23
    sub-int/2addr v3, v5

    goto/32 :goto_16

    nop

    :goto_24
    invoke-static {v2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    goto/32 :goto_1e

    nop

    :goto_25
    invoke-interface {p1}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v4

    goto/32 :goto_3

    nop

    :goto_26
    invoke-static {v6, v8}, Ljava/lang/Math;->max(II)I

    move-result v6

    goto/32 :goto_5

    nop

    :goto_27
    invoke-direct {v8, v9, v9}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    goto/32 :goto_30

    nop

    :goto_28
    move-object v7, v5

    goto/32 :goto_0

    nop

    :goto_29
    return v0

    :goto_2a
    goto/32 :goto_2d

    nop

    :goto_2b
    invoke-interface {p1, v3}, Landroid/widget/SpinnerAdapter;->getItemViewType(I)I

    move-result v8

    goto/32 :goto_c

    nop

    :goto_2c
    if-eqz v8, :cond_2

    goto/32 :goto_31

    :cond_2
    goto/32 :goto_19

    nop

    :goto_2d
    invoke-virtual {p0}, Landroid/widget/Spinner;->getMeasuredWidth()I

    move-result v1

    goto/32 :goto_b

    nop

    :goto_2e
    if-eqz p1, :cond_3

    goto/32 :goto_2a

    :cond_3
    goto/32 :goto_29

    nop

    :goto_2f
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    goto/32 :goto_2c

    nop

    :goto_30
    invoke-virtual {v7, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_31
    goto/32 :goto_2

    nop

    :goto_32
    if-nez p2, :cond_4

    goto/32 :goto_15

    :cond_4
    goto/32 :goto_1a

    nop
.end method

.method a()V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-interface {v0}, Lmiuix/appcompat/widget/Spinner$h;->dismiss()V

    goto/32 :goto_0

    nop
.end method

.method public a(FF)Z
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/widget/Spinner;->getPopupContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lmiuix/core/util/n;->d(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    iget v1, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    div-float v1, p1, v1

    iput v1, p0, Lmiuix/appcompat/widget/Spinner;->i:F

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    div-float v0, p2, v0

    iput v0, p0, Lmiuix/appcompat/widget/Spinner;->j:F

    invoke-direct {p0}, Lmiuix/appcompat/widget/Spinner;->i()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lmiuix/appcompat/widget/Spinner;->e()V

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    invoke-interface {v0}, Lmiuix/appcompat/widget/Spinner$h;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0, p1, p2}, Lmiuix/appcompat/widget/Spinner;->b(FF)V

    sget p1, Lmiuix/view/d;->A:I

    sget p2, Lmiuix/view/d;->o:I

    invoke-static {p0, p1, p2}, Lmiuix/view/HapticCompat;->a(Landroid/view/View;II)Z

    :cond_1
    return v1

    :cond_2
    invoke-super {p0}, Landroid/widget/Spinner;->performClick()Z

    move-result p1

    return p1
.end method

.method public synthetic b()V
    .locals 3

    invoke-virtual {p0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lmiuix/appcompat/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lmiuix/appcompat/widget/Spinner$h;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    instance-of v0, v0, Lmiuix/appcompat/widget/Spinner$e;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->b:Landroid/content/Context;

    invoke-static {v0}, Lmiuix/core/util/h;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lmiuix/appcompat/widget/Spinner;->a()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lmiuix/appcompat/widget/Spinner;->getPopupContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lmiuix/core/util/n;->d(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    iget v1, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget v2, p0, Lmiuix/appcompat/widget/Spinner;->i:F

    mul-float/2addr v1, v2

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    iget v2, p0, Lmiuix/appcompat/widget/Spinner;->j:F

    mul-float/2addr v0, v2

    invoke-virtual {p0, v1, v0}, Lmiuix/appcompat/widget/Spinner;->b(FF)V

    :cond_2
    :goto_0
    return-void
.end method

.method b(FF)V
    .locals 3

    goto/32 :goto_3

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0}, Landroid/widget/Spinner;->getTextDirection()I

    move-result v1

    goto/32 :goto_4

    nop

    :goto_2
    invoke-interface {v0, v1, v2, p1, p2}, Lmiuix/appcompat/widget/Spinner$h;->a(IIFF)V

    goto/32 :goto_0

    nop

    :goto_3
    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    goto/32 :goto_1

    nop

    :goto_4
    invoke-virtual {p0}, Landroid/widget/Spinner;->getTextAlignment()I

    move-result v2

    goto/32 :goto_2

    nop
.end method

.method c()V
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    int-to-float v1, v1

    goto/32 :goto_3

    nop

    :goto_1
    const/4 v1, 0x0

    goto/32 :goto_8

    nop

    :goto_2
    const/4 v0, 0x2

    goto/32 :goto_6

    nop

    :goto_3
    const/4 v2, 0x1

    goto/32 :goto_9

    nop

    :goto_4
    int-to-float v0, v0

    goto/32 :goto_a

    nop

    :goto_5
    return-void

    :goto_6
    new-array v0, v0, [I

    goto/32 :goto_7

    nop

    :goto_7
    invoke-virtual {p0, v0}, Landroid/widget/Spinner;->getLocationInWindow([I)V

    goto/32 :goto_1

    nop

    :goto_8
    aget v1, v0, v1

    goto/32 :goto_0

    nop

    :goto_9
    aget v0, v0, v2

    goto/32 :goto_4

    nop

    :goto_a
    invoke-virtual {p0, v1, v0}, Lmiuix/appcompat/widget/Spinner;->b(FF)V

    goto/32 :goto_5

    nop
.end method

.method public getDropDownHorizontalOffset()I
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmiuix/appcompat/widget/Spinner$h;->a()I

    move-result v0

    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/Spinner;->getDropDownHorizontalOffset()I

    move-result v0

    return v0
.end method

.method public getDropDownVerticalOffset()I
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmiuix/appcompat/widget/Spinner$h;->c()I

    move-result v0

    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/Spinner;->getDropDownVerticalOffset()I

    move-result v0

    return v0
.end method

.method public getDropDownWidth()I
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    if-eqz v0, :cond_0

    iget v0, p0, Lmiuix/appcompat/widget/Spinner;->f:I

    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/Spinner;->getDropDownWidth()I

    move-result v0

    return v0
.end method

.method public getPopupBackground()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmiuix/appcompat/widget/Spinner$h;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-super {p0}, Landroid/widget/Spinner;->getPopupBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public getPopupContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->b:Landroid/content/Context;

    return-object v0
.end method

.method public getPrompt()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmiuix/appcompat/widget/Spinner$h;->b()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-super {p0}, Landroid/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/Spinner;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    new-instance p1, Lmiuix/appcompat/widget/a;

    invoke-direct {p1, p0}, Lmiuix/appcompat/widget/a;-><init>(Lmiuix/appcompat/widget/Spinner;)V

    invoke-virtual {p0, p1}, Landroid/widget/Spinner;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/Spinner;->onDetachedFromWindow()V

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmiuix/appcompat/widget/Spinner$h;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    invoke-interface {v0}, Lmiuix/appcompat/widget/Spinner$h;->dismiss()V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/widget/Spinner;->onMeasure(II)V

    iget-object p2, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    if-eqz p2, :cond_0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result p2

    const/high16 v0, -0x80000000

    if-ne p2, v0, :cond_0

    invoke-virtual {p0}, Landroid/widget/Spinner;->getMeasuredWidth()I

    move-result p2

    invoke-virtual {p0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-virtual {p0}, Landroid/widget/Spinner;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lmiuix/appcompat/widget/Spinner;->b(Landroid/widget/SpinnerAdapter;Landroid/graphics/drawable/Drawable;)I

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result p2

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    invoke-static {p2, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-virtual {p0}, Landroid/widget/Spinner;->getMeasuredHeight()I

    move-result p2

    invoke-virtual {p0, p1, p2}, Landroid/widget/Spinner;->setMeasuredDimension(II)V

    :cond_0
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    check-cast p1, Lmiuix/appcompat/widget/Spinner$SavedState;

    invoke-virtual {p1}, Landroid/view/View$BaseSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/Spinner;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-boolean p1, p1, Lmiuix/appcompat/widget/Spinner$SavedState;->a:Z

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/widget/Spinner;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    if-eqz p1, :cond_0

    new-instance v0, Lmiuix/appcompat/widget/c;

    invoke-direct {v0, p0}, Lmiuix/appcompat/widget/c;-><init>(Lmiuix/appcompat/widget/Spinner;)V

    invoke-virtual {p1, v0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    new-instance v0, Lmiuix/appcompat/widget/Spinner$SavedState;

    invoke-super {p0}, Landroid/widget/Spinner;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lmiuix/appcompat/widget/Spinner$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget-object v1, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lmiuix/appcompat/widget/Spinner$h;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, v0, Lmiuix/appcompat/widget/Spinner$SavedState;->a:Z

    return-object v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmiuix/appcompat/widget/Spinner;->d()V

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Landroid/widget/Spinner;->isPressed()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/widget/Spinner;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    new-array v0, v1, [Landroid/view/View;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {v0}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object v0

    new-array v1, v1, [Lmiuix/animation/a/a;

    invoke-interface {v0, v1}, Lmiuix/animation/m;->g([Lmiuix/animation/a/a;)V

    :cond_1
    invoke-super {p0, p1}, Landroid/widget/Spinner;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public performClick()Z
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [I

    invoke-virtual {p0, v0}, Landroid/widget/Spinner;->getLocationInWindow([I)V

    const/4 v1, 0x0

    aget v1, v0, v1

    int-to-float v1, v1

    const/4 v2, 0x1

    aget v0, v0, v2

    int-to-float v0, v0

    invoke-virtual {p0, v1, v0}, Lmiuix/appcompat/widget/Spinner;->a(FF)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    check-cast p1, Landroid/widget/SpinnerAdapter;

    invoke-virtual {p0, p1}, Lmiuix/appcompat/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/SpinnerAdapter;)V
    .locals 3

    iget-boolean v0, p0, Lmiuix/appcompat/widget/Spinner;->d:Z

    if-nez v0, :cond_0

    iput-object p1, p0, Lmiuix/appcompat/widget/Spinner;->c:Landroid/widget/SpinnerAdapter;

    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    instance-of v1, v0, Lmiuix/appcompat/widget/Spinner$a;

    if-eqz v1, :cond_1

    new-instance v1, Lmiuix/appcompat/widget/Spinner$b;

    invoke-virtual {p0}, Lmiuix/appcompat/widget/Spinner;->getPopupContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Lmiuix/appcompat/widget/Spinner$b;-><init>(Landroid/widget/SpinnerAdapter;Landroid/content/res/Resources$Theme;)V

    invoke-interface {v0, v1}, Lmiuix/appcompat/widget/Spinner$h;->a(Landroid/widget/ListAdapter;)V

    goto :goto_0

    :cond_1
    instance-of v1, v0, Lmiuix/appcompat/widget/Spinner$e;

    if-eqz v1, :cond_2

    new-instance v1, Lmiuix/appcompat/widget/Spinner$d;

    invoke-virtual {p0}, Lmiuix/appcompat/widget/Spinner;->getPopupContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Lmiuix/appcompat/widget/Spinner$d;-><init>(Landroid/widget/SpinnerAdapter;Landroid/content/res/Resources$Theme;)V

    invoke-interface {v0, v1}, Lmiuix/appcompat/widget/Spinner$h;->a(Landroid/widget/ListAdapter;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public setDoubleLineContentAdapter(Ld/b/a/b;)V
    .locals 4

    new-instance v0, Ld/b/b/a/b;

    invoke-virtual {p0}, Landroid/widget/Spinner;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Ld/b/i;->miuix_appcompat_simple_spinner_layout:I

    new-instance v3, Lmiuix/appcompat/widget/Spinner$g;

    invoke-direct {v3, p0}, Lmiuix/appcompat/widget/Spinner$g;-><init>(Lmiuix/appcompat/widget/Spinner;)V

    invoke-direct {v0, v1, v2, p1, v3}, Ld/b/b/a/b;-><init>(Landroid/content/Context;ILandroid/widget/ArrayAdapter;Ld/b/b/a/b$a;)V

    invoke-virtual {p0, v0}, Lmiuix/appcompat/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    return-void
.end method

.method public setDropDownHorizontalOffset(I)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lmiuix/appcompat/widget/Spinner$h;->c(I)V

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    invoke-interface {v0, p1}, Lmiuix/appcompat/widget/Spinner$h;->a(I)V

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setDropDownHorizontalOffset(I)V

    :goto_0
    return-void
.end method

.method public setDropDownVerticalOffset(I)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lmiuix/appcompat/widget/Spinner$h;->b(I)V

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setDropDownVerticalOffset(I)V

    :goto_0
    return-void
.end method

.method public setDropDownWidth(I)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    if-eqz v0, :cond_0

    iput p1, p0, Lmiuix/appcompat/widget/Spinner;->f:I

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setDropDownWidth(I)V

    :goto_0
    return-void
.end method

.method public setFenceView(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    instance-of v1, v0, Lmiuix/appcompat/widget/Spinner$e;

    if-eqz v1, :cond_0

    check-cast v0, Lmiuix/appcompat/widget/Spinner$e;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/widget/Spinner$e;->e(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public setFenceX(I)V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    instance-of v1, v0, Lmiuix/appcompat/widget/Spinner$e;

    if-eqz v1, :cond_0

    check-cast v0, Lmiuix/appcompat/widget/Spinner$e;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/widget/Spinner$e;->g(I)V

    :cond_0
    return-void
.end method

.method public setFenceXFromView(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [I

    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    const/4 p1, 0x0

    aget p1, v0, p1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/widget/Spinner;->setFenceX(I)V

    return-void
.end method

.method public setOnSpinnerDismissListener(Lmiuix/appcompat/widget/Spinner$f;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/widget/Spinner;->l:Lmiuix/appcompat/widget/Spinner$f;

    return-void
.end method

.method public setPopupBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lmiuix/appcompat/widget/Spinner$h;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setPopupBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void
.end method

.method public setPopupBackgroundResource(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param

    invoke-virtual {p0}, Lmiuix/appcompat/widget/Spinner;->getPopupContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, La/a/a/a/a;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/widget/Spinner;->setPopupBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setPrompt(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner;->e:Lmiuix/appcompat/widget/Spinner$h;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lmiuix/appcompat/widget/Spinner$h;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method
