.class public Lmiuix/appcompat/widget/b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/widget/b$a;
    }
.end annotation


# static fields
.field private static a:Lmiuix/appcompat/widget/a/b;


# direct methods
.method public static a()V
    .locals 1

    sget-object v0, Lmiuix/appcompat/widget/b;->a:Lmiuix/appcompat/widget/a/b;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmiuix/appcompat/widget/a/b;->a()V

    :cond_0
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/view/View;Lmiuix/appcompat/widget/b$a;)V
    .locals 1

    sget-object v0, Lmiuix/appcompat/widget/b;->a:Lmiuix/appcompat/widget/a/b;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ld/h/a/e;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ld/h/a/e;->b()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lmiuix/appcompat/widget/a/c;

    invoke-direct {v0}, Lmiuix/appcompat/widget/a/c;-><init>()V

    sput-object v0, Lmiuix/appcompat/widget/b;->a:Lmiuix/appcompat/widget/a/b;

    goto :goto_0

    :cond_0
    new-instance v0, Lmiuix/appcompat/widget/a/g;

    invoke-direct {v0}, Lmiuix/appcompat/widget/a/g;-><init>()V

    sput-object v0, Lmiuix/appcompat/widget/b;->a:Lmiuix/appcompat/widget/a/b;

    :cond_1
    :goto_0
    sget-object v0, Lmiuix/appcompat/widget/b;->a:Lmiuix/appcompat/widget/a/b;

    invoke-interface {v0, p0, p1, p2}, Lmiuix/appcompat/widget/a/b;->a(Landroid/view/View;Landroid/view/View;Lmiuix/appcompat/widget/b$a;)V

    const/4 p0, 0x0

    sput-object p0, Lmiuix/appcompat/widget/b;->a:Lmiuix/appcompat/widget/a/b;

    return-void
.end method

.method public static a(Landroid/view/View;Landroid/view/View;ZLmiuix/appcompat/app/j$c;)V
    .locals 1

    sget-object v0, Lmiuix/appcompat/widget/b;->a:Lmiuix/appcompat/widget/a/b;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ld/h/a/e;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ld/h/a/e;->b()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lmiuix/appcompat/widget/a/c;

    invoke-direct {v0}, Lmiuix/appcompat/widget/a/c;-><init>()V

    sput-object v0, Lmiuix/appcompat/widget/b;->a:Lmiuix/appcompat/widget/a/b;

    goto :goto_0

    :cond_0
    new-instance v0, Lmiuix/appcompat/widget/a/g;

    invoke-direct {v0}, Lmiuix/appcompat/widget/a/g;-><init>()V

    sput-object v0, Lmiuix/appcompat/widget/b;->a:Lmiuix/appcompat/widget/a/b;

    :cond_1
    :goto_0
    sget-object v0, Lmiuix/appcompat/widget/b;->a:Lmiuix/appcompat/widget/a/b;

    invoke-interface {v0, p0, p1, p2, p3}, Lmiuix/appcompat/widget/a/b;->a(Landroid/view/View;Landroid/view/View;ZLmiuix/appcompat/app/j$c;)V

    return-void
.end method
