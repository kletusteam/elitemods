.class public Lmiuix/appcompat/app/o;
.super Lmiuix/appcompat/app/f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/app/o$a;
    }
.end annotation


# instance fields
.field private r:Landroidx/fragment/app/Fragment;

.field private s:Landroid/view/View;

.field private t:I

.field private u:Landroid/content/Context;

.field private v:Lmiuix/appcompat/internal/view/menu/i;

.field private w:B

.field private x:I

.field private y:Ljava/lang/Runnable;

.field private final z:Landroid/view/Window$Callback;


# direct methods
.method public constructor <init>(Landroidx/fragment/app/Fragment;)V
    .locals 1

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/app/AppCompatActivity;

    invoke-direct {p0, v0}, Lmiuix/appcompat/app/f;-><init>(Lmiuix/appcompat/app/AppCompatActivity;)V

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/appcompat/app/o;->x:I

    new-instance v0, Lmiuix/appcompat/app/n;

    invoke-direct {v0, p0}, Lmiuix/appcompat/app/n;-><init>(Lmiuix/appcompat/app/o;)V

    iput-object v0, p0, Lmiuix/appcompat/app/o;->z:Landroid/view/Window$Callback;

    iput-object p1, p0, Lmiuix/appcompat/app/o;->r:Landroidx/fragment/app/Fragment;

    return-void
.end method

.method static synthetic a(Lmiuix/appcompat/app/o;I)B
    .locals 1

    iget-byte v0, p0, Lmiuix/appcompat/app/o;->w:B

    and-int/2addr p1, v0

    int-to-byte p1, p1

    iput-byte p1, p0, Lmiuix/appcompat/app/o;->w:B

    return p1
.end method

.method static synthetic a(Lmiuix/appcompat/app/o;)Landroidx/fragment/app/Fragment;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/app/o;->r:Landroidx/fragment/app/Fragment;

    return-object p0
.end method

.method static synthetic a(Lmiuix/appcompat/app/o;Lmiuix/appcompat/internal/view/menu/i;)Lmiuix/appcompat/internal/view/menu/i;
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/app/o;->v:Lmiuix/appcompat/internal/view/menu/i;

    return-object p1
.end method

.method static synthetic b(Lmiuix/appcompat/app/o;)B
    .locals 0

    iget-byte p0, p0, Lmiuix/appcompat/app/o;->w:B

    return p0
.end method

.method static synthetic c(Lmiuix/appcompat/app/o;)Lmiuix/appcompat/internal/view/menu/i;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/app/o;->v:Lmiuix/appcompat/internal/view/menu/i;

    return-object p0
.end method

.method private q()Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/o;->y:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    new-instance v0, Lmiuix/appcompat/app/o$a;

    invoke-direct {v0, p0}, Lmiuix/appcompat/app/o$a;-><init>(Lmiuix/appcompat/app/o;)V

    iput-object v0, p0, Lmiuix/appcompat/app/o;->y:Ljava/lang/Runnable;

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/app/o;->y:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 1

    invoke-virtual {p0}, Lmiuix/appcompat/app/f;->d()Lmiuix/appcompat/app/d;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/f;->d()Lmiuix/appcompat/app/d;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/u;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/u;->b(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    invoke-virtual {p0}, Lmiuix/appcompat/app/o;->h()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Ld/b/l;->Window:[I

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Ld/b/l;->Window_windowActionBar:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_5

    sget v1, Ld/b/l;->Window_windowActionBar:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lmiuix/appcompat/app/f;->a(I)Z

    :cond_0
    sget v1, Ld/b/l;->Window_windowActionBarOverlay:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x9

    invoke-virtual {p0, v1}, Lmiuix/appcompat/app/f;->a(I)Z

    :cond_1
    sget v1, Ld/b/l;->Window_windowTranslucentStatus:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lmiuix/appcompat/app/f;->b(I)V

    sget v1, Ld/b/l;->Window_immersionMenuEnabled:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    invoke-virtual {p0, v1}, Lmiuix/appcompat/app/f;->b(Z)V

    sget v1, Ld/b/l;->Window_immersionMenuLayout:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lmiuix/appcompat/app/f;->m:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0}, Lmiuix/appcompat/app/o;->h()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iget-boolean v0, p0, Lmiuix/appcompat/app/f;->h:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lmiuix/appcompat/app/o;->h()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p1}, Lmiuix/appcompat/app/o;->a(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V

    iget-object p2, p0, Lmiuix/appcompat/app/o;->s:Landroid/view/View;

    const v0, 0x1020002

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/view/ViewGroup;

    iget-object v0, p0, Lmiuix/appcompat/app/o;->r:Landroidx/fragment/app/Fragment;

    check-cast v0, Lmiuix/appcompat/app/q;

    invoke-interface {v0, p1, p2, p3}, Lmiuix/appcompat/app/q;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p3

    if-eq p3, p2, :cond_4

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p3

    if-eqz p3, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p3

    check-cast p3, Landroid/view/ViewGroup;

    invoke-virtual {p3, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_2
    invoke-virtual {p2}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {p2, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lmiuix/appcompat/app/o;->r:Landroidx/fragment/app/Fragment;

    check-cast v0, Lmiuix/appcompat/app/q;

    invoke-interface {v0, p1, p2, p3}, Lmiuix/appcompat/app/q;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/app/o;->s:Landroid/view/View;

    :cond_4
    :goto_0
    iget-object p1, p0, Lmiuix/appcompat/app/o;->s:Landroid/view/View;

    return-object p1

    :cond_5
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "You need to use a miui theme (or descendant) with this fragment."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a()V
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/app/o;->r:Landroidx/fragment/app/Fragment;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-byte v1, p0, Lmiuix/appcompat/app/o;->w:B

    and-int/lit8 v2, v1, 0x10

    if-nez v2, :cond_0

    or-int/lit8 v1, v1, 0x10

    int-to-byte v1, v1

    iput-byte v1, p0, Lmiuix/appcompat/app/o;->w:B

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0}, Lmiuix/appcompat/app/o;->q()Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method final a(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)V
    .locals 5

    goto/32 :goto_5b

    nop

    :goto_0
    sget p3, Ld/b/g;->action_bar:I

    goto/32 :goto_a

    nop

    :goto_1
    invoke-virtual {v0}, Lmiuix/appcompat/app/AppCompatActivity;->isInFloatingWindowMode()Z

    move-result p3

    goto/32 :goto_51

    nop

    :goto_2
    iget-object p3, p0, Lmiuix/appcompat/app/f;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_37

    nop

    :goto_3
    invoke-virtual {p3, v4, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto/32 :goto_5d

    nop

    :goto_4
    if-eqz v0, :cond_0

    goto/32 :goto_4f

    :cond_0
    goto/32 :goto_41

    nop

    :goto_5
    check-cast p1, Landroid/view/ViewGroup;

    goto/32 :goto_50

    nop

    :goto_6
    if-nez v1, :cond_1

    goto/32 :goto_3b

    :cond_1
    goto/32 :goto_1b

    nop

    :goto_7
    iget v0, p0, Lmiuix/appcompat/app/f;->m:I

    goto/32 :goto_28

    nop

    :goto_8
    invoke-virtual {p3, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setWindowCallback(Landroid/view/Window$Callback;)V

    goto/32 :goto_1f

    nop

    :goto_9
    iget-boolean p3, p0, Lmiuix/appcompat/app/f;->i:Z

    goto/32 :goto_17

    nop

    :goto_a
    invoke-virtual {p2, p3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p3

    goto/32 :goto_10

    nop

    :goto_b
    const/4 v3, 0x1

    goto/32 :goto_34

    nop

    :goto_c
    if-nez p1, :cond_2

    goto/32 :goto_24

    :cond_2
    goto/32 :goto_23

    nop

    :goto_d
    if-nez p1, :cond_3

    goto/32 :goto_2e

    :cond_3
    goto/32 :goto_f

    nop

    :goto_e
    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    goto/32 :goto_3c

    nop

    :goto_f
    iget-object p1, p0, Lmiuix/appcompat/app/o;->s:Landroid/view/View;

    goto/32 :goto_4c

    nop

    :goto_10
    check-cast p3, Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_58

    nop

    :goto_11
    invoke-virtual {p0}, Lmiuix/appcompat/app/f;->j()Ljava/lang/String;

    move-result-object p3

    goto/32 :goto_4d

    nop

    :goto_12
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    goto/32 :goto_4a

    nop

    :goto_13
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    goto/32 :goto_46

    nop

    :goto_14
    iget p3, p0, Lmiuix/appcompat/app/o;->t:I

    goto/32 :goto_1a

    nop

    :goto_15
    check-cast v0, Lmiuix/appcompat/app/AppCompatActivity;

    goto/32 :goto_1

    nop

    :goto_16
    invoke-virtual {p2, v2}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setRootSubDecor(Z)V

    goto/32 :goto_9

    nop

    :goto_17
    invoke-virtual {p2, p3}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setOverlayMode(Z)V

    goto/32 :goto_39

    nop

    :goto_18
    invoke-virtual {v3}, Lmiuix/appcompat/app/AppCompatActivity;->getExtraHorizontalPaddingLevel()I

    move-result v4

    goto/32 :goto_59

    nop

    :goto_19
    if-eqz p2, :cond_4

    goto/32 :goto_2e

    :cond_4
    goto/32 :goto_31

    nop

    :goto_1a
    if-nez p3, :cond_5

    goto/32 :goto_55

    :cond_5
    goto/32 :goto_56

    nop

    :goto_1b
    move-object v3, v0

    goto/32 :goto_43

    nop

    :goto_1c
    invoke-static {p1, p3}, Ld/h/a/d;->d(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p3

    goto/32 :goto_54

    nop

    :goto_1d
    iget-object p3, p0, Lmiuix/appcompat/app/f;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_7

    nop

    :goto_1e
    iget-object v0, p0, Lmiuix/appcompat/app/o;->z:Landroid/view/Window$Callback;

    goto/32 :goto_8

    nop

    :goto_1f
    iget-boolean p3, p0, Lmiuix/appcompat/app/f;->g:Z

    goto/32 :goto_20

    nop

    :goto_20
    if-nez p3, :cond_6

    goto/32 :goto_38

    :cond_6
    goto/32 :goto_2

    nop

    :goto_21
    const/4 v2, 0x0

    goto/32 :goto_6

    nop

    :goto_22
    return-void

    :goto_23
    invoke-virtual {p0, p1, p3, p2}, Lmiuix/appcompat/app/f;->a(ZZLmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;)V

    :goto_24
    goto/32 :goto_27

    nop

    :goto_25
    sget-object v0, Ld/b/l;->Window:[I

    goto/32 :goto_3e

    nop

    :goto_26
    invoke-virtual {p2, p3}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setCallback(Landroid/view/Window$Callback;)V

    goto/32 :goto_16

    nop

    :goto_27
    invoke-virtual {p0, v3}, Lmiuix/appcompat/app/o;->d(I)V

    goto/32 :goto_5c

    nop

    :goto_28
    invoke-virtual {p3, v0, p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->a(ILmiuix/appcompat/app/f;)V

    :goto_29
    goto/32 :goto_11

    nop

    :goto_2a
    iget-object p1, p0, Lmiuix/appcompat/app/o;->s:Landroid/view/View;

    goto/32 :goto_13

    nop

    :goto_2b
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result p1

    goto/32 :goto_44

    nop

    :goto_2c
    instance-of v1, v0, Lmiuix/appcompat/app/AppCompatActivity;

    goto/32 :goto_21

    nop

    :goto_2d
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->endViewTransition(Landroid/view/View;)V

    :goto_2e
    goto/32 :goto_22

    nop

    :goto_2f
    move p1, v0

    :goto_30
    goto/32 :goto_c

    nop

    :goto_31
    iget-object p2, p0, Lmiuix/appcompat/app/o;->s:Landroid/view/View;

    goto/32 :goto_2d

    nop

    :goto_32
    if-nez p3, :cond_7

    goto/32 :goto_29

    :cond_7
    goto/32 :goto_1d

    nop

    :goto_33
    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    goto/32 :goto_49

    nop

    :goto_34
    iput-boolean v3, p0, Lmiuix/appcompat/app/f;->e:Z

    goto/32 :goto_40

    nop

    :goto_35
    invoke-virtual {p0}, Lmiuix/appcompat/app/f;->k()Z

    move-result p3

    goto/32 :goto_32

    nop

    :goto_36
    sget v0, Ld/b/c;->abc_split_action_bar_is_narrow:I

    goto/32 :goto_2b

    nop

    :goto_37
    invoke-virtual {p3}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->p()V

    :goto_38
    goto/32 :goto_35

    nop

    :goto_39
    invoke-virtual {p0}, Lmiuix/appcompat/app/f;->i()I

    move-result p3

    goto/32 :goto_3f

    nop

    :goto_3a
    invoke-virtual {v3, v2}, Lmiuix/appcompat/app/AppCompatActivity;->setExtraHorizontalPaddingEnable(Z)V

    :goto_3b
    goto/32 :goto_b

    nop

    :goto_3c
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    goto/32 :goto_2f

    nop

    :goto_3d
    iget-object p1, p0, Lmiuix/appcompat/app/o;->s:Landroid/view/View;

    goto/32 :goto_12

    nop

    :goto_3e
    invoke-virtual {p1, v0}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object p1

    goto/32 :goto_4b

    nop

    :goto_3f
    invoke-virtual {p2, p3}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setTranslucentStatus(I)V

    goto/32 :goto_14

    nop

    :goto_40
    sget v4, Ld/b/i;->miuix_appcompat_screen_action_bar:I

    goto/32 :goto_3

    nop

    :goto_41
    iget-object v0, p0, Lmiuix/appcompat/app/o;->r:Landroidx/fragment/app/Fragment;

    goto/32 :goto_57

    nop

    :goto_42
    iget-object p3, p0, Lmiuix/appcompat/app/o;->z:Landroid/view/Window$Callback;

    goto/32 :goto_26

    nop

    :goto_43
    check-cast v3, Lmiuix/appcompat/app/AppCompatActivity;

    goto/32 :goto_18

    nop

    :goto_44
    goto :goto_30

    :goto_45
    goto/32 :goto_25

    nop

    :goto_46
    if-nez p1, :cond_8

    goto/32 :goto_2e

    :cond_8
    goto/32 :goto_3d

    nop

    :goto_47
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    goto/32 :goto_36

    nop

    :goto_48
    if-nez v1, :cond_9

    goto/32 :goto_52

    :cond_9
    goto/32 :goto_15

    nop

    :goto_49
    if-nez p3, :cond_a

    goto/32 :goto_45

    :cond_a
    goto/32 :goto_47

    nop

    :goto_4a
    instance-of p1, p1, Landroid/view/ViewGroup;

    goto/32 :goto_d

    nop

    :goto_4b
    sget v0, Ld/b/l;->Window_windowSplitActionBar:I

    goto/32 :goto_e

    nop

    :goto_4c
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    goto/32 :goto_5

    nop

    :goto_4d
    const-string v0, "splitActionBarWhenNarrow"

    goto/32 :goto_33

    nop

    :goto_4e
    goto/16 :goto_2e

    :goto_4f
    goto/32 :goto_2a

    nop

    :goto_50
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p2

    goto/32 :goto_19

    nop

    :goto_51
    invoke-virtual {p2, p3}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->a(Z)V

    :goto_52
    goto/32 :goto_0

    nop

    :goto_53
    iget-object p3, p0, Lmiuix/appcompat/app/f;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_1e

    nop

    :goto_54
    invoke-virtual {p2, p3}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_55
    goto/32 :goto_48

    nop

    :goto_56
    const p3, 0x1010054

    goto/32 :goto_1c

    nop

    :goto_57
    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    goto/32 :goto_2c

    nop

    :goto_58
    iput-object p3, p0, Lmiuix/appcompat/app/f;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_53

    nop

    :goto_59
    invoke-virtual {p0, v4}, Lmiuix/appcompat/app/o;->c(I)V

    goto/32 :goto_3a

    nop

    :goto_5a
    iput-object p2, p0, Lmiuix/appcompat/app/o;->s:Landroid/view/View;

    goto/32 :goto_4e

    nop

    :goto_5b
    iget-boolean v0, p0, Lmiuix/appcompat/app/f;->e:Z

    goto/32 :goto_4

    nop

    :goto_5c
    invoke-virtual {p0}, Lmiuix/appcompat/app/o;->a()V

    goto/32 :goto_5a

    nop

    :goto_5d
    check-cast p2, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    goto/32 :goto_42

    nop
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Lmiuix/appcompat/app/f;->a(Landroid/content/res/Configuration;)V

    iget-object p1, p0, Lmiuix/appcompat/app/o;->s:Landroid/view/View;

    if-eqz p1, :cond_0

    instance-of p1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/app/o;->r:Landroidx/fragment/app/Fragment;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    if-eqz p1, :cond_0

    instance-of v0, p1, Lmiuix/appcompat/app/AppCompatActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/o;->s:Landroid/view/View;

    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    check-cast p1, Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {p1}, Lmiuix/appcompat/app/AppCompatActivity;->isInFloatingWindowMode()Z

    move-result p1

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->a(Z)V

    :cond_0
    return-void
.end method

.method public a(Landroid/view/ActionMode;)V
    .locals 0

    const/4 p1, 0x0

    iput-object p1, p0, Lmiuix/appcompat/app/f;->d:Landroid/view/ActionMode;

    return-void
.end method

.method public a(ILandroid/view/Menu;)Z
    .locals 1

    if-nez p1, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/o;->r:Landroidx/fragment/app/Fragment;

    check-cast v0, Lmiuix/appcompat/app/q;

    invoke-interface {v0, p1, p2}, Lmiuix/appcompat/app/q;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public a(ILandroid/view/MenuItem;)Z
    .locals 1

    if-nez p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/app/o;->r:Landroidx/fragment/app/Fragment;

    invoke-virtual {p1, p2}, Landroidx/fragment/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1

    :cond_0
    const/4 v0, 0x6

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Lmiuix/appcompat/app/o;->r:Landroidx/fragment/app/Fragment;

    invoke-virtual {p1, p2}, Landroidx/fragment/app/Fragment;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public a(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    if-nez p1, :cond_0

    iget-object p2, p0, Lmiuix/appcompat/app/o;->r:Landroidx/fragment/app/Fragment;

    check-cast p2, Lmiuix/appcompat/app/q;

    const/4 v0, 0x0

    invoke-interface {p2, p1, v0, p3}, Lmiuix/appcompat/app/q;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public a(Lmiuix/appcompat/internal/view/menu/i;Landroid/view/MenuItem;)Z
    .locals 0

    const/4 p1, 0x0

    invoke-virtual {p0, p1, p2}, Lmiuix/appcompat/app/o;->a(ILandroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public b()Lmiuix/appcompat/app/d;
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/app/o;->r:Landroidx/fragment/app/Fragment;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lmiuix/appcompat/internal/app/widget/u;

    iget-object v1, p0, Lmiuix/appcompat/app/o;->r:Landroidx/fragment/app/Fragment;

    invoke-direct {v0, v1}, Lmiuix/appcompat/internal/app/widget/u;-><init>(Landroidx/fragment/app/Fragment;)V

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Landroid/view/ActionMode;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/app/f;->d:Landroid/view/ActionMode;

    return-void
.end method

.method public c(I)V
    .locals 1

    invoke-static {p1}, Ld/b/b/b/f;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lmiuix/appcompat/app/o;->x:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lmiuix/appcompat/app/o;->x:I

    iget-object p1, p0, Lmiuix/appcompat/app/o;->s:Landroid/view/View;

    instance-of v0, p1, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    check-cast p1, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iget v0, p0, Lmiuix/appcompat/app/o;->x:I

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setExtraHorizontalPaddingLevel(I)V

    :cond_0
    return-void
.end method

.method protected c(Lmiuix/appcompat/internal/view/menu/i;)Z
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/app/o;->r:Landroidx/fragment/app/Fragment;

    instance-of v1, v0, Lmiuix/appcompat/app/q;

    if-eqz v1, :cond_0

    check-cast v0, Lmiuix/appcompat/app/q;

    invoke-interface {v0, p1}, Lmiuix/appcompat/app/q;->a(Landroid/view/Menu;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public d(I)V
    .locals 1

    iget-byte v0, p0, Lmiuix/appcompat/app/o;->w:B

    and-int/lit8 p1, p1, 0x1

    or-int/2addr p1, v0

    int-to-byte p1, p1

    iput-byte p1, p0, Lmiuix/appcompat/app/o;->w:B

    return-void
.end method

.method protected d(Lmiuix/appcompat/internal/view/menu/i;)Z
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/app/o;->r:Landroidx/fragment/app/Fragment;

    instance-of v1, v0, Lmiuix/appcompat/app/q;

    if-eqz v1, :cond_0

    invoke-virtual {v0, p1}, Landroidx/fragment/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public h()Landroid/content/Context;
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/app/o;->u:Landroid/content/Context;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    iput-object v0, p0, Lmiuix/appcompat/app/o;->u:Landroid/content/Context;

    iget v0, p0, Lmiuix/appcompat/app/o;->t:I

    if-eqz v0, :cond_0

    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Lmiuix/appcompat/app/o;->u:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lmiuix/appcompat/app/o;->u:Landroid/content/Context;

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/app/o;->u:Landroid/content/Context;

    return-object v0
.end method

.method public o()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/o;->s:Landroid/view/View;

    return-object v0
.end method

.method p()V
    .locals 2

    goto/32 :goto_7

    nop

    :goto_0
    iput-object v0, p0, Lmiuix/appcompat/app/f;->j:Lmiuix/appcompat/app/d;

    goto/32 :goto_1

    nop

    :goto_1
    iput-object v0, p0, Lmiuix/appcompat/app/f;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_6

    nop

    :goto_2
    const/4 v1, 0x0

    goto/32 :goto_4

    nop

    :goto_3
    iput-object v0, p0, Lmiuix/appcompat/app/o;->s:Landroid/view/View;

    goto/32 :goto_2

    nop

    :goto_4
    iput-boolean v1, p0, Lmiuix/appcompat/app/f;->e:Z

    goto/32 :goto_0

    nop

    :goto_5
    return-void

    :goto_6
    iput-object v0, p0, Lmiuix/appcompat/app/o;->y:Ljava/lang/Runnable;

    goto/32 :goto_5

    nop

    :goto_7
    const/4 v0, 0x0

    goto/32 :goto_3

    nop
.end method
