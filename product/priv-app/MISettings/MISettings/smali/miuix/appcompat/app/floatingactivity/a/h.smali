.class public Lmiuix/appcompat/app/floatingactivity/a/h;
.super Lmiuix/appcompat/app/floatingactivity/a/d;


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/d;-><init>()V

    const v0, 0x1010054

    invoke-static {p1, v0}, Ld/h/a/d;->d(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/h;->b:Landroid/graphics/drawable/Drawable;

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Z)V
    .locals 1

    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/h;->a:Landroid/view/View;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Ld/h/a/i;->a(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/h;->a:Landroid/view/View;

    new-instance p2, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v0, -0x1000000

    invoke-direct {p2, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/h;->a:Landroid/view/View;

    iget-object p2, p0, Lmiuix/appcompat/app/floatingactivity/a/h;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, p2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public a(Lmiuix/appcompat/app/floatingactivity/j;)V
    .locals 0

    return-void
.end method

.method public a(Lmiuix/appcompat/app/floatingactivity/k;)V
    .locals 0

    return-void
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method public a()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/view/View;Z)Landroid/view/ViewGroup;
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/h;->a:Landroid/view/View;

    check-cast p1, Landroid/view/ViewGroup;

    return-object p1
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method public b(Z)V
    .locals 0

    return-void
.end method

.method public c()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/h;->a:Landroid/view/View;

    return-object v0
.end method

.method public c(Z)V
    .locals 0

    return-void
.end method

.method public d()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/h;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public e()V
    .locals 0

    return-void
.end method

.method public executeCloseEnterAnimation()V
    .locals 0

    return-void
.end method

.method public executeCloseExitAnimation()V
    .locals 0

    return-void
.end method

.method public executeOpenEnterAnimation()V
    .locals 0

    return-void
.end method

.method public executeOpenExitAnimation()V
    .locals 0

    return-void
.end method

.method public f()V
    .locals 0

    return-void
.end method

.method public g()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public i()V
    .locals 0

    return-void
.end method
