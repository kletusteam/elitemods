.class public Lmiuix/appcompat/app/floatingactivity/a/f;
.super Lmiuix/appcompat/app/floatingactivity/a/k;


# direct methods
.method public constructor <init>(Lmiuix/appcompat/app/AppCompatActivity;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/appcompat/app/floatingactivity/a/k;-><init>(Lmiuix/appcompat/app/AppCompatActivity;)V

    return-void
.end method


# virtual methods
.method public g()Z
    .locals 4

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v3, 0x258

    if-lt v0, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    iget-object v3, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-static {v3}, Lmiuix/core/util/screenutils/a;->a(Landroid/content/Context;)I

    move-result v3

    if-eqz v0, :cond_1

    const/16 v0, 0x2000

    if-eq v3, v0, :cond_2

    const/16 v0, 0x2003

    if-ne v3, v0, :cond_1

    goto :goto_1

    :cond_1
    move v1, v2

    :cond_2
    :goto_1
    return v1
.end method

.method public j()V
    .locals 1

    invoke-virtual {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-static {v0}, Lmiuix/appcompat/app/floatingactivity/c;->a(Lmiuix/appcompat/app/AppCompatActivity;)V

    return-void

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-static {v0}, Lmiuix/appcompat/app/floatingactivity/c;->e(Lmiuix/appcompat/app/AppCompatActivity;)I

    move-result v0

    if-ltz v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-static {v0}, Lmiuix/appcompat/app/floatingactivity/c;->c(Lmiuix/appcompat/app/AppCompatActivity;)V

    :cond_1
    return-void
.end method
