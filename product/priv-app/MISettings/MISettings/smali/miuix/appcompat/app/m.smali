.class Lmiuix/appcompat/app/m;
.super Lmiuix/appcompat/app/f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/app/m$a;
    }
.end annotation


# instance fields
.field private A:Lmiuix/appcompat/app/floatingactivity/a/d;

.field private B:Landroid/view/ViewGroup;

.field private final C:Ljava/lang/String;

.field private D:Z

.field private E:Ljava/lang/CharSequence;

.field F:Landroid/view/Window;

.field private G:Lmiuix/appcompat/app/m$a;

.field private final H:Ljava/lang/Runnable;

.field private r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

.field private s:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

.field private t:Landroid/view/ViewGroup;

.field private u:Landroid/view/LayoutInflater;

.field private v:Lmiuix/appcompat/app/h;

.field private w:Lmiuix/appcompat/app/floatingactivity/l;

.field private x:Z

.field private y:Z

.field private z:I


# direct methods
.method constructor <init>(Lmiuix/appcompat/app/AppCompatActivity;Lmiuix/appcompat/app/h;Lmiuix/appcompat/app/floatingactivity/l;)V
    .locals 2

    invoke-direct {p0, p1}, Lmiuix/appcompat/app/f;-><init>(Lmiuix/appcompat/app/AppCompatActivity;)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lmiuix/appcompat/app/m;->x:Z

    iput-boolean p1, p0, Lmiuix/appcompat/app/m;->y:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/appcompat/app/m;->B:Landroid/view/ViewGroup;

    iput-boolean p1, p0, Lmiuix/appcompat/app/m;->D:Z

    new-instance p1, Lmiuix/appcompat/app/l;

    invoke-direct {p1, p0}, Lmiuix/appcompat/app/l;-><init>(Lmiuix/appcompat/app/m;)V

    iput-object p1, p0, Lmiuix/appcompat/app/m;->H:Ljava/lang/Runnable;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/app/m;->C:Ljava/lang/String;

    iput-object p2, p0, Lmiuix/appcompat/app/m;->v:Lmiuix/appcompat/app/h;

    iput-object p3, p0, Lmiuix/appcompat/app/m;->w:Lmiuix/appcompat/app/floatingactivity/l;

    return-void
.end method

.method private F()V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/app/m;->F:Landroid/view/Window;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-direct {p0, v0}, Lmiuix/appcompat/app/m;->a(Landroid/view/Window;)V

    :cond_1
    iget-object v0, p0, Lmiuix/appcompat/app/m;->F:Landroid/view/Window;

    if-eqz v0, :cond_2

    return-void

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "We have not been given a Window"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private G()V
    .locals 6

    iget-boolean v0, p0, Lmiuix/appcompat/app/f;->e:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-direct {p0}, Lmiuix/appcompat/app/m;->F()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/app/f;->e:Z

    iget-object v1, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    iput-object v2, p0, Lmiuix/appcompat/app/m;->u:Landroid/view/LayoutInflater;

    iget-object v2, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    sget-object v3, Ld/b/l;->Window:[I

    invoke-virtual {v2, v3}, Landroid/app/Activity;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v2

    sget v3, Ld/b/l;->Window_windowLayoutMode:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    if-ne v3, v0, :cond_1

    iget-object v3, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {v3}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v5, 0x50

    invoke-virtual {v3, v5}, Landroid/view/Window;->setGravity(I)V

    :cond_1
    sget v3, Ld/b/l;->Window_windowActionBar:I

    invoke-virtual {v2, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    if-eqz v3, :cond_c

    sget v3, Ld/b/l;->Window_windowActionBar:I

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v3, 0x8

    invoke-virtual {p0, v3}, Lmiuix/appcompat/app/f;->a(I)Z

    :cond_2
    sget v3, Ld/b/l;->Window_windowActionBarOverlay:I

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    if-eqz v3, :cond_3

    const/16 v3, 0x9

    invoke-virtual {p0, v3}, Lmiuix/appcompat/app/f;->a(I)Z

    :cond_3
    sget v3, Ld/b/l;->Window_isMiuixFloatingTheme:I

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, p0, Lmiuix/appcompat/app/m;->x:Z

    sget v3, Ld/b/l;->Window_windowFloating:I

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, p0, Lmiuix/appcompat/app/m;->y:Z

    sget v3, Ld/b/l;->Window_windowTranslucentStatus:I

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    invoke-virtual {p0, v3}, Lmiuix/appcompat/app/f;->b(I)V

    iget-object v3, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->uiMode:I

    iput v3, p0, Lmiuix/appcompat/app/m;->z:I

    invoke-direct {p0, v1}, Lmiuix/appcompat/app/m;->c(Landroid/view/Window;)V

    iget-object v1, p0, Lmiuix/appcompat/app/m;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz v1, :cond_4

    iget-object v3, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {v1, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setCallback(Landroid/view/Window$Callback;)V

    iget-object v1, p0, Lmiuix/appcompat/app/m;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {p0}, Lmiuix/appcompat/app/f;->i()I

    move-result v3

    invoke-virtual {v1, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setTranslucentStatus(I)V

    :cond_4
    iget-boolean v1, p0, Lmiuix/appcompat/app/f;->h:Z

    if-eqz v1, :cond_a

    iget-object v1, p0, Lmiuix/appcompat/app/m;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz v1, :cond_a

    sget v3, Ld/b/g;->action_bar_container:I

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    iput-object v1, p0, Lmiuix/appcompat/app/m;->s:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    iget-object v1, p0, Lmiuix/appcompat/app/m;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iget-boolean v3, p0, Lmiuix/appcompat/app/f;->i:Z

    invoke-virtual {v1, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setOverlayMode(Z)V

    iget-object v1, p0, Lmiuix/appcompat/app/m;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    sget v3, Ld/b/g;->action_bar:I

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iput-object v1, p0, Lmiuix/appcompat/app/f;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object v1, p0, Lmiuix/appcompat/app/f;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object v3, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {v1, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setWindowCallback(Landroid/view/Window$Callback;)V

    iget-boolean v1, p0, Lmiuix/appcompat/app/f;->g:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lmiuix/appcompat/app/f;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->p()V

    :cond_5
    sget v1, Ld/b/l;->Window_immersionMenuLayout:I

    invoke-virtual {v2, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lmiuix/appcompat/app/f;->m:I

    invoke-virtual {p0}, Lmiuix/appcompat/app/f;->k()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lmiuix/appcompat/app/f;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget v3, p0, Lmiuix/appcompat/app/f;->m:I

    invoke-virtual {v1, v3, p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->a(ILmiuix/appcompat/app/f;)V

    :cond_6
    iget-object v1, p0, Lmiuix/appcompat/app/f;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getCustomNavigationView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lmiuix/appcompat/app/f;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getDisplayOptions()I

    move-result v3

    or-int/lit8 v3, v3, 0x10

    invoke-virtual {v1, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setDisplayOptions(I)V

    :cond_7
    invoke-virtual {p0}, Lmiuix/appcompat/app/f;->j()Ljava/lang/String;

    move-result-object v1

    const-string v3, "splitActionBarWhenNarrow"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v3, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v5, Ld/b/c;->abc_split_action_bar_is_narrow:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    goto :goto_0

    :cond_8
    sget v3, Ld/b/l;->Window_windowSplitActionBar:I

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    :goto_0
    if-eqz v3, :cond_9

    iget-object v5, p0, Lmiuix/appcompat/app/m;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {p0, v3, v1, v5}, Lmiuix/appcompat/app/f;->a(ZZLmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;)V

    :cond_9
    iget-object v1, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v3, p0, Lmiuix/appcompat/app/m;->H:Ljava/lang/Runnable;

    invoke-virtual {v1, v3}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    :cond_a
    sget v1, Ld/b/l;->Window_immersionMenuEnabled:I

    invoke-virtual {v2, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/f;->b(Z)V

    :cond_b
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :cond_c
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You need to use a miui theme (or descendant) with this activity."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private H()Z
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/app/f;->f()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v1, "android"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private I()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/app/floatingactivity/a/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method static synthetic a(Lmiuix/appcompat/app/m;)Lmiuix/appcompat/app/h;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/app/m;->v:Lmiuix/appcompat/app/h;

    return-object p0
.end method

.method private a(Landroid/view/Window;)V
    .locals 3
    .param p1    # Landroid/view/Window;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lmiuix/appcompat/app/m;->F:Landroid/view/Window;

    const-string v1, "AppCompat has already installed itself into the Window"

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    instance-of v2, v0, Lmiuix/appcompat/app/m$a;

    if-nez v2, :cond_0

    new-instance v1, Lmiuix/appcompat/app/m$a;

    invoke-direct {v1, p0, v0}, Lmiuix/appcompat/app/m$a;-><init>(Lmiuix/appcompat/app/m;Landroid/view/Window$Callback;)V

    iput-object v1, p0, Lmiuix/appcompat/app/m;->G:Lmiuix/appcompat/app/m$a;

    iget-object v0, p0, Lmiuix/appcompat/app/m;->G:Lmiuix/appcompat/app/m$a;

    invoke-virtual {p1, v0}, Landroid/view/Window;->setCallback(Landroid/view/Window$Callback;)V

    iput-object p1, p0, Lmiuix/appcompat/app/m;->F:Landroid/view/Window;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private a(ZIZZ)V
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/app/m;->x:Z

    if-eqz v0, :cond_5

    if-nez p4, :cond_0

    iget-object p4, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-static {p4}, Ld/h/a/e;->a(Landroid/content/Context;)Z

    move-result p4

    if-nez p4, :cond_0

    goto :goto_1

    :cond_0
    iget-boolean p4, p0, Lmiuix/appcompat/app/m;->y:Z

    if-eq p4, p1, :cond_4

    iget-object p4, p0, Lmiuix/appcompat/app/m;->w:Lmiuix/appcompat/app/floatingactivity/l;

    invoke-interface {p4, p1}, Lmiuix/appcompat/app/floatingactivity/l;->a(Z)Z

    move-result p4

    if-eqz p4, :cond_4

    iput-boolean p1, p0, Lmiuix/appcompat/app/m;->y:Z

    iget-object p2, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    invoke-virtual {p2, p1}, Lmiuix/appcompat/app/floatingactivity/a/d;->c(Z)V

    iget-boolean p2, p0, Lmiuix/appcompat/app/m;->y:Z

    invoke-direct {p0, p2}, Lmiuix/appcompat/app/m;->h(Z)V

    iget-object p2, p0, Lmiuix/appcompat/app/m;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz p2, :cond_2

    iget-object p2, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    invoke-virtual {p2}, Lmiuix/appcompat/app/floatingactivity/a/d;->d()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p2

    if-eqz p1, :cond_1

    const/4 p4, -0x2

    iput p4, p2, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput p4, p2, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_0

    :cond_1
    const/4 p4, -0x1

    iput p4, p2, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput p4, p2, Landroid/view/ViewGroup$LayoutParams;->width:I

    :goto_0
    iget-object p4, p0, Lmiuix/appcompat/app/m;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {p4, p2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p2, p0, Lmiuix/appcompat/app/m;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {p2}, Landroid/widget/FrameLayout;->requestLayout()V

    :cond_2
    iget-object p2, p0, Lmiuix/appcompat/app/m;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz p2, :cond_3

    invoke-virtual {p2, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->a(Z)V

    :cond_3
    if-eqz p3, :cond_5

    invoke-direct {p0, p1}, Lmiuix/appcompat/app/m;->g(Z)V

    goto :goto_1

    :cond_4
    iget p3, p0, Lmiuix/appcompat/app/m;->z:I

    if-eq p2, p3, :cond_5

    iput p2, p0, Lmiuix/appcompat/app/m;->z:I

    iget-object p2, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    invoke-virtual {p2, p1}, Lmiuix/appcompat/app/floatingactivity/a/d;->c(Z)V

    :cond_5
    :goto_1
    return-void
.end method

.method private static a(Landroid/content/Context;)Z
    .locals 2

    sget v0, Ld/b/b;->windowActionBar:I

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Ld/h/a/d;->a(Landroid/content/Context;IZ)Z

    move-result p0

    return p0
.end method

.method private b(Landroid/view/Window;)I
    .locals 5

    invoke-virtual {p1}, Landroid/view/Window;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Ld/b/b;->windowActionBar:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Ld/h/a/d;->a(Landroid/content/Context;IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    sget v1, Ld/b/b;->windowActionBarMovable:I

    invoke-static {v0, v1, v2}, Ld/h/a/d;->a(Landroid/content/Context;IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Ld/b/i;->miuix_appcompat_screen_action_bar_movable:I

    goto :goto_0

    :cond_0
    sget v1, Ld/b/i;->miuix_appcompat_screen_action_bar:I

    goto :goto_0

    :cond_1
    sget v1, Ld/b/i;->miuix_appcompat_screen_simple:I

    :goto_0
    sget v3, Ld/b/b;->startingWindowOverlay:I

    invoke-static {v0, v3}, Ld/h/a/d;->a(Landroid/content/Context;I)I

    move-result v3

    if-lez v3, :cond_2

    invoke-direct {p0}, Lmiuix/appcompat/app/m;->H()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {v0}, Lmiuix/appcompat/app/m;->a(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v1, v3

    :cond_2
    invoke-virtual {p1}, Landroid/view/Window;->isFloating()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p1}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v3

    instance-of v3, v3, Landroid/app/Dialog;

    if-eqz v3, :cond_3

    sget v3, Ld/b/b;->windowTranslucentStatus:I

    invoke-static {v0, v3, v2}, Ld/h/a/d;->b(Landroid/content/Context;II)I

    move-result v0

    invoke-static {p1, v0}, Lmiuix/core/util/b/a;->a(Landroid/view/Window;I)Z

    :cond_3
    return v1
.end method

.method private c(Landroid/view/Window;)V
    .locals 7

    iget-boolean v0, p0, Lmiuix/appcompat/app/m;->x:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-static {v0}, Lmiuix/appcompat/app/floatingactivity/a/e;->a(Lmiuix/appcompat/app/AppCompatActivity;)Lmiuix/appcompat/app/floatingactivity/a/d;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    iput-object v1, p0, Lmiuix/appcompat/app/m;->B:Landroid/view/ViewGroup;

    iget-object v0, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-direct {p0, p1}, Lmiuix/appcompat/app/m;->b(Landroid/view/Window;)I

    move-result v2

    invoke-static {v0, v2, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lmiuix/appcompat/app/m;->I()Z

    move-result v2

    iput-boolean v2, p0, Lmiuix/appcompat/app/m;->y:Z

    iget-object v2, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    iget-boolean v3, p0, Lmiuix/appcompat/app/m;->y:Z

    invoke-virtual {v2, v3}, Lmiuix/appcompat/app/floatingactivity/a/d;->c(Z)V

    iget-object v2, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    iget-boolean v3, p0, Lmiuix/appcompat/app/m;->y:Z

    invoke-virtual {v2, v0, v3}, Lmiuix/appcompat/app/floatingactivity/a/d;->b(Landroid/view/View;Z)Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/app/m;->B:Landroid/view/ViewGroup;

    iget-boolean v2, p0, Lmiuix/appcompat/app/m;->y:Z

    invoke-direct {p0, v2}, Lmiuix/appcompat/app/m;->h(Z)V

    :cond_1
    sget v2, Ld/b/g;->action_bar_overlay_layout:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    instance-of v3, v2, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    const v4, 0x1020002

    if-eqz v3, :cond_3

    check-cast v2, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iput-object v2, p0, Lmiuix/appcompat/app/m;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iget-object v2, p0, Lmiuix/appcompat/app/m;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {v2, v4}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {p1, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    if-eqz v3, :cond_3

    :goto_1
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    if-lez v5, :cond_2

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->removeViewAt(I)V

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_1

    :cond_2
    const/4 v5, -0x1

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->setId(I)V

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->setId(I)V

    instance-of v2, v3, Landroid/widget/FrameLayout;

    if-eqz v2, :cond_3

    check-cast v3, Landroid/widget/FrameLayout;

    invoke-virtual {v3, v1}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    :cond_3
    invoke-virtual {p1, v0}, Landroid/view/Window;->setContentView(Landroid/view/View;)V

    iget-object p1, p0, Lmiuix/appcompat/app/m;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz p1, :cond_4

    invoke-virtual {p1, v4}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lmiuix/appcompat/app/m;->t:Landroid/view/ViewGroup;

    :cond_4
    iget-object p1, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    if-eqz p1, :cond_5

    iget-object v0, p0, Lmiuix/appcompat/app/m;->B:Landroid/view/ViewGroup;

    invoke-direct {p0}, Lmiuix/appcompat/app/m;->I()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lmiuix/appcompat/app/floatingactivity/a/d;->a(Landroid/view/View;Z)V

    :cond_5
    return-void
.end method

.method private g(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/m;->w:Lmiuix/appcompat/app/floatingactivity/l;

    invoke-interface {v0, p1}, Lmiuix/appcompat/app/floatingactivity/l;->b(Z)V

    return-void
.end method

.method private h(Z)V
    .locals 6

    iget-object v0, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v1

    and-int/lit16 v2, v1, 0x400

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_0

    move v2, v3

    goto :goto_0

    :cond_0
    move v2, v4

    :goto_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/f;->i()I

    move-result v5

    if-eqz v5, :cond_1

    move v5, v3

    goto :goto_1

    :cond_1
    move v5, v4

    :goto_1
    if-nez v2, :cond_3

    if-eqz v5, :cond_2

    goto :goto_2

    :cond_2
    move v2, v4

    goto :goto_3

    :cond_3
    :goto_2
    move v2, v3

    :goto_3
    const/16 v5, 0x1e

    if-nez p1, :cond_6

    if-eqz v2, :cond_4

    or-int/lit16 p1, v1, 0x400

    goto :goto_4

    :cond_4
    and-int/lit16 p1, v1, -0x401

    :goto_4
    move v1, p1

    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt p1, v5, :cond_7

    if-eqz v2, :cond_5

    invoke-virtual {v0, v4}, Landroid/view/Window;->setDecorFitsSystemWindows(Z)V

    goto :goto_5

    :cond_5
    invoke-virtual {v0, v3}, Landroid/view/Window;->setDecorFitsSystemWindows(Z)V

    goto :goto_5

    :cond_6
    const/high16 p1, 0xc000000

    invoke-virtual {v0, p1}, Landroid/view/Window;->addFlags(I)V

    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt p1, v5, :cond_7

    invoke-virtual {v0, v4}, Landroid/view/Window;->setDecorFitsSystemWindows(Z)V

    :cond_7
    :goto_5
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    return-void
.end method


# virtual methods
.method public A()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/app/m;->x:Z

    return v0
.end method

.method public B()Z
    .locals 1

    invoke-direct {p0}, Lmiuix/appcompat/app/m;->I()Z

    move-result v0

    return v0
.end method

.method C()V
    .locals 1

    goto/32 :goto_b

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->n()Z

    move-result v0

    goto/32 :goto_c

    nop

    :goto_2
    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->l()V

    goto/32 :goto_9

    nop

    :goto_3
    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    goto/32 :goto_13

    nop

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_1

    nop

    :goto_5
    iget-object v0, p0, Lmiuix/appcompat/app/m;->v:Lmiuix/appcompat/app/h;

    goto/32 :goto_7

    nop

    :goto_6
    if-nez v0, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_e

    nop

    :goto_7
    invoke-interface {v0}, Lmiuix/appcompat/app/h;->b()V

    goto/32 :goto_0

    nop

    :goto_8
    if-nez v0, :cond_2

    goto/32 :goto_f

    :cond_2
    goto/32 :goto_15

    nop

    :goto_9
    return-void

    :goto_a
    goto/32 :goto_10

    nop

    :goto_b
    iget-object v0, p0, Lmiuix/appcompat/app/f;->d:Landroid/view/ActionMode;

    goto/32 :goto_d

    nop

    :goto_c
    if-nez v0, :cond_3

    goto/32 :goto_a

    :cond_3
    goto/32 :goto_12

    nop

    :goto_d
    if-nez v0, :cond_4

    goto/32 :goto_14

    :cond_4
    goto/32 :goto_3

    nop

    :goto_e
    return-void

    :goto_f
    goto/32 :goto_5

    nop

    :goto_10
    iget-object v0, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    goto/32 :goto_8

    nop

    :goto_11
    iget-object v0, p0, Lmiuix/appcompat/app/f;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_4

    nop

    :goto_12
    iget-object v0, p0, Lmiuix/appcompat/app/f;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_2

    nop

    :goto_13
    return-void

    :goto_14
    goto/32 :goto_11

    nop

    :goto_15
    invoke-virtual {v0}, Lmiuix/appcompat/app/floatingactivity/a/d;->h()Z

    move-result v0

    goto/32 :goto_6

    nop
.end method

.method public D()Z
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lmiuix/appcompat/app/floatingactivity/a/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lmiuix/appcompat/app/m;->D:Z

    :cond_0
    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public E()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/app/floatingactivity/a/d;->i()V

    :cond_0
    return-void
.end method

.method public a(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 1

    invoke-virtual {p0}, Lmiuix/appcompat/app/f;->d()Lmiuix/appcompat/app/d;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/f;->d()Lmiuix/appcompat/app/d;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/u;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/u;->b(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-super {p0, p1}, Lmiuix/appcompat/app/f;->a(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object p1

    return-object p1
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/m;->H:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 4

    invoke-super {p0, p1}, Lmiuix/appcompat/app/f;->a(Landroid/content/res/Configuration;)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/m;->B()Z

    move-result v0

    iget v1, p1, Landroid/content/res/Configuration;->uiMode:I

    invoke-static {}, Ld/h/a/e;->b()Z

    move-result v2

    const/4 v3, 0x1

    invoke-direct {p0, v0, v1, v3, v2}, Lmiuix/appcompat/app/m;->a(ZIZZ)V

    iget-object v0, p0, Lmiuix/appcompat/app/m;->v:Lmiuix/appcompat/app/h;

    invoke-interface {v0, p1}, Lmiuix/appcompat/app/h;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 7

    sget-boolean v0, Ld/b/b/b/d;->a:Z

    const/4 v1, 0x1

    if-nez v0, :cond_0

    sput-boolean v1, Ld/b/b/b/d;->a:Z

    invoke-virtual {p0}, Lmiuix/appcompat/app/m;->h()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ld/b/b/b/d;->b(Landroid/content/Context;)V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/app/m;->v:Lmiuix/appcompat/app/h;

    invoke-interface {v0, p1}, Lmiuix/appcompat/app/h;->c(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lmiuix/appcompat/app/m;->G()V

    iget-boolean v0, p0, Lmiuix/appcompat/app/m;->x:Z

    invoke-virtual {p0, v0, p1}, Lmiuix/appcompat/app/m;->a(ZLandroid/os/Bundle;)V

    const/16 p1, 0x80

    const/4 v0, 0x0

    :try_start_0
    iget-object v2, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, p1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    move-object v2, v0

    :goto_0
    const-string v3, "miui.extra.window.padding.level"

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    iget-object v5, v2, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v5, :cond_1

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    goto :goto_1

    :cond_1
    move v2, v4

    :goto_1
    :try_start_1
    iget-object v5, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {v5}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    iget-object v6, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {v6}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v6

    invoke-virtual {v5, v6, p1}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception p1

    invoke-virtual {p1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    :goto_2
    if-eqz v0, :cond_2

    iget-object p1, v0, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    if-eqz p1, :cond_2

    iget-object p1, v0, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    invoke-virtual {p1, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    :cond_2
    iget-object p1, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    sget v0, Ld/b/b;->windowExtraPaddingHorizontal:I

    invoke-static {p1, v0, v2}, Ld/h/a/d;->b(Landroid/content/Context;II)I

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_3

    :cond_3
    move v1, v4

    :goto_3
    iget-object v0, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    sget v2, Ld/b/b;->windowExtraPaddingHorizontalEnable:I

    invoke-static {v0, v2, v1}, Ld/h/a/d;->a(Landroid/content/Context;IZ)Z

    move-result v0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/m;->e(I)V

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/m;->d(Z)V

    return-void
.end method

.method public a(Landroid/view/ActionMode;)V
    .locals 0

    const/4 p1, 0x0

    iput-object p1, p0, Lmiuix/appcompat/app/f;->d:Landroid/view/ActionMode;

    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, v0}, Lmiuix/appcompat/app/m;->b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/app/f;->e:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmiuix/appcompat/app/m;->G()V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/app/m;->t:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    iget-object p1, p0, Lmiuix/appcompat/app/m;->G:Lmiuix/appcompat/app/m$a;

    invoke-virtual {p1}, La/a/d/j;->a()Landroid/view/Window$Callback;

    move-result-object p1

    invoke-interface {p1}, Landroid/view/Window$Callback;->onContentChanged()V

    return-void
.end method

.method a(Ljava/lang/CharSequence;)V
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setWindowTitle(Ljava/lang/CharSequence;)V

    :goto_1
    goto/32 :goto_3

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_0

    nop

    :goto_3
    return-void

    :goto_4
    iput-object p1, p0, Lmiuix/appcompat/app/m;->E:Ljava/lang/CharSequence;

    goto/32 :goto_5

    nop

    :goto_5
    iget-object v0, p0, Lmiuix/appcompat/app/f;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_2

    nop
.end method

.method public a(Lmiuix/appcompat/app/floatingactivity/j;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/floatingactivity/a/d;->a(Lmiuix/appcompat/app/floatingactivity/j;)V

    :cond_0
    return-void
.end method

.method public a(Lmiuix/appcompat/app/floatingactivity/k;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/floatingactivity/a/d;->a(Lmiuix/appcompat/app/floatingactivity/k;)V

    :cond_0
    return-void
.end method

.method public a(Lmiuix/appcompat/app/s;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/m;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setOnStatusBarChangeListener(Lmiuix/appcompat/app/s;)V

    :cond_0
    return-void
.end method

.method public a(ZLandroid/os/Bundle;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-static {p1}, Lmiuix/appcompat/app/floatingactivity/multiapp/MultiAppFloatingActivitySwitcher;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-static {v0, p1, p2}, Lmiuix/appcompat/app/floatingactivity/multiapp/MultiAppFloatingActivitySwitcher;->a(Lmiuix/appcompat/app/AppCompatActivity;Landroid/content/Intent;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-static {p1, p2}, Lmiuix/appcompat/app/floatingactivity/FloatingActivitySwitcher;->a(Lmiuix/appcompat/app/AppCompatActivity;Landroid/os/Bundle;)V

    :goto_0
    return-void
.end method

.method public a(ILandroid/view/Menu;)Z
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/m;->v:Lmiuix/appcompat/app/h;

    invoke-interface {v0, p1, p2}, Lmiuix/appcompat/app/h;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public a(ILandroid/view/MenuItem;)Z
    .locals 1
    .param p2    # Landroid/view/MenuItem;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lmiuix/appcompat/app/m;->v:Lmiuix/appcompat/app/h;

    invoke-interface {v0, p1, p2}, Lmiuix/appcompat/app/h;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 v0, 0x0

    if-eqz p1, :cond_1

    return v0

    :cond_1
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result p1

    const p2, 0x102002c

    if-ne p1, p2, :cond_3

    invoke-virtual {p0}, Lmiuix/appcompat/app/f;->d()Lmiuix/appcompat/app/d;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lmiuix/appcompat/app/f;->d()Lmiuix/appcompat/app/d;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/appcompat/app/ActionBar;->g()I

    move-result p1

    and-int/lit8 p1, p1, 0x4

    if-eqz p1, :cond_3

    iget-object p1, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {p1}, Landroid/app/Activity;->getParent()Landroid/app/Activity;

    move-result-object p1

    if-nez p1, :cond_2

    iget-object p1, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {p1}, Landroid/app/Activity;->onNavigateUp()Z

    move-result p1

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {p1}, Landroid/app/Activity;->getParent()Landroid/app/Activity;

    move-result-object p1

    iget-object p2, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {p1, p2}, Landroid/app/Activity;->onNavigateUpFromChild(Landroid/app/Activity;)Z

    move-result p1

    :goto_0
    if-nez p1, :cond_3

    iget-object p1, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {p1}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    :cond_3
    return v0
.end method

.method public a(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/m;->v:Lmiuix/appcompat/app/h;

    invoke-interface {v0, p1, p2, p3}, Lmiuix/appcompat/app/h;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public a(Lmiuix/appcompat/internal/view/menu/i;Landroid/view/MenuItem;)Z
    .locals 1

    iget-object p1, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    const/4 v0, 0x0

    invoke-virtual {p1, v0, p2}, Lmiuix/appcompat/app/AppCompatActivity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public b(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 1

    instance-of v0, p1, Lmiuix/view/f$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/m;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/f;->a(Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;)V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/app/m;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public b()Lmiuix/appcompat/app/d;
    .locals 3

    iget-boolean v0, p0, Lmiuix/appcompat/app/f;->e:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmiuix/appcompat/app/m;->G()V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/app/m;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    return-object v0

    :cond_1
    new-instance v1, Lmiuix/appcompat/internal/app/widget/u;

    iget-object v2, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-direct {v1, v2, v0}, Lmiuix/appcompat/internal/app/widget/u;-><init>(Lmiuix/appcompat/app/AppCompatActivity;Landroid/view/ViewGroup;)V

    return-object v1
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/m;->v:Lmiuix/appcompat/app/h;

    invoke-interface {v0, p1}, Lmiuix/appcompat/app/h;->a(Landroid/os/Bundle;)V

    iget-object v0, p0, Lmiuix/appcompat/app/m;->s:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v0, :cond_0

    const-string v0, "miuix:ActionBar"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/m;->s:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->restoreHierarchyState(Landroid/util/SparseArray;)V

    :cond_0
    return-void
.end method

.method public b(Landroid/view/ActionMode;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/app/f;->d:Landroid/view/ActionMode;

    return-void
.end method

.method public b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/app/f;->e:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmiuix/appcompat/app/m;->G()V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/app/m;->t:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, Lmiuix/appcompat/app/m;->t:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    iget-object p1, p0, Lmiuix/appcompat/app/m;->G:Lmiuix/appcompat/app/m$a;

    invoke-virtual {p1}, La/a/d/j;->a()Landroid/view/Window$Callback;

    move-result-object p1

    invoke-interface {p1}, Landroid/view/Window$Callback;->onContentChanged()V

    return-void
.end method

.method public c(I)Landroid/view/View;
    .locals 4

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/m;->v:Lmiuix/appcompat/app/h;

    invoke-interface {v0, p1}, Lmiuix/appcompat/app/h;->onCreatePanelView(I)Landroid/view/View;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/f;->k()Z

    move-result p1

    const/4 v0, 0x0

    if-nez p1, :cond_6

    iget-object p1, p0, Lmiuix/appcompat/app/f;->c:Lmiuix/appcompat/internal/view/menu/i;

    iget-object v1, p0, Lmiuix/appcompat/app/f;->d:Landroid/view/ActionMode;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v1, :cond_2

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lmiuix/appcompat/app/f;->c()Lmiuix/appcompat/internal/view/menu/i;

    move-result-object p1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/f;->e(Lmiuix/appcompat/internal/view/menu/i;)V

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/i;->q()V

    iget-object v1, p0, Lmiuix/appcompat/app/m;->v:Lmiuix/appcompat/app/h;

    invoke-interface {v1, v3, p1}, Lmiuix/appcompat/app/h;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v2

    :cond_1
    if-eqz v2, :cond_4

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/i;->q()V

    iget-object v1, p0, Lmiuix/appcompat/app/m;->v:Lmiuix/appcompat/app/h;

    invoke-interface {v1, v3, v0, p1}, Lmiuix/appcompat/app/h;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v2

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    move v2, v3

    :cond_4
    :goto_0
    if-eqz v2, :cond_5

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/i;->p()V

    goto :goto_1

    :cond_5
    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/f;->e(Lmiuix/appcompat/internal/view/menu/i;)V

    :cond_6
    :goto_1
    return-object v0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/app/m;->v:Lmiuix/appcompat/app/h;

    invoke-interface {v0, p1}, Lmiuix/appcompat/app/h;->b(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-static {v0, p1}, Lmiuix/appcompat/app/floatingactivity/FloatingActivitySwitcher;->b(Lmiuix/appcompat/app/AppCompatActivity;Landroid/os/Bundle;)V

    iget-object v0, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getTaskId()I

    move-result v0

    iget-object v1, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {v1}, Lmiuix/appcompat/app/AppCompatActivity;->getActivityIdentity()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lmiuix/appcompat/app/floatingactivity/multiapp/MultiAppFloatingActivitySwitcher;->a(ILjava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/app/m;->s:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v0, :cond_1

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iget-object v1, p0, Lmiuix/appcompat/app/m;->s:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->saveHierarchyState(Landroid/util/SparseArray;)V

    const-string v1, "miuix:ActionBar"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    :cond_1
    return-void
.end method

.method public c(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/floatingactivity/a/d;->a(Z)V

    :cond_0
    return-void
.end method

.method protected c(Lmiuix/appcompat/internal/view/menu/i;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public d(I)V
    .locals 2

    iget-boolean v0, p0, Lmiuix/appcompat/app/f;->e:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmiuix/appcompat/app/m;->G()V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/app/m;->t:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, Lmiuix/appcompat/app/m;->u:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lmiuix/appcompat/app/m;->t:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    :cond_1
    iget-object p1, p0, Lmiuix/appcompat/app/m;->G:Lmiuix/appcompat/app/m$a;

    invoke-virtual {p1}, La/a/d/j;->a()Landroid/view/Window$Callback;

    move-result-object p1

    invoke-interface {p1}, Landroid/view/Window$Callback;->onContentChanged()V

    return-void
.end method

.method public d(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/m;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setExtraHorizontalPaddingEnable(Z)V

    :cond_0
    return-void
.end method

.method protected d(Lmiuix/appcompat/internal/view/menu/i;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public e(I)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/m;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setExtraHorizontalPaddingLevel(I)V

    :cond_0
    return-void
.end method

.method public e(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/floatingactivity/a/d;->b(Z)V

    :cond_0
    return-void
.end method

.method public f(Z)V
    .locals 3

    iget v0, p0, Lmiuix/appcompat/app/m;->z:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lmiuix/appcompat/app/m;->a(ZIZZ)V

    return-void
.end method

.method public h()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/f;->a:Lmiuix/appcompat/app/AppCompatActivity;

    return-object v0
.end method

.method public l()V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/app/m;->v:Lmiuix/appcompat/app/h;

    invoke-interface {v0}, Lmiuix/appcompat/app/h;->a()V

    invoke-virtual {p0}, Lmiuix/appcompat/app/f;->d()Lmiuix/appcompat/app/d;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/u;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiuix/appcompat/internal/app/widget/u;->f(Z)V

    :cond_0
    return-void
.end method

.method public m()V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/app/m;->v:Lmiuix/appcompat/app/h;

    invoke-interface {v0}, Lmiuix/appcompat/app/h;->onStop()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/f;->a(Z)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/f;->d()Lmiuix/appcompat/app/d;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/app/widget/u;

    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Lmiuix/appcompat/internal/app/widget/u;->f(Z)V

    :cond_0
    return-void
.end method

.method public o()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmiuix/appcompat/app/floatingactivity/h;->executeCloseEnterAnimation()V

    :cond_0
    return-void
.end method

.method public p()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmiuix/appcompat/app/floatingactivity/h;->executeCloseExitAnimation()V

    :cond_0
    return-void
.end method

.method public q()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmiuix/appcompat/app/floatingactivity/h;->executeOpenEnterAnimation()V

    :cond_0
    return-void
.end method

.method public r()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmiuix/appcompat/app/floatingactivity/h;->executeOpenExitAnimation()V

    :cond_0
    return-void
.end method

.method public s()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/app/floatingactivity/a/d;->b()V

    :cond_0
    return-void
.end method

.method public t()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/m;->C:Ljava/lang/String;

    return-object v0
.end method

.method public u()I
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/m;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->getExtraHorizontalPaddingLevel()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public v()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/app/floatingactivity/a/d;->c()Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public w()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/app/floatingactivity/a/d;->e()V

    :cond_0
    return-void
.end method

.method public x()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/m;->A:Lmiuix/appcompat/app/floatingactivity/a/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/app/floatingactivity/a/d;->f()V

    :cond_0
    return-void
.end method

.method public y()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/app/m;->D:Z

    return v0
.end method

.method public z()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/m;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->a()Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
