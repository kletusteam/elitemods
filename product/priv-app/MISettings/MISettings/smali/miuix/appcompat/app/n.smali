.class Lmiuix/appcompat/app/n;
.super Ld/b/b/c/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/appcompat/app/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lmiuix/appcompat/app/o;


# direct methods
.method constructor <init>(Lmiuix/appcompat/app/o;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/app/n;->a:Lmiuix/appcompat/app/o;

    invoke-direct {p0}, Ld/b/b/c/e;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionModeFinished(Landroid/view/ActionMode;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/n;->a:Lmiuix/appcompat/app/o;

    invoke-static {v0}, Lmiuix/appcompat/app/o;->a(Lmiuix/appcompat/app/o;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/app/q;

    invoke-interface {v0, p1}, Lmiuix/appcompat/app/q;->onActionModeFinished(Landroid/view/ActionMode;)V

    return-void
.end method

.method public onActionModeStarted(Landroid/view/ActionMode;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/n;->a:Lmiuix/appcompat/app/o;

    invoke-static {v0}, Lmiuix/appcompat/app/o;->a(Lmiuix/appcompat/app/o;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/app/q;

    invoke-interface {v0, p1}, Lmiuix/appcompat/app/q;->onActionModeStarted(Landroid/view/ActionMode;)V

    return-void
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/n;->a:Lmiuix/appcompat/app/o;

    invoke-virtual {v0, p1, p2}, Lmiuix/appcompat/app/o;->a(ILandroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/n;->a:Lmiuix/appcompat/app/o;

    invoke-virtual {v0}, Lmiuix/appcompat/app/f;->f()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/n;->a:Lmiuix/appcompat/app/o;

    invoke-virtual {v0}, Lmiuix/appcompat/app/f;->f()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroidx/fragment/app/FragmentActivity;->onPanelClosed(ILandroid/view/Menu;)V

    :cond_0
    return-void
.end method

.method public onWindowStartingActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/n;->a:Lmiuix/appcompat/app/o;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/o;->a(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object p1

    return-object p1
.end method
