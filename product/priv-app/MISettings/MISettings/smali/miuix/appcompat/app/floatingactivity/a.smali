.class public final synthetic Lmiuix/appcompat/app/floatingactivity/a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic a:Lmiuix/appcompat/app/floatingactivity/SingleAppFloatingLifecycleObserver;

.field private final synthetic b:Landroid/view/View;

.field private final synthetic c:Lmiuix/appcompat/app/AppCompatActivity;


# direct methods
.method public synthetic constructor <init>(Lmiuix/appcompat/app/floatingactivity/SingleAppFloatingLifecycleObserver;Landroid/view/View;Lmiuix/appcompat/app/AppCompatActivity;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a;->a:Lmiuix/appcompat/app/floatingactivity/SingleAppFloatingLifecycleObserver;

    iput-object p2, p0, Lmiuix/appcompat/app/floatingactivity/a;->b:Landroid/view/View;

    iput-object p3, p0, Lmiuix/appcompat/app/floatingactivity/a;->c:Lmiuix/appcompat/app/AppCompatActivity;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a;->a:Lmiuix/appcompat/app/floatingactivity/SingleAppFloatingLifecycleObserver;

    iget-object v1, p0, Lmiuix/appcompat/app/floatingactivity/a;->b:Landroid/view/View;

    iget-object v2, p0, Lmiuix/appcompat/app/floatingactivity/a;->c:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {v0, v1, v2}, Lmiuix/appcompat/app/floatingactivity/SingleAppFloatingLifecycleObserver;->a(Landroid/view/View;Lmiuix/appcompat/app/AppCompatActivity;)V

    return-void
.end method
