.class public Lmiuix/appcompat/app/floatingactivity/a/e;
.super Ljava/lang/Object;


# direct methods
.method public static a(Lmiuix/appcompat/app/AppCompatActivity;)Lmiuix/appcompat/app/floatingactivity/a/d;
    .locals 2
    .param p0    # Lmiuix/appcompat/app/AppCompatActivity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lmiuix/appcompat/app/floatingactivity/n;->a(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Ld/h/a/e;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lmiuix/appcompat/app/floatingactivity/a/f;

    invoke-direct {v0, p0}, Lmiuix/appcompat/app/floatingactivity/a/f;-><init>(Lmiuix/appcompat/app/AppCompatActivity;)V

    return-object v0

    :cond_0
    if-nez v0, :cond_1

    invoke-static {p0}, Ld/h/a/e;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lmiuix/appcompat/app/floatingactivity/a/g;

    invoke-direct {v0, p0}, Lmiuix/appcompat/app/floatingactivity/a/g;-><init>(Lmiuix/appcompat/app/AppCompatActivity;)V

    return-object v0

    :cond_1
    new-instance v0, Lmiuix/appcompat/app/floatingactivity/a/h;

    invoke-direct {v0, p0}, Lmiuix/appcompat/app/floatingactivity/a/h;-><init>(Landroid/content/Context;)V

    return-object v0
.end method
