.class Lmiuix/appcompat/app/o$a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/appcompat/app/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lmiuix/appcompat/app/o;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lmiuix/appcompat/app/o;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/appcompat/app/o$a;->a:Ljava/lang/ref/WeakReference;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmiuix/appcompat/app/o$a;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Lmiuix/appcompat/app/o$a;->a:Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object v0, v1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/app/o;

    :goto_0
    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-static {v0}, Lmiuix/appcompat/app/o;->b(Lmiuix/appcompat/app/o;)B

    move-result v2

    const/4 v3, 0x1

    and-int/2addr v2, v3

    if-ne v2, v3, :cond_2

    invoke-static {v0, v1}, Lmiuix/appcompat/app/o;->a(Lmiuix/appcompat/app/o;Lmiuix/appcompat/internal/view/menu/i;)Lmiuix/appcompat/internal/view/menu/i;

    :cond_2
    invoke-static {v0}, Lmiuix/appcompat/app/o;->c(Lmiuix/appcompat/app/o;)Lmiuix/appcompat/internal/view/menu/i;

    move-result-object v2

    const/4 v4, 0x0

    if-nez v2, :cond_3

    invoke-virtual {v0}, Lmiuix/appcompat/app/f;->c()Lmiuix/appcompat/internal/view/menu/i;

    move-result-object v2

    invoke-static {v0, v2}, Lmiuix/appcompat/app/o;->a(Lmiuix/appcompat/app/o;Lmiuix/appcompat/internal/view/menu/i;)Lmiuix/appcompat/internal/view/menu/i;

    invoke-static {v0}, Lmiuix/appcompat/app/o;->c(Lmiuix/appcompat/app/o;)Lmiuix/appcompat/internal/view/menu/i;

    move-result-object v2

    invoke-virtual {v0, v4, v2}, Lmiuix/appcompat/app/o;->a(ILandroid/view/Menu;)Z

    move-result v3

    :cond_3
    if-eqz v3, :cond_4

    invoke-static {v0}, Lmiuix/appcompat/app/o;->c(Lmiuix/appcompat/app/o;)Lmiuix/appcompat/internal/view/menu/i;

    move-result-object v2

    invoke-virtual {v0, v4, v1, v2}, Lmiuix/appcompat/app/o;->a(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v3

    :cond_4
    if-eqz v3, :cond_5

    invoke-static {v0}, Lmiuix/appcompat/app/o;->c(Lmiuix/appcompat/app/o;)Lmiuix/appcompat/internal/view/menu/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/f;->e(Lmiuix/appcompat/internal/view/menu/i;)V

    goto :goto_1

    :cond_5
    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/f;->e(Lmiuix/appcompat/internal/view/menu/i;)V

    invoke-static {v0, v1}, Lmiuix/appcompat/app/o;->a(Lmiuix/appcompat/app/o;Lmiuix/appcompat/internal/view/menu/i;)Lmiuix/appcompat/internal/view/menu/i;

    :goto_1
    const/16 v1, -0x12

    invoke-static {v0, v1}, Lmiuix/appcompat/app/o;->a(Lmiuix/appcompat/app/o;I)B

    return-void
.end method
