.class public abstract Lmiuix/appcompat/app/floatingactivity/a/k;
.super Lmiuix/appcompat/app/floatingactivity/a/d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/app/floatingactivity/a/k$b;,
        Lmiuix/appcompat/app/floatingactivity/a/k$a;
    }
.end annotation


# instance fields
.field protected a:Lmiuix/appcompat/app/AppCompatActivity;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Lmiuix/internal/widget/RoundFrameLayout;

.field private h:Landroid/view/GestureDetector;

.field private i:Landroid/view/ViewGroup$LayoutParams;

.field private j:Lmiuix/appcompat/app/floatingactivity/j;

.field private k:Lmiuix/appcompat/app/floatingactivity/k;

.field private l:F

.field private m:F

.field private n:F

.field private o:F

.field private final p:I

.field private q:Z

.field private r:F

.field private final s:Landroid/os/Handler;

.field private t:Z

.field private u:Z

.field private v:Z

.field private final w:Landroid/graphics/drawable/Drawable;

.field private x:I


# direct methods
.method public constructor <init>(Lmiuix/appcompat/app/AppCompatActivity;)V
    .locals 3

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/d;-><init>()V

    const/16 v0, 0x5a

    iput v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->p:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->q:Z

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->s:Landroid/os/Handler;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->t:Z

    iput-boolean v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->u:Z

    iput-boolean v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->v:Z

    iput v1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->x:I

    iput-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    const v0, 0x1010054

    invoke-static {p1, v0}, Ld/h/a/d;->d(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->w:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method private A()V
    .locals 5

    iget-boolean v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->u:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->g:Lmiuix/internal/widget/RoundFrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getAlpha()F

    move-result v0

    iget-object v1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->g:Lmiuix/internal/widget/RoundFrameLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setAlpha(F)V

    iget-object v1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->g:Lmiuix/internal/widget/RoundFrameLayout;

    new-instance v2, Lmiuix/appcompat/app/floatingactivity/a/b;

    invoke-direct {v2, p0, v0}, Lmiuix/appcompat/app/floatingactivity/a/b;-><init>(Lmiuix/appcompat/app/floatingactivity/a/k;F)V

    const-wide/16 v3, 0x5a

    invoke-virtual {v1, v2, v3, v4}, Landroid/widget/FrameLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private a(I)V
    .locals 1

    invoke-direct {p0, p1}, Lmiuix/appcompat/app/floatingactivity/a/k;->c(I)V

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->u()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {p1}, Lmiuix/appcompat/app/AppCompatActivity;->realFinish()V

    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-static {p1}, Lmiuix/appcompat/app/floatingactivity/c;->g(Lmiuix/appcompat/app/AppCompatActivity;)V

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->t:Z

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lmiuix/appcompat/app/floatingactivity/a/k;->b(I)V

    :cond_1
    :goto_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->j()V

    return-void
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_7

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result p1

    iget v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->n:F

    iget v2, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->m:F

    sub-float v2, p1, v2

    add-float/2addr v0, v2

    iput v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->n:F

    iget v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->n:F

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_1

    invoke-direct {p0, v0}, Lmiuix/appcompat/app/floatingactivity/a/k;->c(F)V

    iget v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->n:F

    iget v1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->r:F

    div-float/2addr v0, v1

    invoke-direct {p0, v0}, Lmiuix/appcompat/app/floatingactivity/a/k;->b(F)V

    :cond_1
    iput p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->m:F

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result p1

    iget v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->l:F

    sub-float/2addr p1, v0

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    cmpl-float p1, p1, v0

    const/4 v0, 0x0

    if-lez p1, :cond_3

    move p1, v2

    goto :goto_0

    :cond_3
    move p1, v0

    :goto_0
    invoke-direct {p0, v2}, Lmiuix/appcompat/app/floatingactivity/a/k;->c(I)V

    if-eqz p1, :cond_6

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->r()V

    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->k:Lmiuix/appcompat/app/floatingactivity/k;

    if-eqz p1, :cond_4

    invoke-interface {p1, v2}, Lmiuix/appcompat/app/floatingactivity/j;->a(I)Z

    move-result p1

    if-nez p1, :cond_5

    :cond_4
    move v0, v2

    :cond_5
    invoke-direct {p0, v0, v2}, Lmiuix/appcompat/app/floatingactivity/a/k;->a(ZI)V

    goto :goto_1

    :cond_6
    invoke-direct {p0, v0, v2}, Lmiuix/appcompat/app/floatingactivity/a/k;->a(ZI)V

    goto :goto_1

    :cond_7
    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->y()V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result p1

    iput p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->l:F

    iget p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->l:F

    iput p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->m:F

    iput v1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->n:F

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->v()V

    :goto_1
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->e:Landroid/view/View;

    return-void
.end method

.method private a(Ljava/lang/Object;)V
    .locals 2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "dismiss"

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {p1}, Lmiuix/appcompat/app/AppCompatActivity;->realFinish()V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "init"

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->x()V

    :cond_1
    :goto_0
    const/4 p1, 0x0

    iput-boolean p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->t:Z

    return-void
.end method

.method static synthetic a(Lmiuix/appcompat/app/floatingactivity/a/k;I)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/appcompat/app/floatingactivity/a/k;->c(I)V

    return-void
.end method

.method static synthetic a(Lmiuix/appcompat/app/floatingactivity/a/k;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/appcompat/app/floatingactivity/a/k;->a(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lmiuix/appcompat/app/floatingactivity/a/k;ZI)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/app/floatingactivity/a/k;->b(ZI)V

    return-void
.end method

.method private a(Lmiuix/internal/widget/RoundFrameLayout;)V
    .locals 4
    .param p1    # Lmiuix/internal/widget/RoundFrameLayout;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-boolean v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->u:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->v:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Ld/b/e;->miuix_appcompat_floating_window_background_border_width:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    sget v3, Ld/b/b;->miuixAppcompatFloatingWindowBorderColor:I

    invoke-static {v2, v3, v1}, Ld/h/a/d;->a(Landroid/content/Context;II)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lmiuix/internal/widget/RoundFrameLayout;->setBorder(FI)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v1}, Lmiuix/internal/widget/RoundFrameLayout;->setBorder(FI)V

    :goto_0
    return-void
.end method

.method private a(ZI)V
    .locals 15

    move-object v6, p0

    iget-boolean v0, v6, Lmiuix/appcompat/app/floatingactivity/a/k;->t:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    return-void

    :cond_0
    const/4 v7, 0x1

    iput-boolean v7, v6, Lmiuix/appcompat/app/floatingactivity/a/k;->t:Z

    const/4 v8, 0x0

    if-eqz p1, :cond_1

    iget v0, v6, Lmiuix/appcompat/app/floatingactivity/a/k;->r:F

    float-to-int v0, v0

    const/4 v1, 0x0

    const-string v2, "dismiss"

    move v9, v0

    move v11, v1

    move-object v10, v2

    goto :goto_0

    :cond_1
    const v1, 0x3e99999a    # 0.3f

    const-string v2, "init"

    move v11, v1

    move-object v10, v2

    move v9, v8

    :goto_0
    const/4 v0, 0x0

    invoke-static {v7, v0}, Lmiuix/appcompat/app/floatingactivity/f;->a(ILjava/lang/Runnable;)Lmiuix/animation/a/a;

    move-result-object v12

    new-array v13, v7, [Lmiuix/animation/e/b;

    new-instance v14, Lmiuix/appcompat/app/floatingactivity/a/k$b;

    const/4 v5, 0x0

    move-object v0, v14

    move-object v1, p0

    move/from16 v2, p1

    move v3, v9

    move/from16 v4, p2

    invoke-direct/range {v0 .. v5}, Lmiuix/appcompat/app/floatingactivity/a/k$b;-><init>(Lmiuix/appcompat/app/floatingactivity/a/k;ZIILmiuix/appcompat/app/floatingactivity/a/i;)V

    aput-object v14, v13, v8

    invoke-virtual {v12, v13}, Lmiuix/animation/a/a;->a([Lmiuix/animation/e/b;)Lmiuix/animation/a/a;

    new-instance v0, Lmiuix/animation/b/a;

    invoke-direct {v0, v10}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    sget-object v1, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    int-to-double v2, v9

    invoke-virtual {v0, v1, v2, v3}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    new-instance v1, Lmiuix/animation/b/a;

    invoke-direct {v1, v10}, Lmiuix/animation/b/a;-><init>(Ljava/lang/Object;)V

    sget-object v2, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    float-to-double v3, v11

    invoke-virtual {v1, v2, v3, v4}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    new-array v2, v7, [Landroid/view/View;

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->q()Landroid/view/View;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v2}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v2

    new-array v3, v7, [Lmiuix/animation/a/a;

    aput-object v12, v3, v8

    invoke-interface {v2, v0, v3}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    new-array v0, v7, [Landroid/view/View;

    iget-object v2, v6, Lmiuix/appcompat/app/floatingactivity/a/k;->c:Landroid/view/View;

    aput-object v2, v0, v8

    invoke-static {v0}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v0

    new-array v2, v8, [Lmiuix/animation/a/a;

    invoke-interface {v0, v1, v2}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    return-void
.end method

.method static synthetic a(Lmiuix/appcompat/app/floatingactivity/a/k;)Z
    .locals 0

    iget-boolean p0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->q:Z

    return p0
.end method

.method private b(F)V
    .locals 2

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    move-result p1

    const/4 v1, 0x0

    invoke-static {v1, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    iget-object v1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->c:Landroid/view/View;

    sub-float/2addr v0, p1

    const p1, 0x3e99999a    # 0.3f

    mul-float/2addr v0, p1

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    return-void
.end method

.method private b(I)V
    .locals 1

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->v()V

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->z()V

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lmiuix/appcompat/app/floatingactivity/a/k;->a(ZI)V

    return-void
.end method

.method static synthetic b(Lmiuix/appcompat/app/floatingactivity/a/k;)V
    .locals 0

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->r()V

    return-void
.end method

.method static synthetic b(Lmiuix/appcompat/app/floatingactivity/a/k;ZI)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/app/floatingactivity/a/k;->c(ZI)V

    return-void
.end method

.method private b(ZI)V
    .locals 0

    if-eqz p1, :cond_0

    iget-boolean p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->t:Z

    if-nez p1, :cond_0

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->v()V

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->z()V

    const/4 p1, 0x1

    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/app/floatingactivity/a/k;->a(ZI)V

    :cond_0
    return-void
.end method

.method private c(F)V
    .locals 1

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->q()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setTranslationY(F)V

    return-void
.end method

.method private c(I)V
    .locals 0

    iput p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->x:I

    return-void
.end method

.method private c(ZI)V
    .locals 1

    invoke-direct {p0, p2}, Lmiuix/appcompat/app/floatingactivity/a/k;->c(I)V

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->j:Lmiuix/appcompat/app/floatingactivity/j;

    if-eqz p1, :cond_0

    invoke-interface {p1, p2}, Lmiuix/appcompat/app/floatingactivity/j;->a(I)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-direct {p0, v0, p2}, Lmiuix/appcompat/app/floatingactivity/a/k;->a(ZI)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->k:Lmiuix/appcompat/app/floatingactivity/k;

    if-eqz p1, :cond_1

    invoke-interface {p1, p2}, Lmiuix/appcompat/app/floatingactivity/j;->a(I)Z

    move-result p1

    if-nez p1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    invoke-direct {p0, v0, p2}, Lmiuix/appcompat/app/floatingactivity/a/k;->a(ZI)V

    goto :goto_0

    :cond_3
    invoke-direct {p0, v0, p2}, Lmiuix/appcompat/app/floatingactivity/a/k;->a(ZI)V

    :goto_0
    return-void
.end method

.method static synthetic c(Lmiuix/appcompat/app/floatingactivity/a/k;)Z
    .locals 0

    iget-boolean p0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->u:Z

    return p0
.end method

.method static synthetic d(Lmiuix/appcompat/app/floatingactivity/a/k;)V
    .locals 0

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->v()V

    return-void
.end method

.method static synthetic e(Lmiuix/appcompat/app/floatingactivity/a/k;)V
    .locals 0

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->z()V

    return-void
.end method

.method static synthetic f(Lmiuix/appcompat/app/floatingactivity/a/k;)Landroid/view/GestureDetector;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->h:Landroid/view/GestureDetector;

    return-object p0
.end method

.method static synthetic g(Lmiuix/appcompat/app/floatingactivity/a/k;)Z
    .locals 0

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->u()Z

    move-result p0

    return p0
.end method

.method private n()Z
    .locals 2

    new-instance v0, Lmiuix/appcompat/app/floatingactivity/a/k$a;

    iget-object v1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-direct {v0, p0, v1}, Lmiuix/appcompat/app/floatingactivity/a/k$a;-><init>(Lmiuix/appcompat/app/floatingactivity/a/k;Lmiuix/appcompat/app/AppCompatActivity;)V

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lmiuix/appcompat/app/floatingactivity/a/k$a;->a(Lmiuix/appcompat/app/floatingactivity/a/k$a;Z)V

    return v1
.end method

.method private o()V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->d:Landroid/view/View;

    new-instance v1, Lmiuix/appcompat/app/floatingactivity/a/a;

    invoke-direct {v1, p0}, Lmiuix/appcompat/app/floatingactivity/a/a;-><init>(Lmiuix/appcompat/app/floatingactivity/a/k;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private p()V
    .locals 7

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->q()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    iget-object v2, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->f:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    const/4 v3, 0x2

    div-int/2addr v2, v3

    add-int/2addr v1, v2

    const/4 v2, 0x1

    new-array v4, v2, [Landroid/view/View;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v4}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v0

    new-array v4, v3, [Ljava/lang/Object;

    sget-object v6, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    aput-object v6, v4, v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v2

    invoke-interface {v0, v4}, Lmiuix/animation/k;->d([Ljava/lang/Object;)Lmiuix/animation/k;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v4, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    aput-object v4, v1, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lmiuix/appcompat/app/floatingactivity/f;->a(ILjava/lang/Runnable;)Lmiuix/animation/a/a;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, Lmiuix/animation/k;->c([Ljava/lang/Object;)Lmiuix/animation/k;

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->c:Landroid/view/View;

    invoke-static {v0}, Lmiuix/appcompat/widget/a/a;->b(Landroid/view/View;)V

    return-void
.end method

.method private q()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->e:Landroid/view/View;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->d:Landroid/view/View;

    :cond_0
    return-object v0
.end method

.method private r()V
    .locals 2

    invoke-static {}, Lmiuix/appcompat/app/floatingactivity/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->k:Lmiuix/appcompat/app/floatingactivity/k;

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->q:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-interface {v0, v1}, Lmiuix/appcompat/app/floatingactivity/k;->a(Lmiuix/appcompat/app/AppCompatActivity;)V

    :cond_1
    return-void
.end method

.method private s()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->u:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private t()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->k:Lmiuix/appcompat/app/floatingactivity/k;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Lmiuix/appcompat/app/floatingactivity/k;->e()Z

    move-result v0

    :goto_0
    return v0
.end method

.method private u()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->u:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->k:Lmiuix/appcompat/app/floatingactivity/k;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Lmiuix/appcompat/app/floatingactivity/k;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method private v()V
    .locals 3

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->q()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    iget-object v2, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->f:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    sub-int/2addr v2, v0

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v0, v1

    iput v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->r:F

    return-void
.end method

.method private w()V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->k:Lmiuix/appcompat/app/floatingactivity/k;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-interface {v0, v1}, Lmiuix/appcompat/app/floatingactivity/k;->b(Lmiuix/appcompat/app/AppCompatActivity;)V

    :cond_0
    return-void
.end method

.method private x()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->k:Lmiuix/appcompat/app/floatingactivity/k;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmiuix/appcompat/app/floatingactivity/k;->f()V

    :cond_0
    return-void
.end method

.method private y()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->k:Lmiuix/appcompat/app/floatingactivity/k;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmiuix/appcompat/app/floatingactivity/k;->b()V

    :cond_0
    return-void
.end method

.method private z()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->k:Lmiuix/appcompat/app/floatingactivity/k;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmiuix/appcompat/app/floatingactivity/k;->d()V

    :cond_0
    return-void
.end method


# virtual methods
.method public synthetic a(F)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->g:Lmiuix/internal/widget/RoundFrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setAlpha(F)V

    return-void
.end method

.method public a(Landroid/view/View;Z)V
    .locals 2

    sget v0, Ld/b/g;->sliding_drawer_handle:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->b:Landroid/view/View;

    sget v0, Ld/b/g;->action_bar_overlay_bg:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->c:Landroid/view/View;

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->c:Landroid/view/View;

    const v1, 0x3e99999a    # 0.3f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    sget v0, Ld/b/g;->action_bar_overlay_layout:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->d:Landroid/view/View;

    sget v0, Ld/b/g;->action_bar_overlay_floating_root:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->f:Landroid/view/View;

    iput-boolean p2, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->u:Z

    const/4 p2, 0x0

    iput-boolean p2, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->q:Z

    new-instance p2, Landroid/view/GestureDetector;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    new-instance v0, Lmiuix/appcompat/app/floatingactivity/a/i;

    invoke-direct {v0, p0}, Lmiuix/appcompat/app/floatingactivity/a/i;-><init>(Lmiuix/appcompat/app/floatingactivity/a/k;)V

    invoke-direct {p2, p1, v0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object p2, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->h:Landroid/view/GestureDetector;

    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->f:Landroid/view/View;

    new-instance p2, Lmiuix/appcompat/app/floatingactivity/a/j;

    invoke-direct {p2, p0}, Lmiuix/appcompat/app/floatingactivity/a/j;-><init>(Lmiuix/appcompat/app/floatingactivity/a/k;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->b:Landroid/view/View;

    new-instance p2, Lmiuix/appcompat/app/floatingactivity/a/c;

    invoke-direct {p2, p0}, Lmiuix/appcompat/app/floatingactivity/a/c;-><init>(Lmiuix/appcompat/app/floatingactivity/a/k;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->o()V

    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    sget p2, Ld/b/d;->miuix_appcompat_transparent:I

    invoke-virtual {p1, p2}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    iget-boolean p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->u:Z

    if-nez p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-static {p1}, Ld/h/a/i;->a(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->d:Landroid/view/View;

    new-instance p2, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v0, -0x1000000

    invoke-direct {p2, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->d:Landroid/view/View;

    iget-object p2, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, p2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void
.end method

.method public a(Lmiuix/appcompat/app/floatingactivity/j;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->j:Lmiuix/appcompat/app/floatingactivity/j;

    return-void
.end method

.method public a(Lmiuix/appcompat/app/floatingactivity/k;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->k:Lmiuix/appcompat/app/floatingactivity/k;

    return-void
.end method

.method public a(Z)V
    .locals 1

    iput-boolean p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->q:Z

    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->b:Landroid/view/View;

    iget-boolean v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->q:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public a()Z
    .locals 4

    invoke-static {}, Lmiuix/appcompat/app/floatingactivity/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->n()Z

    move-result v0

    return v0

    :cond_0
    iget-boolean v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->u:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->r()V

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->s:Landroid/os/Handler;

    new-instance v1, Lmiuix/appcompat/app/floatingactivity/a/k$a;

    iget-object v2, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-direct {v1, p0, v2}, Lmiuix/appcompat/app/floatingactivity/a/k$a;-><init>(Lmiuix/appcompat/app/floatingactivity/a/k;Lmiuix/appcompat/app/AppCompatActivity;)V

    const-wide/16 v2, 0x6e

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Lmiuix/appcompat/app/AppCompatActivity;->realFinish()V

    invoke-virtual {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->j()V

    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic a(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    iget-boolean p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->q:Z

    if-eqz p1, :cond_0

    invoke-direct {p0, p2}, Lmiuix/appcompat/app/floatingactivity/a/k;->a(Landroid/view/MotionEvent;)V

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method public b(Landroid/view/View;Z)Landroid/view/ViewGroup;
    .locals 4

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    sget v1, Ld/b/i;->miuix_appcompat_screen_floating_window:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    sget v1, Ld/b/g;->action_bar_overlay_layout:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    sget v2, Ld/b/g;->sliding_drawer_handle:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    instance-of v3, v3, Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    instance-of v3, p1, Landroid/view/ViewGroup;

    if-eqz v3, :cond_1

    move-object v3, p1

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput-object v2, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->i:Landroid/view/ViewGroup$LayoutParams;

    if-nez p2, :cond_2

    iget-object v2, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->i:Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->i:Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x2

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    :goto_0
    iget-object v2, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->i:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v2, v1, Landroid/view/ViewGroup;

    if-eqz v2, :cond_3

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_3
    iget-object v1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Ld/b/e;->miuix_appcompat_floating_window_background_radius:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->o:F

    new-instance v1, Lmiuix/internal/widget/RoundFrameLayout;

    iget-object v2, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-direct {v1, v2}, Lmiuix/internal/widget/RoundFrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->g:Lmiuix/internal/widget/RoundFrameLayout;

    iget-object v1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->g:Lmiuix/internal/widget/RoundFrameLayout;

    iget-object v2, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->i:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->g:Lmiuix/internal/widget/RoundFrameLayout;

    invoke-virtual {v1, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->g:Lmiuix/internal/widget/RoundFrameLayout;

    if-eqz p2, :cond_4

    iget p2, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->o:F

    goto :goto_1

    :cond_4
    const/4 p2, 0x0

    :goto_1
    invoke-virtual {p1, p2}, Lmiuix/internal/widget/RoundFrameLayout;->setRadius(F)V

    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->g:Lmiuix/internal/widget/RoundFrameLayout;

    invoke-direct {p0, p1}, Lmiuix/appcompat/app/floatingactivity/a/k;->a(Lmiuix/internal/widget/RoundFrameLayout;)V

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->A()V

    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->g:Lmiuix/internal/widget/RoundFrameLayout;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->g:Lmiuix/internal/widget/RoundFrameLayout;

    invoke-direct {p0, p1}, Lmiuix/appcompat/app/floatingactivity/a/k;->a(Landroid/view/View;)V

    return-object v0
.end method

.method public b()V
    .locals 2

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->v()V

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->z()V

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lmiuix/appcompat/app/floatingactivity/a/k;->a(ZI)V

    return-void
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->v:Z

    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->g:Lmiuix/internal/widget/RoundFrameLayout;

    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lmiuix/appcompat/app/floatingactivity/a/k;->a(Lmiuix/internal/widget/RoundFrameLayout;)V

    :cond_0
    return-void
.end method

.method public c()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->d:Landroid/view/View;

    return-object v0
.end method

.method public c(Z)V
    .locals 2

    iput-boolean p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->u:Z

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lmiuix/appcompat/app/floatingactivity/n;->a(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lmiuix/view/b;->a(Landroid/app/Activity;Z)V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->g:Lmiuix/internal/widget/RoundFrameLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Ld/b/e;->miuix_appcompat_floating_window_background_radius:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->o:F

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->g:Lmiuix/internal/widget/RoundFrameLayout;

    if-eqz p1, :cond_1

    iget v1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->o:F

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lmiuix/internal/widget/RoundFrameLayout;->setRadius(F)V

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->g:Lmiuix/internal/widget/RoundFrameLayout;

    invoke-direct {p0, v0}, Lmiuix/appcompat/app/floatingactivity/a/k;->a(Lmiuix/internal/widget/RoundFrameLayout;)V

    :cond_2
    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->d:Landroid/view/View;

    if-eqz v0, :cond_4

    if-nez p1, :cond_3

    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->a:Lmiuix/appcompat/app/AppCompatActivity;

    invoke-static {p1}, Ld/h/a/i;->a(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->d:Landroid/view/View;

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v1, -0x1000000

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_3
    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->d:Landroid/view/View;

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_4
    :goto_1
    return-void
.end method

.method public d()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->i:Landroid/view/ViewGroup$LayoutParams;

    return-object v0
.end method

.method public e()V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public executeCloseEnterAnimation()V
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->d:Landroid/view/View;

    invoke-static {v0}, Lmiuix/appcompat/app/floatingactivity/f;->a(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public executeCloseExitAnimation()V
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->d:Landroid/view/View;

    invoke-static {v0}, Lmiuix/appcompat/app/floatingactivity/f;->b(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public executeOpenEnterAnimation()V
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->d:Landroid/view/View;

    invoke-static {v0}, Lmiuix/appcompat/app/floatingactivity/f;->c(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public executeOpenExitAnimation()V
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->d:Landroid/view/View;

    invoke-static {v0}, Lmiuix/appcompat/app/floatingactivity/f;->d(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public f()V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public h()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->u:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmiuix/appcompat/app/floatingactivity/c;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->r()V

    :cond_0
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lmiuix/appcompat/app/floatingactivity/a/k;->a(I)V

    const/4 v0, 0x1

    return v0
.end method

.method public i()V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public j()V
    .locals 0

    return-void
.end method

.method public k()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->k:Lmiuix/appcompat/app/floatingactivity/k;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmiuix/appcompat/app/floatingactivity/k;->c()V

    :cond_0
    return-void
.end method

.method protected l()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/app/floatingactivity/a/k;->u:Z

    return v0
.end method

.method public synthetic m()V
    .locals 1

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->w()V

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/a/k;->p()V

    :cond_0
    return-void
.end method
