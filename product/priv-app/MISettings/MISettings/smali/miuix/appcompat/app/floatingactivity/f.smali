.class public Lmiuix/appcompat/app/floatingactivity/f;
.super Ljava/lang/Object;


# direct methods
.method public static a()Lmiuix/animation/a/a;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiuix/appcompat/app/floatingactivity/f;->a(ILjava/lang/Runnable;)Lmiuix/animation/a/a;

    move-result-object v0

    return-object v0
.end method

.method private static a(I)Lmiuix/animation/a/a;
    .locals 4

    new-instance v0, Lmiuix/animation/a/a;

    invoke-direct {v0}, Lmiuix/animation/a/a;-><init>()V

    const/4 v1, 0x2

    const/4 v2, -0x2

    if-eqz p0, :cond_1

    const/4 v3, 0x1

    if-eq p0, v3, :cond_0

    const/4 p0, 0x0

    invoke-static {p0}, Lmiuix/appcompat/app/floatingactivity/f;->a(I)Lmiuix/animation/a/a;

    move-result-object v0

    goto :goto_0

    :cond_0
    new-array p0, v1, [F

    fill-array-data p0, :array_0

    invoke-static {v2, p0}, Lmiuix/animation/h/c;->c(I[F)Lmiuix/animation/h/c$a;

    move-result-object p0

    invoke-virtual {v0, p0}, Lmiuix/animation/a/a;->a(Lmiuix/animation/h/c$a;)Lmiuix/animation/a/a;

    goto :goto_0

    :cond_1
    new-array p0, v1, [F

    fill-array-data p0, :array_1

    invoke-static {v2, p0}, Lmiuix/animation/h/c;->c(I[F)Lmiuix/animation/h/c$a;

    move-result-object p0

    invoke-virtual {v0, p0}, Lmiuix/animation/a/a;->a(Lmiuix/animation/h/c$a;)Lmiuix/animation/a/a;

    :goto_0
    return-object v0

    :array_0
    .array-data 4
        0x3f59999a    # 0.85f
        0x3e99999a    # 0.3f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3eeb851f    # 0.46f
    .end array-data
.end method

.method public static a(ILjava/lang/Runnable;)Lmiuix/animation/a/a;
    .locals 3

    invoke-static {p0}, Lmiuix/appcompat/app/floatingactivity/f;->a(I)Lmiuix/animation/a/a;

    move-result-object p0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Lmiuix/animation/e/b;

    const/4 v1, 0x0

    new-instance v2, Lmiuix/appcompat/app/floatingactivity/d;

    invoke-direct {v2, p1, p0}, Lmiuix/appcompat/app/floatingactivity/d;-><init>(Ljava/lang/Runnable;Lmiuix/animation/a/a;)V

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lmiuix/animation/a/a;->a([Lmiuix/animation/e/b;)Lmiuix/animation/a/a;

    :cond_0
    return-object p0
.end method

.method public static a(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lmiuix/appcompat/app/floatingactivity/f;->b(Landroid/view/View;Lmiuix/animation/a/a;)V

    return-void
.end method

.method static synthetic a(Landroid/view/View;Lmiuix/animation/a/a;)V
    .locals 0

    invoke-static {p0, p1}, Lmiuix/appcompat/app/floatingactivity/f;->f(Landroid/view/View;Lmiuix/animation/a/a;)V

    return-void
.end method

.method public static b(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lmiuix/appcompat/app/floatingactivity/f;->c(Landroid/view/View;Lmiuix/animation/a/a;)V

    return-void
.end method

.method public static b(Landroid/view/View;Lmiuix/animation/a/a;)V
    .locals 5

    new-instance v0, Lmiuix/animation/b/a;

    invoke-direct {v0}, Lmiuix/animation/b/a;-><init>()V

    sget-object v1, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    const/4 v2, 0x0

    int-to-double v3, v2

    invoke-virtual {v0, v1, v3, v4}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    const/4 v1, 0x1

    new-array v3, v1, [Landroid/view/View;

    aput-object p0, v3, v2

    invoke-static {v3}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object p0

    invoke-interface {p0}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object p0

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    sget-object v4, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    aput-object v4, v3, v2

    const/16 v4, -0xc8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-interface {p0, v3}, Lmiuix/animation/k;->d([Ljava/lang/Object;)Lmiuix/animation/k;

    new-array v1, v1, [Lmiuix/animation/a/a;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    invoke-static {v2, p1}, Lmiuix/appcompat/app/floatingactivity/f;->a(ILjava/lang/Runnable;)Lmiuix/animation/a/a;

    move-result-object p1

    :cond_0
    aput-object p1, v1, v2

    invoke-interface {p0, v0, v1}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    return-void
.end method

.method public static c(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lmiuix/appcompat/app/floatingactivity/f;->d(Landroid/view/View;Lmiuix/animation/a/a;)V

    return-void
.end method

.method public static c(Landroid/view/View;Lmiuix/animation/a/a;)V
    .locals 5

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    new-instance v1, Lmiuix/animation/b/a;

    invoke-direct {v1}, Lmiuix/animation/b/a;-><init>()V

    sget-object v2, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    int-to-double v3, v0

    invoke-virtual {v1, v2, v3, v4}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    const/4 v0, 0x1

    new-array v2, v0, [Landroid/view/View;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v2}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object p0

    invoke-interface {p0}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object p0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    sget-object v4, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    aput-object v4, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v0

    invoke-interface {p0, v2}, Lmiuix/animation/k;->d([Ljava/lang/Object;)Lmiuix/animation/k;

    new-array v0, v0, [Lmiuix/animation/a/a;

    if-nez p1, :cond_0

    invoke-static {}, Lmiuix/appcompat/app/floatingactivity/f;->a()Lmiuix/animation/a/a;

    move-result-object p1

    :cond_0
    aput-object p1, v0, v3

    invoke-interface {p0, v1, v0}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    return-void
.end method

.method public static d(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lmiuix/appcompat/app/floatingactivity/f;->e(Landroid/view/View;Lmiuix/animation/a/a;)V

    return-void
.end method

.method public static d(Landroid/view/View;Lmiuix/animation/a/a;)V
    .locals 1

    invoke-virtual {p0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Lmiuix/appcompat/app/floatingactivity/f;->f(Landroid/view/View;Lmiuix/animation/a/a;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lmiuix/appcompat/app/floatingactivity/e;

    invoke-direct {v0, p0, p1}, Lmiuix/appcompat/app/floatingactivity/e;-><init>(Landroid/view/View;Lmiuix/animation/a/a;)V

    invoke-virtual {p0, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void
.end method

.method public static e(Landroid/view/View;Lmiuix/animation/a/a;)V
    .locals 4

    new-instance v0, Lmiuix/animation/b/a;

    invoke-direct {v0}, Lmiuix/animation/b/a;-><init>()V

    sget-object v1, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    const-wide/high16 v2, -0x3f97000000000000L    # -200.0

    invoke-virtual {v0, v1, v2, v3}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    const/4 v1, 0x1

    new-array v2, v1, [Landroid/view/View;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v2}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object p0

    invoke-interface {p0}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object p0

    new-array v1, v1, [Lmiuix/animation/a/a;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    invoke-static {v3, p1}, Lmiuix/appcompat/app/floatingactivity/f;->a(ILjava/lang/Runnable;)Lmiuix/animation/a/a;

    move-result-object p1

    :cond_0
    aput-object p1, v1, v3

    invoke-interface {p0, v0, v1}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    return-void
.end method

.method private static f(Landroid/view/View;Lmiuix/animation/a/a;)V
    .locals 6

    new-instance v0, Lmiuix/animation/b/a;

    invoke-direct {v0}, Lmiuix/animation/b/a;-><init>()V

    sget-object v1, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lmiuix/animation/b/a;->a(Ljava/lang/Object;D)Lmiuix/animation/b/a;

    const/4 v1, 0x1

    new-array v2, v1, [Landroid/view/View;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v2}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v2

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    sget-object v5, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    aput-object v5, v4, v3

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v4, v1

    invoke-interface {v2, v4}, Lmiuix/animation/k;->d([Ljava/lang/Object;)Lmiuix/animation/k;

    new-array p0, v1, [Lmiuix/animation/a/a;

    if-nez p1, :cond_0

    invoke-static {}, Lmiuix/appcompat/app/floatingactivity/f;->a()Lmiuix/animation/a/a;

    move-result-object p1

    :cond_0
    aput-object p1, p0, v3

    invoke-interface {v2, v0, p0}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    return-void
.end method
