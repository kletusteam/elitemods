.class public Lmiuix/appcompat/app/u;
.super Lmiuix/appcompat/app/j;


# instance fields
.field private e:Lmiuix/androidbasewidget/widget/ProgressBar;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:I

.field private i:Ljava/lang/String;

.field private j:Ljava/text/NumberFormat;

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:Landroid/graphics/drawable/Drawable;

.field private q:Landroid/graphics/drawable/Drawable;

.field private r:Ljava/lang/CharSequence;

.field private s:Z

.field private t:Z

.field private u:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/app/j;-><init>(Landroid/content/Context;I)V

    const/4 p1, 0x0

    iput p1, p0, Lmiuix/appcompat/app/u;->h:I

    invoke-direct {p0}, Lmiuix/appcompat/app/u;->g()V

    return-void
.end method

.method static synthetic a(Lmiuix/appcompat/app/u;)Ljava/lang/CharSequence;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/app/u;->r:Ljava/lang/CharSequence;

    return-object p0
.end method

.method static synthetic b(Lmiuix/appcompat/app/u;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/app/u;->f:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic c(Lmiuix/appcompat/app/u;)Ljava/text/NumberFormat;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/app/u;->j:Ljava/text/NumberFormat;

    return-object p0
.end method

.method static synthetic d(Lmiuix/appcompat/app/u;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/app/u;->g:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic e(Lmiuix/appcompat/app/u;)I
    .locals 0

    iget p0, p0, Lmiuix/appcompat/app/u;->l:I

    return p0
.end method

.method static synthetic f(Lmiuix/appcompat/app/u;)Lmiuix/androidbasewidget/widget/ProgressBar;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/app/u;->e:Lmiuix/androidbasewidget/widget/ProgressBar;

    return-object p0
.end method

.method private g()V
    .locals 2

    const-string v0, "%1d/%2d"

    iput-object v0, p0, Lmiuix/appcompat/app/u;->i:Ljava/lang/String;

    invoke-static {}, Ljava/text/NumberFormat;->getPercentInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/app/u;->j:Ljava/text/NumberFormat;

    iget-object v0, p0, Lmiuix/appcompat/app/u;->j:Ljava/text/NumberFormat;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    return-void
.end method

.method private h()V
    .locals 2

    iget v0, p0, Lmiuix/appcompat/app/u;->h:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/u;->u:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/u;->u:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/u;->e:Lmiuix/androidbasewidget/widget/ProgressBar;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lmiuix/androidbasewidget/widget/ProgressBar;->setIndeterminateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_0
    iput-object p1, p0, Lmiuix/appcompat/app/u;->q:Landroid/graphics/drawable/Drawable;

    :goto_0
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/app/u;->e:Lmiuix/androidbasewidget/widget/ProgressBar;

    if-eqz v0, :cond_1

    iget v0, p0, Lmiuix/appcompat/app/u;->h:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iput-object p1, p0, Lmiuix/appcompat/app/u;->r:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/app/u;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iput-object p1, p0, Lmiuix/appcompat/app/u;->r:Ljava/lang/CharSequence;

    :goto_0
    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/u;->e:Lmiuix/androidbasewidget/widget/ProgressBar;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    goto :goto_0

    :cond_0
    iput-boolean p1, p0, Lmiuix/appcompat/app/u;->s:Z

    :goto_0
    return-void
.end method

.method public b(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/u;->e:Lmiuix/androidbasewidget/widget/ProgressBar;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_0
    iput-object p1, p0, Lmiuix/appcompat/app/u;->p:Landroid/graphics/drawable/Drawable;

    :goto_0
    return-void
.end method

.method public c(I)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/u;->e:Lmiuix/androidbasewidget/widget/ProgressBar;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->incrementProgressBy(I)V

    invoke-direct {p0}, Lmiuix/appcompat/app/u;->h()V

    goto :goto_0

    :cond_0
    iget v0, p0, Lmiuix/appcompat/app/u;->n:I

    add-int/2addr v0, p1

    iput v0, p0, Lmiuix/appcompat/app/u;->n:I

    :goto_0
    return-void
.end method

.method public d(I)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/u;->e:Lmiuix/androidbasewidget/widget/ProgressBar;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->incrementSecondaryProgressBy(I)V

    invoke-direct {p0}, Lmiuix/appcompat/app/u;->h()V

    goto :goto_0

    :cond_0
    iget v0, p0, Lmiuix/appcompat/app/u;->o:I

    add-int/2addr v0, p1

    iput v0, p0, Lmiuix/appcompat/app/u;->o:I

    :goto_0
    return-void
.end method

.method public e(I)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/u;->e:Lmiuix/androidbasewidget/widget/ProgressBar;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setMax(I)V

    invoke-direct {p0}, Lmiuix/appcompat/app/u;->h()V

    goto :goto_0

    :cond_0
    iput p1, p0, Lmiuix/appcompat/app/u;->k:I

    :goto_0
    return-void
.end method

.method public f(I)V
    .locals 0

    iput p1, p0, Lmiuix/appcompat/app/u;->l:I

    iget-boolean p1, p0, Lmiuix/appcompat/app/u;->t:Z

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lmiuix/appcompat/app/u;->h()V

    :cond_0
    return-void
.end method

.method public g(I)V
    .locals 0

    iput p1, p0, Lmiuix/appcompat/app/u;->h:I

    return-void
.end method

.method public h(I)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/u;->e:Lmiuix/androidbasewidget/widget/ProgressBar;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    invoke-direct {p0}, Lmiuix/appcompat/app/u;->h()V

    goto :goto_0

    :cond_0
    iput p1, p0, Lmiuix/appcompat/app/u;->m:I

    :goto_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Ld/b/l;->AlertDialog:[I

    const/4 v3, 0x0

    const v4, 0x101005d

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v2, v4, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    iget v2, p0, Lmiuix/appcompat/app/u;->h:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_0

    new-instance v2, Lmiuix/appcompat/app/t;

    invoke-direct {v2, p0}, Lmiuix/appcompat/app/t;-><init>(Lmiuix/appcompat/app/u;)V

    iput-object v2, p0, Lmiuix/appcompat/app/u;->u:Landroid/os/Handler;

    sget v2, Ld/b/l;->AlertDialog_horizontalProgressLayout:I

    sget v4, Ld/b/i;->miuix_appcompat_alert_dialog_progress:I

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    sget v2, Ld/b/g;->progress_percent:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lmiuix/appcompat/app/u;->g:Landroid/widget/TextView;

    goto :goto_0

    :cond_0
    sget v2, Ld/b/l;->AlertDialog_progressLayout:I

    sget v4, Ld/b/i;->miuix_appcompat_progress_dialog:I

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :goto_0
    const v2, 0x102000d

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lmiuix/androidbasewidget/widget/ProgressBar;

    iput-object v2, p0, Lmiuix/appcompat/app/u;->e:Lmiuix/androidbasewidget/widget/ProgressBar;

    sget v2, Ld/b/g;->message:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lmiuix/appcompat/app/u;->f:Landroid/widget/TextView;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x1c

    if-lt v2, v3, :cond_1

    iget-object v2, p0, Lmiuix/appcompat/app/u;->f:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Ld/b/e;->miuix_appcompat_dialog_message_line_height:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setLineHeight(I)V

    :cond_1
    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/j;->a(Landroid/view/View;)V

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    iget v0, p0, Lmiuix/appcompat/app/u;->k:I

    if-lez v0, :cond_2

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/u;->e(I)V

    :cond_2
    iget v0, p0, Lmiuix/appcompat/app/u;->l:I

    if-lez v0, :cond_3

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/u;->f(I)V

    :cond_3
    iget v0, p0, Lmiuix/appcompat/app/u;->m:I

    if-lez v0, :cond_4

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/u;->h(I)V

    :cond_4
    iget v0, p0, Lmiuix/appcompat/app/u;->n:I

    if-lez v0, :cond_5

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/u;->c(I)V

    :cond_5
    iget v0, p0, Lmiuix/appcompat/app/u;->o:I

    if-lez v0, :cond_6

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/u;->d(I)V

    :cond_6
    iget-object v0, p0, Lmiuix/appcompat/app/u;->p:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_7

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/u;->b(Landroid/graphics/drawable/Drawable;)V

    :cond_7
    iget-object v0, p0, Lmiuix/appcompat/app/u;->q:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_8

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/u;->a(Landroid/graphics/drawable/Drawable;)V

    :cond_8
    iget-object v0, p0, Lmiuix/appcompat/app/u;->r:Ljava/lang/CharSequence;

    if-eqz v0, :cond_9

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/u;->a(Ljava/lang/CharSequence;)V

    :cond_9
    iget-boolean v0, p0, Lmiuix/appcompat/app/u;->s:Z

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/u;->a(Z)V

    invoke-direct {p0}, Lmiuix/appcompat/app/u;->h()V

    invoke-super {p0, p1}, Lmiuix/appcompat/app/j;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/app/j;->onStart()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/app/u;->t:Z

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/app/j;->onStop()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/appcompat/app/u;->t:Z

    return-void
.end method
