.class Lmiuix/internal/widget/f;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiuix/internal/widget/h;->c(Landroid/view/View;Landroid/view/ViewGroup;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lmiuix/internal/widget/h;


# direct methods
.method constructor <init>(Lmiuix/internal/widget/h;)V
    .locals 0

    iput-object p1, p0, Lmiuix/internal/widget/f;->a:Lmiuix/internal/widget/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 0

    invoke-virtual {p1, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    iget-object p1, p0, Lmiuix/internal/widget/f;->a:Lmiuix/internal/widget/h;

    invoke-static {p1}, Lmiuix/internal/widget/h;->c(Lmiuix/internal/widget/h;)Landroid/widget/ListView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lmiuix/internal/widget/f;->a:Lmiuix/internal/widget/h;

    invoke-virtual {p1}, Lmiuix/internal/widget/h;->i()Z

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    iget-object p2, p0, Lmiuix/internal/widget/f;->a:Lmiuix/internal/widget/h;

    iget-object p2, p2, Lmiuix/internal/widget/h;->h:Landroid/view/View;

    check-cast p2, Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {p2, p1}, Lmiuix/springback/view/SpringBackLayout;->setEnabled(Z)V

    return-void
.end method
