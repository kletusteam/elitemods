.class Lmiuix/internal/view/c;
.super Lmiuix/animation/g/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/internal/view/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiuix/animation/g/b<",
        "Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic b:Lmiuix/internal/view/i;


# direct methods
.method constructor <init>(Lmiuix/internal/view/i;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lmiuix/internal/view/c;->b:Lmiuix/internal/view/i;

    invoke-direct {p0, p2}, Lmiuix/animation/g/b;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;)F
    .locals 0

    iget-object p1, p0, Lmiuix/internal/view/c;->b:Lmiuix/internal/view/i;

    invoke-static {p1}, Lmiuix/internal/view/i;->a(Lmiuix/internal/view/i;)Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;

    move-result-object p1

    invoke-virtual {p1}, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->d()F

    move-result p1

    return p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;F)V
    .locals 0

    check-cast p1, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;

    invoke-virtual {p0, p1, p2}, Lmiuix/internal/view/c;->a(Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;F)V

    return-void
.end method

.method public a(Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;F)V
    .locals 0

    iget-object p1, p0, Lmiuix/internal/view/c;->b:Lmiuix/internal/view/i;

    invoke-static {p1}, Lmiuix/internal/view/i;->a(Lmiuix/internal/view/i;)Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;

    move-result-object p1

    invoke-virtual {p1, p2}, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;->b(F)V

    return-void
.end method

.method public bridge synthetic b(Ljava/lang/Object;)F
    .locals 0

    check-cast p1, Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;

    invoke-virtual {p0, p1}, Lmiuix/internal/view/c;->a(Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;)F

    move-result p1

    return p1
.end method
