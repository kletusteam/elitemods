.class public Lmiuix/internal/view/i;
.super Ljava/lang/Object;


# annotations
.annotation build Landroidx/annotation/RequiresApi;
    api = 0x15
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:F

.field private d:Lmiuix/internal/view/CheckWidgetCircleDrawable;

.field private e:Lmiuix/internal/view/CheckWidgetCircleDrawable;

.field private f:Lmiuix/internal/view/CheckWidgetCircleDrawable;

.field private g:Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;

.field private h:Lmiuix/animation/f/j;

.field private i:Lmiuix/animation/f/j;

.field private j:Lmiuix/animation/f/j;

.field private k:Lmiuix/animation/f/j;

.field private l:Lmiuix/animation/f/j;

.field private m:Lmiuix/animation/f/j;

.field private n:Lmiuix/animation/f/j;

.field private o:Lmiuix/animation/f/j;

.field private p:Lmiuix/animation/f/j;

.field private q:Lmiuix/animation/f/j;

.field private r:Lmiuix/animation/f/e$c;

.field private s:Lmiuix/animation/f/e$c;

.field private t:Lmiuix/animation/g/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiuix/animation/g/b<",
            "Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;",
            ">;"
        }
    .end annotation
.end field

.field private u:Lmiuix/animation/g/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiuix/animation/g/b<",
            "Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lmiuix/animation/g/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiuix/animation/g/b<",
            "Lmiuix/internal/view/i;",
            ">;"
        }
    .end annotation
.end field

.field private w:Lmiuix/animation/g/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiuix/animation/g/b<",
            "Lmiuix/internal/view/CheckWidgetCircleDrawable;",
            ">;"
        }
    .end annotation
.end field

.field private x:Z


# direct methods
.method public constructor <init>(Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;ZIIIIIIII)V
    .locals 12

    move-object v0, p0

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Lmiuix/internal/view/i;->c:F

    new-instance v1, Lmiuix/internal/view/a;

    invoke-direct {v1, p0}, Lmiuix/internal/view/a;-><init>(Lmiuix/internal/view/i;)V

    iput-object v1, v0, Lmiuix/internal/view/i;->r:Lmiuix/animation/f/e$c;

    new-instance v1, Lmiuix/internal/view/b;

    invoke-direct {v1, p0}, Lmiuix/internal/view/b;-><init>(Lmiuix/internal/view/i;)V

    iput-object v1, v0, Lmiuix/internal/view/i;->s:Lmiuix/animation/f/e$c;

    new-instance v1, Lmiuix/internal/view/c;

    const-string v2, "Scale"

    invoke-direct {v1, p0, v2}, Lmiuix/internal/view/c;-><init>(Lmiuix/internal/view/i;Ljava/lang/String;)V

    iput-object v1, v0, Lmiuix/internal/view/i;->t:Lmiuix/animation/g/b;

    new-instance v1, Lmiuix/internal/view/d;

    const-string v3, "ContentAlpha"

    invoke-direct {v1, p0, v3}, Lmiuix/internal/view/d;-><init>(Lmiuix/internal/view/i;Ljava/lang/String;)V

    iput-object v1, v0, Lmiuix/internal/view/i;->u:Lmiuix/animation/g/b;

    new-instance v1, Lmiuix/internal/view/e;

    invoke-direct {v1, p0, v2}, Lmiuix/internal/view/e;-><init>(Lmiuix/internal/view/i;Ljava/lang/String;)V

    iput-object v1, v0, Lmiuix/internal/view/i;->v:Lmiuix/animation/g/b;

    new-instance v1, Lmiuix/internal/view/f;

    const-string v2, "Alpha"

    invoke-direct {v1, p0, v2}, Lmiuix/internal/view/f;-><init>(Lmiuix/internal/view/i;Ljava/lang/String;)V

    iput-object v1, v0, Lmiuix/internal/view/i;->w:Lmiuix/animation/g/b;

    const/4 v10, 0x0

    iput-boolean v10, v0, Lmiuix/internal/view/i;->x:Z

    iput v8, v0, Lmiuix/internal/view/i;->a:I

    iput v9, v0, Lmiuix/internal/view/i;->b:I

    move v1, p2

    iput-boolean v1, v0, Lmiuix/internal/view/i;->x:Z

    new-instance v11, Lmiuix/internal/view/CheckWidgetCircleDrawable;

    move-object v1, v11

    move v2, p3

    move/from16 v3, p6

    move/from16 v4, p7

    move/from16 v5, p8

    move/from16 v6, p9

    move/from16 v7, p10

    invoke-direct/range {v1 .. v7}, Lmiuix/internal/view/CheckWidgetCircleDrawable;-><init>(IIIIII)V

    iput-object v11, v0, Lmiuix/internal/view/i;->d:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    iget-object v1, v0, Lmiuix/internal/view/i;->d:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    iget v2, v0, Lmiuix/internal/view/i;->a:I

    invoke-virtual {v1, v2}, Lmiuix/internal/view/CheckWidgetCircleDrawable;->setAlpha(I)V

    new-instance v1, Lmiuix/internal/view/CheckWidgetCircleDrawable;

    move/from16 v2, p4

    invoke-direct {v1, v2, v8, v9}, Lmiuix/internal/view/CheckWidgetCircleDrawable;-><init>(III)V

    iput-object v1, v0, Lmiuix/internal/view/i;->e:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    iget-object v1, v0, Lmiuix/internal/view/i;->e:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    invoke-virtual {v1, v10}, Lmiuix/internal/view/CheckWidgetCircleDrawable;->setAlpha(I)V

    new-instance v1, Lmiuix/internal/view/CheckWidgetCircleDrawable;

    move/from16 v2, p5

    invoke-direct {v1, v2, v8, v9}, Lmiuix/internal/view/CheckWidgetCircleDrawable;-><init>(III)V

    iput-object v1, v0, Lmiuix/internal/view/i;->f:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    iget-object v1, v0, Lmiuix/internal/view/i;->f:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Lmiuix/internal/view/CheckWidgetCircleDrawable;->setAlpha(I)V

    move-object v1, p1

    iput-object v1, v0, Lmiuix/internal/view/i;->g:Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;

    invoke-direct {p0}, Lmiuix/internal/view/i;->b()V

    return-void
.end method

.method static synthetic a(Lmiuix/internal/view/i;)Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;
    .locals 0

    iget-object p0, p0, Lmiuix/internal/view/i;->g:Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;

    return-object p0
.end method

.method static synthetic b(Lmiuix/internal/view/i;)Lmiuix/animation/f/j;
    .locals 0

    iget-object p0, p0, Lmiuix/internal/view/i;->o:Lmiuix/animation/f/j;

    return-object p0
.end method

.method private b()V
    .locals 11

    new-instance v0, Lmiuix/animation/f/j;

    iget-object v1, p0, Lmiuix/internal/view/i;->v:Lmiuix/animation/g/b;

    const v2, 0x3f19999a    # 0.6f

    invoke-direct {v0, p0, v1, v2}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/internal/view/i;->h:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/internal/view/i;->h:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    const v1, 0x4476bd71

    invoke-virtual {v0, v1}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/internal/view/i;->h:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    const v3, 0x3f7d70a4    # 0.99f

    invoke-virtual {v0, v3}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/internal/view/i;->h:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmiuix/animation/f/l;->b(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/internal/view/i;->h:Lmiuix/animation/f/j;

    const v4, 0x3b03126f    # 0.002f

    invoke-virtual {v0, v4}, Lmiuix/animation/f/e;->a(F)Lmiuix/animation/f/e;

    iget-object v0, p0, Lmiuix/internal/view/i;->h:Lmiuix/animation/f/j;

    iget-object v5, p0, Lmiuix/internal/view/i;->s:Lmiuix/animation/f/e$c;

    invoke-virtual {v0, v5}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    new-instance v0, Lmiuix/animation/f/j;

    iget-object v5, p0, Lmiuix/internal/view/i;->v:Lmiuix/animation/g/b;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-direct {v0, p0, v5, v6}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/internal/view/i;->k:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/internal/view/i;->k:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/internal/view/i;->k:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/internal/view/i;->k:Lmiuix/animation/f/j;

    invoke-virtual {v0, v4}, Lmiuix/animation/f/e;->a(F)Lmiuix/animation/f/e;

    iget-object v0, p0, Lmiuix/internal/view/i;->k:Lmiuix/animation/f/j;

    new-instance v5, Lmiuix/internal/view/g;

    invoke-direct {v5, p0}, Lmiuix/internal/view/g;-><init>(Lmiuix/internal/view/i;)V

    invoke-virtual {v0, v5}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    new-instance v0, Lmiuix/animation/f/j;

    iget-object v5, p0, Lmiuix/internal/view/i;->g:Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;

    iget-object v7, p0, Lmiuix/internal/view/i;->u:Lmiuix/animation/g/b;

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-direct {v0, v5, v7, v8}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/internal/view/i;->n:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/internal/view/i;->n:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/internal/view/i;->n:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v3}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/internal/view/i;->n:Lmiuix/animation/f/j;

    const/high16 v5, 0x3b800000    # 0.00390625f

    invoke-virtual {v0, v5}, Lmiuix/animation/f/e;->a(F)Lmiuix/animation/f/e;

    iget-object v0, p0, Lmiuix/internal/view/i;->n:Lmiuix/animation/f/j;

    iget-object v7, p0, Lmiuix/internal/view/i;->r:Lmiuix/animation/f/e$c;

    invoke-virtual {v0, v7}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    new-instance v0, Lmiuix/animation/f/j;

    iget-object v7, p0, Lmiuix/internal/view/i;->e:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    iget-object v8, p0, Lmiuix/internal/view/i;->w:Lmiuix/animation/g/b;

    const v9, 0x3dcccccd    # 0.1f

    invoke-direct {v0, v7, v8, v9}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/internal/view/i;->i:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/internal/view/i;->i:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/internal/view/i;->i:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v3}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/internal/view/i;->i:Lmiuix/animation/f/j;

    invoke-virtual {v0, v5}, Lmiuix/animation/f/e;->a(F)Lmiuix/animation/f/e;

    iget-object v0, p0, Lmiuix/internal/view/i;->i:Lmiuix/animation/f/j;

    iget-object v7, p0, Lmiuix/internal/view/i;->r:Lmiuix/animation/f/e$c;

    invoke-virtual {v0, v7}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    new-instance v0, Lmiuix/animation/f/j;

    iget-object v7, p0, Lmiuix/internal/view/i;->e:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    iget-object v8, p0, Lmiuix/internal/view/i;->w:Lmiuix/animation/g/b;

    const/4 v9, 0x0

    invoke-direct {v0, v7, v8, v9}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/internal/view/i;->j:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/internal/view/i;->j:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/internal/view/i;->j:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v3}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/internal/view/i;->j:Lmiuix/animation/f/j;

    invoke-virtual {v0, v5}, Lmiuix/animation/f/e;->a(F)Lmiuix/animation/f/e;

    iget-object v0, p0, Lmiuix/internal/view/i;->j:Lmiuix/animation/f/j;

    iget-object v7, p0, Lmiuix/internal/view/i;->r:Lmiuix/animation/f/e$c;

    invoke-virtual {v0, v7}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    new-instance v0, Lmiuix/animation/f/j;

    iget-object v7, p0, Lmiuix/internal/view/i;->f:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    iget-object v8, p0, Lmiuix/internal/view/i;->w:Lmiuix/animation/g/b;

    invoke-direct {v0, v7, v8, v6}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/internal/view/i;->l:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/internal/view/i;->l:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/internal/view/i;->l:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    const v7, 0x3f333333    # 0.7f

    invoke-virtual {v0, v7}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/internal/view/i;->l:Lmiuix/animation/f/j;

    invoke-virtual {v0, v5}, Lmiuix/animation/f/e;->a(F)Lmiuix/animation/f/e;

    iget-object v0, p0, Lmiuix/internal/view/i;->l:Lmiuix/animation/f/j;

    iget-object v7, p0, Lmiuix/internal/view/i;->r:Lmiuix/animation/f/e$c;

    invoke-virtual {v0, v7}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    new-instance v0, Lmiuix/animation/f/j;

    iget-object v7, p0, Lmiuix/internal/view/i;->g:Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;

    iget-object v8, p0, Lmiuix/internal/view/i;->u:Lmiuix/animation/g/b;

    invoke-direct {v0, v7, v8, v6}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/internal/view/i;->o:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/internal/view/i;->o:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    const v7, 0x43db51ec

    invoke-virtual {v0, v7}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/internal/view/i;->o:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/internal/view/i;->o:Lmiuix/animation/f/j;

    invoke-virtual {v0, v5}, Lmiuix/animation/f/e;->a(F)Lmiuix/animation/f/e;

    iget-object v0, p0, Lmiuix/internal/view/i;->o:Lmiuix/animation/f/j;

    iget-object v8, p0, Lmiuix/internal/view/i;->r:Lmiuix/animation/f/e$c;

    invoke-virtual {v0, v8}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    new-instance v0, Lmiuix/animation/f/j;

    iget-object v8, p0, Lmiuix/internal/view/i;->f:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    iget-object v10, p0, Lmiuix/internal/view/i;->w:Lmiuix/animation/g/b;

    invoke-direct {v0, v8, v10, v9}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/internal/view/i;->m:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/internal/view/i;->m:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/internal/view/i;->m:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v3}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/internal/view/i;->m:Lmiuix/animation/f/j;

    invoke-virtual {v0, v5}, Lmiuix/animation/f/e;->a(F)Lmiuix/animation/f/e;

    iget-object v0, p0, Lmiuix/internal/view/i;->m:Lmiuix/animation/f/j;

    iget-object v5, p0, Lmiuix/internal/view/i;->r:Lmiuix/animation/f/e$c;

    invoke-virtual {v0, v5}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    new-instance v0, Lmiuix/animation/f/j;

    iget-object v5, p0, Lmiuix/internal/view/i;->g:Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;

    iget-object v8, p0, Lmiuix/internal/view/i;->t:Lmiuix/animation/g/b;

    invoke-direct {v0, v5, v8, v6}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/internal/view/i;->p:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/internal/view/i;->p:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v7}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/internal/view/i;->p:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v2}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/internal/view/i;->p:Lmiuix/animation/f/j;

    invoke-virtual {v0, v4}, Lmiuix/animation/f/e;->a(F)Lmiuix/animation/f/e;

    iget-object v0, p0, Lmiuix/internal/view/i;->p:Lmiuix/animation/f/j;

    iget-object v2, p0, Lmiuix/internal/view/i;->r:Lmiuix/animation/f/e$c;

    invoke-virtual {v0, v2}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    iget-boolean v0, p0, Lmiuix/internal/view/i;->x:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/internal/view/i;->p:Lmiuix/animation/f/j;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v0, v2}, Lmiuix/animation/f/e;->d(F)Lmiuix/animation/f/e;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/internal/view/i;->p:Lmiuix/animation/f/j;

    const/high16 v2, 0x41200000    # 10.0f

    invoke-virtual {v0, v2}, Lmiuix/animation/f/e;->d(F)Lmiuix/animation/f/e;

    :goto_0
    new-instance v0, Lmiuix/animation/f/j;

    iget-object v2, p0, Lmiuix/internal/view/i;->g:Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;

    iget-object v5, p0, Lmiuix/internal/view/i;->t:Lmiuix/animation/g/b;

    const v6, 0x3e99999a    # 0.3f

    invoke-direct {v0, v2, v5, v6}, Lmiuix/animation/f/j;-><init>(Ljava/lang/Object;Lmiuix/animation/g/b;F)V

    iput-object v0, p0, Lmiuix/internal/view/i;->q:Lmiuix/animation/f/j;

    iget-object v0, p0, Lmiuix/internal/view/i;->q:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v1}, Lmiuix/animation/f/l;->c(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/internal/view/i;->q:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0, v3}, Lmiuix/animation/f/l;->a(F)Lmiuix/animation/f/l;

    iget-object v0, p0, Lmiuix/internal/view/i;->q:Lmiuix/animation/f/j;

    invoke-virtual {v0, v4}, Lmiuix/animation/f/e;->a(F)Lmiuix/animation/f/e;

    iget-object v0, p0, Lmiuix/internal/view/i;->q:Lmiuix/animation/f/j;

    iget-object v1, p0, Lmiuix/internal/view/i;->s:Lmiuix/animation/f/e$c;

    invoke-virtual {v0, v1}, Lmiuix/animation/f/e;->a(Lmiuix/animation/f/e$c;)Lmiuix/animation/f/e;

    return-void
.end method

.method static synthetic c(Lmiuix/internal/view/i;)Lmiuix/animation/f/j;
    .locals 0

    iget-object p0, p0, Lmiuix/internal/view/i;->p:Lmiuix/animation/f/j;

    return-object p0
.end method


# virtual methods
.method public a()F
    .locals 1

    iget v0, p0, Lmiuix/internal/view/i;->c:F

    return v0
.end method

.method public a(F)V
    .locals 1

    iget-object v0, p0, Lmiuix/internal/view/i;->d:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    invoke-virtual {v0, p1}, Lmiuix/internal/view/CheckWidgetCircleDrawable;->a(F)V

    iget-object v0, p0, Lmiuix/internal/view/i;->e:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    invoke-virtual {v0, p1}, Lmiuix/internal/view/CheckWidgetCircleDrawable;->a(F)V

    iget-object v0, p0, Lmiuix/internal/view/i;->f:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    invoke-virtual {v0, p1}, Lmiuix/internal/view/CheckWidgetCircleDrawable;->a(F)V

    iput p1, p0, Lmiuix/internal/view/i;->c:F

    return-void
.end method

.method public a(IIII)V
    .locals 1

    iget-object v0, p0, Lmiuix/internal/view/i;->d:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lmiuix/internal/view/i;->e:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lmiuix/internal/view/i;->f:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 1

    iget-object v0, p0, Lmiuix/internal/view/i;->d:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    invoke-virtual {v0, p1}, Lmiuix/internal/view/CheckWidgetCircleDrawable;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lmiuix/internal/view/i;->e:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    invoke-virtual {v0, p1}, Lmiuix/internal/view/CheckWidgetCircleDrawable;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lmiuix/internal/view/i;->f:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    invoke-virtual {v0, p1}, Lmiuix/internal/view/CheckWidgetCircleDrawable;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public a(Landroid/graphics/Rect;)V
    .locals 1

    iget-object v0, p0, Lmiuix/internal/view/i;->d:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lmiuix/internal/view/i;->e:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lmiuix/internal/view/i;->f:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    return-void
.end method

.method public synthetic a(Lmiuix/animation/f/e;FF)V
    .locals 0

    iget-object p1, p0, Lmiuix/internal/view/i;->g:Lmiuix/internal/view/CheckBoxAnimatedStateListDrawable;

    invoke-virtual {p1}, Landroid/graphics/drawable/AnimatedStateListDrawable;->invalidateSelf()V

    return-void
.end method

.method protected a(ZZ)V
    .locals 1

    if-eqz p2, :cond_a

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    if-eq p2, v0, :cond_0

    goto/16 :goto_0

    :cond_0
    iget-object p2, p0, Lmiuix/internal/view/i;->h:Lmiuix/animation/f/j;

    invoke-virtual {p2}, Lmiuix/animation/f/e;->d()Z

    move-result p2

    if-nez p2, :cond_1

    iget-object p2, p0, Lmiuix/internal/view/i;->h:Lmiuix/animation/f/j;

    invoke-virtual {p2}, Lmiuix/animation/f/j;->e()V

    :cond_1
    iget-object p2, p0, Lmiuix/internal/view/i;->n:Lmiuix/animation/f/j;

    invoke-virtual {p2}, Lmiuix/animation/f/e;->d()Z

    move-result p2

    if-nez p2, :cond_2

    iget-object p2, p0, Lmiuix/internal/view/i;->n:Lmiuix/animation/f/j;

    invoke-virtual {p2}, Lmiuix/animation/f/j;->e()V

    :cond_2
    if-nez p1, :cond_3

    iget-object p1, p0, Lmiuix/internal/view/i;->i:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->d()Z

    move-result p1

    if-nez p1, :cond_3

    iget-object p1, p0, Lmiuix/internal/view/i;->i:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/j;->e()V

    :cond_3
    iget-object p1, p0, Lmiuix/internal/view/i;->j:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->d()Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lmiuix/internal/view/i;->j:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->a()V

    :cond_4
    iget-object p1, p0, Lmiuix/internal/view/i;->k:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->d()Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lmiuix/internal/view/i;->k:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->a()V

    :cond_5
    iget-object p1, p0, Lmiuix/internal/view/i;->o:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->d()Z

    move-result p1

    if-eqz p1, :cond_6

    iget-object p1, p0, Lmiuix/internal/view/i;->o:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->a()V

    :cond_6
    iget-object p1, p0, Lmiuix/internal/view/i;->p:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->d()Z

    move-result p1

    if-eqz p1, :cond_7

    iget-object p1, p0, Lmiuix/internal/view/i;->p:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->a()V

    :cond_7
    iget-object p1, p0, Lmiuix/internal/view/i;->q:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->d()Z

    move-result p1

    if-eqz p1, :cond_8

    iget-object p1, p0, Lmiuix/internal/view/i;->q:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->a()V

    :cond_8
    iget-object p1, p0, Lmiuix/internal/view/i;->m:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->d()Z

    move-result p1

    if-eqz p1, :cond_9

    iget-object p1, p0, Lmiuix/internal/view/i;->m:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->a()V

    :cond_9
    iget-object p1, p0, Lmiuix/internal/view/i;->l:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->d()Z

    move-result p1

    if-eqz p1, :cond_a

    iget-object p1, p0, Lmiuix/internal/view/i;->l:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->a()V

    :cond_a
    :goto_0
    return-void
.end method

.method protected b(ZZ)V
    .locals 2

    if-eqz p2, :cond_c

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    if-eq p2, v0, :cond_0

    goto/16 :goto_1

    :cond_0
    iget-object p2, p0, Lmiuix/internal/view/i;->h:Lmiuix/animation/f/j;

    invoke-virtual {p2}, Lmiuix/animation/f/e;->d()Z

    move-result p2

    if-eqz p2, :cond_1

    iget-object p2, p0, Lmiuix/internal/view/i;->h:Lmiuix/animation/f/j;

    invoke-virtual {p2}, Lmiuix/animation/f/e;->a()V

    :cond_1
    iget-object p2, p0, Lmiuix/internal/view/i;->n:Lmiuix/animation/f/j;

    invoke-virtual {p2}, Lmiuix/animation/f/e;->d()Z

    move-result p2

    if-eqz p2, :cond_2

    iget-object p2, p0, Lmiuix/internal/view/i;->n:Lmiuix/animation/f/j;

    invoke-virtual {p2}, Lmiuix/animation/f/e;->a()V

    :cond_2
    iget-object p2, p0, Lmiuix/internal/view/i;->i:Lmiuix/animation/f/j;

    invoke-virtual {p2}, Lmiuix/animation/f/e;->d()Z

    move-result p2

    if-eqz p2, :cond_3

    iget-object p2, p0, Lmiuix/internal/view/i;->i:Lmiuix/animation/f/j;

    invoke-virtual {p2}, Lmiuix/animation/f/e;->a()V

    :cond_3
    iget-object p2, p0, Lmiuix/internal/view/i;->j:Lmiuix/animation/f/j;

    invoke-virtual {p2}, Lmiuix/animation/f/e;->d()Z

    move-result p2

    if-nez p2, :cond_4

    iget-object p2, p0, Lmiuix/internal/view/i;->j:Lmiuix/animation/f/j;

    invoke-virtual {p2}, Lmiuix/animation/f/j;->e()V

    :cond_4
    if-eqz p1, :cond_8

    iget-object p1, p0, Lmiuix/internal/view/i;->m:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->d()Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lmiuix/internal/view/i;->m:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->a()V

    :cond_5
    iget-object p1, p0, Lmiuix/internal/view/i;->l:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->d()Z

    move-result p1

    if-nez p1, :cond_6

    iget-object p1, p0, Lmiuix/internal/view/i;->l:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/j;->e()V

    :cond_6
    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    new-instance p2, Lmiuix/internal/view/h;

    invoke-direct {p2, p0}, Lmiuix/internal/view/h;-><init>(Lmiuix/internal/view/i;)V

    const-wide/16 v0, 0x32

    invoke-virtual {p1, p2, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-boolean p1, p0, Lmiuix/internal/view/i;->x:Z

    if-eqz p1, :cond_7

    iget-object p1, p0, Lmiuix/internal/view/i;->k:Lmiuix/animation/f/j;

    const/high16 p2, 0x41200000    # 10.0f

    invoke-virtual {p1, p2}, Lmiuix/animation/f/e;->d(F)Lmiuix/animation/f/e;

    goto :goto_0

    :cond_7
    iget-object p1, p0, Lmiuix/internal/view/i;->k:Lmiuix/animation/f/j;

    const/high16 p2, 0x40a00000    # 5.0f

    invoke-virtual {p1, p2}, Lmiuix/animation/f/e;->d(F)Lmiuix/animation/f/e;

    goto :goto_0

    :cond_8
    iget-object p1, p0, Lmiuix/internal/view/i;->l:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->d()Z

    move-result p1

    if-eqz p1, :cond_9

    iget-object p1, p0, Lmiuix/internal/view/i;->l:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->a()V

    :cond_9
    iget-object p1, p0, Lmiuix/internal/view/i;->m:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->d()Z

    move-result p1

    if-nez p1, :cond_a

    iget-object p1, p0, Lmiuix/internal/view/i;->m:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/j;->e()V

    :cond_a
    iget-object p1, p0, Lmiuix/internal/view/i;->q:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/e;->d()Z

    move-result p1

    if-nez p1, :cond_b

    iget-object p1, p0, Lmiuix/internal/view/i;->q:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/j;->e()V

    :cond_b
    :goto_0
    iget-object p1, p0, Lmiuix/internal/view/i;->k:Lmiuix/animation/f/j;

    invoke-virtual {p1}, Lmiuix/animation/f/j;->e()V

    return-void

    :cond_c
    :goto_1
    const/high16 p2, 0x437f0000    # 255.0f

    if-eqz p1, :cond_d

    iget-object p1, p0, Lmiuix/internal/view/i;->f:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    iget-object v0, p0, Lmiuix/internal/view/i;->l:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/animation/f/l;->a()F

    move-result v0

    mul-float/2addr v0, p2

    float-to-int p2, v0

    invoke-virtual {p1, p2}, Lmiuix/internal/view/CheckWidgetCircleDrawable;->setAlpha(I)V

    goto :goto_2

    :cond_d
    iget-object p1, p0, Lmiuix/internal/view/i;->f:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    iget-object v0, p0, Lmiuix/internal/view/i;->m:Lmiuix/animation/f/j;

    invoke-virtual {v0}, Lmiuix/animation/f/j;->g()Lmiuix/animation/f/l;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/animation/f/l;->a()F

    move-result v0

    mul-float/2addr v0, p2

    float-to-int p2, v0

    invoke-virtual {p1, p2}, Lmiuix/internal/view/CheckWidgetCircleDrawable;->setAlpha(I)V

    :goto_2
    return-void
.end method

.method protected c(ZZ)V
    .locals 1

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lmiuix/internal/view/i;->f:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    const/16 p2, 0xff

    invoke-virtual {p1, p2}, Lmiuix/internal/view/CheckWidgetCircleDrawable;->setAlpha(I)V

    iget-object p1, p0, Lmiuix/internal/view/i;->e:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    const/16 p2, 0x19

    invoke-virtual {p1, p2}, Lmiuix/internal/view/CheckWidgetCircleDrawable;->setAlpha(I)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lmiuix/internal/view/i;->f:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    invoke-virtual {p1, v0}, Lmiuix/internal/view/CheckWidgetCircleDrawable;->setAlpha(I)V

    iget-object p1, p0, Lmiuix/internal/view/i;->e:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    invoke-virtual {p1, v0}, Lmiuix/internal/view/CheckWidgetCircleDrawable;->setAlpha(I)V

    :goto_0
    iget-object p1, p0, Lmiuix/internal/view/i;->d:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    iget p2, p0, Lmiuix/internal/view/i;->a:I

    invoke-virtual {p1, p2}, Lmiuix/internal/view/CheckWidgetCircleDrawable;->setAlpha(I)V

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lmiuix/internal/view/i;->f:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    invoke-virtual {p1, v0}, Lmiuix/internal/view/CheckWidgetCircleDrawable;->setAlpha(I)V

    iget-object p1, p0, Lmiuix/internal/view/i;->e:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    invoke-virtual {p1, v0}, Lmiuix/internal/view/CheckWidgetCircleDrawable;->setAlpha(I)V

    iget-object p1, p0, Lmiuix/internal/view/i;->d:Lmiuix/internal/view/CheckWidgetCircleDrawable;

    iget p2, p0, Lmiuix/internal/view/i;->b:I

    invoke-virtual {p1, p2}, Lmiuix/internal/view/CheckWidgetCircleDrawable;->setAlpha(I)V

    :goto_1
    return-void
.end method
