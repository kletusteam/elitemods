.class Lmiuix/preference/v;
.super Landroidx/preference/w;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/preference/v$a;
    }
.end annotation


# static fields
.field private static final g:[I

.field private static final h:[I

.field private static final i:[I

.field private static final j:[I

.field private static final k:[I

.field private static final l:[I


# instance fields
.field private A:Landroid/graphics/Paint;

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field private F:I

.field private m:[Lmiuix/preference/v$a;

.field private n:Landroidx/recyclerview/widget/RecyclerView$c;

.field private o:I

.field private p:I

.field private q:I

.field private r:Landroidx/recyclerview/widget/RecyclerView;

.field private s:I

.field private t:I

.field private u:Z

.field private v:I

.field private w:Landroid/view/View;

.field private x:Z

.field private y:Landroid/view/View$OnTouchListener;

.field private z:Landroidx/recyclerview/widget/RecyclerView$j;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v0, 0x5

    new-array v0, v0, [I

    const v1, 0x10100a3

    const/4 v2, 0x0

    aput v1, v0, v2

    const v3, 0x10100a4

    const/4 v4, 0x1

    aput v3, v0, v4

    const v5, 0x10100a5

    const/4 v6, 0x2

    aput v5, v0, v6

    const v6, 0x10100a6

    const/4 v7, 0x3

    aput v6, v0, v7

    sget v7, Lmiuix/preference/w;->state_no_title:I

    const/4 v8, 0x4

    aput v7, v0, v8

    sput-object v0, Lmiuix/preference/v;->g:[I

    sget-object v0, Lmiuix/preference/v;->g:[I

    invoke-static {v0}, Ljava/util/Arrays;->sort([I)V

    new-array v0, v4, [I

    aput v1, v0, v2

    sput-object v0, Lmiuix/preference/v;->h:[I

    new-array v0, v4, [I

    aput v3, v0, v2

    sput-object v0, Lmiuix/preference/v;->i:[I

    new-array v0, v4, [I

    aput v5, v0, v2

    sput-object v0, Lmiuix/preference/v;->j:[I

    new-array v0, v4, [I

    aput v6, v0, v2

    sput-object v0, Lmiuix/preference/v;->k:[I

    new-array v0, v4, [I

    sget v1, Lmiuix/preference/w;->state_no_title:I

    aput v1, v0, v2

    sput-object v0, Lmiuix/preference/v;->l:[I

    return-void
.end method

.method public constructor <init>(Landroidx/preference/PreferenceGroup;)V
    .locals 2

    invoke-direct {p0, p1}, Landroidx/preference/w;-><init>(Landroidx/preference/PreferenceGroup;)V

    new-instance v0, Lmiuix/preference/r;

    invoke-direct {v0, p0}, Lmiuix/preference/r;-><init>(Lmiuix/preference/v;)V

    iput-object v0, p0, Lmiuix/preference/v;->n:Landroidx/recyclerview/widget/RecyclerView$c;

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/preference/v;->s:I

    iput v0, p0, Lmiuix/preference/v;->t:I

    iput-boolean v0, p0, Lmiuix/preference/v;->u:Z

    const/4 v1, -0x1

    iput v1, p0, Lmiuix/preference/v;->v:I

    const/4 v1, 0x0

    iput-object v1, p0, Lmiuix/preference/v;->w:Landroid/view/View;

    iput-boolean v0, p0, Lmiuix/preference/v;->x:Z

    iput-object v1, p0, Lmiuix/preference/v;->y:Landroid/view/View$OnTouchListener;

    iput-object v1, p0, Lmiuix/preference/v;->z:Landroidx/recyclerview/widget/RecyclerView$j;

    invoke-virtual {p0}, Landroidx/preference/w;->getItemCount()I

    move-result v0

    new-array v0, v0, [Lmiuix/preference/v$a;

    iput-object v0, p0, Lmiuix/preference/v;->m:[Lmiuix/preference/v$a;

    invoke-virtual {p1}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p0, p1}, Lmiuix/preference/v;->a(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Lmiuix/preference/v;)I
    .locals 0

    iget p0, p0, Lmiuix/preference/v;->v:I

    return p0
.end method

.method static synthetic a(Lmiuix/preference/v;I)I
    .locals 0

    iput p1, p0, Lmiuix/preference/v;->v:I

    return p1
.end method

.method private a(Landroid/graphics/drawable/Drawable;ZZ)V
    .locals 8

    instance-of v0, p1, Lmiuix/preference/drawable/MaskTaggingDrawable;

    if-eqz v0, :cond_0

    check-cast p1, Lmiuix/preference/drawable/MaskTaggingDrawable;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lmiuix/preference/drawable/MaskTaggingDrawable;->a(Z)V

    iget-object v2, p0, Lmiuix/preference/v;->A:Landroid/graphics/Paint;

    iget v3, p0, Lmiuix/preference/v;->B:I

    iget v4, p0, Lmiuix/preference/v;->C:I

    iget v5, p0, Lmiuix/preference/v;->D:I

    iget v6, p0, Lmiuix/preference/v;->E:I

    iget v7, p0, Lmiuix/preference/v;->F:I

    move-object v1, p1

    invoke-virtual/range {v1 .. v7}, Lmiuix/preference/drawable/MaskTaggingDrawable;->a(Landroid/graphics/Paint;IIIII)V

    iget-object v0, p0, Lmiuix/preference/v;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {v0}, Landroidx/appcompat/widget/Fa;->a(Landroid/view/View;)Z

    move-result v0

    iget-object v1, p0, Lmiuix/preference/v;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p0, v1, v0}, Lmiuix/preference/v;->a(Landroidx/recyclerview/widget/RecyclerView;Z)Landroid/util/Pair;

    move-result-object v1

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v2, v1, v0}, Lmiuix/preference/drawable/MaskTaggingDrawable;->a(IIZ)V

    invoke-virtual {p1, p2, p3}, Lmiuix/preference/drawable/MaskTaggingDrawable;->a(ZZ)V

    :cond_0
    return-void
.end method

.method private a(Landroid/view/View;ZZ)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-direct {p0, p1, p2, p3}, Lmiuix/preference/v;->a(Landroid/graphics/drawable/Drawable;ZZ)V

    :cond_0
    return-void
.end method

.method private a(Landroidx/preference/Preference;I)V
    .locals 5

    if-ltz p2, :cond_1

    iget-object v0, p0, Lmiuix/preference/v;->m:[Lmiuix/preference/v$a;

    array-length v1, v0

    if-ge p2, v1, :cond_1

    aget-object v1, v0, p2

    if-nez v1, :cond_0

    new-instance v1, Lmiuix/preference/v$a;

    invoke-direct {v1, p0}, Lmiuix/preference/v$a;-><init>(Lmiuix/preference/v;)V

    aput-object v1, v0, p2

    :cond_0
    iget-object v0, p0, Lmiuix/preference/v;->m:[Lmiuix/preference/v$a;

    aget-object v0, v0, p2

    iget-object v0, v0, Lmiuix/preference/v$a;->a:[I

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_7

    invoke-virtual {p1}, Landroidx/preference/Preference;->getParent()Landroidx/preference/PreferenceGroup;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-direct {p0, v0}, Lmiuix/preference/v;->c(Landroidx/preference/PreferenceGroup;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    return-void

    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v1, v2, :cond_3

    sget-object v0, Lmiuix/preference/v;->h:[I

    goto :goto_1

    :cond_3
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroidx/preference/Preference;

    invoke-virtual {p1, v1}, Landroidx/preference/Preference;->compareTo(Landroidx/preference/Preference;)I

    move-result v1

    if-nez v1, :cond_4

    sget-object v0, Lmiuix/preference/v;->i:[I

    const/4 v2, 0x2

    goto :goto_1

    :cond_4
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v1, v2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/preference/Preference;

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->compareTo(Landroidx/preference/Preference;)I

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lmiuix/preference/v;->k:[I

    const/4 v2, 0x4

    goto :goto_1

    :cond_5
    sget-object v0, Lmiuix/preference/v;->j:[I

    const/4 v2, 0x3

    :goto_1
    instance-of v1, p1, Landroidx/preference/PreferenceCategory;

    if-eqz v1, :cond_6

    check-cast p1, Landroidx/preference/PreferenceCategory;

    invoke-virtual {p1}, Landroidx/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_6

    sget-object p1, Lmiuix/preference/v;->l:[I

    array-length v1, p1

    array-length v4, v0

    add-int/2addr v1, v4

    new-array v1, v1, [I

    array-length v4, p1

    invoke-static {p1, v3, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sget-object p1, Lmiuix/preference/v;->l:[I

    array-length p1, p1

    array-length v4, v0

    invoke-static {v0, v3, v1, p1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v0, v1

    :cond_6
    iget-object p1, p0, Lmiuix/preference/v;->m:[Lmiuix/preference/v$a;

    aget-object v1, p1, p2

    iput-object v0, v1, Lmiuix/preference/v$a;->a:[I

    aget-object p1, p1, p2

    iput v2, p1, Lmiuix/preference/v$a;->b:I

    :cond_7
    return-void
.end method

.method private a(Landroidx/preference/Preference;Landroidx/preference/B;)V
    .locals 5

    iget-object p2, p2, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-direct {p0, p1}, Lmiuix/preference/v;->e(Landroidx/preference/Preference;)Z

    move-result v0

    const/4 v1, 0x1

    new-array v2, v1, [Landroid/view/View;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v2}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/h;->c()Lmiuix/animation/i;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/j;->a()V

    if-eqz v0, :cond_0

    new-array v2, v1, [Landroid/view/View;

    aput-object p2, v2, v3

    invoke-static {v2}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/j;->a()V

    :cond_0
    invoke-virtual {p1}, Landroidx/preference/Preference;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    new-array v2, v1, [Landroid/view/View;

    aput-object p2, v2, v3

    invoke-static {v2}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/h;->c()Lmiuix/animation/i;

    move-result-object v2

    sget-object v4, Lmiuix/animation/i$a;->a:Lmiuix/animation/i$a;

    invoke-interface {v2, v4}, Lmiuix/animation/i;->a(Lmiuix/animation/i$a;)Lmiuix/animation/i;

    new-array v4, v3, [Lmiuix/animation/a/a;

    invoke-interface {v2, p2, v4}, Lmiuix/animation/i;->b(Landroid/view/View;[Lmiuix/animation/a/a;)V

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroidx/preference/Preference;->isSelectable()Z

    move-result p1

    if-eqz p1, :cond_1

    new-array p1, v1, [Landroid/view/View;

    aput-object p2, p1, v3

    invoke-static {p1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object p1

    const/high16 v0, 0x3f800000    # 1.0f

    new-array v1, v3, [Lmiuix/animation/m$b;

    invoke-interface {p1, v0, v1}, Lmiuix/animation/m;->b(F[Lmiuix/animation/m$b;)Lmiuix/animation/m;

    new-array v0, v3, [Lmiuix/animation/a/a;

    invoke-interface {p1, p2, v0}, Lmiuix/animation/m;->a(Landroid/view/View;[Lmiuix/animation/a/a;)V

    :cond_1
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    const/4 v2, 0x1

    if-nez v1, :cond_0

    move v3, v2

    goto :goto_1

    :cond_0
    move v3, v0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    sub-int/2addr v4, v2

    if-ne v1, v4, :cond_1

    goto :goto_2

    :cond_1
    move v2, v0

    :goto_2
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    invoke-direct {p0, v4, v3, v2}, Lmiuix/preference/v;->a(Landroid/view/View;ZZ)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private a(Lmiuix/preference/RadioButtonPreferenceCategory;)V
    .locals 4

    invoke-virtual {p1}, Landroidx/preference/PreferenceGroup;->e()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceGroup;->a(I)Landroidx/preference/Preference;

    move-result-object v2

    instance-of v3, v2, Lmiuix/preference/RadioSetPreferenceCategory;

    if-eqz v3, :cond_0

    check-cast v2, Lmiuix/preference/RadioSetPreferenceCategory;

    invoke-direct {p0, v2}, Lmiuix/preference/v;->a(Lmiuix/preference/RadioSetPreferenceCategory;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(Lmiuix/preference/RadioSetPreferenceCategory;)V
    .locals 5

    invoke-virtual {p1}, Landroidx/preference/PreferenceGroup;->e()I

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    invoke-virtual {p1, v2}, Landroidx/preference/PreferenceGroup;->a(I)Landroidx/preference/Preference;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, v3}, Landroidx/preference/w;->c(Landroidx/preference/Preference;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    iget-object v4, p0, Lmiuix/preference/v;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0, v1}, Lmiuix/preference/v;->a(Ljava/util/List;)V

    return-void
.end method

.method static synthetic a(Lmiuix/preference/v;[Lmiuix/preference/v$a;)[Lmiuix/preference/v$a;
    .locals 0

    iput-object p1, p0, Lmiuix/preference/v;->m:[Lmiuix/preference/v$a;

    return-object p1
.end method

.method private b(Landroid/view/View;)V
    .locals 3

    sget v0, Lmiuix/preference/z;->preference_highlighted:I

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    new-array v0, v1, [Landroid/view/View;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/h;->e()Lmiuix/animation/f;

    move-result-object v0

    new-array v1, v1, [Lmiuix/animation/a/a;

    const/4 v2, 0x3

    invoke-interface {v0, v2, v1}, Lmiuix/animation/f;->a(I[Lmiuix/animation/a/a;)V

    iput-object p1, p0, Lmiuix/preference/v;->w:Landroid/view/View;

    return-void
.end method

.method private c(Landroidx/preference/PreferenceGroup;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/preference/PreferenceGroup;",
            ")",
            "Ljava/util/List<",
            "Landroidx/preference/Preference;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Landroidx/preference/PreferenceGroup;->e()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceGroup;->a(I)Landroidx/preference/Preference;

    move-result-object v2

    invoke-virtual {v2}, Landroidx/preference/Preference;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private e(Landroidx/preference/Preference;)Z
    .locals 2

    instance-of v0, p1, Lmiuix/preference/DropDownPreference;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    instance-of v0, p1, Lmiuix/preference/j;

    if-eqz v0, :cond_1

    check-cast p1, Lmiuix/preference/j;

    invoke-interface {p1}, Lmiuix/preference/j;->a()Z

    move-result p1

    if-nez p1, :cond_1

    return v1

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method private f(Landroidx/preference/Preference;)Z
    .locals 1

    invoke-virtual {p1}, Landroidx/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Landroidx/preference/Preference;->getFragment()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Landroidx/preference/Preference;->getOnPreferenceClickListener()Landroidx/preference/Preference$c;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Landroidx/preference/TwoStatePreference;

    if-eqz v0, :cond_2

    :cond_0
    instance-of p1, p1, Landroidx/preference/DialogPreference;

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private g(Landroidx/preference/Preference;)V
    .locals 1

    if-eqz p1, :cond_2

    iget-object v0, p0, Lmiuix/preference/v;->r:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v0, :cond_2

    instance-of v0, p1, Lmiuix/preference/RadioButtonPreferenceCategory;

    if-eqz v0, :cond_0

    check-cast p1, Lmiuix/preference/RadioButtonPreferenceCategory;

    invoke-direct {p0, p1}, Lmiuix/preference/v;->a(Lmiuix/preference/RadioButtonPreferenceCategory;)V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lmiuix/preference/RadioSetPreferenceCategory;

    if-eqz v0, :cond_1

    check-cast p1, Lmiuix/preference/RadioSetPreferenceCategory;

    invoke-direct {p0, p1}, Lmiuix/preference/v;->a(Lmiuix/preference/RadioSetPreferenceCategory;)V

    goto :goto_0

    :cond_1
    instance-of p1, p1, Lmiuix/preference/RadioButtonPreference;

    :cond_2
    :goto_0
    return-void
.end method


# virtual methods
.method a(I)I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lmiuix/preference/v;->m:[Lmiuix/preference/v$a;

    goto/32 :goto_2

    nop

    :goto_1
    return p1

    :goto_2
    aget-object p1, v0, p1

    goto/32 :goto_3

    nop

    :goto_3
    iget p1, p1, Lmiuix/preference/v$a;->b:I

    goto/32 :goto_1

    nop
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView;Z)Landroid/util/Pair;
    .locals 1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getScrollBarSize()I

    move-result v0

    if-eqz p2, :cond_0

    mul-int/lit8 v0, v0, 0x3

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWidth()I

    move-result p1

    move p2, v0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWidth()I

    move-result p1

    mul-int/lit8 v0, v0, 0x3

    sub-int/2addr p1, v0

    :goto_0
    new-instance v0, Landroid/util/Pair;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-direct {v0, p2, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method protected a(IIZ)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lmiuix/preference/v;->a(IIZZ)V

    return-void
.end method

.method protected a(IIZZ)V
    .locals 0

    if-nez p4, :cond_0

    invoke-static {p1}, Ld/b/b/b/f;->a(I)Z

    move-result p4

    if-eqz p4, :cond_1

    iget p4, p0, Lmiuix/preference/v;->s:I

    if-eq p4, p1, :cond_1

    :cond_0
    iput p1, p0, Lmiuix/preference/v;->s:I

    iput p2, p0, Lmiuix/preference/v;->t:I

    iput-boolean p3, p0, Lmiuix/preference/v;->u:Z

    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyDataSetChanged()V

    :cond_1
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    sget v0, Lmiuix/preference/w;->preferenceRadioSetChildExtraPaddingStart:I

    invoke-static {p1, v0}, Ld/h/a/d;->c(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lmiuix/preference/v;->o:I

    sget v0, Lmiuix/preference/w;->checkablePreferenceItemColorFilterChecked:I

    invoke-static {p1, v0}, Ld/h/a/d;->b(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lmiuix/preference/v;->p:I

    sget v0, Lmiuix/preference/w;->checkablePreferenceItemColorFilterNormal:I

    invoke-static {p1, v0}, Ld/h/a/d;->b(Landroid/content/Context;I)I

    move-result p1

    iput p1, p0, Lmiuix/preference/v;->q:I

    return-void
.end method

.method public a(Landroid/graphics/Paint;IIIII)V
    .locals 0

    iput-object p1, p0, Lmiuix/preference/v;->A:Landroid/graphics/Paint;

    iput p2, p0, Lmiuix/preference/v;->B:I

    iput p3, p0, Lmiuix/preference/v;->C:I

    iput p4, p0, Lmiuix/preference/v;->D:I

    iput p5, p0, Lmiuix/preference/v;->E:I

    iput p6, p0, Lmiuix/preference/v;->F:I

    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p0}, Lmiuix/preference/v;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sget v1, Lmiuix/preference/z;->preference_highlighted:I

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/h;->e()Lmiuix/animation/f;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/f;->c()V

    sget v0, Lmiuix/preference/z;->preference_highlighted:I

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget-object v0, p0, Lmiuix/preference/v;->w:Landroid/view/View;

    if-ne v0, p1, :cond_1

    const/4 p1, 0x0

    iput-object p1, p0, Lmiuix/preference/v;->w:Landroid/view/View;

    :cond_1
    const/4 p1, -0x1

    iput p1, p0, Lmiuix/preference/v;->v:I

    :cond_2
    :goto_0
    return-void
.end method

.method public a(Landroidx/preference/B;)V
    .locals 0
    .param p1    # Landroidx/preference/B;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->onViewDetachedFromWindow(Landroidx/recyclerview/widget/RecyclerView$t;)V

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {p0, p1}, Lmiuix/preference/v;->a(Landroid/view/View;)V

    return-void
.end method

.method public a(Landroidx/preference/B;I)V
    .locals 8
    .param p1    # Landroidx/preference/B;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1, p2}, Landroidx/preference/w;->a(Landroidx/preference/B;I)V

    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiuix/view/b;->a(Landroid/view/View;Z)V

    invoke-virtual {p0, p2}, Landroidx/preference/w;->getItem(I)Landroidx/preference/Preference;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lmiuix/preference/v;->a(Landroidx/preference/Preference;I)V

    iget-object v2, p0, Lmiuix/preference/v;->m:[Lmiuix/preference/v$a;

    aget-object v2, v2, p2

    iget-object v2, v2, Lmiuix/preference/v$a;->a:[I

    iget-object v3, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    instance-of v4, v3, Landroid/graphics/drawable/LevelListDrawable;

    if-eqz v4, :cond_2

    instance-of v4, v0, Lmiuix/preference/RadioButtonPreference;

    if-nez v4, :cond_0

    instance-of v4, v0, Landroidx/preference/PreferenceCategory;

    if-eqz v4, :cond_2

    :cond_0
    iget-boolean v4, p0, Lmiuix/preference/v;->u:Z

    if-eqz v4, :cond_1

    iget v4, p0, Lmiuix/preference/v;->s:I

    goto :goto_0

    :cond_1
    move v4, v1

    :goto_0
    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    new-instance v4, Lmiuix/preference/drawable/MaskTaggingDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-direct {v4, v3}, Lmiuix/preference/drawable/MaskTaggingDrawable;-><init>(Landroid/graphics/drawable/Drawable;)V

    iget-object v3, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    move-object v3, v4

    :cond_2
    nop

    instance-of v4, v3, Landroid/graphics/drawable/StateListDrawable;

    if-eqz v4, :cond_3

    move-object v4, v3

    check-cast v4, Landroid/graphics/drawable/StateListDrawable;

    sget-object v5, Lmiuix/preference/v;->g:[I

    invoke-static {v4, v5}, Lmiuix/internal/graphics/drawable/TaggingDrawable;->a(Landroid/graphics/drawable/StateListDrawable;[I)Z

    move-result v4

    if-eqz v4, :cond_3

    new-instance v4, Lmiuix/preference/drawable/MaskTaggingDrawable;

    invoke-direct {v4, v3}, Lmiuix/preference/drawable/MaskTaggingDrawable;-><init>(Landroid/graphics/drawable/Drawable;)V

    iget-object v3, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    move-object v3, v4

    :cond_3
    nop

    instance-of v4, v3, Lmiuix/preference/drawable/MaskTaggingDrawable;

    if-eqz v4, :cond_11

    check-cast v3, Lmiuix/preference/drawable/MaskTaggingDrawable;

    if-eqz v2, :cond_4

    invoke-virtual {v3, v2}, Lmiuix/internal/graphics/drawable/TaggingDrawable;->a([I)Z

    :cond_4
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v3, v2}, Landroidx/appcompat/graphics/drawable/DrawableWrapper;->getPadding(Landroid/graphics/Rect;)Z

    move-result v4

    if-eqz v4, :cond_10

    iget v4, v2, Landroid/graphics/Rect;->left:I

    iget v5, v2, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Lmiuix/preference/v;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {v6}, Landroidx/appcompat/widget/Fa;->a(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_5

    move v6, v4

    goto :goto_1

    :cond_5
    move v6, v5

    :goto_1
    iput v6, v2, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Lmiuix/preference/v;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {v6}, Landroidx/appcompat/widget/Fa;->a(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_6

    move v4, v5

    :cond_6
    iput v4, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0}, Landroidx/preference/Preference;->getParent()Landroidx/preference/PreferenceGroup;

    move-result-object v4

    instance-of v4, v4, Lmiuix/preference/RadioSetPreferenceCategory;

    if-eqz v4, :cond_c

    iget-object v4, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    instance-of v5, v4, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v5, :cond_7

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    goto :goto_2

    :cond_7
    new-instance v5, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v5, v4}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v4, v5

    :goto_2
    iget-object v5, p0, Lmiuix/preference/v;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getScrollBarSize()I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    iget-object v5, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {v5, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0}, Landroidx/preference/Preference;->getParent()Landroidx/preference/PreferenceGroup;

    move-result-object v4

    check-cast v4, Lmiuix/preference/RadioSetPreferenceCategory;

    invoke-virtual {v3, v1}, Lmiuix/preference/drawable/MaskTaggingDrawable;->a(Z)V

    invoke-virtual {v4}, Lmiuix/preference/RadioSetPreferenceCategory;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_8

    iget v4, p0, Lmiuix/preference/v;->p:I

    goto :goto_3

    :cond_8
    iget v4, p0, Lmiuix/preference/v;->q:I

    :goto_3
    sget-object v5, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v4, v5}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    iget-object v4, p0, Lmiuix/preference/v;->r:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v4, :cond_d

    instance-of v5, v0, Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getScrollBarSize()I

    move-result v4

    iget-object v6, p0, Lmiuix/preference/v;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {v6}, Landroidx/appcompat/widget/Fa;->a(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_a

    iget v6, v2, Landroid/graphics/Rect;->right:I

    if-eqz v5, :cond_9

    move v5, v1

    goto :goto_4

    :cond_9
    iget v5, p0, Lmiuix/preference/v;->o:I

    :goto_4
    add-int/2addr v6, v5

    iput v6, v2, Landroid/graphics/Rect;->right:I

    iget v5, v2, Landroid/graphics/Rect;->left:I

    mul-int/lit8 v4, v4, 0x3

    sub-int/2addr v5, v4

    iput v5, v2, Landroid/graphics/Rect;->left:I

    goto :goto_6

    :cond_a
    iget v6, v2, Landroid/graphics/Rect;->left:I

    if-eqz v5, :cond_b

    move v5, v1

    goto :goto_5

    :cond_b
    iget v5, p0, Lmiuix/preference/v;->o:I

    :goto_5
    add-int/2addr v6, v5

    iput v6, v2, Landroid/graphics/Rect;->left:I

    iget v5, v2, Landroid/graphics/Rect;->right:I

    mul-int/lit8 v4, v4, 0x3

    sub-int/2addr v5, v4

    iput v5, v2, Landroid/graphics/Rect;->right:I

    goto :goto_6

    :cond_c
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroidx/appcompat/graphics/drawable/DrawableWrapper;->setColorFilter(Landroid/graphics/ColorFilter;)V

    :cond_d
    :goto_6
    iget v4, v2, Landroid/graphics/Rect;->left:I

    iget-boolean v5, p0, Lmiuix/preference/v;->u:Z

    if-eqz v5, :cond_e

    iget v5, p0, Lmiuix/preference/v;->t:I

    goto :goto_7

    :cond_e
    move v5, v1

    :goto_7
    add-int/2addr v4, v5

    iget v5, v2, Landroid/graphics/Rect;->right:I

    iget-boolean v6, p0, Lmiuix/preference/v;->u:Z

    if-eqz v6, :cond_f

    iget v6, p0, Lmiuix/preference/v;->t:I

    goto :goto_8

    :cond_f
    move v6, v1

    :goto_8
    add-int/2addr v5, v6

    iget-object v6, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    iget v7, v2, Landroid/graphics/Rect;->top:I

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v6, v4, v7, v5, v2}, Landroid/view/View;->setPadding(IIII)V

    :cond_10
    instance-of v2, v0, Lmiuix/preference/RadioButtonPreference;

    if-eqz v2, :cond_11

    move-object v2, v0

    check-cast v2, Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {v2}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_11

    const/4 v2, 0x1

    new-array v2, v2, [I

    const v4, 0x10100a0

    aput v4, v2, v1

    invoke-virtual {v3, v2}, Lmiuix/internal/graphics/drawable/TaggingDrawable;->a([I)Z

    :cond_11
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    sget v3, Lmiuix/preference/z;->arrow_right:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_13

    invoke-direct {p0, v0}, Lmiuix/preference/v;->f(Landroidx/preference/Preference;)Z

    move-result v3

    if-eqz v3, :cond_12

    goto :goto_9

    :cond_12
    const/16 v1, 0x8

    :goto_9
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_13
    invoke-direct {p0, v0, p1}, Lmiuix/preference/v;->a(Landroidx/preference/Preference;Landroidx/preference/B;)V

    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    const v1, 0x1020016

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Ld/b/b/b/e;->a(Landroid/widget/TextView;)V

    invoke-virtual {p0, p1, p2}, Lmiuix/preference/v;->b(Landroidx/preference/B;I)V

    return-void
.end method

.method public a(Landroidx/recyclerview/widget/RecyclerView;Ljava/lang/String;)V
    .locals 4

    invoke-virtual {p0}, Lmiuix/preference/v;->b()Z

    move-result v0

    if-nez v0, :cond_7

    if-eqz p1, :cond_7

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p0, p2}, Landroidx/preference/w;->a(Ljava/lang/String;)I

    move-result p2

    if-gez p2, :cond_1

    return-void

    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, Lmiuix/preference/v;->y:Landroid/view/View$OnTouchListener;

    if-nez v1, :cond_2

    new-instance v1, Lmiuix/preference/s;

    invoke-direct {v1, p0}, Lmiuix/preference/s;-><init>(Lmiuix/preference/v;)V

    iput-object v1, p0, Lmiuix/preference/v;->y:Landroid/view/View$OnTouchListener;

    :cond_2
    iget-object v1, p0, Lmiuix/preference/v;->z:Landroidx/recyclerview/widget/RecyclerView$j;

    if-nez v1, :cond_3

    new-instance v1, Lmiuix/preference/t;

    invoke-direct {v1, p0}, Lmiuix/preference/t;-><init>(Lmiuix/preference/v;)V

    iput-object v1, p0, Lmiuix/preference/v;->z:Landroidx/recyclerview/widget/RecyclerView$j;

    :cond_3
    iget-object v1, p0, Lmiuix/preference/v;->y:Landroid/view/View$OnTouchListener;

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v1, p0, Lmiuix/preference/v;->z:Landroidx/recyclerview/widget/RecyclerView$j;

    invoke-virtual {p1, v1}, Landroidx/recyclerview/widget/RecyclerView;->addOnItemTouchListener(Landroidx/recyclerview/widget/RecyclerView$j;)V

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$g;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroidx/recyclerview/widget/RecyclerView$g;->d(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x1

    if-eqz v1, :cond_5

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v1, v3}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    if-ge v3, v1, :cond_4

    goto :goto_0

    :cond_4
    move v2, v0

    :cond_5
    :goto_0
    if-nez v2, :cond_6

    iput p2, p0, Lmiuix/preference/v;->v:I

    iget p1, p0, Lmiuix/preference/v;->v:I

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->notifyItemChanged(I)V

    goto :goto_1

    :cond_6
    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->smoothScrollToPosition(I)V

    new-instance v0, Lmiuix/preference/u;

    invoke-direct {v0, p0, p2}, Lmiuix/preference/u;-><init>(Lmiuix/preference/v;I)V

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$k;)V

    :cond_7
    :goto_1
    return-void
.end method

.method public b(Landroidx/preference/B;)V
    .locals 0
    .param p1    # Landroidx/preference/B;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->onViewRecycled(Landroidx/recyclerview/widget/RecyclerView$t;)V

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    invoke-virtual {p0, p1}, Lmiuix/preference/v;->a(Landroid/view/View;)V

    return-void
.end method

.method public b(Landroidx/preference/B;I)V
    .locals 1

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    iget v0, p0, Lmiuix/preference/v;->v:I

    if-ne p2, v0, :cond_2

    iget-boolean p2, p0, Lmiuix/preference/v;->x:Z

    if-nez p2, :cond_1

    sget-object p2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sget v0, Lmiuix/preference/z;->preference_highlighted:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    return-void

    :cond_0
    invoke-direct {p0, p1}, Lmiuix/preference/v;->b(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    iput-boolean p1, p0, Lmiuix/preference/v;->x:Z

    goto :goto_0

    :cond_2
    sget-object p2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sget v0, Lmiuix/preference/z;->preference_highlighted:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_3

    invoke-virtual {p0, p1}, Lmiuix/preference/v;->a(Landroid/view/View;)V

    :cond_3
    :goto_0
    return-void
.end method

.method public b(Landroidx/preference/Preference;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroidx/preference/Preference;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lmiuix/preference/v;->g(Landroidx/preference/Preference;)V

    :cond_0
    invoke-super {p0, p1}, Landroidx/preference/w;->b(Landroidx/preference/Preference;)V

    return-void
.end method

.method public b()Z
    .locals 2

    iget v0, p0, Lmiuix/preference/v;->v:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public c()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/preference/v;->x:Z

    iget-object v0, p0, Lmiuix/preference/v;->w:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lmiuix/preference/v;->a(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/preference/v;->x:Z

    :cond_0
    iget-object v0, p0, Lmiuix/preference/v;->r:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lmiuix/preference/v;->z:Landroidx/recyclerview/widget/RecyclerView$j;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->removeOnItemTouchListener(Landroidx/recyclerview/widget/RecyclerView$j;)V

    iget-object v0, p0, Lmiuix/preference/v;->r:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lmiuix/preference/v;->r:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iput-object v1, p0, Lmiuix/preference/v;->z:Landroidx/recyclerview/widget/RecyclerView$j;

    iput-object v1, p0, Lmiuix/preference/v;->y:Landroid/view/View$OnTouchListener;

    :cond_1
    return-void
.end method

.method public d(Landroidx/preference/Preference;)V
    .locals 2

    invoke-super {p0, p1}, Landroidx/preference/w;->d(Landroidx/preference/Preference;)V

    invoke-virtual {p1}, Landroidx/preference/Preference;->getDependency()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p1}, Landroidx/preference/Preference;->getPreferenceManager()Landroidx/preference/y;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroidx/preference/y;->a(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_2

    instance-of v1, p1, Landroidx/preference/PreferenceCategory;

    if-eqz v1, :cond_1

    instance-of v1, v0, Landroidx/preference/TwoStatePreference;

    if-eqz v1, :cond_0

    check-cast v0, Landroidx/preference/TwoStatePreference;

    invoke-virtual {v0}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroidx/preference/Preference;->isEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroidx/preference/Preference;->isEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    :cond_2
    :goto_0
    return-void
.end method

.method public onAttachedToRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 1
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->onAttachedToRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V

    iget-object v0, p0, Lmiuix/preference/v;->n:Landroidx/recyclerview/widget/RecyclerView$c;

    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$a;->registerAdapterDataObserver(Landroidx/recyclerview/widget/RecyclerView$c;)V

    iput-object p1, p0, Lmiuix/preference/v;->r:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$t;I)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    check-cast p1, Landroidx/preference/B;

    invoke-virtual {p0, p1, p2}, Lmiuix/preference/v;->a(Landroidx/preference/B;I)V

    return-void
.end method

.method public onDetachedFromRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->onDetachedFromRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V

    iget-object p1, p0, Lmiuix/preference/v;->n:Landroidx/recyclerview/widget/RecyclerView$c;

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$a;->unregisterAdapterDataObserver(Landroidx/recyclerview/widget/RecyclerView$c;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lmiuix/preference/v;->r:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method

.method public bridge synthetic onViewDetachedFromWindow(Landroidx/recyclerview/widget/RecyclerView$t;)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    check-cast p1, Landroidx/preference/B;

    invoke-virtual {p0, p1}, Lmiuix/preference/v;->a(Landroidx/preference/B;)V

    return-void
.end method

.method public bridge synthetic onViewRecycled(Landroidx/recyclerview/widget/RecyclerView$t;)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$t;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    check-cast p1, Landroidx/preference/B;

    invoke-virtual {p0, p1}, Lmiuix/preference/v;->b(Landroidx/preference/B;)V

    return-void
.end method
