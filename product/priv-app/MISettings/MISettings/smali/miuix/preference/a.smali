.class Lmiuix/preference/a;
.super Landroidx/appcompat/app/k$a;


# instance fields
.field private c:Lmiuix/appcompat/app/j$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILmiuix/appcompat/app/j$a;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroidx/appcompat/app/k$a;-><init>(Landroid/content/Context;I)V

    iput-object p3, p0, Lmiuix/preference/a;->c:Lmiuix/appcompat/app/j$a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lmiuix/appcompat/app/j$a;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lmiuix/preference/a;-><init>(Landroid/content/Context;ILmiuix/appcompat/app/j$a;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface$OnKeyListener;)Landroidx/appcompat/app/k$a;
    .locals 1

    iget-object v0, p0, Lmiuix/preference/a;->c:Lmiuix/appcompat/app/j$a;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/j$a;->a(Landroid/content/DialogInterface$OnKeyListener;)Lmiuix/appcompat/app/j$a;

    return-object p0
.end method

.method public a(Landroid/graphics/drawable/Drawable;)Landroidx/appcompat/app/k$a;
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lmiuix/preference/a;->c:Lmiuix/appcompat/app/j$a;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/j$a;->a(Landroid/graphics/drawable/Drawable;)Lmiuix/appcompat/app/j$a;

    return-object p0
.end method

.method public a(Landroid/view/View;)Landroidx/appcompat/app/k$a;
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lmiuix/preference/a;->c:Lmiuix/appcompat/app/j$a;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/j$a;->a(Landroid/view/View;)Lmiuix/appcompat/app/j$a;

    return-object p0
.end method

.method public a(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/k$a;
    .locals 1

    iget-object v0, p0, Lmiuix/preference/a;->c:Lmiuix/appcompat/app/j$a;

    invoke-virtual {v0, p1, p2, p3}, Lmiuix/appcompat/app/j$a;->a(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/j$a;

    return-object p0
.end method

.method public a(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/k$a;
    .locals 1

    iget-object v0, p0, Lmiuix/preference/a;->c:Lmiuix/appcompat/app/j$a;

    invoke-virtual {v0, p1, p2}, Lmiuix/appcompat/app/j$a;->a(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/j$a;

    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)Landroidx/appcompat/app/k$a;
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lmiuix/preference/a;->c:Lmiuix/appcompat/app/j$a;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/j$a;->a(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/j$a;

    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/k$a;
    .locals 1

    iget-object v0, p0, Lmiuix/preference/a;->c:Lmiuix/appcompat/app/j$a;

    invoke-virtual {v0, p1, p2}, Lmiuix/appcompat/app/j$a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/j$a;

    return-object p0
.end method

.method public a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/k$a;
    .locals 1

    iget-object v0, p0, Lmiuix/preference/a;->c:Lmiuix/appcompat/app/j$a;

    invoke-virtual {v0, p1, p2, p3}, Lmiuix/appcompat/app/j$a;->a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/j$a;

    return-object p0
.end method

.method public a([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroidx/appcompat/app/k$a;
    .locals 1

    iget-object v0, p0, Lmiuix/preference/a;->c:Lmiuix/appcompat/app/j$a;

    invoke-virtual {v0, p1, p2, p3}, Lmiuix/appcompat/app/j$a;->a([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Lmiuix/appcompat/app/j$a;

    return-object p0
.end method

.method public b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/k$a;
    .locals 1

    iget-object v0, p0, Lmiuix/preference/a;->c:Lmiuix/appcompat/app/j$a;

    invoke-virtual {v0, p1, p2}, Lmiuix/appcompat/app/j$a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/j$a;

    return-object p0
.end method

.method public setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/k$a;
    .locals 1

    iget-object v0, p0, Lmiuix/preference/a;->c:Lmiuix/appcompat/app/j$a;

    invoke-virtual {v0, p1, p2}, Lmiuix/appcompat/app/j$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/j$a;

    return-object p0
.end method

.method public setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/k$a;
    .locals 1

    iget-object v0, p0, Lmiuix/preference/a;->c:Lmiuix/appcompat/app/j$a;

    invoke-virtual {v0, p1, p2}, Lmiuix/appcompat/app/j$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/j$a;

    return-object p0
.end method

.method public setTitle(Ljava/lang/CharSequence;)Landroidx/appcompat/app/k$a;
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lmiuix/preference/a;->c:Lmiuix/appcompat/app/j$a;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/j$a;->b(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/j$a;

    return-object p0
.end method

.method public setView(Landroid/view/View;)Landroidx/appcompat/app/k$a;
    .locals 1

    iget-object v0, p0, Lmiuix/preference/a;->c:Lmiuix/appcompat/app/j$a;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/j$a;->b(Landroid/view/View;)Lmiuix/appcompat/app/j$a;

    return-object p0
.end method
