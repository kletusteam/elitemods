.class Lmiuix/preference/g;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiuix/preference/DropDownPreference;->onBindViewHolder(Landroidx/preference/B;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lmiuix/preference/DropDownPreference;


# direct methods
.method constructor <init>(Lmiuix/preference/DropDownPreference;)V
    .locals 0

    iput-object p1, p0, Lmiuix/preference/g;->a:Lmiuix/preference/DropDownPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    if-eq v0, v1, :cond_1

    const/4 p2, 0x3

    if-eq v0, p2, :cond_0

    goto :goto_0

    :cond_0
    new-array p2, v1, [Landroid/view/View;

    aput-object p1, p2, v2

    invoke-static {p2}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object p1

    new-array p2, v2, [Lmiuix/animation/a/a;

    invoke-interface {p1, p2}, Lmiuix/animation/m;->g([Lmiuix/animation/a/a;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result p2

    iget-object v2, p0, Lmiuix/preference/g;->a:Lmiuix/preference/DropDownPreference;

    invoke-static {v2}, Lmiuix/preference/DropDownPreference;->e(Lmiuix/preference/DropDownPreference;)Lmiuix/appcompat/widget/Spinner;

    move-result-object v2

    invoke-virtual {v2, p1}, Lmiuix/appcompat/widget/Spinner;->setFenceXFromView(Landroid/view/View;)V

    iget-object p1, p0, Lmiuix/preference/g;->a:Lmiuix/preference/DropDownPreference;

    invoke-static {p1}, Lmiuix/preference/DropDownPreference;->e(Lmiuix/preference/DropDownPreference;)Lmiuix/appcompat/widget/Spinner;

    move-result-object p1

    invoke-virtual {p1, v0, p2}, Lmiuix/appcompat/widget/Spinner;->a(FF)Z

    goto :goto_0

    :cond_2
    new-array p2, v1, [Landroid/view/View;

    aput-object p1, p2, v2

    invoke-static {p2}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object p1

    const/high16 p2, 0x3f800000    # 1.0f

    new-array v0, v2, [Lmiuix/animation/m$b;

    invoke-interface {p1, p2, v0}, Lmiuix/animation/m;->b(F[Lmiuix/animation/m$b;)Lmiuix/animation/m;

    new-array p2, v2, [Lmiuix/animation/a/a;

    invoke-interface {p1, p2}, Lmiuix/animation/m;->d([Lmiuix/animation/a/a;)V

    :goto_0
    return v1
.end method
