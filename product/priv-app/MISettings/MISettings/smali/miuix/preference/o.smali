.class Lmiuix/preference/o;
.super Ljava/lang/Object;


# instance fields
.field private a:Lmiuix/preference/k;

.field private b:Landroidx/preference/PreferenceDialogFragmentCompat;


# direct methods
.method public constructor <init>(Lmiuix/preference/k;Landroidx/preference/PreferenceDialogFragmentCompat;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiuix/preference/o;->a:Lmiuix/preference/k;

    iput-object p2, p0, Lmiuix/preference/o;->b:Landroidx/preference/PreferenceDialogFragmentCompat;

    return-void
.end method

.method private a(Landroid/app/Dialog;)V
    .locals 1

    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Landroid/view/Window;->setSoftInputMode(I)V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    iget-object p1, p0, Lmiuix/preference/o;->b:Landroidx/preference/PreferenceDialogFragmentCompat;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lmiuix/preference/o;->b:Landroidx/preference/PreferenceDialogFragmentCompat;

    invoke-virtual {v0}, Landroidx/preference/PreferenceDialogFragmentCompat;->j()Landroidx/preference/DialogPreference;

    move-result-object v0

    new-instance v1, Lmiuix/appcompat/app/j$a;

    invoke-direct {v1, p1}, Lmiuix/appcompat/app/j$a;-><init>(Landroid/content/Context;)V

    new-instance v2, Lmiuix/preference/a;

    invoke-direct {v2, p1, v1}, Lmiuix/preference/a;-><init>(Landroid/content/Context;Lmiuix/appcompat/app/j$a;)V

    invoke-virtual {v0}, Landroidx/preference/DialogPreference;->f()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmiuix/preference/a;->setTitle(Ljava/lang/CharSequence;)Landroidx/appcompat/app/k$a;

    invoke-virtual {v0}, Landroidx/preference/DialogPreference;->b()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmiuix/preference/a;->a(Landroid/graphics/drawable/Drawable;)Landroidx/appcompat/app/k$a;

    invoke-virtual {v0}, Landroidx/preference/DialogPreference;->h()Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v4, p0, Lmiuix/preference/o;->b:Landroidx/preference/PreferenceDialogFragmentCompat;

    invoke-virtual {v2, v3, v4}, Lmiuix/preference/a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/k$a;

    invoke-virtual {v0}, Landroidx/preference/DialogPreference;->g()Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v4, p0, Lmiuix/preference/o;->b:Landroidx/preference/PreferenceDialogFragmentCompat;

    invoke-virtual {v2, v3, v4}, Lmiuix/preference/a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/k$a;

    iget-object v3, p0, Lmiuix/preference/o;->a:Lmiuix/preference/k;

    invoke-interface {v3, p1}, Lmiuix/preference/k;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmiuix/preference/o;->a:Lmiuix/preference/k;

    invoke-interface {v0, p1}, Lmiuix/preference/k;->a(Landroid/view/View;)V

    invoke-virtual {v2, p1}, Lmiuix/preference/a;->setView(Landroid/view/View;)Landroidx/appcompat/app/k$a;

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroidx/preference/DialogPreference;->e()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v2, p1}, Lmiuix/preference/a;->a(Ljava/lang/CharSequence;)Landroidx/appcompat/app/k$a;

    :goto_0
    iget-object p1, p0, Lmiuix/preference/o;->a:Lmiuix/preference/k;

    invoke-interface {p1, v1}, Lmiuix/preference/k;->a(Lmiuix/appcompat/app/j$a;)V

    invoke-virtual {v1}, Lmiuix/appcompat/app/j$a;->a()Lmiuix/appcompat/app/j;

    move-result-object p1

    iget-object v0, p0, Lmiuix/preference/o;->a:Lmiuix/preference/k;

    invoke-interface {v0}, Lmiuix/preference/k;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lmiuix/preference/o;->a(Landroid/app/Dialog;)V

    :cond_1
    return-object p1
.end method
