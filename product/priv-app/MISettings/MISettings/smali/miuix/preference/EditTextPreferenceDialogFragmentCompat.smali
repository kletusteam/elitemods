.class public Lmiuix/preference/EditTextPreferenceDialogFragmentCompat;
.super Landroidx/preference/EditTextPreferenceDialogFragmentCompat;


# instance fields
.field private A:Lmiuix/preference/o;

.field private B:Lmiuix/preference/k;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroidx/preference/EditTextPreferenceDialogFragmentCompat;-><init>()V

    new-instance v0, Lmiuix/preference/i;

    invoke-direct {v0, p0}, Lmiuix/preference/i;-><init>(Lmiuix/preference/EditTextPreferenceDialogFragmentCompat;)V

    iput-object v0, p0, Lmiuix/preference/EditTextPreferenceDialogFragmentCompat;->B:Lmiuix/preference/k;

    new-instance v0, Lmiuix/preference/o;

    iget-object v1, p0, Lmiuix/preference/EditTextPreferenceDialogFragmentCompat;->B:Lmiuix/preference/k;

    invoke-direct {v0, v1, p0}, Lmiuix/preference/o;-><init>(Lmiuix/preference/k;Landroidx/preference/PreferenceDialogFragmentCompat;)V

    iput-object v0, p0, Lmiuix/preference/EditTextPreferenceDialogFragmentCompat;->A:Lmiuix/preference/o;

    return-void
.end method

.method static synthetic a(Lmiuix/preference/EditTextPreferenceDialogFragmentCompat;Landroid/content/Context;)Landroid/view/View;
    .locals 0

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceDialogFragmentCompat;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a(Lmiuix/preference/EditTextPreferenceDialogFragmentCompat;Landroid/view/View;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroidx/preference/EditTextPreferenceDialogFragmentCompat;->a(Landroid/view/View;)V

    return-void
.end method

.method public static b(Ljava/lang/String;)Lmiuix/preference/EditTextPreferenceDialogFragmentCompat;
    .locals 3

    new-instance v0, Lmiuix/preference/EditTextPreferenceDialogFragmentCompat;

    invoke-direct {v0}, Lmiuix/preference/EditTextPreferenceDialogFragmentCompat;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(I)V

    const-string v2, "key"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Lmiuix/preference/EditTextPreferenceDialogFragmentCompat;->A:Lmiuix/preference/o;

    invoke-virtual {v0, p1}, Lmiuix/preference/o;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method

.method protected final a(Landroidx/appcompat/app/k$a;)V
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "using miuix builder instead"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method protected a(Lmiuix/appcompat/app/j$a;)V
    .locals 2

    new-instance v0, Lmiuix/preference/a;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lmiuix/preference/a;-><init>(Landroid/content/Context;Lmiuix/appcompat/app/j$a;)V

    invoke-super {p0, v0}, Landroidx/preference/PreferenceDialogFragmentCompat;->a(Landroidx/appcompat/app/k$a;)V

    return-void
.end method
