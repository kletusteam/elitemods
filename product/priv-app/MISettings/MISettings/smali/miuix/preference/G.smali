.class Lmiuix/preference/G;
.super Ljava/lang/Object;

# interfaces
.implements Lmiuix/pickerwidget/widget/DateTimePicker$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiuix/preference/StretchablePickerPreference;->a(Lmiuix/pickerwidget/widget/DateTimePicker;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lmiuix/preference/StretchablePickerPreference;


# direct methods
.method constructor <init>(Lmiuix/preference/StretchablePickerPreference;)V
    .locals 0

    iput-object p1, p0, Lmiuix/preference/G;->a:Lmiuix/preference/StretchablePickerPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lmiuix/pickerwidget/widget/DateTimePicker;J)V
    .locals 1

    iget-object p1, p0, Lmiuix/preference/G;->a:Lmiuix/preference/StretchablePickerPreference;

    invoke-static {p1}, Lmiuix/preference/StretchablePickerPreference;->a(Lmiuix/preference/StretchablePickerPreference;)Ld/l/a/a;

    move-result-object p1

    invoke-virtual {p1, p2, p3}, Ld/l/a/a;->a(J)Ld/l/a/a;

    iget-object p1, p0, Lmiuix/preference/G;->a:Lmiuix/preference/StretchablePickerPreference;

    invoke-static {p1}, Lmiuix/preference/StretchablePickerPreference;->b(Lmiuix/preference/StretchablePickerPreference;)Z

    move-result v0

    invoke-static {p1, v0, p2, p3}, Lmiuix/preference/StretchablePickerPreference;->a(Lmiuix/preference/StretchablePickerPreference;ZJ)V

    iget-object p1, p0, Lmiuix/preference/G;->a:Lmiuix/preference/StretchablePickerPreference;

    invoke-static {p1, p2, p3}, Lmiuix/preference/StretchablePickerPreference;->a(Lmiuix/preference/StretchablePickerPreference;J)J

    iget-object p1, p0, Lmiuix/preference/G;->a:Lmiuix/preference/StretchablePickerPreference;

    invoke-static {p1}, Lmiuix/preference/StretchablePickerPreference;->d(Lmiuix/preference/StretchablePickerPreference;)Lmiuix/preference/StretchablePickerPreference$a;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lmiuix/preference/G;->a:Lmiuix/preference/StretchablePickerPreference;

    invoke-static {p1}, Lmiuix/preference/StretchablePickerPreference;->d(Lmiuix/preference/StretchablePickerPreference;)Lmiuix/preference/StretchablePickerPreference$a;

    move-result-object p1

    iget-object p2, p0, Lmiuix/preference/G;->a:Lmiuix/preference/StretchablePickerPreference;

    invoke-static {p2}, Lmiuix/preference/StretchablePickerPreference;->c(Lmiuix/preference/StretchablePickerPreference;)J

    move-result-wide p2

    invoke-interface {p1, p2, p3}, Lmiuix/preference/StretchablePickerPreference$a;->a(J)J

    :cond_0
    iget-object p1, p0, Lmiuix/preference/G;->a:Lmiuix/preference/StretchablePickerPreference;

    invoke-static {p1}, Lmiuix/preference/StretchablePickerPreference;->e(Lmiuix/preference/StretchablePickerPreference;)V

    return-void
.end method
