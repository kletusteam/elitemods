.class public Lmiuix/springback/view/c;
.super Ljava/lang/Object;


# instance fields
.field private a:J

.field private b:J

.field private c:D

.field private d:D

.field private e:Lmiuix/springback/view/b;

.field private f:D

.field private g:D

.field private h:D

.field private i:D

.field private j:D

.field private k:D

.field private l:D

.field private m:D

.field private n:I

.field private o:Z

.field private p:Z

.field private q:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/springback/view/c;->o:Z

    return-void
.end method


# virtual methods
.method public a(FFFFFIZ)V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/springback/view/c;->o:Z

    iput-boolean v0, p0, Lmiuix/springback/view/c;->p:Z

    float-to-double v0, p1

    iput-wide v0, p0, Lmiuix/springback/view/c;->g:D

    iput-wide v0, p0, Lmiuix/springback/view/c;->h:D

    float-to-double p1, p2

    iput-wide p1, p0, Lmiuix/springback/view/c;->f:D

    float-to-double p1, p3

    iput-wide p1, p0, Lmiuix/springback/view/c;->j:D

    iput-wide p1, p0, Lmiuix/springback/view/c;->k:D

    iget-wide p1, p0, Lmiuix/springback/view/c;->j:D

    double-to-int p1, p1

    int-to-double p1, p1

    iput-wide p1, p0, Lmiuix/springback/view/c;->d:D

    float-to-double p1, p4

    iput-wide p1, p0, Lmiuix/springback/view/c;->i:D

    float-to-double p1, p5

    iput-wide p1, p0, Lmiuix/springback/view/c;->l:D

    iput-wide p1, p0, Lmiuix/springback/view/c;->m:D

    iget-wide p1, p0, Lmiuix/springback/view/c;->m:D

    invoke-static {p1, p2}, Ljava/lang/Math;->abs(D)D

    move-result-wide p1

    const-wide p3, 0x40b3880000000000L    # 5000.0

    cmpg-double p1, p1, p3

    const/high16 p2, 0x3f800000    # 1.0f

    if-lez p1, :cond_1

    if-eqz p7, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Lmiuix/springback/view/b;

    const p3, 0x3f0ccccd    # 0.55f

    invoke-direct {p1, p2, p3}, Lmiuix/springback/view/b;-><init>(FF)V

    iput-object p1, p0, Lmiuix/springback/view/c;->e:Lmiuix/springback/view/b;

    goto :goto_1

    :cond_1
    :goto_0
    new-instance p1, Lmiuix/springback/view/b;

    const p3, 0x3ecccccd    # 0.4f

    invoke-direct {p1, p2, p3}, Lmiuix/springback/view/b;-><init>(FF)V

    iput-object p1, p0, Lmiuix/springback/view/c;->e:Lmiuix/springback/view/b;

    :goto_1
    iput p6, p0, Lmiuix/springback/view/c;->n:I

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lmiuix/springback/view/c;->a:J

    return-void
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lmiuix/springback/view/c;->q:I

    return-void
.end method

.method public a()Z
    .locals 14

    iget-object v0, p0, Lmiuix/springback/view/c;->e:Lmiuix/springback/view/b;

    const/4 v1, 0x0

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lmiuix/springback/view/c;->o:Z

    if-eqz v0, :cond_0

    goto/16 :goto_2

    :cond_0
    iget v0, p0, Lmiuix/springback/view/c;->q:I

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    iget v3, p0, Lmiuix/springback/view/c;->n:I

    if-ne v3, v2, :cond_1

    int-to-double v3, v0

    iput-wide v3, p0, Lmiuix/springback/view/c;->c:D

    int-to-double v3, v0

    iput-wide v3, p0, Lmiuix/springback/view/c;->g:D

    goto :goto_0

    :cond_1
    int-to-double v3, v0

    iput-wide v3, p0, Lmiuix/springback/view/c;->d:D

    int-to-double v3, v0

    iput-wide v3, p0, Lmiuix/springback/view/c;->j:D

    :goto_0
    iput v1, p0, Lmiuix/springback/view/c;->q:I

    return v2

    :cond_2
    iget-boolean v0, p0, Lmiuix/springback/view/c;->p:Z

    if-eqz v0, :cond_3

    iput-boolean v2, p0, Lmiuix/springback/view/c;->o:Z

    return v2

    :cond_3
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lmiuix/springback/view/c;->b:J

    iget-wide v0, p0, Lmiuix/springback/view/c;->b:J

    iget-wide v3, p0, Lmiuix/springback/view/c;->a:J

    sub-long/2addr v0, v3

    long-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    const v1, 0x3c83126f    # 0.016f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const/4 v3, 0x0

    cmpl-float v3, v0, v3

    if-nez v3, :cond_4

    move v0, v1

    :cond_4
    iget-wide v3, p0, Lmiuix/springback/view/c;->b:J

    iput-wide v3, p0, Lmiuix/springback/view/c;->a:J

    iget v1, p0, Lmiuix/springback/view/c;->n:I

    const/4 v3, 0x2

    if-ne v1, v3, :cond_6

    iget-object v4, p0, Lmiuix/springback/view/c;->e:Lmiuix/springback/view/b;

    iget-wide v5, p0, Lmiuix/springback/view/c;->m:D

    iget-wide v8, p0, Lmiuix/springback/view/c;->i:D

    iget-wide v10, p0, Lmiuix/springback/view/c;->j:D

    move v7, v0

    invoke-virtual/range {v4 .. v11}, Lmiuix/springback/view/b;->a(DFDD)D

    move-result-wide v3

    iget-wide v5, p0, Lmiuix/springback/view/c;->j:D

    float-to-double v0, v0

    mul-double/2addr v0, v3

    add-double/2addr v5, v0

    iput-wide v5, p0, Lmiuix/springback/view/c;->d:D

    iput-wide v3, p0, Lmiuix/springback/view/c;->m:D

    iget-wide v8, p0, Lmiuix/springback/view/c;->d:D

    iget-wide v10, p0, Lmiuix/springback/view/c;->k:D

    iget-wide v12, p0, Lmiuix/springback/view/c;->i:D

    move-object v7, p0

    invoke-virtual/range {v7 .. v13}, Lmiuix/springback/view/c;->a(DDD)Z

    move-result v0

    if-eqz v0, :cond_5

    iput-boolean v2, p0, Lmiuix/springback/view/c;->p:Z

    iget-wide v0, p0, Lmiuix/springback/view/c;->i:D

    iput-wide v0, p0, Lmiuix/springback/view/c;->d:D

    goto :goto_1

    :cond_5
    iget-wide v0, p0, Lmiuix/springback/view/c;->d:D

    iput-wide v0, p0, Lmiuix/springback/view/c;->j:D

    goto :goto_1

    :cond_6
    iget-object v4, p0, Lmiuix/springback/view/c;->e:Lmiuix/springback/view/b;

    iget-wide v5, p0, Lmiuix/springback/view/c;->m:D

    iget-wide v8, p0, Lmiuix/springback/view/c;->f:D

    iget-wide v10, p0, Lmiuix/springback/view/c;->g:D

    move v7, v0

    invoke-virtual/range {v4 .. v11}, Lmiuix/springback/view/b;->a(DFDD)D

    move-result-wide v3

    iget-wide v5, p0, Lmiuix/springback/view/c;->g:D

    float-to-double v0, v0

    mul-double/2addr v0, v3

    add-double/2addr v5, v0

    iput-wide v5, p0, Lmiuix/springback/view/c;->c:D

    iput-wide v3, p0, Lmiuix/springback/view/c;->m:D

    iget-wide v8, p0, Lmiuix/springback/view/c;->c:D

    iget-wide v10, p0, Lmiuix/springback/view/c;->h:D

    iget-wide v12, p0, Lmiuix/springback/view/c;->f:D

    move-object v7, p0

    invoke-virtual/range {v7 .. v13}, Lmiuix/springback/view/c;->a(DDD)Z

    move-result v0

    if-eqz v0, :cond_7

    iput-boolean v2, p0, Lmiuix/springback/view/c;->p:Z

    iget-wide v0, p0, Lmiuix/springback/view/c;->f:D

    iput-wide v0, p0, Lmiuix/springback/view/c;->c:D

    goto :goto_1

    :cond_7
    iget-wide v0, p0, Lmiuix/springback/view/c;->c:D

    iput-wide v0, p0, Lmiuix/springback/view/c;->g:D

    :goto_1
    return v2

    :cond_8
    :goto_2
    return v1
.end method

.method public a(DDD)Z
    .locals 4

    cmpg-double v0, p3, p5

    const/4 v1, 0x1

    if-gez v0, :cond_0

    cmpl-double v0, p1, p5

    if-lez v0, :cond_0

    return v1

    :cond_0
    cmpl-double p3, p3, p5

    if-lez p3, :cond_1

    cmpg-double p4, p1, p5

    if-gez p4, :cond_1

    return v1

    :cond_1
    if-nez p3, :cond_2

    iget-wide p3, p0, Lmiuix/springback/view/c;->l:D

    invoke-static {p3, p4}, Ljava/lang/Math;->signum(D)D

    move-result-wide p3

    invoke-static {p1, p2}, Ljava/lang/Math;->signum(D)D

    move-result-wide v2

    cmpl-double p3, p3, v2

    if-eqz p3, :cond_2

    return v1

    :cond_2
    sub-double/2addr p1, p5

    invoke-static {p1, p2}, Ljava/lang/Math;->abs(D)D

    move-result-wide p1

    const-wide/high16 p3, 0x3ff0000000000000L    # 1.0

    cmpg-double p1, p1, p3

    if-gez p1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/springback/view/c;->o:Z

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/springback/view/c;->q:I

    return-void
.end method

.method public final c()I
    .locals 2

    iget-wide v0, p0, Lmiuix/springback/view/c;->c:D

    double-to-int v0, v0

    return v0
.end method

.method public final d()I
    .locals 2

    iget-wide v0, p0, Lmiuix/springback/view/c;->d:D

    double-to-int v0, v0

    return v0
.end method

.method public final e()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/springback/view/c;->o:Z

    return v0
.end method
