.class public Lmiuix/springback/view/a;
.super Ljava/lang/Object;


# instance fields
.field private a:I

.field b:F

.field c:F

.field d:I

.field e:I

.field f:I

.field private g:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lmiuix/springback/view/a;->d:I

    iput-object p1, p0, Lmiuix/springback/view/a;->g:Landroid/view/ViewGroup;

    iput p2, p0, Lmiuix/springback/view/a;->f:I

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result p1

    iput p1, p0, Lmiuix/springback/view/a;->a:I

    return-void
.end method


# virtual methods
.method a(Landroid/view/MotionEvent;)V
    .locals 5

    goto/32 :goto_e

    nop

    :goto_0
    cmpl-float v0, v0, v4

    goto/32 :goto_17

    nop

    :goto_1
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    goto/32 :goto_3

    nop

    :goto_2
    if-ltz v0, :cond_0

    goto/32 :goto_3c

    :cond_0
    goto/32 :goto_3b

    nop

    :goto_3
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result p1

    goto/32 :goto_34

    nop

    :goto_4
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    goto/32 :goto_2

    nop

    :goto_5
    iget-object p1, p0, Lmiuix/springback/view/a;->g:Landroid/view/ViewGroup;

    goto/32 :goto_26

    nop

    :goto_6
    iput v1, p0, Lmiuix/springback/view/a;->e:I

    :goto_7
    goto/32 :goto_16

    nop

    :goto_8
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    goto/32 :goto_14

    nop

    :goto_9
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    goto/32 :goto_d

    nop

    :goto_a
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    goto/32 :goto_1e

    nop

    :goto_b
    return-void

    :goto_c
    goto/32 :goto_4

    nop

    :goto_d
    iget v4, p0, Lmiuix/springback/view/a;->a:I

    goto/32 :goto_1d

    nop

    :goto_e
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    goto/32 :goto_13

    nop

    :goto_f
    iput v0, p0, Lmiuix/springback/view/a;->d:I

    goto/32 :goto_1a

    nop

    :goto_10
    const/4 p1, 0x3

    goto/32 :goto_3a

    nop

    :goto_11
    iput p1, p0, Lmiuix/springback/view/a;->c:F

    goto/32 :goto_6

    nop

    :goto_12
    if-nez v0, :cond_1

    goto/32 :goto_2d

    :cond_1
    goto/32 :goto_25

    nop

    :goto_13
    const/4 v1, 0x0

    goto/32 :goto_12

    nop

    :goto_14
    if-ltz v0, :cond_2

    goto/32 :goto_2a

    :cond_2
    goto/32 :goto_29

    nop

    :goto_15
    sub-float/2addr p1, v0

    goto/32 :goto_27

    nop

    :goto_16
    return-void

    :goto_17
    if-gtz v0, :cond_3

    goto/32 :goto_7

    :cond_3
    :goto_18
    goto/32 :goto_a

    nop

    :goto_19
    const/4 v1, -0x1

    goto/32 :goto_1b

    nop

    :goto_1a
    iget v0, p0, Lmiuix/springback/view/a;->d:I

    goto/32 :goto_8

    nop

    :goto_1b
    if-eq v0, v1, :cond_4

    goto/32 :goto_c

    :cond_4
    goto/32 :goto_b

    nop

    :goto_1c
    iget v0, p0, Lmiuix/springback/view/a;->d:I

    goto/32 :goto_19

    nop

    :goto_1d
    int-to-float v4, v4

    goto/32 :goto_0

    nop

    :goto_1e
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    goto/32 :goto_31

    nop

    :goto_1f
    if-ne v0, v2, :cond_5

    goto/32 :goto_3f

    :cond_5
    goto/32 :goto_36

    nop

    :goto_20
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result p1

    goto/32 :goto_11

    nop

    :goto_21
    iput v2, p0, Lmiuix/springback/view/a;->e:I

    goto/32 :goto_3e

    nop

    :goto_22
    goto :goto_38

    :goto_23
    goto/32 :goto_37

    nop

    :goto_24
    if-gtz p1, :cond_6

    goto/32 :goto_23

    :cond_6
    goto/32 :goto_22

    nop

    :goto_25
    const/4 v2, 0x1

    goto/32 :goto_1f

    nop

    :goto_26
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    goto/32 :goto_2c

    nop

    :goto_27
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    goto/32 :goto_39

    nop

    :goto_28
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    goto/32 :goto_f

    nop

    :goto_29
    return-void

    :goto_2a
    goto/32 :goto_35

    nop

    :goto_2b
    if-ne v0, v3, :cond_7

    goto/32 :goto_30

    :cond_7
    goto/32 :goto_10

    nop

    :goto_2c
    goto/16 :goto_7

    :goto_2d
    goto/32 :goto_28

    nop

    :goto_2e
    iput v2, p0, Lmiuix/springback/view/a;->b:F

    goto/32 :goto_20

    nop

    :goto_2f
    goto/16 :goto_7

    :goto_30
    goto/32 :goto_1c

    nop

    :goto_31
    cmpl-float p1, p1, v0

    goto/32 :goto_24

    nop

    :goto_32
    cmpl-float v0, v0, v4

    goto/32 :goto_3d

    nop

    :goto_33
    iput v1, p0, Lmiuix/springback/view/a;->e:I

    goto/32 :goto_5

    nop

    :goto_34
    iget v0, p0, Lmiuix/springback/view/a;->b:F

    goto/32 :goto_40

    nop

    :goto_35
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    goto/32 :goto_2e

    nop

    :goto_36
    const/4 v3, 0x2

    goto/32 :goto_2b

    nop

    :goto_37
    move v2, v3

    :goto_38
    goto/32 :goto_21

    nop

    :goto_39
    iget v4, p0, Lmiuix/springback/view/a;->a:I

    goto/32 :goto_41

    nop

    :goto_3a
    if-ne v0, p1, :cond_8

    goto/32 :goto_3f

    :cond_8
    goto/32 :goto_2f

    nop

    :goto_3b
    return-void

    :goto_3c
    goto/32 :goto_1

    nop

    :goto_3d
    if-lez v0, :cond_9

    goto/32 :goto_18

    :cond_9
    goto/32 :goto_9

    nop

    :goto_3e
    goto/16 :goto_7

    :goto_3f
    goto/32 :goto_33

    nop

    :goto_40
    sub-float/2addr v1, v0

    goto/32 :goto_42

    nop

    :goto_41
    int-to-float v4, v4

    goto/32 :goto_32

    nop

    :goto_42
    iget v0, p0, Lmiuix/springback/view/a;->c:F

    goto/32 :goto_15

    nop
.end method

.method public b(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v1

    if-ltz v1, :cond_0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result p1

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    iget-object v3, p0, Lmiuix/springback/view/a;->g:Landroid/view/ViewGroup;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getLocationInWindow([I)V

    aget v0, v1, v0

    const/4 v3, 0x1

    aget v1, v1, v3

    iget-object v3, p0, Lmiuix/springback/view/a;->g:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, Lmiuix/springback/view/a;->g:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    add-int/2addr v4, v0

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v0, v1, v4, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    float-to-int p1, p1

    float-to-int v0, v2

    invoke-virtual {v5, p1, v0}, Landroid/graphics/Rect;->contains(II)Z

    move-result p1

    return p1

    :cond_0
    return v0

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method
