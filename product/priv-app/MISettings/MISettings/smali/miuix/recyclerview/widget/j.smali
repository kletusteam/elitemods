.class public Lmiuix/recyclerview/widget/j;
.super Lmiuix/recyclerview/widget/c;


# static fields
.field public static v:Landroid/view/View$OnAttachStateChangeListener;

.field public static w:Lmiuix/animation/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lmiuix/recyclerview/widget/d;

    invoke-direct {v0}, Lmiuix/recyclerview/widget/d;-><init>()V

    sput-object v0, Lmiuix/recyclerview/widget/j;->v:Landroid/view/View$OnAttachStateChangeListener;

    new-instance v0, Lmiuix/animation/a/a;

    invoke-direct {v0}, Lmiuix/animation/a/a;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiuix/animation/a/a;->a(F)Lmiuix/animation/a/a;

    sput-object v0, Lmiuix/recyclerview/widget/j;->w:Lmiuix/animation/a/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiuix/recyclerview/widget/c;-><init>()V

    return-void
.end method


# virtual methods
.method B(Landroidx/recyclerview/widget/RecyclerView$t;)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1}, Lmiuix/recyclerview/widget/j;->C(Landroidx/recyclerview/widget/RecyclerView$t;)V

    goto/32 :goto_3

    nop

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    goto/32 :goto_4

    nop

    :goto_3
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_1

    nop

    :goto_4
    return-void
.end method

.method C(Landroidx/recyclerview/widget/RecyclerView$t;)V
    .locals 5

    goto/32 :goto_13

    nop

    :goto_0
    new-array v1, v0, [Landroid/view/View;

    goto/32 :goto_12

    nop

    :goto_1
    invoke-interface {v1, v2}, Lmiuix/animation/g;->a([Ljava/lang/Object;)V

    goto/32 :goto_11

    nop

    :goto_2
    sget-object v3, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    goto/32 :goto_e

    nop

    :goto_3
    sget-object v3, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    goto/32 :goto_9

    nop

    :goto_4
    invoke-static {p1}, Lmiuix/recyclerview/widget/c;->a(Landroid/view/View;)V

    :goto_5
    goto/32 :goto_b

    nop

    :goto_6
    invoke-interface {v1}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v1

    goto/32 :goto_d

    nop

    :goto_7
    invoke-static {v1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_8
    aput-object v2, v1, v3

    goto/32 :goto_7

    nop

    :goto_9
    aput-object v3, v2, v0

    goto/32 :goto_a

    nop

    :goto_a
    const/4 v0, 0x2

    goto/32 :goto_2

    nop

    :goto_b
    return-void

    :goto_c
    aput-object v4, v2, v3

    goto/32 :goto_3

    nop

    :goto_d
    const/4 v2, 0x3

    goto/32 :goto_14

    nop

    :goto_e
    aput-object v3, v2, v0

    goto/32 :goto_1

    nop

    :goto_f
    sget-object v4, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    goto/32 :goto_c

    nop

    :goto_10
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_11
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_4

    nop

    :goto_12
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_15

    nop

    :goto_13
    if-nez p1, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_10

    nop

    :goto_14
    new-array v2, v2, [Ljava/lang/Object;

    goto/32 :goto_f

    nop

    :goto_15
    const/4 v3, 0x0

    goto/32 :goto_8

    nop
.end method

.method a(Lmiuix/recyclerview/widget/c$a;)V
    .locals 14

    goto/32 :goto_4c

    nop

    :goto_0
    sget-object v4, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    goto/32 :goto_27

    nop

    :goto_1
    invoke-interface {v10}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v10

    goto/32 :goto_35

    nop

    :goto_2
    move-object v2, v1

    goto/32 :goto_56

    nop

    :goto_3
    iget v12, p1, Lmiuix/recyclerview/widget/c$a;->e:I

    goto/32 :goto_49

    nop

    :goto_4
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    goto/32 :goto_3e

    nop

    :goto_5
    sub-int/2addr v12, v13

    goto/32 :goto_1b

    nop

    :goto_6
    invoke-direct {p1, p0, v2, v0}, Lmiuix/recyclerview/widget/h;-><init>(Lmiuix/recyclerview/widget/j;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView$t;)V

    goto/32 :goto_16

    nop

    :goto_7
    aput-object v12, v11, v7

    goto/32 :goto_43

    nop

    :goto_8
    aput-object v12, v11, v9

    goto/32 :goto_3

    nop

    :goto_9
    invoke-virtual {p0, v3, v9}, Lmiuix/recyclerview/widget/c;->f(Landroidx/recyclerview/widget/RecyclerView$t;Z)V

    goto/32 :goto_13

    nop

    :goto_a
    invoke-interface {v10}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v10

    goto/32 :goto_34

    nop

    :goto_b
    invoke-interface {p1}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object p1

    goto/32 :goto_67

    nop

    :goto_c
    aput-object v1, p1, v9

    goto/32 :goto_2e

    nop

    :goto_d
    aput-object v12, v11, v9

    goto/32 :goto_3f

    nop

    :goto_e
    aput-object v2, v10, v9

    goto/32 :goto_28

    nop

    :goto_f
    aput-object v2, v10, v9

    goto/32 :goto_1d

    nop

    :goto_10
    new-instance p1, Lmiuix/recyclerview/widget/i;

    goto/32 :goto_25

    nop

    :goto_11
    iget v12, p1, Lmiuix/recyclerview/widget/c$a;->f:I

    goto/32 :goto_3d

    nop

    :goto_12
    sget-object v4, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    goto/32 :goto_37

    nop

    :goto_13
    new-array p1, v8, [Landroid/view/View;

    goto/32 :goto_21

    nop

    :goto_14
    new-array p1, v8, [Landroid/view/View;

    goto/32 :goto_c

    nop

    :goto_15
    new-instance p1, Lmiuix/recyclerview/widget/h;

    goto/32 :goto_6

    nop

    :goto_16
    invoke-virtual {v2, p1, v10, v11}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_17
    goto/32 :goto_30

    nop

    :goto_18
    sget-object v4, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    goto/32 :goto_4d

    nop

    :goto_19
    invoke-interface {p1, v0}, Lmiuix/animation/k;->b([Ljava/lang/Object;)J

    move-result-wide v4

    goto/32 :goto_10

    nop

    :goto_1a
    new-array v0, v5, [Ljava/lang/Object;

    goto/32 :goto_12

    nop

    :goto_1b
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    goto/32 :goto_24

    nop

    :goto_1c
    invoke-interface {v10, v11}, Lmiuix/animation/k;->b([Ljava/lang/Object;)J

    move-result-wide v10

    goto/32 :goto_15

    nop

    :goto_1d
    invoke-static {v10}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v10

    goto/32 :goto_1

    nop

    :goto_1e
    sget-object v12, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    goto/32 :goto_7

    nop

    :goto_1f
    const/4 v5, 0x4

    goto/32 :goto_2f

    nop

    :goto_20
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto/32 :goto_4b

    nop

    :goto_21
    aput-object v1, p1, v9

    goto/32 :goto_5b

    nop

    :goto_22
    invoke-virtual {p0, v0, v8}, Lmiuix/recyclerview/widget/c;->f(Landroidx/recyclerview/widget/RecyclerView$t;Z)V

    goto/32 :goto_41

    nop

    :goto_23
    aput-object v4, v0, v8

    goto/32 :goto_0

    nop

    :goto_24
    aput-object v12, v11, v8

    goto/32 :goto_1e

    nop

    :goto_25
    invoke-direct {p1, p0, v2, v3}, Lmiuix/recyclerview/widget/i;-><init>(Lmiuix/recyclerview/widget/j;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView$t;)V

    goto/32 :goto_38

    nop

    :goto_26
    sget-object v12, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    goto/32 :goto_64

    nop

    :goto_27
    aput-object v4, v0, v7

    goto/32 :goto_20

    nop

    :goto_28
    invoke-static {v10}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v10

    goto/32 :goto_a

    nop

    :goto_29
    const/4 v8, 0x1

    goto/32 :goto_55

    nop

    :goto_2a
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto/32 :goto_44

    nop

    :goto_2b
    if-nez v2, :cond_0

    goto/32 :goto_17

    :cond_0
    goto/32 :goto_22

    nop

    :goto_2c
    sget-object v12, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    goto/32 :goto_d

    nop

    :goto_2d
    aput-object v4, v0, v8

    goto/32 :goto_18

    nop

    :goto_2e
    invoke-static {p1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object p1

    goto/32 :goto_42

    nop

    :goto_2f
    const/4 v6, 0x3

    goto/32 :goto_68

    nop

    :goto_30
    if-nez v1, :cond_1

    goto/32 :goto_39

    :cond_1
    goto/32 :goto_9

    nop

    :goto_31
    if-eqz v0, :cond_2

    goto/32 :goto_57

    :cond_2
    goto/32 :goto_2

    nop

    :goto_32
    iget-object v1, v3, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    :goto_33
    goto/32 :goto_3b

    nop

    :goto_34
    new-array v11, v5, [Ljava/lang/Object;

    goto/32 :goto_2c

    nop

    :goto_35
    new-array v11, v4, [Ljava/lang/Object;

    goto/32 :goto_54

    nop

    :goto_36
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto/32 :goto_23

    nop

    :goto_37
    aput-object v4, v0, v9

    goto/32 :goto_36

    nop

    :goto_38
    invoke-virtual {v1, p1, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_39
    goto/32 :goto_3c

    nop

    :goto_3a
    aput-object v4, v0, v9

    goto/32 :goto_51

    nop

    :goto_3b
    const/4 v4, 0x5

    goto/32 :goto_1f

    nop

    :goto_3c
    return-void

    :goto_3d
    iget p1, p1, Lmiuix/recyclerview/widget/c$a;->d:I

    goto/32 :goto_5f

    nop

    :goto_3e
    aput-object v12, v11, v8

    goto/32 :goto_26

    nop

    :goto_3f
    iget v12, p1, Lmiuix/recyclerview/widget/c$a;->e:I

    goto/32 :goto_66

    nop

    :goto_40
    sget-object v4, Lmiuix/recyclerview/widget/j;->w:Lmiuix/animation/a/a;

    goto/32 :goto_47

    nop

    :goto_41
    sget-object v10, Lmiuix/recyclerview/widget/j;->v:Landroid/view/View$OnAttachStateChangeListener;

    goto/32 :goto_50

    nop

    :goto_42
    invoke-interface {p1}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object p1

    goto/32 :goto_1a

    nop

    :goto_43
    iget v12, p1, Lmiuix/recyclerview/widget/c$a;->f:I

    goto/32 :goto_63

    nop

    :goto_44
    aput-object p1, v11, v6

    goto/32 :goto_1c

    nop

    :goto_45
    sub-int/2addr v12, v13

    goto/32 :goto_4a

    nop

    :goto_46
    new-array v10, v8, [Landroid/view/View;

    goto/32 :goto_e

    nop

    :goto_47
    aput-object v4, v0, v5

    goto/32 :goto_61

    nop

    :goto_48
    aput-object v4, v0, v6

    goto/32 :goto_40

    nop

    :goto_49
    iget v13, p1, Lmiuix/recyclerview/widget/c$a;->c:I

    goto/32 :goto_5

    nop

    :goto_4a
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    goto/32 :goto_5c

    nop

    :goto_4b
    aput-object v4, v0, v6

    goto/32 :goto_19

    nop

    :goto_4c
    iget-object v0, p1, Lmiuix/recyclerview/widget/c$a;->a:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_4e

    nop

    :goto_4d
    aput-object v4, v0, v7

    goto/32 :goto_5a

    nop

    :goto_4e
    const/4 v1, 0x0

    goto/32 :goto_31

    nop

    :goto_4f
    sget-object v12, Lmiuix/recyclerview/widget/j;->w:Lmiuix/animation/a/a;

    goto/32 :goto_53

    nop

    :goto_50
    invoke-virtual {v2, v10}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    goto/32 :goto_5d

    nop

    :goto_51
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto/32 :goto_2d

    nop

    :goto_52
    sub-int/2addr v12, v13

    goto/32 :goto_4

    nop

    :goto_53
    aput-object v12, v11, v5

    goto/32 :goto_5e

    nop

    :goto_54
    sget-object v12, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    goto/32 :goto_8

    nop

    :goto_55
    const/4 v9, 0x0

    goto/32 :goto_2b

    nop

    :goto_56
    goto :goto_59

    :goto_57
    goto/32 :goto_58

    nop

    :goto_58
    iget-object v2, v0, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    :goto_59
    goto/32 :goto_65

    nop

    :goto_5a
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto/32 :goto_48

    nop

    :goto_5b
    invoke-static {p1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object p1

    goto/32 :goto_b

    nop

    :goto_5c
    aput-object v12, v11, v6

    goto/32 :goto_4f

    nop

    :goto_5d
    new-array v10, v8, [Landroid/view/View;

    goto/32 :goto_f

    nop

    :goto_5e
    invoke-interface {v10, v11}, Lmiuix/animation/k;->c([Ljava/lang/Object;)Lmiuix/animation/k;

    goto/32 :goto_46

    nop

    :goto_5f
    sub-int/2addr v12, p1

    goto/32 :goto_2a

    nop

    :goto_60
    if-nez v3, :cond_3

    goto/32 :goto_33

    :cond_3
    goto/32 :goto_32

    nop

    :goto_61
    invoke-interface {p1, v0}, Lmiuix/animation/k;->c([Ljava/lang/Object;)Lmiuix/animation/k;

    goto/32 :goto_14

    nop

    :goto_62
    sget-object v4, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    goto/32 :goto_3a

    nop

    :goto_63
    iget v13, p1, Lmiuix/recyclerview/widget/c$a;->d:I

    goto/32 :goto_45

    nop

    :goto_64
    aput-object v12, v11, v7

    goto/32 :goto_11

    nop

    :goto_65
    iget-object v3, p1, Lmiuix/recyclerview/widget/c$a;->b:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_60

    nop

    :goto_66
    iget v13, p1, Lmiuix/recyclerview/widget/c$a;->c:I

    goto/32 :goto_52

    nop

    :goto_67
    new-array v0, v4, [Ljava/lang/Object;

    goto/32 :goto_62

    nop

    :goto_68
    const/4 v7, 0x2

    goto/32 :goto_29

    nop
.end method

.method a(Lmiuix/recyclerview/widget/c$b;)V
    .locals 10

    goto/32 :goto_19

    nop

    :goto_0
    const/4 v4, 0x0

    goto/32 :goto_2c

    nop

    :goto_1
    sget-object v8, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    goto/32 :goto_e

    nop

    :goto_2
    aput-object v6, v3, v7

    goto/32 :goto_c

    nop

    :goto_3
    invoke-static {v2}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v2

    goto/32 :goto_1f

    nop

    :goto_4
    new-array v2, v1, [Landroid/view/View;

    goto/32 :goto_f

    nop

    :goto_5
    sget-object v6, Lmiuix/animation/g/A;->b:Lmiuix/animation/g/A;

    goto/32 :goto_6

    nop

    :goto_6
    aput-object v6, v3, v4

    goto/32 :goto_25

    nop

    :goto_7
    const/4 v1, 0x1

    goto/32 :goto_12

    nop

    :goto_8
    sget-object v6, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    goto/32 :goto_1a

    nop

    :goto_9
    iget-object v0, p1, Lmiuix/recyclerview/widget/c$b;->a:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_7

    nop

    :goto_a
    invoke-direct {v3, p0, v0}, Lmiuix/recyclerview/widget/f;-><init>(Lmiuix/recyclerview/widget/j;Landroidx/recyclerview/widget/RecyclerView$t;)V

    goto/32 :goto_27

    nop

    :goto_b
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_1b

    nop

    :goto_c
    const/4 v6, 0x3

    goto/32 :goto_28

    nop

    :goto_d
    iget-object p1, p1, Lmiuix/recyclerview/widget/c$b;->a:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_b

    nop

    :goto_e
    aput-object v8, v3, v4

    goto/32 :goto_21

    nop

    :goto_f
    iget-object v3, p1, Lmiuix/recyclerview/widget/c$b;->a:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_18

    nop

    :goto_10
    aput-object v8, v3, v9

    goto/32 :goto_2a

    nop

    :goto_11
    invoke-interface {v2, v3}, Lmiuix/animation/k;->b([Ljava/lang/Object;)J

    move-result-wide v1

    goto/32 :goto_d

    nop

    :goto_12
    new-array v2, v1, [Landroid/view/View;

    goto/32 :goto_14

    nop

    :goto_13
    aput-object v3, v2, v4

    goto/32 :goto_3

    nop

    :goto_14
    iget-object v3, v0, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_0

    nop

    :goto_15
    sget-object v1, Lmiuix/animation/g/A;->c:Lmiuix/animation/g/A;

    goto/32 :goto_2b

    nop

    :goto_16
    new-array v3, v3, [Ljava/lang/Object;

    goto/32 :goto_5

    nop

    :goto_17
    aput-object v3, v2, v4

    goto/32 :goto_20

    nop

    :goto_18
    iget-object v3, v3, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_13

    nop

    :goto_19
    iget-object v0, p1, Lmiuix/recyclerview/widget/c$b;->a:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_1c

    nop

    :goto_1a
    const/4 v7, 0x2

    goto/32 :goto_2

    nop

    :goto_1b
    new-instance v3, Lmiuix/recyclerview/widget/f;

    goto/32 :goto_a

    nop

    :goto_1c
    invoke-virtual {p0, v0}, Lmiuix/recyclerview/widget/c;->y(Landroidx/recyclerview/widget/RecyclerView$t;)V

    goto/32 :goto_9

    nop

    :goto_1d
    invoke-interface {v2}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v2

    goto/32 :goto_23

    nop

    :goto_1e
    return-void

    :goto_1f
    invoke-interface {v2}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v2

    goto/32 :goto_24

    nop

    :goto_20
    invoke-static {v2}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v2

    goto/32 :goto_1d

    nop

    :goto_21
    aput-object v5, v3, v1

    goto/32 :goto_15

    nop

    :goto_22
    aput-object v5, v3, v6

    goto/32 :goto_11

    nop

    :goto_23
    const/4 v3, 0x5

    goto/32 :goto_16

    nop

    :goto_24
    new-array v3, v9, [Ljava/lang/Object;

    goto/32 :goto_1

    nop

    :goto_25
    aput-object v5, v3, v1

    goto/32 :goto_8

    nop

    :goto_26
    sget-object v8, Lmiuix/recyclerview/widget/j;->w:Lmiuix/animation/a/a;

    goto/32 :goto_29

    nop

    :goto_27
    invoke-virtual {p1, v3, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/32 :goto_1e

    nop

    :goto_28
    aput-object v5, v3, v6

    goto/32 :goto_26

    nop

    :goto_29
    const/4 v9, 0x4

    goto/32 :goto_10

    nop

    :goto_2a
    invoke-interface {v2, v3}, Lmiuix/animation/k;->c([Ljava/lang/Object;)Lmiuix/animation/k;

    goto/32 :goto_4

    nop

    :goto_2b
    aput-object v1, v3, v7

    goto/32 :goto_22

    nop

    :goto_2c
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto/32 :goto_17

    nop
.end method

.method b(Lmiuix/recyclerview/widget/c$a;)V
    .locals 5

    goto/32 :goto_20

    nop

    :goto_0
    int-to-float v2, v2

    goto/32 :goto_1

    nop

    :goto_1
    sub-float/2addr v2, v0

    goto/32 :goto_1b

    nop

    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    goto/32 :goto_22

    nop

    :goto_3
    invoke-virtual {p0, v0}, Lmiuix/recyclerview/widget/j;->C(Landroidx/recyclerview/widget/RecyclerView$t;)V

    goto/32 :goto_1a

    nop

    :goto_4
    neg-int v1, v2

    goto/32 :goto_b

    nop

    :goto_5
    int-to-float v3, v3

    goto/32 :goto_14

    nop

    :goto_6
    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    goto/32 :goto_25

    nop

    :goto_7
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_e

    nop

    :goto_8
    iget-object v0, v0, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_26

    nop

    :goto_9
    iget-object v0, p1, Lmiuix/recyclerview/widget/c$a;->a:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_c

    nop

    :goto_a
    iget-object v1, v1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_21

    nop

    :goto_b
    int-to-float v1, v1

    goto/32 :goto_2

    nop

    :goto_c
    iget-object v0, v0, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_6

    nop

    :goto_d
    invoke-virtual {p0, v2}, Lmiuix/recyclerview/widget/j;->C(Landroidx/recyclerview/widget/RecyclerView$t;)V

    goto/32 :goto_24

    nop

    :goto_e
    neg-int v0, v3

    goto/32 :goto_23

    nop

    :goto_f
    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    :goto_10
    goto/32 :goto_1f

    nop

    :goto_11
    iget-object v2, p1, Lmiuix/recyclerview/widget/c$a;->a:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_d

    nop

    :goto_12
    sub-int/2addr v3, v4

    goto/32 :goto_5

    nop

    :goto_13
    float-to-int v3, v3

    goto/32 :goto_1c

    nop

    :goto_14
    sub-float/2addr v3, v1

    goto/32 :goto_13

    nop

    :goto_15
    if-nez v0, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_3

    nop

    :goto_16
    sub-int/2addr v2, v3

    goto/32 :goto_0

    nop

    :goto_17
    iget-object v1, p1, Lmiuix/recyclerview/widget/c$a;->a:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_a

    nop

    :goto_18
    iget v3, p1, Lmiuix/recyclerview/widget/c$a;->f:I

    goto/32 :goto_27

    nop

    :goto_19
    invoke-virtual {v4, v0}, Landroid/view/View;->setTranslationX(F)V

    goto/32 :goto_9

    nop

    :goto_1a
    iget-object v0, p1, Lmiuix/recyclerview/widget/c$a;->b:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_28

    nop

    :goto_1b
    float-to-int v2, v2

    goto/32 :goto_18

    nop

    :goto_1c
    iget-object v4, p1, Lmiuix/recyclerview/widget/c$a;->a:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_1d

    nop

    :goto_1d
    iget-object v4, v4, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_19

    nop

    :goto_1e
    iget v3, p1, Lmiuix/recyclerview/widget/c$a;->c:I

    goto/32 :goto_16

    nop

    :goto_1f
    return-void

    :goto_20
    iget-object v0, p1, Lmiuix/recyclerview/widget/c$a;->a:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_8

    nop

    :goto_21
    invoke-virtual {v1}, Landroid/view/View;->getTranslationY()F

    move-result v1

    goto/32 :goto_11

    nop

    :goto_22
    iget-object p1, p1, Lmiuix/recyclerview/widget/c$a;->b:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_7

    nop

    :goto_23
    int-to-float v0, v0

    goto/32 :goto_f

    nop

    :goto_24
    iget v2, p1, Lmiuix/recyclerview/widget/c$a;->e:I

    goto/32 :goto_1e

    nop

    :goto_25
    iget-object v0, p1, Lmiuix/recyclerview/widget/c$a;->b:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_15

    nop

    :goto_26
    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v0

    goto/32 :goto_17

    nop

    :goto_27
    iget v4, p1, Lmiuix/recyclerview/widget/c$a;->d:I

    goto/32 :goto_12

    nop

    :goto_28
    iget-object v0, v0, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_4

    nop
.end method

.method b(Lmiuix/recyclerview/widget/c$b;)V
    .locals 3

    goto/32 :goto_a

    nop

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    goto/32 :goto_1

    nop

    :goto_1
    iget-object v0, p1, Lmiuix/recyclerview/widget/c$b;->a:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_2

    nop

    :goto_2
    iget-object v0, v0, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_9

    nop

    :goto_3
    iget v2, p1, Lmiuix/recyclerview/widget/c$b;->d:I

    goto/32 :goto_c

    nop

    :goto_4
    invoke-virtual {v0, p1}, Landroid/view/View;->setTranslationY(F)V

    goto/32 :goto_6

    nop

    :goto_5
    iget v1, p1, Lmiuix/recyclerview/widget/c$b;->b:I

    goto/32 :goto_3

    nop

    :goto_6
    return-void

    :goto_7
    int-to-float p1, v1

    goto/32 :goto_4

    nop

    :goto_8
    iget p1, p1, Lmiuix/recyclerview/widget/c$b;->e:I

    goto/32 :goto_d

    nop

    :goto_9
    iget v1, p1, Lmiuix/recyclerview/widget/c$b;->c:I

    goto/32 :goto_8

    nop

    :goto_a
    iget-object v0, p1, Lmiuix/recyclerview/widget/c$b;->a:Landroidx/recyclerview/widget/RecyclerView$t;

    goto/32 :goto_b

    nop

    :goto_b
    iget-object v0, v0, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_5

    nop

    :goto_c
    sub-int/2addr v1, v2

    goto/32 :goto_e

    nop

    :goto_d
    sub-int/2addr v1, p1

    goto/32 :goto_7

    nop

    :goto_e
    int-to-float v1, v1

    goto/32 :goto_0

    nop
.end method

.method public e()J
    .locals 2

    const-wide/16 v0, 0x12c

    return-wide v0
.end method

.method public f()J
    .locals 2

    const-wide/16 v0, 0x12c

    return-wide v0
.end method

.method t(Landroidx/recyclerview/widget/RecyclerView$t;)V
    .locals 7

    goto/32 :goto_11

    nop

    :goto_0
    invoke-interface {v1}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_1
    sget-object v4, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    goto/32 :goto_1e

    nop

    :goto_2
    new-array v1, v0, [Landroid/view/View;

    goto/32 :goto_a

    nop

    :goto_3
    invoke-virtual {v2, v3, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/32 :goto_d

    nop

    :goto_4
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    goto/32 :goto_c

    nop

    :goto_5
    const/high16 v4, 0x3f800000    # 1.0f

    goto/32 :goto_4

    nop

    :goto_6
    const/4 v2, 0x3

    goto/32 :goto_1f

    nop

    :goto_7
    aput-object v2, v1, v3

    goto/32 :goto_f

    nop

    :goto_8
    invoke-interface {v1}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v1

    goto/32 :goto_16

    nop

    :goto_9
    aput-object v5, v2, v3

    goto/32 :goto_10

    nop

    :goto_a
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_1b

    nop

    :goto_b
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_18

    nop

    :goto_c
    aput-object v4, v2, v0

    goto/32 :goto_14

    nop

    :goto_d
    return-void

    :goto_e
    invoke-direct {v3, p0, p1}, Lmiuix/recyclerview/widget/g;-><init>(Lmiuix/recyclerview/widget/j;Landroidx/recyclerview/widget/RecyclerView$t;)V

    goto/32 :goto_3

    nop

    :goto_f
    invoke-static {v1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_10
    aput-object v4, v2, v0

    goto/32 :goto_12

    nop

    :goto_11
    invoke-virtual {p0, p1}, Lmiuix/recyclerview/widget/c;->w(Landroidx/recyclerview/widget/RecyclerView$t;)V

    goto/32 :goto_21

    nop

    :goto_12
    invoke-interface {v1, v2}, Lmiuix/animation/k;->b([Ljava/lang/Object;)J

    move-result-wide v0

    goto/32 :goto_b

    nop

    :goto_13
    new-array v1, v0, [Landroid/view/View;

    goto/32 :goto_1d

    nop

    :goto_14
    sget-object v5, Lmiuix/recyclerview/widget/j;->w:Lmiuix/animation/a/a;

    goto/32 :goto_1a

    nop

    :goto_15
    aput-object v2, v1, v3

    goto/32 :goto_20

    nop

    :goto_16
    new-array v2, v6, [Ljava/lang/Object;

    goto/32 :goto_17

    nop

    :goto_17
    sget-object v5, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    goto/32 :goto_9

    nop

    :goto_18
    new-instance v3, Lmiuix/recyclerview/widget/g;

    goto/32 :goto_e

    nop

    :goto_19
    invoke-interface {v1, v2}, Lmiuix/animation/k;->c([Ljava/lang/Object;)Lmiuix/animation/k;

    goto/32 :goto_13

    nop

    :goto_1a
    const/4 v6, 0x2

    goto/32 :goto_1c

    nop

    :goto_1b
    const/4 v3, 0x0

    goto/32 :goto_7

    nop

    :goto_1c
    aput-object v5, v2, v6

    goto/32 :goto_19

    nop

    :goto_1d
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_15

    nop

    :goto_1e
    aput-object v4, v2, v3

    goto/32 :goto_5

    nop

    :goto_1f
    new-array v2, v2, [Ljava/lang/Object;

    goto/32 :goto_1

    nop

    :goto_20
    invoke-static {v1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_21
    const/4 v0, 0x1

    goto/32 :goto_2

    nop
.end method

.method u(Landroidx/recyclerview/widget/RecyclerView$t;)V
    .locals 7

    goto/32 :goto_d

    nop

    :goto_0
    new-array v2, v6, [Ljava/lang/Object;

    goto/32 :goto_11

    nop

    :goto_1
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_b

    nop

    :goto_2
    invoke-interface {v1, v2}, Lmiuix/animation/k;->c([Ljava/lang/Object;)Lmiuix/animation/k;

    goto/32 :goto_1b

    nop

    :goto_3
    new-array v2, v2, [Ljava/lang/Object;

    goto/32 :goto_7

    nop

    :goto_4
    invoke-virtual {v0, v1}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    goto/32 :goto_12

    nop

    :goto_5
    aput-object v4, v2, v0

    goto/32 :goto_20

    nop

    :goto_6
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_17

    nop

    :goto_7
    sget-object v4, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    goto/32 :goto_23

    nop

    :goto_8
    aput-object v5, v2, v3

    goto/32 :goto_f

    nop

    :goto_9
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    goto/32 :goto_5

    nop

    :goto_a
    sget-object v1, Lmiuix/recyclerview/widget/j;->v:Landroid/view/View$OnAttachStateChangeListener;

    goto/32 :goto_4

    nop

    :goto_b
    const/4 v3, 0x0

    goto/32 :goto_15

    nop

    :goto_c
    aput-object v5, v2, v6

    goto/32 :goto_2

    nop

    :goto_d
    invoke-virtual {p0, p1}, Lmiuix/recyclerview/widget/c;->A(Landroidx/recyclerview/widget/RecyclerView$t;)V

    goto/32 :goto_21

    nop

    :goto_e
    const/4 v6, 0x2

    goto/32 :goto_c

    nop

    :goto_f
    aput-object v4, v2, v0

    goto/32 :goto_18

    nop

    :goto_10
    return-void

    :goto_11
    sget-object v5, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    goto/32 :goto_8

    nop

    :goto_12
    const/4 v0, 0x1

    goto/32 :goto_1a

    nop

    :goto_13
    invoke-static {v1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v1

    goto/32 :goto_24

    nop

    :goto_14
    invoke-static {v1}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v1

    goto/32 :goto_1f

    nop

    :goto_15
    aput-object v2, v1, v3

    goto/32 :goto_13

    nop

    :goto_16
    const/4 v4, 0x0

    goto/32 :goto_9

    nop

    :goto_17
    new-instance v3, Lmiuix/recyclerview/widget/e;

    goto/32 :goto_1d

    nop

    :goto_18
    invoke-interface {v1, v2}, Lmiuix/animation/k;->b([Ljava/lang/Object;)J

    move-result-wide v0

    goto/32 :goto_6

    nop

    :goto_19
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_1c

    nop

    :goto_1a
    new-array v1, v0, [Landroid/view/View;

    goto/32 :goto_1

    nop

    :goto_1b
    new-array v1, v0, [Landroid/view/View;

    goto/32 :goto_19

    nop

    :goto_1c
    aput-object v2, v1, v3

    goto/32 :goto_14

    nop

    :goto_1d
    invoke-direct {v3, p0, p1}, Lmiuix/recyclerview/widget/e;-><init>(Lmiuix/recyclerview/widget/j;Landroidx/recyclerview/widget/RecyclerView$t;)V

    goto/32 :goto_22

    nop

    :goto_1e
    const/4 v2, 0x3

    goto/32 :goto_3

    nop

    :goto_1f
    invoke-interface {v1}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_20
    sget-object v5, Lmiuix/recyclerview/widget/j;->w:Lmiuix/animation/a/a;

    goto/32 :goto_e

    nop

    :goto_21
    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$t;->itemView:Landroid/view/View;

    goto/32 :goto_a

    nop

    :goto_22
    invoke-virtual {v2, v3, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/32 :goto_10

    nop

    :goto_23
    aput-object v4, v2, v3

    goto/32 :goto_16

    nop

    :goto_24
    invoke-interface {v1}, Lmiuix/animation/h;->a()Lmiuix/animation/k;

    move-result-object v1

    goto/32 :goto_1e

    nop
.end method
