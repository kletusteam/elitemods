.class final Lmiuix/smooth/SmoothContainerDrawable$b;
.super Landroid/graphics/drawable/Drawable$ConstantState;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/smooth/SmoothContainerDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation


# instance fields
.field a:Lmiuix/smooth/SmoothContainerDrawable$a;

.field b:F

.field c:[F

.field d:I

.field e:I

.field f:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable$ConstantState;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->f:I

    new-instance v0, Lmiuix/smooth/SmoothContainerDrawable$a;

    invoke-direct {v0}, Lmiuix/smooth/SmoothContainerDrawable$a;-><init>()V

    iput-object v0, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->a:Lmiuix/smooth/SmoothContainerDrawable$a;

    return-void
.end method

.method public constructor <init>(Lmiuix/smooth/SmoothContainerDrawable$b;Lmiuix/smooth/SmoothContainerDrawable;Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;)V
    .locals 2
    .param p1    # Lmiuix/smooth/SmoothContainerDrawable$b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lmiuix/smooth/SmoothContainerDrawable;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroid/content/res/Resources;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/content/res/Resources$Theme;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable$ConstantState;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->f:I

    new-instance v0, Lmiuix/smooth/SmoothContainerDrawable$a;

    iget-object v1, p1, Lmiuix/smooth/SmoothContainerDrawable$b;->a:Lmiuix/smooth/SmoothContainerDrawable$a;

    invoke-direct {v0, v1, p2, p3, p4}, Lmiuix/smooth/SmoothContainerDrawable$a;-><init>(Lmiuix/smooth/SmoothContainerDrawable$a;Lmiuix/smooth/SmoothContainerDrawable;Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;)V

    iput-object v0, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->a:Lmiuix/smooth/SmoothContainerDrawable$a;

    iget p2, p1, Lmiuix/smooth/SmoothContainerDrawable$b;->b:F

    iput p2, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->b:F

    iget-object p2, p1, Lmiuix/smooth/SmoothContainerDrawable$b;->c:[F

    iput-object p2, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->c:[F

    iget p2, p1, Lmiuix/smooth/SmoothContainerDrawable$b;->d:I

    iput p2, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->d:I

    iget p2, p1, Lmiuix/smooth/SmoothContainerDrawable$b;->e:I

    iput p2, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->e:I

    iget p1, p1, Lmiuix/smooth/SmoothContainerDrawable$b;->f:I

    iput p1, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->f:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->a:Lmiuix/smooth/SmoothContainerDrawable$a;

    iget-object v0, v0, Lmiuix/smooth/SmoothContainerDrawable$a;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getAlpha()I

    move-result v0

    return v0
.end method

.method public a(I)V
    .locals 1

    iget-object v0, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->a:Lmiuix/smooth/SmoothContainerDrawable$a;

    iget-object v0, v0, Lmiuix/smooth/SmoothContainerDrawable$a;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object p1, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->a:Lmiuix/smooth/SmoothContainerDrawable$a;

    iget-object p1, p1, Lmiuix/smooth/SmoothContainerDrawable$a;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    return-void
.end method

.method public a(Landroid/graphics/ColorFilter;)V
    .locals 1

    iget-object v0, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->a:Lmiuix/smooth/SmoothContainerDrawable$a;

    iget-object v0, v0, Lmiuix/smooth/SmoothContainerDrawable$a;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->a:Lmiuix/smooth/SmoothContainerDrawable$a;

    iget-object v0, v0, Lmiuix/smooth/SmoothContainerDrawable$a;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    return-void
.end method

.method public a(Landroid/graphics/Rect;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->a:Lmiuix/smooth/SmoothContainerDrawable$a;

    iget-object v0, v0, Lmiuix/smooth/SmoothContainerDrawable$a;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    move-result p1

    return p1
.end method

.method public a([I)Z
    .locals 1

    invoke-virtual {p0}, Lmiuix/smooth/SmoothContainerDrawable$b;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->a:Lmiuix/smooth/SmoothContainerDrawable$a;

    iget-object v0, v0, Lmiuix/smooth/SmoothContainerDrawable$a;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public b()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->a:Lmiuix/smooth/SmoothContainerDrawable$a;

    iget-object v0, v0, Lmiuix/smooth/SmoothContainerDrawable$a;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public b(I)V
    .locals 1

    iget-object v0, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->a:Lmiuix/smooth/SmoothContainerDrawable$a;

    iget-object v0, v0, Lmiuix/smooth/SmoothContainerDrawable$a;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setChangingConfigurations(I)V

    return-void
.end method

.method public b(Landroid/graphics/Rect;)V
    .locals 1

    iget-object v0, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->a:Lmiuix/smooth/SmoothContainerDrawable$a;

    iget-object v0, v0, Lmiuix/smooth/SmoothContainerDrawable$a;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    return-void
.end method

.method public b(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->a:Lmiuix/smooth/SmoothContainerDrawable$a;

    iget-object v0, v0, Lmiuix/smooth/SmoothContainerDrawable$a;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    return-void
.end method

.method public c()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->a:Lmiuix/smooth/SmoothContainerDrawable$a;

    iget-object v0, v0, Lmiuix/smooth/SmoothContainerDrawable$a;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getDirtyBounds()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public canApplyTheme()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public d()I
    .locals 1

    iget-object v0, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->a:Lmiuix/smooth/SmoothContainerDrawable$a;

    iget-object v0, v0, Lmiuix/smooth/SmoothContainerDrawable$a;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    return v0
.end method

.method public e()I
    .locals 1

    iget-object v0, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->a:Lmiuix/smooth/SmoothContainerDrawable$a;

    iget-object v0, v0, Lmiuix/smooth/SmoothContainerDrawable$a;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    return v0
.end method

.method public f()I
    .locals 1

    iget-object v0, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->a:Lmiuix/smooth/SmoothContainerDrawable$a;

    iget-object v0, v0, Lmiuix/smooth/SmoothContainerDrawable$a;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 1

    iget-object v0, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->a:Lmiuix/smooth/SmoothContainerDrawable$a;

    iget-object v0, v0, Lmiuix/smooth/SmoothContainerDrawable$a;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    return v0
.end method

.method public getChangingConfigurations()I
    .locals 1

    iget-object v0, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->a:Lmiuix/smooth/SmoothContainerDrawable$a;

    iget-object v0, v0, Lmiuix/smooth/SmoothContainerDrawable$a;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getChangingConfigurations()I

    move-result v0

    return v0
.end method

.method public h()V
    .locals 1

    iget-object v0, p0, Lmiuix/smooth/SmoothContainerDrawable$b;->a:Lmiuix/smooth/SmoothContainerDrawable$a;

    iget-object v0, v0, Lmiuix/smooth/SmoothContainerDrawable$a;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    return-void
.end method

.method public newDrawable()Landroid/graphics/drawable/Drawable;
    .locals 2
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    new-instance v0, Lmiuix/smooth/SmoothContainerDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1, p0, v1}, Lmiuix/smooth/SmoothContainerDrawable;-><init>(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;Lmiuix/smooth/SmoothContainerDrawable$b;Lmiuix/smooth/b;)V

    return-object v0
.end method

.method public newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p1    # Landroid/content/res/Resources;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    new-instance v0, Lmiuix/smooth/SmoothContainerDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1, p0, v1}, Lmiuix/smooth/SmoothContainerDrawable;-><init>(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;Lmiuix/smooth/SmoothContainerDrawable$b;Lmiuix/smooth/b;)V

    return-object v0
.end method

.method public newDrawable(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p1    # Landroid/content/res/Resources;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/content/res/Resources$Theme;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    new-instance v0, Lmiuix/smooth/SmoothContainerDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, p0, v1}, Lmiuix/smooth/SmoothContainerDrawable;-><init>(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;Lmiuix/smooth/SmoothContainerDrawable$b;Lmiuix/smooth/b;)V

    return-object v0
.end method
