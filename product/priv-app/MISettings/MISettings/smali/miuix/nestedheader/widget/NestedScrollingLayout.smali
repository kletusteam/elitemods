.class public Lmiuix/nestedheader/widget/NestedScrollingLayout;
.super Landroid/widget/FrameLayout;

# interfaces
.implements Landroidx/core/view/r;
.implements Landroidx/core/view/n;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/nestedheader/widget/NestedScrollingLayout$a;
    }
.end annotation


# instance fields
.field private final a:[I

.field private final b:[I

.field private c:I

.field protected d:Landroid/view/View;

.field private final e:[I

.field private f:I

.field private g:I

.field private h:I

.field private final i:Landroidx/core/view/t;

.field private final j:Landroidx/core/view/p;

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:J

.field private p:J

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:I

.field private u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiuix/nestedheader/widget/NestedScrollingLayout$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiuix/nestedheader/widget/NestedScrollingLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lmiuix/nestedheader/widget/NestedScrollingLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p3, 0x2

    new-array v0, p3, [I

    iput-object v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->a:[I

    new-array v0, p3, [I

    iput-object v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->b:[I

    new-array p3, p3, [I

    iput-object p3, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->e:[I

    const/4 p3, 0x1

    iput-boolean p3, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->n:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->o:J

    iput-wide v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->p:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->q:Z

    iput-boolean v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->r:Z

    iput-boolean v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->s:Z

    iput p3, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->t:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->u:Ljava/util/List;

    new-instance v0, Landroidx/core/view/t;

    invoke-direct {v0, p0}, Landroidx/core/view/t;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->i:Landroidx/core/view/t;

    invoke-static {p0}, Ld/d/b/b;->a(Landroid/view/View;)Landroidx/core/view/p;

    move-result-object v0

    iput-object v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->j:Landroidx/core/view/p;

    sget-object v0, Ld/j/c;->NestedScrollingLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    sget p2, Ld/j/c;->NestedScrollingLayout_scrollableView:I

    const v0, 0x102000a

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p2

    iput p2, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->c:I

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0, p3}, Lmiuix/nestedheader/widget/NestedScrollingLayout;->setNestedScrollingEnabled(Z)V

    return-void
.end method

.method private a()V
    .locals 1

    iget v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->f:I

    invoke-virtual {p0, v0}, Lmiuix/nestedheader/widget/NestedScrollingLayout;->a(I)V

    return-void
.end method

.method private d(I)V
    .locals 2

    iget-object v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/nestedheader/widget/NestedScrollingLayout$a;

    invoke-interface {v1, p1}, Lmiuix/nestedheader/widget/NestedScrollingLayout$a;->c(I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private e(I)V
    .locals 2

    iget-object v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/nestedheader/widget/NestedScrollingLayout$a;

    invoke-interface {v1, p1}, Lmiuix/nestedheader/widget/NestedScrollingLayout$a;->a(I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private f(I)V
    .locals 2

    iget-object v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/nestedheader/widget/NestedScrollingLayout$a;

    invoke-interface {v1, p1}, Lmiuix/nestedheader/widget/NestedScrollingLayout$a;->b(I)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method protected a(I)V
    .locals 0

    return-void
.end method

.method public a(IIII[II[I)V
    .locals 8
    .param p5    # [I
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # [I
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->j:Landroidx/core/view/p;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Landroidx/core/view/p;->a(IIII[II[I)V

    return-void
.end method

.method public a(Landroid/view/View;I)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->i:Landroidx/core/view/t;

    invoke-virtual {v0, p1, p2}, Landroidx/core/view/t;->a(Landroid/view/View;I)V

    invoke-direct {p0, p2}, Lmiuix/nestedheader/widget/NestedScrollingLayout;->e(I)V

    invoke-virtual {p0, p2}, Lmiuix/nestedheader/widget/NestedScrollingLayout;->b(I)V

    iget-boolean p1, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->l:Z

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iput-boolean v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->l:Z

    iget-boolean p1, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->k:Z

    if-nez p1, :cond_2

    iget-boolean p1, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->m:Z

    if-nez p1, :cond_2

    invoke-direct {p0, p2}, Lmiuix/nestedheader/widget/NestedScrollingLayout;->f(I)V

    goto :goto_0

    :cond_0
    iget-boolean p1, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->k:Z

    if-eqz p1, :cond_1

    iput-boolean v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->k:Z

    invoke-direct {p0, p2}, Lmiuix/nestedheader/widget/NestedScrollingLayout;->f(I)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p2}, Lmiuix/nestedheader/widget/NestedScrollingLayout;->f(I)V

    :cond_2
    :goto_0
    return-void
.end method

.method public a(Landroid/view/View;IIIII)V
    .locals 8
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v7, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->a:[I

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v7}, Lmiuix/nestedheader/widget/NestedScrollingLayout;->a(Landroid/view/View;IIIII[I)V

    return-void
.end method

.method public a(Landroid/view/View;IIIII[I)V
    .locals 8
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p7    # [I
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v5, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->b:[I

    move-object v0, p0

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Lmiuix/nestedheader/widget/NestedScrollingLayout;->a(IIII[II[I)V

    const/4 p1, 0x1

    aget p2, p7, p1

    sub-int p2, p5, p2

    if-gez p5, :cond_b

    if-eqz p2, :cond_b

    iget p3, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->f:I

    sub-int/2addr p3, p2

    const/4 p2, 0x0

    if-nez p6, :cond_0

    move p4, p1

    goto :goto_0

    :cond_0
    move p4, p2

    :goto_0
    iget p5, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->g:I

    if-le p3, p5, :cond_1

    move p5, p1

    goto :goto_1

    :cond_1
    move p5, p2

    :goto_1
    iget-boolean v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->s:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->r:Z

    if-nez v0, :cond_2

    if-ne p6, p1, :cond_2

    if-eqz p5, :cond_2

    iget v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->f:I

    iget v1, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->g:I

    if-ne v0, v1, :cond_2

    move v0, p1

    goto :goto_2

    :cond_2
    move v0, p2

    :goto_2
    iget-boolean v1, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->s:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->r:Z

    if-nez v1, :cond_3

    if-ne p6, p1, :cond_3

    if-nez p5, :cond_3

    move p5, p1

    goto :goto_3

    :cond_3
    move p5, p2

    :goto_3
    iget-boolean v1, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->s:Z

    if-eqz v1, :cond_6

    if-ne p6, p1, :cond_6

    iget-boolean p6, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->r:Z

    if-eqz p6, :cond_6

    iget-boolean p6, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->q:Z

    if-nez p6, :cond_4

    if-ltz p3, :cond_5

    :cond_4
    iget-boolean p6, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->q:Z

    if-eqz p6, :cond_6

    iget-wide v1, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->o:J

    iget-wide v3, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->p:J

    cmp-long p6, v1, v3

    if-gtz p6, :cond_6

    :cond_5
    move p6, p1

    goto :goto_4

    :cond_6
    move p6, p2

    :goto_4
    if-nez p4, :cond_8

    iget-boolean p4, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->s:Z

    if-eqz p4, :cond_8

    if-nez p5, :cond_8

    if-eqz p6, :cond_7

    goto :goto_5

    :cond_7
    move p4, p2

    goto :goto_6

    :cond_8
    :goto_5
    move p4, p1

    :goto_6
    if-eqz p4, :cond_9

    iget p4, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->h:I

    goto :goto_7

    :cond_9
    if-eqz v0, :cond_a

    iget p4, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->g:I

    goto :goto_7

    :cond_a
    move p4, p2

    :goto_7
    iget p5, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->g:I

    invoke-static {p4, p3}, Ljava/lang/Math;->min(II)I

    move-result p3

    invoke-static {p5, p3}, Ljava/lang/Math;->max(II)I

    move-result p3

    iget p4, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->f:I

    sub-int/2addr p4, p3

    iput p3, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->f:I

    invoke-direct {p0}, Lmiuix/nestedheader/widget/NestedScrollingLayout;->a()V

    aget p3, p7, p2

    add-int/2addr p3, p2

    aput p3, p7, p2

    aget p2, p7, p1

    add-int/2addr p2, p4

    aput p2, p7, p1

    :cond_b
    return-void
.end method

.method public a(Landroid/view/View;II[II)V
    .locals 8
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # [I
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/4 p1, 0x1

    if-eqz p5, :cond_1

    iget-boolean v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->k:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->p:J

    :cond_0
    iput-boolean p1, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->k:Z

    goto :goto_0

    :cond_1
    iput-boolean p1, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->l:Z

    :goto_0
    iget-object v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->e:[I

    const/4 v1, 0x0

    if-lez p3, :cond_2

    iget v2, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->g:I

    iget v3, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->h:I

    iget v4, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->f:I

    sub-int/2addr v4, p3

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget v3, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->f:I

    sub-int/2addr v3, v2

    iput v2, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->f:I

    invoke-direct {p0}, Lmiuix/nestedheader/widget/NestedScrollingLayout;->a()V

    aget v2, p4, v1

    add-int/2addr v2, v1

    aput v2, p4, v1

    aget v2, p4, p1

    add-int/2addr v2, v3

    aput v2, p4, p1

    :cond_2
    aget v2, p4, v1

    sub-int v3, p2, v2

    aget p2, p4, p1

    sub-int v4, p3, p2

    const/4 v6, 0x0

    move-object v2, p0

    move-object v5, v0

    move v7, p5

    invoke-virtual/range {v2 .. v7}, Lmiuix/nestedheader/widget/NestedScrollingLayout;->a(II[I[II)Z

    move-result p2

    if-eqz p2, :cond_3

    aget p2, p4, v1

    aget p3, v0, v1

    add-int/2addr p2, p3

    aput p2, p4, v1

    aget p2, p4, p1

    aget p3, v0, p1

    add-int/2addr p2, p3

    aput p2, p4, p1

    :cond_3
    return-void
.end method

.method public a(Lmiuix/nestedheader/widget/NestedScrollingLayout$a;)V
    .locals 1

    iget-object v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->u:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Z)V
    .locals 2

    iget-boolean v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->q:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->o:J

    :cond_0
    iput-boolean p1, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->q:Z

    return-void
.end method

.method public a(II[I[II)Z
    .locals 6
    .param p3    # [I
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # [I
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    iget-object v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->j:Landroidx/core/view/p;

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Landroidx/core/view/p;->a(II[I[II)Z

    move-result p1

    return p1
.end method

.method public a(Landroid/view/View;Landroid/view/View;II)Z
    .locals 0
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p4}, Lmiuix/nestedheader/widget/NestedScrollingLayout;->d(I)V

    iget-object p2, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->j:Landroidx/core/view/p;

    invoke-virtual {p2, p3, p4}, Landroidx/core/view/p;->a(II)Z

    move-result p2

    if-nez p2, :cond_1

    invoke-virtual {p0, p1, p1, p3}, Lmiuix/nestedheader/widget/NestedScrollingLayout;->onStartNestedScroll(Landroid/view/View;Landroid/view/View;I)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public b(I)V
    .locals 1

    iget-object v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->j:Landroidx/core/view/p;

    invoke-virtual {v0, p1}, Landroidx/core/view/p;->c(I)V

    return-void
.end method

.method public b(Landroid/view/View;Landroid/view/View;II)V
    .locals 0
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p0, p1, p2, p3}, Lmiuix/nestedheader/widget/NestedScrollingLayout;->onNestedScrollAccepted(Landroid/view/View;Landroid/view/View;I)V

    if-eqz p4, :cond_0

    const/4 p1, 0x1

    iput-boolean p1, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->m:Z

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    iput-boolean p1, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->m:Z

    :goto_0
    return-void
.end method

.method public c(I)V
    .locals 0

    iput p1, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->f:I

    return-void
.end method

.method public getAcceptedNestedFlingInConsumedProgress()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->k:Z

    return v0
.end method

.method public getScrollType()I
    .locals 1

    iget v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->t:I

    return v0
.end method

.method protected getScrollingFrom()I
    .locals 1

    iget v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->g:I

    return v0
.end method

.method public getScrollingProgress()I
    .locals 1

    iget v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->f:I

    return v0
.end method

.method protected getScrollingTo()I
    .locals 1

    iget v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->h:I

    return v0
.end method

.method public isNestedScrollingEnabled()Z
    .locals 1

    iget-object v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->j:Landroidx/core/view/p;

    invoke-virtual {v0}, Landroidx/core/view/p;->b()Z

    move-result v0

    return v0
.end method

.method protected onFinishInflate()V
    .locals 2
    .annotation build Landroidx/annotation/RequiresApi;
        api = 0x15
    .end annotation

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    iget v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->c:I

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->d:Landroid/view/View;

    iget-object v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setNestedScrollingEnabled(Z)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The scrollableView attribute is required and must refer to a valid child."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    invoke-direct {p0}, Lmiuix/nestedheader/widget/NestedScrollingLayout;->a()V

    return-void
.end method

.method public onNestedPreScroll(Landroid/view/View;II[I)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lmiuix/nestedheader/widget/NestedScrollingLayout;->a(Landroid/view/View;II[II)V

    return-void
.end method

.method public onNestedScroll(Landroid/view/View;IIII)V
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lmiuix/nestedheader/widget/NestedScrollingLayout;->a(Landroid/view/View;IIIII)V

    return-void
.end method

.method public onNestedScrollAccepted(Landroid/view/View;Landroid/view/View;I)V
    .locals 1

    iget-object v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->i:Landroidx/core/view/t;

    invoke-virtual {v0, p1, p2, p3}, Landroidx/core/view/t;->a(Landroid/view/View;Landroid/view/View;I)V

    and-int/lit8 p1, p3, 0x2

    invoke-virtual {p0, p1}, Lmiuix/nestedheader/widget/NestedScrollingLayout;->startNestedScroll(I)Z

    return-void
.end method

.method public onStartNestedScroll(Landroid/view/View;Landroid/view/View;I)Z
    .locals 2

    and-int/lit8 p1, p3, 0x2

    const/4 p2, 0x1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    move p1, p2

    goto :goto_0

    :cond_0
    move p1, v0

    :goto_0
    iget-object v1, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->j:Landroidx/core/view/p;

    invoke-virtual {v1, p3}, Landroidx/core/view/p;->b(I)Z

    move-result p3

    if-nez p3, :cond_2

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->isEnabled()Z

    move-result p3

    if-eqz p3, :cond_1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    move p2, v0

    :cond_2
    :goto_1
    return p2
.end method

.method public setNestedScrollingEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->j:Landroidx/core/view/p;

    invoke-virtual {v0, p1}, Landroidx/core/view/p;->a(Z)V

    return-void
.end method

.method public setScrollType(I)V
    .locals 0

    iput p1, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->t:I

    return-void
.end method

.method public setScrollingRange(IIZZZZZ)V
    .locals 1

    if-le p1, p2, :cond_0

    const-string p1, "NestedScrollingLayout"

    const-string v0, "wrong scrolling range: [%d, %d], making from=to"

    invoke-static {p1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move p1, p2

    :cond_0
    iput p1, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->g:I

    iput p2, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->h:I

    iput-boolean p3, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->r:Z

    iput-boolean p4, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->s:Z

    iget p1, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->f:I

    iget p2, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->g:I

    if-ge p1, p2, :cond_1

    iput p2, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->f:I

    :cond_1
    iget p1, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->f:I

    iget p2, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->h:I

    if-le p1, p2, :cond_2

    iput p2, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->f:I

    :cond_2
    const/4 p1, 0x0

    if-eqz p5, :cond_3

    iget-boolean p2, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->n:Z

    if-nez p2, :cond_4

    :cond_3
    if-nez p6, :cond_4

    if-eqz p7, :cond_5

    :cond_4
    iget-boolean p2, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->r:Z

    if-eqz p2, :cond_5

    iput p1, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->f:I

    iput-boolean p1, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->n:Z

    goto :goto_0

    :cond_5
    if-eqz p5, :cond_6

    iget-boolean p2, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->n:Z

    if-nez p2, :cond_7

    :cond_6
    if-eqz p6, :cond_8

    :cond_7
    iget p2, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->g:I

    iput p2, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->f:I

    iput-boolean p1, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->n:Z

    :cond_8
    :goto_0
    invoke-direct {p0}, Lmiuix/nestedheader/widget/NestedScrollingLayout;->a()V

    return-void
.end method

.method public startNestedScroll(I)Z
    .locals 1

    iget-object v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->j:Landroidx/core/view/p;

    invoke-virtual {v0, p1}, Landroidx/core/view/p;->b(I)Z

    move-result p1

    return p1
.end method

.method public stopNestedScroll()V
    .locals 1

    iget-object v0, p0, Lmiuix/nestedheader/widget/NestedScrollingLayout;->j:Landroidx/core/view/p;

    invoke-virtual {v0}, Landroidx/core/view/p;->c()V

    return-void
.end method
