.class public abstract La/i/a/c;
.super Landroidx/core/view/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La/i/a/c$a;
    }
.end annotation


# static fields
.field private static final d:Landroid/graphics/Rect;

.field private static final e:La/i/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/i/a/d<",
            "Landroidx/core/view/accessibility/d;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:La/i/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/i/a/e<",
            "La/d/j<",
            "Landroidx/core/view/accessibility/d;",
            ">;",
            "Landroidx/core/view/accessibility/d;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final g:Landroid/graphics/Rect;

.field private final h:Landroid/graphics/Rect;

.field private final i:Landroid/graphics/Rect;

.field private final j:[I

.field private final k:Landroid/view/accessibility/AccessibilityManager;

.field private final l:Landroid/view/View;

.field private m:La/i/a/c$a;

.field n:I

.field o:I

.field private p:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Landroid/graphics/Rect;

    const/high16 v1, -0x80000000

    const v2, 0x7fffffff

    invoke-direct {v0, v2, v2, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    sput-object v0, La/i/a/c;->d:Landroid/graphics/Rect;

    new-instance v0, La/i/a/a;

    invoke-direct {v0}, La/i/a/a;-><init>()V

    sput-object v0, La/i/a/c;->e:La/i/a/d;

    new-instance v0, La/i/a/b;

    invoke-direct {v0}, La/i/a/b;-><init>()V

    sput-object v0, La/i/a/c;->f:La/i/a/e;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Landroidx/core/view/a;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, La/i/a/c;->g:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, La/i/a/c;->h:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, La/i/a/c;->i:Landroid/graphics/Rect;

    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, La/i/a/c;->j:[I

    const/high16 v0, -0x80000000

    iput v0, p0, La/i/a/c;->n:I

    iput v0, p0, La/i/a/c;->o:I

    iput v0, p0, La/i/a/c;->p:I

    if-eqz p1, :cond_1

    iput-object p1, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, La/i/a/c;->k:Landroid/view/accessibility/AccessibilityManager;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setFocusable(Z)V

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->j(Landroid/view/View;)I

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1, v0}, Landroidx/core/view/ViewCompat;->d(Landroid/view/View;I)V

    :cond_0
    return-void

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "View may not be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private a(ILandroid/os/Bundle;)Z
    .locals 1

    iget-object v0, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-static {v0, p1, p2}, Landroidx/core/view/ViewCompat;->a(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result p1

    return p1
.end method

.method private a(Landroid/graphics/Rect;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_2

    :cond_0
    iget-object p1, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getWindowVisibility()I

    move-result p1

    if-eqz p1, :cond_1

    return v0

    :cond_1
    iget-object p1, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    :goto_0
    instance-of v1, p1, Landroid/view/View;

    if-eqz v1, :cond_4

    check-cast p1, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getAlpha()F

    move-result v1

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-lez v1, :cond_3

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    goto :goto_0

    :cond_3
    :goto_1
    return v0

    :cond_4
    if-eqz p1, :cond_5

    const/4 v0, 0x1

    :cond_5
    :goto_2
    return v0
.end method

.method private c(II)Landroid/view/accessibility/AccessibilityEvent;
    .locals 1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    invoke-direct {p0, p1, p2}, La/i/a/c;->d(II)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-direct {p0, p2}, La/i/a/c;->e(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object p1

    return-object p1
.end method

.method private c()Landroidx/core/view/accessibility/d;
    .locals 6
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-static {v0}, Landroidx/core/view/accessibility/d;->b(Landroid/view/View;)Landroidx/core/view/accessibility/d;

    move-result-object v0

    iget-object v1, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-static {v1, v0}, Landroidx/core/view/ViewCompat;->a(Landroid/view/View;Landroidx/core/view/accessibility/d;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, v1}, La/i/a/c;->a(Ljava/util/List;)V

    invoke-virtual {v0}, Landroidx/core/view/accessibility/d;->c()I

    move-result v2

    if-lez v2, :cond_1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gtz v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Views cannot have both real and virtual children"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    const/4 v2, 0x0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    :goto_1
    if-ge v2, v3, :cond_2

    iget-object v4, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroidx/core/view/accessibility/d;->a(Landroid/view/View;I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    return-object v0
.end method

.method private c(IILandroid/os/Bundle;)Z
    .locals 1

    const/4 v0, 0x1

    if-eq p2, v0, :cond_3

    const/4 v0, 0x2

    if-eq p2, v0, :cond_2

    const/16 v0, 0x40

    if-eq p2, v0, :cond_1

    const/16 v0, 0x80

    if-eq p2, v0, :cond_0

    invoke-virtual {p0, p1, p2, p3}, La/i/a/c;->a(IILandroid/os/Bundle;)Z

    move-result p1

    return p1

    :cond_0
    invoke-direct {p0, p1}, La/i/a/c;->d(I)Z

    move-result p1

    return p1

    :cond_1
    invoke-direct {p0, p1}, La/i/a/c;->g(I)Z

    move-result p1

    return p1

    :cond_2
    invoke-virtual {p0, p1}, La/i/a/c;->a(I)Z

    move-result p1

    return p1

    :cond_3
    invoke-virtual {p0, p1}, La/i/a/c;->c(I)Z

    move-result p1

    return p1
.end method

.method private d(II)Landroid/view/accessibility/AccessibilityEvent;
    .locals 3

    invoke-static {p2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object p2

    invoke-virtual {p0, p1}, La/i/a/c;->b(I)Landroidx/core/view/accessibility/d;

    move-result-object v0

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0}, Landroidx/core/view/accessibility/d;->j()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Landroidx/core/view/accessibility/d;->f()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroidx/core/view/accessibility/d;->u()Z

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/accessibility/AccessibilityEvent;->setScrollable(Z)V

    invoke-virtual {v0}, Landroidx/core/view/accessibility/d;->t()Z

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/accessibility/AccessibilityEvent;->setPassword(Z)V

    invoke-virtual {v0}, Landroidx/core/view/accessibility/d;->p()Z

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    invoke-virtual {v0}, Landroidx/core/view/accessibility/d;->n()Z

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/accessibility/AccessibilityEvent;->setChecked(Z)V

    invoke-virtual {p0, p1, p2}, La/i/a/c;->a(ILandroid/view/accessibility/AccessibilityEvent;)V

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "Callbacks must add text or a content description in populateEventForVirtualViewId()"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    invoke-virtual {v0}, Landroidx/core/view/accessibility/d;->d()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    iget-object v0, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-static {p2, v0, p1}, Landroidx/core/view/accessibility/f;->a(Landroid/view/accessibility/AccessibilityRecord;Landroid/view/View;I)V

    iget-object p1, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    return-object p2
.end method

.method private d(I)Z
    .locals 1

    iget v0, p0, La/i/a/c;->n:I

    if-ne v0, p1, :cond_0

    const/high16 v0, -0x80000000

    iput v0, p0, La/i/a/c;->n:I

    iget-object v0, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    const/high16 v0, 0x10000

    invoke-virtual {p0, p1, v0}, La/i/a/c;->b(II)Z

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method private e(I)Landroid/view/accessibility/AccessibilityEvent;
    .locals 1

    invoke-static {p1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object p1

    iget-object v0, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    return-object p1
.end method

.method private f(I)Landroidx/core/view/accessibility/d;
    .locals 7
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-static {}, Landroidx/core/view/accessibility/d;->x()Landroidx/core/view/accessibility/d;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/core/view/accessibility/d;->d(Z)V

    invoke-virtual {v0, v1}, Landroidx/core/view/accessibility/d;->e(Z)V

    const-string v2, "android.view.View"

    invoke-virtual {v0, v2}, Landroidx/core/view/accessibility/d;->b(Ljava/lang/CharSequence;)V

    sget-object v2, La/i/a/c;->d:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroidx/core/view/accessibility/d;->c(Landroid/graphics/Rect;)V

    sget-object v2, La/i/a/c;->d:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroidx/core/view/accessibility/d;->d(Landroid/graphics/Rect;)V

    iget-object v2, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroidx/core/view/accessibility/d;->c(Landroid/view/View;)V

    invoke-virtual {p0, p1, v0}, La/i/a/c;->a(ILandroidx/core/view/accessibility/d;)V

    invoke-virtual {v0}, Landroidx/core/view/accessibility/d;->j()Ljava/lang/CharSequence;

    move-result-object v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Landroidx/core/view/accessibility/d;->f()Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Callbacks must add text or a content description in populateNodeForVirtualViewId()"

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    iget-object v2, p0, La/i/a/c;->h:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroidx/core/view/accessibility/d;->a(Landroid/graphics/Rect;)V

    iget-object v2, p0, La/i/a/c;->h:Landroid/graphics/Rect;

    sget-object v3, La/i/a/c;->d:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    invoke-virtual {v0}, Landroidx/core/view/accessibility/d;->b()I

    move-result v2

    and-int/lit8 v3, v2, 0x40

    if-nez v3, :cond_b

    const/16 v3, 0x80

    and-int/2addr v2, v3

    if-nez v2, :cond_a

    iget-object v2, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroidx/core/view/accessibility/d;->d(Ljava/lang/CharSequence;)V

    iget-object v2, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-virtual {v0, v2, p1}, Landroidx/core/view/accessibility/d;->c(Landroid/view/View;I)V

    iget v2, p0, La/i/a/c;->n:I

    const/4 v4, 0x0

    if-ne v2, p1, :cond_2

    invoke-virtual {v0, v1}, Landroidx/core/view/accessibility/d;->a(Z)V

    invoke-virtual {v0, v3}, Landroidx/core/view/accessibility/d;->a(I)V

    goto :goto_1

    :cond_2
    invoke-virtual {v0, v4}, Landroidx/core/view/accessibility/d;->a(Z)V

    const/16 v2, 0x40

    invoke-virtual {v0, v2}, Landroidx/core/view/accessibility/d;->a(I)V

    :goto_1
    iget v2, p0, La/i/a/c;->o:I

    if-ne v2, p1, :cond_3

    move p1, v1

    goto :goto_2

    :cond_3
    move p1, v4

    :goto_2
    if-eqz p1, :cond_4

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroidx/core/view/accessibility/d;->a(I)V

    goto :goto_3

    :cond_4
    invoke-virtual {v0}, Landroidx/core/view/accessibility/d;->q()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v0, v1}, Landroidx/core/view/accessibility/d;->a(I)V

    :cond_5
    :goto_3
    invoke-virtual {v0, p1}, Landroidx/core/view/accessibility/d;->f(Z)V

    iget-object p1, p0, La/i/a/c;->l:Landroid/view/View;

    iget-object v2, p0, La/i/a/c;->j:[I

    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    iget-object p1, p0, La/i/a/c;->g:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroidx/core/view/accessibility/d;->b(Landroid/graphics/Rect;)V

    iget-object p1, p0, La/i/a/c;->g:Landroid/graphics/Rect;

    sget-object v2, La/i/a/c;->d:Landroid/graphics/Rect;

    invoke-virtual {p1, v2}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_8

    iget-object p1, p0, La/i/a/c;->g:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroidx/core/view/accessibility/d;->a(Landroid/graphics/Rect;)V

    iget p1, v0, Landroidx/core/view/accessibility/d;->c:I

    const/4 v2, -0x1

    if-eq p1, v2, :cond_7

    invoke-static {}, Landroidx/core/view/accessibility/d;->x()Landroidx/core/view/accessibility/d;

    move-result-object p1

    iget v3, v0, Landroidx/core/view/accessibility/d;->c:I

    :goto_4
    if-eq v3, v2, :cond_6

    iget-object v5, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-virtual {p1, v5, v2}, Landroidx/core/view/accessibility/d;->b(Landroid/view/View;I)V

    sget-object v5, La/i/a/c;->d:Landroid/graphics/Rect;

    invoke-virtual {p1, v5}, Landroidx/core/view/accessibility/d;->c(Landroid/graphics/Rect;)V

    invoke-virtual {p0, v3, p1}, La/i/a/c;->a(ILandroidx/core/view/accessibility/d;)V

    iget-object v3, p0, La/i/a/c;->h:Landroid/graphics/Rect;

    invoke-virtual {p1, v3}, Landroidx/core/view/accessibility/d;->a(Landroid/graphics/Rect;)V

    iget-object v3, p0, La/i/a/c;->g:Landroid/graphics/Rect;

    iget-object v5, p0, La/i/a/c;->h:Landroid/graphics/Rect;

    iget v6, v5, Landroid/graphics/Rect;->left:I

    iget v5, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual {v3, v6, v5}, Landroid/graphics/Rect;->offset(II)V

    iget v3, p1, Landroidx/core/view/accessibility/d;->c:I

    goto :goto_4

    :cond_6
    invoke-virtual {p1}, Landroidx/core/view/accessibility/d;->y()V

    :cond_7
    iget-object p1, p0, La/i/a/c;->g:Landroid/graphics/Rect;

    iget-object v2, p0, La/i/a/c;->j:[I

    aget v2, v2, v4

    iget-object v3, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getScrollX()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, La/i/a/c;->j:[I

    aget v3, v3, v1

    iget-object v5, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getScrollY()I

    move-result v5

    sub-int/2addr v3, v5

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Rect;->offset(II)V

    :cond_8
    iget-object p1, p0, La/i/a/c;->l:Landroid/view/View;

    iget-object v2, p0, La/i/a/c;->i:Landroid/graphics/Rect;

    invoke-virtual {p1, v2}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result p1

    if-eqz p1, :cond_9

    iget-object p1, p0, La/i/a/c;->i:Landroid/graphics/Rect;

    iget-object v2, p0, La/i/a/c;->j:[I

    aget v2, v2, v4

    iget-object v3, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getScrollX()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, La/i/a/c;->j:[I

    aget v3, v3, v1

    iget-object v4, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getScrollY()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Rect;->offset(II)V

    iget-object p1, p0, La/i/a/c;->g:Landroid/graphics/Rect;

    iget-object v2, p0, La/i/a/c;->i:Landroid/graphics/Rect;

    invoke-virtual {p1, v2}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result p1

    if-eqz p1, :cond_9

    iget-object p1, p0, La/i/a/c;->g:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroidx/core/view/accessibility/d;->d(Landroid/graphics/Rect;)V

    iget-object p1, p0, La/i/a/c;->g:Landroid/graphics/Rect;

    invoke-direct {p0, p1}, La/i/a/c;->a(Landroid/graphics/Rect;)Z

    move-result p1

    if-eqz p1, :cond_9

    invoke-virtual {v0, v1}, Landroidx/core/view/accessibility/d;->l(Z)V

    :cond_9
    return-object v0

    :cond_a
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()"

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_b
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()"

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_c
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Callbacks must set parent bounds in populateNodeForVirtualViewId()"

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private g(I)Z
    .locals 2

    iget-object v0, p0, La/i/a/c;->k:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iget-object v0, p0, La/i/a/c;->k:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget v0, p0, La/i/a/c;->n:I

    if-eq v0, p1, :cond_2

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    invoke-direct {p0, v0}, La/i/a/c;->d(I)Z

    :cond_1
    iput p1, p0, La/i/a/c;->n:I

    iget-object v0, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    const v0, 0x8000

    invoke-virtual {p0, p1, v0}, La/i/a/c;->b(II)Z

    const/4 p1, 0x1

    return p1

    :cond_2
    :goto_0
    return v1
.end method

.method private h(I)V
    .locals 2

    iget v0, p0, La/i/a/c;->p:I

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iput p1, p0, La/i/a/c;->p:I

    const/16 v1, 0x80

    invoke-virtual {p0, p1, v1}, La/i/a/c;->b(II)Z

    const/16 p1, 0x100

    invoke-virtual {p0, v0, p1}, La/i/a/c;->b(II)Z

    return-void
.end method


# virtual methods
.method protected abstract a(FF)I
.end method

.method public a(Landroid/view/View;)Landroidx/core/view/accessibility/e;
    .locals 0

    iget-object p1, p0, La/i/a/c;->m:La/i/a/c$a;

    if-nez p1, :cond_0

    new-instance p1, La/i/a/c$a;

    invoke-direct {p1, p0}, La/i/a/c$a;-><init>(La/i/a/c;)V

    iput-object p1, p0, La/i/a/c;->m:La/i/a/c$a;

    :cond_0
    iget-object p1, p0, La/i/a/c;->m:La/i/a/c$a;

    return-object p1
.end method

.method public final a(II)V
    .locals 2

    const/high16 v0, -0x80000000

    if-eq p1, v0, :cond_0

    iget-object v0, p0, La/i/a/c;->k:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v1, 0x800

    invoke-direct {p0, p1, v1}, La/i/a/c;->c(II)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object p1

    invoke-static {p1, p2}, Landroidx/core/view/accessibility/AccessibilityEventCompat;->a(Landroid/view/accessibility/AccessibilityEvent;I)V

    iget-object p2, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-static {v0, p2, p1}, Landroidx/core/view/I;->a(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    :cond_0
    return-void
.end method

.method protected abstract a(ILandroid/view/accessibility/AccessibilityEvent;)V
    .param p2    # Landroid/view/accessibility/AccessibilityEvent;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method protected abstract a(ILandroidx/core/view/accessibility/d;)V
    .param p2    # Landroidx/core/view/accessibility/d;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
.end method

.method protected a(IZ)V
    .locals 0

    return-void
.end method

.method public a(Landroid/view/View;Landroidx/core/view/accessibility/d;)V
    .locals 0

    invoke-super {p0, p1, p2}, Landroidx/core/view/a;->a(Landroid/view/View;Landroidx/core/view/accessibility/d;)V

    invoke-virtual {p0, p2}, La/i/a/c;->a(Landroidx/core/view/accessibility/d;)V

    return-void
.end method

.method protected a(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 0
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method

.method protected a(Landroidx/core/view/accessibility/d;)V
    .locals 0
    .param p1    # Landroidx/core/view/accessibility/d;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    return-void
.end method

.method protected abstract a(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method

.method public final a(I)Z
    .locals 2

    iget v0, p0, La/i/a/c;->o:I

    const/4 v1, 0x0

    if-eq v0, p1, :cond_0

    return v1

    :cond_0
    const/high16 v0, -0x80000000

    iput v0, p0, La/i/a/c;->o:I

    invoke-virtual {p0, p1, v1}, La/i/a/c;->a(IZ)V

    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, La/i/a/c;->b(II)Z

    const/4 p1, 0x1

    return p1
.end method

.method protected abstract a(IILandroid/os/Bundle;)Z
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, La/i/a/c;->k:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    iget-object v0, p0, La/i/a/c;->k:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v2, 0x7

    const/4 v3, 0x1

    const/high16 v4, -0x80000000

    if-eq v0, v2, :cond_3

    const/16 v2, 0x9

    if-eq v0, v2, :cond_3

    const/16 p1, 0xa

    if-eq v0, p1, :cond_1

    return v1

    :cond_1
    iget p1, p0, La/i/a/c;->p:I

    if-eq p1, v4, :cond_2

    invoke-direct {p0, v4}, La/i/a/c;->h(I)V

    return v3

    :cond_2
    return v1

    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    invoke-virtual {p0, v0, p1}, La/i/a/c;->a(FF)I

    move-result p1

    invoke-direct {p0, p1}, La/i/a/c;->h(I)V

    if-eq p1, v4, :cond_4

    move v1, v3

    :cond_4
    :goto_0
    return v1
.end method

.method b(I)Landroidx/core/view/accessibility/d;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    goto/32 :goto_4

    nop

    :goto_0
    invoke-direct {p0}, La/i/a/c;->c()Landroidx/core/view/accessibility/d;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_1
    return-object p1

    :goto_2
    goto/32 :goto_3

    nop

    :goto_3
    invoke-direct {p0, p1}, La/i/a/c;->f(I)Landroidx/core/view/accessibility/d;

    move-result-object p1

    goto/32 :goto_6

    nop

    :goto_4
    const/4 v0, -0x1

    goto/32 :goto_5

    nop

    :goto_5
    if-eq p1, v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_0

    nop

    :goto_6
    return-object p1
.end method

.method public final b()V
    .locals 2

    const/4 v0, -0x1

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, La/i/a/c;->a(II)V

    return-void
.end method

.method public b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 0

    invoke-super {p0, p1, p2}, Landroidx/core/view/a;->b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    invoke-virtual {p0, p2}, La/i/a/c;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    return-void
.end method

.method public final b(II)Z
    .locals 2

    const/4 v0, 0x0

    const/high16 v1, -0x80000000

    if-eq p1, v1, :cond_2

    iget-object v1, p0, La/i/a/c;->k:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_1

    return v0

    :cond_1
    invoke-direct {p0, p1, p2}, La/i/a/c;->c(II)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object p1

    iget-object p2, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-static {v1, p2, p1}, Landroidx/core/view/I;->a(Landroid/view/ViewParent;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result p1

    return p1

    :cond_2
    :goto_0
    return v0
.end method

.method b(IILandroid/os/Bundle;)Z
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    if-ne p1, v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_2

    nop

    :goto_1
    return p1

    :goto_2
    invoke-direct {p0, p1, p2, p3}, La/i/a/c;->c(IILandroid/os/Bundle;)Z

    move-result p1

    goto/32 :goto_5

    nop

    :goto_3
    const/4 v0, -0x1

    goto/32 :goto_0

    nop

    :goto_4
    invoke-direct {p0, p2, p3}, La/i/a/c;->a(ILandroid/os/Bundle;)Z

    move-result p1

    goto/32 :goto_1

    nop

    :goto_5
    return p1

    :goto_6
    goto/32 :goto_4

    nop
.end method

.method public final c(I)Z
    .locals 2

    iget-object v0, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    iget-object v0, p0, La/i/a/c;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    move-result v0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget v0, p0, La/i/a/c;->o:I

    if-ne v0, p1, :cond_1

    return v1

    :cond_1
    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    invoke-virtual {p0, v0}, La/i/a/c;->a(I)Z

    :cond_2
    iput p1, p0, La/i/a/c;->o:I

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, La/i/a/c;->a(IZ)V

    const/16 v1, 0x8

    invoke-virtual {p0, p1, v1}, La/i/a/c;->b(II)Z

    return v0
.end method
