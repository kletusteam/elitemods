.class La/a/d/h;
.super Landroidx/core/view/N;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = La/a/d/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private a:Z

.field private b:I

.field final synthetic c:La/a/d/i;


# direct methods
.method constructor <init>(La/a/d/i;)V
    .locals 0

    iput-object p1, p0, La/a/d/h;->c:La/a/d/i;

    invoke-direct {p0}, Landroidx/core/view/N;-><init>()V

    const/4 p1, 0x0

    iput-boolean p1, p0, La/a/d/h;->a:Z

    iput p1, p0, La/a/d/h;->b:I

    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    goto/32 :goto_5

    nop

    :goto_0
    invoke-virtual {v0}, La/a/d/i;->b()V

    goto/32 :goto_3

    nop

    :goto_1
    iget-object v0, p0, La/a/d/h;->c:La/a/d/i;

    goto/32 :goto_0

    nop

    :goto_2
    iput-boolean v0, p0, La/a/d/h;->a:Z

    goto/32 :goto_1

    nop

    :goto_3
    return-void

    :goto_4
    iput v0, p0, La/a/d/h;->b:I

    goto/32 :goto_2

    nop

    :goto_5
    const/4 v0, 0x0

    goto/32 :goto_4

    nop
.end method

.method public onAnimationEnd(Landroid/view/View;)V
    .locals 1

    iget p1, p0, La/a/d/h;->b:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, La/a/d/h;->b:I

    iget-object v0, p0, La/a/d/h;->c:La/a/d/i;

    iget-object v0, v0, La/a/d/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne p1, v0, :cond_1

    iget-object p1, p0, La/a/d/h;->c:La/a/d/i;

    iget-object p1, p1, La/a/d/i;->d:Landroidx/core/view/M;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroidx/core/view/M;->onAnimationEnd(Landroid/view/View;)V

    :cond_0
    invoke-virtual {p0}, La/a/d/h;->a()V

    :cond_1
    return-void
.end method

.method public onAnimationStart(Landroid/view/View;)V
    .locals 1

    iget-boolean p1, p0, La/a/d/h;->a:Z

    if-eqz p1, :cond_0

    return-void

    :cond_0
    const/4 p1, 0x1

    iput-boolean p1, p0, La/a/d/h;->a:Z

    iget-object p1, p0, La/a/d/h;->c:La/a/d/i;

    iget-object p1, p1, La/a/d/i;->d:Landroidx/core/view/M;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroidx/core/view/M;->onAnimationStart(Landroid/view/View;)V

    :cond_1
    return-void
.end method
