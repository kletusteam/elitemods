.class public La/a/d/i;
.super Ljava/lang/Object;


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$a;->c:Landroidx/annotation/RestrictTo$a;
    }
.end annotation


# instance fields
.field final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroidx/core/view/L;",
            ">;"
        }
    .end annotation
.end field

.field private b:J

.field private c:Landroid/view/animation/Interpolator;

.field d:Landroidx/core/view/M;

.field private e:Z

.field private final f:Landroidx/core/view/N;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, La/a/d/i;->b:J

    new-instance v0, La/a/d/h;

    invoke-direct {v0, p0}, La/a/d/h;-><init>(La/a/d/i;)V

    iput-object v0, p0, La/a/d/i;->f:Landroidx/core/view/N;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, La/a/d/i;->a:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public a(J)La/a/d/i;
    .locals 1

    iget-boolean v0, p0, La/a/d/i;->e:Z

    if-nez v0, :cond_0

    iput-wide p1, p0, La/a/d/i;->b:J

    :cond_0
    return-object p0
.end method

.method public a(Landroid/view/animation/Interpolator;)La/a/d/i;
    .locals 1

    iget-boolean v0, p0, La/a/d/i;->e:Z

    if-nez v0, :cond_0

    iput-object p1, p0, La/a/d/i;->c:Landroid/view/animation/Interpolator;

    :cond_0
    return-object p0
.end method

.method public a(Landroidx/core/view/L;)La/a/d/i;
    .locals 1

    iget-boolean v0, p0, La/a/d/i;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, La/a/d/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0
.end method

.method public a(Landroidx/core/view/L;Landroidx/core/view/L;)La/a/d/i;
    .locals 2

    iget-object v0, p0, La/a/d/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Landroidx/core/view/L;->b()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Landroidx/core/view/L;->b(J)Landroidx/core/view/L;

    iget-object p1, p0, La/a/d/i;->a:Ljava/util/ArrayList;

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public a(Landroidx/core/view/M;)La/a/d/i;
    .locals 1

    iget-boolean v0, p0, La/a/d/i;->e:Z

    if-nez v0, :cond_0

    iput-object p1, p0, La/a/d/i;->d:Landroidx/core/view/M;

    :cond_0
    return-object p0
.end method

.method public a()V
    .locals 2

    iget-boolean v0, p0, La/a/d/i;->e:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, La/a/d/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroidx/core/view/L;

    invoke-virtual {v1}, Landroidx/core/view/L;->a()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, La/a/d/i;->e:Z

    return-void
.end method

.method b()V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    iput-boolean v0, p0, La/a/d/i;->e:Z

    goto/32 :goto_0

    nop

    :goto_2
    const/4 v0, 0x0

    goto/32 :goto_1

    nop
.end method

.method public c()V
    .locals 6

    iget-boolean v0, p0, La/a/d/i;->e:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, La/a/d/i;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroidx/core/view/L;

    iget-wide v2, p0, La/a/d/i;->b:J

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-ltz v4, :cond_1

    invoke-virtual {v1, v2, v3}, Landroidx/core/view/L;->a(J)Landroidx/core/view/L;

    :cond_1
    iget-object v2, p0, La/a/d/i;->c:Landroid/view/animation/Interpolator;

    if-eqz v2, :cond_2

    invoke-virtual {v1, v2}, Landroidx/core/view/L;->a(Landroid/view/animation/Interpolator;)Landroidx/core/view/L;

    :cond_2
    iget-object v2, p0, La/a/d/i;->d:Landroidx/core/view/M;

    if-eqz v2, :cond_3

    iget-object v2, p0, La/a/d/i;->f:Landroidx/core/view/N;

    invoke-virtual {v1, v2}, Landroidx/core/view/L;->a(Landroidx/core/view/M;)Landroidx/core/view/L;

    :cond_3
    invoke-virtual {v1}, Landroidx/core/view/L;->c()V

    goto :goto_0

    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, La/a/d/i;->e:Z

    return-void
.end method
