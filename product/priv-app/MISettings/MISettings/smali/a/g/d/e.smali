.class public final La/g/d/e;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La/g/d/e$f;,
        La/g/d/e$a;,
        La/g/d/e$b;,
        La/g/d/e$c;,
        La/g/d/e$e;,
        La/g/d/e$d;
    }
.end annotation


# static fields
.field public static final a:La/g/d/d;

.field public static final b:La/g/d/d;

.field public static final c:La/g/d/d;

.field public static final d:La/g/d/d;

.field public static final e:La/g/d/d;

.field public static final f:La/g/d/d;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, La/g/d/e$e;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, La/g/d/e$e;-><init>(La/g/d/e$c;Z)V

    sput-object v0, La/g/d/e;->a:La/g/d/d;

    new-instance v0, La/g/d/e$e;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, La/g/d/e$e;-><init>(La/g/d/e$c;Z)V

    sput-object v0, La/g/d/e;->b:La/g/d/d;

    new-instance v0, La/g/d/e$e;

    sget-object v1, La/g/d/e$b;->a:La/g/d/e$b;

    invoke-direct {v0, v1, v2}, La/g/d/e$e;-><init>(La/g/d/e$c;Z)V

    sput-object v0, La/g/d/e;->c:La/g/d/d;

    new-instance v0, La/g/d/e$e;

    sget-object v1, La/g/d/e$b;->a:La/g/d/e$b;

    invoke-direct {v0, v1, v3}, La/g/d/e$e;-><init>(La/g/d/e$c;Z)V

    sput-object v0, La/g/d/e;->d:La/g/d/d;

    new-instance v0, La/g/d/e$e;

    sget-object v1, La/g/d/e$a;->a:La/g/d/e$a;

    invoke-direct {v0, v1, v2}, La/g/d/e$e;-><init>(La/g/d/e$c;Z)V

    sput-object v0, La/g/d/e;->e:La/g/d/d;

    sget-object v0, La/g/d/e$f;->b:La/g/d/e$f;

    sput-object v0, La/g/d/e;->f:La/g/d/d;

    return-void
.end method

.method static a(I)I
    .locals 1

    const/4 v0, 0x1

    if-eqz p0, :cond_1

    if-eq p0, v0, :cond_0

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    return v0

    :cond_0
    const/4 p0, 0x0

    return p0

    :cond_1
    return v0
.end method

.method static b(I)I
    .locals 2

    const/4 v0, 0x1

    if-eqz p0, :cond_1

    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-eq p0, v1, :cond_0

    packed-switch p0, :pswitch_data_0

    return v1

    :cond_0
    :pswitch_0
    const/4 p0, 0x0

    return p0

    :cond_1
    :pswitch_1
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
