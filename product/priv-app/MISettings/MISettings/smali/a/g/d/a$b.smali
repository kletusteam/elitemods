.class La/g/d/a$b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = La/g/d/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# static fields
.field private static final a:[B


# instance fields
.field private final b:Ljava/lang/CharSequence;

.field private final c:Z

.field private final d:I

.field private e:I

.field private f:C


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x700

    new-array v1, v0, [B

    sput-object v1, La/g/d/a$b;->a:[B

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    sget-object v2, La/g/d/a$b;->a:[B

    invoke-static {v1}, Ljava/lang/Character;->getDirectionality(I)B

    move-result v3

    aput-byte v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method constructor <init>(Ljava/lang/CharSequence;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, La/g/d/a$b;->b:Ljava/lang/CharSequence;

    iput-boolean p2, p0, La/g/d/a$b;->c:Z

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    iput p1, p0, La/g/d/a$b;->d:I

    return-void
.end method

.method private static a(C)B
    .locals 1

    const/16 v0, 0x700

    if-ge p0, v0, :cond_0

    sget-object v0, La/g/d/a$b;->a:[B

    aget-byte p0, v0, p0

    goto :goto_0

    :cond_0
    invoke-static {p0}, Ljava/lang/Character;->getDirectionality(C)B

    move-result p0

    :goto_0
    return p0
.end method

.method private e()B
    .locals 4

    iget v0, p0, La/g/d/a$b;->e:I

    :cond_0
    iget v1, p0, La/g/d/a$b;->e:I

    const/16 v2, 0x3b

    if-lez v1, :cond_2

    iget-object v3, p0, La/g/d/a$b;->b:Ljava/lang/CharSequence;

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, La/g/d/a$b;->e:I

    invoke-interface {v3, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    iput-char v1, p0, La/g/d/a$b;->f:C

    iget-char v1, p0, La/g/d/a$b;->f:C

    const/16 v3, 0x26

    if-ne v1, v3, :cond_1

    const/16 v0, 0xc

    return v0

    :cond_1
    if-ne v1, v2, :cond_0

    :cond_2
    iput v0, p0, La/g/d/a$b;->e:I

    iput-char v2, p0, La/g/d/a$b;->f:C

    const/16 v0, 0xd

    return v0
.end method

.method private f()B
    .locals 3

    :goto_0
    iget v0, p0, La/g/d/a$b;->e:I

    iget v1, p0, La/g/d/a$b;->d:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, La/g/d/a$b;->b:Ljava/lang/CharSequence;

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, La/g/d/a$b;->e:I

    invoke-interface {v1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    iput-char v0, p0, La/g/d/a$b;->f:C

    const/16 v1, 0x3b

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/16 v0, 0xc

    return v0
.end method

.method private g()B
    .locals 4

    iget v0, p0, La/g/d/a$b;->e:I

    :cond_0
    iget v1, p0, La/g/d/a$b;->e:I

    const/16 v2, 0x3e

    if-lez v1, :cond_4

    iget-object v3, p0, La/g/d/a$b;->b:Ljava/lang/CharSequence;

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, La/g/d/a$b;->e:I

    invoke-interface {v3, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    iput-char v1, p0, La/g/d/a$b;->f:C

    iget-char v1, p0, La/g/d/a$b;->f:C

    const/16 v3, 0x3c

    if-ne v1, v3, :cond_1

    const/16 v0, 0xc

    return v0

    :cond_1
    if-ne v1, v2, :cond_2

    goto :goto_1

    :cond_2
    const/16 v2, 0x22

    if-eq v1, v2, :cond_3

    const/16 v2, 0x27

    if-ne v1, v2, :cond_0

    :cond_3
    iget-char v1, p0, La/g/d/a$b;->f:C

    :goto_0
    iget v2, p0, La/g/d/a$b;->e:I

    if-lez v2, :cond_0

    iget-object v3, p0, La/g/d/a$b;->b:Ljava/lang/CharSequence;

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, La/g/d/a$b;->e:I

    invoke-interface {v3, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    iput-char v2, p0, La/g/d/a$b;->f:C

    if-eq v2, v1, :cond_0

    goto :goto_0

    :cond_4
    :goto_1
    iput v0, p0, La/g/d/a$b;->e:I

    iput-char v2, p0, La/g/d/a$b;->f:C

    const/16 v0, 0xd

    return v0
.end method

.method private h()B
    .locals 5

    iget v0, p0, La/g/d/a$b;->e:I

    :cond_0
    iget v1, p0, La/g/d/a$b;->e:I

    iget v2, p0, La/g/d/a$b;->d:I

    if-ge v1, v2, :cond_3

    iget-object v2, p0, La/g/d/a$b;->b:Ljava/lang/CharSequence;

    add-int/lit8 v3, v1, 0x1

    iput v3, p0, La/g/d/a$b;->e:I

    invoke-interface {v2, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    iput-char v1, p0, La/g/d/a$b;->f:C

    iget-char v1, p0, La/g/d/a$b;->f:C

    const/16 v2, 0x3e

    if-ne v1, v2, :cond_1

    const/16 v0, 0xc

    return v0

    :cond_1
    const/16 v2, 0x22

    if-eq v1, v2, :cond_2

    const/16 v2, 0x27

    if-ne v1, v2, :cond_0

    :cond_2
    iget-char v1, p0, La/g/d/a$b;->f:C

    :goto_0
    iget v2, p0, La/g/d/a$b;->e:I

    iget v3, p0, La/g/d/a$b;->d:I

    if-ge v2, v3, :cond_0

    iget-object v3, p0, La/g/d/a$b;->b:Ljava/lang/CharSequence;

    add-int/lit8 v4, v2, 0x1

    iput v4, p0, La/g/d/a$b;->e:I

    invoke-interface {v3, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    iput-char v2, p0, La/g/d/a$b;->f:C

    if-eq v2, v1, :cond_0

    goto :goto_0

    :cond_3
    iput v0, p0, La/g/d/a$b;->e:I

    const/16 v0, 0x3c

    iput-char v0, p0, La/g/d/a$b;->f:C

    const/16 v0, 0xd

    return v0
.end method


# virtual methods
.method a()B
    .locals 3

    goto/32 :goto_5

    nop

    :goto_0
    iget-char v1, p0, La/g/d/a$b;->f:C

    goto/32 :goto_1f

    nop

    :goto_1
    iget-object v0, p0, La/g/d/a$b;->b:Ljava/lang/CharSequence;

    goto/32 :goto_6

    nop

    :goto_2
    return v0

    :goto_3
    sub-int/2addr v1, v2

    goto/32 :goto_20

    nop

    :goto_4
    const/16 v2, 0x3b

    goto/32 :goto_15

    nop

    :goto_5
    iget-object v0, p0, La/g/d/a$b;->b:Ljava/lang/CharSequence;

    goto/32 :goto_10

    nop

    :goto_6
    iget v1, p0, La/g/d/a$b;->e:I

    goto/32 :goto_f

    nop

    :goto_7
    invoke-static {v0}, Ljava/lang/Character;->getDirectionality(I)B

    move-result v0

    goto/32 :goto_c

    nop

    :goto_8
    invoke-static {v0}, Ljava/lang/Character;->isLowSurrogate(C)Z

    move-result v0

    goto/32 :goto_13

    nop

    :goto_9
    iget-char v0, p0, La/g/d/a$b;->f:C

    goto/32 :goto_8

    nop

    :goto_a
    if-eq v1, v2, :cond_0

    goto/32 :goto_1d

    :cond_0
    goto/32 :goto_19

    nop

    :goto_b
    if-nez v1, :cond_1

    goto/32 :goto_18

    :cond_1
    goto/32 :goto_0

    nop

    :goto_c
    return v0

    :goto_d
    goto/32 :goto_e

    nop

    :goto_e
    iget v0, p0, La/g/d/a$b;->e:I

    goto/32 :goto_11

    nop

    :goto_f
    invoke-static {v0, v1}, Ljava/lang/Character;->codePointBefore(Ljava/lang/CharSequence;I)I

    move-result v0

    goto/32 :goto_1a

    nop

    :goto_10
    iget v1, p0, La/g/d/a$b;->e:I

    goto/32 :goto_1b

    nop

    :goto_11
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_16

    nop

    :goto_12
    iput-char v0, p0, La/g/d/a$b;->f:C

    goto/32 :goto_9

    nop

    :goto_13
    if-nez v0, :cond_2

    goto/32 :goto_d

    :cond_2
    goto/32 :goto_1

    nop

    :goto_14
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v2

    goto/32 :goto_3

    nop

    :goto_15
    if-eq v1, v2, :cond_3

    goto/32 :goto_18

    :cond_3
    goto/32 :goto_17

    nop

    :goto_16
    iput v0, p0, La/g/d/a$b;->e:I

    goto/32 :goto_1e

    nop

    :goto_17
    invoke-direct {p0}, La/g/d/a$b;->e()B

    move-result v0

    :goto_18
    goto/32 :goto_2

    nop

    :goto_19
    invoke-direct {p0}, La/g/d/a$b;->g()B

    move-result v0

    goto/32 :goto_1c

    nop

    :goto_1a
    iget v1, p0, La/g/d/a$b;->e:I

    goto/32 :goto_14

    nop

    :goto_1b
    add-int/lit8 v1, v1, -0x1

    goto/32 :goto_22

    nop

    :goto_1c
    goto :goto_18

    :goto_1d
    goto/32 :goto_4

    nop

    :goto_1e
    iget-char v0, p0, La/g/d/a$b;->f:C

    goto/32 :goto_21

    nop

    :goto_1f
    const/16 v2, 0x3e

    goto/32 :goto_a

    nop

    :goto_20
    iput v1, p0, La/g/d/a$b;->e:I

    goto/32 :goto_7

    nop

    :goto_21
    invoke-static {v0}, La/g/d/a$b;->a(C)B

    move-result v0

    goto/32 :goto_23

    nop

    :goto_22
    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    goto/32 :goto_12

    nop

    :goto_23
    iget-boolean v1, p0, La/g/d/a$b;->c:Z

    goto/32 :goto_b

    nop
.end method

.method b()B
    .locals 3

    goto/32 :goto_17

    nop

    :goto_0
    return v0

    :goto_1
    goto/32 :goto_1f

    nop

    :goto_2
    iget-object v0, p0, La/g/d/a$b;->b:Ljava/lang/CharSequence;

    goto/32 :goto_7

    nop

    :goto_3
    iget v1, p0, La/g/d/a$b;->e:I

    goto/32 :goto_1c

    nop

    :goto_4
    invoke-direct {p0}, La/g/d/a$b;->h()B

    move-result v0

    goto/32 :goto_c

    nop

    :goto_5
    return v0

    :goto_6
    iget-char v1, p0, La/g/d/a$b;->f:C

    goto/32 :goto_20

    nop

    :goto_7
    iget v1, p0, La/g/d/a$b;->e:I

    goto/32 :goto_1d

    nop

    :goto_8
    iput-char v0, p0, La/g/d/a$b;->f:C

    goto/32 :goto_1a

    nop

    :goto_9
    invoke-static {v0}, La/g/d/a$b;->a(C)B

    move-result v0

    goto/32 :goto_19

    nop

    :goto_a
    iget v1, p0, La/g/d/a$b;->e:I

    goto/32 :goto_f

    nop

    :goto_b
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_2

    nop

    :goto_c
    goto :goto_12

    :goto_d
    goto/32 :goto_13

    nop

    :goto_e
    iget-char v0, p0, La/g/d/a$b;->f:C

    goto/32 :goto_9

    nop

    :goto_f
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v2

    goto/32 :goto_1b

    nop

    :goto_10
    if-nez v1, :cond_1

    goto/32 :goto_12

    :cond_1
    goto/32 :goto_6

    nop

    :goto_11
    invoke-direct {p0}, La/g/d/a$b;->f()B

    move-result v0

    :goto_12
    goto/32 :goto_5

    nop

    :goto_13
    const/16 v2, 0x26

    goto/32 :goto_1e

    nop

    :goto_14
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_18

    nop

    :goto_15
    invoke-static {v0}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v0

    goto/32 :goto_b

    nop

    :goto_16
    iput v1, p0, La/g/d/a$b;->e:I

    goto/32 :goto_22

    nop

    :goto_17
    iget-object v0, p0, La/g/d/a$b;->b:Ljava/lang/CharSequence;

    goto/32 :goto_3

    nop

    :goto_18
    iput v0, p0, La/g/d/a$b;->e:I

    goto/32 :goto_e

    nop

    :goto_19
    iget-boolean v1, p0, La/g/d/a$b;->c:Z

    goto/32 :goto_10

    nop

    :goto_1a
    iget-char v0, p0, La/g/d/a$b;->f:C

    goto/32 :goto_15

    nop

    :goto_1b
    add-int/2addr v1, v2

    goto/32 :goto_16

    nop

    :goto_1c
    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    goto/32 :goto_8

    nop

    :goto_1d
    invoke-static {v0, v1}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v0

    goto/32 :goto_a

    nop

    :goto_1e
    if-eq v1, v2, :cond_2

    goto/32 :goto_12

    :cond_2
    goto/32 :goto_11

    nop

    :goto_1f
    iget v0, p0, La/g/d/a$b;->e:I

    goto/32 :goto_14

    nop

    :goto_20
    const/16 v2, 0x3c

    goto/32 :goto_21

    nop

    :goto_21
    if-eq v1, v2, :cond_3

    goto/32 :goto_d

    :cond_3
    goto/32 :goto_4

    nop

    :goto_22
    invoke-static {v0}, Ljava/lang/Character;->getDirectionality(I)B

    move-result v0

    goto/32 :goto_0

    nop
.end method

.method c()I
    .locals 8

    goto/32 :goto_10

    nop

    :goto_0
    goto :goto_d

    :pswitch_0
    goto/32 :goto_16

    nop

    :goto_1
    move v4, v3

    goto/32 :goto_c

    nop

    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_1a

    nop

    :goto_3
    goto :goto_d

    :pswitch_1
    goto/32 :goto_1f

    nop

    :goto_4
    move v4, v2

    goto/32 :goto_0

    nop

    :goto_5
    if-eqz v3, :cond_0

    goto/32 :goto_3a

    :cond_0
    goto/32 :goto_9

    nop

    :goto_6
    iput v0, p0, La/g/d/a$b;->e:I

    goto/32 :goto_8

    nop

    :goto_7
    add-int/lit8 v5, v5, -0x1

    goto/32 :goto_30

    nop

    :goto_8
    const/4 v1, -0x1

    goto/32 :goto_2b

    nop

    :goto_9
    invoke-virtual {p0}, La/g/d/a$b;->b()B

    move-result v6

    goto/32 :goto_36

    nop

    :goto_a
    move v3, v5

    goto/32 :goto_39

    nop

    :goto_b
    return v0

    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0xe
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_5
    .end packed-switch

    :goto_c
    move v5, v4

    :goto_d
    goto/32 :goto_11

    nop

    :goto_e
    return v1

    :goto_f
    goto/32 :goto_33

    nop

    :goto_10
    const/4 v0, 0x0

    goto/32 :goto_6

    nop

    :goto_11
    iget v6, p0, La/g/d/a$b;->e:I

    goto/32 :goto_1e

    nop

    :goto_12
    return v2

    :goto_13
    goto/32 :goto_17

    nop

    :goto_14
    if-eq v3, v5, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_15

    nop

    :goto_15
    return v2

    :pswitch_2
    goto/32 :goto_18

    nop

    :goto_16
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_32

    nop

    :goto_17
    if-eqz v5, :cond_2

    goto/32 :goto_29

    :cond_2
    goto/32 :goto_28

    nop

    :goto_18
    if-eq v3, v5, :cond_3

    goto/32 :goto_f

    :cond_3
    goto/32 :goto_e

    nop

    :goto_19
    const/4 v7, 0x2

    goto/32 :goto_31

    nop

    :goto_1a
    goto :goto_1d

    :pswitch_3
    goto/32 :goto_14

    nop

    :goto_1b
    if-gtz v4, :cond_4

    goto/32 :goto_2d

    :cond_4
    goto/32 :goto_3b

    nop

    :goto_1c
    return v4

    :goto_1d
    goto/32 :goto_2a

    nop

    :goto_1e
    iget v7, p0, La/g/d/a$b;->d:I

    goto/32 :goto_21

    nop

    :goto_1f
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_4

    nop

    :goto_20
    if-eqz v3, :cond_5

    goto/32 :goto_24

    :cond_5
    goto/32 :goto_23

    nop

    :goto_21
    if-lt v6, v7, :cond_6

    goto/32 :goto_3a

    :cond_6
    goto/32 :goto_5

    nop

    :goto_22
    move v3, v0

    goto/32 :goto_1

    nop

    :goto_23
    return v0

    :goto_24
    goto/32 :goto_34

    nop

    :goto_25
    goto :goto_29

    :pswitch_4
    goto/32 :goto_7

    nop

    :goto_26
    goto :goto_1d

    :pswitch_5
    goto/32 :goto_2

    nop

    :goto_27
    if-eqz v5, :cond_7

    goto/32 :goto_29

    :cond_7
    goto/32 :goto_12

    nop

    :goto_28
    return v1

    :goto_29
    goto/32 :goto_a

    nop

    :goto_2a
    iget v4, p0, La/g/d/a$b;->e:I

    goto/32 :goto_1b

    nop

    :goto_2b
    const/4 v2, 0x1

    goto/32 :goto_22

    nop

    :goto_2c
    goto :goto_1d

    :goto_2d
    goto/32 :goto_b

    nop

    :goto_2e
    goto/16 :goto_d

    :goto_2f
    goto/32 :goto_27

    nop

    :goto_30
    move v4, v0

    goto/32 :goto_3

    nop

    :goto_31
    if-ne v6, v7, :cond_8

    goto/32 :goto_2f

    :cond_8
    goto/32 :goto_37

    nop

    :goto_32
    move v4, v1

    goto/32 :goto_2e

    nop

    :goto_33
    add-int/lit8 v5, v5, -0x1

    goto/32 :goto_2c

    nop

    :goto_34
    if-nez v4, :cond_9

    goto/32 :goto_1d

    :cond_9
    goto/32 :goto_1c

    nop

    :goto_35
    if-ne v6, v7, :cond_a

    goto/32 :goto_d

    :cond_a
    packed-switch v6, :pswitch_data_0

    goto/32 :goto_25

    nop

    :goto_36
    if-nez v6, :cond_b

    goto/32 :goto_13

    :cond_b
    goto/32 :goto_38

    nop

    :goto_37
    const/16 v7, 0x9

    goto/32 :goto_35

    nop

    :goto_38
    if-ne v6, v2, :cond_c

    goto/32 :goto_2f

    :cond_c
    goto/32 :goto_19

    nop

    :goto_39
    goto/16 :goto_d

    :goto_3a
    goto/32 :goto_20

    nop

    :goto_3b
    invoke-virtual {p0}, La/g/d/a$b;->a()B

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto/32 :goto_26

    nop
.end method

.method d()I
    .locals 7

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, La/g/d/a$b;->d:I

    goto/32 :goto_29

    nop

    :goto_1
    if-eqz v1, :cond_0

    goto/32 :goto_17

    :cond_0
    goto/32 :goto_6

    nop

    :goto_2
    if-gtz v3, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_12

    nop

    :goto_3
    if-eq v1, v2, :cond_2

    goto/32 :goto_d

    :cond_2
    goto/32 :goto_27

    nop

    :goto_4
    if-ne v3, v6, :cond_3

    goto/32 :goto_17

    :cond_3
    packed-switch v3, :pswitch_data_0

    goto/32 :goto_18

    nop

    :goto_5
    goto :goto_17

    :pswitch_0
    goto/32 :goto_3

    nop

    :goto_6
    goto :goto_1b

    :goto_7
    goto/32 :goto_2a

    nop

    :goto_8
    const/4 v5, 0x1

    goto/32 :goto_28

    nop

    :goto_9
    return v5

    :goto_a
    goto/32 :goto_1

    nop

    :goto_b
    const/4 v0, 0x0

    goto/32 :goto_26

    nop

    :goto_c
    return v4

    :goto_d
    goto/32 :goto_e

    nop

    :goto_e
    add-int/lit8 v2, v2, -0x1

    goto/32 :goto_13

    nop

    :goto_f
    goto :goto_17

    :goto_10
    goto/32 :goto_1e

    nop

    :goto_11
    const/16 v6, 0x9

    goto/32 :goto_4

    nop

    :goto_12
    invoke-virtual {p0}, La/g/d/a$b;->a()B

    move-result v3

    goto/32 :goto_19

    nop

    :goto_13
    goto :goto_17

    :goto_14
    goto/32 :goto_1d

    nop

    :goto_15
    const/4 v6, 0x2

    goto/32 :goto_25

    nop

    :goto_16
    move v2, v1

    :goto_17
    goto/32 :goto_22

    nop

    :goto_18
    if-eqz v1, :cond_4

    goto/32 :goto_17

    :cond_4
    goto/32 :goto_21

    nop

    :goto_19
    const/4 v4, -0x1

    goto/32 :goto_1c

    nop

    :goto_1a
    if-eqz v1, :cond_5

    goto/32 :goto_17

    :cond_5
    :goto_1b
    goto/32 :goto_2b

    nop

    :goto_1c
    if-nez v3, :cond_6

    goto/32 :goto_7

    :cond_6
    goto/32 :goto_8

    nop

    :goto_1d
    if-eqz v2, :cond_7

    goto/32 :goto_a

    :cond_7
    goto/32 :goto_9

    nop

    :goto_1e
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :goto_1f
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_5

    nop

    :goto_20
    if-eq v1, v2, :cond_8

    goto/32 :goto_d

    :cond_8
    goto/32 :goto_c

    nop

    :goto_21
    goto :goto_1b

    :pswitch_1
    goto/32 :goto_1f

    nop

    :goto_22
    iget v3, p0, La/g/d/a$b;->e:I

    goto/32 :goto_2

    nop

    :goto_23
    return v4

    :goto_24
    goto/32 :goto_1a

    nop

    :goto_25
    if-ne v3, v6, :cond_9

    goto/32 :goto_14

    :cond_9
    goto/32 :goto_11

    nop

    :goto_26
    move v1, v0

    goto/32 :goto_16

    nop

    :goto_27
    return v5

    :pswitch_2
    goto/32 :goto_20

    nop

    :goto_28
    if-ne v3, v5, :cond_a

    goto/32 :goto_14

    :cond_a
    goto/32 :goto_15

    nop

    :goto_29
    iput v0, p0, La/g/d/a$b;->e:I

    goto/32 :goto_b

    nop

    :goto_2a
    if-eqz v2, :cond_b

    goto/32 :goto_24

    :cond_b
    goto/32 :goto_23

    nop

    :goto_2b
    move v1, v2

    goto/32 :goto_f

    nop
.end method
