.class public La/e/b/a/c;
.super Landroidx/constraintlayout/motion/widget/r;


# instance fields
.field private a:La/e/a/a/a/o;

.field private b:La/e/a/a/a/l;

.field private c:La/e/a/a/a/n;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroidx/constraintlayout/motion/widget/r;-><init>()V

    new-instance v0, La/e/a/a/a/o;

    invoke-direct {v0}, La/e/a/a/a/o;-><init>()V

    iput-object v0, p0, La/e/b/a/c;->a:La/e/a/a/a/o;

    iget-object v0, p0, La/e/b/a/c;->a:La/e/a/a/a/o;

    iput-object v0, p0, La/e/b/a/c;->c:La/e/a/a/a/n;

    return-void
.end method


# virtual methods
.method public a()F
    .locals 1

    iget-object v0, p0, La/e/b/a/c;->c:La/e/a/a/a/n;

    invoke-interface {v0}, La/e/a/a/a/n;->b()F

    move-result v0

    return v0
.end method

.method public a(FFFFFF)V
    .locals 7

    iget-object v0, p0, La/e/b/a/c;->a:La/e/a/a/a/o;

    iput-object v0, p0, La/e/b/a/c;->c:La/e/a/a/a/n;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, La/e/a/a/a/o;->a(FFFFFF)V

    return-void
.end method

.method public a(FFFFFFFI)V
    .locals 11

    move-object v0, p0

    iget-object v1, v0, La/e/b/a/c;->b:La/e/a/a/a/l;

    if-nez v1, :cond_0

    new-instance v1, La/e/a/a/a/l;

    invoke-direct {v1}, La/e/a/a/a/l;-><init>()V

    iput-object v1, v0, La/e/b/a/c;->b:La/e/a/a/a/l;

    :cond_0
    iget-object v2, v0, La/e/b/a/c;->b:La/e/a/a/a/l;

    iput-object v2, v0, La/e/b/a/c;->c:La/e/a/a/a/n;

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    invoke-virtual/range {v2 .. v10}, La/e/a/a/a/l;->a(FFFFFFFI)V

    return-void
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, La/e/b/a/c;->c:La/e/a/a/a/n;

    invoke-interface {v0}, La/e/a/a/a/n;->a()Z

    move-result v0

    return v0
.end method

.method public getInterpolation(F)F
    .locals 1

    iget-object v0, p0, La/e/b/a/c;->c:La/e/a/a/a/n;

    invoke-interface {v0, p1}, La/e/a/a/a/n;->getInterpolation(F)F

    move-result p1

    return p1
.end method
