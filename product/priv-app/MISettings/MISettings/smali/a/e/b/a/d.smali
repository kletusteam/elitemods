.class public abstract La/e/b/a/d;
.super La/e/a/a/a/f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La/e/b/a/d$e;,
        La/e/b/a/d$b;,
        La/e/b/a/d$m;,
        La/e/b/a/d$l;,
        La/e/b/a/d$k;,
        La/e/b/a/d$j;,
        La/e/b/a/d$i;,
        La/e/b/a/d$d;,
        La/e/b/a/d$h;,
        La/e/b/a/d$g;,
        La/e/b/a/d$f;,
        La/e/b/a/d$a;,
        La/e/b/a/d$c;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, La/e/a/a/a/f;-><init>()V

    return-void
.end method

.method public static b(Ljava/lang/String;)La/e/b/a/d;
    .locals 2

    const-string v0, "CUSTOM"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p0, La/e/b/a/d$b;

    invoke-direct {p0}, La/e/b/a/d$b;-><init>()V

    return-object p0

    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    const-string v1, "waveOffset"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/16 v0, 0x8

    goto/16 :goto_0

    :sswitch_1
    const-string v1, "alpha"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v0, 0x0

    goto/16 :goto_0

    :sswitch_2
    const-string v1, "transitionPathRotate"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v0, 0x5

    goto/16 :goto_0

    :sswitch_3
    const-string v1, "elevation"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    goto/16 :goto_0

    :sswitch_4
    const-string v1, "rotation"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_5
    const-string v1, "waveVariesBy"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/16 v0, 0x9

    goto :goto_0

    :sswitch_6
    const-string v1, "scaleY"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v0, 0x7

    goto :goto_0

    :sswitch_7
    const-string v1, "scaleX"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v0, 0x6

    goto :goto_0

    :sswitch_8
    const-string v1, "progress"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/16 v0, 0xd

    goto :goto_0

    :sswitch_9
    const-string v1, "translationZ"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/16 v0, 0xc

    goto :goto_0

    :sswitch_a
    const-string v1, "translationY"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/16 v0, 0xb

    goto :goto_0

    :sswitch_b
    const-string v1, "translationX"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/16 v0, 0xa

    goto :goto_0

    :sswitch_c
    const-string v1, "rotationY"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_d
    const-string v1, "rotationX"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v0, 0x3

    :cond_1
    :goto_0
    packed-switch v0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    :pswitch_0
    new-instance p0, La/e/b/a/d$e;

    invoke-direct {p0}, La/e/b/a/d$e;-><init>()V

    return-object p0

    :pswitch_1
    new-instance p0, La/e/b/a/d$m;

    invoke-direct {p0}, La/e/b/a/d$m;-><init>()V

    return-object p0

    :pswitch_2
    new-instance p0, La/e/b/a/d$l;

    invoke-direct {p0}, La/e/b/a/d$l;-><init>()V

    return-object p0

    :pswitch_3
    new-instance p0, La/e/b/a/d$k;

    invoke-direct {p0}, La/e/b/a/d$k;-><init>()V

    return-object p0

    :pswitch_4
    new-instance p0, La/e/b/a/d$a;

    invoke-direct {p0}, La/e/b/a/d$a;-><init>()V

    return-object p0

    :pswitch_5
    new-instance p0, La/e/b/a/d$a;

    invoke-direct {p0}, La/e/b/a/d$a;-><init>()V

    return-object p0

    :pswitch_6
    new-instance p0, La/e/b/a/d$j;

    invoke-direct {p0}, La/e/b/a/d$j;-><init>()V

    return-object p0

    :pswitch_7
    new-instance p0, La/e/b/a/d$i;

    invoke-direct {p0}, La/e/b/a/d$i;-><init>()V

    return-object p0

    :pswitch_8
    new-instance p0, La/e/b/a/d$d;

    invoke-direct {p0}, La/e/b/a/d$d;-><init>()V

    return-object p0

    :pswitch_9
    new-instance p0, La/e/b/a/d$h;

    invoke-direct {p0}, La/e/b/a/d$h;-><init>()V

    return-object p0

    :pswitch_a
    new-instance p0, La/e/b/a/d$g;

    invoke-direct {p0}, La/e/b/a/d$g;-><init>()V

    return-object p0

    :pswitch_b
    new-instance p0, La/e/b/a/d$f;

    invoke-direct {p0}, La/e/b/a/d$f;-><init>()V

    return-object p0

    :pswitch_c
    new-instance p0, La/e/b/a/d$c;

    invoke-direct {p0}, La/e/b/a/d$c;-><init>()V

    return-object p0

    :pswitch_d
    new-instance p0, La/e/b/a/d$a;

    invoke-direct {p0}, La/e/b/a/d$a;-><init>()V

    return-object p0

    :sswitch_data_0
    .sparse-switch
        -0x4a771f66 -> :sswitch_d
        -0x4a771f65 -> :sswitch_c
        -0x490b9c39 -> :sswitch_b
        -0x490b9c38 -> :sswitch_a
        -0x490b9c37 -> :sswitch_9
        -0x3bab3dd3 -> :sswitch_8
        -0x3621dfb2 -> :sswitch_7
        -0x3621dfb1 -> :sswitch_6
        -0x2f893320 -> :sswitch_5
        -0x266f082 -> :sswitch_4
        -0x42d1a3 -> :sswitch_3
        0x2382115 -> :sswitch_2
        0x589b15e -> :sswitch_1
        0x94e04ec -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public abstract a(Landroid/view/View;F)V
.end method
