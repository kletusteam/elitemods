.class public abstract La/e/b/a/e;
.super La/e/a/a/a/k;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La/e/b/a/e$g;,
        La/e/b/a/e$b;,
        La/e/b/a/e$o;,
        La/e/b/a/e$n;,
        La/e/b/a/e$m;,
        La/e/b/a/e$l;,
        La/e/b/a/e$k;,
        La/e/b/a/e$d;,
        La/e/b/a/e$f;,
        La/e/b/a/e$e;,
        La/e/b/a/e$j;,
        La/e/b/a/e$i;,
        La/e/b/a/e$h;,
        La/e/b/a/e$a;,
        La/e/b/a/e$c;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, La/e/a/a/a/k;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/util/SparseArray;)La/e/b/a/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/util/SparseArray<",
            "Landroidx/constraintlayout/widget/b;",
            ">;)",
            "La/e/b/a/e;"
        }
    .end annotation

    new-instance v0, La/e/b/a/e$b;

    invoke-direct {v0, p0, p1}, La/e/b/a/e$b;-><init>(Ljava/lang/String;Landroid/util/SparseArray;)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;)La/e/b/a/e;
    .locals 1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    const-string v0, "waveOffset"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 p0, 0xa

    goto/16 :goto_1

    :sswitch_1
    const-string v0, "alpha"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x0

    goto/16 :goto_1

    :sswitch_2
    const-string v0, "transitionPathRotate"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x7

    goto/16 :goto_1

    :sswitch_3
    const-string v0, "elevation"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto/16 :goto_1

    :sswitch_4
    const-string v0, "rotation"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x2

    goto/16 :goto_1

    :sswitch_5
    const-string v0, "transformPivotY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x6

    goto/16 :goto_1

    :sswitch_6
    const-string v0, "transformPivotX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x5

    goto/16 :goto_1

    :sswitch_7
    const-string v0, "waveVariesBy"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 p0, 0xb

    goto :goto_1

    :sswitch_8
    const-string v0, "scaleY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 p0, 0x9

    goto :goto_1

    :sswitch_9
    const-string v0, "scaleX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 p0, 0x8

    goto :goto_1

    :sswitch_a
    const-string v0, "progress"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 p0, 0xf

    goto :goto_1

    :sswitch_b
    const-string v0, "translationZ"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 p0, 0xe

    goto :goto_1

    :sswitch_c
    const-string v0, "translationY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 p0, 0xd

    goto :goto_1

    :sswitch_d
    const-string v0, "translationX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 p0, 0xc

    goto :goto_1

    :sswitch_e
    const-string v0, "rotationY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x4

    goto :goto_1

    :sswitch_f
    const-string v0, "rotationX"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x3

    goto :goto_1

    :cond_0
    :goto_0
    const/4 p0, -0x1

    :goto_1
    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    :pswitch_0
    new-instance p0, La/e/b/a/e$g;

    invoke-direct {p0}, La/e/b/a/e$g;-><init>()V

    return-object p0

    :pswitch_1
    new-instance p0, La/e/b/a/e$o;

    invoke-direct {p0}, La/e/b/a/e$o;-><init>()V

    return-object p0

    :pswitch_2
    new-instance p0, La/e/b/a/e$n;

    invoke-direct {p0}, La/e/b/a/e$n;-><init>()V

    return-object p0

    :pswitch_3
    new-instance p0, La/e/b/a/e$m;

    invoke-direct {p0}, La/e/b/a/e$m;-><init>()V

    return-object p0

    :pswitch_4
    new-instance p0, La/e/b/a/e$a;

    invoke-direct {p0}, La/e/b/a/e$a;-><init>()V

    return-object p0

    :pswitch_5
    new-instance p0, La/e/b/a/e$a;

    invoke-direct {p0}, La/e/b/a/e$a;-><init>()V

    return-object p0

    :pswitch_6
    new-instance p0, La/e/b/a/e$l;

    invoke-direct {p0}, La/e/b/a/e$l;-><init>()V

    return-object p0

    :pswitch_7
    new-instance p0, La/e/b/a/e$k;

    invoke-direct {p0}, La/e/b/a/e$k;-><init>()V

    return-object p0

    :pswitch_8
    new-instance p0, La/e/b/a/e$d;

    invoke-direct {p0}, La/e/b/a/e$d;-><init>()V

    return-object p0

    :pswitch_9
    new-instance p0, La/e/b/a/e$f;

    invoke-direct {p0}, La/e/b/a/e$f;-><init>()V

    return-object p0

    :pswitch_a
    new-instance p0, La/e/b/a/e$e;

    invoke-direct {p0}, La/e/b/a/e$e;-><init>()V

    return-object p0

    :pswitch_b
    new-instance p0, La/e/b/a/e$j;

    invoke-direct {p0}, La/e/b/a/e$j;-><init>()V

    return-object p0

    :pswitch_c
    new-instance p0, La/e/b/a/e$i;

    invoke-direct {p0}, La/e/b/a/e$i;-><init>()V

    return-object p0

    :pswitch_d
    new-instance p0, La/e/b/a/e$h;

    invoke-direct {p0}, La/e/b/a/e$h;-><init>()V

    return-object p0

    :pswitch_e
    new-instance p0, La/e/b/a/e$c;

    invoke-direct {p0}, La/e/b/a/e$c;-><init>()V

    return-object p0

    :pswitch_f
    new-instance p0, La/e/b/a/e$a;

    invoke-direct {p0}, La/e/b/a/e$a;-><init>()V

    return-object p0

    :sswitch_data_0
    .sparse-switch
        -0x4a771f66 -> :sswitch_f
        -0x4a771f65 -> :sswitch_e
        -0x490b9c39 -> :sswitch_d
        -0x490b9c38 -> :sswitch_c
        -0x490b9c37 -> :sswitch_b
        -0x3bab3dd3 -> :sswitch_a
        -0x3621dfb2 -> :sswitch_9
        -0x3621dfb1 -> :sswitch_8
        -0x2f893320 -> :sswitch_7
        -0x2d5a2d1e -> :sswitch_6
        -0x2d5a2d1d -> :sswitch_5
        -0x266f082 -> :sswitch_4
        -0x42d1a3 -> :sswitch_3
        0x2382115 -> :sswitch_2
        0x589b15e -> :sswitch_1
        0x94e04ec -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public abstract a(Landroid/view/View;F)V
.end method
