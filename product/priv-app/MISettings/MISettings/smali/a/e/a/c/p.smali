.class public La/e/a/c/p;
.super La/e/a/c/m;


# instance fields
.field private Wa:I

.field private Xa:I

.field private Ya:I

.field private Za:I

.field private _a:I

.field private ab:I

.field private bb:I

.field private cb:I

.field private db:Z

.field private eb:I

.field private fb:I

.field protected gb:La/e/a/c/a/b$a;

.field hb:La/e/a/c/a/b$b;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, La/e/a/c/m;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, La/e/a/c/p;->Wa:I

    iput v0, p0, La/e/a/c/p;->Xa:I

    iput v0, p0, La/e/a/c/p;->Ya:I

    iput v0, p0, La/e/a/c/p;->Za:I

    iput v0, p0, La/e/a/c/p;->_a:I

    iput v0, p0, La/e/a/c/p;->ab:I

    iput v0, p0, La/e/a/c/p;->bb:I

    iput v0, p0, La/e/a/c/p;->cb:I

    iput-boolean v0, p0, La/e/a/c/p;->db:Z

    iput v0, p0, La/e/a/c/p;->eb:I

    iput v0, p0, La/e/a/c/p;->fb:I

    new-instance v0, La/e/a/c/a/b$a;

    invoke-direct {v0}, La/e/a/c/a/b$a;-><init>()V

    iput-object v0, p0, La/e/a/c/p;->gb:La/e/a/c/a/b$a;

    const/4 v0, 0x0

    iput-object v0, p0, La/e/a/c/p;->hb:La/e/a/c/a/b$b;

    return-void
.end method


# virtual methods
.method public A(I)V
    .locals 0

    iput p1, p0, La/e/a/c/p;->Xa:I

    return-void
.end method

.method public B(I)V
    .locals 0

    iput p1, p0, La/e/a/c/p;->ab:I

    return-void
.end method

.method public C(I)V
    .locals 0

    iput p1, p0, La/e/a/c/p;->Ya:I

    iput p1, p0, La/e/a/c/p;->bb:I

    return-void
.end method

.method public D(I)V
    .locals 0

    iput p1, p0, La/e/a/c/p;->Za:I

    iput p1, p0, La/e/a/c/p;->cb:I

    return-void
.end method

.method public E(I)V
    .locals 0

    iput p1, p0, La/e/a/c/p;->_a:I

    iput p1, p0, La/e/a/c/p;->bb:I

    iput p1, p0, La/e/a/c/p;->cb:I

    return-void
.end method

.method public F(I)V
    .locals 0

    iput p1, p0, La/e/a/c/p;->Wa:I

    return-void
.end method

.method public Y()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, La/e/a/c/m;->Va:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, La/e/a/c/m;->Ua:[La/e/a/c/g;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, La/e/a/c/g;->c(Z)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public Z()I
    .locals 1

    iget v0, p0, La/e/a/c/p;->fb:I

    return v0
.end method

.method protected a(La/e/a/c/g;La/e/a/c/g$a;ILa/e/a/c/g$a;I)V
    .locals 1

    :goto_0
    iget-object v0, p0, La/e/a/c/p;->hb:La/e/a/c/a/b$b;

    if-nez v0, :cond_0

    invoke-virtual {p0}, La/e/a/c/g;->v()La/e/a/c/g;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, La/e/a/c/g;->v()La/e/a/c/g;

    move-result-object v0

    check-cast v0, La/e/a/c/h;

    invoke-virtual {v0}, La/e/a/c/h;->ba()La/e/a/c/a/b$b;

    move-result-object v0

    iput-object v0, p0, La/e/a/c/p;->hb:La/e/a/c/a/b$b;

    goto :goto_0

    :cond_0
    iget-object v0, p0, La/e/a/c/p;->gb:La/e/a/c/a/b$a;

    iput-object p2, v0, La/e/a/c/a/b$a;->d:La/e/a/c/g$a;

    iput-object p4, v0, La/e/a/c/a/b$a;->e:La/e/a/c/g$a;

    iput p3, v0, La/e/a/c/a/b$a;->f:I

    iput p5, v0, La/e/a/c/a/b$a;->g:I

    iget-object p2, p0, La/e/a/c/p;->hb:La/e/a/c/a/b$b;

    invoke-interface {p2, p1, v0}, La/e/a/c/a/b$b;->a(La/e/a/c/g;La/e/a/c/a/b$a;)V

    iget-object p2, p0, La/e/a/c/p;->gb:La/e/a/c/a/b$a;

    iget p2, p2, La/e/a/c/a/b$a;->h:I

    invoke-virtual {p1, p2}, La/e/a/c/g;->u(I)V

    iget-object p2, p0, La/e/a/c/p;->gb:La/e/a/c/a/b$a;

    iget p2, p2, La/e/a/c/a/b$a;->i:I

    invoke-virtual {p1, p2}, La/e/a/c/g;->m(I)V

    iget-object p2, p0, La/e/a/c/p;->gb:La/e/a/c/a/b$a;

    iget-boolean p2, p2, La/e/a/c/a/b$a;->k:Z

    invoke-virtual {p1, p2}, La/e/a/c/g;->a(Z)V

    iget-object p2, p0, La/e/a/c/p;->gb:La/e/a/c/a/b$a;

    iget p2, p2, La/e/a/c/a/b$a;->j:I

    invoke-virtual {p1, p2}, La/e/a/c/g;->i(I)V

    return-void
.end method

.method public a(La/e/a/c/h;)V
    .locals 0

    invoke-virtual {p0}, La/e/a/c/p;->Y()V

    return-void
.end method

.method public a(Ljava/util/HashSet;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet<",
            "La/e/a/c/g;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget v2, p0, La/e/a/c/m;->Va:I

    if-ge v1, v2, :cond_1

    iget-object v2, p0, La/e/a/c/m;->Ua:[La/e/a/c/g;

    aget-object v2, v2, v1

    invoke-virtual {p1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public aa()I
    .locals 1

    iget v0, p0, La/e/a/c/p;->eb:I

    return v0
.end method

.method public b(IIII)V
    .locals 0

    return-void
.end method

.method public ba()I
    .locals 1

    iget v0, p0, La/e/a/c/p;->Xa:I

    return v0
.end method

.method public ca()I
    .locals 1

    iget v0, p0, La/e/a/c/p;->bb:I

    return v0
.end method

.method public da()I
    .locals 1

    iget v0, p0, La/e/a/c/p;->cb:I

    return v0
.end method

.method public e(Z)V
    .locals 1

    iget v0, p0, La/e/a/c/p;->_a:I

    if-gtz v0, :cond_0

    iget v0, p0, La/e/a/c/p;->ab:I

    if-lez v0, :cond_2

    :cond_0
    if-eqz p1, :cond_1

    iget p1, p0, La/e/a/c/p;->ab:I

    iput p1, p0, La/e/a/c/p;->bb:I

    iget p1, p0, La/e/a/c/p;->_a:I

    iput p1, p0, La/e/a/c/p;->cb:I

    goto :goto_0

    :cond_1
    iget p1, p0, La/e/a/c/p;->_a:I

    iput p1, p0, La/e/a/c/p;->bb:I

    iget p1, p0, La/e/a/c/p;->ab:I

    iput p1, p0, La/e/a/c/p;->cb:I

    :cond_2
    :goto_0
    return-void
.end method

.method public ea()I
    .locals 1

    iget v0, p0, La/e/a/c/p;->Wa:I

    return v0
.end method

.method protected f(Z)V
    .locals 0

    iput-boolean p1, p0, La/e/a/c/p;->db:Z

    return-void
.end method

.method protected fa()Z
    .locals 9

    iget-object v0, p0, La/e/a/c/g;->da:La/e/a/c/g;

    if-eqz v0, :cond_0

    check-cast v0, La/e/a/c/h;

    invoke-virtual {v0}, La/e/a/c/h;->ba()La/e/a/c/a/b$b;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x0

    if-nez v0, :cond_1

    return v1

    :cond_1
    move v2, v1

    :goto_1
    iget v3, p0, La/e/a/c/m;->Va:I

    const/4 v4, 0x1

    if-ge v2, v3, :cond_8

    iget-object v3, p0, La/e/a/c/m;->Ua:[La/e/a/c/g;

    aget-object v3, v3, v2

    if-nez v3, :cond_2

    goto :goto_3

    :cond_2
    instance-of v5, v3, La/e/a/c/k;

    if-eqz v5, :cond_3

    goto :goto_3

    :cond_3
    invoke-virtual {v3, v1}, La/e/a/c/g;->b(I)La/e/a/c/g$a;

    move-result-object v5

    invoke-virtual {v3, v4}, La/e/a/c/g;->b(I)La/e/a/c/g$a;

    move-result-object v6

    sget-object v7, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v5, v7, :cond_4

    iget v8, v3, La/e/a/c/g;->x:I

    if-eq v8, v4, :cond_4

    if-ne v6, v7, :cond_4

    iget v7, v3, La/e/a/c/g;->y:I

    if-eq v7, v4, :cond_4

    goto :goto_2

    :cond_4
    move v4, v1

    :goto_2
    if-eqz v4, :cond_5

    goto :goto_3

    :cond_5
    sget-object v4, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v5, v4, :cond_6

    sget-object v5, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    :cond_6
    sget-object v4, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v6, v4, :cond_7

    sget-object v6, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    :cond_7
    iget-object v4, p0, La/e/a/c/p;->gb:La/e/a/c/a/b$a;

    iput-object v5, v4, La/e/a/c/a/b$a;->d:La/e/a/c/g$a;

    iput-object v6, v4, La/e/a/c/a/b$a;->e:La/e/a/c/g$a;

    invoke-virtual {v3}, La/e/a/c/g;->C()I

    move-result v5

    iput v5, v4, La/e/a/c/a/b$a;->f:I

    iget-object v4, p0, La/e/a/c/p;->gb:La/e/a/c/a/b$a;

    invoke-virtual {v3}, La/e/a/c/g;->k()I

    move-result v5

    iput v5, v4, La/e/a/c/a/b$a;->g:I

    iget-object v4, p0, La/e/a/c/p;->gb:La/e/a/c/a/b$a;

    invoke-interface {v0, v3, v4}, La/e/a/c/a/b$b;->a(La/e/a/c/g;La/e/a/c/a/b$a;)V

    iget-object v4, p0, La/e/a/c/p;->gb:La/e/a/c/a/b$a;

    iget v4, v4, La/e/a/c/a/b$a;->h:I

    invoke-virtual {v3, v4}, La/e/a/c/g;->u(I)V

    iget-object v4, p0, La/e/a/c/p;->gb:La/e/a/c/a/b$a;

    iget v4, v4, La/e/a/c/a/b$a;->i:I

    invoke-virtual {v3, v4}, La/e/a/c/g;->m(I)V

    iget-object v4, p0, La/e/a/c/p;->gb:La/e/a/c/a/b$a;

    iget v4, v4, La/e/a/c/a/b$a;->j:I

    invoke-virtual {v3, v4}, La/e/a/c/g;->i(I)V

    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_8
    return v4
.end method

.method public ga()Z
    .locals 1

    iget-boolean v0, p0, La/e/a/c/p;->db:Z

    return v0
.end method

.method public h(II)V
    .locals 0

    iput p1, p0, La/e/a/c/p;->eb:I

    iput p2, p0, La/e/a/c/p;->fb:I

    return-void
.end method

.method public z(I)V
    .locals 0

    iput p1, p0, La/e/a/c/p;->Ya:I

    iput p1, p0, La/e/a/c/p;->Wa:I

    iput p1, p0, La/e/a/c/p;->Za:I

    iput p1, p0, La/e/a/c/p;->Xa:I

    iput p1, p0, La/e/a/c/p;->_a:I

    iput p1, p0, La/e/a/c/p;->ab:I

    return-void
.end method
