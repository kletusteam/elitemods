.class public La/e/a/c/e;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La/e/a/c/e$a;
    }
.end annotation


# instance fields
.field private a:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "La/e/a/c/e;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:Z

.field public final d:La/e/a/c/g;

.field public final e:La/e/a/c/e$a;

.field public f:La/e/a/c/e;

.field public g:I

.field h:I

.field i:La/e/a/j;


# direct methods
.method public constructor <init>(La/e/a/c/g;La/e/a/c/e$a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, La/e/a/c/e;->a:Ljava/util/HashSet;

    const/4 v0, 0x0

    iput v0, p0, La/e/a/c/e;->g:I

    const/high16 v0, -0x80000000

    iput v0, p0, La/e/a/c/e;->h:I

    iput-object p1, p0, La/e/a/c/e;->d:La/e/a/c/g;

    iput-object p2, p0, La/e/a/c/e;->e:La/e/a/c/e$a;

    return-void
.end method


# virtual methods
.method public a()Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet<",
            "La/e/a/c/e;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, La/e/a/c/e;->a:Ljava/util/HashSet;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, La/e/a/c/e;->b:I

    const/4 p1, 0x1

    iput-boolean p1, p0, La/e/a/c/e;->c:Z

    return-void
.end method

.method public a(ILjava/util/ArrayList;La/e/a/c/a/q;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList<",
            "La/e/a/c/a/q;",
            ">;",
            "La/e/a/c/a/q;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, La/e/a/c/e;->a:Ljava/util/HashSet;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La/e/a/c/e;

    iget-object v1, v1, La/e/a/c/e;->d:La/e/a/c/g;

    invoke-static {v1, p1, p2, p3}, La/e/a/c/a/i;->a(La/e/a/c/g;ILjava/util/ArrayList;La/e/a/c/a/q;)La/e/a/c/a/q;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(La/e/a/c;)V
    .locals 2

    iget-object p1, p0, La/e/a/c/e;->i:La/e/a/j;

    if-nez p1, :cond_0

    new-instance p1, La/e/a/j;

    sget-object v0, La/e/a/j$a;->a:La/e/a/j$a;

    const/4 v1, 0x0

    invoke-direct {p1, v0, v1}, La/e/a/j;-><init>(La/e/a/j$a;Ljava/lang/String;)V

    iput-object p1, p0, La/e/a/c/e;->i:La/e/a/j;

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, La/e/a/j;->b()V

    :goto_0
    return-void
.end method

.method public a(La/e/a/c/e;)Z
    .locals 5

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    invoke-virtual {p1}, La/e/a/c/e;->h()La/e/a/c/e$a;

    move-result-object v1

    iget-object v2, p0, La/e/a/c/e;->e:La/e/a/c/e$a;

    const/4 v3, 0x1

    if-ne v1, v2, :cond_3

    sget-object v1, La/e/a/c/e$a;->f:La/e/a/c/e$a;

    if-ne v2, v1, :cond_2

    invoke-virtual {p1}, La/e/a/c/e;->e()La/e/a/c/g;

    move-result-object p1

    invoke-virtual {p1}, La/e/a/c/g;->F()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, La/e/a/c/e;->e()La/e/a/c/g;

    move-result-object p1

    invoke-virtual {p1}, La/e/a/c/g;->F()Z

    move-result p1

    if-nez p1, :cond_2

    :cond_1
    return v0

    :cond_2
    return v3

    :cond_3
    sget-object v4, La/e/a/c/d;->a:[I

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_0

    new-instance p1, Ljava/lang/AssertionError;

    iget-object v0, p0, La/e/a/c/e;->e:La/e/a/c/e$a;

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    :pswitch_0
    return v0

    :pswitch_1
    sget-object p1, La/e/a/c/e$a;->b:La/e/a/c/e$a;

    if-eq v1, p1, :cond_5

    sget-object p1, La/e/a/c/e$a;->d:La/e/a/c/e$a;

    if-ne v1, p1, :cond_4

    goto :goto_0

    :cond_4
    return v3

    :cond_5
    :goto_0
    return v0

    :pswitch_2
    sget-object v2, La/e/a/c/e$a;->c:La/e/a/c/e$a;

    if-eq v1, v2, :cond_7

    sget-object v2, La/e/a/c/e$a;->e:La/e/a/c/e$a;

    if-ne v1, v2, :cond_6

    goto :goto_1

    :cond_6
    move v2, v0

    goto :goto_2

    :cond_7
    :goto_1
    move v2, v3

    :goto_2
    invoke-virtual {p1}, La/e/a/c/e;->e()La/e/a/c/g;

    move-result-object p1

    instance-of p1, p1, La/e/a/c/k;

    if-eqz p1, :cond_a

    if-nez v2, :cond_9

    sget-object p1, La/e/a/c/e$a;->i:La/e/a/c/e$a;

    if-ne v1, p1, :cond_8

    goto :goto_3

    :cond_8
    move v2, v0

    goto :goto_4

    :cond_9
    :goto_3
    move v2, v3

    :cond_a
    :goto_4
    return v2

    :pswitch_3
    sget-object v2, La/e/a/c/e$a;->b:La/e/a/c/e$a;

    if-eq v1, v2, :cond_c

    sget-object v2, La/e/a/c/e$a;->d:La/e/a/c/e$a;

    if-ne v1, v2, :cond_b

    goto :goto_5

    :cond_b
    move v2, v0

    goto :goto_6

    :cond_c
    :goto_5
    move v2, v3

    :goto_6
    invoke-virtual {p1}, La/e/a/c/e;->e()La/e/a/c/g;

    move-result-object p1

    instance-of p1, p1, La/e/a/c/k;

    if-eqz p1, :cond_f

    if-nez v2, :cond_e

    sget-object p1, La/e/a/c/e$a;->h:La/e/a/c/e$a;

    if-ne v1, p1, :cond_d

    goto :goto_7

    :cond_d
    move v2, v0

    goto :goto_8

    :cond_e
    :goto_7
    move v2, v3

    :cond_f
    :goto_8
    return v2

    :pswitch_4
    sget-object p1, La/e/a/c/e$a;->f:La/e/a/c/e$a;

    if-eq v1, p1, :cond_10

    sget-object p1, La/e/a/c/e$a;->h:La/e/a/c/e$a;

    if-eq v1, p1, :cond_10

    sget-object p1, La/e/a/c/e$a;->i:La/e/a/c/e$a;

    if-eq v1, p1, :cond_10

    move v0, v3

    :cond_10
    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public a(La/e/a/c/e;I)Z
    .locals 2

    const/high16 v0, -0x80000000

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, La/e/a/c/e;->a(La/e/a/c/e;IIZ)Z

    move-result p1

    return p1
.end method

.method public a(La/e/a/c/e;IIZ)Z
    .locals 1

    const/4 v0, 0x1

    if-nez p1, :cond_0

    invoke-virtual {p0}, La/e/a/c/e;->m()V

    return v0

    :cond_0
    if-nez p4, :cond_1

    invoke-virtual {p0, p1}, La/e/a/c/e;->a(La/e/a/c/e;)Z

    move-result p4

    if-nez p4, :cond_1

    const/4 p1, 0x0

    return p1

    :cond_1
    iput-object p1, p0, La/e/a/c/e;->f:La/e/a/c/e;

    iget-object p1, p0, La/e/a/c/e;->f:La/e/a/c/e;

    iget-object p4, p1, La/e/a/c/e;->a:Ljava/util/HashSet;

    if-nez p4, :cond_2

    new-instance p4, Ljava/util/HashSet;

    invoke-direct {p4}, Ljava/util/HashSet;-><init>()V

    iput-object p4, p1, La/e/a/c/e;->a:Ljava/util/HashSet;

    :cond_2
    iget-object p1, p0, La/e/a/c/e;->f:La/e/a/c/e;

    iget-object p1, p1, La/e/a/c/e;->a:Ljava/util/HashSet;

    if-eqz p1, :cond_3

    invoke-virtual {p1, p0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_3
    iput p2, p0, La/e/a/c/e;->g:I

    iput p3, p0, La/e/a/c/e;->h:I

    return v0
.end method

.method public b()I
    .locals 1

    iget-boolean v0, p0, La/e/a/c/e;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget v0, p0, La/e/a/c/e;->b:I

    return v0
.end method

.method public b(I)V
    .locals 1

    invoke-virtual {p0}, La/e/a/c/e;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iput p1, p0, La/e/a/c/e;->h:I

    :cond_0
    return-void
.end method

.method public c()I
    .locals 3

    iget-object v0, p0, La/e/a/c/e;->d:La/e/a/c/g;

    invoke-virtual {v0}, La/e/a/c/g;->B()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget v0, p0, La/e/a/c/e;->h:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_1

    iget-object v0, p0, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v0, :cond_1

    iget-object v0, v0, La/e/a/c/e;->d:La/e/a/c/g;

    invoke-virtual {v0}, La/e/a/c/g;->B()I

    move-result v0

    if-ne v0, v1, :cond_1

    iget v0, p0, La/e/a/c/e;->h:I

    return v0

    :cond_1
    iget v0, p0, La/e/a/c/e;->g:I

    return v0
.end method

.method public final d()La/e/a/c/e;
    .locals 2

    sget-object v0, La/e/a/c/d;->a:[I

    iget-object v1, p0, La/e/a/c/e;->e:La/e/a/c/e$a;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    iget-object v1, p0, La/e/a/c/e;->e:La/e/a/c/e$a;

    invoke-virtual {v1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, La/e/a/c/e;->d:La/e/a/c/g;

    iget-object v0, v0, La/e/a/c/g;->S:La/e/a/c/e;

    return-object v0

    :pswitch_1
    iget-object v0, p0, La/e/a/c/e;->d:La/e/a/c/g;

    iget-object v0, v0, La/e/a/c/g;->U:La/e/a/c/e;

    return-object v0

    :pswitch_2
    iget-object v0, p0, La/e/a/c/e;->d:La/e/a/c/g;

    iget-object v0, v0, La/e/a/c/g;->R:La/e/a/c/e;

    return-object v0

    :pswitch_3
    iget-object v0, p0, La/e/a/c/e;->d:La/e/a/c/g;

    iget-object v0, v0, La/e/a/c/g;->T:La/e/a/c/e;

    return-object v0

    :pswitch_4
    const/4 v0, 0x0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public e()La/e/a/c/g;
    .locals 1

    iget-object v0, p0, La/e/a/c/e;->d:La/e/a/c/g;

    return-object v0
.end method

.method public f()La/e/a/j;
    .locals 1

    iget-object v0, p0, La/e/a/c/e;->i:La/e/a/j;

    return-object v0
.end method

.method public g()La/e/a/c/e;
    .locals 1

    iget-object v0, p0, La/e/a/c/e;->f:La/e/a/c/e;

    return-object v0
.end method

.method public h()La/e/a/c/e$a;
    .locals 1

    iget-object v0, p0, La/e/a/c/e;->e:La/e/a/c/e$a;

    return-object v0
.end method

.method public i()Z
    .locals 3

    iget-object v0, p0, La/e/a/c/e;->a:Ljava/util/HashSet;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, La/e/a/c/e;

    invoke-virtual {v2}, La/e/a/c/e;->d()La/e/a/c/e;

    move-result-object v2

    invoke-virtual {v2}, La/e/a/c/e;->l()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_2
    return v1
.end method

.method public j()Z
    .locals 2

    iget-object v0, p0, La/e/a/c/e;->a:Ljava/util/HashSet;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public k()Z
    .locals 1

    iget-boolean v0, p0, La/e/a/c/e;->c:Z

    return v0
.end method

.method public l()Z
    .locals 1

    iget-object v0, p0, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public m()V
    .locals 2

    iget-object v0, p0, La/e/a/c/e;->f:La/e/a/c/e;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, v0, La/e/a/c/e;->a:Ljava/util/HashSet;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, La/e/a/c/e;->f:La/e/a/c/e;

    iget-object v0, v0, La/e/a/c/e;->a:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, La/e/a/c/e;->f:La/e/a/c/e;

    iput-object v1, v0, La/e/a/c/e;->a:Ljava/util/HashSet;

    :cond_0
    iput-object v1, p0, La/e/a/c/e;->a:Ljava/util/HashSet;

    iput-object v1, p0, La/e/a/c/e;->f:La/e/a/c/e;

    const/4 v0, 0x0

    iput v0, p0, La/e/a/c/e;->g:I

    const/high16 v1, -0x80000000

    iput v1, p0, La/e/a/c/e;->h:I

    iput-boolean v0, p0, La/e/a/c/e;->c:Z

    iput v0, p0, La/e/a/c/e;->b:I

    return-void
.end method

.method public n()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, La/e/a/c/e;->c:Z

    iput v0, p0, La/e/a/c/e;->b:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, La/e/a/c/e;->d:La/e/a/c/g;

    invoke-virtual {v1}, La/e/a/c/g;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, La/e/a/c/e;->e:La/e/a/c/e$a;

    invoke-virtual {v1}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
