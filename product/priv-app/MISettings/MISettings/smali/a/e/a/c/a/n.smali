.class La/e/a/c/a/n;
.super Ljava/lang/Object;


# static fields
.field public static a:I


# instance fields
.field public b:I

.field public c:Z

.field d:La/e/a/c/a/s;

.field e:La/e/a/c/a/s;

.field f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "La/e/a/c/a/s;",
            ">;"
        }
    .end annotation
.end field

.field g:I

.field h:I


# direct methods
.method public constructor <init>(La/e/a/c/a/s;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, La/e/a/c/a/n;->b:I

    iput-boolean v0, p0, La/e/a/c/a/n;->c:Z

    const/4 v1, 0x0

    iput-object v1, p0, La/e/a/c/a/n;->d:La/e/a/c/a/s;

    iput-object v1, p0, La/e/a/c/a/n;->e:La/e/a/c/a/s;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, La/e/a/c/a/n;->f:Ljava/util/ArrayList;

    iput v0, p0, La/e/a/c/a/n;->g:I

    sget v0, La/e/a/c/a/n;->a:I

    iput v0, p0, La/e/a/c/a/n;->g:I

    add-int/lit8 v0, v0, 0x1

    sput v0, La/e/a/c/a/n;->a:I

    iput-object p1, p0, La/e/a/c/a/n;->d:La/e/a/c/a/s;

    iput-object p1, p0, La/e/a/c/a/n;->e:La/e/a/c/a/s;

    iput p2, p0, La/e/a/c/a/n;->h:I

    return-void
.end method

.method private a(La/e/a/c/a/f;J)J
    .locals 8

    iget-object v0, p1, La/e/a/c/a/f;->d:La/e/a/c/a/s;

    instance-of v1, v0, La/e/a/c/a/k;

    if-eqz v1, :cond_0

    return-wide p2

    :cond_0
    iget-object v1, p1, La/e/a/c/a/f;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    move-wide v3, p2

    :goto_0
    if-ge v2, v1, :cond_3

    iget-object v5, p1, La/e/a/c/a/f;->k:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/c/a/d;

    instance-of v6, v5, La/e/a/c/a/f;

    if-eqz v6, :cond_2

    check-cast v5, La/e/a/c/a/f;

    iget-object v6, v5, La/e/a/c/a/f;->d:La/e/a/c/a/s;

    if-ne v6, v0, :cond_1

    goto :goto_1

    :cond_1
    iget v6, v5, La/e/a/c/a/f;->f:I

    int-to-long v6, v6

    add-long/2addr v6, p2

    invoke-direct {p0, v5, v6, v7}, La/e/a/c/a/n;->a(La/e/a/c/a/f;J)J

    move-result-wide v5

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    iget-object v1, v0, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    if-ne p1, v1, :cond_4

    invoke-virtual {v0}, La/e/a/c/a/s;->d()J

    move-result-wide v1

    iget-object p1, v0, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    sub-long/2addr p2, v1

    invoke-direct {p0, p1, p2, p3}, La/e/a/c/a/n;->a(La/e/a/c/a/f;J)J

    move-result-wide v1

    invoke-static {v3, v4, v1, v2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    iget-object p1, v0, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget p1, p1, La/e/a/c/a/f;->f:I

    int-to-long v3, p1

    sub-long/2addr p2, v3

    invoke-static {v1, v2, p2, p3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    :cond_4
    return-wide v3
.end method

.method private b(La/e/a/c/a/f;J)J
    .locals 8

    iget-object v0, p1, La/e/a/c/a/f;->d:La/e/a/c/a/s;

    instance-of v1, v0, La/e/a/c/a/k;

    if-eqz v1, :cond_0

    return-wide p2

    :cond_0
    iget-object v1, p1, La/e/a/c/a/f;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    move-wide v3, p2

    :goto_0
    if-ge v2, v1, :cond_3

    iget-object v5, p1, La/e/a/c/a/f;->k:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/c/a/d;

    instance-of v6, v5, La/e/a/c/a/f;

    if-eqz v6, :cond_2

    check-cast v5, La/e/a/c/a/f;

    iget-object v6, v5, La/e/a/c/a/f;->d:La/e/a/c/a/s;

    if-ne v6, v0, :cond_1

    goto :goto_1

    :cond_1
    iget v6, v5, La/e/a/c/a/f;->f:I

    int-to-long v6, v6

    add-long/2addr v6, p2

    invoke-direct {p0, v5, v6, v7}, La/e/a/c/a/n;->b(La/e/a/c/a/f;J)J

    move-result-wide v5

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v3

    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    iget-object v1, v0, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    if-ne p1, v1, :cond_4

    invoke-virtual {v0}, La/e/a/c/a/s;->d()J

    move-result-wide v1

    iget-object p1, v0, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    add-long/2addr p2, v1

    invoke-direct {p0, p1, p2, p3}, La/e/a/c/a/n;->b(La/e/a/c/a/f;J)J

    move-result-wide v1

    invoke-static {v3, v4, v1, v2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    iget-object p1, v0, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget p1, p1, La/e/a/c/a/f;->f:I

    int-to-long v3, p1

    sub-long/2addr p2, v3

    invoke-static {v1, v2, p2, p3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v3

    :cond_4
    return-wide v3
.end method


# virtual methods
.method public a(La/e/a/c/h;I)J
    .locals 10

    iget-object v0, p0, La/e/a/c/a/n;->d:La/e/a/c/a/s;

    instance-of v1, v0, La/e/a/c/a/c;

    const-wide/16 v2, 0x0

    if-eqz v1, :cond_0

    check-cast v0, La/e/a/c/a/c;

    iget v0, v0, La/e/a/c/a/s;->f:I

    if-eq v0, p2, :cond_2

    return-wide v2

    :cond_0
    if-nez p2, :cond_1

    instance-of v0, v0, La/e/a/c/a/m;

    if-nez v0, :cond_2

    return-wide v2

    :cond_1
    instance-of v0, v0, La/e/a/c/a/p;

    if-nez v0, :cond_2

    return-wide v2

    :cond_2
    if-nez p2, :cond_3

    iget-object v0, p1, La/e/a/c/g;->f:La/e/a/c/a/m;

    goto :goto_0

    :cond_3
    iget-object v0, p1, La/e/a/c/g;->g:La/e/a/c/a/p;

    :goto_0
    iget-object v0, v0, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    if-nez p2, :cond_4

    iget-object p1, p1, La/e/a/c/g;->f:La/e/a/c/a/m;

    goto :goto_1

    :cond_4
    iget-object p1, p1, La/e/a/c/g;->g:La/e/a/c/a/p;

    :goto_1
    iget-object p1, p1, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget-object v1, p0, La/e/a/c/a/n;->d:La/e/a/c/a/s;

    iget-object v1, v1, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget-object v1, v1, La/e/a/c/a/f;->l:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    iget-object v1, p0, La/e/a/c/a/n;->d:La/e/a/c/a/s;

    iget-object v1, v1, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget-object v1, v1, La/e/a/c/a/f;->l:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    iget-object v1, p0, La/e/a/c/a/n;->d:La/e/a/c/a/s;

    invoke-virtual {v1}, La/e/a/c/a/s;->d()J

    move-result-wide v4

    if-eqz v0, :cond_8

    if-eqz p1, :cond_8

    iget-object p1, p0, La/e/a/c/a/n;->d:La/e/a/c/a/s;

    iget-object p1, p1, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    invoke-direct {p0, p1, v2, v3}, La/e/a/c/a/n;->b(La/e/a/c/a/f;J)J

    move-result-wide v0

    iget-object p1, p0, La/e/a/c/a/n;->d:La/e/a/c/a/s;

    iget-object p1, p1, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    invoke-direct {p0, p1, v2, v3}, La/e/a/c/a/n;->a(La/e/a/c/a/f;J)J

    move-result-wide v6

    sub-long/2addr v0, v4

    iget-object p1, p0, La/e/a/c/a/n;->d:La/e/a/c/a/s;

    iget-object p1, p1, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget p1, p1, La/e/a/c/a/f;->f:I

    neg-int v8, p1

    int-to-long v8, v8

    cmp-long v8, v0, v8

    if-ltz v8, :cond_5

    int-to-long v8, p1

    add-long/2addr v0, v8

    :cond_5
    neg-long v6, v6

    sub-long/2addr v6, v4

    iget-object p1, p0, La/e/a/c/a/n;->d:La/e/a/c/a/s;

    iget-object p1, p1, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget p1, p1, La/e/a/c/a/f;->f:I

    int-to-long v8, p1

    sub-long/2addr v6, v8

    int-to-long v8, p1

    cmp-long v8, v6, v8

    if-ltz v8, :cond_6

    int-to-long v8, p1

    sub-long/2addr v6, v8

    :cond_6
    iget-object p1, p0, La/e/a/c/a/n;->d:La/e/a/c/a/s;

    iget-object p1, p1, La/e/a/c/a/s;->b:La/e/a/c/g;

    invoke-virtual {p1, p2}, La/e/a/c/g;->a(I)F

    move-result p1

    const/4 p2, 0x0

    cmpl-float p2, p1, p2

    const/high16 v8, 0x3f800000    # 1.0f

    if-lez p2, :cond_7

    long-to-float p2, v6

    div-float/2addr p2, p1

    long-to-float v0, v0

    sub-float v1, v8, p1

    div-float/2addr v0, v1

    add-float/2addr p2, v0

    float-to-long v2, p2

    :cond_7
    long-to-float p2, v2

    mul-float v0, p2, p1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-long v2, v0

    sub-float/2addr v8, p1

    mul-float/2addr p2, v8

    add-float/2addr p2, v1

    float-to-long p1, p2

    add-long/2addr v2, v4

    add-long/2addr v2, p1

    iget-object p1, p0, La/e/a/c/a/n;->d:La/e/a/c/a/s;

    iget-object p2, p1, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget p2, p2, La/e/a/c/a/f;->f:I

    int-to-long v0, p2

    add-long/2addr v0, v2

    iget-object p1, p1, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget p1, p1, La/e/a/c/a/f;->f:I

    goto :goto_2

    :cond_8
    if-eqz v0, :cond_9

    iget-object p1, p0, La/e/a/c/a/n;->d:La/e/a/c/a/s;

    iget-object p1, p1, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget p2, p1, La/e/a/c/a/f;->f:I

    int-to-long v0, p2

    invoke-direct {p0, p1, v0, v1}, La/e/a/c/a/n;->b(La/e/a/c/a/f;J)J

    move-result-wide p1

    iget-object v0, p0, La/e/a/c/a/n;->d:La/e/a/c/a/s;

    iget-object v0, v0, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget v0, v0, La/e/a/c/a/f;->f:I

    int-to-long v0, v0

    add-long/2addr v0, v4

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_3

    :cond_9
    if-eqz p1, :cond_a

    iget-object p1, p0, La/e/a/c/a/n;->d:La/e/a/c/a/s;

    iget-object p1, p1, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget p2, p1, La/e/a/c/a/f;->f:I

    int-to-long v0, p2

    invoke-direct {p0, p1, v0, v1}, La/e/a/c/a/n;->a(La/e/a/c/a/f;J)J

    move-result-wide p1

    iget-object v0, p0, La/e/a/c/a/n;->d:La/e/a/c/a/s;

    iget-object v0, v0, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget v0, v0, La/e/a/c/a/f;->f:I

    neg-int v0, v0

    int-to-long v0, v0

    add-long/2addr v0, v4

    neg-long p1, p1

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_3

    :cond_a
    iget-object p1, p0, La/e/a/c/a/n;->d:La/e/a/c/a/s;

    iget-object p2, p1, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget p2, p2, La/e/a/c/a/f;->f:I

    int-to-long v0, p2

    invoke-virtual {p1}, La/e/a/c/a/s;->d()J

    move-result-wide p1

    add-long/2addr v0, p1

    iget-object p1, p0, La/e/a/c/a/n;->d:La/e/a/c/a/s;

    iget-object p1, p1, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget p1, p1, La/e/a/c/a/f;->f:I

    :goto_2
    int-to-long p1, p1

    sub-long/2addr v0, p1

    :goto_3
    return-wide v0
.end method

.method public a(La/e/a/c/a/s;)V
    .locals 1

    iget-object v0, p0, La/e/a/c/a/n;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object p1, p0, La/e/a/c/a/n;->e:La/e/a/c/a/s;

    return-void
.end method
