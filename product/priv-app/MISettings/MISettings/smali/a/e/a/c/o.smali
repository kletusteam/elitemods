.class public La/e/a/c/o;
.super La/e/a/c/p;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, La/e/a/c/p;-><init>()V

    return-void
.end method


# virtual methods
.method public a(La/e/a/d;Z)V
    .locals 0

    invoke-super {p0, p1, p2}, La/e/a/c/g;->a(La/e/a/d;Z)V

    iget p1, p0, La/e/a/c/m;->Va:I

    if-lez p1, :cond_0

    iget-object p1, p0, La/e/a/c/m;->Ua:[La/e/a/c/g;

    const/4 p2, 0x0

    aget-object p1, p1, p2

    invoke-virtual {p1}, La/e/a/c/g;->V()V

    sget-object p2, La/e/a/c/e$a;->b:La/e/a/c/e$a;

    invoke-virtual {p1, p2, p0, p2}, La/e/a/c/g;->a(La/e/a/c/e$a;La/e/a/c/g;La/e/a/c/e$a;)V

    sget-object p2, La/e/a/c/e$a;->d:La/e/a/c/e$a;

    invoke-virtual {p1, p2, p0, p2}, La/e/a/c/g;->a(La/e/a/c/e$a;La/e/a/c/g;La/e/a/c/e$a;)V

    sget-object p2, La/e/a/c/e$a;->c:La/e/a/c/e$a;

    invoke-virtual {p1, p2, p0, p2}, La/e/a/c/g;->a(La/e/a/c/e$a;La/e/a/c/g;La/e/a/c/e$a;)V

    sget-object p2, La/e/a/c/e$a;->e:La/e/a/c/e$a;

    invoke-virtual {p1, p2, p0, p2}, La/e/a/c/g;->a(La/e/a/c/e$a;La/e/a/c/g;La/e/a/c/e$a;)V

    :cond_0
    return-void
.end method

.method public b(IIII)V
    .locals 5

    invoke-virtual {p0}, La/e/a/c/p;->ca()I

    move-result v0

    invoke-virtual {p0}, La/e/a/c/p;->da()I

    move-result v1

    invoke-virtual {p0}, La/e/a/c/p;->ea()I

    move-result v2

    invoke-virtual {p0}, La/e/a/c/p;->ba()I

    move-result v3

    add-int/2addr v0, v1

    const/4 v1, 0x0

    add-int/2addr v0, v1

    add-int/2addr v2, v3

    add-int/2addr v2, v1

    iget v3, p0, La/e/a/c/m;->Va:I

    if-lez v3, :cond_0

    iget-object v3, p0, La/e/a/c/m;->Ua:[La/e/a/c/g;

    aget-object v3, v3, v1

    invoke-virtual {v3}, La/e/a/c/g;->C()I

    move-result v3

    add-int/2addr v0, v3

    iget-object v3, p0, La/e/a/c/m;->Ua:[La/e/a/c/g;

    aget-object v3, v3, v1

    invoke-virtual {v3}, La/e/a/c/g;->k()I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, La/e/a/c/g;->u()I

    move-result v3

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0}, La/e/a/c/g;->t()I

    move-result v3

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    const/high16 v3, -0x80000000

    const/high16 v4, 0x40000000    # 2.0f

    if-ne p1, v4, :cond_1

    goto :goto_0

    :cond_1
    if-ne p1, v3, :cond_2

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result p2

    goto :goto_0

    :cond_2
    if-nez p1, :cond_3

    move p2, v0

    goto :goto_0

    :cond_3
    move p2, v1

    :goto_0
    if-ne p3, v4, :cond_4

    goto :goto_1

    :cond_4
    if-ne p3, v3, :cond_5

    invoke-static {v2, p4}, Ljava/lang/Math;->min(II)I

    move-result p4

    goto :goto_1

    :cond_5
    if-nez p3, :cond_6

    move p4, v2

    goto :goto_1

    :cond_6
    move p4, v1

    :goto_1
    invoke-virtual {p0, p2, p4}, La/e/a/c/p;->h(II)V

    invoke-virtual {p0, p2}, La/e/a/c/g;->u(I)V

    invoke-virtual {p0, p4}, La/e/a/c/g;->m(I)V

    iget p1, p0, La/e/a/c/m;->Va:I

    if-lez p1, :cond_7

    const/4 v1, 0x1

    :cond_7
    invoke-virtual {p0, v1}, La/e/a/c/p;->f(Z)V

    return-void
.end method
