.class public abstract La/e/a/c/a/s;
.super Ljava/lang/Object;

# interfaces
.implements La/e/a/c/a/d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La/e/a/c/a/s$a;
    }
.end annotation


# instance fields
.field public a:I

.field b:La/e/a/c/g;

.field c:La/e/a/c/a/n;

.field protected d:La/e/a/c/g$a;

.field e:La/e/a/c/a/g;

.field public f:I

.field g:Z

.field public h:La/e/a/c/a/f;

.field public i:La/e/a/c/a/f;

.field protected j:La/e/a/c/a/s$a;


# direct methods
.method public constructor <init>(La/e/a/c/g;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, La/e/a/c/a/g;

    invoke-direct {v0, p0}, La/e/a/c/a/g;-><init>(La/e/a/c/a/s;)V

    iput-object v0, p0, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    const/4 v0, 0x0

    iput v0, p0, La/e/a/c/a/s;->f:I

    iput-boolean v0, p0, La/e/a/c/a/s;->g:Z

    new-instance v0, La/e/a/c/a/f;

    invoke-direct {v0, p0}, La/e/a/c/a/f;-><init>(La/e/a/c/a/s;)V

    iput-object v0, p0, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    new-instance v0, La/e/a/c/a/f;

    invoke-direct {v0, p0}, La/e/a/c/a/f;-><init>(La/e/a/c/a/s;)V

    iput-object v0, p0, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    sget-object v0, La/e/a/c/a/s$a;->a:La/e/a/c/a/s$a;

    iput-object v0, p0, La/e/a/c/a/s;->j:La/e/a/c/a/s$a;

    iput-object p1, p0, La/e/a/c/a/s;->b:La/e/a/c/g;

    return-void
.end method

.method private b(II)V
    .locals 6

    iget v0, p0, La/e/a/c/a/s;->a:I

    if-eqz v0, :cond_8

    const/4 v1, 0x1

    if-eq v0, v1, :cond_7

    const/4 p2, 0x2

    const/high16 v2, 0x3f000000    # 0.5f

    if-eq v0, p2, :cond_4

    const/4 p2, 0x3

    if-eq v0, p2, :cond_0

    goto/16 :goto_4

    :cond_0
    iget-object v0, p0, La/e/a/c/a/s;->b:La/e/a/c/g;

    iget-object v3, v0, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v4, v3, La/e/a/c/a/s;->d:La/e/a/c/g$a;

    sget-object v5, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v4, v5, :cond_1

    iget v3, v3, La/e/a/c/a/s;->a:I

    if-ne v3, p2, :cond_1

    iget-object v0, v0, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v3, v0, La/e/a/c/a/s;->d:La/e/a/c/g$a;

    if-ne v3, v5, :cond_1

    iget v0, v0, La/e/a/c/a/s;->a:I

    if-ne v0, p2, :cond_1

    goto/16 :goto_4

    :cond_1
    if-nez p1, :cond_2

    iget-object p2, p0, La/e/a/c/a/s;->b:La/e/a/c/g;

    iget-object p2, p2, La/e/a/c/g;->g:La/e/a/c/a/p;

    goto :goto_0

    :cond_2
    iget-object p2, p0, La/e/a/c/a/s;->b:La/e/a/c/g;

    iget-object p2, p2, La/e/a/c/g;->f:La/e/a/c/a/m;

    :goto_0
    iget-object v0, p2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget-boolean v0, v0, La/e/a/c/a/f;->j:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, La/e/a/c/a/s;->b:La/e/a/c/g;

    invoke-virtual {v0}, La/e/a/c/g;->i()F

    move-result v0

    if-ne p1, v1, :cond_3

    iget-object p1, p2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget p1, p1, La/e/a/c/a/f;->g:I

    int-to-float p1, p1

    div-float/2addr p1, v0

    add-float/2addr p1, v2

    float-to-int p1, p1

    goto :goto_1

    :cond_3
    iget-object p1, p2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget p1, p1, La/e/a/c/a/f;->g:I

    int-to-float p1, p1

    mul-float/2addr v0, p1

    add-float/2addr v0, v2

    float-to-int p1, v0

    :goto_1
    iget-object p2, p0, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {p2, p1}, La/e/a/c/a/g;->a(I)V

    goto :goto_4

    :cond_4
    iget-object p2, p0, La/e/a/c/a/s;->b:La/e/a/c/g;

    invoke-virtual {p2}, La/e/a/c/g;->v()La/e/a/c/g;

    move-result-object p2

    if-eqz p2, :cond_9

    if-nez p1, :cond_5

    iget-object p2, p2, La/e/a/c/g;->f:La/e/a/c/a/m;

    goto :goto_2

    :cond_5
    iget-object p2, p2, La/e/a/c/g;->g:La/e/a/c/a/p;

    :goto_2
    iget-object v0, p2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget-boolean v0, v0, La/e/a/c/a/f;->j:Z

    if-eqz v0, :cond_9

    if-nez p1, :cond_6

    iget-object v0, p0, La/e/a/c/a/s;->b:La/e/a/c/g;

    iget v0, v0, La/e/a/c/g;->C:F

    goto :goto_3

    :cond_6
    iget-object v0, p0, La/e/a/c/a/s;->b:La/e/a/c/g;

    iget v0, v0, La/e/a/c/g;->F:F

    :goto_3
    iget-object p2, p2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget p2, p2, La/e/a/c/a/f;->g:I

    int-to-float p2, p2

    mul-float/2addr p2, v0

    add-float/2addr p2, v2

    float-to-int p2, p2

    iget-object v0, p0, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {p0, p2, p1}, La/e/a/c/a/s;->a(II)I

    move-result p1

    invoke-virtual {v0, p1}, La/e/a/c/a/g;->a(I)V

    goto :goto_4

    :cond_7
    iget-object v0, p0, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget v0, v0, La/e/a/c/a/g;->m:I

    invoke-virtual {p0, v0, p1}, La/e/a/c/a/s;->a(II)I

    move-result p1

    iget-object v0, p0, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-virtual {v0, p1}, La/e/a/c/a/g;->a(I)V

    goto :goto_4

    :cond_8
    iget-object v0, p0, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {p0, p2, p1}, La/e/a/c/a/s;->a(II)I

    move-result p1

    invoke-virtual {v0, p1}, La/e/a/c/a/g;->a(I)V

    :cond_9
    :goto_4
    return-void
.end method


# virtual methods
.method protected final a(II)I
    .locals 1

    if-nez p2, :cond_1

    iget-object p2, p0, La/e/a/c/a/s;->b:La/e/a/c/g;

    iget v0, p2, La/e/a/c/g;->B:I

    iget p2, p2, La/e/a/c/g;->A:I

    invoke-static {p2, p1}, Ljava/lang/Math;->max(II)I

    move-result p2

    if-lez v0, :cond_0

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result p2

    :cond_0
    if-eq p2, p1, :cond_3

    goto :goto_0

    :cond_1
    iget-object p2, p0, La/e/a/c/a/s;->b:La/e/a/c/g;

    iget v0, p2, La/e/a/c/g;->E:I

    iget p2, p2, La/e/a/c/g;->D:I

    invoke-static {p2, p1}, Ljava/lang/Math;->max(II)I

    move-result p2

    if-lez v0, :cond_2

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result p2

    :cond_2
    if-eq p2, p1, :cond_3

    :goto_0
    move p1, p2

    :cond_3
    return p1
.end method

.method protected final a(La/e/a/c/e;)La/e/a/c/a/f;
    .locals 3

    iget-object p1, p1, La/e/a/c/e;->f:La/e/a/c/e;

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    iget-object v1, p1, La/e/a/c/e;->d:La/e/a/c/g;

    iget-object p1, p1, La/e/a/c/e;->e:La/e/a/c/e$a;

    sget-object v2, La/e/a/c/a/r;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v2, p1

    const/4 v2, 0x1

    if-eq p1, v2, :cond_5

    const/4 v2, 0x2

    if-eq p1, v2, :cond_4

    const/4 v2, 0x3

    if-eq p1, v2, :cond_3

    const/4 v2, 0x4

    if-eq p1, v2, :cond_2

    const/4 v2, 0x5

    if-eq p1, v2, :cond_1

    goto :goto_0

    :cond_1
    iget-object p1, v1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v0, p1, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    goto :goto_0

    :cond_2
    iget-object p1, v1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v0, p1, La/e/a/c/a/p;->k:La/e/a/c/a/f;

    goto :goto_0

    :cond_3
    iget-object p1, v1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v0, p1, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    goto :goto_0

    :cond_4
    iget-object p1, v1, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v0, p1, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    goto :goto_0

    :cond_5
    iget-object p1, v1, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v0, p1, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    :goto_0
    return-object v0
.end method

.method protected final a(La/e/a/c/e;I)La/e/a/c/a/f;
    .locals 2

    iget-object v0, p1, La/e/a/c/e;->f:La/e/a/c/e;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    iget-object v0, v0, La/e/a/c/e;->d:La/e/a/c/g;

    if-nez p2, :cond_1

    iget-object p2, v0, La/e/a/c/g;->f:La/e/a/c/a/m;

    goto :goto_0

    :cond_1
    iget-object p2, v0, La/e/a/c/g;->g:La/e/a/c/a/p;

    :goto_0
    iget-object p1, p1, La/e/a/c/e;->f:La/e/a/c/e;

    iget-object p1, p1, La/e/a/c/e;->e:La/e/a/c/e$a;

    sget-object v0, La/e/a/c/a/r;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_3

    const/4 v0, 0x5

    if-eq p1, v0, :cond_2

    goto :goto_1

    :cond_2
    iget-object v1, p2, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    goto :goto_1

    :cond_3
    iget-object v1, p2, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    :goto_1
    return-object v1
.end method

.method abstract a()V
.end method

.method public a(La/e/a/c/a/d;)V
    .locals 0

    return-void
.end method

.method protected a(La/e/a/c/a/d;La/e/a/c/e;La/e/a/c/e;I)V
    .locals 4

    invoke-virtual {p0, p2}, La/e/a/c/a/s;->a(La/e/a/c/e;)La/e/a/c/a/f;

    move-result-object p1

    invoke-virtual {p0, p3}, La/e/a/c/a/s;->a(La/e/a/c/e;)La/e/a/c/a/f;

    move-result-object v0

    iget-boolean v1, p1, La/e/a/c/a/f;->j:Z

    if-eqz v1, :cond_6

    iget-boolean v1, v0, La/e/a/c/a/f;->j:Z

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    iget v1, p1, La/e/a/c/a/f;->g:I

    invoke-virtual {p2}, La/e/a/c/e;->c()I

    move-result p2

    add-int/2addr v1, p2

    iget p2, v0, La/e/a/c/a/f;->g:I

    invoke-virtual {p3}, La/e/a/c/e;->c()I

    move-result p3

    sub-int/2addr p2, p3

    sub-int p3, p2, v1

    iget-object v2, p0, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget-boolean v2, v2, La/e/a/c/a/f;->j:Z

    if-nez v2, :cond_1

    iget-object v2, p0, La/e/a/c/a/s;->d:La/e/a/c/g$a;

    sget-object v3, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v2, v3, :cond_1

    invoke-direct {p0, p4, p3}, La/e/a/c/a/s;->b(II)V

    :cond_1
    iget-object v2, p0, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget-boolean v3, v2, La/e/a/c/a/f;->j:Z

    if-nez v3, :cond_2

    return-void

    :cond_2
    iget v2, v2, La/e/a/c/a/f;->g:I

    if-ne v2, p3, :cond_3

    iget-object p1, p0, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    invoke-virtual {p1, v1}, La/e/a/c/a/f;->a(I)V

    iget-object p1, p0, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    invoke-virtual {p1, p2}, La/e/a/c/a/f;->a(I)V

    return-void

    :cond_3
    if-nez p4, :cond_4

    iget-object p3, p0, La/e/a/c/a/s;->b:La/e/a/c/g;

    invoke-virtual {p3}, La/e/a/c/g;->l()F

    move-result p3

    goto :goto_0

    :cond_4
    iget-object p3, p0, La/e/a/c/a/s;->b:La/e/a/c/g;

    invoke-virtual {p3}, La/e/a/c/g;->x()F

    move-result p3

    :goto_0
    const/high16 p4, 0x3f000000    # 0.5f

    if-ne p1, v0, :cond_5

    iget v1, p1, La/e/a/c/a/f;->g:I

    iget p2, v0, La/e/a/c/a/f;->g:I

    move p3, p4

    :cond_5
    sub-int/2addr p2, v1

    iget-object p1, p0, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget p1, p1, La/e/a/c/a/f;->g:I

    sub-int/2addr p2, p1

    iget-object p1, p0, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    int-to-float v0, v1

    add-float/2addr v0, p4

    int-to-float p2, p2

    mul-float/2addr p2, p3

    add-float/2addr v0, p2

    float-to-int p2, v0

    invoke-virtual {p1, p2}, La/e/a/c/a/f;->a(I)V

    iget-object p1, p0, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget-object p2, p0, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget p2, p2, La/e/a/c/a/f;->g:I

    iget-object p3, p0, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget p3, p3, La/e/a/c/a/f;->g:I

    add-int/2addr p2, p3

    invoke-virtual {p1, p2}, La/e/a/c/a/f;->a(I)V

    :cond_6
    :goto_1
    return-void
.end method

.method protected final a(La/e/a/c/a/f;La/e/a/c/a/f;I)V
    .locals 1

    iget-object v0, p1, La/e/a/c/a/f;->l:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput p3, p1, La/e/a/c/a/f;->f:I

    iget-object p2, p2, La/e/a/c/a/f;->k:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected final a(La/e/a/c/a/f;La/e/a/c/a/f;ILa/e/a/c/a/g;)V
    .locals 2

    iget-object v0, p1, La/e/a/c/a/f;->l:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p1, La/e/a/c/a/f;->l:Ljava/util/List;

    iget-object v1, p0, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput p3, p1, La/e/a/c/a/f;->h:I

    iput-object p4, p1, La/e/a/c/a/f;->i:La/e/a/c/a/g;

    iget-object p2, p2, La/e/a/c/a/f;->k:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p2, p4, La/e/a/c/a/f;->k:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method abstract b()V
.end method

.method protected b(La/e/a/c/a/d;)V
    .locals 0

    return-void
.end method

.method abstract c()V
.end method

.method protected c(La/e/a/c/a/d;)V
    .locals 0

    return-void
.end method

.method public d()J
    .locals 2

    iget-object v0, p0, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget-boolean v1, v0, La/e/a/c/a/f;->j:Z

    if-eqz v1, :cond_0

    iget v0, v0, La/e/a/c/a/f;->g:I

    int-to-long v0, v0

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, La/e/a/c/a/s;->g:Z

    return v0
.end method

.method abstract f()Z
.end method
