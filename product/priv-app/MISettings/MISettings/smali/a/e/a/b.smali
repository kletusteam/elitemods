.class public La/e/a/b;
.super Ljava/lang/Object;

# interfaces
.implements La/e/a/d$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La/e/a/b$a;
    }
.end annotation


# instance fields
.field a:La/e/a/j;

.field b:F

.field c:Z

.field d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "La/e/a/j;",
            ">;"
        }
    .end annotation
.end field

.field public e:La/e/a/b$a;

.field f:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, La/e/a/b;->a:La/e/a/j;

    const/4 v0, 0x0

    iput v0, p0, La/e/a/b;->b:F

    const/4 v0, 0x0

    iput-boolean v0, p0, La/e/a/b;->c:Z

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, La/e/a/b;->d:Ljava/util/ArrayList;

    iput-boolean v0, p0, La/e/a/b;->f:Z

    return-void
.end method

.method public constructor <init>(La/e/a/c;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, La/e/a/b;->a:La/e/a/j;

    const/4 v0, 0x0

    iput v0, p0, La/e/a/b;->b:F

    const/4 v0, 0x0

    iput-boolean v0, p0, La/e/a/b;->c:Z

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, La/e/a/b;->d:Ljava/util/ArrayList;

    iput-boolean v0, p0, La/e/a/b;->f:Z

    new-instance v0, La/e/a/a;

    invoke-direct {v0, p0, p1}, La/e/a/a;-><init>(La/e/a/b;La/e/a/c;)V

    iput-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    return-void
.end method

.method private a([ZLa/e/a/j;)La/e/a/j;
    .locals 9

    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v0}, La/e/a/b$a;->a()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    move v4, v1

    :goto_0
    if-ge v3, v0, :cond_3

    iget-object v5, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v5, v3}, La/e/a/b$a;->b(I)F

    move-result v5

    cmpg-float v6, v5, v1

    if-gez v6, :cond_2

    iget-object v6, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v6, v3}, La/e/a/b$a;->a(I)La/e/a/j;

    move-result-object v6

    if-eqz p1, :cond_0

    iget v7, v6, La/e/a/j;->d:I

    aget-boolean v7, p1, v7

    if-nez v7, :cond_2

    :cond_0
    if-eq v6, p2, :cond_2

    iget-object v7, v6, La/e/a/j;->k:La/e/a/j$a;

    sget-object v8, La/e/a/j$a;->c:La/e/a/j$a;

    if-eq v7, v8, :cond_1

    sget-object v8, La/e/a/j$a;->d:La/e/a/j$a;

    if-ne v7, v8, :cond_2

    :cond_1
    cmpg-float v7, v5, v4

    if-gez v7, :cond_2

    move v4, v5

    move-object v2, v6

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    return-object v2
.end method

.method private a(La/e/a/j;La/e/a/d;)Z
    .locals 0

    iget p1, p1, La/e/a/j;->n:I

    const/4 p2, 0x1

    if-gt p1, p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    return p2
.end method


# virtual methods
.method public a(FFFLa/e/a/j;La/e/a/j;La/e/a/j;La/e/a/j;)La/e/a/b;
    .locals 4

    const/4 v0, 0x0

    iput v0, p0, La/e/a/b;->b:F

    cmpl-float v1, p2, v0

    const/high16 v2, -0x40800000    # -1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    if-eqz v1, :cond_3

    cmpl-float v1, p1, p3

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    cmpl-float v1, p1, v0

    if-nez v1, :cond_1

    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p1, p4, v3}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p1, p5, v2}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto :goto_1

    :cond_1
    cmpl-float v0, p3, v0

    if-nez v0, :cond_2

    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p1, p6, v3}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p1, p7, v2}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto :goto_1

    :cond_2
    div-float/2addr p1, p2

    div-float/2addr p3, p2

    div-float/2addr p1, p3

    iget-object p2, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p2, p4, v3}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object p2, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p2, p5, v2}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object p2, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p2, p7, p1}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object p2, p0, La/e/a/b;->e:La/e/a/b$a;

    neg-float p1, p1

    invoke-interface {p2, p6, p1}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto :goto_1

    :cond_3
    :goto_0
    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p1, p4, v3}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p1, p5, v2}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p1, p7, v3}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p1, p6, v2}, La/e/a/b$a;->a(La/e/a/j;F)V

    :goto_1
    return-object p0
.end method

.method public a(La/e/a/d;I)La/e/a/b;
    .locals 3

    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    const-string v1, "ep"

    invoke-virtual {p1, p2, v1}, La/e/a/d;->a(ILjava/lang/String;)La/e/a/j;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v0, v1, v2}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    const-string v1, "em"

    invoke-virtual {p1, p2, v1}, La/e/a/d;->a(ILjava/lang/String;)La/e/a/j;

    move-result-object p1

    const/high16 p2, -0x40800000    # -1.0f

    invoke-interface {v0, p1, p2}, La/e/a/b$a;->a(La/e/a/j;F)V

    return-object p0
.end method

.method a(La/e/a/j;I)La/e/a/b;
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    return-object p0

    :goto_1
    int-to-float p2, p2

    goto/32 :goto_2

    nop

    :goto_2
    invoke-interface {v0, p1, p2}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto/32 :goto_0

    nop

    :goto_3
    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_1

    nop
.end method

.method a(La/e/a/j;La/e/a/j;F)La/e/a/b;
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    invoke-interface {v0, p1, v1}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto/32 :goto_4

    nop

    :goto_1
    const/high16 v1, -0x40800000    # -1.0f

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_1

    nop

    :goto_3
    return-object p0

    :goto_4
    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_5

    nop

    :goto_5
    invoke-interface {p1, p2, p3}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto/32 :goto_3

    nop
.end method

.method public a(La/e/a/j;La/e/a/j;I)La/e/a/b;
    .locals 2

    const/4 v0, 0x0

    if-eqz p3, :cond_1

    if-gez p3, :cond_0

    mul-int/lit8 p3, p3, -0x1

    const/4 v0, 0x1

    :cond_0
    int-to-float p3, p3

    iput p3, p0, La/e/a/b;->b:F

    :cond_1
    const/high16 p3, -0x40800000    # -1.0f

    const/high16 v1, 0x3f800000    # 1.0f

    if-nez v0, :cond_2

    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v0, p1, p3}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p1, p2, v1}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v0, p1, v1}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p1, p2, p3}, La/e/a/b$a;->a(La/e/a/j;F)V

    :goto_0
    return-object p0
.end method

.method a(La/e/a/j;La/e/a/j;IFLa/e/a/j;La/e/a/j;I)La/e/a/b;
    .locals 5

    goto/32 :goto_d

    nop

    :goto_0
    if-lez p3, :cond_0

    goto/32 :goto_2e

    :cond_0
    goto/32 :goto_2d

    nop

    :goto_1
    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_27

    nop

    :goto_2
    int-to-float p2, p7

    goto/32 :goto_4d

    nop

    :goto_3
    invoke-interface {p1, p6, v0}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto/32 :goto_32

    nop

    :goto_4
    invoke-interface {p1, p2, v2}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto/32 :goto_5

    nop

    :goto_5
    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_11

    nop

    :goto_6
    if-eq p2, p5, :cond_1

    goto/32 :goto_31

    :cond_1
    goto/32 :goto_3e

    nop

    :goto_7
    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_e

    nop

    :goto_8
    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_3

    nop

    :goto_9
    mul-float v1, v3, v2

    goto/32 :goto_2a

    nop

    :goto_a
    cmpl-float v1, p4, v1

    goto/32 :goto_c

    nop

    :goto_b
    mul-float/2addr v2, p4

    goto/32 :goto_22

    nop

    :goto_c
    const/high16 v2, -0x40800000    # -1.0f

    goto/32 :goto_24

    nop

    :goto_d
    const/high16 v0, 0x3f800000    # 1.0f

    goto/32 :goto_6

    nop

    :goto_e
    invoke-interface {p1, p2, v0}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto/32 :goto_1a

    nop

    :goto_f
    return-object p0

    :goto_10
    cmpl-float v1, p4, v0

    goto/32 :goto_38

    nop

    :goto_11
    invoke-interface {p1, p5, v2}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto/32 :goto_1f

    nop

    :goto_12
    if-lez v1, :cond_2

    goto/32 :goto_21

    :cond_2
    goto/32 :goto_39

    nop

    :goto_13
    goto/16 :goto_4b

    :goto_14
    goto/32 :goto_3f

    nop

    :goto_15
    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_23

    nop

    :goto_16
    sub-float v3, v0, p4

    goto/32 :goto_44

    nop

    :goto_17
    invoke-interface {p1, p2, p3}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto/32 :goto_30

    nop

    :goto_18
    invoke-interface {p1, p6, v2}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto/32 :goto_15

    nop

    :goto_19
    neg-int p1, p3

    goto/32 :goto_43

    nop

    :goto_1a
    int-to-float p1, p3

    goto/32 :goto_45

    nop

    :goto_1b
    const/4 v1, 0x0

    goto/32 :goto_25

    nop

    :goto_1c
    iget-object p4, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_4c

    nop

    :goto_1d
    mul-float/2addr p1, v3

    goto/32 :goto_2

    nop

    :goto_1e
    if-lez p3, :cond_3

    goto/32 :goto_3d

    :cond_3
    goto/32 :goto_3c

    nop

    :goto_1f
    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_28

    nop

    :goto_20
    goto/16 :goto_4b

    :goto_21
    goto/32 :goto_10

    nop

    :goto_22
    invoke-interface {p1, p5, v2}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto/32 :goto_1

    nop

    :goto_23
    invoke-interface {p1, p5, v0}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto/32 :goto_3a

    nop

    :goto_24
    if-eqz v1, :cond_4

    goto/32 :goto_36

    :cond_4
    goto/32 :goto_1c

    nop

    :goto_25
    cmpg-float v1, p4, v1

    goto/32 :goto_12

    nop

    :goto_26
    add-float/2addr p1, p2

    goto/32 :goto_4a

    nop

    :goto_27
    mul-float/2addr v0, p4

    goto/32 :goto_47

    nop

    :goto_28
    invoke-interface {p1, p6, v0}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto/32 :goto_1e

    nop

    :goto_29
    int-to-float p1, p1

    goto/32 :goto_34

    nop

    :goto_2a
    invoke-interface {p1, p2, v1}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto/32 :goto_48

    nop

    :goto_2b
    const/high16 v1, 0x3f000000    # 0.5f

    goto/32 :goto_a

    nop

    :goto_2c
    iput p1, p0, La/e/a/b;->b:F

    goto/32 :goto_13

    nop

    :goto_2d
    if-gtz p7, :cond_5

    goto/32 :goto_4b

    :cond_5
    :goto_2e
    goto/32 :goto_33

    nop

    :goto_2f
    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_4

    nop

    :goto_30
    return-object p0

    :goto_31
    goto/32 :goto_2b

    nop

    :goto_32
    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_46

    nop

    :goto_33
    neg-int p1, p3

    goto/32 :goto_3b

    nop

    :goto_34
    iput p1, p0, La/e/a/b;->b:F

    goto/32 :goto_35

    nop

    :goto_35
    goto :goto_4b

    :goto_36
    goto/32 :goto_1b

    nop

    :goto_37
    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_18

    nop

    :goto_38
    if-gez v1, :cond_6

    goto/32 :goto_14

    :cond_6
    goto/32 :goto_37

    nop

    :goto_39
    iget-object p4, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_4e

    nop

    :goto_3a
    neg-int p1, p7

    goto/32 :goto_40

    nop

    :goto_3b
    int-to-float p1, p1

    goto/32 :goto_1d

    nop

    :goto_3c
    if-gtz p7, :cond_7

    goto/32 :goto_4b

    :cond_7
    :goto_3d
    goto/32 :goto_19

    nop

    :goto_3e
    iget-object p3, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_49

    nop

    :goto_3f
    iget-object v1, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_16

    nop

    :goto_40
    int-to-float p1, p1

    goto/32 :goto_2c

    nop

    :goto_41
    invoke-interface {v1, p1, v4}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto/32 :goto_42

    nop

    :goto_42
    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_9

    nop

    :goto_43
    add-int/2addr p1, p7

    goto/32 :goto_29

    nop

    :goto_44
    mul-float v4, v3, v0

    goto/32 :goto_41

    nop

    :goto_45
    iput p1, p0, La/e/a/b;->b:F

    goto/32 :goto_20

    nop

    :goto_46
    const/high16 p3, -0x40000000    # -2.0f

    goto/32 :goto_17

    nop

    :goto_47
    invoke-interface {p1, p6, v0}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto/32 :goto_0

    nop

    :goto_48
    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_b

    nop

    :goto_49
    invoke-interface {p3, p1, v0}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto/32 :goto_8

    nop

    :goto_4a
    iput p1, p0, La/e/a/b;->b:F

    :goto_4b
    goto/32 :goto_f

    nop

    :goto_4c
    invoke-interface {p4, p1, v0}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto/32 :goto_2f

    nop

    :goto_4d
    mul-float/2addr p2, p4

    goto/32 :goto_26

    nop

    :goto_4e
    invoke-interface {p4, p1, v2}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto/32 :goto_7

    nop
.end method

.method public a(La/e/a/j;La/e/a/j;La/e/a/j;I)La/e/a/b;
    .locals 2

    const/4 v0, 0x0

    if-eqz p4, :cond_1

    if-gez p4, :cond_0

    mul-int/lit8 p4, p4, -0x1

    const/4 v0, 0x1

    :cond_0
    int-to-float p4, p4

    iput p4, p0, La/e/a/b;->b:F

    :cond_1
    const/high16 p4, -0x40800000    # -1.0f

    const/high16 v1, 0x3f800000    # 1.0f

    if-nez v0, :cond_2

    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v0, p1, p4}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p1, p2, v1}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p1, p3, v1}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v0, p1, v1}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p1, p2, p4}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p1, p3, p4}, La/e/a/b$a;->a(La/e/a/j;F)V

    :goto_0
    return-object p0
.end method

.method public a(La/e/a/j;La/e/a/j;La/e/a/j;La/e/a/j;F)La/e/a/b;
    .locals 2

    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    const/high16 v1, -0x40800000    # -1.0f

    invoke-interface {v0, p1, v1}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-interface {p1, p2, v0}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p1, p3, p5}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    neg-float p2, p5

    invoke-interface {p1, p4, p2}, La/e/a/b$a;->a(La/e/a/j;F)V

    return-object p0
.end method

.method public a(La/e/a/d;[Z)La/e/a/j;
    .locals 0

    const/4 p1, 0x0

    invoke-direct {p0, p2, p1}, La/e/a/b;->a([ZLa/e/a/j;)La/e/a/j;

    move-result-object p1

    return-object p1
.end method

.method a()V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    const/4 v1, 0x0

    goto/32 :goto_9

    nop

    :goto_1
    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_3

    nop

    :goto_2
    iget v0, p0, La/e/a/b;->b:F

    goto/32 :goto_0

    nop

    :goto_3
    invoke-interface {v0}, La/e/a/b$a;->b()V

    :goto_4
    goto/32 :goto_8

    nop

    :goto_5
    iput v0, p0, La/e/a/b;->b:F

    goto/32 :goto_1

    nop

    :goto_6
    mul-float/2addr v0, v1

    goto/32 :goto_5

    nop

    :goto_7
    if-ltz v1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_a

    nop

    :goto_8
    return-void

    :goto_9
    cmpg-float v1, v0, v1

    goto/32 :goto_7

    nop

    :goto_a
    const/high16 v1, -0x40800000    # -1.0f

    goto/32 :goto_6

    nop
.end method

.method public a(La/e/a/d$a;)V
    .locals 5

    instance-of v0, p1, La/e/a/b;

    if-eqz v0, :cond_0

    check-cast p1, La/e/a/b;

    const/4 v0, 0x0

    iput-object v0, p0, La/e/a/b;->a:La/e/a/j;

    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v0}, La/e/a/b$a;->clear()V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p1, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v1}, La/e/a/b$a;->a()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p1, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v1, v0}, La/e/a/b$a;->a(I)La/e/a/j;

    move-result-object v1

    iget-object v2, p1, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v2, v0}, La/e/a/b$a;->b(I)F

    move-result v2

    iget-object v3, p0, La/e/a/b;->e:La/e/a/b$a;

    const/4 v4, 0x1

    invoke-interface {v3, v1, v2, v4}, La/e/a/b$a;->a(La/e/a/j;FZ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(La/e/a/d;La/e/a/b;Z)V
    .locals 3

    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v0, p2, p3}, La/e/a/b$a;->a(La/e/a/b;Z)F

    move-result v0

    iget v1, p0, La/e/a/b;->b:F

    iget v2, p2, La/e/a/b;->b:F

    mul-float/2addr v2, v0

    add-float/2addr v1, v2

    iput v1, p0, La/e/a/b;->b:F

    if-eqz p3, :cond_0

    iget-object p2, p2, La/e/a/b;->a:La/e/a/j;

    invoke-virtual {p2, p0}, La/e/a/j;->b(La/e/a/b;)V

    :cond_0
    sget-boolean p2, La/e/a/d;->c:Z

    if-eqz p2, :cond_1

    iget-object p2, p0, La/e/a/b;->a:La/e/a/j;

    if-eqz p2, :cond_1

    iget-object p2, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p2}, La/e/a/b$a;->a()I

    move-result p2

    if-nez p2, :cond_1

    const/4 p2, 0x1

    iput-boolean p2, p0, La/e/a/b;->f:Z

    iput-boolean p2, p1, La/e/a/d;->j:Z

    :cond_1
    return-void
.end method

.method public a(La/e/a/d;La/e/a/j;Z)V
    .locals 3

    if-eqz p2, :cond_2

    iget-boolean v0, p2, La/e/a/j;->h:Z

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v0, p2}, La/e/a/b$a;->b(La/e/a/j;)F

    move-result v0

    iget v1, p0, La/e/a/b;->b:F

    iget v2, p2, La/e/a/j;->g:F

    mul-float/2addr v2, v0

    add-float/2addr v1, v2

    iput v1, p0, La/e/a/b;->b:F

    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v0, p2, p3}, La/e/a/b$a;->a(La/e/a/j;Z)F

    if-eqz p3, :cond_1

    invoke-virtual {p2, p0}, La/e/a/j;->b(La/e/a/b;)V

    :cond_1
    sget-boolean p2, La/e/a/d;->c:Z

    if-eqz p2, :cond_2

    iget-object p2, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p2}, La/e/a/b$a;->a()I

    move-result p2

    if-nez p2, :cond_2

    const/4 p2, 0x1

    iput-boolean p2, p0, La/e/a/b;->f:Z

    iput-boolean p2, p1, La/e/a/d;->j:Z

    :cond_2
    :goto_0
    return-void
.end method

.method public a(La/e/a/j;)V
    .locals 3

    iget v0, p1, La/e/a/j;->f:I

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    const/high16 v1, 0x447a0000    # 1000.0f

    goto :goto_0

    :cond_1
    const/4 v2, 0x3

    if-ne v0, v2, :cond_2

    const v1, 0x49742400    # 1000000.0f

    goto :goto_0

    :cond_2
    const/4 v2, 0x4

    if-ne v0, v2, :cond_3

    const v1, 0x4e6e6b28    # 1.0E9f

    goto :goto_0

    :cond_3
    const/4 v2, 0x5

    if-ne v0, v2, :cond_4

    const v1, 0x5368d4a5    # 1.0E12f

    :cond_4
    :goto_0
    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v0, p1, v1}, La/e/a/b$a;->a(La/e/a/j;F)V

    return-void
.end method

.method a(La/e/a/d;)Z
    .locals 2

    goto/32 :goto_4

    nop

    :goto_0
    iget-object v1, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_3

    nop

    :goto_1
    move p1, v0

    goto/32 :goto_b

    nop

    :goto_2
    return p1

    :goto_3
    invoke-interface {v1}, La/e/a/b$a;->a()I

    move-result v1

    goto/32 :goto_d

    nop

    :goto_4
    invoke-virtual {p0, p1}, La/e/a/b;->b(La/e/a/d;)La/e/a/j;

    move-result-object p1

    goto/32 :goto_5

    nop

    :goto_5
    const/4 v0, 0x1

    goto/32 :goto_e

    nop

    :goto_6
    invoke-virtual {p0, p1}, La/e/a/b;->d(La/e/a/j;)V

    goto/32 :goto_9

    nop

    :goto_7
    iput-boolean v0, p0, La/e/a/b;->f:Z

    :goto_8
    goto/32 :goto_2

    nop

    :goto_9
    const/4 p1, 0x0

    :goto_a
    goto/32 :goto_0

    nop

    :goto_b
    goto :goto_a

    :goto_c
    goto/32 :goto_6

    nop

    :goto_d
    if-eqz v1, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_7

    nop

    :goto_e
    if-eqz p1, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_1

    nop
.end method

.method b(La/e/a/j;I)La/e/a/b;
    .locals 0

    goto/32 :goto_4

    nop

    :goto_0
    iput-boolean p1, p0, La/e/a/b;->f:Z

    goto/32 :goto_2

    nop

    :goto_1
    iput p2, p0, La/e/a/b;->b:F

    goto/32 :goto_5

    nop

    :goto_2
    return-object p0

    :goto_3
    iput p2, p1, La/e/a/j;->g:F

    goto/32 :goto_1

    nop

    :goto_4
    iput-object p1, p0, La/e/a/b;->a:La/e/a/j;

    goto/32 :goto_6

    nop

    :goto_5
    const/4 p1, 0x1

    goto/32 :goto_0

    nop

    :goto_6
    int-to-float p2, p2

    goto/32 :goto_3

    nop
.end method

.method public b(La/e/a/j;La/e/a/j;La/e/a/j;I)La/e/a/b;
    .locals 2

    const/4 v0, 0x0

    if-eqz p4, :cond_1

    if-gez p4, :cond_0

    mul-int/lit8 p4, p4, -0x1

    const/4 v0, 0x1

    :cond_0
    int-to-float p4, p4

    iput p4, p0, La/e/a/b;->b:F

    :cond_1
    const/high16 p4, -0x40800000    # -1.0f

    const/high16 v1, 0x3f800000    # 1.0f

    if-nez v0, :cond_2

    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v0, p1, p4}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p1, p2, v1}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p1, p3, p4}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v0, p1, v1}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p1, p2, p4}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p1, p3, v1}, La/e/a/b$a;->a(La/e/a/j;F)V

    :goto_0
    return-object p0
.end method

.method public b(La/e/a/j;La/e/a/j;La/e/a/j;La/e/a/j;F)La/e/a/b;
    .locals 2

    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-interface {v0, p3, v1}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object p3, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p3, p4, v1}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object p3, p0, La/e/a/b;->e:La/e/a/b$a;

    const/high16 p4, -0x41000000    # -0.5f

    invoke-interface {p3, p1, p4}, La/e/a/b$a;->a(La/e/a/j;F)V

    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p1, p2, p4}, La/e/a/b$a;->a(La/e/a/j;F)V

    neg-float p1, p5

    iput p1, p0, La/e/a/b;->b:F

    return-object p0
.end method

.method b(La/e/a/d;)La/e/a/j;
    .locals 14

    goto/32 :goto_2c

    nop

    :goto_0
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_32

    nop

    :goto_1
    const/4 v13, 0x1

    goto/32 :goto_16

    nop

    :goto_2
    if-gtz v11, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_12

    nop

    :goto_3
    invoke-interface {v9, v2}, La/e/a/b$a;->b(I)F

    move-result v9

    goto/32 :goto_27

    nop

    :goto_4
    invoke-direct {p0, v10, p1}, La/e/a/b;->a(La/e/a/j;La/e/a/d;)Z

    move-result v4

    :goto_5
    goto/32 :goto_41

    nop

    :goto_6
    move v7, v9

    goto/32 :goto_24

    nop

    :goto_7
    const/4 v1, 0x0

    goto/32 :goto_1c

    nop

    :goto_8
    if-gtz v11, :cond_1

    goto/32 :goto_1f

    :cond_1
    goto/32 :goto_2d

    nop

    :goto_9
    goto/16 :goto_3c

    :goto_a
    goto/32 :goto_e

    nop

    :goto_b
    invoke-interface {v0}, La/e/a/b$a;->a()I

    move-result v0

    goto/32 :goto_7

    nop

    :goto_c
    move-object v4, v10

    goto/32 :goto_f

    nop

    :goto_d
    if-eqz v1, :cond_2

    goto/32 :goto_35

    :cond_2
    goto/32 :goto_3e

    nop

    :goto_e
    if-eqz v6, :cond_3

    goto/32 :goto_35

    :cond_3
    goto/32 :goto_46

    nop

    :goto_f
    goto/16 :goto_35

    :goto_10
    goto/32 :goto_19

    nop

    :goto_11
    move-object v4, v1

    goto/32 :goto_1a

    nop

    :goto_12
    invoke-direct {p0, v10, p1}, La/e/a/b;->a(La/e/a/j;La/e/a/d;)Z

    move-result v1

    goto/32 :goto_9

    nop

    :goto_13
    move v8, v6

    goto/32 :goto_23

    nop

    :goto_14
    sget-object v12, La/e/a/j$a;->a:La/e/a/j$a;

    goto/32 :goto_1

    nop

    :goto_15
    if-ltz v11, :cond_4

    goto/32 :goto_35

    :cond_4
    goto/32 :goto_45

    nop

    :goto_16
    if-eq v11, v12, :cond_5

    goto/32 :goto_26

    :cond_5
    goto/32 :goto_28

    nop

    :goto_17
    if-nez v11, :cond_6

    goto/32 :goto_35

    :cond_6
    goto/32 :goto_6

    nop

    :goto_18
    iget-object v11, v10, La/e/a/j;->k:La/e/a/j$a;

    goto/32 :goto_14

    nop

    :goto_19
    cmpl-float v11, v7, v9

    goto/32 :goto_8

    nop

    :goto_1a
    move v6, v2

    goto/32 :goto_13

    nop

    :goto_1b
    invoke-direct {p0, v10, p1}, La/e/a/b;->a(La/e/a/j;La/e/a/d;)Z

    move-result v11

    goto/32 :goto_17

    nop

    :goto_1c
    const/4 v2, 0x0

    goto/32 :goto_2b

    nop

    :goto_1d
    cmpl-float v11, v5, v9

    goto/32 :goto_2

    nop

    :goto_1e
    goto/16 :goto_5

    :goto_1f
    goto/32 :goto_31

    nop

    :goto_20
    return-object v1

    :goto_21
    goto/32 :goto_30

    nop

    :goto_22
    move v6, v13

    goto/32 :goto_25

    nop

    :goto_23
    move v5, v3

    goto/32 :goto_3f

    nop

    :goto_24
    move-object v4, v10

    goto/32 :goto_34

    nop

    :goto_25
    goto :goto_35

    :goto_26
    goto/32 :goto_d

    nop

    :goto_27
    iget-object v10, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_29

    nop

    :goto_28
    if-eqz v1, :cond_7

    goto/32 :goto_38

    :cond_7
    goto/32 :goto_3b

    nop

    :goto_29
    invoke-interface {v10, v2}, La/e/a/b$a;->a(I)La/e/a/j;

    move-result-object v10

    goto/32 :goto_18

    nop

    :goto_2a
    move v6, v1

    goto/32 :goto_42

    nop

    :goto_2b
    const/4 v3, 0x0

    goto/32 :goto_11

    nop

    :goto_2c
    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_b

    nop

    :goto_2d
    invoke-direct {p0, v10, p1}, La/e/a/b;->a(La/e/a/j;La/e/a/d;)Z

    move-result v4

    goto/32 :goto_1e

    nop

    :goto_2e
    move v7, v9

    goto/32 :goto_c

    nop

    :goto_2f
    iget-object v9, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_3

    nop

    :goto_30
    return-object v4

    :goto_31
    if-eqz v8, :cond_8

    goto/32 :goto_35

    :cond_8
    goto/32 :goto_1b

    nop

    :goto_32
    goto :goto_40

    :goto_33
    goto/32 :goto_44

    nop

    :goto_34
    move v8, v13

    :goto_35
    goto/32 :goto_0

    nop

    :goto_36
    if-nez v11, :cond_9

    goto/32 :goto_35

    :cond_9
    goto/32 :goto_3d

    nop

    :goto_37
    goto :goto_35

    :goto_38
    goto/32 :goto_1d

    nop

    :goto_39
    move-object v1, v10

    goto/32 :goto_37

    nop

    :goto_3a
    move-object v1, v10

    goto/32 :goto_22

    nop

    :goto_3b
    invoke-direct {p0, v10, p1}, La/e/a/b;->a(La/e/a/j;La/e/a/d;)Z

    move-result v1

    :goto_3c
    goto/32 :goto_2a

    nop

    :goto_3d
    move v5, v9

    goto/32 :goto_3a

    nop

    :goto_3e
    cmpg-float v11, v9, v3

    goto/32 :goto_15

    nop

    :goto_3f
    move v7, v5

    :goto_40
    goto/32 :goto_43

    nop

    :goto_41
    move v8, v4

    goto/32 :goto_2e

    nop

    :goto_42
    move v5, v9

    goto/32 :goto_39

    nop

    :goto_43
    if-lt v2, v0, :cond_a

    goto/32 :goto_33

    :cond_a
    goto/32 :goto_2f

    nop

    :goto_44
    if-nez v1, :cond_b

    goto/32 :goto_21

    :cond_b
    goto/32 :goto_20

    nop

    :goto_45
    if-eqz v4, :cond_c

    goto/32 :goto_10

    :cond_c
    goto/32 :goto_4

    nop

    :goto_46
    invoke-direct {p0, v10, p1}, La/e/a/b;->a(La/e/a/j;La/e/a/d;)Z

    move-result v11

    goto/32 :goto_36

    nop
.end method

.method public b(La/e/a/d;La/e/a/j;Z)V
    .locals 3

    if-eqz p2, :cond_2

    iget-boolean v0, p2, La/e/a/j;->o:Z

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v0, p2}, La/e/a/b$a;->b(La/e/a/j;)F

    move-result v0

    iget v1, p0, La/e/a/b;->b:F

    iget v2, p2, La/e/a/j;->q:F

    mul-float/2addr v2, v0

    add-float/2addr v1, v2

    iput v1, p0, La/e/a/b;->b:F

    iget-object v1, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v1, p2, p3}, La/e/a/b$a;->a(La/e/a/j;Z)F

    if-eqz p3, :cond_1

    invoke-virtual {p2, p0}, La/e/a/j;->b(La/e/a/b;)V

    :cond_1
    iget-object v1, p0, La/e/a/b;->e:La/e/a/b$a;

    iget-object v2, p1, La/e/a/d;->w:La/e/a/c;

    iget-object v2, v2, La/e/a/c;->d:[La/e/a/j;

    iget p2, p2, La/e/a/j;->p:I

    aget-object p2, v2, p2

    invoke-interface {v1, p2, v0, p3}, La/e/a/b$a;->a(La/e/a/j;FZ)V

    sget-boolean p2, La/e/a/d;->c:Z

    if-eqz p2, :cond_2

    iget-object p2, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p2}, La/e/a/b$a;->a()I

    move-result p2

    if-nez p2, :cond_2

    const/4 p2, 0x1

    iput-boolean p2, p0, La/e/a/b;->f:Z

    iput-boolean p2, p1, La/e/a/d;->j:Z

    :cond_2
    :goto_0
    return-void
.end method

.method b()Z
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, La/e/a/b;->a:La/e/a/j;

    goto/32 :goto_5

    nop

    :goto_1
    return v0

    :goto_2
    cmpg-float v0, v0, v1

    goto/32 :goto_d

    nop

    :goto_3
    const/4 v0, 0x0

    :goto_4
    goto/32 :goto_1

    nop

    :goto_5
    if-nez v0, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_8

    nop

    :goto_6
    const/4 v1, 0x0

    goto/32 :goto_2

    nop

    :goto_7
    iget v0, p0, La/e/a/b;->b:F

    goto/32 :goto_6

    nop

    :goto_8
    iget-object v0, v0, La/e/a/j;->k:La/e/a/j$a;

    goto/32 :goto_c

    nop

    :goto_9
    if-ne v0, v1, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_7

    nop

    :goto_a
    goto :goto_4

    :goto_b
    goto/32 :goto_3

    nop

    :goto_c
    sget-object v1, La/e/a/j$a;->a:La/e/a/j$a;

    goto/32 :goto_9

    nop

    :goto_d
    if-gez v0, :cond_2

    goto/32 :goto_b

    :cond_2
    :goto_e
    goto/32 :goto_f

    nop

    :goto_f
    const/4 v0, 0x1

    goto/32 :goto_a

    nop
.end method

.method b(La/e/a/j;)Z
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-interface {v0, p1}, La/e/a/b$a;->a(La/e/a/j;)Z

    move-result p1

    goto/32 :goto_1

    nop

    :goto_1
    return p1

    :goto_2
    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_0

    nop
.end method

.method public c(La/e/a/j;I)La/e/a/b;
    .locals 1

    if-gez p2, :cond_0

    mul-int/lit8 p2, p2, -0x1

    int-to-float p2, p2

    iput p2, p0, La/e/a/b;->b:F

    iget-object p2, p0, La/e/a/b;->e:La/e/a/b$a;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-interface {p2, p1, v0}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto :goto_0

    :cond_0
    int-to-float p2, p2

    iput p2, p0, La/e/a/b;->b:F

    iget-object p2, p0, La/e/a/b;->e:La/e/a/b$a;

    const/high16 v0, -0x40800000    # -1.0f

    invoke-interface {p2, p1, v0}, La/e/a/b$a;->a(La/e/a/j;F)V

    :goto_0
    return-object p0
.end method

.method public c(La/e/a/j;)La/e/a/j;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, La/e/a/b;->a([ZLa/e/a/j;)La/e/a/j;

    move-result-object p1

    return-object p1
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, La/e/a/b;->a:La/e/a/j;

    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v0}, La/e/a/b$a;->clear()V

    const/4 v0, 0x0

    iput v0, p0, La/e/a/b;->b:F

    const/4 v0, 0x0

    iput-boolean v0, p0, La/e/a/b;->f:Z

    return-void
.end method

.method public c(La/e/a/d;)V
    .locals 8

    iget-object v0, p1, La/e/a/d;->p:[La/e/a/b;

    array-length v0, v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/4 v2, 0x1

    if-nez v1, :cond_8

    iget-object v3, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v3}, La/e/a/b$a;->a()I

    move-result v3

    move v4, v0

    :goto_1
    if-ge v4, v3, :cond_3

    iget-object v5, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v5, v4}, La/e/a/b$a;->a(I)La/e/a/j;

    move-result-object v5

    iget v6, v5, La/e/a/j;->e:I

    const/4 v7, -0x1

    if-ne v6, v7, :cond_1

    iget-boolean v6, v5, La/e/a/j;->h:Z

    if-nez v6, :cond_1

    iget-boolean v6, v5, La/e/a/j;->o:Z

    if-eqz v6, :cond_2

    :cond_1
    iget-object v6, p0, La/e/a/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_3
    iget-object v3, p0, La/e/a/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_7

    move v4, v0

    :goto_2
    if-ge v4, v3, :cond_6

    iget-object v5, p0, La/e/a/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/j;

    iget-boolean v6, v5, La/e/a/j;->h:Z

    if-eqz v6, :cond_4

    invoke-virtual {p0, p1, v5, v2}, La/e/a/b;->a(La/e/a/d;La/e/a/j;Z)V

    goto :goto_3

    :cond_4
    iget-boolean v6, v5, La/e/a/j;->o:Z

    if-eqz v6, :cond_5

    invoke-virtual {p0, p1, v5, v2}, La/e/a/b;->b(La/e/a/d;La/e/a/j;Z)V

    goto :goto_3

    :cond_5
    iget-object v6, p1, La/e/a/d;->p:[La/e/a/b;

    iget v5, v5, La/e/a/j;->e:I

    aget-object v5, v6, v5

    invoke-virtual {p0, p1, v5, v2}, La/e/a/b;->a(La/e/a/d;La/e/a/b;Z)V

    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_6
    iget-object v2, p0, La/e/a/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    :cond_7
    move v1, v2

    goto :goto_0

    :cond_8
    sget-boolean v0, La/e/a/d;->c:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, La/e/a/b;->a:La/e/a/j;

    if-eqz v0, :cond_9

    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v0}, La/e/a/b$a;->a()I

    move-result v0

    if-nez v0, :cond_9

    iput-boolean v2, p0, La/e/a/b;->f:Z

    iput-boolean v2, p1, La/e/a/d;->j:Z

    :cond_9
    return-void
.end method

.method public clear()V
    .locals 1

    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v0}, La/e/a/b$a;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, La/e/a/b;->a:La/e/a/j;

    const/4 v0, 0x0

    iput v0, p0, La/e/a/b;->b:F

    return-void
.end method

.method d()Ljava/lang/String;
    .locals 10

    goto/32 :goto_3a

    nop

    :goto_0
    const/high16 v9, -0x40800000    # -1.0f

    goto/32 :goto_40

    nop

    :goto_1
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_4a

    nop

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_19

    nop

    :goto_3
    iget v1, p0, La/e/a/b;->b:F

    goto/32 :goto_17

    nop

    :goto_4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1e

    nop

    :goto_5
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_62

    nop

    :goto_6
    invoke-interface {v7, v3}, La/e/a/b$a;->b(I)F

    move-result v7

    goto/32 :goto_d

    nop

    :goto_7
    goto/16 :goto_4b

    :goto_8
    goto/32 :goto_56

    nop

    :goto_9
    if-eqz v1, :cond_0

    goto/32 :goto_29

    :cond_0
    goto/32 :goto_1f

    nop

    :goto_a
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_48

    nop

    :goto_b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_76

    nop

    :goto_c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_32

    nop

    :goto_d
    cmpl-float v8, v7, v2

    goto/32 :goto_16

    nop

    :goto_e
    const-string v0, " - "

    goto/32 :goto_5

    nop

    :goto_f
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_31

    nop

    :goto_10
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_38

    nop

    :goto_11
    if-eqz v6, :cond_1

    goto/32 :goto_2e

    :cond_1
    goto/32 :goto_2d

    nop

    :goto_12
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_3f

    nop

    :goto_13
    iget-object v1, p0, La/e/a/b;->a:La/e/a/j;

    goto/32 :goto_55

    nop

    :goto_14
    if-eqz v0, :cond_2

    goto/32 :goto_59

    :cond_2
    goto/32 :goto_20

    nop

    :goto_15
    const-string v0, "0.0"

    goto/32 :goto_18

    nop

    :goto_16
    if-eqz v8, :cond_3

    goto/32 :goto_3d

    :cond_3
    goto/32 :goto_3c

    nop

    :goto_17
    const/4 v2, 0x0

    goto/32 :goto_6f

    nop

    :goto_18
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_28

    nop

    :goto_19
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_4f

    nop

    :goto_1a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_61

    nop

    :goto_1b
    if-ltz v1, :cond_4

    goto/32 :goto_46

    :cond_4
    goto/32 :goto_a

    nop

    :goto_1c
    move v1, v3

    :goto_1d
    goto/32 :goto_41

    nop

    :goto_1e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_5b

    nop

    :goto_1f
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_c

    nop

    :goto_20
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_4

    nop

    :goto_21
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_65

    nop

    :goto_22
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_10

    nop

    :goto_23
    return-object v0

    :goto_24
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_5f

    nop

    :goto_25
    iget-object v7, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_6

    nop

    :goto_26
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_27
    goto/32 :goto_6b

    nop

    :goto_28
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_29
    goto/32 :goto_23

    nop

    :goto_2a
    cmpg-float v1, v7, v2

    goto/32 :goto_1b

    nop

    :goto_2b
    const-string v0, " = "

    goto/32 :goto_f

    nop

    :goto_2c
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_2b

    nop

    :goto_2d
    goto/16 :goto_6e

    :goto_2e
    goto/32 :goto_25

    nop

    :goto_2f
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_64

    nop

    :goto_30
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    goto/32 :goto_b

    nop

    :goto_31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_32
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_15

    nop

    :goto_33
    const-string v0, "- "

    goto/32 :goto_24

    nop

    :goto_34
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    goto/32 :goto_3b

    nop

    :goto_35
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_2c

    nop

    :goto_36
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_5c

    nop

    :goto_37
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop

    :goto_38
    const-string v0, " + "

    goto/32 :goto_6a

    nop

    :goto_39
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_70

    nop

    :goto_3a
    iget-object v0, p0, La/e/a/b;->a:La/e/a/j;

    goto/32 :goto_49

    nop

    :goto_3b
    const-string v0, " "

    goto/32 :goto_37

    nop

    :goto_3c
    goto/16 :goto_6e

    :goto_3d
    goto/32 :goto_44

    nop

    :goto_3e
    if-lt v3, v5, :cond_5

    goto/32 :goto_71

    :cond_5
    goto/32 :goto_5a

    nop

    :goto_3f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_2

    nop

    :goto_40
    if-eqz v1, :cond_6

    goto/32 :goto_52

    :cond_6
    goto/32 :goto_2a

    nop

    :goto_41
    iget-object v5, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_68

    nop

    :goto_42
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_e

    nop

    :goto_43
    invoke-interface {v6, v3}, La/e/a/b$a;->a(I)La/e/a/j;

    move-result-object v6

    goto/32 :goto_11

    nop

    :goto_44
    invoke-virtual {v6}, La/e/a/j;->toString()Ljava/lang/String;

    move-result-object v6

    goto/32 :goto_0

    nop

    :goto_45
    mul-float/2addr v7, v9

    :goto_46
    goto/32 :goto_47

    nop

    :goto_47
    const/high16 v1, 0x3f800000    # 1.0f

    goto/32 :goto_77

    nop

    :goto_48
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_54

    nop

    :goto_49
    const-string v1, ""

    goto/32 :goto_14

    nop

    :goto_4a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_4b
    goto/32 :goto_6d

    nop

    :goto_4c
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_22

    nop

    :goto_4d
    if-nez v1, :cond_7

    goto/32 :goto_74

    :cond_7
    goto/32 :goto_21

    nop

    :goto_4e
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_75

    nop

    :goto_4f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_50
    if-eqz v1, :cond_8

    goto/32 :goto_8

    :cond_8
    goto/32 :goto_12

    nop

    :goto_51
    goto :goto_63

    :goto_52
    goto/32 :goto_60

    nop

    :goto_53
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_72

    nop

    :goto_54
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_33

    nop

    :goto_55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_26

    nop

    :goto_56
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_1a

    nop

    :goto_57
    const/4 v3, 0x0

    goto/32 :goto_66

    nop

    :goto_58
    goto/16 :goto_27

    :goto_59
    goto/32 :goto_53

    nop

    :goto_5a
    iget-object v6, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_43

    nop

    :goto_5b
    const-string v1, "0"

    goto/32 :goto_67

    nop

    :goto_5c
    goto :goto_46

    :goto_5d
    goto/32 :goto_4e

    nop

    :goto_5e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_13

    nop

    :goto_5f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_51

    nop

    :goto_60
    if-gtz v8, :cond_9

    goto/32 :goto_5d

    :cond_9
    goto/32 :goto_4c

    nop

    :goto_61
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_34

    nop

    :goto_62
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_63
    goto/32 :goto_45

    nop

    :goto_64
    iget v0, p0, La/e/a/b;->b:F

    goto/32 :goto_30

    nop

    :goto_65
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_2f

    nop

    :goto_66
    const/4 v4, 0x1

    goto/32 :goto_4d

    nop

    :goto_67
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_6c

    nop

    :goto_68
    invoke-interface {v5}, La/e/a/b$a;->a()I

    move-result v5

    :goto_69
    goto/32 :goto_3e

    nop

    :goto_6a
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_36

    nop

    :goto_6b
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_35

    nop

    :goto_6c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_58

    nop

    :goto_6d
    move v1, v4

    :goto_6e
    goto/32 :goto_39

    nop

    :goto_6f
    cmpl-float v1, v1, v2

    goto/32 :goto_57

    nop

    :goto_70
    goto/16 :goto_69

    :goto_71
    goto/32 :goto_9

    nop

    :goto_72
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_5e

    nop

    :goto_73
    goto/16 :goto_1d

    :goto_74
    goto/32 :goto_1c

    nop

    :goto_75
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_42

    nop

    :goto_76
    move v1, v4

    goto/32 :goto_73

    nop

    :goto_77
    cmpl-float v1, v7, v1

    goto/32 :goto_50

    nop
.end method

.method d(La/e/a/j;)V
    .locals 3

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, La/e/a/b;->a:La/e/a/j;

    goto/32 :goto_1

    nop

    :goto_1
    const/high16 v1, -0x40800000    # -1.0f

    goto/32 :goto_b

    nop

    :goto_2
    const/4 v0, 0x0

    goto/32 :goto_17

    nop

    :goto_3
    iput v2, v0, La/e/a/j;->e:I

    goto/32 :goto_2

    nop

    :goto_4
    mul-float/2addr v0, v1

    goto/32 :goto_c

    nop

    :goto_5
    return-void

    :goto_6
    invoke-interface {p1, v0}, La/e/a/b$a;->a(F)V

    goto/32 :goto_5

    nop

    :goto_7
    cmpl-float p1, v0, p1

    goto/32 :goto_8

    nop

    :goto_8
    if-eqz p1, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_13

    nop

    :goto_9
    iget-object p1, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_6

    nop

    :goto_a
    const/4 v2, 0x1

    goto/32 :goto_e

    nop

    :goto_b
    if-nez v0, :cond_1

    goto/32 :goto_18

    :cond_1
    goto/32 :goto_1a

    nop

    :goto_c
    iput-object p1, p0, La/e/a/b;->a:La/e/a/j;

    goto/32 :goto_d

    nop

    :goto_d
    const/high16 p1, 0x3f800000    # 1.0f

    goto/32 :goto_7

    nop

    :goto_e
    invoke-interface {v0, p1, v2}, La/e/a/b$a;->a(La/e/a/j;Z)F

    move-result v0

    goto/32 :goto_4

    nop

    :goto_f
    div-float/2addr p1, v0

    goto/32 :goto_19

    nop

    :goto_10
    iget-object v0, p0, La/e/a/b;->a:La/e/a/j;

    goto/32 :goto_15

    nop

    :goto_11
    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_a

    nop

    :goto_12
    invoke-interface {v2, v0, v1}, La/e/a/b$a;->a(La/e/a/j;F)V

    goto/32 :goto_10

    nop

    :goto_13
    return-void

    :goto_14
    goto/32 :goto_16

    nop

    :goto_15
    const/4 v2, -0x1

    goto/32 :goto_3

    nop

    :goto_16
    iget p1, p0, La/e/a/b;->b:F

    goto/32 :goto_f

    nop

    :goto_17
    iput-object v0, p0, La/e/a/b;->a:La/e/a/j;

    :goto_18
    goto/32 :goto_11

    nop

    :goto_19
    iput p1, p0, La/e/a/b;->b:F

    goto/32 :goto_9

    nop

    :goto_1a
    iget-object v2, p0, La/e/a/b;->e:La/e/a/b$a;

    goto/32 :goto_12

    nop
.end method

.method public getKey()La/e/a/j;
    .locals 1

    iget-object v0, p0, La/e/a/b;->a:La/e/a/j;

    return-object v0
.end method

.method public isEmpty()Z
    .locals 2

    iget-object v0, p0, La/e/a/b;->a:La/e/a/j;

    if-nez v0, :cond_0

    iget v0, p0, La/e/a/b;->b:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v0}, La/e/a/b$a;->a()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, La/e/a/b;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
