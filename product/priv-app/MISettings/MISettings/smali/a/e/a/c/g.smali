.class public La/e/a/c/g;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La/e/a/c/g$a;
    }
.end annotation


# static fields
.field public static a:F = 0.5f


# instance fields
.field public A:I

.field Aa:I

.field public B:I

.field Ba:I

.field public C:F

.field Ca:Z

.field public D:I

.field Da:Z

.field public E:I

.field Ea:Z

.field public F:F

.field Fa:Z

.field public G:Z

.field Ga:Z

.field public H:Z

.field Ha:Z

.field I:I

.field Ia:Z

.field J:F

.field Ja:I

.field private K:[I

.field Ka:I

.field private L:F

.field La:Z

.field private M:Z

.field Ma:Z

.field private N:Z

.field public Na:[F

.field private O:Z

.field protected Oa:[La/e/a/c/g;

.field private P:I

.field protected Pa:[La/e/a/c/g;

.field private Q:I

.field Qa:La/e/a/c/g;

.field public R:La/e/a/c/e;

.field Ra:La/e/a/c/g;

.field public S:La/e/a/c/e;

.field public Sa:I

.field public T:La/e/a/c/e;

.field public Ta:I

.field public U:La/e/a/c/e;

.field public V:La/e/a/c/e;

.field W:La/e/a/c/e;

.field X:La/e/a/c/e;

.field public Y:La/e/a/c/e;

.field public Z:[La/e/a/c/e;

.field protected aa:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "La/e/a/c/e;",
            ">;"
        }
    .end annotation
.end field

.field public b:Z

.field private ba:[Z

.field public c:[La/e/a/c/a/s;

.field public ca:[La/e/a/c/g$a;

.field public d:La/e/a/c/a/c;

.field public da:La/e/a/c/g;

.field public e:La/e/a/c/a/c;

.field ea:I

.field public f:La/e/a/c/a/m;

.field fa:I

.field public g:La/e/a/c/a/p;

.field public ga:F

.field public h:[Z

.field protected ha:I

.field i:Z

.field protected ia:I

.field private j:Z

.field protected ja:I

.field private k:Z

.field ka:I

.field private l:Z

.field la:I

.field private m:I

.field protected ma:I

.field private n:I

.field protected na:I

.field public o:La/e/a/b/a;

.field oa:I

.field public p:Ljava/lang/String;

.field protected pa:I

.field private q:Z

.field protected qa:I

.field private r:Z

.field ra:F

.field private s:Z

.field sa:F

.field private t:Z

.field private ta:Ljava/lang/Object;

.field public u:I

.field private ua:I

.field public v:I

.field private va:I

.field private w:I

.field private wa:Ljava/lang/String;

.field public x:I

.field private xa:Ljava/lang/String;

.field public y:I

.field ya:I

.field public z:[I

.field za:I


# direct methods
.method public constructor <init>()V
    .locals 9

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, La/e/a/c/g;->b:Z

    const/4 v1, 0x2

    new-array v2, v1, [La/e/a/c/a/s;

    iput-object v2, p0, La/e/a/c/g;->c:[La/e/a/c/a/s;

    const/4 v2, 0x0

    iput-object v2, p0, La/e/a/c/g;->f:La/e/a/c/a/m;

    iput-object v2, p0, La/e/a/c/g;->g:La/e/a/c/a/p;

    new-array v3, v1, [Z

    fill-array-data v3, :array_0

    iput-object v3, p0, La/e/a/c/g;->h:[Z

    iput-boolean v0, p0, La/e/a/c/g;->i:Z

    const/4 v3, 0x1

    iput-boolean v3, p0, La/e/a/c/g;->j:Z

    iput-boolean v0, p0, La/e/a/c/g;->k:Z

    iput-boolean v3, p0, La/e/a/c/g;->l:Z

    const/4 v4, -0x1

    iput v4, p0, La/e/a/c/g;->m:I

    iput v4, p0, La/e/a/c/g;->n:I

    new-instance v5, La/e/a/b/a;

    invoke-direct {v5, p0}, La/e/a/b/a;-><init>(La/e/a/c/g;)V

    iput-object v5, p0, La/e/a/c/g;->o:La/e/a/b/a;

    iput-boolean v0, p0, La/e/a/c/g;->q:Z

    iput-boolean v0, p0, La/e/a/c/g;->r:Z

    iput-boolean v0, p0, La/e/a/c/g;->s:Z

    iput-boolean v0, p0, La/e/a/c/g;->t:Z

    iput v4, p0, La/e/a/c/g;->u:I

    iput v4, p0, La/e/a/c/g;->v:I

    iput v0, p0, La/e/a/c/g;->w:I

    iput v0, p0, La/e/a/c/g;->x:I

    iput v0, p0, La/e/a/c/g;->y:I

    new-array v5, v1, [I

    iput-object v5, p0, La/e/a/c/g;->z:[I

    iput v0, p0, La/e/a/c/g;->A:I

    iput v0, p0, La/e/a/c/g;->B:I

    const/high16 v5, 0x3f800000    # 1.0f

    iput v5, p0, La/e/a/c/g;->C:F

    iput v0, p0, La/e/a/c/g;->D:I

    iput v0, p0, La/e/a/c/g;->E:I

    iput v5, p0, La/e/a/c/g;->F:F

    iput v4, p0, La/e/a/c/g;->I:I

    iput v5, p0, La/e/a/c/g;->J:F

    new-array v5, v1, [I

    fill-array-data v5, :array_1

    iput-object v5, p0, La/e/a/c/g;->K:[I

    const/4 v5, 0x0

    iput v5, p0, La/e/a/c/g;->L:F

    iput-boolean v0, p0, La/e/a/c/g;->M:Z

    iput-boolean v0, p0, La/e/a/c/g;->O:Z

    iput v0, p0, La/e/a/c/g;->P:I

    iput v0, p0, La/e/a/c/g;->Q:I

    new-instance v6, La/e/a/c/e;

    sget-object v7, La/e/a/c/e$a;->b:La/e/a/c/e$a;

    invoke-direct {v6, p0, v7}, La/e/a/c/e;-><init>(La/e/a/c/g;La/e/a/c/e$a;)V

    iput-object v6, p0, La/e/a/c/g;->R:La/e/a/c/e;

    new-instance v6, La/e/a/c/e;

    sget-object v7, La/e/a/c/e$a;->c:La/e/a/c/e$a;

    invoke-direct {v6, p0, v7}, La/e/a/c/e;-><init>(La/e/a/c/g;La/e/a/c/e$a;)V

    iput-object v6, p0, La/e/a/c/g;->S:La/e/a/c/e;

    new-instance v6, La/e/a/c/e;

    sget-object v7, La/e/a/c/e$a;->d:La/e/a/c/e$a;

    invoke-direct {v6, p0, v7}, La/e/a/c/e;-><init>(La/e/a/c/g;La/e/a/c/e$a;)V

    iput-object v6, p0, La/e/a/c/g;->T:La/e/a/c/e;

    new-instance v6, La/e/a/c/e;

    sget-object v7, La/e/a/c/e$a;->e:La/e/a/c/e$a;

    invoke-direct {v6, p0, v7}, La/e/a/c/e;-><init>(La/e/a/c/g;La/e/a/c/e$a;)V

    iput-object v6, p0, La/e/a/c/g;->U:La/e/a/c/e;

    new-instance v6, La/e/a/c/e;

    sget-object v7, La/e/a/c/e$a;->f:La/e/a/c/e$a;

    invoke-direct {v6, p0, v7}, La/e/a/c/e;-><init>(La/e/a/c/g;La/e/a/c/e$a;)V

    iput-object v6, p0, La/e/a/c/g;->V:La/e/a/c/e;

    new-instance v6, La/e/a/c/e;

    sget-object v7, La/e/a/c/e$a;->h:La/e/a/c/e$a;

    invoke-direct {v6, p0, v7}, La/e/a/c/e;-><init>(La/e/a/c/g;La/e/a/c/e$a;)V

    iput-object v6, p0, La/e/a/c/g;->W:La/e/a/c/e;

    new-instance v6, La/e/a/c/e;

    sget-object v7, La/e/a/c/e$a;->i:La/e/a/c/e$a;

    invoke-direct {v6, p0, v7}, La/e/a/c/e;-><init>(La/e/a/c/g;La/e/a/c/e$a;)V

    iput-object v6, p0, La/e/a/c/g;->X:La/e/a/c/e;

    new-instance v6, La/e/a/c/e;

    sget-object v7, La/e/a/c/e$a;->g:La/e/a/c/e$a;

    invoke-direct {v6, p0, v7}, La/e/a/c/e;-><init>(La/e/a/c/g;La/e/a/c/e$a;)V

    iput-object v6, p0, La/e/a/c/g;->Y:La/e/a/c/e;

    const/4 v6, 0x6

    new-array v6, v6, [La/e/a/c/e;

    iget-object v7, p0, La/e/a/c/g;->R:La/e/a/c/e;

    aput-object v7, v6, v0

    iget-object v7, p0, La/e/a/c/g;->T:La/e/a/c/e;

    aput-object v7, v6, v3

    iget-object v7, p0, La/e/a/c/g;->S:La/e/a/c/e;

    aput-object v7, v6, v1

    iget-object v7, p0, La/e/a/c/g;->U:La/e/a/c/e;

    const/4 v8, 0x3

    aput-object v7, v6, v8

    iget-object v7, p0, La/e/a/c/g;->V:La/e/a/c/e;

    const/4 v8, 0x4

    aput-object v7, v6, v8

    iget-object v7, p0, La/e/a/c/g;->Y:La/e/a/c/e;

    const/4 v8, 0x5

    aput-object v7, v6, v8

    iput-object v6, p0, La/e/a/c/g;->Z:[La/e/a/c/e;

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, La/e/a/c/g;->aa:Ljava/util/ArrayList;

    new-array v6, v1, [Z

    iput-object v6, p0, La/e/a/c/g;->ba:[Z

    new-array v6, v1, [La/e/a/c/g$a;

    sget-object v7, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    aput-object v7, v6, v0

    aput-object v7, v6, v3

    iput-object v6, p0, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    iput-object v2, p0, La/e/a/c/g;->da:La/e/a/c/g;

    iput v0, p0, La/e/a/c/g;->ea:I

    iput v0, p0, La/e/a/c/g;->fa:I

    iput v5, p0, La/e/a/c/g;->ga:F

    iput v4, p0, La/e/a/c/g;->ha:I

    iput v0, p0, La/e/a/c/g;->ia:I

    iput v0, p0, La/e/a/c/g;->ja:I

    iput v0, p0, La/e/a/c/g;->ka:I

    iput v0, p0, La/e/a/c/g;->la:I

    iput v0, p0, La/e/a/c/g;->ma:I

    iput v0, p0, La/e/a/c/g;->na:I

    iput v0, p0, La/e/a/c/g;->oa:I

    sget v5, La/e/a/c/g;->a:F

    iput v5, p0, La/e/a/c/g;->ra:F

    iput v5, p0, La/e/a/c/g;->sa:F

    iput v0, p0, La/e/a/c/g;->ua:I

    iput v0, p0, La/e/a/c/g;->va:I

    iput-object v2, p0, La/e/a/c/g;->wa:Ljava/lang/String;

    iput-object v2, p0, La/e/a/c/g;->xa:Ljava/lang/String;

    iput-boolean v0, p0, La/e/a/c/g;->Ia:Z

    iput v0, p0, La/e/a/c/g;->Ja:I

    iput v0, p0, La/e/a/c/g;->Ka:I

    new-array v5, v1, [F

    fill-array-data v5, :array_2

    iput-object v5, p0, La/e/a/c/g;->Na:[F

    new-array v5, v1, [La/e/a/c/g;

    aput-object v2, v5, v0

    aput-object v2, v5, v3

    iput-object v5, p0, La/e/a/c/g;->Oa:[La/e/a/c/g;

    new-array v1, v1, [La/e/a/c/g;

    aput-object v2, v1, v0

    aput-object v2, v1, v3

    iput-object v1, p0, La/e/a/c/g;->Pa:[La/e/a/c/g;

    iput-object v2, p0, La/e/a/c/g;->Qa:La/e/a/c/g;

    iput-object v2, p0, La/e/a/c/g;->Ra:La/e/a/c/g;

    iput v4, p0, La/e/a/c/g;->Sa:I

    iput v4, p0, La/e/a/c/g;->Ta:I

    invoke-direct {p0}, La/e/a/c/g;->Y()V

    return-void

    :array_0
    .array-data 1
        0x1t
        0x1t
    .end array-data

    nop

    :array_1
    .array-data 4
        0x7fffffff
        0x7fffffff
    .end array-data

    :array_2
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
    .end array-data
.end method

.method private Y()V
    .locals 2

    iget-object v0, p0, La/e/a/c/g;->aa:Ljava/util/ArrayList;

    iget-object v1, p0, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, La/e/a/c/g;->aa:Ljava/util/ArrayList;

    iget-object v1, p0, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, La/e/a/c/g;->aa:Ljava/util/ArrayList;

    iget-object v1, p0, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, La/e/a/c/g;->aa:Ljava/util/ArrayList;

    iget-object v1, p0, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, La/e/a/c/g;->aa:Ljava/util/ArrayList;

    iget-object v1, p0, La/e/a/c/g;->W:La/e/a/c/e;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, La/e/a/c/g;->aa:Ljava/util/ArrayList;

    iget-object v1, p0, La/e/a/c/g;->X:La/e/a/c/e;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, La/e/a/c/g;->aa:Ljava/util/ArrayList;

    iget-object v1, p0, La/e/a/c/g;->Y:La/e/a/c/e;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, La/e/a/c/g;->aa:Ljava/util/ArrayList;

    iget-object v1, p0, La/e/a/c/g;->V:La/e/a/c/e;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private a(La/e/a/d;ZZZZLa/e/a/j;La/e/a/j;La/e/a/c/g$a;ZLa/e/a/c/e;La/e/a/c/e;IIIIFZZZZZIIIIFZ)V
    .locals 35

    move-object/from16 v0, p0

    move-object/from16 v10, p1

    move-object/from16 v11, p6

    move-object/from16 v12, p7

    move-object/from16 v13, p10

    move-object/from16 v14, p11

    move/from16 v15, p14

    move/from16 v1, p15

    move/from16 v2, p23

    move/from16 v3, p24

    move/from16 v4, p25

    invoke-virtual {v10, v13}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v9

    invoke-virtual {v10, v14}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v8

    invoke-virtual/range {p10 .. p10}, La/e/a/c/e;->g()La/e/a/c/e;

    move-result-object v5

    invoke-virtual {v10, v5}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v7

    invoke-virtual/range {p11 .. p11}, La/e/a/c/e;->g()La/e/a/c/e;

    move-result-object v5

    invoke-virtual {v10, v5}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v6

    invoke-static {}, La/e/a/d;->e()La/e/a/e;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-static {}, La/e/a/d;->e()La/e/a/e;

    move-result-object v5

    iget-wide v11, v5, La/e/a/e;->w:J

    const-wide/16 v16, 0x1

    add-long v11, v11, v16

    iput-wide v11, v5, La/e/a/e;->w:J

    :cond_0
    invoke-virtual/range {p10 .. p10}, La/e/a/c/e;->l()Z

    move-result v11

    invoke-virtual/range {p11 .. p11}, La/e/a/c/e;->l()Z

    move-result v12

    iget-object v5, v0, La/e/a/c/g;->Y:La/e/a/c/e;

    invoke-virtual {v5}, La/e/a/c/e;->l()Z

    move-result v16

    if-eqz v11, :cond_1

    const/16 v18, 0x1

    goto :goto_0

    :cond_1
    const/16 v18, 0x0

    :goto_0
    if-eqz v12, :cond_2

    add-int/lit8 v18, v18, 0x1

    :cond_2
    if-eqz v16, :cond_3

    add-int/lit8 v18, v18, 0x1

    :cond_3
    move/from16 v19, v18

    if-eqz p17, :cond_4

    const/16 v20, 0x3

    goto :goto_1

    :cond_4
    move/from16 v20, p22

    :goto_1
    sget-object v21, La/e/a/c/f;->b:[I

    invoke-virtual/range {p8 .. p8}, Ljava/lang/Enum;->ordinal()I

    move-result v22

    aget v5, v21, v22

    const/4 v2, 0x2

    const/4 v14, 0x1

    if-eq v5, v14, :cond_5

    if-eq v5, v2, :cond_5

    const/4 v14, 0x3

    if-eq v5, v14, :cond_5

    const/4 v14, 0x4

    if-eq v5, v14, :cond_7

    :cond_5
    move/from16 v5, v20

    :cond_6
    const/16 v20, 0x0

    goto :goto_2

    :cond_7
    move/from16 v5, v20

    if-eq v5, v14, :cond_6

    const/16 v20, 0x1

    :goto_2
    iget v14, v0, La/e/a/c/g;->m:I

    const/4 v2, -0x1

    if-eq v14, v2, :cond_8

    if-eqz p2, :cond_8

    iput v2, v0, La/e/a/c/g;->m:I

    move-object/from16 v21, v6

    const/16 v20, 0x0

    goto :goto_3

    :cond_8
    move/from16 v14, p13

    move-object/from16 v21, v6

    :goto_3
    iget v6, v0, La/e/a/c/g;->n:I

    if-eq v6, v2, :cond_9

    if-nez p2, :cond_9

    iput v2, v0, La/e/a/c/g;->n:I

    const/16 v20, 0x0

    goto :goto_4

    :cond_9
    move v6, v14

    :goto_4
    iget v14, v0, La/e/a/c/g;->va:I

    const/16 v2, 0x8

    if-ne v14, v2, :cond_a

    const/4 v6, 0x0

    const/16 v20, 0x0

    :cond_a
    if-eqz p27, :cond_c

    if-nez v11, :cond_b

    if-nez v12, :cond_b

    if-nez v16, :cond_b

    move/from16 v14, p12

    invoke-virtual {v10, v9, v14}, La/e/a/d;->a(La/e/a/j;I)V

    goto :goto_5

    :cond_b
    if-eqz v11, :cond_c

    if-nez v12, :cond_c

    invoke-virtual/range {p10 .. p10}, La/e/a/c/e;->c()I

    move-result v14

    invoke-virtual {v10, v9, v7, v14, v2}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    :cond_c
    :goto_5
    if-nez v20, :cond_10

    if-eqz p9, :cond_e

    const/4 v2, 0x0

    const/4 v14, 0x3

    invoke-virtual {v10, v8, v9, v2, v14}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    if-lez v15, :cond_d

    const/16 v6, 0x8

    invoke-virtual {v10, v8, v9, v15, v6}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    goto :goto_6

    :cond_d
    const/16 v6, 0x8

    :goto_6
    const v2, 0x7fffffff

    if-ge v1, v2, :cond_f

    invoke-virtual {v10, v8, v9, v1, v6}, La/e/a/d;->c(La/e/a/j;La/e/a/j;II)V

    goto :goto_7

    :cond_e
    move v1, v2

    const/4 v14, 0x3

    invoke-virtual {v10, v8, v9, v6, v1}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    :cond_f
    :goto_7
    move v15, v5

    move-object/from16 v24, v7

    move-object v14, v8

    move/from16 v17, v19

    move-object/from16 v1, v21

    move/from16 v19, p5

    :goto_8
    move/from16 v21, v3

    goto/16 :goto_11

    :cond_10
    move/from16 v1, v19

    const/4 v2, 0x2

    const/4 v14, 0x3

    if-eq v1, v2, :cond_13

    if-nez p17, :cond_13

    const/4 v2, 0x1

    if-eq v5, v2, :cond_11

    if-nez v5, :cond_13

    :cond_11
    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    move-result v2

    if-lez v4, :cond_12

    invoke-static {v4, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    :cond_12
    const/16 v6, 0x8

    invoke-virtual {v10, v8, v9, v2, v6}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    move/from16 v19, p5

    move/from16 v17, v1

    move v15, v5

    move-object/from16 v24, v7

    move-object v14, v8

    move-object/from16 v1, v21

    const/16 v20, 0x0

    goto :goto_8

    :cond_13
    const/4 v2, -0x2

    if-ne v3, v2, :cond_14

    move v3, v6

    :cond_14
    if-ne v4, v2, :cond_15

    move v2, v6

    goto :goto_9

    :cond_15
    move v2, v4

    :goto_9
    if-lez v6, :cond_16

    const/4 v4, 0x1

    if-eq v5, v4, :cond_16

    const/4 v6, 0x0

    :cond_16
    if-lez v3, :cond_17

    const/16 v4, 0x8

    invoke-virtual {v10, v8, v9, v3, v4}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    invoke-static {v6, v3}, Ljava/lang/Math;->max(II)I

    move-result v6

    :cond_17
    if-lez v2, :cond_1a

    if-eqz p3, :cond_18

    const/4 v4, 0x1

    if-ne v5, v4, :cond_18

    const/4 v4, 0x0

    goto :goto_a

    :cond_18
    const/4 v4, 0x1

    :goto_a
    if-eqz v4, :cond_19

    const/16 v4, 0x8

    invoke-virtual {v10, v8, v9, v2, v4}, La/e/a/d;->c(La/e/a/j;La/e/a/j;II)V

    goto :goto_b

    :cond_19
    const/16 v4, 0x8

    :goto_b
    invoke-static {v6, v2}, Ljava/lang/Math;->min(II)I

    move-result v6

    goto :goto_c

    :cond_1a
    const/16 v4, 0x8

    :goto_c
    const/4 v14, 0x1

    if-ne v5, v14, :cond_1d

    if-eqz p3, :cond_1b

    invoke-virtual {v10, v8, v9, v6, v4}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    goto :goto_d

    :cond_1b
    if-eqz p19, :cond_1c

    const/4 v14, 0x5

    invoke-virtual {v10, v8, v9, v6, v14}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    invoke-virtual {v10, v8, v9, v6, v4}, La/e/a/d;->c(La/e/a/j;La/e/a/j;II)V

    goto :goto_d

    :cond_1c
    const/4 v14, 0x5

    invoke-virtual {v10, v8, v9, v6, v14}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    invoke-virtual {v10, v8, v9, v6, v4}, La/e/a/d;->c(La/e/a/j;La/e/a/j;II)V

    :goto_d
    move/from16 v19, p5

    move/from16 v17, v1

    move v4, v2

    move v15, v5

    move-object/from16 v24, v7

    move-object v14, v8

    move-object/from16 v1, v21

    goto/16 :goto_8

    :cond_1d
    const/4 v4, 0x2

    if-ne v5, v4, :cond_21

    invoke-virtual/range {p10 .. p10}, La/e/a/c/e;->h()La/e/a/c/e$a;

    move-result-object v4

    sget-object v6, La/e/a/c/e$a;->c:La/e/a/c/e$a;

    if-eq v4, v6, :cond_1f

    invoke-virtual/range {p10 .. p10}, La/e/a/c/e;->h()La/e/a/c/e$a;

    move-result-object v4

    sget-object v6, La/e/a/c/e$a;->e:La/e/a/c/e$a;

    if-ne v4, v6, :cond_1e

    goto :goto_e

    :cond_1e
    iget-object v4, v0, La/e/a/c/g;->da:La/e/a/c/g;

    sget-object v6, La/e/a/c/e$a;->b:La/e/a/c/e$a;

    invoke-virtual {v4, v6}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v4

    invoke-virtual {v10, v4}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v4

    iget-object v6, v0, La/e/a/c/g;->da:La/e/a/c/g;

    sget-object v14, La/e/a/c/e$a;->d:La/e/a/c/e$a;

    invoke-virtual {v6, v14}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v6

    invoke-virtual {v10, v6}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v6

    goto :goto_f

    :cond_1f
    :goto_e
    iget-object v4, v0, La/e/a/c/g;->da:La/e/a/c/g;

    sget-object v6, La/e/a/c/e$a;->c:La/e/a/c/e$a;

    invoke-virtual {v4, v6}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v4

    invoke-virtual {v10, v4}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v4

    iget-object v6, v0, La/e/a/c/g;->da:La/e/a/c/g;

    sget-object v14, La/e/a/c/e$a;->e:La/e/a/c/e$a;

    invoke-virtual {v6, v14}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v6

    invoke-virtual {v10, v6}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v6

    :goto_f
    move-object v14, v4

    invoke-virtual/range {p1 .. p1}, La/e/a/d;->b()La/e/a/b;

    move-result-object v4

    move/from16 v19, v3

    move-object v3, v4

    move/from16 p9, v2

    move-object v2, v4

    move-object v4, v8

    move/from16 v17, v1

    move v15, v5

    const/4 v1, 0x0

    move-object v5, v9

    move-object/from16 v1, v21

    move-object/from16 v24, v7

    move-object v7, v14

    move-object v14, v8

    move/from16 v8, p26

    invoke-virtual/range {v3 .. v8}, La/e/a/b;->a(La/e/a/j;La/e/a/j;La/e/a/j;La/e/a/j;F)La/e/a/b;

    invoke-virtual {v10, v2}, La/e/a/d;->a(La/e/a/b;)V

    if-eqz p3, :cond_20

    const/4 v5, 0x0

    goto :goto_10

    :cond_20
    move/from16 v5, v20

    :goto_10
    move/from16 v4, p9

    move/from16 v20, v5

    move/from16 v21, v19

    move/from16 v19, p5

    goto :goto_11

    :cond_21
    move/from16 v17, v1

    move/from16 p9, v2

    move/from16 v19, v3

    move v15, v5

    move-object/from16 v24, v7

    move-object v14, v8

    move-object/from16 v1, v21

    move/from16 v4, p9

    move/from16 v21, v19

    const/16 v19, 0x1

    :goto_11
    if-eqz p27, :cond_62

    if-eqz p19, :cond_22

    move-object/from16 v4, p6

    move-object/from16 v1, p7

    move-object v7, v9

    move/from16 v2, v17

    const/4 v3, 0x2

    const/16 v13, 0x8

    const/16 v23, 0x1

    goto/16 :goto_33

    :cond_22
    if-nez v11, :cond_23

    if-nez v12, :cond_23

    if-nez v16, :cond_23

    goto/16 :goto_2e

    :cond_23
    if-eqz v11, :cond_25

    if-nez v12, :cond_25

    iget-object v2, v13, La/e/a/c/e;->f:La/e/a/c/e;

    iget-object v2, v2, La/e/a/c/e;->d:La/e/a/c/g;

    if-eqz p3, :cond_24

    instance-of v2, v2, La/e/a/c/a;

    if-eqz v2, :cond_24

    const/16 v2, 0x8

    goto :goto_12

    :cond_24
    const/4 v2, 0x5

    :goto_12
    move/from16 v25, p3

    move-object/from16 v12, p11

    move v3, v2

    move-object v2, v1

    goto/16 :goto_30

    :cond_25
    if-nez v11, :cond_28

    if-eqz v12, :cond_28

    invoke-virtual/range {p11 .. p11}, La/e/a/c/e;->c()I

    move-result v2

    neg-int v2, v2

    const/16 v3, 0x8

    invoke-virtual {v10, v14, v1, v2, v3}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    if-eqz p3, :cond_5d

    iget-boolean v2, v0, La/e/a/c/g;->k:Z

    if-eqz v2, :cond_27

    iget-boolean v2, v9, La/e/a/j;->h:Z

    if-eqz v2, :cond_27

    iget-object v2, v0, La/e/a/c/g;->da:La/e/a/c/g;

    if-eqz v2, :cond_27

    check-cast v2, La/e/a/c/h;

    if-eqz p2, :cond_26

    invoke-virtual {v2, v13}, La/e/a/c/h;->b(La/e/a/c/e;)V

    goto/16 :goto_2e

    :cond_26
    invoke-virtual {v2, v13}, La/e/a/c/h;->d(La/e/a/c/e;)V

    goto/16 :goto_2e

    :cond_27
    move-object/from16 v8, p6

    const/4 v2, 0x5

    const/4 v3, 0x0

    invoke-virtual {v10, v9, v8, v3, v2}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    move-object/from16 v12, p11

    move v3, v2

    move-object v2, v1

    goto/16 :goto_2f

    :cond_28
    move-object/from16 v8, p6

    const/4 v3, 0x0

    if-eqz v11, :cond_5d

    if-eqz v12, :cond_5d

    iget-object v2, v13, La/e/a/c/e;->f:La/e/a/c/e;

    iget-object v11, v2, La/e/a/c/e;->d:La/e/a/c/g;

    move-object/from16 v12, p11

    const/16 v16, 0x4

    iget-object v2, v12, La/e/a/c/e;->f:La/e/a/c/e;

    iget-object v7, v2, La/e/a/c/e;->d:La/e/a/c/g;

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->v()La/e/a/c/g;

    move-result-object v6

    const/16 v17, 0x6

    if-eqz v20, :cond_3e

    if-nez v15, :cond_2d

    if-nez v4, :cond_2a

    if-nez v21, :cond_2a

    move-object/from16 v5, v24

    iget-boolean v2, v5, La/e/a/j;->h:Z

    if-eqz v2, :cond_29

    iget-boolean v2, v1, La/e/a/j;->h:Z

    if-eqz v2, :cond_29

    invoke-virtual/range {p10 .. p10}, La/e/a/c/e;->c()I

    move-result v2

    const/16 v4, 0x8

    invoke-virtual {v10, v9, v5, v2, v4}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    invoke-virtual/range {p11 .. p11}, La/e/a/c/e;->c()I

    move-result v2

    neg-int v2, v2

    invoke-virtual {v10, v14, v1, v2, v4}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    return-void

    :cond_29
    const/16 v4, 0x8

    move v2, v3

    move/from16 v24, v2

    move/from16 v25, v4

    move/from16 v26, v25

    const/16 v22, 0x1

    goto :goto_13

    :cond_2a
    move-object/from16 v5, v24

    const/16 v4, 0x8

    move/from16 v22, v3

    const/4 v2, 0x1

    const/16 v24, 0x1

    const/16 v25, 0x5

    const/16 v26, 0x5

    :goto_13
    instance-of v3, v11, La/e/a/c/a;

    if-nez v3, :cond_2c

    instance-of v3, v7, La/e/a/c/a;

    if-eqz v3, :cond_2b

    goto :goto_14

    :cond_2b
    move-object/from16 v4, p7

    goto :goto_15

    :cond_2c
    :goto_14
    move-object/from16 v4, p7

    move/from16 v26, v16

    :goto_15
    move/from16 v18, v24

    const/4 v3, 0x1

    move/from16 v24, v22

    :goto_16
    move/from16 v22, v17

    goto/16 :goto_22

    :cond_2d
    move-object/from16 v5, v24

    const/4 v2, 0x2

    const/16 v3, 0x8

    if-ne v15, v2, :cond_30

    instance-of v2, v11, La/e/a/c/a;

    if-nez v2, :cond_2f

    instance-of v2, v7, La/e/a/c/a;

    if-eqz v2, :cond_2e

    goto :goto_19

    :cond_2e
    move-object/from16 v4, p7

    move/from16 v22, v17

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/16 v18, 0x1

    const/16 v24, 0x0

    :goto_17
    const/16 v25, 0x5

    :goto_18
    const/16 v26, 0x5

    goto/16 :goto_22

    :cond_2f
    :goto_19
    move-object/from16 v4, p7

    move/from16 v26, v16

    move/from16 v22, v17

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/16 v18, 0x1

    goto/16 :goto_20

    :cond_30
    const/4 v2, 0x1

    if-ne v15, v2, :cond_31

    move-object/from16 v4, p7

    move/from16 v25, v3

    move/from16 v26, v16

    move/from16 v22, v17

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/16 v18, 0x1

    const/16 v24, 0x0

    goto/16 :goto_22

    :cond_31
    const/4 v2, 0x3

    if-ne v15, v2, :cond_3d

    iget v2, v0, La/e/a/c/g;->I:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_34

    if-eqz p20, :cond_33

    if-eqz p3, :cond_32

    move-object/from16 v4, p7

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/16 v18, 0x1

    const/16 v22, 0x5

    goto :goto_1a

    :cond_32
    move-object/from16 v4, p7

    move/from16 v22, v16

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/16 v18, 0x1

    goto :goto_1a

    :cond_33
    move-object/from16 v4, p7

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/16 v18, 0x1

    const/16 v22, 0x8

    :goto_1a
    const/16 v24, 0x1

    const/16 v25, 0x8

    goto :goto_18

    :cond_34
    if-eqz p17, :cond_38

    move/from16 v2, p23

    const/4 v3, 0x2

    if-eq v2, v3, :cond_36

    const/4 v3, 0x1

    if-ne v2, v3, :cond_35

    goto :goto_1b

    :cond_35
    const/4 v2, 0x0

    goto :goto_1c

    :cond_36
    const/4 v3, 0x1

    :goto_1b
    move v2, v3

    :goto_1c
    if-nez v2, :cond_37

    const/16 v2, 0x8

    const/4 v4, 0x5

    goto :goto_1d

    :cond_37
    move/from16 v4, v16

    const/4 v2, 0x5

    :goto_1d
    move/from16 v25, v2

    move v2, v3

    move/from16 v18, v2

    move/from16 v24, v18

    move/from16 v26, v4

    move/from16 v22, v17

    move-object/from16 v4, p7

    goto/16 :goto_22

    :cond_38
    const/4 v3, 0x1

    if-lez v4, :cond_39

    move-object/from16 v4, p7

    move v2, v3

    move/from16 v18, v2

    move/from16 v24, v18

    move/from16 v22, v17

    goto/16 :goto_17

    :cond_39
    if-nez v4, :cond_3c

    if-nez v21, :cond_3c

    if-nez p20, :cond_3a

    move-object/from16 v4, p7

    move v2, v3

    move/from16 v18, v2

    move/from16 v24, v18

    move/from16 v22, v17

    const/16 v25, 0x5

    const/16 v26, 0x8

    goto/16 :goto_22

    :cond_3a
    if-eq v11, v6, :cond_3b

    if-eq v7, v6, :cond_3b

    move/from16 v2, v16

    goto :goto_1e

    :cond_3b
    const/4 v2, 0x5

    :goto_1e
    move-object/from16 v4, p7

    move/from16 v25, v2

    move v2, v3

    move/from16 v18, v2

    move/from16 v24, v18

    move/from16 v26, v16

    goto/16 :goto_16

    :cond_3c
    move-object/from16 v4, p7

    move v2, v3

    move/from16 v18, v2

    move/from16 v24, v18

    move/from16 v26, v16

    move/from16 v22, v17

    goto :goto_21

    :cond_3d
    const/4 v3, 0x1

    move-object/from16 v4, p7

    move/from16 v26, v16

    move/from16 v22, v17

    const/4 v2, 0x0

    const/16 v18, 0x0

    goto :goto_20

    :cond_3e
    move-object/from16 v5, v24

    const/4 v3, 0x1

    iget-boolean v2, v5, La/e/a/j;->h:Z

    if-eqz v2, :cond_41

    iget-boolean v2, v1, La/e/a/j;->h:Z

    if-eqz v2, :cond_41

    invoke-virtual/range {p10 .. p10}, La/e/a/c/e;->c()I

    move-result v2

    invoke-virtual/range {p11 .. p11}, La/e/a/c/e;->c()I

    move-result v3

    const/16 v4, 0x8

    move-object/from16 p17, p1

    move-object/from16 p18, v9

    move-object/from16 p19, v5

    move/from16 p20, v2

    move/from16 p21, p16

    move-object/from16 p22, v1

    move-object/from16 p23, v14

    move/from16 p24, v3

    move/from16 p25, v4

    invoke-virtual/range {p17 .. p25}, La/e/a/d;->a(La/e/a/j;La/e/a/j;IFLa/e/a/j;La/e/a/j;II)V

    if-eqz p3, :cond_40

    if-eqz v19, :cond_40

    iget-object v2, v12, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v2, :cond_3f

    invoke-virtual/range {p11 .. p11}, La/e/a/c/e;->c()I

    move-result v5

    move-object/from16 v4, p7

    goto :goto_1f

    :cond_3f
    move-object/from16 v4, p7

    const/4 v5, 0x0

    :goto_1f
    if-eq v1, v4, :cond_40

    const/4 v1, 0x5

    invoke-virtual {v10, v4, v14, v5, v1}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    :cond_40
    return-void

    :cond_41
    move-object/from16 v4, p7

    move v2, v3

    move/from16 v18, v2

    move/from16 v26, v16

    move/from16 v22, v17

    :goto_20
    const/16 v24, 0x0

    :goto_21
    const/16 v25, 0x5

    :goto_22
    if-eqz v18, :cond_42

    if-ne v5, v1, :cond_42

    if-eq v11, v6, :cond_42

    const/16 v18, 0x0

    const/16 v27, 0x0

    goto :goto_23

    :cond_42
    move/from16 v27, v3

    :goto_23
    if-eqz v2, :cond_44

    if-nez v20, :cond_43

    if-nez p18, :cond_43

    if-nez p20, :cond_43

    if-ne v5, v8, :cond_43

    if-ne v1, v4, :cond_43

    const/16 v22, 0x8

    const/16 v25, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x8

    goto :goto_24

    :cond_43
    move/from16 v28, v25

    move/from16 v25, p3

    :goto_24
    invoke-virtual/range {p10 .. p10}, La/e/a/c/e;->c()I

    move-result v29

    invoke-virtual/range {p11 .. p11}, La/e/a/c/e;->c()I

    move-result v30

    move/from16 v23, v3

    const/4 v2, 0x3

    const/4 v13, 0x0

    move-object v3, v1

    move-object/from16 v1, p1

    const/16 v13, 0x8

    move-object v2, v9

    move-object/from16 p8, v3

    move-object v3, v5

    move/from16 v4, v29

    move-object/from16 v31, v5

    move/from16 v5, p16

    move-object/from16 v32, v6

    move-object/from16 v6, p8

    move-object/from16 v33, v7

    move-object v7, v14

    move/from16 v8, v30

    move-object/from16 v34, v9

    move/from16 v9, v22

    invoke-virtual/range {v1 .. v9}, La/e/a/d;->a(La/e/a/j;La/e/a/j;IFLa/e/a/j;La/e/a/j;II)V

    move/from16 v5, v27

    goto :goto_25

    :cond_44
    move-object/from16 p8, v1

    move/from16 v23, v3

    move-object/from16 v31, v5

    move-object/from16 v32, v6

    move-object/from16 v33, v7

    move-object/from16 v34, v9

    const/16 v13, 0x8

    move/from16 v28, v25

    move/from16 v5, v27

    move/from16 v25, p3

    :goto_25
    iget v1, v0, La/e/a/c/g;->va:I

    if-ne v1, v13, :cond_45

    invoke-virtual/range {p11 .. p11}, La/e/a/c/e;->j()Z

    move-result v1

    if-nez v1, :cond_45

    return-void

    :cond_45
    if-eqz v18, :cond_4a

    if-eqz v25, :cond_47

    move-object/from16 v2, p8

    move-object/from16 v1, v31

    if-eq v1, v2, :cond_48

    if-nez v20, :cond_48

    instance-of v3, v11, La/e/a/c/a;

    if-nez v3, :cond_46

    move-object/from16 v3, v33

    instance-of v4, v3, La/e/a/c/a;

    if-eqz v4, :cond_49

    goto :goto_26

    :cond_46
    move-object/from16 v3, v33

    :goto_26
    move/from16 v4, v17

    goto :goto_27

    :cond_47
    move-object/from16 v2, p8

    move-object/from16 v1, v31

    :cond_48
    move-object/from16 v3, v33

    :cond_49
    move/from16 v4, v28

    :goto_27
    invoke-virtual/range {p10 .. p10}, La/e/a/c/e;->c()I

    move-result v6

    move-object/from16 v7, v34

    invoke-virtual {v10, v7, v1, v6, v4}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    invoke-virtual/range {p11 .. p11}, La/e/a/c/e;->c()I

    move-result v6

    neg-int v6, v6

    invoke-virtual {v10, v14, v2, v6, v4}, La/e/a/d;->c(La/e/a/j;La/e/a/j;II)V

    goto :goto_28

    :cond_4a
    move-object/from16 v2, p8

    move-object/from16 v1, v31

    move-object/from16 v3, v33

    move-object/from16 v7, v34

    move/from16 v4, v28

    :goto_28
    if-eqz v25, :cond_4b

    if-eqz p21, :cond_4b

    instance-of v6, v11, La/e/a/c/a;

    if-nez v6, :cond_4b

    instance-of v6, v3, La/e/a/c/a;

    if-nez v6, :cond_4b

    move-object/from16 v6, v32

    if-eq v3, v6, :cond_4c

    move/from16 v4, v17

    move v8, v4

    move/from16 v5, v23

    goto :goto_29

    :cond_4b
    move-object/from16 v6, v32

    :cond_4c
    move v8, v4

    move/from16 v4, v26

    :goto_29
    if-eqz v5, :cond_58

    if-eqz v24, :cond_55

    if-eqz p20, :cond_4d

    if-eqz p4, :cond_55

    :cond_4d
    if-eq v11, v6, :cond_4f

    if-ne v3, v6, :cond_4e

    goto :goto_2a

    :cond_4e
    move/from16 v17, v4

    :cond_4f
    :goto_2a
    instance-of v5, v11, La/e/a/c/k;

    if-nez v5, :cond_50

    instance-of v5, v3, La/e/a/c/k;

    if-eqz v5, :cond_51

    :cond_50
    const/16 v17, 0x5

    :cond_51
    instance-of v5, v11, La/e/a/c/a;

    if-nez v5, :cond_52

    instance-of v5, v3, La/e/a/c/a;

    if-eqz v5, :cond_53

    :cond_52
    const/16 v17, 0x5

    :cond_53
    if-eqz p20, :cond_54

    const/4 v5, 0x5

    goto :goto_2b

    :cond_54
    move/from16 v5, v17

    :goto_2b
    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    :cond_55
    if-eqz v25, :cond_57

    invoke-static {v8, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    if-eqz p17, :cond_57

    if-nez p20, :cond_57

    if-eq v11, v6, :cond_56

    if-ne v3, v6, :cond_57

    :cond_56
    move/from16 v4, v16

    :cond_57
    invoke-virtual/range {p10 .. p10}, La/e/a/c/e;->c()I

    move-result v3

    invoke-virtual {v10, v7, v1, v3, v4}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    invoke-virtual/range {p11 .. p11}, La/e/a/c/e;->c()I

    move-result v3

    neg-int v3, v3

    invoke-virtual {v10, v14, v2, v3, v4}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    :cond_58
    if-eqz v25, :cond_5a

    move-object/from16 v4, p6

    if-ne v4, v1, :cond_59

    invoke-virtual/range {p10 .. p10}, La/e/a/c/e;->c()I

    move-result v5

    goto :goto_2c

    :cond_59
    const/4 v5, 0x0

    :goto_2c
    if-eq v1, v4, :cond_5a

    const/4 v1, 0x5

    invoke-virtual {v10, v7, v4, v5, v1}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    :cond_5a
    if-eqz v25, :cond_5c

    if-eqz v20, :cond_5c

    move v5, v15

    if-nez p14, :cond_5c

    if-nez v21, :cond_5c

    if-eqz v20, :cond_5b

    const/4 v1, 0x3

    if-ne v5, v1, :cond_5b

    const/4 v1, 0x0

    invoke-virtual {v10, v14, v7, v1, v13}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    goto :goto_2d

    :cond_5b
    const/4 v1, 0x0

    const/4 v3, 0x5

    invoke-virtual {v10, v14, v7, v1, v3}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    goto :goto_30

    :cond_5c
    :goto_2d
    const/4 v3, 0x5

    goto :goto_30

    :cond_5d
    :goto_2e
    move-object/from16 v12, p11

    move-object v2, v1

    const/4 v3, 0x5

    :goto_2f
    move/from16 v25, p3

    :goto_30
    if-eqz v25, :cond_61

    if-eqz v19, :cond_61

    iget-object v1, v12, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v1, :cond_5e

    invoke-virtual/range {p11 .. p11}, La/e/a/c/e;->c()I

    move-result v5

    move-object/from16 v1, p7

    goto :goto_31

    :cond_5e
    move-object/from16 v1, p7

    const/4 v5, 0x0

    :goto_31
    if-eq v2, v1, :cond_61

    iget-boolean v2, v0, La/e/a/c/g;->k:Z

    if-eqz v2, :cond_60

    iget-boolean v2, v14, La/e/a/j;->h:Z

    if-eqz v2, :cond_60

    iget-object v2, v0, La/e/a/c/g;->da:La/e/a/c/g;

    if-eqz v2, :cond_60

    check-cast v2, La/e/a/c/h;

    if-eqz p2, :cond_5f

    invoke-virtual {v2, v12}, La/e/a/c/h;->a(La/e/a/c/e;)V

    goto :goto_32

    :cond_5f
    invoke-virtual {v2, v12}, La/e/a/c/h;->c(La/e/a/c/e;)V

    :goto_32
    return-void

    :cond_60
    invoke-virtual {v10, v1, v14, v5, v3}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    :cond_61
    return-void

    :cond_62
    move-object/from16 v4, p6

    move-object/from16 v1, p7

    move-object v7, v9

    const/4 v3, 0x2

    const/16 v13, 0x8

    const/16 v23, 0x1

    move/from16 v2, v17

    :goto_33
    if-ge v2, v3, :cond_67

    if-eqz p3, :cond_67

    if-eqz v19, :cond_67

    const/4 v2, 0x0

    invoke-virtual {v10, v7, v4, v2, v13}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    if-nez p2, :cond_64

    iget-object v2, v0, La/e/a/c/g;->V:La/e/a/c/e;

    iget-object v2, v2, La/e/a/c/e;->f:La/e/a/c/e;

    if-nez v2, :cond_63

    goto :goto_34

    :cond_63
    const/4 v5, 0x0

    goto :goto_35

    :cond_64
    :goto_34
    move/from16 v5, v23

    :goto_35
    if-nez p2, :cond_66

    iget-object v2, v0, La/e/a/c/g;->V:La/e/a/c/e;

    iget-object v2, v2, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v2, :cond_66

    iget-object v2, v2, La/e/a/c/e;->d:La/e/a/c/g;

    iget v3, v2, La/e/a/c/g;->ga:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_65

    iget-object v2, v2, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    const/4 v3, 0x0

    aget-object v4, v2, v3

    sget-object v3, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v4, v3, :cond_65

    aget-object v2, v2, v23

    if-ne v2, v3, :cond_65

    move/from16 v5, v23

    goto :goto_36

    :cond_65
    const/4 v5, 0x0

    :cond_66
    :goto_36
    if-eqz v5, :cond_67

    const/4 v2, 0x0

    invoke-virtual {v10, v1, v14, v2, v13}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    :cond_67
    return-void
.end method

.method private a(Ljava/lang/StringBuilder;Ljava/lang/String;FF)V
    .locals 0

    cmpl-float p4, p3, p4

    if-nez p4, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " :   "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string p2, ",\n"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private a(Ljava/lang/StringBuilder;Ljava/lang/String;FI)V
    .locals 1

    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " :  ["

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string p2, ","

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, ""

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "],\n"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private a(Ljava/lang/StringBuilder;Ljava/lang/String;II)V
    .locals 0

    if-ne p3, p4, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " :   "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, ",\n"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private a(Ljava/lang/StringBuilder;Ljava/lang/String;IIIIIIFF)V
    .locals 0

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " :  {\n"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 p2, 0x0

    const-string p6, "      size"

    invoke-direct {p0, p1, p6, p3, p2}, La/e/a/c/g;->a(Ljava/lang/StringBuilder;Ljava/lang/String;II)V

    const-string p3, "      min"

    invoke-direct {p0, p1, p3, p4, p2}, La/e/a/c/g;->a(Ljava/lang/StringBuilder;Ljava/lang/String;II)V

    const-string p3, "      max"

    const p4, 0x7fffffff

    invoke-direct {p0, p1, p3, p5, p4}, La/e/a/c/g;->a(Ljava/lang/StringBuilder;Ljava/lang/String;II)V

    const-string p3, "      matchMin"

    invoke-direct {p0, p1, p3, p7, p2}, La/e/a/c/g;->a(Ljava/lang/StringBuilder;Ljava/lang/String;II)V

    const-string p3, "      matchDef"

    invoke-direct {p0, p1, p3, p8, p2}, La/e/a/c/g;->a(Ljava/lang/StringBuilder;Ljava/lang/String;II)V

    const-string p2, "      matchPercent"

    const/high16 p3, 0x3f800000    # 1.0f

    invoke-direct {p0, p1, p2, p9, p3}, La/e/a/c/g;->a(Ljava/lang/StringBuilder;Ljava/lang/String;FF)V

    const-string p2, "    },\n"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private a(Ljava/lang/StringBuilder;Ljava/lang/String;La/e/a/c/e;)V
    .locals 2

    iget-object v0, p3, La/e/a/c/e;->f:La/e/a/c/e;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v0, "    "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " : [ \'"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p3, La/e/a/c/e;->f:La/e/a/c/e;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, "\'"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p3, La/e/a/c/e;->h:I

    const/high16 v0, -0x80000000

    if-ne p2, v0, :cond_1

    iget p2, p3, La/e/a/c/e;->g:I

    if-eqz p2, :cond_2

    :cond_1
    const-string p2, ","

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p3, La/e/a/c/e;->g:I

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget v1, p3, La/e/a/c/e;->h:I

    if-eq v1, v0, :cond_2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p3, p3, La/e/a/c/e;->h:I

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const-string p2, " ] ,\n"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private y(I)Z
    .locals 4

    mul-int/lit8 p1, p1, 0x2

    iget-object v0, p0, La/e/a/c/g;->Z:[La/e/a/c/e;

    aget-object v1, v0, p1

    iget-object v1, v1, La/e/a/c/e;->f:La/e/a/c/e;

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    aget-object v1, v0, p1

    iget-object v1, v1, La/e/a/c/e;->f:La/e/a/c/e;

    iget-object v1, v1, La/e/a/c/e;->f:La/e/a/c/e;

    aget-object v3, v0, p1

    if-eq v1, v3, :cond_0

    add-int/2addr p1, v2

    aget-object v1, v0, p1

    iget-object v1, v1, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v1, :cond_0

    aget-object v1, v0, p1

    iget-object v1, v1, La/e/a/c/e;->f:La/e/a/c/e;

    iget-object v1, v1, La/e/a/c/e;->f:La/e/a/c/e;

    aget-object p1, v0, p1

    if-ne v1, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2
.end method


# virtual methods
.method public A()I
    .locals 2

    iget-object v0, p0, La/e/a/c/g;->R:La/e/a/c/e;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, La/e/a/c/g;->S:La/e/a/c/e;

    iget v0, v0, La/e/a/c/e;->g:I

    add-int/2addr v1, v0

    :cond_0
    iget-object v0, p0, La/e/a/c/g;->T:La/e/a/c/e;

    if-eqz v0, :cond_1

    iget-object v0, p0, La/e/a/c/g;->U:La/e/a/c/e;

    iget v0, v0, La/e/a/c/e;->g:I

    add-int/2addr v1, v0

    :cond_1
    return v1
.end method

.method public B()I
    .locals 1

    iget v0, p0, La/e/a/c/g;->va:I

    return v0
.end method

.method public C()I
    .locals 2

    iget v0, p0, La/e/a/c/g;->va:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget v0, p0, La/e/a/c/g;->ea:I

    return v0
.end method

.method public D()I
    .locals 2

    iget-object v0, p0, La/e/a/c/g;->da:La/e/a/c/g;

    if-eqz v0, :cond_0

    instance-of v1, v0, La/e/a/c/h;

    if-eqz v1, :cond_0

    check-cast v0, La/e/a/c/h;

    iget v0, v0, La/e/a/c/h;->bb:I

    iget v1, p0, La/e/a/c/g;->ia:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget v0, p0, La/e/a/c/g;->ia:I

    return v0
.end method

.method public E()I
    .locals 2

    iget-object v0, p0, La/e/a/c/g;->da:La/e/a/c/g;

    if-eqz v0, :cond_0

    instance-of v1, v0, La/e/a/c/h;

    if-eqz v1, :cond_0

    check-cast v0, La/e/a/c/h;

    iget v0, v0, La/e/a/c/h;->cb:I

    iget v1, p0, La/e/a/c/g;->ja:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget v0, p0, La/e/a/c/g;->ja:I

    return v0
.end method

.method public F()Z
    .locals 1

    iget-boolean v0, p0, La/e/a/c/g;->M:Z

    return v0
.end method

.method public G()Z
    .locals 4

    iget-object v0, p0, La/e/a/c/g;->aa:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_1

    iget-object v3, p0, La/e/a/c/g;->aa:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, La/e/a/c/e;

    invoke-virtual {v3}, La/e/a/c/e;->j()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method public H()Z
    .locals 2

    iget v0, p0, La/e/a/c/g;->m:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    iget v0, p0, La/e/a/c/g;->n:I

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public I()Z
    .locals 1

    iget-boolean v0, p0, La/e/a/c/g;->s:Z

    return v0
.end method

.method public J()Z
    .locals 2

    iget-object v0, p0, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v1, v0, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v1, :cond_0

    iget-object v1, v1, La/e/a/c/e;->f:La/e/a/c/e;

    if-eq v1, v0, :cond_1

    :cond_0
    iget-object v0, p0, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v1, v0, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v1, :cond_2

    iget-object v1, v1, La/e/a/c/e;->f:La/e/a/c/e;

    if-ne v1, v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    return v0

    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method public K()Z
    .locals 1

    iget-boolean v0, p0, La/e/a/c/g;->N:Z

    return v0
.end method

.method public L()Z
    .locals 2

    iget-object v0, p0, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v1, v0, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v1, :cond_0

    iget-object v1, v1, La/e/a/c/e;->f:La/e/a/c/e;

    if-eq v1, v0, :cond_1

    :cond_0
    iget-object v0, p0, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object v1, v0, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v1, :cond_2

    iget-object v1, v1, La/e/a/c/e;->f:La/e/a/c/e;

    if-ne v1, v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    return v0

    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method public M()Z
    .locals 1

    iget-boolean v0, p0, La/e/a/c/g;->O:Z

    return v0
.end method

.method public N()Z
    .locals 2

    iget-boolean v0, p0, La/e/a/c/g;->j:Z

    if-eqz v0, :cond_0

    iget v0, p0, La/e/a/c/g;->va:I

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public O()Z
    .locals 1

    iget-boolean v0, p0, La/e/a/c/g;->q:Z

    if-nez v0, :cond_1

    iget-object v0, p0, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {v0}, La/e/a/c/e;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {v0}, La/e/a/c/e;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public P()Z
    .locals 1

    iget-boolean v0, p0, La/e/a/c/g;->r:Z

    if-nez v0, :cond_1

    iget-object v0, p0, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {v0}, La/e/a/c/e;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {v0}, La/e/a/c/e;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public Q()Z
    .locals 1

    iget-boolean v0, p0, La/e/a/c/g;->t:Z

    return v0
.end method

.method public R()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, La/e/a/c/g;->s:Z

    return-void
.end method

.method public S()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, La/e/a/c/g;->t:Z

    return-void
.end method

.method public T()Z
    .locals 5

    iget-object v0, p0, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    const/4 v1, 0x0

    aget-object v2, v0, v1

    sget-object v3, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    const/4 v4, 0x1

    if-ne v2, v3, :cond_0

    aget-object v0, v0, v4

    if-ne v0, v3, :cond_0

    move v1, v4

    :cond_0
    return v1
.end method

.method public U()V
    .locals 6

    iget-object v0, p0, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {v0}, La/e/a/c/e;->m()V

    iget-object v0, p0, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {v0}, La/e/a/c/e;->m()V

    iget-object v0, p0, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {v0}, La/e/a/c/e;->m()V

    iget-object v0, p0, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {v0}, La/e/a/c/e;->m()V

    iget-object v0, p0, La/e/a/c/g;->V:La/e/a/c/e;

    invoke-virtual {v0}, La/e/a/c/e;->m()V

    iget-object v0, p0, La/e/a/c/g;->W:La/e/a/c/e;

    invoke-virtual {v0}, La/e/a/c/e;->m()V

    iget-object v0, p0, La/e/a/c/g;->X:La/e/a/c/e;

    invoke-virtual {v0}, La/e/a/c/e;->m()V

    iget-object v0, p0, La/e/a/c/g;->Y:La/e/a/c/e;

    invoke-virtual {v0}, La/e/a/c/e;->m()V

    const/4 v0, 0x0

    iput-object v0, p0, La/e/a/c/g;->da:La/e/a/c/g;

    const/4 v1, 0x0

    iput v1, p0, La/e/a/c/g;->L:F

    const/4 v2, 0x0

    iput v2, p0, La/e/a/c/g;->ea:I

    iput v2, p0, La/e/a/c/g;->fa:I

    iput v1, p0, La/e/a/c/g;->ga:F

    const/4 v1, -0x1

    iput v1, p0, La/e/a/c/g;->ha:I

    iput v2, p0, La/e/a/c/g;->ia:I

    iput v2, p0, La/e/a/c/g;->ja:I

    iput v2, p0, La/e/a/c/g;->ma:I

    iput v2, p0, La/e/a/c/g;->na:I

    iput v2, p0, La/e/a/c/g;->oa:I

    iput v2, p0, La/e/a/c/g;->pa:I

    iput v2, p0, La/e/a/c/g;->qa:I

    sget v3, La/e/a/c/g;->a:F

    iput v3, p0, La/e/a/c/g;->ra:F

    iput v3, p0, La/e/a/c/g;->sa:F

    iget-object v3, p0, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    sget-object v4, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    aput-object v4, v3, v2

    const/4 v5, 0x1

    aput-object v4, v3, v5

    iput-object v0, p0, La/e/a/c/g;->ta:Ljava/lang/Object;

    iput v2, p0, La/e/a/c/g;->ua:I

    iput v2, p0, La/e/a/c/g;->va:I

    iput-object v0, p0, La/e/a/c/g;->xa:Ljava/lang/String;

    iput-boolean v2, p0, La/e/a/c/g;->Ga:Z

    iput-boolean v2, p0, La/e/a/c/g;->Ha:Z

    iput v2, p0, La/e/a/c/g;->Ja:I

    iput v2, p0, La/e/a/c/g;->Ka:I

    iput-boolean v2, p0, La/e/a/c/g;->La:Z

    iput-boolean v2, p0, La/e/a/c/g;->Ma:Z

    iget-object v0, p0, La/e/a/c/g;->Na:[F

    const/high16 v3, -0x40800000    # -1.0f

    aput v3, v0, v2

    aput v3, v0, v5

    iput v1, p0, La/e/a/c/g;->u:I

    iput v1, p0, La/e/a/c/g;->v:I

    iget-object v0, p0, La/e/a/c/g;->K:[I

    const v3, 0x7fffffff

    aput v3, v0, v2

    aput v3, v0, v5

    iput v2, p0, La/e/a/c/g;->x:I

    iput v2, p0, La/e/a/c/g;->y:I

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, La/e/a/c/g;->C:F

    iput v0, p0, La/e/a/c/g;->F:F

    iput v3, p0, La/e/a/c/g;->B:I

    iput v3, p0, La/e/a/c/g;->E:I

    iput v2, p0, La/e/a/c/g;->A:I

    iput v2, p0, La/e/a/c/g;->D:I

    iput-boolean v2, p0, La/e/a/c/g;->i:Z

    iput v1, p0, La/e/a/c/g;->I:I

    iput v0, p0, La/e/a/c/g;->J:F

    iput-boolean v2, p0, La/e/a/c/g;->Ia:Z

    iget-object v0, p0, La/e/a/c/g;->h:[Z

    aput-boolean v5, v0, v2

    aput-boolean v5, v0, v5

    iput-boolean v2, p0, La/e/a/c/g;->O:Z

    iget-object v0, p0, La/e/a/c/g;->ba:[Z

    aput-boolean v2, v0, v2

    aput-boolean v2, v0, v5

    iput-boolean v5, p0, La/e/a/c/g;->j:Z

    iget-object v0, p0, La/e/a/c/g;->z:[I

    aput v2, v0, v2

    aput v2, v0, v5

    iput v1, p0, La/e/a/c/g;->m:I

    iput v1, p0, La/e/a/c/g;->n:I

    return-void
.end method

.method public V()V
    .locals 1

    invoke-virtual {p0}, La/e/a/c/g;->W()V

    sget v0, La/e/a/c/g;->a:F

    invoke-virtual {p0, v0}, La/e/a/c/g;->c(F)V

    sget v0, La/e/a/c/g;->a:F

    invoke-virtual {p0, v0}, La/e/a/c/g;->a(F)V

    return-void
.end method

.method public W()V
    .locals 3

    invoke-virtual {p0}, La/e/a/c/g;->v()La/e/a/c/g;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v0, v0, La/e/a/c/h;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, La/e/a/c/g;->v()La/e/a/c/g;

    move-result-object v0

    check-cast v0, La/e/a/c/h;

    invoke-virtual {v0}, La/e/a/c/h;->ea()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, La/e/a/c/g;->aa:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v2, p0, La/e/a/c/g;->aa:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, La/e/a/c/e;

    invoke-virtual {v2}, La/e/a/c/e;->m()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public X()V
    .locals 3

    const/4 v0, 0x0

    iput-boolean v0, p0, La/e/a/c/g;->q:Z

    iput-boolean v0, p0, La/e/a/c/g;->r:Z

    iput-boolean v0, p0, La/e/a/c/g;->s:Z

    iput-boolean v0, p0, La/e/a/c/g;->t:Z

    iget-object v1, p0, La/e/a/c/g;->aa:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, La/e/a/c/g;->aa:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, La/e/a/c/e;

    invoke-virtual {v2}, La/e/a/c/e;->n()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(I)F
    .locals 1

    if-nez p1, :cond_0

    iget p1, p0, La/e/a/c/g;->ra:F

    return p1

    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    iget p1, p0, La/e/a/c/g;->sa:F

    return p1

    :cond_1
    const/high16 p1, -0x40800000    # -1.0f

    return p1
.end method

.method public a(La/e/a/c/e$a;)La/e/a/c/e;
    .locals 2

    sget-object v0, La/e/a/c/f;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :pswitch_0
    const/4 p1, 0x0

    return-object p1

    :pswitch_1
    iget-object p1, p0, La/e/a/c/g;->X:La/e/a/c/e;

    return-object p1

    :pswitch_2
    iget-object p1, p0, La/e/a/c/g;->W:La/e/a/c/e;

    return-object p1

    :pswitch_3
    iget-object p1, p0, La/e/a/c/g;->Y:La/e/a/c/e;

    return-object p1

    :pswitch_4
    iget-object p1, p0, La/e/a/c/g;->V:La/e/a/c/e;

    return-object p1

    :pswitch_5
    iget-object p1, p0, La/e/a/c/g;->U:La/e/a/c/e;

    return-object p1

    :pswitch_6
    iget-object p1, p0, La/e/a/c/g;->T:La/e/a/c/e;

    return-object p1

    :pswitch_7
    iget-object p1, p0, La/e/a/c/g;->S:La/e/a/c/e;

    return-object p1

    :pswitch_8
    iget-object p1, p0, La/e/a/c/g;->R:La/e/a/c/e;

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(F)V
    .locals 0

    iput p1, p0, La/e/a/c/g;->ra:F

    return-void
.end method

.method public a(IIIF)V
    .locals 0

    iput p1, p0, La/e/a/c/g;->x:I

    iput p2, p0, La/e/a/c/g;->A:I

    const p1, 0x7fffffff

    if-ne p3, p1, :cond_0

    const/4 p3, 0x0

    :cond_0
    iput p3, p0, La/e/a/c/g;->B:I

    iput p4, p0, La/e/a/c/g;->C:F

    const/4 p1, 0x0

    cmpl-float p1, p4, p1

    if-lez p1, :cond_1

    const/high16 p1, 0x3f800000    # 1.0f

    cmpg-float p1, p4, p1

    if-gez p1, :cond_1

    iget p1, p0, La/e/a/c/g;->x:I

    if-nez p1, :cond_1

    const/4 p1, 0x2

    iput p1, p0, La/e/a/c/g;->x:I

    :cond_1
    return-void
.end method

.method public a(IIII)V
    .locals 2

    sub-int/2addr p3, p1

    sub-int/2addr p4, p2

    iput p1, p0, La/e/a/c/g;->ia:I

    iput p2, p0, La/e/a/c/g;->ja:I

    iget p1, p0, La/e/a/c/g;->va:I

    const/4 p2, 0x0

    const/16 v0, 0x8

    if-ne p1, v0, :cond_0

    iput p2, p0, La/e/a/c/g;->ea:I

    iput p2, p0, La/e/a/c/g;->fa:I

    return-void

    :cond_0
    iget-object p1, p0, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object p1, p1, p2

    sget-object v0, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    if-ne p1, v0, :cond_1

    iget p1, p0, La/e/a/c/g;->ea:I

    if-ge p3, p1, :cond_1

    goto :goto_0

    :cond_1
    move p1, p3

    :goto_0
    iget-object p3, p0, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    const/4 v0, 0x1

    aget-object p3, p3, v0

    sget-object v1, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    if-ne p3, v1, :cond_2

    iget p3, p0, La/e/a/c/g;->fa:I

    if-ge p4, p3, :cond_2

    goto :goto_1

    :cond_2
    move p3, p4

    :goto_1
    iput p1, p0, La/e/a/c/g;->ea:I

    iput p3, p0, La/e/a/c/g;->fa:I

    iget p4, p0, La/e/a/c/g;->fa:I

    iget v1, p0, La/e/a/c/g;->qa:I

    if-ge p4, v1, :cond_3

    iput v1, p0, La/e/a/c/g;->fa:I

    :cond_3
    iget p4, p0, La/e/a/c/g;->ea:I

    iget v1, p0, La/e/a/c/g;->pa:I

    if-ge p4, v1, :cond_4

    iput v1, p0, La/e/a/c/g;->ea:I

    :cond_4
    iget p4, p0, La/e/a/c/g;->B:I

    if-lez p4, :cond_5

    iget-object v1, p0, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object p2, v1, p2

    sget-object v1, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne p2, v1, :cond_5

    iget p2, p0, La/e/a/c/g;->ea:I

    invoke-static {p2, p4}, Ljava/lang/Math;->min(II)I

    move-result p2

    iput p2, p0, La/e/a/c/g;->ea:I

    :cond_5
    iget p2, p0, La/e/a/c/g;->E:I

    if-lez p2, :cond_6

    iget-object p4, p0, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object p4, p4, v0

    sget-object v0, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne p4, v0, :cond_6

    iget p4, p0, La/e/a/c/g;->fa:I

    invoke-static {p4, p2}, Ljava/lang/Math;->min(II)I

    move-result p2

    iput p2, p0, La/e/a/c/g;->fa:I

    :cond_6
    iget p2, p0, La/e/a/c/g;->ea:I

    if-eq p1, p2, :cond_7

    iput p2, p0, La/e/a/c/g;->m:I

    :cond_7
    iget p1, p0, La/e/a/c/g;->fa:I

    if-eq p3, p1, :cond_8

    iput p1, p0, La/e/a/c/g;->n:I

    :cond_8
    return-void
.end method

.method protected a(IZ)V
    .locals 1

    iget-object v0, p0, La/e/a/c/g;->ba:[Z

    aput-boolean p2, v0, p1

    return-void
.end method

.method public a(La/e/a/c/e$a;La/e/a/c/g;La/e/a/c/e$a;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, La/e/a/c/g;->a(La/e/a/c/e$a;La/e/a/c/g;La/e/a/c/e$a;I)V

    return-void
.end method

.method public a(La/e/a/c/e$a;La/e/a/c/g;La/e/a/c/e$a;I)V
    .locals 3

    sget-object v0, La/e/a/c/e$a;->g:La/e/a/c/e$a;

    const/4 v1, 0x0

    if-ne p1, v0, :cond_c

    if-ne p3, v0, :cond_8

    sget-object p1, La/e/a/c/e$a;->b:La/e/a/c/e$a;

    invoke-virtual {p0, p1}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p1

    sget-object p3, La/e/a/c/e$a;->d:La/e/a/c/e$a;

    invoke-virtual {p0, p3}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p3

    sget-object p4, La/e/a/c/e$a;->c:La/e/a/c/e$a;

    invoke-virtual {p0, p4}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p4

    sget-object v0, La/e/a/c/e$a;->e:La/e/a/c/e$a;

    invoke-virtual {p0, v0}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v0

    const/4 v2, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, La/e/a/c/e;->l()Z

    move-result p1

    if-nez p1, :cond_1

    :cond_0
    if-eqz p3, :cond_2

    invoke-virtual {p3}, La/e/a/c/e;->l()Z

    move-result p1

    if-eqz p1, :cond_2

    :cond_1
    move p1, v1

    goto :goto_0

    :cond_2
    sget-object p1, La/e/a/c/e$a;->b:La/e/a/c/e$a;

    invoke-virtual {p0, p1, p2, p1, v1}, La/e/a/c/g;->a(La/e/a/c/e$a;La/e/a/c/g;La/e/a/c/e$a;I)V

    sget-object p1, La/e/a/c/e$a;->d:La/e/a/c/e$a;

    invoke-virtual {p0, p1, p2, p1, v1}, La/e/a/c/g;->a(La/e/a/c/e$a;La/e/a/c/g;La/e/a/c/e$a;I)V

    move p1, v2

    :goto_0
    if-eqz p4, :cond_3

    invoke-virtual {p4}, La/e/a/c/e;->l()Z

    move-result p3

    if-nez p3, :cond_4

    :cond_3
    if-eqz v0, :cond_5

    invoke-virtual {v0}, La/e/a/c/e;->l()Z

    move-result p3

    if-eqz p3, :cond_5

    :cond_4
    move v2, v1

    goto :goto_1

    :cond_5
    sget-object p3, La/e/a/c/e$a;->c:La/e/a/c/e$a;

    invoke-virtual {p0, p3, p2, p3, v1}, La/e/a/c/g;->a(La/e/a/c/e$a;La/e/a/c/g;La/e/a/c/e$a;I)V

    sget-object p3, La/e/a/c/e$a;->e:La/e/a/c/e$a;

    invoke-virtual {p0, p3, p2, p3, v1}, La/e/a/c/g;->a(La/e/a/c/e$a;La/e/a/c/g;La/e/a/c/e$a;I)V

    :goto_1
    if-eqz p1, :cond_6

    if-eqz v2, :cond_6

    sget-object p1, La/e/a/c/e$a;->g:La/e/a/c/e$a;

    invoke-virtual {p0, p1}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p1

    sget-object p3, La/e/a/c/e$a;->g:La/e/a/c/e$a;

    invoke-virtual {p2, p3}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p2

    invoke-virtual {p1, p2, v1}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    goto/16 :goto_5

    :cond_6
    if-eqz p1, :cond_7

    sget-object p1, La/e/a/c/e$a;->h:La/e/a/c/e$a;

    invoke-virtual {p0, p1}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p1

    sget-object p3, La/e/a/c/e$a;->h:La/e/a/c/e$a;

    invoke-virtual {p2, p3}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p2

    invoke-virtual {p1, p2, v1}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    goto/16 :goto_5

    :cond_7
    if-eqz v2, :cond_1c

    sget-object p1, La/e/a/c/e$a;->i:La/e/a/c/e$a;

    invoke-virtual {p0, p1}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p1

    sget-object p3, La/e/a/c/e$a;->i:La/e/a/c/e$a;

    invoke-virtual {p2, p3}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p2

    invoke-virtual {p1, p2, v1}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    goto/16 :goto_5

    :cond_8
    sget-object p1, La/e/a/c/e$a;->b:La/e/a/c/e$a;

    if-eq p3, p1, :cond_b

    sget-object p1, La/e/a/c/e$a;->d:La/e/a/c/e$a;

    if-ne p3, p1, :cond_9

    goto :goto_2

    :cond_9
    sget-object p1, La/e/a/c/e$a;->c:La/e/a/c/e$a;

    if-eq p3, p1, :cond_a

    sget-object p1, La/e/a/c/e$a;->e:La/e/a/c/e$a;

    if-ne p3, p1, :cond_1c

    :cond_a
    sget-object p1, La/e/a/c/e$a;->c:La/e/a/c/e$a;

    invoke-virtual {p0, p1, p2, p3, v1}, La/e/a/c/g;->a(La/e/a/c/e$a;La/e/a/c/g;La/e/a/c/e$a;I)V

    sget-object p1, La/e/a/c/e$a;->e:La/e/a/c/e$a;

    invoke-virtual {p0, p1, p2, p3, v1}, La/e/a/c/g;->a(La/e/a/c/e$a;La/e/a/c/g;La/e/a/c/e$a;I)V

    sget-object p1, La/e/a/c/e$a;->g:La/e/a/c/e$a;

    invoke-virtual {p0, p1}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p1

    invoke-virtual {p2, p3}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p2

    invoke-virtual {p1, p2, v1}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    goto/16 :goto_5

    :cond_b
    :goto_2
    sget-object p1, La/e/a/c/e$a;->b:La/e/a/c/e$a;

    invoke-virtual {p0, p1, p2, p3, v1}, La/e/a/c/g;->a(La/e/a/c/e$a;La/e/a/c/g;La/e/a/c/e$a;I)V

    sget-object p1, La/e/a/c/e$a;->d:La/e/a/c/e$a;

    invoke-virtual {p0, p1, p2, p3, v1}, La/e/a/c/g;->a(La/e/a/c/e$a;La/e/a/c/g;La/e/a/c/e$a;I)V

    sget-object p1, La/e/a/c/e$a;->g:La/e/a/c/e$a;

    invoke-virtual {p0, p1}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p1

    invoke-virtual {p2, p3}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p2

    invoke-virtual {p1, p2, v1}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    goto/16 :goto_5

    :cond_c
    sget-object v0, La/e/a/c/e$a;->h:La/e/a/c/e$a;

    if-ne p1, v0, :cond_e

    sget-object v0, La/e/a/c/e$a;->b:La/e/a/c/e$a;

    if-eq p3, v0, :cond_d

    sget-object v0, La/e/a/c/e$a;->d:La/e/a/c/e$a;

    if-ne p3, v0, :cond_e

    :cond_d
    sget-object p1, La/e/a/c/e$a;->b:La/e/a/c/e$a;

    invoke-virtual {p0, p1}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p1

    invoke-virtual {p2, p3}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p2

    sget-object p3, La/e/a/c/e$a;->d:La/e/a/c/e$a;

    invoke-virtual {p0, p3}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p3

    invoke-virtual {p1, p2, v1}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    invoke-virtual {p3, p2, v1}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    sget-object p1, La/e/a/c/e$a;->h:La/e/a/c/e$a;

    invoke-virtual {p0, p1}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p1

    invoke-virtual {p1, p2, v1}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    goto/16 :goto_5

    :cond_e
    sget-object v0, La/e/a/c/e$a;->i:La/e/a/c/e$a;

    if-ne p1, v0, :cond_10

    sget-object v0, La/e/a/c/e$a;->c:La/e/a/c/e$a;

    if-eq p3, v0, :cond_f

    sget-object v0, La/e/a/c/e$a;->e:La/e/a/c/e$a;

    if-ne p3, v0, :cond_10

    :cond_f
    invoke-virtual {p2, p3}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p1

    sget-object p2, La/e/a/c/e$a;->c:La/e/a/c/e$a;

    invoke-virtual {p0, p2}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p2

    invoke-virtual {p2, p1, v1}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    sget-object p2, La/e/a/c/e$a;->e:La/e/a/c/e$a;

    invoke-virtual {p0, p2}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p2

    invoke-virtual {p2, p1, v1}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    sget-object p2, La/e/a/c/e$a;->i:La/e/a/c/e$a;

    invoke-virtual {p0, p2}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p2

    invoke-virtual {p2, p1, v1}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    goto/16 :goto_5

    :cond_10
    sget-object v0, La/e/a/c/e$a;->h:La/e/a/c/e$a;

    if-ne p1, v0, :cond_11

    if-ne p3, v0, :cond_11

    sget-object p1, La/e/a/c/e$a;->b:La/e/a/c/e$a;

    invoke-virtual {p0, p1}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p1

    sget-object p4, La/e/a/c/e$a;->b:La/e/a/c/e$a;

    invoke-virtual {p2, p4}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p4

    invoke-virtual {p1, p4, v1}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    sget-object p1, La/e/a/c/e$a;->d:La/e/a/c/e$a;

    invoke-virtual {p0, p1}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p1

    sget-object p4, La/e/a/c/e$a;->d:La/e/a/c/e$a;

    invoke-virtual {p2, p4}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p4

    invoke-virtual {p1, p4, v1}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    sget-object p1, La/e/a/c/e$a;->h:La/e/a/c/e$a;

    invoke-virtual {p0, p1}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p1

    invoke-virtual {p2, p3}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p2

    invoke-virtual {p1, p2, v1}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    goto/16 :goto_5

    :cond_11
    sget-object v0, La/e/a/c/e$a;->i:La/e/a/c/e$a;

    if-ne p1, v0, :cond_12

    if-ne p3, v0, :cond_12

    sget-object p1, La/e/a/c/e$a;->c:La/e/a/c/e$a;

    invoke-virtual {p0, p1}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p1

    sget-object p4, La/e/a/c/e$a;->c:La/e/a/c/e$a;

    invoke-virtual {p2, p4}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p4

    invoke-virtual {p1, p4, v1}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    sget-object p1, La/e/a/c/e$a;->e:La/e/a/c/e$a;

    invoke-virtual {p0, p1}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p1

    sget-object p4, La/e/a/c/e$a;->e:La/e/a/c/e$a;

    invoke-virtual {p2, p4}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p4

    invoke-virtual {p1, p4, v1}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    sget-object p1, La/e/a/c/e$a;->i:La/e/a/c/e$a;

    invoke-virtual {p0, p1}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p1

    invoke-virtual {p2, p3}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p2

    invoke-virtual {p1, p2, v1}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    goto/16 :goto_5

    :cond_12
    invoke-virtual {p0, p1}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v0

    invoke-virtual {p2, p3}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p2

    invoke-virtual {v0, p2}, La/e/a/c/e;->a(La/e/a/c/e;)Z

    move-result p3

    if-eqz p3, :cond_1c

    sget-object p3, La/e/a/c/e$a;->f:La/e/a/c/e$a;

    if-ne p1, p3, :cond_14

    sget-object p1, La/e/a/c/e$a;->c:La/e/a/c/e$a;

    invoke-virtual {p0, p1}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p1

    sget-object p3, La/e/a/c/e$a;->e:La/e/a/c/e$a;

    invoke-virtual {p0, p3}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p3

    if-eqz p1, :cond_13

    invoke-virtual {p1}, La/e/a/c/e;->m()V

    :cond_13
    if-eqz p3, :cond_1b

    invoke-virtual {p3}, La/e/a/c/e;->m()V

    goto :goto_4

    :cond_14
    sget-object p3, La/e/a/c/e$a;->c:La/e/a/c/e$a;

    if-eq p1, p3, :cond_18

    sget-object p3, La/e/a/c/e$a;->e:La/e/a/c/e$a;

    if-ne p1, p3, :cond_15

    goto :goto_3

    :cond_15
    sget-object p3, La/e/a/c/e$a;->b:La/e/a/c/e$a;

    if-eq p1, p3, :cond_16

    sget-object p3, La/e/a/c/e$a;->d:La/e/a/c/e$a;

    if-ne p1, p3, :cond_1b

    :cond_16
    sget-object p3, La/e/a/c/e$a;->g:La/e/a/c/e$a;

    invoke-virtual {p0, p3}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p3

    invoke-virtual {p3}, La/e/a/c/e;->g()La/e/a/c/e;

    move-result-object v1

    if-eq v1, p2, :cond_17

    invoke-virtual {p3}, La/e/a/c/e;->m()V

    :cond_17
    invoke-virtual {p0, p1}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p1

    invoke-virtual {p1}, La/e/a/c/e;->d()La/e/a/c/e;

    move-result-object p1

    sget-object p3, La/e/a/c/e$a;->h:La/e/a/c/e$a;

    invoke-virtual {p0, p3}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p3

    invoke-virtual {p3}, La/e/a/c/e;->l()Z

    move-result v1

    if-eqz v1, :cond_1b

    invoke-virtual {p1}, La/e/a/c/e;->m()V

    invoke-virtual {p3}, La/e/a/c/e;->m()V

    goto :goto_4

    :cond_18
    :goto_3
    sget-object p3, La/e/a/c/e$a;->f:La/e/a/c/e$a;

    invoke-virtual {p0, p3}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p3

    if-eqz p3, :cond_19

    invoke-virtual {p3}, La/e/a/c/e;->m()V

    :cond_19
    sget-object p3, La/e/a/c/e$a;->g:La/e/a/c/e$a;

    invoke-virtual {p0, p3}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p3

    invoke-virtual {p3}, La/e/a/c/e;->g()La/e/a/c/e;

    move-result-object v1

    if-eq v1, p2, :cond_1a

    invoke-virtual {p3}, La/e/a/c/e;->m()V

    :cond_1a
    invoke-virtual {p0, p1}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p1

    invoke-virtual {p1}, La/e/a/c/e;->d()La/e/a/c/e;

    move-result-object p1

    sget-object p3, La/e/a/c/e$a;->i:La/e/a/c/e$a;

    invoke-virtual {p0, p3}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p3

    invoke-virtual {p3}, La/e/a/c/e;->l()Z

    move-result v1

    if-eqz v1, :cond_1b

    invoke-virtual {p1}, La/e/a/c/e;->m()V

    invoke-virtual {p3}, La/e/a/c/e;->m()V

    :cond_1b
    :goto_4
    invoke-virtual {v0, p2, p4}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    :cond_1c
    :goto_5
    return-void
.end method

.method public a(La/e/a/c/e$a;La/e/a/c/g;La/e/a/c/e$a;II)V
    .locals 0

    invoke-virtual {p0, p1}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p1

    invoke-virtual {p2, p3}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object p2

    const/4 p3, 0x1

    invoke-virtual {p1, p2, p4, p5, p3}, La/e/a/c/e;->a(La/e/a/c/e;IIZ)Z

    return-void
.end method

.method public a(La/e/a/c/e;La/e/a/c/e;I)V
    .locals 1

    invoke-virtual {p1}, La/e/a/c/e;->e()La/e/a/c/g;

    move-result-object v0

    if-ne v0, p0, :cond_0

    invoke-virtual {p1}, La/e/a/c/e;->h()La/e/a/c/e$a;

    move-result-object p1

    invoke-virtual {p2}, La/e/a/c/e;->e()La/e/a/c/g;

    move-result-object v0

    invoke-virtual {p2}, La/e/a/c/e;->h()La/e/a/c/e$a;

    move-result-object p2

    invoke-virtual {p0, p1, v0, p2, p3}, La/e/a/c/g;->a(La/e/a/c/e$a;La/e/a/c/g;La/e/a/c/e$a;I)V

    :cond_0
    return-void
.end method

.method public a(La/e/a/c/g$a;)V
    .locals 2

    iget-object v0, p0, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    return-void
.end method

.method public a(La/e/a/c/g;FI)V
    .locals 6

    sget-object v3, La/e/a/c/e$a;->g:La/e/a/c/e$a;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, v3

    move-object v2, p1

    move v4, p3

    invoke-virtual/range {v0 .. v5}, La/e/a/c/g;->a(La/e/a/c/e$a;La/e/a/c/g;La/e/a/c/e$a;II)V

    iput p2, p0, La/e/a/c/g;->L:F

    return-void
.end method

.method public a(La/e/a/c/g;Ljava/util/HashMap;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La/e/a/c/g;",
            "Ljava/util/HashMap<",
            "La/e/a/c/g;",
            "La/e/a/c/g;",
            ">;)V"
        }
    .end annotation

    iget v0, p1, La/e/a/c/g;->u:I

    iput v0, p0, La/e/a/c/g;->u:I

    iget v0, p1, La/e/a/c/g;->v:I

    iput v0, p0, La/e/a/c/g;->v:I

    iget v0, p1, La/e/a/c/g;->x:I

    iput v0, p0, La/e/a/c/g;->x:I

    iget v0, p1, La/e/a/c/g;->y:I

    iput v0, p0, La/e/a/c/g;->y:I

    iget-object v0, p0, La/e/a/c/g;->z:[I

    iget-object v1, p1, La/e/a/c/g;->z:[I

    const/4 v2, 0x0

    aget v3, v1, v2

    aput v3, v0, v2

    const/4 v3, 0x1

    aget v1, v1, v3

    aput v1, v0, v3

    iget v0, p1, La/e/a/c/g;->A:I

    iput v0, p0, La/e/a/c/g;->A:I

    iget v0, p1, La/e/a/c/g;->B:I

    iput v0, p0, La/e/a/c/g;->B:I

    iget v0, p1, La/e/a/c/g;->D:I

    iput v0, p0, La/e/a/c/g;->D:I

    iget v0, p1, La/e/a/c/g;->E:I

    iput v0, p0, La/e/a/c/g;->E:I

    iget v0, p1, La/e/a/c/g;->F:F

    iput v0, p0, La/e/a/c/g;->F:F

    iget-boolean v0, p1, La/e/a/c/g;->G:Z

    iput-boolean v0, p0, La/e/a/c/g;->G:Z

    iget-boolean v0, p1, La/e/a/c/g;->H:Z

    iput-boolean v0, p0, La/e/a/c/g;->H:Z

    iget v0, p1, La/e/a/c/g;->I:I

    iput v0, p0, La/e/a/c/g;->I:I

    iget v0, p1, La/e/a/c/g;->J:F

    iput v0, p0, La/e/a/c/g;->J:F

    iget-object v0, p1, La/e/a/c/g;->K:[I

    array-length v1, v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, La/e/a/c/g;->K:[I

    iget v0, p1, La/e/a/c/g;->L:F

    iput v0, p0, La/e/a/c/g;->L:F

    iget-boolean v0, p1, La/e/a/c/g;->M:Z

    iput-boolean v0, p0, La/e/a/c/g;->M:Z

    iget-boolean v0, p1, La/e/a/c/g;->N:Z

    iput-boolean v0, p0, La/e/a/c/g;->N:Z

    iget-object v0, p0, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {v0}, La/e/a/c/e;->m()V

    iget-object v0, p0, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {v0}, La/e/a/c/e;->m()V

    iget-object v0, p0, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {v0}, La/e/a/c/e;->m()V

    iget-object v0, p0, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {v0}, La/e/a/c/e;->m()V

    iget-object v0, p0, La/e/a/c/g;->V:La/e/a/c/e;

    invoke-virtual {v0}, La/e/a/c/e;->m()V

    iget-object v0, p0, La/e/a/c/g;->W:La/e/a/c/e;

    invoke-virtual {v0}, La/e/a/c/e;->m()V

    iget-object v0, p0, La/e/a/c/g;->X:La/e/a/c/e;

    invoke-virtual {v0}, La/e/a/c/e;->m()V

    iget-object v0, p0, La/e/a/c/g;->Y:La/e/a/c/e;

    invoke-virtual {v0}, La/e/a/c/e;->m()V

    iget-object v0, p0, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [La/e/a/c/g$a;

    iput-object v0, p0, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    iget-object v0, p0, La/e/a/c/g;->da:La/e/a/c/g;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object v0, v1

    goto :goto_0

    :cond_0
    iget-object v0, p1, La/e/a/c/g;->da:La/e/a/c/g;

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/e/a/c/g;

    :goto_0
    iput-object v0, p0, La/e/a/c/g;->da:La/e/a/c/g;

    iget v0, p1, La/e/a/c/g;->ea:I

    iput v0, p0, La/e/a/c/g;->ea:I

    iget v0, p1, La/e/a/c/g;->fa:I

    iput v0, p0, La/e/a/c/g;->fa:I

    iget v0, p1, La/e/a/c/g;->ga:F

    iput v0, p0, La/e/a/c/g;->ga:F

    iget v0, p1, La/e/a/c/g;->ha:I

    iput v0, p0, La/e/a/c/g;->ha:I

    iget v0, p1, La/e/a/c/g;->ia:I

    iput v0, p0, La/e/a/c/g;->ia:I

    iget v0, p1, La/e/a/c/g;->ja:I

    iput v0, p0, La/e/a/c/g;->ja:I

    iget v0, p1, La/e/a/c/g;->ka:I

    iput v0, p0, La/e/a/c/g;->ka:I

    iget v0, p1, La/e/a/c/g;->la:I

    iput v0, p0, La/e/a/c/g;->la:I

    iget v0, p1, La/e/a/c/g;->ma:I

    iput v0, p0, La/e/a/c/g;->ma:I

    iget v0, p1, La/e/a/c/g;->na:I

    iput v0, p0, La/e/a/c/g;->na:I

    iget v0, p1, La/e/a/c/g;->oa:I

    iput v0, p0, La/e/a/c/g;->oa:I

    iget v0, p1, La/e/a/c/g;->pa:I

    iput v0, p0, La/e/a/c/g;->pa:I

    iget v0, p1, La/e/a/c/g;->qa:I

    iput v0, p0, La/e/a/c/g;->qa:I

    iget v0, p1, La/e/a/c/g;->ra:F

    iput v0, p0, La/e/a/c/g;->ra:F

    iget v0, p1, La/e/a/c/g;->sa:F

    iput v0, p0, La/e/a/c/g;->sa:F

    iget-object v0, p1, La/e/a/c/g;->ta:Ljava/lang/Object;

    iput-object v0, p0, La/e/a/c/g;->ta:Ljava/lang/Object;

    iget v0, p1, La/e/a/c/g;->ua:I

    iput v0, p0, La/e/a/c/g;->ua:I

    iget v0, p1, La/e/a/c/g;->va:I

    iput v0, p0, La/e/a/c/g;->va:I

    iget-object v0, p1, La/e/a/c/g;->wa:Ljava/lang/String;

    iput-object v0, p0, La/e/a/c/g;->wa:Ljava/lang/String;

    iget-object v0, p1, La/e/a/c/g;->xa:Ljava/lang/String;

    iput-object v0, p0, La/e/a/c/g;->xa:Ljava/lang/String;

    iget v0, p1, La/e/a/c/g;->ya:I

    iput v0, p0, La/e/a/c/g;->ya:I

    iget v0, p1, La/e/a/c/g;->za:I

    iput v0, p0, La/e/a/c/g;->za:I

    iget v0, p1, La/e/a/c/g;->Aa:I

    iput v0, p0, La/e/a/c/g;->Aa:I

    iget v0, p1, La/e/a/c/g;->Ba:I

    iput v0, p0, La/e/a/c/g;->Ba:I

    iget-boolean v0, p1, La/e/a/c/g;->Ca:Z

    iput-boolean v0, p0, La/e/a/c/g;->Ca:Z

    iget-boolean v0, p1, La/e/a/c/g;->Da:Z

    iput-boolean v0, p0, La/e/a/c/g;->Da:Z

    iget-boolean v0, p1, La/e/a/c/g;->Ea:Z

    iput-boolean v0, p0, La/e/a/c/g;->Ea:Z

    iget-boolean v0, p1, La/e/a/c/g;->Fa:Z

    iput-boolean v0, p0, La/e/a/c/g;->Fa:Z

    iget-boolean v0, p1, La/e/a/c/g;->Ga:Z

    iput-boolean v0, p0, La/e/a/c/g;->Ga:Z

    iget-boolean v0, p1, La/e/a/c/g;->Ha:Z

    iput-boolean v0, p0, La/e/a/c/g;->Ha:Z

    iget v0, p1, La/e/a/c/g;->Ja:I

    iput v0, p0, La/e/a/c/g;->Ja:I

    iget v0, p1, La/e/a/c/g;->Ka:I

    iput v0, p0, La/e/a/c/g;->Ka:I

    iget-boolean v0, p1, La/e/a/c/g;->La:Z

    iput-boolean v0, p0, La/e/a/c/g;->La:Z

    iget-boolean v0, p1, La/e/a/c/g;->Ma:Z

    iput-boolean v0, p0, La/e/a/c/g;->Ma:Z

    iget-object v0, p0, La/e/a/c/g;->Na:[F

    iget-object v4, p1, La/e/a/c/g;->Na:[F

    aget v5, v4, v2

    aput v5, v0, v2

    aget v4, v4, v3

    aput v4, v0, v3

    iget-object v0, p0, La/e/a/c/g;->Oa:[La/e/a/c/g;

    iget-object v4, p1, La/e/a/c/g;->Oa:[La/e/a/c/g;

    aget-object v5, v4, v2

    aput-object v5, v0, v2

    aget-object v4, v4, v3

    aput-object v4, v0, v3

    iget-object v0, p0, La/e/a/c/g;->Pa:[La/e/a/c/g;

    iget-object v4, p1, La/e/a/c/g;->Pa:[La/e/a/c/g;

    aget-object v5, v4, v2

    aput-object v5, v0, v2

    aget-object v2, v4, v3

    aput-object v2, v0, v3

    iget-object v0, p1, La/e/a/c/g;->Qa:La/e/a/c/g;

    if-nez v0, :cond_1

    move-object v0, v1

    goto :goto_1

    :cond_1
    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/e/a/c/g;

    :goto_1
    iput-object v0, p0, La/e/a/c/g;->Qa:La/e/a/c/g;

    iget-object p1, p1, La/e/a/c/g;->Ra:La/e/a/c/g;

    if-nez p1, :cond_2

    goto :goto_2

    :cond_2
    invoke-virtual {p2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v1, p1

    check-cast v1, La/e/a/c/g;

    :goto_2
    iput-object v1, p0, La/e/a/c/g;->Ra:La/e/a/c/g;

    return-void
.end method

.method public a(La/e/a/c/h;La/e/a/d;Ljava/util/HashSet;IZ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La/e/a/c/h;",
            "La/e/a/d;",
            "Ljava/util/HashSet<",
            "La/e/a/c/g;",
            ">;IZ)V"
        }
    .end annotation

    if-eqz p5, :cond_1

    invoke-virtual {p3, p0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result p5

    if-nez p5, :cond_0

    return-void

    :cond_0
    invoke-static {p1, p2, p0}, La/e/a/c/n;->a(La/e/a/c/h;La/e/a/d;La/e/a/c/g;)V

    invoke-virtual {p3, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    const/16 p5, 0x40

    invoke-virtual {p1, p5}, La/e/a/c/h;->y(I)Z

    move-result p5

    invoke-virtual {p0, p2, p5}, La/e/a/c/g;->a(La/e/a/d;Z)V

    :cond_1
    if-nez p4, :cond_3

    iget-object p5, p0, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {p5}, La/e/a/c/e;->a()Ljava/util/HashSet;

    move-result-object p5

    if-eqz p5, :cond_2

    invoke-virtual {p5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object p5

    :goto_0
    invoke-interface {p5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/e/a/c/e;

    iget-object v1, v0, La/e/a/c/e;->d:La/e/a/c/g;

    const/4 v6, 0x1

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-virtual/range {v1 .. v6}, La/e/a/c/g;->a(La/e/a/c/h;La/e/a/d;Ljava/util/HashSet;IZ)V

    goto :goto_0

    :cond_2
    iget-object p5, p0, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {p5}, La/e/a/c/e;->a()Ljava/util/HashSet;

    move-result-object p5

    if-eqz p5, :cond_6

    invoke-virtual {p5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object p5

    :goto_1
    invoke-interface {p5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {p5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/e/a/c/e;

    iget-object v1, v0, La/e/a/c/e;->d:La/e/a/c/g;

    const/4 v6, 0x1

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-virtual/range {v1 .. v6}, La/e/a/c/g;->a(La/e/a/c/h;La/e/a/d;Ljava/util/HashSet;IZ)V

    goto :goto_1

    :cond_3
    iget-object p5, p0, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {p5}, La/e/a/c/e;->a()Ljava/util/HashSet;

    move-result-object p5

    if-eqz p5, :cond_4

    invoke-virtual {p5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object p5

    :goto_2
    invoke-interface {p5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/e/a/c/e;

    iget-object v1, v0, La/e/a/c/e;->d:La/e/a/c/g;

    const/4 v6, 0x1

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-virtual/range {v1 .. v6}, La/e/a/c/g;->a(La/e/a/c/h;La/e/a/d;Ljava/util/HashSet;IZ)V

    goto :goto_2

    :cond_4
    iget-object p5, p0, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {p5}, La/e/a/c/e;->a()Ljava/util/HashSet;

    move-result-object p5

    if-eqz p5, :cond_5

    invoke-virtual {p5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object p5

    :goto_3
    invoke-interface {p5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/e/a/c/e;

    iget-object v1, v0, La/e/a/c/e;->d:La/e/a/c/g;

    const/4 v6, 0x1

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-virtual/range {v1 .. v6}, La/e/a/c/g;->a(La/e/a/c/h;La/e/a/d;Ljava/util/HashSet;IZ)V

    goto :goto_3

    :cond_5
    iget-object p5, p0, La/e/a/c/g;->V:La/e/a/c/e;

    invoke-virtual {p5}, La/e/a/c/e;->a()Ljava/util/HashSet;

    move-result-object p5

    if-eqz p5, :cond_6

    invoke-virtual {p5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object p5

    :goto_4
    invoke-interface {p5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {p5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/e/a/c/e;

    iget-object v1, v0, La/e/a/c/e;->d:La/e/a/c/g;

    const/4 v6, 0x1

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-virtual/range {v1 .. v6}, La/e/a/c/g;->a(La/e/a/c/h;La/e/a/d;Ljava/util/HashSet;IZ)V

    goto :goto_4

    :cond_6
    return-void
.end method

.method public a(La/e/a/c;)V
    .locals 1

    iget-object v0, p0, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {v0, p1}, La/e/a/c/e;->a(La/e/a/c;)V

    iget-object v0, p0, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {v0, p1}, La/e/a/c/e;->a(La/e/a/c;)V

    iget-object v0, p0, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {v0, p1}, La/e/a/c/e;->a(La/e/a/c;)V

    iget-object v0, p0, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {v0, p1}, La/e/a/c/e;->a(La/e/a/c;)V

    iget-object v0, p0, La/e/a/c/g;->V:La/e/a/c/e;

    invoke-virtual {v0, p1}, La/e/a/c/e;->a(La/e/a/c;)V

    iget-object v0, p0, La/e/a/c/g;->Y:La/e/a/c/e;

    invoke-virtual {v0, p1}, La/e/a/c/e;->a(La/e/a/c;)V

    iget-object v0, p0, La/e/a/c/g;->W:La/e/a/c/e;

    invoke-virtual {v0, p1}, La/e/a/c/e;->a(La/e/a/c;)V

    iget-object v0, p0, La/e/a/c/g;->X:La/e/a/c/e;

    invoke-virtual {v0, p1}, La/e/a/c/e;->a(La/e/a/c;)V

    return-void
.end method

.method public a(La/e/a/d;)V
    .locals 1

    iget-object v0, p0, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {p1, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    iget-object v0, p0, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {p1, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    iget-object v0, p0, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {p1, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    iget-object v0, p0, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {p1, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    iget v0, p0, La/e/a/c/g;->oa:I

    if-lez v0, :cond_0

    iget-object v0, p0, La/e/a/c/g;->V:La/e/a/c/e;

    invoke-virtual {p1, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    :cond_0
    return-void
.end method

.method public a(La/e/a/d;Z)V
    .locals 50

    move-object/from16 v15, p0

    move-object/from16 v14, p1

    iget-object v0, v15, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {v14, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v13

    iget-object v0, v15, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {v14, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v12

    iget-object v0, v15, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {v14, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v11

    iget-object v0, v15, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {v14, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v10

    iget-object v0, v15, La/e/a/c/g;->V:La/e/a/c/e;

    invoke-virtual {v14, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v9

    iget-object v0, v15, La/e/a/c/g;->da:La/e/a/c/g;

    const/4 v8, 0x2

    const/4 v1, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    if-eqz v0, :cond_4

    if-eqz v0, :cond_0

    iget-object v0, v0, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object v0, v0, v6

    sget-object v2, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v0, v2, :cond_0

    move v0, v7

    goto :goto_0

    :cond_0
    move v0, v6

    :goto_0
    iget-object v2, v15, La/e/a/c/g;->da:La/e/a/c/g;

    if-eqz v2, :cond_1

    iget-object v2, v2, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object v2, v2, v7

    sget-object v3, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v2, v3, :cond_1

    move v2, v7

    goto :goto_1

    :cond_1
    move v2, v6

    :goto_1
    iget v3, v15, La/e/a/c/g;->w:I

    if-eq v3, v7, :cond_3

    if-eq v3, v8, :cond_2

    if-eq v3, v1, :cond_4

    move v5, v0

    move v4, v2

    goto :goto_2

    :cond_2
    move v4, v2

    move v5, v6

    goto :goto_2

    :cond_3
    move v5, v0

    move v4, v6

    goto :goto_2

    :cond_4
    move v4, v6

    move v5, v4

    :goto_2
    iget v0, v15, La/e/a/c/g;->va:I

    const/16 v3, 0x8

    if-ne v0, v3, :cond_5

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->G()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, v15, La/e/a/c/g;->ba:[Z

    aget-boolean v2, v0, v6

    if-nez v2, :cond_5

    aget-boolean v0, v0, v7

    if-nez v0, :cond_5

    return-void

    :cond_5
    iget-boolean v0, v15, La/e/a/c/g;->q:Z

    const/4 v2, 0x5

    if-nez v0, :cond_6

    iget-boolean v0, v15, La/e/a/c/g;->r:Z

    if-eqz v0, :cond_c

    :cond_6
    iget-boolean v0, v15, La/e/a/c/g;->q:Z

    if-eqz v0, :cond_8

    iget v0, v15, La/e/a/c/g;->ia:I

    invoke-virtual {v14, v13, v0}, La/e/a/d;->a(La/e/a/j;I)V

    iget v0, v15, La/e/a/c/g;->ia:I

    iget v8, v15, La/e/a/c/g;->ea:I

    add-int/2addr v0, v8

    invoke-virtual {v14, v12, v0}, La/e/a/d;->a(La/e/a/j;I)V

    if-eqz v5, :cond_8

    iget-object v0, v15, La/e/a/c/g;->da:La/e/a/c/g;

    if-eqz v0, :cond_8

    iget-boolean v8, v15, La/e/a/c/g;->l:Z

    if-eqz v8, :cond_7

    check-cast v0, La/e/a/c/h;

    iget-object v8, v15, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {v0, v8}, La/e/a/c/h;->b(La/e/a/c/e;)V

    iget-object v8, v15, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {v0, v8}, La/e/a/c/h;->a(La/e/a/c/e;)V

    goto :goto_3

    :cond_7
    iget-object v0, v0, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {v14, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v0

    invoke-virtual {v14, v0, v12, v6, v2}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    :cond_8
    :goto_3
    iget-boolean v0, v15, La/e/a/c/g;->r:Z

    if-eqz v0, :cond_b

    iget v0, v15, La/e/a/c/g;->ja:I

    invoke-virtual {v14, v11, v0}, La/e/a/d;->a(La/e/a/j;I)V

    iget v0, v15, La/e/a/c/g;->ja:I

    iget v8, v15, La/e/a/c/g;->fa:I

    add-int/2addr v0, v8

    invoke-virtual {v14, v10, v0}, La/e/a/d;->a(La/e/a/j;I)V

    iget-object v0, v15, La/e/a/c/g;->V:La/e/a/c/e;

    invoke-virtual {v0}, La/e/a/c/e;->j()Z

    move-result v0

    if-eqz v0, :cond_9

    iget v0, v15, La/e/a/c/g;->ja:I

    iget v8, v15, La/e/a/c/g;->oa:I

    add-int/2addr v0, v8

    invoke-virtual {v14, v9, v0}, La/e/a/d;->a(La/e/a/j;I)V

    :cond_9
    if-eqz v4, :cond_b

    iget-object v0, v15, La/e/a/c/g;->da:La/e/a/c/g;

    if-eqz v0, :cond_b

    iget-boolean v8, v15, La/e/a/c/g;->l:Z

    if-eqz v8, :cond_a

    check-cast v0, La/e/a/c/h;

    iget-object v8, v15, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {v0, v8}, La/e/a/c/h;->d(La/e/a/c/e;)V

    iget-object v8, v15, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {v0, v8}, La/e/a/c/h;->c(La/e/a/c/e;)V

    goto :goto_4

    :cond_a
    iget-object v0, v0, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {v14, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v0

    invoke-virtual {v14, v0, v10, v6, v2}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    :cond_b
    :goto_4
    iget-boolean v0, v15, La/e/a/c/g;->q:Z

    if-eqz v0, :cond_c

    iget-boolean v0, v15, La/e/a/c/g;->r:Z

    if-eqz v0, :cond_c

    iput-boolean v6, v15, La/e/a/c/g;->q:Z

    iput-boolean v6, v15, La/e/a/c/g;->r:Z

    return-void

    :cond_c
    sget-object v0, La/e/a/d;->g:La/e/a/e;

    const-wide/16 v17, 0x1

    if-eqz v0, :cond_d

    iget-wide v1, v0, La/e/a/e;->y:J

    add-long v1, v1, v17

    iput-wide v1, v0, La/e/a/e;->y:J

    :cond_d
    if-eqz p2, :cond_11

    iget-object v0, v15, La/e/a/c/g;->f:La/e/a/c/a/m;

    if-eqz v0, :cond_11

    iget-object v1, v15, La/e/a/c/g;->g:La/e/a/c/a/p;

    if-eqz v1, :cond_11

    iget-object v2, v0, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget-boolean v2, v2, La/e/a/c/a/f;->j:Z

    if-eqz v2, :cond_11

    iget-object v0, v0, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget-boolean v0, v0, La/e/a/c/a/f;->j:Z

    if-eqz v0, :cond_11

    iget-object v0, v1, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget-boolean v0, v0, La/e/a/c/a/f;->j:Z

    if-eqz v0, :cond_11

    iget-object v0, v1, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget-boolean v0, v0, La/e/a/c/a/f;->j:Z

    if-eqz v0, :cond_11

    sget-object v0, La/e/a/d;->g:La/e/a/e;

    if-eqz v0, :cond_e

    iget-wide v1, v0, La/e/a/e;->r:J

    add-long v1, v1, v17

    iput-wide v1, v0, La/e/a/e;->r:J

    :cond_e
    iget-object v0, v15, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v0, v0, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget v0, v0, La/e/a/c/a/f;->g:I

    invoke-virtual {v14, v13, v0}, La/e/a/d;->a(La/e/a/j;I)V

    iget-object v0, v15, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v0, v0, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget v0, v0, La/e/a/c/a/f;->g:I

    invoke-virtual {v14, v12, v0}, La/e/a/d;->a(La/e/a/j;I)V

    iget-object v0, v15, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v0, v0, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget v0, v0, La/e/a/c/a/f;->g:I

    invoke-virtual {v14, v11, v0}, La/e/a/d;->a(La/e/a/j;I)V

    iget-object v0, v15, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v0, v0, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget v0, v0, La/e/a/c/a/f;->g:I

    invoke-virtual {v14, v10, v0}, La/e/a/d;->a(La/e/a/j;I)V

    iget-object v0, v15, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v0, v0, La/e/a/c/a/p;->k:La/e/a/c/a/f;

    iget v0, v0, La/e/a/c/a/f;->g:I

    invoke-virtual {v14, v9, v0}, La/e/a/d;->a(La/e/a/j;I)V

    iget-object v0, v15, La/e/a/c/g;->da:La/e/a/c/g;

    if-eqz v0, :cond_10

    if-eqz v5, :cond_f

    iget-object v0, v15, La/e/a/c/g;->h:[Z

    aget-boolean v0, v0, v6

    if-eqz v0, :cond_f

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->J()Z

    move-result v0

    if-nez v0, :cond_f

    iget-object v0, v15, La/e/a/c/g;->da:La/e/a/c/g;

    iget-object v0, v0, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {v14, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v0

    invoke-virtual {v14, v0, v12, v6, v3}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    :cond_f
    if-eqz v4, :cond_10

    iget-object v0, v15, La/e/a/c/g;->h:[Z

    aget-boolean v0, v0, v7

    if-eqz v0, :cond_10

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->L()Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, v15, La/e/a/c/g;->da:La/e/a/c/g;

    iget-object v0, v0, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {v14, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v0

    invoke-virtual {v14, v0, v10, v6, v3}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    :cond_10
    iput-boolean v6, v15, La/e/a/c/g;->q:Z

    iput-boolean v6, v15, La/e/a/c/g;->r:Z

    return-void

    :cond_11
    sget-object v0, La/e/a/d;->g:La/e/a/e;

    if-eqz v0, :cond_12

    iget-wide v1, v0, La/e/a/e;->s:J

    add-long v1, v1, v17

    iput-wide v1, v0, La/e/a/e;->s:J

    :cond_12
    iget-object v0, v15, La/e/a/c/g;->da:La/e/a/c/g;

    if-eqz v0, :cond_17

    invoke-direct {v15, v6}, La/e/a/c/g;->y(I)Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, v15, La/e/a/c/g;->da:La/e/a/c/g;

    check-cast v0, La/e/a/c/h;

    invoke-virtual {v0, v15, v6}, La/e/a/c/h;->a(La/e/a/c/g;I)V

    move v0, v7

    goto :goto_5

    :cond_13
    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->J()Z

    move-result v0

    :goto_5
    invoke-direct {v15, v7}, La/e/a/c/g;->y(I)Z

    move-result v1

    if-eqz v1, :cond_14

    iget-object v1, v15, La/e/a/c/g;->da:La/e/a/c/g;

    check-cast v1, La/e/a/c/h;

    invoke-virtual {v1, v15, v7}, La/e/a/c/h;->a(La/e/a/c/g;I)V

    move v1, v7

    goto :goto_6

    :cond_14
    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->L()Z

    move-result v1

    :goto_6
    if-nez v0, :cond_15

    if-eqz v5, :cond_15

    iget v2, v15, La/e/a/c/g;->va:I

    if-eq v2, v3, :cond_15

    iget-object v2, v15, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v2, v2, La/e/a/c/e;->f:La/e/a/c/e;

    if-nez v2, :cond_15

    iget-object v2, v15, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v2, v2, La/e/a/c/e;->f:La/e/a/c/e;

    if-nez v2, :cond_15

    iget-object v2, v15, La/e/a/c/g;->da:La/e/a/c/g;

    iget-object v2, v2, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {v14, v2}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v2

    invoke-virtual {v14, v2, v12, v6, v7}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    :cond_15
    if-nez v1, :cond_16

    if-eqz v4, :cond_16

    iget v2, v15, La/e/a/c/g;->va:I

    if-eq v2, v3, :cond_16

    iget-object v2, v15, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v2, v2, La/e/a/c/e;->f:La/e/a/c/e;

    if-nez v2, :cond_16

    iget-object v2, v15, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object v2, v2, La/e/a/c/e;->f:La/e/a/c/e;

    if-nez v2, :cond_16

    iget-object v2, v15, La/e/a/c/g;->V:La/e/a/c/e;

    if-nez v2, :cond_16

    iget-object v2, v15, La/e/a/c/g;->da:La/e/a/c/g;

    iget-object v2, v2, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {v14, v2}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v2

    invoke-virtual {v14, v2, v10, v6, v7}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    :cond_16
    move/from16 v28, v0

    move/from16 v29, v1

    goto :goto_7

    :cond_17
    move/from16 v28, v6

    move/from16 v29, v28

    :goto_7
    iget v0, v15, La/e/a/c/g;->ea:I

    iget v1, v15, La/e/a/c/g;->pa:I

    if-ge v0, v1, :cond_18

    move v0, v1

    :cond_18
    iget v1, v15, La/e/a/c/g;->fa:I

    iget v2, v15, La/e/a/c/g;->qa:I

    if-ge v1, v2, :cond_19

    move v1, v2

    :cond_19
    iget-object v2, v15, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object v2, v2, v6

    sget-object v8, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-eq v2, v8, :cond_1a

    move v2, v7

    goto :goto_8

    :cond_1a
    move v2, v6

    :goto_8
    iget-object v8, v15, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object v8, v8, v7

    sget-object v7, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-eq v8, v7, :cond_1b

    const/4 v7, 0x1

    goto :goto_9

    :cond_1b
    move v7, v6

    :goto_9
    iget v8, v15, La/e/a/c/g;->ha:I

    iput v8, v15, La/e/a/c/g;->I:I

    iget v8, v15, La/e/a/c/g;->ga:F

    iput v8, v15, La/e/a/c/g;->J:F

    iget v6, v15, La/e/a/c/g;->x:I

    iget v3, v15, La/e/a/c/g;->y:I

    const/16 v22, 0x0

    cmpl-float v8, v8, v22

    const/16 v22, 0x4

    move/from16 v23, v0

    if-lez v8, :cond_25

    iget v8, v15, La/e/a/c/g;->va:I

    const/16 v0, 0x8

    if-eq v8, v0, :cond_25

    iget-object v0, v15, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    const/4 v8, 0x0

    aget-object v0, v0, v8

    sget-object v8, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v0, v8, :cond_1c

    if-nez v6, :cond_1c

    const/4 v6, 0x3

    :cond_1c
    iget-object v0, v15, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    const/4 v8, 0x1

    aget-object v0, v0, v8

    sget-object v8, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v0, v8, :cond_1d

    if-nez v3, :cond_1d

    const/4 v3, 0x3

    :cond_1d
    iget-object v0, v15, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    move/from16 v25, v1

    const/4 v8, 0x0

    aget-object v1, v0, v8

    sget-object v8, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v1, v8, :cond_1e

    const/4 v1, 0x1

    aget-object v0, v0, v1

    if-ne v0, v8, :cond_1e

    const/4 v0, 0x3

    if-ne v6, v0, :cond_1f

    if-ne v3, v0, :cond_1f

    invoke-virtual {v15, v5, v4, v2, v7}, La/e/a/c/g;->a(ZZZZ)V

    goto/16 :goto_a

    :cond_1e
    const/4 v0, 0x3

    :cond_1f
    iget-object v1, v15, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    const/4 v2, 0x0

    aget-object v7, v1, v2

    sget-object v8, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v7, v8, :cond_21

    if-ne v6, v0, :cond_21

    move-object v0, v8

    iput v2, v15, La/e/a/c/g;->I:I

    iget v2, v15, La/e/a/c/g;->J:F

    iget v7, v15, La/e/a/c/g;->fa:I

    int-to-float v7, v7

    mul-float/2addr v2, v7

    float-to-int v2, v2

    const/4 v7, 0x1

    aget-object v1, v1, v7

    if-eq v1, v0, :cond_20

    move/from16 v23, v2

    move/from16 v31, v3

    move/from16 v30, v22

    goto :goto_c

    :cond_20
    move/from16 v23, v2

    move/from16 v31, v3

    move/from16 v30, v6

    move v8, v7

    move/from16 v32, v25

    goto :goto_e

    :cond_21
    const/4 v7, 0x1

    iget-object v0, v15, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object v0, v0, v7

    sget-object v1, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v0, v1, :cond_24

    const/4 v0, 0x3

    if-ne v3, v0, :cond_24

    iput v7, v15, La/e/a/c/g;->I:I

    iget v0, v15, La/e/a/c/g;->ha:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_22

    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, v15, La/e/a/c/g;->J:F

    div-float/2addr v0, v1

    iput v0, v15, La/e/a/c/g;->J:F

    :cond_22
    iget v0, v15, La/e/a/c/g;->J:F

    iget v1, v15, La/e/a/c/g;->ea:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v1, v0

    iget-object v0, v15, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    sget-object v2, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-eq v0, v2, :cond_23

    move/from16 v32, v1

    move/from16 v30, v6

    move/from16 v31, v22

    goto :goto_d

    :cond_23
    move/from16 v32, v1

    move/from16 v31, v3

    move/from16 v30, v6

    goto :goto_b

    :cond_24
    :goto_a
    move/from16 v31, v3

    move/from16 v30, v6

    move/from16 v32, v25

    :goto_b
    const/4 v8, 0x1

    goto :goto_e

    :cond_25
    move/from16 v25, v1

    move/from16 v31, v3

    move/from16 v30, v6

    :goto_c
    move/from16 v32, v25

    :goto_d
    const/4 v8, 0x0

    :goto_e
    iget-object v0, v15, La/e/a/c/g;->z:[I

    const/4 v1, 0x0

    aput v30, v0, v1

    const/4 v1, 0x1

    aput v31, v0, v1

    iput-boolean v8, v15, La/e/a/c/g;->i:Z

    if-eqz v8, :cond_27

    iget v0, v15, La/e/a/c/g;->I:I

    if-eqz v0, :cond_26

    const/4 v1, -0x1

    if-ne v0, v1, :cond_28

    goto :goto_f

    :cond_26
    const/4 v1, -0x1

    :goto_f
    const/16 v17, 0x1

    goto :goto_10

    :cond_27
    const/4 v1, -0x1

    :cond_28
    const/16 v17, 0x0

    :goto_10
    if-eqz v8, :cond_2a

    iget v0, v15, La/e/a/c/g;->I:I

    const/4 v2, 0x1

    if-eq v0, v2, :cond_29

    if-ne v0, v1, :cond_2a

    :cond_29
    const/16 v33, 0x1

    goto :goto_11

    :cond_2a
    const/16 v33, 0x0

    :goto_11
    iget-object v0, v15, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    sget-object v1, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v0, v1, :cond_2b

    instance-of v0, v15, La/e/a/c/h;

    if-eqz v0, :cond_2b

    const/16 v22, 0x1

    goto :goto_12

    :cond_2b
    const/16 v22, 0x0

    :goto_12
    if-eqz v22, :cond_2c

    const/16 v23, 0x0

    :cond_2c
    iget-object v0, v15, La/e/a/c/g;->Y:La/e/a/c/e;

    invoke-virtual {v0}, La/e/a/c/e;->l()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/lit8 v34, v0, 0x1

    iget-object v0, v15, La/e/a/c/g;->ba:[Z

    const/4 v2, 0x0

    aget-boolean v27, v0, v2

    aget-boolean v35, v0, v1

    iget v0, v15, La/e/a/c/g;->u:I

    const/16 v36, 0x0

    const/4 v7, 0x2

    if-eq v0, v7, :cond_32

    iget-boolean v0, v15, La/e/a/c/g;->q:Z

    if-nez v0, :cond_32

    if-eqz p2, :cond_2e

    iget-object v0, v15, La/e/a/c/g;->f:La/e/a/c/a/m;

    if-eqz v0, :cond_2e

    iget-object v1, v0, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget-boolean v2, v1, La/e/a/c/a/f;->j:Z

    if-eqz v2, :cond_2e

    iget-object v0, v0, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget-boolean v0, v0, La/e/a/c/a/f;->j:Z

    if-nez v0, :cond_2d

    goto :goto_13

    :cond_2d
    if-eqz p2, :cond_32

    iget v0, v1, La/e/a/c/a/f;->g:I

    invoke-virtual {v14, v13, v0}, La/e/a/d;->a(La/e/a/j;I)V

    iget-object v0, v15, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v0, v0, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget v0, v0, La/e/a/c/a/f;->g:I

    invoke-virtual {v14, v12, v0}, La/e/a/d;->a(La/e/a/j;I)V

    iget-object v0, v15, La/e/a/c/g;->da:La/e/a/c/g;

    if-eqz v0, :cond_32

    if-eqz v5, :cond_32

    iget-object v0, v15, La/e/a/c/g;->h:[Z

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    if-eqz v0, :cond_32

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->J()Z

    move-result v0

    if-nez v0, :cond_32

    iget-object v0, v15, La/e/a/c/g;->da:La/e/a/c/g;

    iget-object v0, v0, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {v14, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v0

    const/16 v3, 0x8

    invoke-virtual {v14, v0, v12, v1, v3}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    goto/16 :goto_17

    :cond_2e
    :goto_13
    const/16 v3, 0x8

    iget-object v0, v15, La/e/a/c/g;->da:La/e/a/c/g;

    if-eqz v0, :cond_2f

    iget-object v0, v0, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {v14, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v0

    move-object/from16 v16, v0

    goto :goto_14

    :cond_2f
    move-object/from16 v16, v36

    :goto_14
    iget-object v0, v15, La/e/a/c/g;->da:La/e/a/c/g;

    if-eqz v0, :cond_30

    iget-object v0, v0, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {v14, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v0

    move-object v6, v0

    goto :goto_15

    :cond_30
    move-object/from16 v6, v36

    :goto_15
    iget-object v0, v15, La/e/a/c/g;->h:[Z

    const/16 v20, 0x0

    aget-boolean v21, v0, v20

    iget-object v0, v15, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object v37, v0, v20

    iget-object v1, v15, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v2, v15, La/e/a/c/g;->T:La/e/a/c/e;

    move-object/from16 v39, v2

    iget v2, v15, La/e/a/c/g;->ia:I

    move/from16 v40, v2

    iget v2, v15, La/e/a/c/g;->pa:I

    iget-object v3, v15, La/e/a/c/g;->K:[I

    aget v42, v3, v20

    iget v3, v15, La/e/a/c/g;->ra:F

    const/16 v18, 0x1

    aget-object v0, v0, v18

    sget-object v7, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v0, v7, :cond_31

    move/from16 v44, v18

    goto :goto_16

    :cond_31
    move/from16 v44, v20

    :goto_16
    iget v0, v15, La/e/a/c/g;->A:I

    move/from16 v24, v0

    iget v0, v15, La/e/a/c/g;->B:I

    move/from16 v25, v0

    iget v0, v15, La/e/a/c/g;->C:F

    move/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v45, v1

    move-object/from16 v1, p1

    move-object/from16 v19, v39

    move/from16 v38, v40

    const/4 v7, 0x5

    move/from16 v39, v2

    const/4 v2, 0x1

    move/from16 v40, v3

    move v3, v5

    move/from16 v41, v4

    move/from16 v46, v5

    move/from16 v5, v21

    const/16 v18, 0x2

    move-object/from16 v7, v16

    move/from16 v43, v8

    move-object/from16 v8, v37

    move-object/from16 v47, v9

    move/from16 v9, v22

    move-object/from16 v48, v10

    move-object/from16 v10, v45

    move-object/from16 v49, v11

    move-object/from16 v11, v19

    move-object/from16 v37, v12

    move/from16 v12, v38

    move-object/from16 v38, v13

    move/from16 v13, v23

    move/from16 v14, v39

    move/from16 v15, v42

    move/from16 v16, v40

    move/from16 v18, v44

    move/from16 v19, v28

    move/from16 v20, v29

    move/from16 v21, v27

    move/from16 v22, v30

    move/from16 v23, v31

    move/from16 v27, v34

    invoke-direct/range {v0 .. v27}, La/e/a/c/g;->a(La/e/a/d;ZZZZLa/e/a/j;La/e/a/j;La/e/a/c/g$a;ZLa/e/a/c/e;La/e/a/c/e;IIIIFZZZZZIIIIFZ)V

    goto :goto_18

    :cond_32
    :goto_17
    move/from16 v41, v4

    move/from16 v46, v5

    move/from16 v43, v8

    move-object/from16 v47, v9

    move-object/from16 v48, v10

    move-object/from16 v49, v11

    move-object/from16 v37, v12

    move-object/from16 v38, v13

    :goto_18
    if-eqz p2, :cond_35

    move-object/from16 v15, p0

    iget-object v0, v15, La/e/a/c/g;->g:La/e/a/c/a/p;

    if-eqz v0, :cond_36

    iget-object v1, v0, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget-boolean v2, v1, La/e/a/c/a/f;->j:Z

    if-eqz v2, :cond_36

    iget-object v0, v0, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget-boolean v0, v0, La/e/a/c/a/f;->j:Z

    if-eqz v0, :cond_36

    iget v0, v1, La/e/a/c/a/f;->g:I

    move-object/from16 v14, p1

    move-object/from16 v13, v49

    invoke-virtual {v14, v13, v0}, La/e/a/d;->a(La/e/a/j;I)V

    iget-object v0, v15, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v0, v0, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget v0, v0, La/e/a/c/a/f;->g:I

    move-object/from16 v12, v48

    invoke-virtual {v14, v12, v0}, La/e/a/d;->a(La/e/a/j;I)V

    iget-object v0, v15, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v0, v0, La/e/a/c/a/p;->k:La/e/a/c/a/f;

    iget v0, v0, La/e/a/c/a/f;->g:I

    move-object/from16 v1, v47

    invoke-virtual {v14, v1, v0}, La/e/a/d;->a(La/e/a/j;I)V

    iget-object v0, v15, La/e/a/c/g;->da:La/e/a/c/g;

    if-eqz v0, :cond_34

    if-nez v29, :cond_34

    if-eqz v41, :cond_34

    iget-object v2, v15, La/e/a/c/g;->h:[Z

    const/4 v11, 0x1

    aget-boolean v2, v2, v11

    if-eqz v2, :cond_33

    iget-object v0, v0, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {v14, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v0

    const/16 v2, 0x8

    const/4 v10, 0x0

    invoke-virtual {v14, v0, v12, v10, v2}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    goto :goto_19

    :cond_33
    const/16 v2, 0x8

    const/4 v10, 0x0

    goto :goto_19

    :cond_34
    const/16 v2, 0x8

    const/4 v10, 0x0

    const/4 v11, 0x1

    :goto_19
    move v6, v10

    goto :goto_1a

    :cond_35
    move-object/from16 v15, p0

    :cond_36
    move-object/from16 v14, p1

    move-object/from16 v1, v47

    move-object/from16 v12, v48

    move-object/from16 v13, v49

    const/16 v2, 0x8

    const/4 v10, 0x0

    const/4 v11, 0x1

    move v6, v11

    :goto_1a
    iget v0, v15, La/e/a/c/g;->v:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_37

    move v6, v10

    :cond_37
    if-eqz v6, :cond_42

    iget-boolean v0, v15, La/e/a/c/g;->r:Z

    if-nez v0, :cond_42

    iget-object v0, v15, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object v0, v0, v11

    sget-object v3, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v0, v3, :cond_38

    instance-of v0, v15, La/e/a/c/h;

    if-eqz v0, :cond_38

    move v9, v11

    goto :goto_1b

    :cond_38
    move v9, v10

    :goto_1b
    if-eqz v9, :cond_39

    move/from16 v32, v10

    :cond_39
    iget-object v0, v15, La/e/a/c/g;->da:La/e/a/c/g;

    if-eqz v0, :cond_3a

    iget-object v0, v0, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {v14, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v0

    move-object v7, v0

    goto :goto_1c

    :cond_3a
    move-object/from16 v7, v36

    :goto_1c
    iget-object v0, v15, La/e/a/c/g;->da:La/e/a/c/g;

    if-eqz v0, :cond_3b

    iget-object v0, v0, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {v14, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v0

    move-object v6, v0

    goto :goto_1d

    :cond_3b
    move-object/from16 v6, v36

    :goto_1d
    iget v0, v15, La/e/a/c/g;->oa:I

    if-gtz v0, :cond_3c

    iget v0, v15, La/e/a/c/g;->va:I

    if-ne v0, v2, :cond_40

    :cond_3c
    iget-object v0, v15, La/e/a/c/g;->V:La/e/a/c/e;

    iget-object v3, v0, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v3, :cond_3e

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->e()I

    move-result v0

    invoke-virtual {v14, v1, v13, v0, v2}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    iget-object v0, v15, La/e/a/c/g;->V:La/e/a/c/e;

    iget-object v0, v0, La/e/a/c/e;->f:La/e/a/c/e;

    invoke-virtual {v14, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v0

    iget-object v3, v15, La/e/a/c/g;->V:La/e/a/c/e;

    invoke-virtual {v3}, La/e/a/c/e;->c()I

    move-result v3

    invoke-virtual {v14, v1, v0, v3, v2}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    if-eqz v41, :cond_3d

    iget-object v0, v15, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {v14, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v14, v7, v0, v10, v1}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    :cond_3d
    move/from16 v27, v10

    goto :goto_1f

    :cond_3e
    iget v3, v15, La/e/a/c/g;->va:I

    if-ne v3, v2, :cond_3f

    invoke-virtual {v0}, La/e/a/c/e;->c()I

    move-result v0

    invoke-virtual {v14, v1, v13, v0, v2}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    goto :goto_1e

    :cond_3f
    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->e()I

    move-result v0

    invoke-virtual {v14, v1, v13, v0, v2}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    :cond_40
    :goto_1e
    move/from16 v27, v34

    :goto_1f
    iget-object v0, v15, La/e/a/c/g;->h:[Z

    aget-boolean v5, v0, v11

    iget-object v0, v15, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object v8, v0, v11

    iget-object v4, v15, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v3, v15, La/e/a/c/g;->U:La/e/a/c/e;

    iget v1, v15, La/e/a/c/g;->ja:I

    iget v2, v15, La/e/a/c/g;->qa:I

    iget-object v10, v15, La/e/a/c/g;->K:[I

    aget v16, v10, v11

    iget v10, v15, La/e/a/c/g;->sa:F

    const/16 v17, 0x0

    aget-object v0, v0, v17

    sget-object v11, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v0, v11, :cond_41

    const/16 v18, 0x1

    goto :goto_20

    :cond_41
    move/from16 v18, v17

    :goto_20
    iget v0, v15, La/e/a/c/g;->D:I

    move/from16 v24, v0

    iget v0, v15, La/e/a/c/g;->E:I

    move/from16 v25, v0

    iget v0, v15, La/e/a/c/g;->F:F

    move/from16 v26, v0

    move-object/from16 v0, p0

    move/from16 v19, v1

    move-object/from16 v1, p1

    move/from16 v20, v2

    const/4 v2, 0x0

    move-object v11, v3

    move/from16 v3, v41

    move-object/from16 v21, v4

    move/from16 v4, v46

    move/from16 v17, v10

    move-object/from16 v10, v21

    move-object/from16 v34, v12

    move/from16 v12, v19

    move-object/from16 v36, v13

    move/from16 v13, v32

    move/from16 v14, v20

    move/from16 v15, v16

    move/from16 v16, v17

    move/from16 v17, v33

    move/from16 v19, v29

    move/from16 v20, v28

    move/from16 v21, v35

    move/from16 v22, v31

    move/from16 v23, v30

    invoke-direct/range {v0 .. v27}, La/e/a/c/g;->a(La/e/a/d;ZZZZLa/e/a/j;La/e/a/j;La/e/a/c/g$a;ZLa/e/a/c/e;La/e/a/c/e;IIIIFZZZZZIIIIFZ)V

    goto :goto_21

    :cond_42
    move-object/from16 v34, v12

    move-object/from16 v36, v13

    :goto_21
    if-eqz v43, :cond_44

    const/16 v6, 0x8

    move-object/from16 v7, p0

    iget v0, v7, La/e/a/c/g;->I:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_43

    iget v5, v7, La/e/a/c/g;->J:F

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    move-object/from16 v2, v36

    move-object/from16 v3, v37

    move-object/from16 v4, v38

    invoke-virtual/range {v0 .. v6}, La/e/a/d;->a(La/e/a/j;La/e/a/j;La/e/a/j;La/e/a/j;FI)V

    goto :goto_22

    :cond_43
    iget v5, v7, La/e/a/c/g;->J:F

    const/16 v6, 0x8

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    move-object/from16 v2, v38

    move-object/from16 v3, v34

    move-object/from16 v4, v36

    invoke-virtual/range {v0 .. v6}, La/e/a/d;->a(La/e/a/j;La/e/a/j;La/e/a/j;La/e/a/j;FI)V

    goto :goto_22

    :cond_44
    move-object/from16 v7, p0

    :goto_22
    iget-object v0, v7, La/e/a/c/g;->Y:La/e/a/c/e;

    invoke-virtual {v0}, La/e/a/c/e;->l()Z

    move-result v0

    if-eqz v0, :cond_45

    iget-object v0, v7, La/e/a/c/g;->Y:La/e/a/c/e;

    invoke-virtual {v0}, La/e/a/c/e;->g()La/e/a/c/e;

    move-result-object v0

    invoke-virtual {v0}, La/e/a/c/e;->e()La/e/a/c/g;

    move-result-object v0

    iget v1, v7, La/e/a/c/g;->L:F

    const/high16 v2, 0x42b40000    # 90.0f

    add-float/2addr v1, v2

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v1

    double-to-float v1, v1

    iget-object v2, v7, La/e/a/c/g;->Y:La/e/a/c/e;

    invoke-virtual {v2}, La/e/a/c/e;->c()I

    move-result v2

    move-object/from16 v3, p1

    invoke-virtual {v3, v7, v0, v1, v2}, La/e/a/d;->a(La/e/a/c/g;La/e/a/c/g;FI)V

    :cond_45
    const/4 v0, 0x0

    iput-boolean v0, v7, La/e/a/c/g;->q:Z

    iput-boolean v0, v7, La/e/a/c/g;->r:Z

    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, La/e/a/c/g;->ta:Ljava/lang/Object;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, La/e/a/c/g;->wa:Ljava/lang/String;

    return-void
.end method

.method public a(Ljava/lang/StringBuilder;)V
    .locals 12

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, La/e/a/c/g;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":{\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "    actualWidth:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, La/e/a/c/g;->ea:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "    actualHeight:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, La/e/a/c/g;->fa:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "    actualLeft:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, La/e/a/c/g;->ia:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "    actualTop:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, La/e/a/c/g;->ja:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, La/e/a/c/g;->R:La/e/a/c/e;

    const-string v1, "left"

    invoke-direct {p0, p1, v1, v0}, La/e/a/c/g;->a(Ljava/lang/StringBuilder;Ljava/lang/String;La/e/a/c/e;)V

    iget-object v0, p0, La/e/a/c/g;->S:La/e/a/c/e;

    const-string v1, "top"

    invoke-direct {p0, p1, v1, v0}, La/e/a/c/g;->a(Ljava/lang/StringBuilder;Ljava/lang/String;La/e/a/c/e;)V

    iget-object v0, p0, La/e/a/c/g;->T:La/e/a/c/e;

    const-string v1, "right"

    invoke-direct {p0, p1, v1, v0}, La/e/a/c/g;->a(Ljava/lang/StringBuilder;Ljava/lang/String;La/e/a/c/e;)V

    iget-object v0, p0, La/e/a/c/g;->U:La/e/a/c/e;

    const-string v1, "bottom"

    invoke-direct {p0, p1, v1, v0}, La/e/a/c/g;->a(Ljava/lang/StringBuilder;Ljava/lang/String;La/e/a/c/e;)V

    iget-object v0, p0, La/e/a/c/g;->V:La/e/a/c/e;

    const-string v1, "baseline"

    invoke-direct {p0, p1, v1, v0}, La/e/a/c/g;->a(Ljava/lang/StringBuilder;Ljava/lang/String;La/e/a/c/e;)V

    iget-object v0, p0, La/e/a/c/g;->W:La/e/a/c/e;

    const-string v1, "centerX"

    invoke-direct {p0, p1, v1, v0}, La/e/a/c/g;->a(Ljava/lang/StringBuilder;Ljava/lang/String;La/e/a/c/e;)V

    iget-object v0, p0, La/e/a/c/g;->X:La/e/a/c/e;

    const-string v1, "centerY"

    invoke-direct {p0, p1, v1, v0}, La/e/a/c/g;->a(Ljava/lang/StringBuilder;Ljava/lang/String;La/e/a/c/e;)V

    iget v3, p0, La/e/a/c/g;->ea:I

    iget v4, p0, La/e/a/c/g;->pa:I

    iget-object v0, p0, La/e/a/c/g;->K:[I

    const/4 v11, 0x0

    aget v5, v0, v11

    iget v6, p0, La/e/a/c/g;->m:I

    iget v7, p0, La/e/a/c/g;->A:I

    iget v8, p0, La/e/a/c/g;->x:I

    iget v9, p0, La/e/a/c/g;->C:F

    iget-object v0, p0, La/e/a/c/g;->Na:[F

    aget v10, v0, v11

    const-string v2, "    width"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v10}, La/e/a/c/g;->a(Ljava/lang/StringBuilder;Ljava/lang/String;IIIIIIFF)V

    iget v3, p0, La/e/a/c/g;->fa:I

    iget v4, p0, La/e/a/c/g;->qa:I

    iget-object v0, p0, La/e/a/c/g;->K:[I

    const/4 v1, 0x1

    aget v5, v0, v1

    iget v6, p0, La/e/a/c/g;->n:I

    iget v7, p0, La/e/a/c/g;->D:I

    iget v8, p0, La/e/a/c/g;->y:I

    iget v9, p0, La/e/a/c/g;->F:F

    iget-object v0, p0, La/e/a/c/g;->Na:[F

    aget v10, v0, v1

    const-string v2, "    height"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v10}, La/e/a/c/g;->a(Ljava/lang/StringBuilder;Ljava/lang/String;IIIIIIFF)V

    iget v0, p0, La/e/a/c/g;->ga:F

    iget v1, p0, La/e/a/c/g;->ha:I

    const-string v2, "    dimensionRatio"

    invoke-direct {p0, p1, v2, v0, v1}, La/e/a/c/g;->a(Ljava/lang/StringBuilder;Ljava/lang/String;FI)V

    iget v0, p0, La/e/a/c/g;->ra:F

    sget v1, La/e/a/c/g;->a:F

    const-string v2, "    horizontalBias"

    invoke-direct {p0, p1, v2, v0, v1}, La/e/a/c/g;->a(Ljava/lang/StringBuilder;Ljava/lang/String;FF)V

    iget v0, p0, La/e/a/c/g;->sa:F

    sget v1, La/e/a/c/g;->a:F

    const-string v2, "    verticalBias"

    invoke-direct {p0, p1, v2, v0, v1}, La/e/a/c/g;->a(Ljava/lang/StringBuilder;Ljava/lang/String;FF)V

    iget v0, p0, La/e/a/c/g;->Ja:I

    const-string v1, "    horizontalChainStyle"

    invoke-direct {p0, p1, v1, v0, v11}, La/e/a/c/g;->a(Ljava/lang/StringBuilder;Ljava/lang/String;II)V

    iget v0, p0, La/e/a/c/g;->Ka:I

    const-string v1, "    verticalChainStyle"

    invoke-direct {p0, p1, v1, v0, v11}, La/e/a/c/g;->a(Ljava/lang/StringBuilder;Ljava/lang/String;II)V

    const-string v0, "  }"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, La/e/a/c/g;->M:Z

    return-void
.end method

.method public a(ZZ)V
    .locals 7

    iget-object v0, p0, La/e/a/c/g;->f:La/e/a/c/a/m;

    invoke-virtual {v0}, La/e/a/c/a/s;->e()Z

    move-result v0

    and-int/2addr p1, v0

    iget-object v0, p0, La/e/a/c/g;->g:La/e/a/c/a/p;

    invoke-virtual {v0}, La/e/a/c/a/s;->e()Z

    move-result v0

    and-int/2addr p2, v0

    iget-object v0, p0, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v1, v0, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget v1, v1, La/e/a/c/a/f;->g:I

    iget-object v2, p0, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v3, v2, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget v3, v3, La/e/a/c/a/f;->g:I

    iget-object v0, v0, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget v0, v0, La/e/a/c/a/f;->g:I

    iget-object v2, v2, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget v2, v2, La/e/a/c/a/f;->g:I

    sub-int v4, v0, v1

    sub-int v5, v2, v3

    const/4 v6, 0x0

    if-ltz v4, :cond_0

    if-ltz v5, :cond_0

    const/high16 v4, -0x80000000

    if-eq v1, v4, :cond_0

    const v5, 0x7fffffff

    if-eq v1, v5, :cond_0

    if-eq v3, v4, :cond_0

    if-eq v3, v5, :cond_0

    if-eq v0, v4, :cond_0

    if-eq v0, v5, :cond_0

    if-eq v2, v4, :cond_0

    if-ne v2, v5, :cond_1

    :cond_0
    move v0, v6

    move v1, v0

    move v2, v1

    move v3, v2

    :cond_1
    sub-int/2addr v0, v1

    sub-int/2addr v2, v3

    if-eqz p1, :cond_2

    iput v1, p0, La/e/a/c/g;->ia:I

    :cond_2
    if-eqz p2, :cond_3

    iput v3, p0, La/e/a/c/g;->ja:I

    :cond_3
    iget v1, p0, La/e/a/c/g;->va:I

    const/16 v3, 0x8

    if-ne v1, v3, :cond_4

    iput v6, p0, La/e/a/c/g;->ea:I

    iput v6, p0, La/e/a/c/g;->fa:I

    return-void

    :cond_4
    if-eqz p1, :cond_6

    iget-object p1, p0, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object p1, p1, v6

    sget-object v1, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    if-ne p1, v1, :cond_5

    iget p1, p0, La/e/a/c/g;->ea:I

    if-ge v0, p1, :cond_5

    goto :goto_0

    :cond_5
    move p1, v0

    :goto_0
    iput p1, p0, La/e/a/c/g;->ea:I

    iget p1, p0, La/e/a/c/g;->ea:I

    iget v0, p0, La/e/a/c/g;->pa:I

    if-ge p1, v0, :cond_6

    iput v0, p0, La/e/a/c/g;->ea:I

    :cond_6
    if-eqz p2, :cond_8

    iget-object p1, p0, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    const/4 p2, 0x1

    aget-object p1, p1, p2

    sget-object p2, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    if-ne p1, p2, :cond_7

    iget p1, p0, La/e/a/c/g;->fa:I

    if-ge v2, p1, :cond_7

    goto :goto_1

    :cond_7
    move p1, v2

    :goto_1
    iput p1, p0, La/e/a/c/g;->fa:I

    iget p1, p0, La/e/a/c/g;->fa:I

    iget p2, p0, La/e/a/c/g;->qa:I

    if-ge p1, p2, :cond_8

    iput p2, p0, La/e/a/c/g;->fa:I

    :cond_8
    return-void
.end method

.method public a(ZZZZ)V
    .locals 3

    iget p1, p0, La/e/a/c/g;->I:I

    const/high16 p2, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x1

    if-ne p1, v1, :cond_1

    if-eqz p3, :cond_0

    if-nez p4, :cond_0

    iput v0, p0, La/e/a/c/g;->I:I

    goto :goto_0

    :cond_0
    if-nez p3, :cond_1

    if-eqz p4, :cond_1

    iput v2, p0, La/e/a/c/g;->I:I

    iget p1, p0, La/e/a/c/g;->ha:I

    if-ne p1, v1, :cond_1

    iget p1, p0, La/e/a/c/g;->J:F

    div-float p1, p2, p1

    iput p1, p0, La/e/a/c/g;->J:F

    :cond_1
    :goto_0
    iget p1, p0, La/e/a/c/g;->I:I

    if-nez p1, :cond_3

    iget-object p1, p0, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {p1}, La/e/a/c/e;->l()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {p1}, La/e/a/c/e;->l()Z

    move-result p1

    if-nez p1, :cond_3

    :cond_2
    iput v2, p0, La/e/a/c/g;->I:I

    goto :goto_1

    :cond_3
    iget p1, p0, La/e/a/c/g;->I:I

    if-ne p1, v2, :cond_5

    iget-object p1, p0, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {p1}, La/e/a/c/e;->l()Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {p1}, La/e/a/c/e;->l()Z

    move-result p1

    if-nez p1, :cond_5

    :cond_4
    iput v0, p0, La/e/a/c/g;->I:I

    :cond_5
    :goto_1
    iget p1, p0, La/e/a/c/g;->I:I

    if-ne p1, v1, :cond_8

    iget-object p1, p0, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {p1}, La/e/a/c/e;->l()Z

    move-result p1

    if-eqz p1, :cond_6

    iget-object p1, p0, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {p1}, La/e/a/c/e;->l()Z

    move-result p1

    if-eqz p1, :cond_6

    iget-object p1, p0, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {p1}, La/e/a/c/e;->l()Z

    move-result p1

    if-eqz p1, :cond_6

    iget-object p1, p0, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {p1}, La/e/a/c/e;->l()Z

    move-result p1

    if-nez p1, :cond_8

    :cond_6
    iget-object p1, p0, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {p1}, La/e/a/c/e;->l()Z

    move-result p1

    if-eqz p1, :cond_7

    iget-object p1, p0, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {p1}, La/e/a/c/e;->l()Z

    move-result p1

    if-eqz p1, :cond_7

    iput v0, p0, La/e/a/c/g;->I:I

    goto :goto_2

    :cond_7
    iget-object p1, p0, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {p1}, La/e/a/c/e;->l()Z

    move-result p1

    if-eqz p1, :cond_8

    iget-object p1, p0, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {p1}, La/e/a/c/e;->l()Z

    move-result p1

    if-eqz p1, :cond_8

    iget p1, p0, La/e/a/c/g;->J:F

    div-float p1, p2, p1

    iput p1, p0, La/e/a/c/g;->J:F

    iput v2, p0, La/e/a/c/g;->I:I

    :cond_8
    :goto_2
    iget p1, p0, La/e/a/c/g;->I:I

    if-ne p1, v1, :cond_a

    iget p1, p0, La/e/a/c/g;->A:I

    if-lez p1, :cond_9

    iget p1, p0, La/e/a/c/g;->D:I

    if-nez p1, :cond_9

    iput v0, p0, La/e/a/c/g;->I:I

    goto :goto_3

    :cond_9
    iget p1, p0, La/e/a/c/g;->A:I

    if-nez p1, :cond_a

    iget p1, p0, La/e/a/c/g;->D:I

    if-lez p1, :cond_a

    iget p1, p0, La/e/a/c/g;->J:F

    div-float/2addr p2, p1

    iput p2, p0, La/e/a/c/g;->J:F

    iput v2, p0, La/e/a/c/g;->I:I

    :cond_a
    :goto_3
    return-void
.end method

.method public a(II)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_1

    iget-object p1, p0, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object p1, p1, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, La/e/a/c/e;->k()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object p1, p1, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, La/e/a/c/e;->k()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object p1, p1, La/e/a/c/e;->f:La/e/a/c/e;

    invoke-virtual {p1}, La/e/a/c/e;->b()I

    move-result p1

    iget-object v2, p0, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {v2}, La/e/a/c/e;->c()I

    move-result v2

    sub-int/2addr p1, v2

    iget-object v2, p0, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v2, v2, La/e/a/c/e;->f:La/e/a/c/e;

    invoke-virtual {v2}, La/e/a/c/e;->b()I

    move-result v2

    iget-object v3, p0, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {v3}, La/e/a/c/e;->c()I

    move-result v3

    add-int/2addr v2, v3

    sub-int/2addr p1, v2

    if-lt p1, p2, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    iget-object p1, p0, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object p1, p1, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, La/e/a/c/e;->k()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object p1, p1, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, La/e/a/c/e;->k()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object p1, p1, La/e/a/c/e;->f:La/e/a/c/e;

    invoke-virtual {p1}, La/e/a/c/e;->b()I

    move-result p1

    iget-object v2, p0, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {v2}, La/e/a/c/e;->c()I

    move-result v2

    sub-int/2addr p1, v2

    iget-object v2, p0, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v2, v2, La/e/a/c/e;->f:La/e/a/c/e;

    invoke-virtual {v2}, La/e/a/c/e;->b()I

    move-result v2

    iget-object v3, p0, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {v3}, La/e/a/c/e;->c()I

    move-result v3

    add-int/2addr v2, v3

    sub-int/2addr p1, v2

    if-lt p1, p2, :cond_2

    goto :goto_1

    :cond_2
    move v0, v1

    :goto_1
    return v0

    :cond_3
    return v1
.end method

.method public b(I)La/e/a/c/g$a;
    .locals 1

    if-nez p1, :cond_0

    invoke-virtual {p0}, La/e/a/c/g;->n()La/e/a/c/g$a;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    invoke-virtual {p0}, La/e/a/c/g;->z()La/e/a/c/g$a;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public b(F)V
    .locals 2

    iget-object v0, p0, La/e/a/c/g;->Na:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    return-void
.end method

.method public b(II)V
    .locals 1

    iget-boolean v0, p0, La/e/a/c/g;->q:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {v0, p1}, La/e/a/c/e;->a(I)V

    iget-object v0, p0, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {v0, p2}, La/e/a/c/e;->a(I)V

    iput p1, p0, La/e/a/c/g;->ia:I

    sub-int/2addr p2, p1

    iput p2, p0, La/e/a/c/g;->ea:I

    const/4 p1, 0x1

    iput-boolean p1, p0, La/e/a/c/g;->q:Z

    return-void
.end method

.method public b(IIIF)V
    .locals 0

    iput p1, p0, La/e/a/c/g;->y:I

    iput p2, p0, La/e/a/c/g;->D:I

    const p1, 0x7fffffff

    if-ne p3, p1, :cond_0

    const/4 p3, 0x0

    :cond_0
    iput p3, p0, La/e/a/c/g;->E:I

    iput p4, p0, La/e/a/c/g;->F:F

    const/4 p1, 0x0

    cmpl-float p1, p4, p1

    if-lez p1, :cond_1

    const/high16 p1, 0x3f800000    # 1.0f

    cmpg-float p1, p4, p1

    if-gez p1, :cond_1

    iget p1, p0, La/e/a/c/g;->y:I

    if-nez p1, :cond_1

    const/4 p1, 0x2

    iput p1, p0, La/e/a/c/g;->y:I

    :cond_1
    return-void
.end method

.method public b(La/e/a/c/g$a;)V
    .locals 2

    iget-object v0, p0, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    const/4 v1, 0x1

    aput-object p1, v0, v1

    return-void
.end method

.method public b(La/e/a/c/g;)V
    .locals 0

    iput-object p1, p0, La/e/a/c/g;->da:La/e/a/c/g;

    return-void
.end method

.method public b(La/e/a/d;Z)V
    .locals 6

    iget-object v0, p0, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {p1, v0}, La/e/a/d;->b(Ljava/lang/Object;)I

    move-result v0

    iget-object v1, p0, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {p1, v1}, La/e/a/d;->b(Ljava/lang/Object;)I

    move-result v1

    iget-object v2, p0, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {p1, v2}, La/e/a/d;->b(Ljava/lang/Object;)I

    move-result v2

    iget-object v3, p0, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {p1, v3}, La/e/a/d;->b(Ljava/lang/Object;)I

    move-result p1

    if-eqz p2, :cond_0

    iget-object v3, p0, La/e/a/c/g;->f:La/e/a/c/a/m;

    if-eqz v3, :cond_0

    iget-object v4, v3, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget-boolean v5, v4, La/e/a/c/a/f;->j:Z

    if-eqz v5, :cond_0

    iget-object v3, v3, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget-boolean v5, v3, La/e/a/c/a/f;->j:Z

    if-eqz v5, :cond_0

    iget v0, v4, La/e/a/c/a/f;->g:I

    iget v2, v3, La/e/a/c/a/f;->g:I

    :cond_0
    if-eqz p2, :cond_1

    iget-object p2, p0, La/e/a/c/g;->g:La/e/a/c/a/p;

    if-eqz p2, :cond_1

    iget-object v3, p2, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget-boolean v4, v3, La/e/a/c/a/f;->j:Z

    if-eqz v4, :cond_1

    iget-object p2, p2, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget-boolean v4, p2, La/e/a/c/a/f;->j:Z

    if-eqz v4, :cond_1

    iget v1, v3, La/e/a/c/a/f;->g:I

    iget p1, p2, La/e/a/c/a/f;->g:I

    :cond_1
    sub-int p2, v2, v0

    sub-int v3, p1, v1

    const/4 v4, 0x0

    if-ltz p2, :cond_2

    if-ltz v3, :cond_2

    const/high16 p2, -0x80000000

    if-eq v0, p2, :cond_2

    const v3, 0x7fffffff

    if-eq v0, v3, :cond_2

    if-eq v1, p2, :cond_2

    if-eq v1, v3, :cond_2

    if-eq v2, p2, :cond_2

    if-eq v2, v3, :cond_2

    if-eq p1, p2, :cond_2

    if-ne p1, v3, :cond_3

    :cond_2
    move p1, v4

    move v0, p1

    move v1, v0

    move v2, v1

    :cond_3
    invoke-virtual {p0, v0, v1, v2, p1}, La/e/a/c/g;->a(IIII)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 8

    const/4 v0, 0x0

    if-eqz p1, :cond_8

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_2

    :cond_0
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x2c

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-lez v3, :cond_3

    add-int/lit8 v6, v2, -0x1

    if-ge v3, v6, :cond_3

    invoke-virtual {p1, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    const-string v7, "W"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    move v1, v4

    goto :goto_0

    :cond_1
    const-string v4, "H"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v1, v5

    :cond_2
    :goto_0
    add-int/lit8 v4, v3, 0x1

    :cond_3
    const/16 v3, 0x3a

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-ltz v3, :cond_5

    sub-int/2addr v2, v5

    if-ge v3, v2, :cond_5

    invoke-virtual {p1, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    add-int/2addr v3, v5

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_6

    :try_start_0
    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result p1

    cmpl-float v3, v2, v0

    if-lez v3, :cond_6

    cmpl-float v3, p1, v0

    if-lez v3, :cond_6

    if-ne v1, v5, :cond_4

    div-float/2addr p1, v2

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    goto :goto_1

    :cond_4
    div-float/2addr v2, p1

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result p1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :cond_5
    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_6

    :try_start_1
    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result p1
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    :cond_6
    move p1, v0

    :goto_1
    cmpl-float v0, p1, v0

    if-lez v0, :cond_7

    iput p1, p0, La/e/a/c/g;->ga:F

    iput v1, p0, La/e/a/c/g;->ha:I

    :cond_7
    return-void

    :cond_8
    :goto_2
    iput v0, p0, La/e/a/c/g;->ga:F

    return-void
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, La/e/a/c/g;->N:Z

    return-void
.end method

.method b()Z
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    instance-of v0, p0, La/e/a/c/k;

    goto/32 :goto_b

    nop

    :goto_1
    if-eqz v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_0

    nop

    :goto_2
    instance-of v0, p0, La/e/a/c/p;

    goto/32 :goto_1

    nop

    :goto_3
    return v0

    :goto_4
    goto :goto_7

    :goto_5
    goto/32 :goto_a

    nop

    :goto_6
    goto :goto_9

    :goto_7
    goto/32 :goto_8

    nop

    :goto_8
    const/4 v0, 0x1

    :goto_9
    goto/32 :goto_3

    nop

    :goto_a
    const/4 v0, 0x0

    goto/32 :goto_6

    nop

    :goto_b
    if-nez v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_4

    nop
.end method

.method public c(I)I
    .locals 1

    if-nez p1, :cond_0

    invoke-virtual {p0}, La/e/a/c/g;->C()I

    move-result p1

    return p1

    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    invoke-virtual {p0}, La/e/a/c/g;->k()I

    move-result p1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public c(F)V
    .locals 0

    iput p1, p0, La/e/a/c/g;->sa:F

    return-void
.end method

.method public c(II)V
    .locals 1

    iget-boolean v0, p0, La/e/a/c/g;->r:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {v0, p1}, La/e/a/c/e;->a(I)V

    iget-object v0, p0, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {v0, p2}, La/e/a/c/e;->a(I)V

    iput p1, p0, La/e/a/c/g;->ja:I

    sub-int/2addr p2, p1

    iput p2, p0, La/e/a/c/g;->fa:I

    iget-boolean p2, p0, La/e/a/c/g;->M:Z

    if-eqz p2, :cond_1

    iget-object p2, p0, La/e/a/c/g;->V:La/e/a/c/e;

    iget v0, p0, La/e/a/c/g;->oa:I

    add-int/2addr p1, v0

    invoke-virtual {p2, p1}, La/e/a/c/e;->a(I)V

    :cond_1
    const/4 p1, 0x1

    iput-boolean p1, p0, La/e/a/c/g;->r:Z

    return-void
.end method

.method public c(Z)V
    .locals 0

    iput-boolean p1, p0, La/e/a/c/g;->O:Z

    return-void
.end method

.method public c()Z
    .locals 2

    iget v0, p0, La/e/a/c/g;->va:I

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public d(I)La/e/a/c/g;
    .locals 2

    if-nez p1, :cond_0

    iget-object p1, p0, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v0, p1, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v0, :cond_1

    iget-object v1, v0, La/e/a/c/e;->f:La/e/a/c/e;

    if-ne v1, p1, :cond_1

    iget-object p1, v0, La/e/a/c/e;->d:La/e/a/c/g;

    return-object p1

    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    iget-object p1, p0, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object v0, p1, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v0, :cond_1

    iget-object v1, v0, La/e/a/c/e;->f:La/e/a/c/e;

    if-ne v1, p1, :cond_1

    iget-object p1, v0, La/e/a/c/e;->d:La/e/a/c/g;

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, La/e/a/c/g;->f:La/e/a/c/a/m;

    if-nez v0, :cond_0

    new-instance v0, La/e/a/c/a/m;

    invoke-direct {v0, p0}, La/e/a/c/a/m;-><init>(La/e/a/c/g;)V

    iput-object v0, p0, La/e/a/c/g;->f:La/e/a/c/a/m;

    :cond_0
    iget-object v0, p0, La/e/a/c/g;->g:La/e/a/c/a/p;

    if-nez v0, :cond_1

    new-instance v0, La/e/a/c/a/p;

    invoke-direct {v0, p0}, La/e/a/c/a/p;-><init>(La/e/a/c/g;)V

    iput-object v0, p0, La/e/a/c/g;->g:La/e/a/c/a/p;

    :cond_1
    return-void
.end method

.method public d(F)V
    .locals 2

    iget-object v0, p0, La/e/a/c/g;->Na:[F

    const/4 v1, 0x1

    aput p1, v0, v1

    return-void
.end method

.method public d(II)V
    .locals 0

    iput p1, p0, La/e/a/c/g;->ia:I

    sub-int/2addr p2, p1

    iput p2, p0, La/e/a/c/g;->ea:I

    iget p1, p0, La/e/a/c/g;->ea:I

    iget p2, p0, La/e/a/c/g;->pa:I

    if-ge p1, p2, :cond_0

    iput p2, p0, La/e/a/c/g;->ea:I

    :cond_0
    return-void
.end method

.method public d(Z)V
    .locals 0

    iput-boolean p1, p0, La/e/a/c/g;->j:Z

    return-void
.end method

.method public e()I
    .locals 1

    iget v0, p0, La/e/a/c/g;->oa:I

    return v0
.end method

.method public e(I)La/e/a/c/g;
    .locals 2

    if-nez p1, :cond_0

    iget-object p1, p0, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v0, p1, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v0, :cond_1

    iget-object v1, v0, La/e/a/c/e;->f:La/e/a/c/e;

    if-ne v1, p1, :cond_1

    iget-object p1, v0, La/e/a/c/e;->d:La/e/a/c/g;

    return-object p1

    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    iget-object p1, p0, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v0, p1, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v0, :cond_1

    iget-object v1, v0, La/e/a/c/e;->f:La/e/a/c/e;

    if-ne v1, p1, :cond_1

    iget-object p1, v0, La/e/a/c/e;->d:La/e/a/c/g;

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public e(II)V
    .locals 0

    iput p1, p0, La/e/a/c/g;->P:I

    iput p2, p0, La/e/a/c/g;->Q:I

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, La/e/a/c/g;->d(Z)V

    return-void
.end method

.method public f()I
    .locals 2

    invoke-virtual {p0}, La/e/a/c/g;->E()I

    move-result v0

    iget v1, p0, La/e/a/c/g;->fa:I

    add-int/2addr v0, v1

    return v0
.end method

.method public f(I)La/e/a/c/a/s;
    .locals 1

    if-nez p1, :cond_0

    iget-object p1, p0, La/e/a/c/g;->f:La/e/a/c/a/m;

    return-object p1

    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    iget-object p1, p0, La/e/a/c/g;->g:La/e/a/c/a/p;

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public f(II)V
    .locals 0

    iput p1, p0, La/e/a/c/g;->ia:I

    iput p2, p0, La/e/a/c/g;->ja:I

    return-void
.end method

.method public g()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, La/e/a/c/g;->ta:Ljava/lang/Object;

    return-object v0
.end method

.method public g(II)V
    .locals 0

    iput p1, p0, La/e/a/c/g;->ja:I

    sub-int/2addr p2, p1

    iput p2, p0, La/e/a/c/g;->fa:I

    iget p1, p0, La/e/a/c/g;->fa:I

    iget p2, p0, La/e/a/c/g;->qa:I

    if-ge p1, p2, :cond_0

    iput p2, p0, La/e/a/c/g;->fa:I

    :cond_0
    return-void
.end method

.method public g(I)Z
    .locals 4

    const/4 v0, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_3

    iget-object p1, p0, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object p1, p1, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz p1, :cond_0

    move p1, v1

    goto :goto_0

    :cond_0
    move p1, v2

    :goto_0
    iget-object v3, p0, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v3, v3, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v3, :cond_1

    move v3, v1

    goto :goto_1

    :cond_1
    move v3, v2

    :goto_1
    add-int/2addr p1, v3

    if-ge p1, v0, :cond_2

    goto :goto_2

    :cond_2
    move v1, v2

    :goto_2
    return v1

    :cond_3
    iget-object p1, p0, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object p1, p1, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz p1, :cond_4

    move p1, v1

    goto :goto_3

    :cond_4
    move p1, v2

    :goto_3
    iget-object v3, p0, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object v3, v3, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v3, :cond_5

    move v3, v1

    goto :goto_4

    :cond_5
    move v3, v2

    :goto_4
    add-int/2addr p1, v3

    iget-object v3, p0, La/e/a/c/g;->V:La/e/a/c/e;

    iget-object v3, v3, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v3, :cond_6

    move v3, v1

    goto :goto_5

    :cond_6
    move v3, v2

    :goto_5
    add-int/2addr p1, v3

    if-ge p1, v0, :cond_7

    goto :goto_6

    :cond_7
    move v1, v2

    :goto_6
    return v1
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, La/e/a/c/g;->wa:Ljava/lang/String;

    return-object v0
.end method

.method public h(I)Z
    .locals 1

    iget-object v0, p0, La/e/a/c/g;->ba:[Z

    aget-boolean p1, v0, p1

    return p1
.end method

.method public i()F
    .locals 1

    iget v0, p0, La/e/a/c/g;->ga:F

    return v0
.end method

.method public i(I)V
    .locals 0

    iput p1, p0, La/e/a/c/g;->oa:I

    if-lez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, La/e/a/c/g;->M:Z

    return-void
.end method

.method public j()I
    .locals 1

    iget v0, p0, La/e/a/c/g;->ha:I

    return v0
.end method

.method public j(I)V
    .locals 3

    iget-boolean v0, p0, La/e/a/c/g;->M:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget v0, p0, La/e/a/c/g;->oa:I

    sub-int v0, p1, v0

    iget v1, p0, La/e/a/c/g;->fa:I

    add-int/2addr v1, v0

    iput v0, p0, La/e/a/c/g;->ja:I

    iget-object v2, p0, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {v2, v0}, La/e/a/c/e;->a(I)V

    iget-object v0, p0, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {v0, v1}, La/e/a/c/e;->a(I)V

    iget-object v0, p0, La/e/a/c/g;->V:La/e/a/c/e;

    invoke-virtual {v0, p1}, La/e/a/c/e;->a(I)V

    const/4 p1, 0x1

    iput-boolean p1, p0, La/e/a/c/g;->r:Z

    return-void
.end method

.method public k()I
    .locals 2

    iget v0, p0, La/e/a/c/g;->va:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget v0, p0, La/e/a/c/g;->fa:I

    return v0
.end method

.method public k(I)V
    .locals 1

    iget-object v0, p0, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {v0, p1}, La/e/a/c/e;->a(I)V

    iput p1, p0, La/e/a/c/g;->ia:I

    return-void
.end method

.method public l()F
    .locals 1

    iget v0, p0, La/e/a/c/g;->ra:F

    return v0
.end method

.method public l(I)V
    .locals 1

    iget-object v0, p0, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {v0, p1}, La/e/a/c/e;->a(I)V

    iput p1, p0, La/e/a/c/g;->ja:I

    return-void
.end method

.method public m()I
    .locals 1

    iget v0, p0, La/e/a/c/g;->Ja:I

    return v0
.end method

.method public m(I)V
    .locals 1

    iput p1, p0, La/e/a/c/g;->fa:I

    iget p1, p0, La/e/a/c/g;->fa:I

    iget v0, p0, La/e/a/c/g;->qa:I

    if-ge p1, v0, :cond_0

    iput v0, p0, La/e/a/c/g;->fa:I

    :cond_0
    return-void
.end method

.method public n()La/e/a/c/g$a;
    .locals 2

    iget-object v0, p0, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public n(I)V
    .locals 0

    iput p1, p0, La/e/a/c/g;->Ja:I

    return-void
.end method

.method public o()I
    .locals 2

    iget-object v0, p0, La/e/a/c/g;->R:La/e/a/c/e;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget v0, v0, La/e/a/c/e;->g:I

    add-int/2addr v1, v0

    :cond_0
    iget-object v0, p0, La/e/a/c/g;->T:La/e/a/c/e;

    if-eqz v0, :cond_1

    iget v0, v0, La/e/a/c/e;->g:I

    add-int/2addr v1, v0

    :cond_1
    return v1
.end method

.method public o(I)V
    .locals 2

    iget-object v0, p0, La/e/a/c/g;->K:[I

    const/4 v1, 0x1

    aput p1, v0, v1

    return-void
.end method

.method public p()I
    .locals 1

    iget v0, p0, La/e/a/c/g;->P:I

    return v0
.end method

.method public p(I)V
    .locals 2

    iget-object v0, p0, La/e/a/c/g;->K:[I

    const/4 v1, 0x0

    aput p1, v0, v1

    return-void
.end method

.method public q()I
    .locals 1

    iget v0, p0, La/e/a/c/g;->Q:I

    return v0
.end method

.method public q(I)V
    .locals 0

    if-gez p1, :cond_0

    const/4 p1, 0x0

    iput p1, p0, La/e/a/c/g;->qa:I

    goto :goto_0

    :cond_0
    iput p1, p0, La/e/a/c/g;->qa:I

    :goto_0
    return-void
.end method

.method public r()I
    .locals 2

    iget-object v0, p0, La/e/a/c/g;->K:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    return v0
.end method

.method public r(I)V
    .locals 0

    if-gez p1, :cond_0

    const/4 p1, 0x0

    iput p1, p0, La/e/a/c/g;->pa:I

    goto :goto_0

    :cond_0
    iput p1, p0, La/e/a/c/g;->pa:I

    :goto_0
    return-void
.end method

.method public s()I
    .locals 2

    iget-object v0, p0, La/e/a/c/g;->K:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public s(I)V
    .locals 0

    iput p1, p0, La/e/a/c/g;->Ka:I

    return-void
.end method

.method public t()I
    .locals 1

    iget v0, p0, La/e/a/c/g;->qa:I

    return v0
.end method

.method public t(I)V
    .locals 0

    iput p1, p0, La/e/a/c/g;->va:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, La/e/a/c/g;->xa:Ljava/lang/String;

    const-string v2, " "

    const-string v3, ""

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "type: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, La/e/a/c/g;->xa:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v3

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, La/e/a/c/g;->wa:Ljava/lang/String;

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, La/e/a/c/g;->wa:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_1
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, La/e/a/c/g;->ia:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, La/e/a/c/g;->ja:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ") - ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, La/e/a/c/g;->ea:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " x "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, La/e/a/c/g;->fa:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()I
    .locals 1

    iget v0, p0, La/e/a/c/g;->pa:I

    return v0
.end method

.method public u(I)V
    .locals 1

    iput p1, p0, La/e/a/c/g;->ea:I

    iget p1, p0, La/e/a/c/g;->ea:I

    iget v0, p0, La/e/a/c/g;->pa:I

    if-ge p1, v0, :cond_0

    iput v0, p0, La/e/a/c/g;->ea:I

    :cond_0
    return-void
.end method

.method public v()La/e/a/c/g;
    .locals 1

    iget-object v0, p0, La/e/a/c/g;->da:La/e/a/c/g;

    return-object v0
.end method

.method public v(I)V
    .locals 1

    if-ltz p1, :cond_0

    const/4 v0, 0x3

    if-gt p1, v0, :cond_0

    iput p1, p0, La/e/a/c/g;->w:I

    :cond_0
    return-void
.end method

.method public w()I
    .locals 2

    invoke-virtual {p0}, La/e/a/c/g;->D()I

    move-result v0

    iget v1, p0, La/e/a/c/g;->ea:I

    add-int/2addr v0, v1

    return v0
.end method

.method public w(I)V
    .locals 0

    iput p1, p0, La/e/a/c/g;->ia:I

    return-void
.end method

.method public x()F
    .locals 1

    iget v0, p0, La/e/a/c/g;->sa:F

    return v0
.end method

.method public x(I)V
    .locals 0

    iput p1, p0, La/e/a/c/g;->ja:I

    return-void
.end method

.method public y()I
    .locals 1

    iget v0, p0, La/e/a/c/g;->Ka:I

    return v0
.end method

.method public z()La/e/a/c/g$a;
    .locals 2

    iget-object v0, p0, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method
