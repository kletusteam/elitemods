.class La/e/a/a/a/a$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = La/e/a/a/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# static fields
.field private static a:[D


# instance fields
.field b:[D

.field c:D

.field d:D

.field e:D

.field f:D

.field g:D

.field h:D

.field i:D

.field j:D

.field k:D

.field l:D

.field m:D

.field n:D

.field o:D

.field p:D

.field q:D

.field r:Z

.field s:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x5b

    new-array v0, v0, [D

    sput-object v0, La/e/a/a/a/a$a;->a:[D

    return-void
.end method

.method constructor <init>(IDDDDDD)V
    .locals 20

    move-object/from16 v9, p0

    move/from16 v0, p1

    move-wide/from16 v1, p6

    move-wide/from16 v3, p8

    move-wide/from16 v5, p10

    move-wide/from16 v7, p12

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/4 v10, 0x0

    iput-boolean v10, v9, La/e/a/a/a/a$a;->s:Z

    const/4 v11, 0x1

    if-ne v0, v11, :cond_0

    move v10, v11

    :cond_0
    iput-boolean v10, v9, La/e/a/a/a/a$a;->r:Z

    move-wide/from16 v12, p2

    iput-wide v12, v9, La/e/a/a/a/a$a;->d:D

    move-wide/from16 v12, p4

    iput-wide v12, v9, La/e/a/a/a/a$a;->e:D

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    iget-wide v14, v9, La/e/a/a/a/a$a;->e:D

    iget-wide v3, v9, La/e/a/a/a/a$a;->d:D

    sub-double/2addr v14, v3

    div-double/2addr v12, v14

    iput-wide v12, v9, La/e/a/a/a/a$a;->j:D

    const/4 v3, 0x3

    if-ne v3, v0, :cond_1

    iput-boolean v11, v9, La/e/a/a/a/a$a;->s:Z

    :cond_1
    sub-double v3, v5, v1

    move-wide/from16 v12, p8

    sub-double v14, v7, v12

    iget-boolean v0, v9, La/e/a/a/a/a$a;->s:Z

    if-nez v0, :cond_7

    invoke-static {v3, v4}, Ljava/lang/Math;->abs(D)D

    move-result-wide v16

    const-wide v18, 0x3f50624dd2f1a9fcL    # 0.001

    cmpg-double v0, v16, v18

    if-ltz v0, :cond_7

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(D)D

    move-result-wide v16

    cmpg-double v0, v16, v18

    if-gez v0, :cond_2

    goto :goto_4

    :cond_2
    const/16 v0, 0x65

    new-array v0, v0, [D

    iput-object v0, v9, La/e/a/a/a/a$a;->b:[D

    iget-boolean v0, v9, La/e/a/a/a/a$a;->r:Z

    if-eqz v0, :cond_3

    const/4 v0, -0x1

    goto :goto_0

    :cond_3
    move v0, v11

    :goto_0
    int-to-double v10, v0

    mul-double/2addr v3, v10

    iput-wide v3, v9, La/e/a/a/a/a$a;->k:D

    iget-boolean v0, v9, La/e/a/a/a/a$a;->r:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    const/4 v0, -0x1

    :goto_1
    int-to-double v3, v0

    mul-double/2addr v14, v3

    iput-wide v14, v9, La/e/a/a/a/a$a;->l:D

    iget-boolean v0, v9, La/e/a/a/a/a$a;->r:Z

    if-eqz v0, :cond_5

    move-wide v3, v5

    goto :goto_2

    :cond_5
    move-wide v3, v1

    :goto_2
    iput-wide v3, v9, La/e/a/a/a/a$a;->m:D

    iget-boolean v0, v9, La/e/a/a/a/a$a;->r:Z

    if-eqz v0, :cond_6

    move-wide v3, v12

    goto :goto_3

    :cond_6
    move-wide v3, v7

    :goto_3
    iput-wide v3, v9, La/e/a/a/a/a$a;->n:D

    move-object/from16 v0, p0

    move-wide/from16 v1, p6

    move-wide/from16 v3, p8

    move-wide/from16 v5, p10

    move-wide/from16 v7, p12

    invoke-direct/range {v0 .. v8}, La/e/a/a/a/a$a;->a(DDDD)V

    iget-wide v0, v9, La/e/a/a/a/a$a;->c:D

    iget-wide v2, v9, La/e/a/a/a/a$a;->j:D

    mul-double/2addr v0, v2

    iput-wide v0, v9, La/e/a/a/a/a$a;->o:D

    return-void

    :cond_7
    :goto_4
    move v0, v11

    iput-boolean v0, v9, La/e/a/a/a/a$a;->s:Z

    iput-wide v1, v9, La/e/a/a/a/a$a;->f:D

    iput-wide v5, v9, La/e/a/a/a/a$a;->g:D

    iput-wide v12, v9, La/e/a/a/a/a$a;->h:D

    iput-wide v7, v9, La/e/a/a/a/a$a;->i:D

    invoke-static {v14, v15, v3, v4}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    iput-wide v0, v9, La/e/a/a/a/a$a;->c:D

    iget-wide v0, v9, La/e/a/a/a/a$a;->c:D

    iget-wide v5, v9, La/e/a/a/a/a$a;->j:D

    mul-double/2addr v0, v5

    iput-wide v0, v9, La/e/a/a/a/a$a;->o:D

    iget-wide v0, v9, La/e/a/a/a/a$a;->e:D

    iget-wide v5, v9, La/e/a/a/a/a$a;->d:D

    sub-double v7, v0, v5

    div-double/2addr v3, v7

    iput-wide v3, v9, La/e/a/a/a/a$a;->m:D

    sub-double/2addr v0, v5

    div-double/2addr v14, v0

    iput-wide v14, v9, La/e/a/a/a/a$a;->n:D

    return-void
.end method

.method private a(DDDD)V
    .locals 20

    move-object/from16 v0, p0

    sub-double v1, p5, p1

    sub-double v3, p3, p7

    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    const-wide/16 v11, 0x0

    const-wide/16 v13, 0x0

    :goto_0
    sget-object v15, La/e/a/a/a/a$a;->a:[D

    array-length v5, v15

    if-ge v8, v5, :cond_1

    const-wide v16, 0x4056800000000000L    # 90.0

    int-to-double v6, v8

    mul-double v6, v6, v16

    array-length v5, v15

    add-int/lit8 v5, v5, -0x1

    move-wide/from16 p4, v9

    int-to-double v9, v5

    div-double/2addr v6, v9

    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    move-result-wide v9

    invoke-static {v5, v6}, Ljava/lang/Math;->cos(D)D

    move-result-wide v5

    mul-double/2addr v9, v1

    mul-double/2addr v5, v3

    if-lez v8, :cond_0

    sub-double v11, v9, v11

    sub-double v13, v5, v13

    invoke-static {v11, v12, v13, v14}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v11

    add-double v11, p4, v11

    sget-object v7, La/e/a/a/a/a$a;->a:[D

    aput-wide v11, v7, v8

    goto :goto_1

    :cond_0
    move-wide/from16 v11, p4

    :goto_1
    add-int/lit8 v8, v8, 0x1

    move-wide v13, v5

    move-wide/from16 v18, v9

    move-wide v9, v11

    move-wide/from16 v11, v18

    goto :goto_0

    :cond_1
    move-wide v11, v9

    iput-wide v11, v0, La/e/a/a/a/a$a;->c:D

    const/4 v1, 0x0

    :goto_2
    sget-object v2, La/e/a/a/a/a$a;->a:[D

    array-length v3, v2

    if-ge v1, v3, :cond_2

    aget-wide v3, v2, v1

    div-double/2addr v3, v11

    aput-wide v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_3
    iget-object v2, v0, La/e/a/a/a/a$a;->b:[D

    array-length v3, v2

    if-ge v1, v3, :cond_5

    int-to-double v3, v1

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    int-to-double v5, v2

    div-double/2addr v3, v5

    sget-object v2, La/e/a/a/a/a$a;->a:[D

    invoke-static {v2, v3, v4}, Ljava/util/Arrays;->binarySearch([DD)I

    move-result v2

    if-ltz v2, :cond_3

    iget-object v3, v0, La/e/a/a/a/a$a;->b:[D

    int-to-double v4, v2

    sget-object v2, La/e/a/a/a/a$a;->a:[D

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    int-to-double v6, v2

    div-double/2addr v4, v6

    aput-wide v4, v3, v1

    const-wide/16 v5, 0x0

    goto :goto_4

    :cond_3
    const/4 v5, -0x1

    if-ne v2, v5, :cond_4

    iget-object v2, v0, La/e/a/a/a/a$a;->b:[D

    const-wide/16 v5, 0x0

    aput-wide v5, v2, v1

    goto :goto_4

    :cond_4
    const-wide/16 v5, 0x0

    neg-int v2, v2

    add-int/lit8 v7, v2, -0x2

    add-int/lit8 v2, v2, -0x1

    int-to-double v8, v7

    sget-object v10, La/e/a/a/a/a$a;->a:[D

    aget-wide v11, v10, v7

    sub-double/2addr v3, v11

    aget-wide v11, v10, v2

    aget-wide v13, v10, v7

    sub-double/2addr v11, v13

    div-double/2addr v3, v11

    add-double/2addr v8, v3

    array-length v2, v10

    add-int/lit8 v2, v2, -0x1

    int-to-double v2, v2

    div-double/2addr v8, v2

    iget-object v2, v0, La/e/a/a/a/a$a;->b:[D

    aput-wide v8, v2, v1

    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_5
    return-void
.end method


# virtual methods
.method a()D
    .locals 6

    goto/32 :goto_c

    nop

    :goto_0
    iget-wide v4, p0, La/e/a/a/a/a$a;->o:D

    goto/32 :goto_d

    nop

    :goto_1
    iget-wide v2, p0, La/e/a/a/a/a$a;->q:D

    goto/32 :goto_f

    nop

    :goto_2
    iget-wide v4, p0, La/e/a/a/a/a$a;->p:D

    goto/32 :goto_a

    nop

    :goto_3
    iget-wide v2, p0, La/e/a/a/a/a$a;->l:D

    goto/32 :goto_4

    nop

    :goto_4
    neg-double v2, v2

    goto/32 :goto_2

    nop

    :goto_5
    if-nez v2, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_8

    nop

    :goto_6
    return-wide v0

    :goto_7
    mul-double/2addr v0, v4

    goto/32 :goto_6

    nop

    :goto_8
    neg-double v0, v0

    :goto_9
    goto/32 :goto_7

    nop

    :goto_a
    mul-double/2addr v2, v4

    goto/32 :goto_0

    nop

    :goto_b
    iget-boolean v2, p0, La/e/a/a/a/a$a;->r:Z

    goto/32 :goto_5

    nop

    :goto_c
    iget-wide v0, p0, La/e/a/a/a/a$a;->k:D

    goto/32 :goto_1

    nop

    :goto_d
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v2

    goto/32 :goto_e

    nop

    :goto_e
    div-double/2addr v4, v2

    goto/32 :goto_b

    nop

    :goto_f
    mul-double/2addr v0, v2

    goto/32 :goto_3

    nop
.end method

.method public a(D)D
    .locals 0

    iget-wide p1, p0, La/e/a/a/a/a$a;->m:D

    return-wide p1
.end method

.method b()D
    .locals 6

    goto/32 :goto_3

    nop

    :goto_0
    iget-wide v4, p0, La/e/a/a/a/a$a;->p:D

    goto/32 :goto_8

    nop

    :goto_1
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    goto/32 :goto_10

    nop

    :goto_2
    neg-double v2, v2

    goto/32 :goto_0

    nop

    :goto_3
    iget-wide v0, p0, La/e/a/a/a/a$a;->k:D

    goto/32 :goto_9

    nop

    :goto_4
    iget-boolean v0, p0, La/e/a/a/a/a$a;->r:Z

    goto/32 :goto_11

    nop

    :goto_5
    iget-wide v2, p0, La/e/a/a/a/a$a;->l:D

    goto/32 :goto_2

    nop

    :goto_6
    iget-wide v4, p0, La/e/a/a/a/a$a;->o:D

    goto/32 :goto_1

    nop

    :goto_7
    mul-double/2addr v0, v2

    goto/32 :goto_5

    nop

    :goto_8
    mul-double/2addr v2, v4

    goto/32 :goto_6

    nop

    :goto_9
    iget-wide v2, p0, La/e/a/a/a/a$a;->q:D

    goto/32 :goto_7

    nop

    :goto_a
    goto :goto_e

    :goto_b
    goto/32 :goto_d

    nop

    :goto_c
    mul-double/2addr v0, v4

    goto/32 :goto_a

    nop

    :goto_d
    mul-double v0, v2, v4

    :goto_e
    goto/32 :goto_f

    nop

    :goto_f
    return-wide v0

    :goto_10
    div-double/2addr v4, v0

    goto/32 :goto_4

    nop

    :goto_11
    if-nez v0, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_12

    nop

    :goto_12
    neg-double v0, v2

    goto/32 :goto_c

    nop
.end method

.method public b(D)D
    .locals 0

    iget-wide p1, p0, La/e/a/a/a/a$a;->n:D

    return-wide p1
.end method

.method c()D
    .locals 6

    goto/32 :goto_3

    nop

    :goto_0
    add-double/2addr v0, v2

    goto/32 :goto_2

    nop

    :goto_1
    mul-double/2addr v2, v4

    goto/32 :goto_0

    nop

    :goto_2
    return-wide v0

    :goto_3
    iget-wide v0, p0, La/e/a/a/a/a$a;->m:D

    goto/32 :goto_4

    nop

    :goto_4
    iget-wide v2, p0, La/e/a/a/a/a$a;->k:D

    goto/32 :goto_5

    nop

    :goto_5
    iget-wide v4, p0, La/e/a/a/a/a$a;->p:D

    goto/32 :goto_1

    nop
.end method

.method public c(D)D
    .locals 4

    iget-wide v0, p0, La/e/a/a/a/a$a;->d:D

    sub-double/2addr p1, v0

    iget-wide v0, p0, La/e/a/a/a/a$a;->j:D

    mul-double/2addr p1, v0

    iget-wide v0, p0, La/e/a/a/a/a$a;->f:D

    iget-wide v2, p0, La/e/a/a/a/a$a;->g:D

    sub-double/2addr v2, v0

    mul-double/2addr p1, v2

    add-double/2addr v0, p1

    return-wide v0
.end method

.method d()D
    .locals 6

    goto/32 :goto_3

    nop

    :goto_0
    iget-wide v2, p0, La/e/a/a/a/a$a;->l:D

    goto/32 :goto_2

    nop

    :goto_1
    mul-double/2addr v2, v4

    goto/32 :goto_5

    nop

    :goto_2
    iget-wide v4, p0, La/e/a/a/a/a$a;->q:D

    goto/32 :goto_1

    nop

    :goto_3
    iget-wide v0, p0, La/e/a/a/a/a$a;->n:D

    goto/32 :goto_0

    nop

    :goto_4
    return-wide v0

    :goto_5
    add-double/2addr v0, v2

    goto/32 :goto_4

    nop
.end method

.method public d(D)D
    .locals 4

    iget-wide v0, p0, La/e/a/a/a/a$a;->d:D

    sub-double/2addr p1, v0

    iget-wide v0, p0, La/e/a/a/a/a$a;->j:D

    mul-double/2addr p1, v0

    iget-wide v0, p0, La/e/a/a/a/a$a;->h:D

    iget-wide v2, p0, La/e/a/a/a/a$a;->i:D

    sub-double/2addr v2, v0

    mul-double/2addr p1, v2

    add-double/2addr v0, p1

    return-wide v0
.end method

.method e(D)D
    .locals 8

    goto/32 :goto_0

    nop

    :goto_0
    const-wide/16 v0, 0x0

    goto/32 :goto_5

    nop

    :goto_1
    return-wide v0

    :goto_2
    goto/32 :goto_b

    nop

    :goto_3
    if-lez v2, :cond_0

    goto/32 :goto_13

    :cond_0
    goto/32 :goto_12

    nop

    :goto_4
    sub-double/2addr p1, v2

    goto/32 :goto_d

    nop

    :goto_5
    cmpg-double v2, p1, v0

    goto/32 :goto_3

    nop

    :goto_6
    add-int/lit8 v4, v1, 0x1

    goto/32 :goto_8

    nop

    :goto_7
    int-to-double v2, v1

    goto/32 :goto_4

    nop

    :goto_8
    aget-wide v4, v0, v4

    goto/32 :goto_10

    nop

    :goto_9
    cmpl-double v2, p1, v0

    goto/32 :goto_17

    nop

    :goto_a
    mul-double/2addr p1, v4

    goto/32 :goto_f

    nop

    :goto_b
    iget-object v0, p0, La/e/a/a/a/a$a;->b:[D

    goto/32 :goto_e

    nop

    :goto_c
    return-wide v2

    :goto_d
    aget-wide v2, v0, v1

    goto/32 :goto_6

    nop

    :goto_e
    array-length v1, v0

    goto/32 :goto_16

    nop

    :goto_f
    add-double/2addr v2, p1

    goto/32 :goto_c

    nop

    :goto_10
    aget-wide v6, v0, v1

    goto/32 :goto_18

    nop

    :goto_11
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    goto/32 :goto_9

    nop

    :goto_12
    return-wide v0

    :goto_13
    goto/32 :goto_11

    nop

    :goto_14
    int-to-double v1, v1

    goto/32 :goto_19

    nop

    :goto_15
    double-to-int v1, p1

    goto/32 :goto_7

    nop

    :goto_16
    add-int/lit8 v1, v1, -0x1

    goto/32 :goto_14

    nop

    :goto_17
    if-gez v2, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_1

    nop

    :goto_18
    sub-double/2addr v4, v6

    goto/32 :goto_a

    nop

    :goto_19
    mul-double/2addr p1, v1

    goto/32 :goto_15

    nop
.end method

.method f(D)V
    .locals 2

    goto/32 :goto_4

    nop

    :goto_0
    iget-wide v0, p0, La/e/a/a/a/a$a;->e:D

    goto/32 :goto_2

    nop

    :goto_1
    invoke-virtual {p0, v0, v1}, La/e/a/a/a/a$a;->e(D)D

    move-result-wide v0

    goto/32 :goto_11

    nop

    :goto_2
    sub-double/2addr v0, p1

    goto/32 :goto_c

    nop

    :goto_3
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide p1

    goto/32 :goto_6

    nop

    :goto_4
    iget-boolean v0, p0, La/e/a/a/a/a$a;->r:Z

    goto/32 :goto_5

    nop

    :goto_5
    if-nez v0, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_0

    nop

    :goto_6
    iput-wide p1, p0, La/e/a/a/a/a$a;->p:D

    goto/32 :goto_f

    nop

    :goto_7
    sub-double v0, p1, v0

    :goto_8
    goto/32 :goto_e

    nop

    :goto_9
    return-void

    :goto_a
    const-wide p1, 0x3ff921fb54442d18L    # 1.5707963267948966

    goto/32 :goto_1

    nop

    :goto_b
    iput-wide p1, p0, La/e/a/a/a/a$a;->q:D

    goto/32 :goto_9

    nop

    :goto_c
    goto :goto_8

    :goto_d
    goto/32 :goto_12

    nop

    :goto_e
    iget-wide p1, p0, La/e/a/a/a/a$a;->j:D

    goto/32 :goto_10

    nop

    :goto_f
    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide p1

    goto/32 :goto_b

    nop

    :goto_10
    mul-double/2addr v0, p1

    goto/32 :goto_a

    nop

    :goto_11
    mul-double/2addr v0, p1

    goto/32 :goto_3

    nop

    :goto_12
    iget-wide v0, p0, La/e/a/a/a/a$a;->d:D

    goto/32 :goto_7

    nop
.end method
