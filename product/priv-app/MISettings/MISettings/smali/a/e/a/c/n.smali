.class public La/e/a/c/n;
.super Ljava/lang/Object;


# static fields
.field static a:[Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x3

    new-array v0, v0, [Z

    sput-object v0, La/e/a/c/n;->a:[Z

    return-void
.end method

.method static a(La/e/a/c/h;La/e/a/d;La/e/a/c/g;)V
    .locals 5

    const/4 v0, -0x1

    iput v0, p2, La/e/a/c/g;->u:I

    iput v0, p2, La/e/a/c/g;->v:I

    iget-object v0, p0, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    sget-object v2, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    const/4 v3, 0x2

    if-eq v0, v2, :cond_0

    iget-object v0, p2, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object v0, v0, v1

    sget-object v1, La/e/a/c/g$a;->d:La/e/a/c/g$a;

    if-ne v0, v1, :cond_0

    iget-object v0, p2, La/e/a/c/g;->R:La/e/a/c/e;

    iget v0, v0, La/e/a/c/e;->g:I

    invoke-virtual {p0}, La/e/a/c/g;->C()I

    move-result v1

    iget-object v2, p2, La/e/a/c/g;->T:La/e/a/c/e;

    iget v2, v2, La/e/a/c/e;->g:I

    sub-int/2addr v1, v2

    iget-object v2, p2, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {p1, v2}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v4

    iput-object v4, v2, La/e/a/c/e;->i:La/e/a/j;

    iget-object v2, p2, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {p1, v2}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v4

    iput-object v4, v2, La/e/a/c/e;->i:La/e/a/j;

    iget-object v2, p2, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v2, v2, La/e/a/c/e;->i:La/e/a/j;

    invoke-virtual {p1, v2, v0}, La/e/a/d;->a(La/e/a/j;I)V

    iget-object v2, p2, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v2, v2, La/e/a/c/e;->i:La/e/a/j;

    invoke-virtual {p1, v2, v1}, La/e/a/d;->a(La/e/a/j;I)V

    iput v3, p2, La/e/a/c/g;->u:I

    invoke-virtual {p2, v0, v1}, La/e/a/c/g;->d(II)V

    :cond_0
    iget-object v0, p0, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    sget-object v2, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-eq v0, v2, :cond_3

    iget-object v0, p2, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object v0, v0, v1

    sget-object v1, La/e/a/c/g$a;->d:La/e/a/c/g$a;

    if-ne v0, v1, :cond_3

    iget-object v0, p2, La/e/a/c/g;->S:La/e/a/c/e;

    iget v0, v0, La/e/a/c/e;->g:I

    invoke-virtual {p0}, La/e/a/c/g;->k()I

    move-result p0

    iget-object v1, p2, La/e/a/c/g;->U:La/e/a/c/e;

    iget v1, v1, La/e/a/c/e;->g:I

    sub-int/2addr p0, v1

    iget-object v1, p2, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {p1, v1}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v2

    iput-object v2, v1, La/e/a/c/e;->i:La/e/a/j;

    iget-object v1, p2, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {p1, v1}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v2

    iput-object v2, v1, La/e/a/c/e;->i:La/e/a/j;

    iget-object v1, p2, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v1, v1, La/e/a/c/e;->i:La/e/a/j;

    invoke-virtual {p1, v1, v0}, La/e/a/d;->a(La/e/a/j;I)V

    iget-object v1, p2, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object v1, v1, La/e/a/c/e;->i:La/e/a/j;

    invoke-virtual {p1, v1, p0}, La/e/a/d;->a(La/e/a/j;I)V

    iget v1, p2, La/e/a/c/g;->oa:I

    if-gtz v1, :cond_1

    invoke-virtual {p2}, La/e/a/c/g;->B()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_2

    :cond_1
    iget-object v1, p2, La/e/a/c/g;->V:La/e/a/c/e;

    invoke-virtual {p1, v1}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v2

    iput-object v2, v1, La/e/a/c/e;->i:La/e/a/j;

    iget-object v1, p2, La/e/a/c/g;->V:La/e/a/c/e;

    iget-object v1, v1, La/e/a/c/e;->i:La/e/a/j;

    iget v2, p2, La/e/a/c/g;->oa:I

    add-int/2addr v2, v0

    invoke-virtual {p1, v1, v2}, La/e/a/d;->a(La/e/a/j;I)V

    :cond_2
    iput v3, p2, La/e/a/c/g;->v:I

    invoke-virtual {p2, v0, p0}, La/e/a/c/g;->g(II)V

    :cond_3
    return-void
.end method

.method public static final a(II)Z
    .locals 0

    and-int/2addr p0, p1

    if-ne p0, p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method
