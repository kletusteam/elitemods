.class public La/e/a/a/a/i;
.super Ljava/lang/Object;


# instance fields
.field a:[F

.field b:[D

.field c:[D

.field d:Ljava/lang/String;

.field e:La/e/a/a/a/h;

.field f:I

.field g:D

.field private h:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    new-array v1, v0, [F

    iput-object v1, p0, La/e/a/a/a/i;->a:[F

    new-array v1, v0, [D

    iput-object v1, p0, La/e/a/a/a/i;->b:[D

    const-wide v1, 0x401921fb54442d18L    # 6.283185307179586

    iput-wide v1, p0, La/e/a/a/a/i;->g:D

    iput-boolean v0, p0, La/e/a/a/a/i;->h:Z

    return-void
.end method


# virtual methods
.method a(D)D
    .locals 10

    goto/32 :goto_21

    nop

    :goto_0
    float-to-double v4, v2

    goto/32 :goto_a

    nop

    :goto_1
    sub-double/2addr v6, v8

    goto/32 :goto_1b

    nop

    :goto_2
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    goto/32 :goto_17

    nop

    :goto_3
    sub-float/2addr v2, v4

    goto/32 :goto_0

    nop

    :goto_4
    if-nez v2, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_15

    nop

    :goto_5
    const-wide p1, 0x3feffffde7210be9L    # 0.999999

    :goto_6
    goto/32 :goto_9

    nop

    :goto_7
    add-double/2addr v0, p1

    :goto_8
    goto/32 :goto_26

    nop

    :goto_9
    iget-object v2, p0, La/e/a/a/a/i;->b:[D

    goto/32 :goto_1f

    nop

    :goto_a
    iget-object v2, p0, La/e/a/a/a/i;->b:[D

    goto/32 :goto_1c

    nop

    :goto_b
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_16

    nop

    :goto_c
    if-lez v2, :cond_1

    goto/32 :goto_13

    :cond_1
    goto/32 :goto_18

    nop

    :goto_d
    if-gtz v2, :cond_2

    goto/32 :goto_1a

    :cond_2
    goto/32 :goto_19

    nop

    :goto_e
    float-to-double v0, v0

    goto/32 :goto_22

    nop

    :goto_f
    aget-wide v8, v2, v3

    goto/32 :goto_1

    nop

    :goto_10
    add-int/lit8 v3, v0, -0x1

    goto/32 :goto_25

    nop

    :goto_11
    if-gez v2, :cond_3

    goto/32 :goto_6

    :cond_3
    goto/32 :goto_5

    nop

    :goto_12
    goto :goto_6

    :goto_13
    goto/32 :goto_2

    nop

    :goto_14
    aget v2, v1, v0

    goto/32 :goto_10

    nop

    :goto_15
    neg-int v0, v2

    goto/32 :goto_b

    nop

    :goto_16
    iget-object v1, p0, La/e/a/a/a/i;->a:[F

    goto/32 :goto_14

    nop

    :goto_17
    cmpl-double v2, p1, v2

    goto/32 :goto_11

    nop

    :goto_18
    const-wide p1, 0x3ee4f8b588e368f1L    # 1.0E-5

    goto/32 :goto_12

    nop

    :goto_19
    return-wide v0

    :goto_1a
    goto/32 :goto_4

    nop

    :goto_1b
    div-double/2addr v4, v6

    goto/32 :goto_1d

    nop

    :goto_1c
    aget-wide v6, v2, v0

    goto/32 :goto_f

    nop

    :goto_1d
    mul-double/2addr p1, v4

    goto/32 :goto_23

    nop

    :goto_1e
    sub-double/2addr v0, v4

    goto/32 :goto_7

    nop

    :goto_1f
    invoke-static {v2, p1, p2}, Ljava/util/Arrays;->binarySearch([DD)I

    move-result v2

    goto/32 :goto_d

    nop

    :goto_20
    cmpg-double v2, p1, v0

    goto/32 :goto_c

    nop

    :goto_21
    const-wide/16 v0, 0x0

    goto/32 :goto_20

    nop

    :goto_22
    aget-wide v6, v2, v3

    goto/32 :goto_24

    nop

    :goto_23
    aget v0, v1, v3

    goto/32 :goto_e

    nop

    :goto_24
    mul-double/2addr v4, v6

    goto/32 :goto_1e

    nop

    :goto_25
    aget v4, v1, v3

    goto/32 :goto_3

    nop

    :goto_26
    return-wide v0
.end method

.method public a(DD)D
    .locals 7

    invoke-virtual {p0, p1, p2}, La/e/a/a/a/i;->b(D)D

    move-result-wide p1

    add-double/2addr p1, p3

    iget v0, p0, La/e/a/a/a/i;->f:I

    const-wide/high16 v1, 0x4010000000000000L    # 4.0

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    packed-switch v0, :pswitch_data_0

    iget-wide p3, p0, La/e/a/a/a/i;->g:D

    mul-double/2addr p3, p1

    invoke-static {p3, p4}, Ljava/lang/Math;->sin(D)D

    move-result-wide p1

    return-wide p1

    :pswitch_0
    iget-object p3, p0, La/e/a/a/a/i;->e:La/e/a/a/a/h;

    rem-double/2addr p1, v5

    const/4 p4, 0x0

    invoke-virtual {p3, p1, p2, p4}, La/e/a/a/a/h;->a(DI)D

    move-result-wide p1

    return-wide p1

    :pswitch_1
    mul-double/2addr p1, v1

    rem-double/2addr p1, v1

    sub-double/2addr p1, v3

    invoke-static {p1, p2}, Ljava/lang/Math;->abs(D)D

    move-result-wide p1

    sub-double p1, v5, p1

    mul-double/2addr p1, p1

    :goto_0
    sub-double/2addr v5, p1

    return-wide v5

    :pswitch_2
    iget-wide v0, p0, La/e/a/a/a/i;->g:D

    add-double/2addr p3, p1

    mul-double/2addr v0, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide p1

    return-wide p1

    :pswitch_3
    mul-double/2addr p1, v3

    add-double/2addr p1, v5

    rem-double/2addr p1, v3

    goto :goto_0

    :pswitch_4
    mul-double/2addr p1, v3

    add-double/2addr p1, v5

    rem-double/2addr p1, v3

    sub-double/2addr p1, v5

    return-wide p1

    :pswitch_5
    mul-double/2addr p1, v1

    add-double/2addr p1, v5

    rem-double/2addr p1, v1

    sub-double/2addr p1, v3

    invoke-static {p1, p2}, Ljava/lang/Math;->abs(D)D

    move-result-wide p1

    goto :goto_0

    :pswitch_6
    const-wide/high16 p3, 0x3fe0000000000000L    # 0.5

    rem-double/2addr p1, v5

    sub-double/2addr p3, p1

    invoke-static {p3, p4}, Ljava/lang/Math;->signum(D)D

    move-result-wide p1

    return-wide p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(DDD)D
    .locals 4

    invoke-virtual {p0, p1, p2}, La/e/a/a/a/i;->b(D)D

    move-result-wide v0

    add-double/2addr p3, v0

    invoke-virtual {p0, p1, p2}, La/e/a/a/a/i;->a(D)D

    move-result-wide p1

    add-double/2addr p1, p5

    iget p5, p0, La/e/a/a/a/i;->f:I

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    const-wide/high16 v2, 0x4010000000000000L    # 4.0

    packed-switch p5, :pswitch_data_0

    iget-wide p5, p0, La/e/a/a/a/i;->g:D

    mul-double/2addr p1, p5

    mul-double/2addr p5, p3

    invoke-static {p5, p6}, Ljava/lang/Math;->cos(D)D

    move-result-wide p3

    mul-double/2addr p1, p3

    return-wide p1

    :pswitch_0
    iget-object p1, p0, La/e/a/a/a/i;->e:La/e/a/a/a/h;

    const-wide/high16 p5, 0x3ff0000000000000L    # 1.0

    rem-double/2addr p3, p5

    const/4 p2, 0x0

    invoke-virtual {p1, p3, p4, p2}, La/e/a/a/a/h;->b(DI)D

    move-result-wide p1

    return-wide p1

    :pswitch_1
    mul-double/2addr p1, v2

    mul-double/2addr p3, v2

    add-double/2addr p3, v0

    rem-double/2addr p3, v2

    sub-double/2addr p3, v0

    mul-double/2addr p1, p3

    return-wide p1

    :pswitch_2
    iget-wide p5, p0, La/e/a/a/a/i;->g:D

    neg-double v0, p5

    mul-double/2addr v0, p1

    mul-double/2addr p5, p3

    invoke-static {p5, p6}, Ljava/lang/Math;->sin(D)D

    move-result-wide p1

    mul-double/2addr v0, p1

    return-wide v0

    :pswitch_3
    neg-double p1, p1

    mul-double/2addr p1, v0

    return-wide p1

    :pswitch_4
    mul-double/2addr p1, v0

    return-wide p1

    :pswitch_5
    mul-double/2addr p1, v2

    mul-double/2addr p3, v2

    const-wide/high16 p5, 0x4008000000000000L    # 3.0

    add-double/2addr p3, p5

    rem-double/2addr p3, v2

    sub-double/2addr p3, v0

    invoke-static {p3, p4}, Ljava/lang/Math;->signum(D)D

    move-result-wide p3

    mul-double/2addr p1, p3

    return-wide p1

    :pswitch_6
    const-wide/16 p1, 0x0

    return-wide p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a()V
    .locals 17

    move-object/from16 v0, p0

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    move v4, v1

    move-wide v5, v2

    :goto_0
    iget-object v7, v0, La/e/a/a/a/i;->a:[F

    array-length v8, v7

    if-ge v4, v8, :cond_0

    aget v7, v7, v4

    float-to-double v7, v7

    add-double/2addr v5, v7

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x1

    move-wide v8, v2

    move v7, v4

    :goto_1
    iget-object v10, v0, La/e/a/a/a/i;->a:[F

    array-length v11, v10

    const/high16 v12, 0x40000000    # 2.0f

    if-ge v7, v11, :cond_1

    add-int/lit8 v11, v7, -0x1

    aget v13, v10, v11

    aget v10, v10, v7

    add-float/2addr v13, v10

    div-float/2addr v13, v12

    iget-object v10, v0, La/e/a/a/a/i;->b:[D

    aget-wide v14, v10, v7

    aget-wide v11, v10, v11

    sub-double/2addr v14, v11

    float-to-double v10, v13

    mul-double/2addr v14, v10

    add-double/2addr v8, v14

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_1
    move v7, v1

    :goto_2
    iget-object v10, v0, La/e/a/a/a/i;->a:[F

    array-length v11, v10

    if-ge v7, v11, :cond_2

    aget v11, v10, v7

    float-to-double v13, v11

    div-double v15, v5, v8

    mul-double/2addr v13, v15

    double-to-float v11, v13

    aput v11, v10, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_2
    iget-object v5, v0, La/e/a/a/a/i;->c:[D

    aput-wide v2, v5, v1

    move v1, v4

    :goto_3
    iget-object v2, v0, La/e/a/a/a/i;->a:[F

    array-length v3, v2

    if-ge v1, v3, :cond_3

    add-int/lit8 v3, v1, -0x1

    aget v5, v2, v3

    aget v2, v2, v1

    add-float/2addr v5, v2

    div-float/2addr v5, v12

    iget-object v2, v0, La/e/a/a/a/i;->b:[D

    aget-wide v6, v2, v1

    aget-wide v8, v2, v3

    sub-double/2addr v6, v8

    iget-object v2, v0, La/e/a/a/a/i;->c:[D

    aget-wide v8, v2, v3

    float-to-double v10, v5

    mul-double/2addr v6, v10

    add-double/2addr v8, v6

    aput-wide v8, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_3
    iput-boolean v4, v0, La/e/a/a/a/i;->h:Z

    return-void
.end method

.method public a(DF)V
    .locals 4

    iget-object v0, p0, La/e/a/a/a/i;->a:[F

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, La/e/a/a/a/i;->b:[D

    invoke-static {v1, p1, p2}, Ljava/util/Arrays;->binarySearch([DD)I

    move-result v1

    if-gez v1, :cond_0

    neg-int v1, v1

    add-int/lit8 v1, v1, -0x1

    :cond_0
    iget-object v2, p0, La/e/a/a/a/i;->b:[D

    invoke-static {v2, v0}, Ljava/util/Arrays;->copyOf([DI)[D

    move-result-object v2

    iput-object v2, p0, La/e/a/a/a/i;->b:[D

    iget-object v2, p0, La/e/a/a/a/i;->a:[F

    invoke-static {v2, v0}, Ljava/util/Arrays;->copyOf([FI)[F

    move-result-object v2

    iput-object v2, p0, La/e/a/a/a/i;->a:[F

    new-array v2, v0, [D

    iput-object v2, p0, La/e/a/a/a/i;->c:[D

    iget-object v2, p0, La/e/a/a/a/i;->b:[D

    add-int/lit8 v3, v1, 0x1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    invoke-static {v2, v1, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, La/e/a/a/a/i;->b:[D

    aput-wide p1, v0, v1

    iget-object p1, p0, La/e/a/a/a/i;->a:[F

    aput p3, p1, v1

    const/4 p1, 0x0

    iput-boolean p1, p0, La/e/a/a/a/i;->h:Z

    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 0

    iput p1, p0, La/e/a/a/a/i;->f:I

    iput-object p2, p0, La/e/a/a/a/i;->d:Ljava/lang/String;

    iget-object p1, p0, La/e/a/a/a/i;->d:Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-static {p2}, La/e/a/a/a/h;->a(Ljava/lang/String;)La/e/a/a/a/h;

    move-result-object p1

    iput-object p1, p0, La/e/a/a/a/i;->e:La/e/a/a/a/h;

    :cond_0
    return-void
.end method

.method b(D)D
    .locals 10

    goto/32 :goto_5

    nop

    :goto_0
    iget-object v2, p0, La/e/a/a/a/i;->b:[D

    goto/32 :goto_2e

    nop

    :goto_1
    sub-double/2addr v6, v8

    goto/32 :goto_25

    nop

    :goto_2
    div-double/2addr v4, p1

    goto/32 :goto_20

    nop

    :goto_3
    goto/16 :goto_21

    :goto_4
    goto/32 :goto_28

    nop

    :goto_5
    const-wide/16 v0, 0x0

    goto/32 :goto_19

    nop

    :goto_6
    neg-int v0, v2

    goto/32 :goto_a

    nop

    :goto_7
    float-to-double v4, v2

    goto/32 :goto_e

    nop

    :goto_8
    add-double/2addr v6, v0

    goto/32 :goto_2f

    nop

    :goto_9
    sub-double v8, p1, v8

    goto/32 :goto_2d

    nop

    :goto_a
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_1d

    nop

    :goto_b
    aget v4, v1, v3

    goto/32 :goto_24

    nop

    :goto_c
    if-gtz v2, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_26

    nop

    :goto_d
    iget-object v0, p0, La/e/a/a/a/i;->c:[D

    goto/32 :goto_30

    nop

    :goto_e
    iget-object v2, p0, La/e/a/a/a/i;->b:[D

    goto/32 :goto_15

    nop

    :goto_f
    sub-double/2addr v0, v8

    goto/32 :goto_1c

    nop

    :goto_10
    aget-wide v8, v2, v3

    goto/32 :goto_22

    nop

    :goto_11
    move-wide p1, v3

    :goto_12
    goto/32 :goto_0

    nop

    :goto_13
    mul-double/2addr v4, p1

    goto/32 :goto_2b

    nop

    :goto_14
    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    goto/32 :goto_33

    nop

    :goto_15
    aget-wide v6, v2, v0

    goto/32 :goto_23

    nop

    :goto_16
    goto :goto_12

    :goto_17
    goto/32 :goto_31

    nop

    :goto_18
    mul-double/2addr v8, v4

    goto/32 :goto_f

    nop

    :goto_19
    cmpg-double v2, p1, v0

    goto/32 :goto_14

    nop

    :goto_1a
    sub-double/2addr p1, v0

    goto/32 :goto_13

    nop

    :goto_1b
    aget v0, v1, v3

    goto/32 :goto_29

    nop

    :goto_1c
    aget-wide v8, v2, v3

    goto/32 :goto_9

    nop

    :goto_1d
    iget-object v1, p0, La/e/a/a/a/i;->a:[F

    goto/32 :goto_1e

    nop

    :goto_1e
    aget v2, v1, v0

    goto/32 :goto_32

    nop

    :goto_1f
    aget-wide v8, v2, v3

    goto/32 :goto_18

    nop

    :goto_20
    add-double v0, v6, v4

    :goto_21
    goto/32 :goto_27

    nop

    :goto_22
    mul-double/2addr v0, v8

    goto/32 :goto_1a

    nop

    :goto_23
    aget-wide v8, v2, v3

    goto/32 :goto_1

    nop

    :goto_24
    sub-float/2addr v2, v4

    goto/32 :goto_7

    nop

    :goto_25
    div-double/2addr v4, v6

    goto/32 :goto_d

    nop

    :goto_26
    move-wide v0, v3

    goto/32 :goto_3

    nop

    :goto_27
    return-wide v0

    :goto_28
    if-nez v2, :cond_1

    goto/32 :goto_21

    :cond_1
    goto/32 :goto_6

    nop

    :goto_29
    float-to-double v0, v0

    goto/32 :goto_1f

    nop

    :goto_2a
    if-gtz v2, :cond_2

    goto/32 :goto_12

    :cond_2
    goto/32 :goto_11

    nop

    :goto_2b
    const-wide/high16 p1, 0x4000000000000000L    # 2.0

    goto/32 :goto_2

    nop

    :goto_2c
    move-wide p1, v0

    goto/32 :goto_16

    nop

    :goto_2d
    mul-double/2addr v0, v8

    goto/32 :goto_8

    nop

    :goto_2e
    invoke-static {v2, p1, p2}, Ljava/util/Arrays;->binarySearch([DD)I

    move-result v2

    goto/32 :goto_c

    nop

    :goto_2f
    mul-double/2addr p1, p1

    goto/32 :goto_34

    nop

    :goto_30
    aget-wide v6, v0, v3

    goto/32 :goto_1b

    nop

    :goto_31
    cmpl-double v2, p1, v3

    goto/32 :goto_2a

    nop

    :goto_32
    add-int/lit8 v3, v0, -0x1

    goto/32 :goto_b

    nop

    :goto_33
    if-ltz v2, :cond_3

    goto/32 :goto_17

    :cond_3
    goto/32 :goto_2c

    nop

    :goto_34
    aget-wide v0, v2, v3

    goto/32 :goto_10

    nop
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "pos ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, La/e/a/a/a/i;->b:[D

    invoke-static {v1}, Ljava/util/Arrays;->toString([D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " period="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, La/e/a/a/a/i;->a:[F

    invoke-static {v1}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
