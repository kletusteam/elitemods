.class public La/e/a/c/m;
.super La/e/a/c/g;

# interfaces
.implements La/e/a/c/l;


# instance fields
.field public Ua:[La/e/a/c/g;

.field public Va:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, La/e/a/c/g;-><init>()V

    const/4 v0, 0x4

    new-array v0, v0, [La/e/a/c/g;

    iput-object v0, p0, La/e/a/c/m;->Ua:[La/e/a/c/g;

    const/4 v0, 0x0

    iput v0, p0, La/e/a/c/m;->Va:I

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, La/e/a/c/m;->Va:I

    iget-object v0, p0, La/e/a/c/m;->Ua:[La/e/a/c/g;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public a(La/e/a/c/g;)V
    .locals 3

    if-eq p1, p0, :cond_2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    iget v0, p0, La/e/a/c/m;->Va:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, La/e/a/c/m;->Ua:[La/e/a/c/g;

    array-length v2, v1

    if-le v0, v2, :cond_1

    array-length v0, v1

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [La/e/a/c/g;

    iput-object v0, p0, La/e/a/c/m;->Ua:[La/e/a/c/g;

    :cond_1
    iget-object v0, p0, La/e/a/c/m;->Ua:[La/e/a/c/g;

    iget v1, p0, La/e/a/c/m;->Va:I

    aput-object p1, v0, v1

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, La/e/a/c/m;->Va:I

    :cond_2
    :goto_0
    return-void
.end method

.method public a(La/e/a/c/g;Ljava/util/HashMap;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La/e/a/c/g;",
            "Ljava/util/HashMap<",
            "La/e/a/c/g;",
            "La/e/a/c/g;",
            ">;)V"
        }
    .end annotation

    invoke-super {p0, p1, p2}, La/e/a/c/g;->a(La/e/a/c/g;Ljava/util/HashMap;)V

    check-cast p1, La/e/a/c/m;

    const/4 v0, 0x0

    iput v0, p0, La/e/a/c/m;->Va:I

    iget v1, p1, La/e/a/c/m;->Va:I

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p1, La/e/a/c/m;->Ua:[La/e/a/c/g;

    aget-object v2, v2, v0

    invoke-virtual {p2, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, La/e/a/c/g;

    invoke-virtual {p0, v2}, La/e/a/c/m;->a(La/e/a/c/g;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(La/e/a/c/h;)V
    .locals 0

    return-void
.end method

.method public a(Ljava/util/ArrayList;ILa/e/a/c/a/q;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "La/e/a/c/a/q;",
            ">;I",
            "La/e/a/c/a/q;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget v2, p0, La/e/a/c/m;->Va:I

    if-ge v1, v2, :cond_0

    iget-object v2, p0, La/e/a/c/m;->Ua:[La/e/a/c/g;

    aget-object v2, v2, v1

    invoke-virtual {p3, v2}, La/e/a/c/a/q;->a(La/e/a/c/g;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    :goto_1
    iget v1, p0, La/e/a/c/m;->Va:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, La/e/a/c/m;->Ua:[La/e/a/c/g;

    aget-object v1, v1, v0

    invoke-static {v1, p2, p1, p3}, La/e/a/c/a/i;->a(La/e/a/c/g;ILjava/util/ArrayList;La/e/a/c/a/q;)La/e/a/c/a/q;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method public y(I)I
    .locals 4

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, La/e/a/c/m;->Va:I

    const/4 v2, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, La/e/a/c/m;->Ua:[La/e/a/c/g;

    aget-object v1, v1, v0

    if-nez p1, :cond_0

    iget v3, v1, La/e/a/c/g;->Sa:I

    if-eq v3, v2, :cond_0

    return v3

    :cond_0
    const/4 v3, 0x1

    if-ne p1, v3, :cond_1

    iget v1, v1, La/e/a/c/g;->Ta:I

    if-eq v1, v2, :cond_1

    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return v2
.end method
