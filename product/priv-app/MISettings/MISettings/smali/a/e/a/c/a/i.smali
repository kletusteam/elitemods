.class public La/e/a/c/a/i;
.super Ljava/lang/Object;


# direct methods
.method public static a(La/e/a/c/g;ILjava/util/ArrayList;La/e/a/c/a/q;)La/e/a/c/a/q;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La/e/a/c/g;",
            "I",
            "Ljava/util/ArrayList<",
            "La/e/a/c/a/q;",
            ">;",
            "La/e/a/c/a/q;",
            ")",
            "La/e/a/c/a/q;"
        }
    .end annotation

    if-nez p1, :cond_0

    iget v0, p0, La/e/a/c/g;->Sa:I

    goto :goto_0

    :cond_0
    iget v0, p0, La/e/a/c/g;->Ta:I

    :goto_0
    const/4 v1, 0x0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_4

    if-eqz p3, :cond_1

    iget v3, p3, La/e/a/c/a/q;->c:I

    if-eq v0, v3, :cond_4

    :cond_1
    move v3, v1

    :goto_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_5

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, La/e/a/c/a/q;

    invoke-virtual {v4}, La/e/a/c/a/q;->a()I

    move-result v5

    if-ne v5, v0, :cond_3

    if-eqz p3, :cond_2

    invoke-virtual {p3, p1, v4}, La/e/a/c/a/q;->a(ILa/e/a/c/a/q;)V

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_2
    move-object p3, v4

    goto :goto_2

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    if-eq v0, v2, :cond_5

    return-object p3

    :cond_5
    :goto_2
    if-nez p3, :cond_9

    instance-of v0, p0, La/e/a/c/m;

    if-eqz v0, :cond_7

    move-object v0, p0

    check-cast v0, La/e/a/c/m;

    invoke-virtual {v0, p1}, La/e/a/c/m;->y(I)I

    move-result v0

    if-eq v0, v2, :cond_7

    move v2, v1

    :goto_3
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_7

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, La/e/a/c/a/q;

    invoke-virtual {v3}, La/e/a/c/a/q;->a()I

    move-result v4

    if-ne v4, v0, :cond_6

    move-object p3, v3

    goto :goto_4

    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_7
    :goto_4
    if-nez p3, :cond_8

    new-instance p3, La/e/a/c/a/q;

    invoke-direct {p3, p1}, La/e/a/c/a/q;-><init>(I)V

    :cond_8
    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_9
    invoke-virtual {p3, p0}, La/e/a/c/a/q;->a(La/e/a/c/g;)Z

    move-result v0

    if-eqz v0, :cond_d

    instance-of v0, p0, La/e/a/c/k;

    if-eqz v0, :cond_b

    move-object v0, p0

    check-cast v0, La/e/a/c/k;

    invoke-virtual {v0}, La/e/a/c/k;->Y()La/e/a/c/e;

    move-result-object v2

    invoke-virtual {v0}, La/e/a/c/k;->Z()I

    move-result v0

    if-nez v0, :cond_a

    const/4 v1, 0x1

    :cond_a
    invoke-virtual {v2, v1, p2, p3}, La/e/a/c/e;->a(ILjava/util/ArrayList;La/e/a/c/a/q;)V

    :cond_b
    if-nez p1, :cond_c

    invoke-virtual {p3}, La/e/a/c/a/q;->a()I

    move-result v0

    iput v0, p0, La/e/a/c/g;->Sa:I

    iget-object v0, p0, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {v0, p1, p2, p3}, La/e/a/c/e;->a(ILjava/util/ArrayList;La/e/a/c/a/q;)V

    iget-object v0, p0, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {v0, p1, p2, p3}, La/e/a/c/e;->a(ILjava/util/ArrayList;La/e/a/c/a/q;)V

    goto :goto_5

    :cond_c
    invoke-virtual {p3}, La/e/a/c/a/q;->a()I

    move-result v0

    iput v0, p0, La/e/a/c/g;->Ta:I

    iget-object v0, p0, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {v0, p1, p2, p3}, La/e/a/c/e;->a(ILjava/util/ArrayList;La/e/a/c/a/q;)V

    iget-object v0, p0, La/e/a/c/g;->V:La/e/a/c/e;

    invoke-virtual {v0, p1, p2, p3}, La/e/a/c/e;->a(ILjava/util/ArrayList;La/e/a/c/a/q;)V

    iget-object v0, p0, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {v0, p1, p2, p3}, La/e/a/c/e;->a(ILjava/util/ArrayList;La/e/a/c/a/q;)V

    :goto_5
    iget-object p0, p0, La/e/a/c/g;->Y:La/e/a/c/e;

    invoke-virtual {p0, p1, p2, p3}, La/e/a/c/e;->a(ILjava/util/ArrayList;La/e/a/c/a/q;)V

    :cond_d
    return-object p3
.end method

.method private static a(Ljava/util/ArrayList;I)La/e/a/c/a/q;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "La/e/a/c/a/q;",
            ">;I)",
            "La/e/a/c/a/q;"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, La/e/a/c/a/q;

    iget v3, v2, La/e/a/c/a/q;->c:I

    if-ne p1, v3, :cond_0

    return-object v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static a(La/e/a/c/g$a;La/e/a/c/g$a;La/e/a/c/g$a;La/e/a/c/g$a;)Z
    .locals 3

    sget-object v0, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eq p2, v0, :cond_1

    sget-object v0, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-eq p2, v0, :cond_1

    sget-object v0, La/e/a/c/g$a;->d:La/e/a/c/g$a;

    if-ne p2, v0, :cond_0

    sget-object p2, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-eq p0, p2, :cond_0

    goto :goto_0

    :cond_0
    move p0, v1

    goto :goto_1

    :cond_1
    :goto_0
    move p0, v2

    :goto_1
    sget-object p2, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    if-eq p3, p2, :cond_3

    sget-object p2, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-eq p3, p2, :cond_3

    sget-object p2, La/e/a/c/g$a;->d:La/e/a/c/g$a;

    if-ne p3, p2, :cond_2

    sget-object p2, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-eq p1, p2, :cond_2

    goto :goto_2

    :cond_2
    move p1, v1

    goto :goto_3

    :cond_3
    :goto_2
    move p1, v2

    :goto_3
    if-nez p0, :cond_5

    if-eqz p1, :cond_4

    goto :goto_4

    :cond_4
    return v1

    :cond_5
    :goto_4
    return v2
.end method

.method public static a(La/e/a/c/h;La/e/a/c/a/b$b;)Z
    .locals 16

    move-object/from16 v0, p0

    invoke-virtual/range {p0 .. p0}, La/e/a/c/q;->Y()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v2, :cond_2

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/c/g;

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->n()La/e/a/c/g$a;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->z()La/e/a/c/g$a;

    move-result-object v7

    invoke-virtual {v5}, La/e/a/c/g;->n()La/e/a/c/g$a;

    move-result-object v8

    invoke-virtual {v5}, La/e/a/c/g;->z()La/e/a/c/g$a;

    move-result-object v9

    invoke-static {v6, v7, v8, v9}, La/e/a/c/a/i;->a(La/e/a/c/g$a;La/e/a/c/g$a;La/e/a/c/g$a;La/e/a/c/g$a;)Z

    move-result v6

    if-nez v6, :cond_0

    return v3

    :cond_0
    instance-of v5, v5, La/e/a/c/i;

    if-eqz v5, :cond_1

    return v3

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    iget-object v4, v0, La/e/a/c/h;->_a:La/e/a/e;

    if-eqz v4, :cond_3

    iget-wide v5, v4, La/e/a/e;->A:J

    const-wide/16 v7, 0x1

    add-long/2addr v5, v7

    iput-wide v5, v4, La/e/a/e;->A:J

    :cond_3
    move v5, v3

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    :goto_1
    if-ge v5, v2, :cond_14

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, La/e/a/c/g;

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->n()La/e/a/c/g$a;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->z()La/e/a/c/g$a;

    move-result-object v15

    invoke-virtual {v13}, La/e/a/c/g;->n()La/e/a/c/g$a;

    move-result-object v4

    invoke-virtual {v13}, La/e/a/c/g;->z()La/e/a/c/g$a;

    move-result-object v12

    invoke-static {v14, v15, v4, v12}, La/e/a/c/a/i;->a(La/e/a/c/g$a;La/e/a/c/g$a;La/e/a/c/g$a;La/e/a/c/g$a;)Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, v0, La/e/a/c/h;->yb:La/e/a/c/a/b$a;

    sget v12, La/e/a/c/a/b$a;->a:I

    move-object/from16 v14, p1

    invoke-static {v3, v13, v14, v4, v12}, La/e/a/c/h;->a(ILa/e/a/c/g;La/e/a/c/a/b$b;La/e/a/c/a/b$a;I)Z

    goto :goto_2

    :cond_4
    move-object/from16 v14, p1

    :goto_2
    instance-of v4, v13, La/e/a/c/k;

    if-eqz v4, :cond_8

    move-object v12, v13

    check-cast v12, La/e/a/c/k;

    invoke-virtual {v12}, La/e/a/c/k;->Z()I

    move-result v15

    if-nez v15, :cond_6

    if-nez v8, :cond_5

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    :cond_5
    invoke-virtual {v8, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    invoke-virtual {v12}, La/e/a/c/k;->Z()I

    move-result v15

    const/4 v3, 0x1

    if-ne v15, v3, :cond_8

    if-nez v6, :cond_7

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    :cond_7
    invoke-virtual {v6, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_8
    instance-of v3, v13, La/e/a/c/m;

    if-eqz v3, :cond_f

    instance-of v3, v13, La/e/a/c/a;

    if-eqz v3, :cond_c

    move-object v3, v13

    check-cast v3, La/e/a/c/a;

    invoke-virtual {v3}, La/e/a/c/a;->ca()I

    move-result v12

    if-nez v12, :cond_a

    if-nez v7, :cond_9

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    :cond_9
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_a
    invoke-virtual {v3}, La/e/a/c/a;->ca()I

    move-result v12

    const/4 v15, 0x1

    if-ne v12, v15, :cond_f

    if-nez v9, :cond_b

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    :cond_b
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_c
    move-object v3, v13

    check-cast v3, La/e/a/c/m;

    if-nez v7, :cond_d

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    :cond_d
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-nez v9, :cond_e

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    :cond_e
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_f
    :goto_3
    iget-object v3, v13, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v3, v3, La/e/a/c/e;->f:La/e/a/c/e;

    if-nez v3, :cond_11

    iget-object v3, v13, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v3, v3, La/e/a/c/e;->f:La/e/a/c/e;

    if-nez v3, :cond_11

    if-nez v4, :cond_11

    instance-of v3, v13, La/e/a/c/a;

    if-nez v3, :cond_11

    if-nez v10, :cond_10

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    :cond_10
    invoke-virtual {v10, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_11
    iget-object v3, v13, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v3, v3, La/e/a/c/e;->f:La/e/a/c/e;

    if-nez v3, :cond_13

    iget-object v3, v13, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object v3, v3, La/e/a/c/e;->f:La/e/a/c/e;

    if-nez v3, :cond_13

    iget-object v3, v13, La/e/a/c/g;->V:La/e/a/c/e;

    iget-object v3, v3, La/e/a/c/e;->f:La/e/a/c/e;

    if-nez v3, :cond_13

    if-nez v4, :cond_13

    instance-of v3, v13, La/e/a/c/a;

    if-nez v3, :cond_13

    if-nez v11, :cond_12

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    :cond_12
    invoke-virtual {v11, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_13
    add-int/lit8 v5, v5, 0x1

    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_14
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    if-eqz v6, :cond_15

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_15

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/c/k;

    const/4 v6, 0x0

    const/4 v12, 0x0

    invoke-static {v5, v6, v3, v12}, La/e/a/c/a/i;->a(La/e/a/c/g;ILjava/util/ArrayList;La/e/a/c/a/q;)La/e/a/c/a/q;

    goto :goto_4

    :cond_15
    const/4 v6, 0x0

    const/4 v12, 0x0

    if-eqz v7, :cond_16

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_16

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/c/m;

    invoke-static {v5, v6, v3, v12}, La/e/a/c/a/i;->a(La/e/a/c/g;ILjava/util/ArrayList;La/e/a/c/a/q;)La/e/a/c/a/q;

    move-result-object v7

    invoke-virtual {v5, v3, v6, v7}, La/e/a/c/m;->a(Ljava/util/ArrayList;ILa/e/a/c/a/q;)V

    invoke-virtual {v7, v3}, La/e/a/c/a/q;->a(Ljava/util/ArrayList;)V

    const/4 v6, 0x0

    const/4 v12, 0x0

    goto :goto_5

    :cond_16
    sget-object v4, La/e/a/c/e$a;->b:La/e/a/c/e$a;

    invoke-virtual {v0, v4}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v4

    invoke-virtual {v4}, La/e/a/c/e;->a()Ljava/util/HashSet;

    move-result-object v5

    if-eqz v5, :cond_17

    invoke-virtual {v4}, La/e/a/c/e;->a()Ljava/util/HashSet;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_17

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/c/e;

    iget-object v5, v5, La/e/a/c/e;->d:La/e/a/c/g;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v5, v6, v3, v7}, La/e/a/c/a/i;->a(La/e/a/c/g;ILjava/util/ArrayList;La/e/a/c/a/q;)La/e/a/c/a/q;

    goto :goto_6

    :cond_17
    sget-object v4, La/e/a/c/e$a;->d:La/e/a/c/e$a;

    invoke-virtual {v0, v4}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v4

    invoke-virtual {v4}, La/e/a/c/e;->a()Ljava/util/HashSet;

    move-result-object v5

    if-eqz v5, :cond_18

    invoke-virtual {v4}, La/e/a/c/e;->a()Ljava/util/HashSet;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_18

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/c/e;

    iget-object v5, v5, La/e/a/c/e;->d:La/e/a/c/g;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v5, v6, v3, v7}, La/e/a/c/a/i;->a(La/e/a/c/g;ILjava/util/ArrayList;La/e/a/c/a/q;)La/e/a/c/a/q;

    goto :goto_7

    :cond_18
    sget-object v4, La/e/a/c/e$a;->g:La/e/a/c/e$a;

    invoke-virtual {v0, v4}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v4

    invoke-virtual {v4}, La/e/a/c/e;->a()Ljava/util/HashSet;

    move-result-object v5

    if-eqz v5, :cond_19

    invoke-virtual {v4}, La/e/a/c/e;->a()Ljava/util/HashSet;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_19

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/c/e;

    iget-object v5, v5, La/e/a/c/e;->d:La/e/a/c/g;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v5, v6, v3, v7}, La/e/a/c/a/i;->a(La/e/a/c/g;ILjava/util/ArrayList;La/e/a/c/a/q;)La/e/a/c/a/q;

    goto :goto_8

    :cond_19
    const/4 v6, 0x0

    const/4 v7, 0x0

    if-eqz v10, :cond_1a

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_9
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/c/g;

    invoke-static {v5, v6, v3, v7}, La/e/a/c/a/i;->a(La/e/a/c/g;ILjava/util/ArrayList;La/e/a/c/a/q;)La/e/a/c/a/q;

    goto :goto_9

    :cond_1a
    if-eqz v8, :cond_1b

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/c/k;

    const/4 v6, 0x1

    invoke-static {v5, v6, v3, v7}, La/e/a/c/a/i;->a(La/e/a/c/g;ILjava/util/ArrayList;La/e/a/c/a/q;)La/e/a/c/a/q;

    goto :goto_a

    :cond_1b
    const/4 v6, 0x1

    if-eqz v9, :cond_1c

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_b
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/c/m;

    invoke-static {v5, v6, v3, v7}, La/e/a/c/a/i;->a(La/e/a/c/g;ILjava/util/ArrayList;La/e/a/c/a/q;)La/e/a/c/a/q;

    move-result-object v8

    invoke-virtual {v5, v3, v6, v8}, La/e/a/c/m;->a(Ljava/util/ArrayList;ILa/e/a/c/a/q;)V

    invoke-virtual {v8, v3}, La/e/a/c/a/q;->a(Ljava/util/ArrayList;)V

    const/4 v6, 0x1

    const/4 v7, 0x0

    goto :goto_b

    :cond_1c
    sget-object v4, La/e/a/c/e$a;->c:La/e/a/c/e$a;

    invoke-virtual {v0, v4}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v4

    invoke-virtual {v4}, La/e/a/c/e;->a()Ljava/util/HashSet;

    move-result-object v5

    if-eqz v5, :cond_1d

    invoke-virtual {v4}, La/e/a/c/e;->a()Ljava/util/HashSet;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_c
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1d

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/c/e;

    iget-object v5, v5, La/e/a/c/e;->d:La/e/a/c/g;

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-static {v5, v6, v3, v7}, La/e/a/c/a/i;->a(La/e/a/c/g;ILjava/util/ArrayList;La/e/a/c/a/q;)La/e/a/c/a/q;

    goto :goto_c

    :cond_1d
    sget-object v4, La/e/a/c/e$a;->f:La/e/a/c/e$a;

    invoke-virtual {v0, v4}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v4

    invoke-virtual {v4}, La/e/a/c/e;->a()Ljava/util/HashSet;

    move-result-object v5

    if-eqz v5, :cond_1e

    invoke-virtual {v4}, La/e/a/c/e;->a()Ljava/util/HashSet;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_d
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1e

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/c/e;

    iget-object v5, v5, La/e/a/c/e;->d:La/e/a/c/g;

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-static {v5, v6, v3, v7}, La/e/a/c/a/i;->a(La/e/a/c/g;ILjava/util/ArrayList;La/e/a/c/a/q;)La/e/a/c/a/q;

    goto :goto_d

    :cond_1e
    sget-object v4, La/e/a/c/e$a;->e:La/e/a/c/e$a;

    invoke-virtual {v0, v4}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v4

    invoke-virtual {v4}, La/e/a/c/e;->a()Ljava/util/HashSet;

    move-result-object v5

    if-eqz v5, :cond_1f

    invoke-virtual {v4}, La/e/a/c/e;->a()Ljava/util/HashSet;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_e
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/c/e;

    iget-object v5, v5, La/e/a/c/e;->d:La/e/a/c/g;

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-static {v5, v6, v3, v7}, La/e/a/c/a/i;->a(La/e/a/c/g;ILjava/util/ArrayList;La/e/a/c/a/q;)La/e/a/c/a/q;

    goto :goto_e

    :cond_1f
    sget-object v4, La/e/a/c/e$a;->g:La/e/a/c/e$a;

    invoke-virtual {v0, v4}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v4

    invoke-virtual {v4}, La/e/a/c/e;->a()Ljava/util/HashSet;

    move-result-object v5

    if-eqz v5, :cond_20

    invoke-virtual {v4}, La/e/a/c/e;->a()Ljava/util/HashSet;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_f
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_20

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/c/e;

    iget-object v5, v5, La/e/a/c/e;->d:La/e/a/c/g;

    const/4 v6, 0x1

    const/4 v12, 0x0

    invoke-static {v5, v6, v3, v12}, La/e/a/c/a/i;->a(La/e/a/c/g;ILjava/util/ArrayList;La/e/a/c/a/q;)La/e/a/c/a/q;

    goto :goto_f

    :cond_20
    const/4 v6, 0x1

    const/4 v12, 0x0

    if-eqz v11, :cond_21

    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_10
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_21

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/c/g;

    invoke-static {v5, v6, v3, v12}, La/e/a/c/a/i;->a(La/e/a/c/g;ILjava/util/ArrayList;La/e/a/c/a/q;)La/e/a/c/a/q;

    goto :goto_10

    :cond_21
    const/4 v4, 0x0

    :goto_11
    if-ge v4, v2, :cond_23

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/c/g;

    invoke-virtual {v5}, La/e/a/c/g;->T()Z

    move-result v6

    if-eqz v6, :cond_22

    iget v6, v5, La/e/a/c/g;->Sa:I

    invoke-static {v3, v6}, La/e/a/c/a/i;->a(Ljava/util/ArrayList;I)La/e/a/c/a/q;

    move-result-object v6

    iget v5, v5, La/e/a/c/g;->Ta:I

    invoke-static {v3, v5}, La/e/a/c/a/i;->a(Ljava/util/ArrayList;I)La/e/a/c/a/q;

    move-result-object v5

    if-eqz v6, :cond_22

    if-eqz v5, :cond_22

    const/4 v7, 0x0

    invoke-virtual {v6, v7, v5}, La/e/a/c/a/q;->a(ILa/e/a/c/a/q;)V

    const/4 v7, 0x2

    invoke-virtual {v5, v7}, La/e/a/c/a/q;->a(I)V

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_22
    add-int/lit8 v4, v4, 0x1

    goto :goto_11

    :cond_23
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_24

    const/4 v1, 0x0

    return v1

    :cond_24
    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->n()La/e/a/c/g$a;

    move-result-object v1

    sget-object v2, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v1, v2, :cond_28

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    move-object v4, v12

    const/4 v2, 0x0

    :cond_25
    :goto_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_27

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/c/a/q;

    invoke-virtual {v5}, La/e/a/c/a/q;->b()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_26

    goto :goto_12

    :cond_26
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, La/e/a/c/a/q;->a(Z)V

    invoke-virtual/range {p0 .. p0}, La/e/a/c/h;->da()La/e/a/d;

    move-result-object v7

    invoke-virtual {v5, v7, v6}, La/e/a/c/a/q;->a(La/e/a/d;I)I

    move-result v7

    if-le v7, v2, :cond_25

    move-object v4, v5

    move v2, v7

    goto :goto_12

    :cond_27
    if-eqz v4, :cond_28

    sget-object v1, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    invoke-virtual {v0, v1}, La/e/a/c/g;->a(La/e/a/c/g$a;)V

    invoke-virtual {v0, v2}, La/e/a/c/g;->u(I)V

    const/4 v1, 0x1

    invoke-virtual {v4, v1}, La/e/a/c/a/q;->a(Z)V

    goto :goto_13

    :cond_28
    move-object v4, v12

    :goto_13
    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->z()La/e/a/c/g$a;

    move-result-object v1

    sget-object v2, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v1, v2, :cond_2c

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    move-object v2, v12

    const/4 v3, 0x0

    :cond_29
    :goto_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/c/a/q;

    invoke-virtual {v5}, La/e/a/c/a/q;->b()I

    move-result v6

    if-nez v6, :cond_2a

    goto :goto_14

    :cond_2a
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, La/e/a/c/a/q;->a(Z)V

    invoke-virtual/range {p0 .. p0}, La/e/a/c/h;->da()La/e/a/d;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v5, v7, v8}, La/e/a/c/a/q;->a(La/e/a/d;I)I

    move-result v7

    if-le v7, v3, :cond_29

    move-object v2, v5

    move v3, v7

    goto :goto_14

    :cond_2b
    const/4 v6, 0x0

    const/4 v8, 0x1

    if-eqz v2, :cond_2d

    sget-object v1, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    invoke-virtual {v0, v1}, La/e/a/c/g;->b(La/e/a/c/g$a;)V

    invoke-virtual {v0, v3}, La/e/a/c/g;->m(I)V

    invoke-virtual {v2, v8}, La/e/a/c/a/q;->a(Z)V

    goto :goto_15

    :cond_2c
    const/4 v6, 0x0

    const/4 v8, 0x1

    :cond_2d
    move-object v2, v12

    :goto_15
    if-nez v4, :cond_2e

    if-eqz v2, :cond_2f

    :cond_2e
    move v6, v8

    :cond_2f
    return v6
.end method
