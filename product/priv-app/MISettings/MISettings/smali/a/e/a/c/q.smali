.class public La/e/a/c/q;
.super La/e/a/c/g;


# instance fields
.field public Ua:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "La/e/a/c/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, La/e/a/c/g;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public U()V
    .locals 1

    iget-object v0, p0, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-super {p0}, La/e/a/c/g;->U()V

    return-void
.end method

.method public Y()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "La/e/a/c/g;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    return-object v0
.end method

.method public Z()V
    .locals 4

    iget-object v0, p0, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    iget-object v2, p0, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, La/e/a/c/g;

    instance-of v3, v2, La/e/a/c/q;

    if-eqz v3, :cond_1

    check-cast v2, La/e/a/c/q;

    invoke-virtual {v2}, La/e/a/c/q;->Z()V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public a(La/e/a/c/g;)V
    .locals 1

    iget-object v0, p0, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, La/e/a/c/g;->v()La/e/a/c/g;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, La/e/a/c/g;->v()La/e/a/c/g;

    move-result-object v0

    check-cast v0, La/e/a/c/q;

    invoke-virtual {v0, p1}, La/e/a/c/q;->c(La/e/a/c/g;)V

    :cond_0
    invoke-virtual {p1, p0}, La/e/a/c/g;->b(La/e/a/c/g;)V

    return-void
.end method

.method public a(La/e/a/c;)V
    .locals 3

    invoke-super {p0, p1}, La/e/a/c/g;->a(La/e/a/c;)V

    iget-object v0, p0, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, La/e/a/c/g;

    invoke-virtual {v2, p1}, La/e/a/c/g;->a(La/e/a/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public aa()V
    .locals 1

    iget-object v0, p0, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public c(La/e/a/c/g;)V
    .locals 1

    iget-object v0, p0, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p1}, La/e/a/c/g;->U()V

    return-void
.end method
