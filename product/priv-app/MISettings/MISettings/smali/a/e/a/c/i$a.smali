.class La/e/a/c/i$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = La/e/a/c/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field private a:I

.field private b:La/e/a/c/g;

.field c:I

.field private d:La/e/a/c/e;

.field private e:La/e/a/c/e;

.field private f:La/e/a/c/e;

.field private g:La/e/a/c/e;

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field final synthetic r:La/e/a/c/i;


# direct methods
.method public constructor <init>(La/e/a/c/i;ILa/e/a/c/e;La/e/a/c/e;La/e/a/c/e;La/e/a/c/e;I)V
    .locals 2

    iput-object p1, p0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, La/e/a/c/i$a;->a:I

    const/4 v1, 0x0

    iput-object v1, p0, La/e/a/c/i$a;->b:La/e/a/c/g;

    iput v0, p0, La/e/a/c/i$a;->c:I

    iput v0, p0, La/e/a/c/i$a;->h:I

    iput v0, p0, La/e/a/c/i$a;->i:I

    iput v0, p0, La/e/a/c/i$a;->j:I

    iput v0, p0, La/e/a/c/i$a;->k:I

    iput v0, p0, La/e/a/c/i$a;->l:I

    iput v0, p0, La/e/a/c/i$a;->m:I

    iput v0, p0, La/e/a/c/i$a;->n:I

    iput v0, p0, La/e/a/c/i$a;->o:I

    iput v0, p0, La/e/a/c/i$a;->p:I

    iput v0, p0, La/e/a/c/i$a;->q:I

    iput p2, p0, La/e/a/c/i$a;->a:I

    iput-object p3, p0, La/e/a/c/i$a;->d:La/e/a/c/e;

    iput-object p4, p0, La/e/a/c/i$a;->e:La/e/a/c/e;

    iput-object p5, p0, La/e/a/c/i$a;->f:La/e/a/c/e;

    iput-object p6, p0, La/e/a/c/i$a;->g:La/e/a/c/e;

    invoke-virtual {p1}, La/e/a/c/p;->ca()I

    move-result p2

    iput p2, p0, La/e/a/c/i$a;->h:I

    invoke-virtual {p1}, La/e/a/c/p;->ea()I

    move-result p2

    iput p2, p0, La/e/a/c/i$a;->i:I

    invoke-virtual {p1}, La/e/a/c/p;->da()I

    move-result p2

    iput p2, p0, La/e/a/c/i$a;->j:I

    invoke-virtual {p1}, La/e/a/c/p;->ba()I

    move-result p1

    iput p1, p0, La/e/a/c/i$a;->k:I

    iput p7, p0, La/e/a/c/i$a;->q:I

    return-void
.end method

.method static synthetic a(La/e/a/c/i$a;)La/e/a/c/g;
    .locals 0

    iget-object p0, p0, La/e/a/c/i$a;->b:La/e/a/c/g;

    return-object p0
.end method

.method private d()V
    .locals 9

    const/4 v0, 0x0

    iput v0, p0, La/e/a/c/i$a;->l:I

    iput v0, p0, La/e/a/c/i$a;->m:I

    const/4 v1, 0x0

    iput-object v1, p0, La/e/a/c/i$a;->b:La/e/a/c/g;

    iput v0, p0, La/e/a/c/i$a;->c:I

    iget v1, p0, La/e/a/c/i$a;->o:I

    move v2, v0

    :goto_0
    if-ge v2, v1, :cond_7

    iget v3, p0, La/e/a/c/i$a;->n:I

    add-int/2addr v3, v2

    iget-object v4, p0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v4}, La/e/a/c/i;->m(La/e/a/c/i;)I

    move-result v4

    if-lt v3, v4, :cond_0

    goto/16 :goto_2

    :cond_0
    iget-object v3, p0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v3}, La/e/a/c/i;->n(La/e/a/c/i;)[La/e/a/c/g;

    move-result-object v3

    iget v4, p0, La/e/a/c/i$a;->n:I

    add-int/2addr v4, v2

    aget-object v3, v3, v4

    iget v4, p0, La/e/a/c/i$a;->a:I

    const/16 v5, 0x8

    if-nez v4, :cond_3

    invoke-virtual {v3}, La/e/a/c/g;->C()I

    move-result v4

    iget-object v6, p0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v6}, La/e/a/c/i;->a(La/e/a/c/i;)I

    move-result v6

    invoke-virtual {v3}, La/e/a/c/g;->B()I

    move-result v7

    if-ne v7, v5, :cond_1

    move v6, v0

    :cond_1
    iget v5, p0, La/e/a/c/i$a;->l:I

    add-int/2addr v4, v6

    add-int/2addr v5, v4

    iput v5, p0, La/e/a/c/i$a;->l:I

    iget-object v4, p0, La/e/a/c/i$a;->r:La/e/a/c/i;

    iget v5, p0, La/e/a/c/i$a;->q:I

    invoke-static {v4, v3, v5}, La/e/a/c/i;->b(La/e/a/c/i;La/e/a/c/g;I)I

    move-result v4

    iget-object v5, p0, La/e/a/c/i$a;->b:La/e/a/c/g;

    if-eqz v5, :cond_2

    iget v5, p0, La/e/a/c/i$a;->c:I

    if-ge v5, v4, :cond_6

    :cond_2
    iput-object v3, p0, La/e/a/c/i$a;->b:La/e/a/c/g;

    iput v4, p0, La/e/a/c/i$a;->c:I

    iput v4, p0, La/e/a/c/i$a;->m:I

    goto :goto_1

    :cond_3
    iget-object v4, p0, La/e/a/c/i$a;->r:La/e/a/c/i;

    iget v6, p0, La/e/a/c/i$a;->q:I

    invoke-static {v4, v3, v6}, La/e/a/c/i;->a(La/e/a/c/i;La/e/a/c/g;I)I

    move-result v4

    iget-object v6, p0, La/e/a/c/i$a;->r:La/e/a/c/i;

    iget v7, p0, La/e/a/c/i$a;->q:I

    invoke-static {v6, v3, v7}, La/e/a/c/i;->b(La/e/a/c/i;La/e/a/c/g;I)I

    move-result v6

    iget-object v7, p0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v7}, La/e/a/c/i;->b(La/e/a/c/i;)I

    move-result v7

    invoke-virtual {v3}, La/e/a/c/g;->B()I

    move-result v8

    if-ne v8, v5, :cond_4

    move v7, v0

    :cond_4
    iget v5, p0, La/e/a/c/i$a;->m:I

    add-int/2addr v6, v7

    add-int/2addr v5, v6

    iput v5, p0, La/e/a/c/i$a;->m:I

    iget-object v5, p0, La/e/a/c/i$a;->b:La/e/a/c/g;

    if-eqz v5, :cond_5

    iget v5, p0, La/e/a/c/i$a;->c:I

    if-ge v5, v4, :cond_6

    :cond_5
    iput-object v3, p0, La/e/a/c/i$a;->b:La/e/a/c/g;

    iput v4, p0, La/e/a/c/i$a;->c:I

    iput v4, p0, La/e/a/c/i$a;->l:I

    :cond_6
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_7
    :goto_2
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, La/e/a/c/i$a;->c:I

    const/4 v1, 0x0

    iput-object v1, p0, La/e/a/c/i$a;->b:La/e/a/c/g;

    iput v0, p0, La/e/a/c/i$a;->l:I

    iput v0, p0, La/e/a/c/i$a;->m:I

    iput v0, p0, La/e/a/c/i$a;->n:I

    iput v0, p0, La/e/a/c/i$a;->o:I

    iput v0, p0, La/e/a/c/i$a;->p:I

    return-void
.end method

.method public a(I)V
    .locals 8

    iget v0, p0, La/e/a/c/i$a;->p:I

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget v1, p0, La/e/a/c/i$a;->o:I

    div-int/2addr p1, v0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_4

    iget v2, p0, La/e/a/c/i$a;->n:I

    add-int/2addr v2, v0

    iget-object v3, p0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v3}, La/e/a/c/i;->m(La/e/a/c/i;)I

    move-result v3

    if-lt v2, v3, :cond_1

    goto :goto_2

    :cond_1
    iget-object v2, p0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v2}, La/e/a/c/i;->n(La/e/a/c/i;)[La/e/a/c/g;

    move-result-object v2

    iget v3, p0, La/e/a/c/i$a;->n:I

    add-int/2addr v3, v0

    aget-object v3, v2, v3

    iget v2, p0, La/e/a/c/i$a;->a:I

    if-nez v2, :cond_2

    if-eqz v3, :cond_3

    invoke-virtual {v3}, La/e/a/c/g;->n()La/e/a/c/g$a;

    move-result-object v2

    sget-object v4, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v2, v4, :cond_3

    iget v2, v3, La/e/a/c/g;->x:I

    if-nez v2, :cond_3

    iget-object v2, p0, La/e/a/c/i$a;->r:La/e/a/c/i;

    sget-object v4, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    invoke-virtual {v3}, La/e/a/c/g;->z()La/e/a/c/g$a;

    move-result-object v6

    invoke-virtual {v3}, La/e/a/c/g;->k()I

    move-result v7

    move v5, p1

    invoke-virtual/range {v2 .. v7}, La/e/a/c/p;->a(La/e/a/c/g;La/e/a/c/g$a;ILa/e/a/c/g$a;I)V

    goto :goto_1

    :cond_2
    if-eqz v3, :cond_3

    invoke-virtual {v3}, La/e/a/c/g;->z()La/e/a/c/g$a;

    move-result-object v2

    sget-object v4, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v2, v4, :cond_3

    iget v2, v3, La/e/a/c/g;->y:I

    if-nez v2, :cond_3

    iget-object v2, p0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-virtual {v3}, La/e/a/c/g;->n()La/e/a/c/g$a;

    move-result-object v4

    invoke-virtual {v3}, La/e/a/c/g;->C()I

    move-result v5

    sget-object v6, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    move v7, p1

    invoke-virtual/range {v2 .. v7}, La/e/a/c/p;->a(La/e/a/c/g;La/e/a/c/g$a;ILa/e/a/c/g$a;I)V

    :cond_3
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    :goto_2
    invoke-direct {p0}, La/e/a/c/i$a;->d()V

    return-void
.end method

.method public a(ILa/e/a/c/e;La/e/a/c/e;La/e/a/c/e;La/e/a/c/e;IIIII)V
    .locals 0

    iput p1, p0, La/e/a/c/i$a;->a:I

    iput-object p2, p0, La/e/a/c/i$a;->d:La/e/a/c/e;

    iput-object p3, p0, La/e/a/c/i$a;->e:La/e/a/c/e;

    iput-object p4, p0, La/e/a/c/i$a;->f:La/e/a/c/e;

    iput-object p5, p0, La/e/a/c/i$a;->g:La/e/a/c/e;

    iput p6, p0, La/e/a/c/i$a;->h:I

    iput p7, p0, La/e/a/c/i$a;->i:I

    iput p8, p0, La/e/a/c/i$a;->j:I

    iput p9, p0, La/e/a/c/i$a;->k:I

    iput p10, p0, La/e/a/c/i$a;->q:I

    return-void
.end method

.method public a(La/e/a/c/g;)V
    .locals 6

    iget v0, p0, La/e/a/c/i$a;->a:I

    const/16 v1, 0x8

    const/4 v2, 0x0

    if-nez v0, :cond_3

    iget-object v0, p0, La/e/a/c/i$a;->r:La/e/a/c/i;

    iget v3, p0, La/e/a/c/i$a;->q:I

    invoke-static {v0, p1, v3}, La/e/a/c/i;->a(La/e/a/c/i;La/e/a/c/g;I)I

    move-result v0

    invoke-virtual {p1}, La/e/a/c/g;->n()La/e/a/c/g$a;

    move-result-object v3

    sget-object v4, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v3, v4, :cond_0

    iget v0, p0, La/e/a/c/i$a;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, La/e/a/c/i$a;->p:I

    move v0, v2

    :cond_0
    iget-object v3, p0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v3}, La/e/a/c/i;->a(La/e/a/c/i;)I

    move-result v3

    invoke-virtual {p1}, La/e/a/c/g;->B()I

    move-result v4

    if-ne v4, v1, :cond_1

    goto :goto_0

    :cond_1
    move v2, v3

    :goto_0
    iget v1, p0, La/e/a/c/i$a;->l:I

    add-int/2addr v0, v2

    add-int/2addr v1, v0

    iput v1, p0, La/e/a/c/i$a;->l:I

    iget-object v0, p0, La/e/a/c/i$a;->r:La/e/a/c/i;

    iget v1, p0, La/e/a/c/i$a;->q:I

    invoke-static {v0, p1, v1}, La/e/a/c/i;->b(La/e/a/c/i;La/e/a/c/g;I)I

    move-result v0

    iget-object v1, p0, La/e/a/c/i$a;->b:La/e/a/c/g;

    if-eqz v1, :cond_2

    iget v1, p0, La/e/a/c/i$a;->c:I

    if-ge v1, v0, :cond_7

    :cond_2
    iput-object p1, p0, La/e/a/c/i$a;->b:La/e/a/c/g;

    iput v0, p0, La/e/a/c/i$a;->c:I

    iput v0, p0, La/e/a/c/i$a;->m:I

    goto :goto_2

    :cond_3
    iget-object v0, p0, La/e/a/c/i$a;->r:La/e/a/c/i;

    iget v3, p0, La/e/a/c/i$a;->q:I

    invoke-static {v0, p1, v3}, La/e/a/c/i;->a(La/e/a/c/i;La/e/a/c/g;I)I

    move-result v0

    iget-object v3, p0, La/e/a/c/i$a;->r:La/e/a/c/i;

    iget v4, p0, La/e/a/c/i$a;->q:I

    invoke-static {v3, p1, v4}, La/e/a/c/i;->b(La/e/a/c/i;La/e/a/c/g;I)I

    move-result v3

    invoke-virtual {p1}, La/e/a/c/g;->z()La/e/a/c/g$a;

    move-result-object v4

    sget-object v5, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v4, v5, :cond_4

    iget v3, p0, La/e/a/c/i$a;->p:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, La/e/a/c/i$a;->p:I

    move v3, v2

    :cond_4
    iget-object v4, p0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v4}, La/e/a/c/i;->b(La/e/a/c/i;)I

    move-result v4

    invoke-virtual {p1}, La/e/a/c/g;->B()I

    move-result v5

    if-ne v5, v1, :cond_5

    goto :goto_1

    :cond_5
    move v2, v4

    :goto_1
    iget v1, p0, La/e/a/c/i$a;->m:I

    add-int/2addr v3, v2

    add-int/2addr v1, v3

    iput v1, p0, La/e/a/c/i$a;->m:I

    iget-object v1, p0, La/e/a/c/i$a;->b:La/e/a/c/g;

    if-eqz v1, :cond_6

    iget v1, p0, La/e/a/c/i$a;->c:I

    if-ge v1, v0, :cond_7

    :cond_6
    iput-object p1, p0, La/e/a/c/i$a;->b:La/e/a/c/g;

    iput v0, p0, La/e/a/c/i$a;->c:I

    iput v0, p0, La/e/a/c/i$a;->l:I

    :cond_7
    :goto_2
    iget p1, p0, La/e/a/c/i$a;->o:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, La/e/a/c/i$a;->o:I

    return-void
.end method

.method public a(ZIZ)V
    .locals 16

    move-object/from16 v0, p0

    iget v1, v0, La/e/a/c/i$a;->o:I

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_2

    iget v4, v0, La/e/a/c/i$a;->n:I

    add-int/2addr v4, v3

    iget-object v5, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v5}, La/e/a/c/i;->m(La/e/a/c/i;)I

    move-result v5

    if-lt v4, v5, :cond_0

    goto :goto_1

    :cond_0
    iget-object v4, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v4}, La/e/a/c/i;->n(La/e/a/c/i;)[La/e/a/c/g;

    move-result-object v4

    iget v5, v0, La/e/a/c/i$a;->n:I

    add-int/2addr v5, v3

    aget-object v4, v4, v5

    if-eqz v4, :cond_1

    invoke-virtual {v4}, La/e/a/c/g;->W()V

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    if-eqz v1, :cond_3a

    iget-object v3, v0, La/e/a/c/i$a;->b:La/e/a/c/g;

    if-nez v3, :cond_3

    goto/16 :goto_17

    :cond_3
    if-eqz p3, :cond_4

    if-nez p2, :cond_4

    const/4 v4, 0x1

    goto :goto_2

    :cond_4
    move v4, v2

    :goto_2
    const/4 v5, -0x1

    move v6, v2

    move v7, v5

    move v8, v7

    :goto_3
    if-ge v6, v1, :cond_9

    if-eqz p1, :cond_5

    add-int/lit8 v9, v1, -0x1

    sub-int/2addr v9, v6

    goto :goto_4

    :cond_5
    move v9, v6

    :goto_4
    iget v10, v0, La/e/a/c/i$a;->n:I

    add-int/2addr v10, v9

    iget-object v11, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v11}, La/e/a/c/i;->m(La/e/a/c/i;)I

    move-result v11

    if-lt v10, v11, :cond_6

    goto :goto_5

    :cond_6
    iget-object v10, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v10}, La/e/a/c/i;->n(La/e/a/c/i;)[La/e/a/c/g;

    move-result-object v10

    iget v11, v0, La/e/a/c/i$a;->n:I

    add-int/2addr v11, v9

    aget-object v9, v10, v11

    if-eqz v9, :cond_8

    invoke-virtual {v9}, La/e/a/c/g;->B()I

    move-result v9

    if-nez v9, :cond_8

    if-ne v7, v5, :cond_7

    move v7, v6

    :cond_7
    move v8, v6

    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_9
    :goto_5
    const/4 v6, 0x0

    iget v9, v0, La/e/a/c/i$a;->a:I

    if-nez v9, :cond_23

    iget-object v9, v0, La/e/a/c/i$a;->b:La/e/a/c/g;

    iget-object v10, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v10}, La/e/a/c/i;->o(La/e/a/c/i;)I

    move-result v10

    invoke-virtual {v9, v10}, La/e/a/c/g;->s(I)V

    iget v10, v0, La/e/a/c/i$a;->i:I

    if-lez p2, :cond_a

    iget-object v11, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v11}, La/e/a/c/i;->b(La/e/a/c/i;)I

    move-result v11

    add-int/2addr v10, v11

    :cond_a
    iget-object v11, v9, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v12, v0, La/e/a/c/i$a;->e:La/e/a/c/e;

    invoke-virtual {v11, v12, v10}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    if-eqz p3, :cond_b

    iget-object v10, v9, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object v11, v0, La/e/a/c/i$a;->g:La/e/a/c/e;

    iget v12, v0, La/e/a/c/i$a;->k:I

    invoke-virtual {v10, v11, v12}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    :cond_b
    if-lez p2, :cond_c

    iget-object v10, v0, La/e/a/c/i$a;->e:La/e/a/c/e;

    iget-object v10, v10, La/e/a/c/e;->d:La/e/a/c/g;

    iget-object v10, v10, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object v11, v9, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {v10, v11, v2}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    :cond_c
    iget-object v10, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v10}, La/e/a/c/i;->p(La/e/a/c/i;)I

    move-result v10

    const/4 v11, 0x3

    if-ne v10, v11, :cond_10

    invoke-virtual {v9}, La/e/a/c/g;->F()Z

    move-result v10

    if-nez v10, :cond_10

    move v10, v2

    :goto_6
    if-ge v10, v1, :cond_10

    if-eqz p1, :cond_d

    add-int/lit8 v12, v1, -0x1

    sub-int/2addr v12, v10

    goto :goto_7

    :cond_d
    move v12, v10

    :goto_7
    iget v13, v0, La/e/a/c/i$a;->n:I

    add-int/2addr v13, v12

    iget-object v14, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v14}, La/e/a/c/i;->m(La/e/a/c/i;)I

    move-result v14

    if-lt v13, v14, :cond_e

    goto :goto_8

    :cond_e
    iget-object v13, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v13}, La/e/a/c/i;->n(La/e/a/c/i;)[La/e/a/c/g;

    move-result-object v13

    iget v14, v0, La/e/a/c/i$a;->n:I

    add-int/2addr v14, v12

    aget-object v12, v13, v14

    invoke-virtual {v12}, La/e/a/c/g;->F()Z

    move-result v13

    if-eqz v13, :cond_f

    goto :goto_9

    :cond_f
    add-int/lit8 v10, v10, 0x1

    goto :goto_6

    :cond_10
    :goto_8
    move-object v12, v9

    :goto_9
    move-object v10, v6

    move v6, v2

    :goto_a
    if-ge v6, v1, :cond_3a

    if-eqz p1, :cond_11

    add-int/lit8 v13, v1, -0x1

    sub-int/2addr v13, v6

    goto :goto_b

    :cond_11
    move v13, v6

    :goto_b
    iget v14, v0, La/e/a/c/i$a;->n:I

    add-int/2addr v14, v13

    iget-object v15, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v15}, La/e/a/c/i;->m(La/e/a/c/i;)I

    move-result v15

    if-lt v14, v15, :cond_12

    goto/16 :goto_17

    :cond_12
    iget-object v14, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v14}, La/e/a/c/i;->n(La/e/a/c/i;)[La/e/a/c/g;

    move-result-object v14

    iget v15, v0, La/e/a/c/i$a;->n:I

    add-int/2addr v15, v13

    aget-object v14, v14, v15

    if-nez v14, :cond_13

    move-object v14, v10

    move v10, v11

    goto/16 :goto_10

    :cond_13
    if-nez v6, :cond_14

    iget-object v15, v14, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v11, v0, La/e/a/c/i$a;->d:La/e/a/c/e;

    iget v3, v0, La/e/a/c/i$a;->h:I

    invoke-virtual {v14, v15, v11, v3}, La/e/a/c/g;->a(La/e/a/c/e;La/e/a/c/e;I)V

    :cond_14
    if-nez v13, :cond_1a

    iget-object v3, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v3}, La/e/a/c/i;->q(La/e/a/c/i;)I

    move-result v3

    const/high16 v11, 0x3f800000    # 1.0f

    if-eqz p1, :cond_15

    iget-object v13, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v13}, La/e/a/c/i;->r(La/e/a/c/i;)F

    move-result v13

    sub-float v13, v11, v13

    goto :goto_c

    :cond_15
    iget-object v13, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v13}, La/e/a/c/i;->r(La/e/a/c/i;)F

    move-result v13

    :goto_c
    iget v15, v0, La/e/a/c/i$a;->n:I

    if-nez v15, :cond_17

    iget-object v15, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v15}, La/e/a/c/i;->c(La/e/a/c/i;)I

    move-result v15

    if-eq v15, v5, :cond_17

    iget-object v3, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v3}, La/e/a/c/i;->c(La/e/a/c/i;)I

    move-result v3

    if-eqz p1, :cond_16

    iget-object v13, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v13}, La/e/a/c/i;->d(La/e/a/c/i;)F

    move-result v13

    :goto_d
    sub-float/2addr v11, v13

    goto :goto_e

    :cond_16
    iget-object v11, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v11}, La/e/a/c/i;->d(La/e/a/c/i;)F

    move-result v11

    :goto_e
    move v13, v11

    goto :goto_f

    :cond_17
    if-eqz p3, :cond_19

    iget-object v15, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v15}, La/e/a/c/i;->e(La/e/a/c/i;)I

    move-result v15

    if-eq v15, v5, :cond_19

    iget-object v3, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v3}, La/e/a/c/i;->e(La/e/a/c/i;)I

    move-result v3

    if-eqz p1, :cond_18

    iget-object v13, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v13}, La/e/a/c/i;->f(La/e/a/c/i;)F

    move-result v13

    goto :goto_d

    :cond_18
    iget-object v11, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v11}, La/e/a/c/i;->f(La/e/a/c/i;)F

    move-result v11

    goto :goto_e

    :cond_19
    :goto_f
    invoke-virtual {v14, v3}, La/e/a/c/g;->n(I)V

    invoke-virtual {v14, v13}, La/e/a/c/g;->a(F)V

    :cond_1a
    add-int/lit8 v3, v1, -0x1

    if-ne v6, v3, :cond_1b

    iget-object v3, v14, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v11, v0, La/e/a/c/i$a;->f:La/e/a/c/e;

    iget v13, v0, La/e/a/c/i$a;->j:I

    invoke-virtual {v14, v3, v11, v13}, La/e/a/c/g;->a(La/e/a/c/e;La/e/a/c/e;I)V

    :cond_1b
    if-eqz v10, :cond_1d

    iget-object v3, v14, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v11, v10, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v13, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v13}, La/e/a/c/i;->a(La/e/a/c/i;)I

    move-result v13

    invoke-virtual {v3, v11, v13}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    if-ne v6, v7, :cond_1c

    iget-object v3, v14, La/e/a/c/g;->R:La/e/a/c/e;

    iget v11, v0, La/e/a/c/i$a;->h:I

    invoke-virtual {v3, v11}, La/e/a/c/e;->b(I)V

    :cond_1c
    iget-object v3, v10, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v11, v14, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {v3, v11, v2}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    const/4 v3, 0x1

    add-int/lit8 v11, v8, 0x1

    if-ne v6, v11, :cond_1d

    iget-object v3, v10, La/e/a/c/g;->T:La/e/a/c/e;

    iget v10, v0, La/e/a/c/i$a;->j:I

    invoke-virtual {v3, v10}, La/e/a/c/e;->b(I)V

    :cond_1d
    if-eq v14, v9, :cond_22

    iget-object v3, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v3}, La/e/a/c/i;->p(La/e/a/c/i;)I

    move-result v3

    const/4 v10, 0x3

    if-ne v3, v10, :cond_1e

    invoke-virtual {v12}, La/e/a/c/g;->F()Z

    move-result v3

    if-eqz v3, :cond_1e

    if-eq v14, v12, :cond_1e

    invoke-virtual {v14}, La/e/a/c/g;->F()Z

    move-result v3

    if-eqz v3, :cond_1e

    iget-object v3, v14, La/e/a/c/g;->V:La/e/a/c/e;

    iget-object v11, v12, La/e/a/c/g;->V:La/e/a/c/e;

    invoke-virtual {v3, v11, v2}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    goto :goto_10

    :cond_1e
    iget-object v3, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v3}, La/e/a/c/i;->p(La/e/a/c/i;)I

    move-result v3

    if-eqz v3, :cond_21

    const/4 v11, 0x1

    if-eq v3, v11, :cond_20

    if-eqz v4, :cond_1f

    iget-object v3, v14, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v11, v0, La/e/a/c/i$a;->e:La/e/a/c/e;

    iget v13, v0, La/e/a/c/i$a;->i:I

    invoke-virtual {v3, v11, v13}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    iget-object v3, v14, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object v11, v0, La/e/a/c/i$a;->g:La/e/a/c/e;

    iget v13, v0, La/e/a/c/i$a;->k:I

    invoke-virtual {v3, v11, v13}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    goto :goto_10

    :cond_1f
    iget-object v3, v14, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v11, v9, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {v3, v11, v2}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    iget-object v3, v14, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object v11, v9, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {v3, v11, v2}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    goto :goto_10

    :cond_20
    iget-object v3, v14, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object v11, v9, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {v3, v11, v2}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    goto :goto_10

    :cond_21
    iget-object v3, v14, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v11, v9, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {v3, v11, v2}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    goto :goto_10

    :cond_22
    const/4 v10, 0x3

    :goto_10
    add-int/lit8 v6, v6, 0x1

    move v11, v10

    move-object v10, v14

    goto/16 :goto_a

    :cond_23
    iget-object v3, v0, La/e/a/c/i$a;->b:La/e/a/c/g;

    iget-object v9, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v9}, La/e/a/c/i;->q(La/e/a/c/i;)I

    move-result v9

    invoke-virtual {v3, v9}, La/e/a/c/g;->n(I)V

    iget v9, v0, La/e/a/c/i$a;->h:I

    if-lez p2, :cond_24

    iget-object v10, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v10}, La/e/a/c/i;->a(La/e/a/c/i;)I

    move-result v10

    add-int/2addr v9, v10

    :cond_24
    if-eqz p1, :cond_26

    iget-object v10, v3, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v11, v0, La/e/a/c/i$a;->f:La/e/a/c/e;

    invoke-virtual {v10, v11, v9}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    if-eqz p3, :cond_25

    iget-object v9, v3, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v10, v0, La/e/a/c/i$a;->d:La/e/a/c/e;

    iget v11, v0, La/e/a/c/i$a;->j:I

    invoke-virtual {v9, v10, v11}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    :cond_25
    if-lez p2, :cond_28

    iget-object v9, v0, La/e/a/c/i$a;->f:La/e/a/c/e;

    iget-object v9, v9, La/e/a/c/e;->d:La/e/a/c/g;

    iget-object v9, v9, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v10, v3, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {v9, v10, v2}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    goto :goto_11

    :cond_26
    iget-object v10, v3, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v11, v0, La/e/a/c/i$a;->d:La/e/a/c/e;

    invoke-virtual {v10, v11, v9}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    if-eqz p3, :cond_27

    iget-object v9, v3, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v10, v0, La/e/a/c/i$a;->f:La/e/a/c/e;

    iget v11, v0, La/e/a/c/i$a;->j:I

    invoke-virtual {v9, v10, v11}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    :cond_27
    if-lez p2, :cond_28

    iget-object v9, v0, La/e/a/c/i$a;->d:La/e/a/c/e;

    iget-object v9, v9, La/e/a/c/e;->d:La/e/a/c/g;

    iget-object v9, v9, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v10, v3, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {v9, v10, v2}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    :cond_28
    :goto_11
    move-object v9, v6

    move v6, v2

    :goto_12
    if-ge v6, v1, :cond_3a

    iget v10, v0, La/e/a/c/i$a;->n:I

    add-int/2addr v10, v6

    iget-object v11, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v11}, La/e/a/c/i;->m(La/e/a/c/i;)I

    move-result v11

    if-lt v10, v11, :cond_29

    goto/16 :goto_17

    :cond_29
    iget-object v10, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v10}, La/e/a/c/i;->n(La/e/a/c/i;)[La/e/a/c/g;

    move-result-object v10

    iget v11, v0, La/e/a/c/i$a;->n:I

    add-int/2addr v11, v6

    aget-object v10, v10, v11

    if-nez v10, :cond_2a

    const/4 v12, 0x1

    goto/16 :goto_16

    :cond_2a
    if-nez v6, :cond_2d

    iget-object v11, v10, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v12, v0, La/e/a/c/i$a;->e:La/e/a/c/e;

    iget v13, v0, La/e/a/c/i$a;->i:I

    invoke-virtual {v10, v11, v12, v13}, La/e/a/c/g;->a(La/e/a/c/e;La/e/a/c/e;I)V

    iget-object v11, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v11}, La/e/a/c/i;->o(La/e/a/c/i;)I

    move-result v11

    iget-object v12, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v12}, La/e/a/c/i;->g(La/e/a/c/i;)F

    move-result v12

    iget v13, v0, La/e/a/c/i$a;->n:I

    if-nez v13, :cond_2b

    iget-object v13, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v13}, La/e/a/c/i;->h(La/e/a/c/i;)I

    move-result v13

    if-eq v13, v5, :cond_2b

    iget-object v11, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v11}, La/e/a/c/i;->h(La/e/a/c/i;)I

    move-result v11

    iget-object v12, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v12}, La/e/a/c/i;->i(La/e/a/c/i;)F

    move-result v12

    goto :goto_13

    :cond_2b
    if-eqz p3, :cond_2c

    iget-object v13, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v13}, La/e/a/c/i;->j(La/e/a/c/i;)I

    move-result v13

    if-eq v13, v5, :cond_2c

    iget-object v11, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v11}, La/e/a/c/i;->j(La/e/a/c/i;)I

    move-result v11

    iget-object v12, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v12}, La/e/a/c/i;->k(La/e/a/c/i;)F

    move-result v12

    :cond_2c
    :goto_13
    invoke-virtual {v10, v11}, La/e/a/c/g;->s(I)V

    invoke-virtual {v10, v12}, La/e/a/c/g;->c(F)V

    :cond_2d
    add-int/lit8 v11, v1, -0x1

    if-ne v6, v11, :cond_2e

    iget-object v11, v10, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object v12, v0, La/e/a/c/i$a;->g:La/e/a/c/e;

    iget v13, v0, La/e/a/c/i$a;->k:I

    invoke-virtual {v10, v11, v12, v13}, La/e/a/c/g;->a(La/e/a/c/e;La/e/a/c/e;I)V

    :cond_2e
    if-eqz v9, :cond_30

    iget-object v11, v10, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v12, v9, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object v13, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v13}, La/e/a/c/i;->b(La/e/a/c/i;)I

    move-result v13

    invoke-virtual {v11, v12, v13}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    if-ne v6, v7, :cond_2f

    iget-object v11, v10, La/e/a/c/g;->S:La/e/a/c/e;

    iget v12, v0, La/e/a/c/i$a;->i:I

    invoke-virtual {v11, v12}, La/e/a/c/e;->b(I)V

    :cond_2f
    iget-object v11, v9, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object v12, v10, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {v11, v12, v2}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    const/4 v11, 0x1

    add-int/lit8 v12, v8, 0x1

    if-ne v6, v12, :cond_30

    iget-object v9, v9, La/e/a/c/g;->U:La/e/a/c/e;

    iget v11, v0, La/e/a/c/i$a;->k:I

    invoke-virtual {v9, v11}, La/e/a/c/e;->b(I)V

    :cond_30
    if-eq v10, v3, :cond_39

    const/4 v9, 0x2

    if-eqz p1, :cond_34

    iget-object v11, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v11}, La/e/a/c/i;->l(La/e/a/c/i;)I

    move-result v11

    if-eqz v11, :cond_33

    const/4 v12, 0x1

    if-eq v11, v12, :cond_32

    if-eq v11, v9, :cond_31

    goto :goto_14

    :cond_31
    iget-object v9, v10, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v11, v3, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {v9, v11, v2}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    iget-object v9, v10, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v11, v3, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {v9, v11, v2}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    goto :goto_14

    :cond_32
    iget-object v9, v10, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v11, v3, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {v9, v11, v2}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    goto :goto_14

    :cond_33
    iget-object v9, v10, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v11, v3, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {v9, v11, v2}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    goto :goto_14

    :cond_34
    iget-object v11, v0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v11}, La/e/a/c/i;->l(La/e/a/c/i;)I

    move-result v11

    if-eqz v11, :cond_38

    const/4 v12, 0x1

    if-eq v11, v12, :cond_37

    if-eq v11, v9, :cond_35

    goto :goto_15

    :cond_35
    if-eqz v4, :cond_36

    iget-object v9, v10, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v11, v0, La/e/a/c/i$a;->d:La/e/a/c/e;

    iget v13, v0, La/e/a/c/i$a;->h:I

    invoke-virtual {v9, v11, v13}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    iget-object v9, v10, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v11, v0, La/e/a/c/i$a;->f:La/e/a/c/e;

    iget v13, v0, La/e/a/c/i$a;->j:I

    invoke-virtual {v9, v11, v13}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    goto :goto_15

    :cond_36
    iget-object v9, v10, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v11, v3, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {v9, v11, v2}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    iget-object v9, v10, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v11, v3, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {v9, v11, v2}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    goto :goto_15

    :cond_37
    iget-object v9, v10, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v11, v3, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {v9, v11, v2}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    goto :goto_15

    :cond_38
    const/4 v12, 0x1

    iget-object v9, v10, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v11, v3, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {v9, v11, v2}, La/e/a/c/e;->a(La/e/a/c/e;I)Z

    goto :goto_15

    :cond_39
    :goto_14
    const/4 v12, 0x1

    :goto_15
    move-object v9, v10

    :goto_16
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_12

    :cond_3a
    :goto_17
    return-void
.end method

.method public b()I
    .locals 2

    iget v0, p0, La/e/a/c/i$a;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, La/e/a/c/i$a;->m:I

    iget-object v1, p0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v1}, La/e/a/c/i;->b(La/e/a/c/i;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0

    :cond_0
    iget v0, p0, La/e/a/c/i$a;->m:I

    return v0
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, La/e/a/c/i$a;->n:I

    return-void
.end method

.method public c()I
    .locals 2

    iget v0, p0, La/e/a/c/i$a;->a:I

    if-nez v0, :cond_0

    iget v0, p0, La/e/a/c/i$a;->l:I

    iget-object v1, p0, La/e/a/c/i$a;->r:La/e/a/c/i;

    invoke-static {v1}, La/e/a/c/i;->a(La/e/a/c/i;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0

    :cond_0
    iget v0, p0, La/e/a/c/i$a;->l:I

    return v0
.end method
