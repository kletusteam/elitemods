.class public La/e/a/c/h;
.super La/e/a/c/q;


# instance fields
.field Va:La/e/a/c/a/b;

.field public Wa:La/e/a/c/a/e;

.field private Xa:I

.field protected Ya:La/e/a/c/a/b$b;

.field private Za:Z

.field public _a:La/e/a/e;

.field protected ab:La/e/a/d;

.field bb:I

.field cb:I

.field db:I

.field eb:I

.field public fb:I

.field public gb:I

.field hb:[La/e/a/c/c;

.field ib:[La/e/a/c/c;

.field public jb:Z

.field public kb:Z

.field public lb:Z

.field public mb:I

.field public nb:I

.field private ob:I

.field public pb:Z

.field private qb:Z

.field private rb:Z

.field sb:I

.field private tb:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "La/e/a/c/e;",
            ">;"
        }
    .end annotation
.end field

.field private ub:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "La/e/a/c/e;",
            ">;"
        }
    .end annotation
.end field

.field private vb:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "La/e/a/c/e;",
            ">;"
        }
    .end annotation
.end field

.field private wb:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "La/e/a/c/e;",
            ">;"
        }
    .end annotation
.end field

.field xb:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "La/e/a/c/g;",
            ">;"
        }
    .end annotation
.end field

.field public yb:La/e/a/c/a/b$a;


# direct methods
.method public constructor <init>()V
    .locals 4

    invoke-direct {p0}, La/e/a/c/q;-><init>()V

    new-instance v0, La/e/a/c/a/b;

    invoke-direct {v0, p0}, La/e/a/c/a/b;-><init>(La/e/a/c/h;)V

    iput-object v0, p0, La/e/a/c/h;->Va:La/e/a/c/a/b;

    new-instance v0, La/e/a/c/a/e;

    invoke-direct {v0, p0}, La/e/a/c/a/e;-><init>(La/e/a/c/h;)V

    iput-object v0, p0, La/e/a/c/h;->Wa:La/e/a/c/a/e;

    const/4 v0, 0x0

    iput-object v0, p0, La/e/a/c/h;->Ya:La/e/a/c/a/b$b;

    const/4 v1, 0x0

    iput-boolean v1, p0, La/e/a/c/h;->Za:Z

    new-instance v2, La/e/a/d;

    invoke-direct {v2}, La/e/a/d;-><init>()V

    iput-object v2, p0, La/e/a/c/h;->ab:La/e/a/d;

    iput v1, p0, La/e/a/c/h;->fb:I

    iput v1, p0, La/e/a/c/h;->gb:I

    const/4 v2, 0x4

    new-array v3, v2, [La/e/a/c/c;

    iput-object v3, p0, La/e/a/c/h;->hb:[La/e/a/c/c;

    new-array v2, v2, [La/e/a/c/c;

    iput-object v2, p0, La/e/a/c/h;->ib:[La/e/a/c/c;

    iput-boolean v1, p0, La/e/a/c/h;->jb:Z

    iput-boolean v1, p0, La/e/a/c/h;->kb:Z

    iput-boolean v1, p0, La/e/a/c/h;->lb:Z

    iput v1, p0, La/e/a/c/h;->mb:I

    iput v1, p0, La/e/a/c/h;->nb:I

    const/16 v2, 0x101

    iput v2, p0, La/e/a/c/h;->ob:I

    iput-boolean v1, p0, La/e/a/c/h;->pb:Z

    iput-boolean v1, p0, La/e/a/c/h;->qb:Z

    iput-boolean v1, p0, La/e/a/c/h;->rb:Z

    iput v1, p0, La/e/a/c/h;->sb:I

    iput-object v0, p0, La/e/a/c/h;->tb:Ljava/lang/ref/WeakReference;

    iput-object v0, p0, La/e/a/c/h;->ub:Ljava/lang/ref/WeakReference;

    iput-object v0, p0, La/e/a/c/h;->vb:Ljava/lang/ref/WeakReference;

    iput-object v0, p0, La/e/a/c/h;->wb:Ljava/lang/ref/WeakReference;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, La/e/a/c/h;->xb:Ljava/util/HashSet;

    new-instance v0, La/e/a/c/a/b$a;

    invoke-direct {v0}, La/e/a/c/a/b$a;-><init>()V

    iput-object v0, p0, La/e/a/c/h;->yb:La/e/a/c/a/b$a;

    return-void
.end method

.method private a(La/e/a/c/e;La/e/a/j;)V
    .locals 3

    iget-object v0, p0, La/e/a/c/h;->ab:La/e/a/d;

    invoke-virtual {v0, p1}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object p1

    iget-object v0, p0, La/e/a/c/h;->ab:La/e/a/d;

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-virtual {v0, p2, p1, v2, v1}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    return-void
.end method

.method public static a(ILa/e/a/c/g;La/e/a/c/a/b$b;La/e/a/c/a/b$a;I)Z
    .locals 5

    const/4 p0, 0x0

    if-nez p2, :cond_0

    return p0

    :cond_0
    invoke-virtual {p1}, La/e/a/c/g;->B()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_13

    instance-of v0, p1, La/e/a/c/k;

    if-nez v0, :cond_13

    instance-of v0, p1, La/e/a/c/a;

    if-eqz v0, :cond_1

    goto/16 :goto_8

    :cond_1
    invoke-virtual {p1}, La/e/a/c/g;->n()La/e/a/c/g$a;

    move-result-object v0

    iput-object v0, p3, La/e/a/c/a/b$a;->d:La/e/a/c/g$a;

    invoke-virtual {p1}, La/e/a/c/g;->z()La/e/a/c/g$a;

    move-result-object v0

    iput-object v0, p3, La/e/a/c/a/b$a;->e:La/e/a/c/g$a;

    invoke-virtual {p1}, La/e/a/c/g;->C()I

    move-result v0

    iput v0, p3, La/e/a/c/a/b$a;->f:I

    invoke-virtual {p1}, La/e/a/c/g;->k()I

    move-result v0

    iput v0, p3, La/e/a/c/a/b$a;->g:I

    iput-boolean p0, p3, La/e/a/c/a/b$a;->l:Z

    iput p4, p3, La/e/a/c/a/b$a;->m:I

    iget-object p4, p3, La/e/a/c/a/b$a;->d:La/e/a/c/g$a;

    sget-object v0, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    const/4 v1, 0x1

    if-ne p4, v0, :cond_2

    move p4, v1

    goto :goto_0

    :cond_2
    move p4, p0

    :goto_0
    iget-object v0, p3, La/e/a/c/a/b$a;->e:La/e/a/c/g$a;

    sget-object v2, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v0, v2, :cond_3

    move v0, v1

    goto :goto_1

    :cond_3
    move v0, p0

    :goto_1
    const/4 v2, 0x0

    if-eqz p4, :cond_4

    iget v3, p1, La/e/a/c/g;->ga:F

    cmpl-float v3, v3, v2

    if-lez v3, :cond_4

    move v3, v1

    goto :goto_2

    :cond_4
    move v3, p0

    :goto_2
    if-eqz v0, :cond_5

    iget v4, p1, La/e/a/c/g;->ga:F

    cmpl-float v2, v4, v2

    if-lez v2, :cond_5

    move v2, v1

    goto :goto_3

    :cond_5
    move v2, p0

    :goto_3
    if-eqz p4, :cond_7

    invoke-virtual {p1, p0}, La/e/a/c/g;->g(I)Z

    move-result v4

    if-eqz v4, :cond_7

    iget v4, p1, La/e/a/c/g;->x:I

    if-nez v4, :cond_7

    if-nez v3, :cond_7

    sget-object p4, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    iput-object p4, p3, La/e/a/c/a/b$a;->d:La/e/a/c/g$a;

    if-eqz v0, :cond_6

    iget p4, p1, La/e/a/c/g;->y:I

    if-nez p4, :cond_6

    sget-object p4, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    iput-object p4, p3, La/e/a/c/a/b$a;->d:La/e/a/c/g$a;

    :cond_6
    move p4, p0

    :cond_7
    if-eqz v0, :cond_9

    invoke-virtual {p1, v1}, La/e/a/c/g;->g(I)Z

    move-result v4

    if-eqz v4, :cond_9

    iget v4, p1, La/e/a/c/g;->y:I

    if-nez v4, :cond_9

    if-nez v2, :cond_9

    sget-object v0, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    iput-object v0, p3, La/e/a/c/a/b$a;->e:La/e/a/c/g$a;

    if-eqz p4, :cond_8

    iget v0, p1, La/e/a/c/g;->x:I

    if-nez v0, :cond_8

    sget-object v0, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    iput-object v0, p3, La/e/a/c/a/b$a;->e:La/e/a/c/g$a;

    :cond_8
    move v0, p0

    :cond_9
    invoke-virtual {p1}, La/e/a/c/g;->O()Z

    move-result v4

    if-eqz v4, :cond_a

    sget-object p4, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    iput-object p4, p3, La/e/a/c/a/b$a;->d:La/e/a/c/g$a;

    move p4, p0

    :cond_a
    invoke-virtual {p1}, La/e/a/c/g;->P()Z

    move-result v4

    if-eqz v4, :cond_b

    sget-object v0, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    iput-object v0, p3, La/e/a/c/a/b$a;->e:La/e/a/c/g$a;

    move v0, p0

    :cond_b
    const/4 v4, 0x4

    if-eqz v3, :cond_e

    iget-object v3, p1, La/e/a/c/g;->z:[I

    aget p0, v3, p0

    if-ne p0, v4, :cond_c

    sget-object p0, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    iput-object p0, p3, La/e/a/c/a/b$a;->d:La/e/a/c/g$a;

    goto :goto_5

    :cond_c
    if-nez v0, :cond_e

    iget-object p0, p3, La/e/a/c/a/b$a;->e:La/e/a/c/g$a;

    sget-object v0, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    if-ne p0, v0, :cond_d

    iget p0, p3, La/e/a/c/a/b$a;->g:I

    goto :goto_4

    :cond_d
    sget-object p0, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    iput-object p0, p3, La/e/a/c/a/b$a;->d:La/e/a/c/g$a;

    invoke-interface {p2, p1, p3}, La/e/a/c/a/b$b;->a(La/e/a/c/g;La/e/a/c/a/b$a;)V

    iget p0, p3, La/e/a/c/a/b$a;->i:I

    :goto_4
    sget-object v0, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    iput-object v0, p3, La/e/a/c/a/b$a;->d:La/e/a/c/g$a;

    invoke-virtual {p1}, La/e/a/c/g;->i()F

    move-result v0

    int-to-float p0, p0

    mul-float/2addr v0, p0

    float-to-int p0, v0

    iput p0, p3, La/e/a/c/a/b$a;->f:I

    :cond_e
    :goto_5
    if-eqz v2, :cond_12

    iget-object p0, p1, La/e/a/c/g;->z:[I

    aget p0, p0, v1

    if-ne p0, v4, :cond_f

    sget-object p0, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    iput-object p0, p3, La/e/a/c/a/b$a;->e:La/e/a/c/g$a;

    goto :goto_7

    :cond_f
    if-nez p4, :cond_12

    iget-object p0, p3, La/e/a/c/a/b$a;->d:La/e/a/c/g$a;

    sget-object p4, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    if-ne p0, p4, :cond_10

    iget p0, p3, La/e/a/c/a/b$a;->f:I

    goto :goto_6

    :cond_10
    sget-object p0, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    iput-object p0, p3, La/e/a/c/a/b$a;->e:La/e/a/c/g$a;

    invoke-interface {p2, p1, p3}, La/e/a/c/a/b$b;->a(La/e/a/c/g;La/e/a/c/a/b$a;)V

    iget p0, p3, La/e/a/c/a/b$a;->h:I

    :goto_6
    sget-object p4, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    iput-object p4, p3, La/e/a/c/a/b$a;->e:La/e/a/c/g$a;

    invoke-virtual {p1}, La/e/a/c/g;->j()I

    move-result p4

    const/4 v0, -0x1

    if-ne p4, v0, :cond_11

    int-to-float p0, p0

    invoke-virtual {p1}, La/e/a/c/g;->i()F

    move-result p4

    div-float/2addr p0, p4

    float-to-int p0, p0

    iput p0, p3, La/e/a/c/a/b$a;->g:I

    goto :goto_7

    :cond_11
    invoke-virtual {p1}, La/e/a/c/g;->i()F

    move-result p4

    int-to-float p0, p0

    mul-float/2addr p4, p0

    float-to-int p0, p4

    iput p0, p3, La/e/a/c/a/b$a;->g:I

    :cond_12
    :goto_7
    invoke-interface {p2, p1, p3}, La/e/a/c/a/b$b;->a(La/e/a/c/g;La/e/a/c/a/b$a;)V

    iget p0, p3, La/e/a/c/a/b$a;->h:I

    invoke-virtual {p1, p0}, La/e/a/c/g;->u(I)V

    iget p0, p3, La/e/a/c/a/b$a;->i:I

    invoke-virtual {p1, p0}, La/e/a/c/g;->m(I)V

    iget-boolean p0, p3, La/e/a/c/a/b$a;->k:Z

    invoke-virtual {p1, p0}, La/e/a/c/g;->a(Z)V

    iget p0, p3, La/e/a/c/a/b$a;->j:I

    invoke-virtual {p1, p0}, La/e/a/c/g;->i(I)V

    sget p0, La/e/a/c/a/b$a;->a:I

    iput p0, p3, La/e/a/c/a/b$a;->m:I

    iget-boolean p0, p3, La/e/a/c/a/b$a;->l:Z

    return p0

    :cond_13
    :goto_8
    iput p0, p3, La/e/a/c/a/b$a;->h:I

    iput p0, p3, La/e/a/c/a/b$a;->i:I

    return p0
.end method

.method private b(La/e/a/c/e;La/e/a/j;)V
    .locals 3

    iget-object v0, p0, La/e/a/c/h;->ab:La/e/a/d;

    invoke-virtual {v0, p1}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object p1

    iget-object v0, p0, La/e/a/c/h;->ab:La/e/a/d;

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-virtual {v0, p1, p2, v2, v1}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    return-void
.end method

.method private d(La/e/a/c/g;)V
    .locals 5

    iget v0, p0, La/e/a/c/h;->fb:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, La/e/a/c/h;->ib:[La/e/a/c/c;

    array-length v2, v1

    if-lt v0, v2, :cond_0

    array-length v0, v1

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [La/e/a/c/c;

    iput-object v0, p0, La/e/a/c/h;->ib:[La/e/a/c/c;

    :cond_0
    iget-object v0, p0, La/e/a/c/h;->ib:[La/e/a/c/c;

    iget v1, p0, La/e/a/c/h;->fb:I

    new-instance v2, La/e/a/c/c;

    const/4 v3, 0x0

    invoke-virtual {p0}, La/e/a/c/h;->ia()Z

    move-result v4

    invoke-direct {v2, p1, v3, v4}, La/e/a/c/c;-><init>(La/e/a/c/g;IZ)V

    aput-object v2, v0, v1

    iget p1, p0, La/e/a/c/h;->fb:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, La/e/a/c/h;->fb:I

    return-void
.end method

.method private e(La/e/a/c/g;)V
    .locals 5

    iget v0, p0, La/e/a/c/h;->gb:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    iget-object v2, p0, La/e/a/c/h;->hb:[La/e/a/c/c;

    array-length v3, v2

    if-lt v0, v3, :cond_0

    array-length v0, v2

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v2, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [La/e/a/c/c;

    iput-object v0, p0, La/e/a/c/h;->hb:[La/e/a/c/c;

    :cond_0
    iget-object v0, p0, La/e/a/c/h;->hb:[La/e/a/c/c;

    iget v2, p0, La/e/a/c/h;->gb:I

    new-instance v3, La/e/a/c/c;

    invoke-virtual {p0}, La/e/a/c/h;->ia()Z

    move-result v4

    invoke-direct {v3, p1, v1, v4}, La/e/a/c/c;-><init>(La/e/a/c/g;IZ)V

    aput-object v3, v0, v2

    iget p1, p0, La/e/a/c/h;->gb:I

    add-int/2addr p1, v1

    iput p1, p0, La/e/a/c/h;->gb:I

    return-void
.end method

.method private la()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, La/e/a/c/h;->fb:I

    iput v0, p0, La/e/a/c/h;->gb:I

    return-void
.end method


# virtual methods
.method public A(I)V
    .locals 0

    iput p1, p0, La/e/a/c/h;->Xa:I

    return-void
.end method

.method public U()V
    .locals 1

    iget-object v0, p0, La/e/a/c/h;->ab:La/e/a/d;

    invoke-virtual {v0}, La/e/a/d;->g()V

    const/4 v0, 0x0

    iput v0, p0, La/e/a/c/h;->bb:I

    iput v0, p0, La/e/a/c/h;->db:I

    iput v0, p0, La/e/a/c/h;->cb:I

    iput v0, p0, La/e/a/c/h;->eb:I

    iput-boolean v0, p0, La/e/a/c/h;->pb:Z

    invoke-super {p0}, La/e/a/c/q;->U()V

    return-void
.end method

.method public Z()V
    .locals 18

    move-object/from16 v1, p0

    const/4 v2, 0x0

    iput v2, v1, La/e/a/c/g;->ia:I

    iput v2, v1, La/e/a/c/g;->ja:I

    iput-boolean v2, v1, La/e/a/c/h;->qb:Z

    iput-boolean v2, v1, La/e/a/c/h;->rb:Z

    iget-object v0, v1, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->C()I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->k()I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    iget-object v5, v1, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    const/4 v6, 0x1

    aget-object v7, v5, v6

    aget-object v5, v5, v2

    iget-object v8, v1, La/e/a/c/h;->_a:La/e/a/e;

    if-eqz v8, :cond_0

    iget-wide v9, v8, La/e/a/e;->z:J

    const-wide/16 v11, 0x1

    add-long/2addr v9, v11

    iput-wide v9, v8, La/e/a/e;->z:J

    :cond_0
    iget v8, v1, La/e/a/c/h;->Xa:I

    if-nez v8, :cond_3

    iget v8, v1, La/e/a/c/h;->ob:I

    invoke-static {v8, v6}, La/e/a/c/n;->a(II)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual/range {p0 .. p0}, La/e/a/c/h;->ba()La/e/a/c/a/b$b;

    move-result-object v8

    invoke-static {v1, v8}, La/e/a/c/a/h;->a(La/e/a/c/h;La/e/a/c/a/b$b;)V

    move v8, v2

    :goto_0
    if-ge v8, v3, :cond_3

    iget-object v9, v1, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, La/e/a/c/g;

    invoke-virtual {v9}, La/e/a/c/g;->N()Z

    move-result v10

    if-eqz v10, :cond_2

    instance-of v10, v9, La/e/a/c/k;

    if-nez v10, :cond_2

    instance-of v10, v9, La/e/a/c/a;

    if-nez v10, :cond_2

    instance-of v10, v9, La/e/a/c/p;

    if-nez v10, :cond_2

    invoke-virtual {v9}, La/e/a/c/g;->M()Z

    move-result v10

    if-nez v10, :cond_2

    invoke-virtual {v9, v2}, La/e/a/c/g;->b(I)La/e/a/c/g$a;

    move-result-object v10

    invoke-virtual {v9, v6}, La/e/a/c/g;->b(I)La/e/a/c/g$a;

    move-result-object v11

    sget-object v12, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v10, v12, :cond_1

    iget v10, v9, La/e/a/c/g;->x:I

    if-eq v10, v6, :cond_1

    if-ne v11, v12, :cond_1

    iget v10, v9, La/e/a/c/g;->y:I

    if-eq v10, v6, :cond_1

    move v10, v6

    goto :goto_1

    :cond_1
    move v10, v2

    :goto_1
    if-nez v10, :cond_2

    new-instance v10, La/e/a/c/a/b$a;

    invoke-direct {v10}, La/e/a/c/a/b$a;-><init>()V

    iget-object v11, v1, La/e/a/c/h;->Ya:La/e/a/c/a/b$b;

    sget v12, La/e/a/c/a/b$a;->a:I

    invoke-static {v2, v9, v11, v10, v12}, La/e/a/c/h;->a(ILa/e/a/c/g;La/e/a/c/a/b$b;La/e/a/c/a/b$a;I)Z

    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_3
    const/4 v8, 0x2

    if-le v3, v8, :cond_9

    sget-object v9, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-eq v5, v9, :cond_4

    if-ne v7, v9, :cond_9

    :cond_4
    iget v9, v1, La/e/a/c/h;->ob:I

    const/16 v10, 0x400

    invoke-static {v9, v10}, La/e/a/c/n;->a(II)Z

    move-result v9

    if-eqz v9, :cond_9

    invoke-virtual/range {p0 .. p0}, La/e/a/c/h;->ba()La/e/a/c/a/b$b;

    move-result-object v9

    invoke-static {v1, v9}, La/e/a/c/a/i;->a(La/e/a/c/h;La/e/a/c/a/b$b;)Z

    move-result v9

    if-eqz v9, :cond_9

    sget-object v9, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v5, v9, :cond_6

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->C()I

    move-result v9

    if-ge v0, v9, :cond_5

    if-lez v0, :cond_5

    invoke-virtual {v1, v0}, La/e/a/c/g;->u(I)V

    iput-boolean v6, v1, La/e/a/c/h;->qb:Z

    goto :goto_2

    :cond_5
    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->C()I

    move-result v0

    :cond_6
    :goto_2
    sget-object v9, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v7, v9, :cond_8

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->k()I

    move-result v9

    if-ge v4, v9, :cond_7

    if-lez v4, :cond_7

    invoke-virtual {v1, v4}, La/e/a/c/g;->m(I)V

    iput-boolean v6, v1, La/e/a/c/h;->rb:Z

    goto :goto_3

    :cond_7
    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->k()I

    move-result v4

    :cond_8
    :goto_3
    move v9, v4

    move v4, v0

    move v0, v6

    goto :goto_4

    :cond_9
    move v9, v4

    move v4, v0

    move v0, v2

    :goto_4
    const/16 v10, 0x40

    invoke-virtual {v1, v10}, La/e/a/c/h;->y(I)Z

    move-result v11

    if-nez v11, :cond_b

    const/16 v11, 0x80

    invoke-virtual {v1, v11}, La/e/a/c/h;->y(I)Z

    move-result v11

    if-eqz v11, :cond_a

    goto :goto_5

    :cond_a
    move v11, v2

    goto :goto_6

    :cond_b
    :goto_5
    move v11, v6

    :goto_6
    iget-object v12, v1, La/e/a/c/h;->ab:La/e/a/d;

    iput-boolean v2, v12, La/e/a/d;->q:Z

    iput-boolean v2, v12, La/e/a/d;->r:Z

    iget v13, v1, La/e/a/c/h;->ob:I

    if-eqz v13, :cond_c

    if-eqz v11, :cond_c

    iput-boolean v6, v12, La/e/a/d;->r:Z

    :cond_c
    iget-object v11, v1, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->n()La/e/a/c/g$a;

    move-result-object v12

    sget-object v13, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-eq v12, v13, :cond_e

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->z()La/e/a/c/g$a;

    move-result-object v12

    sget-object v13, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v12, v13, :cond_d

    goto :goto_7

    :cond_d
    move v12, v2

    goto :goto_8

    :cond_e
    :goto_7
    move v12, v6

    :goto_8
    invoke-direct/range {p0 .. p0}, La/e/a/c/h;->la()V

    move v13, v2

    :goto_9
    if-ge v13, v3, :cond_10

    iget-object v14, v1, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v14, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, La/e/a/c/g;

    instance-of v15, v14, La/e/a/c/q;

    if-eqz v15, :cond_f

    check-cast v14, La/e/a/c/q;

    invoke-virtual {v14}, La/e/a/c/q;->Z()V

    :cond_f
    add-int/lit8 v13, v13, 0x1

    goto :goto_9

    :cond_10
    invoke-virtual {v1, v10}, La/e/a/c/h;->y(I)Z

    move-result v10

    move v14, v0

    move v0, v2

    move v13, v6

    :goto_a
    if-eqz v13, :cond_22

    add-int/lit8 v15, v0, 0x1

    :try_start_0
    iget-object v0, v1, La/e/a/c/h;->ab:La/e/a/d;

    invoke-virtual {v0}, La/e/a/d;->g()V

    invoke-direct/range {p0 .. p0}, La/e/a/c/h;->la()V

    iget-object v0, v1, La/e/a/c/h;->ab:La/e/a/d;

    invoke-virtual {v1, v0}, La/e/a/c/g;->a(La/e/a/d;)V

    move v0, v2

    :goto_b
    if-ge v0, v3, :cond_11

    iget-object v6, v1, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, La/e/a/c/g;

    iget-object v2, v1, La/e/a/c/h;->ab:La/e/a/d;

    invoke-virtual {v6, v2}, La/e/a/c/g;->a(La/e/a/d;)V

    add-int/lit8 v0, v0, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x1

    goto :goto_b

    :cond_11
    iget-object v0, v1, La/e/a/c/h;->ab:La/e/a/d;

    invoke-virtual {v1, v0}, La/e/a/c/h;->b(La/e/a/d;)Z

    move-result v13

    iget-object v0, v1, La/e/a/c/h;->tb:Ljava/lang/ref/WeakReference;

    const/4 v2, 0x0

    if-eqz v0, :cond_12

    iget-object v0, v1, La/e/a/c/h;->tb:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_12

    iget-object v0, v1, La/e/a/c/h;->tb:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/e/a/c/e;

    iget-object v6, v1, La/e/a/c/h;->ab:La/e/a/d;

    iget-object v8, v1, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {v6, v8}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v6

    invoke-direct {v1, v0, v6}, La/e/a/c/h;->b(La/e/a/c/e;La/e/a/j;)V

    iput-object v2, v1, La/e/a/c/h;->tb:Ljava/lang/ref/WeakReference;

    :cond_12
    iget-object v0, v1, La/e/a/c/h;->vb:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_13

    iget-object v0, v1, La/e/a/c/h;->vb:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_13

    iget-object v0, v1, La/e/a/c/h;->vb:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/e/a/c/e;

    iget-object v6, v1, La/e/a/c/h;->ab:La/e/a/d;

    iget-object v8, v1, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {v6, v8}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v6

    invoke-direct {v1, v0, v6}, La/e/a/c/h;->a(La/e/a/c/e;La/e/a/j;)V

    iput-object v2, v1, La/e/a/c/h;->vb:Ljava/lang/ref/WeakReference;

    :cond_13
    iget-object v0, v1, La/e/a/c/h;->ub:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_14

    iget-object v0, v1, La/e/a/c/h;->ub:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_14

    iget-object v0, v1, La/e/a/c/h;->ub:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/e/a/c/e;

    iget-object v6, v1, La/e/a/c/h;->ab:La/e/a/d;

    iget-object v8, v1, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {v6, v8}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v6

    invoke-direct {v1, v0, v6}, La/e/a/c/h;->b(La/e/a/c/e;La/e/a/j;)V

    iput-object v2, v1, La/e/a/c/h;->ub:Ljava/lang/ref/WeakReference;

    :cond_14
    iget-object v0, v1, La/e/a/c/h;->wb:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_15

    iget-object v0, v1, La/e/a/c/h;->wb:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_15

    iget-object v0, v1, La/e/a/c/h;->wb:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/e/a/c/e;

    iget-object v6, v1, La/e/a/c/h;->ab:La/e/a/d;

    iget-object v8, v1, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {v6, v8}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v6

    invoke-direct {v1, v0, v6}, La/e/a/c/h;->a(La/e/a/c/e;La/e/a/j;)V

    iput-object v2, v1, La/e/a/c/h;->wb:Ljava/lang/ref/WeakReference;

    :cond_15
    if-eqz v13, :cond_16

    iget-object v0, v1, La/e/a/c/h;->ab:La/e/a/d;

    invoke-virtual {v0}, La/e/a/d;->f()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_c

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "EXCEPTION : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_16
    :goto_c
    if-eqz v13, :cond_17

    iget-object v0, v1, La/e/a/c/h;->ab:La/e/a/d;

    sget-object v2, La/e/a/c/n;->a:[Z

    invoke-virtual {v1, v0, v2}, La/e/a/c/h;->a(La/e/a/d;[Z)Z

    move-result v2

    move v6, v2

    goto :goto_e

    :cond_17
    iget-object v0, v1, La/e/a/c/h;->ab:La/e/a/d;

    invoke-virtual {v1, v0, v10}, La/e/a/c/g;->b(La/e/a/d;Z)V

    const/4 v0, 0x0

    :goto_d
    if-ge v0, v3, :cond_18

    iget-object v2, v1, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, La/e/a/c/g;

    iget-object v6, v1, La/e/a/c/h;->ab:La/e/a/d;

    invoke-virtual {v2, v6, v10}, La/e/a/c/g;->b(La/e/a/d;Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    :cond_18
    const/4 v6, 0x0

    :goto_e
    const/16 v0, 0x8

    if-eqz v12, :cond_1b

    if-ge v15, v0, :cond_1b

    sget-object v2, La/e/a/c/n;->a:[Z

    const/4 v8, 0x2

    aget-boolean v2, v2, v8

    if-eqz v2, :cond_1b

    const/4 v2, 0x0

    const/4 v8, 0x0

    const/4 v13, 0x0

    :goto_f
    if-ge v2, v3, :cond_19

    iget-object v0, v1, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/e/a/c/g;

    move/from16 v16, v3

    iget v3, v0, La/e/a/c/g;->ia:I

    invoke-virtual {v0}, La/e/a/c/g;->C()I

    move-result v17

    add-int v3, v3, v17

    invoke-static {v13, v3}, Ljava/lang/Math;->max(II)I

    move-result v13

    iget v3, v0, La/e/a/c/g;->ja:I

    invoke-virtual {v0}, La/e/a/c/g;->k()I

    move-result v0

    add-int/2addr v3, v0

    invoke-static {v8, v3}, Ljava/lang/Math;->max(II)I

    move-result v8

    add-int/lit8 v2, v2, 0x1

    move/from16 v3, v16

    const/16 v0, 0x8

    goto :goto_f

    :cond_19
    move/from16 v16, v3

    iget v0, v1, La/e/a/c/g;->pa:I

    invoke-static {v0, v13}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v2, v1, La/e/a/c/g;->qa:I

    invoke-static {v2, v8}, Ljava/lang/Math;->max(II)I

    move-result v2

    sget-object v3, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v5, v3, :cond_1a

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->C()I

    move-result v3

    if-ge v3, v0, :cond_1a

    invoke-virtual {v1, v0}, La/e/a/c/g;->u(I)V

    iget-object v0, v1, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    sget-object v3, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    const/4 v6, 0x0

    aput-object v3, v0, v6

    const/4 v6, 0x1

    const/4 v14, 0x1

    :cond_1a
    sget-object v0, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v7, v0, :cond_1c

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->k()I

    move-result v0

    if-ge v0, v2, :cond_1c

    invoke-virtual {v1, v2}, La/e/a/c/g;->m(I)V

    iget-object v0, v1, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    sget-object v2, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    const/4 v3, 0x1

    aput-object v2, v0, v3

    const/4 v0, 0x1

    const/4 v6, 0x1

    goto :goto_10

    :cond_1b
    move/from16 v16, v3

    :cond_1c
    move v0, v6

    move v6, v14

    :goto_10
    iget v2, v1, La/e/a/c/g;->pa:I

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->C()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->C()I

    move-result v3

    if-le v2, v3, :cond_1d

    invoke-virtual {v1, v2}, La/e/a/c/g;->u(I)V

    iget-object v0, v1, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    sget-object v2, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    const/4 v3, 0x0

    aput-object v2, v0, v3

    const/4 v0, 0x1

    const/4 v6, 0x1

    :cond_1d
    iget v2, v1, La/e/a/c/g;->qa:I

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->k()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->k()I

    move-result v3

    if-le v2, v3, :cond_1e

    invoke-virtual {v1, v2}, La/e/a/c/g;->m(I)V

    iget-object v0, v1, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    sget-object v2, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    const/4 v3, 0x1

    aput-object v2, v0, v3

    move v0, v3

    move v6, v0

    goto :goto_11

    :cond_1e
    const/4 v3, 0x1

    :goto_11
    if-nez v6, :cond_20

    iget-object v2, v1, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    const/4 v8, 0x0

    aget-object v2, v2, v8

    sget-object v13, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v2, v13, :cond_1f

    if-lez v4, :cond_1f

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->C()I

    move-result v2

    if-le v2, v4, :cond_1f

    iput-boolean v3, v1, La/e/a/c/h;->qb:Z

    iget-object v0, v1, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    sget-object v2, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    aput-object v2, v0, v8

    invoke-virtual {v1, v4}, La/e/a/c/g;->u(I)V

    move v0, v3

    move v6, v0

    :cond_1f
    iget-object v2, v1, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object v2, v2, v3

    sget-object v8, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v2, v8, :cond_20

    if-lez v9, :cond_20

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->k()I

    move-result v2

    if-le v2, v9, :cond_20

    iput-boolean v3, v1, La/e/a/c/h;->rb:Z

    iget-object v0, v1, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    sget-object v2, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    aput-object v2, v0, v3

    invoke-virtual {v1, v9}, La/e/a/c/g;->m(I)V

    const/16 v0, 0x8

    const/4 v2, 0x1

    const/4 v14, 0x1

    goto :goto_12

    :cond_20
    move v2, v0

    move v14, v6

    const/16 v0, 0x8

    :goto_12
    if-le v15, v0, :cond_21

    const/4 v13, 0x0

    goto :goto_13

    :cond_21
    move v13, v2

    :goto_13
    move v0, v15

    move/from16 v3, v16

    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v8, 0x2

    goto/16 :goto_a

    :cond_22
    iput-object v11, v1, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    if-eqz v14, :cond_23

    iget-object v0, v1, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    const/4 v2, 0x0

    aput-object v5, v0, v2

    const/4 v2, 0x1

    aput-object v7, v0, v2

    :cond_23
    iget-object v0, v1, La/e/a/c/h;->ab:La/e/a/d;

    invoke-virtual {v0}, La/e/a/d;->d()La/e/a/c;

    move-result-object v0

    invoke-virtual {v1, v0}, La/e/a/c/q;->a(La/e/a/c;)V

    return-void
.end method

.method public a(IIIIIIIII)J
    .locals 12

    move-object v11, p0

    move/from16 v3, p8

    iput v3, v11, La/e/a/c/h;->bb:I

    move/from16 v4, p9

    iput v4, v11, La/e/a/c/h;->cb:I

    iget-object v0, v11, La/e/a/c/h;->Va:La/e/a/c/a/b;

    move-object v1, p0

    move v2, p1

    move v5, p2

    move v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    move/from16 v10, p7

    invoke-virtual/range {v0 .. v10}, La/e/a/c/a/b;->a(La/e/a/c/h;IIIIIIIII)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(La/e/a/c/a/b$b;)V
    .locals 1

    iput-object p1, p0, La/e/a/c/h;->Ya:La/e/a/c/a/b$b;

    iget-object v0, p0, La/e/a/c/h;->Wa:La/e/a/c/a/e;

    invoke-virtual {v0, p1}, La/e/a/c/a/e;->a(La/e/a/c/a/b$b;)V

    return-void
.end method

.method public a(La/e/a/c/e;)V
    .locals 2

    iget-object v0, p0, La/e/a/c/h;->wb:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, La/e/a/c/e;->b()I

    move-result v0

    iget-object v1, p0, La/e/a/c/h;->wb:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La/e/a/c/e;

    invoke-virtual {v1}, La/e/a/c/e;->b()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, La/e/a/c/h;->wb:Ljava/lang/ref/WeakReference;

    :cond_1
    return-void
.end method

.method a(La/e/a/c/g;I)V
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    if-eq p2, v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_1
    invoke-direct {p0, p1}, La/e/a/c/h;->e(La/e/a/c/g;)V

    :goto_2
    goto/32 :goto_5

    nop

    :goto_3
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_4
    if-eqz p2, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_6

    nop

    :goto_5
    return-void

    :goto_6
    invoke-direct {p0, p1}, La/e/a/c/h;->d(La/e/a/c/g;)V

    goto/32 :goto_7

    nop

    :goto_7
    goto :goto_2

    :goto_8
    goto/32 :goto_3

    nop
.end method

.method public a(La/e/a/e;)V
    .locals 1

    iput-object p1, p0, La/e/a/c/h;->_a:La/e/a/e;

    iget-object v0, p0, La/e/a/c/h;->ab:La/e/a/d;

    invoke-virtual {v0, p1}, La/e/a/d;->a(La/e/a/e;)V

    return-void
.end method

.method public a(Ljava/lang/StringBuilder;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, La/e/a/c/g;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":{\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  actualWidth:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, La/e/a/c/g;->ea:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  actualHeight:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, La/e/a/c/g;->fa:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, La/e/a/c/q;->Y()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La/e/a/c/g;

    invoke-virtual {v1, p1}, La/e/a/c/g;->a(Ljava/lang/StringBuilder;)V

    const-string v1, ",\n"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    const-string v0, "}"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method public a(ZZ)V
    .locals 3

    invoke-super {p0, p1, p2}, La/e/a/c/g;->a(ZZ)V

    iget-object v0, p0, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, La/e/a/c/g;

    invoke-virtual {v2, p1, p2}, La/e/a/c/g;->a(ZZ)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(La/e/a/d;[Z)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x2

    aput-boolean v0, p2, v1

    const/16 p2, 0x40

    invoke-virtual {p0, p2}, La/e/a/c/h;->y(I)Z

    move-result p2

    invoke-virtual {p0, p1, p2}, La/e/a/c/g;->b(La/e/a/d;Z)V

    iget-object v1, p0, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    move v2, v0

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v3, p0, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, La/e/a/c/g;

    invoke-virtual {v3, p1, p2}, La/e/a/c/g;->b(La/e/a/d;Z)V

    invoke-virtual {v3}, La/e/a/c/g;->H()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method public a(ZI)Z
    .locals 1

    iget-object v0, p0, La/e/a/c/h;->Wa:La/e/a/c/a/e;

    invoke-virtual {v0, p1, p2}, La/e/a/c/a/e;->a(ZI)Z

    move-result p1

    return p1
.end method

.method public b(La/e/a/c/e;)V
    .locals 2

    iget-object v0, p0, La/e/a/c/h;->ub:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, La/e/a/c/e;->b()I

    move-result v0

    iget-object v1, p0, La/e/a/c/h;->ub:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La/e/a/c/e;

    invoke-virtual {v1}, La/e/a/c/e;->b()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, La/e/a/c/h;->ub:Ljava/lang/ref/WeakReference;

    :cond_1
    return-void
.end method

.method public b(La/e/a/d;)Z
    .locals 12

    const/16 v0, 0x40

    invoke-virtual {p0, v0}, La/e/a/c/h;->y(I)Z

    move-result v0

    invoke-virtual {p0, p1, v0}, La/e/a/c/g;->a(La/e/a/d;Z)V

    iget-object v1, p0, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    move v3, v2

    move v4, v3

    :goto_0
    const/4 v5, 0x1

    if-ge v3, v1, :cond_1

    iget-object v6, p0, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, La/e/a/c/g;

    invoke-virtual {v6, v2, v2}, La/e/a/c/g;->a(IZ)V

    invoke-virtual {v6, v5, v2}, La/e/a/c/g;->a(IZ)V

    instance-of v6, v6, La/e/a/c/a;

    if-eqz v6, :cond_0

    move v4, v5

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    if-eqz v4, :cond_3

    move v3, v2

    :goto_1
    if-ge v3, v1, :cond_3

    iget-object v4, p0, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, La/e/a/c/g;

    instance-of v6, v4, La/e/a/c/a;

    if-eqz v6, :cond_2

    check-cast v4, La/e/a/c/a;

    invoke-virtual {v4}, La/e/a/c/a;->da()V

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    iget-object v3, p0, La/e/a/c/h;->xb:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->clear()V

    move v3, v2

    :goto_2
    if-ge v3, v1, :cond_6

    iget-object v4, p0, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, La/e/a/c/g;

    invoke-virtual {v4}, La/e/a/c/g;->b()Z

    move-result v6

    if-eqz v6, :cond_5

    instance-of v6, v4, La/e/a/c/p;

    if-eqz v6, :cond_4

    iget-object v6, p0, La/e/a/c/h;->xb:Ljava/util/HashSet;

    invoke-virtual {v6, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_4
    invoke-virtual {v4, p1, v0}, La/e/a/c/g;->a(La/e/a/d;Z)V

    :cond_5
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_6
    :goto_4
    iget-object v3, p0, La/e/a/c/h;->xb:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v3

    if-lez v3, :cond_a

    iget-object v3, p0, La/e/a/c/h;->xb:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v3

    iget-object v4, p0, La/e/a/c/h;->xb:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, La/e/a/c/p;

    iget-object v7, p0, La/e/a/c/h;->xb:Ljava/util/HashSet;

    invoke-virtual {v6, v7}, La/e/a/c/p;->a(Ljava/util/HashSet;)Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-virtual {v6, p1, v0}, La/e/a/c/g;->a(La/e/a/d;Z)V

    iget-object v4, p0, La/e/a/c/h;->xb:Ljava/util/HashSet;

    invoke-virtual {v4, v6}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    :cond_8
    iget-object v4, p0, La/e/a/c/h;->xb:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v4

    if-ne v3, v4, :cond_6

    iget-object v3, p0, La/e/a/c/h;->xb:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, La/e/a/c/g;

    invoke-virtual {v4, p1, v0}, La/e/a/c/g;->a(La/e/a/d;Z)V

    goto :goto_5

    :cond_9
    iget-object v3, p0, La/e/a/c/h;->xb:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->clear()V

    goto :goto_4

    :cond_a
    sget-boolean v3, La/e/a/d;->a:Z

    if-eqz v3, :cond_e

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    move v4, v2

    :goto_6
    if-ge v4, v1, :cond_c

    iget-object v6, p0, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, La/e/a/c/g;

    invoke-virtual {v6}, La/e/a/c/g;->b()Z

    move-result v7

    if-nez v7, :cond_b

    invoke-virtual {v3, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_b
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    :cond_c
    invoke-virtual {p0}, La/e/a/c/g;->n()La/e/a/c/g$a;

    move-result-object v1

    sget-object v4, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v1, v4, :cond_d

    move v10, v2

    goto :goto_7

    :cond_d
    move v10, v5

    :goto_7
    const/4 v11, 0x0

    move-object v6, p0

    move-object v7, p0

    move-object v8, p1

    move-object v9, v3

    invoke-virtual/range {v6 .. v11}, La/e/a/c/g;->a(La/e/a/c/h;La/e/a/d;Ljava/util/HashSet;IZ)V

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_14

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, La/e/a/c/g;

    invoke-static {p0, p1, v3}, La/e/a/c/n;->a(La/e/a/c/h;La/e/a/d;La/e/a/c/g;)V

    invoke-virtual {v3, p1, v0}, La/e/a/c/g;->a(La/e/a/d;Z)V

    goto :goto_8

    :cond_e
    move v3, v2

    :goto_9
    if-ge v3, v1, :cond_14

    iget-object v4, p0, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, La/e/a/c/g;

    instance-of v6, v4, La/e/a/c/h;

    if-eqz v6, :cond_12

    iget-object v6, v4, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object v7, v6, v2

    aget-object v6, v6, v5

    sget-object v8, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v7, v8, :cond_f

    sget-object v8, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    invoke-virtual {v4, v8}, La/e/a/c/g;->a(La/e/a/c/g$a;)V

    :cond_f
    sget-object v8, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v6, v8, :cond_10

    sget-object v8, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    invoke-virtual {v4, v8}, La/e/a/c/g;->b(La/e/a/c/g$a;)V

    :cond_10
    invoke-virtual {v4, p1, v0}, La/e/a/c/g;->a(La/e/a/d;Z)V

    sget-object v8, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v7, v8, :cond_11

    invoke-virtual {v4, v7}, La/e/a/c/g;->a(La/e/a/c/g$a;)V

    :cond_11
    sget-object v7, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v6, v7, :cond_13

    invoke-virtual {v4, v6}, La/e/a/c/g;->b(La/e/a/c/g$a;)V

    goto :goto_a

    :cond_12
    invoke-static {p0, p1, v4}, La/e/a/c/n;->a(La/e/a/c/h;La/e/a/d;La/e/a/c/g;)V

    invoke-virtual {v4}, La/e/a/c/g;->b()Z

    move-result v6

    if-nez v6, :cond_13

    invoke-virtual {v4, p1, v0}, La/e/a/c/g;->a(La/e/a/d;Z)V

    :cond_13
    :goto_a
    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_14
    iget v0, p0, La/e/a/c/h;->fb:I

    const/4 v1, 0x0

    if-lez v0, :cond_15

    invoke-static {p0, p1, v1, v2}, La/e/a/c/b;->a(La/e/a/c/h;La/e/a/d;Ljava/util/ArrayList;I)V

    :cond_15
    iget v0, p0, La/e/a/c/h;->gb:I

    if-lez v0, :cond_16

    invoke-static {p0, p1, v1, v5}, La/e/a/c/b;->a(La/e/a/c/h;La/e/a/d;Ljava/util/ArrayList;I)V

    :cond_16
    return v5
.end method

.method public ba()La/e/a/c/a/b$b;
    .locals 1

    iget-object v0, p0, La/e/a/c/h;->Ya:La/e/a/c/a/b$b;

    return-object v0
.end method

.method c(La/e/a/c/e;)V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    goto/32 :goto_a

    nop

    :goto_2
    iget-object v0, p0, La/e/a/c/h;->vb:Ljava/lang/ref/WeakReference;

    goto/32 :goto_c

    nop

    :goto_3
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_4
    invoke-virtual {p1}, La/e/a/c/e;->b()I

    move-result v0

    goto/32 :goto_9

    nop

    :goto_5
    new-instance v0, Ljava/lang/ref/WeakReference;

    goto/32 :goto_1

    nop

    :goto_6
    invoke-virtual {v1}, La/e/a/c/e;->b()I

    move-result v1

    goto/32 :goto_e

    nop

    :goto_7
    check-cast v1, La/e/a/c/e;

    goto/32 :goto_6

    nop

    :goto_8
    if-nez v0, :cond_0

    goto/32 :goto_f

    :cond_0
    goto/32 :goto_4

    nop

    :goto_9
    iget-object v1, p0, La/e/a/c/h;->vb:Ljava/lang/ref/WeakReference;

    goto/32 :goto_d

    nop

    :goto_a
    iput-object v0, p0, La/e/a/c/h;->vb:Ljava/lang/ref/WeakReference;

    :goto_b
    goto/32 :goto_0

    nop

    :goto_c
    if-nez v0, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_3

    nop

    :goto_d
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_7

    nop

    :goto_e
    if-gt v0, v1, :cond_2

    goto/32 :goto_b

    :cond_2
    :goto_f
    goto/32 :goto_5

    nop
.end method

.method public ca()I
    .locals 1

    iget v0, p0, La/e/a/c/h;->ob:I

    return v0
.end method

.method d(La/e/a/c/e;)V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    goto/32 :goto_4

    nop

    :goto_1
    iget-object v1, p0, La/e/a/c/h;->tb:Ljava/lang/ref/WeakReference;

    goto/32 :goto_d

    nop

    :goto_2
    iget-object v0, p0, La/e/a/c/h;->tb:Ljava/lang/ref/WeakReference;

    goto/32 :goto_e

    nop

    :goto_3
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_4
    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    goto/32 :goto_8

    nop

    :goto_5
    return-void

    :goto_6
    if-nez v0, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_c

    nop

    :goto_7
    invoke-virtual {v1}, La/e/a/c/e;->b()I

    move-result v1

    goto/32 :goto_a

    nop

    :goto_8
    iput-object v0, p0, La/e/a/c/h;->tb:Ljava/lang/ref/WeakReference;

    :goto_9
    goto/32 :goto_5

    nop

    :goto_a
    if-gt v0, v1, :cond_1

    goto/32 :goto_9

    :cond_1
    :goto_b
    goto/32 :goto_0

    nop

    :goto_c
    invoke-virtual {p1}, La/e/a/c/e;->b()I

    move-result v0

    goto/32 :goto_1

    nop

    :goto_d
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_f

    nop

    :goto_e
    if-nez v0, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_3

    nop

    :goto_f
    check-cast v1, La/e/a/c/e;

    goto/32 :goto_7

    nop
.end method

.method public da()La/e/a/d;
    .locals 1

    iget-object v0, p0, La/e/a/c/h;->ab:La/e/a/d;

    return-object v0
.end method

.method public e(Z)Z
    .locals 1

    iget-object v0, p0, La/e/a/c/h;->Wa:La/e/a/c/a/e;

    invoke-virtual {v0, p1}, La/e/a/c/a/e;->a(Z)Z

    move-result p1

    return p1
.end method

.method public ea()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public f(Z)Z
    .locals 1

    iget-object v0, p0, La/e/a/c/h;->Wa:La/e/a/c/a/e;

    invoke-virtual {v0, p1}, La/e/a/c/a/e;->b(Z)Z

    move-result p1

    return p1
.end method

.method public fa()V
    .locals 1

    iget-object v0, p0, La/e/a/c/h;->Wa:La/e/a/c/a/e;

    invoke-virtual {v0}, La/e/a/c/a/e;->b()V

    return-void
.end method

.method public g(Z)V
    .locals 0

    iput-boolean p1, p0, La/e/a/c/h;->Za:Z

    return-void
.end method

.method public ga()V
    .locals 1

    iget-object v0, p0, La/e/a/c/h;->Wa:La/e/a/c/a/e;

    invoke-virtual {v0}, La/e/a/c/a/e;->c()V

    return-void
.end method

.method public ha()Z
    .locals 1

    iget-boolean v0, p0, La/e/a/c/h;->rb:Z

    return v0
.end method

.method public ia()Z
    .locals 1

    iget-boolean v0, p0, La/e/a/c/h;->Za:Z

    return v0
.end method

.method public ja()Z
    .locals 1

    iget-boolean v0, p0, La/e/a/c/h;->qb:Z

    return v0
.end method

.method public ka()V
    .locals 1

    iget-object v0, p0, La/e/a/c/h;->Va:La/e/a/c/a/b;

    invoke-virtual {v0, p0}, La/e/a/c/a/b;->a(La/e/a/c/h;)V

    return-void
.end method

.method public y(I)Z
    .locals 1

    iget v0, p0, La/e/a/c/h;->ob:I

    and-int/2addr v0, p1

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public z(I)V
    .locals 0

    iput p1, p0, La/e/a/c/h;->ob:I

    iget-object p1, p0, La/e/a/c/h;->ab:La/e/a/d;

    const/16 p1, 0x200

    invoke-virtual {p0, p1}, La/e/a/c/h;->y(I)Z

    move-result p1

    sput-boolean p1, La/e/a/d;->a:Z

    return-void
.end method
