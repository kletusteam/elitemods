.class public abstract La/e/a/a/a/f;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La/e/a/a/a/f$a;,
        La/e/a/a/a/f$b;
    }
.end annotation


# instance fields
.field private a:La/e/a/a/a/b;

.field private b:La/e/a/a/a/f$a;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:Ljava/lang/String;

.field public f:I

.field g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "La/e/a/a/a/f$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, La/e/a/a/a/f;->d:I

    const/4 v1, 0x0

    iput-object v1, p0, La/e/a/a/a/f;->e:Ljava/lang/String;

    iput v0, p0, La/e/a/a/a/f;->f:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, La/e/a/a/a/f;->g:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public a(F)F
    .locals 2

    iget-object v0, p0, La/e/a/a/a/f;->b:La/e/a/a/a/f$a;

    invoke-virtual {v0, p1}, La/e/a/a/a/f$a;->b(F)D

    move-result-wide v0

    double-to-float p1, v0

    return p1
.end method

.method public a(IILjava/lang/String;IFFFF)V
    .locals 10

    move-object v0, p0

    move v1, p4

    iget-object v2, v0, La/e/a/a/a/f;->g:Ljava/util/ArrayList;

    new-instance v9, La/e/a/a/a/f$b;

    move-object v3, v9

    move v4, p1

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-direct/range {v3 .. v8}, La/e/a/a/a/f$b;-><init>(IFFFF)V

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iput v1, v0, La/e/a/a/a/f;->f:I

    :cond_0
    move v1, p2

    iput v1, v0, La/e/a/a/a/f;->d:I

    move-object v1, p3

    iput-object v1, v0, La/e/a/a/a/f;->e:Ljava/lang/String;

    return-void
.end method

.method public a(IILjava/lang/String;IFFFFLjava/lang/Object;)V
    .locals 10

    move-object v0, p0

    move v1, p4

    iget-object v2, v0, La/e/a/a/a/f;->g:Ljava/util/ArrayList;

    new-instance v9, La/e/a/a/a/f$b;

    move-object v3, v9

    move v4, p1

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-direct/range {v3 .. v8}, La/e/a/a/a/f$b;-><init>(IFFFF)V

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iput v1, v0, La/e/a/a/a/f;->f:I

    :cond_0
    move v1, p2

    iput v1, v0, La/e/a/a/a/f;->d:I

    move-object/from16 v1, p9

    invoke-virtual {p0, v1}, La/e/a/a/a/f;->a(Ljava/lang/Object;)V

    move-object v1, p3

    iput-object v1, v0, La/e/a/a/a/f;->e:Ljava/lang/String;

    return-void
.end method

.method protected a(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, La/e/a/a/a/f;->c:Ljava/lang/String;

    return-void
.end method

.method public a()Z
    .locals 2

    iget v0, p0, La/e/a/a/a/f;->f:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public b(F)F
    .locals 2

    iget-object v0, p0, La/e/a/a/a/f;->b:La/e/a/a/a/f$a;

    invoke-virtual {v0, p1}, La/e/a/a/a/f$a;->a(F)D

    move-result-wide v0

    double-to-float p1, v0

    return p1
.end method

.method public c(F)V
    .locals 16

    move-object/from16 v0, p0

    iget-object v1, v0, La/e/a/a/a/f;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget-object v2, v0, La/e/a/a/a/f;->g:Ljava/util/ArrayList;

    new-instance v3, La/e/a/a/a/e;

    invoke-direct {v3, v0}, La/e/a/a/a/e;-><init>(La/e/a/a/a/f;)V

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    new-array v2, v1, [D

    const/4 v3, 0x3

    filled-new-array {v1, v3}, [I

    move-result-object v3

    const-class v4, D

    invoke-static {v4, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [[D

    new-instance v4, La/e/a/a/a/f$a;

    iget v5, v0, La/e/a/a/a/f;->d:I

    iget-object v6, v0, La/e/a/a/a/f;->e:Ljava/lang/String;

    iget v7, v0, La/e/a/a/a/f;->f:I

    invoke-direct {v4, v5, v6, v7, v1}, La/e/a/a/a/f$a;-><init>(ILjava/lang/String;II)V

    iput-object v4, v0, La/e/a/a/a/f;->b:La/e/a/a/a/f$a;

    iget-object v1, v0, La/e/a/a/a/f;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v4, 0x0

    move v12, v4

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/a/a/f$b;

    iget v8, v5, La/e/a/a/a/f$b;->d:F

    float-to-double v6, v8

    const-wide v9, 0x3f847ae147ae147bL    # 0.01

    mul-double/2addr v6, v9

    aput-wide v6, v2, v12

    aget-object v6, v3, v12

    iget v11, v5, La/e/a/a/a/f$b;->b:F

    float-to-double v9, v11

    aput-wide v9, v6, v4

    aget-object v6, v3, v12

    iget v9, v5, La/e/a/a/a/f$b;->c:F

    float-to-double v13, v9

    const/4 v15, 0x1

    aput-wide v13, v6, v15

    aget-object v6, v3, v12

    const/4 v7, 0x2

    iget v10, v5, La/e/a/a/a/f$b;->e:F

    float-to-double v13, v10

    aput-wide v13, v6, v7

    iget-object v6, v0, La/e/a/a/a/f;->b:La/e/a/a/a/f$a;

    iget v7, v5, La/e/a/a/a/f$b;->a:I

    move-object v5, v6

    move v6, v12

    invoke-virtual/range {v5 .. v11}, La/e/a/a/a/f$a;->a(IIFFFF)V

    add-int/2addr v12, v15

    goto :goto_0

    :cond_1
    iget-object v1, v0, La/e/a/a/a/f;->b:La/e/a/a/a/f$a;

    move/from16 v5, p1

    invoke-virtual {v1, v5}, La/e/a/a/a/f$a;->c(F)V

    invoke-static {v4, v2, v3}, La/e/a/a/a/b;->a(I[D[[D)La/e/a/a/a/b;

    move-result-object v1

    iput-object v1, v0, La/e/a/a/a/f;->a:La/e/a/a/a/b;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    iget-object v0, p0, La/e/a/a/a/f;->c:Ljava/lang/String;

    new-instance v1, Ljava/text/DecimalFormat;

    const-string v2, "##.##"

    invoke-direct {v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, La/e/a/a/a/f;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, La/e/a/a/a/f$b;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "["

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, v3, La/e/a/a/a/f$b;->a:I

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " , "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, v3, La/e/a/a/a/f$b;->b:F

    float-to-double v5, v0

    invoke-virtual {v1, v5, v6}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "] "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-object v0
.end method
