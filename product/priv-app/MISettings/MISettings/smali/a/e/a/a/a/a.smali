.class public La/e/a/a/a/a;
.super La/e/a/a/a/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La/e/a/a/a/a$a;
    }
.end annotation


# instance fields
.field private final a:[D

.field b:[La/e/a/a/a/a$a;

.field private c:Z


# direct methods
.method public constructor <init>([I[D[[D)V
    .locals 24

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct/range {p0 .. p0}, La/e/a/a/a/b;-><init>()V

    const/4 v2, 0x1

    iput-boolean v2, v0, La/e/a/a/a/a;->c:Z

    iput-object v1, v0, La/e/a/a/a/a;->a:[D

    array-length v3, v1

    sub-int/2addr v3, v2

    new-array v3, v3, [La/e/a/a/a/a$a;

    iput-object v3, v0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    const/4 v3, 0x0

    move v5, v2

    move v6, v5

    move v4, v3

    :goto_0
    iget-object v7, v0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    array-length v7, v7

    if-ge v4, v7, :cond_5

    aget v7, p1, v4

    const/4 v8, 0x3

    const/4 v9, 0x2

    if-eqz v7, :cond_4

    if-eq v7, v2, :cond_3

    if-eq v7, v9, :cond_2

    if-eq v7, v8, :cond_0

    move v9, v6

    goto :goto_2

    :cond_0
    if-ne v5, v2, :cond_1

    goto :goto_1

    :cond_1
    move v9, v2

    :cond_2
    :goto_1
    move v5, v9

    goto :goto_2

    :cond_3
    move v5, v2

    move v9, v5

    goto :goto_2

    :cond_4
    move v9, v8

    :goto_2
    iget-object v6, v0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    new-instance v7, La/e/a/a/a/a$a;

    aget-wide v12, v1, v4

    add-int/lit8 v8, v4, 0x1

    aget-wide v14, v1, v8

    aget-object v10, p3, v4

    aget-wide v16, v10, v3

    aget-object v10, p3, v4

    aget-wide v18, v10, v2

    aget-object v10, p3, v8

    aget-wide v20, v10, v3

    aget-object v10, p3, v8

    aget-wide v22, v10, v2

    move-object v10, v7

    move v11, v9

    invoke-direct/range {v10 .. v23}, La/e/a/a/a/a$a;-><init>(IDDDDDD)V

    aput-object v7, v6, v4

    move v4, v8

    move v6, v9

    goto :goto_0

    :cond_5
    return-void
.end method


# virtual methods
.method public a(DI)D
    .locals 6

    iget-boolean v0, p0, La/e/a/a/a/a;->c:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v2, v0, v1

    iget-wide v2, v2, La/e/a/a/a/a$a;->d:D

    cmpg-double v2, p1, v2

    if-gez v2, :cond_3

    aget-object v2, v0, v1

    iget-wide v2, v2, La/e/a/a/a/a$a;->d:D

    aget-object v4, v0, v1

    iget-wide v4, v4, La/e/a/a/a/a$a;->d:D

    sub-double/2addr p1, v4

    aget-object v4, v0, v1

    iget-boolean v4, v4, La/e/a/a/a/a$a;->s:Z

    if-eqz v4, :cond_1

    if-nez p3, :cond_0

    aget-object p3, v0, v1

    invoke-virtual {p3, v2, v3}, La/e/a/a/a/a$a;->c(D)D

    move-result-wide v4

    iget-object p3, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object p3, p3, v1

    invoke-virtual {p3, v2, v3}, La/e/a/a/a/a$a;->a(D)D

    move-result-wide v0

    :goto_0
    mul-double/2addr p1, v0

    add-double/2addr v4, p1

    return-wide v4

    :cond_0
    aget-object p3, v0, v1

    invoke-virtual {p3, v2, v3}, La/e/a/a/a/a$a;->d(D)D

    move-result-wide v4

    iget-object p3, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object p3, p3, v1

    invoke-virtual {p3, v2, v3}, La/e/a/a/a/a$a;->b(D)D

    move-result-wide v0

    goto :goto_0

    :cond_1
    aget-object v0, v0, v1

    invoke-virtual {v0, v2, v3}, La/e/a/a/a/a$a;->f(D)V

    if-nez p3, :cond_2

    iget-object p3, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object p3, p3, v1

    invoke-virtual {p3}, La/e/a/a/a/a$a;->c()D

    move-result-wide v2

    iget-object p3, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object p3, p3, v1

    invoke-virtual {p3}, La/e/a/a/a/a$a;->a()D

    move-result-wide v0

    :goto_1
    mul-double/2addr p1, v0

    add-double/2addr v2, p1

    return-wide v2

    :cond_2
    iget-object p3, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object p3, p3, v1

    invoke-virtual {p3}, La/e/a/a/a/a$a;->d()D

    move-result-wide v2

    iget-object p3, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object p3, p3, v1

    invoke-virtual {p3}, La/e/a/a/a/a$a;->b()D

    move-result-wide v0

    goto :goto_1

    :cond_3
    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v0, v2

    iget-wide v2, v2, La/e/a/a/a/a$a;->e:D

    cmpl-double v2, p1, v2

    if-lez v2, :cond_7

    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v1, v0, v1

    iget-wide v1, v1, La/e/a/a/a/a$a;->e:D

    sub-double/2addr p1, v1

    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    if-nez p3, :cond_4

    aget-object p3, v0, v3

    invoke-virtual {p3, v1, v2}, La/e/a/a/a/a$a;->c(D)D

    move-result-wide v4

    iget-object p3, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object p3, p3, v3

    invoke-virtual {p3, v1, v2}, La/e/a/a/a/a$a;->a(D)D

    move-result-wide v0

    goto :goto_0

    :cond_4
    aget-object p3, v0, v3

    invoke-virtual {p3, v1, v2}, La/e/a/a/a/a$a;->d(D)D

    move-result-wide v4

    iget-object p3, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object p3, p3, v3

    invoke-virtual {p3, v1, v2}, La/e/a/a/a/a$a;->b(D)D

    move-result-wide v0

    goto :goto_0

    :cond_5
    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v2, v0, v1

    iget-wide v2, v2, La/e/a/a/a/a$a;->d:D

    cmpg-double v2, p1, v2

    if-gez v2, :cond_6

    aget-object p1, v0, v1

    iget-wide p1, p1, La/e/a/a/a/a$a;->d:D

    goto :goto_2

    :cond_6
    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v0, v2

    iget-wide v2, v2, La/e/a/a/a/a$a;->e:D

    cmpl-double v2, p1, v2

    if-lez v2, :cond_7

    array-length p1, v0

    add-int/lit8 p1, p1, -0x1

    aget-object p1, v0, p1

    iget-wide p1, p1, La/e/a/a/a/a$a;->e:D

    :cond_7
    :goto_2
    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    array-length v2, v0

    if-ge v1, v2, :cond_c

    aget-object v2, v0, v1

    iget-wide v2, v2, La/e/a/a/a/a$a;->e:D

    cmpg-double v2, p1, v2

    if-gtz v2, :cond_b

    aget-object v2, v0, v1

    iget-boolean v2, v2, La/e/a/a/a/a$a;->s:Z

    if-eqz v2, :cond_9

    if-nez p3, :cond_8

    aget-object p3, v0, v1

    invoke-virtual {p3, p1, p2}, La/e/a/a/a/a$a;->c(D)D

    move-result-wide p1

    return-wide p1

    :cond_8
    aget-object p3, v0, v1

    invoke-virtual {p3, p1, p2}, La/e/a/a/a/a$a;->d(D)D

    move-result-wide p1

    return-wide p1

    :cond_9
    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2}, La/e/a/a/a/a$a;->f(D)V

    if-nez p3, :cond_a

    iget-object p1, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object p1, p1, v1

    invoke-virtual {p1}, La/e/a/a/a/a$a;->c()D

    move-result-wide p1

    return-wide p1

    :cond_a
    iget-object p1, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object p1, p1, v1

    invoke-virtual {p1}, La/e/a/a/a/a$a;->d()D

    move-result-wide p1

    return-wide p1

    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_c
    const-wide/high16 p1, 0x7ff8000000000000L    # Double.NaN

    return-wide p1
.end method

.method public a(D[D)V
    .locals 10

    iget-boolean v0, p0, La/e/a/a/a/a;->c:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_3

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v3, v0, v2

    iget-wide v3, v3, La/e/a/a/a/a$a;->d:D

    cmpg-double v3, p1, v3

    if-gez v3, :cond_1

    aget-object v3, v0, v2

    iget-wide v3, v3, La/e/a/a/a/a$a;->d:D

    aget-object v5, v0, v2

    iget-wide v5, v5, La/e/a/a/a/a$a;->d:D

    sub-double/2addr p1, v5

    aget-object v5, v0, v2

    iget-boolean v5, v5, La/e/a/a/a/a$a;->s:Z

    if-eqz v5, :cond_0

    aget-object v0, v0, v2

    invoke-virtual {v0, v3, v4}, La/e/a/a/a/a$a;->c(D)D

    move-result-wide v5

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v0, v0, v2

    invoke-virtual {v0, v3, v4}, La/e/a/a/a/a$a;->a(D)D

    move-result-wide v7

    mul-double/2addr v7, p1

    add-double/2addr v5, v7

    aput-wide v5, p3, v2

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v0, v0, v2

    invoke-virtual {v0, v3, v4}, La/e/a/a/a/a$a;->d(D)D

    move-result-wide v5

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v0, v0, v2

    invoke-virtual {v0, v3, v4}, La/e/a/a/a/a$a;->b(D)D

    move-result-wide v2

    mul-double/2addr p1, v2

    add-double/2addr v5, p1

    aput-wide v5, p3, v1

    goto :goto_0

    :cond_0
    aget-object v0, v0, v2

    invoke-virtual {v0, v3, v4}, La/e/a/a/a/a$a;->f(D)V

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v0, v0, v2

    invoke-virtual {v0}, La/e/a/a/a/a$a;->c()D

    move-result-wide v3

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v0, v0, v2

    invoke-virtual {v0}, La/e/a/a/a/a$a;->a()D

    move-result-wide v5

    mul-double/2addr v5, p1

    add-double/2addr v3, v5

    aput-wide v3, p3, v2

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v0, v0, v2

    invoke-virtual {v0}, La/e/a/a/a/a$a;->d()D

    move-result-wide v3

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v0, v0, v2

    invoke-virtual {v0}, La/e/a/a/a/a$a;->b()D

    move-result-wide v5

    mul-double/2addr p1, v5

    add-double/2addr v3, p1

    aput-wide v3, p3, v1

    :goto_0
    return-void

    :cond_1
    array-length v3, v0

    sub-int/2addr v3, v1

    aget-object v3, v0, v3

    iget-wide v3, v3, La/e/a/a/a/a$a;->e:D

    cmpl-double v3, p1, v3

    if-lez v3, :cond_5

    array-length v3, v0

    sub-int/2addr v3, v1

    aget-object v3, v0, v3

    iget-wide v3, v3, La/e/a/a/a/a$a;->e:D

    sub-double v5, p1, v3

    array-length v7, v0

    sub-int/2addr v7, v1

    aget-object v8, v0, v7

    iget-boolean v8, v8, La/e/a/a/a/a$a;->s:Z

    if-eqz v8, :cond_2

    aget-object p1, v0, v7

    invoke-virtual {p1, v3, v4}, La/e/a/a/a/a$a;->c(D)D

    move-result-wide p1

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v0, v0, v7

    invoke-virtual {v0, v3, v4}, La/e/a/a/a/a$a;->a(D)D

    move-result-wide v8

    mul-double/2addr v8, v5

    add-double/2addr p1, v8

    aput-wide p1, p3, v2

    iget-object p1, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object p1, p1, v7

    invoke-virtual {p1, v3, v4}, La/e/a/a/a/a$a;->d(D)D

    move-result-wide p1

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v0, v0, v7

    invoke-virtual {v0, v3, v4}, La/e/a/a/a/a$a;->b(D)D

    move-result-wide v2

    mul-double/2addr v5, v2

    add-double/2addr p1, v5

    aput-wide p1, p3, v1

    goto :goto_1

    :cond_2
    aget-object v0, v0, v7

    invoke-virtual {v0, p1, p2}, La/e/a/a/a/a$a;->f(D)V

    iget-object p1, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object p1, p1, v7

    invoke-virtual {p1}, La/e/a/a/a/a$a;->c()D

    move-result-wide p1

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v0, v0, v7

    invoke-virtual {v0}, La/e/a/a/a/a$a;->a()D

    move-result-wide v3

    mul-double/2addr v3, v5

    add-double/2addr p1, v3

    aput-wide p1, p3, v2

    iget-object p1, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object p1, p1, v7

    invoke-virtual {p1}, La/e/a/a/a/a$a;->d()D

    move-result-wide p1

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v0, v0, v7

    invoke-virtual {v0}, La/e/a/a/a/a$a;->b()D

    move-result-wide v2

    mul-double/2addr v5, v2

    add-double/2addr p1, v5

    aput-wide p1, p3, v1

    :goto_1
    return-void

    :cond_3
    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v3, v0, v2

    iget-wide v3, v3, La/e/a/a/a/a$a;->d:D

    cmpg-double v3, p1, v3

    if-gez v3, :cond_4

    aget-object p1, v0, v2

    iget-wide p1, p1, La/e/a/a/a/a$a;->d:D

    :cond_4
    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    array-length v3, v0

    sub-int/2addr v3, v1

    aget-object v3, v0, v3

    iget-wide v3, v3, La/e/a/a/a/a$a;->e:D

    cmpl-double v3, p1, v3

    if-lez v3, :cond_5

    array-length p1, v0

    sub-int/2addr p1, v1

    aget-object p1, v0, p1

    iget-wide p1, p1, La/e/a/a/a/a$a;->e:D

    :cond_5
    move v0, v2

    :goto_2
    iget-object v3, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    array-length v4, v3

    if-ge v0, v4, :cond_8

    aget-object v4, v3, v0

    iget-wide v4, v4, La/e/a/a/a/a$a;->e:D

    cmpg-double v4, p1, v4

    if-gtz v4, :cond_7

    aget-object v4, v3, v0

    iget-boolean v4, v4, La/e/a/a/a/a$a;->s:Z

    if-eqz v4, :cond_6

    aget-object v3, v3, v0

    invoke-virtual {v3, p1, p2}, La/e/a/a/a/a$a;->c(D)D

    move-result-wide v3

    aput-wide v3, p3, v2

    iget-object v2, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v0, v2, v0

    invoke-virtual {v0, p1, p2}, La/e/a/a/a/a$a;->d(D)D

    move-result-wide p1

    aput-wide p1, p3, v1

    return-void

    :cond_6
    aget-object v3, v3, v0

    invoke-virtual {v3, p1, p2}, La/e/a/a/a/a$a;->f(D)V

    iget-object p1, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object p1, p1, v0

    invoke-virtual {p1}, La/e/a/a/a/a$a;->c()D

    move-result-wide p1

    aput-wide p1, p3, v2

    iget-object p1, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object p1, p1, v0

    invoke-virtual {p1}, La/e/a/a/a/a$a;->d()D

    move-result-wide p1

    aput-wide p1, p3, v1

    return-void

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_8
    return-void
.end method

.method public a(D[F)V
    .locals 10

    iget-boolean v0, p0, La/e/a/a/a/a;->c:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_3

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v3, v0, v2

    iget-wide v3, v3, La/e/a/a/a/a$a;->d:D

    cmpg-double v3, p1, v3

    if-gez v3, :cond_1

    aget-object v3, v0, v2

    iget-wide v3, v3, La/e/a/a/a/a$a;->d:D

    aget-object v5, v0, v2

    iget-wide v5, v5, La/e/a/a/a/a$a;->d:D

    sub-double/2addr p1, v5

    aget-object v5, v0, v2

    iget-boolean v5, v5, La/e/a/a/a/a$a;->s:Z

    if-eqz v5, :cond_0

    aget-object v0, v0, v2

    invoke-virtual {v0, v3, v4}, La/e/a/a/a/a$a;->c(D)D

    move-result-wide v5

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v0, v0, v2

    invoke-virtual {v0, v3, v4}, La/e/a/a/a/a$a;->a(D)D

    move-result-wide v7

    mul-double/2addr v7, p1

    add-double/2addr v5, v7

    double-to-float v0, v5

    aput v0, p3, v2

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v0, v0, v2

    invoke-virtual {v0, v3, v4}, La/e/a/a/a/a$a;->d(D)D

    move-result-wide v5

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v0, v0, v2

    invoke-virtual {v0, v3, v4}, La/e/a/a/a/a$a;->b(D)D

    move-result-wide v2

    mul-double/2addr p1, v2

    add-double/2addr v5, p1

    double-to-float p1, v5

    aput p1, p3, v1

    goto :goto_0

    :cond_0
    aget-object v0, v0, v2

    invoke-virtual {v0, v3, v4}, La/e/a/a/a/a$a;->f(D)V

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v0, v0, v2

    invoke-virtual {v0}, La/e/a/a/a/a$a;->c()D

    move-result-wide v3

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v0, v0, v2

    invoke-virtual {v0}, La/e/a/a/a/a$a;->a()D

    move-result-wide v5

    mul-double/2addr v5, p1

    add-double/2addr v3, v5

    double-to-float v0, v3

    aput v0, p3, v2

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v0, v0, v2

    invoke-virtual {v0}, La/e/a/a/a/a$a;->d()D

    move-result-wide v3

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v0, v0, v2

    invoke-virtual {v0}, La/e/a/a/a/a$a;->b()D

    move-result-wide v5

    mul-double/2addr p1, v5

    add-double/2addr v3, p1

    double-to-float p1, v3

    aput p1, p3, v1

    :goto_0
    return-void

    :cond_1
    array-length v3, v0

    sub-int/2addr v3, v1

    aget-object v3, v0, v3

    iget-wide v3, v3, La/e/a/a/a/a$a;->e:D

    cmpl-double v3, p1, v3

    if-lez v3, :cond_5

    array-length v3, v0

    sub-int/2addr v3, v1

    aget-object v3, v0, v3

    iget-wide v3, v3, La/e/a/a/a/a$a;->e:D

    sub-double v5, p1, v3

    array-length v7, v0

    sub-int/2addr v7, v1

    aget-object v8, v0, v7

    iget-boolean v8, v8, La/e/a/a/a/a$a;->s:Z

    if-eqz v8, :cond_2

    aget-object p1, v0, v7

    invoke-virtual {p1, v3, v4}, La/e/a/a/a/a$a;->c(D)D

    move-result-wide p1

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v0, v0, v7

    invoke-virtual {v0, v3, v4}, La/e/a/a/a/a$a;->a(D)D

    move-result-wide v8

    mul-double/2addr v8, v5

    add-double/2addr p1, v8

    double-to-float p1, p1

    aput p1, p3, v2

    iget-object p1, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object p1, p1, v7

    invoke-virtual {p1, v3, v4}, La/e/a/a/a/a$a;->d(D)D

    move-result-wide p1

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v0, v0, v7

    invoke-virtual {v0, v3, v4}, La/e/a/a/a/a$a;->b(D)D

    move-result-wide v2

    mul-double/2addr v5, v2

    add-double/2addr p1, v5

    double-to-float p1, p1

    aput p1, p3, v1

    goto :goto_1

    :cond_2
    aget-object v0, v0, v7

    invoke-virtual {v0, p1, p2}, La/e/a/a/a/a$a;->f(D)V

    iget-object p1, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object p1, p1, v7

    invoke-virtual {p1}, La/e/a/a/a/a$a;->c()D

    move-result-wide p1

    double-to-float p1, p1

    aput p1, p3, v2

    iget-object p1, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object p1, p1, v7

    invoke-virtual {p1}, La/e/a/a/a/a$a;->d()D

    move-result-wide p1

    double-to-float p1, p1

    aput p1, p3, v1

    :goto_1
    return-void

    :cond_3
    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v3, v0, v2

    iget-wide v3, v3, La/e/a/a/a/a$a;->d:D

    cmpg-double v3, p1, v3

    if-gez v3, :cond_4

    aget-object p1, v0, v2

    iget-wide p1, p1, La/e/a/a/a/a$a;->d:D

    goto :goto_2

    :cond_4
    array-length v3, v0

    sub-int/2addr v3, v1

    aget-object v3, v0, v3

    iget-wide v3, v3, La/e/a/a/a/a$a;->e:D

    cmpl-double v3, p1, v3

    if-lez v3, :cond_5

    array-length p1, v0

    sub-int/2addr p1, v1

    aget-object p1, v0, p1

    iget-wide p1, p1, La/e/a/a/a/a$a;->e:D

    :cond_5
    :goto_2
    move v0, v2

    :goto_3
    iget-object v3, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    array-length v4, v3

    if-ge v0, v4, :cond_8

    aget-object v4, v3, v0

    iget-wide v4, v4, La/e/a/a/a/a$a;->e:D

    cmpg-double v4, p1, v4

    if-gtz v4, :cond_7

    aget-object v4, v3, v0

    iget-boolean v4, v4, La/e/a/a/a/a$a;->s:Z

    if-eqz v4, :cond_6

    aget-object v3, v3, v0

    invoke-virtual {v3, p1, p2}, La/e/a/a/a/a$a;->c(D)D

    move-result-wide v3

    double-to-float v3, v3

    aput v3, p3, v2

    iget-object v2, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v0, v2, v0

    invoke-virtual {v0, p1, p2}, La/e/a/a/a/a$a;->d(D)D

    move-result-wide p1

    double-to-float p1, p1

    aput p1, p3, v1

    return-void

    :cond_6
    aget-object v3, v3, v0

    invoke-virtual {v3, p1, p2}, La/e/a/a/a/a$a;->f(D)V

    iget-object p1, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object p1, p1, v0

    invoke-virtual {p1}, La/e/a/a/a/a$a;->c()D

    move-result-wide p1

    double-to-float p1, p1

    aput p1, p3, v2

    iget-object p1, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object p1, p1, v0

    invoke-virtual {p1}, La/e/a/a/a/a$a;->d()D

    move-result-wide p1

    double-to-float p1, p1

    aput p1, p3, v1

    return-void

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_8
    return-void
.end method

.method public a()[D
    .locals 1

    iget-object v0, p0, La/e/a/a/a/a;->a:[D

    return-object v0
.end method

.method public b(DI)D
    .locals 4

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    const/4 v1, 0x0

    aget-object v2, v0, v1

    iget-wide v2, v2, La/e/a/a/a/a$a;->d:D

    cmpg-double v2, p1, v2

    if-gez v2, :cond_0

    aget-object p1, v0, v1

    iget-wide p1, p1, La/e/a/a/a/a$a;->d:D

    :cond_0
    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v0, v2

    iget-wide v2, v2, La/e/a/a/a/a$a;->e:D

    cmpl-double v2, p1, v2

    if-lez v2, :cond_1

    array-length p1, v0

    add-int/lit8 p1, p1, -0x1

    aget-object p1, v0, p1

    iget-wide p1, p1, La/e/a/a/a/a$a;->e:D

    :cond_1
    :goto_0
    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    array-length v2, v0

    if-ge v1, v2, :cond_6

    aget-object v2, v0, v1

    iget-wide v2, v2, La/e/a/a/a/a$a;->e:D

    cmpg-double v2, p1, v2

    if-gtz v2, :cond_5

    aget-object v2, v0, v1

    iget-boolean v2, v2, La/e/a/a/a/a$a;->s:Z

    if-eqz v2, :cond_3

    if-nez p3, :cond_2

    aget-object p3, v0, v1

    invoke-virtual {p3, p1, p2}, La/e/a/a/a/a$a;->a(D)D

    move-result-wide p1

    return-wide p1

    :cond_2
    aget-object p3, v0, v1

    invoke-virtual {p3, p1, p2}, La/e/a/a/a/a$a;->b(D)D

    move-result-wide p1

    return-wide p1

    :cond_3
    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2}, La/e/a/a/a/a$a;->f(D)V

    if-nez p3, :cond_4

    iget-object p1, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object p1, p1, v1

    invoke-virtual {p1}, La/e/a/a/a/a$a;->a()D

    move-result-wide p1

    return-wide p1

    :cond_4
    iget-object p1, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object p1, p1, v1

    invoke-virtual {p1}, La/e/a/a/a/a$a;->b()D

    move-result-wide p1

    return-wide p1

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_6
    const-wide/high16 p1, 0x7ff8000000000000L    # Double.NaN

    return-wide p1
.end method

.method public b(D[D)V
    .locals 6

    iget-object v0, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    const/4 v1, 0x0

    aget-object v2, v0, v1

    iget-wide v2, v2, La/e/a/a/a/a$a;->d:D

    cmpg-double v2, p1, v2

    const/4 v3, 0x1

    if-gez v2, :cond_0

    aget-object p1, v0, v1

    iget-wide p1, p1, La/e/a/a/a/a$a;->d:D

    goto :goto_0

    :cond_0
    array-length v2, v0

    sub-int/2addr v2, v3

    aget-object v2, v0, v2

    iget-wide v4, v2, La/e/a/a/a/a$a;->e:D

    cmpl-double v2, p1, v4

    if-lez v2, :cond_1

    array-length p1, v0

    sub-int/2addr p1, v3

    aget-object p1, v0, p1

    iget-wide p1, p1, La/e/a/a/a/a$a;->e:D

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    iget-object v2, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    array-length v4, v2

    if-ge v0, v4, :cond_4

    aget-object v4, v2, v0

    iget-wide v4, v4, La/e/a/a/a/a$a;->e:D

    cmpg-double v4, p1, v4

    if-gtz v4, :cond_3

    aget-object v4, v2, v0

    iget-boolean v4, v4, La/e/a/a/a/a$a;->s:Z

    if-eqz v4, :cond_2

    aget-object v2, v2, v0

    invoke-virtual {v2, p1, p2}, La/e/a/a/a/a$a;->a(D)D

    move-result-wide v4

    aput-wide v4, p3, v1

    iget-object v1, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object v0, v1, v0

    invoke-virtual {v0, p1, p2}, La/e/a/a/a/a$a;->b(D)D

    move-result-wide p1

    aput-wide p1, p3, v3

    return-void

    :cond_2
    aget-object v2, v2, v0

    invoke-virtual {v2, p1, p2}, La/e/a/a/a/a$a;->f(D)V

    iget-object p1, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object p1, p1, v0

    invoke-virtual {p1}, La/e/a/a/a/a$a;->a()D

    move-result-wide p1

    aput-wide p1, p3, v1

    iget-object p1, p0, La/e/a/a/a/a;->b:[La/e/a/a/a/a$a;

    aget-object p1, p1, v0

    invoke-virtual {p1}, La/e/a/a/a/a$a;->b()D

    move-result-wide p1

    aput-wide p1, p3, v3

    return-void

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    return-void
.end method
