.class public La/e/a/c/i;
.super La/e/a/c/p;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La/e/a/c/i$a;
    }
.end annotation


# instance fields
.field private Ab:I

.field private Bb:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "La/e/a/c/i$a;",
            ">;"
        }
    .end annotation
.end field

.field private Cb:[La/e/a/c/g;

.field private Db:[La/e/a/c/g;

.field private Eb:[I

.field private Fb:[La/e/a/c/g;

.field private Gb:I

.field private ib:I

.field private jb:I

.field private kb:I

.field private lb:I

.field private mb:I

.field private nb:I

.field private ob:F

.field private pb:F

.field private qb:F

.field private rb:F

.field private sb:F

.field private tb:F

.field private ub:I

.field private vb:I

.field private wb:I

.field private xb:I

.field private yb:I

.field private zb:I


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, La/e/a/c/p;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, La/e/a/c/i;->ib:I

    iput v0, p0, La/e/a/c/i;->jb:I

    iput v0, p0, La/e/a/c/i;->kb:I

    iput v0, p0, La/e/a/c/i;->lb:I

    iput v0, p0, La/e/a/c/i;->mb:I

    iput v0, p0, La/e/a/c/i;->nb:I

    const/high16 v1, 0x3f000000    # 0.5f

    iput v1, p0, La/e/a/c/i;->ob:F

    iput v1, p0, La/e/a/c/i;->pb:F

    iput v1, p0, La/e/a/c/i;->qb:F

    iput v1, p0, La/e/a/c/i;->rb:F

    iput v1, p0, La/e/a/c/i;->sb:F

    iput v1, p0, La/e/a/c/i;->tb:F

    const/4 v1, 0x0

    iput v1, p0, La/e/a/c/i;->ub:I

    iput v1, p0, La/e/a/c/i;->vb:I

    const/4 v2, 0x2

    iput v2, p0, La/e/a/c/i;->wb:I

    iput v2, p0, La/e/a/c/i;->xb:I

    iput v1, p0, La/e/a/c/i;->yb:I

    iput v0, p0, La/e/a/c/i;->zb:I

    iput v1, p0, La/e/a/c/i;->Ab:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-object v0, p0, La/e/a/c/i;->Cb:[La/e/a/c/g;

    iput-object v0, p0, La/e/a/c/i;->Db:[La/e/a/c/g;

    iput-object v0, p0, La/e/a/c/i;->Eb:[I

    iput v1, p0, La/e/a/c/i;->Gb:I

    return-void
.end method

.method private final a(La/e/a/c/g;I)I
    .locals 9

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    invoke-virtual {p1}, La/e/a/c/g;->z()La/e/a/c/g$a;

    move-result-object v1

    sget-object v2, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v1, v2, :cond_5

    iget v1, p1, La/e/a/c/g;->y:I

    if-nez v1, :cond_1

    return v0

    :cond_1
    const/4 v0, 0x2

    const/4 v2, 0x1

    if-ne v1, v0, :cond_3

    iget v0, p1, La/e/a/c/g;->F:F

    int-to-float p2, p2

    mul-float/2addr v0, p2

    float-to-int p2, v0

    invoke-virtual {p1}, La/e/a/c/g;->k()I

    move-result v0

    if-eq p2, v0, :cond_2

    invoke-virtual {p1, v2}, La/e/a/c/g;->d(Z)V

    invoke-virtual {p1}, La/e/a/c/g;->n()La/e/a/c/g$a;

    move-result-object v5

    invoke-virtual {p1}, La/e/a/c/g;->C()I

    move-result v6

    sget-object v7, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    move-object v3, p0

    move-object v4, p1

    move v8, p2

    invoke-virtual/range {v3 .. v8}, La/e/a/c/p;->a(La/e/a/c/g;La/e/a/c/g$a;ILa/e/a/c/g$a;I)V

    :cond_2
    return p2

    :cond_3
    if-ne v1, v2, :cond_4

    invoke-virtual {p1}, La/e/a/c/g;->k()I

    move-result p1

    return p1

    :cond_4
    const/4 p2, 0x3

    if-ne v1, p2, :cond_5

    invoke-virtual {p1}, La/e/a/c/g;->C()I

    move-result p2

    int-to-float p2, p2

    iget p1, p1, La/e/a/c/g;->ga:F

    mul-float/2addr p2, p1

    const/high16 p1, 0x3f000000    # 0.5f

    add-float/2addr p2, p1

    float-to-int p1, p2

    return p1

    :cond_5
    invoke-virtual {p1}, La/e/a/c/g;->k()I

    move-result p1

    return p1
.end method

.method static synthetic a(La/e/a/c/i;)I
    .locals 0

    iget p0, p0, La/e/a/c/i;->ub:I

    return p0
.end method

.method static synthetic a(La/e/a/c/i;La/e/a/c/g;I)I
    .locals 0

    invoke-direct {p0, p1, p2}, La/e/a/c/i;->b(La/e/a/c/g;I)I

    move-result p0

    return p0
.end method

.method private a([La/e/a/c/g;III[I)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    const/4 v5, 0x0

    if-nez v3, :cond_5

    iget v6, v0, La/e/a/c/i;->zb:I

    if-gtz v6, :cond_3

    move v6, v5

    move v7, v6

    move v8, v7

    :goto_0
    if-ge v6, v2, :cond_4

    if-lez v6, :cond_0

    iget v9, v0, La/e/a/c/i;->ub:I

    add-int/2addr v7, v9

    :cond_0
    aget-object v9, v1, v6

    if-nez v9, :cond_1

    goto :goto_1

    :cond_1
    invoke-direct {v0, v9, v4}, La/e/a/c/i;->b(La/e/a/c/g;I)I

    move-result v9

    add-int/2addr v7, v9

    if-le v7, v4, :cond_2

    goto :goto_2

    :cond_2
    add-int/lit8 v8, v8, 0x1

    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_3
    move v8, v6

    :cond_4
    :goto_2
    move v6, v5

    goto :goto_6

    :cond_5
    iget v6, v0, La/e/a/c/i;->zb:I

    if-gtz v6, :cond_a

    move v6, v5

    move v7, v6

    move v8, v7

    :goto_3
    if-ge v6, v2, :cond_9

    if-lez v6, :cond_6

    iget v9, v0, La/e/a/c/i;->vb:I

    add-int/2addr v7, v9

    :cond_6
    aget-object v9, v1, v6

    if-nez v9, :cond_7

    goto :goto_4

    :cond_7
    invoke-direct {v0, v9, v4}, La/e/a/c/i;->a(La/e/a/c/g;I)I

    move-result v9

    add-int/2addr v7, v9

    if-le v7, v4, :cond_8

    goto :goto_5

    :cond_8
    add-int/lit8 v8, v8, 0x1

    :goto_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_9
    :goto_5
    move v6, v8

    :cond_a
    move v8, v5

    :goto_6
    iget-object v7, v0, La/e/a/c/i;->Eb:[I

    if-nez v7, :cond_b

    const/4 v7, 0x2

    new-array v7, v7, [I

    iput-object v7, v0, La/e/a/c/i;->Eb:[I

    :cond_b
    const/4 v7, 0x1

    if-nez v6, :cond_c

    if-eq v3, v7, :cond_d

    :cond_c
    if-nez v8, :cond_f

    if-nez v3, :cond_f

    :cond_d
    move v9, v6

    :cond_e
    move v6, v7

    goto :goto_7

    :cond_f
    move v9, v6

    move v6, v5

    :goto_7
    if-nez v6, :cond_25

    if-nez v3, :cond_10

    int-to-float v9, v2

    int-to-float v10, v8

    div-float/2addr v9, v10

    float-to-double v9, v9

    invoke-static {v9, v10}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v9

    double-to-int v9, v9

    goto :goto_8

    :cond_10
    int-to-float v8, v2

    int-to-float v10, v9

    div-float/2addr v8, v10

    float-to-double v10, v8

    invoke-static {v10, v11}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v10

    double-to-int v8, v10

    :goto_8
    iget-object v10, v0, La/e/a/c/i;->Db:[La/e/a/c/g;

    const/4 v11, 0x0

    if-eqz v10, :cond_12

    array-length v12, v10

    if-ge v12, v8, :cond_11

    goto :goto_9

    :cond_11
    invoke-static {v10, v11}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_a

    :cond_12
    :goto_9
    new-array v10, v8, [La/e/a/c/g;

    iput-object v10, v0, La/e/a/c/i;->Db:[La/e/a/c/g;

    :goto_a
    iget-object v10, v0, La/e/a/c/i;->Cb:[La/e/a/c/g;

    if-eqz v10, :cond_14

    array-length v12, v10

    if-ge v12, v9, :cond_13

    goto :goto_b

    :cond_13
    invoke-static {v10, v11}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_c

    :cond_14
    :goto_b
    new-array v10, v9, [La/e/a/c/g;

    iput-object v10, v0, La/e/a/c/i;->Cb:[La/e/a/c/g;

    :goto_c
    move v10, v5

    :goto_d
    if-ge v10, v8, :cond_1d

    move v11, v5

    :goto_e
    if-ge v11, v9, :cond_1c

    mul-int v12, v11, v8

    add-int/2addr v12, v10

    if-ne v3, v7, :cond_15

    mul-int v12, v10, v9

    add-int/2addr v12, v11

    :cond_15
    array-length v13, v1

    if-lt v12, v13, :cond_16

    goto :goto_f

    :cond_16
    aget-object v12, v1, v12

    if-nez v12, :cond_17

    goto :goto_f

    :cond_17
    invoke-direct {v0, v12, v4}, La/e/a/c/i;->b(La/e/a/c/g;I)I

    move-result v13

    iget-object v14, v0, La/e/a/c/i;->Db:[La/e/a/c/g;

    aget-object v15, v14, v10

    if-eqz v15, :cond_18

    aget-object v14, v14, v10

    invoke-virtual {v14}, La/e/a/c/g;->C()I

    move-result v14

    if-ge v14, v13, :cond_19

    :cond_18
    iget-object v13, v0, La/e/a/c/i;->Db:[La/e/a/c/g;

    aput-object v12, v13, v10

    :cond_19
    invoke-direct {v0, v12, v4}, La/e/a/c/i;->a(La/e/a/c/g;I)I

    move-result v13

    iget-object v14, v0, La/e/a/c/i;->Cb:[La/e/a/c/g;

    aget-object v15, v14, v11

    if-eqz v15, :cond_1a

    aget-object v14, v14, v11

    invoke-virtual {v14}, La/e/a/c/g;->k()I

    move-result v14

    if-ge v14, v13, :cond_1b

    :cond_1a
    iget-object v13, v0, La/e/a/c/i;->Cb:[La/e/a/c/g;

    aput-object v12, v13, v11

    :cond_1b
    :goto_f
    add-int/lit8 v11, v11, 0x1

    goto :goto_e

    :cond_1c
    add-int/lit8 v10, v10, 0x1

    goto :goto_d

    :cond_1d
    move v10, v5

    move v11, v10

    :goto_10
    if-ge v10, v8, :cond_20

    iget-object v12, v0, La/e/a/c/i;->Db:[La/e/a/c/g;

    aget-object v12, v12, v10

    if-eqz v12, :cond_1f

    if-lez v10, :cond_1e

    iget v13, v0, La/e/a/c/i;->ub:I

    add-int/2addr v11, v13

    :cond_1e
    invoke-direct {v0, v12, v4}, La/e/a/c/i;->b(La/e/a/c/g;I)I

    move-result v12

    add-int/2addr v11, v12

    :cond_1f
    add-int/lit8 v10, v10, 0x1

    goto :goto_10

    :cond_20
    move v10, v5

    move v12, v10

    :goto_11
    if-ge v10, v9, :cond_23

    iget-object v13, v0, La/e/a/c/i;->Cb:[La/e/a/c/g;

    aget-object v13, v13, v10

    if-eqz v13, :cond_22

    if-lez v10, :cond_21

    iget v14, v0, La/e/a/c/i;->vb:I

    add-int/2addr v12, v14

    :cond_21
    invoke-direct {v0, v13, v4}, La/e/a/c/i;->a(La/e/a/c/g;I)I

    move-result v13

    add-int/2addr v12, v13

    :cond_22
    add-int/lit8 v10, v10, 0x1

    goto :goto_11

    :cond_23
    aput v11, p5, v5

    aput v12, p5, v7

    if-nez v3, :cond_24

    if-le v11, v4, :cond_e

    if-le v8, v7, :cond_e

    add-int/lit8 v8, v8, -0x1

    goto/16 :goto_7

    :cond_24
    if-le v12, v4, :cond_e

    if-le v9, v7, :cond_e

    add-int/lit8 v9, v9, -0x1

    goto/16 :goto_7

    :cond_25
    iget-object v1, v0, La/e/a/c/i;->Eb:[I

    aput v8, v1, v5

    aput v9, v1, v7

    return-void
.end method

.method private final b(La/e/a/c/g;I)I
    .locals 9

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    invoke-virtual {p1}, La/e/a/c/g;->n()La/e/a/c/g$a;

    move-result-object v1

    sget-object v2, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v1, v2, :cond_5

    iget v1, p1, La/e/a/c/g;->x:I

    if-nez v1, :cond_1

    return v0

    :cond_1
    const/4 v0, 0x2

    const/4 v2, 0x1

    if-ne v1, v0, :cond_3

    iget v0, p1, La/e/a/c/g;->C:F

    int-to-float p2, p2

    mul-float/2addr v0, p2

    float-to-int p2, v0

    invoke-virtual {p1}, La/e/a/c/g;->C()I

    move-result v0

    if-eq p2, v0, :cond_2

    invoke-virtual {p1, v2}, La/e/a/c/g;->d(Z)V

    sget-object v5, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    invoke-virtual {p1}, La/e/a/c/g;->z()La/e/a/c/g$a;

    move-result-object v7

    invoke-virtual {p1}, La/e/a/c/g;->k()I

    move-result v8

    move-object v3, p0

    move-object v4, p1

    move v6, p2

    invoke-virtual/range {v3 .. v8}, La/e/a/c/p;->a(La/e/a/c/g;La/e/a/c/g$a;ILa/e/a/c/g$a;I)V

    :cond_2
    return p2

    :cond_3
    if-ne v1, v2, :cond_4

    invoke-virtual {p1}, La/e/a/c/g;->C()I

    move-result p1

    return p1

    :cond_4
    const/4 p2, 0x3

    if-ne v1, p2, :cond_5

    invoke-virtual {p1}, La/e/a/c/g;->k()I

    move-result p2

    int-to-float p2, p2

    iget p1, p1, La/e/a/c/g;->ga:F

    mul-float/2addr p2, p1

    const/high16 p1, 0x3f000000    # 0.5f

    add-float/2addr p2, p1

    float-to-int p1, p2

    return p1

    :cond_5
    invoke-virtual {p1}, La/e/a/c/g;->C()I

    move-result p1

    return p1
.end method

.method static synthetic b(La/e/a/c/i;)I
    .locals 0

    iget p0, p0, La/e/a/c/i;->vb:I

    return p0
.end method

.method static synthetic b(La/e/a/c/i;La/e/a/c/g;I)I
    .locals 0

    invoke-direct {p0, p1, p2}, La/e/a/c/i;->a(La/e/a/c/g;I)I

    move-result p0

    return p0
.end method

.method private b([La/e/a/c/g;III[I)V
    .locals 27

    move-object/from16 v8, p0

    move/from16 v9, p2

    move/from16 v15, p4

    if-nez v9, :cond_0

    return-void

    :cond_0
    iget-object v0, v8, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    new-instance v10, La/e/a/c/i$a;

    iget-object v3, v8, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v4, v8, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v5, v8, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v6, v8, La/e/a/c/g;->U:La/e/a/c/e;

    move-object v0, v10

    move-object/from16 v1, p0

    move/from16 v2, p3

    move/from16 v7, p4

    invoke-direct/range {v0 .. v7}, La/e/a/c/i$a;-><init>(La/e/a/c/i;ILa/e/a/c/e;La/e/a/c/e;La/e/a/c/e;La/e/a/c/e;I)V

    iget-object v0, v8, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v20, 0x1

    const/16 v21, 0x0

    if-nez p3, :cond_7

    move-object v2, v10

    move/from16 v0, v21

    move v1, v0

    move v10, v1

    :goto_0
    if-ge v10, v9, :cond_e

    aget-object v11, p1, v10

    invoke-direct {v8, v11, v15}, La/e/a/c/i;->b(La/e/a/c/g;I)I

    move-result v12

    invoke-virtual {v11}, La/e/a/c/g;->n()La/e/a/c/g$a;

    move-result-object v3

    sget-object v4, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v3, v4, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    move v13, v0

    if-eq v1, v15, :cond_2

    iget v0, v8, La/e/a/c/i;->ub:I

    add-int/2addr v0, v1

    add-int/2addr v0, v12

    if-le v0, v15, :cond_3

    :cond_2
    invoke-static {v2}, La/e/a/c/i$a;->a(La/e/a/c/i$a;)La/e/a/c/g;

    move-result-object v0

    if-eqz v0, :cond_3

    move/from16 v0, v20

    goto :goto_1

    :cond_3
    move/from16 v0, v21

    :goto_1
    if-nez v0, :cond_4

    if-lez v10, :cond_4

    iget v3, v8, La/e/a/c/i;->zb:I

    if-lez v3, :cond_4

    rem-int v3, v10, v3

    if-nez v3, :cond_4

    move/from16 v0, v20

    :cond_4
    if-eqz v0, :cond_5

    new-instance v14, La/e/a/c/i$a;

    iget-object v3, v8, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v4, v8, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v5, v8, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v6, v8, La/e/a/c/g;->U:La/e/a/c/e;

    move-object v0, v14

    move-object/from16 v1, p0

    move/from16 v2, p3

    move/from16 v7, p4

    invoke-direct/range {v0 .. v7}, La/e/a/c/i$a;-><init>(La/e/a/c/i;ILa/e/a/c/e;La/e/a/c/e;La/e/a/c/e;La/e/a/c/e;I)V

    invoke-virtual {v14, v10}, La/e/a/c/i$a;->b(I)V

    iget-object v0, v8, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v12

    move-object v2, v14

    goto :goto_2

    :cond_5
    if-lez v10, :cond_6

    iget v0, v8, La/e/a/c/i;->ub:I

    add-int/2addr v0, v12

    add-int/2addr v1, v0

    goto :goto_2

    :cond_6
    move v1, v12

    :goto_2
    invoke-virtual {v2, v11}, La/e/a/c/i$a;->a(La/e/a/c/g;)V

    add-int/lit8 v10, v10, 0x1

    move v0, v13

    goto :goto_0

    :cond_7
    move-object v2, v10

    move/from16 v0, v21

    move v1, v0

    move v10, v1

    :goto_3
    if-ge v10, v9, :cond_e

    aget-object v11, p1, v10

    invoke-direct {v8, v11, v15}, La/e/a/c/i;->a(La/e/a/c/g;I)I

    move-result v12

    invoke-virtual {v11}, La/e/a/c/g;->z()La/e/a/c/g$a;

    move-result-object v3

    sget-object v4, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v3, v4, :cond_8

    add-int/lit8 v0, v0, 0x1

    :cond_8
    move v13, v0

    if-eq v1, v15, :cond_9

    iget v0, v8, La/e/a/c/i;->vb:I

    add-int/2addr v0, v1

    add-int/2addr v0, v12

    if-le v0, v15, :cond_a

    :cond_9
    invoke-static {v2}, La/e/a/c/i$a;->a(La/e/a/c/i$a;)La/e/a/c/g;

    move-result-object v0

    if-eqz v0, :cond_a

    move/from16 v0, v20

    goto :goto_4

    :cond_a
    move/from16 v0, v21

    :goto_4
    if-nez v0, :cond_b

    if-lez v10, :cond_b

    iget v3, v8, La/e/a/c/i;->zb:I

    if-lez v3, :cond_b

    rem-int v3, v10, v3

    if-nez v3, :cond_b

    move/from16 v0, v20

    :cond_b
    if-eqz v0, :cond_c

    new-instance v14, La/e/a/c/i$a;

    iget-object v3, v8, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v4, v8, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v5, v8, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v6, v8, La/e/a/c/g;->U:La/e/a/c/e;

    move-object v0, v14

    move-object/from16 v1, p0

    move/from16 v2, p3

    move/from16 v7, p4

    invoke-direct/range {v0 .. v7}, La/e/a/c/i$a;-><init>(La/e/a/c/i;ILa/e/a/c/e;La/e/a/c/e;La/e/a/c/e;La/e/a/c/e;I)V

    invoke-virtual {v14, v10}, La/e/a/c/i$a;->b(I)V

    iget-object v0, v8, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v12

    move-object v2, v14

    goto :goto_5

    :cond_c
    if-lez v10, :cond_d

    iget v0, v8, La/e/a/c/i;->vb:I

    add-int/2addr v0, v12

    add-int/2addr v1, v0

    goto :goto_5

    :cond_d
    move v1, v12

    :goto_5
    invoke-virtual {v2, v11}, La/e/a/c/i$a;->a(La/e/a/c/g;)V

    add-int/lit8 v10, v10, 0x1

    move v0, v13

    goto :goto_3

    :cond_e
    iget-object v1, v8, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v2, v8, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v3, v8, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v4, v8, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v5, v8, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual/range {p0 .. p0}, La/e/a/c/p;->ca()I

    move-result v6

    invoke-virtual/range {p0 .. p0}, La/e/a/c/p;->ea()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, La/e/a/c/p;->da()I

    move-result v9

    invoke-virtual/range {p0 .. p0}, La/e/a/c/p;->ba()I

    move-result v10

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->n()La/e/a/c/g$a;

    move-result-object v11

    sget-object v12, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-eq v11, v12, :cond_10

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->z()La/e/a/c/g$a;

    move-result-object v11

    sget-object v12, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v11, v12, :cond_f

    goto :goto_6

    :cond_f
    move/from16 v11, v21

    goto :goto_7

    :cond_10
    :goto_6
    move/from16 v11, v20

    :goto_7
    if-lez v0, :cond_12

    if-eqz v11, :cond_12

    move/from16 v0, v21

    :goto_8
    if-ge v0, v1, :cond_12

    iget-object v11, v8, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, La/e/a/c/i$a;

    if-nez p3, :cond_11

    invoke-virtual {v11}, La/e/a/c/i$a;->c()I

    move-result v12

    sub-int v12, v15, v12

    invoke-virtual {v11, v12}, La/e/a/c/i$a;->a(I)V

    goto :goto_9

    :cond_11
    invoke-virtual {v11}, La/e/a/c/i$a;->b()I

    move-result v12

    sub-int v12, v15, v12

    invoke-virtual {v11, v12}, La/e/a/c/i$a;->a(I)V

    :goto_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_12
    move-object/from16 v24, v4

    move/from16 v22, v7

    move/from16 v25, v9

    move/from16 v23, v10

    move/from16 v0, v21

    move-object v4, v2

    move v7, v6

    move v2, v0

    move-object v6, v5

    move-object v5, v3

    move v3, v2

    :goto_a
    if-ge v0, v1, :cond_18

    iget-object v9, v8, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    move-object/from16 v26, v9

    check-cast v26, La/e/a/c/i$a;

    if-nez p3, :cond_15

    add-int/lit8 v6, v1, -0x1

    if-ge v0, v6, :cond_13

    iget-object v6, v8, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    add-int/lit8 v9, v0, 0x1

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, La/e/a/c/i$a;

    invoke-static {v6}, La/e/a/c/i$a;->a(La/e/a/c/i$a;)La/e/a/c/g;

    move-result-object v6

    iget-object v6, v6, La/e/a/c/g;->S:La/e/a/c/e;

    move/from16 v23, v21

    goto :goto_b

    :cond_13
    iget-object v6, v8, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual/range {p0 .. p0}, La/e/a/c/p;->ba()I

    move-result v9

    move/from16 v23, v9

    :goto_b
    invoke-static/range {v26 .. v26}, La/e/a/c/i$a;->a(La/e/a/c/i$a;)La/e/a/c/g;

    move-result-object v9

    iget-object v14, v9, La/e/a/c/g;->U:La/e/a/c/e;

    move-object/from16 v9, v26

    move/from16 v10, p3

    move-object v11, v4

    move-object v12, v5

    move-object/from16 v13, v24

    move-object v5, v14

    move-object v14, v6

    move v15, v7

    move/from16 v16, v22

    move/from16 v17, v25

    move/from16 v18, v23

    move/from16 v19, p4

    invoke-virtual/range {v9 .. v19}, La/e/a/c/i$a;->a(ILa/e/a/c/e;La/e/a/c/e;La/e/a/c/e;La/e/a/c/e;IIIII)V

    invoke-virtual/range {v26 .. v26}, La/e/a/c/i$a;->c()I

    move-result v9

    invoke-static {v2, v9}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-virtual/range {v26 .. v26}, La/e/a/c/i$a;->b()I

    move-result v9

    add-int/2addr v3, v9

    if-lez v0, :cond_14

    iget v9, v8, La/e/a/c/i;->vb:I

    add-int/2addr v3, v9

    :cond_14
    move/from16 v22, v21

    goto :goto_d

    :cond_15
    add-int/lit8 v9, v1, -0x1

    if-ge v0, v9, :cond_16

    iget-object v9, v8, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    add-int/lit8 v10, v0, 0x1

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, La/e/a/c/i$a;

    invoke-static {v9}, La/e/a/c/i$a;->a(La/e/a/c/i$a;)La/e/a/c/g;

    move-result-object v9

    iget-object v9, v9, La/e/a/c/g;->R:La/e/a/c/e;

    move-object/from16 v24, v9

    move/from16 v25, v21

    goto :goto_c

    :cond_16
    iget-object v9, v8, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual/range {p0 .. p0}, La/e/a/c/p;->da()I

    move-result v10

    move-object/from16 v24, v9

    move/from16 v25, v10

    :goto_c
    invoke-static/range {v26 .. v26}, La/e/a/c/i$a;->a(La/e/a/c/i$a;)La/e/a/c/g;

    move-result-object v9

    iget-object v15, v9, La/e/a/c/g;->T:La/e/a/c/e;

    move-object/from16 v9, v26

    move/from16 v10, p3

    move-object v11, v4

    move-object v12, v5

    move-object/from16 v13, v24

    move-object v14, v6

    move-object v4, v15

    move v15, v7

    move/from16 v16, v22

    move/from16 v17, v25

    move/from16 v18, v23

    move/from16 v19, p4

    invoke-virtual/range {v9 .. v19}, La/e/a/c/i$a;->a(ILa/e/a/c/e;La/e/a/c/e;La/e/a/c/e;La/e/a/c/e;IIIII)V

    invoke-virtual/range {v26 .. v26}, La/e/a/c/i$a;->c()I

    move-result v7

    add-int/2addr v2, v7

    invoke-virtual/range {v26 .. v26}, La/e/a/c/i$a;->b()I

    move-result v7

    invoke-static {v3, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    if-lez v0, :cond_17

    iget v7, v8, La/e/a/c/i;->ub:I

    add-int/2addr v2, v7

    :cond_17
    move/from16 v7, v21

    :goto_d
    add-int/lit8 v0, v0, 0x1

    move/from16 v15, p4

    goto/16 :goto_a

    :cond_18
    aput v2, p5, v21

    aput v3, p5, v20

    return-void
.end method

.method static synthetic c(La/e/a/c/i;)I
    .locals 0

    iget p0, p0, La/e/a/c/i;->kb:I

    return p0
.end method

.method private c([La/e/a/c/g;III[I)V
    .locals 27

    move-object/from16 v8, p0

    move/from16 v9, p2

    move/from16 v15, p4

    if-nez v9, :cond_0

    return-void

    :cond_0
    iget-object v0, v8, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    new-instance v10, La/e/a/c/i$a;

    iget-object v3, v8, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v4, v8, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v5, v8, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v6, v8, La/e/a/c/g;->U:La/e/a/c/e;

    move-object v0, v10

    move-object/from16 v1, p0

    move/from16 v2, p3

    move/from16 v7, p4

    invoke-direct/range {v0 .. v7}, La/e/a/c/i$a;-><init>(La/e/a/c/i;ILa/e/a/c/e;La/e/a/c/e;La/e/a/c/e;La/e/a/c/e;I)V

    iget-object v0, v8, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v20, 0x1

    const/16 v21, 0x0

    if-nez p3, :cond_7

    move-object v3, v10

    move/from16 v0, v21

    move v1, v0

    move v2, v1

    move v10, v2

    :goto_0
    if-ge v10, v9, :cond_e

    add-int/lit8 v11, v0, 0x1

    aget-object v12, p1, v10

    invoke-direct {v8, v12, v15}, La/e/a/c/i;->b(La/e/a/c/g;I)I

    move-result v13

    invoke-virtual {v12}, La/e/a/c/g;->n()La/e/a/c/g$a;

    move-result-object v0

    sget-object v4, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v0, v4, :cond_1

    add-int/lit8 v1, v1, 0x1

    :cond_1
    move v14, v1

    if-eq v2, v15, :cond_2

    iget v0, v8, La/e/a/c/i;->ub:I

    add-int/2addr v0, v2

    add-int/2addr v0, v13

    if-le v0, v15, :cond_3

    :cond_2
    invoke-static {v3}, La/e/a/c/i$a;->a(La/e/a/c/i$a;)La/e/a/c/g;

    move-result-object v0

    if-eqz v0, :cond_3

    move/from16 v0, v20

    goto :goto_1

    :cond_3
    move/from16 v0, v21

    :goto_1
    if-nez v0, :cond_4

    if-lez v10, :cond_4

    iget v1, v8, La/e/a/c/i;->zb:I

    if-lez v1, :cond_4

    if-le v11, v1, :cond_4

    move/from16 v0, v20

    :cond_4
    if-eqz v0, :cond_5

    new-instance v7, La/e/a/c/i$a;

    iget-object v3, v8, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v4, v8, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v5, v8, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v6, v8, La/e/a/c/g;->U:La/e/a/c/e;

    move-object v0, v7

    move-object/from16 v1, p0

    move/from16 v2, p3

    move/from16 v16, v11

    move-object v11, v7

    move/from16 v7, p4

    invoke-direct/range {v0 .. v7}, La/e/a/c/i$a;-><init>(La/e/a/c/i;ILa/e/a/c/e;La/e/a/c/e;La/e/a/c/e;La/e/a/c/e;I)V

    invoke-virtual {v11, v10}, La/e/a/c/i$a;->b(I)V

    iget-object v0, v8, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v3, v11

    move v2, v13

    move/from16 v0, v16

    goto :goto_3

    :cond_5
    if-lez v10, :cond_6

    iget v0, v8, La/e/a/c/i;->ub:I

    add-int/2addr v0, v13

    add-int/2addr v2, v0

    goto :goto_2

    :cond_6
    move v2, v13

    :goto_2
    move/from16 v0, v21

    :goto_3
    invoke-virtual {v3, v12}, La/e/a/c/i$a;->a(La/e/a/c/g;)V

    add-int/lit8 v10, v10, 0x1

    move v1, v14

    goto :goto_0

    :cond_7
    move-object v2, v10

    move/from16 v0, v21

    move v1, v0

    move v10, v1

    :goto_4
    if-ge v10, v9, :cond_e

    aget-object v11, p1, v10

    invoke-direct {v8, v11, v15}, La/e/a/c/i;->a(La/e/a/c/g;I)I

    move-result v12

    invoke-virtual {v11}, La/e/a/c/g;->z()La/e/a/c/g$a;

    move-result-object v3

    sget-object v4, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v3, v4, :cond_8

    add-int/lit8 v1, v1, 0x1

    :cond_8
    move v13, v1

    if-eq v0, v15, :cond_9

    iget v1, v8, La/e/a/c/i;->vb:I

    add-int/2addr v1, v0

    add-int/2addr v1, v12

    if-le v1, v15, :cond_a

    :cond_9
    invoke-static {v2}, La/e/a/c/i$a;->a(La/e/a/c/i$a;)La/e/a/c/g;

    move-result-object v1

    if-eqz v1, :cond_a

    move/from16 v1, v20

    goto :goto_5

    :cond_a
    move/from16 v1, v21

    :goto_5
    if-nez v1, :cond_b

    if-lez v10, :cond_b

    iget v3, v8, La/e/a/c/i;->zb:I

    if-lez v3, :cond_b

    if-gez v3, :cond_b

    move/from16 v1, v20

    :cond_b
    if-eqz v1, :cond_c

    new-instance v14, La/e/a/c/i$a;

    iget-object v3, v8, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v4, v8, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v5, v8, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v6, v8, La/e/a/c/g;->U:La/e/a/c/e;

    move-object v0, v14

    move-object/from16 v1, p0

    move/from16 v2, p3

    move/from16 v7, p4

    invoke-direct/range {v0 .. v7}, La/e/a/c/i$a;-><init>(La/e/a/c/i;ILa/e/a/c/e;La/e/a/c/e;La/e/a/c/e;La/e/a/c/e;I)V

    invoke-virtual {v14, v10}, La/e/a/c/i$a;->b(I)V

    iget-object v0, v8, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v12

    move-object v2, v14

    goto :goto_6

    :cond_c
    if-lez v10, :cond_d

    iget v1, v8, La/e/a/c/i;->vb:I

    add-int/2addr v1, v12

    add-int/2addr v0, v1

    goto :goto_6

    :cond_d
    move v0, v12

    :goto_6
    invoke-virtual {v2, v11}, La/e/a/c/i$a;->a(La/e/a/c/g;)V

    add-int/lit8 v10, v10, 0x1

    move v1, v13

    goto :goto_4

    :cond_e
    iget-object v0, v8, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v2, v8, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v3, v8, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v4, v8, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v5, v8, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual/range {p0 .. p0}, La/e/a/c/p;->ca()I

    move-result v6

    invoke-virtual/range {p0 .. p0}, La/e/a/c/p;->ea()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, La/e/a/c/p;->da()I

    move-result v9

    invoke-virtual/range {p0 .. p0}, La/e/a/c/p;->ba()I

    move-result v10

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->n()La/e/a/c/g$a;

    move-result-object v11

    sget-object v12, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-eq v11, v12, :cond_10

    invoke-virtual/range {p0 .. p0}, La/e/a/c/g;->z()La/e/a/c/g$a;

    move-result-object v11

    sget-object v12, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v11, v12, :cond_f

    goto :goto_7

    :cond_f
    move/from16 v11, v21

    goto :goto_8

    :cond_10
    :goto_7
    move/from16 v11, v20

    :goto_8
    if-lez v1, :cond_12

    if-eqz v11, :cond_12

    move/from16 v1, v21

    :goto_9
    if-ge v1, v0, :cond_12

    iget-object v11, v8, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, La/e/a/c/i$a;

    if-nez p3, :cond_11

    invoke-virtual {v11}, La/e/a/c/i$a;->c()I

    move-result v12

    sub-int v12, v15, v12

    invoke-virtual {v11, v12}, La/e/a/c/i$a;->a(I)V

    goto :goto_a

    :cond_11
    invoke-virtual {v11}, La/e/a/c/i$a;->b()I

    move-result v12

    sub-int v12, v15, v12

    invoke-virtual {v11, v12}, La/e/a/c/i$a;->a(I)V

    :goto_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    :cond_12
    move-object/from16 v24, v4

    move/from16 v22, v7

    move/from16 v25, v9

    move/from16 v23, v10

    move/from16 v1, v21

    move-object v4, v2

    move v7, v6

    move v2, v1

    move-object v6, v5

    move-object v5, v3

    move v3, v2

    :goto_b
    if-ge v1, v0, :cond_18

    iget-object v9, v8, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    move-object/from16 v26, v9

    check-cast v26, La/e/a/c/i$a;

    if-nez p3, :cond_15

    add-int/lit8 v6, v0, -0x1

    if-ge v1, v6, :cond_13

    iget-object v6, v8, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    add-int/lit8 v9, v1, 0x1

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, La/e/a/c/i$a;

    invoke-static {v6}, La/e/a/c/i$a;->a(La/e/a/c/i$a;)La/e/a/c/g;

    move-result-object v6

    iget-object v6, v6, La/e/a/c/g;->S:La/e/a/c/e;

    move/from16 v23, v21

    goto :goto_c

    :cond_13
    iget-object v6, v8, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual/range {p0 .. p0}, La/e/a/c/p;->ba()I

    move-result v9

    move/from16 v23, v9

    :goto_c
    invoke-static/range {v26 .. v26}, La/e/a/c/i$a;->a(La/e/a/c/i$a;)La/e/a/c/g;

    move-result-object v9

    iget-object v14, v9, La/e/a/c/g;->U:La/e/a/c/e;

    move-object/from16 v9, v26

    move/from16 v10, p3

    move-object v11, v4

    move-object v12, v5

    move-object/from16 v13, v24

    move-object v5, v14

    move-object v14, v6

    move v15, v7

    move/from16 v16, v22

    move/from16 v17, v25

    move/from16 v18, v23

    move/from16 v19, p4

    invoke-virtual/range {v9 .. v19}, La/e/a/c/i$a;->a(ILa/e/a/c/e;La/e/a/c/e;La/e/a/c/e;La/e/a/c/e;IIIII)V

    invoke-virtual/range {v26 .. v26}, La/e/a/c/i$a;->c()I

    move-result v9

    invoke-static {v2, v9}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-virtual/range {v26 .. v26}, La/e/a/c/i$a;->b()I

    move-result v9

    add-int/2addr v3, v9

    if-lez v1, :cond_14

    iget v9, v8, La/e/a/c/i;->vb:I

    add-int/2addr v3, v9

    :cond_14
    move/from16 v22, v21

    goto :goto_e

    :cond_15
    add-int/lit8 v9, v0, -0x1

    if-ge v1, v9, :cond_16

    iget-object v9, v8, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    add-int/lit8 v10, v1, 0x1

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, La/e/a/c/i$a;

    invoke-static {v9}, La/e/a/c/i$a;->a(La/e/a/c/i$a;)La/e/a/c/g;

    move-result-object v9

    iget-object v9, v9, La/e/a/c/g;->R:La/e/a/c/e;

    move-object/from16 v24, v9

    move/from16 v25, v21

    goto :goto_d

    :cond_16
    iget-object v9, v8, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual/range {p0 .. p0}, La/e/a/c/p;->da()I

    move-result v10

    move-object/from16 v24, v9

    move/from16 v25, v10

    :goto_d
    invoke-static/range {v26 .. v26}, La/e/a/c/i$a;->a(La/e/a/c/i$a;)La/e/a/c/g;

    move-result-object v9

    iget-object v15, v9, La/e/a/c/g;->T:La/e/a/c/e;

    move-object/from16 v9, v26

    move/from16 v10, p3

    move-object v11, v4

    move-object v12, v5

    move-object/from16 v13, v24

    move-object v14, v6

    move-object v4, v15

    move v15, v7

    move/from16 v16, v22

    move/from16 v17, v25

    move/from16 v18, v23

    move/from16 v19, p4

    invoke-virtual/range {v9 .. v19}, La/e/a/c/i$a;->a(ILa/e/a/c/e;La/e/a/c/e;La/e/a/c/e;La/e/a/c/e;IIIII)V

    invoke-virtual/range {v26 .. v26}, La/e/a/c/i$a;->c()I

    move-result v7

    add-int/2addr v2, v7

    invoke-virtual/range {v26 .. v26}, La/e/a/c/i$a;->b()I

    move-result v7

    invoke-static {v3, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    if-lez v1, :cond_17

    iget v7, v8, La/e/a/c/i;->ub:I

    add-int/2addr v2, v7

    :cond_17
    move/from16 v7, v21

    :goto_e
    add-int/lit8 v1, v1, 0x1

    move/from16 v15, p4

    goto/16 :goto_b

    :cond_18
    aput v2, p5, v21

    aput v3, p5, v20

    return-void
.end method

.method static synthetic d(La/e/a/c/i;)F
    .locals 0

    iget p0, p0, La/e/a/c/i;->qb:F

    return p0
.end method

.method private d([La/e/a/c/g;III[I)V
    .locals 22

    move-object/from16 v8, p0

    move/from16 v9, p2

    if-nez v9, :cond_0

    return-void

    :cond_0
    iget-object v0, v8, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v10, 0x0

    if-nez v0, :cond_1

    new-instance v11, La/e/a/c/i$a;

    iget-object v3, v8, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v4, v8, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v5, v8, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v6, v8, La/e/a/c/g;->U:La/e/a/c/e;

    move-object v0, v11

    move-object/from16 v1, p0

    move/from16 v2, p3

    move/from16 v7, p4

    invoke-direct/range {v0 .. v7}, La/e/a/c/i$a;-><init>(La/e/a/c/i;ILa/e/a/c/e;La/e/a/c/e;La/e/a/c/e;La/e/a/c/e;I)V

    iget-object v0, v8, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, v8, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/e/a/c/i$a;

    invoke-virtual {v0}, La/e/a/c/i$a;->a()V

    iget-object v13, v8, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v14, v8, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v15, v8, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v1, v8, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual/range {p0 .. p0}, La/e/a/c/p;->ca()I

    move-result v17

    invoke-virtual/range {p0 .. p0}, La/e/a/c/p;->ea()I

    move-result v18

    invoke-virtual/range {p0 .. p0}, La/e/a/c/p;->da()I

    move-result v19

    invoke-virtual/range {p0 .. p0}, La/e/a/c/p;->ba()I

    move-result v20

    move-object v11, v0

    move/from16 v12, p3

    move-object/from16 v16, v1

    move/from16 v21, p4

    invoke-virtual/range {v11 .. v21}, La/e/a/c/i$a;->a(ILa/e/a/c/e;La/e/a/c/e;La/e/a/c/e;La/e/a/c/e;IIIII)V

    :goto_0
    move v0, v10

    :goto_1
    if-ge v0, v9, :cond_2

    aget-object v1, p1, v0

    invoke-virtual {v11, v1}, La/e/a/c/i$a;->a(La/e/a/c/g;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v11}, La/e/a/c/i$a;->c()I

    move-result v0

    aput v0, p5, v10

    invoke-virtual {v11}, La/e/a/c/i$a;->b()I

    move-result v0

    const/4 v1, 0x1

    aput v0, p5, v1

    return-void
.end method

.method static synthetic e(La/e/a/c/i;)I
    .locals 0

    iget p0, p0, La/e/a/c/i;->mb:I

    return p0
.end method

.method static synthetic f(La/e/a/c/i;)F
    .locals 0

    iget p0, p0, La/e/a/c/i;->sb:F

    return p0
.end method

.method static synthetic g(La/e/a/c/i;)F
    .locals 0

    iget p0, p0, La/e/a/c/i;->pb:F

    return p0
.end method

.method private g(Z)V
    .locals 11

    iget-object v0, p0, La/e/a/c/i;->Eb:[I

    if-eqz v0, :cond_15

    iget-object v0, p0, La/e/a/c/i;->Db:[La/e/a/c/g;

    if-eqz v0, :cond_15

    iget-object v0, p0, La/e/a/c/i;->Cb:[La/e/a/c/g;

    if-nez v0, :cond_0

    goto/16 :goto_9

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget v2, p0, La/e/a/c/i;->Gb:I

    if-ge v1, v2, :cond_1

    iget-object v2, p0, La/e/a/c/i;->Fb:[La/e/a/c/g;

    aget-object v2, v2, v1

    invoke-virtual {v2}, La/e/a/c/g;->W()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, La/e/a/c/i;->Eb:[I

    aget v2, v1, v0

    const/4 v3, 0x1

    aget v1, v1, v3

    const/4 v4, 0x0

    iget v5, p0, La/e/a/c/i;->ob:F

    move-object v6, v4

    move v4, v0

    :goto_1
    const/16 v7, 0x8

    if-ge v4, v2, :cond_8

    if-eqz p1, :cond_2

    sub-int v5, v2, v4

    sub-int/2addr v5, v3

    const/high16 v8, 0x3f800000    # 1.0f

    iget v9, p0, La/e/a/c/i;->ob:F

    sub-float/2addr v8, v9

    goto :goto_2

    :cond_2
    move v8, v5

    move v5, v4

    :goto_2
    iget-object v9, p0, La/e/a/c/i;->Db:[La/e/a/c/g;

    aget-object v5, v9, v5

    if-eqz v5, :cond_7

    invoke-virtual {v5}, La/e/a/c/g;->B()I

    move-result v9

    if-ne v9, v7, :cond_3

    goto :goto_3

    :cond_3
    if-nez v4, :cond_4

    iget-object v7, v5, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v9, p0, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {p0}, La/e/a/c/p;->ca()I

    move-result v10

    invoke-virtual {v5, v7, v9, v10}, La/e/a/c/g;->a(La/e/a/c/e;La/e/a/c/e;I)V

    iget v7, p0, La/e/a/c/i;->ib:I

    invoke-virtual {v5, v7}, La/e/a/c/g;->n(I)V

    invoke-virtual {v5, v8}, La/e/a/c/g;->a(F)V

    :cond_4
    add-int/lit8 v7, v2, -0x1

    if-ne v4, v7, :cond_5

    iget-object v7, v5, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v9, p0, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {p0}, La/e/a/c/p;->da()I

    move-result v10

    invoke-virtual {v5, v7, v9, v10}, La/e/a/c/g;->a(La/e/a/c/e;La/e/a/c/e;I)V

    :cond_5
    if-lez v4, :cond_6

    if-eqz v6, :cond_6

    iget-object v7, v5, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v9, v6, La/e/a/c/g;->T:La/e/a/c/e;

    iget v10, p0, La/e/a/c/i;->ub:I

    invoke-virtual {v5, v7, v9, v10}, La/e/a/c/g;->a(La/e/a/c/e;La/e/a/c/e;I)V

    iget-object v7, v6, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v9, v5, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {v6, v7, v9, v0}, La/e/a/c/g;->a(La/e/a/c/e;La/e/a/c/e;I)V

    :cond_6
    move-object v6, v5

    :cond_7
    :goto_3
    add-int/lit8 v4, v4, 0x1

    move v5, v8

    goto :goto_1

    :cond_8
    move p1, v0

    :goto_4
    if-ge p1, v1, :cond_e

    iget-object v4, p0, La/e/a/c/i;->Cb:[La/e/a/c/g;

    aget-object v4, v4, p1

    if-eqz v4, :cond_d

    invoke-virtual {v4}, La/e/a/c/g;->B()I

    move-result v5

    if-ne v5, v7, :cond_9

    goto :goto_5

    :cond_9
    if-nez p1, :cond_a

    iget-object v5, v4, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v8, p0, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {p0}, La/e/a/c/p;->ea()I

    move-result v9

    invoke-virtual {v4, v5, v8, v9}, La/e/a/c/g;->a(La/e/a/c/e;La/e/a/c/e;I)V

    iget v5, p0, La/e/a/c/i;->jb:I

    invoke-virtual {v4, v5}, La/e/a/c/g;->s(I)V

    iget v5, p0, La/e/a/c/i;->pb:F

    invoke-virtual {v4, v5}, La/e/a/c/g;->c(F)V

    :cond_a
    add-int/lit8 v5, v1, -0x1

    if-ne p1, v5, :cond_b

    iget-object v5, v4, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object v8, p0, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {p0}, La/e/a/c/p;->ba()I

    move-result v9

    invoke-virtual {v4, v5, v8, v9}, La/e/a/c/g;->a(La/e/a/c/e;La/e/a/c/e;I)V

    :cond_b
    if-lez p1, :cond_c

    if-eqz v6, :cond_c

    iget-object v5, v4, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v8, v6, La/e/a/c/g;->U:La/e/a/c/e;

    iget v9, p0, La/e/a/c/i;->vb:I

    invoke-virtual {v4, v5, v8, v9}, La/e/a/c/g;->a(La/e/a/c/e;La/e/a/c/e;I)V

    iget-object v5, v6, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object v8, v4, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {v6, v5, v8, v0}, La/e/a/c/g;->a(La/e/a/c/e;La/e/a/c/e;I)V

    :cond_c
    move-object v6, v4

    :cond_d
    :goto_5
    add-int/lit8 p1, p1, 0x1

    goto :goto_4

    :cond_e
    move p1, v0

    :goto_6
    if-ge p1, v2, :cond_15

    move v4, v0

    :goto_7
    if-ge v4, v1, :cond_14

    mul-int v5, v4, v2

    add-int/2addr v5, p1

    iget v6, p0, La/e/a/c/i;->Ab:I

    if-ne v6, v3, :cond_f

    mul-int v5, p1, v1

    add-int/2addr v5, v4

    :cond_f
    iget-object v6, p0, La/e/a/c/i;->Fb:[La/e/a/c/g;

    array-length v8, v6

    if-lt v5, v8, :cond_10

    goto :goto_8

    :cond_10
    aget-object v5, v6, v5

    if-eqz v5, :cond_13

    invoke-virtual {v5}, La/e/a/c/g;->B()I

    move-result v6

    if-ne v6, v7, :cond_11

    goto :goto_8

    :cond_11
    iget-object v6, p0, La/e/a/c/i;->Db:[La/e/a/c/g;

    aget-object v6, v6, p1

    iget-object v8, p0, La/e/a/c/i;->Cb:[La/e/a/c/g;

    aget-object v8, v8, v4

    if-eq v5, v6, :cond_12

    iget-object v9, v5, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v10, v6, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {v5, v9, v10, v0}, La/e/a/c/g;->a(La/e/a/c/e;La/e/a/c/e;I)V

    iget-object v9, v5, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v6, v6, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {v5, v9, v6, v0}, La/e/a/c/g;->a(La/e/a/c/e;La/e/a/c/e;I)V

    :cond_12
    if-eq v5, v8, :cond_13

    iget-object v6, v5, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v9, v8, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {v5, v6, v9, v0}, La/e/a/c/g;->a(La/e/a/c/e;La/e/a/c/e;I)V

    iget-object v6, v5, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object v8, v8, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {v5, v6, v8, v0}, La/e/a/c/g;->a(La/e/a/c/e;La/e/a/c/e;I)V

    :cond_13
    :goto_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    :cond_14
    add-int/lit8 p1, p1, 0x1

    goto :goto_6

    :cond_15
    :goto_9
    return-void
.end method

.method static synthetic h(La/e/a/c/i;)I
    .locals 0

    iget p0, p0, La/e/a/c/i;->lb:I

    return p0
.end method

.method static synthetic i(La/e/a/c/i;)F
    .locals 0

    iget p0, p0, La/e/a/c/i;->rb:F

    return p0
.end method

.method static synthetic j(La/e/a/c/i;)I
    .locals 0

    iget p0, p0, La/e/a/c/i;->nb:I

    return p0
.end method

.method static synthetic k(La/e/a/c/i;)F
    .locals 0

    iget p0, p0, La/e/a/c/i;->tb:F

    return p0
.end method

.method static synthetic l(La/e/a/c/i;)I
    .locals 0

    iget p0, p0, La/e/a/c/i;->wb:I

    return p0
.end method

.method static synthetic m(La/e/a/c/i;)I
    .locals 0

    iget p0, p0, La/e/a/c/i;->Gb:I

    return p0
.end method

.method static synthetic n(La/e/a/c/i;)[La/e/a/c/g;
    .locals 0

    iget-object p0, p0, La/e/a/c/i;->Fb:[La/e/a/c/g;

    return-object p0
.end method

.method static synthetic o(La/e/a/c/i;)I
    .locals 0

    iget p0, p0, La/e/a/c/i;->jb:I

    return p0
.end method

.method static synthetic p(La/e/a/c/i;)I
    .locals 0

    iget p0, p0, La/e/a/c/i;->xb:I

    return p0
.end method

.method static synthetic q(La/e/a/c/i;)I
    .locals 0

    iget p0, p0, La/e/a/c/i;->ib:I

    return p0
.end method

.method static synthetic r(La/e/a/c/i;)F
    .locals 0

    iget p0, p0, La/e/a/c/i;->ob:F

    return p0
.end method


# virtual methods
.method public G(I)V
    .locals 0

    iput p1, p0, La/e/a/c/i;->kb:I

    return-void
.end method

.method public H(I)V
    .locals 0

    iput p1, p0, La/e/a/c/i;->lb:I

    return-void
.end method

.method public I(I)V
    .locals 0

    iput p1, p0, La/e/a/c/i;->wb:I

    return-void
.end method

.method public J(I)V
    .locals 0

    iput p1, p0, La/e/a/c/i;->ub:I

    return-void
.end method

.method public K(I)V
    .locals 0

    iput p1, p0, La/e/a/c/i;->ib:I

    return-void
.end method

.method public L(I)V
    .locals 0

    iput p1, p0, La/e/a/c/i;->mb:I

    return-void
.end method

.method public M(I)V
    .locals 0

    iput p1, p0, La/e/a/c/i;->nb:I

    return-void
.end method

.method public N(I)V
    .locals 0

    iput p1, p0, La/e/a/c/i;->zb:I

    return-void
.end method

.method public O(I)V
    .locals 0

    iput p1, p0, La/e/a/c/i;->Ab:I

    return-void
.end method

.method public P(I)V
    .locals 0

    iput p1, p0, La/e/a/c/i;->xb:I

    return-void
.end method

.method public Q(I)V
    .locals 0

    iput p1, p0, La/e/a/c/i;->vb:I

    return-void
.end method

.method public R(I)V
    .locals 0

    iput p1, p0, La/e/a/c/i;->jb:I

    return-void
.end method

.method public S(I)V
    .locals 0

    iput p1, p0, La/e/a/c/i;->yb:I

    return-void
.end method

.method public a(La/e/a/c/g;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La/e/a/c/g;",
            "Ljava/util/HashMap<",
            "La/e/a/c/g;",
            "La/e/a/c/g;",
            ">;)V"
        }
    .end annotation

    invoke-super {p0, p1, p2}, La/e/a/c/m;->a(La/e/a/c/g;Ljava/util/HashMap;)V

    check-cast p1, La/e/a/c/i;

    iget p2, p1, La/e/a/c/i;->ib:I

    iput p2, p0, La/e/a/c/i;->ib:I

    iget p2, p1, La/e/a/c/i;->jb:I

    iput p2, p0, La/e/a/c/i;->jb:I

    iget p2, p1, La/e/a/c/i;->kb:I

    iput p2, p0, La/e/a/c/i;->kb:I

    iget p2, p1, La/e/a/c/i;->lb:I

    iput p2, p0, La/e/a/c/i;->lb:I

    iget p2, p1, La/e/a/c/i;->mb:I

    iput p2, p0, La/e/a/c/i;->mb:I

    iget p2, p1, La/e/a/c/i;->nb:I

    iput p2, p0, La/e/a/c/i;->nb:I

    iget p2, p1, La/e/a/c/i;->ob:F

    iput p2, p0, La/e/a/c/i;->ob:F

    iget p2, p1, La/e/a/c/i;->pb:F

    iput p2, p0, La/e/a/c/i;->pb:F

    iget p2, p1, La/e/a/c/i;->qb:F

    iput p2, p0, La/e/a/c/i;->qb:F

    iget p2, p1, La/e/a/c/i;->rb:F

    iput p2, p0, La/e/a/c/i;->rb:F

    iget p2, p1, La/e/a/c/i;->sb:F

    iput p2, p0, La/e/a/c/i;->sb:F

    iget p2, p1, La/e/a/c/i;->tb:F

    iput p2, p0, La/e/a/c/i;->tb:F

    iget p2, p1, La/e/a/c/i;->ub:I

    iput p2, p0, La/e/a/c/i;->ub:I

    iget p2, p1, La/e/a/c/i;->vb:I

    iput p2, p0, La/e/a/c/i;->vb:I

    iget p2, p1, La/e/a/c/i;->wb:I

    iput p2, p0, La/e/a/c/i;->wb:I

    iget p2, p1, La/e/a/c/i;->xb:I

    iput p2, p0, La/e/a/c/i;->xb:I

    iget p2, p1, La/e/a/c/i;->yb:I

    iput p2, p0, La/e/a/c/i;->yb:I

    iget p2, p1, La/e/a/c/i;->zb:I

    iput p2, p0, La/e/a/c/i;->zb:I

    iget p1, p1, La/e/a/c/i;->Ab:I

    iput p1, p0, La/e/a/c/i;->Ab:I

    return-void
.end method

.method public a(La/e/a/d;Z)V
    .locals 5

    invoke-super {p0, p1, p2}, La/e/a/c/g;->a(La/e/a/d;Z)V

    invoke-virtual {p0}, La/e/a/c/g;->v()La/e/a/c/g;

    move-result-object p1

    const/4 p2, 0x0

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, La/e/a/c/g;->v()La/e/a/c/g;

    move-result-object p1

    check-cast p1, La/e/a/c/h;

    invoke-virtual {p1}, La/e/a/c/h;->ia()Z

    move-result p1

    if-eqz p1, :cond_0

    move p1, v0

    goto :goto_0

    :cond_0
    move p1, p2

    :goto_0
    iget v1, p0, La/e/a/c/i;->yb:I

    if-eqz v1, :cond_6

    if-eq v1, v0, :cond_4

    const/4 v2, 0x2

    if-eq v1, v2, :cond_3

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    goto :goto_5

    :cond_1
    iget-object v1, p0, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    move v2, p2

    :goto_1
    if-ge v2, v1, :cond_7

    iget-object v3, p0, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, La/e/a/c/i$a;

    add-int/lit8 v4, v1, -0x1

    if-ne v2, v4, :cond_2

    move v4, v0

    goto :goto_2

    :cond_2
    move v4, p2

    :goto_2
    invoke-virtual {v3, p1, v2, v4}, La/e/a/c/i$a;->a(ZIZ)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    invoke-direct {p0, p1}, La/e/a/c/i;->g(Z)V

    goto :goto_5

    :cond_4
    iget-object v1, p0, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    move v2, p2

    :goto_3
    if-ge v2, v1, :cond_7

    iget-object v3, p0, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, La/e/a/c/i$a;

    add-int/lit8 v4, v1, -0x1

    if-ne v2, v4, :cond_5

    move v4, v0

    goto :goto_4

    :cond_5
    move v4, p2

    :goto_4
    invoke-virtual {v3, p1, v2, v4}, La/e/a/c/i$a;->a(ZIZ)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_6
    iget-object v1, p0, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_7

    iget-object v1, p0, La/e/a/c/i;->Bb:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La/e/a/c/i$a;

    invoke-virtual {v1, p1, p2, v0}, La/e/a/c/i$a;->a(ZIZ)V

    :cond_7
    :goto_5
    invoke-virtual {p0, p2}, La/e/a/c/p;->f(Z)V

    return-void
.end method

.method public b(IIII)V
    .locals 19

    move-object/from16 v6, p0

    move/from16 v7, p1

    move/from16 v8, p2

    move/from16 v9, p3

    move/from16 v10, p4

    iget v0, v6, La/e/a/c/m;->Va:I

    const/4 v11, 0x0

    if-lez v0, :cond_0

    invoke-virtual/range {p0 .. p0}, La/e/a/c/p;->fa()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v6, v11, v11}, La/e/a/c/p;->h(II)V

    invoke-virtual {v6, v11}, La/e/a/c/p;->f(Z)V

    return-void

    :cond_0
    invoke-virtual/range {p0 .. p0}, La/e/a/c/p;->ca()I

    move-result v12

    invoke-virtual/range {p0 .. p0}, La/e/a/c/p;->da()I

    move-result v13

    invoke-virtual/range {p0 .. p0}, La/e/a/c/p;->ea()I

    move-result v14

    invoke-virtual/range {p0 .. p0}, La/e/a/c/p;->ba()I

    move-result v15

    const/4 v0, 0x2

    new-array v5, v0, [I

    sub-int v1, v8, v12

    sub-int/2addr v1, v13

    iget v2, v6, La/e/a/c/i;->Ab:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_1

    sub-int v1, v10, v14

    sub-int/2addr v1, v15

    :cond_1
    move/from16 v16, v1

    iget v1, v6, La/e/a/c/i;->Ab:I

    const/4 v2, -0x1

    if-nez v1, :cond_3

    iget v1, v6, La/e/a/c/i;->ib:I

    if-ne v1, v2, :cond_2

    iput v11, v6, La/e/a/c/i;->ib:I

    :cond_2
    iget v1, v6, La/e/a/c/i;->jb:I

    if-ne v1, v2, :cond_5

    iput v11, v6, La/e/a/c/i;->jb:I

    goto :goto_0

    :cond_3
    iget v1, v6, La/e/a/c/i;->ib:I

    if-ne v1, v2, :cond_4

    iput v11, v6, La/e/a/c/i;->ib:I

    :cond_4
    iget v1, v6, La/e/a/c/i;->jb:I

    if-ne v1, v2, :cond_5

    iput v11, v6, La/e/a/c/i;->jb:I

    :cond_5
    :goto_0
    iget-object v1, v6, La/e/a/c/m;->Ua:[La/e/a/c/g;

    move v2, v11

    move v3, v2

    :goto_1
    iget v11, v6, La/e/a/c/m;->Va:I

    const/16 v0, 0x8

    if-ge v2, v11, :cond_7

    iget-object v11, v6, La/e/a/c/m;->Ua:[La/e/a/c/g;

    aget-object v11, v11, v2

    invoke-virtual {v11}, La/e/a/c/g;->B()I

    move-result v11

    if-ne v11, v0, :cond_6

    add-int/lit8 v3, v3, 0x1

    :cond_6
    add-int/lit8 v2, v2, 0x1

    const/4 v0, 0x2

    goto :goto_1

    :cond_7
    if-lez v3, :cond_a

    sub-int/2addr v11, v3

    new-array v1, v11, [La/e/a/c/g;

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_2
    iget v11, v6, La/e/a/c/m;->Va:I

    if-ge v2, v11, :cond_9

    iget-object v11, v6, La/e/a/c/m;->Ua:[La/e/a/c/g;

    aget-object v11, v11, v2

    invoke-virtual {v11}, La/e/a/c/g;->B()I

    move-result v4

    if-eq v4, v0, :cond_8

    aput-object v11, v1, v3

    add-int/lit8 v3, v3, 0x1

    :cond_8
    add-int/lit8 v2, v2, 0x1

    const/4 v4, 0x1

    goto :goto_2

    :cond_9
    move v2, v3

    goto :goto_3

    :cond_a
    move v2, v11

    :goto_3
    iput-object v1, v6, La/e/a/c/i;->Fb:[La/e/a/c/g;

    iput v2, v6, La/e/a/c/i;->Gb:I

    iget v0, v6, La/e/a/c/i;->yb:I

    if-eqz v0, :cond_e

    const/4 v4, 0x1

    if-eq v0, v4, :cond_d

    const/4 v3, 0x2

    if-eq v0, v3, :cond_c

    const/4 v3, 0x3

    if-eq v0, v3, :cond_b

    move v11, v4

    move-object/from16 v17, v5

    :goto_4
    const/16 v18, 0x0

    goto :goto_5

    :cond_b
    iget v3, v6, La/e/a/c/i;->Ab:I

    move-object/from16 v0, p0

    move v11, v4

    move/from16 v4, v16

    move-object/from16 v17, v5

    invoke-direct/range {v0 .. v5}, La/e/a/c/i;->c([La/e/a/c/g;III[I)V

    goto :goto_4

    :cond_c
    move v11, v4

    move-object/from16 v17, v5

    iget v3, v6, La/e/a/c/i;->Ab:I

    move-object/from16 v0, p0

    move/from16 v4, v16

    invoke-direct/range {v0 .. v5}, La/e/a/c/i;->a([La/e/a/c/g;III[I)V

    goto :goto_4

    :cond_d
    move v11, v4

    move-object/from16 v17, v5

    iget v3, v6, La/e/a/c/i;->Ab:I

    move-object/from16 v0, p0

    move/from16 v4, v16

    invoke-direct/range {v0 .. v5}, La/e/a/c/i;->b([La/e/a/c/g;III[I)V

    goto :goto_4

    :cond_e
    move-object/from16 v17, v5

    const/4 v11, 0x1

    iget v3, v6, La/e/a/c/i;->Ab:I

    move-object/from16 v0, p0

    move/from16 v4, v16

    invoke-direct/range {v0 .. v5}, La/e/a/c/i;->d([La/e/a/c/g;III[I)V

    goto :goto_4

    :goto_5
    aget v0, v17, v18

    add-int/2addr v0, v12

    add-int/2addr v0, v13

    aget v1, v17, v11

    add-int/2addr v1, v14

    add-int/2addr v1, v15

    const/high16 v2, -0x80000000

    const/high16 v3, 0x40000000    # 2.0f

    if-ne v7, v3, :cond_f

    move v0, v8

    goto :goto_6

    :cond_f
    if-ne v7, v2, :cond_10

    invoke-static {v0, v8}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_6

    :cond_10
    if-nez v7, :cond_11

    goto :goto_6

    :cond_11
    move/from16 v0, v18

    :goto_6
    if-ne v9, v3, :cond_12

    move v1, v10

    goto :goto_7

    :cond_12
    if-ne v9, v2, :cond_13

    invoke-static {v1, v10}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_7

    :cond_13
    if-nez v9, :cond_14

    goto :goto_7

    :cond_14
    move/from16 v1, v18

    :goto_7
    invoke-virtual {v6, v0, v1}, La/e/a/c/p;->h(II)V

    invoke-virtual {v6, v0}, La/e/a/c/g;->u(I)V

    invoke-virtual {v6, v1}, La/e/a/c/g;->m(I)V

    iget v0, v6, La/e/a/c/m;->Va:I

    if-lez v0, :cond_15

    goto :goto_8

    :cond_15
    move/from16 v11, v18

    :goto_8
    invoke-virtual {v6, v11}, La/e/a/c/p;->f(Z)V

    return-void
.end method

.method public e(F)V
    .locals 0

    iput p1, p0, La/e/a/c/i;->qb:F

    return-void
.end method

.method public f(F)V
    .locals 0

    iput p1, p0, La/e/a/c/i;->rb:F

    return-void
.end method

.method public g(F)V
    .locals 0

    iput p1, p0, La/e/a/c/i;->ob:F

    return-void
.end method

.method public h(F)V
    .locals 0

    iput p1, p0, La/e/a/c/i;->sb:F

    return-void
.end method

.method public i(F)V
    .locals 0

    iput p1, p0, La/e/a/c/i;->tb:F

    return-void
.end method

.method public j(F)V
    .locals 0

    iput p1, p0, La/e/a/c/i;->pb:F

    return-void
.end method
