.class public La/e/a/c/a/e;
.super Ljava/lang/Object;


# instance fields
.field private a:La/e/a/c/h;

.field private b:Z

.field private c:Z

.field private d:La/e/a/c/h;

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "La/e/a/c/a/s;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "La/e/a/c/a/n;",
            ">;"
        }
    .end annotation
.end field

.field private g:La/e/a/c/a/b$b;

.field private h:La/e/a/c/a/b$a;

.field i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "La/e/a/c/a/n;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(La/e/a/c/h;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, La/e/a/c/a/e;->b:Z

    iput-boolean v0, p0, La/e/a/c/a/e;->c:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, La/e/a/c/a/e;->e:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, La/e/a/c/a/e;->f:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-object v0, p0, La/e/a/c/a/e;->g:La/e/a/c/a/b$b;

    new-instance v0, La/e/a/c/a/b$a;

    invoke-direct {v0}, La/e/a/c/a/b$a;-><init>()V

    iput-object v0, p0, La/e/a/c/a/e;->h:La/e/a/c/a/b$a;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, La/e/a/c/a/e;->i:Ljava/util/ArrayList;

    iput-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iput-object p1, p0, La/e/a/c/a/e;->d:La/e/a/c/h;

    return-void
.end method

.method private a(La/e/a/c/h;I)I
    .locals 6

    iget-object v0, p0, La/e/a/c/a/e;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_0

    iget-object v4, p0, La/e/a/c/a/e;->i:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, La/e/a/c/a/n;

    invoke-virtual {v4, p1, p2}, La/e/a/c/a/n;->a(La/e/a/c/h;I)J

    move-result-wide v4

    invoke-static {v1, v2, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    long-to-int p1, v1

    return p1
.end method

.method private a(La/e/a/c/a/f;IILa/e/a/c/a/f;Ljava/util/ArrayList;La/e/a/c/a/n;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La/e/a/c/a/f;",
            "II",
            "La/e/a/c/a/f;",
            "Ljava/util/ArrayList<",
            "La/e/a/c/a/n;",
            ">;",
            "La/e/a/c/a/n;",
            ")V"
        }
    .end annotation

    iget-object p1, p1, La/e/a/c/a/f;->d:La/e/a/c/a/s;

    iget-object v0, p1, La/e/a/c/a/s;->c:La/e/a/c/a/n;

    if-nez v0, :cond_c

    iget-object v0, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object v1, v0, La/e/a/c/g;->f:La/e/a/c/a/m;

    if-eq p1, v1, :cond_c

    iget-object v0, v0, La/e/a/c/g;->g:La/e/a/c/a/p;

    if-ne p1, v0, :cond_0

    goto/16 :goto_6

    :cond_0
    if-nez p6, :cond_1

    new-instance p6, La/e/a/c/a/n;

    invoke-direct {p6, p1, p3}, La/e/a/c/a/n;-><init>(La/e/a/c/a/s;I)V

    invoke-virtual {p5, p6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iput-object p6, p1, La/e/a/c/a/s;->c:La/e/a/c/a/n;

    invoke-virtual {p6, p1}, La/e/a/c/a/n;->a(La/e/a/c/a/s;)V

    iget-object p3, p1, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget-object p3, p3, La/e/a/c/a/f;->k:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_2
    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/e/a/c/a/d;

    instance-of v1, v0, La/e/a/c/a/f;

    if-eqz v1, :cond_2

    move-object v1, v0

    check-cast v1, La/e/a/c/a/f;

    const/4 v3, 0x0

    move-object v0, p0

    move v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, La/e/a/c/a/e;->a(La/e/a/c/a/f;IILa/e/a/c/a/f;Ljava/util/ArrayList;La/e/a/c/a/n;)V

    goto :goto_0

    :cond_3
    iget-object p3, p1, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget-object p3, p3, La/e/a/c/a/f;->k:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_4
    :goto_1
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/e/a/c/a/d;

    instance-of v1, v0, La/e/a/c/a/f;

    if-eqz v1, :cond_4

    move-object v1, v0

    check-cast v1, La/e/a/c/a/f;

    const/4 v3, 0x1

    move-object v0, p0

    move v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, La/e/a/c/a/e;->a(La/e/a/c/a/f;IILa/e/a/c/a/f;Ljava/util/ArrayList;La/e/a/c/a/n;)V

    goto :goto_1

    :cond_5
    const/4 p3, 0x1

    if-ne p2, p3, :cond_7

    instance-of v0, p1, La/e/a/c/a/p;

    if-eqz v0, :cond_7

    move-object v0, p1

    check-cast v0, La/e/a/c/a/p;

    iget-object v0, v0, La/e/a/c/a/p;->k:La/e/a/c/a/f;

    iget-object v0, v0, La/e/a/c/a/f;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_6
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/e/a/c/a/d;

    instance-of v1, v0, La/e/a/c/a/f;

    if-eqz v1, :cond_6

    move-object v1, v0

    check-cast v1, La/e/a/c/a/f;

    const/4 v3, 0x2

    move-object v0, p0

    move v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, La/e/a/c/a/e;->a(La/e/a/c/a/f;IILa/e/a/c/a/f;Ljava/util/ArrayList;La/e/a/c/a/n;)V

    goto :goto_2

    :cond_7
    iget-object v0, p1, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget-object v0, v0, La/e/a/c/a/f;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, La/e/a/c/a/f;

    if-ne v1, p4, :cond_8

    iput-boolean p3, p6, La/e/a/c/a/n;->c:Z

    :cond_8
    const/4 v3, 0x0

    move-object v0, p0

    move v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, La/e/a/c/a/e;->a(La/e/a/c/a/f;IILa/e/a/c/a/f;Ljava/util/ArrayList;La/e/a/c/a/n;)V

    goto :goto_3

    :cond_9
    iget-object v0, p1, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget-object v0, v0, La/e/a/c/a/f;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, La/e/a/c/a/f;

    if-ne v1, p4, :cond_a

    iput-boolean p3, p6, La/e/a/c/a/n;->c:Z

    :cond_a
    const/4 v3, 0x1

    move-object v0, p0

    move v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, La/e/a/c/a/e;->a(La/e/a/c/a/f;IILa/e/a/c/a/f;Ljava/util/ArrayList;La/e/a/c/a/n;)V

    goto :goto_4

    :cond_b
    if-ne p2, p3, :cond_c

    instance-of p3, p1, La/e/a/c/a/p;

    if-eqz p3, :cond_c

    check-cast p1, La/e/a/c/a/p;

    iget-object p1, p1, La/e/a/c/a/p;->k:La/e/a/c/a/f;

    iget-object p1, p1, La/e/a/c/a/f;->l:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_5
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_c

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    move-object v1, p3

    check-cast v1, La/e/a/c/a/f;

    const/4 v3, 0x2

    move-object v0, p0

    move v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, La/e/a/c/a/e;->a(La/e/a/c/a/f;IILa/e/a/c/a/f;Ljava/util/ArrayList;La/e/a/c/a/n;)V

    goto :goto_5

    :cond_c
    :goto_6
    return-void
.end method

.method private a(La/e/a/c/a/s;ILjava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La/e/a/c/a/s;",
            "I",
            "Ljava/util/ArrayList<",
            "La/e/a/c/a/n;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p1, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget-object v0, v0, La/e/a/c/a/f;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La/e/a/c/a/d;

    instance-of v2, v1, La/e/a/c/a/f;

    if-eqz v2, :cond_1

    move-object v4, v1

    check-cast v4, La/e/a/c/a/f;

    const/4 v6, 0x0

    iget-object v7, p1, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    const/4 v9, 0x0

    move-object v3, p0

    move v5, p2

    move-object v8, p3

    invoke-direct/range {v3 .. v9}, La/e/a/c/a/e;->a(La/e/a/c/a/f;IILa/e/a/c/a/f;Ljava/util/ArrayList;La/e/a/c/a/n;)V

    goto :goto_0

    :cond_1
    instance-of v2, v1, La/e/a/c/a/s;

    if-eqz v2, :cond_0

    check-cast v1, La/e/a/c/a/s;

    iget-object v3, v1, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    const/4 v5, 0x0

    iget-object v6, p1, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    const/4 v8, 0x0

    move-object v2, p0

    move v4, p2

    move-object v7, p3

    invoke-direct/range {v2 .. v8}, La/e/a/c/a/e;->a(La/e/a/c/a/f;IILa/e/a/c/a/f;Ljava/util/ArrayList;La/e/a/c/a/n;)V

    goto :goto_0

    :cond_2
    iget-object v0, p1, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget-object v0, v0, La/e/a/c/a/f;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La/e/a/c/a/d;

    instance-of v2, v1, La/e/a/c/a/f;

    if-eqz v2, :cond_4

    move-object v4, v1

    check-cast v4, La/e/a/c/a/f;

    const/4 v6, 0x1

    iget-object v7, p1, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    const/4 v9, 0x0

    move-object v3, p0

    move v5, p2

    move-object v8, p3

    invoke-direct/range {v3 .. v9}, La/e/a/c/a/e;->a(La/e/a/c/a/f;IILa/e/a/c/a/f;Ljava/util/ArrayList;La/e/a/c/a/n;)V

    goto :goto_1

    :cond_4
    instance-of v2, v1, La/e/a/c/a/s;

    if-eqz v2, :cond_3

    check-cast v1, La/e/a/c/a/s;

    iget-object v3, v1, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    const/4 v5, 0x1

    iget-object v6, p1, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    const/4 v8, 0x0

    move-object v2, p0

    move v4, p2

    move-object v7, p3

    invoke-direct/range {v2 .. v8}, La/e/a/c/a/e;->a(La/e/a/c/a/f;IILa/e/a/c/a/f;Ljava/util/ArrayList;La/e/a/c/a/n;)V

    goto :goto_1

    :cond_5
    const/4 v0, 0x1

    if-ne p2, v0, :cond_7

    check-cast p1, La/e/a/c/a/p;

    iget-object p1, p1, La/e/a/c/a/p;->k:La/e/a/c/a/f;

    iget-object p1, p1, La/e/a/c/a/f;->k:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_6
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/e/a/c/a/d;

    instance-of v1, v0, La/e/a/c/a/f;

    if-eqz v1, :cond_6

    move-object v3, v0

    check-cast v3, La/e/a/c/a/f;

    const/4 v5, 0x2

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object v2, p0

    move v4, p2

    move-object v7, p3

    invoke-direct/range {v2 .. v8}, La/e/a/c/a/e;->a(La/e/a/c/a/f;IILa/e/a/c/a/f;Ljava/util/ArrayList;La/e/a/c/a/n;)V

    goto :goto_2

    :cond_7
    return-void
.end method

.method private a(La/e/a/c/g;La/e/a/c/g$a;ILa/e/a/c/g$a;I)V
    .locals 1

    iget-object v0, p0, La/e/a/c/a/e;->h:La/e/a/c/a/b$a;

    iput-object p2, v0, La/e/a/c/a/b$a;->d:La/e/a/c/g$a;

    iput-object p4, v0, La/e/a/c/a/b$a;->e:La/e/a/c/g$a;

    iput p3, v0, La/e/a/c/a/b$a;->f:I

    iput p5, v0, La/e/a/c/a/b$a;->g:I

    iget-object p2, p0, La/e/a/c/a/e;->g:La/e/a/c/a/b$b;

    invoke-interface {p2, p1, v0}, La/e/a/c/a/b$b;->a(La/e/a/c/g;La/e/a/c/a/b$a;)V

    iget-object p2, p0, La/e/a/c/a/e;->h:La/e/a/c/a/b$a;

    iget p2, p2, La/e/a/c/a/b$a;->h:I

    invoke-virtual {p1, p2}, La/e/a/c/g;->u(I)V

    iget-object p2, p0, La/e/a/c/a/e;->h:La/e/a/c/a/b$a;

    iget p2, p2, La/e/a/c/a/b$a;->i:I

    invoke-virtual {p1, p2}, La/e/a/c/g;->m(I)V

    iget-object p2, p0, La/e/a/c/a/e;->h:La/e/a/c/a/b$a;

    iget-boolean p2, p2, La/e/a/c/a/b$a;->k:Z

    invoke-virtual {p1, p2}, La/e/a/c/g;->a(Z)V

    iget-object p2, p0, La/e/a/c/a/e;->h:La/e/a/c/a/b$a;

    iget p2, p2, La/e/a/c/a/b$a;->j:I

    invoke-virtual {p1, p2}, La/e/a/c/g;->i(I)V

    return-void
.end method

.method private a(La/e/a/c/h;)Z
    .locals 13

    iget-object v0, p1, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_26

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La/e/a/c/g;

    iget-object v3, v1, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object v4, v3, v2

    const/4 v9, 0x1

    aget-object v3, v3, v9

    invoke-virtual {v1}, La/e/a/c/g;->B()I

    move-result v5

    const/16 v6, 0x8

    if-ne v5, v6, :cond_1

    iput-boolean v9, v1, La/e/a/c/g;->b:Z

    goto :goto_0

    :cond_1
    iget v5, v1, La/e/a/c/g;->C:F

    const/high16 v10, 0x3f800000    # 1.0f

    cmpg-float v5, v5, v10

    const/4 v6, 0x2

    if-gez v5, :cond_2

    sget-object v5, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v4, v5, :cond_2

    iput v6, v1, La/e/a/c/g;->x:I

    :cond_2
    iget v5, v1, La/e/a/c/g;->F:F

    cmpg-float v5, v5, v10

    if-gez v5, :cond_3

    sget-object v5, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v3, v5, :cond_3

    iput v6, v1, La/e/a/c/g;->y:I

    :cond_3
    invoke-virtual {v1}, La/e/a/c/g;->i()F

    move-result v5

    const/4 v7, 0x0

    cmpl-float v5, v5, v7

    const/4 v7, 0x3

    if-lez v5, :cond_9

    sget-object v5, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v4, v5, :cond_5

    sget-object v5, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-eq v3, v5, :cond_4

    sget-object v5, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    if-ne v3, v5, :cond_5

    :cond_4
    iput v7, v1, La/e/a/c/g;->x:I

    goto :goto_1

    :cond_5
    sget-object v5, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v3, v5, :cond_7

    sget-object v5, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-eq v4, v5, :cond_6

    sget-object v5, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    if-ne v4, v5, :cond_7

    :cond_6
    iput v7, v1, La/e/a/c/g;->y:I

    goto :goto_1

    :cond_7
    sget-object v5, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v4, v5, :cond_9

    if-ne v3, v5, :cond_9

    iget v5, v1, La/e/a/c/g;->x:I

    if-nez v5, :cond_8

    iput v7, v1, La/e/a/c/g;->x:I

    :cond_8
    iget v5, v1, La/e/a/c/g;->y:I

    if-nez v5, :cond_9

    iput v7, v1, La/e/a/c/g;->y:I

    :cond_9
    :goto_1
    sget-object v5, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v4, v5, :cond_b

    iget v5, v1, La/e/a/c/g;->x:I

    if-ne v5, v9, :cond_b

    iget-object v5, v1, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v5, v5, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v5, :cond_a

    iget-object v5, v1, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v5, v5, La/e/a/c/e;->f:La/e/a/c/e;

    if-nez v5, :cond_b

    :cond_a
    sget-object v4, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    :cond_b
    move-object v5, v4

    sget-object v4, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v3, v4, :cond_d

    iget v4, v1, La/e/a/c/g;->y:I

    if-ne v4, v9, :cond_d

    iget-object v4, v1, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v4, v4, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v4, :cond_c

    iget-object v4, v1, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object v4, v4, La/e/a/c/e;->f:La/e/a/c/e;

    if-nez v4, :cond_d

    :cond_c
    sget-object v3, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    :cond_d
    move-object v8, v3

    iget-object v3, v1, La/e/a/c/g;->f:La/e/a/c/a/m;

    iput-object v5, v3, La/e/a/c/a/s;->d:La/e/a/c/g$a;

    iget v4, v1, La/e/a/c/g;->x:I

    iput v4, v3, La/e/a/c/a/s;->a:I

    iget-object v3, v1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iput-object v8, v3, La/e/a/c/a/s;->d:La/e/a/c/g$a;

    iget v4, v1, La/e/a/c/g;->y:I

    iput v4, v3, La/e/a/c/a/s;->a:I

    sget-object v3, La/e/a/c/g$a;->d:La/e/a/c/g$a;

    if-eq v5, v3, :cond_e

    sget-object v3, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    if-eq v5, v3, :cond_e

    sget-object v3, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v5, v3, :cond_f

    :cond_e
    sget-object v3, La/e/a/c/g$a;->d:La/e/a/c/g$a;

    if-eq v8, v3, :cond_23

    sget-object v3, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    if-eq v8, v3, :cond_23

    sget-object v3, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v8, v3, :cond_f

    goto/16 :goto_3

    :cond_f
    sget-object v3, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    const/high16 v11, 0x3f000000    # 0.5f

    if-ne v5, v3, :cond_17

    sget-object v3, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-eq v8, v3, :cond_10

    sget-object v3, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    if-ne v8, v3, :cond_17

    :cond_10
    iget v3, v1, La/e/a/c/g;->x:I

    if-ne v3, v7, :cond_12

    sget-object v7, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v8, v7, :cond_11

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object v3, p0

    move-object v4, v1

    move-object v5, v7

    invoke-direct/range {v3 .. v8}, La/e/a/c/a/e;->a(La/e/a/c/g;La/e/a/c/g$a;ILa/e/a/c/g$a;I)V

    :cond_11
    invoke-virtual {v1}, La/e/a/c/g;->k()I

    move-result v8

    int-to-float v2, v8

    iget v3, v1, La/e/a/c/g;->ga:F

    mul-float/2addr v2, v3

    add-float/2addr v2, v11

    float-to-int v6, v2

    sget-object v7, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    move-object v3, p0

    move-object v4, v1

    move-object v5, v7

    invoke-direct/range {v3 .. v8}, La/e/a/c/a/e;->a(La/e/a/c/g;La/e/a/c/g$a;ILa/e/a/c/g$a;I)V

    iget-object v2, v1, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v1}, La/e/a/c/g;->C()I

    move-result v3

    invoke-virtual {v2, v3}, La/e/a/c/a/g;->a(I)V

    iget-object v2, v1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v1}, La/e/a/c/g;->k()I

    move-result v3

    invoke-virtual {v2, v3}, La/e/a/c/a/g;->a(I)V

    iput-boolean v9, v1, La/e/a/c/g;->b:Z

    goto/16 :goto_0

    :cond_12
    if-ne v3, v9, :cond_13

    sget-object v5, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    const/4 v6, 0x0

    const/4 v2, 0x0

    move-object v3, p0

    move-object v4, v1

    move-object v7, v8

    move v8, v2

    invoke-direct/range {v3 .. v8}, La/e/a/c/a/e;->a(La/e/a/c/g;La/e/a/c/g$a;ILa/e/a/c/g$a;I)V

    iget-object v2, v1, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v1}, La/e/a/c/g;->C()I

    move-result v1

    iput v1, v2, La/e/a/c/a/g;->m:I

    goto/16 :goto_0

    :cond_13
    if-ne v3, v6, :cond_15

    iget-object v3, p1, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object v4, v3, v2

    sget-object v12, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    if-eq v4, v12, :cond_14

    aget-object v3, v3, v2

    sget-object v4, La/e/a/c/g$a;->d:La/e/a/c/g$a;

    if-ne v3, v4, :cond_17

    :cond_14
    iget v2, v1, La/e/a/c/g;->C:F

    invoke-virtual {p1}, La/e/a/c/g;->C()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    add-float/2addr v2, v11

    float-to-int v6, v2

    invoke-virtual {v1}, La/e/a/c/g;->k()I

    move-result v2

    sget-object v5, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    move-object v3, p0

    move-object v4, v1

    move-object v7, v8

    move v8, v2

    invoke-direct/range {v3 .. v8}, La/e/a/c/a/e;->a(La/e/a/c/g;La/e/a/c/g$a;ILa/e/a/c/g$a;I)V

    iget-object v2, v1, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v1}, La/e/a/c/g;->C()I

    move-result v3

    invoke-virtual {v2, v3}, La/e/a/c/a/g;->a(I)V

    iget-object v2, v1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v1}, La/e/a/c/g;->k()I

    move-result v3

    invoke-virtual {v2, v3}, La/e/a/c/a/g;->a(I)V

    iput-boolean v9, v1, La/e/a/c/g;->b:Z

    goto/16 :goto_0

    :cond_15
    iget-object v3, v1, La/e/a/c/g;->Z:[La/e/a/c/e;

    aget-object v4, v3, v2

    iget-object v4, v4, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v4, :cond_16

    aget-object v3, v3, v9

    iget-object v3, v3, La/e/a/c/e;->f:La/e/a/c/e;

    if-nez v3, :cond_17

    :cond_16
    sget-object v5, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    const/4 v6, 0x0

    const/4 v2, 0x0

    move-object v3, p0

    move-object v4, v1

    move-object v7, v8

    move v8, v2

    invoke-direct/range {v3 .. v8}, La/e/a/c/a/e;->a(La/e/a/c/g;La/e/a/c/g$a;ILa/e/a/c/g$a;I)V

    iget-object v2, v1, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v1}, La/e/a/c/g;->C()I

    move-result v3

    invoke-virtual {v2, v3}, La/e/a/c/a/g;->a(I)V

    iget-object v2, v1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v1}, La/e/a/c/g;->k()I

    move-result v3

    invoke-virtual {v2, v3}, La/e/a/c/a/g;->a(I)V

    iput-boolean v9, v1, La/e/a/c/g;->b:Z

    goto/16 :goto_0

    :cond_17
    sget-object v3, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v8, v3, :cond_20

    sget-object v3, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-eq v5, v3, :cond_18

    sget-object v3, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    if-ne v5, v3, :cond_20

    :cond_18
    iget v3, v1, La/e/a/c/g;->y:I

    if-ne v3, v7, :cond_1b

    sget-object v7, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v5, v7, :cond_19

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object v3, p0

    move-object v4, v1

    move-object v5, v7

    invoke-direct/range {v3 .. v8}, La/e/a/c/a/e;->a(La/e/a/c/g;La/e/a/c/g$a;ILa/e/a/c/g$a;I)V

    :cond_19
    invoke-virtual {v1}, La/e/a/c/g;->C()I

    move-result v6

    iget v2, v1, La/e/a/c/g;->ga:F

    invoke-virtual {v1}, La/e/a/c/g;->j()I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1a

    div-float v2, v10, v2

    :cond_1a
    int-to-float v3, v6

    mul-float/2addr v3, v2

    add-float/2addr v3, v11

    float-to-int v8, v3

    sget-object v7, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    move-object v3, p0

    move-object v4, v1

    move-object v5, v7

    invoke-direct/range {v3 .. v8}, La/e/a/c/a/e;->a(La/e/a/c/g;La/e/a/c/g$a;ILa/e/a/c/g$a;I)V

    iget-object v2, v1, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v1}, La/e/a/c/g;->C()I

    move-result v3

    invoke-virtual {v2, v3}, La/e/a/c/a/g;->a(I)V

    iget-object v2, v1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v1}, La/e/a/c/g;->k()I

    move-result v3

    invoke-virtual {v2, v3}, La/e/a/c/a/g;->a(I)V

    iput-boolean v9, v1, La/e/a/c/g;->b:Z

    goto/16 :goto_0

    :cond_1b
    if-ne v3, v9, :cond_1c

    const/4 v6, 0x0

    sget-object v7, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    const/4 v8, 0x0

    move-object v3, p0

    move-object v4, v1

    invoke-direct/range {v3 .. v8}, La/e/a/c/a/e;->a(La/e/a/c/g;La/e/a/c/g$a;ILa/e/a/c/g$a;I)V

    iget-object v2, v1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v1}, La/e/a/c/g;->k()I

    move-result v1

    iput v1, v2, La/e/a/c/a/g;->m:I

    goto/16 :goto_0

    :cond_1c
    if-ne v3, v6, :cond_1e

    iget-object v3, p1, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object v4, v3, v9

    sget-object v7, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    if-eq v4, v7, :cond_1d

    aget-object v3, v3, v9

    sget-object v4, La/e/a/c/g$a;->d:La/e/a/c/g$a;

    if-ne v3, v4, :cond_20

    :cond_1d
    iget v2, v1, La/e/a/c/g;->F:F

    invoke-virtual {v1}, La/e/a/c/g;->C()I

    move-result v6

    invoke-virtual {p1}, La/e/a/c/g;->k()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    add-float/2addr v2, v11

    float-to-int v8, v2

    sget-object v7, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    move-object v3, p0

    move-object v4, v1

    invoke-direct/range {v3 .. v8}, La/e/a/c/a/e;->a(La/e/a/c/g;La/e/a/c/g$a;ILa/e/a/c/g$a;I)V

    iget-object v2, v1, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v1}, La/e/a/c/g;->C()I

    move-result v3

    invoke-virtual {v2, v3}, La/e/a/c/a/g;->a(I)V

    iget-object v2, v1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v1}, La/e/a/c/g;->k()I

    move-result v3

    invoke-virtual {v2, v3}, La/e/a/c/a/g;->a(I)V

    iput-boolean v9, v1, La/e/a/c/g;->b:Z

    goto/16 :goto_0

    :cond_1e
    iget-object v3, v1, La/e/a/c/g;->Z:[La/e/a/c/e;

    aget-object v4, v3, v6

    iget-object v4, v4, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v4, :cond_1f

    aget-object v3, v3, v7

    iget-object v3, v3, La/e/a/c/e;->f:La/e/a/c/e;

    if-nez v3, :cond_20

    :cond_1f
    sget-object v5, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    const/4 v6, 0x0

    const/4 v2, 0x0

    move-object v3, p0

    move-object v4, v1

    move-object v7, v8

    move v8, v2

    invoke-direct/range {v3 .. v8}, La/e/a/c/a/e;->a(La/e/a/c/g;La/e/a/c/g$a;ILa/e/a/c/g$a;I)V

    iget-object v2, v1, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v1}, La/e/a/c/g;->C()I

    move-result v3

    invoke-virtual {v2, v3}, La/e/a/c/a/g;->a(I)V

    iget-object v2, v1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v1}, La/e/a/c/g;->k()I

    move-result v3

    invoke-virtual {v2, v3}, La/e/a/c/a/g;->a(I)V

    iput-boolean v9, v1, La/e/a/c/g;->b:Z

    goto/16 :goto_0

    :cond_20
    sget-object v3, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v5, v3, :cond_0

    if-ne v8, v3, :cond_0

    iget v3, v1, La/e/a/c/g;->x:I

    if-eq v3, v9, :cond_22

    iget v4, v1, La/e/a/c/g;->y:I

    if-ne v4, v9, :cond_21

    goto :goto_2

    :cond_21
    if-ne v4, v6, :cond_0

    if-ne v3, v6, :cond_0

    iget-object v3, p1, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object v2, v3, v2

    sget-object v4, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    if-ne v2, v4, :cond_0

    aget-object v2, v3, v9

    if-ne v2, v4, :cond_0

    iget v2, v1, La/e/a/c/g;->C:F

    iget v3, v1, La/e/a/c/g;->F:F

    invoke-virtual {p1}, La/e/a/c/g;->C()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v2, v4

    add-float/2addr v2, v11

    float-to-int v6, v2

    invoke-virtual {p1}, La/e/a/c/g;->k()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v3, v2

    add-float/2addr v3, v11

    float-to-int v8, v3

    sget-object v7, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    move-object v3, p0

    move-object v4, v1

    move-object v5, v7

    invoke-direct/range {v3 .. v8}, La/e/a/c/a/e;->a(La/e/a/c/g;La/e/a/c/g$a;ILa/e/a/c/g$a;I)V

    iget-object v2, v1, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v1}, La/e/a/c/g;->C()I

    move-result v3

    invoke-virtual {v2, v3}, La/e/a/c/a/g;->a(I)V

    iget-object v2, v1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v1}, La/e/a/c/g;->k()I

    move-result v3

    invoke-virtual {v2, v3}, La/e/a/c/a/g;->a(I)V

    iput-boolean v9, v1, La/e/a/c/g;->b:Z

    goto/16 :goto_0

    :cond_22
    :goto_2
    sget-object v7, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object v3, p0

    move-object v4, v1

    move-object v5, v7

    invoke-direct/range {v3 .. v8}, La/e/a/c/a/e;->a(La/e/a/c/g;La/e/a/c/g$a;ILa/e/a/c/g$a;I)V

    iget-object v2, v1, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v1}, La/e/a/c/g;->C()I

    move-result v3

    iput v3, v2, La/e/a/c/a/g;->m:I

    iget-object v2, v1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v1}, La/e/a/c/g;->k()I

    move-result v1

    iput v1, v2, La/e/a/c/a/g;->m:I

    goto/16 :goto_0

    :cond_23
    :goto_3
    invoke-virtual {v1}, La/e/a/c/g;->C()I

    move-result v2

    sget-object v3, La/e/a/c/g$a;->d:La/e/a/c/g$a;

    if-ne v5, v3, :cond_24

    invoke-virtual {p1}, La/e/a/c/g;->C()I

    move-result v2

    iget-object v3, v1, La/e/a/c/g;->R:La/e/a/c/e;

    iget v3, v3, La/e/a/c/e;->g:I

    sub-int/2addr v2, v3

    iget-object v3, v1, La/e/a/c/g;->T:La/e/a/c/e;

    iget v3, v3, La/e/a/c/e;->g:I

    sub-int/2addr v2, v3

    sget-object v3, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    move v6, v2

    move-object v5, v3

    goto :goto_4

    :cond_24
    move v6, v2

    :goto_4
    invoke-virtual {v1}, La/e/a/c/g;->k()I

    move-result v2

    sget-object v3, La/e/a/c/g$a;->d:La/e/a/c/g$a;

    if-ne v8, v3, :cond_25

    invoke-virtual {p1}, La/e/a/c/g;->k()I

    move-result v2

    iget-object v3, v1, La/e/a/c/g;->S:La/e/a/c/e;

    iget v3, v3, La/e/a/c/e;->g:I

    sub-int/2addr v2, v3

    iget-object v3, v1, La/e/a/c/g;->U:La/e/a/c/e;

    iget v3, v3, La/e/a/c/e;->g:I

    sub-int/2addr v2, v3

    sget-object v3, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    move v8, v2

    move-object v7, v3

    goto :goto_5

    :cond_25
    move-object v7, v8

    move v8, v2

    :goto_5
    move-object v3, p0

    move-object v4, v1

    invoke-direct/range {v3 .. v8}, La/e/a/c/a/e;->a(La/e/a/c/g;La/e/a/c/g$a;ILa/e/a/c/g$a;I)V

    iget-object v2, v1, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v1}, La/e/a/c/g;->C()I

    move-result v3

    invoke-virtual {v2, v3}, La/e/a/c/a/g;->a(I)V

    iget-object v2, v1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v1}, La/e/a/c/g;->k()I

    move-result v3

    invoke-virtual {v2, v3}, La/e/a/c/a/g;->a(I)V

    iput-boolean v9, v1, La/e/a/c/g;->b:Z

    goto/16 :goto_0

    :cond_26
    return v2
.end method


# virtual methods
.method public a()V
    .locals 4

    iget-object v0, p0, La/e/a/c/a/e;->e:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, La/e/a/c/a/e;->a(Ljava/util/ArrayList;)V

    iget-object v0, p0, La/e/a/c/a/e;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    sput v0, La/e/a/c/a/n;->a:I

    iget-object v1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object v1, v1, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v2, p0, La/e/a/c/a/e;->i:Ljava/util/ArrayList;

    invoke-direct {p0, v1, v0, v2}, La/e/a/c/a/e;->a(La/e/a/c/a/s;ILjava/util/ArrayList;)V

    iget-object v1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object v1, v1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v2, p0, La/e/a/c/a/e;->i:Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-direct {p0, v1, v3, v2}, La/e/a/c/a/e;->a(La/e/a/c/a/s;ILjava/util/ArrayList;)V

    iput-boolean v0, p0, La/e/a/c/a/e;->b:Z

    return-void
.end method

.method public a(La/e/a/c/a/b$b;)V
    .locals 0

    iput-object p1, p0, La/e/a/c/a/e;->g:La/e/a/c/a/b$b;

    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "La/e/a/c/a/s;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, La/e/a/c/a/e;->d:La/e/a/c/h;

    iget-object v0, v0, La/e/a/c/g;->f:La/e/a/c/a/m;

    invoke-virtual {v0}, La/e/a/c/a/m;->c()V

    iget-object v0, p0, La/e/a/c/a/e;->d:La/e/a/c/h;

    iget-object v0, v0, La/e/a/c/g;->g:La/e/a/c/a/p;

    invoke-virtual {v0}, La/e/a/c/a/p;->c()V

    iget-object v0, p0, La/e/a/c/a/e;->d:La/e/a/c/h;

    iget-object v0, v0, La/e/a/c/g;->f:La/e/a/c/a/m;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, La/e/a/c/a/e;->d:La/e/a/c/h;

    iget-object v0, v0, La/e/a/c/g;->g:La/e/a/c/a/p;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, La/e/a/c/a/e;->d:La/e/a/c/h;

    iget-object v0, v0, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, La/e/a/c/g;

    instance-of v3, v2, La/e/a/c/k;

    if-eqz v3, :cond_1

    new-instance v3, La/e/a/c/a/j;

    invoke-direct {v3, v2}, La/e/a/c/a/j;-><init>(La/e/a/c/g;)V

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, La/e/a/c/g;->J()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, v2, La/e/a/c/g;->d:La/e/a/c/a/c;

    if-nez v3, :cond_2

    new-instance v3, La/e/a/c/a/c;

    const/4 v4, 0x0

    invoke-direct {v3, v2, v4}, La/e/a/c/a/c;-><init>(La/e/a/c/g;I)V

    iput-object v3, v2, La/e/a/c/g;->d:La/e/a/c/a/c;

    :cond_2
    if-nez v1, :cond_3

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    :cond_3
    iget-object v3, v2, La/e/a/c/g;->d:La/e/a/c/a/c;

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    iget-object v3, v2, La/e/a/c/g;->f:La/e/a/c/a/m;

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    invoke-virtual {v2}, La/e/a/c/g;->L()Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, v2, La/e/a/c/g;->e:La/e/a/c/a/c;

    if-nez v3, :cond_5

    new-instance v3, La/e/a/c/a/c;

    const/4 v4, 0x1

    invoke-direct {v3, v2, v4}, La/e/a/c/a/c;-><init>(La/e/a/c/g;I)V

    iput-object v3, v2, La/e/a/c/g;->e:La/e/a/c/a/c;

    :cond_5
    if-nez v1, :cond_6

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    :cond_6
    iget-object v3, v2, La/e/a/c/g;->e:La/e/a/c/a/c;

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_7
    iget-object v3, v2, La/e/a/c/g;->g:La/e/a/c/a/p;

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    instance-of v3, v2, La/e/a/c/m;

    if-eqz v3, :cond_0

    new-instance v3, La/e/a/c/a/k;

    invoke-direct {v3, v2}, La/e/a/c/a/k;-><init>(La/e/a/c/g;)V

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_8
    if-eqz v1, :cond_9

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_9
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La/e/a/c/a/s;

    invoke-virtual {v1}, La/e/a/c/a/s;->c()V

    goto :goto_3

    :cond_a
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/e/a/c/a/s;

    iget-object v1, v0, La/e/a/c/a/s;->b:La/e/a/c/g;

    iget-object v2, p0, La/e/a/c/a/e;->d:La/e/a/c/h;

    if-ne v1, v2, :cond_b

    goto :goto_4

    :cond_b
    invoke-virtual {v0}, La/e/a/c/a/s;->a()V

    goto :goto_4

    :cond_c
    return-void
.end method

.method public a(Z)Z
    .locals 8

    const/4 v0, 0x1

    and-int/2addr p1, v0

    iget-boolean v1, p0, La/e/a/c/a/e;->b:Z

    const/4 v2, 0x0

    if-nez v1, :cond_0

    iget-boolean v1, p0, La/e/a/c/a/e;->c:Z

    if-eqz v1, :cond_2

    :cond_0
    iget-object v1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object v1, v1, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, La/e/a/c/g;

    invoke-virtual {v3}, La/e/a/c/g;->d()V

    iput-boolean v2, v3, La/e/a/c/g;->b:Z

    iget-object v4, v3, La/e/a/c/g;->f:La/e/a/c/a/m;

    invoke-virtual {v4}, La/e/a/c/a/m;->g()V

    iget-object v3, v3, La/e/a/c/g;->g:La/e/a/c/a/p;

    invoke-virtual {v3}, La/e/a/c/a/p;->g()V

    goto :goto_0

    :cond_1
    iget-object v1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-virtual {v1}, La/e/a/c/g;->d()V

    iget-object v1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iput-boolean v2, v1, La/e/a/c/g;->b:Z

    iget-object v1, v1, La/e/a/c/g;->f:La/e/a/c/a/m;

    invoke-virtual {v1}, La/e/a/c/a/m;->g()V

    iget-object v1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object v1, v1, La/e/a/c/g;->g:La/e/a/c/a/p;

    invoke-virtual {v1}, La/e/a/c/a/p;->g()V

    iput-boolean v2, p0, La/e/a/c/a/e;->c:Z

    :cond_2
    iget-object v1, p0, La/e/a/c/a/e;->d:La/e/a/c/h;

    invoke-direct {p0, v1}, La/e/a/c/a/e;->a(La/e/a/c/h;)Z

    move-result v1

    if-eqz v1, :cond_3

    return v2

    :cond_3
    iget-object v1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-virtual {v1, v2}, La/e/a/c/g;->w(I)V

    iget-object v1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-virtual {v1, v2}, La/e/a/c/g;->x(I)V

    iget-object v1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-virtual {v1, v2}, La/e/a/c/g;->b(I)La/e/a/c/g$a;

    move-result-object v1

    iget-object v3, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-virtual {v3, v0}, La/e/a/c/g;->b(I)La/e/a/c/g$a;

    move-result-object v3

    iget-boolean v4, p0, La/e/a/c/a/e;->b:Z

    if-eqz v4, :cond_4

    invoke-virtual {p0}, La/e/a/c/a/e;->a()V

    :cond_4
    iget-object v4, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-virtual {v4}, La/e/a/c/g;->D()I

    move-result v4

    iget-object v5, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-virtual {v5}, La/e/a/c/g;->E()I

    move-result v5

    iget-object v6, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object v6, v6, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v6, v6, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    invoke-virtual {v6, v4}, La/e/a/c/a/f;->a(I)V

    iget-object v6, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object v6, v6, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v6, v6, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    invoke-virtual {v6, v5}, La/e/a/c/a/f;->a(I)V

    invoke-virtual {p0}, La/e/a/c/a/e;->d()V

    sget-object v6, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-eq v1, v6, :cond_5

    if-ne v3, v6, :cond_9

    :cond_5
    if-eqz p1, :cond_7

    iget-object v6, p0, La/e/a/c/a/e;->e:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, La/e/a/c/a/s;

    invoke-virtual {v7}, La/e/a/c/a/s;->f()Z

    move-result v7

    if-nez v7, :cond_6

    move p1, v2

    :cond_7
    if-eqz p1, :cond_8

    sget-object v6, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v1, v6, :cond_8

    iget-object v6, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    sget-object v7, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    invoke-virtual {v6, v7}, La/e/a/c/g;->a(La/e/a/c/g$a;)V

    iget-object v6, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-direct {p0, v6, v2}, La/e/a/c/a/e;->a(La/e/a/c/h;I)I

    move-result v7

    invoke-virtual {v6, v7}, La/e/a/c/g;->u(I)V

    iget-object v6, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object v7, v6, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v7, v7, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v6}, La/e/a/c/g;->C()I

    move-result v6

    invoke-virtual {v7, v6}, La/e/a/c/a/g;->a(I)V

    :cond_8
    if-eqz p1, :cond_9

    sget-object p1, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v3, p1, :cond_9

    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    sget-object v6, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    invoke-virtual {p1, v6}, La/e/a/c/g;->b(La/e/a/c/g$a;)V

    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-direct {p0, p1, v0}, La/e/a/c/a/e;->a(La/e/a/c/h;I)I

    move-result v6

    invoke-virtual {p1, v6}, La/e/a/c/g;->m(I)V

    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object v6, p1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v6, v6, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {p1}, La/e/a/c/g;->k()I

    move-result p1

    invoke-virtual {v6, p1}, La/e/a/c/a/g;->a(I)V

    :cond_9
    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object p1, p1, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object v6, p1, v2

    sget-object v7, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    if-eq v6, v7, :cond_b

    aget-object p1, p1, v2

    sget-object v6, La/e/a/c/g$a;->d:La/e/a/c/g$a;

    if-ne p1, v6, :cond_a

    goto :goto_1

    :cond_a
    move p1, v2

    goto :goto_2

    :cond_b
    :goto_1
    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-virtual {p1}, La/e/a/c/g;->C()I

    move-result p1

    add-int/2addr p1, v4

    iget-object v6, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object v6, v6, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v6, v6, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    invoke-virtual {v6, p1}, La/e/a/c/a/f;->a(I)V

    iget-object v6, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object v6, v6, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v6, v6, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    sub-int/2addr p1, v4

    invoke-virtual {v6, p1}, La/e/a/c/a/g;->a(I)V

    invoke-virtual {p0}, La/e/a/c/a/e;->d()V

    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object p1, p1, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object v4, p1, v0

    sget-object v6, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    if-eq v4, v6, :cond_c

    aget-object p1, p1, v0

    sget-object v4, La/e/a/c/g$a;->d:La/e/a/c/g$a;

    if-ne p1, v4, :cond_d

    :cond_c
    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-virtual {p1}, La/e/a/c/g;->k()I

    move-result p1

    add-int/2addr p1, v5

    iget-object v4, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object v4, v4, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v4, v4, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    invoke-virtual {v4, p1}, La/e/a/c/a/f;->a(I)V

    iget-object v4, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object v4, v4, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v4, v4, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    sub-int/2addr p1, v5

    invoke-virtual {v4, p1}, La/e/a/c/a/g;->a(I)V

    :cond_d
    invoke-virtual {p0}, La/e/a/c/a/e;->d()V

    move p1, v0

    :goto_2
    iget-object v4, p0, La/e/a/c/a/e;->e:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/c/a/s;

    iget-object v6, v5, La/e/a/c/a/s;->b:La/e/a/c/g;

    iget-object v7, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    if-ne v6, v7, :cond_e

    iget-boolean v6, v5, La/e/a/c/a/s;->g:Z

    if-nez v6, :cond_e

    goto :goto_3

    :cond_e
    invoke-virtual {v5}, La/e/a/c/a/s;->b()V

    goto :goto_3

    :cond_f
    iget-object v4, p0, La/e/a/c/a/e;->e:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_10
    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_14

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/c/a/s;

    if-nez p1, :cond_11

    iget-object v6, v5, La/e/a/c/a/s;->b:La/e/a/c/g;

    iget-object v7, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    if-ne v6, v7, :cond_11

    goto :goto_4

    :cond_11
    iget-object v6, v5, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget-boolean v6, v6, La/e/a/c/a/f;->j:Z

    if-nez v6, :cond_12

    goto :goto_5

    :cond_12
    iget-object v6, v5, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget-boolean v6, v6, La/e/a/c/a/f;->j:Z

    if-nez v6, :cond_13

    instance-of v6, v5, La/e/a/c/a/j;

    if-nez v6, :cond_13

    goto :goto_5

    :cond_13
    iget-object v6, v5, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget-boolean v6, v6, La/e/a/c/a/f;->j:Z

    if-nez v6, :cond_10

    instance-of v6, v5, La/e/a/c/a/c;

    if-nez v6, :cond_10

    instance-of v5, v5, La/e/a/c/a/j;

    if-nez v5, :cond_10

    goto :goto_5

    :cond_14
    move v2, v0

    :goto_5
    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-virtual {p1, v1}, La/e/a/c/g;->a(La/e/a/c/g$a;)V

    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-virtual {p1, v3}, La/e/a/c/g;->b(La/e/a/c/g$a;)V

    return v2
.end method

.method public a(ZI)Z
    .locals 9

    const/4 v0, 0x1

    and-int/2addr p1, v0

    iget-object v1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, La/e/a/c/g;->b(I)La/e/a/c/g$a;

    move-result-object v1

    iget-object v3, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-virtual {v3, v0}, La/e/a/c/g;->b(I)La/e/a/c/g$a;

    move-result-object v3

    iget-object v4, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-virtual {v4}, La/e/a/c/g;->D()I

    move-result v4

    iget-object v5, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-virtual {v5}, La/e/a/c/g;->E()I

    move-result v5

    if-eqz p1, :cond_4

    sget-object v6, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-eq v1, v6, :cond_0

    if-ne v3, v6, :cond_4

    :cond_0
    iget-object v6, p0, La/e/a/c/a/e;->e:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, La/e/a/c/a/s;

    iget v8, v7, La/e/a/c/a/s;->f:I

    if-ne v8, p2, :cond_1

    invoke-virtual {v7}, La/e/a/c/a/s;->f()Z

    move-result v7

    if-nez v7, :cond_1

    move p1, v2

    :cond_2
    if-nez p2, :cond_3

    if-eqz p1, :cond_4

    sget-object p1, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v1, p1, :cond_4

    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    sget-object v6, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    invoke-virtual {p1, v6}, La/e/a/c/g;->a(La/e/a/c/g$a;)V

    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-direct {p0, p1, v2}, La/e/a/c/a/e;->a(La/e/a/c/h;I)I

    move-result v6

    invoke-virtual {p1, v6}, La/e/a/c/g;->u(I)V

    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object v6, p1, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v6, v6, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {p1}, La/e/a/c/g;->C()I

    move-result p1

    invoke-virtual {v6, p1}, La/e/a/c/a/g;->a(I)V

    goto :goto_0

    :cond_3
    if-eqz p1, :cond_4

    sget-object p1, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v3, p1, :cond_4

    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    sget-object v6, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    invoke-virtual {p1, v6}, La/e/a/c/g;->b(La/e/a/c/g$a;)V

    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-direct {p0, p1, v0}, La/e/a/c/a/e;->a(La/e/a/c/h;I)I

    move-result v6

    invoke-virtual {p1, v6}, La/e/a/c/g;->m(I)V

    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object v6, p1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v6, v6, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {p1}, La/e/a/c/g;->k()I

    move-result p1

    invoke-virtual {v6, p1}, La/e/a/c/a/g;->a(I)V

    :cond_4
    :goto_0
    if-nez p2, :cond_6

    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object p1, p1, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object v5, p1, v2

    sget-object v6, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    if-eq v5, v6, :cond_5

    aget-object p1, p1, v2

    sget-object v5, La/e/a/c/g$a;->d:La/e/a/c/g$a;

    if-ne p1, v5, :cond_7

    :cond_5
    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-virtual {p1}, La/e/a/c/g;->C()I

    move-result p1

    add-int/2addr p1, v4

    iget-object v5, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object v5, v5, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v5, v5, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    invoke-virtual {v5, p1}, La/e/a/c/a/f;->a(I)V

    iget-object v5, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object v5, v5, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v5, v5, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    sub-int/2addr p1, v4

    invoke-virtual {v5, p1}, La/e/a/c/a/g;->a(I)V

    goto :goto_2

    :cond_6
    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object p1, p1, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object v4, p1, v0

    sget-object v6, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    if-eq v4, v6, :cond_8

    aget-object p1, p1, v0

    sget-object v4, La/e/a/c/g$a;->d:La/e/a/c/g$a;

    if-ne p1, v4, :cond_7

    goto :goto_1

    :cond_7
    move p1, v2

    goto :goto_3

    :cond_8
    :goto_1
    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-virtual {p1}, La/e/a/c/g;->k()I

    move-result p1

    add-int/2addr p1, v5

    iget-object v4, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object v4, v4, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v4, v4, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    invoke-virtual {v4, p1}, La/e/a/c/a/f;->a(I)V

    iget-object v4, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object v4, v4, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v4, v4, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    sub-int/2addr p1, v5

    invoke-virtual {v4, p1}, La/e/a/c/a/g;->a(I)V

    :goto_2
    move p1, v0

    :goto_3
    invoke-virtual {p0}, La/e/a/c/a/e;->d()V

    iget-object v4, p0, La/e/a/c/a/e;->e:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/c/a/s;

    iget v6, v5, La/e/a/c/a/s;->f:I

    if-eq v6, p2, :cond_9

    goto :goto_4

    :cond_9
    iget-object v6, v5, La/e/a/c/a/s;->b:La/e/a/c/g;

    iget-object v7, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    if-ne v6, v7, :cond_a

    iget-boolean v6, v5, La/e/a/c/a/s;->g:Z

    if-nez v6, :cond_a

    goto :goto_4

    :cond_a
    invoke-virtual {v5}, La/e/a/c/a/s;->b()V

    goto :goto_4

    :cond_b
    iget-object v4, p0, La/e/a/c/a/e;->e:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_c
    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_11

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/c/a/s;

    iget v6, v5, La/e/a/c/a/s;->f:I

    if-eq v6, p2, :cond_d

    goto :goto_5

    :cond_d
    if-nez p1, :cond_e

    iget-object v6, v5, La/e/a/c/a/s;->b:La/e/a/c/g;

    iget-object v7, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    if-ne v6, v7, :cond_e

    goto :goto_5

    :cond_e
    iget-object v6, v5, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget-boolean v6, v6, La/e/a/c/a/f;->j:Z

    if-nez v6, :cond_f

    goto :goto_6

    :cond_f
    iget-object v6, v5, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget-boolean v6, v6, La/e/a/c/a/f;->j:Z

    if-nez v6, :cond_10

    goto :goto_6

    :cond_10
    instance-of v6, v5, La/e/a/c/a/c;

    if-nez v6, :cond_c

    iget-object v5, v5, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget-boolean v5, v5, La/e/a/c/a/f;->j:Z

    if-nez v5, :cond_c

    goto :goto_6

    :cond_11
    move v2, v0

    :goto_6
    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-virtual {p1, v1}, La/e/a/c/g;->a(La/e/a/c/g$a;)V

    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-virtual {p1, v3}, La/e/a/c/g;->b(La/e/a/c/g$a;)V

    return v2
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, La/e/a/c/a/e;->b:Z

    return-void
.end method

.method public b(Z)Z
    .locals 4

    iget-boolean p1, p0, La/e/a/c/a/e;->b:Z

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object p1, p1, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La/e/a/c/g;

    invoke-virtual {v1}, La/e/a/c/g;->d()V

    iput-boolean v0, v1, La/e/a/c/g;->b:Z

    iget-object v2, v1, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v3, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iput-boolean v0, v3, La/e/a/c/a/f;->j:Z

    iput-boolean v0, v2, La/e/a/c/a/s;->g:Z

    invoke-virtual {v2}, La/e/a/c/a/m;->g()V

    iget-object v1, v1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v2, v1, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iput-boolean v0, v2, La/e/a/c/a/f;->j:Z

    iput-boolean v0, v1, La/e/a/c/a/s;->g:Z

    invoke-virtual {v1}, La/e/a/c/a/p;->g()V

    goto :goto_0

    :cond_0
    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-virtual {p1}, La/e/a/c/g;->d()V

    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iput-boolean v0, p1, La/e/a/c/g;->b:Z

    iget-object p1, p1, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v1, p1, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iput-boolean v0, v1, La/e/a/c/a/f;->j:Z

    iput-boolean v0, p1, La/e/a/c/a/s;->g:Z

    invoke-virtual {p1}, La/e/a/c/a/m;->g()V

    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object p1, p1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v1, p1, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iput-boolean v0, v1, La/e/a/c/a/f;->j:Z

    iput-boolean v0, p1, La/e/a/c/a/s;->g:Z

    invoke-virtual {p1}, La/e/a/c/a/p;->g()V

    invoke-virtual {p0}, La/e/a/c/a/e;->a()V

    :cond_1
    iget-object p1, p0, La/e/a/c/a/e;->d:La/e/a/c/h;

    invoke-direct {p0, p1}, La/e/a/c/a/e;->a(La/e/a/c/h;)Z

    move-result p1

    if-eqz p1, :cond_2

    return v0

    :cond_2
    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-virtual {p1, v0}, La/e/a/c/g;->w(I)V

    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    invoke-virtual {p1, v0}, La/e/a/c/g;->x(I)V

    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object p1, p1, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object p1, p1, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    invoke-virtual {p1, v0}, La/e/a/c/a/f;->a(I)V

    iget-object p1, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object p1, p1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object p1, p1, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    invoke-virtual {p1, v0}, La/e/a/c/a/f;->a(I)V

    const/4 p1, 0x1

    return p1
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, La/e/a/c/a/e;->c:Z

    return-void
.end method

.method public d()V
    .locals 11

    iget-object v0, p0, La/e/a/c/a/e;->a:La/e/a/c/h;

    iget-object v0, v0, La/e/a/c/q;->Ua:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La/e/a/c/g;

    iget-boolean v2, v1, La/e/a/c/g;->b:Z

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    iget-object v2, v1, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    const/4 v3, 0x0

    aget-object v8, v2, v3

    const/4 v9, 0x1

    aget-object v10, v2, v9

    iget v2, v1, La/e/a/c/g;->x:I

    iget v4, v1, La/e/a/c/g;->y:I

    sget-object v5, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-eq v8, v5, :cond_3

    sget-object v5, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v8, v5, :cond_2

    if-ne v2, v9, :cond_2

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2

    :cond_3
    :goto_1
    move v2, v9

    :goto_2
    sget-object v5, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-eq v10, v5, :cond_4

    sget-object v5, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v10, v5, :cond_5

    if-ne v4, v9, :cond_5

    :cond_4
    move v3, v9

    :cond_5
    iget-object v4, v1, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v4, v4, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget-boolean v5, v4, La/e/a/c/a/f;->j:Z

    iget-object v6, v1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v6, v6, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget-boolean v7, v6, La/e/a/c/a/f;->j:Z

    if-eqz v5, :cond_6

    if-eqz v7, :cond_6

    sget-object v7, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    iget v5, v4, La/e/a/c/a/f;->g:I

    iget v8, v6, La/e/a/c/a/f;->g:I

    move-object v2, p0

    move-object v3, v1

    move-object v4, v7

    move-object v6, v7

    move v7, v8

    invoke-direct/range {v2 .. v7}, La/e/a/c/a/e;->a(La/e/a/c/g;La/e/a/c/g$a;ILa/e/a/c/g$a;I)V

    iput-boolean v9, v1, La/e/a/c/g;->b:Z

    goto/16 :goto_3

    :cond_6
    if-eqz v5, :cond_8

    if-eqz v3, :cond_8

    sget-object v4, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    iget-object v2, v1, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget v5, v2, La/e/a/c/a/f;->g:I

    sget-object v6, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    iget-object v2, v1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget v7, v2, La/e/a/c/a/f;->g:I

    move-object v2, p0

    move-object v3, v1

    invoke-direct/range {v2 .. v7}, La/e/a/c/a/e;->a(La/e/a/c/g;La/e/a/c/g$a;ILa/e/a/c/g$a;I)V

    sget-object v2, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v10, v2, :cond_7

    iget-object v2, v1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v1}, La/e/a/c/g;->k()I

    move-result v3

    iput v3, v2, La/e/a/c/a/g;->m:I

    goto :goto_3

    :cond_7
    iget-object v2, v1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v1}, La/e/a/c/g;->k()I

    move-result v3

    invoke-virtual {v2, v3}, La/e/a/c/a/g;->a(I)V

    iput-boolean v9, v1, La/e/a/c/g;->b:Z

    goto :goto_3

    :cond_8
    if-eqz v7, :cond_a

    if-eqz v2, :cond_a

    sget-object v4, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    iget-object v2, v1, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget v5, v2, La/e/a/c/a/f;->g:I

    sget-object v6, La/e/a/c/g$a;->a:La/e/a/c/g$a;

    iget-object v2, v1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget v7, v2, La/e/a/c/a/f;->g:I

    move-object v2, p0

    move-object v3, v1

    invoke-direct/range {v2 .. v7}, La/e/a/c/a/e;->a(La/e/a/c/g;La/e/a/c/g$a;ILa/e/a/c/g$a;I)V

    sget-object v2, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v8, v2, :cond_9

    iget-object v2, v1, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v1}, La/e/a/c/g;->C()I

    move-result v3

    iput v3, v2, La/e/a/c/a/g;->m:I

    goto :goto_3

    :cond_9
    iget-object v2, v1, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v2, v2, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v1}, La/e/a/c/g;->C()I

    move-result v3

    invoke-virtual {v2, v3}, La/e/a/c/a/g;->a(I)V

    iput-boolean v9, v1, La/e/a/c/g;->b:Z

    :cond_a
    :goto_3
    iget-boolean v2, v1, La/e/a/c/g;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, v1, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v2, v2, La/e/a/c/a/p;->l:La/e/a/c/a/g;

    if-eqz v2, :cond_0

    invoke-virtual {v1}, La/e/a/c/g;->e()I

    move-result v1

    invoke-virtual {v2, v1}, La/e/a/c/a/g;->a(I)V

    goto/16 :goto_0

    :cond_b
    return-void
.end method
