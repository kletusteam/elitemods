.class public La/e/a/c/a;
.super La/e/a/c/m;


# instance fields
.field private Wa:I

.field private Xa:Z

.field private Ya:I

.field Za:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, La/e/a/c/m;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, La/e/a/c/a;->Wa:I

    const/4 v1, 0x1

    iput-boolean v1, p0, La/e/a/c/a;->Xa:Z

    iput v0, p0, La/e/a/c/a;->Ya:I

    iput-boolean v0, p0, La/e/a/c/a;->Za:Z

    return-void
.end method


# virtual methods
.method public A(I)V
    .locals 0

    iput p1, p0, La/e/a/c/a;->Ya:I

    return-void
.end method

.method public O()Z
    .locals 1

    iget-boolean v0, p0, La/e/a/c/a;->Za:Z

    return v0
.end method

.method public P()Z
    .locals 1

    iget-boolean v0, p0, La/e/a/c/a;->Za:Z

    return v0
.end method

.method public Y()Z
    .locals 8

    const/4 v0, 0x0

    const/4 v1, 0x1

    move v2, v0

    move v3, v1

    :goto_0
    iget v4, p0, La/e/a/c/m;->Va:I

    const/4 v5, 0x3

    const/4 v6, 0x2

    if-ge v2, v4, :cond_5

    iget-object v4, p0, La/e/a/c/m;->Ua:[La/e/a/c/g;

    aget-object v4, v4, v2

    iget-boolean v7, p0, La/e/a/c/a;->Xa:Z

    if-nez v7, :cond_0

    invoke-virtual {v4}, La/e/a/c/g;->c()Z

    move-result v7

    if-nez v7, :cond_0

    goto :goto_2

    :cond_0
    iget v7, p0, La/e/a/c/a;->Wa:I

    if-eqz v7, :cond_1

    if-ne v7, v1, :cond_2

    :cond_1
    invoke-virtual {v4}, La/e/a/c/g;->O()Z

    move-result v7

    if-nez v7, :cond_2

    :goto_1
    move v3, v0

    goto :goto_2

    :cond_2
    iget v7, p0, La/e/a/c/a;->Wa:I

    if-eq v7, v6, :cond_3

    if-ne v7, v5, :cond_4

    :cond_3
    invoke-virtual {v4}, La/e/a/c/g;->P()Z

    move-result v4

    if-nez v4, :cond_4

    goto :goto_1

    :cond_4
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_5
    if-eqz v3, :cond_13

    if-lez v4, :cond_13

    move v2, v0

    move v3, v2

    :goto_3
    iget v4, p0, La/e/a/c/m;->Va:I

    if-ge v0, v4, :cond_10

    iget-object v4, p0, La/e/a/c/m;->Ua:[La/e/a/c/g;

    aget-object v4, v4, v0

    iget-boolean v7, p0, La/e/a/c/a;->Xa:Z

    if-nez v7, :cond_6

    invoke-virtual {v4}, La/e/a/c/g;->c()Z

    move-result v7

    if-nez v7, :cond_6

    goto/16 :goto_5

    :cond_6
    if-nez v3, :cond_b

    iget v3, p0, La/e/a/c/a;->Wa:I

    if-nez v3, :cond_7

    sget-object v2, La/e/a/c/e$a;->b:La/e/a/c/e$a;

    invoke-virtual {v4, v2}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v2

    invoke-virtual {v2}, La/e/a/c/e;->b()I

    move-result v2

    goto :goto_4

    :cond_7
    if-ne v3, v1, :cond_8

    sget-object v2, La/e/a/c/e$a;->d:La/e/a/c/e$a;

    invoke-virtual {v4, v2}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v2

    invoke-virtual {v2}, La/e/a/c/e;->b()I

    move-result v2

    goto :goto_4

    :cond_8
    if-ne v3, v6, :cond_9

    sget-object v2, La/e/a/c/e$a;->c:La/e/a/c/e$a;

    invoke-virtual {v4, v2}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v2

    invoke-virtual {v2}, La/e/a/c/e;->b()I

    move-result v2

    goto :goto_4

    :cond_9
    if-ne v3, v5, :cond_a

    sget-object v2, La/e/a/c/e$a;->e:La/e/a/c/e$a;

    invoke-virtual {v4, v2}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v2

    invoke-virtual {v2}, La/e/a/c/e;->b()I

    move-result v2

    :cond_a
    :goto_4
    move v3, v1

    :cond_b
    iget v7, p0, La/e/a/c/a;->Wa:I

    if-nez v7, :cond_c

    sget-object v7, La/e/a/c/e$a;->b:La/e/a/c/e$a;

    invoke-virtual {v4, v7}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v4

    invoke-virtual {v4}, La/e/a/c/e;->b()I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    goto :goto_5

    :cond_c
    if-ne v7, v1, :cond_d

    sget-object v7, La/e/a/c/e$a;->d:La/e/a/c/e$a;

    invoke-virtual {v4, v7}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v4

    invoke-virtual {v4}, La/e/a/c/e;->b()I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto :goto_5

    :cond_d
    if-ne v7, v6, :cond_e

    sget-object v7, La/e/a/c/e$a;->c:La/e/a/c/e$a;

    invoke-virtual {v4, v7}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v4

    invoke-virtual {v4}, La/e/a/c/e;->b()I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    goto :goto_5

    :cond_e
    if-ne v7, v5, :cond_f

    sget-object v7, La/e/a/c/e$a;->e:La/e/a/c/e$a;

    invoke-virtual {v4, v7}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v4

    invoke-virtual {v4}, La/e/a/c/e;->b()I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    :cond_f
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_3

    :cond_10
    iget v0, p0, La/e/a/c/a;->Ya:I

    add-int/2addr v2, v0

    iget v0, p0, La/e/a/c/a;->Wa:I

    if-eqz v0, :cond_12

    if-ne v0, v1, :cond_11

    goto :goto_6

    :cond_11
    invoke-virtual {p0, v2, v2}, La/e/a/c/g;->c(II)V

    goto :goto_7

    :cond_12
    :goto_6
    invoke-virtual {p0, v2, v2}, La/e/a/c/g;->b(II)V

    :goto_7
    iput-boolean v1, p0, La/e/a/c/a;->Za:Z

    return v1

    :cond_13
    return v0
.end method

.method public Z()Z
    .locals 1

    iget-boolean v0, p0, La/e/a/c/a;->Xa:Z

    return v0
.end method

.method public a(La/e/a/c/g;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La/e/a/c/g;",
            "Ljava/util/HashMap<",
            "La/e/a/c/g;",
            "La/e/a/c/g;",
            ">;)V"
        }
    .end annotation

    invoke-super {p0, p1, p2}, La/e/a/c/m;->a(La/e/a/c/g;Ljava/util/HashMap;)V

    check-cast p1, La/e/a/c/a;

    iget p2, p1, La/e/a/c/a;->Wa:I

    iput p2, p0, La/e/a/c/a;->Wa:I

    iget-boolean p2, p1, La/e/a/c/a;->Xa:Z

    iput-boolean p2, p0, La/e/a/c/a;->Xa:Z

    iget p1, p1, La/e/a/c/a;->Ya:I

    iput p1, p0, La/e/a/c/a;->Ya:I

    return-void
.end method

.method public a(La/e/a/d;Z)V
    .locals 12

    iget-object p2, p0, La/e/a/c/g;->Z:[La/e/a/c/e;

    iget-object v0, p0, La/e/a/c/g;->R:La/e/a/c/e;

    const/4 v1, 0x0

    aput-object v0, p2, v1

    iget-object v0, p0, La/e/a/c/g;->S:La/e/a/c/e;

    const/4 v2, 0x2

    aput-object v0, p2, v2

    iget-object v0, p0, La/e/a/c/g;->T:La/e/a/c/e;

    const/4 v3, 0x1

    aput-object v0, p2, v3

    iget-object v0, p0, La/e/a/c/g;->U:La/e/a/c/e;

    const/4 v4, 0x3

    aput-object v0, p2, v4

    move p2, v1

    :goto_0
    iget-object v0, p0, La/e/a/c/g;->Z:[La/e/a/c/e;

    array-length v5, v0

    if-ge p2, v5, :cond_0

    aget-object v5, v0, p2

    aget-object v0, v0, p2

    invoke-virtual {p1, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v0

    iput-object v0, v5, La/e/a/c/e;->i:La/e/a/j;

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    iget p2, p0, La/e/a/c/a;->Wa:I

    if-ltz p2, :cond_1f

    const/4 v5, 0x4

    if-ge p2, v5, :cond_1f

    aget-object p2, v0, p2

    iget-boolean v0, p0, La/e/a/c/a;->Za:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, La/e/a/c/a;->Y()Z

    :cond_1
    iget-boolean v0, p0, La/e/a/c/a;->Za:Z

    if-eqz v0, :cond_6

    iput-boolean v1, p0, La/e/a/c/a;->Za:Z

    iget p2, p0, La/e/a/c/a;->Wa:I

    if-eqz p2, :cond_4

    if-ne p2, v3, :cond_2

    goto :goto_1

    :cond_2
    if-eq p2, v2, :cond_3

    if-ne p2, v4, :cond_5

    :cond_3
    iget-object p2, p0, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object p2, p2, La/e/a/c/e;->i:La/e/a/j;

    iget v0, p0, La/e/a/c/g;->ja:I

    invoke-virtual {p1, p2, v0}, La/e/a/d;->a(La/e/a/j;I)V

    iget-object p2, p0, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object p2, p2, La/e/a/c/e;->i:La/e/a/j;

    iget v0, p0, La/e/a/c/g;->ja:I

    invoke-virtual {p1, p2, v0}, La/e/a/d;->a(La/e/a/j;I)V

    goto :goto_2

    :cond_4
    :goto_1
    iget-object p2, p0, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object p2, p2, La/e/a/c/e;->i:La/e/a/j;

    iget v0, p0, La/e/a/c/g;->ia:I

    invoke-virtual {p1, p2, v0}, La/e/a/d;->a(La/e/a/j;I)V

    iget-object p2, p0, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object p2, p2, La/e/a/c/e;->i:La/e/a/j;

    iget v0, p0, La/e/a/c/g;->ia:I

    invoke-virtual {p1, p2, v0}, La/e/a/d;->a(La/e/a/j;I)V

    :cond_5
    :goto_2
    return-void

    :cond_6
    move v0, v1

    :goto_3
    iget v6, p0, La/e/a/c/m;->Va:I

    if-ge v0, v6, :cond_c

    iget-object v6, p0, La/e/a/c/m;->Ua:[La/e/a/c/g;

    aget-object v6, v6, v0

    iget-boolean v7, p0, La/e/a/c/a;->Xa:Z

    if-nez v7, :cond_7

    invoke-virtual {v6}, La/e/a/c/g;->c()Z

    move-result v7

    if-nez v7, :cond_7

    goto :goto_5

    :cond_7
    iget v7, p0, La/e/a/c/a;->Wa:I

    if-eqz v7, :cond_8

    if-ne v7, v3, :cond_9

    :cond_8
    invoke-virtual {v6}, La/e/a/c/g;->n()La/e/a/c/g$a;

    move-result-object v7

    sget-object v8, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v7, v8, :cond_9

    iget-object v7, v6, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v7, v7, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v7, :cond_9

    iget-object v7, v6, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v7, v7, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v7, :cond_9

    :goto_4
    move v0, v3

    goto :goto_6

    :cond_9
    iget v7, p0, La/e/a/c/a;->Wa:I

    if-eq v7, v2, :cond_a

    if-ne v7, v4, :cond_b

    :cond_a
    invoke-virtual {v6}, La/e/a/c/g;->z()La/e/a/c/g$a;

    move-result-object v7

    sget-object v8, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v7, v8, :cond_b

    iget-object v7, v6, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v7, v7, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v7, :cond_b

    iget-object v6, v6, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object v6, v6, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v6, :cond_b

    goto :goto_4

    :cond_b
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_c
    move v0, v1

    :goto_6
    iget-object v6, p0, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {v6}, La/e/a/c/e;->i()Z

    move-result v6

    if-nez v6, :cond_e

    iget-object v6, p0, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {v6}, La/e/a/c/e;->i()Z

    move-result v6

    if-eqz v6, :cond_d

    goto :goto_7

    :cond_d
    move v6, v1

    goto :goto_8

    :cond_e
    :goto_7
    move v6, v3

    :goto_8
    iget-object v7, p0, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {v7}, La/e/a/c/e;->i()Z

    move-result v7

    if-nez v7, :cond_10

    iget-object v7, p0, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {v7}, La/e/a/c/e;->i()Z

    move-result v7

    if-eqz v7, :cond_f

    goto :goto_9

    :cond_f
    move v7, v1

    goto :goto_a

    :cond_10
    :goto_9
    move v7, v3

    :goto_a
    if-nez v0, :cond_15

    iget v8, p0, La/e/a/c/a;->Wa:I

    if-nez v8, :cond_11

    if-nez v6, :cond_14

    :cond_11
    iget v8, p0, La/e/a/c/a;->Wa:I

    if-ne v8, v2, :cond_12

    if-nez v7, :cond_14

    :cond_12
    iget v8, p0, La/e/a/c/a;->Wa:I

    if-ne v8, v3, :cond_13

    if-nez v6, :cond_14

    :cond_13
    iget v6, p0, La/e/a/c/a;->Wa:I

    if-ne v6, v4, :cond_15

    if-eqz v7, :cond_15

    :cond_14
    move v6, v3

    goto :goto_b

    :cond_15
    move v6, v1

    :goto_b
    const/4 v7, 0x5

    if-nez v6, :cond_16

    move v7, v5

    :cond_16
    move v6, v1

    :goto_c
    iget v8, p0, La/e/a/c/m;->Va:I

    if-ge v6, v8, :cond_1b

    iget-object v8, p0, La/e/a/c/m;->Ua:[La/e/a/c/g;

    aget-object v8, v8, v6

    iget-boolean v9, p0, La/e/a/c/a;->Xa:Z

    if-nez v9, :cond_17

    invoke-virtual {v8}, La/e/a/c/g;->c()Z

    move-result v9

    if-nez v9, :cond_17

    goto :goto_10

    :cond_17
    iget-object v9, v8, La/e/a/c/g;->Z:[La/e/a/c/e;

    iget v10, p0, La/e/a/c/a;->Wa:I

    aget-object v9, v9, v10

    invoke-virtual {p1, v9}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v9

    iget-object v8, v8, La/e/a/c/g;->Z:[La/e/a/c/e;

    iget v10, p0, La/e/a/c/a;->Wa:I

    aget-object v11, v8, v10

    iput-object v9, v11, La/e/a/c/e;->i:La/e/a/j;

    aget-object v11, v8, v10

    iget-object v11, v11, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v11, :cond_18

    aget-object v11, v8, v10

    iget-object v11, v11, La/e/a/c/e;->f:La/e/a/c/e;

    iget-object v11, v11, La/e/a/c/e;->d:La/e/a/c/g;

    if-ne v11, p0, :cond_18

    aget-object v8, v8, v10

    iget v8, v8, La/e/a/c/e;->g:I

    add-int/2addr v8, v1

    goto :goto_d

    :cond_18
    move v8, v1

    :goto_d
    iget v10, p0, La/e/a/c/a;->Wa:I

    if-eqz v10, :cond_1a

    if-ne v10, v2, :cond_19

    goto :goto_e

    :cond_19
    iget-object v10, p2, La/e/a/c/e;->i:La/e/a/j;

    iget v11, p0, La/e/a/c/a;->Ya:I

    add-int/2addr v11, v8

    invoke-virtual {p1, v10, v9, v11, v0}, La/e/a/d;->a(La/e/a/j;La/e/a/j;IZ)V

    goto :goto_f

    :cond_1a
    :goto_e
    iget-object v10, p2, La/e/a/c/e;->i:La/e/a/j;

    iget v11, p0, La/e/a/c/a;->Ya:I

    sub-int/2addr v11, v8

    invoke-virtual {p1, v10, v9, v11, v0}, La/e/a/d;->b(La/e/a/j;La/e/a/j;IZ)V

    :goto_f
    iget-object v10, p2, La/e/a/c/e;->i:La/e/a/j;

    iget v11, p0, La/e/a/c/a;->Ya:I

    add-int/2addr v11, v8

    invoke-virtual {p1, v10, v9, v11, v7}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    :goto_10
    add-int/lit8 v6, v6, 0x1

    goto :goto_c

    :cond_1b
    iget p2, p0, La/e/a/c/a;->Wa:I

    const/16 v0, 0x8

    if-nez p2, :cond_1c

    iget-object p2, p0, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object p2, p2, La/e/a/c/e;->i:La/e/a/j;

    iget-object v2, p0, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v2, v2, La/e/a/c/e;->i:La/e/a/j;

    invoke-virtual {p1, p2, v2, v1, v0}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    iget-object p2, p0, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object p2, p2, La/e/a/c/e;->i:La/e/a/j;

    iget-object v0, p0, La/e/a/c/g;->da:La/e/a/c/g;

    iget-object v0, v0, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v0, v0, La/e/a/c/e;->i:La/e/a/j;

    invoke-virtual {p1, p2, v0, v1, v5}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    iget-object p2, p0, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object p2, p2, La/e/a/c/e;->i:La/e/a/j;

    iget-object v0, p0, La/e/a/c/g;->da:La/e/a/c/g;

    iget-object v0, v0, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v0, v0, La/e/a/c/e;->i:La/e/a/j;

    invoke-virtual {p1, p2, v0, v1, v1}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    goto/16 :goto_11

    :cond_1c
    if-ne p2, v3, :cond_1d

    iget-object p2, p0, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object p2, p2, La/e/a/c/e;->i:La/e/a/j;

    iget-object v2, p0, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v2, v2, La/e/a/c/e;->i:La/e/a/j;

    invoke-virtual {p1, p2, v2, v1, v0}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    iget-object p2, p0, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object p2, p2, La/e/a/c/e;->i:La/e/a/j;

    iget-object v0, p0, La/e/a/c/g;->da:La/e/a/c/g;

    iget-object v0, v0, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object v0, v0, La/e/a/c/e;->i:La/e/a/j;

    invoke-virtual {p1, p2, v0, v1, v5}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    iget-object p2, p0, La/e/a/c/g;->R:La/e/a/c/e;

    iget-object p2, p2, La/e/a/c/e;->i:La/e/a/j;

    iget-object v0, p0, La/e/a/c/g;->da:La/e/a/c/g;

    iget-object v0, v0, La/e/a/c/g;->T:La/e/a/c/e;

    iget-object v0, v0, La/e/a/c/e;->i:La/e/a/j;

    invoke-virtual {p1, p2, v0, v1, v1}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    goto :goto_11

    :cond_1d
    if-ne p2, v2, :cond_1e

    iget-object p2, p0, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object p2, p2, La/e/a/c/e;->i:La/e/a/j;

    iget-object v2, p0, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v2, v2, La/e/a/c/e;->i:La/e/a/j;

    invoke-virtual {p1, p2, v2, v1, v0}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    iget-object p2, p0, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object p2, p2, La/e/a/c/e;->i:La/e/a/j;

    iget-object v0, p0, La/e/a/c/g;->da:La/e/a/c/g;

    iget-object v0, v0, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object v0, v0, La/e/a/c/e;->i:La/e/a/j;

    invoke-virtual {p1, p2, v0, v1, v5}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    iget-object p2, p0, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object p2, p2, La/e/a/c/e;->i:La/e/a/j;

    iget-object v0, p0, La/e/a/c/g;->da:La/e/a/c/g;

    iget-object v0, v0, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v0, v0, La/e/a/c/e;->i:La/e/a/j;

    invoke-virtual {p1, p2, v0, v1, v1}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    goto :goto_11

    :cond_1e
    if-ne p2, v4, :cond_1f

    iget-object p2, p0, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object p2, p2, La/e/a/c/e;->i:La/e/a/j;

    iget-object v2, p0, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object v2, v2, La/e/a/c/e;->i:La/e/a/j;

    invoke-virtual {p1, p2, v2, v1, v0}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    iget-object p2, p0, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object p2, p2, La/e/a/c/e;->i:La/e/a/j;

    iget-object v0, p0, La/e/a/c/g;->da:La/e/a/c/g;

    iget-object v0, v0, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object v0, v0, La/e/a/c/e;->i:La/e/a/j;

    invoke-virtual {p1, p2, v0, v1, v5}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    iget-object p2, p0, La/e/a/c/g;->S:La/e/a/c/e;

    iget-object p2, p2, La/e/a/c/e;->i:La/e/a/j;

    iget-object v0, p0, La/e/a/c/g;->da:La/e/a/c/g;

    iget-object v0, v0, La/e/a/c/g;->U:La/e/a/c/e;

    iget-object v0, v0, La/e/a/c/e;->i:La/e/a/j;

    invoke-virtual {p1, p2, v0, v1, v1}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    :cond_1f
    :goto_11
    return-void
.end method

.method public aa()I
    .locals 1

    iget v0, p0, La/e/a/c/a;->Wa:I

    return v0
.end method

.method public ba()I
    .locals 1

    iget v0, p0, La/e/a/c/a;->Ya:I

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public ca()I
    .locals 3

    iget v0, p0, La/e/a/c/a;->Wa:I

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    const/4 v2, 0x3

    if-eq v0, v2, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    return v1

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method protected da()V
    .locals 6

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget v2, p0, La/e/a/c/m;->Va:I

    if-ge v1, v2, :cond_5

    iget-object v2, p0, La/e/a/c/m;->Ua:[La/e/a/c/g;

    aget-object v2, v2, v1

    iget-boolean v3, p0, La/e/a/c/a;->Xa:Z

    if-nez v3, :cond_0

    invoke-virtual {v2}, La/e/a/c/g;->c()Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_2

    :cond_0
    iget v3, p0, La/e/a/c/a;->Wa:I

    const/4 v4, 0x1

    if-eqz v3, :cond_3

    if-ne v3, v4, :cond_1

    goto :goto_1

    :cond_1
    const/4 v5, 0x2

    if-eq v3, v5, :cond_2

    const/4 v5, 0x3

    if-ne v3, v5, :cond_4

    :cond_2
    invoke-virtual {v2, v4, v4}, La/e/a/c/g;->a(IZ)V

    goto :goto_2

    :cond_3
    :goto_1
    invoke-virtual {v2, v0, v4}, La/e/a/c/g;->a(IZ)V

    :cond_4
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_5
    return-void
.end method

.method public e(Z)V
    .locals 0

    iput-boolean p1, p0, La/e/a/c/a;->Xa:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Barrier] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, La/e/a/c/g;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " {"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    iget v2, p0, La/e/a/c/m;->Va:I

    if-ge v1, v2, :cond_1

    iget-object v2, p0, La/e/a/c/m;->Ua:[La/e/a/c/g;

    aget-object v2, v2, v1

    if-lez v1, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, La/e/a/c/g;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public z(I)V
    .locals 0

    iput p1, p0, La/e/a/c/a;->Wa:I

    return-void
.end method
