.class public La/e/a/c/a/q;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La/e/a/c/a/q$a;
    }
.end annotation


# static fields
.field static a:I


# instance fields
.field b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "La/e/a/c/g;",
            ">;"
        }
    .end annotation
.end field

.field c:I

.field d:Z

.field e:I

.field f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "La/e/a/c/a/q$a;",
            ">;"
        }
    .end annotation
.end field

.field private g:I


# direct methods
.method public constructor <init>(I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, La/e/a/c/a/q;->b:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, La/e/a/c/a/q;->c:I

    const/4 v1, 0x0

    iput-boolean v1, p0, La/e/a/c/a/q;->d:Z

    iput v1, p0, La/e/a/c/a/q;->e:I

    const/4 v1, 0x0

    iput-object v1, p0, La/e/a/c/a/q;->f:Ljava/util/ArrayList;

    iput v0, p0, La/e/a/c/a/q;->g:I

    sget v0, La/e/a/c/a/q;->a:I

    add-int/lit8 v1, v0, 0x1

    sput v1, La/e/a/c/a/q;->a:I

    iput v0, p0, La/e/a/c/a/q;->c:I

    iput p1, p0, La/e/a/c/a/q;->e:I

    return-void
.end method

.method private a(La/e/a/d;Ljava/util/ArrayList;I)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La/e/a/d;",
            "Ljava/util/ArrayList<",
            "La/e/a/c/g;",
            ">;I)I"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La/e/a/c/g;

    invoke-virtual {v1}, La/e/a/c/g;->v()La/e/a/c/g;

    move-result-object v1

    check-cast v1, La/e/a/c/h;

    invoke-virtual {p1}, La/e/a/d;->g()V

    invoke-virtual {v1, p1, v0}, La/e/a/c/g;->a(La/e/a/d;Z)V

    move v2, v0

    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, La/e/a/c/g;

    invoke-virtual {v3, p1, v0}, La/e/a/c/g;->a(La/e/a/d;Z)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    if-nez p3, :cond_1

    iget v2, v1, La/e/a/c/h;->fb:I

    if-lez v2, :cond_1

    invoke-static {v1, p1, p2, v0}, La/e/a/c/b;->a(La/e/a/c/h;La/e/a/d;Ljava/util/ArrayList;I)V

    :cond_1
    const/4 v2, 0x1

    if-ne p3, v2, :cond_2

    iget v3, v1, La/e/a/c/h;->gb:I

    if-lez v3, :cond_2

    invoke-static {v1, p1, p2, v2}, La/e/a/c/b;->a(La/e/a/c/h;La/e/a/d;Ljava/util/ArrayList;I)V

    :cond_2
    :try_start_0
    invoke-virtual {p1}, La/e/a/d;->f()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    :goto_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, La/e/a/c/a/q;->f:Ljava/util/ArrayList;

    :goto_2
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, La/e/a/c/g;

    new-instance v3, La/e/a/c/a/q$a;

    invoke-direct {v3, p0, v2, p1, p3}, La/e/a/c/a/q$a;-><init>(La/e/a/c/a/q;La/e/a/c/g;La/e/a/d;I)V

    iget-object v2, p0, La/e/a/c/a/q;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    if-nez p3, :cond_4

    iget-object p2, v1, La/e/a/c/g;->R:La/e/a/c/e;

    invoke-virtual {p1, p2}, La/e/a/d;->b(Ljava/lang/Object;)I

    move-result p2

    iget-object p3, v1, La/e/a/c/g;->T:La/e/a/c/e;

    invoke-virtual {p1, p3}, La/e/a/d;->b(Ljava/lang/Object;)I

    move-result p3

    invoke-virtual {p1}, La/e/a/d;->g()V

    :goto_3
    sub-int/2addr p3, p2

    return p3

    :cond_4
    iget-object p2, v1, La/e/a/c/g;->S:La/e/a/c/e;

    invoke-virtual {p1, p2}, La/e/a/d;->b(Ljava/lang/Object;)I

    move-result p2

    iget-object p3, v1, La/e/a/c/g;->U:La/e/a/c/e;

    invoke-virtual {p1, p3}, La/e/a/d;->b(Ljava/lang/Object;)I

    move-result p3

    invoke-virtual {p1}, La/e/a/d;->g()V

    goto :goto_3
.end method

.method private c()Ljava/lang/String;
    .locals 2

    iget v0, p0, La/e/a/c/a/q;->e:I

    if-nez v0, :cond_0

    const-string v0, "Horizontal"

    return-object v0

    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const-string v0, "Vertical"

    return-object v0

    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    const-string v0, "Both"

    return-object v0

    :cond_2
    const-string v0, "Unknown"

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, La/e/a/c/a/q;->c:I

    return v0
.end method

.method public a(La/e/a/d;I)I
    .locals 1

    iget-object v0, p0, La/e/a/c/a/q;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    iget-object v0, p0, La/e/a/c/a/q;->b:Ljava/util/ArrayList;

    invoke-direct {p0, p1, v0, p2}, La/e/a/c/a/q;->a(La/e/a/d;Ljava/util/ArrayList;I)I

    move-result p1

    return p1
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, La/e/a/c/a/q;->e:I

    return-void
.end method

.method public a(ILa/e/a/c/a/q;)V
    .locals 3

    iget-object v0, p0, La/e/a/c/a/q;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La/e/a/c/g;

    invoke-virtual {p2, v1}, La/e/a/c/a/q;->a(La/e/a/c/g;)Z

    if-nez p1, :cond_0

    invoke-virtual {p2}, La/e/a/c/a/q;->a()I

    move-result v2

    iput v2, v1, La/e/a/c/g;->Sa:I

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, La/e/a/c/a/q;->a()I

    move-result v2

    iput v2, v1, La/e/a/c/g;->Ta:I

    goto :goto_0

    :cond_1
    iget p1, p2, La/e/a/c/a/q;->c:I

    iput p1, p0, La/e/a/c/a/q;->g:I

    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "La/e/a/c/a/q;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, La/e/a/c/a/q;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, La/e/a/c/a/q;->g:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-lez v0, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, La/e/a/c/a/q;

    iget v3, p0, La/e/a/c/a/q;->g:I

    iget v4, v2, La/e/a/c/a/q;->c:I

    if-ne v3, v4, :cond_0

    iget v3, p0, La/e/a/c/a/q;->e:I

    invoke-virtual {p0, v3, v2}, La/e/a/c/a/q;->a(ILa/e/a/c/a/q;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    if-nez v0, :cond_2

    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_2
    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, La/e/a/c/a/q;->d:Z

    return-void
.end method

.method public a(La/e/a/c/g;)Z
    .locals 1

    iget-object v0, p0, La/e/a/c/a/q;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    iget-object v0, p0, La/e/a/c/a/q;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 p1, 0x1

    return p1
.end method

.method public b()I
    .locals 1

    iget v0, p0, La/e/a/c/a/q;->e:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, La/e/a/c/a/q;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, La/e/a/c/a/q;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "] <"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, La/e/a/c/a/q;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, La/e/a/c/g;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, La/e/a/c/g;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " >"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
