.class public La/e/a/c/k;
.super La/e/a/c/g;


# instance fields
.field protected Ua:F

.field protected Va:I

.field protected Wa:I

.field protected Xa:Z

.field private Ya:La/e/a/c/e;

.field private Za:I

.field private _a:I

.field private ab:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    invoke-direct {p0}, La/e/a/c/g;-><init>()V

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, La/e/a/c/k;->Ua:F

    const/4 v0, -0x1

    iput v0, p0, La/e/a/c/k;->Va:I

    iput v0, p0, La/e/a/c/k;->Wa:I

    const/4 v0, 0x1

    iput-boolean v0, p0, La/e/a/c/k;->Xa:Z

    iget-object v0, p0, La/e/a/c/g;->S:La/e/a/c/e;

    iput-object v0, p0, La/e/a/c/k;->Ya:La/e/a/c/e;

    const/4 v0, 0x0

    iput v0, p0, La/e/a/c/k;->Za:I

    iput v0, p0, La/e/a/c/k;->_a:I

    iget-object v1, p0, La/e/a/c/g;->aa:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v1, p0, La/e/a/c/g;->aa:Ljava/util/ArrayList;

    iget-object v2, p0, La/e/a/c/k;->Ya:La/e/a/c/e;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, La/e/a/c/g;->Z:[La/e/a/c/e;

    array-length v1, v1

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, La/e/a/c/g;->Z:[La/e/a/c/e;

    iget-object v3, p0, La/e/a/c/k;->Ya:La/e/a/c/e;

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public A(I)V
    .locals 2

    const/4 v0, -0x1

    if-le p1, v0, :cond_0

    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, La/e/a/c/k;->Ua:F

    iput v0, p0, La/e/a/c/k;->Va:I

    iput p1, p0, La/e/a/c/k;->Wa:I

    :cond_0
    return-void
.end method

.method public B(I)V
    .locals 3

    iget v0, p0, La/e/a/c/k;->Za:I

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iput p1, p0, La/e/a/c/k;->Za:I

    iget-object p1, p0, La/e/a/c/g;->aa:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    iget p1, p0, La/e/a/c/k;->Za:I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    iget-object p1, p0, La/e/a/c/g;->R:La/e/a/c/e;

    iput-object p1, p0, La/e/a/c/k;->Ya:La/e/a/c/e;

    goto :goto_0

    :cond_1
    iget-object p1, p0, La/e/a/c/g;->S:La/e/a/c/e;

    iput-object p1, p0, La/e/a/c/k;->Ya:La/e/a/c/e;

    :goto_0
    iget-object p1, p0, La/e/a/c/g;->aa:Ljava/util/ArrayList;

    iget-object v0, p0, La/e/a/c/k;->Ya:La/e/a/c/e;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, La/e/a/c/g;->Z:[La/e/a/c/e;

    array-length p1, p1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, p1, :cond_2

    iget-object v1, p0, La/e/a/c/g;->Z:[La/e/a/c/e;

    iget-object v2, p0, La/e/a/c/k;->Ya:La/e/a/c/e;

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    return-void
.end method

.method public O()Z
    .locals 1

    iget-boolean v0, p0, La/e/a/c/k;->ab:Z

    return v0
.end method

.method public P()Z
    .locals 1

    iget-boolean v0, p0, La/e/a/c/k;->ab:Z

    return v0
.end method

.method public Y()La/e/a/c/e;
    .locals 1

    iget-object v0, p0, La/e/a/c/k;->Ya:La/e/a/c/e;

    return-object v0
.end method

.method public Z()I
    .locals 1

    iget v0, p0, La/e/a/c/k;->Za:I

    return v0
.end method

.method public a(La/e/a/c/e$a;)La/e/a/c/e;
    .locals 2

    sget-object v0, La/e/a/c/j;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    return-object v0

    :pswitch_1
    iget p1, p0, La/e/a/c/k;->Za:I

    if-nez p1, :cond_0

    iget-object p1, p0, La/e/a/c/k;->Ya:La/e/a/c/e;

    return-object p1

    :pswitch_2
    iget p1, p0, La/e/a/c/k;->Za:I

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    iget-object p1, p0, La/e/a/c/k;->Ya:La/e/a/c/e;

    return-object p1

    :cond_0
    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public a(La/e/a/c/g;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La/e/a/c/g;",
            "Ljava/util/HashMap<",
            "La/e/a/c/g;",
            "La/e/a/c/g;",
            ">;)V"
        }
    .end annotation

    invoke-super {p0, p1, p2}, La/e/a/c/g;->a(La/e/a/c/g;Ljava/util/HashMap;)V

    check-cast p1, La/e/a/c/k;

    iget p2, p1, La/e/a/c/k;->Ua:F

    iput p2, p0, La/e/a/c/k;->Ua:F

    iget p2, p1, La/e/a/c/k;->Va:I

    iput p2, p0, La/e/a/c/k;->Va:I

    iget p2, p1, La/e/a/c/k;->Wa:I

    iput p2, p0, La/e/a/c/k;->Wa:I

    iget-boolean p2, p1, La/e/a/c/k;->Xa:Z

    iput-boolean p2, p0, La/e/a/c/k;->Xa:Z

    iget p1, p1, La/e/a/c/k;->Za:I

    invoke-virtual {p0, p1}, La/e/a/c/k;->B(I)V

    return-void
.end method

.method public a(La/e/a/d;Z)V
    .locals 7

    invoke-virtual {p0}, La/e/a/c/g;->v()La/e/a/c/g;

    move-result-object p2

    check-cast p2, La/e/a/c/h;

    if-nez p2, :cond_0

    return-void

    :cond_0
    sget-object v0, La/e/a/c/e$a;->b:La/e/a/c/e$a;

    invoke-virtual {p2, v0}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v0

    sget-object v1, La/e/a/c/e$a;->d:La/e/a/c/e$a;

    invoke-virtual {p2, v1}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v1

    iget-object v2, p0, La/e/a/c/g;->da:La/e/a/c/g;

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    iget-object v2, v2, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object v2, v2, v4

    sget-object v5, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne v2, v5, :cond_1

    move v2, v3

    goto :goto_0

    :cond_1
    move v2, v4

    :goto_0
    iget v5, p0, La/e/a/c/k;->Za:I

    if-nez v5, :cond_3

    sget-object v0, La/e/a/c/e$a;->c:La/e/a/c/e$a;

    invoke-virtual {p2, v0}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v0

    sget-object v1, La/e/a/c/e$a;->e:La/e/a/c/e$a;

    invoke-virtual {p2, v1}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v1

    iget-object p2, p0, La/e/a/c/g;->da:La/e/a/c/g;

    if-eqz p2, :cond_2

    iget-object p2, p2, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object p2, p2, v3

    sget-object v2, La/e/a/c/g$a;->b:La/e/a/c/g$a;

    if-ne p2, v2, :cond_2

    move v2, v3

    goto :goto_1

    :cond_2
    move v2, v4

    :cond_3
    :goto_1
    iget-boolean p2, p0, La/e/a/c/k;->ab:Z

    const/4 v3, -0x1

    const/4 v5, 0x5

    if-eqz p2, :cond_6

    iget-object p2, p0, La/e/a/c/k;->Ya:La/e/a/c/e;

    invoke-virtual {p2}, La/e/a/c/e;->k()Z

    move-result p2

    if-eqz p2, :cond_6

    iget-object p2, p0, La/e/a/c/k;->Ya:La/e/a/c/e;

    invoke-virtual {p1, p2}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object p2

    iget-object v6, p0, La/e/a/c/k;->Ya:La/e/a/c/e;

    invoke-virtual {v6}, La/e/a/c/e;->b()I

    move-result v6

    invoke-virtual {p1, p2, v6}, La/e/a/d;->a(La/e/a/j;I)V

    iget v6, p0, La/e/a/c/k;->Va:I

    if-eq v6, v3, :cond_4

    if-eqz v2, :cond_5

    invoke-virtual {p1, v1}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v0

    invoke-virtual {p1, v0, p2, v4, v5}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    goto :goto_2

    :cond_4
    iget v6, p0, La/e/a/c/k;->Wa:I

    if-eq v6, v3, :cond_5

    if-eqz v2, :cond_5

    invoke-virtual {p1, v1}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v1

    invoke-virtual {p1, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v0

    invoke-virtual {p1, p2, v0, v4, v5}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    invoke-virtual {p1, v1, p2, v4, v5}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    :cond_5
    :goto_2
    iput-boolean v4, p0, La/e/a/c/k;->ab:Z

    return-void

    :cond_6
    iget p2, p0, La/e/a/c/k;->Va:I

    const/16 v6, 0x8

    if-eq p2, v3, :cond_7

    iget-object p2, p0, La/e/a/c/k;->Ya:La/e/a/c/e;

    invoke-virtual {p1, p2}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object p2

    invoke-virtual {p1, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v0

    iget v3, p0, La/e/a/c/k;->Va:I

    invoke-virtual {p1, p2, v0, v3, v6}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    if-eqz v2, :cond_9

    invoke-virtual {p1, v1}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v0

    invoke-virtual {p1, v0, p2, v4, v5}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    goto :goto_3

    :cond_7
    iget p2, p0, La/e/a/c/k;->Wa:I

    if-eq p2, v3, :cond_8

    iget-object p2, p0, La/e/a/c/k;->Ya:La/e/a/c/e;

    invoke-virtual {p1, p2}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object p2

    invoke-virtual {p1, v1}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v1

    iget v3, p0, La/e/a/c/k;->Wa:I

    neg-int v3, v3

    invoke-virtual {p1, p2, v1, v3, v6}, La/e/a/d;->a(La/e/a/j;La/e/a/j;II)La/e/a/b;

    if-eqz v2, :cond_9

    invoke-virtual {p1, v0}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v0

    invoke-virtual {p1, p2, v0, v4, v5}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    invoke-virtual {p1, v1, p2, v4, v5}, La/e/a/d;->b(La/e/a/j;La/e/a/j;II)V

    goto :goto_3

    :cond_8
    iget p2, p0, La/e/a/c/k;->Ua:F

    const/high16 v0, -0x40800000    # -1.0f

    cmpl-float p2, p2, v0

    if-eqz p2, :cond_9

    iget-object p2, p0, La/e/a/c/k;->Ya:La/e/a/c/e;

    invoke-virtual {p1, p2}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object p2

    invoke-virtual {p1, v1}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v0

    iget v1, p0, La/e/a/c/k;->Ua:F

    invoke-static {p1, p2, v0, v1}, La/e/a/d;->a(La/e/a/d;La/e/a/j;La/e/a/j;F)La/e/a/b;

    move-result-object p2

    invoke-virtual {p1, p2}, La/e/a/d;->a(La/e/a/b;)V

    :cond_9
    :goto_3
    return-void
.end method

.method public aa()I
    .locals 1

    iget v0, p0, La/e/a/c/k;->Va:I

    return v0
.end method

.method public b(La/e/a/d;Z)V
    .locals 2

    invoke-virtual {p0}, La/e/a/c/g;->v()La/e/a/c/g;

    move-result-object p2

    if-nez p2, :cond_0

    return-void

    :cond_0
    iget-object p2, p0, La/e/a/c/k;->Ya:La/e/a/c/e;

    invoke-virtual {p1, p2}, La/e/a/d;->b(Ljava/lang/Object;)I

    move-result p1

    iget p2, p0, La/e/a/c/k;->Za:I

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p2, v0, :cond_1

    invoke-virtual {p0, p1}, La/e/a/c/g;->w(I)V

    invoke-virtual {p0, v1}, La/e/a/c/g;->x(I)V

    invoke-virtual {p0}, La/e/a/c/g;->v()La/e/a/c/g;

    move-result-object p1

    invoke-virtual {p1}, La/e/a/c/g;->k()I

    move-result p1

    invoke-virtual {p0, p1}, La/e/a/c/g;->m(I)V

    invoke-virtual {p0, v1}, La/e/a/c/g;->u(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v1}, La/e/a/c/g;->w(I)V

    invoke-virtual {p0, p1}, La/e/a/c/g;->x(I)V

    invoke-virtual {p0}, La/e/a/c/g;->v()La/e/a/c/g;

    move-result-object p1

    invoke-virtual {p1}, La/e/a/c/g;->C()I

    move-result p1

    invoke-virtual {p0, p1}, La/e/a/c/g;->u(I)V

    invoke-virtual {p0, v1}, La/e/a/c/g;->m(I)V

    :goto_0
    return-void
.end method

.method public ba()I
    .locals 1

    iget v0, p0, La/e/a/c/k;->Wa:I

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public ca()F
    .locals 1

    iget v0, p0, La/e/a/c/k;->Ua:F

    return v0
.end method

.method public e(F)V
    .locals 1

    const/high16 v0, -0x40800000    # -1.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    iput p1, p0, La/e/a/c/k;->Ua:F

    const/4 p1, -0x1

    iput p1, p0, La/e/a/c/k;->Va:I

    iput p1, p0, La/e/a/c/k;->Wa:I

    :cond_0
    return-void
.end method

.method public y(I)V
    .locals 1

    iget-object v0, p0, La/e/a/c/k;->Ya:La/e/a/c/e;

    invoke-virtual {v0, p1}, La/e/a/c/e;->a(I)V

    const/4 p1, 0x1

    iput-boolean p1, p0, La/e/a/c/k;->ab:Z

    return-void
.end method

.method public z(I)V
    .locals 2

    const/4 v0, -0x1

    if-le p1, v0, :cond_0

    const/high16 v1, -0x40800000    # -1.0f

    iput v1, p0, La/e/a/c/k;->Ua:F

    iput p1, p0, La/e/a/c/k;->Va:I

    iput v0, p0, La/e/a/c/k;->Wa:I

    :cond_0
    return-void
.end method
