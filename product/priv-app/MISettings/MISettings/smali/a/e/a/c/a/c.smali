.class public La/e/a/c/a/c;
.super La/e/a/c/a/s;


# instance fields
.field k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "La/e/a/c/a/s;",
            ">;"
        }
    .end annotation
.end field

.field private l:I


# direct methods
.method public constructor <init>(La/e/a/c/g;I)V
    .locals 0

    invoke-direct {p0, p1}, La/e/a/c/a/s;-><init>(La/e/a/c/g;)V

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    iput p2, p0, La/e/a/c/a/s;->f:I

    invoke-direct {p0}, La/e/a/c/a/c;->g()V

    return-void
.end method

.method private g()V
    .locals 5

    iget-object v0, p0, La/e/a/c/a/s;->b:La/e/a/c/g;

    iget v1, p0, La/e/a/c/a/s;->f:I

    invoke-virtual {v0, v1}, La/e/a/c/g;->e(I)La/e/a/c/g;

    move-result-object v1

    :goto_0
    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    if-eqz v0, :cond_0

    iget v1, p0, La/e/a/c/a/s;->f:I

    invoke-virtual {v0, v1}, La/e/a/c/g;->e(I)La/e/a/c/g;

    move-result-object v1

    goto :goto_0

    :cond_0
    iput-object v1, p0, La/e/a/c/a/s;->b:La/e/a/c/g;

    iget-object v0, p0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    iget v2, p0, La/e/a/c/a/s;->f:I

    invoke-virtual {v1, v2}, La/e/a/c/g;->f(I)La/e/a/c/a/s;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v0, p0, La/e/a/c/a/s;->f:I

    invoke-virtual {v1, v0}, La/e/a/c/g;->d(I)La/e/a/c/g;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_1

    iget-object v1, p0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    iget v2, p0, La/e/a/c/a/s;->f:I

    invoke-virtual {v0, v2}, La/e/a/c/g;->f(I)La/e/a/c/a/s;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v1, p0, La/e/a/c/a/s;->f:I

    invoke-virtual {v0, v1}, La/e/a/c/g;->d(I)La/e/a/c/g;

    move-result-object v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La/e/a/c/a/s;

    iget v3, p0, La/e/a/c/a/s;->f:I

    if-nez v3, :cond_3

    iget-object v1, v1, La/e/a/c/a/s;->b:La/e/a/c/g;

    iput-object p0, v1, La/e/a/c/g;->d:La/e/a/c/a/c;

    goto :goto_2

    :cond_3
    if-ne v3, v2, :cond_2

    iget-object v1, v1, La/e/a/c/a/s;->b:La/e/a/c/g;

    iput-object p0, v1, La/e/a/c/g;->e:La/e/a/c/a/c;

    goto :goto_2

    :cond_4
    iget v0, p0, La/e/a/c/a/s;->f:I

    if-nez v0, :cond_5

    iget-object v0, p0, La/e/a/c/a/s;->b:La/e/a/c/g;

    invoke-virtual {v0}, La/e/a/c/g;->v()La/e/a/c/g;

    move-result-object v0

    check-cast v0, La/e/a/c/h;

    invoke-virtual {v0}, La/e/a/c/h;->ia()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v2

    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    :goto_3
    if-eqz v0, :cond_6

    iget-object v0, p0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v2, :cond_6

    iget-object v0, p0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/e/a/c/a/s;

    iget-object v0, v0, La/e/a/c/a/s;->b:La/e/a/c/g;

    iput-object v0, p0, La/e/a/c/a/s;->b:La/e/a/c/g;

    :cond_6
    iget v0, p0, La/e/a/c/a/s;->f:I

    if-nez v0, :cond_7

    iget-object v0, p0, La/e/a/c/a/s;->b:La/e/a/c/g;

    invoke-virtual {v0}, La/e/a/c/g;->m()I

    move-result v0

    goto :goto_4

    :cond_7
    iget-object v0, p0, La/e/a/c/a/s;->b:La/e/a/c/g;

    invoke-virtual {v0}, La/e/a/c/g;->y()I

    move-result v0

    :goto_4
    iput v0, p0, La/e/a/c/a/c;->l:I

    return-void
.end method

.method private h()La/e/a/c/g;
    .locals 4

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La/e/a/c/a/s;

    iget-object v2, v1, La/e/a/c/a/s;->b:La/e/a/c/g;

    invoke-virtual {v2}, La/e/a/c/g;->B()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_0

    iget-object v0, v1, La/e/a/c/a/s;->b:La/e/a/c/g;

    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private i()La/e/a/c/g;
    .locals 4

    iget-object v0, p0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La/e/a/c/a/s;

    iget-object v2, v1, La/e/a/c/a/s;->b:La/e/a/c/g;

    invoke-virtual {v2}, La/e/a/c/g;->B()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_0

    iget-object v0, v1, La/e/a/c/a/s;->b:La/e/a/c/g;

    return-object v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 5

    goto/32 :goto_4e

    nop

    :goto_0
    invoke-virtual {v0}, La/e/a/c/e;->c()I

    move-result v0

    goto/32 :goto_3f

    nop

    :goto_1
    iput-object p0, v0, La/e/a/c/a/f;->a:La/e/a/c/a/d;

    goto/32 :goto_c

    nop

    :goto_2
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_46

    nop

    :goto_3
    invoke-virtual {p0, v4, v3, v2}, La/e/a/c/a/s;->a(La/e/a/c/a/f;La/e/a/c/a/f;I)V

    :goto_4
    goto/32 :goto_2f

    nop

    :goto_5
    iget-object v2, v2, La/e/a/c/g;->S:La/e/a/c/e;

    goto/32 :goto_4b

    nop

    :goto_6
    iput-object p0, v0, La/e/a/c/a/f;->a:La/e/a/c/a/d;

    goto/32 :goto_19

    nop

    :goto_7
    invoke-virtual {v0}, La/e/a/c/e;->c()I

    move-result v0

    :goto_8
    goto/32 :goto_28

    nop

    :goto_9
    invoke-virtual {v2}, La/e/a/c/e;->c()I

    move-result v2

    goto/32 :goto_30

    nop

    :goto_a
    if-nez v1, :cond_0

    goto/32 :goto_3b

    :cond_0
    goto/32 :goto_13

    nop

    :goto_b
    iget-object v4, p0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    goto/32 :goto_49

    nop

    :goto_c
    return-void

    :goto_d
    iget-object v2, v4, La/e/a/c/g;->S:La/e/a/c/e;

    goto/32 :goto_41

    nop

    :goto_e
    invoke-virtual {v1}, La/e/a/c/e;->c()I

    move-result v1

    :goto_f
    goto/32 :goto_2b

    nop

    :goto_10
    invoke-virtual {p0, v2, v1, v0}, La/e/a/c/a/s;->a(La/e/a/c/a/f;La/e/a/c/a/f;I)V

    :goto_11
    goto/32 :goto_40

    nop

    :goto_12
    iget-object v0, v2, La/e/a/c/g;->T:La/e/a/c/e;

    goto/32 :goto_1f

    nop

    :goto_13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_4a

    nop

    :goto_14
    neg-int v0, v0

    goto/32 :goto_10

    nop

    :goto_15
    check-cast v2, La/e/a/c/a/s;

    goto/32 :goto_22

    nop

    :goto_16
    return-void

    :goto_17
    goto/32 :goto_23

    nop

    :goto_18
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_15

    nop

    :goto_19
    iget-object v0, p0, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    goto/32 :goto_1

    nop

    :goto_1a
    invoke-direct {p0}, La/e/a/c/a/c;->h()La/e/a/c/g;

    move-result-object v4

    goto/32 :goto_26

    nop

    :goto_1b
    invoke-virtual {p0, v4, v2, v1}, La/e/a/c/a/s;->a(La/e/a/c/a/f;La/e/a/c/a/f;I)V

    :goto_1c
    goto/32 :goto_33

    nop

    :goto_1d
    if-eqz v4, :cond_1

    goto/32 :goto_36

    :cond_1
    goto/32 :goto_52

    nop

    :goto_1e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_a

    nop

    :goto_1f
    invoke-virtual {v0}, La/e/a/c/e;->c()I

    move-result v0

    :goto_20
    goto/32 :goto_2c

    nop

    :goto_21
    iget-object v2, p0, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    goto/32 :goto_14

    nop

    :goto_22
    iget-object v2, v2, La/e/a/c/a/s;->b:La/e/a/c/g;

    goto/32 :goto_b

    nop

    :goto_23
    iget-object v2, p0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    goto/32 :goto_47

    nop

    :goto_24
    if-nez v4, :cond_2

    goto/32 :goto_42

    :cond_2
    goto/32 :goto_d

    nop

    :goto_25
    iget-object v4, p0, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    goto/32 :goto_3

    nop

    :goto_26
    if-nez v4, :cond_3

    goto/32 :goto_f

    :cond_3
    goto/32 :goto_4f

    nop

    :goto_27
    iget-object v0, v0, La/e/a/c/g;->T:La/e/a/c/e;

    goto/32 :goto_2d

    nop

    :goto_28
    if-nez v1, :cond_4

    goto/32 :goto_11

    :cond_4
    goto/32 :goto_21

    nop

    :goto_29
    iget-object v4, p0, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    goto/32 :goto_1b

    nop

    :goto_2a
    if-nez v2, :cond_5

    goto/32 :goto_20

    :cond_5
    goto/32 :goto_12

    nop

    :goto_2b
    if-nez v2, :cond_6

    goto/32 :goto_1c

    :cond_6
    goto/32 :goto_29

    nop

    :goto_2c
    if-nez v1, :cond_7

    goto/32 :goto_11

    :cond_7
    goto/32 :goto_44

    nop

    :goto_2d
    invoke-virtual {p0, v1, v3}, La/e/a/c/a/s;->a(La/e/a/c/e;I)La/e/a/c/a/f;

    move-result-object v2

    goto/32 :goto_3d

    nop

    :goto_2e
    iget v4, p0, La/e/a/c/a/s;->f:I

    goto/32 :goto_1d

    nop

    :goto_2f
    invoke-virtual {p0, v0, v1}, La/e/a/c/a/s;->a(La/e/a/c/e;I)La/e/a/c/a/f;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_30
    invoke-direct {p0}, La/e/a/c/a/c;->h()La/e/a/c/g;

    move-result-object v4

    goto/32 :goto_24

    nop

    :goto_31
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_32
    goto/32 :goto_1e

    nop

    :goto_33
    invoke-virtual {p0, v0, v3}, La/e/a/c/a/s;->a(La/e/a/c/e;I)La/e/a/c/a/f;

    move-result-object v1

    goto/32 :goto_43

    nop

    :goto_34
    invoke-direct {p0}, La/e/a/c/a/c;->i()La/e/a/c/g;

    move-result-object v2

    goto/32 :goto_2a

    nop

    :goto_35
    goto/16 :goto_11

    :goto_36
    goto/32 :goto_5

    nop

    :goto_37
    iget-object v0, p0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    goto/32 :goto_53

    nop

    :goto_38
    const/4 v1, 0x1

    goto/32 :goto_48

    nop

    :goto_39
    invoke-virtual {v1}, La/e/a/c/a/s;->a()V

    goto/32 :goto_3a

    nop

    :goto_3a
    goto :goto_32

    :goto_3b
    goto/32 :goto_37

    nop

    :goto_3c
    neg-int v0, v0

    goto/32 :goto_3e

    nop

    :goto_3d
    invoke-virtual {v1}, La/e/a/c/e;->c()I

    move-result v1

    goto/32 :goto_1a

    nop

    :goto_3e
    invoke-virtual {p0, v2, v1, v0}, La/e/a/c/a/s;->a(La/e/a/c/a/f;La/e/a/c/a/f;I)V

    goto/32 :goto_35

    nop

    :goto_3f
    invoke-direct {p0}, La/e/a/c/a/c;->i()La/e/a/c/g;

    move-result-object v2

    goto/32 :goto_51

    nop

    :goto_40
    iget-object v0, p0, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    goto/32 :goto_6

    nop

    :goto_41
    invoke-virtual {v2}, La/e/a/c/e;->c()I

    move-result v2

    :goto_42
    goto/32 :goto_45

    nop

    :goto_43
    invoke-virtual {v0}, La/e/a/c/e;->c()I

    move-result v0

    goto/32 :goto_34

    nop

    :goto_44
    iget-object v2, p0, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    goto/32 :goto_3c

    nop

    :goto_45
    if-nez v3, :cond_8

    goto/32 :goto_4

    :cond_8
    goto/32 :goto_25

    nop

    :goto_46
    check-cast v0, La/e/a/c/a/s;

    goto/32 :goto_4c

    nop

    :goto_47
    const/4 v3, 0x0

    goto/32 :goto_18

    nop

    :goto_48
    if-lt v0, v1, :cond_9

    goto/32 :goto_17

    :cond_9
    goto/32 :goto_16

    nop

    :goto_49
    sub-int/2addr v0, v1

    goto/32 :goto_2

    nop

    :goto_4a
    check-cast v1, La/e/a/c/a/s;

    goto/32 :goto_39

    nop

    :goto_4b
    iget-object v0, v0, La/e/a/c/g;->U:La/e/a/c/e;

    goto/32 :goto_4d

    nop

    :goto_4c
    iget-object v0, v0, La/e/a/c/a/s;->b:La/e/a/c/g;

    goto/32 :goto_2e

    nop

    :goto_4d
    invoke-virtual {p0, v2, v1}, La/e/a/c/a/s;->a(La/e/a/c/e;I)La/e/a/c/a/f;

    move-result-object v3

    goto/32 :goto_9

    nop

    :goto_4e
    iget-object v0, p0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    goto/32 :goto_31

    nop

    :goto_4f
    iget-object v1, v4, La/e/a/c/g;->R:La/e/a/c/e;

    goto/32 :goto_e

    nop

    :goto_50
    iget-object v0, v2, La/e/a/c/g;->U:La/e/a/c/e;

    goto/32 :goto_7

    nop

    :goto_51
    if-nez v2, :cond_a

    goto/32 :goto_8

    :cond_a
    goto/32 :goto_50

    nop

    :goto_52
    iget-object v1, v2, La/e/a/c/g;->R:La/e/a/c/e;

    goto/32 :goto_27

    nop

    :goto_53
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_38

    nop
.end method

.method public a(La/e/a/c/a/d;)V
    .locals 22

    move-object/from16 v0, p0

    iget-object v1, v0, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget-boolean v1, v1, La/e/a/c/a/f;->j:Z

    if-eqz v1, :cond_55

    iget-object v1, v0, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget-boolean v1, v1, La/e/a/c/a/f;->j:Z

    if-nez v1, :cond_0

    goto/16 :goto_31

    :cond_0
    iget-object v1, v0, La/e/a/c/a/s;->b:La/e/a/c/g;

    invoke-virtual {v1}, La/e/a/c/g;->v()La/e/a/c/g;

    move-result-object v1

    instance-of v2, v1, La/e/a/c/h;

    if-eqz v2, :cond_1

    check-cast v1, La/e/a/c/h;

    invoke-virtual {v1}, La/e/a/c/h;->ia()Z

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    iget-object v2, v0, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget v2, v2, La/e/a/c/a/f;->g:I

    iget-object v4, v0, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget v4, v4, La/e/a/c/a/f;->g:I

    sub-int/2addr v2, v4

    iget-object v4, v0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x0

    :goto_1
    const/4 v6, -0x1

    const/16 v7, 0x8

    if-ge v5, v4, :cond_2

    iget-object v8, v0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, La/e/a/c/a/s;

    iget-object v8, v8, La/e/a/c/a/s;->b:La/e/a/c/g;

    invoke-virtual {v8}, La/e/a/c/g;->B()I

    move-result v8

    if-ne v8, v7, :cond_3

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_2
    move v5, v6

    :cond_3
    add-int/lit8 v8, v4, -0x1

    move v9, v8

    :goto_2
    if-ltz v9, :cond_5

    iget-object v10, v0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, La/e/a/c/a/s;

    iget-object v10, v10, La/e/a/c/a/s;->b:La/e/a/c/g;

    invoke-virtual {v10}, La/e/a/c/g;->B()I

    move-result v10

    if-ne v10, v7, :cond_4

    add-int/lit8 v9, v9, -0x1

    goto :goto_2

    :cond_4
    move v6, v9

    :cond_5
    const/4 v9, 0x0

    :goto_3
    const/4 v11, 0x2

    const/4 v12, 0x1

    if-ge v9, v11, :cond_13

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    :goto_4
    if-ge v13, v4, :cond_10

    iget-object v3, v0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    invoke-virtual {v3, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, La/e/a/c/a/s;

    iget-object v11, v3, La/e/a/c/a/s;->b:La/e/a/c/g;

    invoke-virtual {v11}, La/e/a/c/g;->B()I

    move-result v11

    if-ne v11, v7, :cond_6

    goto/16 :goto_8

    :cond_6
    add-int/lit8 v16, v16, 0x1

    if-lez v13, :cond_7

    if-lt v13, v5, :cond_7

    iget-object v11, v3, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget v11, v11, La/e/a/c/a/f;->f:I

    add-int/2addr v14, v11

    :cond_7
    iget-object v11, v3, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget v11, v11, La/e/a/c/a/f;->g:I

    iget-object v7, v3, La/e/a/c/a/s;->d:La/e/a/c/g$a;

    sget-object v10, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-eq v7, v10, :cond_8

    move v7, v12

    goto :goto_5

    :cond_8
    const/4 v7, 0x0

    :goto_5
    if-eqz v7, :cond_a

    iget v10, v0, La/e/a/c/a/s;->f:I

    if-nez v10, :cond_9

    iget-object v10, v3, La/e/a/c/a/s;->b:La/e/a/c/g;

    iget-object v10, v10, La/e/a/c/g;->f:La/e/a/c/a/m;

    iget-object v10, v10, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget-boolean v10, v10, La/e/a/c/a/f;->j:Z

    if-nez v10, :cond_9

    return-void

    :cond_9
    iget v10, v0, La/e/a/c/a/s;->f:I

    if-ne v10, v12, :cond_c

    iget-object v10, v3, La/e/a/c/a/s;->b:La/e/a/c/g;

    iget-object v10, v10, La/e/a/c/g;->g:La/e/a/c/a/p;

    iget-object v10, v10, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget-boolean v10, v10, La/e/a/c/a/f;->j:Z

    if-nez v10, :cond_c

    return-void

    :cond_a
    iget v10, v3, La/e/a/c/a/s;->a:I

    if-ne v10, v12, :cond_b

    if-nez v9, :cond_b

    iget-object v7, v3, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget v11, v7, La/e/a/c/a/g;->m:I

    add-int/lit8 v15, v15, 0x1

    goto :goto_6

    :cond_b
    iget-object v10, v3, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget-boolean v10, v10, La/e/a/c/a/f;->j:Z

    if-eqz v10, :cond_c

    :goto_6
    move v7, v12

    :cond_c
    if-nez v7, :cond_d

    add-int/lit8 v15, v15, 0x1

    iget-object v7, v3, La/e/a/c/a/s;->b:La/e/a/c/g;

    iget-object v7, v7, La/e/a/c/g;->Na:[F

    iget v10, v0, La/e/a/c/a/s;->f:I

    aget v7, v7, v10

    const/4 v10, 0x0

    cmpl-float v11, v7, v10

    if-ltz v11, :cond_e

    add-float v17, v17, v7

    goto :goto_7

    :cond_d
    add-int/2addr v14, v11

    :cond_e
    :goto_7
    if-ge v13, v8, :cond_f

    if-ge v13, v6, :cond_f

    iget-object v3, v3, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget v3, v3, La/e/a/c/a/f;->f:I

    neg-int v3, v3

    add-int/2addr v14, v3

    :cond_f
    :goto_8
    add-int/lit8 v13, v13, 0x1

    const/16 v7, 0x8

    const/4 v11, 0x2

    goto/16 :goto_4

    :cond_10
    if-lt v14, v2, :cond_12

    if-nez v15, :cond_11

    goto :goto_9

    :cond_11
    add-int/lit8 v9, v9, 0x1

    const/16 v7, 0x8

    goto/16 :goto_3

    :cond_12
    :goto_9
    move/from16 v3, v16

    goto :goto_a

    :cond_13
    const/4 v3, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v17, 0x0

    :goto_a
    iget-object v7, v0, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget v7, v7, La/e/a/c/a/f;->g:I

    if-eqz v1, :cond_14

    iget-object v7, v0, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget v7, v7, La/e/a/c/a/f;->g:I

    :cond_14
    const/high16 v9, 0x3f000000    # 0.5f

    if-le v14, v2, :cond_16

    const/high16 v10, 0x40000000    # 2.0f

    if-eqz v1, :cond_15

    sub-int v11, v14, v2

    int-to-float v11, v11

    div-float/2addr v11, v10

    add-float/2addr v11, v9

    float-to-int v10, v11

    add-int/2addr v7, v10

    goto :goto_b

    :cond_15
    sub-int v11, v14, v2

    int-to-float v11, v11

    div-float/2addr v11, v10

    add-float/2addr v11, v9

    float-to-int v10, v11

    sub-int/2addr v7, v10

    :cond_16
    :goto_b
    if-lez v15, :cond_25

    sub-int v10, v2, v14

    int-to-float v10, v10

    int-to-float v11, v15

    div-float v11, v10, v11

    add-float/2addr v11, v9

    float-to-int v11, v11

    const/4 v13, 0x0

    const/16 v16, 0x0

    :goto_c
    if-ge v13, v4, :cond_1e

    iget-object v12, v0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, La/e/a/c/a/s;

    iget-object v9, v12, La/e/a/c/a/s;->b:La/e/a/c/g;

    invoke-virtual {v9}, La/e/a/c/g;->B()I

    move-result v9

    move/from16 v18, v11

    const/16 v11, 0x8

    if-ne v9, v11, :cond_17

    goto :goto_11

    :cond_17
    iget-object v9, v12, La/e/a/c/a/s;->d:La/e/a/c/g$a;

    sget-object v11, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v9, v11, :cond_1d

    iget-object v9, v12, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget-boolean v9, v9, La/e/a/c/a/f;->j:Z

    if-nez v9, :cond_1d

    const/4 v9, 0x0

    cmpl-float v11, v17, v9

    if-lez v11, :cond_18

    iget-object v11, v12, La/e/a/c/a/s;->b:La/e/a/c/g;

    iget-object v11, v11, La/e/a/c/g;->Na:[F

    iget v9, v0, La/e/a/c/a/s;->f:I

    aget v9, v11, v9

    mul-float/2addr v9, v10

    div-float v9, v9, v17

    const/high16 v11, 0x3f000000    # 0.5f

    add-float/2addr v9, v11

    float-to-int v11, v9

    goto :goto_d

    :cond_18
    move/from16 v11, v18

    :goto_d
    iget v9, v0, La/e/a/c/a/s;->f:I

    if-nez v9, :cond_19

    iget-object v9, v12, La/e/a/c/a/s;->b:La/e/a/c/g;

    move/from16 v19, v10

    iget v10, v9, La/e/a/c/g;->B:I

    iget v9, v9, La/e/a/c/g;->A:I

    goto :goto_e

    :cond_19
    move/from16 v19, v10

    iget-object v9, v12, La/e/a/c/a/s;->b:La/e/a/c/g;

    iget v10, v9, La/e/a/c/g;->E:I

    iget v9, v9, La/e/a/c/g;->D:I

    :goto_e
    move/from16 v20, v14

    iget v14, v12, La/e/a/c/a/s;->a:I

    move/from16 v21, v7

    const/4 v7, 0x1

    if-ne v14, v7, :cond_1a

    iget-object v7, v12, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget v7, v7, La/e/a/c/a/g;->m:I

    invoke-static {v11, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    goto :goto_f

    :cond_1a
    move v7, v11

    :goto_f
    invoke-static {v9, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    if-lez v10, :cond_1b

    invoke-static {v10, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    :cond_1b
    if-eq v7, v11, :cond_1c

    add-int/lit8 v16, v16, 0x1

    goto :goto_10

    :cond_1c
    move v7, v11

    :goto_10
    iget-object v9, v12, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    invoke-virtual {v9, v7}, La/e/a/c/a/g;->a(I)V

    goto :goto_12

    :cond_1d
    :goto_11
    move/from16 v21, v7

    move/from16 v19, v10

    move/from16 v20, v14

    :goto_12
    add-int/lit8 v13, v13, 0x1

    move/from16 v11, v18

    move/from16 v10, v19

    move/from16 v14, v20

    move/from16 v7, v21

    const/high16 v9, 0x3f000000    # 0.5f

    const/4 v12, 0x1

    goto/16 :goto_c

    :cond_1e
    move/from16 v21, v7

    move/from16 v20, v14

    if-lez v16, :cond_23

    sub-int v15, v15, v16

    const/4 v7, 0x0

    const/4 v9, 0x0

    :goto_13
    if-ge v7, v4, :cond_22

    iget-object v10, v0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, La/e/a/c/a/s;

    iget-object v11, v10, La/e/a/c/a/s;->b:La/e/a/c/g;

    invoke-virtual {v11}, La/e/a/c/g;->B()I

    move-result v11

    const/16 v12, 0x8

    if-ne v11, v12, :cond_1f

    goto :goto_14

    :cond_1f
    if-lez v7, :cond_20

    if-lt v7, v5, :cond_20

    iget-object v11, v10, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget v11, v11, La/e/a/c/a/f;->f:I

    add-int/2addr v9, v11

    :cond_20
    iget-object v11, v10, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget v11, v11, La/e/a/c/a/f;->g:I

    add-int/2addr v9, v11

    if-ge v7, v8, :cond_21

    if-ge v7, v6, :cond_21

    iget-object v10, v10, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget v10, v10, La/e/a/c/a/f;->f:I

    neg-int v10, v10

    add-int/2addr v9, v10

    :cond_21
    :goto_14
    add-int/lit8 v7, v7, 0x1

    goto :goto_13

    :cond_22
    move v14, v9

    goto :goto_15

    :cond_23
    move/from16 v14, v20

    :goto_15
    iget v7, v0, La/e/a/c/a/c;->l:I

    const/4 v9, 0x2

    if-ne v7, v9, :cond_24

    if-nez v16, :cond_24

    const/4 v7, 0x0

    iput v7, v0, La/e/a/c/a/c;->l:I

    goto :goto_16

    :cond_24
    const/4 v7, 0x0

    goto :goto_16

    :cond_25
    move/from16 v21, v7

    move/from16 v20, v14

    const/4 v7, 0x0

    const/4 v9, 0x2

    :goto_16
    if-le v14, v2, :cond_26

    iput v9, v0, La/e/a/c/a/c;->l:I

    :cond_26
    if-lez v3, :cond_27

    if-nez v15, :cond_27

    if-ne v5, v6, :cond_27

    iput v9, v0, La/e/a/c/a/c;->l:I

    :cond_27
    iget v9, v0, La/e/a/c/a/c;->l:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_37

    if-le v3, v10, :cond_28

    sub-int/2addr v2, v14

    sub-int/2addr v3, v10

    div-int v3, v2, v3

    goto :goto_17

    :cond_28
    if-ne v3, v10, :cond_29

    sub-int/2addr v2, v14

    const/4 v3, 0x2

    div-int/lit8 v3, v2, 0x2

    goto :goto_17

    :cond_29
    move v3, v7

    :goto_17
    if-lez v15, :cond_2a

    move v3, v7

    :cond_2a
    move/from16 v2, v21

    :goto_18
    if-ge v7, v4, :cond_55

    if-eqz v1, :cond_2b

    add-int/lit8 v9, v7, 0x1

    sub-int v9, v4, v9

    goto :goto_19

    :cond_2b
    move v9, v7

    :goto_19
    iget-object v10, v0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, La/e/a/c/a/s;

    iget-object v10, v9, La/e/a/c/a/s;->b:La/e/a/c/g;

    invoke-virtual {v10}, La/e/a/c/g;->B()I

    move-result v10

    const/16 v11, 0x8

    if-ne v10, v11, :cond_2c

    iget-object v10, v9, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    invoke-virtual {v10, v2}, La/e/a/c/a/f;->a(I)V

    iget-object v9, v9, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    invoke-virtual {v9, v2}, La/e/a/c/a/f;->a(I)V

    goto :goto_1f

    :cond_2c
    if-lez v7, :cond_2e

    if-eqz v1, :cond_2d

    sub-int/2addr v2, v3

    goto :goto_1a

    :cond_2d
    add-int/2addr v2, v3

    :cond_2e
    :goto_1a
    if-lez v7, :cond_30

    if-lt v7, v5, :cond_30

    if-eqz v1, :cond_2f

    iget-object v10, v9, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget v10, v10, La/e/a/c/a/f;->f:I

    sub-int/2addr v2, v10

    goto :goto_1b

    :cond_2f
    iget-object v10, v9, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget v10, v10, La/e/a/c/a/f;->f:I

    add-int/2addr v2, v10

    :cond_30
    :goto_1b
    if-eqz v1, :cond_31

    iget-object v10, v9, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    invoke-virtual {v10, v2}, La/e/a/c/a/f;->a(I)V

    goto :goto_1c

    :cond_31
    iget-object v10, v9, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    invoke-virtual {v10, v2}, La/e/a/c/a/f;->a(I)V

    :goto_1c
    iget-object v10, v9, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget v11, v10, La/e/a/c/a/f;->g:I

    iget-object v12, v9, La/e/a/c/a/s;->d:La/e/a/c/g$a;

    sget-object v13, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v12, v13, :cond_32

    iget v12, v9, La/e/a/c/a/s;->a:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_32

    iget v11, v10, La/e/a/c/a/g;->m:I

    :cond_32
    if-eqz v1, :cond_33

    sub-int/2addr v2, v11

    goto :goto_1d

    :cond_33
    add-int/2addr v2, v11

    :goto_1d
    if-eqz v1, :cond_34

    iget-object v10, v9, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    invoke-virtual {v10, v2}, La/e/a/c/a/f;->a(I)V

    goto :goto_1e

    :cond_34
    iget-object v10, v9, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    invoke-virtual {v10, v2}, La/e/a/c/a/f;->a(I)V

    :goto_1e
    const/4 v10, 0x1

    iput-boolean v10, v9, La/e/a/c/a/s;->g:Z

    if-ge v7, v8, :cond_36

    if-ge v7, v6, :cond_36

    if-eqz v1, :cond_35

    iget-object v9, v9, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget v9, v9, La/e/a/c/a/f;->f:I

    neg-int v9, v9

    sub-int/2addr v2, v9

    goto :goto_1f

    :cond_35
    iget-object v9, v9, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget v9, v9, La/e/a/c/a/f;->f:I

    neg-int v9, v9

    add-int/2addr v2, v9

    :cond_36
    :goto_1f
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_18

    :cond_37
    if-nez v9, :cond_44

    sub-int/2addr v2, v14

    const/4 v9, 0x1

    add-int/2addr v3, v9

    div-int v3, v2, v3

    if-lez v15, :cond_38

    move v3, v7

    :cond_38
    move/from16 v2, v21

    :goto_20
    if-ge v7, v4, :cond_55

    if-eqz v1, :cond_39

    add-int/lit8 v9, v7, 0x1

    sub-int v9, v4, v9

    goto :goto_21

    :cond_39
    move v9, v7

    :goto_21
    iget-object v10, v0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, La/e/a/c/a/s;

    iget-object v10, v9, La/e/a/c/a/s;->b:La/e/a/c/g;

    invoke-virtual {v10}, La/e/a/c/g;->B()I

    move-result v10

    const/16 v11, 0x8

    if-ne v10, v11, :cond_3a

    iget-object v10, v9, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    invoke-virtual {v10, v2}, La/e/a/c/a/f;->a(I)V

    iget-object v9, v9, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    invoke-virtual {v9, v2}, La/e/a/c/a/f;->a(I)V

    goto :goto_27

    :cond_3a
    if-eqz v1, :cond_3b

    sub-int/2addr v2, v3

    goto :goto_22

    :cond_3b
    add-int/2addr v2, v3

    :goto_22
    if-lez v7, :cond_3d

    if-lt v7, v5, :cond_3d

    if-eqz v1, :cond_3c

    iget-object v10, v9, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget v10, v10, La/e/a/c/a/f;->f:I

    sub-int/2addr v2, v10

    goto :goto_23

    :cond_3c
    iget-object v10, v9, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget v10, v10, La/e/a/c/a/f;->f:I

    add-int/2addr v2, v10

    :cond_3d
    :goto_23
    if-eqz v1, :cond_3e

    iget-object v10, v9, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    invoke-virtual {v10, v2}, La/e/a/c/a/f;->a(I)V

    goto :goto_24

    :cond_3e
    iget-object v10, v9, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    invoke-virtual {v10, v2}, La/e/a/c/a/f;->a(I)V

    :goto_24
    iget-object v10, v9, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget v11, v10, La/e/a/c/a/f;->g:I

    iget-object v12, v9, La/e/a/c/a/s;->d:La/e/a/c/g$a;

    sget-object v13, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v12, v13, :cond_3f

    iget v12, v9, La/e/a/c/a/s;->a:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_3f

    iget v10, v10, La/e/a/c/a/g;->m:I

    invoke-static {v11, v10}, Ljava/lang/Math;->min(II)I

    move-result v11

    :cond_3f
    if-eqz v1, :cond_40

    sub-int/2addr v2, v11

    goto :goto_25

    :cond_40
    add-int/2addr v2, v11

    :goto_25
    if-eqz v1, :cond_41

    iget-object v10, v9, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    invoke-virtual {v10, v2}, La/e/a/c/a/f;->a(I)V

    goto :goto_26

    :cond_41
    iget-object v10, v9, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    invoke-virtual {v10, v2}, La/e/a/c/a/f;->a(I)V

    :goto_26
    if-ge v7, v8, :cond_43

    if-ge v7, v6, :cond_43

    if-eqz v1, :cond_42

    iget-object v9, v9, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget v9, v9, La/e/a/c/a/f;->f:I

    neg-int v9, v9

    sub-int/2addr v2, v9

    goto :goto_27

    :cond_42
    iget-object v9, v9, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget v9, v9, La/e/a/c/a/f;->f:I

    neg-int v9, v9

    add-int/2addr v2, v9

    :cond_43
    :goto_27
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_20

    :cond_44
    const/4 v3, 0x2

    if-ne v9, v3, :cond_55

    iget v3, v0, La/e/a/c/a/s;->f:I

    if-nez v3, :cond_45

    iget-object v3, v0, La/e/a/c/a/s;->b:La/e/a/c/g;

    invoke-virtual {v3}, La/e/a/c/g;->l()F

    move-result v3

    goto :goto_28

    :cond_45
    iget-object v3, v0, La/e/a/c/a/s;->b:La/e/a/c/g;

    invoke-virtual {v3}, La/e/a/c/g;->x()F

    move-result v3

    :goto_28
    if-eqz v1, :cond_46

    const/high16 v9, 0x3f800000    # 1.0f

    sub-float v3, v9, v3

    :cond_46
    sub-int/2addr v2, v14

    int-to-float v2, v2

    mul-float/2addr v2, v3

    const/high16 v3, 0x3f000000    # 0.5f

    add-float/2addr v2, v3

    float-to-int v3, v2

    if-ltz v3, :cond_47

    if-lez v15, :cond_48

    :cond_47
    move v3, v7

    :cond_48
    if-eqz v1, :cond_49

    sub-int v2, v21, v3

    goto :goto_29

    :cond_49
    add-int v2, v21, v3

    :goto_29
    if-ge v7, v4, :cond_55

    if-eqz v1, :cond_4a

    add-int/lit8 v3, v7, 0x1

    sub-int v3, v4, v3

    goto :goto_2a

    :cond_4a
    move v3, v7

    :goto_2a
    iget-object v9, v0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, La/e/a/c/a/s;

    iget-object v9, v3, La/e/a/c/a/s;->b:La/e/a/c/g;

    invoke-virtual {v9}, La/e/a/c/g;->B()I

    move-result v9

    const/16 v10, 0x8

    if-ne v9, v10, :cond_4b

    iget-object v9, v3, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    invoke-virtual {v9, v2}, La/e/a/c/a/f;->a(I)V

    iget-object v3, v3, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    invoke-virtual {v3, v2}, La/e/a/c/a/f;->a(I)V

    const/4 v13, 0x1

    goto :goto_30

    :cond_4b
    if-lez v7, :cond_4d

    if-lt v7, v5, :cond_4d

    if-eqz v1, :cond_4c

    iget-object v9, v3, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget v9, v9, La/e/a/c/a/f;->f:I

    sub-int/2addr v2, v9

    goto :goto_2b

    :cond_4c
    iget-object v9, v3, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget v9, v9, La/e/a/c/a/f;->f:I

    add-int/2addr v2, v9

    :cond_4d
    :goto_2b
    if-eqz v1, :cond_4e

    iget-object v9, v3, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    invoke-virtual {v9, v2}, La/e/a/c/a/f;->a(I)V

    goto :goto_2c

    :cond_4e
    iget-object v9, v3, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    invoke-virtual {v9, v2}, La/e/a/c/a/f;->a(I)V

    :goto_2c
    iget-object v9, v3, La/e/a/c/a/s;->e:La/e/a/c/a/g;

    iget v11, v9, La/e/a/c/a/f;->g:I

    iget-object v12, v3, La/e/a/c/a/s;->d:La/e/a/c/g$a;

    sget-object v13, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v12, v13, :cond_4f

    iget v12, v3, La/e/a/c/a/s;->a:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_50

    iget v11, v9, La/e/a/c/a/g;->m:I

    goto :goto_2d

    :cond_4f
    const/4 v13, 0x1

    :cond_50
    :goto_2d
    if-eqz v1, :cond_51

    sub-int/2addr v2, v11

    goto :goto_2e

    :cond_51
    add-int/2addr v2, v11

    :goto_2e
    if-eqz v1, :cond_52

    iget-object v9, v3, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    invoke-virtual {v9, v2}, La/e/a/c/a/f;->a(I)V

    goto :goto_2f

    :cond_52
    iget-object v9, v3, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    invoke-virtual {v9, v2}, La/e/a/c/a/f;->a(I)V

    :goto_2f
    if-ge v7, v8, :cond_54

    if-ge v7, v6, :cond_54

    if-eqz v1, :cond_53

    iget-object v3, v3, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget v3, v3, La/e/a/c/a/f;->f:I

    neg-int v3, v3

    sub-int/2addr v2, v3

    goto :goto_30

    :cond_53
    iget-object v3, v3, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget v3, v3, La/e/a/c/a/f;->f:I

    neg-int v3, v3

    add-int/2addr v2, v3

    :cond_54
    :goto_30
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_29

    :cond_55
    :goto_31
    return-void
.end method

.method public b()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La/e/a/c/a/s;

    invoke-virtual {v1}, La/e/a/c/a/s;->b()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method c()V
    .locals 2

    goto/32 :goto_7

    nop

    :goto_0
    goto :goto_3

    :goto_1
    goto/32 :goto_c

    nop

    :goto_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    goto/32 :goto_8

    nop

    :goto_4
    if-nez v1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_a

    nop

    :goto_5
    invoke-virtual {v1}, La/e/a/c/a/s;->c()V

    goto/32 :goto_0

    nop

    :goto_6
    check-cast v1, La/e/a/c/a/s;

    goto/32 :goto_5

    nop

    :goto_7
    const/4 v0, 0x0

    goto/32 :goto_9

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_4

    nop

    :goto_9
    iput-object v0, p0, La/e/a/c/a/s;->c:La/e/a/c/a/n;

    goto/32 :goto_b

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_b
    iget-object v0, p0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    goto/32 :goto_2

    nop

    :goto_c
    return-void
.end method

.method public d()J
    .locals 7

    iget-object v0, p0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_0

    iget-object v4, p0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, La/e/a/c/a/s;

    iget-object v5, v4, La/e/a/c/a/s;->h:La/e/a/c/a/f;

    iget v5, v5, La/e/a/c/a/f;->f:I

    int-to-long v5, v5

    add-long/2addr v1, v5

    invoke-virtual {v4}, La/e/a/c/a/s;->d()J

    move-result-wide v5

    add-long/2addr v1, v5

    iget-object v4, v4, La/e/a/c/a/s;->i:La/e/a/c/a/f;

    iget v4, v4, La/e/a/c/a/f;->f:I

    int-to-long v4, v4

    add-long/2addr v1, v4

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-wide v1
.end method

.method f()Z
    .locals 4

    goto/32 :goto_d

    nop

    :goto_0
    if-eqz v3, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_a

    nop

    :goto_1
    move v2, v1

    :goto_2
    goto/32 :goto_7

    nop

    :goto_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_11

    nop

    :goto_4
    goto :goto_2

    :goto_5
    goto/32 :goto_f

    nop

    :goto_6
    return v0

    :goto_7
    if-lt v2, v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_c

    nop

    :goto_8
    check-cast v3, La/e/a/c/a/s;

    goto/32 :goto_9

    nop

    :goto_9
    invoke-virtual {v3}, La/e/a/c/a/s;->f()Z

    move-result v3

    goto/32 :goto_0

    nop

    :goto_a
    return v1

    :goto_b
    goto/32 :goto_10

    nop

    :goto_c
    iget-object v3, p0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    goto/32 :goto_e

    nop

    :goto_d
    iget-object v0, p0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    goto/32 :goto_3

    nop

    :goto_e
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_8

    nop

    :goto_f
    const/4 v0, 0x1

    goto/32 :goto_6

    nop

    :goto_10
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_4

    nop

    :goto_11
    const/4 v1, 0x0

    goto/32 :goto_1

    nop
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ChainRun "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, La/e/a/c/a/s;->f:I

    if-nez v1, :cond_0

    const-string v1, "horizontal : "

    goto :goto_0

    :cond_0
    const-string v1, "vertical : "

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, La/e/a/c/a/c;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, La/e/a/c/a/s;

    const-string v3, "<"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, "> "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
