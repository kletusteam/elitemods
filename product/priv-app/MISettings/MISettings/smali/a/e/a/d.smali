.class public La/e/a/d;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La/e/a/d$a;,
        La/e/a/d$b;
    }
.end annotation


# static fields
.field public static a:Z = false

.field public static b:Z = true

.field public static c:Z = true

.field public static d:Z = true

.field public static e:Z = false

.field private static f:I = 0x3e8

.field public static g:La/e/a/e;

.field public static h:J

.field public static i:J


# instance fields
.field public j:Z

.field k:I

.field private l:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "La/e/a/j;",
            ">;"
        }
    .end annotation
.end field

.field private m:La/e/a/d$a;

.field private n:I

.field private o:I

.field p:[La/e/a/b;

.field public q:Z

.field public r:Z

.field private s:[Z

.field t:I

.field u:I

.field private v:I

.field final w:La/e/a/c;

.field private x:[La/e/a/j;

.field private y:I

.field private z:La/e/a/d$a;


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, La/e/a/d;->j:Z

    iput v0, p0, La/e/a/d;->k:I

    const/4 v1, 0x0

    iput-object v1, p0, La/e/a/d;->l:Ljava/util/HashMap;

    const/16 v2, 0x20

    iput v2, p0, La/e/a/d;->n:I

    iget v2, p0, La/e/a/d;->n:I

    iput v2, p0, La/e/a/d;->o:I

    iput-object v1, p0, La/e/a/d;->p:[La/e/a/b;

    iput-boolean v0, p0, La/e/a/d;->q:Z

    iput-boolean v0, p0, La/e/a/d;->r:Z

    new-array v1, v2, [Z

    iput-object v1, p0, La/e/a/d;->s:[Z

    const/4 v1, 0x1

    iput v1, p0, La/e/a/d;->t:I

    iput v0, p0, La/e/a/d;->u:I

    iput v2, p0, La/e/a/d;->v:I

    sget v1, La/e/a/d;->f:I

    new-array v1, v1, [La/e/a/j;

    iput-object v1, p0, La/e/a/d;->x:[La/e/a/j;

    iput v0, p0, La/e/a/d;->y:I

    new-array v0, v2, [La/e/a/b;

    iput-object v0, p0, La/e/a/d;->p:[La/e/a/b;

    invoke-direct {p0}, La/e/a/d;->j()V

    new-instance v0, La/e/a/c;

    invoke-direct {v0}, La/e/a/c;-><init>()V

    iput-object v0, p0, La/e/a/d;->w:La/e/a/c;

    new-instance v0, La/e/a/i;

    iget-object v1, p0, La/e/a/d;->w:La/e/a/c;

    invoke-direct {v0, v1}, La/e/a/i;-><init>(La/e/a/c;)V

    iput-object v0, p0, La/e/a/d;->m:La/e/a/d$a;

    sget-boolean v0, La/e/a/d;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, La/e/a/d$b;

    iget-object v1, p0, La/e/a/d;->w:La/e/a/c;

    invoke-direct {v0, p0, v1}, La/e/a/d$b;-><init>(La/e/a/d;La/e/a/c;)V

    iput-object v0, p0, La/e/a/d;->z:La/e/a/d$a;

    goto :goto_0

    :cond_0
    new-instance v0, La/e/a/b;

    iget-object v1, p0, La/e/a/d;->w:La/e/a/c;

    invoke-direct {v0, v1}, La/e/a/b;-><init>(La/e/a/c;)V

    iput-object v0, p0, La/e/a/d;->z:La/e/a/d$a;

    :goto_0
    return-void
.end method

.method private final a(La/e/a/d$a;Z)I
    .locals 12

    sget-object p2, La/e/a/d;->g:La/e/a/e;

    const-wide/16 v0, 0x1

    if-eqz p2, :cond_0

    iget-wide v2, p2, La/e/a/e;->h:J

    add-long/2addr v2, v0

    iput-wide v2, p2, La/e/a/e;->h:J

    :cond_0
    const/4 p2, 0x0

    move v2, p2

    :goto_0
    iget v3, p0, La/e/a/d;->t:I

    if-ge v2, v3, :cond_1

    iget-object v3, p0, La/e/a/d;->s:[Z

    aput-boolean p2, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v2, p2

    move v3, v2

    :cond_2
    :goto_1
    if-nez v2, :cond_e

    sget-object v4, La/e/a/d;->g:La/e/a/e;

    if-eqz v4, :cond_3

    iget-wide v5, v4, La/e/a/e;->i:J

    add-long/2addr v5, v0

    iput-wide v5, v4, La/e/a/e;->i:J

    :cond_3
    add-int/lit8 v3, v3, 0x1

    iget v4, p0, La/e/a/d;->t:I

    mul-int/lit8 v4, v4, 0x2

    if-lt v3, v4, :cond_4

    return v3

    :cond_4
    invoke-interface {p1}, La/e/a/d$a;->getKey()La/e/a/j;

    move-result-object v4

    const/4 v5, 0x1

    if-eqz v4, :cond_5

    iget-object v4, p0, La/e/a/d;->s:[Z

    invoke-interface {p1}, La/e/a/d$a;->getKey()La/e/a/j;

    move-result-object v6

    iget v6, v6, La/e/a/j;->d:I

    aput-boolean v5, v4, v6

    :cond_5
    iget-object v4, p0, La/e/a/d;->s:[Z

    invoke-interface {p1, p0, v4}, La/e/a/d$a;->a(La/e/a/d;[Z)La/e/a/j;

    move-result-object v4

    if-eqz v4, :cond_7

    iget-object v6, p0, La/e/a/d;->s:[Z

    iget v7, v4, La/e/a/j;->d:I

    aget-boolean v8, v6, v7

    if-eqz v8, :cond_6

    return v3

    :cond_6
    aput-boolean v5, v6, v7

    :cond_7
    if-eqz v4, :cond_d

    const v5, 0x7f7fffff    # Float.MAX_VALUE

    const/4 v6, -0x1

    move v8, v5

    move v7, v6

    move v5, p2

    :goto_2
    iget v9, p0, La/e/a/d;->u:I

    if-ge v5, v9, :cond_b

    iget-object v9, p0, La/e/a/d;->p:[La/e/a/b;

    aget-object v9, v9, v5

    iget-object v10, v9, La/e/a/b;->a:La/e/a/j;

    iget-object v10, v10, La/e/a/j;->k:La/e/a/j$a;

    sget-object v11, La/e/a/j$a;->a:La/e/a/j$a;

    if-ne v10, v11, :cond_8

    goto :goto_3

    :cond_8
    iget-boolean v10, v9, La/e/a/b;->f:Z

    if-eqz v10, :cond_9

    goto :goto_3

    :cond_9
    invoke-virtual {v9, v4}, La/e/a/b;->b(La/e/a/j;)Z

    move-result v10

    if-eqz v10, :cond_a

    iget-object v10, v9, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v10, v4}, La/e/a/b$a;->b(La/e/a/j;)F

    move-result v10

    const/4 v11, 0x0

    cmpg-float v11, v10, v11

    if-gez v11, :cond_a

    iget v9, v9, La/e/a/b;->b:F

    neg-float v9, v9

    div-float/2addr v9, v10

    cmpg-float v10, v9, v8

    if-gez v10, :cond_a

    move v7, v5

    move v8, v9

    :cond_a
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_b
    if-le v7, v6, :cond_2

    iget-object v5, p0, La/e/a/d;->p:[La/e/a/b;

    aget-object v5, v5, v7

    iget-object v8, v5, La/e/a/b;->a:La/e/a/j;

    iput v6, v8, La/e/a/j;->e:I

    sget-object v6, La/e/a/d;->g:La/e/a/e;

    if-eqz v6, :cond_c

    iget-wide v8, v6, La/e/a/e;->j:J

    add-long/2addr v8, v0

    iput-wide v8, v6, La/e/a/e;->j:J

    :cond_c
    invoke-virtual {v5, v4}, La/e/a/b;->d(La/e/a/j;)V

    iget-object v4, v5, La/e/a/b;->a:La/e/a/j;

    iput v7, v4, La/e/a/j;->e:I

    invoke-virtual {v4, p0, v5}, La/e/a/j;->a(La/e/a/d;La/e/a/b;)V

    goto/16 :goto_1

    :cond_d
    move v2, v5

    goto/16 :goto_1

    :cond_e
    return v3
.end method

.method public static a(La/e/a/d;La/e/a/j;La/e/a/j;F)La/e/a/b;
    .locals 0

    invoke-virtual {p0}, La/e/a/d;->b()La/e/a/b;

    move-result-object p0

    invoke-virtual {p0, p1, p2, p3}, La/e/a/b;->a(La/e/a/j;La/e/a/j;F)La/e/a/b;

    return-object p0
.end method

.method private a(La/e/a/j$a;Ljava/lang/String;)La/e/a/j;
    .locals 2

    iget-object v0, p0, La/e/a/d;->w:La/e/a/c;

    iget-object v0, v0, La/e/a/c;->c:La/e/a/f;

    invoke-interface {v0}, La/e/a/f;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/e/a/j;

    if-nez v0, :cond_0

    new-instance v0, La/e/a/j;

    invoke-direct {v0, p1, p2}, La/e/a/j;-><init>(La/e/a/j$a;Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, La/e/a/j;->a(La/e/a/j$a;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, La/e/a/j;->b()V

    invoke-virtual {v0, p1, p2}, La/e/a/j;->a(La/e/a/j$a;Ljava/lang/String;)V

    :goto_0
    iget p1, p0, La/e/a/d;->y:I

    sget p2, La/e/a/d;->f:I

    if-lt p1, p2, :cond_1

    mul-int/lit8 p2, p2, 0x2

    sput p2, La/e/a/d;->f:I

    iget-object p1, p0, La/e/a/d;->x:[La/e/a/j;

    sget p2, La/e/a/d;->f:I

    invoke-static {p1, p2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [La/e/a/j;

    iput-object p1, p0, La/e/a/d;->x:[La/e/a/j;

    :cond_1
    iget-object p1, p0, La/e/a/d;->x:[La/e/a/j;

    iget p2, p0, La/e/a/d;->y:I

    add-int/lit8 v1, p2, 0x1

    iput v1, p0, La/e/a/d;->y:I

    aput-object v0, p1, p2

    return-object v0
.end method

.method private b(La/e/a/d$a;)I
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object/from16 v0, p0

    const/4 v2, 0x0

    :goto_0
    iget v3, v0, La/e/a/d;->u:I

    const/4 v4, 0x0

    if-ge v2, v3, :cond_2

    iget-object v3, v0, La/e/a/d;->p:[La/e/a/b;

    aget-object v6, v3, v2

    iget-object v6, v6, La/e/a/b;->a:La/e/a/j;

    iget-object v6, v6, La/e/a/j;->k:La/e/a/j$a;

    sget-object v7, La/e/a/j$a;->a:La/e/a/j$a;

    if-ne v6, v7, :cond_0

    goto :goto_1

    :cond_0
    aget-object v3, v3, v2

    iget v3, v3, La/e/a/b;->b:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_1

    const/4 v2, 0x1

    goto :goto_2

    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :goto_2
    if-eqz v2, :cond_17

    const/4 v2, 0x0

    const/4 v3, 0x0

    :cond_3
    :goto_3
    if-nez v2, :cond_18

    sget-object v6, La/e/a/d;->g:La/e/a/e;

    const-wide/16 v7, 0x1

    if-eqz v6, :cond_4

    iget-wide v9, v6, La/e/a/e;->k:J

    add-long/2addr v9, v7

    iput-wide v9, v6, La/e/a/e;->k:J

    :cond_4
    add-int/lit8 v3, v3, 0x1

    const v6, 0x7f7fffff    # Float.MAX_VALUE

    move v12, v6

    const/4 v6, 0x0

    const/4 v10, -0x1

    const/4 v11, -0x1

    const/4 v13, 0x0

    :goto_4
    iget v14, v0, La/e/a/d;->u:I

    if-ge v6, v14, :cond_14

    iget-object v14, v0, La/e/a/d;->p:[La/e/a/b;

    aget-object v14, v14, v6

    iget-object v15, v14, La/e/a/b;->a:La/e/a/j;

    iget-object v15, v15, La/e/a/j;->k:La/e/a/j$a;

    sget-object v1, La/e/a/j$a;->a:La/e/a/j$a;

    if-ne v15, v1, :cond_5

    goto/16 :goto_b

    :cond_5
    iget-boolean v1, v14, La/e/a/b;->f:Z

    if-eqz v1, :cond_6

    goto/16 :goto_b

    :cond_6
    iget v1, v14, La/e/a/b;->b:F

    cmpg-float v1, v1, v4

    if-gez v1, :cond_13

    sget-boolean v1, La/e/a/d;->d:Z

    const/16 v15, 0x9

    if-eqz v1, :cond_d

    iget-object v1, v14, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v1}, La/e/a/b$a;->a()I

    move-result v1

    move/from16 v16, v13

    move v13, v12

    move v12, v11

    move v11, v10

    const/4 v10, 0x0

    :goto_5
    if-ge v10, v1, :cond_c

    iget-object v5, v14, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v5, v10}, La/e/a/b$a;->a(I)La/e/a/j;

    move-result-object v5

    iget-object v7, v14, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v7, v5}, La/e/a/b$a;->b(La/e/a/j;)F

    move-result v7

    cmpg-float v8, v7, v4

    if-gtz v8, :cond_7

    goto :goto_7

    :cond_7
    move/from16 v9, v16

    const/4 v8, 0x0

    :goto_6
    if-ge v8, v15, :cond_b

    iget-object v15, v5, La/e/a/j;->i:[F

    aget v15, v15, v8

    div-float/2addr v15, v7

    cmpg-float v16, v15, v13

    if-gez v16, :cond_8

    if-eq v8, v9, :cond_9

    :cond_8
    if-le v8, v9, :cond_a

    :cond_9
    iget v9, v5, La/e/a/j;->d:I

    move v11, v6

    move v12, v9

    move v13, v15

    move v9, v8

    :cond_a
    add-int/lit8 v8, v8, 0x1

    const/16 v15, 0x9

    goto :goto_6

    :cond_b
    move/from16 v16, v9

    :goto_7
    add-int/lit8 v10, v10, 0x1

    const-wide/16 v7, 0x1

    const/16 v15, 0x9

    goto :goto_5

    :cond_c
    move v10, v11

    move v11, v12

    move v12, v13

    move/from16 v13, v16

    goto :goto_b

    :cond_d
    const/4 v1, 0x1

    :goto_8
    iget v5, v0, La/e/a/d;->t:I

    if-ge v1, v5, :cond_13

    iget-object v5, v0, La/e/a/d;->w:La/e/a/c;

    iget-object v5, v5, La/e/a/c;->d:[La/e/a/j;

    aget-object v5, v5, v1

    iget-object v7, v14, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v7, v5}, La/e/a/b$a;->b(La/e/a/j;)F

    move-result v7

    cmpg-float v8, v7, v4

    if-gtz v8, :cond_e

    const/16 v9, 0x9

    goto :goto_a

    :cond_e
    const/4 v8, 0x0

    const/16 v9, 0x9

    :goto_9
    if-ge v8, v9, :cond_12

    iget-object v15, v5, La/e/a/j;->i:[F

    aget v15, v15, v8

    div-float/2addr v15, v7

    cmpg-float v16, v15, v12

    if-gez v16, :cond_f

    if-eq v8, v13, :cond_10

    :cond_f
    if-le v8, v13, :cond_11

    :cond_10
    move v11, v1

    move v10, v6

    move v13, v8

    move v12, v15

    :cond_11
    add-int/lit8 v8, v8, 0x1

    goto :goto_9

    :cond_12
    :goto_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_13
    :goto_b
    add-int/lit8 v6, v6, 0x1

    const-wide/16 v7, 0x1

    goto/16 :goto_4

    :cond_14
    const/4 v1, -0x1

    if-eq v10, v1, :cond_16

    iget-object v5, v0, La/e/a/d;->p:[La/e/a/b;

    aget-object v5, v5, v10

    iget-object v6, v5, La/e/a/b;->a:La/e/a/j;

    iput v1, v6, La/e/a/j;->e:I

    sget-object v1, La/e/a/d;->g:La/e/a/e;

    if-eqz v1, :cond_15

    iget-wide v6, v1, La/e/a/e;->j:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, v1, La/e/a/e;->j:J

    :cond_15
    iget-object v1, v0, La/e/a/d;->w:La/e/a/c;

    iget-object v1, v1, La/e/a/c;->d:[La/e/a/j;

    aget-object v1, v1, v11

    invoke-virtual {v5, v1}, La/e/a/b;->d(La/e/a/j;)V

    iget-object v1, v5, La/e/a/b;->a:La/e/a/j;

    iput v10, v1, La/e/a/j;->e:I

    invoke-virtual {v1, v0, v5}, La/e/a/j;->a(La/e/a/d;La/e/a/b;)V

    goto :goto_c

    :cond_16
    const/4 v2, 0x1

    :goto_c
    iget v1, v0, La/e/a/d;->t:I

    div-int/lit8 v1, v1, 0x2

    if-le v3, v1, :cond_3

    const/4 v2, 0x1

    goto/16 :goto_3

    :cond_17
    const/4 v3, 0x0

    :cond_18
    return v3
.end method

.method private final b(La/e/a/b;)V
    .locals 7

    sget-boolean v0, La/e/a/d;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p1, La/e/a/b;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, La/e/a/b;->a:La/e/a/j;

    iget p1, p1, La/e/a/b;->b:F

    invoke-virtual {v0, p0, p1}, La/e/a/j;->a(La/e/a/d;F)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, La/e/a/d;->p:[La/e/a/b;

    iget v1, p0, La/e/a/d;->u:I

    aput-object p1, v0, v1

    iget-object v0, p1, La/e/a/b;->a:La/e/a/j;

    iput v1, v0, La/e/a/j;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, La/e/a/d;->u:I

    invoke-virtual {v0, p0, p1}, La/e/a/j;->a(La/e/a/d;La/e/a/b;)V

    :goto_0
    sget-boolean p1, La/e/a/d;->c:Z

    if-eqz p1, :cond_8

    iget-boolean p1, p0, La/e/a/d;->j:Z

    if-eqz p1, :cond_8

    const/4 p1, 0x0

    move v0, p1

    :goto_1
    iget v1, p0, La/e/a/d;->u:I

    if-ge v0, v1, :cond_7

    iget-object v1, p0, La/e/a/d;->p:[La/e/a/b;

    aget-object v1, v1, v0

    if-nez v1, :cond_1

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "WTF"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_1
    iget-object v1, p0, La/e/a/d;->p:[La/e/a/b;

    aget-object v2, v1, v0

    if-eqz v2, :cond_6

    aget-object v2, v1, v0

    iget-boolean v2, v2, La/e/a/b;->f:Z

    if-eqz v2, :cond_6

    aget-object v1, v1, v0

    iget-object v2, v1, La/e/a/b;->a:La/e/a/j;

    iget v3, v1, La/e/a/b;->b:F

    invoke-virtual {v2, p0, v3}, La/e/a/j;->a(La/e/a/d;F)V

    sget-boolean v2, La/e/a/d;->e:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, La/e/a/d;->w:La/e/a/c;

    iget-object v2, v2, La/e/a/c;->a:La/e/a/f;

    invoke-interface {v2, v1}, La/e/a/f;->a(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    iget-object v2, p0, La/e/a/d;->w:La/e/a/c;

    iget-object v2, v2, La/e/a/c;->b:La/e/a/f;

    invoke-interface {v2, v1}, La/e/a/f;->a(Ljava/lang/Object;)Z

    :goto_2
    iget-object v1, p0, La/e/a/d;->p:[La/e/a/b;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    add-int/lit8 v1, v0, 0x1

    move v3, v1

    :goto_3
    iget v4, p0, La/e/a/d;->u:I

    if-ge v1, v4, :cond_4

    iget-object v3, p0, La/e/a/d;->p:[La/e/a/b;

    add-int/lit8 v4, v1, -0x1

    aget-object v5, v3, v1

    aput-object v5, v3, v4

    aget-object v5, v3, v4

    iget-object v5, v5, La/e/a/b;->a:La/e/a/j;

    iget v5, v5, La/e/a/j;->e:I

    if-ne v5, v1, :cond_3

    aget-object v3, v3, v4

    iget-object v3, v3, La/e/a/b;->a:La/e/a/j;

    iput v4, v3, La/e/a/j;->e:I

    :cond_3
    add-int/lit8 v3, v1, 0x1

    move v6, v3

    move v3, v1

    move v1, v6

    goto :goto_3

    :cond_4
    if-ge v3, v4, :cond_5

    iget-object v1, p0, La/e/a/d;->p:[La/e/a/b;

    aput-object v2, v1, v3

    :cond_5
    iget v1, p0, La/e/a/d;->u:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, La/e/a/d;->u:I

    add-int/lit8 v0, v0, -0x1

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_7
    iput-boolean p1, p0, La/e/a/d;->j:Z

    :cond_8
    return-void
.end method

.method public static e()La/e/a/e;
    .locals 1

    sget-object v0, La/e/a/d;->g:La/e/a/e;

    return-object v0
.end method

.method private h()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, La/e/a/d;->u:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, La/e/a/d;->p:[La/e/a/b;

    aget-object v1, v1, v0

    iget-object v2, v1, La/e/a/b;->a:La/e/a/j;

    iget v1, v1, La/e/a/b;->b:F

    iput v1, v2, La/e/a/j;->g:F

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private i()V
    .locals 6

    iget v0, p0, La/e/a/d;->n:I

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, La/e/a/d;->n:I

    iget-object v0, p0, La/e/a/d;->p:[La/e/a/b;

    iget v1, p0, La/e/a/d;->n:I

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [La/e/a/b;

    iput-object v0, p0, La/e/a/d;->p:[La/e/a/b;

    iget-object v0, p0, La/e/a/d;->w:La/e/a/c;

    iget-object v1, v0, La/e/a/c;->d:[La/e/a/j;

    iget v2, p0, La/e/a/d;->n:I

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [La/e/a/j;

    iput-object v1, v0, La/e/a/c;->d:[La/e/a/j;

    iget v0, p0, La/e/a/d;->n:I

    new-array v1, v0, [Z

    iput-object v1, p0, La/e/a/d;->s:[Z

    iput v0, p0, La/e/a/d;->o:I

    iput v0, p0, La/e/a/d;->v:I

    sget-object v1, La/e/a/d;->g:La/e/a/e;

    if-eqz v1, :cond_0

    iget-wide v2, v1, La/e/a/e;->d:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, La/e/a/e;->d:J

    iget-wide v2, v1, La/e/a/e;->o:J

    int-to-long v4, v0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, v1, La/e/a/e;->o:J

    sget-object v0, La/e/a/d;->g:La/e/a/e;

    iget-wide v1, v0, La/e/a/e;->o:J

    iput-wide v1, v0, La/e/a/e;->x:J

    :cond_0
    return-void
.end method

.method private j()V
    .locals 4

    sget-boolean v0, La/e/a/d;->e:Z

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    :goto_0
    iget v0, p0, La/e/a/d;->u:I

    if-ge v2, v0, :cond_3

    iget-object v0, p0, La/e/a/d;->p:[La/e/a/b;

    aget-object v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v3, p0, La/e/a/d;->w:La/e/a/c;

    iget-object v3, v3, La/e/a/c;->a:La/e/a/f;

    invoke-interface {v3, v0}, La/e/a/f;->a(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, La/e/a/d;->p:[La/e/a/b;

    aput-object v1, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    iget v0, p0, La/e/a/d;->u:I

    if-ge v2, v0, :cond_3

    iget-object v0, p0, La/e/a/d;->p:[La/e/a/b;

    aget-object v0, v0, v2

    if-eqz v0, :cond_2

    iget-object v3, p0, La/e/a/d;->w:La/e/a/c;

    iget-object v3, v3, La/e/a/c;->b:La/e/a/f;

    invoke-interface {v3, v0}, La/e/a/f;->a(Ljava/lang/Object;)Z

    :cond_2
    iget-object v0, p0, La/e/a/d;->p:[La/e/a/b;

    aput-object v1, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    return-void
.end method


# virtual methods
.method public a(La/e/a/j;La/e/a/j;II)La/e/a/b;
    .locals 3

    sget-boolean v0, La/e/a/d;->b:Z

    const/16 v1, 0x8

    if-eqz v0, :cond_0

    if-ne p4, v1, :cond_0

    iget-boolean v0, p2, La/e/a/j;->h:Z

    if-eqz v0, :cond_0

    iget v0, p1, La/e/a/j;->e:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    iget p2, p2, La/e/a/j;->g:F

    int-to-float p3, p3

    add-float/2addr p2, p3

    invoke-virtual {p1, p0, p2}, La/e/a/j;->a(La/e/a/d;F)V

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-virtual {p0}, La/e/a/d;->b()La/e/a/b;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, La/e/a/b;->a(La/e/a/j;La/e/a/j;I)La/e/a/b;

    if-eq p4, v1, :cond_1

    invoke-virtual {v0, p0, p4}, La/e/a/b;->a(La/e/a/d;I)La/e/a/b;

    :cond_1
    invoke-virtual {p0, v0}, La/e/a/d;->a(La/e/a/b;)V

    return-object v0
.end method

.method public a()La/e/a/j;
    .locals 5

    sget-object v0, La/e/a/d;->g:La/e/a/e;

    if-eqz v0, :cond_0

    iget-wide v1, v0, La/e/a/e;->n:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, v0, La/e/a/e;->n:J

    :cond_0
    iget v0, p0, La/e/a/d;->t:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/e/a/d;->o:I

    if-lt v0, v1, :cond_1

    invoke-direct {p0}, La/e/a/d;->i()V

    :cond_1
    sget-object v0, La/e/a/j$a;->c:La/e/a/j$a;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, La/e/a/d;->a(La/e/a/j$a;Ljava/lang/String;)La/e/a/j;

    move-result-object v0

    iget v1, p0, La/e/a/d;->k:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, La/e/a/d;->k:I

    iget v1, p0, La/e/a/d;->t:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, La/e/a/d;->t:I

    iget v1, p0, La/e/a/d;->k:I

    iput v1, v0, La/e/a/j;->d:I

    iget-object v2, p0, La/e/a/d;->w:La/e/a/c;

    iget-object v2, v2, La/e/a/c;->d:[La/e/a/j;

    aput-object v0, v2, v1

    return-object v0
.end method

.method public a(ILjava/lang/String;)La/e/a/j;
    .locals 5

    sget-object v0, La/e/a/d;->g:La/e/a/e;

    if-eqz v0, :cond_0

    iget-wide v1, v0, La/e/a/e;->l:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, v0, La/e/a/e;->l:J

    :cond_0
    iget v0, p0, La/e/a/d;->t:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/e/a/d;->o:I

    if-lt v0, v1, :cond_1

    invoke-direct {p0}, La/e/a/d;->i()V

    :cond_1
    sget-object v0, La/e/a/j$a;->d:La/e/a/j$a;

    invoke-direct {p0, v0, p2}, La/e/a/d;->a(La/e/a/j$a;Ljava/lang/String;)La/e/a/j;

    move-result-object p2

    iget v0, p0, La/e/a/d;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, La/e/a/d;->k:I

    iget v0, p0, La/e/a/d;->t:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, La/e/a/d;->t:I

    iget v0, p0, La/e/a/d;->k:I

    iput v0, p2, La/e/a/j;->d:I

    iput p1, p2, La/e/a/j;->f:I

    iget-object p1, p0, La/e/a/d;->w:La/e/a/c;

    iget-object p1, p1, La/e/a/c;->d:[La/e/a/j;

    aput-object p2, p1, v0

    iget-object p1, p0, La/e/a/d;->m:La/e/a/d$a;

    invoke-interface {p1, p2}, La/e/a/d$a;->a(La/e/a/j;)V

    return-object p2
.end method

.method public a(Ljava/lang/Object;)La/e/a/j;
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    iget v1, p0, La/e/a/d;->t:I

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, La/e/a/d;->o:I

    if-lt v1, v2, :cond_1

    invoke-direct {p0}, La/e/a/d;->i()V

    :cond_1
    instance-of v1, p1, La/e/a/c/e;

    if-eqz v1, :cond_5

    check-cast p1, La/e/a/c/e;

    invoke-virtual {p1}, La/e/a/c/e;->f()La/e/a/j;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, La/e/a/d;->w:La/e/a/c;

    invoke-virtual {p1, v0}, La/e/a/c/e;->a(La/e/a/c;)V

    invoke-virtual {p1}, La/e/a/c/e;->f()La/e/a/j;

    move-result-object p1

    move-object v0, p1

    :cond_2
    iget p1, v0, La/e/a/j;->d:I

    const/4 v1, -0x1

    if-eq p1, v1, :cond_3

    iget v2, p0, La/e/a/d;->k:I

    if-gt p1, v2, :cond_3

    iget-object v2, p0, La/e/a/d;->w:La/e/a/c;

    iget-object v2, v2, La/e/a/c;->d:[La/e/a/j;

    aget-object p1, v2, p1

    if-nez p1, :cond_5

    :cond_3
    iget p1, v0, La/e/a/j;->d:I

    if-eq p1, v1, :cond_4

    invoke-virtual {v0}, La/e/a/j;->b()V

    :cond_4
    iget p1, p0, La/e/a/d;->k:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, La/e/a/d;->k:I

    iget p1, p0, La/e/a/d;->t:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, La/e/a/d;->t:I

    iget p1, p0, La/e/a/d;->k:I

    iput p1, v0, La/e/a/j;->d:I

    sget-object v1, La/e/a/j$a;->a:La/e/a/j$a;

    iput-object v1, v0, La/e/a/j;->k:La/e/a/j$a;

    iget-object v1, p0, La/e/a/d;->w:La/e/a/c;

    iget-object v1, v1, La/e/a/c;->d:[La/e/a/j;

    aput-object v0, v1, p1

    :cond_5
    return-object v0
.end method

.method public a(La/e/a/b;)V
    .locals 7

    if-nez p1, :cond_0

    return-void

    :cond_0
    sget-object v0, La/e/a/d;->g:La/e/a/e;

    const-wide/16 v1, 0x1

    if-eqz v0, :cond_1

    iget-wide v3, v0, La/e/a/e;->f:J

    add-long/2addr v3, v1

    iput-wide v3, v0, La/e/a/e;->f:J

    iget-boolean v3, p1, La/e/a/b;->f:Z

    if-eqz v3, :cond_1

    iget-wide v3, v0, La/e/a/e;->g:J

    add-long/2addr v3, v1

    iput-wide v3, v0, La/e/a/e;->g:J

    :cond_1
    iget v0, p0, La/e/a/d;->u:I

    const/4 v3, 0x1

    add-int/2addr v0, v3

    iget v4, p0, La/e/a/d;->v:I

    if-ge v0, v4, :cond_2

    iget v0, p0, La/e/a/d;->t:I

    add-int/2addr v0, v3

    iget v4, p0, La/e/a/d;->o:I

    if-lt v0, v4, :cond_3

    :cond_2
    invoke-direct {p0}, La/e/a/d;->i()V

    :cond_3
    const/4 v0, 0x0

    iget-boolean v4, p1, La/e/a/b;->f:Z

    if-nez v4, :cond_b

    invoke-virtual {p1, p0}, La/e/a/b;->c(La/e/a/d;)V

    invoke-virtual {p1}, La/e/a/b;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    return-void

    :cond_4
    invoke-virtual {p1}, La/e/a/b;->a()V

    invoke-virtual {p1, p0}, La/e/a/b;->a(La/e/a/d;)Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-virtual {p0}, La/e/a/d;->a()La/e/a/j;

    move-result-object v4

    iput-object v4, p1, La/e/a/b;->a:La/e/a/j;

    iget v5, p0, La/e/a/d;->u:I

    invoke-direct {p0, p1}, La/e/a/d;->b(La/e/a/b;)V

    iget v6, p0, La/e/a/d;->u:I

    add-int/2addr v5, v3

    if-ne v6, v5, :cond_a

    iget-object v0, p0, La/e/a/d;->z:La/e/a/d$a;

    invoke-interface {v0, p1}, La/e/a/d$a;->a(La/e/a/d$a;)V

    iget-object v0, p0, La/e/a/d;->z:La/e/a/d$a;

    invoke-direct {p0, v0, v3}, La/e/a/d;->a(La/e/a/d$a;Z)I

    iget v0, v4, La/e/a/j;->e:I

    const/4 v5, -0x1

    if-ne v0, v5, :cond_9

    iget-object v0, p1, La/e/a/b;->a:La/e/a/j;

    if-ne v0, v4, :cond_6

    invoke-virtual {p1, v4}, La/e/a/b;->c(La/e/a/j;)La/e/a/j;

    move-result-object v0

    if-eqz v0, :cond_6

    sget-object v4, La/e/a/d;->g:La/e/a/e;

    if-eqz v4, :cond_5

    iget-wide v5, v4, La/e/a/e;->j:J

    add-long/2addr v5, v1

    iput-wide v5, v4, La/e/a/e;->j:J

    :cond_5
    invoke-virtual {p1, v0}, La/e/a/b;->d(La/e/a/j;)V

    :cond_6
    iget-boolean v0, p1, La/e/a/b;->f:Z

    if-nez v0, :cond_7

    iget-object v0, p1, La/e/a/b;->a:La/e/a/j;

    invoke-virtual {v0, p0, p1}, La/e/a/j;->a(La/e/a/d;La/e/a/b;)V

    :cond_7
    sget-boolean v0, La/e/a/d;->e:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, La/e/a/d;->w:La/e/a/c;

    iget-object v0, v0, La/e/a/c;->a:La/e/a/f;

    invoke-interface {v0, p1}, La/e/a/f;->a(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_8
    iget-object v0, p0, La/e/a/d;->w:La/e/a/c;

    iget-object v0, v0, La/e/a/c;->b:La/e/a/f;

    invoke-interface {v0, p1}, La/e/a/f;->a(Ljava/lang/Object;)Z

    :goto_0
    iget v0, p0, La/e/a/d;->u:I

    sub-int/2addr v0, v3

    iput v0, p0, La/e/a/d;->u:I

    :cond_9
    move v0, v3

    :cond_a
    invoke-virtual {p1}, La/e/a/b;->b()Z

    move-result v1

    if-nez v1, :cond_b

    return-void

    :cond_b
    if-nez v0, :cond_c

    invoke-direct {p0, p1}, La/e/a/d;->b(La/e/a/b;)V

    :cond_c
    return-void
.end method

.method a(La/e/a/b;II)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_3

    nop

    :goto_1
    invoke-virtual {p1, p3, p2}, La/e/a/b;->a(La/e/a/j;I)La/e/a/b;

    goto/32 :goto_2

    nop

    :goto_2
    return-void

    :goto_3
    invoke-virtual {p0, p3, v0}, La/e/a/d;->a(ILjava/lang/String;)La/e/a/j;

    move-result-object p3

    goto/32 :goto_1

    nop
.end method

.method public a(La/e/a/c/g;La/e/a/c/g;FI)V
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    sget-object v3, La/e/a/c/e$a;->b:La/e/a/c/e$a;

    invoke-virtual {v1, v3}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v3

    invoke-virtual {v0, v3}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v5

    sget-object v3, La/e/a/c/e$a;->c:La/e/a/c/e$a;

    invoke-virtual {v1, v3}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v3

    invoke-virtual {v0, v3}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v7

    sget-object v3, La/e/a/c/e$a;->d:La/e/a/c/e$a;

    invoke-virtual {v1, v3}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v3

    invoke-virtual {v0, v3}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v3

    sget-object v4, La/e/a/c/e$a;->e:La/e/a/c/e$a;

    invoke-virtual {v1, v4}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v1

    invoke-virtual {v0, v1}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v8

    sget-object v1, La/e/a/c/e$a;->b:La/e/a/c/e$a;

    invoke-virtual {v2, v1}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v1

    invoke-virtual {v0, v1}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v1

    sget-object v4, La/e/a/c/e$a;->c:La/e/a/c/e$a;

    invoke-virtual {v2, v4}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v4

    invoke-virtual {v0, v4}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v9

    sget-object v4, La/e/a/c/e$a;->d:La/e/a/c/e$a;

    invoke-virtual {v2, v4}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v4

    invoke-virtual {v0, v4}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v12

    sget-object v4, La/e/a/c/e$a;->e:La/e/a/c/e$a;

    invoke-virtual {v2, v4}, La/e/a/c/g;->a(La/e/a/c/e$a;)La/e/a/c/e;

    move-result-object v2

    invoke-virtual {v0, v2}, La/e/a/d;->a(Ljava/lang/Object;)La/e/a/j;

    move-result-object v10

    invoke-virtual/range {p0 .. p0}, La/e/a/d;->b()La/e/a/b;

    move-result-object v2

    move/from16 v4, p3

    float-to-double v13, v4

    invoke-static {v13, v14}, Ljava/lang/Math;->sin(D)D

    move-result-wide v15

    move/from16 v4, p4

    move-object/from16 v17, v3

    int-to-double v3, v4

    move-object/from16 p1, v12

    mul-double v11, v15, v3

    double-to-float v11, v11

    move-object v6, v2

    invoke-virtual/range {v6 .. v11}, La/e/a/b;->b(La/e/a/j;La/e/a/j;La/e/a/j;La/e/a/j;F)La/e/a/b;

    invoke-virtual {v0, v2}, La/e/a/d;->a(La/e/a/b;)V

    invoke-virtual/range {p0 .. p0}, La/e/a/d;->b()La/e/a/b;

    move-result-object v2

    invoke-static {v13, v14}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    mul-double/2addr v6, v3

    double-to-float v9, v6

    move-object v4, v2

    move-object/from16 v6, v17

    move-object v7, v1

    move-object/from16 v8, p1

    invoke-virtual/range {v4 .. v9}, La/e/a/b;->b(La/e/a/j;La/e/a/j;La/e/a/j;La/e/a/j;F)La/e/a/b;

    invoke-virtual {v0, v2}, La/e/a/d;->a(La/e/a/b;)V

    return-void
.end method

.method a(La/e/a/d$a;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    goto/32 :goto_10

    nop

    :goto_0
    return-void

    :goto_1
    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    goto/32 :goto_2

    nop

    :goto_2
    iput-wide v1, v0, La/e/a/e;->u:J

    goto/32 :goto_8

    nop

    :goto_3
    invoke-direct {p0}, La/e/a/d;->h()V

    goto/32 :goto_0

    nop

    :goto_4
    iput-wide v1, v0, La/e/a/e;->v:J

    :goto_5
    goto/32 :goto_14

    nop

    :goto_6
    iget-wide v1, v0, La/e/a/e;->v:J

    goto/32 :goto_12

    nop

    :goto_7
    const/4 v0, 0x0

    goto/32 :goto_f

    nop

    :goto_8
    sget-object v0, La/e/a/d;->g:La/e/a/e;

    goto/32 :goto_6

    nop

    :goto_9
    iget v3, p0, La/e/a/d;->t:I

    goto/32 :goto_e

    nop

    :goto_a
    iput-wide v1, v0, La/e/a/e;->t:J

    goto/32 :goto_13

    nop

    :goto_b
    iget-wide v1, v0, La/e/a/e;->t:J

    goto/32 :goto_11

    nop

    :goto_c
    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    goto/32 :goto_4

    nop

    :goto_d
    int-to-long v3, v3

    goto/32 :goto_c

    nop

    :goto_e
    int-to-long v3, v3

    goto/32 :goto_1

    nop

    :goto_f
    invoke-direct {p0, p1, v0}, La/e/a/d;->a(La/e/a/d$a;Z)I

    goto/32 :goto_3

    nop

    :goto_10
    sget-object v0, La/e/a/d;->g:La/e/a/e;

    goto/32 :goto_15

    nop

    :goto_11
    const-wide/16 v3, 0x1

    goto/32 :goto_16

    nop

    :goto_12
    iget v3, p0, La/e/a/d;->u:I

    goto/32 :goto_d

    nop

    :goto_13
    iget-wide v1, v0, La/e/a/e;->u:J

    goto/32 :goto_9

    nop

    :goto_14
    invoke-direct {p0, p1}, La/e/a/d;->b(La/e/a/d$a;)I

    goto/32 :goto_7

    nop

    :goto_15
    if-nez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_b

    nop

    :goto_16
    add-long/2addr v1, v3

    goto/32 :goto_a

    nop
.end method

.method public a(La/e/a/e;)V
    .locals 0

    sput-object p1, La/e/a/d;->g:La/e/a/e;

    return-void
.end method

.method public a(La/e/a/j;I)V
    .locals 5

    sget-boolean v0, La/e/a/d;->b:Z

    const/4 v1, -0x1

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    iget v0, p1, La/e/a/j;->e:I

    if-ne v0, v1, :cond_2

    int-to-float p2, p2

    invoke-virtual {p1, p0, p2}, La/e/a/j;->a(La/e/a/d;F)V

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, La/e/a/d;->k:I

    add-int/2addr v1, v2

    if-ge v0, v1, :cond_1

    iget-object v1, p0, La/e/a/d;->w:La/e/a/c;

    iget-object v1, v1, La/e/a/c;->d:[La/e/a/j;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    iget-boolean v3, v1, La/e/a/j;->o:Z

    if-eqz v3, :cond_0

    iget v3, v1, La/e/a/j;->p:I

    iget v4, p1, La/e/a/j;->d:I

    if-ne v3, v4, :cond_0

    iget v3, v1, La/e/a/j;->q:F

    add-float/2addr v3, p2

    invoke-virtual {v1, p0, v3}, La/e/a/j;->a(La/e/a/d;F)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void

    :cond_2
    iget v0, p1, La/e/a/j;->e:I

    if-eq v0, v1, :cond_5

    iget-object v1, p0, La/e/a/d;->p:[La/e/a/b;

    aget-object v0, v1, v0

    iget-boolean v1, v0, La/e/a/b;->f:Z

    if-eqz v1, :cond_3

    int-to-float p1, p2

    iput p1, v0, La/e/a/b;->b:F

    goto :goto_1

    :cond_3
    iget-object v1, v0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {v1}, La/e/a/b$a;->a()I

    move-result v1

    if-nez v1, :cond_4

    iput-boolean v2, v0, La/e/a/b;->f:Z

    int-to-float p1, p2

    iput p1, v0, La/e/a/b;->b:F

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, La/e/a/d;->b()La/e/a/b;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, La/e/a/b;->c(La/e/a/j;I)La/e/a/b;

    invoke-virtual {p0, v0}, La/e/a/d;->a(La/e/a/b;)V

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, La/e/a/d;->b()La/e/a/b;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, La/e/a/b;->b(La/e/a/j;I)La/e/a/b;

    invoke-virtual {p0, v0}, La/e/a/d;->a(La/e/a/b;)V

    :goto_1
    return-void
.end method

.method public a(La/e/a/j;La/e/a/j;IFLa/e/a/j;La/e/a/j;II)V
    .locals 11

    move-object v0, p0

    move/from16 v1, p8

    invoke-virtual {p0}, La/e/a/d;->b()La/e/a/b;

    move-result-object v10

    move-object v2, v10

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move/from16 v9, p7

    invoke-virtual/range {v2 .. v9}, La/e/a/b;->a(La/e/a/j;La/e/a/j;IFLa/e/a/j;La/e/a/j;I)La/e/a/b;

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    invoke-virtual {v10, p0, v1}, La/e/a/b;->a(La/e/a/d;I)La/e/a/b;

    :cond_0
    invoke-virtual {p0, v10}, La/e/a/d;->a(La/e/a/b;)V

    return-void
.end method

.method public a(La/e/a/j;La/e/a/j;IZ)V
    .locals 2

    invoke-virtual {p0}, La/e/a/d;->b()La/e/a/b;

    move-result-object p4

    invoke-virtual {p0}, La/e/a/d;->c()La/e/a/j;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, La/e/a/j;->f:I

    invoke-virtual {p4, p1, p2, v0, p3}, La/e/a/b;->a(La/e/a/j;La/e/a/j;La/e/a/j;I)La/e/a/b;

    invoke-virtual {p0, p4}, La/e/a/d;->a(La/e/a/b;)V

    return-void
.end method

.method public a(La/e/a/j;La/e/a/j;La/e/a/j;La/e/a/j;FI)V
    .locals 7

    invoke-virtual {p0}, La/e/a/d;->b()La/e/a/b;

    move-result-object v6

    move-object v0, v6

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, La/e/a/b;->a(La/e/a/j;La/e/a/j;La/e/a/j;La/e/a/j;F)La/e/a/b;

    const/16 p1, 0x8

    if-eq p6, p1, :cond_0

    invoke-virtual {v6, p0, p6}, La/e/a/b;->a(La/e/a/d;I)La/e/a/b;

    :cond_0
    invoke-virtual {p0, v6}, La/e/a/d;->a(La/e/a/b;)V

    return-void
.end method

.method public b(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, La/e/a/c/e;

    invoke-virtual {p1}, La/e/a/c/e;->f()La/e/a/j;

    move-result-object p1

    if-eqz p1, :cond_0

    iget p1, p1, La/e/a/j;->g:F

    const/high16 v0, 0x3f000000    # 0.5f

    add-float/2addr p1, v0

    float-to-int p1, p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public b()La/e/a/b;
    .locals 5

    sget-boolean v0, La/e/a/d;->e:Z

    const-wide/16 v1, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, La/e/a/d;->w:La/e/a/c;

    iget-object v0, v0, La/e/a/c;->a:La/e/a/f;

    invoke-interface {v0}, La/e/a/f;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/e/a/b;

    if-nez v0, :cond_0

    new-instance v0, La/e/a/d$b;

    iget-object v3, p0, La/e/a/d;->w:La/e/a/c;

    invoke-direct {v0, p0, v3}, La/e/a/d$b;-><init>(La/e/a/d;La/e/a/c;)V

    sget-wide v3, La/e/a/d;->i:J

    add-long/2addr v3, v1

    sput-wide v3, La/e/a/d;->i:J

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, La/e/a/b;->c()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, La/e/a/d;->w:La/e/a/c;

    iget-object v0, v0, La/e/a/c;->b:La/e/a/f;

    invoke-interface {v0}, La/e/a/f;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La/e/a/b;

    if-nez v0, :cond_2

    new-instance v0, La/e/a/b;

    iget-object v3, p0, La/e/a/d;->w:La/e/a/c;

    invoke-direct {v0, v3}, La/e/a/b;-><init>(La/e/a/c;)V

    sget-wide v3, La/e/a/d;->h:J

    add-long/2addr v3, v1

    sput-wide v3, La/e/a/d;->h:J

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, La/e/a/b;->c()V

    :goto_0
    invoke-static {}, La/e/a/j;->a()V

    return-object v0
.end method

.method public b(La/e/a/j;La/e/a/j;II)V
    .locals 3

    invoke-virtual {p0}, La/e/a/d;->b()La/e/a/b;

    move-result-object v0

    invoke-virtual {p0}, La/e/a/d;->c()La/e/a/j;

    move-result-object v1

    const/4 v2, 0x0

    iput v2, v1, La/e/a/j;->f:I

    invoke-virtual {v0, p1, p2, v1, p3}, La/e/a/b;->a(La/e/a/j;La/e/a/j;La/e/a/j;I)La/e/a/b;

    const/16 p1, 0x8

    if-eq p4, p1, :cond_0

    iget-object p1, v0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p1, v1}, La/e/a/b$a;->b(La/e/a/j;)F

    move-result p1

    const/high16 p2, -0x40800000    # -1.0f

    mul-float/2addr p1, p2

    float-to-int p1, p1

    invoke-virtual {p0, v0, p1, p4}, La/e/a/d;->a(La/e/a/b;II)V

    :cond_0
    invoke-virtual {p0, v0}, La/e/a/d;->a(La/e/a/b;)V

    return-void
.end method

.method public b(La/e/a/j;La/e/a/j;IZ)V
    .locals 2

    invoke-virtual {p0}, La/e/a/d;->b()La/e/a/b;

    move-result-object p4

    invoke-virtual {p0}, La/e/a/d;->c()La/e/a/j;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, La/e/a/j;->f:I

    invoke-virtual {p4, p1, p2, v0, p3}, La/e/a/b;->b(La/e/a/j;La/e/a/j;La/e/a/j;I)La/e/a/b;

    invoke-virtual {p0, p4}, La/e/a/d;->a(La/e/a/b;)V

    return-void
.end method

.method public c()La/e/a/j;
    .locals 5

    sget-object v0, La/e/a/d;->g:La/e/a/e;

    if-eqz v0, :cond_0

    iget-wide v1, v0, La/e/a/e;->m:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, v0, La/e/a/e;->m:J

    :cond_0
    iget v0, p0, La/e/a/d;->t:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, La/e/a/d;->o:I

    if-lt v0, v1, :cond_1

    invoke-direct {p0}, La/e/a/d;->i()V

    :cond_1
    sget-object v0, La/e/a/j$a;->c:La/e/a/j$a;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, La/e/a/d;->a(La/e/a/j$a;Ljava/lang/String;)La/e/a/j;

    move-result-object v0

    iget v1, p0, La/e/a/d;->k:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, La/e/a/d;->k:I

    iget v1, p0, La/e/a/d;->t:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, La/e/a/d;->t:I

    iget v1, p0, La/e/a/d;->k:I

    iput v1, v0, La/e/a/j;->d:I

    iget-object v2, p0, La/e/a/d;->w:La/e/a/c;

    iget-object v2, v2, La/e/a/c;->d:[La/e/a/j;

    aput-object v0, v2, v1

    return-object v0
.end method

.method public c(La/e/a/j;La/e/a/j;II)V
    .locals 3

    invoke-virtual {p0}, La/e/a/d;->b()La/e/a/b;

    move-result-object v0

    invoke-virtual {p0}, La/e/a/d;->c()La/e/a/j;

    move-result-object v1

    const/4 v2, 0x0

    iput v2, v1, La/e/a/j;->f:I

    invoke-virtual {v0, p1, p2, v1, p3}, La/e/a/b;->b(La/e/a/j;La/e/a/j;La/e/a/j;I)La/e/a/b;

    const/16 p1, 0x8

    if-eq p4, p1, :cond_0

    iget-object p1, v0, La/e/a/b;->e:La/e/a/b$a;

    invoke-interface {p1, v1}, La/e/a/b$a;->b(La/e/a/j;)F

    move-result p1

    const/high16 p2, -0x40800000    # -1.0f

    mul-float/2addr p1, p2

    float-to-int p1, p1

    invoke-virtual {p0, v0, p1, p4}, La/e/a/d;->a(La/e/a/b;II)V

    :cond_0
    invoke-virtual {p0, v0}, La/e/a/d;->a(La/e/a/b;)V

    return-void
.end method

.method public d()La/e/a/c;
    .locals 1

    iget-object v0, p0, La/e/a/d;->w:La/e/a/c;

    return-object v0
.end method

.method public f()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    sget-object v0, La/e/a/d;->g:La/e/a/e;

    const-wide/16 v1, 0x1

    if-eqz v0, :cond_0

    iget-wide v3, v0, La/e/a/e;->e:J

    add-long/2addr v3, v1

    iput-wide v3, v0, La/e/a/e;->e:J

    :cond_0
    iget-object v0, p0, La/e/a/d;->m:La/e/a/d$a;

    invoke-interface {v0}, La/e/a/d$a;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, La/e/a/d;->h()V

    return-void

    :cond_1
    iget-boolean v0, p0, La/e/a/d;->q:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, La/e/a/d;->r:Z

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    iget-object v0, p0, La/e/a/d;->m:La/e/a/d$a;

    invoke-virtual {p0, v0}, La/e/a/d;->a(La/e/a/d$a;)V

    goto :goto_3

    :cond_3
    :goto_0
    sget-object v0, La/e/a/d;->g:La/e/a/e;

    if-eqz v0, :cond_4

    iget-wide v3, v0, La/e/a/e;->q:J

    add-long/2addr v3, v1

    iput-wide v3, v0, La/e/a/e;->q:J

    :cond_4
    const/4 v0, 0x0

    move v3, v0

    :goto_1
    iget v4, p0, La/e/a/d;->u:I

    if-ge v3, v4, :cond_6

    iget-object v4, p0, La/e/a/d;->p:[La/e/a/b;

    aget-object v4, v4, v3

    iget-boolean v4, v4, La/e/a/b;->f:Z

    if-nez v4, :cond_5

    goto :goto_2

    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_6
    const/4 v0, 0x1

    :goto_2
    if-nez v0, :cond_7

    iget-object v0, p0, La/e/a/d;->m:La/e/a/d$a;

    invoke-virtual {p0, v0}, La/e/a/d;->a(La/e/a/d$a;)V

    goto :goto_3

    :cond_7
    sget-object v0, La/e/a/d;->g:La/e/a/e;

    if-eqz v0, :cond_8

    iget-wide v3, v0, La/e/a/e;->p:J

    add-long/2addr v3, v1

    iput-wide v3, v0, La/e/a/e;->p:J

    :cond_8
    invoke-direct {p0}, La/e/a/d;->h()V

    :goto_3
    return-void
.end method

.method public g()V
    .locals 5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v2, p0, La/e/a/d;->w:La/e/a/c;

    iget-object v3, v2, La/e/a/c;->d:[La/e/a/j;

    array-length v4, v3

    if-ge v1, v4, :cond_1

    aget-object v2, v3, v1

    if-eqz v2, :cond_0

    invoke-virtual {v2}, La/e/a/j;->b()V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, v2, La/e/a/c;->c:La/e/a/f;

    iget-object v2, p0, La/e/a/d;->x:[La/e/a/j;

    iget v3, p0, La/e/a/d;->y:I

    invoke-interface {v1, v2, v3}, La/e/a/f;->a([Ljava/lang/Object;I)V

    iput v0, p0, La/e/a/d;->y:I

    iget-object v1, p0, La/e/a/d;->w:La/e/a/c;

    iget-object v1, v1, La/e/a/c;->d:[La/e/a/j;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v1, p0, La/e/a/d;->l:Ljava/util/HashMap;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    :cond_2
    iput v0, p0, La/e/a/d;->k:I

    iget-object v1, p0, La/e/a/d;->m:La/e/a/d$a;

    invoke-interface {v1}, La/e/a/d$a;->clear()V

    const/4 v1, 0x1

    iput v1, p0, La/e/a/d;->t:I

    move v1, v0

    :goto_1
    iget v2, p0, La/e/a/d;->u:I

    if-ge v1, v2, :cond_4

    iget-object v2, p0, La/e/a/d;->p:[La/e/a/b;

    aget-object v3, v2, v1

    if-eqz v3, :cond_3

    aget-object v2, v2, v1

    iput-boolean v0, v2, La/e/a/b;->c:Z

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    invoke-direct {p0}, La/e/a/d;->j()V

    iput v0, p0, La/e/a/d;->u:I

    sget-boolean v0, La/e/a/d;->e:Z

    if-eqz v0, :cond_5

    new-instance v0, La/e/a/d$b;

    iget-object v1, p0, La/e/a/d;->w:La/e/a/c;

    invoke-direct {v0, p0, v1}, La/e/a/d$b;-><init>(La/e/a/d;La/e/a/c;)V

    iput-object v0, p0, La/e/a/d;->z:La/e/a/d$a;

    goto :goto_2

    :cond_5
    new-instance v0, La/e/a/b;

    iget-object v1, p0, La/e/a/d;->w:La/e/a/c;

    invoke-direct {v0, v1}, La/e/a/b;-><init>(La/e/a/c;)V

    iput-object v0, p0, La/e/a/d;->z:La/e/a/d$a;

    :goto_2
    return-void
.end method
