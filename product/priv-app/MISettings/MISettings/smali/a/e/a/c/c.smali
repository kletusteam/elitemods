.class public La/e/a/c/c;
.super Ljava/lang/Object;


# instance fields
.field protected a:La/e/a/c/g;

.field protected b:La/e/a/c/g;

.field protected c:La/e/a/c/g;

.field protected d:La/e/a/c/g;

.field protected e:La/e/a/c/g;

.field protected f:La/e/a/c/g;

.field protected g:La/e/a/c/g;

.field protected h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "La/e/a/c/g;",
            ">;"
        }
    .end annotation
.end field

.field protected i:I

.field protected j:I

.field protected k:F

.field l:I

.field m:I

.field n:I

.field o:Z

.field private p:I

.field private q:Z

.field protected r:Z

.field protected s:Z

.field protected t:Z

.field protected u:Z

.field private v:Z


# direct methods
.method public constructor <init>(La/e/a/c/g;IZ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, La/e/a/c/c;->k:F

    const/4 v0, 0x0

    iput-boolean v0, p0, La/e/a/c/c;->q:Z

    iput-object p1, p0, La/e/a/c/c;->a:La/e/a/c/g;

    iput p2, p0, La/e/a/c/c;->p:I

    iput-boolean p3, p0, La/e/a/c/c;->q:Z

    return-void
.end method

.method private static a(La/e/a/c/g;I)Z
    .locals 2

    invoke-virtual {p0}, La/e/a/c/g;->B()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    iget-object v0, p0, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    aget-object v0, v0, p1

    sget-object v1, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v0, v1, :cond_1

    iget-object p0, p0, La/e/a/c/g;->z:[I

    aget v0, p0, p1

    if-eqz v0, :cond_0

    aget p0, p0, p1

    const/4 p1, 0x3

    if-ne p0, p1, :cond_1

    :cond_0
    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private b()V
    .locals 13

    iget v0, p0, La/e/a/c/c;->p:I

    const/4 v1, 0x2

    mul-int/2addr v0, v1

    iget-object v2, p0, La/e/a/c/c;->a:La/e/a/c/g;

    const/4 v3, 0x1

    iput-boolean v3, p0, La/e/a/c/c;->o:Z

    const/4 v4, 0x0

    move-object v5, v2

    move-object v6, v5

    move v2, v4

    :goto_0
    if-nez v2, :cond_15

    iget v7, p0, La/e/a/c/c;->i:I

    add-int/2addr v7, v3

    iput v7, p0, La/e/a/c/c;->i:I

    iget-object v7, v5, La/e/a/c/g;->Pa:[La/e/a/c/g;

    iget v8, p0, La/e/a/c/c;->p:I

    const/4 v9, 0x0

    aput-object v9, v7, v8

    iget-object v7, v5, La/e/a/c/g;->Oa:[La/e/a/c/g;

    aput-object v9, v7, v8

    invoke-virtual {v5}, La/e/a/c/g;->B()I

    move-result v7

    const/16 v8, 0x8

    if-eq v7, v8, :cond_10

    iget v7, p0, La/e/a/c/c;->l:I

    add-int/2addr v7, v3

    iput v7, p0, La/e/a/c/c;->l:I

    iget v7, p0, La/e/a/c/c;->p:I

    invoke-virtual {v5, v7}, La/e/a/c/g;->b(I)La/e/a/c/g$a;

    move-result-object v7

    sget-object v8, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-eq v7, v8, :cond_0

    iget v7, p0, La/e/a/c/c;->m:I

    iget v8, p0, La/e/a/c/c;->p:I

    invoke-virtual {v5, v8}, La/e/a/c/g;->c(I)I

    move-result v8

    add-int/2addr v7, v8

    iput v7, p0, La/e/a/c/c;->m:I

    :cond_0
    iget v7, p0, La/e/a/c/c;->m:I

    iget-object v8, v5, La/e/a/c/g;->Z:[La/e/a/c/e;

    aget-object v8, v8, v0

    invoke-virtual {v8}, La/e/a/c/e;->c()I

    move-result v8

    add-int/2addr v7, v8

    iput v7, p0, La/e/a/c/c;->m:I

    iget v7, p0, La/e/a/c/c;->m:I

    iget-object v8, v5, La/e/a/c/g;->Z:[La/e/a/c/e;

    add-int/lit8 v10, v0, 0x1

    aget-object v8, v8, v10

    invoke-virtual {v8}, La/e/a/c/e;->c()I

    move-result v8

    add-int/2addr v7, v8

    iput v7, p0, La/e/a/c/c;->m:I

    iget v7, p0, La/e/a/c/c;->n:I

    iget-object v8, v5, La/e/a/c/g;->Z:[La/e/a/c/e;

    aget-object v8, v8, v0

    invoke-virtual {v8}, La/e/a/c/e;->c()I

    move-result v8

    add-int/2addr v7, v8

    iput v7, p0, La/e/a/c/c;->n:I

    iget v7, p0, La/e/a/c/c;->n:I

    iget-object v8, v5, La/e/a/c/g;->Z:[La/e/a/c/e;

    aget-object v8, v8, v10

    invoke-virtual {v8}, La/e/a/c/e;->c()I

    move-result v8

    add-int/2addr v7, v8

    iput v7, p0, La/e/a/c/c;->n:I

    iget-object v7, p0, La/e/a/c/c;->b:La/e/a/c/g;

    if-nez v7, :cond_1

    iput-object v5, p0, La/e/a/c/c;->b:La/e/a/c/g;

    :cond_1
    iput-object v5, p0, La/e/a/c/c;->d:La/e/a/c/g;

    iget-object v7, v5, La/e/a/c/g;->ca:[La/e/a/c/g$a;

    iget v8, p0, La/e/a/c/c;->p:I

    aget-object v7, v7, v8

    sget-object v10, La/e/a/c/g$a;->c:La/e/a/c/g$a;

    if-ne v7, v10, :cond_10

    iget-object v7, v5, La/e/a/c/g;->z:[I

    aget v10, v7, v8

    const/4 v11, 0x0

    if-eqz v10, :cond_2

    aget v10, v7, v8

    const/4 v12, 0x3

    if-eq v10, v12, :cond_2

    aget v7, v7, v8

    if-ne v7, v1, :cond_9

    :cond_2
    iget v7, p0, La/e/a/c/c;->j:I

    add-int/2addr v7, v3

    iput v7, p0, La/e/a/c/c;->j:I

    iget-object v7, v5, La/e/a/c/g;->Na:[F

    iget v8, p0, La/e/a/c/c;->p:I

    aget v10, v7, v8

    cmpl-float v12, v10, v11

    if-lez v12, :cond_3

    iget v12, p0, La/e/a/c/c;->k:F

    aget v7, v7, v8

    add-float/2addr v12, v7

    iput v12, p0, La/e/a/c/c;->k:F

    :cond_3
    iget v7, p0, La/e/a/c/c;->p:I

    invoke-static {v5, v7}, La/e/a/c/c;->a(La/e/a/c/g;I)Z

    move-result v7

    if-eqz v7, :cond_6

    cmpg-float v7, v10, v11

    if-gez v7, :cond_4

    iput-boolean v3, p0, La/e/a/c/c;->r:Z

    goto :goto_1

    :cond_4
    iput-boolean v3, p0, La/e/a/c/c;->s:Z

    :goto_1
    iget-object v7, p0, La/e/a/c/c;->h:Ljava/util/ArrayList;

    if-nez v7, :cond_5

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, La/e/a/c/c;->h:Ljava/util/ArrayList;

    :cond_5
    iget-object v7, p0, La/e/a/c/c;->h:Ljava/util/ArrayList;

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    iget-object v7, p0, La/e/a/c/c;->f:La/e/a/c/g;

    if-nez v7, :cond_7

    iput-object v5, p0, La/e/a/c/c;->f:La/e/a/c/g;

    :cond_7
    iget-object v7, p0, La/e/a/c/c;->g:La/e/a/c/g;

    if-eqz v7, :cond_8

    iget-object v7, v7, La/e/a/c/g;->Oa:[La/e/a/c/g;

    iget v8, p0, La/e/a/c/c;->p:I

    aput-object v5, v7, v8

    :cond_8
    iput-object v5, p0, La/e/a/c/c;->g:La/e/a/c/g;

    :cond_9
    iget v7, p0, La/e/a/c/c;->p:I

    if-nez v7, :cond_c

    iget v7, v5, La/e/a/c/g;->x:I

    if-eqz v7, :cond_a

    iput-boolean v4, p0, La/e/a/c/c;->o:Z

    goto :goto_2

    :cond_a
    iget v7, v5, La/e/a/c/g;->A:I

    if-nez v7, :cond_b

    iget v7, v5, La/e/a/c/g;->B:I

    if-eqz v7, :cond_f

    :cond_b
    iput-boolean v4, p0, La/e/a/c/c;->o:Z

    goto :goto_2

    :cond_c
    iget v7, v5, La/e/a/c/g;->y:I

    if-eqz v7, :cond_d

    iput-boolean v4, p0, La/e/a/c/c;->o:Z

    goto :goto_2

    :cond_d
    iget v7, v5, La/e/a/c/g;->D:I

    if-nez v7, :cond_e

    iget v7, v5, La/e/a/c/g;->E:I

    if-eqz v7, :cond_f

    :cond_e
    iput-boolean v4, p0, La/e/a/c/c;->o:Z

    :cond_f
    :goto_2
    iget v7, v5, La/e/a/c/g;->ga:F

    cmpl-float v7, v7, v11

    if-eqz v7, :cond_10

    iput-boolean v4, p0, La/e/a/c/c;->o:Z

    iput-boolean v3, p0, La/e/a/c/c;->u:Z

    :cond_10
    if-eq v6, v5, :cond_11

    iget-object v6, v6, La/e/a/c/g;->Pa:[La/e/a/c/g;

    iget v7, p0, La/e/a/c/c;->p:I

    aput-object v5, v6, v7

    :cond_11
    iget-object v6, v5, La/e/a/c/g;->Z:[La/e/a/c/e;

    add-int/lit8 v7, v0, 0x1

    aget-object v6, v6, v7

    iget-object v6, v6, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v6, :cond_13

    iget-object v6, v6, La/e/a/c/e;->d:La/e/a/c/g;

    iget-object v7, v6, La/e/a/c/g;->Z:[La/e/a/c/e;

    aget-object v8, v7, v0

    iget-object v8, v8, La/e/a/c/e;->f:La/e/a/c/e;

    if-eqz v8, :cond_13

    aget-object v7, v7, v0

    iget-object v7, v7, La/e/a/c/e;->f:La/e/a/c/e;

    iget-object v7, v7, La/e/a/c/e;->d:La/e/a/c/g;

    if-eq v7, v5, :cond_12

    goto :goto_3

    :cond_12
    move-object v9, v6

    :cond_13
    :goto_3
    if-eqz v9, :cond_14

    goto :goto_4

    :cond_14
    move v2, v3

    move-object v9, v5

    :goto_4
    move-object v6, v5

    move-object v5, v9

    goto/16 :goto_0

    :cond_15
    iget-object v1, p0, La/e/a/c/c;->b:La/e/a/c/g;

    if-eqz v1, :cond_16

    iget v2, p0, La/e/a/c/c;->m:I

    iget-object v1, v1, La/e/a/c/g;->Z:[La/e/a/c/e;

    aget-object v1, v1, v0

    invoke-virtual {v1}, La/e/a/c/e;->c()I

    move-result v1

    sub-int/2addr v2, v1

    iput v2, p0, La/e/a/c/c;->m:I

    :cond_16
    iget-object v1, p0, La/e/a/c/c;->d:La/e/a/c/g;

    if-eqz v1, :cond_17

    iget v2, p0, La/e/a/c/c;->m:I

    iget-object v1, v1, La/e/a/c/g;->Z:[La/e/a/c/e;

    add-int/2addr v0, v3

    aget-object v0, v1, v0

    invoke-virtual {v0}, La/e/a/c/e;->c()I

    move-result v0

    sub-int/2addr v2, v0

    iput v2, p0, La/e/a/c/c;->m:I

    :cond_17
    iput-object v5, p0, La/e/a/c/c;->c:La/e/a/c/g;

    iget v0, p0, La/e/a/c/c;->p:I

    if-nez v0, :cond_18

    iget-boolean v0, p0, La/e/a/c/c;->q:Z

    if-eqz v0, :cond_18

    iget-object v0, p0, La/e/a/c/c;->c:La/e/a/c/g;

    iput-object v0, p0, La/e/a/c/c;->e:La/e/a/c/g;

    goto :goto_5

    :cond_18
    iget-object v0, p0, La/e/a/c/c;->a:La/e/a/c/g;

    iput-object v0, p0, La/e/a/c/c;->e:La/e/a/c/g;

    :goto_5
    iget-boolean v0, p0, La/e/a/c/c;->s:Z

    if-eqz v0, :cond_19

    iget-boolean v0, p0, La/e/a/c/c;->r:Z

    if-eqz v0, :cond_19

    goto :goto_6

    :cond_19
    move v3, v4

    :goto_6
    iput-boolean v3, p0, La/e/a/c/c;->t:Z

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-boolean v0, p0, La/e/a/c/c;->v:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, La/e/a/c/c;->b()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, La/e/a/c/c;->v:Z

    return-void
.end method
