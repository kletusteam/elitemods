.class public final Lio/reactivex/rxjava3/internal/util/b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field final a:F

.field b:I

.field c:I

.field d:I

.field e:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/16 v0, 0x10

    const/high16 v1, 0x3f400000    # 0.75f

    invoke-direct {p0, v0, v1}, Lio/reactivex/rxjava3/internal/util/b;-><init>(IF)V

    return-void
.end method

.method public constructor <init>(IF)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lio/reactivex/rxjava3/internal/util/b;->a:F

    invoke-static {p1}, Lio/reactivex/rxjava3/internal/util/c;->a(I)I

    move-result p1

    add-int/lit8 v0, p1, -0x1

    iput v0, p0, Lio/reactivex/rxjava3/internal/util/b;->b:I

    int-to-float v0, p1

    mul-float/2addr p2, v0

    float-to-int p2, p2

    iput p2, p0, Lio/reactivex/rxjava3/internal/util/b;->d:I

    new-array p1, p1, [Ljava/lang/Object;

    iput-object p1, p0, Lio/reactivex/rxjava3/internal/util/b;->e:[Ljava/lang/Object;

    return-void
.end method

.method static a(I)I
    .locals 1

    const v0, -0x61c88647

    mul-int/2addr p0, v0

    ushr-int/lit8 v0, p0, 0x10

    xor-int/2addr p0, v0

    return p0
.end method


# virtual methods
.method a(I[Ljava/lang/Object;I)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I[TT;I)Z"
        }
    .end annotation

    goto/32 :goto_f

    nop

    :goto_0
    if-gt v3, v0, :cond_0

    goto/32 :goto_1e

    :cond_0
    :goto_1
    goto/32 :goto_13

    nop

    :goto_2
    add-int/lit8 v0, p1, 0x1

    :goto_3
    goto/32 :goto_1a

    nop

    :goto_4
    invoke-static {v3}, Lio/reactivex/rxjava3/internal/util/b;->a(I)I

    move-result v3

    goto/32 :goto_5

    nop

    :goto_5
    and-int/2addr v3, p3

    goto/32 :goto_e

    nop

    :goto_6
    const/4 p3, 0x0

    goto/32 :goto_10

    nop

    :goto_7
    if-lt p1, v3, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_17

    nop

    :goto_8
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_18

    nop

    :goto_9
    aget-object v2, p2, v0

    goto/32 :goto_11

    nop

    :goto_a
    goto :goto_1

    :goto_b
    goto/32 :goto_1b

    nop

    :goto_c
    iput v0, p0, Lio/reactivex/rxjava3/internal/util/b;->c:I

    :goto_d
    goto/32 :goto_2

    nop

    :goto_e
    if-le p1, v0, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_7

    nop

    :goto_f
    iget v0, p0, Lio/reactivex/rxjava3/internal/util/b;->c:I

    goto/32 :goto_19

    nop

    :goto_10
    aput-object p3, p2, p1

    goto/32 :goto_15

    nop

    :goto_11
    if-eqz v2, :cond_3

    goto/32 :goto_16

    :cond_3
    goto/32 :goto_6

    nop

    :goto_12
    sub-int/2addr v0, v1

    goto/32 :goto_c

    nop

    :goto_13
    aput-object v2, p2, p1

    goto/32 :goto_14

    nop

    :goto_14
    move p1, v0

    goto/32 :goto_1d

    nop

    :goto_15
    return v1

    :goto_16
    goto/32 :goto_1c

    nop

    :goto_17
    if-gt v3, v0, :cond_4

    goto/32 :goto_1e

    :cond_4
    goto/32 :goto_a

    nop

    :goto_18
    goto :goto_3

    :goto_19
    const/4 v1, 0x1

    goto/32 :goto_12

    nop

    :goto_1a
    and-int/2addr v0, p3

    goto/32 :goto_9

    nop

    :goto_1b
    if-ge p1, v3, :cond_5

    goto/32 :goto_1e

    :cond_5
    goto/32 :goto_0

    nop

    :goto_1c
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v3

    goto/32 :goto_4

    nop

    :goto_1d
    goto :goto_d

    :goto_1e
    goto/32 :goto_8

    nop
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    iget-object v0, p0, Lio/reactivex/rxjava3/internal/util/b;->e:[Ljava/lang/Object;

    iget v1, p0, Lio/reactivex/rxjava3/internal/util/b;->b:I

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Lio/reactivex/rxjava3/internal/util/b;->a(I)I

    move-result v2

    and-int/2addr v2, v1

    aget-object v3, v0, v2

    const/4 v4, 0x1

    if-eqz v3, :cond_2

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/4 v5, 0x0

    if-eqz v3, :cond_0

    return v5

    :cond_0
    add-int/2addr v2, v4

    and-int/2addr v2, v1

    aget-object v3, v0, v2

    if-nez v3, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    return v5

    :cond_2
    :goto_0
    aput-object p1, v0, v2

    iget p1, p0, Lio/reactivex/rxjava3/internal/util/b;->c:I

    add-int/2addr p1, v4

    iput p1, p0, Lio/reactivex/rxjava3/internal/util/b;->c:I

    iget v0, p0, Lio/reactivex/rxjava3/internal/util/b;->d:I

    if-lt p1, v0, :cond_3

    invoke-virtual {p0}, Lio/reactivex/rxjava3/internal/util/b;->b()V

    :cond_3
    return v4
.end method

.method public a()[Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lio/reactivex/rxjava3/internal/util/b;->e:[Ljava/lang/Object;

    return-object v0
.end method

.method b()V
    .locals 8

    goto/32 :goto_23

    nop

    :goto_0
    move v5, v6

    goto/32 :goto_11

    nop

    :goto_1
    iput v0, p0, Lio/reactivex/rxjava3/internal/util/b;->d:I

    goto/32 :goto_24

    nop

    :goto_2
    int-to-float v0, v2

    goto/32 :goto_18

    nop

    :goto_3
    return-void

    :goto_4
    shl-int/lit8 v2, v1, 0x1

    goto/32 :goto_1f

    nop

    :goto_5
    if-nez v5, :cond_0

    goto/32 :goto_12

    :cond_0
    :goto_6
    goto/32 :goto_13

    nop

    :goto_7
    aget-object v5, v0, v1

    goto/32 :goto_14

    nop

    :goto_8
    iget v5, p0, Lio/reactivex/rxjava3/internal/util/b;->c:I

    :goto_9
    goto/32 :goto_f

    nop

    :goto_a
    aget-object v7, v0, v1

    goto/32 :goto_1e

    nop

    :goto_b
    goto :goto_6

    :goto_c
    goto/32 :goto_10

    nop

    :goto_d
    if-eqz v7, :cond_1

    goto/32 :goto_22

    :cond_1
    :goto_e
    goto/32 :goto_a

    nop

    :goto_f
    add-int/lit8 v6, v5, -0x1

    goto/32 :goto_5

    nop

    :goto_10
    aget-object v5, v0, v1

    goto/32 :goto_20

    nop

    :goto_11
    goto :goto_9

    :goto_12
    goto/32 :goto_26

    nop

    :goto_13
    add-int/lit8 v1, v1, -0x1

    goto/32 :goto_7

    nop

    :goto_14
    if-eqz v5, :cond_2

    goto/32 :goto_c

    :cond_2
    goto/32 :goto_b

    nop

    :goto_15
    float-to-int v0, v0

    goto/32 :goto_1

    nop

    :goto_16
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_1c

    nop

    :goto_17
    invoke-static {v5}, Lio/reactivex/rxjava3/internal/util/b;->a(I)I

    move-result v5

    goto/32 :goto_1a

    nop

    :goto_18
    iget v1, p0, Lio/reactivex/rxjava3/internal/util/b;->a:F

    goto/32 :goto_25

    nop

    :goto_19
    aget-object v7, v4, v5

    goto/32 :goto_d

    nop

    :goto_1a
    and-int/2addr v5, v3

    goto/32 :goto_1d

    nop

    :goto_1b
    array-length v1, v0

    goto/32 :goto_4

    nop

    :goto_1c
    and-int/2addr v5, v3

    goto/32 :goto_19

    nop

    :goto_1d
    aget-object v7, v4, v5

    goto/32 :goto_21

    nop

    :goto_1e
    aput-object v7, v4, v5

    goto/32 :goto_0

    nop

    :goto_1f
    add-int/lit8 v3, v2, -0x1

    goto/32 :goto_27

    nop

    :goto_20
    invoke-virtual {v5}, Ljava/lang/Object;->hashCode()I

    move-result v5

    goto/32 :goto_17

    nop

    :goto_21
    if-nez v7, :cond_3

    goto/32 :goto_e

    :cond_3
    :goto_22
    goto/32 :goto_16

    nop

    :goto_23
    iget-object v0, p0, Lio/reactivex/rxjava3/internal/util/b;->e:[Ljava/lang/Object;

    goto/32 :goto_1b

    nop

    :goto_24
    iput-object v4, p0, Lio/reactivex/rxjava3/internal/util/b;->e:[Ljava/lang/Object;

    goto/32 :goto_3

    nop

    :goto_25
    mul-float/2addr v0, v1

    goto/32 :goto_15

    nop

    :goto_26
    iput v3, p0, Lio/reactivex/rxjava3/internal/util/b;->b:I

    goto/32 :goto_2

    nop

    :goto_27
    new-array v4, v2, [Ljava/lang/Object;

    goto/32 :goto_8

    nop
.end method

.method public b(Ljava/lang/Object;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    iget-object v0, p0, Lio/reactivex/rxjava3/internal/util/b;->e:[Ljava/lang/Object;

    iget v1, p0, Lio/reactivex/rxjava3/internal/util/b;->b:I

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Lio/reactivex/rxjava3/internal/util/b;->a(I)I

    move-result v2

    and-int/2addr v2, v1

    aget-object v3, v0, v2

    const/4 v4, 0x0

    if-nez v3, :cond_0

    return v4

    :cond_0
    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0, v2, v0, v1}, Lio/reactivex/rxjava3/internal/util/b;->a(I[Ljava/lang/Object;I)Z

    move-result p1

    return p1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    and-int/2addr v2, v1

    aget-object v3, v0, v2

    if-nez v3, :cond_2

    return v4

    :cond_2
    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0, v2, v0, v1}, Lio/reactivex/rxjava3/internal/util/b;->a(I[Ljava/lang/Object;I)Z

    move-result p1

    return p1
.end method
