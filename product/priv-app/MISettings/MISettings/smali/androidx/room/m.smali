.class Landroidx/room/m;
.super Ljava/lang/Object;
.source "MultiInstanceInvalidationClient.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/room/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroidx/room/r;


# direct methods
.method constructor <init>(Landroidx/room/r;)V
    .locals 0

    iput-object p1, p0, Landroidx/room/m;->a:Landroidx/room/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 0

    iget-object p1, p0, Landroidx/room/m;->a:Landroidx/room/r;

    invoke-static {p2}, Landroidx/room/g$a;->a(Landroid/os/IBinder;)Landroidx/room/g;

    move-result-object p2

    iput-object p2, p1, Landroidx/room/r;->f:Landroidx/room/g;

    iget-object p1, p0, Landroidx/room/m;->a:Landroidx/room/r;

    iget-object p2, p1, Landroidx/room/r;->g:Ljava/util/concurrent/Executor;

    iget-object p1, p1, Landroidx/room/r;->k:Ljava/lang/Runnable;

    invoke-interface {p2, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1

    iget-object p1, p0, Landroidx/room/m;->a:Landroidx/room/r;

    iget-object v0, p1, Landroidx/room/r;->g:Ljava/util/concurrent/Executor;

    iget-object p1, p1, Landroidx/room/r;->l:Ljava/lang/Runnable;

    invoke-interface {v0, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    iget-object p1, p0, Landroidx/room/m;->a:Landroidx/room/r;

    const/4 v0, 0x0

    iput-object v0, p1, Landroidx/room/r;->f:Landroidx/room/g;

    return-void
.end method
