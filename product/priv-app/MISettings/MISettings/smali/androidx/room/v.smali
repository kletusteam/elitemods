.class public abstract Landroidx/room/v;
.super Ljava/lang/Object;
.source "RoomDatabase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/room/v$b;,
        Landroidx/room/v$d;,
        Landroidx/room/v$a;,
        Landroidx/room/v$c;
    }
.end annotation


# instance fields
.field protected volatile a:La/p/a/b;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private b:Ljava/util/concurrent/Executor;

.field private c:Ljava/util/concurrent/Executor;

.field private d:La/p/a/c;

.field private final e:Landroidx/room/j;

.field private f:Z

.field g:Z

.field protected h:Ljava/util/List;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroidx/room/v$b;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final i:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

.field private final j:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    iput-object v0, p0, Landroidx/room/v;->i:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Landroidx/room/v;->j:Ljava/lang/ThreadLocal;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Landroidx/room/v;->k:Ljava/util/Map;

    invoke-virtual {p0}, Landroidx/room/v;->d()Landroidx/room/j;

    move-result-object v0

    iput-object v0, p0, Landroidx/room/v;->e:Landroidx/room/j;

    return-void
.end method

.method private static l()Z
    .locals 2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method protected abstract a(Landroidx/room/a;)La/p/a/c;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public a(Ljava/lang/String;)La/p/a/f;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p0}, Landroidx/room/v;->a()V

    invoke-virtual {p0}, Landroidx/room/v;->b()V

    iget-object v0, p0, Landroidx/room/v;->d:La/p/a/c;

    invoke-interface {v0}, La/p/a/c;->getWritableDatabase()La/p/a/b;

    move-result-object v0

    invoke-interface {v0, p1}, La/p/a/b;->c(Ljava/lang/String;)La/p/a/f;

    move-result-object p1

    return-object p1
.end method

.method public a(La/p/a/e;)Landroid/database/Cursor;
    .locals 1
    .param p1    # La/p/a/e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroidx/room/v;->a(La/p/a/e;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object p1

    return-object p1
.end method

.method public a(La/p/a/e;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    .locals 2
    .param p1    # La/p/a/e;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/os/CancellationSignal;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-virtual {p0}, Landroidx/room/v;->a()V

    invoke-virtual {p0}, Landroidx/room/v;->b()V

    if-eqz p2, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Landroidx/room/v;->d:La/p/a/c;

    invoke-interface {v0}, La/p/a/c;->getWritableDatabase()La/p/a/b;

    move-result-object v0

    invoke-interface {v0, p1, p2}, La/p/a/b;->a(La/p/a/e;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object p1

    return-object p1

    :cond_0
    iget-object p2, p0, Landroidx/room/v;->d:La/p/a/c;

    invoke-interface {p2}, La/p/a/c;->getWritableDatabase()La/p/a/b;

    move-result-object p2

    invoke-interface {p2, p1}, La/p/a/b;->a(La/p/a/e;)Landroid/database/Cursor;

    move-result-object p1

    return-object p1
.end method

.method public a()V
    .locals 2
    .annotation build Landroidx/annotation/RestrictTo;
        value = {
            .enum Landroidx/annotation/RestrictTo$a;->c:Landroidx/annotation/RestrictTo$a;
        }
    .end annotation

    iget-boolean v0, p0, Landroidx/room/v;->f:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Landroidx/room/v;->l()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot access database on the main thread since it may potentially lock the UI for a long period of time."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected a(La/p/a/b;)V
    .locals 1
    .param p1    # La/p/a/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Landroidx/room/v;->e:Landroidx/room/j;

    invoke-virtual {v0, p1}, Landroidx/room/j;->a(La/p/a/b;)V

    return-void
.end method

.method public b()V
    .locals 2
    .annotation build Landroidx/annotation/RestrictTo;
        value = {
            .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
        }
    .end annotation

    invoke-virtual {p0}, Landroidx/room/v;->i()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Landroidx/room/v;->j:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot access database on a different coroutine context inherited from a suspending transaction."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    return-void
.end method

.method public b(Landroidx/room/a;)V
    .locals 3
    .param p1    # Landroidx/room/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/CallSuper;
    .end annotation

    invoke-virtual {p0, p1}, Landroidx/room/v;->a(Landroidx/room/a;)La/p/a/c;

    move-result-object v0

    iput-object v0, p0, Landroidx/room/v;->d:La/p/a/c;

    iget-object v0, p0, Landroidx/room/v;->d:La/p/a/c;

    instance-of v1, v0, Landroidx/room/z;

    if-eqz v1, :cond_0

    check-cast v0, Landroidx/room/z;

    invoke-virtual {v0, p1}, Landroidx/room/z;->a(Landroidx/room/a;)V

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    const/4 v2, 0x0

    if-lt v0, v1, :cond_2

    iget-object v0, p1, Landroidx/room/a;->g:Landroidx/room/v$c;

    sget-object v1, Landroidx/room/v$c;->c:Landroidx/room/v$c;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    move v2, v0

    :cond_1
    iget-object v0, p0, Landroidx/room/v;->d:La/p/a/c;

    invoke-interface {v0, v2}, La/p/a/c;->setWriteAheadLoggingEnabled(Z)V

    :cond_2
    iget-object v0, p1, Landroidx/room/a;->e:Ljava/util/List;

    iput-object v0, p0, Landroidx/room/v;->h:Ljava/util/List;

    iget-object v0, p1, Landroidx/room/a;->h:Ljava/util/concurrent/Executor;

    iput-object v0, p0, Landroidx/room/v;->b:Ljava/util/concurrent/Executor;

    new-instance v0, Landroidx/room/D;

    iget-object v1, p1, Landroidx/room/a;->i:Ljava/util/concurrent/Executor;

    invoke-direct {v0, v1}, Landroidx/room/D;-><init>(Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Landroidx/room/v;->c:Ljava/util/concurrent/Executor;

    iget-boolean v0, p1, Landroidx/room/a;->f:Z

    iput-boolean v0, p0, Landroidx/room/v;->f:Z

    iput-boolean v2, p0, Landroidx/room/v;->g:Z

    iget-boolean v0, p1, Landroidx/room/a;->j:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroidx/room/v;->e:Landroidx/room/j;

    iget-object v1, p1, Landroidx/room/a;->b:Landroid/content/Context;

    iget-object p1, p1, Landroidx/room/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroidx/room/j;->a(Landroid/content/Context;Ljava/lang/String;)V

    :cond_3
    return-void
.end method

.method public c()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p0}, Landroidx/room/v;->a()V

    iget-object v0, p0, Landroidx/room/v;->d:La/p/a/c;

    invoke-interface {v0}, La/p/a/c;->getWritableDatabase()La/p/a/b;

    move-result-object v0

    iget-object v1, p0, Landroidx/room/v;->e:Landroidx/room/j;

    invoke-virtual {v1, v0}, Landroidx/room/j;->b(La/p/a/b;)V

    invoke-interface {v0}, La/p/a/b;->k()V

    return-void
.end method

.method protected abstract d()Landroidx/room/j;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public e()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Landroidx/room/v;->d:La/p/a/c;

    invoke-interface {v0}, La/p/a/c;->getWritableDatabase()La/p/a/b;

    move-result-object v0

    invoke-interface {v0}, La/p/a/b;->n()V

    invoke-virtual {p0}, Landroidx/room/v;->i()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroidx/room/v;->e:Landroidx/room/j;

    invoke-virtual {v0}, Landroidx/room/j;->b()V

    :cond_0
    return-void
.end method

.method f()Ljava/util/concurrent/locks/Lock;
    .locals 1

    iget-object v0, p0, Landroidx/room/v;->i:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    return-object v0
.end method

.method public g()La/p/a/c;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Landroidx/room/v;->d:La/p/a/c;

    return-object v0
.end method

.method public h()Ljava/util/concurrent/Executor;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Landroidx/room/v;->b:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public i()Z
    .locals 1

    iget-object v0, p0, Landroidx/room/v;->d:La/p/a/c;

    invoke-interface {v0}, La/p/a/c;->getWritableDatabase()La/p/a/b;

    move-result-object v0

    invoke-interface {v0}, La/p/a/b;->o()Z

    move-result v0

    return v0
.end method

.method public j()Z
    .locals 1

    iget-object v0, p0, Landroidx/room/v;->a:La/p/a/b;

    if-eqz v0, :cond_0

    invoke-interface {v0}, La/p/a/b;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public k()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Landroidx/room/v;->d:La/p/a/c;

    invoke-interface {v0}, La/p/a/c;->getWritableDatabase()La/p/a/b;

    move-result-object v0

    invoke-interface {v0}, La/p/a/b;->m()V

    return-void
.end method
