.class Landroidx/room/r;
.super Ljava/lang/Object;
.source "MultiInstanceInvalidationClient.java"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Ljava/lang/String;

.field c:I

.field final d:Landroidx/room/j;

.field final e:Landroidx/room/j$b;

.field f:Landroidx/room/g;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field final g:Ljava/util/concurrent/Executor;

.field final h:Landroidx/room/f;

.field final i:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final j:Landroid/content/ServiceConnection;

.field final k:Ljava/lang/Runnable;

.field final l:Ljava/lang/Runnable;

.field private final m:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroidx/room/j;Ljava/util/concurrent/Executor;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroidx/room/l;

    invoke-direct {v0, p0}, Landroidx/room/l;-><init>(Landroidx/room/r;)V

    iput-object v0, p0, Landroidx/room/r;->h:Landroidx/room/f;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Landroidx/room/r;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Landroidx/room/m;

    invoke-direct {v0, p0}, Landroidx/room/m;-><init>(Landroidx/room/r;)V

    iput-object v0, p0, Landroidx/room/r;->j:Landroid/content/ServiceConnection;

    new-instance v0, Landroidx/room/n;

    invoke-direct {v0, p0}, Landroidx/room/n;-><init>(Landroidx/room/r;)V

    iput-object v0, p0, Landroidx/room/r;->k:Ljava/lang/Runnable;

    new-instance v0, Landroidx/room/o;

    invoke-direct {v0, p0}, Landroidx/room/o;-><init>(Landroidx/room/r;)V

    iput-object v0, p0, Landroidx/room/r;->l:Ljava/lang/Runnable;

    new-instance v0, Landroidx/room/p;

    invoke-direct {v0, p0}, Landroidx/room/p;-><init>(Landroidx/room/r;)V

    iput-object v0, p0, Landroidx/room/r;->m:Ljava/lang/Runnable;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Landroidx/room/r;->a:Landroid/content/Context;

    iput-object p2, p0, Landroidx/room/r;->b:Ljava/lang/String;

    iput-object p3, p0, Landroidx/room/r;->d:Landroidx/room/j;

    iput-object p4, p0, Landroidx/room/r;->g:Ljava/util/concurrent/Executor;

    iget-object p1, p3, Landroidx/room/j;->b:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object p1

    new-instance p2, Landroidx/room/q;

    new-array p3, v1, [Ljava/lang/String;

    invoke-interface {p1, p3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/String;

    invoke-direct {p2, p0, p1}, Landroidx/room/q;-><init>(Landroidx/room/r;[Ljava/lang/String;)V

    iput-object p2, p0, Landroidx/room/r;->e:Landroidx/room/j$b;

    new-instance p1, Landroid/content/Intent;

    iget-object p2, p0, Landroidx/room/r;->a:Landroid/content/Context;

    const-class p3, Landroidx/room/MultiInstanceInvalidationService;

    invoke-direct {p1, p2, p3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object p2, p0, Landroidx/room/r;->a:Landroid/content/Context;

    iget-object p3, p0, Landroidx/room/r;->j:Landroid/content/ServiceConnection;

    const/4 p4, 0x1

    invoke-virtual {p2, p1, p3, p4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method
