.class Landroidx/room/n;
.super Ljava/lang/Object;
.source "MultiInstanceInvalidationClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/room/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroidx/room/r;


# direct methods
.method constructor <init>(Landroidx/room/r;)V
    .locals 0

    iput-object p1, p0, Landroidx/room/n;->a:Landroidx/room/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    :try_start_0
    iget-object v0, p0, Landroidx/room/n;->a:Landroidx/room/r;

    iget-object v0, v0, Landroidx/room/r;->f:Landroidx/room/g;

    if-eqz v0, :cond_0

    iget-object v1, p0, Landroidx/room/n;->a:Landroidx/room/r;

    iget-object v2, p0, Landroidx/room/n;->a:Landroidx/room/r;

    iget-object v2, v2, Landroidx/room/r;->h:Landroidx/room/f;

    iget-object v3, p0, Landroidx/room/n;->a:Landroidx/room/r;

    iget-object v3, v3, Landroidx/room/r;->b:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroidx/room/g;->a(Landroidx/room/f;Ljava/lang/String;)I

    move-result v0

    iput v0, v1, Landroidx/room/r;->c:I

    iget-object v0, p0, Landroidx/room/n;->a:Landroidx/room/r;

    iget-object v0, v0, Landroidx/room/r;->d:Landroidx/room/j;

    iget-object v1, p0, Landroidx/room/n;->a:Landroidx/room/r;

    iget-object v1, v1, Landroidx/room/r;->e:Landroidx/room/j$b;

    invoke-virtual {v0, v1}, Landroidx/room/j;->a(Landroidx/room/j$b;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "ROOM"

    const-string v2, "Cannot register multi-instance invalidation callback"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    return-void
.end method
