.class public abstract Landroidx/room/c;
.super Landroidx/room/B;
.source "EntityInsertionAdapter.java"


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$a;->c:Landroidx/annotation/RestrictTo$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroidx/room/B;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroidx/room/v;)V
    .locals 0

    invoke-direct {p0, p1}, Landroidx/room/B;-><init>(Landroidx/room/v;)V

    return-void
.end method


# virtual methods
.method protected abstract a(La/p/a/f;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La/p/a/f;",
            "TT;)V"
        }
    .end annotation
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    invoke-virtual {p0}, Landroidx/room/B;->a()La/p/a/f;

    move-result-object v0

    :try_start_0
    invoke-virtual {p0, v0, p1}, Landroidx/room/c;->a(La/p/a/f;Ljava/lang/Object;)V

    invoke-interface {v0}, La/p/a/f;->j()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0, v0}, Landroidx/room/B;->a(La/p/a/f;)V

    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {p0, v0}, Landroidx/room/B;->a(La/p/a/f;)V

    throw p1
.end method

.method public final a([Ljava/lang/Object;)[Ljava/lang/Long;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)[",
            "Ljava/lang/Long;"
        }
    .end annotation

    invoke-virtual {p0}, Landroidx/room/B;->a()La/p/a/f;

    move-result-object v0

    :try_start_0
    array-length v1, p1

    new-array v1, v1, [Ljava/lang/Long;

    array-length v2, p1

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v3, v2, :cond_0

    aget-object v5, p1, v3

    invoke-virtual {p0, v0, v5}, Landroidx/room/c;->a(La/p/a/f;Ljava/lang/Object;)V

    invoke-interface {v0}, La/p/a/f;->j()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v1, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v0}, Landroidx/room/B;->a(La/p/a/f;)V

    return-object v1

    :catchall_0
    move-exception p1

    invoke-virtual {p0, v0}, Landroidx/room/B;->a(La/p/a/f;)V

    throw p1
.end method
