.class public abstract Landroidx/work/w;
.super Ljava/lang/Object;
.source "WorkContinuation.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Landroidx/work/r;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public final a(Landroidx/work/p;)Landroidx/work/w;
    .locals 0
    .param p1    # Landroidx/work/p;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroidx/work/w;->a(Ljava/util/List;)Landroidx/work/w;

    move-result-object p1

    return-object p1
.end method

.method public abstract a(Ljava/util/List;)Landroidx/work/w;
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroidx/work/p;",
            ">;)",
            "Landroidx/work/w;"
        }
    .end annotation
.end method
