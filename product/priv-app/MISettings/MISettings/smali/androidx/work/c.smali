.class public final Landroidx/work/c;
.super Ljava/lang/Object;
.source "Configuration.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/work/c$b;,
        Landroidx/work/c$a;
    }
.end annotation


# instance fields
.field final a:Ljava/util/concurrent/Executor;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field final b:Ljava/util/concurrent/Executor;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field final c:Landroidx/work/C;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field final d:Landroidx/work/m;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field final e:Landroidx/work/v;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field final f:Landroidx/work/j;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field final g:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field final h:I

.field final i:I

.field final j:I

.field final k:I

.field private final l:Z


# direct methods
.method constructor <init>(Landroidx/work/c$a;)V
    .locals 2
    .param p1    # Landroidx/work/c$a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Landroidx/work/c$a;->a:Ljava/util/concurrent/Executor;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Landroidx/work/c;->a(Z)Ljava/util/concurrent/Executor;

    move-result-object v0

    iput-object v0, p0, Landroidx/work/c;->a:Ljava/util/concurrent/Executor;

    goto :goto_0

    :cond_0
    iput-object v0, p0, Landroidx/work/c;->a:Ljava/util/concurrent/Executor;

    :goto_0
    iget-object v0, p1, Landroidx/work/c$a;->d:Ljava/util/concurrent/Executor;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroidx/work/c;->l:Z

    invoke-direct {p0, v0}, Landroidx/work/c;->a(Z)Ljava/util/concurrent/Executor;

    move-result-object v0

    iput-object v0, p0, Landroidx/work/c;->b:Ljava/util/concurrent/Executor;

    goto :goto_1

    :cond_1
    iput-boolean v1, p0, Landroidx/work/c;->l:Z

    iput-object v0, p0, Landroidx/work/c;->b:Ljava/util/concurrent/Executor;

    :goto_1
    iget-object v0, p1, Landroidx/work/c$a;->b:Landroidx/work/C;

    if-nez v0, :cond_2

    invoke-static {}, Landroidx/work/C;->a()Landroidx/work/C;

    move-result-object v0

    iput-object v0, p0, Landroidx/work/c;->c:Landroidx/work/C;

    goto :goto_2

    :cond_2
    iput-object v0, p0, Landroidx/work/c;->c:Landroidx/work/C;

    :goto_2
    iget-object v0, p1, Landroidx/work/c$a;->c:Landroidx/work/m;

    if-nez v0, :cond_3

    invoke-static {}, Landroidx/work/m;->a()Landroidx/work/m;

    move-result-object v0

    iput-object v0, p0, Landroidx/work/c;->d:Landroidx/work/m;

    goto :goto_3

    :cond_3
    iput-object v0, p0, Landroidx/work/c;->d:Landroidx/work/m;

    :goto_3
    iget-object v0, p1, Landroidx/work/c$a;->e:Landroidx/work/v;

    if-nez v0, :cond_4

    new-instance v0, Landroidx/work/impl/a;

    invoke-direct {v0}, Landroidx/work/impl/a;-><init>()V

    iput-object v0, p0, Landroidx/work/c;->e:Landroidx/work/v;

    goto :goto_4

    :cond_4
    iput-object v0, p0, Landroidx/work/c;->e:Landroidx/work/v;

    :goto_4
    iget v0, p1, Landroidx/work/c$a;->h:I

    iput v0, p0, Landroidx/work/c;->h:I

    iget v0, p1, Landroidx/work/c$a;->i:I

    iput v0, p0, Landroidx/work/c;->i:I

    iget v0, p1, Landroidx/work/c$a;->j:I

    iput v0, p0, Landroidx/work/c;->j:I

    iget v0, p1, Landroidx/work/c$a;->k:I

    iput v0, p0, Landroidx/work/c;->k:I

    iget-object v0, p1, Landroidx/work/c$a;->f:Landroidx/work/j;

    iput-object v0, p0, Landroidx/work/c;->f:Landroidx/work/j;

    iget-object p1, p1, Landroidx/work/c$a;->g:Ljava/lang/String;

    iput-object p1, p0, Landroidx/work/c;->g:Ljava/lang/String;

    return-void
.end method

.method private a(Z)Ljava/util/concurrent/Executor;
    .locals 2
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x4

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0, p1}, Landroidx/work/c;->b(Z)Ljava/util/concurrent/ThreadFactory;

    move-result-object p1

    invoke-static {v0, p1}, Ljava/util/concurrent/Executors;->newFixedThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object p1

    return-object p1
.end method

.method private b(Z)Ljava/util/concurrent/ThreadFactory;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    new-instance v0, Landroidx/work/b;

    invoke-direct {v0, p0, p1}, Landroidx/work/b;-><init>(Landroidx/work/c;Z)V

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Landroidx/work/c;->g:Ljava/lang/String;

    return-object v0
.end method

.method public b()Landroidx/work/j;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation build Landroidx/annotation/RestrictTo;
        value = {
            .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
        }
    .end annotation

    iget-object v0, p0, Landroidx/work/c;->f:Landroidx/work/j;

    return-object v0
.end method

.method public c()Ljava/util/concurrent/Executor;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Landroidx/work/c;->a:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public d()Landroidx/work/m;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Landroidx/work/c;->d:Landroidx/work/m;

    return-object v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Landroidx/work/c;->j:I

    return v0
.end method

.method public f()I
    .locals 2
    .annotation build Landroidx/annotation/IntRange;
        from = 0x14L
        to = 0x32L
    .end annotation

    .annotation build Landroidx/annotation/RestrictTo;
        value = {
            .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
        }
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-ne v0, v1, :cond_0

    iget v0, p0, Landroidx/work/c;->k:I

    div-int/lit8 v0, v0, 0x2

    return v0

    :cond_0
    iget v0, p0, Landroidx/work/c;->k:I

    return v0
.end method

.method public g()I
    .locals 1

    iget v0, p0, Landroidx/work/c;->i:I

    return v0
.end method

.method public h()I
    .locals 1
    .annotation build Landroidx/annotation/RestrictTo;
        value = {
            .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
        }
    .end annotation

    iget v0, p0, Landroidx/work/c;->h:I

    return v0
.end method

.method public i()Landroidx/work/v;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Landroidx/work/c;->e:Landroidx/work/v;

    return-object v0
.end method

.method public j()Ljava/util/concurrent/Executor;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Landroidx/work/c;->b:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public k()Landroidx/work/C;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Landroidx/work/c;->c:Landroidx/work/C;

    return-object v0
.end method
