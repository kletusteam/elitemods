.class public final Landroidx/work/p;
.super Landroidx/work/z;
.source "OneTimeWorkRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/work/p$a;
    }
.end annotation


# direct methods
.method constructor <init>(Landroidx/work/p$a;)V
    .locals 2

    iget-object v0, p1, Landroidx/work/z$a;->b:Ljava/util/UUID;

    iget-object v1, p1, Landroidx/work/z$a;->c:Landroidx/work/impl/c/y;

    iget-object p1, p1, Landroidx/work/z$a;->d:Ljava/util/Set;

    invoke-direct {p0, v0, v1, p1}, Landroidx/work/z;-><init>(Ljava/util/UUID;Landroidx/work/impl/c/y;Ljava/util/Set;)V

    return-void
.end method

.method public static a(Ljava/lang/Class;)Landroidx/work/p;
    .locals 1
    .param p0    # Ljava/lang/Class;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Landroidx/work/ListenableWorker;",
            ">;)",
            "Landroidx/work/p;"
        }
    .end annotation

    new-instance v0, Landroidx/work/p$a;

    invoke-direct {v0, p0}, Landroidx/work/p$a;-><init>(Ljava/lang/Class;)V

    invoke-virtual {v0}, Landroidx/work/z$a;->a()Landroidx/work/z;

    move-result-object p0

    check-cast p0, Landroidx/work/p;

    return-object p0
.end method
