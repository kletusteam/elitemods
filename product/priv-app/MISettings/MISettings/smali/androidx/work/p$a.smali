.class public final Landroidx/work/p$a;
.super Landroidx/work/z$a;
.source "OneTimeWorkRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/work/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/work/z$a<",
        "Landroidx/work/p$a;",
        "Landroidx/work/p;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .param p1    # Ljava/lang/Class;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Landroidx/work/ListenableWorker;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Landroidx/work/z$a;-><init>(Ljava/lang/Class;)V

    iget-object p1, p0, Landroidx/work/z$a;->c:Landroidx/work/impl/c/y;

    const-class v0, Landroidx/work/OverwritingInputMerger;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Landroidx/work/impl/c/y;->f:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method b()Landroidx/work/p;
    .locals 2
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-boolean v0, p0, Landroidx/work/z$a;->a:Z

    if-eqz v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Landroidx/work/z$a;->c:Landroidx/work/impl/c/y;

    iget-object v0, v0, Landroidx/work/impl/c/y;->l:Landroidx/work/d;

    invoke-virtual {v0}, Landroidx/work/d;->h()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot set backoff criteria on an idle mode job"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    new-instance v0, Landroidx/work/p;

    invoke-direct {v0, p0}, Landroidx/work/p;-><init>(Landroidx/work/p$a;)V

    return-object v0
.end method

.method bridge synthetic b()Landroidx/work/z;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-virtual {p0}, Landroidx/work/p$a;->b()Landroidx/work/p;

    move-result-object v0

    return-object v0
.end method

.method c()Landroidx/work/p$a;
    .locals 0
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    return-object p0
.end method

.method bridge synthetic c()Landroidx/work/z$a;
    .locals 0
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-virtual {p0}, Landroidx/work/p$a;->c()Landroidx/work/p$a;

    return-object p0
.end method
