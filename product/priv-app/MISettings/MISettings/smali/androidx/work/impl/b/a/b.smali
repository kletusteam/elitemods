.class public Landroidx/work/impl/b/a/b;
.super Landroidx/work/impl/b/a/c;
.source "BatteryNotLowController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/work/impl/b/a/c<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroidx/work/impl/utils/b/a;)V
    .locals 0

    invoke-static {p1, p2}, Landroidx/work/impl/b/b/i;->a(Landroid/content/Context;Landroidx/work/impl/utils/b/a;)Landroidx/work/impl/b/b/i;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/work/impl/b/b/i;->b()Landroidx/work/impl/b/b/b;

    move-result-object p1

    invoke-direct {p0, p1}, Landroidx/work/impl/b/a/c;-><init>(Landroidx/work/impl/b/b/f;)V

    return-void
.end method


# virtual methods
.method a(Landroidx/work/impl/c/y;)Z
    .locals 0
    .param p1    # Landroidx/work/impl/c/y;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object p1, p1, Landroidx/work/impl/c/y;->l:Landroidx/work/d;

    invoke-virtual {p1}, Landroidx/work/d;->f()Z

    move-result p1

    return p1
.end method

.method a(Ljava/lang/Boolean;)Z
    .locals 0
    .param p1    # Ljava/lang/Boolean;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method bridge synthetic b(Ljava/lang/Object;)Z
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Landroidx/work/impl/b/a/b;->a(Ljava/lang/Boolean;)Z

    move-result p1

    return p1
.end method
