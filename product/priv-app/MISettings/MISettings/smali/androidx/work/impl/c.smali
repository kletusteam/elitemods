.class public Landroidx/work/impl/c;
.super Ljava/lang/Object;
.source "OperationImpl.java"

# interfaces
.implements Landroidx/work/r;


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
    }
.end annotation


# instance fields
.field private final a:Landroidx/lifecycle/r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/r<",
            "Landroidx/work/r$a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroidx/work/impl/utils/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/work/impl/utils/a/e<",
            "Landroidx/work/r$a$c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroidx/lifecycle/r;

    invoke-direct {v0}, Landroidx/lifecycle/r;-><init>()V

    iput-object v0, p0, Landroidx/work/impl/c;->a:Landroidx/lifecycle/r;

    invoke-static {}, Landroidx/work/impl/utils/a/e;->d()Landroidx/work/impl/utils/a/e;

    move-result-object v0

    iput-object v0, p0, Landroidx/work/impl/c;->b:Landroidx/work/impl/utils/a/e;

    sget-object v0, Landroidx/work/r;->b:Landroidx/work/r$a$b;

    invoke-virtual {p0, v0}, Landroidx/work/impl/c;->a(Landroidx/work/r$a;)V

    return-void
.end method


# virtual methods
.method public a(Landroidx/work/r$a;)V
    .locals 1
    .param p1    # Landroidx/work/r$a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Landroidx/work/impl/c;->a:Landroidx/lifecycle/r;

    invoke-virtual {v0, p1}, Landroidx/lifecycle/r;->a(Ljava/lang/Object;)V

    instance-of v0, p1, Landroidx/work/r$a$c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroidx/work/impl/c;->b:Landroidx/work/impl/utils/a/e;

    check-cast p1, Landroidx/work/r$a$c;

    invoke-virtual {v0, p1}, Landroidx/work/impl/utils/a/e;->b(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    instance-of v0, p1, Landroidx/work/r$a$a;

    if-eqz v0, :cond_1

    check-cast p1, Landroidx/work/r$a$a;

    iget-object v0, p0, Landroidx/work/impl/c;->b:Landroidx/work/impl/utils/a/e;

    invoke-virtual {p1}, Landroidx/work/r$a$a;->a()Ljava/lang/Throwable;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroidx/work/impl/utils/a/e;->a(Ljava/lang/Throwable;)Z

    :cond_1
    :goto_0
    return-void
.end method
