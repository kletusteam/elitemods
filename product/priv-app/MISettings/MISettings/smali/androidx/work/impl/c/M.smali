.class Landroidx/work/impl/c/M;
.super Landroidx/room/c;
.source "WorkTagDao_Impl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroidx/work/impl/c/N;-><init>(Landroidx/room/v;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/room/c<",
        "Landroidx/work/impl/c/K;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic d:Landroidx/work/impl/c/N;


# direct methods
.method constructor <init>(Landroidx/work/impl/c/N;Landroidx/room/v;)V
    .locals 0

    iput-object p1, p0, Landroidx/work/impl/c/M;->d:Landroidx/work/impl/c/N;

    invoke-direct {p0, p2}, Landroidx/room/c;-><init>(Landroidx/room/v;)V

    return-void
.end method


# virtual methods
.method public a(La/p/a/f;Landroidx/work/impl/c/K;)V
    .locals 2

    iget-object v0, p2, Landroidx/work/impl/c/K;->a:Ljava/lang/String;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    invoke-interface {p1, v1}, La/p/a/d;->a(I)V

    goto :goto_0

    :cond_0
    invoke-interface {p1, v1, v0}, La/p/a/d;->a(ILjava/lang/String;)V

    :goto_0
    iget-object p2, p2, Landroidx/work/impl/c/K;->b:Ljava/lang/String;

    const/4 v0, 0x2

    if-nez p2, :cond_1

    invoke-interface {p1, v0}, La/p/a/d;->a(I)V

    goto :goto_1

    :cond_1
    invoke-interface {p1, v0, p2}, La/p/a/d;->a(ILjava/lang/String;)V

    :goto_1
    return-void
.end method

.method public bridge synthetic a(La/p/a/f;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroidx/work/impl/c/K;

    invoke-virtual {p0, p1, p2}, Landroidx/work/impl/c/M;->a(La/p/a/f;Landroidx/work/impl/c/K;)V

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    const-string v0, "INSERT OR IGNORE INTO `WorkTag` (`tag`,`work_spec_id`) VALUES (?,?)"

    return-object v0
.end method
