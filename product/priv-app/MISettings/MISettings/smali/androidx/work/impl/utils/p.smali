.class Landroidx/work/impl/utils/p;
.super Ljava/lang/Object;
.source "WorkForegroundUpdater.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroidx/work/impl/utils/q;->a(Landroid/content/Context;Ljava/util/UUID;Landroidx/work/h;)Lcom/google/common/util/concurrent/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroidx/work/impl/utils/a/e;

.field final synthetic b:Ljava/util/UUID;

.field final synthetic c:Landroidx/work/h;

.field final synthetic d:Landroid/content/Context;

.field final synthetic e:Landroidx/work/impl/utils/q;


# direct methods
.method constructor <init>(Landroidx/work/impl/utils/q;Landroidx/work/impl/utils/a/e;Ljava/util/UUID;Landroidx/work/h;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Landroidx/work/impl/utils/p;->e:Landroidx/work/impl/utils/q;

    iput-object p2, p0, Landroidx/work/impl/utils/p;->a:Landroidx/work/impl/utils/a/e;

    iput-object p3, p0, Landroidx/work/impl/utils/p;->b:Ljava/util/UUID;

    iput-object p4, p0, Landroidx/work/impl/utils/p;->c:Landroidx/work/h;

    iput-object p5, p0, Landroidx/work/impl/utils/p;->d:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Landroidx/work/impl/utils/p;->a:Landroidx/work/impl/utils/a/e;

    invoke-virtual {v0}, Landroidx/work/impl/utils/a/b;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Landroidx/work/impl/utils/p;->b:Ljava/util/UUID;

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Landroidx/work/impl/utils/p;->e:Landroidx/work/impl/utils/q;

    iget-object v1, v1, Landroidx/work/impl/utils/q;->d:Landroidx/work/impl/c/z;

    invoke-interface {v1, v0}, Landroidx/work/impl/c/z;->c(Ljava/lang/String;)Landroidx/work/x$a;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroidx/work/x$a;->a()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Landroidx/work/impl/utils/p;->e:Landroidx/work/impl/utils/q;

    iget-object v1, v1, Landroidx/work/impl/utils/q;->c:Landroidx/work/impl/foreground/a;

    iget-object v2, p0, Landroidx/work/impl/utils/p;->c:Landroidx/work/h;

    invoke-interface {v1, v0, v2}, Landroidx/work/impl/foreground/a;->a(Ljava/lang/String;Landroidx/work/h;)V

    iget-object v1, p0, Landroidx/work/impl/utils/p;->d:Landroid/content/Context;

    iget-object v2, p0, Landroidx/work/impl/utils/p;->c:Landroidx/work/h;

    invoke-static {v1, v0, v2}, Landroidx/work/impl/foreground/c;->a(Landroid/content/Context;Ljava/lang/String;Landroidx/work/h;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Landroidx/work/impl/utils/p;->d:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    :cond_0
    const-string v0, "Calls to setForegroundAsync() must complete before a ListenableWorker signals completion of work by returning an instance of Result."

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    :goto_0
    iget-object v0, p0, Landroidx/work/impl/utils/p;->a:Landroidx/work/impl/utils/a/e;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/work/impl/utils/a/e;->b(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    iget-object v1, p0, Landroidx/work/impl/utils/p;->a:Landroidx/work/impl/utils/a/e;

    invoke-virtual {v1, v0}, Landroidx/work/impl/utils/a/e;->a(Ljava/lang/Throwable;)Z

    :goto_1
    return-void
.end method
