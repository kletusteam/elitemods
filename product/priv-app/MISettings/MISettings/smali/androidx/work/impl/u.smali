.class Landroidx/work/impl/u;
.super Ljava/lang/Object;
.source "WorkerWrapper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroidx/work/impl/w;->h()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/common/util/concurrent/a;

.field final synthetic b:Landroidx/work/impl/utils/a/e;

.field final synthetic c:Landroidx/work/impl/w;


# direct methods
.method constructor <init>(Landroidx/work/impl/w;Lcom/google/common/util/concurrent/a;Landroidx/work/impl/utils/a/e;)V
    .locals 0

    iput-object p1, p0, Landroidx/work/impl/u;->c:Landroidx/work/impl/w;

    iput-object p2, p0, Landroidx/work/impl/u;->a:Lcom/google/common/util/concurrent/a;

    iput-object p3, p0, Landroidx/work/impl/u;->b:Landroidx/work/impl/utils/a/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    :try_start_0
    iget-object v0, p0, Landroidx/work/impl/u;->a:Lcom/google/common/util/concurrent/a;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    invoke-static {}, Landroidx/work/n;->a()Landroidx/work/n;

    move-result-object v0

    sget-object v1, Landroidx/work/impl/w;->a:Ljava/lang/String;

    const-string v2, "Starting work for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Landroidx/work/impl/u;->c:Landroidx/work/impl/w;

    iget-object v4, v4, Landroidx/work/impl/w;->f:Landroidx/work/impl/c/y;

    iget-object v4, v4, Landroidx/work/impl/c/y;->e:Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Throwable;

    invoke-virtual {v0, v1, v2, v3}, Landroidx/work/n;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Throwable;)V

    iget-object v0, p0, Landroidx/work/impl/u;->c:Landroidx/work/impl/w;

    iget-object v1, p0, Landroidx/work/impl/u;->c:Landroidx/work/impl/w;

    iget-object v1, v1, Landroidx/work/impl/w;->g:Landroidx/work/ListenableWorker;

    invoke-virtual {v1}, Landroidx/work/ListenableWorker;->l()Lcom/google/common/util/concurrent/a;

    move-result-object v1

    iput-object v1, v0, Landroidx/work/impl/w;->s:Lcom/google/common/util/concurrent/a;

    iget-object v0, p0, Landroidx/work/impl/u;->b:Landroidx/work/impl/utils/a/e;

    iget-object v1, p0, Landroidx/work/impl/u;->c:Landroidx/work/impl/w;

    iget-object v1, v1, Landroidx/work/impl/w;->s:Lcom/google/common/util/concurrent/a;

    invoke-virtual {v0, v1}, Landroidx/work/impl/utils/a/e;->b(Lcom/google/common/util/concurrent/a;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Landroidx/work/impl/u;->b:Landroidx/work/impl/utils/a/e;

    invoke-virtual {v1, v0}, Landroidx/work/impl/utils/a/e;->a(Ljava/lang/Throwable;)Z

    :goto_0
    return-void
.end method
