.class Landroidx/work/impl/utils/b;
.super Landroidx/work/impl/utils/c;
.source "CancelWorkRunnable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroidx/work/impl/utils/c;->a(Ljava/lang/String;Landroidx/work/impl/t;Z)Landroidx/work/impl/utils/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic b:Landroidx/work/impl/t;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Z


# direct methods
.method constructor <init>(Landroidx/work/impl/t;Ljava/lang/String;Z)V
    .locals 0

    iput-object p1, p0, Landroidx/work/impl/utils/b;->b:Landroidx/work/impl/t;

    iput-object p2, p0, Landroidx/work/impl/utils/b;->c:Ljava/lang/String;

    iput-boolean p3, p0, Landroidx/work/impl/utils/b;->d:Z

    invoke-direct {p0}, Landroidx/work/impl/utils/c;-><init>()V

    return-void
.end method


# virtual methods
.method b()V
    .locals 4
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, Landroidx/work/impl/utils/b;->b:Landroidx/work/impl/t;

    invoke-virtual {v0}, Landroidx/work/impl/t;->g()Landroidx/work/impl/WorkDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/room/v;->c()V

    :try_start_0
    invoke-virtual {v0}, Landroidx/work/impl/WorkDatabase;->t()Landroidx/work/impl/c/z;

    move-result-object v1

    iget-object v2, p0, Landroidx/work/impl/utils/b;->c:Ljava/lang/String;

    invoke-interface {v1, v2}, Landroidx/work/impl/c/z;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Landroidx/work/impl/utils/b;->b:Landroidx/work/impl/t;

    invoke-virtual {p0, v3, v2}, Landroidx/work/impl/utils/c;->a(Landroidx/work/impl/t;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroidx/room/v;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Landroidx/room/v;->e()V

    iget-boolean v0, p0, Landroidx/work/impl/utils/b;->d:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroidx/work/impl/utils/b;->b:Landroidx/work/impl/t;

    invoke-virtual {p0, v0}, Landroidx/work/impl/utils/c;->a(Landroidx/work/impl/t;)V

    :cond_1
    return-void

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroidx/room/v;->e()V

    throw v1
.end method
