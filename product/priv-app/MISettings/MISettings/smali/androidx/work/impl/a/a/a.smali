.class Landroidx/work/impl/a/a/a;
.super Ljava/lang/Object;
.source "DelayedWorkTracker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroidx/work/impl/a/a/b;->a(Landroidx/work/impl/c/y;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroidx/work/impl/c/y;

.field final synthetic b:Landroidx/work/impl/a/a/b;


# direct methods
.method constructor <init>(Landroidx/work/impl/a/a/b;Landroidx/work/impl/c/y;)V
    .locals 0

    iput-object p1, p0, Landroidx/work/impl/a/a/a;->b:Landroidx/work/impl/a/a/b;

    iput-object p2, p0, Landroidx/work/impl/a/a/a;->a:Landroidx/work/impl/c/y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    invoke-static {}, Landroidx/work/n;->a()Landroidx/work/n;

    move-result-object v0

    sget-object v1, Landroidx/work/impl/a/a/b;->a:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    iget-object v4, p0, Landroidx/work/impl/a/a/a;->a:Landroidx/work/impl/c/y;

    iget-object v4, v4, Landroidx/work/impl/c/y;->c:Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const-string v4, "Scheduling work %s"

    invoke-static {v4, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Throwable;

    invoke-virtual {v0, v1, v3, v4}, Landroidx/work/n;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Throwable;)V

    iget-object v0, p0, Landroidx/work/impl/a/a/a;->b:Landroidx/work/impl/a/a/b;

    iget-object v0, v0, Landroidx/work/impl/a/a/b;->b:Landroidx/work/impl/a/a/c;

    new-array v1, v2, [Landroidx/work/impl/c/y;

    iget-object v2, p0, Landroidx/work/impl/a/a/a;->a:Landroidx/work/impl/c/y;

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Landroidx/work/impl/a/a/c;->a([Landroidx/work/impl/c/y;)V

    return-void
.end method
