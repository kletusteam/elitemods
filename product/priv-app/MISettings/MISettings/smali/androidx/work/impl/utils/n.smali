.class Landroidx/work/impl/utils/n;
.super Ljava/lang/Object;
.source "WorkForegroundRunnable.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroidx/work/impl/utils/o;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroidx/work/impl/utils/a/e;

.field final synthetic b:Landroidx/work/impl/utils/o;


# direct methods
.method constructor <init>(Landroidx/work/impl/utils/o;Landroidx/work/impl/utils/a/e;)V
    .locals 0

    iput-object p1, p0, Landroidx/work/impl/utils/n;->b:Landroidx/work/impl/utils/o;

    iput-object p2, p0, Landroidx/work/impl/utils/n;->a:Landroidx/work/impl/utils/a/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    :try_start_0
    iget-object v0, p0, Landroidx/work/impl/utils/n;->a:Landroidx/work/impl/utils/a/e;

    invoke-virtual {v0}, Landroidx/work/impl/utils/a/b;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/work/h;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    invoke-static {}, Landroidx/work/n;->a()Landroidx/work/n;

    move-result-object v3

    sget-object v4, Landroidx/work/impl/utils/o;->a:Ljava/lang/String;

    const-string v5, "Updating notification for %s"

    new-array v6, v2, [Ljava/lang/Object;

    iget-object v7, p0, Landroidx/work/impl/utils/n;->b:Landroidx/work/impl/utils/o;

    iget-object v7, v7, Landroidx/work/impl/utils/o;->d:Landroidx/work/impl/c/y;

    iget-object v7, v7, Landroidx/work/impl/c/y;->e:Ljava/lang/String;

    aput-object v7, v6, v1

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-array v1, v1, [Ljava/lang/Throwable;

    invoke-virtual {v3, v4, v5, v1}, Landroidx/work/n;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Throwable;)V

    iget-object v1, p0, Landroidx/work/impl/utils/n;->b:Landroidx/work/impl/utils/o;

    iget-object v1, v1, Landroidx/work/impl/utils/o;->e:Landroidx/work/ListenableWorker;

    invoke-virtual {v1, v2}, Landroidx/work/ListenableWorker;->a(Z)V

    iget-object v1, p0, Landroidx/work/impl/utils/n;->b:Landroidx/work/impl/utils/o;

    iget-object v1, v1, Landroidx/work/impl/utils/o;->b:Landroidx/work/impl/utils/a/e;

    iget-object v2, p0, Landroidx/work/impl/utils/n;->b:Landroidx/work/impl/utils/o;

    iget-object v2, v2, Landroidx/work/impl/utils/o;->f:Landroidx/work/i;

    iget-object v3, p0, Landroidx/work/impl/utils/n;->b:Landroidx/work/impl/utils/o;

    iget-object v3, v3, Landroidx/work/impl/utils/o;->c:Landroid/content/Context;

    iget-object v4, p0, Landroidx/work/impl/utils/n;->b:Landroidx/work/impl/utils/o;

    iget-object v4, v4, Landroidx/work/impl/utils/o;->e:Landroidx/work/ListenableWorker;

    invoke-virtual {v4}, Landroidx/work/ListenableWorker;->d()Ljava/util/UUID;

    move-result-object v4

    invoke-interface {v2, v3, v4, v0}, Landroidx/work/i;->a(Landroid/content/Context;Ljava/util/UUID;Landroidx/work/h;)Lcom/google/common/util/concurrent/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroidx/work/impl/utils/a/e;->b(Lcom/google/common/util/concurrent/a;)Z

    goto :goto_0

    :cond_0
    const-string v0, "Worker was marked important (%s) but did not provide ForegroundInfo"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Landroidx/work/impl/utils/n;->b:Landroidx/work/impl/utils/o;

    iget-object v3, v3, Landroidx/work/impl/utils/o;->d:Landroidx/work/impl/c/y;

    iget-object v3, v3, Landroidx/work/impl/c/y;->e:Ljava/lang/String;

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Landroidx/work/impl/utils/n;->b:Landroidx/work/impl/utils/o;

    iget-object v1, v1, Landroidx/work/impl/utils/o;->b:Landroidx/work/impl/utils/a/e;

    invoke-virtual {v1, v0}, Landroidx/work/impl/utils/a/e;->a(Ljava/lang/Throwable;)Z

    :goto_0
    return-void
.end method
