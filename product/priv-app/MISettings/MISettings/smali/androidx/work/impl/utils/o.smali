.class public Landroidx/work/impl/utils/o;
.super Ljava/lang/Object;
.source "WorkForegroundRunnable.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
    }
.end annotation


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Landroidx/work/impl/utils/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/work/impl/utils/a/e<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field final c:Landroid/content/Context;

.field final d:Landroidx/work/impl/c/y;

.field final e:Landroidx/work/ListenableWorker;

.field final f:Landroidx/work/i;

.field final g:Landroidx/work/impl/utils/b/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "WorkForegroundRunnable"

    invoke-static {v0}, Landroidx/work/n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroidx/work/impl/utils/o;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroidx/work/impl/c/y;Landroidx/work/ListenableWorker;Landroidx/work/i;Landroidx/work/impl/utils/b/a;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroidx/work/impl/c/y;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroidx/work/ListenableWorker;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Landroidx/work/i;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Landroidx/work/impl/utils/b/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "LambdaLast"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroidx/work/impl/utils/a/e;->d()Landroidx/work/impl/utils/a/e;

    move-result-object v0

    iput-object v0, p0, Landroidx/work/impl/utils/o;->b:Landroidx/work/impl/utils/a/e;

    iput-object p1, p0, Landroidx/work/impl/utils/o;->c:Landroid/content/Context;

    iput-object p2, p0, Landroidx/work/impl/utils/o;->d:Landroidx/work/impl/c/y;

    iput-object p3, p0, Landroidx/work/impl/utils/o;->e:Landroidx/work/ListenableWorker;

    iput-object p4, p0, Landroidx/work/impl/utils/o;->f:Landroidx/work/i;

    iput-object p5, p0, Landroidx/work/impl/utils/o;->g:Landroidx/work/impl/utils/b/a;

    return-void
.end method


# virtual methods
.method public a()Lcom/google/common/util/concurrent/a;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/a<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Landroidx/work/impl/utils/o;->b:Landroidx/work/impl/utils/a/e;

    return-object v0
.end method

.method public run()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UnsafeExperimentalUsageError"
        }
    .end annotation

    iget-object v0, p0, Landroidx/work/impl/utils/o;->d:Landroidx/work/impl/c/y;

    iget-boolean v0, v0, Landroidx/work/impl/c/y;->s:Z

    if-eqz v0, :cond_1

    invoke-static {}, Landroidx/core/os/BuildCompat;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Landroidx/work/impl/utils/a/e;->d()Landroidx/work/impl/utils/a/e;

    move-result-object v0

    iget-object v1, p0, Landroidx/work/impl/utils/o;->g:Landroidx/work/impl/utils/b/a;

    invoke-interface {v1}, Landroidx/work/impl/utils/b/a;->a()Ljava/util/concurrent/Executor;

    move-result-object v1

    new-instance v2, Landroidx/work/impl/utils/m;

    invoke-direct {v2, p0, v0}, Landroidx/work/impl/utils/m;-><init>(Landroidx/work/impl/utils/o;Landroidx/work/impl/utils/a/e;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    new-instance v1, Landroidx/work/impl/utils/n;

    invoke-direct {v1, p0, v0}, Landroidx/work/impl/utils/n;-><init>(Landroidx/work/impl/utils/o;Landroidx/work/impl/utils/a/e;)V

    iget-object v2, p0, Landroidx/work/impl/utils/o;->g:Landroidx/work/impl/utils/b/a;

    invoke-interface {v2}, Landroidx/work/impl/utils/b/a;->a()Ljava/util/concurrent/Executor;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroidx/work/impl/utils/a/b;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    return-void

    :cond_1
    :goto_0
    iget-object v0, p0, Landroidx/work/impl/utils/o;->b:Landroidx/work/impl/utils/a/e;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/work/impl/utils/a/e;->b(Ljava/lang/Object;)Z

    return-void
.end method
