.class public Landroidx/work/impl/q$b;
.super Landroidx/room/a/a;
.source "WorkDatabaseMigrations.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/work/impl/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const/16 v0, 0x9

    const/16 v1, 0xa

    invoke-direct {p0, v0, v1}, Landroidx/room/a/a;-><init>(II)V

    iput-object p1, p0, Landroidx/work/impl/q$b;->c:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public a(La/p/a/b;)V
    .locals 1
    .param p1    # La/p/a/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    const-string v0, "CREATE TABLE IF NOT EXISTS `Preference` (`key` TEXT NOT NULL, `long_value` INTEGER, PRIMARY KEY(`key`))"

    invoke-interface {p1, v0}, La/p/a/b;->b(Ljava/lang/String;)V

    iget-object v0, p0, Landroidx/work/impl/q$b;->c:Landroid/content/Context;

    invoke-static {v0, p1}, Landroidx/work/impl/utils/g;->a(Landroid/content/Context;La/p/a/b;)V

    iget-object v0, p0, Landroidx/work/impl/q$b;->c:Landroid/content/Context;

    invoke-static {v0, p1}, Landroidx/work/impl/utils/e;->a(Landroid/content/Context;La/p/a/b;)V

    return-void
.end method
