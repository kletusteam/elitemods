.class public abstract Landroidx/work/impl/WorkDatabase;
.super Landroidx/room/v;
.source "WorkDatabase.java"


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
    }
.end annotation

.annotation build Landroidx/room/Database;
    entities = {
        Landroidx/work/impl/c/a;,
        Landroidx/work/impl/c/y;,
        Landroidx/work/impl/c/K;,
        Landroidx/work/impl/c/i;,
        Landroidx/work/impl/c/n;,
        Landroidx/work/impl/c/r;,
        Landroidx/work/impl/c/e;
    }
    version = 0xc
.end annotation

.annotation build Landroidx/room/TypeConverters;
    value = {
        Landroidx/work/f;,
        Landroidx/work/impl/c/P;
    }
.end annotation


# static fields
.field private static final l:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Landroidx/work/impl/WorkDatabase;->l:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroidx/room/v;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/util/concurrent/Executor;Z)Landroidx/work/impl/WorkDatabase;
    .locals 5
    .param p0    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    if-eqz p2, :cond_0

    const-class p2, Landroidx/work/impl/WorkDatabase;

    invoke-static {p0, p2}, Landroidx/room/u;->a(Landroid/content/Context;Ljava/lang/Class;)Landroidx/room/v$a;

    move-result-object p2

    invoke-virtual {p2}, Landroidx/room/v$a;->a()Landroidx/room/v$a;

    goto :goto_0

    :cond_0
    invoke-static {}, Landroidx/work/impl/r;->a()Ljava/lang/String;

    move-result-object p2

    const-class v0, Landroidx/work/impl/WorkDatabase;

    invoke-static {p0, v0, p2}, Landroidx/room/u;->a(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;)Landroidx/room/v$a;

    move-result-object p2

    new-instance v0, Landroidx/work/impl/h;

    invoke-direct {v0, p0}, Landroidx/work/impl/h;-><init>(Landroid/content/Context;)V

    invoke-virtual {p2, v0}, Landroidx/room/v$a;->a(La/p/a/c$c;)Landroidx/room/v$a;

    :goto_0
    invoke-virtual {p2, p1}, Landroidx/room/v$a;->a(Ljava/util/concurrent/Executor;)Landroidx/room/v$a;

    invoke-static {}, Landroidx/work/impl/WorkDatabase;->m()Landroidx/room/v$b;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroidx/room/v$a;->a(Landroidx/room/v$b;)Landroidx/room/v$a;

    const/4 p1, 0x1

    new-array v0, p1, [Landroidx/room/a/a;

    sget-object v1, Landroidx/work/impl/q;->a:Landroidx/room/a/a;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p2, v0}, Landroidx/room/v$a;->a([Landroidx/room/a/a;)Landroidx/room/v$a;

    new-array v0, p1, [Landroidx/room/a/a;

    new-instance v1, Landroidx/work/impl/q$a;

    const/4 v3, 0x2

    const/4 v4, 0x3

    invoke-direct {v1, p0, v3, v4}, Landroidx/work/impl/q$a;-><init>(Landroid/content/Context;II)V

    aput-object v1, v0, v2

    invoke-virtual {p2, v0}, Landroidx/room/v$a;->a([Landroidx/room/a/a;)Landroidx/room/v$a;

    new-array v0, p1, [Landroidx/room/a/a;

    sget-object v1, Landroidx/work/impl/q;->b:Landroidx/room/a/a;

    aput-object v1, v0, v2

    invoke-virtual {p2, v0}, Landroidx/room/v$a;->a([Landroidx/room/a/a;)Landroidx/room/v$a;

    new-array v0, p1, [Landroidx/room/a/a;

    sget-object v1, Landroidx/work/impl/q;->c:Landroidx/room/a/a;

    aput-object v1, v0, v2

    invoke-virtual {p2, v0}, Landroidx/room/v$a;->a([Landroidx/room/a/a;)Landroidx/room/v$a;

    new-array v0, p1, [Landroidx/room/a/a;

    new-instance v1, Landroidx/work/impl/q$a;

    const/4 v3, 0x5

    const/4 v4, 0x6

    invoke-direct {v1, p0, v3, v4}, Landroidx/work/impl/q$a;-><init>(Landroid/content/Context;II)V

    aput-object v1, v0, v2

    invoke-virtual {p2, v0}, Landroidx/room/v$a;->a([Landroidx/room/a/a;)Landroidx/room/v$a;

    new-array v0, p1, [Landroidx/room/a/a;

    sget-object v1, Landroidx/work/impl/q;->d:Landroidx/room/a/a;

    aput-object v1, v0, v2

    invoke-virtual {p2, v0}, Landroidx/room/v$a;->a([Landroidx/room/a/a;)Landroidx/room/v$a;

    new-array v0, p1, [Landroidx/room/a/a;

    sget-object v1, Landroidx/work/impl/q;->e:Landroidx/room/a/a;

    aput-object v1, v0, v2

    invoke-virtual {p2, v0}, Landroidx/room/v$a;->a([Landroidx/room/a/a;)Landroidx/room/v$a;

    new-array v0, p1, [Landroidx/room/a/a;

    sget-object v1, Landroidx/work/impl/q;->f:Landroidx/room/a/a;

    aput-object v1, v0, v2

    invoke-virtual {p2, v0}, Landroidx/room/v$a;->a([Landroidx/room/a/a;)Landroidx/room/v$a;

    new-array v0, p1, [Landroidx/room/a/a;

    new-instance v1, Landroidx/work/impl/q$b;

    invoke-direct {v1, p0}, Landroidx/work/impl/q$b;-><init>(Landroid/content/Context;)V

    aput-object v1, v0, v2

    invoke-virtual {p2, v0}, Landroidx/room/v$a;->a([Landroidx/room/a/a;)Landroidx/room/v$a;

    new-array v0, p1, [Landroidx/room/a/a;

    new-instance v1, Landroidx/work/impl/q$a;

    const/16 v3, 0xa

    const/16 v4, 0xb

    invoke-direct {v1, p0, v3, v4}, Landroidx/work/impl/q$a;-><init>(Landroid/content/Context;II)V

    aput-object v1, v0, v2

    invoke-virtual {p2, v0}, Landroidx/room/v$a;->a([Landroidx/room/a/a;)Landroidx/room/v$a;

    new-array p0, p1, [Landroidx/room/a/a;

    sget-object p1, Landroidx/work/impl/q;->g:Landroidx/room/a/a;

    aput-object p1, p0, v2

    invoke-virtual {p2, p0}, Landroidx/room/v$a;->a([Landroidx/room/a/a;)Landroidx/room/v$a;

    invoke-virtual {p2}, Landroidx/room/v$a;->c()Landroidx/room/v$a;

    invoke-virtual {p2}, Landroidx/room/v$a;->b()Landroidx/room/v;

    move-result-object p0

    check-cast p0, Landroidx/work/impl/WorkDatabase;

    return-object p0
.end method

.method static m()Landroidx/room/v$b;
    .locals 1

    new-instance v0, Landroidx/work/impl/i;

    invoke-direct {v0}, Landroidx/work/impl/i;-><init>()V

    return-object v0
.end method

.method static n()J
    .locals 4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-wide v2, Landroidx/work/impl/WorkDatabase;->l:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method static o()Ljava/lang/String;
    .locals 3
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DELETE FROM workspec WHERE state IN (2, 3, 5) AND (period_start_time + minimum_retention_duration) < "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Landroidx/work/impl/WorkDatabase;->n()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, " AND (SELECT COUNT(*)=0 FROM dependency WHERE     prerequisite_id=id AND     work_spec_id NOT IN         (SELECT id FROM workspec WHERE state IN (2, 3, 5)))"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract l()Landroidx/work/impl/c/b;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract p()Landroidx/work/impl/c/f;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract q()Landroidx/work/impl/c/j;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract r()Landroidx/work/impl/c/o;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract s()Landroidx/work/impl/c/s;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract t()Landroidx/work/impl/c/z;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method

.method public abstract u()Landroidx/work/impl/c/L;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end method
