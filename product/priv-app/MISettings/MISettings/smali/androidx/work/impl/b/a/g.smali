.class public Landroidx/work/impl/b/a/g;
.super Landroidx/work/impl/b/a/c;
.source "NetworkUnmeteredController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/work/impl/b/a/c<",
        "Landroidx/work/impl/b/b;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroidx/work/impl/utils/b/a;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroidx/work/impl/utils/b/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-static {p1, p2}, Landroidx/work/impl/b/b/i;->a(Landroid/content/Context;Landroidx/work/impl/utils/b/a;)Landroidx/work/impl/b/b/i;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/work/impl/b/b/i;->c()Landroidx/work/impl/b/b/g;

    move-result-object p1

    invoke-direct {p0, p1}, Landroidx/work/impl/b/a/c;-><init>(Landroidx/work/impl/b/b/f;)V

    return-void
.end method


# virtual methods
.method a(Landroidx/work/impl/b/b;)Z
    .locals 1
    .param p1    # Landroidx/work/impl/b/b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-virtual {p1}, Landroidx/work/impl/b/b;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroidx/work/impl/b/b;->b()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method a(Landroidx/work/impl/c/y;)Z
    .locals 2
    .param p1    # Landroidx/work/impl/c/y;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p1, Landroidx/work/impl/c/y;->l:Landroidx/work/d;

    invoke-virtual {v0}, Landroidx/work/d;->b()Landroidx/work/o;

    move-result-object v0

    sget-object v1, Landroidx/work/o;->c:Landroidx/work/o;

    if-eq v0, v1, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1e

    if-lt v0, v1, :cond_0

    iget-object p1, p1, Landroidx/work/impl/c/y;->l:Landroidx/work/d;

    invoke-virtual {p1}, Landroidx/work/d;->b()Landroidx/work/o;

    move-result-object p1

    sget-object v0, Landroidx/work/o;->f:Landroidx/work/o;

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method bridge synthetic b(Ljava/lang/Object;)Z
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    check-cast p1, Landroidx/work/impl/b/b;

    invoke-virtual {p0, p1}, Landroidx/work/impl/b/a/g;->a(Landroidx/work/impl/b/b;)Z

    move-result p1

    return p1
.end method
