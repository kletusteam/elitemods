.class public Landroidx/work/impl/utils/b/c;
.super Ljava/lang/Object;
.source "WorkManagerTaskExecutor.java"

# interfaces
.implements Landroidx/work/impl/utils/b/a;


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
    }
.end annotation


# instance fields
.field private final a:Landroidx/work/impl/utils/i;

.field private final b:Landroid/os/Handler;

.field private final c:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 2
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Landroidx/work/impl/utils/b/c;->b:Landroid/os/Handler;

    new-instance v0, Landroidx/work/impl/utils/b/b;

    invoke-direct {v0, p0}, Landroidx/work/impl/utils/b/b;-><init>(Landroidx/work/impl/utils/b/c;)V

    iput-object v0, p0, Landroidx/work/impl/utils/b/c;->c:Ljava/util/concurrent/Executor;

    new-instance v0, Landroidx/work/impl/utils/i;

    invoke-direct {v0, p1}, Landroidx/work/impl/utils/i;-><init>(Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Landroidx/work/impl/utils/b/c;->a:Landroidx/work/impl/utils/i;

    return-void
.end method


# virtual methods
.method public a()Ljava/util/concurrent/Executor;
    .locals 1

    iget-object v0, p0, Landroidx/work/impl/utils/b/c;->c:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 1

    iget-object v0, p0, Landroidx/work/impl/utils/b/c;->a:Landroidx/work/impl/utils/i;

    invoke-virtual {v0, p1}, Landroidx/work/impl/utils/i;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public b()Landroidx/work/impl/utils/i;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Landroidx/work/impl/utils/b/c;->a:Landroidx/work/impl/utils/i;

    return-object v0
.end method

.method public b(Ljava/lang/Runnable;)V
    .locals 1

    iget-object v0, p0, Landroidx/work/impl/utils/b/c;->b:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
