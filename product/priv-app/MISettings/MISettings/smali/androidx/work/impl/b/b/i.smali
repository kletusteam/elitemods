.class public Landroidx/work/impl/b/b/i;
.super Ljava/lang/Object;
.source "Trackers.java"


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
    }
.end annotation


# static fields
.field private static a:Landroidx/work/impl/b/b/i;


# instance fields
.field private b:Landroidx/work/impl/b/b/a;

.field private c:Landroidx/work/impl/b/b/b;

.field private d:Landroidx/work/impl/b/b/g;

.field private e:Landroidx/work/impl/b/b/h;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroidx/work/impl/utils/b/a;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroidx/work/impl/utils/b/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    new-instance v0, Landroidx/work/impl/b/b/a;

    invoke-direct {v0, p1, p2}, Landroidx/work/impl/b/b/a;-><init>(Landroid/content/Context;Landroidx/work/impl/utils/b/a;)V

    iput-object v0, p0, Landroidx/work/impl/b/b/i;->b:Landroidx/work/impl/b/b/a;

    new-instance v0, Landroidx/work/impl/b/b/b;

    invoke-direct {v0, p1, p2}, Landroidx/work/impl/b/b/b;-><init>(Landroid/content/Context;Landroidx/work/impl/utils/b/a;)V

    iput-object v0, p0, Landroidx/work/impl/b/b/i;->c:Landroidx/work/impl/b/b/b;

    new-instance v0, Landroidx/work/impl/b/b/g;

    invoke-direct {v0, p1, p2}, Landroidx/work/impl/b/b/g;-><init>(Landroid/content/Context;Landroidx/work/impl/utils/b/a;)V

    iput-object v0, p0, Landroidx/work/impl/b/b/i;->d:Landroidx/work/impl/b/b/g;

    new-instance v0, Landroidx/work/impl/b/b/h;

    invoke-direct {v0, p1, p2}, Landroidx/work/impl/b/b/h;-><init>(Landroid/content/Context;Landroidx/work/impl/utils/b/a;)V

    iput-object v0, p0, Landroidx/work/impl/b/b/i;->e:Landroidx/work/impl/b/b/h;

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;Landroidx/work/impl/utils/b/a;)Landroidx/work/impl/b/b/i;
    .locals 2
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    const-class v0, Landroidx/work/impl/b/b/i;

    monitor-enter v0

    :try_start_0
    sget-object v1, Landroidx/work/impl/b/b/i;->a:Landroidx/work/impl/b/b/i;

    if-nez v1, :cond_0

    new-instance v1, Landroidx/work/impl/b/b/i;

    invoke-direct {v1, p0, p1}, Landroidx/work/impl/b/b/i;-><init>(Landroid/content/Context;Landroidx/work/impl/utils/b/a;)V

    sput-object v1, Landroidx/work/impl/b/b/i;->a:Landroidx/work/impl/b/b/i;

    :cond_0
    sget-object p0, Landroidx/work/impl/b/b/i;->a:Landroidx/work/impl/b/b/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method


# virtual methods
.method public a()Landroidx/work/impl/b/b/a;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Landroidx/work/impl/b/b/i;->b:Landroidx/work/impl/b/b/a;

    return-object v0
.end method

.method public b()Landroidx/work/impl/b/b/b;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Landroidx/work/impl/b/b/i;->c:Landroidx/work/impl/b/b/b;

    return-object v0
.end method

.method public c()Landroidx/work/impl/b/b/g;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Landroidx/work/impl/b/b/i;->d:Landroidx/work/impl/b/b/g;

    return-object v0
.end method

.method public d()Landroidx/work/impl/b/b/h;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Landroidx/work/impl/b/b/i;->e:Landroidx/work/impl/b/b/h;

    return-object v0
.end method
