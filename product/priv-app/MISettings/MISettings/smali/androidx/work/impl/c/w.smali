.class public final Landroidx/work/impl/c/w;
.super Ljava/lang/Object;
.source "WorkProgressDao_Impl.java"

# interfaces
.implements Landroidx/work/impl/c/s;


# instance fields
.field private final a:Landroidx/room/v;

.field private final b:Landroidx/room/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/room/c<",
            "Landroidx/work/impl/c/r;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroidx/room/B;

.field private final d:Landroidx/room/B;


# direct methods
.method public constructor <init>(Landroidx/room/v;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroidx/work/impl/c/w;->a:Landroidx/room/v;

    new-instance v0, Landroidx/work/impl/c/t;

    invoke-direct {v0, p0, p1}, Landroidx/work/impl/c/t;-><init>(Landroidx/work/impl/c/w;Landroidx/room/v;)V

    iput-object v0, p0, Landroidx/work/impl/c/w;->b:Landroidx/room/c;

    new-instance v0, Landroidx/work/impl/c/u;

    invoke-direct {v0, p0, p1}, Landroidx/work/impl/c/u;-><init>(Landroidx/work/impl/c/w;Landroidx/room/v;)V

    iput-object v0, p0, Landroidx/work/impl/c/w;->c:Landroidx/room/B;

    new-instance v0, Landroidx/work/impl/c/v;

    invoke-direct {v0, p0, p1}, Landroidx/work/impl/c/v;-><init>(Landroidx/work/impl/c/w;Landroidx/room/v;)V

    iput-object v0, p0, Landroidx/work/impl/c/w;->d:Landroidx/room/B;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    iget-object v0, p0, Landroidx/work/impl/c/w;->a:Landroidx/room/v;

    invoke-virtual {v0}, Landroidx/room/v;->b()V

    iget-object v0, p0, Landroidx/work/impl/c/w;->d:Landroidx/room/B;

    invoke-virtual {v0}, Landroidx/room/B;->a()La/p/a/f;

    move-result-object v0

    iget-object v1, p0, Landroidx/work/impl/c/w;->a:Landroidx/room/v;

    invoke-virtual {v1}, Landroidx/room/v;->c()V

    :try_start_0
    invoke-interface {v0}, La/p/a/f;->i()I

    iget-object v1, p0, Landroidx/work/impl/c/w;->a:Landroidx/room/v;

    invoke-virtual {v1}, Landroidx/room/v;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Landroidx/work/impl/c/w;->a:Landroidx/room/v;

    invoke-virtual {v1}, Landroidx/room/v;->e()V

    iget-object v1, p0, Landroidx/work/impl/c/w;->d:Landroidx/room/B;

    invoke-virtual {v1, v0}, Landroidx/room/B;->a(La/p/a/f;)V

    return-void

    :catchall_0
    move-exception v1

    iget-object v2, p0, Landroidx/work/impl/c/w;->a:Landroidx/room/v;

    invoke-virtual {v2}, Landroidx/room/v;->e()V

    iget-object v2, p0, Landroidx/work/impl/c/w;->d:Landroidx/room/B;

    invoke-virtual {v2, v0}, Landroidx/room/B;->a(La/p/a/f;)V

    throw v1
.end method

.method public delete(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Landroidx/work/impl/c/w;->a:Landroidx/room/v;

    invoke-virtual {v0}, Landroidx/room/v;->b()V

    iget-object v0, p0, Landroidx/work/impl/c/w;->c:Landroidx/room/B;

    invoke-virtual {v0}, Landroidx/room/B;->a()La/p/a/f;

    move-result-object v0

    const/4 v1, 0x1

    if-nez p1, :cond_0

    invoke-interface {v0, v1}, La/p/a/d;->a(I)V

    goto :goto_0

    :cond_0
    invoke-interface {v0, v1, p1}, La/p/a/d;->a(ILjava/lang/String;)V

    :goto_0
    iget-object p1, p0, Landroidx/work/impl/c/w;->a:Landroidx/room/v;

    invoke-virtual {p1}, Landroidx/room/v;->c()V

    :try_start_0
    invoke-interface {v0}, La/p/a/f;->i()I

    iget-object p1, p0, Landroidx/work/impl/c/w;->a:Landroidx/room/v;

    invoke-virtual {p1}, Landroidx/room/v;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Landroidx/work/impl/c/w;->a:Landroidx/room/v;

    invoke-virtual {p1}, Landroidx/room/v;->e()V

    iget-object p1, p0, Landroidx/work/impl/c/w;->c:Landroidx/room/B;

    invoke-virtual {p1, v0}, Landroidx/room/B;->a(La/p/a/f;)V

    return-void

    :catchall_0
    move-exception p1

    iget-object v1, p0, Landroidx/work/impl/c/w;->a:Landroidx/room/v;

    invoke-virtual {v1}, Landroidx/room/v;->e()V

    iget-object v1, p0, Landroidx/work/impl/c/w;->c:Landroidx/room/B;

    invoke-virtual {v1, v0}, Landroidx/room/B;->a(La/p/a/f;)V

    throw p1
.end method
