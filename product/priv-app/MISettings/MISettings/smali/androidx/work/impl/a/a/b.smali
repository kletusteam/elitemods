.class public Landroidx/work/impl/a/a/b;
.super Ljava/lang/Object;
.source "DelayedWorkTracker.java"


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
    }
.end annotation


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Landroidx/work/impl/a/a/c;

.field private final c:Landroidx/work/v;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "DelayedWorkTracker"

    invoke-static {v0}, Landroidx/work/n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroidx/work/impl/a/a/b;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroidx/work/impl/a/a/c;Landroidx/work/v;)V
    .locals 0
    .param p1    # Landroidx/work/impl/a/a/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroidx/work/v;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroidx/work/impl/a/a/b;->b:Landroidx/work/impl/a/a/c;

    iput-object p2, p0, Landroidx/work/impl/a/a/b;->c:Landroidx/work/v;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Landroidx/work/impl/a/a/b;->d:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public a(Landroidx/work/impl/c/y;)V
    .locals 5
    .param p1    # Landroidx/work/impl/c/y;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Landroidx/work/impl/a/a/b;->d:Ljava/util/Map;

    iget-object v1, p1, Landroidx/work/impl/c/y;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v1, p0, Landroidx/work/impl/a/a/b;->c:Landroidx/work/v;

    invoke-interface {v1, v0}, Landroidx/work/v;->a(Ljava/lang/Runnable;)V

    :cond_0
    new-instance v0, Landroidx/work/impl/a/a/a;

    invoke-direct {v0, p0, p1}, Landroidx/work/impl/a/a/a;-><init>(Landroidx/work/impl/a/a/b;Landroidx/work/impl/c/y;)V

    iget-object v1, p0, Landroidx/work/impl/a/a/b;->d:Ljava/util/Map;

    iget-object v2, p1, Landroidx/work/impl/c/y;->c:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p1}, Landroidx/work/impl/c/y;->a()J

    move-result-wide v3

    sub-long/2addr v3, v1

    iget-object p1, p0, Landroidx/work/impl/a/a/b;->c:Landroidx/work/v;

    invoke-interface {p1, v3, v4, v0}, Landroidx/work/v;->a(JLjava/lang/Runnable;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget-object v0, p0, Landroidx/work/impl/a/a/b;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Runnable;

    if-eqz p1, :cond_0

    iget-object v0, p0, Landroidx/work/impl/a/a/b;->c:Landroidx/work/v;

    invoke-interface {v0, p1}, Landroidx/work/v;->a(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method
