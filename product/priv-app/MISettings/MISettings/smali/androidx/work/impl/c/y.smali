.class public final Landroidx/work/impl/c/y;
.super Ljava/lang/Object;
.source "WorkSpec.java"


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
    }
.end annotation

.annotation build Landroidx/room/Entity;
    indices = {
        .subannotation Landroidx/room/Index;
            value = {
                "schedule_requested_at"
            }
        .end subannotation,
        .subannotation Landroidx/room/Index;
            value = {
                "period_start_time"
            }
        .end subannotation
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/work/impl/c/y$b;,
        Landroidx/work/impl/c/y$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field public static final b:La/b/a/c/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/a/c/a<",
            "Ljava/util/List<",
            "Landroidx/work/impl/c/y$b;",
            ">;",
            "Ljava/util/List<",
            "Landroidx/work/x;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field public c:Ljava/lang/String;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation build Landroidx/room/ColumnInfo;
        name = "id"
    .end annotation

    .annotation build Landroidx/room/PrimaryKey;
    .end annotation
.end field

.field public d:Landroidx/work/x$a;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation build Landroidx/room/ColumnInfo;
        name = "state"
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation build Landroidx/room/ColumnInfo;
        name = "worker_class_name"
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Landroidx/room/ColumnInfo;
        name = "input_merger_class_name"
    .end annotation
.end field

.field public g:Landroidx/work/f;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation build Landroidx/room/ColumnInfo;
        name = "input"
    .end annotation
.end field

.field public h:Landroidx/work/f;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation build Landroidx/room/ColumnInfo;
        name = "output"
    .end annotation
.end field

.field public i:J
    .annotation build Landroidx/room/ColumnInfo;
        name = "initial_delay"
    .end annotation
.end field

.field public j:J
    .annotation build Landroidx/room/ColumnInfo;
        name = "interval_duration"
    .end annotation
.end field

.field public k:J
    .annotation build Landroidx/room/ColumnInfo;
        name = "flex_duration"
    .end annotation
.end field

.field public l:Landroidx/work/d;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation build Landroidx/room/Embedded;
    .end annotation
.end field

.field public m:I
    .annotation build Landroidx/annotation/IntRange;
        from = 0x0L
    .end annotation

    .annotation build Landroidx/room/ColumnInfo;
        name = "run_attempt_count"
    .end annotation
.end field

.field public n:Landroidx/work/a;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation build Landroidx/room/ColumnInfo;
        name = "backoff_policy"
    .end annotation
.end field

.field public o:J
    .annotation build Landroidx/room/ColumnInfo;
        name = "backoff_delay_duration"
    .end annotation
.end field

.field public p:J
    .annotation build Landroidx/room/ColumnInfo;
        name = "period_start_time"
    .end annotation
.end field

.field public q:J
    .annotation build Landroidx/room/ColumnInfo;
        name = "minimum_retention_duration"
    .end annotation
.end field

.field public r:J
    .annotation build Landroidx/room/ColumnInfo;
        name = "schedule_requested_at"
    .end annotation
.end field

.field public s:Z
    .annotation build Landroidx/room/ColumnInfo;
        name = "run_in_foreground"
    .end annotation
.end field

.field public t:Landroidx/work/s;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation build Landroidx/room/ColumnInfo;
        name = "out_of_quota_policy"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "WorkSpec"

    invoke-static {v0}, Landroidx/work/n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroidx/work/impl/c/y;->a:Ljava/lang/String;

    new-instance v0, Landroidx/work/impl/c/x;

    invoke-direct {v0}, Landroidx/work/impl/c/x;-><init>()V

    sput-object v0, Landroidx/work/impl/c/y;->b:La/b/a/c/a;

    return-void
.end method

.method public constructor <init>(Landroidx/work/impl/c/y;)V
    .locals 2
    .param p1    # Landroidx/work/impl/c/y;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Landroidx/work/x$a;->a:Landroidx/work/x$a;

    iput-object v0, p0, Landroidx/work/impl/c/y;->d:Landroidx/work/x$a;

    sget-object v0, Landroidx/work/f;->b:Landroidx/work/f;

    iput-object v0, p0, Landroidx/work/impl/c/y;->g:Landroidx/work/f;

    iput-object v0, p0, Landroidx/work/impl/c/y;->h:Landroidx/work/f;

    sget-object v0, Landroidx/work/d;->a:Landroidx/work/d;

    iput-object v0, p0, Landroidx/work/impl/c/y;->l:Landroidx/work/d;

    sget-object v0, Landroidx/work/a;->a:Landroidx/work/a;

    iput-object v0, p0, Landroidx/work/impl/c/y;->n:Landroidx/work/a;

    const-wide/16 v0, 0x7530

    iput-wide v0, p0, Landroidx/work/impl/c/y;->o:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroidx/work/impl/c/y;->r:J

    sget-object v0, Landroidx/work/s;->a:Landroidx/work/s;

    iput-object v0, p0, Landroidx/work/impl/c/y;->t:Landroidx/work/s;

    iget-object v0, p1, Landroidx/work/impl/c/y;->c:Ljava/lang/String;

    iput-object v0, p0, Landroidx/work/impl/c/y;->c:Ljava/lang/String;

    iget-object v0, p1, Landroidx/work/impl/c/y;->e:Ljava/lang/String;

    iput-object v0, p0, Landroidx/work/impl/c/y;->e:Ljava/lang/String;

    iget-object v0, p1, Landroidx/work/impl/c/y;->d:Landroidx/work/x$a;

    iput-object v0, p0, Landroidx/work/impl/c/y;->d:Landroidx/work/x$a;

    iget-object v0, p1, Landroidx/work/impl/c/y;->f:Ljava/lang/String;

    iput-object v0, p0, Landroidx/work/impl/c/y;->f:Ljava/lang/String;

    new-instance v0, Landroidx/work/f;

    iget-object v1, p1, Landroidx/work/impl/c/y;->g:Landroidx/work/f;

    invoke-direct {v0, v1}, Landroidx/work/f;-><init>(Landroidx/work/f;)V

    iput-object v0, p0, Landroidx/work/impl/c/y;->g:Landroidx/work/f;

    new-instance v0, Landroidx/work/f;

    iget-object v1, p1, Landroidx/work/impl/c/y;->h:Landroidx/work/f;

    invoke-direct {v0, v1}, Landroidx/work/f;-><init>(Landroidx/work/f;)V

    iput-object v0, p0, Landroidx/work/impl/c/y;->h:Landroidx/work/f;

    iget-wide v0, p1, Landroidx/work/impl/c/y;->i:J

    iput-wide v0, p0, Landroidx/work/impl/c/y;->i:J

    iget-wide v0, p1, Landroidx/work/impl/c/y;->j:J

    iput-wide v0, p0, Landroidx/work/impl/c/y;->j:J

    iget-wide v0, p1, Landroidx/work/impl/c/y;->k:J

    iput-wide v0, p0, Landroidx/work/impl/c/y;->k:J

    new-instance v0, Landroidx/work/d;

    iget-object v1, p1, Landroidx/work/impl/c/y;->l:Landroidx/work/d;

    invoke-direct {v0, v1}, Landroidx/work/d;-><init>(Landroidx/work/d;)V

    iput-object v0, p0, Landroidx/work/impl/c/y;->l:Landroidx/work/d;

    iget v0, p1, Landroidx/work/impl/c/y;->m:I

    iput v0, p0, Landroidx/work/impl/c/y;->m:I

    iget-object v0, p1, Landroidx/work/impl/c/y;->n:Landroidx/work/a;

    iput-object v0, p0, Landroidx/work/impl/c/y;->n:Landroidx/work/a;

    iget-wide v0, p1, Landroidx/work/impl/c/y;->o:J

    iput-wide v0, p0, Landroidx/work/impl/c/y;->o:J

    iget-wide v0, p1, Landroidx/work/impl/c/y;->p:J

    iput-wide v0, p0, Landroidx/work/impl/c/y;->p:J

    iget-wide v0, p1, Landroidx/work/impl/c/y;->q:J

    iput-wide v0, p0, Landroidx/work/impl/c/y;->q:J

    iget-wide v0, p1, Landroidx/work/impl/c/y;->r:J

    iput-wide v0, p0, Landroidx/work/impl/c/y;->r:J

    iget-boolean v0, p1, Landroidx/work/impl/c/y;->s:Z

    iput-boolean v0, p0, Landroidx/work/impl/c/y;->s:Z

    iget-object p1, p1, Landroidx/work/impl/c/y;->t:Landroidx/work/s;

    iput-object p1, p0, Landroidx/work/impl/c/y;->t:Landroidx/work/s;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Landroidx/work/x$a;->a:Landroidx/work/x$a;

    iput-object v0, p0, Landroidx/work/impl/c/y;->d:Landroidx/work/x$a;

    sget-object v0, Landroidx/work/f;->b:Landroidx/work/f;

    iput-object v0, p0, Landroidx/work/impl/c/y;->g:Landroidx/work/f;

    iput-object v0, p0, Landroidx/work/impl/c/y;->h:Landroidx/work/f;

    sget-object v0, Landroidx/work/d;->a:Landroidx/work/d;

    iput-object v0, p0, Landroidx/work/impl/c/y;->l:Landroidx/work/d;

    sget-object v0, Landroidx/work/a;->a:Landroidx/work/a;

    iput-object v0, p0, Landroidx/work/impl/c/y;->n:Landroidx/work/a;

    const-wide/16 v0, 0x7530

    iput-wide v0, p0, Landroidx/work/impl/c/y;->o:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroidx/work/impl/c/y;->r:J

    sget-object v0, Landroidx/work/s;->a:Landroidx/work/s;

    iput-object v0, p0, Landroidx/work/impl/c/y;->t:Landroidx/work/s;

    iput-object p1, p0, Landroidx/work/impl/c/y;->c:Ljava/lang/String;

    iput-object p2, p0, Landroidx/work/impl/c/y;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()J
    .locals 11

    invoke-virtual {p0}, Landroidx/work/impl/c/y;->c()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroidx/work/impl/c/y;->n:Landroidx/work/a;

    sget-object v3, Landroidx/work/a;->b:Landroidx/work/a;

    if-ne v0, v3, :cond_0

    move v1, v2

    :cond_0
    if-eqz v1, :cond_1

    iget-wide v0, p0, Landroidx/work/impl/c/y;->o:J

    iget v2, p0, Landroidx/work/impl/c/y;->m:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    goto :goto_0

    :cond_1
    iget-wide v0, p0, Landroidx/work/impl/c/y;->o:J

    long-to-float v0, v0

    iget v1, p0, Landroidx/work/impl/c/y;->m:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->scalb(FI)F

    move-result v0

    float-to-long v0, v0

    :goto_0
    iget-wide v2, p0, Landroidx/work/impl/c/y;->p:J

    const-wide/32 v4, 0x112a880

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    add-long/2addr v2, v0

    return-wide v2

    :cond_2
    invoke-virtual {p0}, Landroidx/work/impl/c/y;->d()Z

    move-result v0

    const-wide/16 v3, 0x0

    if-eqz v0, :cond_8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iget-wide v7, p0, Landroidx/work/impl/c/y;->p:J

    cmp-long v0, v7, v3

    if-nez v0, :cond_3

    iget-wide v7, p0, Landroidx/work/impl/c/y;->i:J

    add-long/2addr v7, v5

    :cond_3
    iget-wide v5, p0, Landroidx/work/impl/c/y;->k:J

    iget-wide v9, p0, Landroidx/work/impl/c/y;->j:J

    cmp-long v0, v5, v9

    if-eqz v0, :cond_4

    move v1, v2

    :cond_4
    if-eqz v1, :cond_6

    iget-wide v0, p0, Landroidx/work/impl/c/y;->p:J

    cmp-long v0, v0, v3

    if-nez v0, :cond_5

    const-wide/16 v0, -0x1

    iget-wide v2, p0, Landroidx/work/impl/c/y;->k:J

    mul-long v3, v2, v0

    :cond_5
    iget-wide v0, p0, Landroidx/work/impl/c/y;->j:J

    add-long/2addr v7, v0

    add-long/2addr v7, v3

    return-wide v7

    :cond_6
    iget-wide v0, p0, Landroidx/work/impl/c/y;->p:J

    cmp-long v0, v0, v3

    if-nez v0, :cond_7

    goto :goto_1

    :cond_7
    iget-wide v3, p0, Landroidx/work/impl/c/y;->j:J

    :goto_1
    add-long/2addr v7, v3

    return-wide v7

    :cond_8
    iget-wide v0, p0, Landroidx/work/impl/c/y;->p:J

    cmp-long v2, v0, v3

    if-nez v2, :cond_9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    :cond_9
    iget-wide v2, p0, Landroidx/work/impl/c/y;->i:J

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public a(J)V
    .locals 5

    const-wide/32 v0, 0x112a880

    cmp-long v2, p1, v0

    const/4 v3, 0x0

    if-lez v2, :cond_0

    invoke-static {}, Landroidx/work/n;->a()Landroidx/work/n;

    move-result-object p1

    sget-object p2, Landroidx/work/impl/c/y;->a:Ljava/lang/String;

    new-array v2, v3, [Ljava/lang/Throwable;

    const-string v4, "Backoff delay duration exceeds maximum value"

    invoke-virtual {p1, p2, v4, v2}, Landroidx/work/n;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Throwable;)V

    move-wide p1, v0

    :cond_0
    const-wide/16 v0, 0x2710

    cmp-long v2, p1, v0

    if-gez v2, :cond_1

    invoke-static {}, Landroidx/work/n;->a()Landroidx/work/n;

    move-result-object p1

    sget-object p2, Landroidx/work/impl/c/y;->a:Ljava/lang/String;

    new-array v2, v3, [Ljava/lang/Throwable;

    const-string v3, "Backoff delay duration less than minimum value"

    invoke-virtual {p1, p2, v3, v2}, Landroidx/work/n;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Throwable;)V

    move-wide p1, v0

    :cond_1
    iput-wide p1, p0, Landroidx/work/impl/c/y;->o:J

    return-void
.end method

.method public b()Z
    .locals 2

    sget-object v0, Landroidx/work/d;->a:Landroidx/work/d;

    iget-object v1, p0, Landroidx/work/impl/c/y;->l:Landroidx/work/d;

    invoke-virtual {v0, v1}, Landroidx/work/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public c()Z
    .locals 2

    iget-object v0, p0, Landroidx/work/impl/c/y;->d:Landroidx/work/x$a;

    sget-object v1, Landroidx/work/x$a;->a:Landroidx/work/x$a;

    if-ne v0, v1, :cond_0

    iget v0, p0, Landroidx/work/impl/c/y;->m:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public d()Z
    .locals 4

    iget-wide v0, p0, Landroidx/work/impl/c/y;->j:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_15

    const-class v2, Landroidx/work/impl/c/y;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto/16 :goto_2

    :cond_1
    check-cast p1, Landroidx/work/impl/c/y;

    iget-wide v2, p0, Landroidx/work/impl/c/y;->i:J

    iget-wide v4, p1, Landroidx/work/impl/c/y;->i:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    return v1

    :cond_2
    iget-wide v2, p0, Landroidx/work/impl/c/y;->j:J

    iget-wide v4, p1, Landroidx/work/impl/c/y;->j:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    return v1

    :cond_3
    iget-wide v2, p0, Landroidx/work/impl/c/y;->k:J

    iget-wide v4, p1, Landroidx/work/impl/c/y;->k:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    return v1

    :cond_4
    iget v2, p0, Landroidx/work/impl/c/y;->m:I

    iget v3, p1, Landroidx/work/impl/c/y;->m:I

    if-eq v2, v3, :cond_5

    return v1

    :cond_5
    iget-wide v2, p0, Landroidx/work/impl/c/y;->o:J

    iget-wide v4, p1, Landroidx/work/impl/c/y;->o:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    return v1

    :cond_6
    iget-wide v2, p0, Landroidx/work/impl/c/y;->p:J

    iget-wide v4, p1, Landroidx/work/impl/c/y;->p:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    return v1

    :cond_7
    iget-wide v2, p0, Landroidx/work/impl/c/y;->q:J

    iget-wide v4, p1, Landroidx/work/impl/c/y;->q:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_8

    return v1

    :cond_8
    iget-wide v2, p0, Landroidx/work/impl/c/y;->r:J

    iget-wide v4, p1, Landroidx/work/impl/c/y;->r:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_9

    return v1

    :cond_9
    iget-boolean v2, p0, Landroidx/work/impl/c/y;->s:Z

    iget-boolean v3, p1, Landroidx/work/impl/c/y;->s:Z

    if-eq v2, v3, :cond_a

    return v1

    :cond_a
    iget-object v2, p0, Landroidx/work/impl/c/y;->c:Ljava/lang/String;

    iget-object v3, p1, Landroidx/work/impl/c/y;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    return v1

    :cond_b
    iget-object v2, p0, Landroidx/work/impl/c/y;->d:Landroidx/work/x$a;

    iget-object v3, p1, Landroidx/work/impl/c/y;->d:Landroidx/work/x$a;

    if-eq v2, v3, :cond_c

    return v1

    :cond_c
    iget-object v2, p0, Landroidx/work/impl/c/y;->e:Ljava/lang/String;

    iget-object v3, p1, Landroidx/work/impl/c/y;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    return v1

    :cond_d
    iget-object v2, p0, Landroidx/work/impl/c/y;->f:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v3, p1, Landroidx/work/impl/c/y;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    goto :goto_0

    :cond_e
    iget-object v2, p1, Landroidx/work/impl/c/y;->f:Ljava/lang/String;

    if-eqz v2, :cond_f

    :goto_0
    return v1

    :cond_f
    iget-object v2, p0, Landroidx/work/impl/c/y;->g:Landroidx/work/f;

    iget-object v3, p1, Landroidx/work/impl/c/y;->g:Landroidx/work/f;

    invoke-virtual {v2, v3}, Landroidx/work/f;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    return v1

    :cond_10
    iget-object v2, p0, Landroidx/work/impl/c/y;->h:Landroidx/work/f;

    iget-object v3, p1, Landroidx/work/impl/c/y;->h:Landroidx/work/f;

    invoke-virtual {v2, v3}, Landroidx/work/f;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    return v1

    :cond_11
    iget-object v2, p0, Landroidx/work/impl/c/y;->l:Landroidx/work/d;

    iget-object v3, p1, Landroidx/work/impl/c/y;->l:Landroidx/work/d;

    invoke-virtual {v2, v3}, Landroidx/work/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    return v1

    :cond_12
    iget-object v2, p0, Landroidx/work/impl/c/y;->n:Landroidx/work/a;

    iget-object v3, p1, Landroidx/work/impl/c/y;->n:Landroidx/work/a;

    if-eq v2, v3, :cond_13

    return v1

    :cond_13
    iget-object v2, p0, Landroidx/work/impl/c/y;->t:Landroidx/work/s;

    iget-object p1, p1, Landroidx/work/impl/c/y;->t:Landroidx/work/s;

    if-ne v2, p1, :cond_14

    goto :goto_1

    :cond_14
    move v0, v1

    :goto_1
    return v0

    :cond_15
    :goto_2
    return v1
.end method

.method public hashCode()I
    .locals 6

    iget-object v0, p0, Landroidx/work/impl/c/y;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Landroidx/work/impl/c/y;->d:Landroidx/work/x$a;

    invoke-virtual {v1}, Ljava/lang/Enum;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Landroidx/work/impl/c/y;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Landroidx/work/impl/c/y;->f:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Landroidx/work/impl/c/y;->g:Landroidx/work/f;

    invoke-virtual {v1}, Landroidx/work/f;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Landroidx/work/impl/c/y;->h:Landroidx/work/f;

    invoke-virtual {v1}, Landroidx/work/f;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Landroidx/work/impl/c/y;->i:J

    const/16 v3, 0x20

    ushr-long v4, v1, v3

    xor-long/2addr v1, v4

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Landroidx/work/impl/c/y;->j:J

    ushr-long v4, v1, v3

    xor-long/2addr v1, v4

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Landroidx/work/impl/c/y;->k:J

    ushr-long v4, v1, v3

    xor-long/2addr v1, v4

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Landroidx/work/impl/c/y;->l:Landroidx/work/d;

    invoke-virtual {v1}, Landroidx/work/d;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Landroidx/work/impl/c/y;->m:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Landroidx/work/impl/c/y;->n:Landroidx/work/a;

    invoke-virtual {v1}, Ljava/lang/Enum;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Landroidx/work/impl/c/y;->o:J

    ushr-long v4, v1, v3

    xor-long/2addr v1, v4

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Landroidx/work/impl/c/y;->p:J

    ushr-long v4, v1, v3

    xor-long/2addr v1, v4

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Landroidx/work/impl/c/y;->q:J

    ushr-long v4, v1, v3

    xor-long/2addr v1, v4

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Landroidx/work/impl/c/y;->r:J

    ushr-long v3, v1, v3

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Landroidx/work/impl/c/y;->s:Z

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Landroidx/work/impl/c/y;->t:Landroidx/work/s;

    invoke-virtual {v1}, Ljava/lang/Enum;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "{WorkSpec: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Landroidx/work/impl/c/y;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
