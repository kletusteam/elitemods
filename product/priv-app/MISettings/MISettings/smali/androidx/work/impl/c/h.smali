.class public final Landroidx/work/impl/c/h;
.super Ljava/lang/Object;
.source "PreferenceDao_Impl.java"

# interfaces
.implements Landroidx/work/impl/c/f;


# instance fields
.field private final a:Landroidx/room/v;

.field private final b:Landroidx/room/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/room/c<",
            "Landroidx/work/impl/c/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroidx/room/v;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroidx/work/impl/c/h;->a:Landroidx/room/v;

    new-instance v0, Landroidx/work/impl/c/g;

    invoke-direct {v0, p0, p1}, Landroidx/work/impl/c/g;-><init>(Landroidx/work/impl/c/h;Landroidx/room/v;)V

    iput-object v0, p0, Landroidx/work/impl/c/h;->b:Landroidx/room/c;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/Long;
    .locals 4

    const/4 v0, 0x1

    const-string v1, "SELECT long_value FROM Preference where `key`=?"

    invoke-static {v1, v0}, Landroidx/room/y;->a(Ljava/lang/String;I)Landroidx/room/y;

    move-result-object v1

    if-nez p1, :cond_0

    invoke-virtual {v1, v0}, Landroidx/room/y;->a(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {v1, v0, p1}, Landroidx/room/y;->a(ILjava/lang/String;)V

    :goto_0
    iget-object p1, p0, Landroidx/work/impl/c/h;->a:Landroidx/room/v;

    invoke-virtual {p1}, Landroidx/room/v;->b()V

    iget-object p1, p0, Landroidx/work/impl/c/h;->a:Landroidx/room/v;

    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-static {p1, v1, v2, v0}, Landroidx/room/b/c;->a(Landroidx/room/v;La/p/a/e;ZLandroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object p1

    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    invoke-virtual {v1}, Landroidx/room/y;->b()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    invoke-virtual {v1}, Landroidx/room/y;->b()V

    throw v0
.end method

.method public a(Landroidx/work/impl/c/e;)V
    .locals 1

    iget-object v0, p0, Landroidx/work/impl/c/h;->a:Landroidx/room/v;

    invoke-virtual {v0}, Landroidx/room/v;->b()V

    iget-object v0, p0, Landroidx/work/impl/c/h;->a:Landroidx/room/v;

    invoke-virtual {v0}, Landroidx/room/v;->c()V

    :try_start_0
    iget-object v0, p0, Landroidx/work/impl/c/h;->b:Landroidx/room/c;

    invoke-virtual {v0, p1}, Landroidx/room/c;->a(Ljava/lang/Object;)V

    iget-object p1, p0, Landroidx/work/impl/c/h;->a:Landroidx/room/v;

    invoke-virtual {p1}, Landroidx/room/v;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Landroidx/work/impl/c/h;->a:Landroidx/room/v;

    invoke-virtual {p1}, Landroidx/room/v;->e()V

    return-void

    :catchall_0
    move-exception p1

    iget-object v0, p0, Landroidx/work/impl/c/h;->a:Landroidx/room/v;

    invoke-virtual {v0}, Landroidx/room/v;->e()V

    throw p1
.end method
