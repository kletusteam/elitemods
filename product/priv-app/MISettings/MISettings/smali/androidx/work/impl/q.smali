.class public Landroidx/work/impl/q;
.super Ljava/lang/Object;
.source "WorkDatabaseMigrations.java"


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/work/impl/q$b;,
        Landroidx/work/impl/q$a;
    }
.end annotation


# static fields
.field public static a:Landroidx/room/a/a;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public static b:Landroidx/room/a/a;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public static c:Landroidx/room/a/a;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public static d:Landroidx/room/a/a;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public static e:Landroidx/room/a/a;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public static f:Landroidx/room/a/a;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public static g:Landroidx/room/a/a;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Landroidx/work/impl/j;

    const/4 v1, 0x1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Landroidx/work/impl/j;-><init>(II)V

    sput-object v0, Landroidx/work/impl/q;->a:Landroidx/room/a/a;

    new-instance v0, Landroidx/work/impl/k;

    const/4 v1, 0x4

    const/4 v2, 0x3

    invoke-direct {v0, v2, v1}, Landroidx/work/impl/k;-><init>(II)V

    sput-object v0, Landroidx/work/impl/q;->b:Landroidx/room/a/a;

    new-instance v0, Landroidx/work/impl/l;

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Landroidx/work/impl/l;-><init>(II)V

    sput-object v0, Landroidx/work/impl/q;->c:Landroidx/room/a/a;

    new-instance v0, Landroidx/work/impl/m;

    const/4 v1, 0x7

    const/4 v2, 0x6

    invoke-direct {v0, v2, v1}, Landroidx/work/impl/m;-><init>(II)V

    sput-object v0, Landroidx/work/impl/q;->d:Landroidx/room/a/a;

    new-instance v0, Landroidx/work/impl/n;

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Landroidx/work/impl/n;-><init>(II)V

    sput-object v0, Landroidx/work/impl/q;->e:Landroidx/room/a/a;

    new-instance v0, Landroidx/work/impl/o;

    const/16 v1, 0x9

    invoke-direct {v0, v2, v1}, Landroidx/work/impl/o;-><init>(II)V

    sput-object v0, Landroidx/work/impl/q;->f:Landroidx/room/a/a;

    new-instance v0, Landroidx/work/impl/p;

    const/16 v1, 0xb

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Landroidx/work/impl/p;-><init>(II)V

    sput-object v0, Landroidx/work/impl/q;->g:Landroidx/room/a/a;

    return-void
.end method
