.class Landroidx/work/impl/utils/a;
.super Landroidx/work/impl/utils/c;
.source "CancelWorkRunnable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroidx/work/impl/utils/c;->a(Ljava/util/UUID;Landroidx/work/impl/t;)Landroidx/work/impl/utils/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic b:Landroidx/work/impl/t;

.field final synthetic c:Ljava/util/UUID;


# direct methods
.method constructor <init>(Landroidx/work/impl/t;Ljava/util/UUID;)V
    .locals 0

    iput-object p1, p0, Landroidx/work/impl/utils/a;->b:Landroidx/work/impl/t;

    iput-object p2, p0, Landroidx/work/impl/utils/a;->c:Ljava/util/UUID;

    invoke-direct {p0}, Landroidx/work/impl/utils/c;-><init>()V

    return-void
.end method


# virtual methods
.method b()V
    .locals 3
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, Landroidx/work/impl/utils/a;->b:Landroidx/work/impl/t;

    invoke-virtual {v0}, Landroidx/work/impl/t;->g()Landroidx/work/impl/WorkDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/room/v;->c()V

    :try_start_0
    iget-object v1, p0, Landroidx/work/impl/utils/a;->b:Landroidx/work/impl/t;

    iget-object v2, p0, Landroidx/work/impl/utils/a;->c:Ljava/util/UUID;

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Landroidx/work/impl/utils/c;->a(Landroidx/work/impl/t;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroidx/room/v;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Landroidx/room/v;->e()V

    iget-object v0, p0, Landroidx/work/impl/utils/a;->b:Landroidx/work/impl/t;

    invoke-virtual {p0, v0}, Landroidx/work/impl/utils/c;->a(Landroidx/work/impl/t;)V

    return-void

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroidx/room/v;->e()V

    throw v1
.end method
