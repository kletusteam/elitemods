.class public Landroidx/work/impl/w;
.super Ljava/lang/Object;
.source "WorkerWrapper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/work/impl/w$a;
    }
.end annotation


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field b:Landroid/content/Context;

.field private c:Ljava/lang/String;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroidx/work/impl/e;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroidx/work/WorkerParameters$a;

.field f:Landroidx/work/impl/c/y;

.field g:Landroidx/work/ListenableWorker;

.field h:Landroidx/work/impl/utils/b/a;

.field i:Landroidx/work/ListenableWorker$a;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field private j:Landroidx/work/c;

.field private k:Landroidx/work/impl/foreground/a;

.field private l:Landroidx/work/impl/WorkDatabase;

.field private m:Landroidx/work/impl/c/z;

.field private n:Landroidx/work/impl/c/b;

.field private o:Landroidx/work/impl/c/L;

.field private p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljava/lang/String;

.field r:Landroidx/work/impl/utils/a/e;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/work/impl/utils/a/e<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field s:Lcom/google/common/util/concurrent/a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/a<",
            "Landroidx/work/ListenableWorker$a;",
            ">;"
        }
    .end annotation
.end field

.field private volatile t:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "WorkerWrapper"

    invoke-static {v0}, Landroidx/work/n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroidx/work/impl/w;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroidx/work/impl/w$a;)V
    .locals 1
    .param p1    # Landroidx/work/impl/w$a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroidx/work/ListenableWorker$a;->a()Landroidx/work/ListenableWorker$a;

    move-result-object v0

    iput-object v0, p0, Landroidx/work/impl/w;->i:Landroidx/work/ListenableWorker$a;

    invoke-static {}, Landroidx/work/impl/utils/a/e;->d()Landroidx/work/impl/utils/a/e;

    move-result-object v0

    iput-object v0, p0, Landroidx/work/impl/w;->r:Landroidx/work/impl/utils/a/e;

    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/work/impl/w;->s:Lcom/google/common/util/concurrent/a;

    iget-object v0, p1, Landroidx/work/impl/w$a;->a:Landroid/content/Context;

    iput-object v0, p0, Landroidx/work/impl/w;->b:Landroid/content/Context;

    iget-object v0, p1, Landroidx/work/impl/w$a;->d:Landroidx/work/impl/utils/b/a;

    iput-object v0, p0, Landroidx/work/impl/w;->h:Landroidx/work/impl/utils/b/a;

    iget-object v0, p1, Landroidx/work/impl/w$a;->c:Landroidx/work/impl/foreground/a;

    iput-object v0, p0, Landroidx/work/impl/w;->k:Landroidx/work/impl/foreground/a;

    iget-object v0, p1, Landroidx/work/impl/w$a;->g:Ljava/lang/String;

    iput-object v0, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    iget-object v0, p1, Landroidx/work/impl/w$a;->h:Ljava/util/List;

    iput-object v0, p0, Landroidx/work/impl/w;->d:Ljava/util/List;

    iget-object v0, p1, Landroidx/work/impl/w$a;->i:Landroidx/work/WorkerParameters$a;

    iput-object v0, p0, Landroidx/work/impl/w;->e:Landroidx/work/WorkerParameters$a;

    iget-object v0, p1, Landroidx/work/impl/w$a;->b:Landroidx/work/ListenableWorker;

    iput-object v0, p0, Landroidx/work/impl/w;->g:Landroidx/work/ListenableWorker;

    iget-object v0, p1, Landroidx/work/impl/w$a;->e:Landroidx/work/c;

    iput-object v0, p0, Landroidx/work/impl/w;->j:Landroidx/work/c;

    iget-object p1, p1, Landroidx/work/impl/w$a;->f:Landroidx/work/impl/WorkDatabase;

    iput-object p1, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    iget-object p1, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {p1}, Landroidx/work/impl/WorkDatabase;->t()Landroidx/work/impl/c/z;

    move-result-object p1

    iput-object p1, p0, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    iget-object p1, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {p1}, Landroidx/work/impl/WorkDatabase;->l()Landroidx/work/impl/c/b;

    move-result-object p1

    iput-object p1, p0, Landroidx/work/impl/w;->n:Landroidx/work/impl/c/b;

    iget-object p1, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {p1}, Landroidx/work/impl/WorkDatabase;->u()Landroidx/work/impl/c/L;

    move-result-object p1

    iput-object p1, p0, Landroidx/work/impl/w;->o:Landroidx/work/impl/c/L;

    return-void
.end method

.method private a(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Work [ id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", tags={ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v1, 0x1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    :cond_0
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    const-string p1, " } ]"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private a(Landroidx/work/ListenableWorker$a;)V
    .locals 4

    instance-of v0, p1, Landroidx/work/ListenableWorker$a$c;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    invoke-static {}, Landroidx/work/n;->a()Landroidx/work/n;

    move-result-object p1

    sget-object v0, Landroidx/work/impl/w;->a:Ljava/lang/String;

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v3, p0, Landroidx/work/impl/w;->q:Ljava/lang/String;

    aput-object v3, v1, v2

    const-string v3, "Worker result SUCCESS for %s"

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Throwable;

    invoke-virtual {p1, v0, v1, v2}, Landroidx/work/n;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Throwable;)V

    iget-object p1, p0, Landroidx/work/impl/w;->f:Landroidx/work/impl/c/y;

    invoke-virtual {p1}, Landroidx/work/impl/c/y;->d()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Landroidx/work/impl/w;->f()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Landroidx/work/impl/w;->i()V

    goto :goto_0

    :cond_1
    instance-of p1, p1, Landroidx/work/ListenableWorker$a$b;

    if-eqz p1, :cond_2

    invoke-static {}, Landroidx/work/n;->a()Landroidx/work/n;

    move-result-object p1

    sget-object v0, Landroidx/work/impl/w;->a:Ljava/lang/String;

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v3, p0, Landroidx/work/impl/w;->q:Ljava/lang/String;

    aput-object v3, v1, v2

    const-string v3, "Worker result RETRY for %s"

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Throwable;

    invoke-virtual {p1, v0, v1, v2}, Landroidx/work/n;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Throwable;)V

    invoke-direct {p0}, Landroidx/work/impl/w;->e()V

    goto :goto_0

    :cond_2
    invoke-static {}, Landroidx/work/n;->a()Landroidx/work/n;

    move-result-object p1

    sget-object v0, Landroidx/work/impl/w;->a:Ljava/lang/String;

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v3, p0, Landroidx/work/impl/w;->q:Ljava/lang/String;

    aput-object v3, v1, v2

    const-string v3, "Worker result FAILURE for %s"

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Throwable;

    invoke-virtual {p1, v0, v1, v2}, Landroidx/work/n;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Throwable;)V

    iget-object p1, p0, Landroidx/work/impl/w;->f:Landroidx/work/impl/c/y;

    invoke-virtual {p1}, Landroidx/work/impl/c/y;->d()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-direct {p0}, Landroidx/work/impl/w;->f()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Landroidx/work/impl/w;->d()V

    :goto_0
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 5

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :goto_0
    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_1

    invoke-virtual {v0}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iget-object v1, p0, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    invoke-interface {v1, p1}, Landroidx/work/impl/c/z;->c(Ljava/lang/String;)Landroidx/work/x$a;

    move-result-object v1

    sget-object v2, Landroidx/work/x$a;->f:Landroidx/work/x$a;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    sget-object v2, Landroidx/work/x$a;->d:Landroidx/work/x$a;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-interface {v1, v2, v3}, Landroidx/work/impl/c/z;->a(Landroidx/work/x$a;[Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Landroidx/work/impl/w;->n:Landroidx/work/impl/c/b;

    invoke-interface {v1, p1}, Landroidx/work/impl/c/b;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(Z)V
    .locals 5

    iget-object v0, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/v;->c()V

    :try_start_0
    iget-object v0, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/work/impl/WorkDatabase;->t()Landroidx/work/impl/c/z;

    move-result-object v0

    invoke-interface {v0}, Landroidx/work/impl/c/z;->c()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroidx/work/impl/w;->b:Landroid/content/Context;

    const-class v2, Landroidx/work/impl/background/systemalarm/RescheduleReceiver;

    invoke-static {v0, v2, v1}, Landroidx/work/impl/utils/f;->a(Landroid/content/Context;Ljava/lang/Class;Z)V

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    sget-object v2, Landroidx/work/x$a;->a:Landroidx/work/x$a;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    aput-object v4, v3, v1

    invoke-interface {v0, v2, v3}, Landroidx/work/impl/c/z;->a(Landroidx/work/x$a;[Ljava/lang/String;)I

    iget-object v0, p0, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    iget-object v1, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroidx/work/impl/c/z;->a(Ljava/lang/String;J)I

    :cond_1
    iget-object v0, p0, Landroidx/work/impl/w;->f:Landroidx/work/impl/c/y;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroidx/work/impl/w;->g:Landroidx/work/ListenableWorker;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroidx/work/impl/w;->g:Landroidx/work/ListenableWorker;

    invoke-virtual {v0}, Landroidx/work/ListenableWorker;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroidx/work/impl/w;->k:Landroidx/work/impl/foreground/a;

    iget-object v1, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroidx/work/impl/foreground/a;->a(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/v;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/v;->e()V

    iget-object v0, p0, Landroidx/work/impl/w;->r:Landroidx/work/impl/utils/a/e;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroidx/work/impl/utils/a/e;->b(Ljava/lang/Object;)Z

    return-void

    :catchall_0
    move-exception p1

    iget-object v0, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/v;->e()V

    throw p1
.end method

.method private e()V
    .locals 6

    iget-object v0, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/v;->c()V

    const/4 v0, 0x1

    :try_start_0
    iget-object v1, p0, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    sget-object v2, Landroidx/work/x$a;->a:Landroidx/work/x$a;

    new-array v3, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-interface {v1, v2, v3}, Landroidx/work/impl/c/z;->a(Landroidx/work/x$a;[Ljava/lang/String;)I

    iget-object v1, p0, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    iget-object v2, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-interface {v1, v2, v3, v4}, Landroidx/work/impl/c/z;->b(Ljava/lang/String;J)V

    iget-object v1, p0, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    iget-object v2, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    const-wide/16 v3, -0x1

    invoke-interface {v1, v2, v3, v4}, Landroidx/work/impl/c/z;->a(Ljava/lang/String;J)I

    iget-object v1, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/room/v;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/room/v;->e()V

    invoke-direct {p0, v0}, Landroidx/work/impl/w;->a(Z)V

    return-void

    :catchall_0
    move-exception v1

    iget-object v2, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v2}, Landroidx/room/v;->e()V

    invoke-direct {p0, v0}, Landroidx/work/impl/w;->a(Z)V

    throw v1
.end method

.method private f()V
    .locals 5

    iget-object v0, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/v;->c()V

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    iget-object v2, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-interface {v1, v2, v3, v4}, Landroidx/work/impl/c/z;->b(Ljava/lang/String;J)V

    iget-object v1, p0, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    sget-object v2, Landroidx/work/x$a;->a:Landroidx/work/x$a;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    aput-object v4, v3, v0

    invoke-interface {v1, v2, v3}, Landroidx/work/impl/c/z;->a(Landroidx/work/x$a;[Ljava/lang/String;)I

    iget-object v1, p0, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    iget-object v2, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    invoke-interface {v1, v2}, Landroidx/work/impl/c/z;->e(Ljava/lang/String;)I

    iget-object v1, p0, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    iget-object v2, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    const-wide/16 v3, -0x1

    invoke-interface {v1, v2, v3, v4}, Landroidx/work/impl/c/z;->a(Ljava/lang/String;J)I

    iget-object v1, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/room/v;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/room/v;->e()V

    invoke-direct {p0, v0}, Landroidx/work/impl/w;->a(Z)V

    return-void

    :catchall_0
    move-exception v1

    iget-object v2, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v2}, Landroidx/room/v;->e()V

    invoke-direct {p0, v0}, Landroidx/work/impl/w;->a(Z)V

    throw v1
.end method

.method private g()V
    .locals 7

    iget-object v0, p0, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    iget-object v1, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroidx/work/impl/c/z;->c(Ljava/lang/String;)Landroidx/work/x$a;

    move-result-object v0

    sget-object v1, Landroidx/work/x$a;->b:Landroidx/work/x$a;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    invoke-static {}, Landroidx/work/n;->a()Landroidx/work/n;

    move-result-object v0

    sget-object v1, Landroidx/work/impl/w;->a:Ljava/lang/String;

    new-array v4, v2, [Ljava/lang/Object;

    iget-object v5, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    aput-object v5, v4, v3

    const-string v5, "Status for %s is RUNNING;not doing any work and rescheduling for later execution"

    invoke-static {v5, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-array v3, v3, [Ljava/lang/Throwable;

    invoke-virtual {v0, v1, v4, v3}, Landroidx/work/n;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Throwable;)V

    invoke-direct {p0, v2}, Landroidx/work/impl/w;->a(Z)V

    goto :goto_0

    :cond_0
    invoke-static {}, Landroidx/work/n;->a()Landroidx/work/n;

    move-result-object v1

    sget-object v4, Landroidx/work/impl/w;->a:Ljava/lang/String;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    aput-object v6, v5, v3

    aput-object v0, v5, v2

    const-string v0, "Status for %s is %s; not doing any work"

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v2, v3, [Ljava/lang/Throwable;

    invoke-virtual {v1, v4, v0, v2}, Landroidx/work/n;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Throwable;)V

    invoke-direct {p0, v3}, Landroidx/work/impl/w;->a(Z)V

    :goto_0
    return-void
.end method

.method private h()V
    .locals 16

    move-object/from16 v1, p0

    invoke-direct/range {p0 .. p0}, Landroidx/work/impl/w;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, v1, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/v;->c()V

    :try_start_0
    iget-object v0, v1, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    iget-object v2, v1, Landroidx/work/impl/w;->c:Ljava/lang/String;

    invoke-interface {v0, v2}, Landroidx/work/impl/c/z;->d(Ljava/lang/String;)Landroidx/work/impl/c/y;

    move-result-object v0

    iput-object v0, v1, Landroidx/work/impl/w;->f:Landroidx/work/impl/c/y;

    iget-object v0, v1, Landroidx/work/impl/w;->f:Landroidx/work/impl/c/y;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v0, :cond_1

    invoke-static {}, Landroidx/work/n;->a()Landroidx/work/n;

    move-result-object v0

    sget-object v4, Landroidx/work/impl/w;->a:Ljava/lang/String;

    const-string v5, "Didn\'t find WorkSpec for id %s"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v6, v1, Landroidx/work/impl/w;->c:Ljava/lang/String;

    aput-object v6, v2, v3

    invoke-static {v5, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-array v5, v3, [Ljava/lang/Throwable;

    invoke-virtual {v0, v4, v2, v5}, Landroidx/work/n;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Throwable;)V

    invoke-direct {v1, v3}, Landroidx/work/impl/w;->a(Z)V

    iget-object v0, v1, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/v;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, v1, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/v;->e()V

    return-void

    :cond_1
    :try_start_1
    iget-object v0, v1, Landroidx/work/impl/w;->f:Landroidx/work/impl/c/y;

    iget-object v0, v0, Landroidx/work/impl/c/y;->d:Landroidx/work/x$a;

    sget-object v4, Landroidx/work/x$a;->a:Landroidx/work/x$a;

    if-eq v0, v4, :cond_2

    invoke-direct/range {p0 .. p0}, Landroidx/work/impl/w;->g()V

    iget-object v0, v1, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/v;->k()V

    invoke-static {}, Landroidx/work/n;->a()Landroidx/work/n;

    move-result-object v0

    sget-object v4, Landroidx/work/impl/w;->a:Ljava/lang/String;

    const-string v5, "%s is not in ENQUEUED state. Nothing more to do."

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v6, v1, Landroidx/work/impl/w;->f:Landroidx/work/impl/c/y;

    iget-object v6, v6, Landroidx/work/impl/c/y;->e:Ljava/lang/String;

    aput-object v6, v2, v3

    invoke-static {v5, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Throwable;

    invoke-virtual {v0, v4, v2, v3}, Landroidx/work/n;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, v1, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/v;->e()V

    return-void

    :cond_2
    :try_start_2
    iget-object v0, v1, Landroidx/work/impl/w;->f:Landroidx/work/impl/c/y;

    invoke-virtual {v0}, Landroidx/work/impl/c/y;->d()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, v1, Landroidx/work/impl/w;->f:Landroidx/work/impl/c/y;

    invoke-virtual {v0}, Landroidx/work/impl/c/y;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v0, v1, Landroidx/work/impl/w;->f:Landroidx/work/impl/c/y;

    iget-wide v6, v0, Landroidx/work/impl/c/y;->p:J

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-nez v0, :cond_4

    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v3

    :goto_0
    if-nez v0, :cond_5

    iget-object v0, v1, Landroidx/work/impl/w;->f:Landroidx/work/impl/c/y;

    invoke-virtual {v0}, Landroidx/work/impl/c/y;->a()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-gez v0, :cond_5

    invoke-static {}, Landroidx/work/n;->a()Landroidx/work/n;

    move-result-object v0

    sget-object v4, Landroidx/work/impl/w;->a:Ljava/lang/String;

    const-string v5, "Delaying execution for %s because it is being executed before schedule."

    new-array v6, v2, [Ljava/lang/Object;

    iget-object v7, v1, Landroidx/work/impl/w;->f:Landroidx/work/impl/c/y;

    iget-object v7, v7, Landroidx/work/impl/c/y;->e:Ljava/lang/String;

    aput-object v7, v6, v3

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-array v3, v3, [Ljava/lang/Throwable;

    invoke-virtual {v0, v4, v5, v3}, Landroidx/work/n;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Throwable;)V

    invoke-direct {v1, v2}, Landroidx/work/impl/w;->a(Z)V

    iget-object v0, v1, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/v;->k()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, v1, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/v;->e()V

    return-void

    :cond_5
    :try_start_3
    iget-object v0, v1, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/v;->k()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v0, v1, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/v;->e()V

    iget-object v0, v1, Landroidx/work/impl/w;->f:Landroidx/work/impl/c/y;

    invoke-virtual {v0}, Landroidx/work/impl/c/y;->d()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, v1, Landroidx/work/impl/w;->f:Landroidx/work/impl/c/y;

    iget-object v0, v0, Landroidx/work/impl/c/y;->g:Landroidx/work/f;

    :goto_1
    move-object v6, v0

    goto :goto_2

    :cond_6
    iget-object v0, v1, Landroidx/work/impl/w;->j:Landroidx/work/c;

    invoke-virtual {v0}, Landroidx/work/c;->d()Landroidx/work/m;

    move-result-object v0

    iget-object v4, v1, Landroidx/work/impl/w;->f:Landroidx/work/impl/c/y;

    iget-object v4, v4, Landroidx/work/impl/c/y;->f:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroidx/work/m;->b(Ljava/lang/String;)Landroidx/work/k;

    move-result-object v0

    if-nez v0, :cond_7

    invoke-static {}, Landroidx/work/n;->a()Landroidx/work/n;

    move-result-object v0

    sget-object v4, Landroidx/work/impl/w;->a:Ljava/lang/String;

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, v1, Landroidx/work/impl/w;->f:Landroidx/work/impl/c/y;

    iget-object v5, v5, Landroidx/work/impl/c/y;->f:Ljava/lang/String;

    aput-object v5, v2, v3

    const-string v5, "Could not create Input Merger %s"

    invoke-static {v5, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Throwable;

    invoke-virtual {v0, v4, v2, v3}, Landroidx/work/n;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Throwable;)V

    invoke-virtual/range {p0 .. p0}, Landroidx/work/impl/w;->d()V

    return-void

    :cond_7
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v5, v1, Landroidx/work/impl/w;->f:Landroidx/work/impl/c/y;

    iget-object v5, v5, Landroidx/work/impl/c/y;->g:Landroidx/work/f;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v5, v1, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    iget-object v6, v1, Landroidx/work/impl/w;->c:Ljava/lang/String;

    invoke-interface {v5, v6}, Landroidx/work/impl/c/z;->f(Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v0, v4}, Landroidx/work/k;->a(Ljava/util/List;)Landroidx/work/f;

    move-result-object v0

    goto :goto_1

    :goto_2
    new-instance v0, Landroidx/work/WorkerParameters;

    iget-object v4, v1, Landroidx/work/impl/w;->c:Ljava/lang/String;

    invoke-static {v4}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v5

    iget-object v7, v1, Landroidx/work/impl/w;->p:Ljava/util/List;

    iget-object v8, v1, Landroidx/work/impl/w;->e:Landroidx/work/WorkerParameters$a;

    iget-object v4, v1, Landroidx/work/impl/w;->f:Landroidx/work/impl/c/y;

    iget v9, v4, Landroidx/work/impl/c/y;->m:I

    iget-object v4, v1, Landroidx/work/impl/w;->j:Landroidx/work/c;

    invoke-virtual {v4}, Landroidx/work/c;->c()Ljava/util/concurrent/Executor;

    move-result-object v10

    iget-object v11, v1, Landroidx/work/impl/w;->h:Landroidx/work/impl/utils/b/a;

    iget-object v4, v1, Landroidx/work/impl/w;->j:Landroidx/work/c;

    invoke-virtual {v4}, Landroidx/work/c;->k()Landroidx/work/C;

    move-result-object v12

    new-instance v13, Landroidx/work/impl/utils/r;

    iget-object v4, v1, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    iget-object v14, v1, Landroidx/work/impl/w;->h:Landroidx/work/impl/utils/b/a;

    invoke-direct {v13, v4, v14}, Landroidx/work/impl/utils/r;-><init>(Landroidx/work/impl/WorkDatabase;Landroidx/work/impl/utils/b/a;)V

    new-instance v14, Landroidx/work/impl/utils/q;

    iget-object v4, v1, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    iget-object v15, v1, Landroidx/work/impl/w;->k:Landroidx/work/impl/foreground/a;

    iget-object v3, v1, Landroidx/work/impl/w;->h:Landroidx/work/impl/utils/b/a;

    invoke-direct {v14, v4, v15, v3}, Landroidx/work/impl/utils/q;-><init>(Landroidx/work/impl/WorkDatabase;Landroidx/work/impl/foreground/a;Landroidx/work/impl/utils/b/a;)V

    move-object v4, v0

    invoke-direct/range {v4 .. v14}, Landroidx/work/WorkerParameters;-><init>(Ljava/util/UUID;Landroidx/work/f;Ljava/util/Collection;Landroidx/work/WorkerParameters$a;ILjava/util/concurrent/Executor;Landroidx/work/impl/utils/b/a;Landroidx/work/C;Landroidx/work/t;Landroidx/work/i;)V

    iget-object v3, v1, Landroidx/work/impl/w;->g:Landroidx/work/ListenableWorker;

    if-nez v3, :cond_8

    iget-object v3, v1, Landroidx/work/impl/w;->j:Landroidx/work/c;

    invoke-virtual {v3}, Landroidx/work/c;->k()Landroidx/work/C;

    move-result-object v3

    iget-object v4, v1, Landroidx/work/impl/w;->b:Landroid/content/Context;

    iget-object v5, v1, Landroidx/work/impl/w;->f:Landroidx/work/impl/c/y;

    iget-object v5, v5, Landroidx/work/impl/c/y;->e:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v0}, Landroidx/work/C;->b(Landroid/content/Context;Ljava/lang/String;Landroidx/work/WorkerParameters;)Landroidx/work/ListenableWorker;

    move-result-object v3

    iput-object v3, v1, Landroidx/work/impl/w;->g:Landroidx/work/ListenableWorker;

    :cond_8
    iget-object v3, v1, Landroidx/work/impl/w;->g:Landroidx/work/ListenableWorker;

    if-nez v3, :cond_9

    invoke-static {}, Landroidx/work/n;->a()Landroidx/work/n;

    move-result-object v0

    sget-object v3, Landroidx/work/impl/w;->a:Ljava/lang/String;

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, v1, Landroidx/work/impl/w;->f:Landroidx/work/impl/c/y;

    iget-object v4, v4, Landroidx/work/impl/c/y;->e:Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v2, v5

    const-string v4, "Could not create Worker %s"

    invoke-static {v4, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-array v4, v5, [Ljava/lang/Throwable;

    invoke-virtual {v0, v3, v2, v4}, Landroidx/work/n;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Throwable;)V

    invoke-virtual/range {p0 .. p0}, Landroidx/work/impl/w;->d()V

    return-void

    :cond_9
    const/4 v5, 0x0

    invoke-virtual {v3}, Landroidx/work/ListenableWorker;->i()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-static {}, Landroidx/work/n;->a()Landroidx/work/n;

    move-result-object v0

    sget-object v3, Landroidx/work/impl/w;->a:Ljava/lang/String;

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, v1, Landroidx/work/impl/w;->f:Landroidx/work/impl/c/y;

    iget-object v4, v4, Landroidx/work/impl/c/y;->e:Ljava/lang/String;

    aput-object v4, v2, v5

    const-string v4, "Received an already-used Worker %s; WorkerFactory should return new instances"

    invoke-static {v4, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-array v4, v5, [Ljava/lang/Throwable;

    invoke-virtual {v0, v3, v2, v4}, Landroidx/work/n;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Throwable;)V

    invoke-virtual/range {p0 .. p0}, Landroidx/work/impl/w;->d()V

    return-void

    :cond_a
    iget-object v2, v1, Landroidx/work/impl/w;->g:Landroidx/work/ListenableWorker;

    invoke-virtual {v2}, Landroidx/work/ListenableWorker;->k()V

    invoke-direct/range {p0 .. p0}, Landroidx/work/impl/w;->k()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-direct/range {p0 .. p0}, Landroidx/work/impl/w;->j()Z

    move-result v2

    if-eqz v2, :cond_b

    return-void

    :cond_b
    invoke-static {}, Landroidx/work/impl/utils/a/e;->d()Landroidx/work/impl/utils/a/e;

    move-result-object v2

    new-instance v9, Landroidx/work/impl/utils/o;

    iget-object v4, v1, Landroidx/work/impl/w;->b:Landroid/content/Context;

    iget-object v5, v1, Landroidx/work/impl/w;->f:Landroidx/work/impl/c/y;

    iget-object v6, v1, Landroidx/work/impl/w;->g:Landroidx/work/ListenableWorker;

    invoke-virtual {v0}, Landroidx/work/WorkerParameters;->b()Landroidx/work/i;

    move-result-object v7

    iget-object v8, v1, Landroidx/work/impl/w;->h:Landroidx/work/impl/utils/b/a;

    move-object v3, v9

    invoke-direct/range {v3 .. v8}, Landroidx/work/impl/utils/o;-><init>(Landroid/content/Context;Landroidx/work/impl/c/y;Landroidx/work/ListenableWorker;Landroidx/work/i;Landroidx/work/impl/utils/b/a;)V

    iget-object v0, v1, Landroidx/work/impl/w;->h:Landroidx/work/impl/utils/b/a;

    invoke-interface {v0}, Landroidx/work/impl/utils/b/a;->a()Ljava/util/concurrent/Executor;

    move-result-object v0

    invoke-interface {v0, v9}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    invoke-virtual {v9}, Landroidx/work/impl/utils/o;->a()Lcom/google/common/util/concurrent/a;

    move-result-object v0

    new-instance v3, Landroidx/work/impl/u;

    invoke-direct {v3, v1, v0, v2}, Landroidx/work/impl/u;-><init>(Landroidx/work/impl/w;Lcom/google/common/util/concurrent/a;Landroidx/work/impl/utils/a/e;)V

    iget-object v4, v1, Landroidx/work/impl/w;->h:Landroidx/work/impl/utils/b/a;

    invoke-interface {v4}, Landroidx/work/impl/utils/b/a;->a()Ljava/util/concurrent/Executor;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Lcom/google/common/util/concurrent/a;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    iget-object v0, v1, Landroidx/work/impl/w;->q:Ljava/lang/String;

    new-instance v3, Landroidx/work/impl/v;

    invoke-direct {v3, v1, v2, v0}, Landroidx/work/impl/v;-><init>(Landroidx/work/impl/w;Landroidx/work/impl/utils/a/e;Ljava/lang/String;)V

    iget-object v0, v1, Landroidx/work/impl/w;->h:Landroidx/work/impl/utils/b/a;

    invoke-interface {v0}, Landroidx/work/impl/utils/b/a;->b()Landroidx/work/impl/utils/i;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroidx/work/impl/utils/a/b;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    goto :goto_3

    :cond_c
    invoke-direct/range {p0 .. p0}, Landroidx/work/impl/w;->g()V

    :goto_3
    return-void

    :catchall_0
    move-exception v0

    iget-object v2, v1, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v2}, Landroidx/room/v;->e()V

    throw v0
.end method

.method private i()V
    .locals 10

    iget-object v0, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/v;->c()V

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    sget-object v2, Landroidx/work/x$a;->c:Landroidx/work/x$a;

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/String;

    iget-object v5, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    aput-object v5, v4, v0

    invoke-interface {v1, v2, v4}, Landroidx/work/impl/c/z;->a(Landroidx/work/x$a;[Ljava/lang/String;)I

    iget-object v1, p0, Landroidx/work/impl/w;->i:Landroidx/work/ListenableWorker$a;

    check-cast v1, Landroidx/work/ListenableWorker$a$c;

    invoke-virtual {v1}, Landroidx/work/ListenableWorker$a$c;->d()Landroidx/work/f;

    move-result-object v1

    iget-object v2, p0, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    iget-object v4, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    invoke-interface {v2, v4, v1}, Landroidx/work/impl/c/z;->a(Ljava/lang/String;Landroidx/work/f;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-object v4, p0, Landroidx/work/impl/w;->n:Landroidx/work/impl/c/b;

    iget-object v5, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    invoke-interface {v4, v5}, Landroidx/work/impl/c/b;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iget-object v6, p0, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    invoke-interface {v6, v5}, Landroidx/work/impl/c/z;->c(Ljava/lang/String;)Landroidx/work/x$a;

    move-result-object v6

    sget-object v7, Landroidx/work/x$a;->e:Landroidx/work/x$a;

    if-ne v6, v7, :cond_0

    iget-object v6, p0, Landroidx/work/impl/w;->n:Landroidx/work/impl/c/b;

    invoke-interface {v6, v5}, Landroidx/work/impl/c/b;->b(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-static {}, Landroidx/work/n;->a()Landroidx/work/n;

    move-result-object v6

    sget-object v7, Landroidx/work/impl/w;->a:Ljava/lang/String;

    const-string v8, "Setting status to enqueued for %s"

    new-array v9, v3, [Ljava/lang/Object;

    aput-object v5, v9, v0

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    new-array v9, v0, [Ljava/lang/Throwable;

    invoke-virtual {v6, v7, v8, v9}, Landroidx/work/n;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Throwable;)V

    iget-object v6, p0, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    sget-object v7, Landroidx/work/x$a;->a:Landroidx/work/x$a;

    new-array v8, v3, [Ljava/lang/String;

    aput-object v5, v8, v0

    invoke-interface {v6, v7, v8}, Landroidx/work/impl/c/z;->a(Landroidx/work/x$a;[Ljava/lang/String;)I

    iget-object v6, p0, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    invoke-interface {v6, v5, v1, v2}, Landroidx/work/impl/c/z;->b(Ljava/lang/String;J)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/room/v;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/room/v;->e()V

    invoke-direct {p0, v0}, Landroidx/work/impl/w;->a(Z)V

    return-void

    :catchall_0
    move-exception v1

    iget-object v2, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v2}, Landroidx/room/v;->e()V

    invoke-direct {p0, v0}, Landroidx/work/impl/w;->a(Z)V

    throw v1
.end method

.method private j()Z
    .locals 6

    iget-boolean v0, p0, Landroidx/work/impl/w;->t:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-static {}, Landroidx/work/n;->a()Landroidx/work/n;

    move-result-object v0

    sget-object v2, Landroidx/work/impl/w;->a:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Object;

    iget-object v5, p0, Landroidx/work/impl/w;->q:Ljava/lang/String;

    aput-object v5, v4, v1

    const-string v5, "Work interrupted for %s"

    invoke-static {v5, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-array v5, v1, [Ljava/lang/Throwable;

    invoke-virtual {v0, v2, v4, v5}, Landroidx/work/n;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Throwable;)V

    iget-object v0, p0, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    iget-object v2, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    invoke-interface {v0, v2}, Landroidx/work/impl/c/z;->c(Ljava/lang/String;)Landroidx/work/x$a;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Landroidx/work/impl/w;->a(Z)V

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroidx/work/x$a;->a()Z

    move-result v0

    xor-int/2addr v0, v3

    invoke-direct {p0, v0}, Landroidx/work/impl/w;->a(Z)V

    :goto_0
    return v3

    :cond_1
    return v1
.end method

.method private k()Z
    .locals 6

    iget-object v0, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/v;->c()V

    :try_start_0
    iget-object v0, p0, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    iget-object v1, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroidx/work/impl/c/z;->c(Ljava/lang/String;)Landroidx/work/x$a;

    move-result-object v0

    sget-object v1, Landroidx/work/x$a;->a:Landroidx/work/x$a;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    sget-object v1, Landroidx/work/x$a;->b:Landroidx/work/x$a;

    new-array v4, v2, [Ljava/lang/String;

    iget-object v5, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    aput-object v5, v4, v3

    invoke-interface {v0, v1, v4}, Landroidx/work/impl/c/z;->a(Landroidx/work/x$a;[Ljava/lang/String;)I

    iget-object v0, p0, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    iget-object v1, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroidx/work/impl/c/z;->g(Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    move v2, v3

    :goto_0
    iget-object v0, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/v;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/v;->e()V

    return v2

    :catchall_0
    move-exception v0

    iget-object v1, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/room/v;->e()V

    throw v0
.end method


# virtual methods
.method public a()Lcom/google/common/util/concurrent/a;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/a<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Landroidx/work/impl/w;->r:Landroidx/work/impl/utils/a/e;

    return-object v0
.end method

.method public b()V
    .locals 4
    .annotation build Landroidx/annotation/RestrictTo;
        value = {
            .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
        }
    .end annotation

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroidx/work/impl/w;->t:Z

    invoke-direct {p0}, Landroidx/work/impl/w;->j()Z

    iget-object v1, p0, Landroidx/work/impl/w;->s:Lcom/google/common/util/concurrent/a;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v1

    iget-object v3, p0, Landroidx/work/impl/w;->s:Lcom/google/common/util/concurrent/a;

    invoke-interface {v3, v0}, Ljava/util/concurrent/Future;->cancel(Z)Z

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    iget-object v3, p0, Landroidx/work/impl/w;->g:Landroidx/work/ListenableWorker;

    if-eqz v3, :cond_1

    if-nez v1, :cond_1

    invoke-virtual {v3}, Landroidx/work/ListenableWorker;->m()V

    goto :goto_1

    :cond_1
    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Landroidx/work/impl/w;->f:Landroidx/work/impl/c/y;

    aput-object v1, v0, v2

    const-string v1, "WorkSpec %s is already done. Not interrupting."

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Landroidx/work/n;->a()Landroidx/work/n;

    move-result-object v1

    sget-object v3, Landroidx/work/impl/w;->a:Ljava/lang/String;

    new-array v2, v2, [Ljava/lang/Throwable;

    invoke-virtual {v1, v3, v0, v2}, Landroidx/work/n;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Throwable;)V

    :goto_1
    return-void
.end method

.method c()V
    .locals 3

    invoke-direct {p0}, Landroidx/work/impl/w;->j()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/v;->c()V

    :try_start_0
    iget-object v0, p0, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    iget-object v1, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroidx/work/impl/c/z;->c(Ljava/lang/String;)Landroidx/work/x$a;

    move-result-object v0

    iget-object v1, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/work/impl/WorkDatabase;->s()Landroidx/work/impl/c/s;

    move-result-object v1

    iget-object v2, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    invoke-interface {v1, v2}, Landroidx/work/impl/c/s;->delete(Ljava/lang/String;)V

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroidx/work/impl/w;->a(Z)V

    goto :goto_0

    :cond_0
    sget-object v1, Landroidx/work/x$a;->b:Landroidx/work/x$a;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Landroidx/work/impl/w;->i:Landroidx/work/ListenableWorker$a;

    invoke-direct {p0, v0}, Landroidx/work/impl/w;->a(Landroidx/work/ListenableWorker$a;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroidx/work/x$a;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Landroidx/work/impl/w;->e()V

    :cond_2
    :goto_0
    iget-object v0, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/v;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/v;->e()V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/room/v;->e()V

    throw v0

    :cond_3
    :goto_1
    iget-object v0, p0, Landroidx/work/impl/w;->d:Ljava/util/List;

    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroidx/work/impl/e;

    iget-object v2, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    invoke-interface {v1, v2}, Landroidx/work/impl/e;->a(Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    iget-object v0, p0, Landroidx/work/impl/w;->j:Landroidx/work/c;

    iget-object v1, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    iget-object v2, p0, Landroidx/work/impl/w;->d:Ljava/util/List;

    invoke-static {v0, v1, v2}, Landroidx/work/impl/f;->a(Landroidx/work/c;Landroidx/work/impl/WorkDatabase;Ljava/util/List;)V

    :cond_5
    return-void
.end method

.method d()V
    .locals 4
    .annotation build Landroidx/annotation/VisibleForTesting;
    .end annotation

    iget-object v0, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/v;->c()V

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    invoke-direct {p0, v1}, Landroidx/work/impl/w;->a(Ljava/lang/String;)V

    iget-object v1, p0, Landroidx/work/impl/w;->i:Landroidx/work/ListenableWorker$a;

    check-cast v1, Landroidx/work/ListenableWorker$a$a;

    invoke-virtual {v1}, Landroidx/work/ListenableWorker$a$a;->d()Landroidx/work/f;

    move-result-object v1

    iget-object v2, p0, Landroidx/work/impl/w;->m:Landroidx/work/impl/c/z;

    iget-object v3, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Landroidx/work/impl/c/z;->a(Ljava/lang/String;Landroidx/work/f;)V

    iget-object v1, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/room/v;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/room/v;->e()V

    invoke-direct {p0, v0}, Landroidx/work/impl/w;->a(Z)V

    return-void

    :catchall_0
    move-exception v1

    iget-object v2, p0, Landroidx/work/impl/w;->l:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v2}, Landroidx/room/v;->e()V

    invoke-direct {p0, v0}, Landroidx/work/impl/w;->a(Z)V

    throw v1
.end method

.method public run()V
    .locals 2
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    iget-object v0, p0, Landroidx/work/impl/w;->o:Landroidx/work/impl/c/L;

    iget-object v1, p0, Landroidx/work/impl/w;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroidx/work/impl/c/L;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Landroidx/work/impl/w;->p:Ljava/util/List;

    iget-object v0, p0, Landroidx/work/impl/w;->p:Ljava/util/List;

    invoke-direct {p0, v0}, Landroidx/work/impl/w;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroidx/work/impl/w;->q:Ljava/lang/String;

    invoke-direct {p0}, Landroidx/work/impl/w;->h()V

    return-void
.end method
