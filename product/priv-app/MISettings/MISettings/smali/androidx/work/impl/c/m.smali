.class public final Landroidx/work/impl/c/m;
.super Ljava/lang/Object;
.source "SystemIdInfoDao_Impl.java"

# interfaces
.implements Landroidx/work/impl/c/j;


# instance fields
.field private final a:Landroidx/room/v;

.field private final b:Landroidx/room/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/room/c<",
            "Landroidx/work/impl/c/i;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroidx/room/B;


# direct methods
.method public constructor <init>(Landroidx/room/v;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroidx/work/impl/c/m;->a:Landroidx/room/v;

    new-instance v0, Landroidx/work/impl/c/k;

    invoke-direct {v0, p0, p1}, Landroidx/work/impl/c/k;-><init>(Landroidx/work/impl/c/m;Landroidx/room/v;)V

    iput-object v0, p0, Landroidx/work/impl/c/m;->b:Landroidx/room/c;

    new-instance v0, Landroidx/work/impl/c/l;

    invoke-direct {v0, p0, p1}, Landroidx/work/impl/c/l;-><init>(Landroidx/work/impl/c/m;Landroidx/room/v;)V

    iput-object v0, p0, Landroidx/work/impl/c/m;->c:Landroidx/room/B;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Landroidx/work/impl/c/i;
    .locals 5

    const/4 v0, 0x1

    const-string v1, "SELECT `SystemIdInfo`.`work_spec_id` AS `work_spec_id`, `SystemIdInfo`.`system_id` AS `system_id` FROM SystemIdInfo WHERE work_spec_id=?"

    invoke-static {v1, v0}, Landroidx/room/y;->a(Ljava/lang/String;I)Landroidx/room/y;

    move-result-object v1

    if-nez p1, :cond_0

    invoke-virtual {v1, v0}, Landroidx/room/y;->a(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {v1, v0, p1}, Landroidx/room/y;->a(ILjava/lang/String;)V

    :goto_0
    iget-object p1, p0, Landroidx/work/impl/c/m;->a:Landroidx/room/v;

    invoke-virtual {p1}, Landroidx/room/v;->b()V

    iget-object p1, p0, Landroidx/work/impl/c/m;->a:Landroidx/room/v;

    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-static {p1, v1, v0, v2}, Landroidx/room/b/c;->a(Landroidx/room/v;La/p/a/e;ZLandroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object p1

    :try_start_0
    const-string v0, "work_spec_id"

    invoke-static {p1, v0}, Landroidx/room/b/b;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    const-string v3, "system_id"

    invoke-static {p1, v3}, Landroidx/room/b/b;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    new-instance v3, Landroidx/work/impl/c/i;

    invoke-direct {v3, v0, v2}, Landroidx/work/impl/c/i;-><init>(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v2, v3

    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    invoke-virtual {v1}, Landroidx/room/y;->b()V

    return-object v2

    :catchall_0
    move-exception v0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    invoke-virtual {v1}, Landroidx/room/y;->b()V

    throw v0
.end method

.method public a()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    const-string v1, "SELECT DISTINCT work_spec_id FROM SystemIdInfo"

    invoke-static {v1, v0}, Landroidx/room/y;->a(Ljava/lang/String;I)Landroidx/room/y;

    move-result-object v1

    iget-object v2, p0, Landroidx/work/impl/c/m;->a:Landroidx/room/v;

    invoke-virtual {v2}, Landroidx/room/v;->b()V

    iget-object v2, p0, Landroidx/work/impl/c/m;->a:Landroidx/room/v;

    const/4 v3, 0x0

    invoke-static {v2, v1, v0, v3}, Landroidx/room/b/c;->a(Landroidx/room/v;La/p/a/e;ZLandroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v2

    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    invoke-virtual {v1}, Landroidx/room/y;->b()V

    return-object v3

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    invoke-virtual {v1}, Landroidx/room/y;->b()V

    throw v0
.end method

.method public a(Landroidx/work/impl/c/i;)V
    .locals 1

    iget-object v0, p0, Landroidx/work/impl/c/m;->a:Landroidx/room/v;

    invoke-virtual {v0}, Landroidx/room/v;->b()V

    iget-object v0, p0, Landroidx/work/impl/c/m;->a:Landroidx/room/v;

    invoke-virtual {v0}, Landroidx/room/v;->c()V

    :try_start_0
    iget-object v0, p0, Landroidx/work/impl/c/m;->b:Landroidx/room/c;

    invoke-virtual {v0, p1}, Landroidx/room/c;->a(Ljava/lang/Object;)V

    iget-object p1, p0, Landroidx/work/impl/c/m;->a:Landroidx/room/v;

    invoke-virtual {p1}, Landroidx/room/v;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Landroidx/work/impl/c/m;->a:Landroidx/room/v;

    invoke-virtual {p1}, Landroidx/room/v;->e()V

    return-void

    :catchall_0
    move-exception p1

    iget-object v0, p0, Landroidx/work/impl/c/m;->a:Landroidx/room/v;

    invoke-virtual {v0}, Landroidx/room/v;->e()V

    throw p1
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Landroidx/work/impl/c/m;->a:Landroidx/room/v;

    invoke-virtual {v0}, Landroidx/room/v;->b()V

    iget-object v0, p0, Landroidx/work/impl/c/m;->c:Landroidx/room/B;

    invoke-virtual {v0}, Landroidx/room/B;->a()La/p/a/f;

    move-result-object v0

    const/4 v1, 0x1

    if-nez p1, :cond_0

    invoke-interface {v0, v1}, La/p/a/d;->a(I)V

    goto :goto_0

    :cond_0
    invoke-interface {v0, v1, p1}, La/p/a/d;->a(ILjava/lang/String;)V

    :goto_0
    iget-object p1, p0, Landroidx/work/impl/c/m;->a:Landroidx/room/v;

    invoke-virtual {p1}, Landroidx/room/v;->c()V

    :try_start_0
    invoke-interface {v0}, La/p/a/f;->i()I

    iget-object p1, p0, Landroidx/work/impl/c/m;->a:Landroidx/room/v;

    invoke-virtual {p1}, Landroidx/room/v;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Landroidx/work/impl/c/m;->a:Landroidx/room/v;

    invoke-virtual {p1}, Landroidx/room/v;->e()V

    iget-object p1, p0, Landroidx/work/impl/c/m;->c:Landroidx/room/B;

    invoke-virtual {p1, v0}, Landroidx/room/B;->a(La/p/a/f;)V

    return-void

    :catchall_0
    move-exception p1

    iget-object v1, p0, Landroidx/work/impl/c/m;->a:Landroidx/room/v;

    invoke-virtual {v1}, Landroidx/room/v;->e()V

    iget-object v1, p0, Landroidx/work/impl/c/m;->c:Landroidx/room/B;

    invoke-virtual {v1, v0}, Landroidx/room/B;->a(La/p/a/f;)V

    throw p1
.end method
