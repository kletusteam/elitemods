.class public final Landroidx/work/d$a;
.super Ljava/lang/Object;
.source "Constraints.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/work/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field a:Z

.field b:Z

.field c:Landroidx/work/o;

.field d:Z

.field e:Z

.field f:J

.field g:J

.field h:Landroidx/work/e;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroidx/work/d$a;->a:Z

    iput-boolean v0, p0, Landroidx/work/d$a;->b:Z

    sget-object v1, Landroidx/work/o;->a:Landroidx/work/o;

    iput-object v1, p0, Landroidx/work/d$a;->c:Landroidx/work/o;

    iput-boolean v0, p0, Landroidx/work/d$a;->d:Z

    iput-boolean v0, p0, Landroidx/work/d$a;->e:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroidx/work/d$a;->f:J

    iput-wide v0, p0, Landroidx/work/d$a;->g:J

    new-instance v0, Landroidx/work/e;

    invoke-direct {v0}, Landroidx/work/e;-><init>()V

    iput-object v0, p0, Landroidx/work/d$a;->h:Landroidx/work/e;

    return-void
.end method


# virtual methods
.method public a(Landroidx/work/o;)Landroidx/work/d$a;
    .locals 0
    .param p1    # Landroidx/work/o;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iput-object p1, p0, Landroidx/work/d$a;->c:Landroidx/work/o;

    return-object p0
.end method

.method public a(Z)Landroidx/work/d$a;
    .locals 0
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iput-boolean p1, p0, Landroidx/work/d$a;->d:Z

    return-object p0
.end method

.method public a()Landroidx/work/d;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    new-instance v0, Landroidx/work/d;

    invoke-direct {v0, p0}, Landroidx/work/d;-><init>(Landroidx/work/d$a;)V

    return-object v0
.end method
