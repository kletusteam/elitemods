.class public final enum Landroidx/work/o;
.super Ljava/lang/Enum;
.source "NetworkType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Landroidx/work/o;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Landroidx/work/o;

.field public static final enum b:Landroidx/work/o;

.field public static final enum c:Landroidx/work/o;

.field public static final enum d:Landroidx/work/o;

.field public static final enum e:Landroidx/work/o;

.field public static final enum f:Landroidx/work/o;
    .annotation build Landroidx/annotation/RequiresApi;
        value = 0x1e
    .end annotation
.end field

.field private static final synthetic g:[Landroidx/work/o;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    new-instance v0, Landroidx/work/o;

    const/4 v1, 0x0

    const-string v2, "NOT_REQUIRED"

    invoke-direct {v0, v2, v1}, Landroidx/work/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroidx/work/o;->a:Landroidx/work/o;

    new-instance v0, Landroidx/work/o;

    const/4 v2, 0x1

    const-string v3, "CONNECTED"

    invoke-direct {v0, v3, v2}, Landroidx/work/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroidx/work/o;->b:Landroidx/work/o;

    new-instance v0, Landroidx/work/o;

    const/4 v3, 0x2

    const-string v4, "UNMETERED"

    invoke-direct {v0, v4, v3}, Landroidx/work/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroidx/work/o;->c:Landroidx/work/o;

    new-instance v0, Landroidx/work/o;

    const/4 v4, 0x3

    const-string v5, "NOT_ROAMING"

    invoke-direct {v0, v5, v4}, Landroidx/work/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroidx/work/o;->d:Landroidx/work/o;

    new-instance v0, Landroidx/work/o;

    const/4 v5, 0x4

    const-string v6, "METERED"

    invoke-direct {v0, v6, v5}, Landroidx/work/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroidx/work/o;->e:Landroidx/work/o;

    new-instance v0, Landroidx/work/o;

    const/4 v6, 0x5

    const-string v7, "TEMPORARILY_UNMETERED"

    invoke-direct {v0, v7, v6}, Landroidx/work/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroidx/work/o;->f:Landroidx/work/o;

    const/4 v0, 0x6

    new-array v0, v0, [Landroidx/work/o;

    sget-object v7, Landroidx/work/o;->a:Landroidx/work/o;

    aput-object v7, v0, v1

    sget-object v1, Landroidx/work/o;->b:Landroidx/work/o;

    aput-object v1, v0, v2

    sget-object v1, Landroidx/work/o;->c:Landroidx/work/o;

    aput-object v1, v0, v3

    sget-object v1, Landroidx/work/o;->d:Landroidx/work/o;

    aput-object v1, v0, v4

    sget-object v1, Landroidx/work/o;->e:Landroidx/work/o;

    aput-object v1, v0, v5

    sget-object v1, Landroidx/work/o;->f:Landroidx/work/o;

    aput-object v1, v0, v6

    sput-object v0, Landroidx/work/o;->g:[Landroidx/work/o;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroidx/work/o;
    .locals 1

    const-class v0, Landroidx/work/o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Landroidx/work/o;

    return-object p0
.end method

.method public static values()[Landroidx/work/o;
    .locals 1

    sget-object v0, Landroidx/work/o;->g:[Landroidx/work/o;

    invoke-virtual {v0}, [Landroidx/work/o;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroidx/work/o;

    return-object v0
.end method
