.class public final Landroidx/work/d;
.super Ljava/lang/Object;
.source "Constraints.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/work/d$a;
    }
.end annotation


# static fields
.field public static final a:Landroidx/work/d;


# instance fields
.field private b:Landroidx/work/o;
    .annotation build Landroidx/room/ColumnInfo;
        name = "required_network_type"
    .end annotation
.end field

.field private c:Z
    .annotation build Landroidx/room/ColumnInfo;
        name = "requires_charging"
    .end annotation
.end field

.field private d:Z
    .annotation build Landroidx/room/ColumnInfo;
        name = "requires_device_idle"
    .end annotation
.end field

.field private e:Z
    .annotation build Landroidx/room/ColumnInfo;
        name = "requires_battery_not_low"
    .end annotation
.end field

.field private f:Z
    .annotation build Landroidx/room/ColumnInfo;
        name = "requires_storage_not_low"
    .end annotation
.end field

.field private g:J
    .annotation build Landroidx/room/ColumnInfo;
        name = "trigger_content_update_delay"
    .end annotation
.end field

.field private h:J
    .annotation build Landroidx/room/ColumnInfo;
        name = "trigger_max_content_delay"
    .end annotation
.end field

.field private i:Landroidx/work/e;
    .annotation build Landroidx/room/ColumnInfo;
        name = "content_uri_triggers"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroidx/work/d$a;

    invoke-direct {v0}, Landroidx/work/d$a;-><init>()V

    invoke-virtual {v0}, Landroidx/work/d$a;->a()Landroidx/work/d;

    move-result-object v0

    sput-object v0, Landroidx/work/d;->a:Landroidx/work/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation build Landroidx/annotation/RestrictTo;
        value = {
            .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Landroidx/work/o;->a:Landroidx/work/o;

    iput-object v0, p0, Landroidx/work/d;->b:Landroidx/work/o;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroidx/work/d;->g:J

    iput-wide v0, p0, Landroidx/work/d;->h:J

    new-instance v0, Landroidx/work/e;

    invoke-direct {v0}, Landroidx/work/e;-><init>()V

    iput-object v0, p0, Landroidx/work/d;->i:Landroidx/work/e;

    return-void
.end method

.method constructor <init>(Landroidx/work/d$a;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Landroidx/work/o;->a:Landroidx/work/o;

    iput-object v0, p0, Landroidx/work/d;->b:Landroidx/work/o;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroidx/work/d;->g:J

    iput-wide v0, p0, Landroidx/work/d;->h:J

    new-instance v0, Landroidx/work/e;

    invoke-direct {v0}, Landroidx/work/e;-><init>()V

    iput-object v0, p0, Landroidx/work/d;->i:Landroidx/work/e;

    iget-boolean v0, p1, Landroidx/work/d$a;->a:Z

    iput-boolean v0, p0, Landroidx/work/d;->c:Z

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    iget-boolean v0, p1, Landroidx/work/d$a;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Landroidx/work/d;->d:Z

    iget-object v0, p1, Landroidx/work/d$a;->c:Landroidx/work/o;

    iput-object v0, p0, Landroidx/work/d;->b:Landroidx/work/o;

    iget-boolean v0, p1, Landroidx/work/d$a;->d:Z

    iput-boolean v0, p0, Landroidx/work/d;->e:Z

    iget-boolean v0, p1, Landroidx/work/d$a;->e:Z

    iput-boolean v0, p0, Landroidx/work/d;->f:Z

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_1

    iget-object v0, p1, Landroidx/work/d$a;->h:Landroidx/work/e;

    iput-object v0, p0, Landroidx/work/d;->i:Landroidx/work/e;

    iget-wide v0, p1, Landroidx/work/d$a;->f:J

    iput-wide v0, p0, Landroidx/work/d;->g:J

    iget-wide v0, p1, Landroidx/work/d$a;->g:J

    iput-wide v0, p0, Landroidx/work/d;->h:J

    :cond_1
    return-void
.end method

.method public constructor <init>(Landroidx/work/d;)V
    .locals 2
    .param p1    # Landroidx/work/d;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Landroidx/work/o;->a:Landroidx/work/o;

    iput-object v0, p0, Landroidx/work/d;->b:Landroidx/work/o;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroidx/work/d;->g:J

    iput-wide v0, p0, Landroidx/work/d;->h:J

    new-instance v0, Landroidx/work/e;

    invoke-direct {v0}, Landroidx/work/e;-><init>()V

    iput-object v0, p0, Landroidx/work/d;->i:Landroidx/work/e;

    iget-boolean v0, p1, Landroidx/work/d;->c:Z

    iput-boolean v0, p0, Landroidx/work/d;->c:Z

    iget-boolean v0, p1, Landroidx/work/d;->d:Z

    iput-boolean v0, p0, Landroidx/work/d;->d:Z

    iget-object v0, p1, Landroidx/work/d;->b:Landroidx/work/o;

    iput-object v0, p0, Landroidx/work/d;->b:Landroidx/work/o;

    iget-boolean v0, p1, Landroidx/work/d;->e:Z

    iput-boolean v0, p0, Landroidx/work/d;->e:Z

    iget-boolean v0, p1, Landroidx/work/d;->f:Z

    iput-boolean v0, p0, Landroidx/work/d;->f:Z

    iget-object p1, p1, Landroidx/work/d;->i:Landroidx/work/e;

    iput-object p1, p0, Landroidx/work/d;->i:Landroidx/work/e;

    return-void
.end method


# virtual methods
.method public a()Landroidx/work/e;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation build Landroidx/annotation/RequiresApi;
        value = 0x18
    .end annotation

    .annotation build Landroidx/annotation/RestrictTo;
        value = {
            .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
        }
    .end annotation

    iget-object v0, p0, Landroidx/work/d;->i:Landroidx/work/e;

    return-object v0
.end method

.method public a(J)V
    .locals 0
    .annotation build Landroidx/annotation/RestrictTo;
        value = {
            .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
        }
    .end annotation

    iput-wide p1, p0, Landroidx/work/d;->g:J

    return-void
.end method

.method public a(Landroidx/work/e;)V
    .locals 0
    .param p1    # Landroidx/work/e;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/RequiresApi;
        value = 0x18
    .end annotation

    .annotation build Landroidx/annotation/RestrictTo;
        value = {
            .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
        }
    .end annotation

    iput-object p1, p0, Landroidx/work/d;->i:Landroidx/work/e;

    return-void
.end method

.method public a(Landroidx/work/o;)V
    .locals 0
    .param p1    # Landroidx/work/o;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/RestrictTo;
        value = {
            .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
        }
    .end annotation

    iput-object p1, p0, Landroidx/work/d;->b:Landroidx/work/o;

    return-void
.end method

.method public a(Z)V
    .locals 0
    .annotation build Landroidx/annotation/RestrictTo;
        value = {
            .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
        }
    .end annotation

    iput-boolean p1, p0, Landroidx/work/d;->e:Z

    return-void
.end method

.method public b()Landroidx/work/o;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Landroidx/work/d;->b:Landroidx/work/o;

    return-object v0
.end method

.method public b(J)V
    .locals 0
    .annotation build Landroidx/annotation/RestrictTo;
        value = {
            .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
        }
    .end annotation

    iput-wide p1, p0, Landroidx/work/d;->h:J

    return-void
.end method

.method public b(Z)V
    .locals 0
    .annotation build Landroidx/annotation/RestrictTo;
        value = {
            .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
        }
    .end annotation

    iput-boolean p1, p0, Landroidx/work/d;->c:Z

    return-void
.end method

.method public c()J
    .locals 2
    .annotation build Landroidx/annotation/RestrictTo;
        value = {
            .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
        }
    .end annotation

    iget-wide v0, p0, Landroidx/work/d;->g:J

    return-wide v0
.end method

.method public c(Z)V
    .locals 0
    .annotation build Landroidx/annotation/RequiresApi;
        value = 0x17
    .end annotation

    .annotation build Landroidx/annotation/RestrictTo;
        value = {
            .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
        }
    .end annotation

    iput-boolean p1, p0, Landroidx/work/d;->d:Z

    return-void
.end method

.method public d()J
    .locals 2
    .annotation build Landroidx/annotation/RestrictTo;
        value = {
            .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
        }
    .end annotation

    iget-wide v0, p0, Landroidx/work/d;->h:J

    return-wide v0
.end method

.method public d(Z)V
    .locals 0
    .annotation build Landroidx/annotation/RestrictTo;
        value = {
            .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
        }
    .end annotation

    iput-boolean p1, p0, Landroidx/work/d;->f:Z

    return-void
.end method

.method public e()Z
    .locals 1
    .annotation build Landroidx/annotation/RequiresApi;
        value = 0x18
    .end annotation

    .annotation build Landroidx/annotation/RestrictTo;
        value = {
            .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
        }
    .end annotation

    iget-object v0, p0, Landroidx/work/d;->i:Landroidx/work/e;

    invoke-virtual {v0}, Landroidx/work/e;->b()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 v0, 0x0

    if-eqz p1, :cond_9

    const-class v1, Landroidx/work/d;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_1

    goto :goto_0

    :cond_1
    check-cast p1, Landroidx/work/d;

    iget-boolean v1, p0, Landroidx/work/d;->c:Z

    iget-boolean v2, p1, Landroidx/work/d;->c:Z

    if-eq v1, v2, :cond_2

    return v0

    :cond_2
    iget-boolean v1, p0, Landroidx/work/d;->d:Z

    iget-boolean v2, p1, Landroidx/work/d;->d:Z

    if-eq v1, v2, :cond_3

    return v0

    :cond_3
    iget-boolean v1, p0, Landroidx/work/d;->e:Z

    iget-boolean v2, p1, Landroidx/work/d;->e:Z

    if-eq v1, v2, :cond_4

    return v0

    :cond_4
    iget-boolean v1, p0, Landroidx/work/d;->f:Z

    iget-boolean v2, p1, Landroidx/work/d;->f:Z

    if-eq v1, v2, :cond_5

    return v0

    :cond_5
    iget-wide v1, p0, Landroidx/work/d;->g:J

    iget-wide v3, p1, Landroidx/work/d;->g:J

    cmp-long v1, v1, v3

    if-eqz v1, :cond_6

    return v0

    :cond_6
    iget-wide v1, p0, Landroidx/work/d;->h:J

    iget-wide v3, p1, Landroidx/work/d;->h:J

    cmp-long v1, v1, v3

    if-eqz v1, :cond_7

    return v0

    :cond_7
    iget-object v1, p0, Landroidx/work/d;->b:Landroidx/work/o;

    iget-object v2, p1, Landroidx/work/d;->b:Landroidx/work/o;

    if-eq v1, v2, :cond_8

    return v0

    :cond_8
    iget-object v0, p0, Landroidx/work/d;->i:Landroidx/work/e;

    iget-object p1, p1, Landroidx/work/d;->i:Landroidx/work/e;

    invoke-virtual {v0, p1}, Landroidx/work/e;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_9
    :goto_0
    return v0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Landroidx/work/d;->e:Z

    return v0
.end method

.method public g()Z
    .locals 1

    iget-boolean v0, p0, Landroidx/work/d;->c:Z

    return v0
.end method

.method public h()Z
    .locals 1
    .annotation build Landroidx/annotation/RequiresApi;
        value = 0x17
    .end annotation

    iget-boolean v0, p0, Landroidx/work/d;->d:Z

    return v0
.end method

.method public hashCode()I
    .locals 6

    iget-object v0, p0, Landroidx/work/d;->b:Landroidx/work/o;

    invoke-virtual {v0}, Ljava/lang/Enum;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Landroidx/work/d;->c:Z

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Landroidx/work/d;->d:Z

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Landroidx/work/d;->e:Z

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Landroidx/work/d;->f:Z

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Landroidx/work/d;->g:J

    const/16 v3, 0x20

    ushr-long v4, v1, v3

    xor-long/2addr v1, v4

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Landroidx/work/d;->h:J

    ushr-long v3, v1, v3

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Landroidx/work/d;->i:Landroidx/work/e;

    invoke-virtual {v1}, Landroidx/work/e;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, Landroidx/work/d;->f:Z

    return v0
.end method
