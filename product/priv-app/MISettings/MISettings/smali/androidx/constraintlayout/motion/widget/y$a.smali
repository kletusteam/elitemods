.class public Landroidx/constraintlayout/motion/widget/y$a;
.super Ljava/lang/Object;
.source "MotionScene.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/constraintlayout/motion/widget/y;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/constraintlayout/motion/widget/y$a$a;
    }
.end annotation


# instance fields
.field private a:I

.field private b:Z

.field private c:I

.field private d:I

.field private e:I

.field private f:Ljava/lang/String;

.field private g:I

.field private h:I

.field private i:F

.field private final j:Landroidx/constraintlayout/motion/widget/y;

.field private k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroidx/constraintlayout/motion/widget/i;",
            ">;"
        }
    .end annotation
.end field

.field private l:Landroidx/constraintlayout/motion/widget/C;

.field private m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroidx/constraintlayout/motion/widget/y$a$a;",
            ">;"
        }
    .end annotation
.end field

.field private n:I

.field private o:Z

.field private p:I

.field private q:I

.field private r:I


# direct methods
.method public constructor <init>(ILandroidx/constraintlayout/motion/widget/y;II)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->a:I

    const/4 v1, 0x0

    iput-boolean v1, p0, Landroidx/constraintlayout/motion/widget/y$a;->b:Z

    iput v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->c:I

    iput v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->d:I

    iput v1, p0, Landroidx/constraintlayout/motion/widget/y$a;->e:I

    const/4 v2, 0x0

    iput-object v2, p0, Landroidx/constraintlayout/motion/widget/y$a;->f:Ljava/lang/String;

    iput v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->g:I

    const/16 v3, 0x190

    iput v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->h:I

    const/4 v3, 0x0

    iput v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->i:F

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->k:Ljava/util/ArrayList;

    iput-object v2, p0, Landroidx/constraintlayout/motion/widget/y$a;->l:Landroidx/constraintlayout/motion/widget/C;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Landroidx/constraintlayout/motion/widget/y$a;->m:Ljava/util/ArrayList;

    iput v1, p0, Landroidx/constraintlayout/motion/widget/y$a;->n:I

    iput-boolean v1, p0, Landroidx/constraintlayout/motion/widget/y$a;->o:Z

    iput v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->p:I

    iput v1, p0, Landroidx/constraintlayout/motion/widget/y$a;->q:I

    iput v1, p0, Landroidx/constraintlayout/motion/widget/y$a;->r:I

    iput p1, p0, Landroidx/constraintlayout/motion/widget/y$a;->a:I

    iput-object p2, p0, Landroidx/constraintlayout/motion/widget/y$a;->j:Landroidx/constraintlayout/motion/widget/y;

    iput p3, p0, Landroidx/constraintlayout/motion/widget/y$a;->d:I

    iput p4, p0, Landroidx/constraintlayout/motion/widget/y$a;->c:I

    invoke-static {p2}, Landroidx/constraintlayout/motion/widget/y;->d(Landroidx/constraintlayout/motion/widget/y;)I

    move-result p1

    iput p1, p0, Landroidx/constraintlayout/motion/widget/y$a;->h:I

    invoke-static {p2}, Landroidx/constraintlayout/motion/widget/y;->a(Landroidx/constraintlayout/motion/widget/y;)I

    move-result p1

    iput p1, p0, Landroidx/constraintlayout/motion/widget/y$a;->q:I

    return-void
.end method

.method constructor <init>(Landroidx/constraintlayout/motion/widget/y;Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->a:I

    const/4 v1, 0x0

    iput-boolean v1, p0, Landroidx/constraintlayout/motion/widget/y$a;->b:Z

    iput v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->c:I

    iput v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->d:I

    iput v1, p0, Landroidx/constraintlayout/motion/widget/y$a;->e:I

    const/4 v2, 0x0

    iput-object v2, p0, Landroidx/constraintlayout/motion/widget/y$a;->f:Ljava/lang/String;

    iput v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->g:I

    const/16 v3, 0x190

    iput v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->h:I

    const/4 v3, 0x0

    iput v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->i:F

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->k:Ljava/util/ArrayList;

    iput-object v2, p0, Landroidx/constraintlayout/motion/widget/y$a;->l:Landroidx/constraintlayout/motion/widget/C;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Landroidx/constraintlayout/motion/widget/y$a;->m:Ljava/util/ArrayList;

    iput v1, p0, Landroidx/constraintlayout/motion/widget/y$a;->n:I

    iput-boolean v1, p0, Landroidx/constraintlayout/motion/widget/y$a;->o:Z

    iput v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->p:I

    iput v1, p0, Landroidx/constraintlayout/motion/widget/y$a;->q:I

    iput v1, p0, Landroidx/constraintlayout/motion/widget/y$a;->r:I

    invoke-static {p1}, Landroidx/constraintlayout/motion/widget/y;->d(Landroidx/constraintlayout/motion/widget/y;)I

    move-result v0

    iput v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->h:I

    invoke-static {p1}, Landroidx/constraintlayout/motion/widget/y;->a(Landroidx/constraintlayout/motion/widget/y;)I

    move-result v0

    iput v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->q:I

    iput-object p1, p0, Landroidx/constraintlayout/motion/widget/y$a;->j:Landroidx/constraintlayout/motion/widget/y;

    invoke-static {p3}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object p3

    invoke-direct {p0, p1, p2, p3}, Landroidx/constraintlayout/motion/widget/y$a;->a(Landroidx/constraintlayout/motion/widget/y;Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method constructor <init>(Landroidx/constraintlayout/motion/widget/y;Landroidx/constraintlayout/motion/widget/y$a;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->a:I

    const/4 v1, 0x0

    iput-boolean v1, p0, Landroidx/constraintlayout/motion/widget/y$a;->b:Z

    iput v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->c:I

    iput v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->d:I

    iput v1, p0, Landroidx/constraintlayout/motion/widget/y$a;->e:I

    const/4 v2, 0x0

    iput-object v2, p0, Landroidx/constraintlayout/motion/widget/y$a;->f:Ljava/lang/String;

    iput v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->g:I

    const/16 v3, 0x190

    iput v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->h:I

    const/4 v3, 0x0

    iput v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->i:F

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->k:Ljava/util/ArrayList;

    iput-object v2, p0, Landroidx/constraintlayout/motion/widget/y$a;->l:Landroidx/constraintlayout/motion/widget/C;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Landroidx/constraintlayout/motion/widget/y$a;->m:Ljava/util/ArrayList;

    iput v1, p0, Landroidx/constraintlayout/motion/widget/y$a;->n:I

    iput-boolean v1, p0, Landroidx/constraintlayout/motion/widget/y$a;->o:Z

    iput v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->p:I

    iput v1, p0, Landroidx/constraintlayout/motion/widget/y$a;->q:I

    iput v1, p0, Landroidx/constraintlayout/motion/widget/y$a;->r:I

    iput-object p1, p0, Landroidx/constraintlayout/motion/widget/y$a;->j:Landroidx/constraintlayout/motion/widget/y;

    invoke-static {p1}, Landroidx/constraintlayout/motion/widget/y;->d(Landroidx/constraintlayout/motion/widget/y;)I

    move-result p1

    iput p1, p0, Landroidx/constraintlayout/motion/widget/y$a;->h:I

    if-eqz p2, :cond_0

    iget p1, p2, Landroidx/constraintlayout/motion/widget/y$a;->p:I

    iput p1, p0, Landroidx/constraintlayout/motion/widget/y$a;->p:I

    iget p1, p2, Landroidx/constraintlayout/motion/widget/y$a;->e:I

    iput p1, p0, Landroidx/constraintlayout/motion/widget/y$a;->e:I

    iget-object p1, p2, Landroidx/constraintlayout/motion/widget/y$a;->f:Ljava/lang/String;

    iput-object p1, p0, Landroidx/constraintlayout/motion/widget/y$a;->f:Ljava/lang/String;

    iget p1, p2, Landroidx/constraintlayout/motion/widget/y$a;->g:I

    iput p1, p0, Landroidx/constraintlayout/motion/widget/y$a;->g:I

    iget p1, p2, Landroidx/constraintlayout/motion/widget/y$a;->h:I

    iput p1, p0, Landroidx/constraintlayout/motion/widget/y$a;->h:I

    iget-object p1, p2, Landroidx/constraintlayout/motion/widget/y$a;->k:Ljava/util/ArrayList;

    iput-object p1, p0, Landroidx/constraintlayout/motion/widget/y$a;->k:Ljava/util/ArrayList;

    iget p1, p2, Landroidx/constraintlayout/motion/widget/y$a;->i:F

    iput p1, p0, Landroidx/constraintlayout/motion/widget/y$a;->i:F

    iget p1, p2, Landroidx/constraintlayout/motion/widget/y$a;->q:I

    iput p1, p0, Landroidx/constraintlayout/motion/widget/y$a;->q:I

    :cond_0
    return-void
.end method

.method static synthetic a(Landroidx/constraintlayout/motion/widget/y$a;)I
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/y$a;->c:I

    return p0
.end method

.method static synthetic a(Landroidx/constraintlayout/motion/widget/y$a;I)I
    .locals 0

    iput p1, p0, Landroidx/constraintlayout/motion/widget/y$a;->c:I

    return p1
.end method

.method static synthetic a(Landroidx/constraintlayout/motion/widget/y$a;Landroidx/constraintlayout/motion/widget/C;)Landroidx/constraintlayout/motion/widget/C;
    .locals 0

    iput-object p1, p0, Landroidx/constraintlayout/motion/widget/y$a;->l:Landroidx/constraintlayout/motion/widget/C;

    return-object p1
.end method

.method private a(Landroidx/constraintlayout/motion/widget/y;Landroid/content/Context;Landroid/content/res/TypedArray;)V
    .locals 9

    invoke-virtual {p3}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    const/4 v3, 0x1

    const/4 v4, -0x1

    if-ge v2, v0, :cond_10

    invoke-virtual {p3, v2}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v5

    sget v6, Landroidx/constraintlayout/widget/h;->Transition_constraintSetEnd:I

    const-string v7, "xml"

    const-string v8, "layout"

    if-ne v5, v6, :cond_1

    invoke-virtual {p3, v5, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->c:I

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p0, Landroidx/constraintlayout/motion/widget/y$a;->c:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getResourceTypeName(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v3, Landroidx/constraintlayout/widget/e;

    invoke-direct {v3}, Landroidx/constraintlayout/widget/e;-><init>()V

    iget v4, p0, Landroidx/constraintlayout/motion/widget/y$a;->c:I

    invoke-virtual {v3, p2, v4}, Landroidx/constraintlayout/widget/e;->b(Landroid/content/Context;I)V

    invoke-static {p1}, Landroidx/constraintlayout/motion/widget/y;->b(Landroidx/constraintlayout/motion/widget/y;)Landroid/util/SparseArray;

    move-result-object v4

    iget v5, p0, Landroidx/constraintlayout/motion/widget/y$a;->c:I

    invoke-virtual {v4, v5, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_1

    :cond_0
    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    iget v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->c:I

    invoke-static {p1, p2, v3}, Landroidx/constraintlayout/motion/widget/y;->a(Landroidx/constraintlayout/motion/widget/y;Landroid/content/Context;I)I

    move-result v3

    iput v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->c:I

    goto/16 :goto_1

    :cond_1
    sget v6, Landroidx/constraintlayout/widget/h;->Transition_constraintSetStart:I

    if-ne v5, v6, :cond_3

    iget v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->d:I

    invoke-virtual {p3, v5, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->d:I

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p0, Landroidx/constraintlayout/motion/widget/y$a;->d:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getResourceTypeName(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    new-instance v3, Landroidx/constraintlayout/widget/e;

    invoke-direct {v3}, Landroidx/constraintlayout/widget/e;-><init>()V

    iget v4, p0, Landroidx/constraintlayout/motion/widget/y$a;->d:I

    invoke-virtual {v3, p2, v4}, Landroidx/constraintlayout/widget/e;->b(Landroid/content/Context;I)V

    invoke-static {p1}, Landroidx/constraintlayout/motion/widget/y;->b(Landroidx/constraintlayout/motion/widget/y;)Landroid/util/SparseArray;

    move-result-object v4

    iget v5, p0, Landroidx/constraintlayout/motion/widget/y$a;->d:I

    invoke-virtual {v4, v5, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto/16 :goto_1

    :cond_2
    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    iget v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->d:I

    invoke-static {p1, p2, v3}, Landroidx/constraintlayout/motion/widget/y;->a(Landroidx/constraintlayout/motion/widget/y;Landroid/content/Context;I)I

    move-result v3

    iput v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->d:I

    goto/16 :goto_1

    :cond_3
    sget v6, Landroidx/constraintlayout/widget/h;->Transition_motionInterpolator:I

    if-ne v5, v6, :cond_7

    invoke-virtual {p3, v5}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    move-result-object v6

    iget v6, v6, Landroid/util/TypedValue;->type:I

    const/4 v7, -0x2

    if-ne v6, v3, :cond_4

    invoke-virtual {p3, v5, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->g:I

    iget v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->g:I

    if-eq v3, v4, :cond_f

    iput v7, p0, Landroidx/constraintlayout/motion/widget/y$a;->e:I

    goto/16 :goto_1

    :cond_4
    const/4 v3, 0x3

    if-ne v6, v3, :cond_6

    invoke-virtual {p3, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->f:Ljava/lang/String;

    iget-object v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->f:Ljava/lang/String;

    if-eqz v3, :cond_f

    const-string v6, "/"

    invoke-virtual {v3, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_5

    invoke-virtual {p3, v5, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->g:I

    iput v7, p0, Landroidx/constraintlayout/motion/widget/y$a;->e:I

    goto/16 :goto_1

    :cond_5
    iput v4, p0, Landroidx/constraintlayout/motion/widget/y$a;->e:I

    goto/16 :goto_1

    :cond_6
    iget v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->e:I

    invoke-virtual {p3, v5, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    iput v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->e:I

    goto/16 :goto_1

    :cond_7
    sget v3, Landroidx/constraintlayout/widget/h;->Transition_duration:I

    if-ne v5, v3, :cond_8

    iget v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->h:I

    invoke-virtual {p3, v5, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    iput v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->h:I

    iget v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->h:I

    const/16 v4, 0x8

    if-ge v3, v4, :cond_f

    iput v4, p0, Landroidx/constraintlayout/motion/widget/y$a;->h:I

    goto :goto_1

    :cond_8
    sget v3, Landroidx/constraintlayout/widget/h;->Transition_staggered:I

    if-ne v5, v3, :cond_9

    iget v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->i:F

    invoke-virtual {p3, v5, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    iput v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->i:F

    goto :goto_1

    :cond_9
    sget v3, Landroidx/constraintlayout/widget/h;->Transition_autoTransition:I

    if-ne v5, v3, :cond_a

    iget v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->n:I

    invoke-virtual {p3, v5, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    iput v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->n:I

    goto :goto_1

    :cond_a
    sget v3, Landroidx/constraintlayout/widget/h;->Transition_android_id:I

    if-ne v5, v3, :cond_b

    iget v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->a:I

    invoke-virtual {p3, v5, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->a:I

    goto :goto_1

    :cond_b
    sget v3, Landroidx/constraintlayout/widget/h;->Transition_transitionDisable:I

    if-ne v5, v3, :cond_c

    iget-boolean v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->o:Z

    invoke-virtual {p3, v5, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->o:Z

    goto :goto_1

    :cond_c
    sget v3, Landroidx/constraintlayout/widget/h;->Transition_pathMotionArc:I

    if-ne v5, v3, :cond_d

    invoke-virtual {p3, v5, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    iput v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->p:I

    goto :goto_1

    :cond_d
    sget v3, Landroidx/constraintlayout/widget/h;->Transition_layoutDuringTransition:I

    if-ne v5, v3, :cond_e

    invoke-virtual {p3, v5, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    iput v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->q:I

    goto :goto_1

    :cond_e
    sget v3, Landroidx/constraintlayout/widget/h;->Transition_transitionFlags:I

    if-ne v5, v3, :cond_f

    invoke-virtual {p3, v5, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    iput v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->r:I

    :cond_f
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_10
    iget p1, p0, Landroidx/constraintlayout/motion/widget/y$a;->d:I

    if-ne p1, v4, :cond_11

    iput-boolean v3, p0, Landroidx/constraintlayout/motion/widget/y$a;->b:Z

    :cond_11
    return-void
.end method

.method private a(Landroidx/constraintlayout/motion/widget/y;Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget-object v0, Landroidx/constraintlayout/widget/h;->Transition:[I

    invoke-virtual {p2, p3, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p3

    invoke-direct {p0, p1, p2, p3}, Landroidx/constraintlayout/motion/widget/y$a;->a(Landroidx/constraintlayout/motion/widget/y;Landroid/content/Context;Landroid/content/res/TypedArray;)V

    invoke-virtual {p3}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method static synthetic b(Landroidx/constraintlayout/motion/widget/y$a;)I
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/y$a;->d:I

    return p0
.end method

.method static synthetic b(Landroidx/constraintlayout/motion/widget/y$a;I)I
    .locals 0

    iput p1, p0, Landroidx/constraintlayout/motion/widget/y$a;->d:I

    return p1
.end method

.method static synthetic c(Landroidx/constraintlayout/motion/widget/y$a;)Z
    .locals 0

    iget-boolean p0, p0, Landroidx/constraintlayout/motion/widget/y$a;->b:Z

    return p0
.end method

.method static synthetic d(Landroidx/constraintlayout/motion/widget/y$a;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Landroidx/constraintlayout/motion/widget/y$a;->k:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic e(Landroidx/constraintlayout/motion/widget/y$a;)I
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/y$a;->e:I

    return p0
.end method

.method static synthetic f(Landroidx/constraintlayout/motion/widget/y$a;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Landroidx/constraintlayout/motion/widget/y$a;->f:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic g(Landroidx/constraintlayout/motion/widget/y$a;)I
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/y$a;->g:I

    return p0
.end method

.method static synthetic h(Landroidx/constraintlayout/motion/widget/y$a;)I
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/y$a;->h:I

    return p0
.end method

.method static synthetic i(Landroidx/constraintlayout/motion/widget/y$a;)I
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/y$a;->p:I

    return p0
.end method

.method static synthetic j(Landroidx/constraintlayout/motion/widget/y$a;)Landroidx/constraintlayout/motion/widget/C;
    .locals 0

    iget-object p0, p0, Landroidx/constraintlayout/motion/widget/y$a;->l:Landroidx/constraintlayout/motion/widget/C;

    return-object p0
.end method

.method static synthetic k(Landroidx/constraintlayout/motion/widget/y$a;)F
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/y$a;->i:F

    return p0
.end method

.method static synthetic l(Landroidx/constraintlayout/motion/widget/y$a;)I
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/y$a;->a:I

    return p0
.end method

.method static synthetic m(Landroidx/constraintlayout/motion/widget/y$a;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Landroidx/constraintlayout/motion/widget/y$a;->m:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic n(Landroidx/constraintlayout/motion/widget/y$a;)Z
    .locals 0

    iget-boolean p0, p0, Landroidx/constraintlayout/motion/widget/y$a;->o:Z

    return p0
.end method

.method static synthetic o(Landroidx/constraintlayout/motion/widget/y$a;)I
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/y$a;->n:I

    return p0
.end method

.method static synthetic p(Landroidx/constraintlayout/motion/widget/y$a;)Landroidx/constraintlayout/motion/widget/y;
    .locals 0

    iget-object p0, p0, Landroidx/constraintlayout/motion/widget/y$a;->j:Landroidx/constraintlayout/motion/widget/y;

    return-object p0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->n:I

    return v0
.end method

.method public a(ILjava/lang/String;I)V
    .locals 0

    iput p1, p0, Landroidx/constraintlayout/motion/widget/y$a;->e:I

    iput-object p2, p0, Landroidx/constraintlayout/motion/widget/y$a;->f:Ljava/lang/String;

    iput p3, p0, Landroidx/constraintlayout/motion/widget/y$a;->g:I

    return-void
.end method

.method public a(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 2

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->m:Ljava/util/ArrayList;

    new-instance v1, Landroidx/constraintlayout/motion/widget/y$a$a;

    invoke-direct {v1, p1, p0, p2}, Landroidx/constraintlayout/motion/widget/y$a$a;-><init>(Landroid/content/Context;Landroidx/constraintlayout/motion/widget/y$a;Lorg/xmlpull/v1/XmlPullParser;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Landroidx/constraintlayout/motion/widget/i;)V
    .locals 1

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Z)V
    .locals 0

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Landroidx/constraintlayout/motion/widget/y$a;->o:Z

    return-void
.end method

.method public a(I)Z
    .locals 1

    iget v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->r:I

    and-int/2addr p1, v0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public b()I
    .locals 1

    iget v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->c:I

    return v0
.end method

.method public b(I)V
    .locals 1

    const/16 v0, 0x8

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    iput p1, p0, Landroidx/constraintlayout/motion/widget/y$a;->h:I

    return-void
.end method

.method public c()I
    .locals 1

    iget v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->q:I

    return v0
.end method

.method public c(I)V
    .locals 1

    invoke-virtual {p0}, Landroidx/constraintlayout/motion/widget/y$a;->e()Landroidx/constraintlayout/motion/widget/C;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroidx/constraintlayout/motion/widget/C;->a(I)V

    :cond_0
    return-void
.end method

.method public d()I
    .locals 1

    iget v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->d:I

    return v0
.end method

.method public d(I)V
    .locals 0

    iput p1, p0, Landroidx/constraintlayout/motion/widget/y$a;->p:I

    return-void
.end method

.method public e()Landroidx/constraintlayout/motion/widget/C;
    .locals 1

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->l:Landroidx/constraintlayout/motion/widget/C;

    return-object v0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Landroidx/constraintlayout/motion/widget/y$a;->o:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
