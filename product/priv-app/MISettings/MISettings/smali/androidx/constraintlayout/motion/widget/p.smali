.class public Landroidx/constraintlayout/motion/widget/p;
.super Ljava/lang/Object;
.source "MotionController.java"


# instance fields
.field private A:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroidx/constraintlayout/motion/widget/f;",
            ">;"
        }
    .end annotation
.end field

.field private B:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "La/e/b/a/g;",
            ">;"
        }
    .end annotation
.end field

.field private C:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "La/e/b/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private D:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "La/e/b/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private E:[Landroidx/constraintlayout/motion/widget/m;

.field private F:I

.field private G:I

.field private H:Landroid/view/View;

.field private I:I

.field private J:F

.field private K:Landroid/view/animation/Interpolator;

.field private L:Z

.field a:Landroid/graphics/Rect;

.field b:Landroid/view/View;

.field c:I

.field d:Z

.field e:Ljava/lang/String;

.field private f:I

.field private g:Landroidx/constraintlayout/motion/widget/w;

.field private h:Landroidx/constraintlayout/motion/widget/w;

.field private i:Landroidx/constraintlayout/motion/widget/n;

.field private j:Landroidx/constraintlayout/motion/widget/n;

.field private k:[La/e/a/a/a/b;

.field private l:La/e/a/a/a/b;

.field m:F

.field n:F

.field o:F

.field p:F

.field q:F

.field private r:[I

.field private s:[D

.field private t:[D

.field private u:[Ljava/lang/String;

.field private v:[I

.field private w:I

.field private x:[F

.field private y:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroidx/constraintlayout/motion/widget/w;",
            ">;"
        }
    .end annotation
.end field

.field private z:[F


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->a:Landroid/graphics/Rect;

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroidx/constraintlayout/motion/widget/p;->d:Z

    const/4 v1, -0x1

    iput v1, p0, Landroidx/constraintlayout/motion/widget/p;->f:I

    new-instance v1, Landroidx/constraintlayout/motion/widget/w;

    invoke-direct {v1}, Landroidx/constraintlayout/motion/widget/w;-><init>()V

    iput-object v1, p0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    new-instance v1, Landroidx/constraintlayout/motion/widget/w;

    invoke-direct {v1}, Landroidx/constraintlayout/motion/widget/w;-><init>()V

    iput-object v1, p0, Landroidx/constraintlayout/motion/widget/p;->h:Landroidx/constraintlayout/motion/widget/w;

    new-instance v1, Landroidx/constraintlayout/motion/widget/n;

    invoke-direct {v1}, Landroidx/constraintlayout/motion/widget/n;-><init>()V

    iput-object v1, p0, Landroidx/constraintlayout/motion/widget/p;->i:Landroidx/constraintlayout/motion/widget/n;

    new-instance v1, Landroidx/constraintlayout/motion/widget/n;

    invoke-direct {v1}, Landroidx/constraintlayout/motion/widget/n;-><init>()V

    iput-object v1, p0, Landroidx/constraintlayout/motion/widget/p;->j:Landroidx/constraintlayout/motion/widget/n;

    const/high16 v1, 0x7fc00000    # Float.NaN

    iput v1, p0, Landroidx/constraintlayout/motion/widget/p;->m:F

    const/4 v2, 0x0

    iput v2, p0, Landroidx/constraintlayout/motion/widget/p;->n:F

    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Landroidx/constraintlayout/motion/widget/p;->o:F

    const/4 v2, 0x4

    iput v2, p0, Landroidx/constraintlayout/motion/widget/p;->w:I

    iget v2, p0, Landroidx/constraintlayout/motion/widget/p;->w:I

    new-array v2, v2, [F

    iput-object v2, p0, Landroidx/constraintlayout/motion/widget/p;->x:[F

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Landroidx/constraintlayout/motion/widget/p;->y:Ljava/util/ArrayList;

    const/4 v2, 0x1

    new-array v2, v2, [F

    iput-object v2, p0, Landroidx/constraintlayout/motion/widget/p;->z:[F

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Landroidx/constraintlayout/motion/widget/p;->A:Ljava/util/ArrayList;

    sget v2, Landroidx/constraintlayout/motion/widget/f;->a:I

    iput v2, p0, Landroidx/constraintlayout/motion/widget/p;->F:I

    iput v2, p0, Landroidx/constraintlayout/motion/widget/p;->G:I

    const/4 v3, 0x0

    iput-object v3, p0, Landroidx/constraintlayout/motion/widget/p;->H:Landroid/view/View;

    iput v2, p0, Landroidx/constraintlayout/motion/widget/p;->I:I

    iput v1, p0, Landroidx/constraintlayout/motion/widget/p;->J:F

    iput-object v3, p0, Landroidx/constraintlayout/motion/widget/p;->K:Landroid/view/animation/Interpolator;

    iput-boolean v0, p0, Landroidx/constraintlayout/motion/widget/p;->L:Z

    invoke-virtual {p0, p1}, Landroidx/constraintlayout/motion/widget/p;->c(Landroid/view/View;)V

    return-void
.end method

.method private a(F[F)F
    .locals 10

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    if-eqz p2, :cond_0

    aput v2, p2, v1

    goto :goto_0

    :cond_0
    iget v3, p0, Landroidx/constraintlayout/motion/widget/p;->o:F

    float-to-double v3, v3

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    cmpl-double v3, v3, v5

    if-eqz v3, :cond_2

    iget v3, p0, Landroidx/constraintlayout/motion/widget/p;->n:F

    cmpg-float v3, p1, v3

    if-gez v3, :cond_1

    move p1, v0

    :cond_1
    iget v3, p0, Landroidx/constraintlayout/motion/widget/p;->n:F

    cmpl-float v4, p1, v3

    if-lez v4, :cond_2

    float-to-double v7, p1

    cmpg-double v4, v7, v5

    if-gez v4, :cond_2

    sub-float/2addr p1, v3

    iget v3, p0, Landroidx/constraintlayout/motion/widget/p;->o:F

    mul-float/2addr p1, v3

    invoke-static {p1, v2}, Ljava/lang/Math;->min(FF)F

    move-result p1

    :cond_2
    :goto_0
    iget-object v3, p0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget-object v3, v3, Landroidx/constraintlayout/motion/widget/w;->b:La/e/a/a/a/c;

    const/high16 v4, 0x7fc00000    # Float.NaN

    iget-object v5, p0, Landroidx/constraintlayout/motion/widget/p;->y:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroidx/constraintlayout/motion/widget/w;

    iget-object v7, v6, Landroidx/constraintlayout/motion/widget/w;->b:La/e/a/a/a/c;

    if-eqz v7, :cond_3

    iget v8, v6, Landroidx/constraintlayout/motion/widget/w;->d:F

    cmpg-float v9, v8, p1

    if-gez v9, :cond_4

    move-object v3, v7

    move v0, v8

    goto :goto_1

    :cond_4
    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v7

    if-eqz v7, :cond_3

    iget v4, v6, Landroidx/constraintlayout/motion/widget/w;->d:F

    goto :goto_1

    :cond_5
    if-eqz v3, :cond_7

    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-eqz v5, :cond_6

    goto :goto_2

    :cond_6
    move v2, v4

    :goto_2
    sub-float/2addr p1, v0

    sub-float/2addr v2, v0

    div-float/2addr p1, v2

    float-to-double v4, p1

    invoke-virtual {v3, v4, v5}, La/e/a/a/a/c;->a(D)D

    move-result-wide v6

    double-to-float p1, v6

    mul-float/2addr p1, v2

    add-float/2addr p1, v0

    if-eqz p2, :cond_7

    invoke-virtual {v3, v4, v5}, La/e/a/a/a/c;->b(D)D

    move-result-wide v2

    double-to-float v0, v2

    aput v0, p2, v1

    :cond_7
    return p1
.end method

.method private static a(Landroid/content/Context;ILjava/lang/String;I)Landroid/view/animation/Interpolator;
    .locals 1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    return-object v0

    :pswitch_0
    new-instance p0, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {p0}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    return-object p0

    :pswitch_1
    new-instance p0, Landroid/view/animation/BounceInterpolator;

    invoke-direct {p0}, Landroid/view/animation/BounceInterpolator;-><init>()V

    return-object p0

    :pswitch_2
    return-object v0

    :pswitch_3
    new-instance p0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {p0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    return-object p0

    :pswitch_4
    new-instance p0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {p0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    return-object p0

    :pswitch_5
    new-instance p0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {p0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    return-object p0

    :pswitch_6
    invoke-static {p2}, La/e/a/a/a/c;->a(Ljava/lang/String;)La/e/a/a/a/c;

    move-result-object p0

    new-instance p1, Landroidx/constraintlayout/motion/widget/o;

    invoke-direct {p1, p0}, Landroidx/constraintlayout/motion/widget/o;-><init>(La/e/a/a/a/c;)V

    return-object p1

    :pswitch_7
    invoke-static {p0, p3}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object p0

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private a(Landroidx/constraintlayout/motion/widget/w;)V
    .locals 3

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->y:Ljava/util/ArrayList;

    invoke-static {v0, p1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " KeyPath position \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p1, Landroidx/constraintlayout/motion/widget/w;->e:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v2, "\" outside of range"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MotionController"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Landroidx/constraintlayout/motion/widget/p;->y:Ljava/util/ArrayList;

    neg-int v0, v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    return-void
.end method

.method private b(Landroidx/constraintlayout/motion/widget/w;)V
    .locals 4

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v0

    float-to-int v0, v0

    int-to-float v0, v0

    iget-object v1, p0, Landroidx/constraintlayout/motion/widget/p;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getY()F

    move-result v1

    float-to-int v1, v1

    int-to-float v1, v1

    iget-object v2, p0, Landroidx/constraintlayout/motion/widget/p;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Landroidx/constraintlayout/motion/widget/p;->b:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroidx/constraintlayout/motion/widget/w;->a(FFFF)V

    return-void
.end method

.method private k()F
    .locals 21

    move-object/from16 v0, p0

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/16 v2, 0x63

    int-to-float v2, v2

    const/high16 v9, 0x3f800000    # 1.0f

    div-float v10, v9, v2

    const-wide/16 v2, 0x0

    move-wide v15, v2

    move-wide/from16 v17, v15

    const/4 v13, 0x0

    const/4 v14, 0x0

    :goto_0
    const/16 v2, 0x64

    if-ge v13, v2, :cond_6

    int-to-float v2, v13

    mul-float/2addr v2, v10

    float-to-double v3, v2

    iget-object v5, v0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget-object v5, v5, Landroidx/constraintlayout/motion/widget/w;->b:La/e/a/a/a/c;

    const/high16 v6, 0x7fc00000    # Float.NaN

    iget-object v7, v0, Landroidx/constraintlayout/motion/widget/p;->y:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    const/4 v8, 0x0

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    move-object/from16 v9, v19

    check-cast v9, Landroidx/constraintlayout/motion/widget/w;

    iget-object v11, v9, Landroidx/constraintlayout/motion/widget/w;->b:La/e/a/a/a/c;

    if-eqz v11, :cond_1

    iget v12, v9, Landroidx/constraintlayout/motion/widget/w;->d:F

    cmpg-float v20, v12, v2

    if-gez v20, :cond_0

    move-object v5, v11

    move v8, v12

    goto :goto_2

    :cond_0
    invoke-static {v6}, Ljava/lang/Float;->isNaN(F)Z

    move-result v11

    if-eqz v11, :cond_1

    iget v6, v9, Landroidx/constraintlayout/motion/widget/w;->d:F

    :cond_1
    :goto_2
    const/high16 v9, 0x3f800000    # 1.0f

    goto :goto_1

    :cond_2
    if-eqz v5, :cond_4

    invoke-static {v6}, Ljava/lang/Float;->isNaN(F)Z

    move-result v3

    if-eqz v3, :cond_3

    const/high16 v6, 0x3f800000    # 1.0f

    :cond_3
    sub-float/2addr v2, v8

    sub-float/2addr v6, v8

    div-float/2addr v2, v6

    float-to-double v2, v2

    invoke-virtual {v5, v2, v3}, La/e/a/a/a/c;->a(D)D

    move-result-wide v2

    double-to-float v2, v2

    mul-float/2addr v2, v6

    add-float/2addr v2, v8

    float-to-double v2, v2

    move-wide v3, v2

    :cond_4
    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->k:[La/e/a/a/a/b;

    const/4 v5, 0x0

    aget-object v2, v2, v5

    iget-object v5, v0, Landroidx/constraintlayout/motion/widget/p;->s:[D

    invoke-virtual {v2, v3, v4, v5}, La/e/a/a/a/b;->a(D[D)V

    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget-object v5, v0, Landroidx/constraintlayout/motion/widget/p;->r:[I

    iget-object v6, v0, Landroidx/constraintlayout/motion/widget/p;->s:[D

    const/4 v8, 0x0

    move-object v7, v1

    invoke-virtual/range {v2 .. v8}, Landroidx/constraintlayout/motion/widget/w;->a(D[I[D[FI)V

    const/4 v2, 0x1

    if-lez v13, :cond_5

    float-to-double v3, v14

    aget v5, v1, v2

    float-to-double v5, v5

    sub-double v5, v17, v5

    const/4 v7, 0x0

    aget v8, v1, v7

    float-to-double v8, v8

    sub-double v8, v15, v8

    invoke-static {v5, v6, v8, v9}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v5

    add-double/2addr v3, v5

    double-to-float v3, v3

    move v14, v3

    goto :goto_3

    :cond_5
    const/4 v7, 0x0

    :goto_3
    aget v3, v1, v7

    float-to-double v3, v3

    aget v2, v1, v2

    float-to-double v5, v2

    add-int/lit8 v13, v13, 0x1

    move-wide v15, v3

    move-wide/from16 v17, v5

    const/high16 v9, 0x3f800000    # 1.0f

    goto/16 :goto_0

    :cond_6
    return v14
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget v0, v0, Landroidx/constraintlayout/motion/widget/w;->m:I

    return v0
.end method

.method a([F[I)I
    .locals 10

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    iget-object v1, p0, Landroidx/constraintlayout/motion/widget/p;->k:[La/e/a/a/a/b;

    aget-object v1, v1, v0

    invoke-virtual {v1}, La/e/a/a/a/b;->a()[D

    move-result-object v1

    if-eqz p2, :cond_0

    iget-object v2, p0, Landroidx/constraintlayout/motion/widget/p;->y:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v3, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroidx/constraintlayout/motion/widget/w;

    add-int/lit8 v5, v3, 0x1

    iget v4, v4, Landroidx/constraintlayout/motion/widget/w;->q:I

    aput v4, p2, v3

    move v3, v5

    goto :goto_0

    :cond_0
    move p2, v0

    move v9, p2

    :goto_1
    array-length v2, v1

    if-ge p2, v2, :cond_1

    iget-object v2, p0, Landroidx/constraintlayout/motion/widget/p;->k:[La/e/a/a/a/b;

    aget-object v2, v2, v0

    aget-wide v3, v1, p2

    iget-object v5, p0, Landroidx/constraintlayout/motion/widget/p;->s:[D

    invoke-virtual {v2, v3, v4, v5}, La/e/a/a/a/b;->a(D[D)V

    iget-object v2, p0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    aget-wide v3, v1, p2

    iget-object v5, p0, Landroidx/constraintlayout/motion/widget/p;->r:[I

    iget-object v6, p0, Landroidx/constraintlayout/motion/widget/p;->s:[D

    move-object v7, p1

    move v8, v9

    invoke-virtual/range {v2 .. v8}, Landroidx/constraintlayout/motion/widget/w;->a(D[I[D[FI)V

    add-int/lit8 v9, v9, 0x2

    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    :cond_1
    div-int/lit8 v9, v9, 0x2

    return v9

    :cond_2
    return v0
.end method

.method a(I)Landroidx/constraintlayout/motion/widget/w;
    .locals 1

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->y:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroidx/constraintlayout/motion/widget/w;

    return-object p1
.end method

.method public a(D[F[F)V
    .locals 9

    const/4 v0, 0x4

    new-array v5, v0, [D

    new-array v7, v0, [D

    new-array v0, v0, [I

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->k:[La/e/a/a/a/b;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2, v5}, La/e/a/a/a/b;->a(D[D)V

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->k:[La/e/a/a/a/b;

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2, v7}, La/e/a/a/a/b;->b(D[D)V

    const/4 v0, 0x0

    invoke-static {p4, v0}, Ljava/util/Arrays;->fill([FF)V

    iget-object v1, p0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget-object v4, p0, Landroidx/constraintlayout/motion/widget/p;->r:[I

    move-wide v2, p1

    move-object v6, p3

    move-object v8, p4

    invoke-virtual/range {v1 .. v8}, Landroidx/constraintlayout/motion/widget/w;->a(D[I[D[F[D[F)V

    return-void
.end method

.method a(FFF[F)V
    .locals 11

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->z:[F

    invoke-direct {p0, p1, v0}, Landroidx/constraintlayout/motion/widget/p;->a(F[F)F

    move-result p1

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->k:[La/e/a/a/a/b;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    aget-object v0, v0, v1

    float-to-double v2, p1

    iget-object p1, p0, Landroidx/constraintlayout/motion/widget/p;->t:[D

    invoke-virtual {v0, v2, v3, p1}, La/e/a/a/a/b;->b(D[D)V

    iget-object p1, p0, Landroidx/constraintlayout/motion/widget/p;->k:[La/e/a/a/a/b;

    aget-object p1, p1, v1

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->s:[D

    invoke-virtual {p1, v2, v3, v0}, La/e/a/a/a/b;->a(D[D)V

    iget-object p1, p0, Landroidx/constraintlayout/motion/widget/p;->z:[F

    aget p1, p1, v1

    :goto_0
    iget-object v9, p0, Landroidx/constraintlayout/motion/widget/p;->t:[D

    array-length v0, v9

    if-ge v1, v0, :cond_0

    aget-wide v4, v9, v1

    float-to-double v6, p1

    mul-double/2addr v4, v6

    aput-wide v4, v9, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Landroidx/constraintlayout/motion/widget/p;->l:La/e/a/a/a/b;

    if-eqz p1, :cond_2

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->s:[D

    array-length v1, v0

    if-lez v1, :cond_1

    invoke-virtual {p1, v2, v3, v0}, La/e/a/a/a/b;->a(D[D)V

    iget-object p1, p0, Landroidx/constraintlayout/motion/widget/p;->l:La/e/a/a/a/b;

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->t:[D

    invoke-virtual {p1, v2, v3, v0}, La/e/a/a/a/b;->b(D[D)V

    iget-object v4, p0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget-object v8, p0, Landroidx/constraintlayout/motion/widget/p;->r:[I

    iget-object v9, p0, Landroidx/constraintlayout/motion/widget/p;->t:[D

    iget-object v10, p0, Landroidx/constraintlayout/motion/widget/p;->s:[D

    move v5, p2

    move v6, p3

    move-object v7, p4

    invoke-virtual/range {v4 .. v10}, Landroidx/constraintlayout/motion/widget/w;->a(FF[F[I[D[D)V

    :cond_1
    return-void

    :cond_2
    iget-object v4, p0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget-object v8, p0, Landroidx/constraintlayout/motion/widget/p;->r:[I

    iget-object v10, p0, Landroidx/constraintlayout/motion/widget/p;->s:[D

    move v5, p2

    move v6, p3

    move-object v7, p4

    invoke-virtual/range {v4 .. v10}, Landroidx/constraintlayout/motion/widget/w;->a(FF[F[I[D[D)V

    return-void

    :cond_3
    iget-object p1, p0, Landroidx/constraintlayout/motion/widget/p;->h:Landroidx/constraintlayout/motion/widget/w;

    iget v0, p1, Landroidx/constraintlayout/motion/widget/w;->f:F

    iget-object v2, p0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget v3, v2, Landroidx/constraintlayout/motion/widget/w;->f:F

    sub-float/2addr v0, v3

    iget v3, p1, Landroidx/constraintlayout/motion/widget/w;->g:F

    iget v4, v2, Landroidx/constraintlayout/motion/widget/w;->g:F

    sub-float/2addr v3, v4

    iget v4, p1, Landroidx/constraintlayout/motion/widget/w;->h:F

    iget v5, v2, Landroidx/constraintlayout/motion/widget/w;->h:F

    sub-float/2addr v4, v5

    iget p1, p1, Landroidx/constraintlayout/motion/widget/w;->i:F

    iget v2, v2, Landroidx/constraintlayout/motion/widget/w;->i:F

    sub-float/2addr p1, v2

    add-float/2addr v4, v0

    add-float/2addr p1, v3

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v5, v2, p2

    mul-float/2addr v0, v5

    mul-float/2addr v4, p2

    add-float/2addr v0, v4

    aput v0, p4, v1

    sub-float/2addr v2, p3

    mul-float/2addr v3, v2

    mul-float/2addr p1, p3

    add-float/2addr v3, p1

    const/4 p1, 0x1

    aput v3, p4, p1

    return-void
.end method

.method a(FIIFF[F)V
    .locals 18

    move-object/from16 v0, p0

    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->z:[F

    move/from16 v2, p1

    invoke-direct {v0, v2, v1}, Landroidx/constraintlayout/motion/widget/p;->a(F[F)F

    move-result v1

    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->C:Ljava/util/HashMap;

    const-string v3, "translationX"

    const/4 v4, 0x0

    if-nez v2, :cond_0

    move-object v2, v4

    goto :goto_0

    :cond_0
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, La/e/a/a/a/k;

    :goto_0
    iget-object v5, v0, Landroidx/constraintlayout/motion/widget/p;->C:Ljava/util/HashMap;

    const-string v6, "translationY"

    if-nez v5, :cond_1

    move-object v5, v4

    goto :goto_1

    :cond_1
    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, La/e/a/a/a/k;

    :goto_1
    iget-object v7, v0, Landroidx/constraintlayout/motion/widget/p;->C:Ljava/util/HashMap;

    const-string v8, "rotation"

    if-nez v7, :cond_2

    move-object v7, v4

    goto :goto_2

    :cond_2
    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, La/e/a/a/a/k;

    :goto_2
    iget-object v9, v0, Landroidx/constraintlayout/motion/widget/p;->C:Ljava/util/HashMap;

    const-string v10, "scaleX"

    if-nez v9, :cond_3

    move-object v9, v4

    goto :goto_3

    :cond_3
    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, La/e/a/a/a/k;

    :goto_3
    iget-object v11, v0, Landroidx/constraintlayout/motion/widget/p;->C:Ljava/util/HashMap;

    const-string v12, "scaleY"

    if-nez v11, :cond_4

    move-object v11, v4

    goto :goto_4

    :cond_4
    invoke-virtual {v11, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, La/e/a/a/a/k;

    :goto_4
    iget-object v13, v0, Landroidx/constraintlayout/motion/widget/p;->D:Ljava/util/HashMap;

    if-nez v13, :cond_5

    move-object v3, v4

    goto :goto_5

    :cond_5
    invoke-virtual {v13, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, La/e/b/a/d;

    :goto_5
    iget-object v13, v0, Landroidx/constraintlayout/motion/widget/p;->D:Ljava/util/HashMap;

    if-nez v13, :cond_6

    move-object v6, v4

    goto :goto_6

    :cond_6
    invoke-virtual {v13, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, La/e/b/a/d;

    :goto_6
    iget-object v13, v0, Landroidx/constraintlayout/motion/widget/p;->D:Ljava/util/HashMap;

    if-nez v13, :cond_7

    move-object v8, v4

    goto :goto_7

    :cond_7
    invoke-virtual {v13, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, La/e/b/a/d;

    :goto_7
    iget-object v13, v0, Landroidx/constraintlayout/motion/widget/p;->D:Ljava/util/HashMap;

    if-nez v13, :cond_8

    move-object v10, v4

    goto :goto_8

    :cond_8
    invoke-virtual {v13, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, La/e/b/a/d;

    :goto_8
    iget-object v13, v0, Landroidx/constraintlayout/motion/widget/p;->D:Ljava/util/HashMap;

    if-nez v13, :cond_9

    goto :goto_9

    :cond_9
    invoke-virtual {v13, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, La/e/b/a/d;

    :goto_9
    new-instance v12, La/e/a/a/a/q;

    invoke-direct {v12}, La/e/a/a/a/q;-><init>()V

    invoke-virtual {v12}, La/e/a/a/a/q;->a()V

    invoke-virtual {v12, v7, v1}, La/e/a/a/a/q;->a(La/e/a/a/a/k;F)V

    invoke-virtual {v12, v2, v5, v1}, La/e/a/a/a/q;->b(La/e/a/a/a/k;La/e/a/a/a/k;F)V

    invoke-virtual {v12, v9, v11, v1}, La/e/a/a/a/q;->a(La/e/a/a/a/k;La/e/a/a/a/k;F)V

    invoke-virtual {v12, v8, v1}, La/e/a/a/a/q;->a(La/e/a/a/a/f;F)V

    invoke-virtual {v12, v3, v6, v1}, La/e/a/a/a/q;->b(La/e/a/a/a/f;La/e/a/a/a/f;F)V

    invoke-virtual {v12, v10, v4, v1}, La/e/a/a/a/q;->a(La/e/a/a/a/f;La/e/a/a/a/f;F)V

    iget-object v13, v0, Landroidx/constraintlayout/motion/widget/p;->l:La/e/a/a/a/b;

    if-eqz v13, :cond_b

    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->s:[D

    array-length v3, v2

    if-lez v3, :cond_a

    float-to-double v3, v1

    invoke-virtual {v13, v3, v4, v2}, La/e/a/a/a/b;->a(D[D)V

    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->l:La/e/a/a/a/b;

    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->t:[D

    invoke-virtual {v1, v3, v4, v2}, La/e/a/a/a/b;->b(D[D)V

    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget-object v5, v0, Landroidx/constraintlayout/motion/widget/p;->r:[I

    iget-object v6, v0, Landroidx/constraintlayout/motion/widget/p;->t:[D

    iget-object v7, v0, Landroidx/constraintlayout/motion/widget/p;->s:[D

    move/from16 v2, p4

    move/from16 v3, p5

    move-object/from16 v4, p6

    invoke-virtual/range {v1 .. v7}, Landroidx/constraintlayout/motion/widget/w;->a(FF[F[I[D[D)V

    :cond_a
    move-object v1, v12

    move/from16 v2, p4

    move/from16 v3, p5

    move/from16 v4, p2

    move/from16 v5, p3

    move-object/from16 v6, p6

    invoke-virtual/range {v1 .. v6}, La/e/a/a/a/q;->a(FFII[F)V

    return-void

    :cond_b
    iget-object v13, v0, Landroidx/constraintlayout/motion/widget/p;->k:[La/e/a/a/a/b;

    const/4 v14, 0x0

    if-eqz v13, :cond_d

    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->z:[F

    invoke-direct {v0, v1, v2}, Landroidx/constraintlayout/motion/widget/p;->a(F[F)F

    move-result v1

    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->k:[La/e/a/a/a/b;

    aget-object v2, v2, v14

    float-to-double v3, v1

    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->t:[D

    invoke-virtual {v2, v3, v4, v1}, La/e/a/a/a/b;->b(D[D)V

    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->k:[La/e/a/a/a/b;

    aget-object v1, v1, v14

    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->s:[D

    invoke-virtual {v1, v3, v4, v2}, La/e/a/a/a/b;->a(D[D)V

    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->z:[F

    aget v1, v1, v14

    :goto_a
    iget-object v6, v0, Landroidx/constraintlayout/motion/widget/p;->t:[D

    array-length v2, v6

    if-ge v14, v2, :cond_c

    aget-wide v2, v6, v14

    float-to-double v4, v1

    mul-double/2addr v2, v4

    aput-wide v2, v6, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_a

    :cond_c
    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget-object v5, v0, Landroidx/constraintlayout/motion/widget/p;->r:[I

    iget-object v7, v0, Landroidx/constraintlayout/motion/widget/p;->s:[D

    move/from16 v2, p4

    move/from16 v3, p5

    move-object/from16 v4, p6

    invoke-virtual/range {v1 .. v7}, Landroidx/constraintlayout/motion/widget/w;->a(FF[F[I[D[D)V

    move-object v1, v12

    move/from16 v4, p2

    move/from16 v5, p3

    move-object/from16 v6, p6

    invoke-virtual/range {v1 .. v6}, La/e/a/a/a/q;->a(FFII[F)V

    return-void

    :cond_d
    iget-object v13, v0, Landroidx/constraintlayout/motion/widget/p;->h:Landroidx/constraintlayout/motion/widget/w;

    iget v15, v13, Landroidx/constraintlayout/motion/widget/w;->f:F

    iget-object v14, v0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget v0, v14, Landroidx/constraintlayout/motion/widget/w;->f:F

    sub-float/2addr v15, v0

    iget v0, v13, Landroidx/constraintlayout/motion/widget/w;->g:F

    move-object/from16 v16, v4

    iget v4, v14, Landroidx/constraintlayout/motion/widget/w;->g:F

    sub-float/2addr v0, v4

    iget v4, v13, Landroidx/constraintlayout/motion/widget/w;->h:F

    move-object/from16 v17, v10

    iget v10, v14, Landroidx/constraintlayout/motion/widget/w;->h:F

    sub-float/2addr v4, v10

    iget v10, v13, Landroidx/constraintlayout/motion/widget/w;->i:F

    iget v13, v14, Landroidx/constraintlayout/motion/widget/w;->i:F

    sub-float/2addr v10, v13

    add-float/2addr v4, v15

    add-float/2addr v10, v0

    const/high16 v13, 0x3f800000    # 1.0f

    sub-float v14, v13, p4

    mul-float/2addr v15, v14

    mul-float v4, v4, p4

    add-float/2addr v15, v4

    const/4 v4, 0x0

    aput v15, p6, v4

    sub-float v13, v13, p5

    mul-float/2addr v0, v13

    mul-float v10, v10, p5

    add-float/2addr v0, v10

    const/4 v4, 0x1

    aput v0, p6, v4

    invoke-virtual {v12}, La/e/a/a/a/q;->a()V

    invoke-virtual {v12, v7, v1}, La/e/a/a/a/q;->a(La/e/a/a/a/k;F)V

    invoke-virtual {v12, v2, v5, v1}, La/e/a/a/a/q;->b(La/e/a/a/a/k;La/e/a/a/a/k;F)V

    invoke-virtual {v12, v9, v11, v1}, La/e/a/a/a/q;->a(La/e/a/a/a/k;La/e/a/a/a/k;F)V

    invoke-virtual {v12, v8, v1}, La/e/a/a/a/q;->a(La/e/a/a/a/f;F)V

    invoke-virtual {v12, v3, v6, v1}, La/e/a/a/a/q;->b(La/e/a/a/a/f;La/e/a/a/a/f;F)V

    move-object/from16 v4, v16

    move-object/from16 v10, v17

    invoke-virtual {v12, v10, v4, v1}, La/e/a/a/a/q;->a(La/e/a/a/a/f;La/e/a/a/a/f;F)V

    move-object v1, v12

    move/from16 v2, p4

    move/from16 v3, p5

    move/from16 v4, p2

    move/from16 v5, p3

    move-object/from16 v6, p6

    invoke-virtual/range {v1 .. v6}, La/e/a/a/a/q;->a(FFII[F)V

    return-void
.end method

.method a(F[FI)V
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroidx/constraintlayout/motion/widget/p;->a(F[F)F

    move-result p1

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->k:[La/e/a/a/a/b;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    float-to-double v1, p1

    iget-object p1, p0, Landroidx/constraintlayout/motion/widget/p;->s:[D

    invoke-virtual {v0, v1, v2, p1}, La/e/a/a/a/b;->a(D[D)V

    iget-object p1, p0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->r:[I

    iget-object v1, p0, Landroidx/constraintlayout/motion/widget/p;->s:[D

    invoke-virtual {p1, v0, v1, p2, p3}, Landroidx/constraintlayout/motion/widget/w;->a([I[D[FI)V

    return-void
.end method

.method public a(IIFJ)V
    .locals 16

    move-object/from16 v0, p0

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iget v5, v0, Landroidx/constraintlayout/motion/widget/p;->F:I

    sget v6, Landroidx/constraintlayout/motion/widget/f;->a:I

    if-eq v5, v6, :cond_0

    iget-object v6, v0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iput v5, v6, Landroidx/constraintlayout/motion/widget/w;->l:I

    :cond_0
    iget-object v5, v0, Landroidx/constraintlayout/motion/widget/p;->i:Landroidx/constraintlayout/motion/widget/n;

    iget-object v6, v0, Landroidx/constraintlayout/motion/widget/p;->j:Landroidx/constraintlayout/motion/widget/n;

    invoke-virtual {v5, v6, v2}, Landroidx/constraintlayout/motion/widget/n;->a(Landroidx/constraintlayout/motion/widget/n;Ljava/util/HashSet;)V

    iget-object v5, v0, Landroidx/constraintlayout/motion/widget/p;->A:Ljava/util/ArrayList;

    if-eqz v5, :cond_7

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    const/4 v7, 0x0

    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroidx/constraintlayout/motion/widget/f;

    instance-of v9, v8, Landroidx/constraintlayout/motion/widget/j;

    if-eqz v9, :cond_2

    check-cast v8, Landroidx/constraintlayout/motion/widget/j;

    new-instance v9, Landroidx/constraintlayout/motion/widget/w;

    iget-object v14, v0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget-object v15, v0, Landroidx/constraintlayout/motion/widget/p;->h:Landroidx/constraintlayout/motion/widget/w;

    move-object v10, v9

    move/from16 v11, p1

    move/from16 v12, p2

    move-object v13, v8

    invoke-direct/range {v10 .. v15}, Landroidx/constraintlayout/motion/widget/w;-><init>(IILandroidx/constraintlayout/motion/widget/j;Landroidx/constraintlayout/motion/widget/w;Landroidx/constraintlayout/motion/widget/w;)V

    invoke-direct {v0, v9}, Landroidx/constraintlayout/motion/widget/p;->a(Landroidx/constraintlayout/motion/widget/w;)V

    iget v8, v8, Landroidx/constraintlayout/motion/widget/k;->g:I

    sget v9, Landroidx/constraintlayout/motion/widget/f;->a:I

    if-eq v8, v9, :cond_1

    iput v8, v0, Landroidx/constraintlayout/motion/widget/p;->f:I

    goto :goto_0

    :cond_2
    instance-of v9, v8, Landroidx/constraintlayout/motion/widget/h;

    if-eqz v9, :cond_3

    invoke-virtual {v8, v3}, Landroidx/constraintlayout/motion/widget/f;->a(Ljava/util/HashSet;)V

    goto :goto_0

    :cond_3
    instance-of v9, v8, Landroidx/constraintlayout/motion/widget/l;

    if-eqz v9, :cond_4

    invoke-virtual {v8, v1}, Landroidx/constraintlayout/motion/widget/f;->a(Ljava/util/HashSet;)V

    goto :goto_0

    :cond_4
    instance-of v9, v8, Landroidx/constraintlayout/motion/widget/m;

    if-eqz v9, :cond_6

    if-nez v7, :cond_5

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    :cond_5
    check-cast v8, Landroidx/constraintlayout/motion/widget/m;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_6
    invoke-virtual {v8, v4}, Landroidx/constraintlayout/motion/widget/f;->b(Ljava/util/HashMap;)V

    invoke-virtual {v8, v2}, Landroidx/constraintlayout/motion/widget/f;->a(Ljava/util/HashSet;)V

    goto :goto_0

    :cond_7
    const/4 v7, 0x0

    :cond_8
    const/4 v5, 0x0

    if-eqz v7, :cond_9

    new-array v8, v5, [Landroidx/constraintlayout/motion/widget/m;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Landroidx/constraintlayout/motion/widget/m;

    iput-object v7, v0, Landroidx/constraintlayout/motion/widget/p;->E:[Landroidx/constraintlayout/motion/widget/m;

    :cond_9
    invoke-virtual {v2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v7

    const-string v8, ","

    const-string v9, "CUSTOM,"

    const/4 v10, 0x1

    if-nez v7, :cond_14

    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    iput-object v7, v0, Landroidx/constraintlayout/motion/widget/p;->C:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_f

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v11, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_d

    new-instance v12, Landroid/util/SparseArray;

    invoke-direct {v12}, Landroid/util/SparseArray;-><init>()V

    invoke-virtual {v11, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    aget-object v13, v13, v10

    iget-object v14, v0, Landroidx/constraintlayout/motion/widget/p;->A:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_a
    :goto_2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_c

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroidx/constraintlayout/motion/widget/f;

    iget-object v6, v15, Landroidx/constraintlayout/motion/widget/f;->f:Ljava/util/HashMap;

    if-nez v6, :cond_b

    goto :goto_2

    :cond_b
    invoke-virtual {v6, v13}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroidx/constraintlayout/widget/b;

    if-eqz v6, :cond_a

    iget v15, v15, Landroidx/constraintlayout/motion/widget/f;->b:I

    invoke-virtual {v12, v15, v6}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_2

    :cond_c
    invoke-static {v11, v12}, La/e/b/a/e;->a(Ljava/lang/String;Landroid/util/SparseArray;)La/e/b/a/e;

    move-result-object v6

    goto :goto_3

    :cond_d
    invoke-static {v11}, La/e/b/a/e;->b(Ljava/lang/String;)La/e/b/a/e;

    move-result-object v6

    :goto_3
    if-nez v6, :cond_e

    goto :goto_1

    :cond_e
    invoke-virtual {v6, v11}, La/e/a/a/a/k;->a(Ljava/lang/String;)V

    iget-object v12, v0, Landroidx/constraintlayout/motion/widget/p;->C:Ljava/util/HashMap;

    invoke-virtual {v12, v11, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_f
    iget-object v6, v0, Landroidx/constraintlayout/motion/widget/p;->A:Ljava/util/ArrayList;

    if-eqz v6, :cond_11

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_10
    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_11

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroidx/constraintlayout/motion/widget/f;

    instance-of v11, v7, Landroidx/constraintlayout/motion/widget/g;

    if-eqz v11, :cond_10

    iget-object v11, v0, Landroidx/constraintlayout/motion/widget/p;->C:Ljava/util/HashMap;

    invoke-virtual {v7, v11}, Landroidx/constraintlayout/motion/widget/f;->a(Ljava/util/HashMap;)V

    goto :goto_4

    :cond_11
    iget-object v6, v0, Landroidx/constraintlayout/motion/widget/p;->i:Landroidx/constraintlayout/motion/widget/n;

    iget-object v7, v0, Landroidx/constraintlayout/motion/widget/p;->C:Ljava/util/HashMap;

    invoke-virtual {v6, v7, v5}, Landroidx/constraintlayout/motion/widget/n;->a(Ljava/util/HashMap;I)V

    iget-object v6, v0, Landroidx/constraintlayout/motion/widget/p;->j:Landroidx/constraintlayout/motion/widget/n;

    iget-object v7, v0, Landroidx/constraintlayout/motion/widget/p;->C:Ljava/util/HashMap;

    const/16 v11, 0x64

    invoke-virtual {v6, v7, v11}, Landroidx/constraintlayout/motion/widget/n;->a(Ljava/util/HashMap;I)V

    iget-object v6, v0, Landroidx/constraintlayout/motion/widget/p;->C:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_12
    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_14

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_13

    invoke-virtual {v4, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    if-eqz v11, :cond_13

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    goto :goto_6

    :cond_13
    move v11, v5

    :goto_6
    iget-object v12, v0, Landroidx/constraintlayout/motion/widget/p;->C:Ljava/util/HashMap;

    invoke-virtual {v12, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, La/e/a/a/a/k;

    if-eqz v7, :cond_12

    invoke-virtual {v7, v11}, La/e/a/a/a/k;->a(I)V

    goto :goto_5

    :cond_14
    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_20

    iget-object v6, v0, Landroidx/constraintlayout/motion/widget/p;->B:Ljava/util/HashMap;

    if-nez v6, :cond_15

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, v0, Landroidx/constraintlayout/motion/widget/p;->B:Ljava/util/HashMap;

    :cond_15
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iget-object v7, v0, Landroidx/constraintlayout/motion/widget/p;->B:Ljava/util/HashMap;

    invoke-virtual {v7, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_16

    goto :goto_7

    :cond_16
    invoke-virtual {v6, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1a

    new-instance v7, Landroid/util/SparseArray;

    invoke-direct {v7}, Landroid/util/SparseArray;-><init>()V

    invoke-virtual {v6, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    aget-object v11, v11, v10

    iget-object v12, v0, Landroidx/constraintlayout/motion/widget/p;->A:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_17
    :goto_8
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_19

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroidx/constraintlayout/motion/widget/f;

    iget-object v14, v13, Landroidx/constraintlayout/motion/widget/f;->f:Ljava/util/HashMap;

    if-nez v14, :cond_18

    goto :goto_8

    :cond_18
    invoke-virtual {v14, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroidx/constraintlayout/widget/b;

    if-eqz v14, :cond_17

    iget v13, v13, Landroidx/constraintlayout/motion/widget/f;->b:I

    invoke-virtual {v7, v13, v14}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_8

    :cond_19
    invoke-static {v6, v7}, La/e/b/a/g;->a(Ljava/lang/String;Landroid/util/SparseArray;)La/e/b/a/g;

    move-result-object v7

    move-wide/from16 v11, p4

    goto :goto_9

    :cond_1a
    move-wide/from16 v11, p4

    invoke-static {v6, v11, v12}, La/e/b/a/g;->a(Ljava/lang/String;J)La/e/b/a/g;

    move-result-object v7

    :goto_9
    if-nez v7, :cond_1b

    goto :goto_7

    :cond_1b
    invoke-virtual {v7, v6}, La/e/a/a/a/p;->a(Ljava/lang/String;)V

    iget-object v13, v0, Landroidx/constraintlayout/motion/widget/p;->B:Ljava/util/HashMap;

    invoke-virtual {v13, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_7

    :cond_1c
    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->A:Ljava/util/ArrayList;

    if-eqz v1, :cond_1e

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1d
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroidx/constraintlayout/motion/widget/f;

    instance-of v7, v6, Landroidx/constraintlayout/motion/widget/l;

    if-eqz v7, :cond_1d

    check-cast v6, Landroidx/constraintlayout/motion/widget/l;

    iget-object v7, v0, Landroidx/constraintlayout/motion/widget/p;->B:Ljava/util/HashMap;

    invoke-virtual {v6, v7}, Landroidx/constraintlayout/motion/widget/l;->c(Ljava/util/HashMap;)V

    goto :goto_a

    :cond_1e
    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->B:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_20

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1f

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    goto :goto_c

    :cond_1f
    move v7, v5

    :goto_c
    iget-object v8, v0, Landroidx/constraintlayout/motion/widget/p;->B:Ljava/util/HashMap;

    invoke-virtual {v8, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, La/e/b/a/g;

    invoke-virtual {v6, v7}, La/e/a/a/a/p;->a(I)V

    goto :goto_b

    :cond_20
    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->y:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v4, 0x2

    add-int/2addr v1, v4

    new-array v1, v1, [Landroidx/constraintlayout/motion/widget/w;

    iget-object v6, v0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    aput-object v6, v1, v5

    array-length v6, v1

    sub-int/2addr v6, v10

    iget-object v7, v0, Landroidx/constraintlayout/motion/widget/p;->h:Landroidx/constraintlayout/motion/widget/w;

    aput-object v7, v1, v6

    iget-object v6, v0, Landroidx/constraintlayout/motion/widget/p;->y:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_21

    iget v6, v0, Landroidx/constraintlayout/motion/widget/p;->f:I

    const/4 v7, -0x1

    if-ne v6, v7, :cond_21

    iput v5, v0, Landroidx/constraintlayout/motion/widget/p;->f:I

    :cond_21
    iget-object v6, v0, Landroidx/constraintlayout/motion/widget/p;->y:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v7, v10

    :goto_d
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_22

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroidx/constraintlayout/motion/widget/w;

    add-int/lit8 v11, v7, 0x1

    aput-object v8, v1, v7

    move v7, v11

    goto :goto_d

    :cond_22
    const/16 v6, 0x12

    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    iget-object v8, v0, Landroidx/constraintlayout/motion/widget/p;->h:Landroidx/constraintlayout/motion/widget/w;

    iget-object v8, v8, Landroidx/constraintlayout/motion/widget/w;->p:Ljava/util/LinkedHashMap;

    invoke-virtual {v8}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_23
    :goto_e
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_24

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    iget-object v12, v0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget-object v12, v12, Landroidx/constraintlayout/motion/widget/w;->p:Ljava/util/LinkedHashMap;

    invoke-virtual {v12, v11}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_23

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v12}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_23

    invoke-virtual {v7, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_e

    :cond_24
    new-array v2, v5, [Ljava/lang/String;

    invoke-virtual {v7, v2}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    iput-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->u:[Ljava/lang/String;

    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->u:[Ljava/lang/String;

    array-length v2, v2

    new-array v2, v2, [I

    iput-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->v:[I

    move v2, v5

    :goto_f
    iget-object v7, v0, Landroidx/constraintlayout/motion/widget/p;->u:[Ljava/lang/String;

    array-length v8, v7

    if-ge v2, v8, :cond_27

    aget-object v7, v7, v2

    iget-object v8, v0, Landroidx/constraintlayout/motion/widget/p;->v:[I

    aput v5, v8, v2

    move v8, v5

    :goto_10
    array-length v9, v1

    if-ge v8, v9, :cond_26

    aget-object v9, v1, v8

    iget-object v9, v9, Landroidx/constraintlayout/motion/widget/w;->p:Ljava/util/LinkedHashMap;

    invoke-virtual {v9, v7}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_25

    aget-object v9, v1, v8

    iget-object v9, v9, Landroidx/constraintlayout/motion/widget/w;->p:Ljava/util/LinkedHashMap;

    invoke-virtual {v9, v7}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroidx/constraintlayout/widget/b;

    if-eqz v9, :cond_25

    iget-object v7, v0, Landroidx/constraintlayout/motion/widget/p;->v:[I

    aget v8, v7, v2

    invoke-virtual {v9}, Landroidx/constraintlayout/widget/b;->e()I

    move-result v9

    add-int/2addr v8, v9

    aput v8, v7, v2

    goto :goto_11

    :cond_25
    add-int/lit8 v8, v8, 0x1

    goto :goto_10

    :cond_26
    :goto_11
    add-int/lit8 v2, v2, 0x1

    goto :goto_f

    :cond_27
    aget-object v2, v1, v5

    iget v2, v2, Landroidx/constraintlayout/motion/widget/w;->l:I

    sget v7, Landroidx/constraintlayout/motion/widget/f;->a:I

    if-eq v2, v7, :cond_28

    move v2, v10

    goto :goto_12

    :cond_28
    move v2, v5

    :goto_12
    iget-object v7, v0, Landroidx/constraintlayout/motion/widget/p;->u:[Ljava/lang/String;

    array-length v7, v7

    add-int/2addr v6, v7

    new-array v6, v6, [Z

    move v7, v10

    :goto_13
    array-length v8, v1

    if-ge v7, v8, :cond_29

    aget-object v8, v1, v7

    add-int/lit8 v9, v7, -0x1

    aget-object v9, v1, v9

    iget-object v11, v0, Landroidx/constraintlayout/motion/widget/p;->u:[Ljava/lang/String;

    invoke-virtual {v8, v9, v6, v11, v2}, Landroidx/constraintlayout/motion/widget/w;->a(Landroidx/constraintlayout/motion/widget/w;[Z[Ljava/lang/String;Z)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_13

    :cond_29
    move v7, v5

    move v2, v10

    :goto_14
    array-length v8, v6

    if-ge v2, v8, :cond_2b

    aget-boolean v8, v6, v2

    if-eqz v8, :cond_2a

    add-int/lit8 v7, v7, 0x1

    :cond_2a
    add-int/lit8 v2, v2, 0x1

    goto :goto_14

    :cond_2b
    new-array v2, v7, [I

    iput-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->r:[I

    invoke-static {v4, v7}, Ljava/lang/Math;->max(II)I

    move-result v2

    new-array v7, v2, [D

    iput-object v7, v0, Landroidx/constraintlayout/motion/widget/p;->s:[D

    new-array v2, v2, [D

    iput-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->t:[D

    move v7, v5

    move v2, v10

    :goto_15
    array-length v8, v6

    if-ge v2, v8, :cond_2d

    aget-boolean v8, v6, v2

    if-eqz v8, :cond_2c

    iget-object v8, v0, Landroidx/constraintlayout/motion/widget/p;->r:[I

    add-int/lit8 v9, v7, 0x1

    aput v2, v8, v7

    move v7, v9

    :cond_2c
    add-int/lit8 v2, v2, 0x1

    goto :goto_15

    :cond_2d
    array-length v2, v1

    iget-object v6, v0, Landroidx/constraintlayout/motion/widget/p;->r:[I

    array-length v6, v6

    filled-new-array {v2, v6}, [I

    move-result-object v2

    const-class v6, D

    invoke-static {v6, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[D

    array-length v6, v1

    new-array v6, v6, [D

    move v7, v5

    :goto_16
    array-length v8, v1

    if-ge v7, v8, :cond_2e

    aget-object v8, v1, v7

    aget-object v9, v2, v7

    iget-object v11, v0, Landroidx/constraintlayout/motion/widget/p;->r:[I

    invoke-virtual {v8, v9, v11}, Landroidx/constraintlayout/motion/widget/w;->a([D[I)V

    aget-object v8, v1, v7

    iget v8, v8, Landroidx/constraintlayout/motion/widget/w;->d:F

    float-to-double v8, v8

    aput-wide v8, v6, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_16

    :cond_2e
    move v7, v5

    :goto_17
    iget-object v8, v0, Landroidx/constraintlayout/motion/widget/p;->r:[I

    array-length v9, v8

    if-ge v7, v9, :cond_30

    aget v8, v8, v7

    sget-object v9, Landroidx/constraintlayout/motion/widget/w;->a:[Ljava/lang/String;

    array-length v9, v9

    if-ge v8, v9, :cond_2f

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Landroidx/constraintlayout/motion/widget/w;->a:[Ljava/lang/String;

    iget-object v11, v0, Landroidx/constraintlayout/motion/widget/p;->r:[I

    aget v11, v11, v7

    aget-object v9, v9, v11

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, " ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object v9, v8

    move v8, v5

    :goto_18
    array-length v11, v1

    if-ge v8, v11, :cond_2f

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v9, v2, v8

    aget-wide v12, v9, v7

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v8, v8, 0x1

    goto :goto_18

    :cond_2f
    add-int/lit8 v7, v7, 0x1

    goto :goto_17

    :cond_30
    iget-object v7, v0, Landroidx/constraintlayout/motion/widget/p;->u:[Ljava/lang/String;

    array-length v7, v7

    add-int/2addr v7, v10

    new-array v7, v7, [La/e/a/a/a/b;

    iput-object v7, v0, Landroidx/constraintlayout/motion/widget/p;->k:[La/e/a/a/a/b;

    move v7, v5

    :goto_19
    iget-object v8, v0, Landroidx/constraintlayout/motion/widget/p;->u:[Ljava/lang/String;

    array-length v9, v8

    if-ge v7, v9, :cond_34

    aget-object v8, v8, v7

    move v9, v5

    move v12, v9

    const/4 v11, 0x0

    const/4 v13, 0x0

    :goto_1a
    array-length v14, v1

    if-ge v9, v14, :cond_33

    aget-object v14, v1, v9

    invoke-virtual {v14, v8}, Landroidx/constraintlayout/motion/widget/w;->b(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_32

    if-nez v13, :cond_31

    array-length v11, v1

    new-array v11, v11, [D

    array-length v13, v1

    aget-object v14, v1, v9

    invoke-virtual {v14, v8}, Landroidx/constraintlayout/motion/widget/w;->a(Ljava/lang/String;)I

    move-result v14

    filled-new-array {v13, v14}, [I

    move-result-object v13

    const-class v14, D

    invoke-static {v14, v13}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [[D

    :cond_31
    aget-object v14, v1, v9

    iget v14, v14, Landroidx/constraintlayout/motion/widget/w;->d:F

    float-to-double v14, v14

    aput-wide v14, v11, v12

    aget-object v14, v1, v9

    aget-object v15, v13, v12

    invoke-virtual {v14, v8, v15, v5}, Landroidx/constraintlayout/motion/widget/w;->a(Ljava/lang/String;[DI)I

    add-int/lit8 v12, v12, 0x1

    :cond_32
    add-int/lit8 v9, v9, 0x1

    goto :goto_1a

    :cond_33
    invoke-static {v11, v12}, Ljava/util/Arrays;->copyOf([DI)[D

    move-result-object v8

    invoke-static {v13, v12}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [[D

    iget-object v11, v0, Landroidx/constraintlayout/motion/widget/p;->k:[La/e/a/a/a/b;

    add-int/lit8 v7, v7, 0x1

    iget v12, v0, Landroidx/constraintlayout/motion/widget/p;->f:I

    invoke-static {v12, v8, v9}, La/e/a/a/a/b;->a(I[D[[D)La/e/a/a/a/b;

    move-result-object v8

    aput-object v8, v11, v7

    goto :goto_19

    :cond_34
    iget-object v7, v0, Landroidx/constraintlayout/motion/widget/p;->k:[La/e/a/a/a/b;

    iget v8, v0, Landroidx/constraintlayout/motion/widget/p;->f:I

    invoke-static {v8, v6, v2}, La/e/a/a/a/b;->a(I[D[[D)La/e/a/a/a/b;

    move-result-object v2

    aput-object v2, v7, v5

    aget-object v2, v1, v5

    iget v2, v2, Landroidx/constraintlayout/motion/widget/w;->l:I

    sget v6, Landroidx/constraintlayout/motion/widget/f;->a:I

    if-eq v2, v6, :cond_36

    array-length v2, v1

    new-array v6, v2, [I

    new-array v7, v2, [D

    filled-new-array {v2, v4}, [I

    move-result-object v4

    const-class v8, D

    invoke-static {v8, v4}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [[D

    move v8, v5

    :goto_1b
    if-ge v8, v2, :cond_35

    aget-object v9, v1, v8

    iget v9, v9, Landroidx/constraintlayout/motion/widget/w;->l:I

    aput v9, v6, v8

    aget-object v9, v1, v8

    iget v9, v9, Landroidx/constraintlayout/motion/widget/w;->d:F

    float-to-double v11, v9

    aput-wide v11, v7, v8

    aget-object v9, v4, v8

    aget-object v11, v1, v8

    iget v11, v11, Landroidx/constraintlayout/motion/widget/w;->f:F

    float-to-double v11, v11

    aput-wide v11, v9, v5

    aget-object v9, v4, v8

    aget-object v11, v1, v8

    iget v11, v11, Landroidx/constraintlayout/motion/widget/w;->g:F

    float-to-double v11, v11

    aput-wide v11, v9, v10

    add-int/lit8 v8, v8, 0x1

    goto :goto_1b

    :cond_35
    invoke-static {v6, v7, v4}, La/e/a/a/a/b;->a([I[D[[D)La/e/a/a/a/b;

    move-result-object v1

    iput-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->l:La/e/a/a/a/b;

    :cond_36
    const/high16 v1, 0x7fc00000    # Float.NaN

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->D:Ljava/util/HashMap;

    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->A:Ljava/util/ArrayList;

    if-eqz v2, :cond_3c

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_39

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, La/e/b/a/d;->b(Ljava/lang/String;)La/e/b/a/d;

    move-result-object v4

    if-nez v4, :cond_37

    goto :goto_1c

    :cond_37
    invoke-virtual {v4}, La/e/a/a/a/f;->a()Z

    move-result v5

    if-eqz v5, :cond_38

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-eqz v5, :cond_38

    invoke-direct/range {p0 .. p0}, Landroidx/constraintlayout/motion/widget/p;->k()F

    move-result v1

    :cond_38
    invoke-virtual {v4, v3}, La/e/a/a/a/f;->a(Ljava/lang/String;)V

    iget-object v5, v0, Landroidx/constraintlayout/motion/widget/p;->D:Ljava/util/HashMap;

    invoke-virtual {v5, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1c

    :cond_39
    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->A:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3a
    :goto_1d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroidx/constraintlayout/motion/widget/f;

    instance-of v4, v3, Landroidx/constraintlayout/motion/widget/h;

    if-eqz v4, :cond_3a

    check-cast v3, Landroidx/constraintlayout/motion/widget/h;

    iget-object v4, v0, Landroidx/constraintlayout/motion/widget/p;->D:Ljava/util/HashMap;

    invoke-virtual {v3, v4}, Landroidx/constraintlayout/motion/widget/h;->c(Ljava/util/HashMap;)V

    goto :goto_1d

    :cond_3b
    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->D:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, La/e/b/a/d;

    invoke-virtual {v3, v1}, La/e/a/a/a/f;->c(F)V

    goto :goto_1e

    :cond_3c
    return-void
.end method

.method public a(La/e/b/a/f;Landroid/view/View;III)V
    .locals 4

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    const/4 v1, 0x0

    iput v1, v0, Landroidx/constraintlayout/motion/widget/w;->d:F

    iput v1, v0, Landroidx/constraintlayout/motion/widget/w;->e:F

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    const/4 v1, 0x1

    const/4 v2, 0x2

    if-eq p3, v1, :cond_1

    if-eq p3, v2, :cond_0

    goto :goto_0

    :cond_0
    iget p4, p1, La/e/b/a/f;->b:I

    iget v1, p1, La/e/b/a/f;->d:I

    add-int/2addr p4, v1

    iget v1, p1, La/e/b/a/f;->c:I

    iget v3, p1, La/e/b/a/f;->e:I

    add-int/2addr v1, v3

    invoke-virtual {p1}, La/e/b/a/f;->b()I

    move-result v3

    add-int/2addr v1, v3

    div-int/2addr v1, v2

    sub-int/2addr p5, v1

    iput p5, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, La/e/b/a/f;->a()I

    move-result p5

    sub-int/2addr p4, p5

    div-int/2addr p4, v2

    iput p4, v0, Landroid/graphics/Rect;->top:I

    iget p4, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, La/e/b/a/f;->b()I

    move-result p5

    add-int/2addr p4, p5

    iput p4, v0, Landroid/graphics/Rect;->right:I

    iget p4, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, La/e/b/a/f;->a()I

    move-result p5

    add-int/2addr p4, p5

    iput p4, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    :cond_1
    iget p5, p1, La/e/b/a/f;->b:I

    iget v1, p1, La/e/b/a/f;->d:I

    add-int/2addr p5, v1

    iget v1, p1, La/e/b/a/f;->c:I

    iget v3, p1, La/e/b/a/f;->e:I

    add-int/2addr v1, v3

    invoke-virtual {p1}, La/e/b/a/f;->b()I

    move-result v3

    sub-int/2addr v1, v3

    div-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, La/e/b/a/f;->a()I

    move-result v1

    add-int/2addr p5, v1

    div-int/2addr p5, v2

    sub-int/2addr p4, p5

    iput p4, v0, Landroid/graphics/Rect;->top:I

    iget p4, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, La/e/b/a/f;->b()I

    move-result p5

    add-int/2addr p4, p5

    iput p4, v0, Landroid/graphics/Rect;->right:I

    iget p4, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, La/e/b/a/f;->a()I

    move-result p5

    add-int/2addr p4, p5

    iput p4, v0, Landroid/graphics/Rect;->bottom:I

    :goto_0
    iget-object p4, p0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget p5, v0, Landroid/graphics/Rect;->left:I

    int-to-float p5, p5

    iget v1, v0, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p4, p5, v1, v2, v3}, Landroidx/constraintlayout/motion/widget/w;->a(FFFF)V

    iget-object p4, p0, Landroidx/constraintlayout/motion/widget/p;->i:Landroidx/constraintlayout/motion/widget/n;

    iget p1, p1, La/e/b/a/f;->a:F

    invoke-virtual {p4, v0, p2, p3, p1}, Landroidx/constraintlayout/motion/widget/n;->a(Landroid/graphics/Rect;Landroid/view/View;IF)V

    return-void
.end method

.method a(Landroid/graphics/Rect;Landroid/graphics/Rect;III)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x2

    if-eq p3, v0, :cond_3

    if-eq p3, v1, :cond_2

    const/4 v0, 0x3

    if-eq p3, v0, :cond_1

    const/4 p5, 0x4

    if-eq p3, p5, :cond_0

    goto/16 :goto_0

    :cond_0
    iget p3, p1, Landroid/graphics/Rect;->left:I

    iget p5, p1, Landroid/graphics/Rect;->right:I

    add-int/2addr p3, p5

    iget p5, p1, Landroid/graphics/Rect;->bottom:I

    iget v0, p1, Landroid/graphics/Rect;->top:I

    add-int/2addr p5, v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    add-int/2addr p5, v0

    div-int/2addr p5, v1

    sub-int/2addr p4, p5

    iput p4, p2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p4

    sub-int/2addr p3, p4

    div-int/2addr p3, v1

    iput p3, p2, Landroid/graphics/Rect;->top:I

    iget p3, p2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p4

    add-int/2addr p3, p4

    iput p3, p2, Landroid/graphics/Rect;->right:I

    iget p3, p2, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p1

    add-int/2addr p3, p1

    iput p3, p2, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_0

    :cond_1
    iget p3, p1, Landroid/graphics/Rect;->left:I

    iget p4, p1, Landroid/graphics/Rect;->right:I

    add-int/2addr p3, p4

    iget p4, p1, Landroid/graphics/Rect;->top:I

    iget p4, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p4

    div-int/2addr p4, v1

    iget v0, p1, Landroid/graphics/Rect;->top:I

    add-int/2addr p4, v0

    div-int/lit8 v0, p3, 0x2

    sub-int/2addr p4, v0

    iput p4, p2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p4

    add-int/2addr p3, p4

    div-int/2addr p3, v1

    sub-int/2addr p5, p3

    iput p5, p2, Landroid/graphics/Rect;->top:I

    iget p3, p2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p4

    add-int/2addr p3, p4

    iput p3, p2, Landroid/graphics/Rect;->right:I

    iget p3, p2, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p1

    add-int/2addr p3, p1

    iput p3, p2, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    :cond_2
    iget p3, p1, Landroid/graphics/Rect;->left:I

    iget p5, p1, Landroid/graphics/Rect;->right:I

    add-int/2addr p3, p5

    iget p5, p1, Landroid/graphics/Rect;->top:I

    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr p5, v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    add-int/2addr p5, v0

    div-int/2addr p5, v1

    sub-int/2addr p4, p5

    iput p4, p2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p4

    sub-int/2addr p3, p4

    div-int/2addr p3, v1

    iput p3, p2, Landroid/graphics/Rect;->top:I

    iget p3, p2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p4

    add-int/2addr p3, p4

    iput p3, p2, Landroid/graphics/Rect;->right:I

    iget p3, p2, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p1

    add-int/2addr p3, p1

    iput p3, p2, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    :cond_3
    iget p3, p1, Landroid/graphics/Rect;->left:I

    iget p4, p1, Landroid/graphics/Rect;->right:I

    add-int/2addr p3, p4

    iget p4, p1, Landroid/graphics/Rect;->top:I

    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr p4, v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    sub-int/2addr p4, v0

    div-int/2addr p4, v1

    iput p4, p2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p4

    add-int/2addr p3, p4

    div-int/2addr p3, v1

    sub-int/2addr p5, p3

    iput p5, p2, Landroid/graphics/Rect;->top:I

    iget p3, p2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p4

    add-int/2addr p3, p4

    iput p3, p2, Landroid/graphics/Rect;->right:I

    iget p3, p2, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p1

    add-int/2addr p3, p1

    iput p3, p2, Landroid/graphics/Rect;->bottom:I

    :goto_0
    return-void
.end method

.method a(Landroid/graphics/Rect;Landroidx/constraintlayout/widget/e;II)V
    .locals 7

    iget v6, p2, Landroidx/constraintlayout/widget/e;->g:I

    if-eqz v6, :cond_0

    iget-object v2, p0, Landroidx/constraintlayout/motion/widget/p;->a:Landroid/graphics/Rect;

    move-object v0, p0

    move-object v1, p1

    move v3, v6

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Landroidx/constraintlayout/motion/widget/p;->a(Landroid/graphics/Rect;Landroid/graphics/Rect;III)V

    iget-object p1, p0, Landroidx/constraintlayout/motion/widget/p;->a:Landroid/graphics/Rect;

    :cond_0
    iget-object p3, p0, Landroidx/constraintlayout/motion/widget/p;->h:Landroidx/constraintlayout/motion/widget/w;

    const/high16 p4, 0x3f800000    # 1.0f

    iput p4, p3, Landroidx/constraintlayout/motion/widget/w;->d:F

    iput p4, p3, Landroidx/constraintlayout/motion/widget/w;->e:F

    invoke-direct {p0, p3}, Landroidx/constraintlayout/motion/widget/p;->b(Landroidx/constraintlayout/motion/widget/w;)V

    iget-object p3, p0, Landroidx/constraintlayout/motion/widget/p;->h:Landroidx/constraintlayout/motion/widget/w;

    iget p4, p1, Landroid/graphics/Rect;->left:I

    int-to-float p4, p4

    iget v0, p1, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p3, p4, v0, v1, v2}, Landroidx/constraintlayout/motion/widget/w;->a(FFFF)V

    iget-object p3, p0, Landroidx/constraintlayout/motion/widget/p;->h:Landroidx/constraintlayout/motion/widget/w;

    iget p4, p0, Landroidx/constraintlayout/motion/widget/p;->c:I

    invoke-virtual {p2, p4}, Landroidx/constraintlayout/widget/e;->c(I)Landroidx/constraintlayout/widget/e$a;

    move-result-object p4

    invoke-virtual {p3, p4}, Landroidx/constraintlayout/motion/widget/w;->a(Landroidx/constraintlayout/widget/e$a;)V

    iget-object p3, p0, Landroidx/constraintlayout/motion/widget/p;->j:Landroidx/constraintlayout/motion/widget/n;

    iget p4, p0, Landroidx/constraintlayout/motion/widget/p;->c:I

    invoke-virtual {p3, p1, p2, v6, p4}, Landroidx/constraintlayout/motion/widget/n;->a(Landroid/graphics/Rect;Landroidx/constraintlayout/widget/e;II)V

    return-void
.end method

.method a(Landroid/view/View;)V
    .locals 5

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    const/4 v1, 0x0

    iput v1, v0, Landroidx/constraintlayout/motion/widget/w;->d:F

    iput v1, v0, Landroidx/constraintlayout/motion/widget/w;->e:F

    const/4 v1, 0x1

    iput-boolean v1, p0, Landroidx/constraintlayout/motion/widget/p;->L:Z

    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroidx/constraintlayout/motion/widget/w;->a(FFFF)V

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->h:Landroidx/constraintlayout/motion/widget/w;

    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroidx/constraintlayout/motion/widget/w;->a(FFFF)V

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->i:Landroidx/constraintlayout/motion/widget/n;

    invoke-virtual {v0, p1}, Landroidx/constraintlayout/motion/widget/n;->b(Landroid/view/View;)V

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->j:Landroidx/constraintlayout/motion/widget/n;

    invoke-virtual {v0, p1}, Landroidx/constraintlayout/motion/widget/n;->b(Landroid/view/View;)V

    return-void
.end method

.method public a(Landroidx/constraintlayout/motion/widget/f;)V
    .locals 1

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->A:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Landroidx/constraintlayout/motion/widget/p;)V
    .locals 2

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget-object v1, p1, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    invoke-virtual {v0, p1, v1}, Landroidx/constraintlayout/motion/widget/w;->a(Landroidx/constraintlayout/motion/widget/p;Landroidx/constraintlayout/motion/widget/w;)V

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->h:Landroidx/constraintlayout/motion/widget/w;

    iget-object v1, p1, Landroidx/constraintlayout/motion/widget/p;->h:Landroidx/constraintlayout/motion/widget/w;

    invoke-virtual {v0, p1, v1}, Landroidx/constraintlayout/motion/widget/w;->a(Landroidx/constraintlayout/motion/widget/p;Landroidx/constraintlayout/motion/widget/w;)V

    return-void
.end method

.method a(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroidx/constraintlayout/motion/widget/f;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->A:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method a(Z)V
    .locals 4

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->b:Landroid/view/View;

    invoke-static {v0}, Landroidx/constraintlayout/motion/widget/c;->a(Landroid/view/View;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "button"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->E:[Landroidx/constraintlayout/motion/widget/m;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Landroidx/constraintlayout/motion/widget/p;->E:[Landroidx/constraintlayout/motion/widget/m;

    array-length v2, v1

    if-ge v0, v2, :cond_1

    aget-object v1, v1, v0

    if-eqz p1, :cond_0

    const/high16 v2, -0x3d380000    # -100.0f

    goto :goto_1

    :cond_0
    const/high16 v2, 0x42c80000    # 100.0f

    :goto_1
    iget-object v3, p0, Landroidx/constraintlayout/motion/widget/p;->b:Landroid/view/View;

    invoke-virtual {v1, v2, v3}, Landroidx/constraintlayout/motion/widget/m;->a(FLandroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method a([FI)V
    .locals 20

    move-object/from16 v0, p0

    move/from16 v8, p2

    add-int/lit8 v1, v8, -0x1

    int-to-float v1, v1

    const/high16 v9, 0x3f800000    # 1.0f

    div-float v10, v9, v1

    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->C:Ljava/util/HashMap;

    const-string v2, "translationX"

    const/4 v3, 0x0

    if-nez v1, :cond_0

    move-object v11, v3

    goto :goto_0

    :cond_0
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La/e/a/a/a/k;

    move-object v11, v1

    :goto_0
    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->C:Ljava/util/HashMap;

    const-string v4, "translationY"

    if-nez v1, :cond_1

    move-object v12, v3

    goto :goto_1

    :cond_1
    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La/e/a/a/a/k;

    move-object v12, v1

    :goto_1
    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->D:Ljava/util/HashMap;

    if-nez v1, :cond_2

    move-object v13, v3

    goto :goto_2

    :cond_2
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La/e/b/a/d;

    move-object v13, v1

    :goto_2
    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->D:Ljava/util/HashMap;

    if-nez v1, :cond_3

    goto :goto_3

    :cond_3
    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, La/e/b/a/d;

    :goto_3
    move-object v14, v3

    const/4 v7, 0x0

    :goto_4
    if-ge v7, v8, :cond_11

    int-to-float v1, v7

    mul-float/2addr v1, v10

    iget v2, v0, Landroidx/constraintlayout/motion/widget/p;->o:F

    cmpl-float v2, v2, v9

    if-eqz v2, :cond_5

    iget v2, v0, Landroidx/constraintlayout/motion/widget/p;->n:F

    cmpg-float v2, v1, v2

    if-gez v2, :cond_4

    const/4 v1, 0x0

    :cond_4
    iget v2, v0, Landroidx/constraintlayout/motion/widget/p;->n:F

    cmpl-float v4, v1, v2

    if-lez v4, :cond_5

    float-to-double v4, v1

    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    cmpg-double v4, v4, v16

    if-gez v4, :cond_5

    sub-float/2addr v1, v2

    iget v2, v0, Landroidx/constraintlayout/motion/widget/p;->o:F

    mul-float/2addr v1, v2

    invoke-static {v1, v9}, Ljava/lang/Math;->min(FF)F

    move-result v1

    :cond_5
    move v6, v1

    float-to-double v1, v6

    iget-object v4, v0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget-object v4, v4, Landroidx/constraintlayout/motion/widget/w;->b:La/e/a/a/a/c;

    const/high16 v5, 0x7fc00000    # Float.NaN

    iget-object v3, v0, Landroidx/constraintlayout/motion/widget/p;->y:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const/16 v16, 0x0

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v9, v17

    check-cast v9, Landroidx/constraintlayout/motion/widget/w;

    iget-object v15, v9, Landroidx/constraintlayout/motion/widget/w;->b:La/e/a/a/a/c;

    if-eqz v15, :cond_7

    move-wide/from16 v18, v1

    iget v1, v9, Landroidx/constraintlayout/motion/widget/w;->d:F

    cmpg-float v2, v1, v6

    if-gez v2, :cond_6

    move/from16 v16, v1

    move-object v4, v15

    goto :goto_6

    :cond_6
    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-eqz v1, :cond_8

    iget v5, v9, Landroidx/constraintlayout/motion/widget/w;->d:F

    goto :goto_6

    :cond_7
    move-wide/from16 v18, v1

    :cond_8
    :goto_6
    move-wide/from16 v1, v18

    const/high16 v9, 0x3f800000    # 1.0f

    goto :goto_5

    :cond_9
    move-wide/from16 v18, v1

    if-eqz v4, :cond_b

    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-eqz v1, :cond_a

    const/high16 v5, 0x3f800000    # 1.0f

    :cond_a
    sub-float v1, v6, v16

    sub-float v5, v5, v16

    div-float/2addr v1, v5

    float-to-double v1, v1

    invoke-virtual {v4, v1, v2}, La/e/a/a/a/c;->a(D)D

    move-result-wide v1

    double-to-float v1, v1

    mul-float/2addr v1, v5

    add-float v1, v1, v16

    float-to-double v1, v1

    move-wide v2, v1

    goto :goto_7

    :cond_b
    move-wide/from16 v2, v18

    :goto_7
    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->k:[La/e/a/a/a/b;

    const/4 v9, 0x0

    aget-object v1, v1, v9

    iget-object v4, v0, Landroidx/constraintlayout/motion/widget/p;->s:[D

    invoke-virtual {v1, v2, v3, v4}, La/e/a/a/a/b;->a(D[D)V

    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->l:La/e/a/a/a/b;

    if-eqz v1, :cond_c

    iget-object v4, v0, Landroidx/constraintlayout/motion/widget/p;->s:[D

    array-length v5, v4

    if-lez v5, :cond_c

    invoke-virtual {v1, v2, v3, v4}, La/e/a/a/a/b;->a(D[D)V

    :cond_c
    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget-object v4, v0, Landroidx/constraintlayout/motion/widget/p;->r:[I

    iget-object v5, v0, Landroidx/constraintlayout/motion/widget/p;->s:[D

    mul-int/lit8 v15, v7, 0x2

    move v9, v6

    move-object/from16 v6, p1

    move/from16 v16, v7

    move v7, v15

    invoke-virtual/range {v1 .. v7}, Landroidx/constraintlayout/motion/widget/w;->a(D[I[D[FI)V

    if-eqz v13, :cond_d

    aget v1, p1, v15

    invoke-virtual {v13, v9}, La/e/a/a/a/f;->a(F)F

    move-result v2

    add-float/2addr v1, v2

    aput v1, p1, v15

    goto :goto_8

    :cond_d
    if-eqz v11, :cond_e

    aget v1, p1, v15

    invoke-virtual {v11, v9}, La/e/a/a/a/k;->a(F)F

    move-result v2

    add-float/2addr v1, v2

    aput v1, p1, v15

    :cond_e
    :goto_8
    if-eqz v14, :cond_f

    add-int/lit8 v15, v15, 0x1

    aget v1, p1, v15

    invoke-virtual {v14, v9}, La/e/a/a/a/f;->a(F)F

    move-result v2

    add-float/2addr v1, v2

    aput v1, p1, v15

    goto :goto_9

    :cond_f
    if-eqz v12, :cond_10

    add-int/lit8 v15, v15, 0x1

    aget v1, p1, v15

    invoke-virtual {v12, v9}, La/e/a/a/a/k;->a(F)F

    move-result v2

    add-float/2addr v1, v2

    aput v1, p1, v15

    :cond_10
    :goto_9
    add-int/lit8 v7, v16, 0x1

    const/high16 v9, 0x3f800000    # 1.0f

    goto/16 :goto_4

    :cond_11
    return-void
.end method

.method a(Landroid/view/View;FJLa/e/a/a/a/d;)Z
    .locals 20

    move-object/from16 v0, p0

    move-object/from16 v11, p1

    const/4 v1, 0x0

    move/from16 v2, p2

    invoke-direct {v0, v2, v1}, Landroidx/constraintlayout/motion/widget/p;->a(F[F)F

    move-result v2

    iget v3, v0, Landroidx/constraintlayout/motion/widget/p;->I:I

    sget v4, Landroidx/constraintlayout/motion/widget/f;->a:I

    const/high16 v13, 0x3f800000    # 1.0f

    if-eq v3, v4, :cond_3

    int-to-float v3, v3

    div-float v3, v13, v3

    div-float v4, v2, v3

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v4, v3

    rem-float/2addr v2, v3

    div-float/2addr v2, v3

    iget v5, v0, Landroidx/constraintlayout/motion/widget/p;->J:F

    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-nez v5, :cond_0

    iget v5, v0, Landroidx/constraintlayout/motion/widget/p;->J:F

    add-float/2addr v2, v5

    rem-float/2addr v2, v13

    :cond_0
    iget-object v5, v0, Landroidx/constraintlayout/motion/widget/p;->K:Landroid/view/animation/Interpolator;

    if-eqz v5, :cond_1

    invoke-interface {v5, v2}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v2

    goto :goto_0

    :cond_1
    float-to-double v5, v2

    const-wide/high16 v7, 0x3fe0000000000000L    # 0.5

    cmpl-double v2, v5, v7

    if-lez v2, :cond_2

    move v2, v13

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :goto_0
    mul-float/2addr v2, v3

    add-float/2addr v2, v4

    :cond_3
    move v14, v2

    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->C:Ljava/util/HashMap;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, La/e/b/a/e;

    invoke-virtual {v3, v11, v14}, La/e/b/a/e;->a(Landroid/view/View;F)V

    goto :goto_1

    :cond_4
    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->B:Ljava/util/HashMap;

    const/4 v15, 0x0

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v9, v1

    move v8, v15

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La/e/b/a/g;

    instance-of v2, v1, La/e/b/a/g$d;

    if-eqz v2, :cond_5

    move-object v9, v1

    check-cast v9, La/e/b/a/g$d;

    goto :goto_2

    :cond_5
    move-object/from16 v2, p1

    move v3, v14

    move-wide/from16 v4, p3

    move-object/from16 v6, p5

    invoke-virtual/range {v1 .. v6}, La/e/b/a/g;->a(Landroid/view/View;FJLa/e/a/a/a/d;)Z

    move-result v1

    or-int/2addr v8, v1

    goto :goto_2

    :cond_6
    move/from16 v16, v8

    goto :goto_3

    :cond_7
    move-object v9, v1

    move/from16 v16, v15

    :goto_3
    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->k:[La/e/a/a/a/b;

    const/4 v10, 0x1

    if-eqz v1, :cond_13

    aget-object v1, v1, v15

    float-to-double v7, v14

    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->s:[D

    invoke-virtual {v1, v7, v8, v2}, La/e/a/a/a/b;->a(D[D)V

    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->k:[La/e/a/a/a/b;

    aget-object v1, v1, v15

    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->t:[D

    invoke-virtual {v1, v7, v8, v2}, La/e/a/a/a/b;->b(D[D)V

    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->l:La/e/a/a/a/b;

    if-eqz v1, :cond_8

    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->s:[D

    array-length v3, v2

    if-lez v3, :cond_8

    invoke-virtual {v1, v7, v8, v2}, La/e/a/a/a/b;->a(D[D)V

    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->l:La/e/a/a/a/b;

    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->t:[D

    invoke-virtual {v1, v7, v8, v2}, La/e/a/a/a/b;->b(D[D)V

    :cond_8
    iget-boolean v1, v0, Landroidx/constraintlayout/motion/widget/p;->L:Z

    if-nez v1, :cond_9

    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget-object v4, v0, Landroidx/constraintlayout/motion/widget/p;->r:[I

    iget-object v5, v0, Landroidx/constraintlayout/motion/widget/p;->s:[D

    iget-object v6, v0, Landroidx/constraintlayout/motion/widget/p;->t:[D

    const/16 v17, 0x0

    iget-boolean v3, v0, Landroidx/constraintlayout/motion/widget/p;->d:Z

    move v2, v14

    move/from16 v18, v3

    move-object/from16 v3, p1

    move-wide v12, v7

    move-object/from16 v7, v17

    move/from16 v8, v18

    invoke-virtual/range {v1 .. v8}, Landroidx/constraintlayout/motion/widget/w;->a(FLandroid/view/View;[I[D[D[DZ)V

    iput-boolean v15, v0, Landroidx/constraintlayout/motion/widget/p;->d:Z

    goto :goto_4

    :cond_9
    move-wide v12, v7

    :goto_4
    iget v1, v0, Landroidx/constraintlayout/motion/widget/p;->G:I

    sget v2, Landroidx/constraintlayout/motion/widget/f;->a:I

    if-eq v1, v2, :cond_b

    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->H:Landroid/view/View;

    if-nez v1, :cond_a

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iget v2, v0, Landroidx/constraintlayout/motion/widget/p;->G:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->H:Landroid/view/View;

    :cond_a
    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->H:Landroid/view/View;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->H:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iget-object v3, v0, Landroidx/constraintlayout/motion/widget/p;->H:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    iget-object v4, v0, Landroidx/constraintlayout/motion/widget/p;->H:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    div-float/2addr v3, v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getRight()I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLeft()I

    move-result v4

    sub-int/2addr v2, v4

    if-lez v2, :cond_b

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getBottom()I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTop()I

    move-result v4

    sub-int/2addr v2, v4

    if-lez v2, :cond_b

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLeft()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v3, v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTop()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {v11, v3}, Landroid/view/View;->setPivotX(F)V

    invoke-virtual {v11, v1}, Landroid/view/View;->setPivotY(F)V

    :cond_b
    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->C:Ljava/util/HashMap;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_c
    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La/e/a/a/a/k;

    instance-of v2, v1, La/e/b/a/e$d;

    if-eqz v2, :cond_c

    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->t:[D

    array-length v3, v2

    if-le v3, v10, :cond_c

    check-cast v1, La/e/b/a/e$d;

    aget-wide v4, v2, v15

    aget-wide v6, v2, v10

    move-object/from16 v2, p1

    move v3, v14

    invoke-virtual/range {v1 .. v7}, La/e/b/a/e$d;->a(Landroid/view/View;FDD)V

    goto :goto_5

    :cond_d
    if-eqz v9, :cond_e

    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->t:[D

    aget-wide v7, v1, v15

    aget-wide v17, v1, v10

    move-object v1, v9

    move-object/from16 v2, p1

    move-object/from16 v3, p5

    move v4, v14

    move-wide/from16 v5, p3

    move/from16 v19, v10

    move-wide/from16 v9, v17

    invoke-virtual/range {v1 .. v10}, La/e/b/a/g$d;->a(Landroid/view/View;La/e/a/a/a/d;FJDD)Z

    move-result v1

    or-int v1, v16, v1

    move/from16 v16, v1

    goto :goto_6

    :cond_e
    move/from16 v19, v10

    :goto_6
    move/from16 v1, v19

    :goto_7
    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->k:[La/e/a/a/a/b;

    array-length v3, v2

    if-ge v1, v3, :cond_f

    aget-object v2, v2, v1

    iget-object v3, v0, Landroidx/constraintlayout/motion/widget/p;->x:[F

    invoke-virtual {v2, v12, v13, v3}, La/e/a/a/a/b;->a(D[F)V

    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget-object v2, v2, Landroidx/constraintlayout/motion/widget/w;->p:Ljava/util/LinkedHashMap;

    iget-object v3, v0, Landroidx/constraintlayout/motion/widget/p;->u:[Ljava/lang/String;

    add-int/lit8 v4, v1, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroidx/constraintlayout/widget/b;

    iget-object v3, v0, Landroidx/constraintlayout/motion/widget/p;->x:[F

    invoke-static {v2, v11, v3}, La/e/b/a/b;->a(Landroidx/constraintlayout/widget/b;Landroid/view/View;[F)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_f
    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->i:Landroidx/constraintlayout/motion/widget/n;

    iget v2, v1, Landroidx/constraintlayout/motion/widget/n;->c:I

    if-nez v2, :cond_12

    const/4 v2, 0x0

    cmpg-float v2, v14, v2

    if-gtz v2, :cond_10

    iget v1, v1, Landroidx/constraintlayout/motion/widget/n;->d:I

    invoke-virtual {v11, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_8

    :cond_10
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v14, v2

    if-ltz v2, :cond_11

    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->j:Landroidx/constraintlayout/motion/widget/n;

    iget v1, v1, Landroidx/constraintlayout/motion/widget/n;->d:I

    invoke-virtual {v11, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_8

    :cond_11
    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->j:Landroidx/constraintlayout/motion/widget/n;

    iget v2, v2, Landroidx/constraintlayout/motion/widget/n;->d:I

    iget v1, v1, Landroidx/constraintlayout/motion/widget/n;->d:I

    if-eq v2, v1, :cond_12

    invoke-virtual {v11, v15}, Landroid/view/View;->setVisibility(I)V

    :cond_12
    :goto_8
    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->E:[Landroidx/constraintlayout/motion/widget/m;

    if-eqz v1, :cond_16

    move v1, v15

    :goto_9
    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->E:[Landroidx/constraintlayout/motion/widget/m;

    array-length v3, v2

    if-ge v1, v3, :cond_16

    aget-object v2, v2, v1

    invoke-virtual {v2, v14, v11}, Landroidx/constraintlayout/motion/widget/m;->a(FLandroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    :cond_13
    move/from16 v19, v10

    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget v2, v1, Landroidx/constraintlayout/motion/widget/w;->f:F

    iget-object v3, v0, Landroidx/constraintlayout/motion/widget/p;->h:Landroidx/constraintlayout/motion/widget/w;

    iget v4, v3, Landroidx/constraintlayout/motion/widget/w;->f:F

    sub-float/2addr v4, v2

    mul-float/2addr v4, v14

    add-float/2addr v2, v4

    iget v4, v1, Landroidx/constraintlayout/motion/widget/w;->g:F

    iget v5, v3, Landroidx/constraintlayout/motion/widget/w;->g:F

    sub-float/2addr v5, v4

    mul-float/2addr v5, v14

    add-float/2addr v4, v5

    iget v5, v1, Landroidx/constraintlayout/motion/widget/w;->h:F

    iget v6, v3, Landroidx/constraintlayout/motion/widget/w;->h:F

    sub-float v7, v6, v5

    mul-float/2addr v7, v14

    add-float/2addr v7, v5

    iget v1, v1, Landroidx/constraintlayout/motion/widget/w;->i:F

    iget v3, v3, Landroidx/constraintlayout/motion/widget/w;->i:F

    sub-float v8, v3, v1

    mul-float/2addr v8, v14

    add-float/2addr v8, v1

    const/high16 v9, 0x3f000000    # 0.5f

    add-float/2addr v2, v9

    float-to-int v10, v2

    add-float/2addr v4, v9

    float-to-int v9, v4

    add-float/2addr v2, v7

    float-to-int v2, v2

    add-float/2addr v4, v8

    float-to-int v4, v4

    sub-int v7, v2, v10

    sub-int v8, v4, v9

    cmpl-float v5, v6, v5

    if-nez v5, :cond_14

    cmpl-float v1, v3, v1

    if-nez v1, :cond_14

    iget-boolean v1, v0, Landroidx/constraintlayout/motion/widget/p;->d:Z

    if-eqz v1, :cond_15

    :cond_14
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v7, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v8, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v11, v3, v1}, Landroid/view/View;->measure(II)V

    iput-boolean v15, v0, Landroidx/constraintlayout/motion/widget/p;->d:Z

    :cond_15
    invoke-virtual {v11, v10, v9, v2, v4}, Landroid/view/View;->layout(IIII)V

    :cond_16
    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/p;->D:Ljava/util/HashMap;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_a
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_18

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La/e/b/a/d;

    instance-of v2, v1, La/e/b/a/d$d;

    if-eqz v2, :cond_17

    check-cast v1, La/e/b/a/d$d;

    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/p;->t:[D

    aget-wide v4, v2, v15

    aget-wide v6, v2, v19

    move-object/from16 v2, p1

    move v3, v14

    invoke-virtual/range {v1 .. v7}, La/e/b/a/d$d;->a(Landroid/view/View;FDD)V

    goto :goto_a

    :cond_17
    invoke-virtual {v1, v11, v14}, La/e/b/a/d;->a(Landroid/view/View;F)V

    goto :goto_a

    :cond_18
    return v16
.end method

.method public b()F
    .locals 1

    iget v0, p0, Landroidx/constraintlayout/motion/widget/p;->p:F

    return v0
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Landroidx/constraintlayout/motion/widget/p;->F:I

    return-void
.end method

.method b(Landroid/graphics/Rect;Landroidx/constraintlayout/widget/e;II)V
    .locals 7

    iget v6, p2, Landroidx/constraintlayout/widget/e;->g:I

    if-eqz v6, :cond_0

    iget-object v2, p0, Landroidx/constraintlayout/motion/widget/p;->a:Landroid/graphics/Rect;

    move-object v0, p0

    move-object v1, p1

    move v3, v6

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Landroidx/constraintlayout/motion/widget/p;->a(Landroid/graphics/Rect;Landroid/graphics/Rect;III)V

    :cond_0
    iget-object p3, p0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    const/4 p4, 0x0

    iput p4, p3, Landroidx/constraintlayout/motion/widget/w;->d:F

    iput p4, p3, Landroidx/constraintlayout/motion/widget/w;->e:F

    invoke-direct {p0, p3}, Landroidx/constraintlayout/motion/widget/p;->b(Landroidx/constraintlayout/motion/widget/w;)V

    iget-object p3, p0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget p4, p1, Landroid/graphics/Rect;->left:I

    int-to-float p4, p4

    iget v0, p1, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p3, p4, v0, v1, v2}, Landroidx/constraintlayout/motion/widget/w;->a(FFFF)V

    iget p3, p0, Landroidx/constraintlayout/motion/widget/p;->c:I

    invoke-virtual {p2, p3}, Landroidx/constraintlayout/widget/e;->c(I)Landroidx/constraintlayout/widget/e$a;

    move-result-object p3

    iget-object p4, p0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    invoke-virtual {p4, p3}, Landroidx/constraintlayout/motion/widget/w;->a(Landroidx/constraintlayout/widget/e$a;)V

    iget-object p4, p3, Landroidx/constraintlayout/widget/e$a;->d:Landroidx/constraintlayout/widget/e$c;

    iget p4, p4, Landroidx/constraintlayout/widget/e$c;->h:F

    iput p4, p0, Landroidx/constraintlayout/motion/widget/p;->m:F

    iget-object p4, p0, Landroidx/constraintlayout/motion/widget/p;->i:Landroidx/constraintlayout/motion/widget/n;

    iget v0, p0, Landroidx/constraintlayout/motion/widget/p;->c:I

    invoke-virtual {p4, p1, p2, v6, v0}, Landroidx/constraintlayout/motion/widget/n;->a(Landroid/graphics/Rect;Landroidx/constraintlayout/widget/e;II)V

    iget-object p1, p3, Landroidx/constraintlayout/widget/e$a;->f:Landroidx/constraintlayout/widget/e$e;

    iget p1, p1, Landroidx/constraintlayout/widget/e$e;->j:I

    iput p1, p0, Landroidx/constraintlayout/motion/widget/p;->G:I

    iget-object p1, p3, Landroidx/constraintlayout/widget/e$a;->d:Landroidx/constraintlayout/widget/e$c;

    iget p2, p1, Landroidx/constraintlayout/widget/e$c;->l:I

    iput p2, p0, Landroidx/constraintlayout/motion/widget/p;->I:I

    iget p1, p1, Landroidx/constraintlayout/widget/e$c;->k:F

    iput p1, p0, Landroidx/constraintlayout/motion/widget/p;->J:F

    iget-object p1, p0, Landroidx/constraintlayout/motion/widget/p;->b:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p3, Landroidx/constraintlayout/widget/e$a;->d:Landroidx/constraintlayout/widget/e$c;

    iget p3, p2, Landroidx/constraintlayout/widget/e$c;->n:I

    iget-object p4, p2, Landroidx/constraintlayout/widget/e$c;->m:Ljava/lang/String;

    iget p2, p2, Landroidx/constraintlayout/widget/e$c;->o:I

    invoke-static {p1, p3, p4, p2}, Landroidx/constraintlayout/motion/widget/p;->a(Landroid/content/Context;ILjava/lang/String;I)Landroid/view/animation/Interpolator;

    move-result-object p1

    iput-object p1, p0, Landroidx/constraintlayout/motion/widget/p;->K:Landroid/view/animation/Interpolator;

    return-void
.end method

.method b(Landroid/view/View;)V
    .locals 5

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    const/4 v1, 0x0

    iput v1, v0, Landroidx/constraintlayout/motion/widget/w;->d:F

    iput v1, v0, Landroidx/constraintlayout/motion/widget/w;->e:F

    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroidx/constraintlayout/motion/widget/w;->a(FFFF)V

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->i:Landroidx/constraintlayout/motion/widget/n;

    invoke-virtual {v0, p1}, Landroidx/constraintlayout/motion/widget/n;->b(Landroid/view/View;)V

    return-void
.end method

.method public c()F
    .locals 1

    iget v0, p0, Landroidx/constraintlayout/motion/widget/p;->q:F

    return v0
.end method

.method public c(Landroid/view/View;)V
    .locals 1

    iput-object p1, p0, Landroidx/constraintlayout/motion/widget/p;->b:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iput v0, p0, Landroidx/constraintlayout/motion/widget/p;->c:I

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    instance-of v0, p1, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    if-eqz v0, :cond_0

    check-cast p1, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    invoke-virtual {p1}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;->a()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Landroidx/constraintlayout/motion/widget/p;->e:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public d()I
    .locals 3

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget v0, v0, Landroidx/constraintlayout/motion/widget/w;->c:I

    iget-object v1, p0, Landroidx/constraintlayout/motion/widget/p;->y:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroidx/constraintlayout/motion/widget/w;

    iget v2, v2, Landroidx/constraintlayout/motion/widget/w;->c:I

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Landroidx/constraintlayout/motion/widget/p;->h:Landroidx/constraintlayout/motion/widget/w;

    iget v1, v1, Landroidx/constraintlayout/motion/widget/w;->c:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public e()F
    .locals 1

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->h:Landroidx/constraintlayout/motion/widget/w;

    iget v0, v0, Landroidx/constraintlayout/motion/widget/w;->f:F

    return v0
.end method

.method public f()F
    .locals 1

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->h:Landroidx/constraintlayout/motion/widget/w;

    iget v0, v0, Landroidx/constraintlayout/motion/widget/w;->g:F

    return v0
.end method

.method public g()F
    .locals 1

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget v0, v0, Landroidx/constraintlayout/motion/widget/w;->f:F

    return v0
.end method

.method public h()F
    .locals 1

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget v0, v0, Landroidx/constraintlayout/motion/widget/w;->g:F

    return v0
.end method

.method public i()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/p;->b:Landroid/view/View;

    return-object v0
.end method

.method public j()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroidx/constraintlayout/motion/widget/p;->d:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " start: x: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget v1, v1, Landroidx/constraintlayout/motion/widget/w;->f:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, " y: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Landroidx/constraintlayout/motion/widget/p;->g:Landroidx/constraintlayout/motion/widget/w;

    iget v2, v2, Landroidx/constraintlayout/motion/widget/w;->g:F

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v2, " end: x: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Landroidx/constraintlayout/motion/widget/p;->h:Landroidx/constraintlayout/motion/widget/w;

    iget v2, v2, Landroidx/constraintlayout/motion/widget/w;->f:F

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Landroidx/constraintlayout/motion/widget/p;->h:Landroidx/constraintlayout/motion/widget/w;

    iget v1, v1, Landroidx/constraintlayout/motion/widget/w;->g:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
