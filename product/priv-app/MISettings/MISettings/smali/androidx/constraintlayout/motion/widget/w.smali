.class Landroidx/constraintlayout/motion/widget/w;
.super Ljava/lang/Object;
.source "MotionPaths.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Landroidx/constraintlayout/motion/widget/w;",
        ">;"
    }
.end annotation


# static fields
.field static a:[Ljava/lang/String;


# instance fields
.field b:La/e/a/a/a/c;

.field c:I

.field d:F

.field e:F

.field f:F

.field g:F

.field h:F

.field i:F

.field j:F

.field k:F

.field l:I

.field m:I

.field n:F

.field o:Landroidx/constraintlayout/motion/widget/p;

.field p:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Landroidx/constraintlayout/widget/b;",
            ">;"
        }
    .end annotation
.end field

.field q:I

.field r:I

.field s:[D

.field t:[D


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "position"

    const-string v1, "x"

    const-string v2, "y"

    const-string v3, "width"

    const-string v4, "height"

    const-string v5, "pathRotate"

    filled-new-array/range {v0 .. v5}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroidx/constraintlayout/motion/widget/w;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Landroidx/constraintlayout/motion/widget/w;->c:I

    const/high16 v1, 0x7fc00000    # Float.NaN

    iput v1, p0, Landroidx/constraintlayout/motion/widget/w;->j:F

    iput v1, p0, Landroidx/constraintlayout/motion/widget/w;->k:F

    sget v2, Landroidx/constraintlayout/motion/widget/f;->a:I

    iput v2, p0, Landroidx/constraintlayout/motion/widget/w;->l:I

    iput v2, p0, Landroidx/constraintlayout/motion/widget/w;->m:I

    iput v1, p0, Landroidx/constraintlayout/motion/widget/w;->n:F

    const/4 v1, 0x0

    iput-object v1, p0, Landroidx/constraintlayout/motion/widget/w;->o:Landroidx/constraintlayout/motion/widget/p;

    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v1, p0, Landroidx/constraintlayout/motion/widget/w;->p:Ljava/util/LinkedHashMap;

    iput v0, p0, Landroidx/constraintlayout/motion/widget/w;->q:I

    const/16 v0, 0x12

    new-array v1, v0, [D

    iput-object v1, p0, Landroidx/constraintlayout/motion/widget/w;->s:[D

    new-array v0, v0, [D

    iput-object v0, p0, Landroidx/constraintlayout/motion/widget/w;->t:[D

    return-void
.end method

.method public constructor <init>(IILandroidx/constraintlayout/motion/widget/j;Landroidx/constraintlayout/motion/widget/w;Landroidx/constraintlayout/motion/widget/w;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Landroidx/constraintlayout/motion/widget/w;->c:I

    const/high16 v1, 0x7fc00000    # Float.NaN

    iput v1, p0, Landroidx/constraintlayout/motion/widget/w;->j:F

    iput v1, p0, Landroidx/constraintlayout/motion/widget/w;->k:F

    sget v2, Landroidx/constraintlayout/motion/widget/f;->a:I

    iput v2, p0, Landroidx/constraintlayout/motion/widget/w;->l:I

    iput v2, p0, Landroidx/constraintlayout/motion/widget/w;->m:I

    iput v1, p0, Landroidx/constraintlayout/motion/widget/w;->n:F

    const/4 v1, 0x0

    iput-object v1, p0, Landroidx/constraintlayout/motion/widget/w;->o:Landroidx/constraintlayout/motion/widget/p;

    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v1, p0, Landroidx/constraintlayout/motion/widget/w;->p:Ljava/util/LinkedHashMap;

    iput v0, p0, Landroidx/constraintlayout/motion/widget/w;->q:I

    const/16 v0, 0x12

    new-array v1, v0, [D

    iput-object v1, p0, Landroidx/constraintlayout/motion/widget/w;->s:[D

    new-array v0, v0, [D

    iput-object v0, p0, Landroidx/constraintlayout/motion/widget/w;->t:[D

    iget v0, p4, Landroidx/constraintlayout/motion/widget/w;->m:I

    sget v1, Landroidx/constraintlayout/motion/widget/f;->a:I

    if-eq v0, v1, :cond_0

    invoke-virtual/range {p0 .. p5}, Landroidx/constraintlayout/motion/widget/w;->a(IILandroidx/constraintlayout/motion/widget/j;Landroidx/constraintlayout/motion/widget/w;Landroidx/constraintlayout/motion/widget/w;)V

    return-void

    :cond_0
    iget v0, p3, Landroidx/constraintlayout/motion/widget/j;->q:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    invoke-virtual {p0, p3, p4, p5}, Landroidx/constraintlayout/motion/widget/w;->a(Landroidx/constraintlayout/motion/widget/j;Landroidx/constraintlayout/motion/widget/w;Landroidx/constraintlayout/motion/widget/w;)V

    return-void

    :cond_1
    invoke-virtual/range {p0 .. p5}, Landroidx/constraintlayout/motion/widget/w;->b(IILandroidx/constraintlayout/motion/widget/j;Landroidx/constraintlayout/motion/widget/w;Landroidx/constraintlayout/motion/widget/w;)V

    return-void

    :cond_2
    invoke-virtual {p0, p3, p4, p5}, Landroidx/constraintlayout/motion/widget/w;->b(Landroidx/constraintlayout/motion/widget/j;Landroidx/constraintlayout/motion/widget/w;Landroidx/constraintlayout/motion/widget/w;)V

    return-void
.end method

.method private a(FF)Z
    .locals 3

    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_2

    invoke-static {p2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    sub-float/2addr p1, p2

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    const p2, 0x358637bd    # 1.0E-6f

    cmpl-float p1, p1, p2

    if-lez p1, :cond_1

    goto :goto_0

    :cond_1
    move v1, v2

    :goto_0
    return v1

    :cond_2
    :goto_1
    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    move-result p1

    invoke-static {p2}, Ljava/lang/Float;->isNaN(F)Z

    move-result p2

    if-eq p1, p2, :cond_3

    goto :goto_2

    :cond_3
    move v1, v2

    :goto_2
    return v1
.end method


# virtual methods
.method public a(Landroidx/constraintlayout/motion/widget/w;)I
    .locals 1
    .param p1    # Landroidx/constraintlayout/motion/widget/w;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iget v0, p0, Landroidx/constraintlayout/motion/widget/w;->e:F

    iget p1, p1, Landroidx/constraintlayout/motion/widget/w;->e:F

    invoke-static {v0, p1}, Ljava/lang/Float;->compare(FF)I

    move-result p1

    return p1
.end method

.method a(Ljava/lang/String;)I
    .locals 1

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/w;->p:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroidx/constraintlayout/widget/b;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    invoke-virtual {p1}, Landroidx/constraintlayout/widget/b;->e()I

    move-result p1

    return p1
.end method

.method a(Ljava/lang/String;[DI)I
    .locals 5

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/w;->p:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroidx/constraintlayout/widget/b;

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    invoke-virtual {p1}, Landroidx/constraintlayout/widget/b;->e()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    invoke-virtual {p1}, Landroidx/constraintlayout/widget/b;->c()F

    move-result p1

    float-to-double v0, p1

    aput-wide v0, p2, p3

    return v2

    :cond_1
    invoke-virtual {p1}, Landroidx/constraintlayout/widget/b;->e()I

    move-result v1

    new-array v2, v1, [F

    invoke-virtual {p1, v2}, Landroidx/constraintlayout/widget/b;->a([F)V

    :goto_0
    if-ge v0, v1, :cond_2

    add-int/lit8 p1, p3, 0x1

    aget v3, v2, v0

    float-to-double v3, v3

    aput-wide v3, p2, p3

    add-int/lit8 v0, v0, 0x1

    move p3, p1

    goto :goto_0

    :cond_2
    return v1
.end method

.method a(D[I[D[FI)V
    .locals 15

    move-object v0, p0

    move-object/from16 v1, p3

    iget v2, v0, Landroidx/constraintlayout/motion/widget/w;->f:F

    iget v3, v0, Landroidx/constraintlayout/motion/widget/w;->g:F

    iget v4, v0, Landroidx/constraintlayout/motion/widget/w;->h:F

    iget v5, v0, Landroidx/constraintlayout/motion/widget/w;->i:F

    const/4 v6, 0x0

    move v7, v5

    move v5, v4

    move v4, v3

    move v3, v2

    move v2, v6

    :goto_0
    array-length v8, v1

    const/4 v9, 0x2

    const/4 v10, 0x1

    if-ge v2, v8, :cond_4

    aget-wide v11, p4, v2

    double-to-float v8, v11

    aget v11, v1, v2

    if-eq v11, v10, :cond_3

    if-eq v11, v9, :cond_2

    const/4 v9, 0x3

    if-eq v11, v9, :cond_1

    const/4 v9, 0x4

    if-eq v11, v9, :cond_0

    goto :goto_1

    :cond_0
    move v7, v8

    goto :goto_1

    :cond_1
    move v5, v8

    goto :goto_1

    :cond_2
    move v4, v8

    goto :goto_1

    :cond_3
    move v3, v8

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    iget-object v1, v0, Landroidx/constraintlayout/motion/widget/w;->o:Landroidx/constraintlayout/motion/widget/p;

    const/high16 v2, 0x40000000    # 2.0f

    if-eqz v1, :cond_5

    new-array v8, v9, [F

    new-array v9, v9, [F

    move-wide/from16 v11, p1

    invoke-virtual {v1, v11, v12, v8, v9}, Landroidx/constraintlayout/motion/widget/p;->a(D[F[F)V

    aget v1, v8, v6

    aget v6, v8, v10

    float-to-double v8, v1

    float-to-double v11, v3

    float-to-double v3, v4

    invoke-static {v3, v4}, Ljava/lang/Math;->sin(D)D

    move-result-wide v13

    mul-double/2addr v13, v11

    add-double/2addr v8, v13

    div-float v1, v5, v2

    float-to-double v13, v1

    sub-double/2addr v8, v13

    double-to-float v1, v8

    float-to-double v8, v6

    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    mul-double/2addr v11, v3

    sub-double/2addr v8, v11

    div-float v3, v7, v2

    float-to-double v3, v3

    sub-double/2addr v8, v3

    double-to-float v4, v8

    goto :goto_2

    :cond_5
    move v1, v3

    :goto_2
    div-float/2addr v5, v2

    add-float/2addr v1, v5

    const/4 v3, 0x0

    add-float/2addr v1, v3

    aput v1, p5, p6

    add-int/lit8 v1, p6, 0x1

    div-float/2addr v7, v2

    add-float/2addr v4, v7

    add-float/2addr v4, v3

    aput v4, p5, v1

    return-void
.end method

.method a(D[I[D[F[D[F)V
    .locals 19

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    iget v2, v0, Landroidx/constraintlayout/motion/widget/w;->f:F

    iget v3, v0, Landroidx/constraintlayout/motion/widget/w;->g:F

    iget v4, v0, Landroidx/constraintlayout/motion/widget/w;->h:F

    iget v5, v0, Landroidx/constraintlayout/motion/widget/w;->i:F

    move v9, v2

    move v10, v3

    move v11, v4

    move v12, v5

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    :goto_0
    array-length v13, v1

    const/4 v15, 0x1

    if-ge v2, v13, :cond_4

    aget-wide v6, p4, v2

    double-to-float v6, v6

    aget-wide v13, p6, v2

    double-to-float v13, v13

    aget v14, v1, v2

    if-eq v14, v15, :cond_3

    const/4 v7, 0x2

    if-eq v14, v7, :cond_2

    const/4 v7, 0x3

    if-eq v14, v7, :cond_1

    const/4 v7, 0x4

    if-eq v14, v7, :cond_0

    goto :goto_1

    :cond_0
    move v12, v6

    move v8, v13

    goto :goto_1

    :cond_1
    move v11, v6

    move v4, v13

    goto :goto_1

    :cond_2
    move v10, v6

    move v5, v13

    goto :goto_1

    :cond_3
    move v9, v6

    move v3, v13

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v4, v1

    add-float/2addr v4, v3

    div-float/2addr v8, v1

    add-float/2addr v8, v5

    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/w;->o:Landroidx/constraintlayout/motion/widget/p;

    if-eqz v2, :cond_5

    const/4 v6, 0x2

    new-array v4, v6, [F

    new-array v6, v6, [F

    move-wide/from16 v7, p1

    invoke-virtual {v2, v7, v8, v4, v6}, Landroidx/constraintlayout/motion/widget/p;->a(D[F[F)V

    const/4 v2, 0x0

    aget v7, v4, v2

    aget v4, v4, v15

    aget v8, v6, v2

    aget v2, v6, v15

    float-to-double v6, v7

    float-to-double v13, v9

    float-to-double v9, v10

    invoke-static {v9, v10}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    mul-double v16, v16, v13

    add-double v6, v6, v16

    div-float v15, v11, v1

    move/from16 p1, v2

    float-to-double v1, v15

    sub-double/2addr v6, v1

    double-to-float v1, v6

    float-to-double v6, v4

    invoke-static {v9, v10}, Ljava/lang/Math;->cos(D)D

    move-result-wide v17

    mul-double v13, v13, v17

    sub-double/2addr v6, v13

    const/high16 v2, 0x40000000    # 2.0f

    div-float v4, v12, v2

    float-to-double v13, v4

    sub-double/2addr v6, v13

    double-to-float v2, v6

    float-to-double v6, v8

    float-to-double v3, v3

    invoke-static {v9, v10}, Ljava/lang/Math;->sin(D)D

    move-result-wide v13

    mul-double/2addr v13, v3

    add-double/2addr v6, v13

    invoke-static {v9, v10}, Ljava/lang/Math;->cos(D)D

    move-result-wide v13

    move v8, v1

    float-to-double v0, v5

    mul-double/2addr v13, v0

    add-double/2addr v6, v13

    double-to-float v5, v6

    move/from16 v6, p1

    float-to-double v6, v6

    invoke-static {v9, v10}, Ljava/lang/Math;->cos(D)D

    move-result-wide v13

    mul-double/2addr v3, v13

    sub-double/2addr v6, v3

    invoke-static {v9, v10}, Ljava/lang/Math;->sin(D)D

    move-result-wide v3

    mul-double/2addr v3, v0

    add-double/2addr v6, v3

    double-to-float v0, v6

    move v10, v2

    move v4, v5

    const/high16 v1, 0x40000000    # 2.0f

    goto :goto_2

    :cond_5
    move v0, v8

    move v8, v9

    :goto_2
    div-float/2addr v11, v1

    add-float/2addr v8, v11

    const/4 v2, 0x0

    add-float/2addr v8, v2

    const/4 v3, 0x0

    aput v8, p5, v3

    div-float/2addr v12, v1

    add-float/2addr v10, v12

    add-float/2addr v10, v2

    const/4 v1, 0x1

    aput v10, p5, v1

    aput v4, p7, v3

    aput v0, p7, v1

    return-void
.end method

.method a(FFFF)V
    .locals 0

    iput p1, p0, Landroidx/constraintlayout/motion/widget/w;->f:F

    iput p2, p0, Landroidx/constraintlayout/motion/widget/w;->g:F

    iput p3, p0, Landroidx/constraintlayout/motion/widget/w;->h:F

    iput p4, p0, Landroidx/constraintlayout/motion/widget/w;->i:F

    return-void
.end method

.method a(FF[F[I[D[D)V
    .locals 12

    move-object/from16 v0, p4

    const/4 v1, 0x0

    const/4 v2, 0x0

    move v3, v1

    move v4, v2

    move v5, v4

    move v6, v5

    move v7, v6

    :goto_0
    array-length v8, v0

    const/4 v9, 0x1

    if-ge v3, v8, :cond_5

    aget-wide v10, p5, v3

    double-to-float v8, v10

    aget-wide v10, p6, v3

    aget v10, v0, v3

    if-eqz v10, :cond_4

    if-eq v10, v9, :cond_3

    const/4 v9, 0x2

    if-eq v10, v9, :cond_2

    const/4 v9, 0x3

    if-eq v10, v9, :cond_1

    const/4 v9, 0x4

    if-eq v10, v9, :cond_0

    goto :goto_1

    :cond_0
    move v7, v8

    goto :goto_1

    :cond_1
    move v5, v8

    goto :goto_1

    :cond_2
    move v6, v8

    goto :goto_1

    :cond_3
    move v4, v8

    :cond_4
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_5
    mul-float v0, v2, v5

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v0, v3

    sub-float/2addr v4, v0

    mul-float v0, v2, v7

    div-float/2addr v0, v3

    sub-float/2addr v6, v0

    const/high16 v0, 0x3f800000    # 1.0f

    mul-float/2addr v5, v0

    mul-float/2addr v7, v0

    add-float/2addr v5, v4

    add-float/2addr v7, v6

    sub-float v3, v0, p1

    mul-float/2addr v4, v3

    mul-float/2addr v5, p1

    add-float/2addr v4, v5

    add-float/2addr v4, v2

    aput v4, p3, v1

    sub-float/2addr v0, p2

    mul-float/2addr v6, v0

    mul-float/2addr v7, p2

    add-float/2addr v6, v7

    add-float/2addr v6, v2

    aput v6, p3, v9

    return-void
.end method

.method a(FLandroid/view/View;[I[D[D[DZ)V
    .locals 21

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p5

    iget v4, v0, Landroidx/constraintlayout/motion/widget/w;->f:F

    iget v5, v0, Landroidx/constraintlayout/motion/widget/w;->g:F

    iget v6, v0, Landroidx/constraintlayout/motion/widget/w;->h:F

    iget v7, v0, Landroidx/constraintlayout/motion/widget/w;->i:F

    array-length v8, v2

    const/4 v9, 0x1

    if-eqz v8, :cond_0

    iget-object v8, v0, Landroidx/constraintlayout/motion/widget/w;->s:[D

    array-length v8, v8

    array-length v10, v2

    sub-int/2addr v10, v9

    aget v10, v2, v10

    if-gt v8, v10, :cond_0

    array-length v8, v2

    sub-int/2addr v8, v9

    aget v8, v2, v8

    add-int/2addr v8, v9

    new-array v10, v8, [D

    iput-object v10, v0, Landroidx/constraintlayout/motion/widget/w;->s:[D

    new-array v8, v8, [D

    iput-object v8, v0, Landroidx/constraintlayout/motion/widget/w;->t:[D

    :cond_0
    iget-object v8, v0, Landroidx/constraintlayout/motion/widget/w;->s:[D

    const-wide/high16 v10, 0x7ff8000000000000L    # Double.NaN

    invoke-static {v8, v10, v11}, Ljava/util/Arrays;->fill([DD)V

    const/4 v10, 0x0

    :goto_0
    array-length v11, v2

    if-ge v10, v11, :cond_1

    iget-object v11, v0, Landroidx/constraintlayout/motion/widget/w;->s:[D

    aget v12, v2, v10

    aget-wide v13, p4, v10

    aput-wide v13, v11, v12

    iget-object v11, v0, Landroidx/constraintlayout/motion/widget/w;->t:[D

    aget v12, v2, v10

    aget-wide v13, v3, v10

    aput-wide v13, v11, v12

    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    :cond_1
    const/high16 v10, 0x7fc00000    # Float.NaN

    move v12, v6

    move v13, v7

    const/4 v7, 0x0

    const/4 v11, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move v6, v5

    move v5, v4

    const/4 v4, 0x0

    :goto_1
    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/w;->s:[D

    array-length v8, v2

    if-ge v4, v8, :cond_c

    aget-wide v17, v2, v4

    invoke-static/range {v17 .. v18}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    const-wide/16 v17, 0x0

    if-eqz v2, :cond_3

    if-eqz p6, :cond_2

    aget-wide v19, p6, v4

    cmpl-double v2, v19, v17

    if-nez v2, :cond_3

    :cond_2
    move/from16 p4, v10

    goto :goto_3

    :cond_3
    if-eqz p6, :cond_4

    aget-wide v17, p6, v4

    :cond_4
    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/w;->s:[D

    aget-wide v19, v2, v4

    invoke-static/range {v19 .. v20}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-eqz v2, :cond_5

    goto :goto_2

    :cond_5
    iget-object v2, v0, Landroidx/constraintlayout/motion/widget/w;->s:[D

    aget-wide v19, v2, v4

    add-double v17, v19, v17

    :goto_2
    move/from16 p4, v10

    move-wide/from16 v9, v17

    double-to-float v8, v9

    iget-object v9, v0, Landroidx/constraintlayout/motion/widget/w;->t:[D

    aget-wide v2, v9, v4

    double-to-float v2, v2

    if-eqz v4, :cond_b

    const/4 v3, 0x1

    if-eq v4, v3, :cond_a

    const/4 v3, 0x2

    if-eq v4, v3, :cond_9

    const/4 v3, 0x3

    if-eq v4, v3, :cond_8

    const/4 v3, 0x4

    if-eq v4, v3, :cond_7

    const/4 v2, 0x5

    if-eq v4, v2, :cond_6

    goto :goto_3

    :cond_6
    move v10, v8

    goto :goto_4

    :cond_7
    move/from16 v10, p4

    move v15, v2

    move v13, v8

    goto :goto_4

    :cond_8
    move/from16 v10, p4

    move v14, v2

    move v12, v8

    goto :goto_4

    :cond_9
    move/from16 v10, p4

    move v11, v2

    move v6, v8

    goto :goto_4

    :cond_a
    move/from16 v10, p4

    move v7, v2

    move v5, v8

    goto :goto_4

    :cond_b
    :goto_3
    move/from16 v10, p4

    :goto_4
    add-int/lit8 v4, v4, 0x1

    move-object/from16 v3, p5

    const/4 v9, 0x1

    goto :goto_1

    :cond_c
    move/from16 p4, v10

    iget-object v3, v0, Landroidx/constraintlayout/motion/widget/w;->o:Landroidx/constraintlayout/motion/widget/p;

    const/high16 v4, 0x40000000    # 2.0f

    if-eqz v3, :cond_f

    const/4 v2, 0x2

    new-array v8, v2, [F

    new-array v9, v2, [F

    move/from16 v10, p1

    float-to-double v14, v10

    invoke-virtual {v3, v14, v15, v8, v9}, Landroidx/constraintlayout/motion/widget/p;->a(D[F[F)V

    const/4 v3, 0x0

    aget v10, v8, v3

    const/4 v14, 0x1

    aget v8, v8, v14

    aget v15, v9, v3

    aget v3, v9, v14

    float-to-double v9, v10

    move/from16 p1, v3

    float-to-double v2, v5

    float-to-double v5, v6

    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    move-result-wide v17

    mul-double v17, v17, v2

    add-double v9, v9, v17

    div-float v14, v12, v4

    move-wide/from16 v17, v5

    float-to-double v4, v14

    sub-double/2addr v9, v4

    double-to-float v5, v9

    float-to-double v8, v8

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->cos(D)D

    move-result-wide v19

    mul-double v19, v19, v2

    sub-double v8, v8, v19

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v13, v4

    move v6, v5

    float-to-double v4, v4

    sub-double/2addr v8, v4

    double-to-float v4, v8

    float-to-double v8, v15

    float-to-double v14, v7

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->sin(D)D

    move-result-wide v19

    mul-double v19, v19, v14

    add-double v8, v8, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->cos(D)D

    move-result-wide v19

    mul-double v19, v19, v2

    float-to-double v10, v11

    mul-double v19, v19, v10

    add-double v8, v8, v19

    double-to-float v5, v8

    move/from16 v7, p1

    float-to-double v7, v7

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->cos(D)D

    move-result-wide v19

    mul-double v14, v14, v19

    sub-double/2addr v7, v14

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    mul-double/2addr v2, v14

    mul-double/2addr v2, v10

    add-double/2addr v7, v2

    double-to-float v2, v7

    move-object/from16 v3, p5

    array-length v7, v3

    const/4 v8, 0x2

    if-lt v7, v8, :cond_d

    float-to-double v7, v5

    const/4 v9, 0x0

    aput-wide v7, v3, v9

    float-to-double v7, v2

    const/4 v10, 0x1

    aput-wide v7, v3, v10

    goto :goto_5

    :cond_d
    const/4 v9, 0x0

    const/4 v10, 0x1

    :goto_5
    invoke-static/range {p4 .. p4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v3

    if-nez v3, :cond_e

    move/from16 v3, p4

    float-to-double v7, v3

    float-to-double v2, v2

    float-to-double v14, v5

    invoke-static {v2, v3, v14, v15}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v2

    add-double/2addr v7, v2

    double-to-float v2, v7

    invoke-virtual {v1, v2}, Landroid/view/View;->setRotation(F)V

    :cond_e
    move v5, v6

    goto :goto_6

    :cond_f
    move/from16 v3, p4

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-static {v3}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_10

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v14, v2

    add-float/2addr v7, v14

    div-float/2addr v15, v2

    add-float/2addr v11, v15

    const/4 v2, 0x0

    float-to-double v14, v2

    float-to-double v2, v3

    float-to-double v9, v11

    float-to-double v7, v7

    invoke-static {v9, v10, v7, v8}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v7

    add-double/2addr v2, v7

    add-double/2addr v14, v2

    double-to-float v2, v14

    invoke-virtual {v1, v2}, Landroid/view/View;->setRotation(F)V

    :cond_10
    move v4, v6

    :goto_6
    instance-of v2, v1, Landroidx/constraintlayout/motion/widget/e;

    if-eqz v2, :cond_11

    add-float/2addr v12, v5

    add-float/2addr v13, v4

    check-cast v1, Landroidx/constraintlayout/motion/widget/e;

    invoke-interface {v1, v5, v4, v12, v13}, Landroidx/constraintlayout/motion/widget/e;->a(FFFF)V

    return-void

    :cond_11
    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v5, v2

    float-to-int v3, v5

    add-float/2addr v4, v2

    float-to-int v2, v4

    add-float/2addr v5, v12

    float-to-int v5, v5

    add-float/2addr v4, v13

    float-to-int v4, v4

    sub-int v6, v5, v3

    sub-int v7, v4, v2

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    if-ne v6, v8, :cond_13

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    if-eq v7, v8, :cond_12

    goto :goto_7

    :cond_12
    const/16 v16, 0x0

    goto :goto_8

    :cond_13
    :goto_7
    const/16 v16, 0x1

    :goto_8
    if-nez v16, :cond_14

    if-eqz p7, :cond_15

    :cond_14
    const/high16 v8, 0x40000000    # 2.0f

    invoke-static {v6, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v7, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v1, v6, v7}, Landroid/view/View;->measure(II)V

    :cond_15
    invoke-virtual {v1, v3, v2, v5, v4}, Landroid/view/View;->layout(IIII)V

    return-void
.end method

.method a(IILandroidx/constraintlayout/motion/widget/j;Landroidx/constraintlayout/motion/widget/w;Landroidx/constraintlayout/motion/widget/w;)V
    .locals 6

    iget p1, p3, Landroidx/constraintlayout/motion/widget/f;->b:I

    int-to-float p1, p1

    const/high16 p2, 0x42c80000    # 100.0f

    div-float/2addr p1, p2

    iput p1, p0, Landroidx/constraintlayout/motion/widget/w;->d:F

    iget p2, p3, Landroidx/constraintlayout/motion/widget/j;->j:I

    iput p2, p0, Landroidx/constraintlayout/motion/widget/w;->c:I

    iget p2, p3, Landroidx/constraintlayout/motion/widget/j;->q:I

    iput p2, p0, Landroidx/constraintlayout/motion/widget/w;->q:I

    iget p2, p3, Landroidx/constraintlayout/motion/widget/j;->k:F

    invoke-static {p2}, Ljava/lang/Float;->isNaN(F)Z

    move-result p2

    if-eqz p2, :cond_0

    move p2, p1

    goto :goto_0

    :cond_0
    iget p2, p3, Landroidx/constraintlayout/motion/widget/j;->k:F

    :goto_0
    iget v0, p3, Landroidx/constraintlayout/motion/widget/j;->l:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, p1

    goto :goto_1

    :cond_1
    iget v0, p3, Landroidx/constraintlayout/motion/widget/j;->l:F

    :goto_1
    iget v1, p5, Landroidx/constraintlayout/motion/widget/w;->h:F

    iget v2, p4, Landroidx/constraintlayout/motion/widget/w;->h:F

    sub-float/2addr v1, v2

    iget v3, p5, Landroidx/constraintlayout/motion/widget/w;->i:F

    iget v4, p4, Landroidx/constraintlayout/motion/widget/w;->i:F

    sub-float/2addr v3, v4

    iget v5, p0, Landroidx/constraintlayout/motion/widget/w;->d:F

    iput v5, p0, Landroidx/constraintlayout/motion/widget/w;->e:F

    mul-float/2addr v1, p2

    add-float/2addr v2, v1

    float-to-int v1, v2

    int-to-float v1, v1

    iput v1, p0, Landroidx/constraintlayout/motion/widget/w;->h:F

    mul-float/2addr v3, v0

    add-float/2addr v4, v3

    float-to-int v1, v4

    int-to-float v1, v1

    iput v1, p0, Landroidx/constraintlayout/motion/widget/w;->i:F

    iget v1, p3, Landroidx/constraintlayout/motion/widget/j;->q:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_7

    const/4 v2, 0x2

    if-eq v1, v2, :cond_4

    iget p2, p3, Landroidx/constraintlayout/motion/widget/j;->m:F

    invoke-static {p2}, Ljava/lang/Float;->isNaN(F)Z

    move-result p2

    if-eqz p2, :cond_2

    move p2, p1

    goto :goto_2

    :cond_2
    iget p2, p3, Landroidx/constraintlayout/motion/widget/j;->m:F

    :goto_2
    iget v0, p5, Landroidx/constraintlayout/motion/widget/w;->f:F

    iget v1, p4, Landroidx/constraintlayout/motion/widget/w;->f:F

    sub-float/2addr v0, v1

    mul-float/2addr p2, v0

    add-float/2addr p2, v1

    iput p2, p0, Landroidx/constraintlayout/motion/widget/w;->f:F

    iget p2, p3, Landroidx/constraintlayout/motion/widget/j;->n:F

    invoke-static {p2}, Ljava/lang/Float;->isNaN(F)Z

    move-result p2

    if-eqz p2, :cond_3

    goto :goto_3

    :cond_3
    iget p1, p3, Landroidx/constraintlayout/motion/widget/j;->n:F

    :goto_3
    iget p2, p5, Landroidx/constraintlayout/motion/widget/w;->g:F

    iget p5, p4, Landroidx/constraintlayout/motion/widget/w;->g:F

    sub-float/2addr p2, p5

    mul-float/2addr p1, p2

    add-float/2addr p1, p5

    iput p1, p0, Landroidx/constraintlayout/motion/widget/w;->g:F

    goto :goto_8

    :cond_4
    iget v1, p3, Landroidx/constraintlayout/motion/widget/j;->m:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-eqz v1, :cond_5

    iget p2, p5, Landroidx/constraintlayout/motion/widget/w;->f:F

    iget v0, p4, Landroidx/constraintlayout/motion/widget/w;->f:F

    sub-float/2addr p2, v0

    mul-float/2addr p2, p1

    add-float/2addr p2, v0

    goto :goto_4

    :cond_5
    iget v1, p3, Landroidx/constraintlayout/motion/widget/j;->m:F

    invoke-static {v0, p2}, Ljava/lang/Math;->min(FF)F

    move-result p2

    mul-float/2addr p2, v1

    :goto_4
    iput p2, p0, Landroidx/constraintlayout/motion/widget/w;->f:F

    iget p2, p3, Landroidx/constraintlayout/motion/widget/j;->n:F

    invoke-static {p2}, Ljava/lang/Float;->isNaN(F)Z

    move-result p2

    if-eqz p2, :cond_6

    iget p2, p5, Landroidx/constraintlayout/motion/widget/w;->g:F

    iget p5, p4, Landroidx/constraintlayout/motion/widget/w;->g:F

    sub-float/2addr p2, p5

    mul-float/2addr p1, p2

    add-float/2addr p1, p5

    goto :goto_5

    :cond_6
    iget p1, p3, Landroidx/constraintlayout/motion/widget/j;->n:F

    :goto_5
    iput p1, p0, Landroidx/constraintlayout/motion/widget/w;->g:F

    goto :goto_8

    :cond_7
    iget p2, p3, Landroidx/constraintlayout/motion/widget/j;->m:F

    invoke-static {p2}, Ljava/lang/Float;->isNaN(F)Z

    move-result p2

    if-eqz p2, :cond_8

    move p2, p1

    goto :goto_6

    :cond_8
    iget p2, p3, Landroidx/constraintlayout/motion/widget/j;->m:F

    :goto_6
    iget v0, p5, Landroidx/constraintlayout/motion/widget/w;->f:F

    iget v1, p4, Landroidx/constraintlayout/motion/widget/w;->f:F

    sub-float/2addr v0, v1

    mul-float/2addr p2, v0

    add-float/2addr p2, v1

    iput p2, p0, Landroidx/constraintlayout/motion/widget/w;->f:F

    iget p2, p3, Landroidx/constraintlayout/motion/widget/j;->n:F

    invoke-static {p2}, Ljava/lang/Float;->isNaN(F)Z

    move-result p2

    if-eqz p2, :cond_9

    goto :goto_7

    :cond_9
    iget p1, p3, Landroidx/constraintlayout/motion/widget/j;->n:F

    :goto_7
    iget p2, p5, Landroidx/constraintlayout/motion/widget/w;->g:F

    iget p5, p4, Landroidx/constraintlayout/motion/widget/w;->g:F

    sub-float/2addr p2, p5

    mul-float/2addr p1, p2

    add-float/2addr p1, p5

    iput p1, p0, Landroidx/constraintlayout/motion/widget/w;->g:F

    :goto_8
    iget p1, p4, Landroidx/constraintlayout/motion/widget/w;->m:I

    iput p1, p0, Landroidx/constraintlayout/motion/widget/w;->m:I

    iget-object p1, p3, Landroidx/constraintlayout/motion/widget/j;->h:Ljava/lang/String;

    invoke-static {p1}, La/e/a/a/a/c;->a(Ljava/lang/String;)La/e/a/a/a/c;

    move-result-object p1

    iput-object p1, p0, Landroidx/constraintlayout/motion/widget/w;->b:La/e/a/a/a/c;

    iget p1, p3, Landroidx/constraintlayout/motion/widget/j;->i:I

    iput p1, p0, Landroidx/constraintlayout/motion/widget/w;->l:I

    return-void
.end method

.method a(Landroidx/constraintlayout/motion/widget/j;Landroidx/constraintlayout/motion/widget/w;Landroidx/constraintlayout/motion/widget/w;)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    iget v4, v1, Landroidx/constraintlayout/motion/widget/f;->b:I

    int-to-float v4, v4

    const/high16 v5, 0x42c80000    # 100.0f

    div-float/2addr v4, v5

    iput v4, v0, Landroidx/constraintlayout/motion/widget/w;->d:F

    iget v5, v1, Landroidx/constraintlayout/motion/widget/j;->j:I

    iput v5, v0, Landroidx/constraintlayout/motion/widget/w;->c:I

    iget v5, v1, Landroidx/constraintlayout/motion/widget/j;->k:F

    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-eqz v5, :cond_0

    move v5, v4

    goto :goto_0

    :cond_0
    iget v5, v1, Landroidx/constraintlayout/motion/widget/j;->k:F

    :goto_0
    iget v6, v1, Landroidx/constraintlayout/motion/widget/j;->l:F

    invoke-static {v6}, Ljava/lang/Float;->isNaN(F)Z

    move-result v6

    if-eqz v6, :cond_1

    move v6, v4

    goto :goto_1

    :cond_1
    iget v6, v1, Landroidx/constraintlayout/motion/widget/j;->l:F

    :goto_1
    iget v7, v3, Landroidx/constraintlayout/motion/widget/w;->h:F

    iget v8, v2, Landroidx/constraintlayout/motion/widget/w;->h:F

    sub-float v9, v7, v8

    iget v10, v3, Landroidx/constraintlayout/motion/widget/w;->i:F

    iget v11, v2, Landroidx/constraintlayout/motion/widget/w;->i:F

    sub-float v12, v10, v11

    iget v13, v0, Landroidx/constraintlayout/motion/widget/w;->d:F

    iput v13, v0, Landroidx/constraintlayout/motion/widget/w;->e:F

    iget v13, v2, Landroidx/constraintlayout/motion/widget/w;->f:F

    const/high16 v14, 0x40000000    # 2.0f

    div-float v15, v8, v14

    add-float/2addr v15, v13

    iget v1, v2, Landroidx/constraintlayout/motion/widget/w;->g:F

    div-float v16, v11, v14

    add-float v16, v1, v16

    iget v2, v3, Landroidx/constraintlayout/motion/widget/w;->f:F

    div-float/2addr v7, v14

    add-float/2addr v2, v7

    iget v3, v3, Landroidx/constraintlayout/motion/widget/w;->g:F

    div-float/2addr v10, v14

    add-float/2addr v3, v10

    sub-float/2addr v2, v15

    sub-float v3, v3, v16

    mul-float v7, v2, v4

    add-float/2addr v13, v7

    mul-float/2addr v9, v5

    div-float v5, v9, v14

    sub-float/2addr v13, v5

    float-to-int v7, v13

    int-to-float v7, v7

    iput v7, v0, Landroidx/constraintlayout/motion/widget/w;->f:F

    mul-float v7, v3, v4

    add-float/2addr v1, v7

    mul-float/2addr v12, v6

    div-float v6, v12, v14

    sub-float/2addr v1, v6

    float-to-int v1, v1

    int-to-float v1, v1

    iput v1, v0, Landroidx/constraintlayout/motion/widget/w;->g:F

    add-float/2addr v8, v9

    float-to-int v1, v8

    int-to-float v1, v1

    iput v1, v0, Landroidx/constraintlayout/motion/widget/w;->h:F

    add-float/2addr v11, v12

    float-to-int v1, v11

    int-to-float v1, v1

    iput v1, v0, Landroidx/constraintlayout/motion/widget/w;->i:F

    move-object/from16 v1, p1

    iget v7, v1, Landroidx/constraintlayout/motion/widget/j;->m:F

    invoke-static {v7}, Ljava/lang/Float;->isNaN(F)Z

    move-result v7

    if-eqz v7, :cond_2

    move v7, v4

    goto :goto_2

    :cond_2
    iget v7, v1, Landroidx/constraintlayout/motion/widget/j;->m:F

    :goto_2
    iget v8, v1, Landroidx/constraintlayout/motion/widget/j;->p:F

    invoke-static {v8}, Ljava/lang/Float;->isNaN(F)Z

    move-result v8

    const/4 v9, 0x0

    if-eqz v8, :cond_3

    move v8, v9

    goto :goto_3

    :cond_3
    iget v8, v1, Landroidx/constraintlayout/motion/widget/j;->p:F

    :goto_3
    iget v10, v1, Landroidx/constraintlayout/motion/widget/j;->n:F

    invoke-static {v10}, Ljava/lang/Float;->isNaN(F)Z

    move-result v10

    if-eqz v10, :cond_4

    goto :goto_4

    :cond_4
    iget v4, v1, Landroidx/constraintlayout/motion/widget/j;->n:F

    :goto_4
    iget v10, v1, Landroidx/constraintlayout/motion/widget/j;->o:F

    invoke-static {v10}, Ljava/lang/Float;->isNaN(F)Z

    move-result v10

    if-eqz v10, :cond_5

    goto :goto_5

    :cond_5
    iget v9, v1, Landroidx/constraintlayout/motion/widget/j;->o:F

    :goto_5
    const/4 v10, 0x0

    iput v10, v0, Landroidx/constraintlayout/motion/widget/w;->q:I

    move-object/from16 v10, p2

    iget v11, v10, Landroidx/constraintlayout/motion/widget/w;->f:F

    mul-float/2addr v7, v2

    add-float/2addr v11, v7

    mul-float/2addr v9, v3

    add-float/2addr v11, v9

    sub-float/2addr v11, v5

    float-to-int v5, v11

    int-to-float v5, v5

    iput v5, v0, Landroidx/constraintlayout/motion/widget/w;->f:F

    iget v5, v10, Landroidx/constraintlayout/motion/widget/w;->g:F

    mul-float/2addr v2, v8

    add-float/2addr v5, v2

    mul-float/2addr v3, v4

    add-float/2addr v5, v3

    sub-float/2addr v5, v6

    float-to-int v2, v5

    int-to-float v2, v2

    iput v2, v0, Landroidx/constraintlayout/motion/widget/w;->g:F

    iget-object v2, v1, Landroidx/constraintlayout/motion/widget/j;->h:Ljava/lang/String;

    invoke-static {v2}, La/e/a/a/a/c;->a(Ljava/lang/String;)La/e/a/a/a/c;

    move-result-object v2

    iput-object v2, v0, Landroidx/constraintlayout/motion/widget/w;->b:La/e/a/a/a/c;

    iget v1, v1, Landroidx/constraintlayout/motion/widget/j;->i:I

    iput v1, v0, Landroidx/constraintlayout/motion/widget/w;->l:I

    return-void
.end method

.method public a(Landroidx/constraintlayout/motion/widget/p;Landroidx/constraintlayout/motion/widget/w;)V
    .locals 5

    iget v0, p0, Landroidx/constraintlayout/motion/widget/w;->f:F

    iget v1, p0, Landroidx/constraintlayout/motion/widget/w;->h:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p2, Landroidx/constraintlayout/motion/widget/w;->f:F

    sub-float/2addr v0, v1

    iget v1, p2, Landroidx/constraintlayout/motion/widget/w;->h:F

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-double v0, v0

    iget v3, p0, Landroidx/constraintlayout/motion/widget/w;->g:F

    iget v4, p0, Landroidx/constraintlayout/motion/widget/w;->i:F

    div-float/2addr v4, v2

    add-float/2addr v3, v4

    iget v4, p2, Landroidx/constraintlayout/motion/widget/w;->g:F

    sub-float/2addr v3, v4

    iget p2, p2, Landroidx/constraintlayout/motion/widget/w;->i:F

    div-float/2addr p2, v2

    sub-float/2addr v3, p2

    float-to-double v2, v3

    iput-object p1, p0, Landroidx/constraintlayout/motion/widget/w;->o:Landroidx/constraintlayout/motion/widget/p;

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide p1

    double-to-float p1, p1

    iput p1, p0, Landroidx/constraintlayout/motion/widget/w;->f:F

    iget p1, p0, Landroidx/constraintlayout/motion/widget/w;->n:F

    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide p1

    const-wide v0, 0x3ff921fb54442d18L    # 1.5707963267948966

    add-double/2addr p1, v0

    double-to-float p1, p1

    iput p1, p0, Landroidx/constraintlayout/motion/widget/w;->g:F

    goto :goto_0

    :cond_0
    iget p1, p0, Landroidx/constraintlayout/motion/widget/w;->n:F

    float-to-double p1, p1

    invoke-static {p1, p2}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide p1

    double-to-float p1, p1

    iput p1, p0, Landroidx/constraintlayout/motion/widget/w;->g:F

    :goto_0
    return-void
.end method

.method a(Landroidx/constraintlayout/motion/widget/w;[Z[Ljava/lang/String;Z)V
    .locals 5

    iget p3, p0, Landroidx/constraintlayout/motion/widget/w;->f:F

    iget v0, p1, Landroidx/constraintlayout/motion/widget/w;->f:F

    invoke-direct {p0, p3, v0}, Landroidx/constraintlayout/motion/widget/w;->a(FF)Z

    move-result p3

    iget v0, p0, Landroidx/constraintlayout/motion/widget/w;->g:F

    iget v1, p1, Landroidx/constraintlayout/motion/widget/w;->g:F

    invoke-direct {p0, v0, v1}, Landroidx/constraintlayout/motion/widget/w;->a(FF)Z

    move-result v0

    const/4 v1, 0x0

    aget-boolean v2, p2, v1

    iget v3, p0, Landroidx/constraintlayout/motion/widget/w;->e:F

    iget v4, p1, Landroidx/constraintlayout/motion/widget/w;->e:F

    invoke-direct {p0, v3, v4}, Landroidx/constraintlayout/motion/widget/w;->a(FF)Z

    move-result v3

    or-int/2addr v2, v3

    aput-boolean v2, p2, v1

    const/4 v1, 0x1

    aget-boolean v2, p2, v1

    or-int/2addr p3, v0

    or-int/2addr p3, p4

    or-int p4, v2, p3

    aput-boolean p4, p2, v1

    const/4 p4, 0x2

    aget-boolean v0, p2, p4

    or-int/2addr p3, v0

    aput-boolean p3, p2, p4

    const/4 p3, 0x3

    aget-boolean p4, p2, p3

    iget v0, p0, Landroidx/constraintlayout/motion/widget/w;->h:F

    iget v1, p1, Landroidx/constraintlayout/motion/widget/w;->h:F

    invoke-direct {p0, v0, v1}, Landroidx/constraintlayout/motion/widget/w;->a(FF)Z

    move-result v0

    or-int/2addr p4, v0

    aput-boolean p4, p2, p3

    const/4 p3, 0x4

    aget-boolean p4, p2, p3

    iget v0, p0, Landroidx/constraintlayout/motion/widget/w;->i:F

    iget p1, p1, Landroidx/constraintlayout/motion/widget/w;->i:F

    invoke-direct {p0, v0, p1}, Landroidx/constraintlayout/motion/widget/w;->a(FF)Z

    move-result p1

    or-int/2addr p1, p4

    aput-boolean p1, p2, p3

    return-void
.end method

.method public a(Landroidx/constraintlayout/widget/e$a;)V
    .locals 4

    iget-object v0, p1, Landroidx/constraintlayout/widget/e$a;->d:Landroidx/constraintlayout/widget/e$c;

    iget-object v0, v0, Landroidx/constraintlayout/widget/e$c;->e:Ljava/lang/String;

    invoke-static {v0}, La/e/a/a/a/c;->a(Ljava/lang/String;)La/e/a/a/a/c;

    move-result-object v0

    iput-object v0, p0, Landroidx/constraintlayout/motion/widget/w;->b:La/e/a/a/a/c;

    iget-object v0, p1, Landroidx/constraintlayout/widget/e$a;->d:Landroidx/constraintlayout/widget/e$c;

    iget v1, v0, Landroidx/constraintlayout/widget/e$c;->f:I

    iput v1, p0, Landroidx/constraintlayout/motion/widget/w;->l:I

    iget v1, v0, Landroidx/constraintlayout/widget/e$c;->c:I

    iput v1, p0, Landroidx/constraintlayout/motion/widget/w;->m:I

    iget v1, v0, Landroidx/constraintlayout/widget/e$c;->j:F

    iput v1, p0, Landroidx/constraintlayout/motion/widget/w;->j:F

    iget v1, v0, Landroidx/constraintlayout/widget/e$c;->g:I

    iput v1, p0, Landroidx/constraintlayout/motion/widget/w;->c:I

    iget v0, v0, Landroidx/constraintlayout/widget/e$c;->d:I

    iput v0, p0, Landroidx/constraintlayout/motion/widget/w;->r:I

    iget-object v0, p1, Landroidx/constraintlayout/widget/e$a;->c:Landroidx/constraintlayout/widget/e$d;

    iget v0, v0, Landroidx/constraintlayout/widget/e$d;->e:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/w;->k:F

    iget-object v0, p1, Landroidx/constraintlayout/widget/e$a;->e:Landroidx/constraintlayout/widget/e$b;

    iget v0, v0, Landroidx/constraintlayout/widget/e$b;->E:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/w;->n:F

    iget-object v0, p1, Landroidx/constraintlayout/widget/e$a;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p1, Landroidx/constraintlayout/widget/e$a;->g:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroidx/constraintlayout/widget/b;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroidx/constraintlayout/widget/b;->d()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Landroidx/constraintlayout/motion/widget/w;->p:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-void
.end method

.method a([D[I)V
    .locals 6

    const/4 v0, 0x6

    new-array v0, v0, [F

    iget v1, p0, Landroidx/constraintlayout/motion/widget/w;->e:F

    const/4 v2, 0x0

    aput v1, v0, v2

    iget v1, p0, Landroidx/constraintlayout/motion/widget/w;->f:F

    const/4 v3, 0x1

    aput v1, v0, v3

    iget v1, p0, Landroidx/constraintlayout/motion/widget/w;->g:F

    const/4 v3, 0x2

    aput v1, v0, v3

    iget v1, p0, Landroidx/constraintlayout/motion/widget/w;->h:F

    const/4 v3, 0x3

    aput v1, v0, v3

    iget v1, p0, Landroidx/constraintlayout/motion/widget/w;->i:F

    const/4 v3, 0x4

    aput v1, v0, v3

    iget v1, p0, Landroidx/constraintlayout/motion/widget/w;->j:F

    const/4 v3, 0x5

    aput v1, v0, v3

    move v1, v2

    :goto_0
    array-length v3, p2

    if-ge v2, v3, :cond_1

    aget v3, p2, v2

    array-length v4, v0

    if-ge v3, v4, :cond_0

    add-int/lit8 v3, v1, 0x1

    aget v4, p2, v2

    aget v4, v0, v4

    float-to-double v4, v4

    aput-wide v4, p1, v1

    move v1, v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method a([I[D[FI)V
    .locals 10

    iget v0, p0, Landroidx/constraintlayout/motion/widget/w;->f:F

    iget v1, p0, Landroidx/constraintlayout/motion/widget/w;->g:F

    iget v2, p0, Landroidx/constraintlayout/motion/widget/w;->h:F

    iget v3, p0, Landroidx/constraintlayout/motion/widget/w;->i:F

    const/4 v4, 0x0

    :goto_0
    array-length v5, p1

    if-ge v4, v5, :cond_5

    aget-wide v5, p2, v4

    double-to-float v5, v5

    aget v6, p1, v4

    if-eqz v6, :cond_4

    const/4 v7, 0x1

    if-eq v6, v7, :cond_3

    const/4 v7, 0x2

    if-eq v6, v7, :cond_2

    const/4 v7, 0x3

    if-eq v6, v7, :cond_1

    const/4 v7, 0x4

    if-eq v6, v7, :cond_0

    goto :goto_1

    :cond_0
    move v3, v5

    goto :goto_1

    :cond_1
    move v2, v5

    goto :goto_1

    :cond_2
    move v1, v5

    goto :goto_1

    :cond_3
    move v0, v5

    :cond_4
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_5
    iget-object p1, p0, Landroidx/constraintlayout/motion/widget/w;->o:Landroidx/constraintlayout/motion/widget/p;

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Landroidx/constraintlayout/motion/widget/p;->b()F

    move-result p1

    iget-object p2, p0, Landroidx/constraintlayout/motion/widget/w;->o:Landroidx/constraintlayout/motion/widget/p;

    invoke-virtual {p2}, Landroidx/constraintlayout/motion/widget/p;->c()F

    move-result p2

    float-to-double v4, p1

    float-to-double v6, v0

    float-to-double v0, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    mul-double/2addr v8, v6

    add-double/2addr v4, v8

    const/high16 p1, 0x40000000    # 2.0f

    div-float v8, v2, p1

    float-to-double v8, v8

    sub-double/2addr v4, v8

    double-to-float v4, v4

    float-to-double v8, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    mul-double/2addr v6, v0

    sub-double/2addr v8, v6

    div-float p1, v3, p1

    float-to-double p1, p1

    sub-double/2addr v8, p1

    double-to-float v1, v8

    move v0, v4

    :cond_6
    add-float/2addr v2, v0

    add-float/2addr v3, v1

    const/high16 p1, 0x7fc00000    # Float.NaN

    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    move-result p2

    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    move-result p1

    const/4 p1, 0x0

    add-float p2, v0, p1

    add-float v4, v1, p1

    add-float v5, v2, p1

    add-float/2addr v1, p1

    add-float/2addr v2, p1

    add-float v6, v3, p1

    add-float/2addr v0, p1

    add-float/2addr v3, p1

    add-int/lit8 p1, p4, 0x1

    aput p2, p3, p4

    add-int/lit8 p2, p1, 0x1

    aput v4, p3, p1

    add-int/lit8 p1, p2, 0x1

    aput v5, p3, p2

    add-int/lit8 p2, p1, 0x1

    aput v1, p3, p1

    add-int/lit8 p1, p2, 0x1

    aput v2, p3, p2

    add-int/lit8 p2, p1, 0x1

    aput v6, p3, p1

    add-int/lit8 p1, p2, 0x1

    aput v0, p3, p2

    aput v3, p3, p1

    return-void
.end method

.method b(IILandroidx/constraintlayout/motion/widget/j;Landroidx/constraintlayout/motion/widget/w;Landroidx/constraintlayout/motion/widget/w;)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    move-object/from16 v3, p5

    iget v4, v1, Landroidx/constraintlayout/motion/widget/f;->b:I

    int-to-float v4, v4

    const/high16 v5, 0x42c80000    # 100.0f

    div-float/2addr v4, v5

    iput v4, v0, Landroidx/constraintlayout/motion/widget/w;->d:F

    iget v5, v1, Landroidx/constraintlayout/motion/widget/j;->j:I

    iput v5, v0, Landroidx/constraintlayout/motion/widget/w;->c:I

    iget v5, v1, Landroidx/constraintlayout/motion/widget/j;->k:F

    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-eqz v5, :cond_0

    move v5, v4

    goto :goto_0

    :cond_0
    iget v5, v1, Landroidx/constraintlayout/motion/widget/j;->k:F

    :goto_0
    iget v6, v1, Landroidx/constraintlayout/motion/widget/j;->l:F

    invoke-static {v6}, Ljava/lang/Float;->isNaN(F)Z

    move-result v6

    if-eqz v6, :cond_1

    move v6, v4

    goto :goto_1

    :cond_1
    iget v6, v1, Landroidx/constraintlayout/motion/widget/j;->l:F

    :goto_1
    iget v7, v3, Landroidx/constraintlayout/motion/widget/w;->h:F

    iget v8, v2, Landroidx/constraintlayout/motion/widget/w;->h:F

    sub-float v9, v7, v8

    iget v10, v3, Landroidx/constraintlayout/motion/widget/w;->i:F

    iget v11, v2, Landroidx/constraintlayout/motion/widget/w;->i:F

    sub-float v12, v10, v11

    iget v13, v0, Landroidx/constraintlayout/motion/widget/w;->d:F

    iput v13, v0, Landroidx/constraintlayout/motion/widget/w;->e:F

    iget v13, v2, Landroidx/constraintlayout/motion/widget/w;->f:F

    const/high16 v14, 0x40000000    # 2.0f

    div-float v15, v8, v14

    add-float/2addr v15, v13

    iget v2, v2, Landroidx/constraintlayout/motion/widget/w;->g:F

    div-float v16, v11, v14

    add-float v16, v2, v16

    iget v1, v3, Landroidx/constraintlayout/motion/widget/w;->f:F

    div-float/2addr v7, v14

    add-float/2addr v1, v7

    iget v3, v3, Landroidx/constraintlayout/motion/widget/w;->g:F

    div-float/2addr v10, v14

    add-float/2addr v3, v10

    sub-float/2addr v1, v15

    sub-float v3, v3, v16

    mul-float/2addr v1, v4

    add-float/2addr v13, v1

    mul-float/2addr v9, v5

    div-float v1, v9, v14

    sub-float/2addr v13, v1

    float-to-int v1, v13

    int-to-float v1, v1

    iput v1, v0, Landroidx/constraintlayout/motion/widget/w;->f:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    mul-float/2addr v12, v6

    div-float v1, v12, v14

    sub-float/2addr v2, v1

    float-to-int v1, v2

    int-to-float v1, v1

    iput v1, v0, Landroidx/constraintlayout/motion/widget/w;->g:F

    add-float/2addr v8, v9

    float-to-int v1, v8

    int-to-float v1, v1

    iput v1, v0, Landroidx/constraintlayout/motion/widget/w;->h:F

    add-float/2addr v11, v12

    float-to-int v1, v11

    int-to-float v1, v1

    iput v1, v0, Landroidx/constraintlayout/motion/widget/w;->i:F

    const/4 v1, 0x2

    iput v1, v0, Landroidx/constraintlayout/motion/widget/w;->q:I

    move-object/from16 v1, p3

    iget v2, v1, Landroidx/constraintlayout/motion/widget/j;->m:F

    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_2

    move/from16 v2, p1

    int-to-float v2, v2

    iget v3, v0, Landroidx/constraintlayout/motion/widget/w;->h:F

    sub-float/2addr v2, v3

    float-to-int v2, v2

    iget v3, v1, Landroidx/constraintlayout/motion/widget/j;->m:F

    int-to-float v2, v2

    mul-float/2addr v3, v2

    float-to-int v2, v3

    int-to-float v2, v2

    iput v2, v0, Landroidx/constraintlayout/motion/widget/w;->f:F

    :cond_2
    iget v2, v1, Landroidx/constraintlayout/motion/widget/j;->n:F

    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_3

    move/from16 v2, p2

    int-to-float v2, v2

    iget v3, v0, Landroidx/constraintlayout/motion/widget/w;->i:F

    sub-float/2addr v2, v3

    float-to-int v2, v2

    iget v3, v1, Landroidx/constraintlayout/motion/widget/j;->n:F

    int-to-float v2, v2

    mul-float/2addr v3, v2

    float-to-int v2, v3

    int-to-float v2, v2

    iput v2, v0, Landroidx/constraintlayout/motion/widget/w;->g:F

    :cond_3
    iget v2, v0, Landroidx/constraintlayout/motion/widget/w;->m:I

    iput v2, v0, Landroidx/constraintlayout/motion/widget/w;->m:I

    iget-object v2, v1, Landroidx/constraintlayout/motion/widget/j;->h:Ljava/lang/String;

    invoke-static {v2}, La/e/a/a/a/c;->a(Ljava/lang/String;)La/e/a/a/a/c;

    move-result-object v2

    iput-object v2, v0, Landroidx/constraintlayout/motion/widget/w;->b:La/e/a/a/a/c;

    iget v1, v1, Landroidx/constraintlayout/motion/widget/j;->i:I

    iput v1, v0, Landroidx/constraintlayout/motion/widget/w;->l:I

    return-void
.end method

.method b(Landroidx/constraintlayout/motion/widget/j;Landroidx/constraintlayout/motion/widget/w;Landroidx/constraintlayout/motion/widget/w;)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    iget v4, v1, Landroidx/constraintlayout/motion/widget/f;->b:I

    int-to-float v4, v4

    const/high16 v5, 0x42c80000    # 100.0f

    div-float/2addr v4, v5

    iput v4, v0, Landroidx/constraintlayout/motion/widget/w;->d:F

    iget v5, v1, Landroidx/constraintlayout/motion/widget/j;->j:I

    iput v5, v0, Landroidx/constraintlayout/motion/widget/w;->c:I

    iget v5, v1, Landroidx/constraintlayout/motion/widget/j;->k:F

    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-eqz v5, :cond_0

    move v5, v4

    goto :goto_0

    :cond_0
    iget v5, v1, Landroidx/constraintlayout/motion/widget/j;->k:F

    :goto_0
    iget v6, v1, Landroidx/constraintlayout/motion/widget/j;->l:F

    invoke-static {v6}, Ljava/lang/Float;->isNaN(F)Z

    move-result v6

    if-eqz v6, :cond_1

    move v6, v4

    goto :goto_1

    :cond_1
    iget v6, v1, Landroidx/constraintlayout/motion/widget/j;->l:F

    :goto_1
    iget v7, v3, Landroidx/constraintlayout/motion/widget/w;->h:F

    iget v8, v2, Landroidx/constraintlayout/motion/widget/w;->h:F

    sub-float/2addr v7, v8

    iget v8, v3, Landroidx/constraintlayout/motion/widget/w;->i:F

    iget v9, v2, Landroidx/constraintlayout/motion/widget/w;->i:F

    sub-float/2addr v8, v9

    iget v9, v0, Landroidx/constraintlayout/motion/widget/w;->d:F

    iput v9, v0, Landroidx/constraintlayout/motion/widget/w;->e:F

    iget v9, v1, Landroidx/constraintlayout/motion/widget/j;->m:F

    invoke-static {v9}, Ljava/lang/Float;->isNaN(F)Z

    move-result v9

    if-eqz v9, :cond_2

    goto :goto_2

    :cond_2
    iget v4, v1, Landroidx/constraintlayout/motion/widget/j;->m:F

    :goto_2
    iget v9, v2, Landroidx/constraintlayout/motion/widget/w;->f:F

    iget v10, v2, Landroidx/constraintlayout/motion/widget/w;->h:F

    const/high16 v11, 0x40000000    # 2.0f

    div-float v12, v10, v11

    add-float/2addr v12, v9

    iget v13, v2, Landroidx/constraintlayout/motion/widget/w;->g:F

    iget v14, v2, Landroidx/constraintlayout/motion/widget/w;->i:F

    div-float v15, v14, v11

    add-float/2addr v15, v13

    iget v2, v3, Landroidx/constraintlayout/motion/widget/w;->f:F

    iget v1, v3, Landroidx/constraintlayout/motion/widget/w;->h:F

    div-float/2addr v1, v11

    add-float/2addr v2, v1

    iget v1, v3, Landroidx/constraintlayout/motion/widget/w;->g:F

    iget v3, v3, Landroidx/constraintlayout/motion/widget/w;->i:F

    div-float/2addr v3, v11

    add-float/2addr v1, v3

    sub-float/2addr v2, v12

    sub-float/2addr v1, v15

    mul-float v3, v2, v4

    add-float/2addr v9, v3

    mul-float/2addr v7, v5

    div-float v5, v7, v11

    sub-float/2addr v9, v5

    float-to-int v9, v9

    int-to-float v9, v9

    iput v9, v0, Landroidx/constraintlayout/motion/widget/w;->f:F

    mul-float/2addr v4, v1

    add-float/2addr v13, v4

    mul-float/2addr v8, v6

    div-float v6, v8, v11

    sub-float/2addr v13, v6

    float-to-int v9, v13

    int-to-float v9, v9

    iput v9, v0, Landroidx/constraintlayout/motion/widget/w;->g:F

    add-float/2addr v10, v7

    float-to-int v7, v10

    int-to-float v7, v7

    iput v7, v0, Landroidx/constraintlayout/motion/widget/w;->h:F

    add-float/2addr v14, v8

    float-to-int v7, v14

    int-to-float v7, v7

    iput v7, v0, Landroidx/constraintlayout/motion/widget/w;->i:F

    move-object/from16 v7, p1

    iget v8, v7, Landroidx/constraintlayout/motion/widget/j;->n:F

    invoke-static {v8}, Ljava/lang/Float;->isNaN(F)Z

    move-result v8

    if-eqz v8, :cond_3

    const/4 v8, 0x0

    goto :goto_3

    :cond_3
    iget v8, v7, Landroidx/constraintlayout/motion/widget/j;->n:F

    :goto_3
    neg-float v1, v1

    mul-float/2addr v1, v8

    mul-float/2addr v2, v8

    const/4 v8, 0x1

    iput v8, v0, Landroidx/constraintlayout/motion/widget/w;->q:I

    move-object/from16 v8, p2

    iget v9, v8, Landroidx/constraintlayout/motion/widget/w;->f:F

    add-float/2addr v9, v3

    sub-float/2addr v9, v5

    float-to-int v3, v9

    int-to-float v3, v3

    iput v3, v0, Landroidx/constraintlayout/motion/widget/w;->f:F

    iget v3, v8, Landroidx/constraintlayout/motion/widget/w;->g:F

    add-float/2addr v3, v4

    sub-float/2addr v3, v6

    float-to-int v3, v3

    int-to-float v3, v3

    iput v3, v0, Landroidx/constraintlayout/motion/widget/w;->g:F

    iget v3, v0, Landroidx/constraintlayout/motion/widget/w;->f:F

    add-float/2addr v3, v1

    iput v3, v0, Landroidx/constraintlayout/motion/widget/w;->f:F

    iget v1, v0, Landroidx/constraintlayout/motion/widget/w;->g:F

    add-float/2addr v1, v2

    iput v1, v0, Landroidx/constraintlayout/motion/widget/w;->g:F

    iget v1, v0, Landroidx/constraintlayout/motion/widget/w;->m:I

    iput v1, v0, Landroidx/constraintlayout/motion/widget/w;->m:I

    iget-object v1, v7, Landroidx/constraintlayout/motion/widget/j;->h:Ljava/lang/String;

    invoke-static {v1}, La/e/a/a/a/c;->a(Ljava/lang/String;)La/e/a/a/a/c;

    move-result-object v1

    iput-object v1, v0, Landroidx/constraintlayout/motion/widget/w;->b:La/e/a/a/a/c;

    iget v1, v7, Landroidx/constraintlayout/motion/widget/j;->i:I

    iput v1, v0, Landroidx/constraintlayout/motion/widget/w;->l:I

    return-void
.end method

.method b(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/w;->p:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    check-cast p1, Landroidx/constraintlayout/motion/widget/w;

    invoke-virtual {p0, p1}, Landroidx/constraintlayout/motion/widget/w;->a(Landroidx/constraintlayout/motion/widget/w;)I

    move-result p1

    return p1
.end method
