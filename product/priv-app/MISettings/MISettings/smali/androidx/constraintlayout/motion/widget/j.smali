.class public Landroidx/constraintlayout/motion/widget/j;
.super Landroidx/constraintlayout/motion/widget/k;
.source "KeyPosition.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/constraintlayout/motion/widget/j$a;
    }
.end annotation


# instance fields
.field h:Ljava/lang/String;

.field i:I

.field j:I

.field k:F

.field l:F

.field m:F

.field n:F

.field o:F

.field p:F

.field q:I

.field private r:F

.field private s:F


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroidx/constraintlayout/motion/widget/k;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/constraintlayout/motion/widget/j;->h:Ljava/lang/String;

    sget v0, Landroidx/constraintlayout/motion/widget/f;->a:I

    iput v0, p0, Landroidx/constraintlayout/motion/widget/j;->i:I

    const/4 v0, 0x0

    iput v0, p0, Landroidx/constraintlayout/motion/widget/j;->j:I

    const/high16 v1, 0x7fc00000    # Float.NaN

    iput v1, p0, Landroidx/constraintlayout/motion/widget/j;->k:F

    iput v1, p0, Landroidx/constraintlayout/motion/widget/j;->l:F

    iput v1, p0, Landroidx/constraintlayout/motion/widget/j;->m:F

    iput v1, p0, Landroidx/constraintlayout/motion/widget/j;->n:F

    iput v1, p0, Landroidx/constraintlayout/motion/widget/j;->o:F

    iput v1, p0, Landroidx/constraintlayout/motion/widget/j;->p:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/j;->q:I

    iput v1, p0, Landroidx/constraintlayout/motion/widget/j;->r:F

    iput v1, p0, Landroidx/constraintlayout/motion/widget/j;->s:F

    const/4 v0, 0x2

    iput v0, p0, Landroidx/constraintlayout/motion/widget/f;->e:I

    return-void
.end method


# virtual methods
.method public a(Landroidx/constraintlayout/motion/widget/f;)Landroidx/constraintlayout/motion/widget/f;
    .locals 1

    invoke-super {p0, p1}, Landroidx/constraintlayout/motion/widget/f;->a(Landroidx/constraintlayout/motion/widget/f;)Landroidx/constraintlayout/motion/widget/f;

    check-cast p1, Landroidx/constraintlayout/motion/widget/j;

    iget-object v0, p1, Landroidx/constraintlayout/motion/widget/j;->h:Ljava/lang/String;

    iput-object v0, p0, Landroidx/constraintlayout/motion/widget/j;->h:Ljava/lang/String;

    iget v0, p1, Landroidx/constraintlayout/motion/widget/j;->i:I

    iput v0, p0, Landroidx/constraintlayout/motion/widget/j;->i:I

    iget v0, p1, Landroidx/constraintlayout/motion/widget/j;->j:I

    iput v0, p0, Landroidx/constraintlayout/motion/widget/j;->j:I

    iget v0, p1, Landroidx/constraintlayout/motion/widget/j;->k:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/j;->k:F

    const/high16 v0, 0x7fc00000    # Float.NaN

    iput v0, p0, Landroidx/constraintlayout/motion/widget/j;->l:F

    iget v0, p1, Landroidx/constraintlayout/motion/widget/j;->m:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/j;->m:F

    iget v0, p1, Landroidx/constraintlayout/motion/widget/j;->n:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/j;->n:F

    iget v0, p1, Landroidx/constraintlayout/motion/widget/j;->o:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/j;->o:F

    iget v0, p1, Landroidx/constraintlayout/motion/widget/j;->p:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/j;->p:F

    iget v0, p1, Landroidx/constraintlayout/motion/widget/j;->r:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/j;->r:F

    iget p1, p1, Landroidx/constraintlayout/motion/widget/j;->s:F

    iput p1, p0, Landroidx/constraintlayout/motion/widget/j;->s:F

    return-object p0
.end method

.method public a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget-object v0, Landroidx/constraintlayout/widget/h;->KeyPosition:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    invoke-static {p0, p1}, Landroidx/constraintlayout/motion/widget/j$a;->a(Landroidx/constraintlayout/motion/widget/j;Landroid/content/res/TypedArray;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v0, "percentY"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x6

    goto :goto_1

    :sswitch_1
    const-string v0, "percentX"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x5

    goto :goto_1

    :sswitch_2
    const-string v0, "sizePercent"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x4

    goto :goto_1

    :sswitch_3
    const-string v0, "drawPath"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_1

    :sswitch_4
    const-string v0, "percentHeight"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x3

    goto :goto_1

    :sswitch_5
    const-string v0, "percentWidth"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x2

    goto :goto_1

    :sswitch_6
    const-string v0, "transitionEasing"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_1

    :cond_0
    :goto_0
    const/4 p1, -0x1

    :goto_1
    packed-switch p1, :pswitch_data_0

    goto :goto_2

    :pswitch_0
    invoke-virtual {p0, p2}, Landroidx/constraintlayout/motion/widget/f;->b(Ljava/lang/Object;)F

    move-result p1

    iput p1, p0, Landroidx/constraintlayout/motion/widget/j;->n:F

    goto :goto_2

    :pswitch_1
    invoke-virtual {p0, p2}, Landroidx/constraintlayout/motion/widget/f;->b(Ljava/lang/Object;)F

    move-result p1

    iput p1, p0, Landroidx/constraintlayout/motion/widget/j;->m:F

    goto :goto_2

    :pswitch_2
    invoke-virtual {p0, p2}, Landroidx/constraintlayout/motion/widget/f;->b(Ljava/lang/Object;)F

    move-result p1

    iput p1, p0, Landroidx/constraintlayout/motion/widget/j;->k:F

    iput p1, p0, Landroidx/constraintlayout/motion/widget/j;->l:F

    goto :goto_2

    :pswitch_3
    invoke-virtual {p0, p2}, Landroidx/constraintlayout/motion/widget/f;->b(Ljava/lang/Object;)F

    move-result p1

    iput p1, p0, Landroidx/constraintlayout/motion/widget/j;->l:F

    goto :goto_2

    :pswitch_4
    invoke-virtual {p0, p2}, Landroidx/constraintlayout/motion/widget/f;->b(Ljava/lang/Object;)F

    move-result p1

    iput p1, p0, Landroidx/constraintlayout/motion/widget/j;->k:F

    goto :goto_2

    :pswitch_5
    invoke-virtual {p0, p2}, Landroidx/constraintlayout/motion/widget/f;->c(Ljava/lang/Object;)I

    move-result p1

    iput p1, p0, Landroidx/constraintlayout/motion/widget/j;->j:I

    goto :goto_2

    :pswitch_6
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Landroidx/constraintlayout/motion/widget/j;->h:Ljava/lang/String;

    :goto_2
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x6c0d7d20 -> :sswitch_6
        -0x4330437f -> :sswitch_5
        -0x3ca72634 -> :sswitch_4
        -0x314b3c77 -> :sswitch_3
        -0xbefb6fc -> :sswitch_2
        0x198424b3 -> :sswitch_1
        0x198424b4 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "La/e/b/a/e;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public c(I)V
    .locals 0

    iput p1, p0, Landroidx/constraintlayout/motion/widget/j;->q:I

    return-void
.end method

.method public clone()Landroidx/constraintlayout/motion/widget/f;
    .locals 1

    new-instance v0, Landroidx/constraintlayout/motion/widget/j;

    invoke-direct {v0}, Landroidx/constraintlayout/motion/widget/j;-><init>()V

    invoke-virtual {v0, p0}, Landroidx/constraintlayout/motion/widget/j;->a(Landroidx/constraintlayout/motion/widget/f;)Landroidx/constraintlayout/motion/widget/f;

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Landroidx/constraintlayout/motion/widget/j;->clone()Landroidx/constraintlayout/motion/widget/f;

    move-result-object v0

    return-object v0
.end method
