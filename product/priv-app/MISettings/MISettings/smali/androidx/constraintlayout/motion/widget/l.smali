.class public Landroidx/constraintlayout/motion/widget/l;
.super Landroidx/constraintlayout/motion/widget/f;
.source "KeyTimeCycle.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/constraintlayout/motion/widget/l$a;
    }
.end annotation


# instance fields
.field private g:Ljava/lang/String;

.field private h:I

.field private i:F

.field private j:F

.field private k:F

.field private l:F

.field private m:F

.field private n:F

.field private o:F

.field private p:F

.field private q:F

.field private r:F

.field private s:F

.field private t:F

.field private u:I

.field private v:Ljava/lang/String;

.field private w:F

.field private x:F


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroidx/constraintlayout/motion/widget/f;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->h:I

    const/high16 v0, 0x7fc00000    # Float.NaN

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->i:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->j:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->k:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->l:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->m:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->n:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->o:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->p:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->q:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->r:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->s:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->t:F

    const/4 v1, 0x0

    iput v1, p0, Landroidx/constraintlayout/motion/widget/l;->u:I

    const/4 v1, 0x0

    iput-object v1, p0, Landroidx/constraintlayout/motion/widget/l;->v:Ljava/lang/String;

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->w:F

    const/4 v0, 0x0

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->x:F

    const/4 v0, 0x3

    iput v0, p0, Landroidx/constraintlayout/motion/widget/f;->e:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Landroidx/constraintlayout/motion/widget/f;->f:Ljava/util/HashMap;

    return-void
.end method

.method static synthetic a(Landroidx/constraintlayout/motion/widget/l;)F
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/l;->i:F

    return p0
.end method

.method static synthetic a(Landroidx/constraintlayout/motion/widget/l;F)F
    .locals 0

    iput p1, p0, Landroidx/constraintlayout/motion/widget/l;->i:F

    return p1
.end method

.method static synthetic a(Landroidx/constraintlayout/motion/widget/l;I)I
    .locals 0

    iput p1, p0, Landroidx/constraintlayout/motion/widget/l;->h:I

    return p1
.end method

.method static synthetic a(Landroidx/constraintlayout/motion/widget/l;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Landroidx/constraintlayout/motion/widget/l;->g:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Landroidx/constraintlayout/motion/widget/l;)F
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/l;->j:F

    return p0
.end method

.method static synthetic b(Landroidx/constraintlayout/motion/widget/l;F)F
    .locals 0

    iput p1, p0, Landroidx/constraintlayout/motion/widget/l;->m:F

    return p1
.end method

.method static synthetic b(Landroidx/constraintlayout/motion/widget/l;I)I
    .locals 0

    iput p1, p0, Landroidx/constraintlayout/motion/widget/l;->u:I

    return p1
.end method

.method static synthetic b(Landroidx/constraintlayout/motion/widget/l;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Landroidx/constraintlayout/motion/widget/l;->v:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Landroidx/constraintlayout/motion/widget/l;)F
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/l;->m:F

    return p0
.end method

.method static synthetic c(Landroidx/constraintlayout/motion/widget/l;F)F
    .locals 0

    iput p1, p0, Landroidx/constraintlayout/motion/widget/l;->j:F

    return p1
.end method

.method static synthetic d(Landroidx/constraintlayout/motion/widget/l;)F
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/l;->p:F

    return p0
.end method

.method static synthetic d(Landroidx/constraintlayout/motion/widget/l;F)F
    .locals 0

    iput p1, p0, Landroidx/constraintlayout/motion/widget/l;->p:F

    return p1
.end method

.method static synthetic e(Landroidx/constraintlayout/motion/widget/l;)F
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/l;->n:F

    return p0
.end method

.method static synthetic e(Landroidx/constraintlayout/motion/widget/l;F)F
    .locals 0

    iput p1, p0, Landroidx/constraintlayout/motion/widget/l;->n:F

    return p1
.end method

.method static synthetic f(Landroidx/constraintlayout/motion/widget/l;)F
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/l;->q:F

    return p0
.end method

.method static synthetic f(Landroidx/constraintlayout/motion/widget/l;F)F
    .locals 0

    iput p1, p0, Landroidx/constraintlayout/motion/widget/l;->q:F

    return p1
.end method

.method static synthetic g(Landroidx/constraintlayout/motion/widget/l;)F
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/l;->r:F

    return p0
.end method

.method static synthetic g(Landroidx/constraintlayout/motion/widget/l;F)F
    .locals 0

    iput p1, p0, Landroidx/constraintlayout/motion/widget/l;->r:F

    return p1
.end method

.method static synthetic h(Landroidx/constraintlayout/motion/widget/l;)F
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/l;->s:F

    return p0
.end method

.method static synthetic h(Landroidx/constraintlayout/motion/widget/l;F)F
    .locals 0

    iput p1, p0, Landroidx/constraintlayout/motion/widget/l;->s:F

    return p1
.end method

.method static synthetic i(Landroidx/constraintlayout/motion/widget/l;)F
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/l;->t:F

    return p0
.end method

.method static synthetic i(Landroidx/constraintlayout/motion/widget/l;F)F
    .locals 0

    iput p1, p0, Landroidx/constraintlayout/motion/widget/l;->t:F

    return p1
.end method

.method static synthetic j(Landroidx/constraintlayout/motion/widget/l;)F
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/l;->k:F

    return p0
.end method

.method static synthetic j(Landroidx/constraintlayout/motion/widget/l;F)F
    .locals 0

    iput p1, p0, Landroidx/constraintlayout/motion/widget/l;->k:F

    return p1
.end method

.method static synthetic k(Landroidx/constraintlayout/motion/widget/l;F)F
    .locals 0

    iput p1, p0, Landroidx/constraintlayout/motion/widget/l;->w:F

    return p1
.end method

.method static synthetic k(Landroidx/constraintlayout/motion/widget/l;)I
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/l;->h:I

    return p0
.end method

.method static synthetic l(Landroidx/constraintlayout/motion/widget/l;F)F
    .locals 0

    iput p1, p0, Landroidx/constraintlayout/motion/widget/l;->x:F

    return p1
.end method

.method static synthetic l(Landroidx/constraintlayout/motion/widget/l;)I
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/l;->u:I

    return p0
.end method

.method static synthetic m(Landroidx/constraintlayout/motion/widget/l;)F
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/l;->w:F

    return p0
.end method

.method static synthetic m(Landroidx/constraintlayout/motion/widget/l;F)F
    .locals 0

    iput p1, p0, Landroidx/constraintlayout/motion/widget/l;->o:F

    return p1
.end method

.method static synthetic n(Landroidx/constraintlayout/motion/widget/l;)F
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/l;->x:F

    return p0
.end method

.method static synthetic n(Landroidx/constraintlayout/motion/widget/l;F)F
    .locals 0

    iput p1, p0, Landroidx/constraintlayout/motion/widget/l;->l:F

    return p1
.end method

.method static synthetic o(Landroidx/constraintlayout/motion/widget/l;)F
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/l;->o:F

    return p0
.end method

.method static synthetic p(Landroidx/constraintlayout/motion/widget/l;)F
    .locals 0

    iget p0, p0, Landroidx/constraintlayout/motion/widget/l;->l:F

    return p0
.end method


# virtual methods
.method public a(Landroidx/constraintlayout/motion/widget/f;)Landroidx/constraintlayout/motion/widget/f;
    .locals 1

    invoke-super {p0, p1}, Landroidx/constraintlayout/motion/widget/f;->a(Landroidx/constraintlayout/motion/widget/f;)Landroidx/constraintlayout/motion/widget/f;

    check-cast p1, Landroidx/constraintlayout/motion/widget/l;

    iget-object v0, p1, Landroidx/constraintlayout/motion/widget/l;->g:Ljava/lang/String;

    iput-object v0, p0, Landroidx/constraintlayout/motion/widget/l;->g:Ljava/lang/String;

    iget v0, p1, Landroidx/constraintlayout/motion/widget/l;->h:I

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->h:I

    iget v0, p1, Landroidx/constraintlayout/motion/widget/l;->u:I

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->u:I

    iget v0, p1, Landroidx/constraintlayout/motion/widget/l;->w:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->w:F

    iget v0, p1, Landroidx/constraintlayout/motion/widget/l;->x:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->x:F

    iget v0, p1, Landroidx/constraintlayout/motion/widget/l;->t:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->t:F

    iget v0, p1, Landroidx/constraintlayout/motion/widget/l;->i:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->i:F

    iget v0, p1, Landroidx/constraintlayout/motion/widget/l;->j:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->j:F

    iget v0, p1, Landroidx/constraintlayout/motion/widget/l;->k:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->k:F

    iget v0, p1, Landroidx/constraintlayout/motion/widget/l;->n:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->n:F

    iget v0, p1, Landroidx/constraintlayout/motion/widget/l;->l:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->l:F

    iget v0, p1, Landroidx/constraintlayout/motion/widget/l;->m:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->m:F

    iget v0, p1, Landroidx/constraintlayout/motion/widget/l;->o:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->o:F

    iget v0, p1, Landroidx/constraintlayout/motion/widget/l;->p:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->p:F

    iget v0, p1, Landroidx/constraintlayout/motion/widget/l;->q:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->q:F

    iget v0, p1, Landroidx/constraintlayout/motion/widget/l;->r:F

    iput v0, p0, Landroidx/constraintlayout/motion/widget/l;->r:F

    iget p1, p1, Landroidx/constraintlayout/motion/widget/l;->s:F

    iput p1, p0, Landroidx/constraintlayout/motion/widget/l;->s:F

    return-object p0
.end method

.method public a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget-object v0, Landroidx/constraintlayout/widget/h;->KeyTimeCycle:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    invoke-static {p0, p1}, Landroidx/constraintlayout/motion/widget/l$a;->a(Landroidx/constraintlayout/motion/widget/l;Landroid/content/res/TypedArray;)V

    return-void
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "La/e/b/a/e;",
            ">;)V"
        }
    .end annotation

    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, " KeyTimeCycles do not support SplineSet"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(Ljava/util/HashSet;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->i:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "alpha"

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->j:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "elevation"

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_1
    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->k:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "rotation"

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_2
    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->l:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "rotationX"

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_3
    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->m:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "rotationY"

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_4
    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->q:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "translationX"

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_5
    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->r:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "translationY"

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_6
    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->s:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "translationZ"

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_7
    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->n:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "transitionPathRotate"

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_8
    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->o:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_9

    const-string v0, "scaleX"

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_9
    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->p:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_a

    const-string v0, "scaleY"

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_a
    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->t:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_b

    const-string v0, "progress"

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_b
    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/f;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_c

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/f;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CUSTOM,"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_c
    return-void
.end method

.method public b(Ljava/util/HashMap;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->h:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    return-void

    :cond_0
    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->i:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "alpha"

    invoke-virtual {p1, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->j:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "elevation"

    invoke-virtual {p1, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->k:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_3

    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "rotation"

    invoke-virtual {p1, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->l:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_4

    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "rotationX"

    invoke-virtual {p1, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->m:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_5

    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "rotationY"

    invoke-virtual {p1, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->q:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_6

    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "translationX"

    invoke-virtual {p1, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->r:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_7

    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "translationY"

    invoke-virtual {p1, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7
    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->s:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_8

    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "translationZ"

    invoke-virtual {p1, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->n:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_9

    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "transitionPathRotate"

    invoke-virtual {p1, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->o:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_a

    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "scaleX"

    invoke-virtual {p1, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_a
    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->o:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_b

    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "scaleY"

    invoke-virtual {p1, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_b
    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->t:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_c

    iget v0, p0, Landroidx/constraintlayout/motion/widget/l;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "progress"

    invoke-virtual {p1, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_c
    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/f;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_d

    iget-object v0, p0, Landroidx/constraintlayout/motion/widget/f;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CUSTOM,"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Landroidx/constraintlayout/motion/widget/l;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_d
    return-void
.end method

.method public c(Ljava/util/HashMap;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "La/e/b/a/g;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, La/e/b/a/g;

    if-nez v3, :cond_1

    goto :goto_0

    :cond_1
    const-string v2, "CUSTOM"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    const/4 v4, 0x7

    if-eqz v2, :cond_2

    invoke-virtual {v1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Landroidx/constraintlayout/motion/widget/f;->f:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Landroidx/constraintlayout/widget/b;

    if-eqz v6, :cond_0

    move-object v4, v3

    check-cast v4, La/e/b/a/g$b;

    iget v5, p0, Landroidx/constraintlayout/motion/widget/f;->b:I

    iget v7, p0, Landroidx/constraintlayout/motion/widget/l;->w:F

    iget v8, p0, Landroidx/constraintlayout/motion/widget/l;->u:I

    iget v9, p0, Landroidx/constraintlayout/motion/widget/l;->x:F

    invoke-virtual/range {v4 .. v9}, La/e/b/a/g$b;->a(ILandroidx/constraintlayout/widget/b;FIF)V

    goto :goto_0

    :cond_2
    const/4 v2, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    goto/16 :goto_1

    :sswitch_0
    const-string v4, "alpha"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x0

    goto/16 :goto_2

    :sswitch_1
    const-string v4, "transitionPathRotate"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x5

    goto/16 :goto_2

    :sswitch_2
    const-string v4, "elevation"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x1

    goto :goto_2

    :sswitch_3
    const-string v4, "rotation"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x2

    goto :goto_2

    :sswitch_4
    const-string v5, "scaleY"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    goto :goto_2

    :sswitch_5
    const-string v4, "scaleX"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x6

    goto :goto_2

    :sswitch_6
    const-string v4, "progress"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/16 v4, 0xb

    goto :goto_2

    :sswitch_7
    const-string v4, "translationZ"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/16 v4, 0xa

    goto :goto_2

    :sswitch_8
    const-string v4, "translationY"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/16 v4, 0x9

    goto :goto_2

    :sswitch_9
    const-string v4, "translationX"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/16 v4, 0x8

    goto :goto_2

    :sswitch_a
    const-string v4, "rotationY"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    goto :goto_2

    :sswitch_b
    const-string v4, "rotationX"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x3

    goto :goto_2

    :cond_3
    :goto_1
    move v4, v2

    :goto_2
    packed-switch v4, :pswitch_data_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UNKNOWN addValues \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\""

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "KeyTimeCycles"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_0
    iget v1, p0, Landroidx/constraintlayout/motion/widget/l;->t:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_0

    iget v4, p0, Landroidx/constraintlayout/motion/widget/f;->b:I

    iget v5, p0, Landroidx/constraintlayout/motion/widget/l;->t:F

    iget v6, p0, Landroidx/constraintlayout/motion/widget/l;->w:F

    iget v7, p0, Landroidx/constraintlayout/motion/widget/l;->u:I

    iget v8, p0, Landroidx/constraintlayout/motion/widget/l;->x:F

    invoke-virtual/range {v3 .. v8}, La/e/a/a/a/p;->a(IFFIF)V

    goto/16 :goto_0

    :pswitch_1
    iget v1, p0, Landroidx/constraintlayout/motion/widget/l;->s:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_0

    iget v4, p0, Landroidx/constraintlayout/motion/widget/f;->b:I

    iget v5, p0, Landroidx/constraintlayout/motion/widget/l;->s:F

    iget v6, p0, Landroidx/constraintlayout/motion/widget/l;->w:F

    iget v7, p0, Landroidx/constraintlayout/motion/widget/l;->u:I

    iget v8, p0, Landroidx/constraintlayout/motion/widget/l;->x:F

    invoke-virtual/range {v3 .. v8}, La/e/a/a/a/p;->a(IFFIF)V

    goto/16 :goto_0

    :pswitch_2
    iget v1, p0, Landroidx/constraintlayout/motion/widget/l;->r:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_0

    iget v4, p0, Landroidx/constraintlayout/motion/widget/f;->b:I

    iget v5, p0, Landroidx/constraintlayout/motion/widget/l;->r:F

    iget v6, p0, Landroidx/constraintlayout/motion/widget/l;->w:F

    iget v7, p0, Landroidx/constraintlayout/motion/widget/l;->u:I

    iget v8, p0, Landroidx/constraintlayout/motion/widget/l;->x:F

    invoke-virtual/range {v3 .. v8}, La/e/a/a/a/p;->a(IFFIF)V

    goto/16 :goto_0

    :pswitch_3
    iget v1, p0, Landroidx/constraintlayout/motion/widget/l;->q:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_0

    iget v4, p0, Landroidx/constraintlayout/motion/widget/f;->b:I

    iget v5, p0, Landroidx/constraintlayout/motion/widget/l;->q:F

    iget v6, p0, Landroidx/constraintlayout/motion/widget/l;->w:F

    iget v7, p0, Landroidx/constraintlayout/motion/widget/l;->u:I

    iget v8, p0, Landroidx/constraintlayout/motion/widget/l;->x:F

    invoke-virtual/range {v3 .. v8}, La/e/a/a/a/p;->a(IFFIF)V

    goto/16 :goto_0

    :pswitch_4
    iget v1, p0, Landroidx/constraintlayout/motion/widget/l;->p:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_0

    iget v4, p0, Landroidx/constraintlayout/motion/widget/f;->b:I

    iget v5, p0, Landroidx/constraintlayout/motion/widget/l;->p:F

    iget v6, p0, Landroidx/constraintlayout/motion/widget/l;->w:F

    iget v7, p0, Landroidx/constraintlayout/motion/widget/l;->u:I

    iget v8, p0, Landroidx/constraintlayout/motion/widget/l;->x:F

    invoke-virtual/range {v3 .. v8}, La/e/a/a/a/p;->a(IFFIF)V

    goto/16 :goto_0

    :pswitch_5
    iget v1, p0, Landroidx/constraintlayout/motion/widget/l;->o:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_0

    iget v4, p0, Landroidx/constraintlayout/motion/widget/f;->b:I

    iget v5, p0, Landroidx/constraintlayout/motion/widget/l;->o:F

    iget v6, p0, Landroidx/constraintlayout/motion/widget/l;->w:F

    iget v7, p0, Landroidx/constraintlayout/motion/widget/l;->u:I

    iget v8, p0, Landroidx/constraintlayout/motion/widget/l;->x:F

    invoke-virtual/range {v3 .. v8}, La/e/a/a/a/p;->a(IFFIF)V

    goto/16 :goto_0

    :pswitch_6
    iget v1, p0, Landroidx/constraintlayout/motion/widget/l;->n:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_0

    iget v4, p0, Landroidx/constraintlayout/motion/widget/f;->b:I

    iget v5, p0, Landroidx/constraintlayout/motion/widget/l;->n:F

    iget v6, p0, Landroidx/constraintlayout/motion/widget/l;->w:F

    iget v7, p0, Landroidx/constraintlayout/motion/widget/l;->u:I

    iget v8, p0, Landroidx/constraintlayout/motion/widget/l;->x:F

    invoke-virtual/range {v3 .. v8}, La/e/a/a/a/p;->a(IFFIF)V

    goto/16 :goto_0

    :pswitch_7
    iget v1, p0, Landroidx/constraintlayout/motion/widget/l;->m:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_0

    iget v4, p0, Landroidx/constraintlayout/motion/widget/f;->b:I

    iget v5, p0, Landroidx/constraintlayout/motion/widget/l;->m:F

    iget v6, p0, Landroidx/constraintlayout/motion/widget/l;->w:F

    iget v7, p0, Landroidx/constraintlayout/motion/widget/l;->u:I

    iget v8, p0, Landroidx/constraintlayout/motion/widget/l;->x:F

    invoke-virtual/range {v3 .. v8}, La/e/a/a/a/p;->a(IFFIF)V

    goto/16 :goto_0

    :pswitch_8
    iget v1, p0, Landroidx/constraintlayout/motion/widget/l;->l:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_0

    iget v4, p0, Landroidx/constraintlayout/motion/widget/f;->b:I

    iget v5, p0, Landroidx/constraintlayout/motion/widget/l;->l:F

    iget v6, p0, Landroidx/constraintlayout/motion/widget/l;->w:F

    iget v7, p0, Landroidx/constraintlayout/motion/widget/l;->u:I

    iget v8, p0, Landroidx/constraintlayout/motion/widget/l;->x:F

    invoke-virtual/range {v3 .. v8}, La/e/a/a/a/p;->a(IFFIF)V

    goto/16 :goto_0

    :pswitch_9
    iget v1, p0, Landroidx/constraintlayout/motion/widget/l;->k:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_0

    iget v4, p0, Landroidx/constraintlayout/motion/widget/f;->b:I

    iget v5, p0, Landroidx/constraintlayout/motion/widget/l;->k:F

    iget v6, p0, Landroidx/constraintlayout/motion/widget/l;->w:F

    iget v7, p0, Landroidx/constraintlayout/motion/widget/l;->u:I

    iget v8, p0, Landroidx/constraintlayout/motion/widget/l;->x:F

    invoke-virtual/range {v3 .. v8}, La/e/a/a/a/p;->a(IFFIF)V

    goto/16 :goto_0

    :pswitch_a
    iget v1, p0, Landroidx/constraintlayout/motion/widget/l;->j:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_0

    iget v4, p0, Landroidx/constraintlayout/motion/widget/f;->b:I

    iget v5, p0, Landroidx/constraintlayout/motion/widget/l;->j:F

    iget v6, p0, Landroidx/constraintlayout/motion/widget/l;->w:F

    iget v7, p0, Landroidx/constraintlayout/motion/widget/l;->u:I

    iget v8, p0, Landroidx/constraintlayout/motion/widget/l;->x:F

    invoke-virtual/range {v3 .. v8}, La/e/a/a/a/p;->a(IFFIF)V

    goto/16 :goto_0

    :pswitch_b
    iget v1, p0, Landroidx/constraintlayout/motion/widget/l;->i:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_0

    iget v4, p0, Landroidx/constraintlayout/motion/widget/f;->b:I

    iget v5, p0, Landroidx/constraintlayout/motion/widget/l;->i:F

    iget v6, p0, Landroidx/constraintlayout/motion/widget/l;->w:F

    iget v7, p0, Landroidx/constraintlayout/motion/widget/l;->u:I

    iget v8, p0, Landroidx/constraintlayout/motion/widget/l;->x:F

    invoke-virtual/range {v3 .. v8}, La/e/a/a/a/p;->a(IFFIF)V

    goto/16 :goto_0

    :cond_4
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x4a771f66 -> :sswitch_b
        -0x4a771f65 -> :sswitch_a
        -0x490b9c39 -> :sswitch_9
        -0x490b9c38 -> :sswitch_8
        -0x490b9c37 -> :sswitch_7
        -0x3bab3dd3 -> :sswitch_6
        -0x3621dfb2 -> :sswitch_5
        -0x3621dfb1 -> :sswitch_4
        -0x266f082 -> :sswitch_3
        -0x42d1a3 -> :sswitch_2
        0x2382115 -> :sswitch_1
        0x589b15e -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public clone()Landroidx/constraintlayout/motion/widget/f;
    .locals 1

    new-instance v0, Landroidx/constraintlayout/motion/widget/l;

    invoke-direct {v0}, Landroidx/constraintlayout/motion/widget/l;-><init>()V

    invoke-virtual {v0, p0}, Landroidx/constraintlayout/motion/widget/l;->a(Landroidx/constraintlayout/motion/widget/f;)Landroidx/constraintlayout/motion/widget/f;

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Landroidx/constraintlayout/motion/widget/l;->clone()Landroidx/constraintlayout/motion/widget/f;

    move-result-object v0

    return-object v0
.end method
