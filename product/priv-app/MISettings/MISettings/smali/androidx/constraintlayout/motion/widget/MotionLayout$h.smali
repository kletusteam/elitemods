.class final enum Landroidx/constraintlayout/motion/widget/MotionLayout$h;
.super Ljava/lang/Enum;
.source "MotionLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/constraintlayout/motion/widget/MotionLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "h"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Landroidx/constraintlayout/motion/widget/MotionLayout$h;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Landroidx/constraintlayout/motion/widget/MotionLayout$h;

.field public static final enum b:Landroidx/constraintlayout/motion/widget/MotionLayout$h;

.field public static final enum c:Landroidx/constraintlayout/motion/widget/MotionLayout$h;

.field public static final enum d:Landroidx/constraintlayout/motion/widget/MotionLayout$h;

.field private static final synthetic e:[Landroidx/constraintlayout/motion/widget/MotionLayout$h;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Landroidx/constraintlayout/motion/widget/MotionLayout$h;

    const/4 v1, 0x0

    const-string v2, "UNDEFINED"

    invoke-direct {v0, v2, v1}, Landroidx/constraintlayout/motion/widget/MotionLayout$h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroidx/constraintlayout/motion/widget/MotionLayout$h;->a:Landroidx/constraintlayout/motion/widget/MotionLayout$h;

    new-instance v0, Landroidx/constraintlayout/motion/widget/MotionLayout$h;

    const/4 v2, 0x1

    const-string v3, "SETUP"

    invoke-direct {v0, v3, v2}, Landroidx/constraintlayout/motion/widget/MotionLayout$h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroidx/constraintlayout/motion/widget/MotionLayout$h;->b:Landroidx/constraintlayout/motion/widget/MotionLayout$h;

    new-instance v0, Landroidx/constraintlayout/motion/widget/MotionLayout$h;

    const/4 v3, 0x2

    const-string v4, "MOVING"

    invoke-direct {v0, v4, v3}, Landroidx/constraintlayout/motion/widget/MotionLayout$h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroidx/constraintlayout/motion/widget/MotionLayout$h;->c:Landroidx/constraintlayout/motion/widget/MotionLayout$h;

    new-instance v0, Landroidx/constraintlayout/motion/widget/MotionLayout$h;

    const/4 v4, 0x3

    const-string v5, "FINISHED"

    invoke-direct {v0, v5, v4}, Landroidx/constraintlayout/motion/widget/MotionLayout$h;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroidx/constraintlayout/motion/widget/MotionLayout$h;->d:Landroidx/constraintlayout/motion/widget/MotionLayout$h;

    const/4 v0, 0x4

    new-array v0, v0, [Landroidx/constraintlayout/motion/widget/MotionLayout$h;

    sget-object v5, Landroidx/constraintlayout/motion/widget/MotionLayout$h;->a:Landroidx/constraintlayout/motion/widget/MotionLayout$h;

    aput-object v5, v0, v1

    sget-object v1, Landroidx/constraintlayout/motion/widget/MotionLayout$h;->b:Landroidx/constraintlayout/motion/widget/MotionLayout$h;

    aput-object v1, v0, v2

    sget-object v1, Landroidx/constraintlayout/motion/widget/MotionLayout$h;->c:Landroidx/constraintlayout/motion/widget/MotionLayout$h;

    aput-object v1, v0, v3

    sget-object v1, Landroidx/constraintlayout/motion/widget/MotionLayout$h;->d:Landroidx/constraintlayout/motion/widget/MotionLayout$h;

    aput-object v1, v0, v4

    sput-object v0, Landroidx/constraintlayout/motion/widget/MotionLayout$h;->e:[Landroidx/constraintlayout/motion/widget/MotionLayout$h;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroidx/constraintlayout/motion/widget/MotionLayout$h;
    .locals 1

    const-class v0, Landroidx/constraintlayout/motion/widget/MotionLayout$h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Landroidx/constraintlayout/motion/widget/MotionLayout$h;

    return-object p0
.end method

.method public static values()[Landroidx/constraintlayout/motion/widget/MotionLayout$h;
    .locals 1

    sget-object v0, Landroidx/constraintlayout/motion/widget/MotionLayout$h;->e:[Landroidx/constraintlayout/motion/widget/MotionLayout$h;

    invoke-virtual {v0}, [Landroidx/constraintlayout/motion/widget/MotionLayout$h;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroidx/constraintlayout/motion/widget/MotionLayout$h;

    return-object v0
.end method
