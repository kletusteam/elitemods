.class public final Landroidx/media/v;
.super Ljava/lang/Object;
.source "MediaSessionManager.java"


# instance fields
.field a:Landroidx/media/w;


# direct methods
.method public constructor <init>(Landroid/media/session/MediaSessionManager$RemoteUserInfo;)V
    .locals 1
    .annotation build Landroidx/annotation/RequiresApi;
        value = 0x1c
    .end annotation

    .annotation build Landroidx/annotation/RestrictTo;
        value = {
            .enum Landroidx/annotation/RestrictTo$a;->b:Landroidx/annotation/RestrictTo$a;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroidx/media/x;

    invoke-direct {v0, p1}, Landroidx/media/x;-><init>(Landroid/media/session/MediaSessionManager$RemoteUserInfo;)V

    iput-object v0, p0, Landroidx/media/v;->a:Landroidx/media/w;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-lt v0, v1, :cond_0

    new-instance v0, Landroidx/media/x;

    invoke-direct {v0, p1, p2, p3}, Landroidx/media/x;-><init>(Ljava/lang/String;II)V

    iput-object v0, p0, Landroidx/media/v;->a:Landroidx/media/w;

    goto :goto_0

    :cond_0
    new-instance v0, Landroidx/media/y;

    invoke-direct {v0, p1, p2, p3}, Landroidx/media/y;-><init>(Ljava/lang/String;II)V

    iput-object v0, p0, Landroidx/media/v;->a:Landroidx/media/w;

    :goto_0
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    instance-of v0, p1, Landroidx/media/v;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    :cond_1
    iget-object v0, p0, Landroidx/media/v;->a:Landroidx/media/w;

    check-cast p1, Landroidx/media/v;

    iget-object p1, p1, Landroidx/media/v;->a:Landroidx/media/w;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Landroidx/media/v;->a:Landroidx/media/w;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method
