.class Landroidx/activity/result/f;
.super Landroidx/activity/result/d;
.source "ActivityResultRegistry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroidx/activity/result/g;->a(Ljava/lang/String;Landroidx/activity/result/a/a;Landroidx/activity/result/b;)Landroidx/activity/result/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/activity/result/d<",
        "TI;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:I

.field final synthetic c:Landroidx/activity/result/a/a;

.field final synthetic d:Landroidx/activity/result/g;


# direct methods
.method constructor <init>(Landroidx/activity/result/g;Ljava/lang/String;ILandroidx/activity/result/a/a;)V
    .locals 0

    iput-object p1, p0, Landroidx/activity/result/f;->d:Landroidx/activity/result/g;

    iput-object p2, p0, Landroidx/activity/result/f;->a:Ljava/lang/String;

    iput p3, p0, Landroidx/activity/result/f;->b:I

    iput-object p4, p0, Landroidx/activity/result/f;->c:Landroidx/activity/result/a/a;

    invoke-direct {p0}, Landroidx/activity/result/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Landroidx/activity/result/f;->d:Landroidx/activity/result/g;

    iget-object v1, p0, Landroidx/activity/result/f;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroidx/activity/result/g;->a(Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/Object;Landroidx/core/app/ActivityOptionsCompat;)V
    .locals 3
    .param p2    # Landroidx/core/app/ActivityOptionsCompat;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TI;",
            "Landroidx/core/app/ActivityOptionsCompat;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Landroidx/activity/result/f;->d:Landroidx/activity/result/g;

    iget-object v0, v0, Landroidx/activity/result/g;->e:Ljava/util/ArrayList;

    iget-object v1, p0, Landroidx/activity/result/f;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Landroidx/activity/result/f;->d:Landroidx/activity/result/g;

    iget v1, p0, Landroidx/activity/result/f;->b:I

    iget-object v2, p0, Landroidx/activity/result/f;->c:Landroidx/activity/result/a/a;

    invoke-virtual {v0, v1, v2, p1, p2}, Landroidx/activity/result/g;->a(ILandroidx/activity/result/a/a;Ljava/lang/Object;Landroidx/core/app/ActivityOptionsCompat;)V

    return-void
.end method
