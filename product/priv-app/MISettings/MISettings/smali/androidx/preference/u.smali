.class Landroidx/preference/u;
.super Landroidx/recyclerview/widget/o$a;
.source "PreferenceGroupAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroidx/preference/w;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Landroidx/preference/y$d;

.field final synthetic d:Landroidx/preference/w;


# direct methods
.method constructor <init>(Landroidx/preference/w;Ljava/util/List;Ljava/util/List;Landroidx/preference/y$d;)V
    .locals 0

    iput-object p1, p0, Landroidx/preference/u;->d:Landroidx/preference/w;

    iput-object p2, p0, Landroidx/preference/u;->a:Ljava/util/List;

    iput-object p3, p0, Landroidx/preference/u;->b:Ljava/util/List;

    iput-object p4, p0, Landroidx/preference/u;->c:Landroidx/preference/y$d;

    invoke-direct {p0}, Landroidx/recyclerview/widget/o$a;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Landroidx/preference/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public a(II)Z
    .locals 2

    iget-object v0, p0, Landroidx/preference/u;->c:Landroidx/preference/y$d;

    iget-object v1, p0, Landroidx/preference/u;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroidx/preference/Preference;

    iget-object v1, p0, Landroidx/preference/u;->b:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroidx/preference/Preference;

    invoke-virtual {v0, p1, p2}, Landroidx/preference/y$d;->a(Landroidx/preference/Preference;Landroidx/preference/Preference;)Z

    move-result p1

    return p1
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Landroidx/preference/u;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public b(II)Z
    .locals 2

    iget-object v0, p0, Landroidx/preference/u;->c:Landroidx/preference/y$d;

    iget-object v1, p0, Landroidx/preference/u;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroidx/preference/Preference;

    iget-object v1, p0, Landroidx/preference/u;->b:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroidx/preference/Preference;

    invoke-virtual {v0, p1, p2}, Landroidx/preference/y$d;->b(Landroidx/preference/Preference;Landroidx/preference/Preference;)Z

    move-result p1

    return p1
.end method
