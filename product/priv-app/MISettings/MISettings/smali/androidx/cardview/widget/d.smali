.class Landroidx/cardview/widget/d;
.super Ljava/lang/Object;
.source "CardViewApi21Impl.java"

# interfaces
.implements Landroidx/cardview/widget/h;


# annotations
.annotation build Landroidx/annotation/RequiresApi;
    value = 0x15
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private j(Landroidx/cardview/widget/g;)Landroidx/cardview/widget/i;
    .locals 0

    invoke-interface {p1}, Landroidx/cardview/widget/g;->d()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    check-cast p1, Landroidx/cardview/widget/i;

    return-object p1
.end method


# virtual methods
.method public a(Landroidx/cardview/widget/g;)F
    .locals 0

    invoke-interface {p1}, Landroidx/cardview/widget/g;->a()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getElevation()F

    move-result p1

    return p1
.end method

.method public a()V
    .locals 0

    return-void
.end method

.method public a(Landroidx/cardview/widget/g;F)V
    .locals 0

    invoke-direct {p0, p1}, Landroidx/cardview/widget/d;->j(Landroidx/cardview/widget/g;)Landroidx/cardview/widget/i;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroidx/cardview/widget/i;->a(F)V

    return-void
.end method

.method public a(Landroidx/cardview/widget/g;Landroid/content/Context;Landroid/content/res/ColorStateList;FFF)V
    .locals 0

    new-instance p2, Landroidx/cardview/widget/i;

    invoke-direct {p2, p3, p4}, Landroidx/cardview/widget/i;-><init>(Landroid/content/res/ColorStateList;F)V

    invoke-interface {p1, p2}, Landroidx/cardview/widget/g;->a(Landroid/graphics/drawable/Drawable;)V

    invoke-interface {p1}, Landroidx/cardview/widget/g;->a()Landroid/view/View;

    move-result-object p2

    const/4 p3, 0x1

    invoke-virtual {p2, p3}, Landroid/view/View;->setClipToOutline(Z)V

    invoke-virtual {p2, p5}, Landroid/view/View;->setElevation(F)V

    invoke-virtual {p0, p1, p6}, Landroidx/cardview/widget/d;->c(Landroidx/cardview/widget/g;F)V

    return-void
.end method

.method public a(Landroidx/cardview/widget/g;Landroid/content/res/ColorStateList;)V
    .locals 0
    .param p2    # Landroid/content/res/ColorStateList;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Landroidx/cardview/widget/d;->j(Landroidx/cardview/widget/g;)Landroidx/cardview/widget/i;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroidx/cardview/widget/i;->a(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public b(Landroidx/cardview/widget/g;)F
    .locals 0

    invoke-direct {p0, p1}, Landroidx/cardview/widget/d;->j(Landroidx/cardview/widget/g;)Landroidx/cardview/widget/i;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/cardview/widget/i;->c()F

    move-result p1

    return p1
.end method

.method public b(Landroidx/cardview/widget/g;F)V
    .locals 0

    invoke-interface {p1}, Landroidx/cardview/widget/g;->a()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/view/View;->setElevation(F)V

    return-void
.end method

.method public c(Landroidx/cardview/widget/g;)V
    .locals 1

    invoke-virtual {p0, p1}, Landroidx/cardview/widget/d;->d(Landroidx/cardview/widget/g;)F

    move-result v0

    invoke-virtual {p0, p1, v0}, Landroidx/cardview/widget/d;->c(Landroidx/cardview/widget/g;F)V

    return-void
.end method

.method public c(Landroidx/cardview/widget/g;F)V
    .locals 3

    invoke-direct {p0, p1}, Landroidx/cardview/widget/d;->j(Landroidx/cardview/widget/g;)Landroidx/cardview/widget/i;

    move-result-object v0

    invoke-interface {p1}, Landroidx/cardview/widget/g;->c()Z

    move-result v1

    invoke-interface {p1}, Landroidx/cardview/widget/g;->b()Z

    move-result v2

    invoke-virtual {v0, p2, v1, v2}, Landroidx/cardview/widget/i;->a(FZZ)V

    invoke-virtual {p0, p1}, Landroidx/cardview/widget/d;->f(Landroidx/cardview/widget/g;)V

    return-void
.end method

.method public d(Landroidx/cardview/widget/g;)F
    .locals 0

    invoke-direct {p0, p1}, Landroidx/cardview/widget/d;->j(Landroidx/cardview/widget/g;)Landroidx/cardview/widget/i;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/cardview/widget/i;->b()F

    move-result p1

    return p1
.end method

.method public e(Landroidx/cardview/widget/g;)Landroid/content/res/ColorStateList;
    .locals 0

    invoke-direct {p0, p1}, Landroidx/cardview/widget/d;->j(Landroidx/cardview/widget/g;)Landroidx/cardview/widget/i;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/cardview/widget/i;->a()Landroid/content/res/ColorStateList;

    move-result-object p1

    return-object p1
.end method

.method public f(Landroidx/cardview/widget/g;)V
    .locals 4

    invoke-interface {p1}, Landroidx/cardview/widget/g;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {p1, v0, v0, v0, v0}, Landroidx/cardview/widget/g;->a(IIII)V

    return-void

    :cond_0
    invoke-virtual {p0, p1}, Landroidx/cardview/widget/d;->d(Landroidx/cardview/widget/g;)F

    move-result v0

    invoke-virtual {p0, p1}, Landroidx/cardview/widget/d;->b(Landroidx/cardview/widget/g;)F

    move-result v1

    invoke-interface {p1}, Landroidx/cardview/widget/g;->b()Z

    move-result v2

    invoke-static {v0, v1, v2}, Landroidx/cardview/widget/j;->a(FFZ)F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    invoke-interface {p1}, Landroidx/cardview/widget/g;->b()Z

    move-result v3

    invoke-static {v0, v1, v3}, Landroidx/cardview/widget/j;->b(FFZ)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    invoke-interface {p1, v2, v0, v2, v0}, Landroidx/cardview/widget/g;->a(IIII)V

    return-void
.end method

.method public g(Landroidx/cardview/widget/g;)F
    .locals 1

    invoke-virtual {p0, p1}, Landroidx/cardview/widget/d;->b(Landroidx/cardview/widget/g;)F

    move-result p1

    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr p1, v0

    return p1
.end method

.method public h(Landroidx/cardview/widget/g;)F
    .locals 1

    invoke-virtual {p0, p1}, Landroidx/cardview/widget/d;->b(Landroidx/cardview/widget/g;)F

    move-result p1

    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr p1, v0

    return p1
.end method

.method public i(Landroidx/cardview/widget/g;)V
    .locals 1

    invoke-virtual {p0, p1}, Landroidx/cardview/widget/d;->d(Landroidx/cardview/widget/g;)F

    move-result v0

    invoke-virtual {p0, p1, v0}, Landroidx/cardview/widget/d;->c(Landroidx/cardview/widget/g;F)V

    return-void
.end method
