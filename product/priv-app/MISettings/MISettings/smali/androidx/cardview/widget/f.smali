.class Landroidx/cardview/widget/f;
.super Ljava/lang/Object;
.source "CardViewBaseImpl.java"

# interfaces
.implements Landroidx/cardview/widget/h;


# instance fields
.field final a:Landroid/graphics/RectF;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Landroidx/cardview/widget/f;->a:Landroid/graphics/RectF;

    return-void
.end method

.method private a(Landroid/content/Context;Landroid/content/res/ColorStateList;FFF)Landroidx/cardview/widget/j;
    .locals 7

    new-instance v6, Landroidx/cardview/widget/j;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    move-object v0, v6

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Landroidx/cardview/widget/j;-><init>(Landroid/content/res/Resources;Landroid/content/res/ColorStateList;FFF)V

    return-object v6
.end method

.method private j(Landroidx/cardview/widget/g;)Landroidx/cardview/widget/j;
    .locals 0

    invoke-interface {p1}, Landroidx/cardview/widget/g;->d()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    check-cast p1, Landroidx/cardview/widget/j;

    return-object p1
.end method


# virtual methods
.method public a(Landroidx/cardview/widget/g;)F
    .locals 0

    invoke-direct {p0, p1}, Landroidx/cardview/widget/f;->j(Landroidx/cardview/widget/g;)Landroidx/cardview/widget/j;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/cardview/widget/j;->f()F

    move-result p1

    return p1
.end method

.method public a()V
    .locals 1

    new-instance v0, Landroidx/cardview/widget/e;

    invoke-direct {v0, p0}, Landroidx/cardview/widget/e;-><init>(Landroidx/cardview/widget/f;)V

    sput-object v0, Landroidx/cardview/widget/j;->b:Landroidx/cardview/widget/j$a;

    return-void
.end method

.method public a(Landroidx/cardview/widget/g;F)V
    .locals 1

    invoke-direct {p0, p1}, Landroidx/cardview/widget/f;->j(Landroidx/cardview/widget/g;)Landroidx/cardview/widget/j;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroidx/cardview/widget/j;->a(F)V

    invoke-virtual {p0, p1}, Landroidx/cardview/widget/f;->f(Landroidx/cardview/widget/g;)V

    return-void
.end method

.method public a(Landroidx/cardview/widget/g;Landroid/content/Context;Landroid/content/res/ColorStateList;FFF)V
    .locals 6

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Landroidx/cardview/widget/f;->a(Landroid/content/Context;Landroid/content/res/ColorStateList;FFF)Landroidx/cardview/widget/j;

    move-result-object p2

    invoke-interface {p1}, Landroidx/cardview/widget/g;->b()Z

    move-result p3

    invoke-virtual {p2, p3}, Landroidx/cardview/widget/j;->a(Z)V

    invoke-interface {p1, p2}, Landroidx/cardview/widget/g;->a(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, p1}, Landroidx/cardview/widget/f;->f(Landroidx/cardview/widget/g;)V

    return-void
.end method

.method public a(Landroidx/cardview/widget/g;Landroid/content/res/ColorStateList;)V
    .locals 0
    .param p2    # Landroid/content/res/ColorStateList;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Landroidx/cardview/widget/f;->j(Landroidx/cardview/widget/g;)Landroidx/cardview/widget/j;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroidx/cardview/widget/j;->a(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public b(Landroidx/cardview/widget/g;)F
    .locals 0

    invoke-direct {p0, p1}, Landroidx/cardview/widget/f;->j(Landroidx/cardview/widget/g;)Landroidx/cardview/widget/j;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/cardview/widget/j;->b()F

    move-result p1

    return p1
.end method

.method public b(Landroidx/cardview/widget/g;F)V
    .locals 0

    invoke-direct {p0, p1}, Landroidx/cardview/widget/f;->j(Landroidx/cardview/widget/g;)Landroidx/cardview/widget/j;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroidx/cardview/widget/j;->c(F)V

    return-void
.end method

.method public c(Landroidx/cardview/widget/g;)V
    .locals 0

    return-void
.end method

.method public c(Landroidx/cardview/widget/g;F)V
    .locals 1

    invoke-direct {p0, p1}, Landroidx/cardview/widget/f;->j(Landroidx/cardview/widget/g;)Landroidx/cardview/widget/j;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroidx/cardview/widget/j;->b(F)V

    invoke-virtual {p0, p1}, Landroidx/cardview/widget/f;->f(Landroidx/cardview/widget/g;)V

    return-void
.end method

.method public d(Landroidx/cardview/widget/g;)F
    .locals 0

    invoke-direct {p0, p1}, Landroidx/cardview/widget/f;->j(Landroidx/cardview/widget/g;)Landroidx/cardview/widget/j;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/cardview/widget/j;->c()F

    move-result p1

    return p1
.end method

.method public e(Landroidx/cardview/widget/g;)Landroid/content/res/ColorStateList;
    .locals 0

    invoke-direct {p0, p1}, Landroidx/cardview/widget/f;->j(Landroidx/cardview/widget/g;)Landroidx/cardview/widget/j;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/cardview/widget/j;->a()Landroid/content/res/ColorStateList;

    move-result-object p1

    return-object p1
.end method

.method public f(Landroidx/cardview/widget/g;)V
    .locals 4

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-direct {p0, p1}, Landroidx/cardview/widget/f;->j(Landroidx/cardview/widget/g;)Landroidx/cardview/widget/j;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroidx/cardview/widget/j;->a(Landroid/graphics/Rect;)V

    invoke-virtual {p0, p1}, Landroidx/cardview/widget/f;->h(Landroidx/cardview/widget/g;)F

    move-result v1

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v1, v1

    invoke-virtual {p0, p1}, Landroidx/cardview/widget/f;->g(Landroidx/cardview/widget/g;)F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    invoke-interface {p1, v1, v2}, Landroidx/cardview/widget/g;->a(II)V

    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, v0, Landroid/graphics/Rect;->top:I

    iget v3, v0, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-interface {p1, v1, v2, v3, v0}, Landroidx/cardview/widget/g;->a(IIII)V

    return-void
.end method

.method public g(Landroidx/cardview/widget/g;)F
    .locals 0

    invoke-direct {p0, p1}, Landroidx/cardview/widget/f;->j(Landroidx/cardview/widget/g;)Landroidx/cardview/widget/j;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/cardview/widget/j;->d()F

    move-result p1

    return p1
.end method

.method public h(Landroidx/cardview/widget/g;)F
    .locals 0

    invoke-direct {p0, p1}, Landroidx/cardview/widget/f;->j(Landroidx/cardview/widget/g;)Landroidx/cardview/widget/j;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/cardview/widget/j;->e()F

    move-result p1

    return p1
.end method

.method public i(Landroidx/cardview/widget/g;)V
    .locals 2

    invoke-direct {p0, p1}, Landroidx/cardview/widget/f;->j(Landroidx/cardview/widget/g;)Landroidx/cardview/widget/j;

    move-result-object v0

    invoke-interface {p1}, Landroidx/cardview/widget/g;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/cardview/widget/j;->a(Z)V

    invoke-virtual {p0, p1}, Landroidx/cardview/widget/f;->f(Landroidx/cardview/widget/g;)V

    return-void
.end method
