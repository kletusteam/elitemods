.class public interface abstract Landroidx/appcompat/widget/L;
.super Ljava/lang/Object;
.source "DecorToolbar.java"


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$a;->c:Landroidx/annotation/RestrictTo$a;
    }
.end annotation


# virtual methods
.method public abstract a(IJ)Landroidx/core/view/L;
.end method

.method public abstract a(I)V
.end method

.method public abstract a(Landroidx/appcompat/widget/ScrollingTabContainerView;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract a()Z
.end method

.method public abstract b(I)V
.end method

.method public abstract b(Z)V
.end method

.method public abstract b()Z
.end method

.method public abstract c(I)V
.end method

.method public abstract c()Z
.end method

.method public abstract collapseActionView()V
.end method

.method public abstract d()Z
.end method

.method public abstract e()Z
.end method

.method public abstract f()V
.end method

.method public abstract g()Z
.end method

.method public abstract getTitle()Ljava/lang/CharSequence;
.end method

.method public abstract h()I
.end method

.method public abstract i()Landroid/view/ViewGroup;
.end method

.method public abstract j()Landroid/content/Context;
.end method

.method public abstract k()I
.end method

.method public abstract l()V
.end method

.method public abstract m()V
.end method

.method public abstract setIcon(I)V
.end method

.method public abstract setIcon(Landroid/graphics/drawable/Drawable;)V
.end method

.method public abstract setMenu(Landroid/view/Menu;Landroidx/appcompat/view/menu/s$a;)V
.end method

.method public abstract setMenuPrepared()V
.end method

.method public abstract setWindowCallback(Landroid/view/Window$Callback;)V
.end method

.method public abstract setWindowTitle(Ljava/lang/CharSequence;)V
.end method
