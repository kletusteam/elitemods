.class Landroidx/appcompat/app/G;
.super Landroidx/core/view/N;
.source "WindowDecorActionBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/appcompat/app/J;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroidx/appcompat/app/J;


# direct methods
.method constructor <init>(Landroidx/appcompat/app/J;)V
    .locals 0

    iput-object p1, p0, Landroidx/appcompat/app/G;->a:Landroidx/appcompat/app/J;

    invoke-direct {p0}, Landroidx/core/view/N;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Landroidx/appcompat/app/G;->a:Landroidx/appcompat/app/J;

    iget-boolean v0, p1, Landroidx/appcompat/app/J;->v:Z

    if-eqz v0, :cond_0

    iget-object p1, p1, Landroidx/appcompat/app/J;->j:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    iget-object p1, p0, Landroidx/appcompat/app/G;->a:Landroidx/appcompat/app/J;

    iget-object p1, p1, Landroidx/appcompat/app/J;->g:Landroidx/appcompat/widget/ActionBarContainer;

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    :cond_0
    iget-object p1, p0, Landroidx/appcompat/app/G;->a:Landroidx/appcompat/app/J;

    iget-object p1, p1, Landroidx/appcompat/app/J;->g:Landroidx/appcompat/widget/ActionBarContainer;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/ActionBarContainer;->setVisibility(I)V

    iget-object p1, p0, Landroidx/appcompat/app/G;->a:Landroidx/appcompat/app/J;

    iget-object p1, p1, Landroidx/appcompat/app/J;->g:Landroidx/appcompat/widget/ActionBarContainer;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/ActionBarContainer;->setTransitioning(Z)V

    iget-object p1, p0, Landroidx/appcompat/app/G;->a:Landroidx/appcompat/app/J;

    const/4 v0, 0x0

    iput-object v0, p1, Landroidx/appcompat/app/J;->A:La/a/d/i;

    invoke-virtual {p1}, Landroidx/appcompat/app/J;->p()V

    iget-object p1, p0, Landroidx/appcompat/app/G;->a:Landroidx/appcompat/app/J;

    iget-object p1, p1, Landroidx/appcompat/app/J;->f:Landroidx/appcompat/widget/ActionBarOverlayLayout;

    if-eqz p1, :cond_1

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->E(Landroid/view/View;)V

    :cond_1
    return-void
.end method
