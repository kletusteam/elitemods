.class Landroidx/core/provider/l;
.super Ljava/lang/Object;
.source "FontRequestWorker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/core/provider/l$a;
    }
.end annotation


# static fields
.field static final a:La/d/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/d/g<",
            "Ljava/lang/String;",
            "Landroid/graphics/Typeface;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/concurrent/ExecutorService;

.field static final c:Ljava/lang/Object;

.field static final d:La/d/i;
    .annotation build Landroidx/annotation/GuardedBy;
        value = "LOCK"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/d/i<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "La/g/e/a<",
            "Landroidx/core/provider/l$a;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, La/d/g;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, La/d/g;-><init>(I)V

    sput-object v0, Landroidx/core/provider/l;->a:La/d/g;

    const-string v0, "fonts-androidx"

    const/16 v1, 0xa

    const/16 v2, 0x2710

    invoke-static {v0, v1, v2}, Landroidx/core/provider/m;->a(Ljava/lang/String;II)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v0

    sput-object v0, Landroidx/core/provider/l;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroidx/core/provider/l;->c:Ljava/lang/Object;

    new-instance v0, La/d/i;

    invoke-direct {v0}, La/d/i;-><init>()V

    sput-object v0, Landroidx/core/provider/l;->d:La/d/i;

    return-void
.end method

.method private static a(Landroidx/core/provider/FontsContractCompat$a;)I
    .locals 5
    .param p0    # Landroidx/core/provider/FontsContractCompat$a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongConstant"
        }
    .end annotation

    invoke-virtual {p0}, Landroidx/core/provider/FontsContractCompat$a;->b()I

    move-result v0

    const/4 v1, -0x3

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroidx/core/provider/FontsContractCompat$a;->b()I

    move-result p0

    if-eq p0, v2, :cond_0

    return v1

    :cond_0
    const/4 p0, -0x2

    return p0

    :cond_1
    invoke-virtual {p0}, Landroidx/core/provider/FontsContractCompat$a;->a()[Landroidx/core/provider/FontsContractCompat$b;

    move-result-object p0

    if-eqz p0, :cond_5

    array-length v0, p0

    if-nez v0, :cond_2

    goto :goto_2

    :cond_2
    array-length v0, p0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v0, :cond_5

    aget-object v4, p0, v3

    invoke-virtual {v4}, Landroidx/core/provider/FontsContractCompat$b;->a()I

    move-result v4

    if-eqz v4, :cond_4

    if-gez v4, :cond_3

    goto :goto_1

    :cond_3
    move v1, v4

    :goto_1
    return v1

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_5
    :goto_2
    return v2
.end method

.method static a(Landroid/content/Context;Landroidx/core/provider/g;ILjava/util/concurrent/Executor;Landroidx/core/provider/c;)Landroid/graphics/Typeface;
    .locals 4
    .param p0    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Landroidx/core/provider/g;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroidx/core/provider/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-static {p1, p2}, Landroidx/core/provider/l;->a(Landroidx/core/provider/g;I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Landroidx/core/provider/l;->a:La/d/g;

    invoke-virtual {v1, v0}, La/d/g;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Typeface;

    if-eqz v1, :cond_0

    new-instance p0, Landroidx/core/provider/l$a;

    invoke-direct {p0, v1}, Landroidx/core/provider/l$a;-><init>(Landroid/graphics/Typeface;)V

    invoke-virtual {p4, p0}, Landroidx/core/provider/c;->a(Landroidx/core/provider/l$a;)V

    return-object v1

    :cond_0
    new-instance v1, Landroidx/core/provider/i;

    invoke-direct {v1, p4}, Landroidx/core/provider/i;-><init>(Landroidx/core/provider/c;)V

    sget-object p4, Landroidx/core/provider/l;->c:Ljava/lang/Object;

    monitor-enter p4

    :try_start_0
    sget-object v2, Landroidx/core/provider/l;->d:La/d/i;

    invoke-virtual {v2, v0}, La/d/i;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit p4

    return-object v3

    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v1, Landroidx/core/provider/l;->d:La/d/i;

    invoke-virtual {v1, v0, v2}, La/d/i;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit p4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance p4, Landroidx/core/provider/j;

    invoke-direct {p4, v0, p0, p1, p2}, Landroidx/core/provider/j;-><init>(Ljava/lang/String;Landroid/content/Context;Landroidx/core/provider/g;I)V

    if-nez p3, :cond_2

    sget-object p3, Landroidx/core/provider/l;->b:Ljava/util/concurrent/ExecutorService;

    :cond_2
    new-instance p0, Landroidx/core/provider/k;

    invoke-direct {p0, v0}, Landroidx/core/provider/k;-><init>(Ljava/lang/String;)V

    invoke-static {p3, p4, p0}, Landroidx/core/provider/m;->a(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Callable;La/g/e/a;)V

    return-object v3

    :catchall_0
    move-exception p0

    :try_start_1
    monitor-exit p4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p0
.end method

.method static a(Landroid/content/Context;Landroidx/core/provider/g;Landroidx/core/provider/c;II)Landroid/graphics/Typeface;
    .locals 2
    .param p0    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Landroidx/core/provider/g;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroidx/core/provider/c;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-static {p1, p3}, Landroidx/core/provider/l;->a(Landroidx/core/provider/g;I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Landroidx/core/provider/l;->a:La/d/g;

    invoke-virtual {v1, v0}, La/d/g;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Typeface;

    if-eqz v1, :cond_0

    new-instance p0, Landroidx/core/provider/l$a;

    invoke-direct {p0, v1}, Landroidx/core/provider/l$a;-><init>(Landroid/graphics/Typeface;)V

    invoke-virtual {p2, p0}, Landroidx/core/provider/c;->a(Landroidx/core/provider/l$a;)V

    return-object v1

    :cond_0
    const/4 v1, -0x1

    if-ne p4, v1, :cond_1

    invoke-static {v0, p0, p1, p3}, Landroidx/core/provider/l;->a(Ljava/lang/String;Landroid/content/Context;Landroidx/core/provider/g;I)Landroidx/core/provider/l$a;

    move-result-object p0

    invoke-virtual {p2, p0}, Landroidx/core/provider/c;->a(Landroidx/core/provider/l$a;)V

    iget-object p0, p0, Landroidx/core/provider/l$a;->a:Landroid/graphics/Typeface;

    return-object p0

    :cond_1
    new-instance v1, Landroidx/core/provider/h;

    invoke-direct {v1, v0, p0, p1, p3}, Landroidx/core/provider/h;-><init>(Ljava/lang/String;Landroid/content/Context;Landroidx/core/provider/g;I)V

    :try_start_0
    sget-object p0, Landroidx/core/provider/l;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {p0, v1, p4}, Landroidx/core/provider/m;->a(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Callable;I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroidx/core/provider/l$a;

    invoke-virtual {p2, p0}, Landroidx/core/provider/c;->a(Landroidx/core/provider/l$a;)V

    iget-object p0, p0, Landroidx/core/provider/l$a;->a:Landroid/graphics/Typeface;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    new-instance p0, Landroidx/core/provider/l$a;

    const/4 p1, -0x3

    invoke-direct {p0, p1}, Landroidx/core/provider/l$a;-><init>(I)V

    invoke-virtual {p2, p0}, Landroidx/core/provider/c;->a(Landroidx/core/provider/l$a;)V

    const/4 p0, 0x0

    return-object p0
.end method

.method static a(Ljava/lang/String;Landroid/content/Context;Landroidx/core/provider/g;I)Landroidx/core/provider/l$a;
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroidx/core/provider/g;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    sget-object v0, Landroidx/core/provider/l;->a:La/d/g;

    invoke-virtual {v0, p0}, La/d/g;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Typeface;

    if-eqz v0, :cond_0

    new-instance p0, Landroidx/core/provider/l$a;

    invoke-direct {p0, v0}, Landroidx/core/provider/l$a;-><init>(Landroid/graphics/Typeface;)V

    return-object p0

    :cond_0
    const/4 v0, 0x0

    :try_start_0
    invoke-static {p1, p2, v0}, Landroidx/core/provider/f;->a(Landroid/content/Context;Landroidx/core/provider/g;Landroid/os/CancellationSignal;)Landroidx/core/provider/FontsContractCompat$a;

    move-result-object p2
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-static {p2}, Landroidx/core/provider/l;->a(Landroidx/core/provider/FontsContractCompat$a;)I

    move-result v1

    if-eqz v1, :cond_1

    new-instance p0, Landroidx/core/provider/l$a;

    invoke-direct {p0, v1}, Landroidx/core/provider/l$a;-><init>(I)V

    return-object p0

    :cond_1
    invoke-virtual {p2}, Landroidx/core/provider/FontsContractCompat$a;->a()[Landroidx/core/provider/FontsContractCompat$b;

    move-result-object p2

    invoke-static {p1, v0, p2, p3}, La/g/a/d;->a(Landroid/content/Context;Landroid/os/CancellationSignal;[Landroidx/core/provider/FontsContractCompat$b;I)Landroid/graphics/Typeface;

    move-result-object p1

    if-eqz p1, :cond_2

    sget-object p2, Landroidx/core/provider/l;->a:La/d/g;

    invoke-virtual {p2, p0, p1}, La/d/g;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p0, Landroidx/core/provider/l$a;

    invoke-direct {p0, p1}, Landroidx/core/provider/l$a;-><init>(Landroid/graphics/Typeface;)V

    return-object p0

    :cond_2
    new-instance p0, Landroidx/core/provider/l$a;

    const/4 p1, -0x3

    invoke-direct {p0, p1}, Landroidx/core/provider/l$a;-><init>(I)V

    return-object p0

    :catch_0
    new-instance p0, Landroidx/core/provider/l$a;

    const/4 p1, -0x1

    invoke-direct {p0, p1}, Landroidx/core/provider/l$a;-><init>(I)V

    return-object p0
.end method

.method private static a(Landroidx/core/provider/g;I)Ljava/lang/String;
    .locals 1
    .param p0    # Landroidx/core/provider/g;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroidx/core/provider/g;->c()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "-"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
