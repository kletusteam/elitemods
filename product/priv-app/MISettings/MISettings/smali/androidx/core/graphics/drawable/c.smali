.class public interface abstract Landroidx/core/graphics/drawable/c;
.super Ljava/lang/Object;
.source "WrappedDrawable.java"


# annotations
.annotation build Landroidx/annotation/RestrictTo;
    value = {
        .enum Landroidx/annotation/RestrictTo$a;->c:Landroidx/annotation/RestrictTo$a;
    }
.end annotation


# virtual methods
.method public abstract getWrappedDrawable()Landroid/graphics/drawable/Drawable;
.end method

.method public abstract setWrappedDrawable(Landroid/graphics/drawable/Drawable;)V
.end method
