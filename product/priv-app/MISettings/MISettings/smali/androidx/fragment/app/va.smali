.class Landroidx/fragment/app/va;
.super Ljava/lang/Object;
.source "FragmentTransition.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroidx/fragment/app/xa;->b(Landroidx/fragment/app/Ha;Landroid/view/ViewGroup;Landroid/view/View;La/d/b;Landroidx/fragment/app/xa$b;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroidx/fragment/app/Fragment;

.field final synthetic b:Landroidx/fragment/app/Fragment;

.field final synthetic c:Z

.field final synthetic d:La/d/b;

.field final synthetic e:Landroid/view/View;

.field final synthetic f:Landroidx/fragment/app/Ha;

.field final synthetic g:Landroid/graphics/Rect;


# direct methods
.method constructor <init>(Landroidx/fragment/app/Fragment;Landroidx/fragment/app/Fragment;ZLa/d/b;Landroid/view/View;Landroidx/fragment/app/Ha;Landroid/graphics/Rect;)V
    .locals 0

    iput-object p1, p0, Landroidx/fragment/app/va;->a:Landroidx/fragment/app/Fragment;

    iput-object p2, p0, Landroidx/fragment/app/va;->b:Landroidx/fragment/app/Fragment;

    iput-boolean p3, p0, Landroidx/fragment/app/va;->c:Z

    iput-object p4, p0, Landroidx/fragment/app/va;->d:La/d/b;

    iput-object p5, p0, Landroidx/fragment/app/va;->e:Landroid/view/View;

    iput-object p6, p0, Landroidx/fragment/app/va;->f:Landroidx/fragment/app/Ha;

    iput-object p7, p0, Landroidx/fragment/app/va;->g:Landroid/graphics/Rect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Landroidx/fragment/app/va;->a:Landroidx/fragment/app/Fragment;

    iget-object v1, p0, Landroidx/fragment/app/va;->b:Landroidx/fragment/app/Fragment;

    iget-boolean v2, p0, Landroidx/fragment/app/va;->c:Z

    iget-object v3, p0, Landroidx/fragment/app/va;->d:La/d/b;

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroidx/fragment/app/xa;->a(Landroidx/fragment/app/Fragment;Landroidx/fragment/app/Fragment;ZLa/d/b;Z)V

    iget-object v0, p0, Landroidx/fragment/app/va;->e:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v1, p0, Landroidx/fragment/app/va;->f:Landroidx/fragment/app/Ha;

    iget-object v2, p0, Landroidx/fragment/app/va;->g:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v2}, Landroidx/fragment/app/Ha;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    :cond_0
    return-void
.end method
