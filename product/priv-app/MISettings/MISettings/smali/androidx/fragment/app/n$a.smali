.class Landroidx/fragment/app/n$a;
.super Landroidx/fragment/app/n$b;
.source "DefaultSpecialEffectsController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/fragment/app/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private c:Z

.field private d:Z

.field private e:Landroidx/fragment/app/I$a;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroidx/fragment/app/Pa$b;Landroidx/core/os/a;Z)V
    .locals 0
    .param p1    # Landroidx/fragment/app/Pa$b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroidx/core/os/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Landroidx/fragment/app/n$b;-><init>(Landroidx/fragment/app/Pa$b;Landroidx/core/os/a;)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Landroidx/fragment/app/n$a;->d:Z

    iput-boolean p3, p0, Landroidx/fragment/app/n$a;->c:Z

    return-void
.end method


# virtual methods
.method a(Landroid/content/Context;)Landroidx/fragment/app/I$a;
    .locals 4
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    iget-boolean v0, p0, Landroidx/fragment/app/n$a;->d:Z

    if-eqz v0, :cond_0

    iget-object p1, p0, Landroidx/fragment/app/n$a;->e:Landroidx/fragment/app/I$a;

    return-object p1

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/n$b;->b()Landroidx/fragment/app/Pa$b;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/Pa$b;->d()Landroidx/fragment/app/Fragment;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/n$b;->b()Landroidx/fragment/app/Pa$b;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/fragment/app/Pa$b;->c()Landroidx/fragment/app/Pa$b$b;

    move-result-object v1

    sget-object v2, Landroidx/fragment/app/Pa$b$b;->b:Landroidx/fragment/app/Pa$b$b;

    const/4 v3, 0x1

    if-ne v1, v2, :cond_1

    move v1, v3

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    iget-boolean v2, p0, Landroidx/fragment/app/n$a;->c:Z

    invoke-static {p1, v0, v1, v2}, Landroidx/fragment/app/I;->a(Landroid/content/Context;Landroidx/fragment/app/Fragment;ZZ)Landroidx/fragment/app/I$a;

    move-result-object p1

    iput-object p1, p0, Landroidx/fragment/app/n$a;->e:Landroidx/fragment/app/I$a;

    iput-boolean v3, p0, Landroidx/fragment/app/n$a;->d:Z

    iget-object p1, p0, Landroidx/fragment/app/n$a;->e:Landroidx/fragment/app/I$a;

    return-object p1
.end method
