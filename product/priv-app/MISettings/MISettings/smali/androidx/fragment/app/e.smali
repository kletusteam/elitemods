.class Landroidx/fragment/app/e;
.super Landroid/animation/AnimatorListenerAdapter;
.source "DefaultSpecialEffectsController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroidx/fragment/app/n;->a(Ljava/util/List;Ljava/util/List;ZLjava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/view/ViewGroup;

.field final synthetic b:Landroid/view/View;

.field final synthetic c:Z

.field final synthetic d:Landroidx/fragment/app/Pa$b;

.field final synthetic e:Landroidx/fragment/app/n$a;

.field final synthetic f:Landroidx/fragment/app/n;


# direct methods
.method constructor <init>(Landroidx/fragment/app/n;Landroid/view/ViewGroup;Landroid/view/View;ZLandroidx/fragment/app/Pa$b;Landroidx/fragment/app/n$a;)V
    .locals 0

    iput-object p1, p0, Landroidx/fragment/app/e;->f:Landroidx/fragment/app/n;

    iput-object p2, p0, Landroidx/fragment/app/e;->a:Landroid/view/ViewGroup;

    iput-object p3, p0, Landroidx/fragment/app/e;->b:Landroid/view/View;

    iput-boolean p4, p0, Landroidx/fragment/app/e;->c:Z

    iput-object p5, p0, Landroidx/fragment/app/e;->d:Landroidx/fragment/app/Pa$b;

    iput-object p6, p0, Landroidx/fragment/app/e;->e:Landroidx/fragment/app/n$a;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    iget-object p1, p0, Landroidx/fragment/app/e;->a:Landroid/view/ViewGroup;

    iget-object v0, p0, Landroidx/fragment/app/e;->b:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->endViewTransition(Landroid/view/View;)V

    iget-boolean p1, p0, Landroidx/fragment/app/e;->c:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Landroidx/fragment/app/e;->d:Landroidx/fragment/app/Pa$b;

    invoke-virtual {p1}, Landroidx/fragment/app/Pa$b;->c()Landroidx/fragment/app/Pa$b$b;

    move-result-object p1

    iget-object v0, p0, Landroidx/fragment/app/e;->b:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroidx/fragment/app/Pa$b$b;->a(Landroid/view/View;)V

    :cond_0
    iget-object p1, p0, Landroidx/fragment/app/e;->e:Landroidx/fragment/app/n$a;

    invoke-virtual {p1}, Landroidx/fragment/app/n$b;->a()V

    return-void
.end method
