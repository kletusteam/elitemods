.class Landroidx/fragment/app/n$b;
.super Ljava/lang/Object;
.source "DefaultSpecialEffectsController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/fragment/app/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private final a:Landroidx/fragment/app/Pa$b;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field private final b:Landroidx/core/os/a;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroidx/fragment/app/Pa$b;Landroidx/core/os/a;)V
    .locals 0
    .param p1    # Landroidx/fragment/app/Pa$b;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroidx/core/os/a;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroidx/fragment/app/n$b;->a:Landroidx/fragment/app/Pa$b;

    iput-object p2, p0, Landroidx/fragment/app/n$b;->b:Landroidx/core/os/a;

    return-void
.end method


# virtual methods
.method a()V
    .locals 2

    iget-object v0, p0, Landroidx/fragment/app/n$b;->a:Landroidx/fragment/app/Pa$b;

    iget-object v1, p0, Landroidx/fragment/app/n$b;->b:Landroidx/core/os/a;

    invoke-virtual {v0, v1}, Landroidx/fragment/app/Pa$b;->a(Landroidx/core/os/a;)V

    return-void
.end method

.method b()Landroidx/fragment/app/Pa$b;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Landroidx/fragment/app/n$b;->a:Landroidx/fragment/app/Pa$b;

    return-object v0
.end method

.method c()Landroidx/core/os/a;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    iget-object v0, p0, Landroidx/fragment/app/n$b;->b:Landroidx/core/os/a;

    return-object v0
.end method

.method d()Z
    .locals 3

    iget-object v0, p0, Landroidx/fragment/app/n$b;->a:Landroidx/fragment/app/Pa$b;

    invoke-virtual {v0}, Landroidx/fragment/app/Pa$b;->d()Landroidx/fragment/app/Fragment;

    move-result-object v0

    iget-object v0, v0, Landroidx/fragment/app/Fragment;->mView:Landroid/view/View;

    invoke-static {v0}, Landroidx/fragment/app/Pa$b$b;->b(Landroid/view/View;)Landroidx/fragment/app/Pa$b$b;

    move-result-object v0

    iget-object v1, p0, Landroidx/fragment/app/n$b;->a:Landroidx/fragment/app/Pa$b;

    invoke-virtual {v1}, Landroidx/fragment/app/Pa$b;->c()Landroidx/fragment/app/Pa$b$b;

    move-result-object v1

    if-eq v0, v1, :cond_1

    sget-object v2, Landroidx/fragment/app/Pa$b$b;->b:Landroidx/fragment/app/Pa$b$b;

    if-eq v0, v2, :cond_0

    if-eq v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method
