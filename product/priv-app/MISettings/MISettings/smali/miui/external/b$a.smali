.class public final enum Lmiui/external/b$a;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/external/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lmiui/external/b$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lmiui/external/b$a;

.field public static final enum b:Lmiui/external/b$a;

.field public static final enum c:Lmiui/external/b$a;

.field private static final synthetic d:[Lmiui/external/b$a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lmiui/external/b$a;

    const/4 v1, 0x0

    const-string v2, "GENERIC"

    invoke-direct {v0, v2, v1}, Lmiui/external/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiui/external/b$a;->a:Lmiui/external/b$a;

    new-instance v0, Lmiui/external/b$a;

    const/4 v2, 0x1

    const-string v3, "NO_SDK"

    invoke-direct {v0, v3, v2}, Lmiui/external/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiui/external/b$a;->b:Lmiui/external/b$a;

    new-instance v0, Lmiui/external/b$a;

    const/4 v3, 0x2

    const-string v4, "LOW_SDK_VERSION"

    invoke-direct {v0, v4, v3}, Lmiui/external/b$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiui/external/b$a;->c:Lmiui/external/b$a;

    const/4 v0, 0x3

    new-array v0, v0, [Lmiui/external/b$a;

    sget-object v4, Lmiui/external/b$a;->a:Lmiui/external/b$a;

    aput-object v4, v0, v1

    sget-object v1, Lmiui/external/b$a;->b:Lmiui/external/b$a;

    aput-object v1, v0, v2

    sget-object v1, Lmiui/external/b$a;->c:Lmiui/external/b$a;

    aput-object v1, v0, v3

    sput-object v0, Lmiui/external/b$a;->d:[Lmiui/external/b$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmiui/external/b$a;
    .locals 1

    const-class v0, Lmiui/external/b$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lmiui/external/b$a;

    return-object p0
.end method

.method public static values()[Lmiui/external/b$a;
    .locals 1

    sget-object v0, Lmiui/external/b$a;->d:[Lmiui/external/b$a;

    invoke-virtual {v0}, [Lmiui/external/b$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmiui/external/b$a;

    return-object v0
.end method
