.class public Lmiui/external/Application;
.super Landroid/app/Application;

# interfaces
.implements Lmiui/external/b;


# instance fields
.field private a:Z

.field private b:Z

.field private c:Lmiui/external/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    invoke-direct {p0}, Lmiui/external/Application;->f()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-direct {p0}, Lmiui/external/Application;->e()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/external/Application;->a:Z

    return-void
.end method

.method private a(Ljava/lang/String;I)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MIUI SDK encounter errors, please contact miuisdk@xiaomi.com for support. phase: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " code: "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "miuisdk"

    invoke-static {p2, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object p1, Lmiui/external/b$a;->a:Lmiui/external/b$a;

    invoke-static {p1}, Lmiui/external/h;->a(Lmiui/external/b$a;)V

    return-void
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 2

    :goto_0
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    instance-of v0, p1, Ljava/lang/reflect/InvocationTargetException;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    goto :goto_0

    :cond_1
    instance-of v0, p1, Ljava/lang/ExceptionInInitializerError;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    goto :goto_0

    :cond_2
    :goto_1
    const-string v0, "miuisdk"

    const-string v1, "MIUI SDK encounter errors, please contact miuisdk@xiaomi.com for support."

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    sget-object p1, Lmiui/external/b$a;->a:Lmiui/external/b$a;

    invoke-static {p1}, Lmiui/external/h;->a(Lmiui/external/b$a;)V

    return-void
.end method

.method private e()Z
    .locals 8

    const-string v0, "initialize"

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-static {}, Lmiui/external/c;->a()Ljava/lang/Class;

    move-result-object v3

    const/4 v4, 0x2

    new-array v5, v4, [Ljava/lang/Class;

    const-class v6, Landroid/app/Application;

    aput-object v6, v5, v1

    const-class v6, Ljava/util/Map;

    const/4 v7, 0x1

    aput-object v6, v5, v7

    invoke-virtual {v3, v0, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    const/4 v5, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v1

    aput-object v2, v4, v7

    invoke-virtual {v3, v5, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v0, v2}, Lmiui/external/Application;->a(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    :cond_0
    return v7

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lmiui/external/Application;->a(Ljava/lang/Throwable;)V

    return v1
.end method

.method private f()Z
    .locals 5

    const-string v0, "com.miui.core"

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lmiui/external/i;->a()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "miui"

    const/4 v3, 0x0

    invoke-static {v3, v0, v2}, Lmiui/external/i;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0}, Lmiui/external/i;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-class v4, Lmiui/external/Application;

    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    invoke-static {v2, v3, v0, v4}, Lmiui/external/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lmiui/external/b$a;->b:Lmiui/external/b$a;

    invoke-static {v0}, Lmiui/external/h;->a(Lmiui/external/b$a;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    :cond_0
    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lmiui/external/Application;->a(Ljava/lang/Throwable;)V

    return v1
.end method

.method private g()Z
    .locals 7

    const-string v0, "start"

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-static {}, Lmiui/external/c;->a()Ljava/lang/Class;

    move-result-object v3

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Class;

    const-class v6, Ljava/util/Map;

    aput-object v6, v5, v1

    invoke-virtual {v3, v0, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    const/4 v5, 0x0

    new-array v6, v4, [Ljava/lang/Object;

    aput-object v2, v6, v1

    invoke-virtual {v3, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v4, :cond_0

    sget-object v0, Lmiui/external/b$a;->c:Lmiui/external/b$a;

    invoke-static {v0}, Lmiui/external/h;->a(Lmiui/external/b$a;)V

    return v1

    :cond_0
    if-eqz v2, :cond_1

    invoke-direct {p0, v0, v2}, Lmiui/external/Application;->a(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    :cond_1
    return v4

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lmiui/external/Application;->a(Ljava/lang/Throwable;)V

    return v1
.end method


# virtual methods
.method public a()Lmiui/external/a;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method final a(I)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-super {p0, p1}, Landroid/app/Application;->onTrimMemory(I)V

    goto/32 :goto_0

    nop
.end method

.method final a(Landroid/content/res/Configuration;)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-super {p0, p1}, Landroid/app/Application;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    goto/32 :goto_0

    nop
.end method

.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/app/Application;->attachBaseContext(Landroid/content/Context;)V

    iget-boolean p1, p0, Lmiui/external/Application;->a:Z

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-direct {p0}, Lmiui/external/Application;->g()Z

    move-result p1

    if-nez p1, :cond_1

    return-void

    :cond_1
    invoke-virtual {p0}, Lmiui/external/Application;->a()Lmiui/external/a;

    move-result-object p1

    iput-object p1, p0, Lmiui/external/Application;->c:Lmiui/external/a;

    iget-object p1, p0, Lmiui/external/Application;->c:Lmiui/external/a;

    if-eqz p1, :cond_2

    invoke-virtual {p1, p0}, Lmiui/external/a;->a(Lmiui/external/Application;)V

    :cond_2
    const/4 p1, 0x1

    iput-boolean p1, p0, Lmiui/external/Application;->b:Z

    return-void
.end method

.method final b()V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    goto/32 :goto_0

    nop
.end method

.method final c()V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-super {p0}, Landroid/app/Application;->onLowMemory()V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method final d()V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    iget-object v0, p0, Lmiui/external/Application;->c:Lmiui/external/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lmiui/external/a;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lmiui/external/Application;->a(Landroid/content/res/Configuration;)V

    :goto_0
    return-void
.end method

.method public final onCreate()V
    .locals 1

    iget-boolean v0, p0, Lmiui/external/Application;->b:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lmiui/external/Application;->c:Lmiui/external/a;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lmiui/external/a;->a()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lmiui/external/Application;->b()V

    :goto_0
    return-void
.end method

.method public final onLowMemory()V
    .locals 1

    iget-object v0, p0, Lmiui/external/Application;->c:Lmiui/external/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiui/external/a;->onLowMemory()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lmiui/external/Application;->c()V

    :goto_0
    return-void
.end method

.method public final onTerminate()V
    .locals 1

    iget-object v0, p0, Lmiui/external/Application;->c:Lmiui/external/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiui/external/a;->b()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lmiui/external/Application;->d()V

    :goto_0
    return-void
.end method

.method public final onTrimMemory(I)V
    .locals 1

    iget-object v0, p0, Lmiui/external/Application;->c:Lmiui/external/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lmiui/external/a;->onTrimMemory(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lmiui/external/Application;->a(I)V

    :goto_0
    return-void
.end method
