.class public Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;
.super Landroid/widget/FrameLayout;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$H;,
        Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "settings_element"

.field private static final codePositionAfterEvent:I = 0x65

.field private static final init:I = 0x64

.field private static final nameElement:[Ljava/lang/String;


# instance fields
.field private CenterCameraEnable:Z

.field private final defSettings:Ljava/lang/String;

.field private isDoubleStatusBar:Z

.field private isDrip:Z

.field private isNight:Z

.field private mBorderZoneHeight:F

.field private final mContext:Landroid/content/Context;

.field private mElementHeight:F

.field private mElementWidth:F

.field private final mElements:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;",
            ">;"
        }
    .end annotation
.end field

.field private final mHandler:Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$H;

.field private mHeight:F

.field mLeftCenterZoneEnd:F

.field private mMargins:F

.field private mMaxWidth:F

.field mRightCenterZoneStart:F

.field private settingsString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const-string v0, "elem_status"

    const-string v1, "elem_clock"

    const-string v2, "elem_bat"

    const-string v3, "elem_net1"

    const-string v4, "elem_net2"

    const-string v5, "elem_wifi"

    const-string v6, "elem_notif"

    const-string v7, "elem_speed"

    const-string v8, "elem_weather"

    const-string v9, "elem_date"

    filled-new-array/range {v0 .. v9}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->nameElement:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->CenterCameraEnable:Z

    const-string v0, "elem_clock.1;elem_status.2;elem_notif.3;elem_weather.4;elem_date.16;elem_speed.15;elem_net1.14;elem_net2.13;elem_wifi.12;elem_bat.11;"

    iput-object v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->defSettings:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElements:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$H;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$H;-><init>(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$1;)V

    iput-object v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mHandler:Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$H;

    invoke-direct {p0}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->init()V

    return-void
.end method

.method private DrawableToID(Ljava/lang/String;)I
    .locals 3

    iget-object v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "drawable"

    invoke-virtual {v0, p1, v2, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private IDtoID(Ljava/lang/String;)I
    .locals 3

    iget-object v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "id"

    invoke-virtual {v0, p1, v2, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;)F
    .locals 1

    iget v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mMargins:F

    return v0
.end method

.method static synthetic access$1200(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;)F
    .locals 1

    iget v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mMaxWidth:F

    return v0
.end method

.method static synthetic access$1300(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;)F
    .locals 1

    iget v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElementWidth:F

    return v0
.end method

.method static synthetic access$1400(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;)F
    .locals 1

    iget v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mHeight:F

    return v0
.end method

.method static synthetic access$1500(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;)F
    .locals 1

    iget v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElementHeight:F

    return v0
.end method

.method static synthetic access$1600(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;I)Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->getElementOnPosition(I)Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;FZ)I
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->getEmptyPozition(FZ)I

    move-result v0

    return v0
.end method

.method static synthetic access$1800(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->codePositionAfterEvent()V

    return-void
.end method

.method static synthetic access$1900(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->isDoubleStatusBar:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;FZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->upPosition(FZ)V

    return-void
.end method

.method static synthetic access$2100(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;Z)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->getFlagAllElements(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2200(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;FZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->downPosition(FZ)V

    return-void
.end method

.method static synthetic access$2300(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;FFZZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->sendMovePosition(FFZZ)V

    return-void
.end method

.method static synthetic access$2700(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElements:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->init()V

    return-void
.end method

.method static synthetic access$900(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;)Z
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->getTouchAllElements()Z

    move-result v0

    return v0
.end method

.method private codePosition()V
    .locals 6

    const-string/jumbo v0, "status_bar_elem_poz"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElements:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;

    invoke-static {v3}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->access$100(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v3}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->access$300(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ";"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->settingsString:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :try_start_0
    iget-object v2, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v0, v3}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    iget-object v3, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :goto_1
    iget-object v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    const-string/jumbo v3, "my.settings.intent.element_position_change.CHANGE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->settingsString:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method private codePositionAfterEvent()V
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->getFlagAllElements(Z)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->getTouchAllElements()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->codePosition()V

    goto :goto_1

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mHandler:Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$H;

    const/16 v1, 0x65

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$H;->sendEmptyMessageDelayed(IJ)Z

    :goto_1
    return-void
.end method

.method private createView(Ljava/lang/String;)Landroid/widget/ImageView;
    .locals 3

    new-instance v0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;

    iget-object v1, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;-><init>(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->DrawableToID(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    invoke-direct {p0, p1}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->IDtoID(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setId(I)V

    return-object v0
.end method

.method private decodePosition()V
    .locals 14

    const-string v0, ";"

    const-string v1, "."

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string/jumbo v4, "status_bar_elem_poz"

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "elem_clock.1;elem_status.2;elem_notif.3;elem_weather.4;elem_date.16;elem_speed.15;elem_net1.14;elem_net2.13;elem_wifi.12;elem_bat.11;"

    if-nez v3, :cond_0

    iget-object v6, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mContext:Landroid/content/Context;

    invoke-static {v6}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-interface {v6, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    iput-object v3, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->settingsString:Ljava/lang/String;

    const-string v6, "elem_date"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v3, "elem_clock.1;elem_status.2;elem_notif.3;elem_weather.4;elem_date.16;elem_speed.15;elem_net1.14;elem_net2.13;elem_wifi.12;elem_bat.11;"

    :cond_1
    const/4 v6, 0x0

    :goto_0
    iget-object v7, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElements:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x1

    if-ge v6, v7, :cond_7

    :try_start_0
    invoke-virtual {v3, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    add-int/2addr v7, v9

    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v3, v7, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v3, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v3, v8, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    nop

    invoke-direct {p0, v10}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->getElementForName(Ljava/lang/String;)Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;

    move-result-object v11

    iget-boolean v12, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->isDrip:Z

    const-string v13, "elem_clock"

    if-eqz v12, :cond_2

    iget-boolean v12, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->isDoubleStatusBar:Z

    if-eqz v12, :cond_3

    :cond_2
    iget-boolean v12, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->isDoubleStatusBar:Z

    if-eqz v12, :cond_4

    if-nez v7, :cond_4

    invoke-virtual {v10, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_4

    :cond_3
    move v12, v9

    goto :goto_1

    :cond_4
    move v12, v8

    :goto_1
    move v2, v12

    invoke-virtual {v11, v7}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->setPosition(I)V

    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v12

    add-int/2addr v12, v9

    invoke-virtual {v3, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    iget-boolean v12, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->isDoubleStatusBar:Z

    if-nez v12, :cond_5

    iget-boolean v12, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->isDrip:Z

    if-nez v12, :cond_5

    invoke-static {v11, v9}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->access$202(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;Z)Z

    goto :goto_2

    :cond_5
    invoke-virtual {v10, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    iget-boolean v12, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->isDoubleStatusBar:Z

    if-eqz v12, :cond_6

    invoke-static {v11, v9}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->access$202(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;Z)Z

    :cond_6
    :goto_2
    invoke-virtual {v11, v8}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->moveToPosition(Z)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "settings_element"

    const-string v7, "decodePosition: first start"

    invoke-static {v1, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_1
    iget-object v1, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v4, v5}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    nop

    invoke-direct {p0}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->decodePosition()V

    return-void

    :catch_1
    move-exception v1

    iget-object v7, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-direct {p0}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->decodePosition()V

    return-void

    :cond_7
    if-eqz v2, :cond_8

    invoke-direct {p0, v8}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->getElementOnPosition(I)Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;

    move-result-object v0

    if-eqz v0, :cond_8

    const/16 v1, 0x33

    invoke-virtual {v0, v1}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->setPosition(I)V

    const/4 v1, 0x0

    invoke-direct {p0, v1, v9}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->getEmptyPozition(FZ)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->setPosition(I)V

    invoke-virtual {v0, v8}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->moveToPosition(Z)V

    invoke-direct {p0}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->codePosition()V

    :cond_8
    return-void
.end method

.method private downPosition(FZ)V
    .locals 6

    invoke-direct {p0, p1, p2}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->sectorOfdX(FZ)I

    move-result v0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->getEmptyPozition(FZ)I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElements:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;

    invoke-static {v3}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->access$300(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;)I

    move-result v4

    if-le v4, v0, :cond_0

    add-int/lit8 v5, v0, 0xa

    if-ge v4, v5, :cond_0

    if-le v4, v1, :cond_0

    invoke-static {v3}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->access$310(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;)I

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->moveToPosition(Z)V

    :cond_0
    goto :goto_0

    :cond_1
    return-void
.end method

.method private getBackPicture()Landroid/graphics/drawable/Drawable;
    .locals 18

    move-object/from16 v0, p0

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0xa

    invoke-static {v1, v2}, Landroid/Utils/ImageUtils;->convertDpToPx(Landroid/content/Context;I)I

    move-result v1

    const/4 v2, 0x2

    mul-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-eqz v3, :cond_2

    iget v4, v0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mMaxWidth:F

    float-to-int v4, v4

    iget v5, v0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mHeight:F

    add-float/2addr v5, v1

    float-to-int v5, v5

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    iget-boolean v5, v0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->isNight:Z

    if-eqz v5, :cond_0

    const-string v5, "#40ffffff"

    goto :goto_0

    :cond_0
    const-string v5, "#40000000"

    :goto_0
    invoke-static {v5}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v5

    const-string v6, "#ff00e2ff"

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    iget-boolean v7, v0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->isNight:Z

    if-eqz v7, :cond_1

    const-string v7, "#ffffffff"

    goto :goto_1

    :cond_1
    const-string v7, "#ff000000"

    :goto_1
    invoke-static {v7}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v7

    new-instance v8, Landroid/graphics/Canvas;

    invoke-direct {v8, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v9, Landroid/graphics/Paint;

    invoke-direct {v9}, Landroid/graphics/Paint;-><init>()V

    move-object v15, v9

    invoke-virtual {v15, v5}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v9, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v15, v9}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/4 v9, 0x0

    iget v10, v0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mHeight:F

    const/high16 v16, 0x40000000    # 2.0f

    div-float v11, v10, v16

    iget v12, v0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mBorderZoneHeight:F

    div-float v13, v12, v16

    sub-float/2addr v11, v13

    float-to-int v11, v11

    int-to-float v11, v11

    div-float v13, v1, v16

    add-float/2addr v11, v13

    iget v13, v0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mMaxWidth:F

    float-to-int v13, v13

    int-to-float v13, v13

    div-float v10, v10, v16

    add-float/2addr v10, v12

    float-to-int v10, v10

    int-to-float v10, v10

    div-float v12, v1, v16

    add-float/2addr v12, v10

    move v10, v11

    move v11, v13

    move-object v13, v15

    invoke-virtual/range {v8 .. v13}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    invoke-virtual {v15, v7}, Landroid/graphics/Paint;->setColor(I)V

    const/high16 v9, 0x40c00000    # 6.0f

    invoke-virtual {v15, v9}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget v9, v0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mMaxWidth:F

    div-float v10, v9, v16

    iget v11, v0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElementWidth:F

    sub-float/2addr v10, v11

    iput v10, v0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mLeftCenterZoneEnd:F

    div-float v10, v9, v16

    add-float/2addr v10, v11

    iput v10, v0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mRightCenterZoneStart:F

    div-float v10, v9, v16

    const/high16 v17, 0x40400000    # 3.0f

    sub-float v11, v10, v17

    div-float v12, v1, v16

    div-float v9, v9, v16

    sub-float v13, v9, v17

    iget v9, v0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mHeight:F

    div-float v9, v9, v16

    iget v10, v0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mBorderZoneHeight:F

    div-float v10, v10, v16

    sub-float/2addr v9, v10

    div-float v10, v1, v16

    add-float v14, v9, v10

    move-object v10, v8

    move-object v9, v15

    invoke-virtual/range {v10 .. v15}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget v10, v0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mMaxWidth:F

    div-float v11, v10, v16

    sub-float v11, v11, v17

    iget v12, v0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mHeight:F

    div-float v13, v12, v16

    iget v14, v0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mBorderZoneHeight:F

    add-float/2addr v13, v14

    div-float v14, v1, v16

    add-float/2addr v13, v14

    div-float v10, v10, v16

    sub-float v14, v10, v17

    div-float v10, v1, v16

    add-float v15, v12, v10

    move-object v10, v8

    move v12, v13

    move v13, v14

    move v14, v15

    move-object v15, v9

    invoke-virtual/range {v10 .. v15}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    new-instance v10, Landroid/graphics/drawable/LayerDrawable;

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    const/4 v11, 0x0

    aput-object v3, v2, v11

    const/4 v11, 0x1

    new-instance v12, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-direct {v12, v13, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v12, v2, v11

    invoke-direct {v10, v2}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    return-object v10

    :cond_2
    const/4 v2, 0x0

    return-object v2
.end method

.method private getElementForName(Ljava/lang/String;)Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;
    .locals 3

    iget-object v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElements:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;

    invoke-static {v1}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->access$100(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_0
    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElements:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;

    return-object v0
.end method

.method private getElementOnPosition(I)Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;
    .locals 3

    iget-object v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElements:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;

    invoke-static {v1}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->access$300(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;)I

    move-result v2

    if-ne v2, p1, :cond_0

    return-object v1

    :cond_0
    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private getEmptyPozition(FZ)I
    .locals 6

    invoke-direct {p0, p1, p2}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->sectorOfdX(FZ)I

    move-result v0

    add-int/lit8 v1, v0, 0x1

    :goto_0
    iget-object v2, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElements:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/2addr v2, v0

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_3

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElements:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;

    invoke-static {v4}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->access$300(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;)I

    move-result v5

    if-ne v5, v1, :cond_0

    const/4 v2, 0x1

    goto :goto_2

    :cond_0
    goto :goto_1

    :cond_1
    :goto_2
    if-nez v2, :cond_2

    return v1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElements:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    return v1
.end method

.method private getFlagAllElements(Z)Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElements:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;

    invoke-static {v2}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->access$400(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;)Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    :goto_1
    goto :goto_0

    :cond_2
    return v0
.end method

.method private getPositionOfDx(FZ)I
    .locals 5

    const/16 v0, 0x33

    iget-object v1, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElements:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;

    invoke-virtual {v2}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->getX()F

    move-result v3

    invoke-static {v2}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->access$500(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v2}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->access$700(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;)Z

    move-result v4

    if-ne v4, p2, :cond_0

    cmpl-float v4, p1, v3

    if-ltz v4, :cond_0

    iget v4, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElementWidth:F

    add-float/2addr v4, v3

    cmpg-float v4, p1, v4

    if-gtz v4, :cond_0

    invoke-static {v2}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->access$300(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;)I

    move-result v0

    :cond_0
    goto :goto_0

    :cond_1
    return v0
.end method

.method private getTouchAllElements()Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElements:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;

    invoke-static {v2}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->access$500(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    goto :goto_0

    :cond_1
    return v0
.end method

.method private init()V
    .locals 11

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->isDrip:Z

    const-string/jumbo v1, "status_bar_duoble_enabled"

    invoke-static {v1}, Landroid/preference/SettingsEliteHelper;->getBoolofSettings(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->isDoubleStatusBar:Z

    invoke-virtual {p0}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mMaxWidth:F

    invoke-virtual {p0}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mHeight:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mMaxWidth:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    goto/16 :goto_1

    :cond_0
    iget-object v1, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {v1, v2}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v3, v3

    iget v4, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mMaxWidth:F

    sub-float/2addr v3, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v3, v5

    iput v3, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mMargins:F

    const/high16 v3, 0x41800000    # 16.0f

    div-float/2addr v4, v3

    iput v4, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElementWidth:F

    const/high16 v3, 0x41200000    # 10.0f

    iget-object v4, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v4, v3

    iput v4, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mBorderZoneHeight:F

    iget v3, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mHeight:F

    div-float/2addr v3, v5

    sub-float/2addr v3, v4

    iput v3, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElementHeight:F

    sget-object v3, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->nameElement:[Ljava/lang/String;

    array-length v4, v3

    move v5, v0

    :goto_0
    if-ge v5, v4, :cond_1

    aget-object v6, v3, v5

    invoke-direct {p0, v6}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->createView(Ljava/lang/String;)Landroid/widget/ImageView;

    move-result-object v7

    check-cast v7, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;

    invoke-static {v7, v6}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->access$102(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;Ljava/lang/String;)Ljava/lang/String;

    iget-object v8, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElements:Ljava/util/ArrayList;

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v8, Landroid/widget/FrameLayout$LayoutParams;

    iget v9, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElementWidth:F

    float-to-int v9, v9

    iget v10, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElementHeight:F

    float-to-int v10, v10

    invoke-direct {v8, v9, v10}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v7, v8}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "uimode"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/UiModeManager;

    invoke-virtual {v3}, Landroid/app/UiModeManager;->getNightMode()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    const/4 v0, 0x1

    :cond_2
    iput-boolean v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->isNight:Z

    invoke-virtual {p0}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->getBackPicture()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-direct {p0}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->decodePosition()V

    return-void

    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mHandler:Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$H;

    const/16 v1, 0x64

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$H;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method private sectorOfdX(FZ)I
    .locals 3

    const/high16 v0, 0x40000000    # 2.0f

    if-eqz p2, :cond_1

    iget v1, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mMaxWidth:F

    div-float/2addr v1, v0

    iget v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElementWidth:F

    sub-float/2addr v1, v0

    cmpg-float v0, p1, v1

    if-gez v0, :cond_0

    const/16 v0, 0x14

    goto :goto_0

    :cond_0
    const/16 v0, 0x1e

    :goto_0
    goto :goto_1

    :cond_1
    iget v1, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElementWidth:F

    div-float/2addr v1, v0

    add-float/2addr v1, p1

    iget v2, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mMaxWidth:F

    div-float/2addr v2, v0

    const/high16 v0, 0x40400000    # 3.0f

    sub-float/2addr v2, v0

    cmpg-float v0, v1, v2

    if-gez v0, :cond_2

    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/16 v0, 0xa

    :goto_1
    return v0
.end method

.method private sendMovePosition(FFZZ)V
    .locals 3

    if-eqz p4, :cond_2

    iget v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mHeight:F

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iget v2, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElementHeight:F

    div-float/2addr v2, v1

    sub-float/2addr v0, v2

    cmpl-float v0, p2, v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-lez v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    move v1, v2

    :goto_1
    invoke-direct {p0, p1, v1}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->downPosition(FZ)V

    invoke-direct {p0, p1, v0}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->upPosition(FZ)V

    goto :goto_3

    :cond_2
    iget-object v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElements:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;

    invoke-static {v1, p1, p2, p3}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->access$600(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;FFZ)V

    goto :goto_2

    :cond_3
    :goto_3
    return-void
.end method

.method private upPosition(FZ)V
    .locals 5

    iget v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElementWidth:F

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    add-float/2addr v0, p1

    invoke-direct {p0, v0, p2}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->getPositionOfDx(FZ)I

    move-result v0

    const/16 v1, 0x33

    if-eq v0, v1, :cond_1

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElements:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;

    invoke-static {v2}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->access$400(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v2}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->access$300(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;)I

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v2}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->access$300(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;)I

    move-result v3

    if-lt v3, v0, :cond_0

    invoke-static {v2}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->access$300(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;)I

    move-result v3

    div-int/lit8 v4, v0, 0xa

    mul-int/lit8 v4, v4, 0xa

    add-int/lit8 v4, v4, 0xa

    if-ge v3, v4, :cond_0

    invoke-static {v2}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->access$308(Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;)I

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble$ElementView;->moveToPosition(Z)V

    :cond_0
    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public onCoordinateAdd()V
    .locals 0

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->removeAllViews()V

    iget-object v0, p0, Lcom/android/settings/statusbarelement/PositionsElementsStatusbarDouble;->mElements:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method
