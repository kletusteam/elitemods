.class public Lcom/android/settings/LockScreenActionsSettings;
.super Lcom/android/settings/BaseSettingsPreferenceFragment;


# instance fields
.field private mLockScreenControlCenter:Landroidx/preference/CheckBoxPreference;

.field private mLockScreenNotification:Landroidx/preference/CheckBoxPreference;

.field private mLockScreenSmartHome:Landroidx/preference/CheckBoxPreference;

.field private mSmartHomePreferenceManager:Lcom/android/settings/smarthome/SmartHomePreferenceManager;


# direct methods
.method static bridge synthetic -$$Nest$mupdateLockScreenSmartHome(Lcom/android/settings/LockScreenActionsSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/LockScreenActionsSettings;->updateLockScreenSmartHome()V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/BaseSettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method private setupLockScreenControlCenter()V
    .locals 2

    const-string v0, "lock_screen_control_center"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/LockScreenActionsSettings;->mLockScreenControlCenter:Landroidx/preference/CheckBoxPreference;

    new-instance v1, Lcom/android/settings/LockScreenActionsSettings$2;

    invoke-direct {v1, p0}, Lcom/android/settings/LockScreenActionsSettings$2;-><init>(Lcom/android/settings/LockScreenActionsSettings;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method

.method private setupLockScreenNotification()V
    .locals 2

    const-string v0, "lock_screen_notification"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/LockScreenActionsSettings;->mLockScreenNotification:Landroidx/preference/CheckBoxPreference;

    new-instance v1, Lcom/android/settings/LockScreenActionsSettings$1;

    invoke-direct {v1, p0}, Lcom/android/settings/LockScreenActionsSettings$1;-><init>(Lcom/android/settings/LockScreenActionsSettings;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method

.method private setupLockScreenSmartHome()V
    .locals 1

    const-string v0, "lock_screen_smart_home"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/LockScreenActionsSettings;->mLockScreenSmartHome:Landroidx/preference/CheckBoxPreference;

    return-void
.end method

.method private updateLockScreenControlCenter()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/LockScreenActionsSettings;->mLockScreenControlCenter:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/utils/StatusBarUtils;->isExpandableUnderLockscreen(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/LockScreenActionsSettings;->mLockScreenControlCenter:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0}, Lcom/android/settings/utils/StatusBarUtils;->isUseControlPanel(Landroid/content/Context;)Z

    move-result p0

    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method private updateLockScreenNotification()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/LockScreenActionsSettings;->mLockScreenNotification:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0}, Landroid/app/MiuiStatusBarManager;->isExpandableUnderKeyguard(Landroid/content/Context;)Z

    move-result p0

    invoke-virtual {v0, p0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method private updateLockScreenSmartHome()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/LockScreenActionsSettings;->mSmartHomePreferenceManager:Lcom/android/settings/smarthome/SmartHomePreferenceManager;

    iget-object p0, p0, Lcom/android/settings/LockScreenActionsSettings;->mLockScreenSmartHome:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/smarthome/SmartHomePreferenceManager;->updateLockScreenSmartHome(Landroidx/preference/CheckBoxPreference;)V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    sget p1, Lcom/android/settings/R$xml;->lock_screen_actions_settings:I

    invoke-virtual {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->addPreferencesFromResource(I)V

    new-instance p1, Lcom/android/settings/smarthome/SmartHomePreferenceManager;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p1, v0, v1}, Lcom/android/settings/smarthome/SmartHomePreferenceManager;-><init>(Landroid/content/Context;Z)V

    iput-object p1, p0, Lcom/android/settings/LockScreenActionsSettings;->mSmartHomePreferenceManager:Lcom/android/settings/smarthome/SmartHomePreferenceManager;

    invoke-direct {p0}, Lcom/android/settings/LockScreenActionsSettings;->setupLockScreenNotification()V

    invoke-direct {p0}, Lcom/android/settings/LockScreenActionsSettings;->setupLockScreenControlCenter()V

    invoke-direct {p0}, Lcom/android/settings/LockScreenActionsSettings;->setupLockScreenSmartHome()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/LockScreenActionsSettings;->updateLockScreenNotification()V

    invoke-direct {p0}, Lcom/android/settings/LockScreenActionsSettings;->updateLockScreenControlCenter()V

    invoke-direct {p0}, Lcom/android/settings/LockScreenActionsSettings;->updateLockScreenSmartHome()V

    return-void
.end method
