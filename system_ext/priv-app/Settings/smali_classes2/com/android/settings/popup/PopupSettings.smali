.class public Lcom/android/settings/popup/PopupSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mGridVIew:Lcom/android/settings/popup/PopupCameraSoundChoosePreference;

.field private mLedGridVIew:Lcom/android/settings/popup/PopupCameraLedChoosePreference;

.field private mPopupGesture:Landroidx/preference/CheckBoxPreference;

.field private mPopupGridviewSettings:Landroidx/preference/PreferenceCategory;

.field private mPopupLed:Landroidx/preference/CheckBoxPreference;

.field private mPopupSound:Landroidx/preference/CheckBoxPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method private gestureStateChange(Z)V
    .locals 2

    iget-object p0, p0, Lcom/android/settings/popup/PopupSettings;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const/4 v0, -0x2

    const-string/jumbo v1, "miui_popup_gesture_check"

    invoke-static {p0, v1, p1, v0}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    return-void
.end method

.method private ledStateChange(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/popup/PopupSettings;->mLedGridVIew:Lcom/android/settings/popup/PopupCameraLedChoosePreference;

    invoke-virtual {v0, p1}, Landroidx/preference/Preference;->setEnabled(Z)V

    iget-object p0, p0, Lcom/android/settings/popup/PopupSettings;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const/4 v0, -0x2

    const-string/jumbo v1, "miui_popup_led_check"

    invoke-static {p0, v1, p1, v0}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    return-void
.end method

.method private soundStateChange(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/popup/PopupSettings;->mGridVIew:Lcom/android/settings/popup/PopupCameraSoundChoosePreference;

    invoke-virtual {v0, p1}, Landroidx/preference/Preference;->setEnabled(Z)V

    iget-object p0, p0, Lcom/android/settings/popup/PopupSettings;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const/4 v0, -0x2

    const-string/jumbo v1, "miui_popup_sound_check"

    invoke-static {p0, v1, p1, v0}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 0

    const-class p0, Lcom/android/settings/popup/PopupSettings;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    sget p1, Lcom/android/settings/R$xml;->popupcamera_function_settings:I

    invoke-virtual {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    sget v0, Lcom/android/settings/R$string;->popup_title:I

    invoke-virtual {p1, v0}, Landroid/app/Activity;->setTitle(I)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/popup/PopupSettings;->mContext:Landroid/content/Context;

    const-string/jumbo p1, "popup_voice_setting"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/PreferenceCategory;

    iput-object p1, p0, Lcom/android/settings/popup/PopupSettings;->mPopupGridviewSettings:Landroidx/preference/PreferenceCategory;

    const-string/jumbo v0, "popup_voice_check"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/popup/PopupSettings;->mPopupSound:Landroidx/preference/CheckBoxPreference;

    iget-object p1, p0, Lcom/android/settings/popup/PopupSettings;->mPopupGridviewSettings:Landroidx/preference/PreferenceCategory;

    const-string/jumbo v0, "popup_led_check"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/popup/PopupSettings;->mPopupLed:Landroidx/preference/CheckBoxPreference;

    iget-object p1, p0, Lcom/android/settings/popup/PopupSettings;->mPopupGridviewSettings:Landroidx/preference/PreferenceCategory;

    const-string/jumbo v0, "popup_gesture_check"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/popup/PopupSettings;->mPopupGesture:Landroidx/preference/CheckBoxPreference;

    iget-object p1, p0, Lcom/android/settings/popup/PopupSettings;->mPopupGridviewSettings:Landroidx/preference/PreferenceCategory;

    const-string/jumbo v0, "popup_voice_preference"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;

    iput-object p1, p0, Lcom/android/settings/popup/PopupSettings;->mGridVIew:Lcom/android/settings/popup/PopupCameraSoundChoosePreference;

    iget-object p1, p0, Lcom/android/settings/popup/PopupSettings;->mPopupGridviewSettings:Landroidx/preference/PreferenceCategory;

    const-string/jumbo v0, "popup_led_preference"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/popup/PopupCameraLedChoosePreference;

    iput-object p1, p0, Lcom/android/settings/popup/PopupSettings;->mLedGridVIew:Lcom/android/settings/popup/PopupCameraLedChoosePreference;

    iget-object p1, p0, Lcom/android/settings/popup/PopupSettings;->mPopupSound:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-object p1, p0, Lcom/android/settings/popup/PopupSettings;->mPopupLed:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-object p1, p0, Lcom/android/settings/popup/PopupSettings;->mPopupGesture:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->isNeedShowColorLamp()Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/popup/PopupSettings;->mPopupGridviewSettings:Landroidx/preference/PreferenceCategory;

    iget-object v0, p0, Lcom/android/settings/popup/PopupSettings;->mLedGridVIew:Lcom/android/settings/popup/PopupCameraLedChoosePreference;

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_0
    sget-object p1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    const-string v0, "cezanne"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/android/settings/popup/PopupSettings;->mPopupGridviewSettings:Landroidx/preference/PreferenceCategory;

    iget-object p0, p0, Lcom/android/settings/popup/PopupSettings;->mPopupGesture:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_1
    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/android/settingslib/core/lifecycle/ObservablePreferenceFragment;->onDestroy()V

    iget-object v0, p0, Lcom/android/settings/popup/PopupSettings;->mGridVIew:Lcom/android/settings/popup/PopupCameraSoundChoosePreference;

    invoke-virtual {v0}, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->onDestroy()V

    iget-object p0, p0, Lcom/android/settings/popup/PopupSettings;->mLedGridVIew:Lcom/android/settings/popup/PopupCameraLedChoosePreference;

    invoke-virtual {p0}, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onPause()V

    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "popup_voice_check"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/popup/PopupSettings;->soundStateChange(Z)V

    return v1

    :cond_0
    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "popup_led_check"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/popup/PopupSettings;->ledStateChange(Z)V

    return v1

    :cond_1
    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    const-string/jumbo v0, "popup_gesture_check"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/popup/PopupSettings;->gestureStateChange(Z)V

    return v1

    :cond_2
    const/4 p0, 0x0

    return p0
.end method

.method public onResume()V
    .locals 5

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/popup/PopupSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "miui_popup_sound_check"

    const/4 v2, 0x1

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    iget-object v4, p0, Lcom/android/settings/popup/PopupSettings;->mPopupSound:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v4, v0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v4, p0, Lcom/android/settings/popup/PopupSettings;->mGridVIew:Lcom/android/settings/popup/PopupCameraSoundChoosePreference;

    invoke-virtual {v4, v0}, Landroidx/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/popup/PopupSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "miui_popup_led_check"

    invoke-static {v0, v4, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    goto :goto_1

    :cond_1
    move v0, v1

    :goto_1
    iget-object v4, p0, Lcom/android/settings/popup/PopupSettings;->mPopupLed:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v4, v0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v4, p0, Lcom/android/settings/popup/PopupSettings;->mLedGridVIew:Lcom/android/settings/popup/PopupCameraLedChoosePreference;

    invoke-virtual {v4, v0}, Landroidx/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/popup/PopupSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v4, "miui_popup_gesture_check"

    invoke-static {v0, v4, v1, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    move v2, v1

    :goto_2
    iget-object p0, p0, Lcom/android/settings/popup/PopupSettings;->mPopupGesture:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p0, v2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    return-void
.end method
