.class Lcom/android/settings/popup/PopupCameraLedChoosePreference$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/popup/PopupCameraLedChoosePreference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/popup/PopupCameraLedChoosePreference;

.field final synthetic val$enable:Z


# direct methods
.method constructor <init>(Lcom/android/settings/popup/PopupCameraLedChoosePreference;Z)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference$1;->this$0:Lcom/android/settings/popup/PopupCameraLedChoosePreference;

    iput-boolean p2, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference$1;->val$enable:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object p1, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference$1;->this$0:Lcom/android/settings/popup/PopupCameraLedChoosePreference;

    invoke-static {p1}, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->-$$Nest$fgetmClickable(Lcom/android/settings/popup/PopupCameraLedChoosePreference;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-boolean p1, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference$1;->val$enable:Z

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference$1;->this$0:Lcom/android/settings/popup/PopupCameraLedChoosePreference;

    invoke-static {p1}, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->-$$Nest$fgetmAdapter(Lcom/android/settings/popup/PopupCameraLedChoosePreference;)Lcom/android/settings/popup/PopupCameraLedListAdapter;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/android/settings/popup/PopupCameraLedListAdapter;->setChooseItem(I)V

    iget-object p0, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference$1;->this$0:Lcom/android/settings/popup/PopupCameraLedChoosePreference;

    invoke-virtual {p0, p3}, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->onLedSelected(I)V

    goto :goto_0

    :cond_0
    iget-object p0, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference$1;->this$0:Lcom/android/settings/popup/PopupCameraLedChoosePreference;

    invoke-static {p0}, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->-$$Nest$fgetmContext(Lcom/android/settings/popup/PopupCameraLedChoosePreference;)Landroid/content/Context;

    move-result-object p0

    sget p1, Lcom/android/settings/R$string;->popup_led_try_later:I

    const/4 p2, 0x0

    invoke-static {p0, p1, p2}, Lcom/android/settingslib/util/ToastUtil;->show(Landroid/content/Context;II)V

    :cond_1
    :goto_0
    return-void
.end method
