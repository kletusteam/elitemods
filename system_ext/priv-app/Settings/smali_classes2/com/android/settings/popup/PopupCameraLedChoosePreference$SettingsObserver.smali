.class final Lcom/android/settings/popup/PopupCameraLedChoosePreference$SettingsObserver;
.super Landroid/database/ContentObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/popup/PopupCameraLedChoosePreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SettingsObserver"
.end annotation


# instance fields
.field private final POPUP_TAKEBACK_OK_URI:Landroid/net/Uri;

.field final synthetic this$0:Lcom/android/settings/popup/PopupCameraLedChoosePreference;


# direct methods
.method public constructor <init>(Lcom/android/settings/popup/PopupCameraLedChoosePreference;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference$SettingsObserver;->this$0:Lcom/android/settings/popup/PopupCameraLedChoosePreference;

    invoke-static {p1}, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->-$$Nest$fgetmHandler(Lcom/android/settings/popup/PopupCameraLedChoosePreference;)Landroid/os/Handler;

    move-result-object p1

    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    const-string/jumbo p1, "popup_takeback_ok"

    invoke-static {p1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference$SettingsObserver;->POPUP_TAKEBACK_OK_URI:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    iget-object p1, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference$SettingsObserver;->this$0:Lcom/android/settings/popup/PopupCameraLedChoosePreference;

    invoke-virtual {p1}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string/jumbo p2, "popup_takeback_ok"

    const/4 v0, 0x1

    const/4 v1, -0x2

    invoke-static {p1, p2, v0, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result p1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {}, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onChange POPUP_TAKEBACK_OK mTakebackOk: "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p0, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference$SettingsObserver;->this$0:Lcom/android/settings/popup/PopupCameraLedChoosePreference;

    invoke-static {p0, v0}, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->-$$Nest$fputmClickable(Lcom/android/settings/popup/PopupCameraLedChoosePreference;Z)V

    return-void
.end method

.method public register()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference$SettingsObserver;->this$0:Lcom/android/settings/popup/PopupCameraLedChoosePreference;

    invoke-virtual {v0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference$SettingsObserver;->POPUP_TAKEBACK_OK_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    return-void
.end method

.method public unregister()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference$SettingsObserver;->this$0:Lcom/android/settings/popup/PopupCameraLedChoosePreference;

    invoke-virtual {v0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method
