.class public Lcom/android/settings/popup/PopupCameraSoundChoosePreference;
.super Lcom/android/settingslib/miuisettings/preference/Preference;


# instance fields
.field private mAdapter:Lcom/android/settings/popup/PopupCameraSoundListAdapter;

.field private mContext:Landroid/content/Context;

.field private mGridView:Landroid/widget/GridView;

.field private mHandler:Landroid/os/Handler;

.field private mImages:[I

.field private mLastIndex:I

.field private mSoundPool:Landroid/media/SoundPool;

.field private mSounds:[Ljava/lang/String;

.field private mTask:Ljava/lang/Runnable;

.field private mTexts:[I


# direct methods
.method public static synthetic $r8$lambda$OeRfD_FqPEedkDB5o4nF_RYWt78(Lcom/android/settings/popup/PopupCameraSoundChoosePreference;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->lambda$onSoundSelected$0(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmAdapter(Lcom/android/settings/popup/PopupCameraSoundChoosePreference;)Lcom/android/settings/popup/PopupCameraSoundListAdapter;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mAdapter:Lcom/android/settings/popup/PopupCameraSoundListAdapter;

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settingslib/miuisettings/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p2, 0x6

    new-array p3, p2, [I

    sget v0, Lcom/android/settings/R$drawable;->popup_muqin_up:I

    const/4 v1, 0x0

    aput v0, p3, v1

    sget v0, Lcom/android/settings/R$drawable;->popup_yingyan_up:I

    const/4 v2, 0x1

    aput v0, p3, v2

    sget v0, Lcom/android/settings/R$drawable;->popup_mofa_up:I

    const/4 v3, 0x2

    aput v0, p3, v3

    sget v0, Lcom/android/settings/R$drawable;->popup_jijia_up:I

    const/4 v4, 0x3

    aput v0, p3, v4

    sget v0, Lcom/android/settings/R$drawable;->popup_chilun_up:I

    const/4 v5, 0x4

    aput v0, p3, v5

    sget v0, Lcom/android/settings/R$drawable;->popup_cangmen_up:I

    const/4 v6, 0x5

    aput v0, p3, v6

    iput-object p3, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mImages:[I

    new-array p2, p2, [I

    sget v0, Lcom/android/settings/R$string;->popup_title_muqin:I

    aput v0, p2, v1

    sget v0, Lcom/android/settings/R$string;->popup_title_yingyan:I

    aput v0, p2, v2

    sget v0, Lcom/android/settings/R$string;->popup_title_mofa:I

    aput v0, p2, v3

    sget v0, Lcom/android/settings/R$string;->popup_title_jijia:I

    aput v0, p2, v4

    sget v0, Lcom/android/settings/R$string;->popup_title_chilun:I

    aput v0, p2, v5

    sget v0, Lcom/android/settings/R$string;->popup_title_cangmen:I

    aput v0, p2, v6

    iput-object p2, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mTexts:[I

    array-length p2, p3

    mul-int/2addr p2, v3

    new-array p2, p2, [Ljava/lang/String;

    iput-object p2, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mSounds:[Ljava/lang/String;

    iput-object p1, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$layout;->popup_gridview_list:I

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setLayoutResource(I)V

    iget-object p1, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/android/settings/R$array;->key_popup_voice_choice:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object p1

    move p2, v1

    :goto_0
    iget-object p3, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mSounds:[Ljava/lang/String;

    array-length v0, p3

    if-ge p2, v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "system/media/audio/ui/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v3, p1, p2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, p3, p2

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mHandler:Landroid/os/Handler;

    iget-object p1, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mSoundPool:Landroid/media/SoundPool;

    if-nez p1, :cond_1

    new-instance p1, Landroid/media/SoundPool;

    const/16 p2, 0xa

    invoke-direct {p1, p2, v2, v1}, Landroid/media/SoundPool;-><init>(III)V

    iput-object p1, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mSoundPool:Landroid/media/SoundPool;

    :cond_1
    iget-object p1, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mSounds:[Ljava/lang/String;

    array-length p2, p1

    :goto_1
    if-ge v1, p2, :cond_2

    aget-object p3, p1, v1

    iget-object v0, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mSoundPool:Landroid/media/SoundPool;

    invoke-virtual {v0, p3, v2}, Landroid/media/SoundPool;->load(Ljava/lang/String;I)I

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    return-void
.end method

.method private synthetic lambda$onSoundSelected$0(I)V
    .locals 0

    mul-int/lit8 p1, p1, 0x2

    add-int/lit8 p1, p1, 0x1

    invoke-direct {p0, p1}, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->realPlaySound(I)V

    return-void
.end method

.method private realPlaySound(I)V
    .locals 7

    iget-object v0, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mSoundPool:Landroid/media/SoundPool;

    add-int/lit8 v1, p1, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    return-void
.end method

.method private updateSelectedItem(I)V
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "miui_popup_sound_index"

    const/4 v1, -0x2

    invoke-static {p0, v0, p1, v1}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    return-void
.end method


# virtual methods
.method public getGridView()Landroid/widget/GridView;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mGridView:Landroid/widget/GridView;

    return-object p0
.end method

.method public onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/android/settingslib/miuisettings/preference/Preference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {}, Lcom/android/settings/MiuiUtils;->isMiuiSdkSupportFolme()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    aput-object p1, v0, v1

    invoke-static {v0}, Lmiuix/animation/Folme;->clean([Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundColor(I)V

    sget v0, Lcom/android/settings/R$id;->popup_gridview:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/GridView;

    iput-object p1, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mGridView:Landroid/widget/GridView;

    new-instance p1, Lcom/android/settings/popup/PopupCameraSoundListAdapter;

    iget-object v0, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mImages:[I

    iget-object v3, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mTexts:[I

    invoke-direct {p1, v0, p0, v2, v3}, Lcom/android/settings/popup/PopupCameraSoundListAdapter;-><init>(Landroid/content/Context;Lcom/android/settings/popup/PopupCameraSoundChoosePreference;[I[I)V

    iput-object p1, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mAdapter:Lcom/android/settings/popup/PopupCameraSoundListAdapter;

    iget-object v0, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0, p1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const/4 v0, -0x2

    const-string/jumbo v2, "miui_popup_sound_index"

    invoke-static {p1, v2, v1, v0}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result p1

    iput p1, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mLastIndex:I

    iget-object v0, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mAdapter:Lcom/android/settings/popup/PopupCameraSoundListAdapter;

    invoke-virtual {v0, p1}, Lcom/android/settings/popup/PopupCameraSoundListAdapter;->setChooseItem(I)V

    invoke-virtual {p0}, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->getGridView()Landroid/widget/GridView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/GridView;->isEnabled()Z

    move-result p1

    iget-object v0, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mGridView:Landroid/widget/GridView;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mGridView:Landroid/widget/GridView;

    new-instance v1, Lcom/android/settings/popup/PopupCameraSoundChoosePreference$1;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/popup/PopupCameraSoundChoosePreference$1;-><init>(Lcom/android/settings/popup/PopupCameraSoundChoosePreference;Z)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    iget-object p0, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mSoundPool:Landroid/media/SoundPool;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/media/SoundPool;->release()V

    :cond_0
    return-void
.end method

.method public onSoundSelected(I)V
    .locals 2

    iget v0, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mLastIndex:I

    if-eq v0, p1, :cond_0

    invoke-direct {p0, p1}, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->updateSelectedItem(I)V

    :cond_0
    iput p1, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mLastIndex:I

    iget-object v0, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    new-instance v0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/popup/PopupCameraSoundChoosePreference$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/popup/PopupCameraSoundChoosePreference;I)V

    iput-object v0, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mTask:Ljava/lang/Runnable;

    mul-int/lit8 p1, p1, 0x2

    invoke-direct {p0, p1}, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->realPlaySound(I)V

    iget-object p1, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mHandler:Landroid/os/Handler;

    iget-object p0, p0, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->mTask:Ljava/lang/Runnable;

    const-wide/16 v0, 0x3e8

    invoke-virtual {p1, p0, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
