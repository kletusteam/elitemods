.class public Lcom/android/settings/popup/PopupCameraSoundListAdapter;
.super Landroid/widget/BaseAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/popup/PopupCameraSoundListAdapter$Holder;
    }
.end annotation


# instance fields
.field private mChooseIndex:I

.field private mContext:Landroid/content/Context;

.field private mPictures:[I

.field private mPreference:Lcom/android/settings/popup/PopupCameraSoundChoosePreference;

.field private mTexts:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/popup/PopupCameraSoundChoosePreference;[I[I)V
    .locals 0

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/android/settings/popup/PopupCameraSoundListAdapter;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/popup/PopupCameraSoundListAdapter;->mPreference:Lcom/android/settings/popup/PopupCameraSoundChoosePreference;

    iput-object p3, p0, Lcom/android/settings/popup/PopupCameraSoundListAdapter;->mPictures:[I

    iput-object p4, p0, Lcom/android/settings/popup/PopupCameraSoundListAdapter;->mTexts:[I

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 0

    iget-object p0, p0, Lcom/android/settings/popup/PopupCameraSoundListAdapter;->mPictures:[I

    array-length p0, p0

    return p0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/popup/PopupCameraSoundListAdapter;->mPictures:[I

    aget p0, p0, p1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method

.method public getItemId(I)J
    .locals 0

    int-to-long p0, p1

    return-wide p0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    iget-object v0, p0, Lcom/android/settings/popup/PopupCameraSoundListAdapter;->mPreference:Lcom/android/settings/popup/PopupCameraSoundChoosePreference;

    invoke-virtual {v0}, Lcom/android/settings/popup/PopupCameraSoundChoosePreference;->getGridView()Landroid/widget/GridView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/GridView;->isEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-nez p2, :cond_0

    new-instance p2, Lcom/android/settings/popup/PopupCameraSoundListAdapter$Holder;

    invoke-direct {p2, p0}, Lcom/android/settings/popup/PopupCameraSoundListAdapter$Holder;-><init>(Lcom/android/settings/popup/PopupCameraSoundListAdapter;)V

    iget-object v2, p0, Lcom/android/settings/popup/PopupCameraSoundListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    sget v3, Lcom/android/settings/R$layout;->popup_gridview_item:I

    invoke-virtual {v2, v3, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    sget v2, Lcom/android/settings/R$id;->iv_item:I

    invoke-virtual {p3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p2, Lcom/android/settings/popup/PopupCameraSoundListAdapter$Holder;->iv:Landroid/widget/ImageView;

    sget v2, Lcom/android/settings/R$id;->tv_item:I

    invoke-virtual {p3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p2, Lcom/android/settings/popup/PopupCameraSoundListAdapter$Holder;->tv:Landroid/widget/TextView;

    invoke-virtual {p3, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/android/settings/popup/PopupCameraSoundListAdapter$Holder;

    move-object v5, p3

    move-object p3, p2

    move-object p2, v5

    :goto_0
    iget-object v2, p2, Lcom/android/settings/popup/PopupCameraSoundListAdapter$Holder;->iv:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/android/settings/popup/PopupCameraSoundListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/popup/PopupCameraSoundListAdapter;->mPictures:[I

    aget v4, v4, p1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v2, p2, Lcom/android/settings/popup/PopupCameraSoundListAdapter$Holder;->tv:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/settings/popup/PopupCameraSoundListAdapter;->mTexts:[I

    aget v3, v3, p1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object p2, p2, Lcom/android/settings/popup/PopupCameraSoundListAdapter$Holder;->iv:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    const/high16 v2, 0x3f800000    # 1.0f

    goto :goto_1

    :cond_1
    const v2, 0x3ecccccd    # 0.4f

    :goto_1
    invoke-virtual {p2, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    iget p0, p0, Lcom/android/settings/popup/PopupCameraSoundListAdapter;->mChooseIndex:I

    if-ne p1, p0, :cond_2

    if-eqz v0, :cond_2

    sget p0, Lcom/android/settings/R$drawable;->popup_background_view:I

    goto :goto_2

    :cond_2
    move p0, v1

    :goto_2
    invoke-virtual {p3, p0}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-static {}, Lcom/android/settings/MiuiUtils;->isMiuiSdkSupportFolme()Z

    move-result p0

    if-eqz p0, :cond_3

    const/4 p0, 0x1

    new-array p0, p0, [Landroid/view/View;

    aput-object p3, p0, v1

    invoke-static {p0}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object p0

    invoke-interface {p0}, Lmiuix/animation/IFolme;->touch()Lmiuix/animation/ITouchStyle;

    move-result-object p0

    new-array p1, v1, [Lmiuix/animation/base/AnimConfig;

    invoke-interface {p0, p3, p1}, Lmiuix/animation/ITouchStyle;->handleTouchOf(Landroid/view/View;[Lmiuix/animation/base/AnimConfig;)V

    :cond_3
    return-object p3
.end method

.method public setChooseItem(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/popup/PopupCameraSoundListAdapter;->mChooseIndex:I

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method
