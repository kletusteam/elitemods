.class public Lcom/android/settings/popup/PopupCameraLedChoosePreference;
.super Lcom/android/settingslib/miuisettings/preference/Preference;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/popup/PopupCameraLedChoosePreference$SettingsObserver;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAdapter:Lcom/android/settings/popup/PopupCameraLedListAdapter;

.field private mClickable:Z

.field private mContext:Landroid/content/Context;

.field private mGridView:Landroid/widget/GridView;

.field private mHandler:Landroid/os/Handler;

.field private mImages:[I

.field private mLastIndex:I

.field private final mSettingsObserver:Lcom/android/settings/popup/PopupCameraLedChoosePreference$SettingsObserver;

.field private service:Lmiui/popupcamera/IPopupCameraManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAdapter(Lcom/android/settings/popup/PopupCameraLedChoosePreference;)Lcom/android/settings/popup/PopupCameraLedListAdapter;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->mAdapter:Lcom/android/settings/popup/PopupCameraLedListAdapter;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmClickable(Lcom/android/settings/popup/PopupCameraLedChoosePreference;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->mClickable:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/settings/popup/PopupCameraLedChoosePreference;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/settings/popup/PopupCameraLedChoosePreference;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmClickable(Lcom/android/settings/popup/PopupCameraLedChoosePreference;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->mClickable:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetTAG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/popup/PopupCameraLedChoosePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/popup/PopupCameraLedChoosePreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settingslib/miuisettings/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p2, 0x0

    iput-object p2, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->service:Lmiui/popupcamera/IPopupCameraManager;

    new-instance p2, Lcom/android/settings/popup/PopupCameraLedChoosePreference$SettingsObserver;

    invoke-direct {p2, p0}, Lcom/android/settings/popup/PopupCameraLedChoosePreference$SettingsObserver;-><init>(Lcom/android/settings/popup/PopupCameraLedChoosePreference;)V

    iput-object p2, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->mSettingsObserver:Lcom/android/settings/popup/PopupCameraLedChoosePreference$SettingsObserver;

    const/4 p3, 0x1

    iput-boolean p3, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->mClickable:Z

    const/4 v0, 0x5

    new-array v0, v0, [I

    sget v1, Lcom/android/settings/R$drawable;->popup_led_color1:I

    const/4 v2, 0x0

    aput v1, v0, v2

    sget v1, Lcom/android/settings/R$drawable;->popup_led_color2:I

    aput v1, v0, p3

    sget v1, Lcom/android/settings/R$drawable;->popup_led_color3:I

    const/4 v3, 0x2

    aput v1, v0, v3

    sget v1, Lcom/android/settings/R$drawable;->popup_led_color4:I

    const/4 v3, 0x3

    aput v1, v0, v3

    sget v1, Lcom/android/settings/R$drawable;->popup_led_color5:I

    const/4 v3, 0x4

    aput v1, v0, v3

    iput-object v0, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->mImages:[I

    iput-object p1, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->mContext:Landroid/content/Context;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result p1

    if-ne p1, p3, :cond_0

    goto :goto_0

    :cond_0
    move p3, v2

    :goto_0
    if-eqz p3, :cond_1

    sget p1, Lcom/android/settings/R$layout;->popup_led_gridview_list_rtl:I

    goto :goto_1

    :cond_1
    sget p1, Lcom/android/settings/R$layout;->popup_led_gridview_list:I

    :goto_1
    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setLayoutResource(I)V

    const-string/jumbo p1, "popupcamera"

    invoke-static {p1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object p1

    invoke-static {p1}, Lmiui/popupcamera/IPopupCameraManager$Stub;->asInterface(Landroid/os/IBinder;)Lmiui/popupcamera/IPopupCameraManager;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->service:Lmiui/popupcamera/IPopupCameraManager;

    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->mHandler:Landroid/os/Handler;

    invoke-virtual {p2}, Lcom/android/settings/popup/PopupCameraLedChoosePreference$SettingsObserver;->register()V

    return-void
.end method

.method private popupCamera()V
    .locals 3

    :try_start_0
    iget-object p0, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->service:Lmiui/popupcamera/IPopupCameraManager;

    invoke-interface {p0}, Lmiui/popupcamera/IPopupCameraManager;->popupMotor()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    sget-object v0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "error:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception p0

    sget-object v0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->TAG:Ljava/lang/String;

    const-string v1, "PopupCameraManagerService connection failed"

    invoke-static {v0, v1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method private updateSelectedItem(I)V
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "miui_popup_led_index"

    const/4 v1, -0x2

    invoke-static {p0, v0, p1, v1}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    return-void
.end method


# virtual methods
.method public getGridView()Landroid/widget/GridView;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->mGridView:Landroid/widget/GridView;

    return-object p0
.end method

.method public onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settingslib/miuisettings/preference/Preference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-static {}, Lcom/android/settings/MiuiUtils;->isMiuiSdkSupportFolme()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    aput-object p1, v0, v1

    invoke-static {v0}, Lmiuix/animation/Folme;->clean([Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundColor(I)V

    sget v0, Lcom/android/settings/R$id;->popup_led_gridview:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/GridView;

    iput-object p1, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->mGridView:Landroid/widget/GridView;

    new-instance p1, Lcom/android/settings/popup/PopupCameraLedListAdapter;

    iget-object v0, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->mImages:[I

    invoke-direct {p1, v0, p0, v2}, Lcom/android/settings/popup/PopupCameraLedListAdapter;-><init>(Landroid/content/Context;Lcom/android/settings/popup/PopupCameraLedChoosePreference;[I)V

    iput-object p1, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->mAdapter:Lcom/android/settings/popup/PopupCameraLedListAdapter;

    iget-object v0, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v0, p1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const/4 v0, -0x2

    const-string/jumbo v2, "miui_popup_led_index"

    invoke-static {p1, v2, v1, v0}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result p1

    iput p1, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->mLastIndex:I

    iget-object v0, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->mAdapter:Lcom/android/settings/popup/PopupCameraLedListAdapter;

    invoke-virtual {v0, p1}, Lcom/android/settings/popup/PopupCameraLedListAdapter;->setChooseItem(I)V

    invoke-virtual {p0}, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->getGridView()Landroid/widget/GridView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/GridView;->isEnabled()Z

    move-result p1

    iget-object v0, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->mGridView:Landroid/widget/GridView;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->mGridView:Landroid/widget/GridView;

    new-instance v1, Lcom/android/settings/popup/PopupCameraLedChoosePreference$1;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/popup/PopupCameraLedChoosePreference$1;-><init>(Lcom/android/settings/popup/PopupCameraLedChoosePreference;Z)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->mClickable:Z

    iget-object p0, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->mSettingsObserver:Lcom/android/settings/popup/PopupCameraLedChoosePreference$SettingsObserver;

    invoke-virtual {p0}, Lcom/android/settings/popup/PopupCameraLedChoosePreference$SettingsObserver;->unregister()V

    return-void
.end method

.method public onLedSelected(I)V
    .locals 1

    iget v0, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->mLastIndex:I

    if-eq v0, p1, :cond_0

    invoke-direct {p0, p1}, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->updateSelectedItem(I)V

    :cond_0
    iput p1, p0, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->mLastIndex:I

    invoke-direct {p0}, Lcom/android/settings/popup/PopupCameraLedChoosePreference;->popupCamera()V

    return-void
.end method
