.class public Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;
.super Lcom/android/settings/CustomListPreference;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mBackground:I

.field private mContext:Landroid/content/Context;

.field private mDaysOfWeek:Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

.field private mIsLast:Z

.field private mNewDaysOfWeek:Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

.field private mRepeatLabel:Ljava/lang/String;


# direct methods
.method static bridge synthetic -$$Nest$fgetmNewDaysOfWeek(Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;)Lcom/android/settings/dndmode/Alarm$DaysOfWeek;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mNewDaysOfWeek:Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    invoke-direct {p0, p1, p2}, Lcom/android/settings/CustomListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/settings/dndmode/Alarm$DaysOfWeek;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mDaysOfWeek:Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

    new-instance v0, Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

    invoke-direct {v0, v1}, Lcom/android/settings/dndmode/Alarm$DaysOfWeek;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mNewDaysOfWeek:Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

    invoke-direct {p0, p1, p2}, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->initTypedArray(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mRepeatLabel:Ljava/lang/String;

    new-instance p1, Ljava/text/DateFormatSymbols;

    invoke-direct {p1}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {p1}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x7

    new-array v0, p2, [Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v3, p1, v2

    aput-object v3, v0, v1

    const/4 v1, 0x3

    aget-object v3, p1, v1

    const/4 v4, 0x1

    aput-object v3, v0, v4

    const/4 v3, 0x4

    aget-object v5, p1, v3

    aput-object v5, v0, v2

    const/4 v2, 0x5

    aget-object v5, p1, v2

    aput-object v5, v0, v1

    const/4 v1, 0x6

    aget-object v5, p1, v1

    aput-object v5, v0, v3

    aget-object p2, p1, p2

    aput-object p2, v0, v2

    aget-object p1, p1, v4

    aput-object p1, v0, v1

    invoke-virtual {p0, v0}, Landroidx/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v0}, Landroidx/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    return-void
.end method

.method private initTypedArray(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    iput-object p1, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mContext:Landroid/content/Context;

    if-eqz p2, :cond_0

    sget-object v0, Lcom/android/settings/R$styleable;->RepeatPreferenceWithBg:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    sget p2, Lcom/android/settings/R$styleable;->RepeatPreferenceWithBg_backgroundRes:I

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p2

    iput p2, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mBackground:I

    sget p2, Lcom/android/settings/R$styleable;->RepeatPreferenceWithBg_last:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mIsLast:Z

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    :cond_0
    return-void
.end method

.method private showMultiChoiceDialog()V
    .locals 4

    invoke-virtual {p0}, Landroidx/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v0

    new-instance v1, Lmiuix/appcompat/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroidx/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mDaysOfWeek:Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

    invoke-virtual {v2}, Lcom/android/settings/dndmode/Alarm$DaysOfWeek;->getBooleanArray()[Z

    move-result-object v2

    new-instance v3, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg$2;

    invoke-direct {v3, p0}, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg$2;-><init>(Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;)V

    invoke-virtual {v1, v0, v2, v3}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/android/settings/R$string;->button_text_ok:I

    new-instance v2, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg$1;

    invoke-direct {v2, p0}, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg$1;-><init>(Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;)V

    invoke-virtual {v0, v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p0

    sget v0, Lcom/android/settings/R$string;->button_text_cancel:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lmiuix/appcompat/app/AlertDialog$Builder;->show()Lmiuix/appcompat/app/AlertDialog;

    return-void
.end method


# virtual methods
.method public getDaysOfWeek()Lcom/android/settings/dndmode/Alarm$DaysOfWeek;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mDaysOfWeek:Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

    return-object p0
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 6

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$dimen;->auto_rule_margin_left:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iget-object v3, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iget-boolean v3, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mIsLast:Z

    const/4 v4, 0x0

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v5, Lcom/android/settings/R$dimen;->auto_rule_first_margin_top:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    goto :goto_0

    :cond_0
    move v3, v4

    :goto_0
    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/settings/R$dimen;->auto_rule_padding_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iget-boolean v1, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mIsLast:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$dimen;->auto_rule_last_item_padding_bottom:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    goto :goto_1

    :cond_1
    move v1, v4

    :goto_1
    invoke-virtual {p1, v0, v4, v0, v1}, Landroid/view/View;->setPadding(IIII)V

    sget v0, Lcom/android/settings/R$id;->label:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mRepeatLabel:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-super {p0, p1}, Lcom/android/settingslib/miuisettings/preference/ListPreference;->onBindView(Landroid/view/View;)V

    iget v0, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mBackground:I

    if-eqz v0, :cond_2

    iget-object p0, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_2
    return-void
.end method

.method protected onClick()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->showMultiChoiceDialog()V

    return-void
.end method

.method protected onDialogClosed(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mDaysOfWeek:Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

    iget-object v0, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mNewDaysOfWeek:Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

    invoke-virtual {p1, v0}, Lcom/android/settings/dndmode/Alarm$DaysOfWeek;->set(Lcom/android/settings/dndmode/Alarm$DaysOfWeek;)V

    iget-object p1, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mDaysOfWeek:Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/android/settings/dndmode/Alarm$DaysOfWeek;->toString(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->setLabel(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mDaysOfWeek:Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->callChangeListener(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mNewDaysOfWeek:Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

    iget-object p0, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mDaysOfWeek:Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

    invoke-virtual {p1, p0}, Lcom/android/settings/dndmode/Alarm$DaysOfWeek;->set(Lcom/android/settings/dndmode/Alarm$DaysOfWeek;)V

    :goto_0
    return-void
.end method

.method protected onPrepareDialogBuilder(Lmiuix/appcompat/app/AlertDialog$Builder;)V
    .locals 3

    invoke-virtual {p0}, Landroidx/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mDaysOfWeek:Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

    invoke-virtual {v1}, Lcom/android/settings/dndmode/Alarm$DaysOfWeek;->getBooleanArray()[Z

    move-result-object v1

    new-instance v2, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg$3;

    invoke-direct {v2, p0}, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg$3;-><init>(Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;)V

    invoke-virtual {p1, v0, v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    return-void
.end method

.method public setDaysOfWeek(Lcom/android/settings/dndmode/Alarm$DaysOfWeek;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mDaysOfWeek:Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

    invoke-virtual {v0, p1}, Lcom/android/settings/dndmode/Alarm$DaysOfWeek;->set(Lcom/android/settings/dndmode/Alarm$DaysOfWeek;)V

    iget-object v0, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mNewDaysOfWeek:Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

    invoke-virtual {v0, p1}, Lcom/android/settings/dndmode/Alarm$DaysOfWeek;->set(Lcom/android/settings/dndmode/Alarm$DaysOfWeek;)V

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/android/settings/dndmode/Alarm$DaysOfWeek;->toString(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroidx/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0, v1}, Lcom/android/settings/dndmode/Alarm$DaysOfWeek;->toString(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->setLabel(Ljava/lang/String;)V

    return-void
.end method

.method public setLabel(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mRepeatLabel:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/android/settings/soundsettings/RepeatPreferenceWithBg;->mRepeatLabel:Ljava/lang/String;

    invoke-virtual {p0}, Landroidx/preference/Preference;->notifyChanged()V

    :cond_0
    return-void
.end method
