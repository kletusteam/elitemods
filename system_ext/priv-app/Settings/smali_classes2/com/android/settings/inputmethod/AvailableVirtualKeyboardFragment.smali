.class public Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;
.super Lcom/android/settings/dashboard/DashboardFragment;

# interfaces
.implements Lcom/android/settingslib/inputmethod/InputMethodPreference$OnSavePreferenceListener;


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/BaseSearchIndexProvider;


# instance fields
.field private final mAllInputMethodPreferenceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroidx/preference/Preference;",
            ">;"
        }
    .end annotation
.end field

.field private mCustomizedInputMethod:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mEnabledList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mImm:Landroid/view/inputmethod/InputMethodManager;

.field final mInputMethodPreferenceList:Ljava/util/ArrayList;
    .annotation build Lcom/android/internal/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/settingslib/inputmethod/InputMethodPreference;",
            ">;"
        }
    .end annotation
.end field

.field mInputMethodSettingValues:Lcom/android/settingslib/inputmethod/InputMethodSettingValuesWrapper;
    .annotation build Lcom/android/internal/annotations/VisibleForTesting;
    .end annotation
.end field

.field private mUserAwareContext:Landroid/content/Context;

.field private mUserId:I


# direct methods
.method public static synthetic $r8$lambda$51MoM0cG4WHVQpLRyDbkl3vvWQU(Ljava/text/Collator;Lcom/android/settingslib/inputmethod/InputMethodPreference;Lcom/android/settingslib/inputmethod/InputMethodPreference;)I
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->lambda$updateInputMethodPreferenceViews$0(Ljava/text/Collator;Lcom/android/settingslib/inputmethod/InputMethodPreference;Lcom/android/settingslib/inputmethod/InputMethodPreference;)I

    move-result p0

    return p0
.end method

.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment$1;

    invoke-direct {v0}, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment$1;-><init>()V

    sput-object v0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/BaseSearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/dashboard/DashboardFragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mInputMethodPreferenceList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mAllInputMethodPreferenceList:Ljava/util/ArrayList;

    const-string v0, "com.sohu.inputmethod.sogou.xiaomi"

    const-string v1, "com.baidu.input_mi"

    const-string v2, "com.iflytek.inputmethod.miui"

    filled-new-array {v0, v1, v2}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mCustomizedInputMethod:Ljava/util/List;

    return-void
.end method

.method private static synthetic lambda$updateInputMethodPreferenceViews$0(Ljava/text/Collator;Lcom/android/settingslib/inputmethod/InputMethodPreference;Lcom/android/settingslib/inputmethod/InputMethodPreference;)I
    .locals 0

    invoke-virtual {p1, p2, p0}, Lcom/android/settingslib/inputmethod/InputMethodPreference;->compareTo(Lcom/android/settingslib/inputmethod/InputMethodPreference;Ljava/text/Collator;)I

    move-result p0

    return p0
.end method


# virtual methods
.method protected getLogTag()Ljava/lang/String;
    .locals 0

    const-string p0, "AvailableVirtualKeyboardFragment"

    return-object p0
.end method

.method public getMetricsCategory()I
    .locals 0

    const/16 p0, 0x15b

    return p0
.end method

.method protected getPreferenceScreenResId()I
    .locals 0

    sget p0, Lcom/android/settings/R$xml;->available_virtual_keyboard:I

    return p0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 5

    invoke-super {p0, p1}, Lcom/android/settings/dashboard/DashboardFragment;->onAttach(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "profile"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const-class v1, Landroid/os/UserManager;

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/UserManager;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eq v0, v3, :cond_3

    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {v1}, Landroid/os/UserManager;->isManagedProfile()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    invoke-static {v1, v2}, Lcom/android/settings/Utils;->getManagedProfileId(Landroid/os/UserManager;I)I

    move-result v0

    const/16 v3, 0x3e7

    if-ne v0, v3, :cond_2

    goto :goto_0

    :cond_2
    invoke-static {v1, v2}, Lcom/android/settings/Utils;->getManagedProfileId(Landroid/os/UserManager;I)I

    move-result v2

    :goto_0
    invoke-static {v2}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object v0

    invoke-virtual {p1, v0, v4}, Landroid/content/Context;->createContextAsUser(Landroid/os/UserHandle;I)Landroid/content/Context;

    move-result-object p1

    goto :goto_1

    :cond_3
    invoke-virtual {v1}, Landroid/os/UserManager;->getPrimaryUser()Landroid/content/pm/UserInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/UserInfo;->getUserHandle()Landroid/os/UserHandle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v2

    invoke-virtual {p1, v0, v4}, Landroid/content/Context;->createContextAsUser(Landroid/os/UserHandle;I)Landroid/content/Context;

    move-result-object p1

    :goto_1
    iput v2, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mUserId:I

    iput-object p1, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mUserAwareContext:Landroid/content/Context;

    return-void
.end method

.method public onCreatePreferences(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    sget p1, Lcom/android/settings/R$xml;->available_virtual_keyboard:I

    invoke-virtual {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->addPreferencesFromResource(I)V

    iget-object p1, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mUserAwareContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settingslib/inputmethod/InputMethodSettingValuesWrapper;->getInstance(Landroid/content/Context;)Lcom/android/settingslib/inputmethod/InputMethodSettingValuesWrapper;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mInputMethodSettingValues:Lcom/android/settingslib/inputmethod/InputMethodSettingValuesWrapper;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    const-class p2, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p1, p2}, Landroid/app/Activity;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/inputmethod/InputMethodManager;

    iput-object p1, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mImm:Landroid/view/inputmethod/InputMethodManager;

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/dashboard/DashboardFragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mImm:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->getEnabledInputMethodList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mEnabledList:Ljava/util/List;

    iget-object v0, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mInputMethodSettingValues:Lcom/android/settingslib/inputmethod/InputMethodSettingValuesWrapper;

    invoke-virtual {v0}, Lcom/android/settingslib/inputmethod/InputMethodSettingValuesWrapper;->refreshAllInputMethodAndSubtypes()V

    invoke-virtual {p0}, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->updateInputMethodPreferenceViews()V

    return-void
.end method

.method public onSaveInputMethodPreference(Lcom/android/settingslib/inputmethod/InputMethodPreference;)V
    .locals 6

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->keyboard:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mUserAwareContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/inputmethod/InputMethodManager;

    iget v5, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mUserId:I

    invoke-virtual {v4, v5}, Landroid/view/inputmethod/InputMethodManager;->getInputMethodListAsUser(I)Ljava/util/List;

    move-result-object v4

    iget v5, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mUserId:I

    invoke-static {p0, v3, v4, v0, v5}, Lcom/android/settingslib/inputmethod/InputMethodAndSubtypeUtilCompat;->saveInputMethodSubtypeListForUser(Lmiuix/preference/PreferenceFragment;Landroid/content/ContentResolver;Ljava/util/List;ZI)V

    iget-object v0, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mInputMethodSettingValues:Lcom/android/settingslib/inputmethod/InputMethodSettingValuesWrapper;

    invoke-virtual {v0}, Lcom/android/settingslib/inputmethod/InputMethodSettingValuesWrapper;->refreshAllInputMethodAndSubtypes()V

    iget-object v0, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mInputMethodPreferenceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/settingslib/inputmethod/InputMethodPreference;

    invoke-virtual {v3}, Lcom/android/settingslib/inputmethod/InputMethodPreference;->updatePreferenceViews()V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mInputMethodPreferenceList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mAllInputMethodPreferenceList:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mInputMethodPreferenceList:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/preference/Preference;

    invoke-virtual {p1}, Lcom/android/settingslib/PrimarySwitchPreference;->isChecked()Z

    move-result p1

    invoke-virtual {v0, p1}, Landroidx/preference/Preference;->setEnabled(Z)V

    :cond_2
    iget-object p1, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mImm:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p1}, Landroid/view/inputmethod/InputMethodManager;->getEnabledInputMethodList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mEnabledList:Ljava/util/List;

    move p1, v1

    :goto_2
    iget-object v0, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mAllInputMethodPreferenceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mAllInputMethodPreferenceList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/inputmethod/CustomInputMethodPreference;

    invoke-virtual {v0}, Lcom/android/settingslib/inputmethod/InputMethodPreference;->getInputMethodInfo()Landroid/view/inputmethod/InputMethodInfo;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mEnabledList:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mEnabledList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v2, :cond_3

    invoke-virtual {v0, v1}, Lcom/android/settings/inputmethod/CustomInputMethodPreference;->setEnableModeText(Z)V

    goto :goto_3

    :cond_3
    invoke-virtual {v0, v2}, Lcom/android/settings/inputmethod/CustomInputMethodPreference;->setEnableModeText(Z)V

    :goto_3
    add-int/lit8 p1, p1, 0x1

    goto :goto_2

    :cond_4
    return-void
.end method

.method updateInputMethodPreferenceViews()V
    .locals 14
    .annotation build Lcom/android/internal/annotations/VisibleForTesting;
    .end annotation

    goto/32 :goto_6e

    nop

    :goto_0
    invoke-virtual {v2, v4}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    :goto_1
    goto/32 :goto_11

    nop

    :goto_2
    iget-object v1, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mInputMethodPreferenceList:Ljava/util/ArrayList;

    goto/32 :goto_5e

    nop

    :goto_3
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_72

    nop

    :goto_4
    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    goto/32 :goto_1d

    nop

    :goto_5
    move v12, v10

    :goto_6
    goto/32 :goto_78

    nop

    :goto_7
    check-cast v3, Landroid/view/inputmethod/InputMethodInfo;

    goto/32 :goto_63

    nop

    :goto_8
    invoke-virtual {v4, v1}, Lcom/android/settings/inputmethod/CustomInputMethodPreference;->setEnableModeText(Z)V

    :goto_9
    goto/32 :goto_62

    nop

    :goto_a
    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_4

    nop

    :goto_b
    invoke-virtual {v6}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v6

    goto/32 :goto_48

    nop

    :goto_c
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_3b

    nop

    :goto_d
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_55

    nop

    :goto_e
    iget-object v3, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mUserAwareContext:Landroid/content/Context;

    goto/32 :goto_3f

    nop

    :goto_f
    sget v4, Lcom/android/settings/R$string;->other_input_method:I

    goto/32 :goto_c

    nop

    :goto_10
    new-instance v13, Lcom/android/settings/inputmethod/CustomInputMethodPreference;

    goto/32 :goto_21

    nop

    :goto_11
    iget-object v5, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mAllInputMethodPreferenceList:Ljava/util/ArrayList;

    goto/32 :goto_46

    nop

    :goto_12
    invoke-virtual {v1, v2}, Landroid/view/inputmethod/InputMethodManager;->getEnabledInputMethodListAsUser(I)Ljava/util/List;

    move-result-object v9

    goto/32 :goto_24

    nop

    :goto_13
    iget v2, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mUserId:I

    goto/32 :goto_12

    nop

    :goto_14
    move-object v5, p0

    goto/32 :goto_43

    nop

    :goto_15
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_5f

    nop

    :goto_16
    if-nez v5, :cond_0

    goto/32 :goto_7d

    :cond_0
    goto/32 :goto_67

    nop

    :goto_17
    return-void

    :goto_18
    iget-object v0, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mInputMethodPreferenceList:Ljava/util/ArrayList;

    goto/32 :goto_4c

    nop

    :goto_19
    move v3, v10

    :goto_1a
    goto/32 :goto_42

    nop

    :goto_1b
    sget v4, Lcom/android/settings/R$string;->xiaomi_custom_input_method:I

    goto/32 :goto_5a

    nop

    :goto_1c
    move-object v3, v2

    goto/32 :goto_7

    nop

    :goto_1d
    if-eqz v2, :cond_1

    goto/32 :goto_2d

    :cond_1
    goto/32 :goto_3a

    nop

    :goto_1e
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_52

    nop

    :goto_1f
    invoke-interface {v6, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    goto/32 :goto_33

    nop

    :goto_20
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    goto/32 :goto_f

    nop

    :goto_21
    iget v6, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mUserId:I

    goto/32 :goto_37

    nop

    :goto_22
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    goto/32 :goto_5c

    nop

    :goto_23
    add-int/lit8 v12, v12, 0x1

    goto/32 :goto_79

    nop

    :goto_24
    const/4 v10, 0x0

    goto/32 :goto_4e

    nop

    :goto_25
    iget-object v6, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mInputMethodPreferenceList:Ljava/util/ArrayList;

    goto/32 :goto_59

    nop

    :goto_26
    invoke-virtual {v3, v2}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    goto/32 :goto_19

    nop

    :goto_27
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    goto/32 :goto_69

    nop

    :goto_28
    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v3

    goto/32 :goto_2a

    nop

    :goto_29
    invoke-virtual {v6}, Lcom/android/settingslib/inputmethod/InputMethodPreference;->getInputMethodInfo()Landroid/view/inputmethod/InputMethodInfo;

    move-result-object v6

    goto/32 :goto_b

    nop

    :goto_2a
    invoke-virtual {v3, v0}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    goto/32 :goto_34

    nop

    :goto_2b
    new-instance v0, Landroidx/preference/PreferenceCategory;

    goto/32 :goto_76

    nop

    :goto_2c
    goto/16 :goto_71

    :goto_2d
    goto/32 :goto_70

    nop

    :goto_2e
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    goto/32 :goto_58

    nop

    :goto_2f
    move v11, v10

    goto/32 :goto_65

    nop

    :goto_30
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_40

    nop

    :goto_31
    iget-object v4, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mInputMethodPreferenceList:Ljava/util/ArrayList;

    goto/32 :goto_d

    nop

    :goto_32
    invoke-direct {v3, v0}, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment$$ExternalSyntheticLambda0;-><init>(Ljava/text/Collator;)V

    goto/32 :goto_7b

    nop

    :goto_33
    if-nez v5, :cond_2

    goto/32 :goto_39

    :cond_2
    goto/32 :goto_3e

    nop

    :goto_34
    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v3

    goto/32 :goto_26

    nop

    :goto_35
    invoke-virtual {v0, v3}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    goto/32 :goto_4d

    nop

    :goto_36
    iget-object v0, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mAllInputMethodPreferenceList:Ljava/util/ArrayList;

    goto/32 :goto_57

    nop

    :goto_37
    move-object v1, v13

    goto/32 :goto_3d

    nop

    :goto_38
    goto/16 :goto_9

    :goto_39
    goto/32 :goto_8

    nop

    :goto_3a
    invoke-interface {v9, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    goto/32 :goto_45

    nop

    :goto_3b
    invoke-virtual {v2, v3}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    :goto_3c
    goto/32 :goto_75

    nop

    :goto_3d
    move-object v2, v7

    goto/32 :goto_14

    nop

    :goto_3e
    iget-object v5, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mEnabledList:Ljava/util/List;

    goto/32 :goto_27

    nop

    :goto_3f
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    goto/32 :goto_1b

    nop

    :goto_40
    goto/16 :goto_1a

    :goto_41
    goto/32 :goto_17

    nop

    :goto_42
    if-lt v3, v11, :cond_3

    goto/32 :goto_41

    :cond_3
    goto/32 :goto_31

    nop

    :goto_43
    invoke-direct/range {v1 .. v6}, Lcom/android/settings/inputmethod/CustomInputMethodPreference;-><init>(Landroid/content/Context;Landroid/view/inputmethod/InputMethodInfo;ZLcom/android/settingslib/inputmethod/InputMethodPreference$OnSavePreferenceListener;I)V

    goto/32 :goto_2

    nop

    :goto_44
    new-instance v2, Landroidx/preference/PreferenceCategory;

    goto/32 :goto_4b

    nop

    :goto_45
    if-nez v2, :cond_4

    goto/32 :goto_6d

    :cond_4
    goto/32 :goto_6c

    nop

    :goto_46
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_64

    nop

    :goto_47
    invoke-virtual {v1}, Lcom/android/settingslib/inputmethod/InputMethodSettingValuesWrapper;->getInputMethodList()Ljava/util/List;

    move-result-object v8

    goto/32 :goto_1e

    nop

    :goto_48
    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    goto/32 :goto_16

    nop

    :goto_49
    if-lt v12, v11, :cond_5

    goto/32 :goto_7a

    :cond_5
    goto/32 :goto_73

    nop

    :goto_4a
    new-instance v3, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment$$ExternalSyntheticLambda0;

    goto/32 :goto_32

    nop

    :goto_4b
    invoke-direct {v2, v7}, Landroidx/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    goto/32 :goto_56

    nop

    :goto_4c
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto/32 :goto_36

    nop

    :goto_4d
    iget-object v3, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mUserAwareContext:Landroid/content/Context;

    goto/32 :goto_20

    nop

    :goto_4e
    if-eqz v8, :cond_6

    goto/32 :goto_66

    :cond_6
    goto/32 :goto_2f

    nop

    :goto_4f
    check-cast v6, Lcom/android/settingslib/inputmethod/InputMethodPreference;

    goto/32 :goto_29

    nop

    :goto_50
    iget-object v5, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mCustomizedInputMethod:Ljava/util/List;

    goto/32 :goto_25

    nop

    :goto_51
    invoke-virtual {v0}, Lcom/android/settingslib/inputmethod/InputMethodSettingValuesWrapper;->refreshAllInputMethodAndSubtypes()V

    goto/32 :goto_18

    nop

    :goto_52
    const-class v2, Landroid/view/inputmethod/InputMethodManager;

    goto/32 :goto_15

    nop

    :goto_53
    iget-object v6, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mEnabledList:Ljava/util/List;

    goto/32 :goto_1f

    nop

    :goto_54
    iget-object v0, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mUserAwareContext:Landroid/content/Context;

    goto/32 :goto_6f

    nop

    :goto_55
    check-cast v4, Lcom/android/settings/inputmethod/CustomInputMethodPreference;

    goto/32 :goto_50

    nop

    :goto_56
    sget-boolean v3, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    goto/32 :goto_74

    nop

    :goto_57
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto/32 :goto_54

    nop

    :goto_58
    iget-object v2, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mInputMethodPreferenceList:Ljava/util/ArrayList;

    goto/32 :goto_4a

    nop

    :goto_59
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    goto/32 :goto_4f

    nop

    :goto_5a
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_35

    nop

    :goto_5b
    invoke-virtual {p0}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->getPrefContext()Landroid/content/Context;

    move-result-object v7

    goto/32 :goto_60

    nop

    :goto_5c
    move v11, v1

    :goto_5d
    goto/32 :goto_5

    nop

    :goto_5e
    invoke-virtual {v1, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_23

    nop

    :goto_5f
    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    goto/32 :goto_13

    nop

    :goto_60
    iget-object v1, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mInputMethodSettingValues:Lcom/android/settingslib/inputmethod/InputMethodSettingValuesWrapper;

    goto/32 :goto_47

    nop

    :goto_61
    invoke-virtual {v3}, Landroidx/preference/PreferenceGroup;->removeAll()V

    goto/32 :goto_28

    nop

    :goto_62
    invoke-static {v4}, Lcom/android/settingslib/inputmethod/InputMethodAndSubtypeUtil;->removeUnnecessaryNonPersistentPreference(Landroidx/preference/Preference;)V

    goto/32 :goto_68

    nop

    :goto_63
    if-nez v0, :cond_7

    goto/32 :goto_2d

    :cond_7
    goto/32 :goto_a

    nop

    :goto_64
    invoke-virtual {v4}, Lcom/android/settingslib/inputmethod/InputMethodPreference;->getInputMethodInfo()Landroid/view/inputmethod/InputMethodInfo;

    move-result-object v5

    goto/32 :goto_53

    nop

    :goto_65
    goto :goto_5d

    :goto_66
    goto/32 :goto_22

    nop

    :goto_67
    invoke-virtual {v0, v4}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    goto/32 :goto_7c

    nop

    :goto_68
    invoke-virtual {v4}, Lcom/android/settingslib/inputmethod/InputMethodPreference;->updatePreferenceViews()V

    goto/32 :goto_30

    nop

    :goto_69
    if-eq v5, v1, :cond_8

    goto/32 :goto_39

    :cond_8
    goto/32 :goto_77

    nop

    :goto_6a
    move v4, v10

    goto/32 :goto_2c

    nop

    :goto_6b
    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->getPermittedInputMethods()Ljava/util/List;

    move-result-object v0

    goto/32 :goto_5b

    nop

    :goto_6c
    goto/16 :goto_2d

    :goto_6d
    goto/32 :goto_6a

    nop

    :goto_6e
    iget-object v0, p0, Lcom/android/settings/inputmethod/AvailableVirtualKeyboardFragment;->mInputMethodSettingValues:Lcom/android/settingslib/inputmethod/InputMethodSettingValuesWrapper;

    goto/32 :goto_51

    nop

    :goto_6f
    const-class v1, Landroid/app/admin/DevicePolicyManager;

    goto/32 :goto_3

    nop

    :goto_70
    move v4, v1

    :goto_71
    goto/32 :goto_10

    nop

    :goto_72
    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    goto/32 :goto_6b

    nop

    :goto_73
    invoke-interface {v8, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_1c

    nop

    :goto_74
    if-eqz v3, :cond_9

    goto/32 :goto_3c

    :cond_9
    goto/32 :goto_e

    nop

    :goto_75
    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v3

    goto/32 :goto_61

    nop

    :goto_76
    invoke-direct {v0, v7}, Landroidx/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    goto/32 :goto_44

    nop

    :goto_77
    invoke-virtual {v4, v10}, Lcom/android/settings/inputmethod/CustomInputMethodPreference;->setEnableModeText(Z)V

    goto/32 :goto_38

    nop

    :goto_78
    const/4 v1, 0x1

    goto/32 :goto_49

    nop

    :goto_79
    goto/16 :goto_6

    :goto_7a
    goto/32 :goto_2e

    nop

    :goto_7b
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->sort(Ljava/util/Comparator;)V

    goto/32 :goto_2b

    nop

    :goto_7c
    goto/16 :goto_1

    :goto_7d
    goto/32 :goto_0

    nop
.end method
