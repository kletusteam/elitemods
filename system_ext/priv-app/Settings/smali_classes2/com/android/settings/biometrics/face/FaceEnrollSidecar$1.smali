.class Lcom/android/settings/biometrics/face/FaceEnrollSidecar$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/biometrics/face/FaceEnrollSidecar;->startEnrollment()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/biometrics/face/FaceEnrollSidecar;


# direct methods
.method constructor <init>(Lcom/android/settings/biometrics/face/FaceEnrollSidecar;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/biometrics/face/FaceEnrollSidecar$1;->this$0:Lcom/android/settings/biometrics/face/FaceEnrollSidecar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    :try_start_0
    sget-object v0, Lcom/android/settings/biometrics/face/FaceEnrollPreviewFragment;->mSurfaceReadyLock:Ljava/util/concurrent/Semaphore;

    const-wide/16 v1, 0x3e8

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/settings/biometrics/face/FaceEnrollPreviewFragment;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v7
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v0, "FaceEnrollSidecar"

    if-nez v7, :cond_0

    :try_start_1
    const-string/jumbo v1, "previewSurface is null."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "calling mFaceUpdater.enroll with previewSurface: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez v7, :cond_1

    const-string/jumbo v2, "null"

    goto :goto_0

    :cond_1
    invoke-virtual {v7}, Landroid/view/Surface;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/biometrics/face/FaceEnrollSidecar$1;->this$0:Lcom/android/settings/biometrics/face/FaceEnrollSidecar;

    invoke-static {v0}, Lcom/android/settings/biometrics/face/FaceEnrollSidecar;->-$$Nest$fgetmFaceUpdater(Lcom/android/settings/biometrics/face/FaceEnrollSidecar;)Lcom/android/settings/biometrics/face/FaceUpdater;

    move-result-object v1

    iget-object v0, p0, Lcom/android/settings/biometrics/face/FaceEnrollSidecar$1;->this$0:Lcom/android/settings/biometrics/face/FaceEnrollSidecar;

    invoke-static {v0}, Lcom/android/settings/biometrics/face/FaceEnrollSidecar;->access$000(Lcom/android/settings/biometrics/face/FaceEnrollSidecar;)I

    move-result v2

    iget-object v0, p0, Lcom/android/settings/biometrics/face/FaceEnrollSidecar$1;->this$0:Lcom/android/settings/biometrics/face/FaceEnrollSidecar;

    invoke-static {v0}, Lcom/android/settings/biometrics/face/FaceEnrollSidecar;->access$100(Lcom/android/settings/biometrics/face/FaceEnrollSidecar;)[B

    move-result-object v3

    iget-object v0, p0, Lcom/android/settings/biometrics/face/FaceEnrollSidecar$1;->this$0:Lcom/android/settings/biometrics/face/FaceEnrollSidecar;

    invoke-static {v0}, Lcom/android/settings/biometrics/face/FaceEnrollSidecar;->access$200(Lcom/android/settings/biometrics/face/FaceEnrollSidecar;)Landroid/os/CancellationSignal;

    move-result-object v4

    iget-object v0, p0, Lcom/android/settings/biometrics/face/FaceEnrollSidecar$1;->this$0:Lcom/android/settings/biometrics/face/FaceEnrollSidecar;

    invoke-static {v0}, Lcom/android/settings/biometrics/face/FaceEnrollSidecar;->-$$Nest$fgetmEnrollmentCallback(Lcom/android/settings/biometrics/face/FaceEnrollSidecar;)Landroid/hardware/face/FaceManager$EnrollmentCallback;

    move-result-object v5

    iget-object p0, p0, Lcom/android/settings/biometrics/face/FaceEnrollSidecar$1;->this$0:Lcom/android/settings/biometrics/face/FaceEnrollSidecar;

    invoke-static {p0}, Lcom/android/settings/biometrics/face/FaceEnrollSidecar;->-$$Nest$fgetmDisabledFeatures(Lcom/android/settings/biometrics/face/FaceEnrollSidecar;)[I

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Lcom/android/settings/biometrics/face/FaceUpdater;->enroll(I[BLandroid/os/CancellationSignal;Landroid/hardware/face/FaceManager$EnrollmentCallback;[ILandroid/view/Surface;Z)V

    goto :goto_1

    :cond_2
    new-instance p0, Ljava/lang/RuntimeException;

    const-string v0, "Time out waiting for surface."

    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p0

    goto :goto_2

    :catch_0
    move-exception p0

    :try_start_2
    invoke-virtual {p0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    sget-object p0, Lcom/android/settings/biometrics/face/FaceEnrollPreviewFragment;->mSurfaceReadyLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {p0}, Ljava/util/concurrent/Semaphore;->release()V

    return-void

    :goto_2
    sget-object v0, Lcom/android/settings/biometrics/face/FaceEnrollPreviewFragment;->mSurfaceReadyLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    throw p0
.end method
