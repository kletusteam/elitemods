.class Lcom/android/settings/utils/KeyboardHelper$2;
.super Ljava/lang/Object;

# interfaces
.implements Lmiuix/appcompat/app/ActionBarTransitionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/utils/KeyboardHelper;-><init>(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/utils/KeyboardHelper;


# direct methods
.method constructor <init>(Lcom/android/settings/utils/KeyboardHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/utils/KeyboardHelper$2;->this$0:Lcom/android/settings/utils/KeyboardHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionBarMove(FF)V
    .locals 1

    const/4 v0, 0x0

    cmpl-float p1, p1, v0

    if-eqz p1, :cond_0

    cmpl-float p1, p2, v0

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/utils/KeyboardHelper$2;->this$0:Lcom/android/settings/utils/KeyboardHelper;

    invoke-static {p1}, Lcom/android/settings/utils/KeyboardHelper;->-$$Nest$fgetmNestedScrollView(Lcom/android/settings/utils/KeyboardHelper;)Landroidx/core/widget/NestedScrollView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->findFocus()Landroid/view/View;

    move-result-object p1

    instance-of p2, p1, Landroid/widget/EditText;

    if-eqz p2, :cond_0

    iget-object p0, p0, Lcom/android/settings/utils/KeyboardHelper$2;->this$0:Lcom/android/settings/utils/KeyboardHelper;

    invoke-static {p0}, Lcom/android/settings/utils/KeyboardHelper;->-$$Nest$fgetmKeyboardShow(Lcom/android/settings/utils/KeyboardHelper;)Z

    move-result p0

    if-nez p0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->clearFocus()V

    :cond_0
    return-void
.end method

.method public onTransitionBegin(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public onTransitionComplete(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public onTransitionUpdate(Ljava/lang/Object;Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/Collection<",
            "Lmiuix/animation/listener/UpdateInfo;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
