.class public Lcom/android/settings/utils/NotificationUtils;
.super Ljava/lang/Object;


# static fields
.field private static TAG:Ljava/lang/String; = "NotificationUtils"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public static createNotificationBuilder(Landroid/content/Context;Ljava/lang/String;)Landroid/app/Notification$Builder;
    .locals 1

    new-instance v0, Landroid/app/Notification$Builder;

    invoke-direct {v0, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setChannelId(Ljava/lang/String;)Landroid/app/Notification$Builder;

    return-object v0
.end method

.method public static createNotificationChannel(Landroid/app/NotificationManager;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    new-instance v0, Landroid/app/NotificationChannel;

    invoke-direct {v0, p1, p2, p3}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    invoke-virtual {p0, v0}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    return-void
.end method

.method public static setCustomizedIcon(Landroid/app/Notification;Z)V
    .locals 5

    :try_start_0
    invoke-static {p0}, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->getObject(Ljava/lang/Object;)Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;

    move-result-object p0

    const-string v0, "extraNotification"

    invoke-virtual {p0, v0}, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->getObjectFiled(Ljava/lang/String;)Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;

    move-result-object p0

    invoke-virtual {p0}, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->setResultToSelf()Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;

    move-result-object p0

    const-string/jumbo v0, "setCustomizedIcon"

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    aput-object p1, v1, v4

    invoke-virtual {p0, v0, v2, v1}, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->call(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    sget-object p1, Lcom/android/settings/utils/NotificationUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v0, "setCustomizedIcon exception: "

    invoke-static {p1, v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public static setEnableFloat(Landroid/app/Notification;Z)V
    .locals 5

    :try_start_0
    invoke-static {p0}, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->getObject(Ljava/lang/Object;)Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;

    move-result-object p0

    const-string v0, "extraNotification"

    invoke-virtual {p0, v0}, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->getObjectFiled(Ljava/lang/String;)Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;

    move-result-object p0

    invoke-virtual {p0}, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->setResultToSelf()Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;

    move-result-object p0

    const-string/jumbo v0, "setEnableFloat"

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    aput-object p1, v1, v4

    invoke-virtual {p0, v0, v2, v1}, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->call(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    sget-object p1, Lcom/android/settings/utils/NotificationUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v0, "setEnableFloat exception: "

    invoke-static {p1, v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public static setEnableKeyguard(Landroid/app/Notification;Z)V
    .locals 5

    :try_start_0
    invoke-static {p0}, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->getObject(Ljava/lang/Object;)Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;

    move-result-object p0

    const-string v0, "extraNotification"

    invoke-virtual {p0, v0}, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->getObjectFiled(Ljava/lang/String;)Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;

    move-result-object p0

    invoke-virtual {p0}, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->setResultToSelf()Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;

    move-result-object p0

    const-string/jumbo v0, "setEnableKeyguard"

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    aput-object p1, v1, v4

    invoke-virtual {p0, v0, v2, v1}, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->call(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    sget-object p1, Lcom/android/settings/utils/NotificationUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v0, "setEnableKeyguard exception: "

    invoke-static {p1, v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public static setMessageCount(Landroid/app/Notification;I)V
    .locals 5

    :try_start_0
    invoke-static {p0}, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->getObject(Ljava/lang/Object;)Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;

    move-result-object p0

    const-string v0, "extraNotification"

    invoke-virtual {p0, v0}, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->getObjectFiled(Ljava/lang/String;)Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;

    move-result-object p0

    invoke-virtual {p0}, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->setResultToSelf()Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;

    move-result-object p0

    const-string/jumbo v0, "setMessageCount"

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v1, v4

    invoke-virtual {p0, v0, v2, v1}, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->call(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    sget-object p1, Lcom/android/settings/utils/NotificationUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v0, "setMessageCount exception: "

    invoke-static {p1, v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method
