.class public Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/utils/ReflectBuilderUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ReflAgent"
.end annotation


# instance fields
.field private mClass:Ljava/lang/Class;

.field private mObject:Ljava/lang/Object;

.field private mResult:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getObject(Ljava/lang/Object;)Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;
    .locals 1

    new-instance v0, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;

    invoke-direct {v0}, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;-><init>()V

    if-eqz p0, :cond_0

    iput-object p0, v0, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->mObject:Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    iput-object p0, v0, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->mClass:Ljava/lang/Class;

    :cond_0
    return-object v0
.end method


# virtual methods
.method public varargs call(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Class<",
            "*>;[",
            "Ljava/lang/Object;",
            ")",
            "Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->mObject:Ljava/lang/Object;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-static {v0, p1, p2, p3}, Lcom/android/settings/utils/ReflectBuilderUtil;->callObjectMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->mResult:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    :cond_0
    :goto_0
    return-object p0
.end method

.method public getObjectFiled(Ljava/lang/String;)Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->mObject:Ljava/lang/Object;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-static {v0, p1}, Lcom/android/settings/utils/ReflectBuilderUtil;->getObjectField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->mResult:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    :cond_0
    :goto_0
    return-object p0
.end method

.method public setResultToSelf()Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->mResult:Ljava/lang/Object;

    iput-object v0, p0, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->mObject:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/utils/ReflectBuilderUtil$ReflAgent;->mResult:Ljava/lang/Object;

    return-object p0
.end method
