.class public final Lcom/android/settings/fuelgauge/BatteryBackupHelper;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/app/backup/BackupHelper;


# static fields
.field private static final DEBUG:Z


# instance fields
.field mBatteryOptimizeUtils:Lcom/android/settings/fuelgauge/BatteryOptimizeUtils;

.field private final mContext:Landroid/content/Context;

.field mIDeviceIdleController:Landroid/os/IDeviceIdleController;

.field mIPackageManager:Landroid/content/pm/IPackageManager;

.field mPowerAllowlistBackend:Lcom/android/settingslib/fuelgauge/PowerAllowlistBackend;

.field mTestApplicationInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/content/pm/ApplicationInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string/jumbo v1, "userdebug"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->DEBUG:Z

    return-void
.end method

.method private backupFullPowerList(Landroid/app/backup/BackupDataOutput;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/backup/BackupDataOutput;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "BatteryBackupHelper"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    :try_start_0
    invoke-direct {p0}, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->getIDeviceIdleController()Landroid/os/IDeviceIdleController;

    move-result-object p0

    invoke-interface {p0}, Landroid/os/IDeviceIdleController;->getFullPowerWhitelist()[Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p0, :cond_1

    array-length v3, p0

    if-nez v3, :cond_0

    goto :goto_0

    :cond_0
    const-string v3, ","

    invoke-static {v3, p0}, Ljava/lang/String;->join(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "full_power_list"

    invoke-static {p1, v4, v3}, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->writeBackupData(Landroid/app/backup/BackupDataOutput;Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    const/4 v3, 0x0

    array-length v4, p0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, p1, v3

    const/4 v3, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, p1, v3

    const-string v1, "backup getFullPowerList() size=%d in %d/ms"

    invoke-static {v1, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    return-object p0

    :cond_1
    :goto_0
    const-string/jumbo p0, "no data found in the getFullPowerList()"

    invoke-static {v0, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    return-object p0

    :catch_0
    move-exception p0

    const-string p1, "backupFullPowerList() failed"

    invoke-static {v0, p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 p0, 0x0

    return-object p0
.end method

.method private debugLog(Ljava/lang/String;)V
    .locals 0

    sget-boolean p0, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->DEBUG:Z

    if-eqz p0, :cond_0

    const-string p0, "BatteryBackupHelper"

    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method private getIDeviceIdleController()Landroid/os/IDeviceIdleController;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->mIDeviceIdleController:Landroid/os/IDeviceIdleController;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "deviceidle"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/os/IDeviceIdleController$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IDeviceIdleController;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->mIDeviceIdleController:Landroid/os/IDeviceIdleController;

    return-object v0
.end method

.method private getIPackageManager()Landroid/content/pm/IPackageManager;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->mIPackageManager:Landroid/content/pm/IPackageManager;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->mIPackageManager:Landroid/content/pm/IPackageManager;

    return-object v0
.end method

.method private getInstalledApplications()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/content/pm/ApplicationInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->mTestApplicationInfoList:Ljava/util/List;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->mContext:Landroid/content/Context;

    const-class v2, Landroid/os/UserManager;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/UserManager;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/os/UserManager;->getProfiles(I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/UserInfo;

    :try_start_0
    invoke-direct {p0}, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->getIPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v3

    invoke-virtual {v2}, Landroid/content/pm/UserInfo;->isAdmin()Z

    move-result v4

    if-eqz v4, :cond_2

    const-wide/32 v4, 0x408200

    goto :goto_1

    :cond_2
    const-wide/32 v4, 0x8200

    :goto_1
    iget v2, v2, Landroid/content/pm/UserInfo;->id:I

    invoke-interface {v3, v4, v5, v2}, Landroid/content/pm/IPackageManager;->getInstalledApplications(JI)Landroid/content/pm/ParceledListSlice;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/content/pm/ParceledListSlice;->getList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    const-string v0, "BatteryBackupHelper"

    const-string v1, "getInstalledApplications() is failed"

    invoke-static {v0, v1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 p0, 0x0

    return-object p0

    :cond_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p0

    add-int/lit8 p0, p0, -0x1

    :goto_2
    if-ltz p0, :cond_5

    invoke-interface {v0, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ApplicationInfo;

    iget v2, v1, Landroid/content/pm/ApplicationInfo;->enabledSetting:I

    const/4 v3, 0x3

    if-eq v2, v3, :cond_4

    iget-boolean v1, v1, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-nez v1, :cond_4

    invoke-interface {v0, p0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_4
    add-int/lit8 p0, p0, -0x1

    goto :goto_2

    :cond_5
    return-object v0
.end method

.method private getPowerAllowlistBackend()Lcom/android/settingslib/fuelgauge/PowerAllowlistBackend;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->mPowerAllowlistBackend:Lcom/android/settingslib/fuelgauge/PowerAllowlistBackend;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settingslib/fuelgauge/PowerAllowlistBackend;->getInstance(Landroid/content/Context;)Lcom/android/settingslib/fuelgauge/PowerAllowlistBackend;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->mPowerAllowlistBackend:Lcom/android/settingslib/fuelgauge/PowerAllowlistBackend;

    return-object v0
.end method

.method private static isOwner()Z
    .locals 1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isSystemOrDefaultApp(Ljava/lang/String;)Z
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->getPowerAllowlistBackend()Lcom/android/settingslib/fuelgauge/PowerAllowlistBackend;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/android/settingslib/fuelgauge/PowerAllowlistBackend;->isSysAllowlisted(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/android/settingslib/fuelgauge/PowerAllowlistBackend;->isDefaultActiveApp(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private restoreOptimizationMode(Ljava/lang/String;I)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/fuelgauge/BatteryUtils;->getInstance(Landroid/content/Context;)Lcom/android/settings/fuelgauge/BatteryUtils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/settings/fuelgauge/BatteryUtils;->getPackageUid(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->mBatteryOptimizeUtils:Lcom/android/settings/fuelgauge/BatteryOptimizeUtils;

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/android/settings/fuelgauge/BatteryOptimizeUtils;

    iget-object p0, p0, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->mContext:Landroid/content/Context;

    invoke-direct {v1, p0, v0, p1}, Lcom/android/settings/fuelgauge/BatteryOptimizeUtils;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    :goto_0
    invoke-virtual {v1, p2}, Lcom/android/settings/fuelgauge/BatteryOptimizeUtils;->setAppUsageState(I)V

    const/4 p0, 0x2

    new-array p0, p0, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p1, p0, v0

    const/4 p1, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, p0, p1

    const-string/jumbo p1, "restore:%s mode=%d"

    invoke-static {p1, p0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "BatteryBackupHelper"

    invoke-static {p1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private static writeBackupData(Landroid/app/backup/BackupDataOutput;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object p2

    :try_start_0
    array-length v0, p2

    invoke-virtual {p0, p1, v0}, Landroid/app/backup/BackupDataOutput;->writeEntityHeader(Ljava/lang/String;I)I

    array-length v0, p2

    invoke-virtual {p0, p2, v0}, Landroid/app/backup/BackupDataOutput;->writeEntityData([BI)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v0, "writeBackupData() is failed for "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "BatteryBackupHelper"

    invoke-static {p2, p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method


# virtual methods
.method backupOptimizationMode(Landroid/app/backup/BackupDataOutput;Ljava/util/List;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/backup/BackupDataOutput;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_4

    nop

    :goto_0
    invoke-static {v6, v5, v0}, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->writeBackupData(Landroid/app/backup/BackupDataOutput;Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_14

    nop

    :goto_1
    invoke-direct {p0}, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->getInstalledApplications()Ljava/util/List;

    move-result-object v3

    goto/32 :goto_3b

    nop

    :goto_2
    const/16 v12, 0x46

    goto/32 :goto_3

    nop

    :goto_3
    iget v13, v10, Landroid/content/pm/ApplicationInfo;->uid:I

    goto/32 :goto_16

    nop

    :goto_4
    move-object v0, p0

    goto/32 :goto_19

    nop

    :goto_5
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_21

    nop

    :goto_6
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_35

    nop

    :goto_7
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto/32 :goto_37

    nop

    :goto_8
    new-instance v11, Ljava/lang/StringBuilder;

    goto/32 :goto_38

    nop

    :goto_9
    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_10

    nop

    :goto_a
    aput-object v3, v0, v5

    goto/32 :goto_4c

    nop

    :goto_b
    invoke-interface {v14, v13}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v13

    goto/32 :goto_3f

    nop

    :goto_c
    aput-object v3, v0, v8

    goto/32 :goto_2f

    nop

    :goto_d
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_25

    nop

    :goto_e
    move v9, v8

    :goto_f
    goto/32 :goto_20

    nop

    :goto_10
    invoke-direct {p0, v10}, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->debugLog(Ljava/lang/String;)V

    goto/32 :goto_46

    nop

    :goto_11
    new-instance v11, Ljava/lang/StringBuilder;

    goto/32 :goto_50

    nop

    :goto_12
    iget-object v11, v10, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    goto/32 :goto_29

    nop

    :goto_13
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_1e

    nop

    :goto_14
    new-array v0, v11, [Ljava/lang/Object;

    goto/32 :goto_44

    nop

    :goto_15
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_51

    nop

    :goto_16
    iget-object v14, v10, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    goto/32 :goto_28

    nop

    :goto_17
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/32 :goto_c

    nop

    :goto_18
    if-nez v11, :cond_0

    goto/32 :goto_2e

    :cond_0
    goto/32 :goto_2d

    nop

    :goto_19
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    goto/32 :goto_1

    nop

    :goto_1a
    if-nez v5, :cond_1

    goto/32 :goto_34

    :cond_1
    goto/32 :goto_33

    nop

    :goto_1b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_49

    nop

    :goto_1c
    return-void

    :goto_1d
    goto/32 :goto_27

    nop

    :goto_1e
    const-string/jumbo v5, "optimization_mode_list"

    goto/32 :goto_4b

    nop

    :goto_1f
    const/4 v11, 0x3

    goto/32 :goto_2c

    nop

    :goto_20
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    goto/32 :goto_1f

    nop

    :goto_21
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto/32 :goto_9

    nop

    :goto_22
    if-ne v12, v11, :cond_2

    goto/32 :goto_f

    :cond_2
    goto/32 :goto_4e

    nop

    :goto_23
    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    goto/32 :goto_30

    nop

    :goto_24
    const-string v12, ","

    goto/32 :goto_5

    nop

    :goto_25
    const-string v10, ":"

    goto/32 :goto_4f

    nop

    :goto_26
    iget-object v10, v10, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    goto/32 :goto_d

    nop

    :goto_27
    const-string/jumbo v0, "no data found in the getInstalledApplications()"

    goto/32 :goto_39

    nop

    :goto_28
    invoke-virtual {v6, v12, v13, v14}, Landroid/app/AppOpsManager;->checkOpNoThrow(IILjava/lang/String;)I

    move-result v12

    goto/32 :goto_36

    nop

    :goto_29
    invoke-direct {p0, v11}, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->isSystemOrDefaultApp(Ljava/lang/String;)Z

    move-result v11

    goto/32 :goto_18

    nop

    :goto_2a
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v5

    goto/32 :goto_1a

    nop

    :goto_2b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    goto/32 :goto_45

    nop

    :goto_2c
    if-nez v10, :cond_3

    goto/32 :goto_41

    :cond_3
    goto/32 :goto_32

    nop

    :goto_2d
    goto/16 :goto_f

    :goto_2e
    goto/32 :goto_8

    nop

    :goto_2f
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/32 :goto_47

    nop

    :goto_30
    check-cast v6, Landroid/app/AppOpsManager;

    goto/32 :goto_3e

    nop

    :goto_31
    move-object/from16 v14, p2

    goto/32 :goto_b

    nop

    :goto_32
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    goto/32 :goto_43

    nop

    :goto_33
    goto/16 :goto_1d

    :goto_34
    goto/32 :goto_4a

    nop

    :goto_35
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/32 :goto_11

    nop

    :goto_36
    iget-object v13, v10, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    goto/32 :goto_31

    nop

    :goto_37
    aput-object v1, v0, v3

    goto/32 :goto_3c

    nop

    :goto_38
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_26

    nop

    :goto_39
    invoke-static {v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_42

    nop

    :goto_3a
    const-class v7, Landroid/app/AppOpsManager;

    goto/32 :goto_23

    nop

    :goto_3b
    const-string v4, "BatteryBackupHelper"

    goto/32 :goto_48

    nop

    :goto_3c
    const-string v1, "backup getInstalledApplications():%d count=%d in %d/ms"

    goto/32 :goto_15

    nop

    :goto_3d
    const/4 v8, 0x0

    goto/32 :goto_e

    nop

    :goto_3e
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    goto/32 :goto_3d

    nop

    :goto_3f
    invoke-static {v12, v13}, Lcom/android/settings/fuelgauge/BatteryOptimizeUtils;->getAppOptimizationMode(IZ)I

    move-result v12

    goto/32 :goto_22

    nop

    :goto_40
    goto/16 :goto_f

    :goto_41
    goto/32 :goto_13

    nop

    :goto_42
    return-void

    :goto_43
    check-cast v10, Landroid/content/pm/ApplicationInfo;

    goto/32 :goto_2

    nop

    :goto_44
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    goto/32 :goto_17

    nop

    :goto_45
    sub-long/2addr v5, v1

    goto/32 :goto_7

    nop

    :goto_46
    add-int/lit8 v9, v9, 0x1

    goto/32 :goto_40

    nop

    :goto_47
    const/4 v5, 0x1

    goto/32 :goto_a

    nop

    :goto_48
    if-nez v3, :cond_4

    goto/32 :goto_1d

    :cond_4
    goto/32 :goto_2a

    nop

    :goto_49
    iget-object v6, v0, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->mContext:Landroid/content/Context;

    goto/32 :goto_3a

    nop

    :goto_4a
    new-instance v5, Ljava/lang/StringBuilder;

    goto/32 :goto_1b

    nop

    :goto_4b
    move-object/from16 v6, p1

    goto/32 :goto_0

    nop

    :goto_4c
    const/4 v3, 0x2

    goto/32 :goto_2b

    nop

    :goto_4d
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_24

    nop

    :goto_4e
    if-nez v12, :cond_5

    goto/32 :goto_f

    :cond_5
    goto/32 :goto_12

    nop

    :goto_4f
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_50
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_4d

    nop

    :goto_51
    invoke-static {v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_1c

    nop
.end method

.method public performBackup(Landroid/os/ParcelFileDescriptor;Landroid/app/backup/BackupDataOutput;Landroid/os/ParcelFileDescriptor;)V
    .locals 0

    invoke-static {}, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->isOwner()Z

    move-result p1

    if-eqz p1, :cond_2

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0, p2}, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->backupFullPowerList(Landroid/app/backup/BackupDataOutput;)Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p0, p2, p1}, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->backupOptimizationMode(Landroid/app/backup/BackupDataOutput;Ljava/util/List;)V

    :cond_1
    return-void

    :cond_2
    :goto_0
    const-string p0, "BatteryBackupHelper"

    const-string p1, "ignore performBackup() for non-owner or empty data"

    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public restoreEntity(Landroid/app/backup/BackupDataInputStream;)V
    .locals 4

    invoke-static {}, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->isOwner()Z

    move-result v0

    const-string v1, "BatteryBackupHelper"

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/app/backup/BackupDataInputStream;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Landroid/app/backup/BackupDataInputStream;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "optimization_mode_list"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/app/backup/BackupDataInputStream;->size()I

    move-result v0

    new-array v2, v0, [B

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p1, v2, v3, v0}, Landroid/app/backup/BackupDataInputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {p0, v2}, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->restoreOptimizationMode([B)V

    goto :goto_0

    :catch_0
    move-exception p0

    const-string p1, "failed to load BackupDataInputStream"

    invoke-static {v1, p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    :goto_1
    const-string p0, "ignore restoreEntity() for non-owner or empty data"

    invoke-static {v1, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method restoreOptimizationMode([B)V
    .locals 10

    goto/32 :goto_37

    nop

    :goto_0
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto/32 :goto_3b

    nop

    :goto_1
    invoke-virtual {v6, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    goto/32 :goto_26

    nop

    :goto_2
    invoke-static {v3, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_6

    nop

    :goto_3
    invoke-static {v3, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_29

    nop

    :goto_4
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    goto/32 :goto_42

    nop

    :goto_5
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result p1

    goto/32 :goto_12

    nop

    :goto_6
    return-void

    :goto_7
    goto/32 :goto_c

    nop

    :goto_8
    invoke-direct {p0, v7}, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->isSystemOrDefaultApp(Ljava/lang/String;)Z

    move-result v9

    goto/32 :goto_27

    nop

    :goto_9
    goto/16 :goto_25

    :catch_0
    move-exception v6

    goto/32 :goto_30

    nop

    :goto_a
    if-eqz v2, :cond_0

    goto/32 :goto_4a

    :cond_0
    goto/32 :goto_49

    nop

    :goto_b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_20

    nop

    :goto_c
    const-string/jumbo p0, "no data found from the split() processing"

    goto/32 :goto_2c

    nop

    :goto_d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_46

    nop

    :goto_e
    goto/16 :goto_34

    :goto_f
    goto/32 :goto_11

    nop

    :goto_10
    invoke-static {v3, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_1d

    nop

    :goto_11
    aget-object v7, v6, v2

    goto/32 :goto_8

    nop

    :goto_12
    const-string v3, "BatteryBackupHelper"

    goto/32 :goto_18

    nop

    :goto_13
    sub-long/2addr v4, v0

    goto/32 :goto_4

    nop

    :goto_14
    return-void

    :goto_15
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_3f

    nop

    :goto_16
    new-instance v2, Ljava/lang/String;

    goto/32 :goto_23

    nop

    :goto_17
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1a

    nop

    :goto_18
    if-nez p1, :cond_1

    goto/32 :goto_1e

    :cond_1
    goto/32 :goto_32

    nop

    :goto_19
    const-string v8, "failed to parse the optimization mode: "

    goto/32 :goto_15

    nop

    :goto_1a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_22

    nop

    :goto_1b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    goto/32 :goto_13

    nop

    :goto_1c
    const-string v7, "invalid raw data found:"

    goto/32 :goto_d

    nop

    :goto_1d
    return-void

    :goto_1e
    goto/32 :goto_55

    nop

    :goto_1f
    new-instance v6, Ljava/lang/StringBuilder;

    goto/32 :goto_38

    nop

    :goto_20
    const-string v8, "ignore from isSystemOrDefaultApp():"

    goto/32 :goto_17

    nop

    :goto_21
    const/4 v7, 0x2

    goto/32 :goto_44

    nop

    :goto_22
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/32 :goto_3

    nop

    :goto_23
    sget-object v3, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    goto/32 :goto_45

    nop

    :goto_24
    invoke-static {v3, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_25
    goto/32 :goto_52

    nop

    :goto_26
    if-nez v6, :cond_2

    goto/32 :goto_34

    :cond_2
    goto/32 :goto_2d

    nop

    :goto_27
    if-nez v9, :cond_3

    goto/32 :goto_2a

    :cond_3
    goto/32 :goto_2f

    nop

    :goto_28
    invoke-static {p1, p0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_2

    nop

    :goto_29
    goto :goto_25

    :goto_2a
    :try_start_0
    aget-object v6, v6, v8

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_47

    nop

    :goto_2b
    invoke-static {v3, v7, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/32 :goto_33

    nop

    :goto_2c
    invoke-static {v3, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_14

    nop

    :goto_2d
    array-length v9, v6

    goto/32 :goto_31

    nop

    :goto_2e
    array-length v6, p1

    goto/32 :goto_21

    nop

    :goto_2f
    new-instance v6, Ljava/lang/StringBuilder;

    goto/32 :goto_b

    nop

    :goto_30
    new-instance v7, Ljava/lang/StringBuilder;

    goto/32 :goto_51

    nop

    :goto_31
    if-ne v9, v7, :cond_4

    goto/32 :goto_f

    :cond_4
    goto/32 :goto_e

    nop

    :goto_32
    const-string/jumbo p0, "no data found in the restoreOptimizationMode()"

    goto/32 :goto_10

    nop

    :goto_33
    goto :goto_25

    :goto_34
    goto/32 :goto_1f

    nop

    :goto_35
    goto/16 :goto_50

    :goto_36
    goto/32 :goto_3c

    nop

    :goto_37
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    goto/32 :goto_16

    nop

    :goto_38
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1c

    nop

    :goto_39
    const/4 v2, 0x0

    goto/32 :goto_48

    nop

    :goto_3a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_54

    nop

    :goto_3b
    aput-object p1, p0, v2

    goto/32 :goto_1b

    nop

    :goto_3c
    new-array p0, v7, [Ljava/lang/Object;

    goto/32 :goto_0

    nop

    :goto_3d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/32 :goto_24

    nop

    :goto_3e
    if-nez p1, :cond_5

    goto/32 :goto_7

    :cond_5
    goto/32 :goto_4e

    nop

    :goto_3f
    aget-object v8, p1, v4

    goto/32 :goto_3a

    nop

    :goto_40
    if-lt v4, v6, :cond_6

    goto/32 :goto_36

    :cond_6
    goto/32 :goto_4b

    nop

    :goto_41
    const-string v9, ":"

    goto/32 :goto_1

    nop

    :goto_42
    aput-object p1, p0, v8

    goto/32 :goto_53

    nop

    :goto_43
    invoke-virtual {v2, p1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_3e

    nop

    :goto_44
    const/4 v8, 0x1

    goto/32 :goto_40

    nop

    :goto_45
    invoke-direct {v2, p1, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    goto/32 :goto_5

    nop

    :goto_46
    aget-object v7, p1, v4

    goto/32 :goto_4d

    nop

    :goto_47
    invoke-direct {p0, v7, v6}, Lcom/android/settings/fuelgauge/BatteryBackupHelper;->restoreOptimizationMode(Ljava/lang/String;I)V

    goto/32 :goto_4c

    nop

    :goto_48
    move v4, v2

    goto/32 :goto_4f

    nop

    :goto_49
    goto/16 :goto_7

    :goto_4a
    goto/32 :goto_39

    nop

    :goto_4b
    aget-object v6, p1, v4

    goto/32 :goto_41

    nop

    :goto_4c
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_9

    nop

    :goto_4d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_3d

    nop

    :goto_4e
    array-length v2, p1

    goto/32 :goto_a

    nop

    :goto_4f
    move v5, v4

    :goto_50
    goto/32 :goto_2e

    nop

    :goto_51
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_19

    nop

    :goto_52
    add-int/lit8 v4, v4, 0x1

    goto/32 :goto_35

    nop

    :goto_53
    const-string/jumbo p1, "restoreOptimizationMode() count=%d in %d/ms"

    goto/32 :goto_28

    nop

    :goto_54
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto/32 :goto_2b

    nop

    :goto_55
    const-string p1, ","

    goto/32 :goto_43

    nop
.end method

.method public writeNewStateDescription(Landroid/os/ParcelFileDescriptor;)V
    .locals 0

    return-void
.end method
