.class Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;
.super Landroid/database/ContentObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/freeform/FlashBackSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FlashBackMainSwitchObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/freeform/FlashBackSettings;


# direct methods
.method public constructor <init>(Lcom/android/settings/freeform/FlashBackSettings;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;->this$0:Lcom/android/settings/freeform/FlashBackSettings;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method observe()V
    .locals 3

    goto/32 :goto_7

    nop

    :goto_0
    invoke-static {v0}, Lcom/android/settings/freeform/FlashBackSettings;->access$000(Lcom/android/settings/freeform/FlashBackSettings;)Landroid/content/Context;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_1
    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_2
    return-void

    :goto_3
    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto/32 :goto_2

    nop

    :goto_4
    const-string v1, "FlashBackMainSwitch"

    goto/32 :goto_1

    nop

    :goto_5
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_6
    const/4 v2, 0x0

    goto/32 :goto_3

    nop

    :goto_7
    iget-object v0, p0, Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;->this$0:Lcom/android/settings/freeform/FlashBackSettings;

    goto/32 :goto_0

    nop
.end method

.method public onChange(Z)V
    .locals 3

    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    invoke-static {}, Lcom/android/settings/freeform/FlashBackSettings;->-$$Nest$sfgetsFlashBacSettingDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MainSetting Value change: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p1, " mFlashBackMainSwitchValue: "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;->this$0:Lcom/android/settings/freeform/FlashBackSettings;

    invoke-static {p1}, Lcom/android/settings/freeform/FlashBackSettings;->-$$Nest$fgetmFlashBackMainSwitchState(Lcom/android/settings/freeform/FlashBackSettings;)I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "FBSettings"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object p1, p0, Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;->this$0:Lcom/android/settings/freeform/FlashBackSettings;

    invoke-static {p1}, Lcom/android/settings/freeform/FlashBackSettings;->-$$Nest$fgetmFlashBackMainSwitchState(Lcom/android/settings/freeform/FlashBackSettings;)I

    move-result p1

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    iget-object p1, p0, Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;->this$0:Lcom/android/settings/freeform/FlashBackSettings;

    invoke-static {p1}, Lcom/android/settings/freeform/FlashBackSettings;->-$$Nest$fgetmPreferenceCategoryForBackgroundSupport(Lcom/android/settings/freeform/FlashBackSettings;)Landroidx/preference/PreferenceCategory;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p1, p0, Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;->this$0:Lcom/android/settings/freeform/FlashBackSettings;

    invoke-static {p1}, Lcom/android/settings/freeform/FlashBackSettings;->-$$Nest$fgetmPreferenceCategoryForAppSupport(Lcom/android/settings/freeform/FlashBackSettings;)Landroidx/preference/PreferenceCategory;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p1, p0, Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;->this$0:Lcom/android/settings/freeform/FlashBackSettings;

    invoke-static {p1}, Lcom/android/settings/freeform/FlashBackSettings;->-$$Nest$fgetmBackgroundStartSwitchPreference(Lcom/android/settings/freeform/FlashBackSettings;)Landroidx/preference/CheckBoxPreference;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroidx/preference/Preference;->setVisible(Z)V

    :goto_0
    iget-object p1, p0, Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;->this$0:Lcom/android/settings/freeform/FlashBackSettings;

    invoke-static {p1}, Lcom/android/settings/freeform/FlashBackSettings;->-$$Nest$fgetmAppsCheckBoxPreferenceList(Lcom/android/settings/freeform/FlashBackSettings;)Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-ge v0, p1, :cond_2

    iget-object p1, p0, Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;->this$0:Lcom/android/settings/freeform/FlashBackSettings;

    invoke-virtual {p1}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object v2, p0, Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;->this$0:Lcom/android/settings/freeform/FlashBackSettings;

    invoke-static {v2}, Lcom/android/settings/freeform/FlashBackSettings;->-$$Nest$fgetmAppsCheckBoxPreferenceList(Lcom/android/settings/freeform/FlashBackSettings;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroidx/preference/Preference;

    invoke-virtual {p1, v2}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    iget-object p1, p0, Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;->this$0:Lcom/android/settings/freeform/FlashBackSettings;

    invoke-static {p1}, Lcom/android/settings/freeform/FlashBackSettings;->-$$Nest$fgetmAppsCheckBoxPreferenceList(Lcom/android/settings/freeform/FlashBackSettings;)Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, v1}, Landroidx/preference/Preference;->setVisible(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;->this$0:Lcom/android/settings/freeform/FlashBackSettings;

    invoke-static {p1}, Lcom/android/settings/freeform/FlashBackSettings;->-$$Nest$fgetmPreferenceCategoryForBackgroundSupport(Lcom/android/settings/freeform/FlashBackSettings;)Landroidx/preference/PreferenceCategory;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p1, p0, Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;->this$0:Lcom/android/settings/freeform/FlashBackSettings;

    invoke-static {p1}, Lcom/android/settings/freeform/FlashBackSettings;->-$$Nest$fgetmPreferenceCategoryForAppSupport(Lcom/android/settings/freeform/FlashBackSettings;)Landroidx/preference/PreferenceCategory;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p1, p0, Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;->this$0:Lcom/android/settings/freeform/FlashBackSettings;

    invoke-static {p1}, Lcom/android/settings/freeform/FlashBackSettings;->-$$Nest$fgetmBackgroundStartSwitchPreference(Lcom/android/settings/freeform/FlashBackSettings;)Landroidx/preference/CheckBoxPreference;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    move p1, v0

    :goto_1
    iget-object v1, p0, Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;->this$0:Lcom/android/settings/freeform/FlashBackSettings;

    invoke-static {v1}, Lcom/android/settings/freeform/FlashBackSettings;->-$$Nest$fgetmAppsCheckBoxPreferenceList(Lcom/android/settings/freeform/FlashBackSettings;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;->this$0:Lcom/android/settings/freeform/FlashBackSettings;

    invoke-virtual {v1}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;->this$0:Lcom/android/settings/freeform/FlashBackSettings;

    invoke-static {v2}, Lcom/android/settings/freeform/FlashBackSettings;->-$$Nest$fgetmAppsCheckBoxPreferenceList(Lcom/android/settings/freeform/FlashBackSettings;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroidx/preference/Preference;

    invoke-virtual {v1, v2}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    iget-object v1, p0, Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;->this$0:Lcom/android/settings/freeform/FlashBackSettings;

    invoke-static {v1}, Lcom/android/settings/freeform/FlashBackSettings;->-$$Nest$fgetmAppsCheckBoxPreferenceList(Lcom/android/settings/freeform/FlashBackSettings;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    :cond_2
    return-void
.end method

.method unObserve()V
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    goto/32 :goto_3

    nop

    :goto_2
    invoke-static {v0}, Lcom/android/settings/freeform/FlashBackSettings;->access$100(Lcom/android/settings/freeform/FlashBackSettings;)Landroid/content/Context;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_3
    return-void

    :goto_4
    iget-object v0, p0, Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;->this$0:Lcom/android/settings/freeform/FlashBackSettings;

    goto/32 :goto_2

    nop
.end method
