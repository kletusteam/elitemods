.class public Lcom/android/settings/freeform/FlashBackSettings;
.super Lcom/android/settings/SettingsPreferenceFragment;

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;
    }
.end annotation


# static fields
.field private static sFlashBacSettingDebug:Z = true


# instance fields
.field private mAppsCheckBoxPreferenceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroidx/preference/CheckBoxPreference;",
            ">;"
        }
    .end annotation
.end field

.field private mBackgroundStartSwitchPreference:Landroidx/preference/CheckBoxPreference;

.field private mDB:Landroid/database/sqlite/SQLiteDatabase;

.field private mDBHelper:Lcom/android/settings/freeform/FlashBackDataHelper;

.field private mFlashBackBackgroundStartSwitchState:I

.field private mFlashBackMainSwitchObserver:Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;

.field private mFlashBackMainSwitchState:I

.field private mKillFlashBackServiceIntent:Landroid/content/Intent;

.field private mMainSwitchPreference:Landroidx/preference/CheckBoxPreference;

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mPackageNameMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPreferenceCategoryForAppSupport:Landroidx/preference/PreferenceCategory;

.field private mPreferenceCategoryForBackgroundSupport:Landroidx/preference/PreferenceCategory;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAppsCheckBoxPreferenceList(Lcom/android/settings/freeform/FlashBackSettings;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mAppsCheckBoxPreferenceList:Ljava/util/ArrayList;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmBackgroundStartSwitchPreference(Lcom/android/settings/freeform/FlashBackSettings;)Landroidx/preference/CheckBoxPreference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mBackgroundStartSwitchPreference:Landroidx/preference/CheckBoxPreference;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFlashBackMainSwitchState(Lcom/android/settings/freeform/FlashBackSettings;)I
    .locals 0

    iget p0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mFlashBackMainSwitchState:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmPreferenceCategoryForAppSupport(Lcom/android/settings/freeform/FlashBackSettings;)Landroidx/preference/PreferenceCategory;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mPreferenceCategoryForAppSupport:Landroidx/preference/PreferenceCategory;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPreferenceCategoryForBackgroundSupport(Lcom/android/settings/freeform/FlashBackSettings;)Landroidx/preference/PreferenceCategory;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mPreferenceCategoryForBackgroundSupport:Landroidx/preference/PreferenceCategory;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mbackgroundStartSwitchPreferenceOnChange(Lcom/android/settings/freeform/FlashBackSettings;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/freeform/FlashBackSettings;->backgroundStartSwitchPreferenceOnChange(Ljava/lang/Object;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mmainSwitchPreferenceOnChange(Lcom/android/settings/freeform/FlashBackSettings;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/freeform/FlashBackSettings;->mainSwitchPreferenceOnChange(Ljava/lang/Object;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mperformClick(Lcom/android/settings/freeform/FlashBackSettings;Landroidx/preference/Preference;Ljava/lang/Boolean;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/freeform/FlashBackSettings;->performClick(Landroidx/preference/Preference;Ljava/lang/Boolean;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetsFlashBacSettingDebug()Z
    .locals 1

    sget-boolean v0, Lcom/android/settings/freeform/FlashBackSettings;->sFlashBacSettingDebug:Z

    return v0
.end method

.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/SettingsPreferenceFragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mAppsCheckBoxPreferenceList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mPackageNameMap:Ljava/util/HashMap;

    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/freeform/FlashBackSettings;)Landroid/content/Context;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->getPrefContext()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/android/settings/freeform/FlashBackSettings;)Landroid/content/Context;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->getPrefContext()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method private addAppSupportStatus(Ljava/lang/String;)V
    .locals 13

    const-string/jumbo v0, "packageName"

    const-string v1, "com.tencent.tmgp.sgame"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "com.xiaomi.joyose"

    invoke-direct {p0, v1}, Lcom/android/settings/freeform/FlashBackSettings;->readMetaData(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/settings/freeform/FlashBackSettings;->readMetaData(Ljava/lang/String;)Z

    move-result v1

    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/android/settings/freeform/FlashBackSettings;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "FlashBack_Support_Apps"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "packageName=?"

    const/4 v11, 0x1

    new-array v6, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    aput-object p1, v6, v12

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v3, "FlashBack_Support_Apps"

    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_2

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {v1, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo p1, "switch"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object p0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const/4 p1, 0x0

    invoke-virtual {p0, v3, p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    goto :goto_1

    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    iget-object p0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string/jumbo v0, "packageName=?"

    new-array v1, v11, [Ljava/lang/String;

    aput-object p1, v1, v12

    invoke-virtual {p0, v3, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_2
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_2
    return-void
.end method

.method private backgroundStartSwitchPreferenceOnChange(Ljava/lang/Object;)V
    .locals 5

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    move v1, v3

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    const-string v4, "FlashBackBackgroundStartSwitch"

    invoke-static {v0, v4, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-ne p1, v3, :cond_1

    move v2, v3

    :cond_1
    iput v2, p0, Lcom/android/settings/freeform/FlashBackSettings;->mFlashBackBackgroundStartSwitchState:I

    sget-boolean p1, Lcom/android/settings/freeform/FlashBackSettings;->sFlashBacSettingDebug:Z

    if-eqz p1, :cond_2

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "mFlashBackBackgroundStartSwitch New Result: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mFlashBackBackgroundStartSwitchState:I

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "FBSettings"

    invoke-static {p1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method private buildCheckBoxPreference(Landroid/content/pm/ApplicationInfo;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_1

    new-instance p1, Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->getPrefContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Landroidx/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, p3}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p1, p5}, Lcom/android/settings/freeform/FlashBackSettings;->setCheckBoxPreferenceSummary(Landroidx/preference/CheckBoxPreference;Ljava/lang/String;)Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p2}, Landroidx/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    const/4 p2, 0x1

    if-ne p4, p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-virtual {p1, p2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    iget-object p2, p0, Lcom/android/settings/freeform/FlashBackSettings;->mAppsCheckBoxPreferenceList:Ljava/util/ArrayList;

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance p2, Lcom/android/settings/freeform/FlashBackSettings$3;

    invoke-direct {p2, p0}, Lcom/android/settings/freeform/FlashBackSettings$3;-><init>(Lcom/android/settings/freeform/FlashBackSettings;)V

    invoke-virtual {p1, p2}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_1
    return-void
.end method

.method private checkVisibility()V
    .locals 4

    iget v0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mFlashBackMainSwitchState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mPreferenceCategoryForBackgroundSupport:Landroidx/preference/PreferenceCategory;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object v0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mPreferenceCategoryForAppSupport:Landroidx/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object v0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mBackgroundStartSwitchPreference:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setVisible(Z)V

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/android/settings/freeform/FlashBackSettings;->mAppsCheckBoxPreferenceList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/freeform/FlashBackSettings;->mAppsCheckBoxPreferenceList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroidx/preference/Preference;

    invoke-virtual {v2, v3}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    iget-object v2, p0, Lcom/android/settings/freeform/FlashBackSettings;->mAppsCheckBoxPreferenceList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v2, v1}, Landroidx/preference/Preference;->setVisible(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private initAppsData()V
    .locals 1

    const-string v0, "com.tencent.tmgp.sgame"

    invoke-direct {p0, v0}, Lcom/android/settings/freeform/FlashBackSettings;->addAppSupportStatus(Ljava/lang/String;)V

    const-string v0, "com.sankuai.meituan"

    invoke-direct {p0, v0}, Lcom/android/settings/freeform/FlashBackSettings;->addAppSupportStatus(Ljava/lang/String;)V

    const-string v0, "com.sankuai.meituan.takeoutnew"

    invoke-direct {p0, v0}, Lcom/android/settings/freeform/FlashBackSettings;->addAppSupportStatus(Ljava/lang/String;)V

    const-string v0, "com.baidu.BaiduMap"

    invoke-direct {p0, v0}, Lcom/android/settings/freeform/FlashBackSettings;->addAppSupportStatus(Ljava/lang/String;)V

    const-string v0, "cn.ishansong"

    invoke-direct {p0, v0}, Lcom/android/settings/freeform/FlashBackSettings;->addAppSupportStatus(Ljava/lang/String;)V

    const-string v0, "com.sdu.didi.psnger"

    invoke-direct {p0, v0}, Lcom/android/settings/freeform/FlashBackSettings;->addAppSupportStatus(Ljava/lang/String;)V

    return-void
.end method

.method private initCurrentFlashBackApp()V
    .locals 8

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "FlashBack_Current_App"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-void

    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v2, "packageName"

    const-string v3, "No_Current_Running_FlashBack_App"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "FlashBack_Current_App"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private initSettingsData()V
    .locals 5

    const-string v0, "FlashBackBackgroundStartSwitch"

    const-string v1, "FlashBackMainSwitch"

    :try_start_0
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, -0x1

    invoke-static {v2, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/android/settings/freeform/FlashBackSettings;->mFlashBackMainSwitchState:I

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    iput v2, p0, Lcom/android/settings/freeform/FlashBackSettings;->mFlashBackMainSwitchState:I

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_0
    sget-boolean v1, Lcom/android/settings/freeform/FlashBackSettings;->sFlashBacSettingDebug:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v2, "FBSettings"

    if-eqz v1, :cond_1

    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "FlashBackMainSwitch Current Result: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/android/settings/freeform/FlashBackSettings;->mFlashBackMainSwitchState:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v0, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/settings/freeform/FlashBackSettings;->mFlashBackBackgroundStartSwitchState:I

    if-ne v1, v3, :cond_2

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/settings/freeform/FlashBackSettings;->mFlashBackBackgroundStartSwitchState:I

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v0, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_2
    sget-boolean v0, Lcom/android/settings/freeform/FlashBackSettings;->sFlashBacSettingDebug:Z

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mFlashBackBackgroundStartSwitchValue Current Result: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mFlashBackBackgroundStartSwitchState:I

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v2, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_3
    :goto_0
    return-void
.end method

.method private initSettingsView()V
    .locals 4

    const-string v0, "flashback_main_switch_preference"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mMainSwitchPreference:Landroidx/preference/CheckBoxPreference;

    const-string v0, "flashback_background_start_switch_preference"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mBackgroundStartSwitchPreference:Landroidx/preference/CheckBoxPreference;

    const-string v0, "flashback_support_background"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mPreferenceCategoryForBackgroundSupport:Landroidx/preference/PreferenceCategory;

    const-string v0, "flashback_support_apps"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mPreferenceCategoryForAppSupport:Landroidx/preference/PreferenceCategory;

    iget-object v0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mMainSwitchPreference:Landroidx/preference/CheckBoxPreference;

    iget v1, p0, Lcom/android/settings/freeform/FlashBackSettings;->mFlashBackMainSwitchState:I

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    move v1, v3

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mBackgroundStartSwitchPreference:Landroidx/preference/CheckBoxPreference;

    iget v1, p0, Lcom/android/settings/freeform/FlashBackSettings;->mFlashBackBackgroundStartSwitchState:I

    if-ne v1, v3, :cond_1

    move v2, v3

    :cond_1
    invoke-virtual {v0, v2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mMainSwitchPreference:Landroidx/preference/CheckBoxPreference;

    new-instance v1, Lcom/android/settings/freeform/FlashBackSettings$1;

    invoke-direct {v1, p0}, Lcom/android/settings/freeform/FlashBackSettings$1;-><init>(Lcom/android/settings/freeform/FlashBackSettings;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mBackgroundStartSwitchPreference:Landroidx/preference/CheckBoxPreference;

    new-instance v1, Lcom/android/settings/freeform/FlashBackSettings$2;

    invoke-direct {v1, p0}, Lcom/android/settings/freeform/FlashBackSettings$2;-><init>(Lcom/android/settings/freeform/FlashBackSettings;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method

.method private initSupportAppsView(Ljava/util/HashMap;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/android/settings/freeform/FlashBackSettings;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "FlashBack_Support_Apps"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_0
    const-string/jumbo v2, "packageName"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v2, "com.sankuai.meituan.takeoutnew"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_2

    :cond_1
    const-string/jumbo v2, "switch"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :try_start_1
    iget-object v2, p0, Lcom/android/settings/freeform/FlashBackSettings;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v3, 0x80

    invoke-virtual {v2, v8, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v4, v2

    goto :goto_1

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    move-object v4, v0

    :goto_1
    if-eqz v4, :cond_2

    iget-object v2, p0, Lcom/android/settings/freeform/FlashBackSettings;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v4}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iget-object v2, p0, Lcom/android/settings/freeform/FlashBackSettings;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v4}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/android/settings/freeform/FlashBackSettings;->buildCheckBoxPreference(Landroid/content/pm/ApplicationInfo;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ILjava/lang/String;)V

    :cond_2
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    if-eqz v1, :cond_4

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    return-void
.end method

.method private mainSwitchPreferenceOnChange(Ljava/lang/Object;)V
    .locals 5

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    move v1, v3

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    const-string v4, "FlashBackMainSwitch"

    invoke-static {v0, v4, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-ne p1, v3, :cond_1

    move v2, v3

    :cond_1
    iput v2, p0, Lcom/android/settings/freeform/FlashBackSettings;->mFlashBackMainSwitchState:I

    sget-boolean p1, Lcom/android/settings/freeform/FlashBackSettings;->sFlashBacSettingDebug:Z

    if-eqz p1, :cond_2

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "mFlashBackMainSwitch New Result: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mFlashBackMainSwitchState:I

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "FBSettings"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget p1, p0, Lcom/android/settings/freeform/FlashBackSettings;->mFlashBackMainSwitchState:I

    if-nez p1, :cond_3

    invoke-virtual {p0}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->getPrefContext()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mKillFlashBackServiceIntent:Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    :cond_3
    invoke-virtual {p0}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->getPrefContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    invoke-static {v4}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    return-void
.end method

.method private performClick(Landroidx/preference/Preference;Ljava/lang/Boolean;)V
    .locals 24

    move-object/from16 v0, p0

    const-string/jumbo v1, "switch"

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroidx/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, v0, Lcom/android/settings/freeform/FlashBackSettings;->mPackageNameMap:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, v0, Lcom/android/settings/freeform/FlashBackSettings;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "FlashBack_Support_Apps"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "packageName=?"

    const/4 v13, 0x1

    new-array v8, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    aput-object v3, v8, v14

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v4 .. v12}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-ne v5, v13, :cond_0

    move v5, v13

    goto :goto_0

    :cond_0
    move v5, v14

    :goto_0
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v1, v0, Lcom/android/settings/freeform/FlashBackSettings;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "FlashBack_Support_Apps"

    const-string/jumbo v6, "packageName=?"

    new-array v7, v13, [Ljava/lang/String;

    aput-object v3, v7, v14

    invoke-virtual {v1, v5, v4, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v15, v0, Lcom/android/settings/freeform/FlashBackSettings;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v16, "FlashBack_Current_App"

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    invoke-virtual/range {v15 .. v23}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    const-string/jumbo v1, "packageName"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v3, "miui.intent.action_kill_flashback_leash"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->getPrefContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v2, :cond_2

    :cond_1
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_2
    return-void

    :goto_2
    if-eqz v2, :cond_3

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private readMetaData(Ljava/lang/String;)Z
    .locals 1

    :try_start_0
    iget-object p0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v0, 0x80

    invoke-virtual {p0, p1, v0}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object p0

    iget-object p0, p0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string/jumbo p1, "miui_flashback_support"

    invoke-virtual {p0, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private removeCheckBoxPreference()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/settings/freeform/FlashBackSettings;->mAppsCheckBoxPreferenceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/freeform/FlashBackSettings;->mAppsCheckBoxPreferenceList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroidx/preference/Preference;

    invoke-virtual {v1, v2}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object p0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mAppsCheckBoxPreferenceList:Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method private setCheckBoxPreferenceSummary(Landroidx/preference/CheckBoxPreference;Ljava/lang/String;)Landroidx/preference/CheckBoxPreference;
    .locals 1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result p0

    const/4 v0, -0x1

    sparse-switch p0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string p0, "cn.ishansong"

    invoke-virtual {p2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x5

    goto :goto_0

    :sswitch_1
    const-string p0, "com.baidu.BaiduMap"

    invoke-virtual {p2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    goto :goto_0

    :sswitch_2
    const-string p0, "com.sdu.didi.psnger"

    invoke-virtual {p2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x3

    goto :goto_0

    :sswitch_3
    const-string p0, "com.sankuai.meituan.takeoutnew"

    invoke-virtual {p2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_3

    goto :goto_0

    :cond_3
    const/4 v0, 0x2

    goto :goto_0

    :sswitch_4
    const-string p0, "com.sankuai.meituan"

    invoke-virtual {p2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_4

    goto :goto_0

    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    :sswitch_5
    const-string p0, "com.tencent.tmgp.sgame"

    invoke-virtual {p2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_5

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    :goto_0
    packed-switch v0, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    sget p0, Lcom/android/settings/R$string;->flashback_shansong_summary:I

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setSummary(I)V

    goto :goto_1

    :pswitch_1
    sget p0, Lcom/android/settings/R$string;->flashback_baidu_map_summary:I

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setSummary(I)V

    goto :goto_1

    :pswitch_2
    sget p0, Lcom/android/settings/R$string;->flashback_didi_summary:I

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setSummary(I)V

    goto :goto_1

    :pswitch_3
    sget p0, Lcom/android/settings/R$string;->flashback_meituan_waimai_summary:I

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setSummary(I)V

    goto :goto_1

    :pswitch_4
    sget p0, Lcom/android/settings/R$string;->flashback_meituan_summary:I

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setSummary(I)V

    goto :goto_1

    :pswitch_5
    sget p0, Lcom/android/settings/R$string;->flashback_wangzhe_summary:I

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setSummary(I)V

    :goto_1
    return-object p1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6fa46511 -> :sswitch_5
        -0x65eabdaa -> :sswitch_4
        -0x3893528f -> :sswitch_3
        -0x36bf0dca -> :sswitch_2
        0x2c649fe1 -> :sswitch_1
        0x7cb4fafd -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    sget p1, Lcom/android/settings/R$xml;->flashback_settings:I

    invoke-virtual {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->addPreferencesFromResource(I)V

    new-instance p1, Lcom/android/settings/freeform/FlashBackDataHelper;

    invoke-virtual {p0}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->getPrefContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "FlashBackSupportApps.db"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p1, v0, v1, v2, v3}, Lcom/android/settings/freeform/FlashBackDataHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    iput-object p1, p0, Lcom/android/settings/freeform/FlashBackSettings;->mDBHelper:Lcom/android/settings/freeform/FlashBackDataHelper;

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/freeform/FlashBackSettings;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/freeform/FlashBackSettings;->mPackageManager:Landroid/content/pm/PackageManager;

    new-instance p1, Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p1, p0, v0}, Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;-><init>(Lcom/android/settings/freeform/FlashBackSettings;Landroid/os/Handler;)V

    iput-object p1, p0, Lcom/android/settings/freeform/FlashBackSettings;->mFlashBackMainSwitchObserver:Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;

    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    iput-object p1, p0, Lcom/android/settings/freeform/FlashBackSettings;->mKillFlashBackServiceIntent:Landroid/content/Intent;

    const-string v0, "com.miui.freeform"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object p1, p0, Lcom/android/settings/freeform/FlashBackSettings;->mKillFlashBackServiceIntent:Landroid/content/Intent;

    const-string/jumbo v1, "miui.intent.action.FLASHBACK_WINDOW"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object p1, p0, Lcom/android/settings/freeform/FlashBackSettings;->mKillFlashBackServiceIntent:Landroid/content/Intent;

    const-string v1, "com.miui.flashback.MiuiFlashbackWindowService"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/android/settings/freeform/FlashBackSettings;->initSettingsData()V

    invoke-direct {p0}, Lcom/android/settings/freeform/FlashBackSettings;->initSettingsView()V

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onPause()V

    invoke-direct {p0}, Lcom/android/settings/freeform/FlashBackSettings;->removeCheckBoxPreference()V

    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 0

    const/4 p0, 0x1

    return p0
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mFlashBackMainSwitchObserver:Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;

    invoke-virtual {v0}, Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;->observe()V

    invoke-direct {p0}, Lcom/android/settings/freeform/FlashBackSettings;->initAppsData()V

    invoke-direct {p0}, Lcom/android/settings/freeform/FlashBackSettings;->initCurrentFlashBackApp()V

    iget-object v0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mPackageNameMap:Ljava/util/HashMap;

    invoke-direct {p0, v0}, Lcom/android/settings/freeform/FlashBackSettings;->initSupportAppsView(Ljava/util/HashMap;)V

    invoke-direct {p0}, Lcom/android/settings/freeform/FlashBackSettings;->checkVisibility()V

    return-void
.end method

.method public onStop()V
    .locals 0

    invoke-super {p0}, Lcom/android/settingslib/core/lifecycle/ObservablePreferenceFragment;->onStop()V

    iget-object p0, p0, Lcom/android/settings/freeform/FlashBackSettings;->mFlashBackMainSwitchObserver:Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;

    invoke-virtual {p0}, Lcom/android/settings/freeform/FlashBackSettings$FlashBackMainSwitchObserver;->unObserve()V

    return-void
.end method
