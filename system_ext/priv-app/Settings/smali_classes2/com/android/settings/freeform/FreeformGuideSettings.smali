.class public Lcom/android/settings/freeform/FreeformGuideSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;


# static fields
.field private static mCvw:Z

.field private static mPin:Z


# instance fields
.field public final ATLEAST_MIUI_13:Z

.field public final IS_MIUI_13:Z

.field public final MIUI_V13_VERSION_CODE:I

.field public final MIUI_VERSION_CODE:I

.field private final PUSH_WORLD_CIRCULATE_NAME:Landroid/content/ComponentName;

.field public final WORLD_CIRCULATE_PACKAGE_NAME:Ljava/lang/String;

.field private final WORLD_CIRCULATE_URI:Landroid/net/Uri;

.field private mBubblesNotification:Lmiuix/preference/TextPreference;

.field private mContext:Landroid/content/Context;

.field private mDockAssistant:Landroidx/preference/Preference;

.field private mDropDown:Landroidx/preference/Preference;

.field private mFlashBackTextPreference:Lmiuix/preference/TextPreference;

.field private mFreeFormDockAssistant:Landroidx/preference/PreferenceCategory;

.field private mFreeformGuideToSidehide:Landroidx/preference/Preference;

.field private mFullToFreeform:Landroidx/preference/Preference;

.field private mHang:Landroidx/preference/Preference;

.field private mMove:Landroidx/preference/Preference;

.field private mMultiWindow:Landroidx/preference/Preference;

.field private mNotification:Landroidx/preference/Preference;

.field public sIsSupportPushAppEnterWorldCirculate:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/settings/freeform/FreeformGuideSettings;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    const-string v0, "content://com.milink.service.circulate"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->WORLD_CIRCULATE_URI:Landroid/net/Uri;

    const-string v0, "com.milink.service"

    iput-object v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->WORLD_CIRCULATE_PACKAGE_NAME:Ljava/lang/String;

    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.miui.circulate.world.AppCirculateActivity"

    invoke-direct {v1, v0, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->PUSH_WORLD_CIRCULATE_NAME:Landroid/content/ComponentName;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->sIsSupportPushAppEnterWorldCirculate:Z

    const/16 v1, 0xd

    iput v1, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->MIUI_V13_VERSION_CODE:I

    invoke-virtual {p0}, Lcom/android/settings/freeform/FreeformGuideSettings;->getMiuiVersionCode()I

    move-result v2

    iput v2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->MIUI_VERSION_CODE:I

    const/4 v3, 0x1

    if-ne v2, v1, :cond_0

    move v4, v3

    goto :goto_0

    :cond_0
    move v4, v0

    :goto_0
    iput-boolean v4, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->IS_MIUI_13:Z

    if-lt v2, v1, :cond_1

    move v0, v3

    :cond_1
    iput-boolean v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->ATLEAST_MIUI_13:Z

    return-void
.end method

.method private canTaskPushEnterWorldCirculate()Z
    .locals 5

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->WORLD_CIRCULATE_URI:Landroid/net/Uri;

    const-string v2, "check_permission"

    const-string/jumbo v3, "recentlist_app"

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p0

    :catch_0
    move-exception v0

    const-string v1, "FreeformGuideSettings"

    const-string v2, "canTaskPushEnterWorldCirculate"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->PUSH_WORLD_CIRCULATE_NAME:Landroid/content/ComponentName;

    invoke-direct {p0, v0}, Lcom/android/settings/freeform/FreeformGuideSettings;->getWorldCirculateMeta(Landroid/content/ComponentName;)Landroid/os/Bundle;

    move-result-object p0

    if-eqz p0, :cond_0

    const-string v0, "appcirculate_support_recentlist"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p0

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static getCvw()Z
    .locals 1

    sget-boolean v0, Lcom/android/settings/freeform/FreeformGuideSettings;->mCvw:Z

    return v0
.end method

.method private static getGestureLineHeight(Landroid/content/Context;)I
    .locals 1

    invoke-static {p0}, Lcom/android/settings/freeform/FreeformGuideSettings;->isShowNavigationHandle(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const v0, 0x10501e1

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    return p0
.end method

.method public static getPin()Z
    .locals 1

    sget-boolean v0, Lcom/android/settings/freeform/FreeformGuideSettings;->mPin:Z

    return v0
.end method

.method private getWorldCirculateMeta(Landroid/content/ComponentName;)Landroid/os/Bundle;
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    iget-object p0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    const v1, 0xc0080

    invoke-virtual {p0, p1, v1}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ComponentName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "FreeformGuideSettingsgetWorldCirculateMeta"

    invoke-static {v1, p1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object p0, v0

    :goto_0
    if-eqz p0, :cond_0

    iget-object p0, p0, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    return-object p0

    :cond_0
    return-object v0
.end method

.method private initBubblesNotification()Z
    .locals 1

    const-string/jumbo v0, "miui_bubbles_notification"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiuix/preference/TextPreference;

    iput-object v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mBubblesNotification:Lmiuix/preference/TextPreference;

    iget-object v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/utils/SettingsFeatures;->isSupportBubblesNotification(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mBubblesNotification:Lmiuix/preference/TextPreference;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    return v0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/freeform/FreeformGuideSettings;->refreshBubbleNotificationStatus()V

    const/4 p0, 0x1

    return p0
.end method

.method private initDockAssistant()Z
    .locals 1

    const-string v0, "dock_assistant"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mDockAssistant:Landroidx/preference/Preference;

    iget-object v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/utils/SettingsFeatures;->isSupportDock(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mDockAssistant:Landroidx/preference/Preference;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    return v0

    :cond_0
    const/4 p0, 0x1

    return p0
.end method

.method private initDropDown()V
    .locals 2

    const-string v0, "key_freeform_guide_drop_down_to_fullscreen"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mDropDown:Landroidx/preference/Preference;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/settings/freeform/FreeformGuideSettings$5;

    invoke-direct {v1, p0}, Lcom/android/settings/freeform/FreeformGuideSettings$5;-><init>(Lcom/android/settings/freeform/FreeformGuideSettings;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    :cond_0
    return-void
.end method

.method private initFlashBack()Z
    .locals 2

    const-string v0, "flashback_entrance_preference"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiuix/preference/TextPreference;

    iput-object v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mFlashBackTextPreference:Lmiuix/preference/TextPreference;

    invoke-static {}, Lcom/android/settings/lab/MiuiFlashbackController;->isNotSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mFlashBackTextPreference:Lmiuix/preference/TextPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setVisible(Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mFlashBackTextPreference:Lmiuix/preference/TextPreference;

    return v1

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/freeform/FreeformGuideSettings;->refreshFlashBackStatus()V

    const/4 p0, 0x1

    return p0
.end method

.method private initFreeformDockCategory()V
    .locals 3

    const-string v0, "freeform_dock_category"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mFreeFormDockAssistant:Landroidx/preference/PreferenceCategory;

    invoke-direct {p0}, Lcom/android/settings/freeform/FreeformGuideSettings;->initDockAssistant()Z

    move-result v0

    invoke-direct {p0}, Lcom/android/settings/freeform/FreeformGuideSettings;->initBubblesNotification()Z

    move-result v1

    invoke-direct {p0}, Lcom/android/settings/freeform/FreeformGuideSettings;->initFlashBack()Z

    move-result v2

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    if-nez v2, :cond_0

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object p0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mFreeFormDockAssistant:Landroidx/preference/PreferenceCategory;

    invoke-virtual {v0, p0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_0
    return-void
.end method

.method private initFreeformSideHide()V
    .locals 2

    const-string v0, "key_freeform_guide_to_sidehide"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mFreeformGuideToSidehide:Landroidx/preference/Preference;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/settings/freeform/FreeformGuideSettings$4;

    invoke-direct {v1, p0}, Lcom/android/settings/freeform/FreeformGuideSettings$4;-><init>(Lcom/android/settings/freeform/FreeformGuideSettings;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    :cond_0
    return-void
.end method

.method private initFullToFreeform()V
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    const-string v1, "key_freeform_guide_slide_to_small_freeform"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mFullToFreeform:Landroidx/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/freeform/FreeformGuideSettings;->getSupportPushAppEnterWorldCirculate()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mFullToFreeform:Landroidx/preference/Preference;

    sget v1, Lcom/android/settings/R$string;->freeform_guide_slide_to_small_freeform_summary_squarehot:I

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setSummary(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mFullToFreeform:Landroidx/preference/Preference;

    if-eqz v0, :cond_1

    new-instance v1, Lcom/android/settings/freeform/FreeformGuideSettings$7;

    invoke-direct {v1, p0}, Lcom/android/settings/freeform/FreeformGuideSettings$7;-><init>(Lcom/android/settings/freeform/FreeformGuideSettings;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    :cond_1
    return-void
.end method

.method private initHang()V
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    const-string v1, "key_freeform_guide_move_to_small_freeform_window"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mHang:Landroidx/preference/Preference;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/settings/freeform/FreeformGuideSettings$6;

    invoke-direct {v1, p0}, Lcom/android/settings/freeform/FreeformGuideSettings$6;-><init>(Lcom/android/settings/freeform/FreeformGuideSettings;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    :cond_0
    return-void
.end method

.method private initMove()V
    .locals 2

    const-string v0, "key_freeform_guide_move"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mMove:Landroidx/preference/Preference;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/settings/freeform/FreeformGuideSettings$3;

    invoke-direct {v1, p0}, Lcom/android/settings/freeform/FreeformGuideSettings$3;-><init>(Lcom/android/settings/freeform/FreeformGuideSettings;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    :cond_0
    return-void
.end method

.method private initMultiWindow()V
    .locals 2

    const-string v0, "key_multi_window_cvw"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mMultiWindow:Landroidx/preference/Preference;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/settings/freeform/FreeformGuideSettings$1;

    invoke-direct {v1, p0}, Lcom/android/settings/freeform/FreeformGuideSettings$1;-><init>(Lcom/android/settings/freeform/FreeformGuideSettings;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    :cond_0
    return-void
.end method

.method private initNotification()V
    .locals 2

    const-string v0, "key_freeform_guide_notification_drop_down"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mNotification:Landroidx/preference/Preference;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/settings/freeform/FreeformGuideSettings$2;

    invoke-direct {v1, p0}, Lcom/android/settings/freeform/FreeformGuideSettings$2;-><init>(Lcom/android/settings/freeform/FreeformGuideSettings;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    :cond_0
    return-void
.end method

.method private static isHideGestureLine(Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "hide_gesture_line"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    if-eqz p0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public static isShowNavigationHandle(Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "force_fsg_nav_bar"

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$Global;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/android/settings/freeform/FreeformGuideSettings;->isHideGestureLine(Landroid/content/Context;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private refreshBubbleNotificationStatus()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mBubblesNotification:Lmiuix/preference/TextPreference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/utils/SettingsFeatures;->isBubbleNotificationOpen(Landroid/content/Context;)Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mBubblesNotification:Lmiuix/preference/TextPreference;

    if-eqz v0, :cond_0

    sget v0, Lcom/android/settings/R$string;->miui_bubbles_notification_status_opened:I

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    const-string p0, ""

    :goto_0
    invoke-virtual {v1, p0}, Lmiuix/preference/TextPreference;->setText(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private refreshFlashBackStatus()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mFlashBackTextPreference:Lmiuix/preference/TextPreference;

    if-eqz v0, :cond_2

    :try_start_0
    invoke-virtual {v0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "FlashBackMainSwitch"

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v2, :cond_0

    move v0, v1

    :cond_0
    iget-object p0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mFlashBackTextPreference:Lmiuix/preference/TextPreference;

    if-ne v0, v1, :cond_1

    sget v0, Lcom/android/settings/R$string;->flashback_status_open:I

    goto :goto_0

    :cond_1
    sget v0, Lcom/android/settings/R$string;->flashback_status_close:I

    :goto_0
    invoke-virtual {p0, v0}, Lmiuix/preference/TextPreference;->setText(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_2
    :goto_1
    return-void
.end method

.method private static setCvw(Z)V
    .locals 0

    sput-boolean p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mCvw:Z

    return-void
.end method

.method private static setPin(Z)V
    .locals 0

    sput-boolean p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mPin:Z

    return-void
.end method


# virtual methods
.method public checkAndUpdateWorldCirculateView(Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Lmiui/os/UserHandle;->myUserId()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/freeform/FreeformGuideSettings;->getUserSystemId()I

    move-result v1

    if-eq v0, v1, :cond_0

    const-string p0, "FreeformGuideSettingscheckAndUpdateWorldCirculateView"

    const-string/jumbo p1, "no in main space"

    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->ATLEAST_MIUI_13:Z

    if-eqz v0, :cond_1

    const-string v0, "com.milink.service"

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/android/settings/freeform/FreeformGuideSettings;->canTaskPushEnterWorldCirculate()Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->sIsSupportPushAppEnterWorldCirculate:Z

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v0, "sIsSupportPushAppEnterWorldCirculate = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean p0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->sIsSupportPushAppEnterWorldCirculate:Z

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "FreeformGuideSettings"

    invoke-static {p1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method public getMiuiVersionCode()I
    .locals 2

    const-string/jumbo v0, "ro.miui.ui.version.code"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/freeform/FreeformGuideSettings;->getStringFromSystemProperites(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public getStringFromSystemProperites(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-static {p1, p2}, Lmiuix/core/util/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public getSupportPushAppEnterWorldCirculate()Z
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "sIsSupportPushAppEnterWorldCirculate = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->sIsSupportPushAppEnterWorldCirculate:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FreeformGuideSettings"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean p0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->sIsSupportPushAppEnterWorldCirculate:Z

    return p0
.end method

.method public getUserSystemId()I
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/freeform/FreeformGuideSettings;->refreshBubbleNotificationStatus()V

    invoke-direct {p0}, Lcom/android/settings/freeform/FreeformGuideSettings;->refreshFlashBackStatus()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/android/settings/SettingsPreferenceFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$xml;->freeform_settings:I

    invoke-virtual {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/android/settings/R$dimen;->freeform_settings_preference_bottom_padding_inner:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p1

    float-to-int p1, p1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lcom/android/settings/freeform/FreeformGuideSettings;->getGestureLineHeight(Landroid/content/Context;)I

    move-result p2

    add-int/2addr p1, p2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getListView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p2

    const/4 v0, 0x0

    invoke-virtual {p2, v0, v0, v0, p1}, Landroid/view/ViewGroup;->setPadding(IIII)V

    const-string/jumbo p1, "setting_Special_features_freeform"

    invoke-static {p1}, Lcom/android/settings/report/InternationalCompat;->trackReportEvent(Ljava/lang/String;)V

    const-string p1, "com.milink.service"

    invoke-virtual {p0, p1}, Lcom/android/settings/freeform/FreeformGuideSettings;->checkAndUpdateWorldCirculateView(Ljava/lang/String;)V

    const/4 p1, 0x1

    invoke-static {p1}, Lcom/android/settings/freeform/FreeformGuideSettings;->setCvw(Z)V

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->isSupportPin()Z

    move-result p2

    invoke-static {p2}, Lcom/android/settings/freeform/FreeformGuideSettings;->setPin(Z)V

    invoke-direct {p0}, Lcom/android/settings/freeform/FreeformGuideSettings;->initFreeformDockCategory()V

    invoke-direct {p0}, Lcom/android/settings/freeform/FreeformGuideSettings;->initMultiWindow()V

    invoke-direct {p0}, Lcom/android/settings/freeform/FreeformGuideSettings;->initNotification()V

    invoke-direct {p0}, Lcom/android/settings/freeform/FreeformGuideSettings;->initMove()V

    invoke-direct {p0}, Lcom/android/settings/freeform/FreeformGuideSettings;->initFreeformSideHide()V

    invoke-direct {p0}, Lcom/android/settings/freeform/FreeformGuideSettings;->initDropDown()V

    invoke-direct {p0}, Lcom/android/settings/freeform/FreeformGuideSettings;->initHang()V

    invoke-direct {p0}, Lcom/android/settings/freeform/FreeformGuideSettings;->initFullToFreeform()V

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->isFoldDevice()Z

    move-result p2

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mMultiWindow:Landroidx/preference/Preference;

    invoke-virtual {p2, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mNotification:Landroidx/preference/Preference;

    invoke-virtual {p2, p1}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mMove:Landroidx/preference/Preference;

    sget-boolean v0, Lcom/android/settings/freeform/FreeformGuideSettings;->mPin:Z

    xor-int/2addr v0, p1

    invoke-virtual {p2, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mFreeformGuideToSidehide:Landroidx/preference/Preference;

    sget-boolean v0, Lcom/android/settings/freeform/FreeformGuideSettings;->mPin:Z

    invoke-virtual {p2, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mDropDown:Landroidx/preference/Preference;

    invoke-virtual {p2, p1}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mHang:Landroidx/preference/Preference;

    invoke-virtual {p2, p1}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mFullToFreeform:Landroidx/preference/Preference;

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setVisible(Z)V

    goto/16 :goto_0

    :cond_0
    sget-boolean p2, Lmiui/os/Build;->IS_TABLET:Z

    const/16 v1, 0x64

    if-eqz p2, :cond_1

    sget-boolean p2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez p2, :cond_1

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mMultiWindow:Landroidx/preference/Preference;

    sget-boolean v2, Lcom/android/settings/freeform/FreeformGuideSettings;->mCvw:Z

    invoke-virtual {p2, v2}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mNotification:Landroidx/preference/Preference;

    invoke-virtual {p2, p1}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mMove:Landroidx/preference/Preference;

    invoke-virtual {p2, p1}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mFreeformGuideToSidehide:Landroidx/preference/Preference;

    sget-boolean v2, Lcom/android/settings/freeform/FreeformGuideSettings;->mPin:Z

    invoke-virtual {p2, v2}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mFreeformGuideToSidehide:Landroidx/preference/Preference;

    invoke-virtual {p2, v1}, Landroidx/preference/Preference;->setOrder(I)V

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mDropDown:Landroidx/preference/Preference;

    invoke-virtual {p2, p1}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mHang:Landroidx/preference/Preference;

    invoke-virtual {p2, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mFullToFreeform:Landroidx/preference/Preference;

    sget-boolean v0, Lcom/android/settings/freeform/FreeformGuideSettings;->mPin:Z

    xor-int/2addr p1, v0

    invoke-virtual {p2, p1}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mFullToFreeform:Landroidx/preference/Preference;

    sget p1, Lcom/android/settings/R$string;->freeform_guide_slide_to_small_freeform_summary_pad:I

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setSummary(I)V

    goto :goto_0

    :cond_1
    sget-boolean p2, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz p2, :cond_2

    sget-boolean p2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz p2, :cond_2

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mMultiWindow:Landroidx/preference/Preference;

    sget-boolean v2, Lcom/android/settings/freeform/FreeformGuideSettings;->mCvw:Z

    invoke-virtual {p2, v2}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mMultiWindow:Landroidx/preference/Preference;

    sget v2, Lcom/android/settings/R$string;->multi_window_cvw_summary_global:I

    invoke-virtual {p2, v2}, Landroidx/preference/Preference;->setSummary(I)V

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mNotification:Landroidx/preference/Preference;

    invoke-virtual {p2, p1}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mMove:Landroidx/preference/Preference;

    invoke-virtual {p2, p1}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mFreeformGuideToSidehide:Landroidx/preference/Preference;

    sget-boolean v2, Lcom/android/settings/freeform/FreeformGuideSettings;->mPin:Z

    invoke-virtual {p2, v2}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mFreeformGuideToSidehide:Landroidx/preference/Preference;

    invoke-virtual {p2, v1}, Landroidx/preference/Preference;->setOrder(I)V

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mDropDown:Landroidx/preference/Preference;

    invoke-virtual {p2, p1}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p1, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mHang:Landroidx/preference/Preference;

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mFullToFreeform:Landroidx/preference/Preference;

    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    goto :goto_0

    :cond_2
    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mMultiWindow:Landroidx/preference/Preference;

    invoke-virtual {p2, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mNotification:Landroidx/preference/Preference;

    invoke-virtual {p2, p1}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mMove:Landroidx/preference/Preference;

    sget-boolean v0, Lcom/android/settings/freeform/FreeformGuideSettings;->mPin:Z

    xor-int/2addr v0, p1

    invoke-virtual {p2, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mFreeformGuideToSidehide:Landroidx/preference/Preference;

    sget-boolean v0, Lcom/android/settings/freeform/FreeformGuideSettings;->mPin:Z

    invoke-virtual {p2, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mDropDown:Landroidx/preference/Preference;

    invoke-virtual {p2, p1}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p2, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mHang:Landroidx/preference/Preference;

    invoke-virtual {p2, p1}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p0, p0, Lcom/android/settings/freeform/FreeformGuideSettings;->mFullToFreeform:Landroidx/preference/Preference;

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setVisible(Z)V

    :goto_0
    return-void
.end method
