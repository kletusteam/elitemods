.class Lcom/android/settings/CommonDialog$DialogClickListener;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/CommonDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DialogClickListener"
.end annotation


# instance fields
.field private mCommonDialog:Lcom/android/settings/CommonDialog;

.field private mDialogRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/android/settings/CommonDialog;",
            ">;"
        }
    .end annotation
.end field

.field private mWeakRefEnabled:Z


# direct methods
.method private constructor <init>(Lcom/android/settings/CommonDialog;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p2, :cond_0

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/CommonDialog$DialogClickListener;->mDialogRef:Ljava/lang/ref/WeakReference;

    goto :goto_0

    :cond_0
    iput-object p1, p0, Lcom/android/settings/CommonDialog$DialogClickListener;->mCommonDialog:Lcom/android/settings/CommonDialog;

    :goto_0
    iput-boolean p2, p0, Lcom/android/settings/CommonDialog$DialogClickListener;->mWeakRefEnabled:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/CommonDialog;ZLcom/android/settings/CommonDialog$DialogClickListener-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/CommonDialog$DialogClickListener;-><init>(Lcom/android/settings/CommonDialog;Z)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/CommonDialog$DialogClickListener;->mCommonDialog:Lcom/android/settings/CommonDialog;

    iget-boolean v1, p0, Lcom/android/settings/CommonDialog$DialogClickListener;->mWeakRefEnabled:Z

    if-eqz v1, :cond_0

    iget-object p0, p0, Lcom/android/settings/CommonDialog$DialogClickListener;->mDialogRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p0

    move-object v0, p0

    check-cast v0, Lcom/android/settings/CommonDialog;

    :cond_0
    if-eqz v0, :cond_1

    invoke-static {v0, p1, p2}, Lcom/android/settings/CommonDialog;->-$$Nest$monClick(Lcom/android/settings/CommonDialog;Landroid/content/DialogInterface;I)V

    :cond_1
    return-void
.end method
