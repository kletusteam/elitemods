.class public final Lcom/android/settings/R$plurals;
.super Ljava/lang/Object;


# static fields
.field public static final a_hour_ago:I = 0x7f100000

.field public static final abbrev_a_hour_ago:I = 0x7f100001

.field public static final abbrev_half_hour_ago:I = 0x7f100002

.field public static final abbrev_in_a_hour:I = 0x7f100003

.field public static final abbrev_in_half_hour:I = 0x7f100004

.field public static final abbrev_in_less_than_one_minute:I = 0x7f100005

.field public static final abbrev_in_num_minutes:I = 0x7f100006

.field public static final abbrev_less_than_one_minute_ago:I = 0x7f100007

.field public static final abbrev_num_minutes_ago:I = 0x7f100008

.field public static final abc_slice_duration_days:I = 0x7f100009

.field public static final abc_slice_duration_min:I = 0x7f10000a

.field public static final abc_slice_duration_years:I = 0x7f10000b

.field public static final accessibility_usage_summary:I = 0x7f10000c

.field public static final accessibilty_autoclick_delay_unit_second:I = 0x7f10000d

.field public static final accessibilty_autoclick_preference_subtitle_long_delay:I = 0x7f10000e

.field public static final accessibilty_autoclick_preference_subtitle_medium_delay:I = 0x7f10000f

.field public static final accessibilty_autoclick_preference_subtitle_short_delay:I = 0x7f100010

.field public static final app_launch_supported_links_title:I = 0x7f100011

.field public static final app_launch_verified_links_message:I = 0x7f100012

.field public static final app_launch_verified_links_title:I = 0x7f100013

.field public static final app_link_open_always_summary:I = 0x7f100014

.field public static final app_notification_listing_summary_others:I = 0x7f100015

.field public static final auto_disable_screenbuttons_disabled_header_title:I = 0x7f100016

.field public static final auto_disable_screenbuttons_enabled_header_title:I = 0x7f100017

.field public static final auto_startup_count:I = 0x7f100018

.field public static final autofill_passwords_count:I = 0x7f100019

.field public static final battery_manager_app_restricted:I = 0x7f10001a

.field public static final battery_tip_restrict_app_dialog_title:I = 0x7f10001b

.field public static final battery_tip_restrict_handled_summary:I = 0x7f10001c

.field public static final battery_tip_restrict_handled_title:I = 0x7f10001d

.field public static final battery_tip_restrict_summary:I = 0x7f10001e

.field public static final battery_tip_restrict_title:I = 0x7f10001f

.field public static final bg_network_allow_count:I = 0x7f100020

.field public static final bg_network_restrict_count:I = 0x7f100021

.field public static final billing_cycle_days_left:I = 0x7f100022

.field public static final bt_usable_device_title:I = 0x7f100023

.field public static final clean_time:I = 0x7f100024

.field public static final color_game_led_freq_entries:I = 0x7f100025

.field public static final connected_devices_number:I = 0x7f100026

.field public static final data_saver_unrestricted_summary:I = 0x7f100027

.field public static final default_camera_app_title:I = 0x7f100028

.field public static final default_email_app_title:I = 0x7f100029

.field public static final default_phone_app_title:I = 0x7f10002a

.field public static final deleted_channels:I = 0x7f10002b

.field public static final dlg_remove_locales_title:I = 0x7f10002c

.field public static final domain_urls_apps_summary_on:I = 0x7f10002d

.field public static final enterprise_privacy_number_ca_certs:I = 0x7f10002e

.field public static final enterprise_privacy_number_failed_password_wipe:I = 0x7f10002f

.field public static final enterprise_privacy_number_packages:I = 0x7f100030

.field public static final enterprise_privacy_number_packages_lower_bound:I = 0x7f100031

.field public static final firewall_item_summary:I = 0x7f100032

.field public static final free_wifi_connected_count:I = 0x7f100033

.field public static final half_hour_ago:I = 0x7f100034

.field public static final hours:I = 0x7f100035

.field public static final in_a_hour:I = 0x7f100036

.field public static final in_half_hour:I = 0x7f100037

.field public static final in_less_than_one_minute:I = 0x7f100038

.field public static final in_num_minutes:I = 0x7f100039

.field public static final interact_across_profiles_number_of_connected_apps:I = 0x7f10003a

.field public static final less_than_one_minute_ago:I = 0x7f10003b

.field public static final location_app_permission_summary_location_on:I = 0x7f10003c

.field public static final location_settings_summary_location_on:I = 0x7f10003d

.field public static final lockpassword_password_requires_letters:I = 0x7f10003e

.field public static final lockpassword_password_requires_lowercase:I = 0x7f10003f

.field public static final lockpassword_password_requires_nonletter:I = 0x7f100040

.field public static final lockpassword_password_requires_nonnumerical:I = 0x7f100041

.field public static final lockpassword_password_requires_numeric:I = 0x7f100042

.field public static final lockpassword_password_requires_symbols:I = 0x7f100043

.field public static final lockpassword_password_requires_uppercase:I = 0x7f100044

.field public static final lockpassword_password_too_long:I = 0x7f100045

.field public static final lockpassword_password_too_short:I = 0x7f100046

.field public static final lockpassword_pin_fixed_length:I = 0x7f100047

.field public static final lockpassword_pin_too_long:I = 0x7f100048

.field public static final lockpassword_pin_too_short:I = 0x7f100049

.field public static final lockpattern_recording_incorrect_too_short:I = 0x7f10004a

.field public static final lockpattern_too_many_failed_confirmation_attempts:I = 0x7f10004b

.field public static final lockpattern_too_many_failed_confirmation_attempts_footer:I = 0x7f10004c

.field public static final manage_notification_access_summary_nonzero:I = 0x7f10004d

.field public static final manage_trust_agents_summary_on:I = 0x7f10004e

.field public static final max_number_of_processes:I = 0x7f10004f

.field public static final memory_usage_apps_summary:I = 0x7f100050

.field public static final minutes:I = 0x7f100051

.field public static final miui_sos_emergency_contacts_quantity:I = 0x7f100052

.field public static final miui_wifi_wakeup_summary:I = 0x7f100053

.field public static final miuix_appcompat_items_selected:I = 0x7f100054

.field public static final mobile_network_summary_count:I = 0x7f100055

.field public static final mtrl_badge_content_description:I = 0x7f100056

.field public static final network_restrictions_summary:I = 0x7f100057

.field public static final notification_group_summary:I = 0x7f100058

.field public static final notification_history_count:I = 0x7f100059

.field public static final notification_summary:I = 0x7f10005a

.field public static final notifications_categories_off:I = 0x7f10005b

.field public static final notifications_sent_daily:I = 0x7f10005c

.field public static final notifications_sent_weekly:I = 0x7f10005d

.field public static final num_minutes_ago:I = 0x7f10005e

.field public static final number_of_device_admins:I = 0x7f10005f

.field public static final number_of_urls:I = 0x7f100060

.field public static final permission_bar_chart_label:I = 0x7f100061

.field public static final permissions_summary:I = 0x7f100062

.field public static final power_high_usage_summary:I = 0x7f100063

.field public static final power_high_usage_title:I = 0x7f100064

.field public static final print_jobs_summary:I = 0x7f100065

.field public static final print_settings_summary:I = 0x7f100066

.field public static final priority_conversation_count:I = 0x7f100067

.field public static final restricted_app_summary:I = 0x7f100068

.field public static final runtime_permissions_additional_count:I = 0x7f100069

.field public static final runtime_permissions_summary:I = 0x7f10006a

.field public static final saved_network_checked_num:I = 0x7f10006b

.field public static final seconds:I = 0x7f10006c

.field public static final security_settings_fingerprint_preference_summary:I = 0x7f10006d

.field public static final see_all_apps_title:I = 0x7f10006e

.field public static final settings_suggestion_header_summary_hidden_items:I = 0x7f10006f

.field public static final show_cit_countdown:I = 0x7f100070

.field public static final show_dev_countdown:I = 0x7f100071

.field public static final show_number_hearingaid_count:I = 0x7f100072

.field public static final show_pho_countdown:I = 0x7f100073

.field public static final show_rep_countdown:I = 0x7f100074

.field public static final special_access_summary:I = 0x7f100075

.field public static final ssl_ca_cert_dialog_title:I = 0x7f100076

.field public static final ssl_ca_cert_settings_button:I = 0x7f100077

.field public static final status_bar_settings_disabled_header_title:I = 0x7f100078

.field public static final status_bar_settings_enabled_header_title:I = 0x7f100079

.field public static final string_after_int_minute:I = 0x7f10007a

.field public static final string_bits_per_sample:I = 0x7f10007b

.field public static final string_int:I = 0x7f10007c

.field public static final string_int_hour:I = 0x7f10007d

.field public static final string_int_minute:I = 0x7f10007e

.field public static final string_int_second:I = 0x7f10007f

.field public static final suggestions_collapsed_summary:I = 0x7f100080

.field public static final suggestions_collapsed_title:I = 0x7f100081

.field public static final switch_headset_anti_lost_summary:I = 0x7f100082

.field public static final unused_apps_summary:I = 0x7f100083

.field public static final uri_permissions_text:I = 0x7f100084

.field public static final usage_state_device_explain_today:I = 0x7f100085

.field public static final usage_state_device_explain_week:I = 0x7f100086

.field public static final usage_state_device_noti_explain_today:I = 0x7f100087

.field public static final usage_state_device_noti_explain_week:I = 0x7f100088

.field public static final usage_state_hour:I = 0x7f100089

.field public static final usage_state_hour24:I = 0x7f10008a

.field public static final usage_state_hour_text:I = 0x7f10008b

.field public static final usage_state_minute:I = 0x7f10008c

.field public static final usage_state_miunte_text:I = 0x7f10008d

.field public static final usage_state_noti_count:I = 0x7f10008e

.field public static final usage_state_unlock_count:I = 0x7f10008f

.field public static final usage_state_week_day_hour:I = 0x7f100090

.field public static final usage_state_week_day_minute:I = 0x7f100091

.field public static final usage_state_work_day_hour:I = 0x7f100092

.field public static final usage_state_work_day_minute:I = 0x7f100093

.field public static final usagestats_device_notification_des_hour:I = 0x7f100094

.field public static final usagestats_device_notification_des_min:I = 0x7f100095

.field public static final virus_scan_app_find_risk:I = 0x7f100096

.field public static final wifi_saved_access_points_summary:I = 0x7f100097

.field public static final wifi_saved_all_access_points_summary:I = 0x7f100098

.field public static final wifi_saved_passpoint_access_points_summary:I = 0x7f100099

.field public static final wifi_share_count:I = 0x7f10009a

.field public static final wifi_tether_connected_summary:I = 0x7f10009b

.field public static final wrong_pin_code:I = 0x7f10009c

.field public static final zen_mode_summary_alarms_only_by_hour:I = 0x7f10009d

.field public static final zen_mode_summary_alarms_only_by_minute:I = 0x7f10009e


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
