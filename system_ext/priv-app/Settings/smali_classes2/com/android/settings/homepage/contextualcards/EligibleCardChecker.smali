.class public Lcom/android/settings/homepage/contextualcards/EligibleCardChecker;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/android/settings/homepage/contextualcards/ContextualCard;",
        ">;"
    }
.end annotation


# instance fields
.field mCard:Lcom/android/settings/homepage/contextualcards/ContextualCard;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public static synthetic $r8$lambda$BwI-2KTeC8hobrQyjPtD0-GpKcY(Landroidx/slice/SliceViewManager;Landroid/net/Uri;Landroidx/slice/SliceViewManager$SliceCallback;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker;->lambda$bindSlice$1(Landroidx/slice/SliceViewManager;Landroid/net/Uri;Landroidx/slice/SliceViewManager$SliceCallback;)V

    return-void
.end method

.method public static synthetic $r8$lambda$TkgG58nQLDEUj_aUjhSgUmAW95g(Landroidx/slice/SliceViewManager;Landroid/net/Uri;Landroidx/slice/SliceViewManager$SliceCallback;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker;->lambda$bindSlice$2(Landroidx/slice/SliceViewManager;Landroid/net/Uri;Landroidx/slice/SliceViewManager$SliceCallback;)V

    return-void
.end method

.method public static synthetic $r8$lambda$st0-Tx_YxUNQcfTAn_WhjSwbCKQ(Landroidx/slice/Slice;)V
    .locals 0

    invoke-static {p0}, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker;->lambda$bindSlice$0(Landroidx/slice/Slice;)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/android/settings/homepage/contextualcards/ContextualCard;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker;->mCard:Lcom/android/settings/homepage/contextualcards/ContextualCard;

    return-void
.end method

.method private static synthetic lambda$bindSlice$0(Landroidx/slice/Slice;)V
    .locals 0

    return-void
.end method

.method private static synthetic lambda$bindSlice$1(Landroidx/slice/SliceViewManager;Landroid/net/Uri;Landroidx/slice/SliceViewManager$SliceCallback;)V
    .locals 0

    :try_start_0
    invoke-virtual {p0, p1, p2}, Landroidx/slice/SliceViewManager;->unregisterSliceCallback(Landroid/net/Uri;Landroidx/slice/SliceViewManager$SliceCallback;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "No permission currently: "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "EligibleCardChecker"

    invoke-static {p1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method private static synthetic lambda$bindSlice$2(Landroidx/slice/SliceViewManager;Landroid/net/Uri;Landroidx/slice/SliceViewManager$SliceCallback;)V
    .locals 1

    new-instance v0, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker$$ExternalSyntheticLambda2;-><init>(Landroidx/slice/SliceViewManager;Landroid/net/Uri;Landroidx/slice/SliceViewManager$SliceCallback;)V

    invoke-static {v0}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method bindSlice(Landroid/net/Uri;)Landroidx/slice/Slice;
    .locals 3

    goto/32 :goto_4

    nop

    :goto_0
    new-instance v2, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker$$ExternalSyntheticLambda1;

    goto/32 :goto_6

    nop

    :goto_1
    new-instance v0, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker$$ExternalSyntheticLambda0;

    goto/32 :goto_7

    nop

    :goto_2
    return-object v1

    :goto_3
    invoke-virtual {p0, p1}, Landroidx/slice/SliceViewManager;->bindSlice(Landroid/net/Uri;)Landroidx/slice/Slice;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_4
    iget-object p0, p0, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker;->mContext:Landroid/content/Context;

    goto/32 :goto_8

    nop

    :goto_5
    invoke-virtual {p0, p1, v0}, Landroidx/slice/SliceViewManager;->registerSliceCallback(Landroid/net/Uri;Landroidx/slice/SliceViewManager$SliceCallback;)V

    goto/32 :goto_3

    nop

    :goto_6
    invoke-direct {v2, p0, p1, v0}, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker$$ExternalSyntheticLambda1;-><init>(Landroidx/slice/SliceViewManager;Landroid/net/Uri;Landroidx/slice/SliceViewManager$SliceCallback;)V

    goto/32 :goto_9

    nop

    :goto_7
    invoke-direct {v0}, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker$$ExternalSyntheticLambda0;-><init>()V

    goto/32 :goto_5

    nop

    :goto_8
    invoke-static {p0}, Landroidx/slice/SliceViewManager;->getInstance(Landroid/content/Context;)Landroidx/slice/SliceViewManager;

    move-result-object p0

    goto/32 :goto_1

    nop

    :goto_9
    invoke-static {v2}, Lcom/android/settingslib/utils/ThreadUtils;->postOnMainThread(Ljava/lang/Runnable;)V

    goto/32 :goto_2

    nop
.end method

.method public call()Lcom/android/settings/homepage/contextualcards/ContextualCard;
    .locals 12

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/settings/overlay/FeatureFactory;->getFactory(Landroid/content/Context;)Lcom/android/settings/overlay/FeatureFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/settings/overlay/FeatureFactory;->getMetricsFeatureProvider()Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker;->mCard:Lcom/android/settings/homepage/contextualcards/ContextualCard;

    invoke-virtual {p0, v3}, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker;->isCardEligibleToDisplay(Lcom/android/settings/homepage/contextualcards/ContextualCard;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v4, 0x0

    const/16 v5, 0x696

    const/16 v6, 0x5de

    iget-object v3, p0, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker;->mCard:Lcom/android/settings/homepage/contextualcards/ContextualCard;

    invoke-virtual {v3}, Lcom/android/settings/homepage/contextualcards/ContextualCard;->getTextSliceUri()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    move-object v3, v2

    invoke-virtual/range {v3 .. v8}, Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;->action(IIILjava/lang/String;I)V

    iget-object v3, p0, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker;->mCard:Lcom/android/settings/homepage/contextualcards/ContextualCard;

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    const/16 v5, 0x696

    const/16 v6, 0x5de

    iget-object v3, p0, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker;->mCard:Lcom/android/settings/homepage/contextualcards/ContextualCard;

    invoke-virtual {v3}, Lcom/android/settings/homepage/contextualcards/ContextualCard;->getTextSliceUri()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    move-object v3, v2

    invoke-virtual/range {v3 .. v8}, Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;->action(IIILjava/lang/String;I)V

    const/4 v3, 0x0

    :goto_0
    move-object v9, v3

    const/4 v4, 0x0

    const/16 v5, 0x694

    const/16 v6, 0x5de

    iget-object p0, p0, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker;->mCard:Lcom/android/settings/homepage/contextualcards/ContextualCard;

    invoke-virtual {p0}, Lcom/android/settings/homepage/contextualcards/ContextualCard;->getTextSliceUri()Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v0

    long-to-int v8, v10

    move-object v3, v2

    invoke-virtual/range {v3 .. v8}, Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;->action(IIILjava/lang/String;I)V

    return-object v9
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker;->call()Lcom/android/settings/homepage/contextualcards/ContextualCard;

    move-result-object p0

    return-object p0
.end method

.method isCardEligibleToDisplay(Lcom/android/settings/homepage/contextualcards/ContextualCard;)Z
    .locals 4

    goto/32 :goto_1f

    nop

    :goto_0
    return v1

    :goto_1
    goto/32 :goto_14

    nop

    :goto_2
    invoke-virtual {p1}, Lcom/android/settings/homepage/contextualcards/ContextualCard;->mutate()Lcom/android/settings/homepage/contextualcards/ContextualCard$Builder;

    move-result-object v0

    goto/32 :goto_10

    nop

    :goto_3
    const-string p1, "Failed to bind slice, not eligible for display "

    goto/32 :goto_11

    nop

    :goto_4
    const-string v3, "error"

    goto/32 :goto_8

    nop

    :goto_5
    if-nez v0, :cond_0

    goto/32 :goto_17

    :cond_0
    goto/32 :goto_25

    nop

    :goto_6
    if-ltz v0, :cond_1

    goto/32 :goto_1c

    :cond_1
    goto/32 :goto_1b

    nop

    :goto_7
    invoke-virtual {p1, v1}, Lcom/android/settings/homepage/contextualcards/ContextualCard$Builder;->setHasInlineAction(Z)Lcom/android/settings/homepage/contextualcards/ContextualCard$Builder;

    move-result-object p1

    goto/32 :goto_15

    nop

    :goto_8
    invoke-virtual {v2, v3}, Landroidx/slice/Slice;->hasHint(Ljava/lang/String;)Z

    move-result v3

    goto/32 :goto_27

    nop

    :goto_9
    invoke-virtual {p1}, Lcom/android/settings/homepage/contextualcards/ContextualCard;->getSliceUri()Landroid/net/Uri;

    move-result-object v0

    goto/32 :goto_2b

    nop

    :goto_a
    goto :goto_1

    :goto_b
    goto/32 :goto_2

    nop

    :goto_c
    const/4 v1, 0x0

    goto/32 :goto_6

    nop

    :goto_d
    const-string p1, "EligibleCardChecker"

    goto/32 :goto_e

    nop

    :goto_e
    invoke-static {p1, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_f

    nop

    :goto_f
    return v1

    :goto_10
    invoke-virtual {v0, v2}, Lcom/android/settings/homepage/contextualcards/ContextualCard$Builder;->setSlice(Landroidx/slice/Slice;)Lcom/android/settings/homepage/contextualcards/ContextualCard$Builder;

    move-result-object v0

    goto/32 :goto_22

    nop

    :goto_11
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1e

    nop

    :goto_12
    const-wide/16 v2, 0x0

    goto/32 :goto_13

    nop

    :goto_13
    cmpg-double v0, v0, v2

    goto/32 :goto_c

    nop

    :goto_14
    new-instance p0, Ljava/lang/StringBuilder;

    goto/32 :goto_21

    nop

    :goto_15
    invoke-virtual {p1}, Lcom/android/settings/homepage/contextualcards/ContextualCard$Builder;->build()Lcom/android/settings/homepage/contextualcards/ContextualCard;

    move-result-object p1

    goto/32 :goto_16

    nop

    :goto_16
    iput-object p1, p0, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker;->mCard:Lcom/android/settings/homepage/contextualcards/ContextualCard;

    :goto_17
    goto/32 :goto_0

    nop

    :goto_18
    const-string v3, "content"

    goto/32 :goto_20

    nop

    :goto_19
    invoke-virtual {p0, v0}, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker;->bindSlice(Landroid/net/Uri;)Landroidx/slice/Slice;

    move-result-object v2

    goto/32 :goto_24

    nop

    :goto_1a
    if-eqz v2, :cond_2

    goto/32 :goto_2a

    :cond_2
    goto/32 :goto_29

    nop

    :goto_1b
    return v1

    :goto_1c
    goto/32 :goto_9

    nop

    :goto_1d
    const/4 v1, 0x1

    goto/32 :goto_5

    nop

    :goto_1e
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_28

    nop

    :goto_1f
    invoke-virtual {p1}, Lcom/android/settings/homepage/contextualcards/ContextualCard;->getRankingScore()D

    move-result-wide v0

    goto/32 :goto_12

    nop

    :goto_20
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto/32 :goto_1a

    nop

    :goto_21
    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_3

    nop

    :goto_22
    invoke-virtual {v0}, Lcom/android/settings/homepage/contextualcards/ContextualCard$Builder;->build()Lcom/android/settings/homepage/contextualcards/ContextualCard;

    move-result-object v0

    goto/32 :goto_23

    nop

    :goto_23
    iput-object v0, p0, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker;->mCard:Lcom/android/settings/homepage/contextualcards/ContextualCard;

    goto/32 :goto_26

    nop

    :goto_24
    if-nez v2, :cond_3

    goto/32 :goto_1

    :cond_3
    goto/32 :goto_4

    nop

    :goto_25
    invoke-virtual {p1}, Lcom/android/settings/homepage/contextualcards/ContextualCard;->mutate()Lcom/android/settings/homepage/contextualcards/ContextualCard$Builder;

    move-result-object p1

    goto/32 :goto_7

    nop

    :goto_26
    invoke-virtual {p0, v2}, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker;->isSliceToggleable(Landroidx/slice/Slice;)Z

    move-result v0

    goto/32 :goto_1d

    nop

    :goto_27
    if-nez v3, :cond_4

    goto/32 :goto_b

    :cond_4
    goto/32 :goto_a

    nop

    :goto_28
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_d

    nop

    :goto_29
    return v1

    :goto_2a
    goto/32 :goto_19

    nop

    :goto_2b
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_18

    nop
.end method

.method isSliceToggleable(Landroidx/slice/Slice;)Z
    .locals 0

    goto/32 :goto_3

    nop

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result p0

    goto/32 :goto_4

    nop

    :goto_1
    return p0

    :goto_2
    invoke-static {p0, p1}, Landroidx/slice/SliceMetadata;->from(Landroid/content/Context;Landroidx/slice/Slice;)Landroidx/slice/SliceMetadata;

    move-result-object p0

    goto/32 :goto_5

    nop

    :goto_3
    iget-object p0, p0, Lcom/android/settings/homepage/contextualcards/EligibleCardChecker;->mContext:Landroid/content/Context;

    goto/32 :goto_2

    nop

    :goto_4
    xor-int/lit8 p0, p0, 0x1

    goto/32 :goto_1

    nop

    :goto_5
    invoke-virtual {p0}, Landroidx/slice/SliceMetadata;->getToggles()Ljava/util/List;

    move-result-object p0

    goto/32 :goto_0

    nop
.end method
