.class Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper$SliceViewHolder;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public static synthetic $r8$lambda$Ha1eNRLfiQDhhc5SyU7bRIFujtQ(Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper;Lcom/android/settings/homepage/contextualcards/ContextualCard;Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper$SliceViewHolder;Landroidx/slice/widget/EventInfo;Landroidx/slice/SliceItem;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper;->lambda$bindView$0(Lcom/android/settings/homepage/contextualcards/ContextualCard;Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper$SliceViewHolder;Landroidx/slice/widget/EventInfo;Landroidx/slice/SliceItem;)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper;->mContext:Landroid/content/Context;

    return-void
.end method

.method private synthetic lambda$bindView$0(Lcom/android/settings/homepage/contextualcards/ContextualCard;Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper$SliceViewHolder;Landroidx/slice/widget/EventInfo;Landroidx/slice/SliceItem;)V
    .locals 0

    iget p4, p3, Landroidx/slice/widget/EventInfo;->rowIndex:I

    iget p3, p3, Landroidx/slice/widget/EventInfo;->actionType:I

    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result p2

    invoke-static {p1, p4, p3, p2}, Lcom/android/settings/homepage/contextualcards/logging/ContextualCardLogUtils;->buildCardClickLog(Lcom/android/settings/homepage/contextualcards/ContextualCard;III)Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper;->mContext:Landroid/content/Context;

    invoke-static {p2}, Lcom/android/settings/overlay/FeatureFactory;->getFactory(Landroid/content/Context;)Lcom/android/settings/overlay/FeatureFactory;

    move-result-object p2

    invoke-virtual {p2}, Lcom/android/settings/overlay/FeatureFactory;->getMetricsFeatureProvider()Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;

    move-result-object p2

    iget-object p0, p0, Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper;->mContext:Landroid/content/Context;

    const/16 p3, 0x682

    invoke-virtual {p2, p0, p3, p1}, Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;->action(Landroid/content/Context;ILjava/lang/String;)V

    return-void
.end method


# virtual methods
.method bindView(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Lcom/android/settings/homepage/contextualcards/ContextualCard;Landroidx/slice/Slice;)V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p0, p3}, Landroidx/slice/widget/SliceView;->setShowHeaderDivider(Z)V

    goto/32 :goto_7

    nop

    :goto_1
    invoke-virtual {v0, p3}, Landroidx/slice/widget/SliceView;->setSlice(Landroidx/slice/Slice;)V

    goto/32 :goto_b

    nop

    :goto_2
    check-cast p1, Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper$SliceViewHolder;

    goto/32 :goto_17

    nop

    :goto_3
    invoke-virtual {v0, v1}, Landroidx/slice/widget/SliceView;->setMode(I)V

    goto/32 :goto_6

    nop

    :goto_4
    const/4 v1, 0x2

    goto/32 :goto_3

    nop

    :goto_5
    iget-object p0, p1, Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper$SliceViewHolder;->sliceView:Landroidx/slice/widget/SliceView;

    goto/32 :goto_8

    nop

    :goto_6
    iget-object v0, p1, Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper$SliceViewHolder;->sliceView:Landroidx/slice/widget/SliceView;

    goto/32 :goto_1

    nop

    :goto_7
    iget-object p0, p1, Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper$SliceViewHolder;->sliceView:Landroidx/slice/widget/SliceView;

    goto/32 :goto_11

    nop

    :goto_8
    const/4 p3, 0x1

    goto/32 :goto_18

    nop

    :goto_9
    new-instance v0, Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper$$ExternalSyntheticLambda0;

    goto/32 :goto_13

    nop

    :goto_a
    invoke-virtual {p2}, Lcom/android/settings/homepage/contextualcards/ContextualCard;->getSliceUri()Landroid/net/Uri;

    move-result-object v1

    goto/32 :goto_d

    nop

    :goto_b
    iget-object p3, p1, Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper$SliceViewHolder;->sliceView:Landroidx/slice/widget/SliceView;

    goto/32 :goto_9

    nop

    :goto_c
    invoke-virtual {p3, v0}, Landroidx/slice/widget/SliceView;->setOnSliceActionListener(Landroidx/slice/widget/SliceView$OnSliceActionListener;)V

    goto/32 :goto_5

    nop

    :goto_d
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    goto/32 :goto_15

    nop

    :goto_e
    return-void

    :goto_f
    const/4 v1, 0x0

    goto/32 :goto_16

    nop

    :goto_10
    if-nez p0, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_1a

    nop

    :goto_11
    invoke-virtual {p0, p3}, Landroidx/slice/widget/SliceView;->setShowActionDividers(Z)V

    :goto_12
    goto/32 :goto_e

    nop

    :goto_13
    invoke-direct {v0, p0, p2, p1}, Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper;Lcom/android/settings/homepage/contextualcards/ContextualCard;Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper$SliceViewHolder;)V

    goto/32 :goto_c

    nop

    :goto_14
    invoke-virtual {p2}, Lcom/android/settings/homepage/contextualcards/ContextualCard;->isLargeCard()Z

    move-result p0

    goto/32 :goto_10

    nop

    :goto_15
    iget-object v0, p1, Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper$SliceViewHolder;->sliceView:Landroidx/slice/widget/SliceView;

    goto/32 :goto_4

    nop

    :goto_16
    invoke-virtual {v0, v1}, Landroidx/slice/widget/SliceView;->setScrollable(Z)V

    goto/32 :goto_19

    nop

    :goto_17
    iget-object v0, p1, Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper$SliceViewHolder;->sliceView:Landroidx/slice/widget/SliceView;

    goto/32 :goto_f

    nop

    :goto_18
    invoke-virtual {p0, p3}, Landroidx/slice/widget/SliceView;->setShowTitleItems(Z)V

    goto/32 :goto_14

    nop

    :goto_19
    iget-object v0, p1, Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper$SliceViewHolder;->sliceView:Landroidx/slice/widget/SliceView;

    goto/32 :goto_a

    nop

    :goto_1a
    iget-object p0, p1, Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper$SliceViewHolder;->sliceView:Landroidx/slice/widget/SliceView;

    goto/32 :goto_0

    nop
.end method

.method createViewHolder(Landroid/view/View;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    new-instance p0, Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper$SliceViewHolder;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-direct {p0, p1}, Lcom/android/settings/homepage/contextualcards/slices/SliceFullCardRendererHelper$SliceViewHolder;-><init>(Landroid/view/View;)V

    goto/32 :goto_2

    nop

    :goto_2
    return-object p0
.end method
