.class public Lcom/android/settings/homepage/contextualcards/ContextualCardFeatureProviderImpl;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/settings/homepage/contextualcards/ContextualCardFeatureProvider;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public static synthetic $r8$lambda$hmd8YNfieIIfqyB2sON4cseRyMU(Lcom/android/settings/homepage/contextualcards/ContextualCardFeatureProviderImpl;J)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/homepage/contextualcards/ContextualCardFeatureProviderImpl;->lambda$getContextualCards$0(J)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/homepage/contextualcards/ContextualCardFeatureProviderImpl;->mContext:Landroid/content/Context;

    return-void
.end method

.method private synthetic lambda$getContextualCards$0(J)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/homepage/contextualcards/ContextualCardFeatureProviderImpl;->resetDismissedTime(J)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getContextualCards()Landroid/database/Cursor;
    .locals 11

    iget-object v0, p0, Lcom/android/settings/homepage/contextualcards/ContextualCardFeatureProviderImpl;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/homepage/contextualcards/CardDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/android/settings/homepage/contextualcards/CardDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    sub-long v9, v2, v4

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    aput-object v0, v5, v2

    const-string v2, "cards"

    const/4 v3, 0x0

    const-string v4, "dismissed_timestamp < ? OR dismissed_timestamp IS NULL"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string/jumbo v8, "score DESC"

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    new-instance v1, Lcom/android/settings/homepage/contextualcards/ContextualCardFeatureProviderImpl$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, v9, v10}, Lcom/android/settings/homepage/contextualcards/ContextualCardFeatureProviderImpl$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/homepage/contextualcards/ContextualCardFeatureProviderImpl;J)V

    invoke-static {v1}, Lcom/android/settingslib/utils/ThreadUtils;->postOnBackgroundThread(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    return-object v0
.end method

.method public markCardAsDismissed(Landroid/content/Context;Ljava/lang/String;)I
    .locals 3

    iget-object p0, p0, Lcom/android/settings/homepage/contextualcards/ContextualCardFeatureProviderImpl;->mContext:Landroid/content/Context;

    invoke-static {p0}, Lcom/android/settings/homepage/contextualcards/CardDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/android/settings/homepage/contextualcards/CardDatabaseHelper;

    move-result-object p0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object p0

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "dismissed_timestamp"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const-string p2, "cards"

    const-string/jumbo v2, "name=?"

    invoke-virtual {p0, p2, v0, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result p0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    sget-object p2, Lcom/android/settings/homepage/contextualcards/CardContentProvider;->DELETE_CARD_URI:Landroid/net/Uri;

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    return p0
.end method

.method resetDismissedTime(J)I
    .locals 2

    goto/32 :goto_e

    nop

    :goto_0
    const-string p1, "cards"

    goto/32 :goto_16

    nop

    :goto_1
    new-instance v0, Landroid/content/ContentValues;

    goto/32 :goto_17

    nop

    :goto_2
    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_3
    aput-object p1, v1, p2

    goto/32 :goto_0

    nop

    :goto_4
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_7

    nop

    :goto_5
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_1a

    nop

    :goto_6
    const-string p2, " records of dismissed time."

    goto/32 :goto_8

    nop

    :goto_7
    const/4 p2, 0x0

    goto/32 :goto_3

    nop

    :goto_8
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_5

    nop

    :goto_9
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_2

    nop

    :goto_a
    invoke-static {p0}, Lcom/android/settings/homepage/contextualcards/CardDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/android/settings/homepage/contextualcards/CardDatabaseHelper;

    move-result-object p0

    goto/32 :goto_c

    nop

    :goto_b
    new-instance p1, Ljava/lang/StringBuilder;

    goto/32 :goto_d

    nop

    :goto_c
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object p0

    goto/32 :goto_1

    nop

    :goto_d
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_11

    nop

    :goto_e
    iget-object p0, p0, Lcom/android/settings/homepage/contextualcards/ContextualCardFeatureProviderImpl;->mContext:Landroid/content/Context;

    goto/32 :goto_a

    nop

    :goto_f
    invoke-static {p2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_10
    goto/32 :goto_18

    nop

    :goto_11
    const-string p2, "Reset "

    goto/32 :goto_9

    nop

    :goto_12
    const/4 v1, 0x1

    goto/32 :goto_14

    nop

    :goto_13
    invoke-virtual {p0, p1, v0, p2, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result p0

    goto/32 :goto_1b

    nop

    :goto_14
    new-array v1, v1, [Ljava/lang/String;

    goto/32 :goto_4

    nop

    :goto_15
    if-nez p1, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_b

    nop

    :goto_16
    const-string p2, "dismissed_timestamp < ? AND dismissed_timestamp IS NOT NULL"

    goto/32 :goto_13

    nop

    :goto_17
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    goto/32 :goto_1c

    nop

    :goto_18
    return p0

    :goto_19
    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/32 :goto_12

    nop

    :goto_1a
    const-string p2, "ContextualCardFeatureProvider"

    goto/32 :goto_f

    nop

    :goto_1b
    sget-boolean p1, Landroid/os/Build;->IS_DEBUGGABLE:Z

    goto/32 :goto_15

    nop

    :goto_1c
    const-string v1, "dismissed_timestamp"

    goto/32 :goto_19

    nop
.end method
