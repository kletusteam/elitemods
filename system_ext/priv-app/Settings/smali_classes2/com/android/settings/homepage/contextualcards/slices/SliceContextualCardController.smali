.class public Lcom/android/settings/homepage/contextualcards/slices/SliceContextualCardController;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/settings/homepage/contextualcards/ContextualCardController;


# instance fields
.field private mCardUpdateListener:Lcom/android/settings/homepage/contextualcards/ContextualCardUpdateListener;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public static synthetic $r8$lambda$875daOTsujjPYlcxZESRBc18oa0(Lcom/android/settings/homepage/contextualcards/slices/SliceContextualCardController;Lcom/android/settings/homepage/contextualcards/ContextualCard;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/homepage/contextualcards/slices/SliceContextualCardController;->lambda$onDismissed$0(Lcom/android/settings/homepage/contextualcards/ContextualCard;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/homepage/contextualcards/slices/SliceContextualCardController;->mContext:Landroid/content/Context;

    return-void
.end method

.method private getSimpleCardName(Lcom/android/settings/homepage/contextualcards/ContextualCard;)Ljava/lang/String;
    .locals 0

    invoke-virtual {p1}, Lcom/android/settings/homepage/contextualcards/ContextualCard;->getName()Ljava/lang/String;

    move-result-object p0

    const-string p1, "/"

    invoke-virtual {p0, p1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    array-length p1, p0

    add-int/lit8 p1, p1, -0x1

    aget-object p0, p0, p1

    return-object p0
.end method

.method private synthetic lambda$onDismissed$0(Lcom/android/settings/homepage/contextualcards/ContextualCard;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/homepage/contextualcards/slices/SliceContextualCardController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/overlay/FeatureFactory;->getFactory(Landroid/content/Context;)Lcom/android/settings/overlay/FeatureFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/homepage/contextualcards/slices/SliceContextualCardController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/android/settings/overlay/FeatureFactory;->getContextualCardFeatureProvider(Landroid/content/Context;)Lcom/android/settings/homepage/contextualcards/ContextualCardFeatureProvider;

    move-result-object v0

    iget-object p0, p0, Lcom/android/settings/homepage/contextualcards/slices/SliceContextualCardController;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/android/settings/homepage/contextualcards/ContextualCard;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p0, p1}, Lcom/android/settings/homepage/contextualcards/ContextualCardFeatureProvider;->markCardAsDismissed(Landroid/content/Context;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method isFeedbackEnabled(Ljava/lang/String;)Z
    .locals 0

    goto/32 :goto_3

    nop

    :goto_0
    if-eqz p0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_2

    nop

    :goto_1
    if-nez p0, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_9

    nop

    :goto_2
    sget-boolean p0, Landroid/os/Build;->IS_DEBUGGABLE:Z

    goto/32 :goto_1

    nop

    :goto_3
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    goto/32 :goto_0

    nop

    :goto_4
    return p0

    :goto_5
    const/4 p0, 0x0

    :goto_6
    goto/32 :goto_4

    nop

    :goto_7
    goto :goto_6

    :goto_8
    goto/32 :goto_5

    nop

    :goto_9
    const/4 p0, 0x1

    goto/32 :goto_7

    nop
.end method

.method public onActionClick(Lcom/android/settings/homepage/contextualcards/ContextualCard;)V
    .locals 0

    return-void
.end method

.method public onDismissed(Lcom/android/settings/homepage/contextualcards/ContextualCard;)V
    .locals 2

    new-instance v0, Lcom/android/settings/homepage/contextualcards/slices/SliceContextualCardController$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/homepage/contextualcards/slices/SliceContextualCardController$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/homepage/contextualcards/slices/SliceContextualCardController;Lcom/android/settings/homepage/contextualcards/ContextualCard;)V

    invoke-static {v0}, Lcom/android/settingslib/utils/ThreadUtils;->postOnBackgroundThread(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    invoke-virtual {p0, p1}, Lcom/android/settings/homepage/contextualcards/slices/SliceContextualCardController;->showFeedbackDialog(Lcom/android/settings/homepage/contextualcards/ContextualCard;)V

    iget-object v0, p0, Lcom/android/settings/homepage/contextualcards/slices/SliceContextualCardController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/overlay/FeatureFactory;->getFactory(Landroid/content/Context;)Lcom/android/settings/overlay/FeatureFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/overlay/FeatureFactory;->getMetricsFeatureProvider()Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;

    move-result-object v0

    iget-object p0, p0, Lcom/android/settings/homepage/contextualcards/slices/SliceContextualCardController;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/homepage/contextualcards/logging/ContextualCardLogUtils;->buildCardDismissLog(Lcom/android/settings/homepage/contextualcards/ContextualCard;)Ljava/lang/String;

    move-result-object p1

    const/16 v1, 0x681

    invoke-virtual {v0, p0, v1, p1}, Lcom/android/settingslib/core/instrumentation/MetricsFeatureProvider;->action(Landroid/content/Context;ILjava/lang/String;)V

    return-void
.end method

.method public onPrimaryClick(Lcom/android/settings/homepage/contextualcards/ContextualCard;)V
    .locals 0

    return-void
.end method

.method public setCardUpdateListener(Lcom/android/settings/homepage/contextualcards/ContextualCardUpdateListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/homepage/contextualcards/slices/SliceContextualCardController;->mCardUpdateListener:Lcom/android/settings/homepage/contextualcards/ContextualCardUpdateListener;

    return-void
.end method

.method showFeedbackDialog(Lcom/android/settings/homepage/contextualcards/ContextualCard;)V
    .locals 4

    goto/32 :goto_b

    nop

    :goto_0
    invoke-virtual {v1, p1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/32 :goto_14

    nop

    :goto_1
    return-void

    :goto_2
    const-string v2, "card_name"

    goto/32 :goto_13

    nop

    :goto_3
    const-string p1, "feedback_email"

    goto/32 :goto_0

    nop

    :goto_4
    invoke-virtual {p0, v0}, Lcom/android/settings/homepage/contextualcards/slices/SliceContextualCardController;->isFeedbackEnabled(Ljava/lang/String;)Z

    move-result v1

    goto/32 :goto_e

    nop

    :goto_5
    const-class v3, Lcom/android/settings/homepage/contextualcards/ContextualCardFeedbackDialog;

    goto/32 :goto_10

    nop

    :goto_6
    iget-object v2, p0, Lcom/android/settings/homepage/contextualcards/slices/SliceContextualCardController;->mContext:Landroid/content/Context;

    goto/32 :goto_5

    nop

    :goto_7
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/32 :goto_1

    nop

    :goto_8
    return-void

    :goto_9
    goto/32 :goto_12

    nop

    :goto_a
    invoke-virtual {v1, p1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto/32 :goto_f

    nop

    :goto_b
    iget-object v0, p0, Lcom/android/settings/homepage/contextualcards/slices/SliceContextualCardController;->mContext:Landroid/content/Context;

    goto/32 :goto_c

    nop

    :goto_c
    sget v1, Lcom/android/settings/R$string;->config_contextual_card_feedback_email:I

    goto/32 :goto_d

    nop

    :goto_d
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_e
    if-eqz v1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_8

    nop

    :goto_f
    iget-object p0, p0, Lcom/android/settings/homepage/contextualcards/slices/SliceContextualCardController;->mContext:Landroid/content/Context;

    goto/32 :goto_7

    nop

    :goto_10
    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto/32 :goto_11

    nop

    :goto_11
    invoke-direct {p0, p1}, Lcom/android/settings/homepage/contextualcards/slices/SliceContextualCardController;->getSimpleCardName(Lcom/android/settings/homepage/contextualcards/ContextualCard;)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_2

    nop

    :goto_12
    new-instance v1, Landroid/content/Intent;

    goto/32 :goto_6

    nop

    :goto_13
    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/32 :goto_3

    nop

    :goto_14
    const/high16 p1, 0x10000000

    goto/32 :goto_a

    nop
.end method
