.class public Lcom/android/settings/password/ManagedLockPasswordProvider;
.super Ljava/lang/Object;


# direct methods
.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get(Landroid/content/Context;I)Lcom/android/settings/password/ManagedLockPasswordProvider;
    .locals 0

    new-instance p0, Lcom/android/settings/password/ManagedLockPasswordProvider;

    invoke-direct {p0}, Lcom/android/settings/password/ManagedLockPasswordProvider;-><init>()V

    return-object p0
.end method


# virtual methods
.method createIntent(ZLcom/android/internal/widget/LockscreenCredential;)Landroid/content/Intent;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    const/4 p0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    return-object p0
.end method

.method getPickerOptionTitle(Z)Ljava/lang/CharSequence;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    const-string p0, ""

    goto/32 :goto_1

    nop

    :goto_1
    return-object p0
.end method

.method isManagedPasswordChoosable()Z
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    const/4 p0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    return p0
.end method

.method isSettingManagedPasswordSupported()Z
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    const/4 p0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    return p0
.end method
