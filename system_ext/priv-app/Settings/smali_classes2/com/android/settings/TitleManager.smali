.class public Lcom/android/settings/TitleManager;
.super Ljava/lang/Object;


# direct methods
.method public static getFeedbackSettingsTitle(Landroid/content/Context;)I
    .locals 4

    const/4 v0, -0x1

    if-eqz p0, :cond_3

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.miui.miservice"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    if-nez p0, :cond_1

    return v0

    :cond_1
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    :goto_0
    if-nez v1, :cond_2

    return v0

    :cond_2
    iget-object p0, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget p0, p0, Landroid/content/pm/ApplicationInfo;->labelRes:I

    return p0

    :cond_3
    :goto_1
    return v0
.end method

.method public static getScreenTitle(Landroid/content/Context;)I
    .locals 0

    invoke-static {p0}, Lcom/android/settings/AodCompatibilityHelper;->isAodAvailable(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_0

    sget p0, Lcom/android/settings/R$string;->aod_and_lock_screen_settings_title:I

    goto :goto_0

    :cond_0
    sget p0, Lcom/android/settings/R$string;->lock_screen_settings_title:I

    :goto_0
    return p0
.end method

.method public static getStatusBarTitle()I
    .locals 1

    sget-boolean v0, Lmiui/util/CustomizeUtil;->HAS_NOTCH:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/android/settings/R$string;->notch_and_status_bar_settings:I

    goto :goto_0

    :cond_0
    sget v0, Lcom/android/settings/R$string;->status_bar_title:I

    :goto_0
    return v0
.end method
