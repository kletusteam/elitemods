.class public Lcom/android/settings/MainClear;
.super Lcom/android/settings/core/InstrumentedFragment;

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# static fields
.field static final CREDENTIAL_CONFIRM_REQUEST:I = 0x38

.field static final KEYGUARD_REQUEST:I = 0x37


# instance fields
.field private mContentView:Landroid/view/View;

.field mEsimStorage:Landroid/widget/CheckBox;

.field mEsimStorageContainer:Landroid/view/View;

.field mExternalStorage:Landroid/widget/CheckBox;

.field private mExternalStorageContainer:Landroid/view/View;

.field mInitiateButton:Lcom/google/android/setupcompat/template/FooterButton;

.field protected final mInitiateListener:Landroid/view/View$OnClickListener;

.field mScrollView:Landroid/widget/ScrollView;


# direct methods
.method public static synthetic $r8$lambda$1Pf9eJ-AsQC9IU_-p0XtoU3n4rU(Lcom/android/settings/MainClear;Landroid/content/DialogInterface;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/MainClear;->lambda$onInflateView$2(Landroid/content/DialogInterface;)V

    return-void
.end method

.method public static synthetic $r8$lambda$1obZxIkdPycDdeXljB-QHrbcnMs(Lcom/android/settings/MainClear;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MainClear;->lambda$loadAccountList$0()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$vytkT4JMo9D29JLHC3-O7q8aquI(Lcom/android/settings/MainClear;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MainClear;->lambda$loadAccountList$1()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mrunKeyguardConfirmation(Lcom/android/settings/MainClear;I)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/MainClear;->runKeyguardConfirmation(I)Z

    move-result p0

    return p0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/core/InstrumentedFragment;-><init>()V

    new-instance v0, Lcom/android/settings/MainClear$1;

    invoke-direct {v0, p0}, Lcom/android/settings/MainClear$1;-><init>(Lcom/android/settings/MainClear;)V

    iput-object v0, p0, Lcom/android/settings/MainClear;->mInitiateListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private getContentDescription(Landroid/view/View;Ljava/lang/StringBuffer;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    check-cast p1, Landroid/view/ViewGroup;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lcom/android/settings/MainClear;->getContentDescription(Landroid/view/View;Ljava/lang/StringBuffer;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    instance-of p0, p1, Landroid/widget/TextView;

    if-eqz p0, :cond_2

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-virtual {p2, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuffer;

    const-string p0, ","

    invoke-virtual {p2, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_2
    return-void
.end method

.method private isExtStorageEncrypted()Z
    .locals 1

    invoke-static {}, Landroid/sysprop/VoldProperties;->decrypt()Ljava/util/Optional;

    move-result-object p0

    const-string v0, ""

    invoke-virtual {p0, v0}, Ljava/util/Optional;->orElse(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    return p0
.end method

.method private synthetic lambda$loadAccountList$0()Ljava/lang/String;
    .locals 1

    sget v0, Lcom/android/settings/R$string;->category_work:I

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private synthetic lambda$loadAccountList$1()Ljava/lang/String;
    .locals 1

    sget v0, Lcom/android/settings/R$string;->category_personal:I

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private synthetic lambda$onInflateView$2(Landroid/content/DialogInterface;)V
    .locals 0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method private loadAccountList(Landroid/os/UserManager;)V
    .locals 22

    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/android/settings/MainClear;->mContentView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->accounts_label:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-object v0, v1, Lcom/android/settings/MainClear;->mContentView:Landroid/view/View;

    sget v3, Lcom/android/settings/R$id;->accounts:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v4

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    move-object/from16 v5, p1

    invoke-virtual {v5, v0}, Landroid/os/UserManager;->getProfiles(I)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v8

    const-string v0, "layout_inflater"

    invoke-virtual {v4, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/view/LayoutInflater;

    const/4 v0, 0x0

    const/4 v11, 0x0

    :goto_0
    if-ge v11, v7, :cond_9

    invoke-interface {v6, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/content/pm/UserInfo;

    iget v14, v13, Landroid/content/pm/UserInfo;->id:I

    new-instance v15, Landroid/os/UserHandle;

    invoke-direct {v15, v14}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v8, v14}, Landroid/accounts/AccountManager;->getAccountsAsUser(I)[Landroid/accounts/Account;

    move-result-object v10

    array-length v12, v10

    if-nez v12, :cond_0

    move-object/from16 v17, v6

    move/from16 v19, v7

    move-object/from16 v18, v8

    goto/16 :goto_b

    :cond_0
    add-int v16, v0, v12

    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0, v14}, Landroid/accounts/AccountManager;->getAuthenticatorTypesAsUser(I)[Landroid/accounts/AuthenticatorDescription;

    move-result-object v14

    array-length v5, v14

    move-object/from16 v17, v6

    const/4 v0, 0x1

    if-le v7, v0, :cond_2

    invoke-static {v9, v3}, Lcom/android/settings/Utils;->inflateCategoryHeader(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v6

    move-object/from16 v18, v8

    invoke-virtual {v0}, Landroid/view/View;->getPaddingBottom()I

    move-result v8

    move/from16 v19, v7

    const/4 v7, 0x0

    invoke-virtual {v0, v7, v6, v7, v8}, Landroid/view/View;->setPadding(IIII)V

    const v6, 0x1020016

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    const-class v6, Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v4, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v13}, Landroid/content/pm/UserInfo;->isManagedProfile()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {v6}, Landroid/app/admin/DevicePolicyManager;->getResources()Landroid/app/admin/DevicePolicyResourcesManager;

    move-result-object v6

    new-instance v8, Lcom/android/settings/MainClear$$ExternalSyntheticLambda1;

    invoke-direct {v8, v1}, Lcom/android/settings/MainClear$$ExternalSyntheticLambda1;-><init>(Lcom/android/settings/MainClear;)V

    const-string v13, "Settings.WORK_CATEGORY_HEADER"

    invoke-virtual {v6, v13, v8}, Landroid/app/admin/DevicePolicyResourcesManager;->getString(Ljava/lang/String;Ljava/util/function/Supplier;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v6}, Landroid/app/admin/DevicePolicyManager;->getResources()Landroid/app/admin/DevicePolicyResourcesManager;

    move-result-object v6

    new-instance v8, Lcom/android/settings/MainClear$$ExternalSyntheticLambda2;

    invoke-direct {v8, v1}, Lcom/android/settings/MainClear$$ExternalSyntheticLambda2;-><init>(Lcom/android/settings/MainClear;)V

    const-string v13, "Settings.PERSONAL_CATEGORY_HEADER"

    invoke-virtual {v6, v13, v8}, Landroid/app/admin/DevicePolicyResourcesManager;->getString(Ljava/lang/String;Ljava/util/function/Supplier;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_2

    :cond_2
    move/from16 v19, v7

    move-object/from16 v18, v8

    :goto_2
    const/4 v7, 0x0

    :goto_3
    if-ge v7, v12, :cond_8

    aget-object v6, v10, v7

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v5, :cond_4

    iget-object v13, v6, Landroid/accounts/Account;->type:Ljava/lang/String;

    aget-object v8, v14, v0

    iget-object v8, v8, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    invoke-virtual {v13, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    aget-object v0, v14, v0

    move-object v8, v0

    goto :goto_5

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_4
    const/4 v8, 0x0

    :goto_5
    const-string v13, "MainClear"

    if-nez v8, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "No descriptor for account name="

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, " type="

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, v6, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v13, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move/from16 v20, v5

    move-object/from16 v21, v10

    const v5, 0x1020016

    goto/16 :goto_a

    :cond_5
    :try_start_0
    iget v0, v8, Landroid/accounts/AuthenticatorDescription;->iconId:I

    if-eqz v0, :cond_6

    iget-object v0, v8, Landroid/accounts/AuthenticatorDescription;->packageName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    move/from16 v20, v5

    const/4 v5, 0x0

    :try_start_1
    invoke-virtual {v4, v0, v5, v15}, Landroid/content/Context;->createPackageContextAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-object/from16 v21, v10

    :try_start_2
    iget v10, v8, Landroid/accounts/AuthenticatorDescription;->iconId:I

    invoke-virtual {v0, v10}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v5, v0, v15}, Landroid/content/pm/PackageManager;->getUserBadgedIcon(Landroid/graphics/drawable/Drawable;Landroid/os/UserHandle;)Landroid/graphics/drawable/Drawable;

    move-result-object v0
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v8, v0

    goto :goto_9

    :catch_0
    move-exception v0

    goto :goto_7

    :catch_1
    move-exception v0

    goto :goto_6

    :cond_6
    move/from16 v20, v5

    move-object/from16 v21, v10

    goto :goto_8

    :catch_2
    move-exception v0

    move/from16 v20, v5

    :goto_6
    move-object/from16 v21, v10

    :goto_7
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Invalid icon id for account type "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, v8, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v13, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_8

    :catch_3
    move/from16 v20, v5

    :catch_4
    move-object/from16 v21, v10

    :catch_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Bad package name for account type "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v8, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v13, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_8
    const/4 v8, 0x0

    :goto_9
    if-nez v8, :cond_7

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getDefaultActivityIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    :cond_7
    sget v0, Lcom/android/settings/R$layout;->main_clear_account:I

    const/4 v5, 0x0

    invoke-virtual {v9, v0, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v5, 0x1020006

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const v5, 0x1020016

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iget-object v6, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v8, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :goto_a
    add-int/lit8 v7, v7, 0x1

    move/from16 v5, v20

    move-object/from16 v10, v21

    goto/16 :goto_3

    :cond_8
    move/from16 v0, v16

    :goto_b
    add-int/lit8 v11, v11, 0x1

    move-object/from16 v5, p1

    move-object/from16 v6, v17

    move-object/from16 v8, v18

    move/from16 v7, v19

    goto/16 :goto_0

    :cond_9
    move/from16 v19, v7

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-lez v0, :cond_a

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_a
    iget-object v0, v1, Lcom/android/settings/MainClear;->mContentView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->other_users_present:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Landroid/os/UserManager;->getUserCount()I

    move-result v1

    sub-int v1, v1, v19

    if-lez v1, :cond_b

    move v12, v5

    goto :goto_c

    :cond_b
    move v12, v4

    :goto_c
    if-eqz v12, :cond_c

    move v10, v4

    goto :goto_d

    :cond_c
    const/16 v10, 0x8

    :goto_d
    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private runKeyguardConfirmation(I)Z
    .locals 3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Lcom/android/settings/password/ChooseLockSettingsHelper$Builder;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Lcom/android/settings/password/ChooseLockSettingsHelper$Builder;-><init>(Landroid/app/Activity;Landroidx/fragment/app/Fragment;)V

    invoke-virtual {v1, p1}, Lcom/android/settings/password/ChooseLockSettingsHelper$Builder;->setRequestCode(I)Lcom/android/settings/password/ChooseLockSettingsHelper$Builder;

    move-result-object p0

    sget p1, Lcom/android/settings/R$string;->main_clear_short_title:I

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/android/settings/password/ChooseLockSettingsHelper$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/android/settings/password/ChooseLockSettingsHelper$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/android/settings/password/ChooseLockSettingsHelper$Builder;->show()Z

    move-result p0

    return p0
.end method

.method private setUpActionBarAndTitle()V
    .locals 2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    const-string v0, "MainClear"

    if-nez p0, :cond_0

    const-string p0, "No activity attached, skipping setUpActionBarAndTitle"

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    if-nez v1, :cond_1

    const-string p0, "No actionbar, skipping setUpActionBarAndTitle"

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    invoke-virtual {v1}, Landroid/app/ActionBar;->hide()V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/Window;->setStatusBarColor(I)V

    return-void
.end method

.method private setUpInitiateButton()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/MainClear;->mInitiateButton:Lcom/google/android/setupcompat/template/FooterButton;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MainClear;->mContentView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->setup_wizard_layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/setupdesign/GlifLayout;

    const-class v1, Lcom/google/android/setupcompat/template/FooterBarMixin;

    invoke-virtual {v0, v1}, Lcom/google/android/setupcompat/internal/TemplateLayout;->getMixin(Ljava/lang/Class;)Lcom/google/android/setupcompat/template/Mixin;

    move-result-object v0

    check-cast v0, Lcom/google/android/setupcompat/template/FooterBarMixin;

    new-instance v1, Lcom/google/android/setupcompat/template/FooterButton$Builder;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/setupcompat/template/FooterButton$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/android/settings/R$string;->main_clear_button_text:I

    invoke-virtual {v1, v2}, Lcom/google/android/setupcompat/template/FooterButton$Builder;->setText(I)Lcom/google/android/setupcompat/template/FooterButton$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/MainClear;->mInitiateListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/google/android/setupcompat/template/FooterButton$Builder;->setListener(Landroid/view/View$OnClickListener;)Lcom/google/android/setupcompat/template/FooterButton$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/setupcompat/template/FooterButton$Builder;->setButtonType(I)Lcom/google/android/setupcompat/template/FooterButton$Builder;

    move-result-object v1

    sget v2, Lcom/android/settings/R$style;->SudGlifButton_Primary:I

    invoke-virtual {v1, v2}, Lcom/google/android/setupcompat/template/FooterButton$Builder;->setTheme(I)Lcom/google/android/setupcompat/template/FooterButton$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/setupcompat/template/FooterButton$Builder;->build()Lcom/google/android/setupcompat/template/FooterButton;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/setupcompat/template/FooterBarMixin;->setPrimaryButton(Lcom/google/android/setupcompat/template/FooterButton;)V

    invoke-virtual {v0}, Lcom/google/android/setupcompat/template/FooterBarMixin;->getPrimaryButton()Lcom/google/android/setupcompat/template/FooterButton;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MainClear;->mInitiateButton:Lcom/google/android/setupcompat/template/FooterButton;

    return-void
.end method


# virtual methods
.method establishInitialState()V
    .locals 6

    goto/32 :goto_43

    nop

    :goto_0
    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto/32 :goto_4e

    nop

    :goto_1
    if-eqz v0, :cond_0

    goto/32 :goto_50

    :cond_0
    goto/32 :goto_1d

    nop

    :goto_2
    sget v1, Lcom/android/settings/R$id;->erase_external_container:I

    goto/32 :goto_41

    nop

    :goto_3
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    goto/32 :goto_3e

    nop

    :goto_4
    const/4 v2, 0x0

    goto/32 :goto_1

    nop

    :goto_5
    iget-object v3, p0, Lcom/android/settings/MainClear;->mExternalStorageContainer:Landroid/view/View;

    goto/32 :goto_25

    nop

    :goto_6
    iget-object v0, p0, Lcom/android/settings/MainClear;->mEsimStorage:Landroid/widget/CheckBox;

    goto/32 :goto_3a

    nop

    :goto_7
    new-instance v1, Lcom/android/settings/MainClear$3;

    goto/32 :goto_27

    nop

    :goto_8
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/32 :goto_11

    nop

    :goto_9
    iput-object v0, p0, Lcom/android/settings/MainClear;->mEsimStorage:Landroid/widget/CheckBox;

    goto/32 :goto_1f

    nop

    :goto_a
    sget v3, Lcom/android/settings/R$id;->no_cancel_mobile_plan:I

    goto/32 :goto_14

    nop

    :goto_b
    xor-int/2addr v0, v1

    goto/32 :goto_69

    nop

    :goto_c
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/32 :goto_2b

    nop

    :goto_d
    sget v4, Lcom/android/settings/R$id;->also_erases_external:I

    goto/32 :goto_42

    nop

    :goto_e
    if-eqz v3, :cond_1

    goto/32 :goto_3d

    :cond_1
    goto/32 :goto_5b

    nop

    :goto_f
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/32 :goto_37

    nop

    :goto_10
    check-cast v0, Landroid/widget/CheckBox;

    goto/32 :goto_23

    nop

    :goto_11
    goto/16 :goto_3b

    :goto_12
    goto/32 :goto_54

    nop

    :goto_13
    sget v3, Lcom/android/settings/R$id;->also_erases_esim:I

    goto/32 :goto_48

    nop

    :goto_14
    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto/32 :goto_51

    nop

    :goto_15
    if-nez v3, :cond_2

    goto/32 :goto_3d

    :cond_2
    goto/32 :goto_3c

    nop

    :goto_16
    sget v1, Lcom/android/settings/R$id;->erase_esim:I

    goto/32 :goto_36

    nop

    :goto_17
    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    goto/32 :goto_67

    nop

    :goto_18
    new-instance v1, Lcom/android/settings/MainClear$4;

    goto/32 :goto_5a

    nop

    :goto_19
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto/32 :goto_44

    nop

    :goto_1a
    iput-object v0, p0, Lcom/android/settings/MainClear;->mExternalStorageContainer:Landroid/view/View;

    goto/32 :goto_68

    nop

    :goto_1b
    if-nez v0, :cond_3

    goto/32 :goto_12

    :cond_3
    goto/32 :goto_31

    nop

    :goto_1c
    iget-object v0, p0, Lcom/android/settings/MainClear;->mEsimStorage:Landroid/widget/CheckBox;

    goto/32 :goto_f

    nop

    :goto_1d
    invoke-static {}, Landroid/os/Environment;->isExternalStorageRemovable()Z

    move-result v3

    goto/32 :goto_e

    nop

    :goto_1e
    if-nez v0, :cond_4

    goto/32 :goto_38

    :cond_4
    goto/32 :goto_59

    nop

    :goto_1f
    iget-object v0, p0, Lcom/android/settings/MainClear;->mScrollView:Landroid/widget/ScrollView;

    goto/32 :goto_5f

    nop

    :goto_20
    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/32 :goto_64

    nop

    :goto_21
    invoke-static {}, Landroid/os/Environment;->isExternalStorageEmulated()Z

    move-result v0

    goto/32 :goto_24

    nop

    :goto_22
    iget-object v0, p0, Lcom/android/settings/MainClear;->mContentView:Landroid/view/View;

    goto/32 :goto_2

    nop

    :goto_23
    iput-object v0, p0, Lcom/android/settings/MainClear;->mExternalStorage:Landroid/widget/CheckBox;

    goto/32 :goto_65

    nop

    :goto_24
    const/4 v1, 0x1

    goto/32 :goto_4

    nop

    :goto_25
    const/16 v4, 0x8

    goto/32 :goto_0

    nop

    :goto_26
    invoke-virtual {v0}, Landroid/widget/ScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    goto/32 :goto_62

    nop

    :goto_27
    invoke-direct {v1, p0}, Lcom/android/settings/MainClear$3;-><init>(Lcom/android/settings/MainClear;)V

    goto/32 :goto_8

    nop

    :goto_28
    iget-object v1, p0, Lcom/android/settings/MainClear;->mContentView:Landroid/view/View;

    goto/32 :goto_2d

    nop

    :goto_29
    iget-object v0, p0, Lcom/android/settings/MainClear;->mContentView:Landroid/view/View;

    goto/32 :goto_53

    nop

    :goto_2a
    new-instance v0, Ljava/lang/StringBuffer;

    goto/32 :goto_61

    nop

    :goto_2b
    iget-object v0, p0, Lcom/android/settings/MainClear;->mEsimStorageContainer:Landroid/view/View;

    goto/32 :goto_7

    nop

    :goto_2c
    invoke-direct {p0}, Lcom/android/settings/MainClear;->setUpInitiateButton()V

    goto/32 :goto_22

    nop

    :goto_2d
    sget v2, Lcom/android/settings/R$id;->main_clear_container:I

    goto/32 :goto_3

    nop

    :goto_2e
    invoke-direct {v3, p0}, Lcom/android/settings/MainClear$2;-><init>(Lcom/android/settings/MainClear;)V

    goto/32 :goto_4d

    nop

    :goto_2f
    check-cast v0, Landroid/os/UserManager;

    goto/32 :goto_40

    nop

    :goto_30
    iget-object v0, p0, Lcom/android/settings/MainClear;->mContentView:Landroid/view/View;

    goto/32 :goto_16

    nop

    :goto_31
    iget-object v0, p0, Lcom/android/settings/MainClear;->mEsimStorageContainer:Landroid/view/View;

    goto/32 :goto_c

    nop

    :goto_32
    iget-object v3, p0, Lcom/android/settings/MainClear;->mExternalStorage:Landroid/widget/CheckBox;

    goto/32 :goto_b

    nop

    :goto_33
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/32 :goto_66

    nop

    :goto_34
    return-void

    :goto_35
    const-string/jumbo v1, "user"

    goto/32 :goto_5e

    nop

    :goto_36
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto/32 :goto_6b

    nop

    :goto_37
    goto :goto_3b

    :goto_38
    goto/32 :goto_6

    nop

    :goto_39
    iput-object v0, p0, Lcom/android/settings/MainClear;->mScrollView:Landroid/widget/ScrollView;

    goto/32 :goto_21

    nop

    :goto_3a
    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    :goto_3b
    goto/32 :goto_52

    nop

    :goto_3c
    goto :goto_50

    :goto_3d
    goto/32 :goto_55

    nop

    :goto_3e
    invoke-direct {p0, v1, v0}, Lcom/android/settings/MainClear;->getContentDescription(Landroid/view/View;Ljava/lang/StringBuffer;)V

    goto/32 :goto_20

    nop

    :goto_3f
    iget-object v0, p0, Lcom/android/settings/MainClear;->mScrollView:Landroid/widget/ScrollView;

    goto/32 :goto_26

    nop

    :goto_40
    invoke-direct {p0, v0}, Lcom/android/settings/MainClear;->loadAccountList(Landroid/os/UserManager;)V

    goto/32 :goto_2a

    nop

    :goto_41
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto/32 :goto_1a

    nop

    :goto_42
    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    goto/32 :goto_56

    nop

    :goto_43
    invoke-direct {p0}, Lcom/android/settings/MainClear;->setUpActionBarAndTitle()V

    goto/32 :goto_2c

    nop

    :goto_44
    check-cast v0, Landroid/widget/ScrollView;

    goto/32 :goto_39

    nop

    :goto_45
    sget v1, Lcom/android/settings/R$id;->erase_esim_container:I

    goto/32 :goto_47

    nop

    :goto_46
    invoke-virtual {v0}, Landroid/widget/ScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    goto/32 :goto_49

    nop

    :goto_47
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto/32 :goto_58

    nop

    :goto_48
    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto/32 :goto_33

    nop

    :goto_49
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :goto_4a
    goto/32 :goto_29

    nop

    :goto_4b
    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setOnScrollChangeListener(Landroid/view/View$OnScrollChangeListener;)V

    goto/32 :goto_3f

    nop

    :goto_4c
    sget v1, Lcom/android/settings/R$id;->erase_external:I

    goto/32 :goto_60

    nop

    :goto_4d
    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/32 :goto_4f

    nop

    :goto_4e
    iget-object v3, p0, Lcom/android/settings/MainClear;->mContentView:Landroid/view/View;

    goto/32 :goto_63

    nop

    :goto_4f
    goto/16 :goto_6a

    :goto_50
    goto/32 :goto_5

    nop

    :goto_51
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/32 :goto_1c

    nop

    :goto_52
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    goto/32 :goto_35

    nop

    :goto_53
    sget v1, Lcom/android/settings/R$id;->main_clear_scrollview:I

    goto/32 :goto_19

    nop

    :goto_54
    iget-object v0, p0, Lcom/android/settings/MainClear;->mContentView:Landroid/view/View;

    goto/32 :goto_13

    nop

    :goto_55
    iget-object v0, p0, Lcom/android/settings/MainClear;->mExternalStorageContainer:Landroid/view/View;

    goto/32 :goto_5c

    nop

    :goto_56
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    goto/32 :goto_32

    nop

    :goto_57
    iget-object v3, p0, Lcom/android/settings/MainClear;->mContentView:Landroid/view/View;

    goto/32 :goto_d

    nop

    :goto_58
    iput-object v0, p0, Lcom/android/settings/MainClear;->mEsimStorageContainer:Landroid/view/View;

    goto/32 :goto_30

    nop

    :goto_59
    invoke-virtual {p0}, Lcom/android/settings/MainClear;->showWipeEuiccCheckbox()Z

    move-result v0

    goto/32 :goto_1b

    nop

    :goto_5a
    invoke-direct {v1, p0}, Lcom/android/settings/MainClear$4;-><init>(Lcom/android/settings/MainClear;)V

    goto/32 :goto_4b

    nop

    :goto_5b
    invoke-direct {p0}, Lcom/android/settings/MainClear;->isExtStorageEncrypted()Z

    move-result v3

    goto/32 :goto_15

    nop

    :goto_5c
    new-instance v3, Lcom/android/settings/MainClear$2;

    goto/32 :goto_2e

    nop

    :goto_5d
    invoke-virtual {p0}, Lcom/android/settings/MainClear;->showWipeEuicc()Z

    move-result v0

    goto/32 :goto_1e

    nop

    :goto_5e
    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_2f

    nop

    :goto_5f
    if-nez v0, :cond_5

    goto/32 :goto_4a

    :cond_5
    goto/32 :goto_46

    nop

    :goto_60
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto/32 :goto_10

    nop

    :goto_61
    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    goto/32 :goto_28

    nop

    :goto_62
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto/32 :goto_34

    nop

    :goto_63
    sget v5, Lcom/android/settings/R$id;->erase_external_option_text:I

    goto/32 :goto_17

    nop

    :goto_64
    iget-object v0, p0, Lcom/android/settings/MainClear;->mScrollView:Landroid/widget/ScrollView;

    goto/32 :goto_18

    nop

    :goto_65
    iget-object v0, p0, Lcom/android/settings/MainClear;->mContentView:Landroid/view/View;

    goto/32 :goto_45

    nop

    :goto_66
    iget-object v0, p0, Lcom/android/settings/MainClear;->mContentView:Landroid/view/View;

    goto/32 :goto_a

    nop

    :goto_67
    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto/32 :goto_57

    nop

    :goto_68
    iget-object v0, p0, Lcom/android/settings/MainClear;->mContentView:Landroid/view/View;

    goto/32 :goto_4c

    nop

    :goto_69
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    :goto_6a
    goto/32 :goto_5d

    nop

    :goto_6b
    check-cast v0, Landroid/widget/CheckBox;

    goto/32 :goto_9

    nop
.end method

.method getAccountConfirmationIntent()Landroid/content/Intent;
    .locals 6

    goto/32 :goto_8

    nop

    :goto_0
    if-nez v3, :cond_0

    goto/32 :goto_38

    :cond_0
    goto/32 :goto_3b

    nop

    :goto_1
    invoke-static {v5, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_31

    nop

    :goto_2
    const-string p0, "Resources not set for account confirmation."

    goto/32 :goto_1

    nop

    :goto_3
    if-eqz v3, :cond_1

    goto/32 :goto_14

    :cond_1
    goto/32 :goto_16

    nop

    :goto_4
    new-instance p0, Ljava/lang/StringBuilder;

    goto/32 :goto_9

    nop

    :goto_5
    if-nez p0, :cond_2

    goto/32 :goto_10

    :cond_2
    goto/32 :goto_f

    nop

    :goto_6
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_12

    nop

    :goto_7
    iget-object p0, p0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    goto/32 :goto_2f

    nop

    :goto_8
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    goto/32 :goto_c

    nop

    :goto_9
    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_3e

    nop

    :goto_a
    new-instance v3, Landroid/content/ComponentName;

    goto/32 :goto_30

    nop

    :goto_b
    sget v2, Lcom/android/settings/R$string;->account_confirmation_class:I

    goto/32 :goto_2d

    nop

    :goto_c
    sget v0, Lcom/android/settings/R$string;->account_type:I

    goto/32 :goto_21

    nop

    :goto_d
    invoke-static {v5, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_e
    goto/32 :goto_13

    nop

    :goto_f
    return-object v0

    :goto_10
    goto/32 :goto_1b

    nop

    :goto_11
    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_18

    nop

    :goto_12
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_3c

    nop

    :goto_13
    return-object v4

    :goto_14
    goto/32 :goto_2

    nop

    :goto_15
    new-instance v0, Landroid/content/Intent;

    goto/32 :goto_3f

    nop

    :goto_16
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    goto/32 :goto_29

    nop

    :goto_17
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_26

    nop

    :goto_18
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_1a

    nop

    :goto_19
    const/4 v4, 0x0

    goto/32 :goto_28

    nop

    :goto_1a
    invoke-static {v5, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_37

    nop

    :goto_1b
    new-instance p0, Ljava/lang/StringBuilder;

    goto/32 :goto_1e

    nop

    :goto_1c
    const-string v0, "Unable to resolve Activity: "

    goto/32 :goto_6

    nop

    :goto_1d
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    goto/32 :goto_1f

    nop

    :goto_1e
    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1c

    nop

    :goto_1f
    invoke-virtual {v3, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    goto/32 :goto_0

    nop

    :goto_20
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_11

    nop

    :goto_21
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_41

    nop

    :goto_22
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    goto/32 :goto_2a

    nop

    :goto_23
    if-nez p0, :cond_3

    goto/32 :goto_10

    :cond_3
    goto/32 :goto_7

    nop

    :goto_24
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto/32 :goto_a

    nop

    :goto_25
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    goto/32 :goto_3d

    nop

    :goto_26
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_d

    nop

    :goto_27
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    goto/32 :goto_19

    nop

    :goto_28
    const-string v5, "MainClear"

    goto/32 :goto_3

    nop

    :goto_29
    if-eqz v3, :cond_4

    goto/32 :goto_14

    :cond_4
    goto/32 :goto_22

    nop

    :goto_2a
    if-nez v3, :cond_5

    goto/32 :goto_34

    :cond_5
    goto/32 :goto_33

    nop

    :goto_2b
    const-string v0, " accounts installed!"

    goto/32 :goto_17

    nop

    :goto_2c
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_2b

    nop

    :goto_2d
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_27

    nop

    :goto_2e
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_b

    nop

    :goto_2f
    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    goto/32 :goto_5

    nop

    :goto_30
    invoke-direct {v3, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_25

    nop

    :goto_31
    return-object v4

    :goto_32
    const/4 v3, 0x0

    goto/32 :goto_36

    nop

    :goto_33
    goto/16 :goto_14

    :goto_34
    goto/32 :goto_1d

    nop

    :goto_35
    if-nez p0, :cond_6

    goto/32 :goto_10

    :cond_6
    goto/32 :goto_3a

    nop

    :goto_36
    invoke-virtual {p0, v0, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object p0

    goto/32 :goto_35

    nop

    :goto_37
    goto/16 :goto_e

    :goto_38
    goto/32 :goto_4

    nop

    :goto_39
    if-gtz v3, :cond_7

    goto/32 :goto_38

    :cond_7
    goto/32 :goto_15

    nop

    :goto_3a
    iget-object p0, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    goto/32 :goto_23

    nop

    :goto_3b
    array-length v3, v3

    goto/32 :goto_39

    nop

    :goto_3c
    const-string v0, "/"

    goto/32 :goto_20

    nop

    :goto_3d
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    goto/32 :goto_32

    nop

    :goto_3e
    const-string v1, "No "

    goto/32 :goto_40

    nop

    :goto_3f
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    goto/32 :goto_24

    nop

    :goto_40
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_2c

    nop

    :goto_41
    sget v1, Lcom/android/settings/R$string;->account_confirmation_package:I

    goto/32 :goto_2e

    nop
.end method

.method public getMetricsCategory()I
    .locals 0

    const/16 p0, 0x42

    return p0
.end method

.method hasReachedBottom(Landroid/widget/ScrollView;)Z
    .locals 3

    goto/32 :goto_4

    nop

    :goto_0
    return v0

    :goto_1
    goto :goto_11

    :goto_2
    goto/32 :goto_10

    nop

    :goto_3
    add-int/2addr v2, p1

    goto/32 :goto_8

    nop

    :goto_4
    invoke-virtual {p1}, Landroid/widget/ScrollView;->getChildCount()I

    move-result p0

    goto/32 :goto_d

    nop

    :goto_5
    invoke-virtual {p1}, Landroid/widget/ScrollView;->getScrollY()I

    move-result p1

    goto/32 :goto_3

    nop

    :goto_6
    const/4 p0, 0x0

    goto/32 :goto_7

    nop

    :goto_7
    invoke-virtual {p1, p0}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_8
    sub-int/2addr v1, v2

    goto/32 :goto_f

    nop

    :goto_9
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    goto/32 :goto_a

    nop

    :goto_a
    invoke-virtual {p1}, Landroid/widget/ScrollView;->getHeight()I

    move-result v2

    goto/32 :goto_5

    nop

    :goto_b
    return v0

    :goto_c
    goto/32 :goto_6

    nop

    :goto_d
    const/4 v0, 0x1

    goto/32 :goto_e

    nop

    :goto_e
    if-lt p0, v0, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_b

    nop

    :goto_f
    if-lez v1, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_1

    nop

    :goto_10
    move v0, p0

    :goto_11
    goto/32 :goto_0

    nop
.end method

.method protected isEuiccEnabled(Landroid/content/Context;)Z
    .locals 0

    const-string p0, "euicc"

    invoke-virtual {p1, p0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/telephony/euicc/EuiccManager;

    invoke-virtual {p0}, Landroid/telephony/euicc/EuiccManager;->isEnabled()Z

    move-result p0

    return p0
.end method

.method isValidRequestCode(I)Z
    .locals 0

    goto/32 :goto_3

    nop

    :goto_0
    const/4 p0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    goto :goto_5

    :goto_2
    goto/32 :goto_4

    nop

    :goto_3
    const/16 p0, 0x37

    goto/32 :goto_b

    nop

    :goto_4
    const/4 p0, 0x1

    :goto_5
    goto/32 :goto_6

    nop

    :goto_6
    return p0

    :goto_7
    goto :goto_2

    :goto_8
    goto/32 :goto_0

    nop

    :goto_9
    const/16 p0, 0x38

    goto/32 :goto_a

    nop

    :goto_a
    if-eq p1, p0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_7

    nop

    :goto_b
    if-ne p1, p0, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_9

    nop
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    invoke-virtual {p0, p1, p2, p3}, Lcom/android/settings/MainClear;->onActivityResultInternal(IILandroid/content/Intent;)V

    return-void
.end method

.method onActivityResultInternal(IILandroid/content/Intent;)V
    .locals 0

    goto/32 :goto_8

    nop

    :goto_0
    if-nez p1, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_7

    nop

    :goto_1
    return-void

    :goto_2
    goto/32 :goto_5

    nop

    :goto_3
    invoke-virtual {p0}, Lcom/android/settings/MainClear;->showFinalConfirmation()V

    :goto_4
    goto/32 :goto_e

    nop

    :goto_5
    const/4 p3, -0x1

    goto/32 :goto_6

    nop

    :goto_6
    if-ne p2, p3, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_a

    nop

    :goto_7
    invoke-virtual {p0, p1}, Lcom/android/settings/MainClear;->showAccountCredentialConfirmation(Landroid/content/Intent;)V

    goto/32 :goto_11

    nop

    :goto_8
    invoke-virtual {p0, p1}, Lcom/android/settings/MainClear;->isValidRequestCode(I)Z

    move-result p3

    goto/32 :goto_b

    nop

    :goto_9
    const/16 p2, 0x38

    goto/32 :goto_d

    nop

    :goto_a
    invoke-virtual {p0}, Lcom/android/settings/MainClear;->establishInitialState()V

    goto/32 :goto_f

    nop

    :goto_b
    if-eqz p3, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_1

    nop

    :goto_c
    invoke-virtual {p0}, Lcom/android/settings/MainClear;->getAccountConfirmationIntent()Landroid/content/Intent;

    move-result-object p1

    goto/32 :goto_0

    nop

    :goto_d
    if-ne p2, p1, :cond_3

    goto/32 :goto_12

    :cond_3
    goto/32 :goto_c

    nop

    :goto_e
    return-void

    :goto_f
    return-void

    :goto_10
    goto/32 :goto_9

    nop

    :goto_11
    goto :goto_4

    :goto_12
    goto/32 :goto_3

    nop
.end method

.method public onGlobalLayout()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/MainClear;->mInitiateButton:Lcom/google/android/setupcompat/template/FooterButton;

    iget-object v1, p0, Lcom/android/settings/MainClear;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {p0, v1}, Lcom/android/settings/MainClear;->hasReachedBottom(Landroid/widget/ScrollView;)Z

    move-result p0

    invoke-virtual {v0, p0}, Lcom/google/android/setupcompat/template/FooterButton;->setEnabled(Z)V

    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result p3

    const-string/jumbo v0, "no_factory_reset"

    invoke-static {p2, v0, p3}, Lcom/android/settingslib/RestrictedLockUtilsInternal;->checkIfRestrictionEnforced(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    move-result-object p3

    invoke-static {p2}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/UserManager;->isAdminUser()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-static {p2, v0, v1}, Lcom/android/settingslib/RestrictedLockUtilsInternal;->hasBaseUserRestriction(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    const/4 v2, 0x0

    if-eqz v1, :cond_2

    invoke-static {p2}, Lcom/android/settings/Utils;->isDemoUser(Landroid/content/Context;)Z

    move-result p2

    if-nez p2, :cond_2

    sget p0, Lcom/android/settings/R$layout;->main_clear_disallowed_screen:I

    invoke-virtual {p1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p0

    return-object p0

    :cond_2
    if-eqz p3, :cond_3

    new-instance p1, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;-><init>(Landroid/app/Activity;)V

    invoke-virtual {p1, v0, p3}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->prepareDialogBuilder(Ljava/lang/String;Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    new-instance p2, Lcom/android/settings/MainClear$$ExternalSyntheticLambda0;

    invoke-direct {p2, p0}, Lcom/android/settings/MainClear$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/MainClear;)V

    invoke-virtual {p1, p2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lmiuix/appcompat/app/AlertDialog$Builder;->show()Lmiuix/appcompat/app/AlertDialog;

    new-instance p1, Landroid/view/View;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {p1, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    return-object p1

    :cond_3
    sget p2, Lcom/android/settings/R$layout;->main_clear:I

    invoke-virtual {p1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/MainClear;->mContentView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/settings/MainClear;->establishInitialState()V

    iget-object p0, p0, Lcom/android/settings/MainClear;->mContentView:Landroid/view/View;

    return-object p0
.end method

.method showAccountCredentialConfirmation(Landroid/content/Intent;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0, p1, v0}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/32 :goto_2

    nop

    :goto_1
    const/16 v0, 0x38

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method showFinalConfirmation()V
    .locals 4

    goto/32 :goto_b

    nop

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    goto/32 :goto_4

    nop

    :goto_1
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/32 :goto_e

    nop

    :goto_2
    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    goto/32 :goto_18

    nop

    :goto_3
    const-string v2, ":settings:show_fragment_title_resid"

    goto/32 :goto_10

    nop

    :goto_4
    sget v0, Lcom/android/settings/R$string;->main_clear_confirm_title:I

    goto/32 :goto_3

    nop

    :goto_5
    iget-object v1, p0, Lcom/android/settings/MainClear;->mExternalStorage:Landroid/widget/CheckBox;

    goto/32 :goto_2

    nop

    :goto_6
    const-string v2, "erase_esim"

    goto/32 :goto_15

    nop

    :goto_7
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/32 :goto_11

    nop

    :goto_8
    const-string v2, ":settings:show_fragment_args"

    goto/32 :goto_0

    nop

    :goto_9
    invoke-virtual {p0}, Lcom/android/settings/MainClear;->getMetricsCategory()I

    move-result v0

    goto/32 :goto_a

    nop

    :goto_a
    const-string v2, ":settings:source_metrics"

    goto/32 :goto_c

    nop

    :goto_b
    new-instance v0, Landroid/os/Bundle;

    goto/32 :goto_1b

    nop

    :goto_c
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/32 :goto_d

    nop

    :goto_d
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    goto/32 :goto_1

    nop

    :goto_e
    return-void

    :goto_f
    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    goto/32 :goto_6

    nop

    :goto_10
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/32 :goto_9

    nop

    :goto_11
    iget-object v1, p0, Lcom/android/settings/MainClear;->mEsimStorage:Landroid/widget/CheckBox;

    goto/32 :goto_f

    nop

    :goto_12
    const-string v3, ":settings:show_fragment"

    goto/32 :goto_14

    nop

    :goto_13
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    goto/32 :goto_16

    nop

    :goto_14
    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/32 :goto_8

    nop

    :goto_15
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/32 :goto_19

    nop

    :goto_16
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    goto/32 :goto_17

    nop

    :goto_17
    const-class v3, Lcom/android/settings/Settings$FactoryResetConfirmActivity;

    goto/32 :goto_1c

    nop

    :goto_18
    const-string v2, "erase_sd"

    goto/32 :goto_7

    nop

    :goto_19
    new-instance v1, Landroid/content/Intent;

    goto/32 :goto_13

    nop

    :goto_1a
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_12

    nop

    :goto_1b
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto/32 :goto_5

    nop

    :goto_1c
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto/32 :goto_1d

    nop

    :goto_1d
    const-class v2, Lcom/android/settings/MainClearConfirm;

    goto/32 :goto_1a

    nop
.end method

.method showWipeEuicc()Z
    .locals 3

    goto/32 :goto_c

    nop

    :goto_0
    if-nez p0, :cond_0

    goto/32 :goto_8

    :cond_0
    :goto_1
    goto/32 :goto_7

    nop

    :goto_2
    return v1

    :goto_3
    goto/32 :goto_6

    nop

    :goto_4
    const/4 v1, 0x0

    goto/32 :goto_9

    nop

    :goto_5
    return v1

    :goto_6
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    goto/32 :goto_e

    nop

    :goto_7
    const/4 v1, 0x1

    :goto_8
    goto/32 :goto_5

    nop

    :goto_9
    if-eqz p0, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_2

    nop

    :goto_a
    invoke-virtual {p0, v0}, Lcom/android/settings/MainClear;->isEuiccEnabled(Landroid/content/Context;)Z

    move-result p0

    goto/32 :goto_4

    nop

    :goto_b
    invoke-static {v0}, Lcom/android/settingslib/development/DevelopmentSettingsEnabler;->isDevelopmentSettingsEnabled(Landroid/content/Context;)Z

    move-result p0

    goto/32 :goto_0

    nop

    :goto_c
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    goto/32 :goto_a

    nop

    :goto_d
    invoke-static {p0, v2, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    goto/32 :goto_f

    nop

    :goto_e
    const-string v2, "euicc_provisioned"

    goto/32 :goto_d

    nop

    :goto_f
    if-eqz p0, :cond_2

    goto/32 :goto_1

    :cond_2
    goto/32 :goto_b

    nop
.end method

.method showWipeEuiccCheckbox()Z
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return p0

    :goto_1
    const-string p0, "masterclear.allow_retain_esim_profiles_after_fdr"

    goto/32 :goto_3

    nop

    :goto_2
    invoke-static {p0, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result p0

    goto/32 :goto_0

    nop

    :goto_3
    const/4 v0, 0x0

    goto/32 :goto_2

    nop
.end method
