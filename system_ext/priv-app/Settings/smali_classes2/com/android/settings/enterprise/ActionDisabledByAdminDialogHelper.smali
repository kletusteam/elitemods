.class public final Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;
.super Ljava/lang/Object;


# instance fields
.field private final mActionDisabledByAdminController:Lcom/android/settingslib/enterprise/ActionDisabledByAdminController;

.field private final mActivity:Landroid/app/Activity;

.field private mDialogView:Landroid/view/ViewGroup;

.field mEnforcedAdmin:Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

.field private mRestriction:Ljava/lang/String;


# direct methods
.method public static synthetic $r8$lambda$7w-ykFNeSpX7Vp1LlgTBKpJfaCM(Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->lambda$new$0()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mActivity:Landroid/app/Activity;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/android/settings/R$layout;->support_details_dialog:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mDialogView:Landroid/view/ViewGroup;

    new-instance v0, Lcom/android/settings/enterprise/DeviceAdminStringProviderImpl;

    invoke-direct {v0, p1}, Lcom/android/settings/enterprise/DeviceAdminStringProviderImpl;-><init>(Landroid/content/Context;)V

    sget-object v1, Landroid/os/UserHandle;->SYSTEM:Landroid/os/UserHandle;

    invoke-static {p1, p2, v0, v1}, Lcom/android/settingslib/enterprise/ActionDisabledByAdminControllerFactory;->createInstance(Landroid/content/Context;Ljava/lang/String;Lcom/android/settingslib/enterprise/DeviceAdminStringProvider;Landroid/os/UserHandle;)Lcom/android/settingslib/enterprise/ActionDisabledByAdminController;

    move-result-object p2

    iput-object p2, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mActionDisabledByAdminController:Lcom/android/settingslib/enterprise/ActionDisabledByAdminController;

    const-class p2, Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {p1, p2}, Landroid/app/Activity;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/admin/DevicePolicyManager;

    iget-object p2, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mDialogView:Landroid/view/ViewGroup;

    sget v0, Lcom/android/settings/R$id;->admin_support_dialog_title:I

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/app/admin/DevicePolicyManager;->getResources()Landroid/app/admin/DevicePolicyResourcesManager;

    move-result-object p1

    new-instance v0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;)V

    const-string p0, "Settings.DISABLED_BY_IT_ADMIN_TITLE"

    invoke-virtual {p1, p0, v0}, Landroid/app/admin/DevicePolicyResourcesManager;->getString(Ljava/lang/String;Ljava/util/function/Supplier;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private getEnforcementAdminUserId()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mEnforcedAdmin:Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    invoke-direct {p0, v0}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->getEnforcementAdminUserId(Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)I

    move-result p0

    return p0
.end method

.method private getEnforcementAdminUserId(Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)I
    .locals 0

    iget-object p0, p1, Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;->user:Landroid/os/UserHandle;

    if-nez p0, :cond_0

    const/16 p0, -0x2710

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/os/UserHandle;->getIdentifier()I

    move-result p0

    :goto_0
    return p0
.end method

.method private initializeDialogViews(Landroid/view/View;Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;ILjava/lang/String;)V
    .locals 2

    iget-object v0, p2, Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;->component:Landroid/content/ComponentName;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mActionDisabledByAdminController:Lcom/android/settingslib/enterprise/ActionDisabledByAdminController;

    invoke-interface {v1, p2, p3}, Lcom/android/settingslib/enterprise/ActionDisabledByAdminController;->updateEnforcedAdmin(Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;I)V

    invoke-virtual {p0, p1, v0, p3}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->setAdminSupportIcon(Landroid/view/View;Landroid/content/ComponentName;I)V

    invoke-direct {p0, v0, p3}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->isNotCurrentUserOrProfile(Landroid/content/ComponentName;I)Z

    move-result p2

    const/4 v1, 0x0

    if-eqz p2, :cond_1

    move-object v0, v1

    :cond_1
    invoke-virtual {p0, p1, p4}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->setAdminSupportTitle(Landroid/view/View;Ljava/lang/String;)V

    const/16 p2, -0x2710

    if-ne p3, p2, :cond_2

    goto :goto_0

    :cond_2
    invoke-static {p3}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object v1

    :goto_0
    iget-object p2, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mActivity:Landroid/app/Activity;

    new-instance p3, Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    invoke-direct {p3, v0, v1}, Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;-><init>(Landroid/content/ComponentName;Landroid/os/UserHandle;)V

    invoke-virtual {p0, p2, p1, p3}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->setAdminSupportDetails(Landroid/app/Activity;Landroid/view/View;Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)V

    return-void
.end method

.method private isNotCurrentUserOrProfile(Landroid/content/ComponentName;I)Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mActivity:Landroid/app/Activity;

    invoke-static {v0, p1}, Lcom/android/settingslib/RestrictedLockUtilsInternal;->isAdminInCurrentUserOrProfile(Landroid/content/Context;Landroid/content/ComponentName;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p0, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mActivity:Landroid/app/Activity;

    invoke-static {p0, p2}, Lcom/android/settingslib/RestrictedLockUtils;->isCurrentUserOrProfile(Landroid/content/Context;I)Z

    move-result p0

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private synthetic lambda$new$0()Ljava/lang/String;
    .locals 1

    iget-object p0, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mActivity:Landroid/app/Activity;

    sget v0, Lcom/android/settings/R$string;->disabled_by_policy_title:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public prepareDialogBuilder(Ljava/lang/String;Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)Lmiuix/appcompat/app/AlertDialog$Builder;
    .locals 4

    new-instance v0, Lmiuix/appcompat/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/android/settings/R$string;->okay:I

    iget-object v2, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mActionDisabledByAdminController:Lcom/android/settingslib/enterprise/ActionDisabledByAdminController;

    iget-object v3, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mActivity:Landroid/app/Activity;

    invoke-interface {v2, v3, p2}, Lcom/android/settingslib/enterprise/ActionDisabledByAdminController;->getPositiveButtonListener(Landroid/content/Context;Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mDialogView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setView(Landroid/view/View;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->prepareDialogBuilder(Lmiuix/appcompat/app/AlertDialog$Builder;Ljava/lang/String;Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)V

    return-object v0
.end method

.method prepareDialogBuilder(Lmiuix/appcompat/app/AlertDialog$Builder;Ljava/lang/String;Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)V
    .locals 3

    goto/32 :goto_7

    nop

    :goto_0
    iget-object v2, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mActivity:Landroid/app/Activity;

    goto/32 :goto_9

    nop

    :goto_1
    invoke-direct {p0}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->getEnforcementAdminUserId()I

    move-result p2

    goto/32 :goto_e

    nop

    :goto_2
    iget-object p0, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mActivity:Landroid/app/Activity;

    goto/32 :goto_d

    nop

    :goto_3
    invoke-direct {p0, p1, p3, p2, v0}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->initializeDialogViews(Landroid/view/View;Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;ILjava/lang/String;)V

    goto/32 :goto_6

    nop

    :goto_4
    iput-object p3, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mEnforcedAdmin:Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    goto/32 :goto_b

    nop

    :goto_5
    return-void

    :goto_6
    iget-object p1, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mActionDisabledByAdminController:Lcom/android/settingslib/enterprise/ActionDisabledByAdminController;

    goto/32 :goto_2

    nop

    :goto_7
    iget-object v0, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mActionDisabledByAdminController:Lcom/android/settingslib/enterprise/ActionDisabledByAdminController;

    goto/32 :goto_c

    nop

    :goto_8
    iget-object p1, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mDialogView:Landroid/view/ViewGroup;

    goto/32 :goto_1

    nop

    :goto_9
    invoke-direct {v1, v2, p1}, Lcom/android/settings/enterprise/ActionDisabledLearnMoreButtonLauncherImpl;-><init>(Landroid/app/Activity;Lmiuix/appcompat/app/AlertDialog$Builder;)V

    goto/32 :goto_a

    nop

    :goto_a
    invoke-interface {v0, v1}, Lcom/android/settingslib/enterprise/ActionDisabledByAdminController;->initialize(Lcom/android/settingslib/enterprise/ActionDisabledLearnMoreButtonLauncher;)V

    goto/32 :goto_4

    nop

    :goto_b
    iput-object p2, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mRestriction:Ljava/lang/String;

    goto/32 :goto_8

    nop

    :goto_c
    new-instance v1, Lcom/android/settings/enterprise/ActionDisabledLearnMoreButtonLauncherImpl;

    goto/32 :goto_0

    nop

    :goto_d
    invoke-interface {p1, p0}, Lcom/android/settingslib/enterprise/ActionDisabledByAdminController;->setupLearnMoreButton(Landroid/content/Context;)V

    goto/32 :goto_5

    nop

    :goto_e
    iget-object v0, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mRestriction:Ljava/lang/String;

    goto/32 :goto_3

    nop
.end method

.method setAdminSupportDetails(Landroid/app/Activity;Landroid/view/View;Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)V
    .locals 3

    goto/32 :goto_28

    nop

    :goto_0
    invoke-static {p1, v1}, Landroid/os/UserHandle;->isSameApp(II)Z

    move-result p1

    goto/32 :goto_18

    nop

    :goto_1
    goto :goto_d

    :goto_2
    goto/32 :goto_29

    nop

    :goto_3
    invoke-virtual {v0, p1, p3}, Landroid/app/admin/DevicePolicyManager;->getShortSupportMessageForUser(Landroid/content/ComponentName;I)Ljava/lang/CharSequence;

    move-result-object v2

    goto/32 :goto_2a

    nop

    :goto_4
    iget-object p0, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mActivity:Landroid/app/Activity;

    goto/32 :goto_21

    nop

    :goto_5
    if-nez v1, :cond_0

    goto/32 :goto_2b

    :cond_0
    goto/32 :goto_17

    nop

    :goto_6
    iget-object p1, p3, Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;->component:Landroid/content/ComponentName;

    goto/32 :goto_8

    nop

    :goto_7
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result p1

    goto/32 :goto_16

    nop

    :goto_8
    invoke-direct {p0, p3}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->getEnforcementAdminUserId(Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)I

    move-result p3

    goto/32 :goto_3

    nop

    :goto_9
    const/4 v2, 0x0

    goto/32 :goto_5

    nop

    :goto_a
    sget p1, Lcom/android/settings/R$id;->admin_support_msg:I

    goto/32 :goto_1f

    nop

    :goto_b
    invoke-static {p1}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object p1

    goto/32 :goto_1b

    nop

    :goto_c
    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_d
    goto/32 :goto_27

    nop

    :goto_e
    iget-object p1, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mActionDisabledByAdminController:Lcom/android/settingslib/enterprise/ActionDisabledByAdminController;

    goto/32 :goto_4

    nop

    :goto_f
    invoke-static {p1, v1}, Lcom/android/settingslib/RestrictedLockUtils;->isCurrentUserOrProfile(Landroid/content/Context;I)Z

    move-result p1

    goto/32 :goto_11

    nop

    :goto_10
    if-nez p0, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_c

    nop

    :goto_11
    if-eqz p1, :cond_2

    goto/32 :goto_1a

    :cond_2
    goto/32 :goto_19

    nop

    :goto_12
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result p1

    goto/32 :goto_b

    nop

    :goto_13
    iget-object v1, p3, Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;->component:Landroid/content/ComponentName;

    goto/32 :goto_1e

    nop

    :goto_14
    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_15

    nop

    :goto_15
    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    goto/32 :goto_13

    nop

    :goto_16
    const/16 v1, 0x3e8

    goto/32 :goto_0

    nop

    :goto_17
    invoke-direct {p0, p3}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->getEnforcementAdminUserId(Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)I

    move-result v1

    goto/32 :goto_f

    nop

    :goto_18
    if-nez p1, :cond_3

    goto/32 :goto_24

    :cond_3
    goto/32 :goto_6

    nop

    :goto_19
    goto :goto_2b

    :goto_1a
    goto/32 :goto_25

    nop

    :goto_1b
    iput-object p1, p3, Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;->user:Landroid/os/UserHandle;

    :goto_1c
    goto/32 :goto_7

    nop

    :goto_1d
    if-eqz p1, :cond_4

    goto/32 :goto_1c

    :cond_4
    goto/32 :goto_12

    nop

    :goto_1e
    invoke-static {p1, v1}, Lcom/android/settingslib/RestrictedLockUtilsInternal;->isAdminInCurrentUserOrProfile(Landroid/content/Context;Landroid/content/ComponentName;)Z

    move-result v1

    goto/32 :goto_9

    nop

    :goto_1f
    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    goto/32 :goto_26

    nop

    :goto_20
    iget-object v0, p3, Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;->component:Landroid/content/ComponentName;

    goto/32 :goto_22

    nop

    :goto_21
    invoke-interface {p1, p0, v2}, Lcom/android/settingslib/enterprise/ActionDisabledByAdminController;->getAdminSupportContentString(Landroid/content/Context;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p0

    goto/32 :goto_a

    nop

    :goto_22
    if-eqz v0, :cond_5

    goto/32 :goto_2

    :cond_5
    goto/32 :goto_1

    nop

    :goto_23
    iput-object v2, p3, Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;->component:Landroid/content/ComponentName;

    :goto_24
    goto/32 :goto_e

    nop

    :goto_25
    iget-object p1, p3, Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;->user:Landroid/os/UserHandle;

    goto/32 :goto_1d

    nop

    :goto_26
    check-cast p1, Landroid/widget/TextView;

    goto/32 :goto_10

    nop

    :goto_27
    return-void

    :goto_28
    if-nez p3, :cond_6

    goto/32 :goto_d

    :cond_6
    goto/32 :goto_20

    nop

    :goto_29
    const-string v0, "device_policy"

    goto/32 :goto_14

    nop

    :goto_2a
    goto :goto_24

    :goto_2b
    goto/32 :goto_23

    nop
.end method

.method setAdminSupportIcon(Landroid/view/View;Landroid/content/ComponentName;I)V
    .locals 0

    goto/32 :goto_2

    nop

    :goto_0
    sget p3, Lcom/android/settings/R$drawable;->ic_lock_closed:I

    goto/32 :goto_6

    nop

    :goto_1
    return-void

    :goto_2
    sget p2, Lcom/android/settings/R$id;->admin_support_icon:I

    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {p1, p2}, Landroid/view/View;->requireViewById(I)Landroid/view/View;

    move-result-object p1

    goto/32 :goto_7

    nop

    :goto_4
    invoke-virtual {p1, p0}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    goto/32 :goto_1

    nop

    :goto_5
    invoke-static {p0}, Lcom/android/settingslib/Utils;->getColorAccent(Landroid/content/Context;)Landroid/content/res/ColorStateList;

    move-result-object p0

    goto/32 :goto_4

    nop

    :goto_6
    invoke-virtual {p2, p3}, Landroid/app/Activity;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    goto/32 :goto_8

    nop

    :goto_7
    check-cast p1, Landroid/widget/ImageView;

    goto/32 :goto_a

    nop

    :goto_8
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/32 :goto_9

    nop

    :goto_9
    iget-object p0, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mActivity:Landroid/app/Activity;

    goto/32 :goto_5

    nop

    :goto_a
    iget-object p2, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mActivity:Landroid/app/Activity;

    goto/32 :goto_0

    nop
.end method

.method setAdminSupportTitle(Landroid/view/View;Ljava/lang/String;)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    sget v0, Lcom/android/settings/R$id;->admin_support_dialog_title:I

    goto/32 :goto_3

    nop

    :goto_1
    iget-object p0, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mActionDisabledByAdminController:Lcom/android/settingslib/enterprise/ActionDisabledByAdminController;

    goto/32 :goto_9

    nop

    :goto_2
    if-eqz p1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_6

    nop

    :goto_3
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    goto/32 :goto_5

    nop

    :goto_4
    return-void

    :goto_5
    check-cast p1, Landroid/widget/TextView;

    goto/32 :goto_2

    nop

    :goto_6
    return-void

    :goto_7
    goto/32 :goto_1

    nop

    :goto_8
    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_4

    nop

    :goto_9
    invoke-interface {p0, p2}, Lcom/android/settingslib/enterprise/ActionDisabledByAdminController;->getAdminSupportTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_8

    nop
.end method

.method public updateDialog(Ljava/lang/String;Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mEnforcedAdmin:Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    invoke-virtual {v0, p2}, Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mRestriction:Ljava/lang/String;

    invoke-static {v0, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iput-object p2, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mEnforcedAdmin:Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    iput-object p1, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mRestriction:Ljava/lang/String;

    iget-object p1, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mDialogView:Landroid/view/ViewGroup;

    invoke-direct {p0}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->getEnforcementAdminUserId()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->mRestriction:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->initializeDialogViews(Landroid/view/View;Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;ILjava/lang/String;)V

    return-void
.end method
