.class public Lcom/android/settings/enterprise/ActionDisabledByAdminDialog;
.super Lmiuix/appcompat/app/AppCompatActivity;

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field private mDialogHelper:Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiuix/appcompat/app/AppCompatActivity;-><init>()V

    return-void
.end method


# virtual methods
.method getAdminDetailsFromIntent(Landroid/content/Intent;)Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;
    .locals 6

    goto/32 :goto_21

    nop

    :goto_0
    iput-object v3, v0, Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;->component:Landroid/content/ComponentName;

    goto/32 :goto_31

    nop

    :goto_1
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    goto/32 :goto_f

    nop

    :goto_2
    const-string v1, "android.intent.extra.USER"

    goto/32 :goto_2e

    nop

    :goto_3
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p0

    goto/32 :goto_32

    nop

    :goto_4
    goto :goto_14

    :goto_5
    goto/32 :goto_30

    nop

    :goto_6
    const-string v4, "android.intent.extra.USER_ID"

    goto/32 :goto_1f

    nop

    :goto_7
    if-eq v3, p0, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_17

    nop

    :goto_8
    iput-object p0, v0, Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;->user:Landroid/os/UserHandle;

    goto/32 :goto_4

    nop

    :goto_9
    goto :goto_14

    :goto_a
    goto/32 :goto_28

    nop

    :goto_b
    invoke-virtual {v5, v3, p0}, Landroid/app/admin/DevicePolicyManager;->getEnforcingAdminAndUserDetails(ILjava/lang/String;)Landroid/os/Bundle;

    move-result-object p0

    goto/32 :goto_19

    nop

    :goto_c
    goto :goto_1b

    :goto_d
    goto/32 :goto_1a

    nop

    :goto_e
    if-nez v5, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_3

    nop

    :goto_f
    check-cast v3, Landroid/content/ComponentName;

    goto/32 :goto_0

    nop

    :goto_10
    iput-object v1, v0, Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;->component:Landroid/content/ComponentName;

    goto/32 :goto_c

    nop

    :goto_11
    invoke-virtual {p0, p1}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialog;->getRestrictionFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_b

    nop

    :goto_12
    check-cast v5, Landroid/app/admin/DevicePolicyManager;

    goto/32 :goto_11

    nop

    :goto_13
    iput-object p0, v0, Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;->user:Landroid/os/UserHandle;

    :goto_14
    goto/32 :goto_18

    nop

    :goto_15
    iget-object v5, v0, Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;->component:Landroid/content/ComponentName;

    goto/32 :goto_22

    nop

    :goto_16
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result p1

    goto/32 :goto_23

    nop

    :goto_17
    iput-object v2, v0, Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;->user:Landroid/os/UserHandle;

    goto/32 :goto_9

    nop

    :goto_18
    return-object v0

    :goto_19
    if-nez p0, :cond_2

    goto/32 :goto_1b

    :cond_2
    goto/32 :goto_2f

    nop

    :goto_1a
    move-object p0, v2

    :goto_1b
    goto/32 :goto_2

    nop

    :goto_1c
    const-string v1, "android.app.extra.DEVICE_ADMIN"

    goto/32 :goto_1

    nop

    :goto_1d
    const-class v5, Landroid/app/admin/DevicePolicyManager;

    goto/32 :goto_27

    nop

    :goto_1e
    const/4 v2, 0x0

    goto/32 :goto_2b

    nop

    :goto_1f
    invoke-virtual {p1, v4, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    goto/32 :goto_15

    nop

    :goto_20
    const/16 p0, -0x2710

    goto/32 :goto_7

    nop

    :goto_21
    new-instance v0, Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    goto/32 :goto_2a

    nop

    :goto_22
    if-eqz v5, :cond_3

    goto/32 :goto_d

    :cond_3
    goto/32 :goto_1d

    nop

    :goto_23
    invoke-virtual {p0, v4, p1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    :goto_24
    goto/32 :goto_20

    nop

    :goto_25
    if-eqz p1, :cond_4

    goto/32 :goto_2d

    :cond_4
    goto/32 :goto_2c

    nop

    :goto_26
    check-cast v1, Landroid/content/ComponentName;

    goto/32 :goto_10

    nop

    :goto_27
    invoke-virtual {p0, v5}, Landroid/app/Activity;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    goto/32 :goto_12

    nop

    :goto_28
    invoke-static {v3}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object p0

    goto/32 :goto_13

    nop

    :goto_29
    invoke-static {v1}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object v1

    goto/32 :goto_1e

    nop

    :goto_2a
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    goto/32 :goto_29

    nop

    :goto_2b
    invoke-direct {v0, v2, v1}, Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;-><init>(Landroid/content/ComponentName;Landroid/os/UserHandle;)V

    goto/32 :goto_25

    nop

    :goto_2c
    return-object v0

    :goto_2d
    goto/32 :goto_1c

    nop

    :goto_2e
    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    goto/32 :goto_e

    nop

    :goto_2f
    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    goto/32 :goto_26

    nop

    :goto_30
    if-nez p0, :cond_5

    goto/32 :goto_24

    :cond_5
    goto/32 :goto_16

    nop

    :goto_31
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    goto/32 :goto_6

    nop

    :goto_32
    check-cast p0, Landroid/os/UserHandle;

    goto/32 :goto_8

    nop
.end method

.method getRestrictionFromIntent(Landroid/content/Intent;)Ljava/lang/String;
    .locals 0

    goto/32 :goto_3

    nop

    :goto_0
    const/4 p0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    return-object p0

    :goto_2
    goto/32 :goto_6

    nop

    :goto_3
    if-eqz p1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_0

    nop

    :goto_4
    invoke-virtual {p1, p0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_5

    nop

    :goto_5
    return-object p0

    :goto_6
    const-string p0, "android.app.extra.RESTRICTION"

    goto/32 :goto_4

    nop
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialog;->getAdminDetailsFromIntent(Landroid/content/Intent;)Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    move-result-object p1

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialog;->getRestrictionFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;

    invoke-direct {v1, p0, v0}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialog;->mDialogHelper:Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;

    invoke-virtual {v1, v0, p1}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->prepareDialogBuilder(Ljava/lang/String;Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    invoke-virtual {p1, p0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lmiuix/appcompat/app/AlertDialog$Builder;->show()Lmiuix/appcompat/app/AlertDialog;

    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 0

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/FragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p0, p1}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialog;->getAdminDetailsFromIntent(Landroid/content/Intent;)Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialog;->getRestrictionFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object p1

    iget-object p0, p0, Lcom/android/settings/enterprise/ActionDisabledByAdminDialog;->mDialogHelper:Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/enterprise/ActionDisabledByAdminDialogHelper;->updateDialog(Ljava/lang/String;Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)V

    return-void
.end method
