.class public Lcom/android/settings/AppWidgetLoader;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/AppWidgetLoader$ItemConstructor;,
        Lcom/android/settings/AppWidgetLoader$LabelledItem;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Item::",
        "Lcom/android/settings/AppWidgetLoader$LabelledItem;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

.field private mContext:Landroid/content/Context;

.field mItemConstructor:Lcom/android/settings/AppWidgetLoader$ItemConstructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/settings/AppWidgetLoader$ItemConstructor<",
            "TItem;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;Lcom/android/settings/AppWidgetLoader$ItemConstructor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/appwidget/AppWidgetManager;",
            "Lcom/android/settings/AppWidgetLoader$ItemConstructor<",
            "TItem;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/AppWidgetLoader;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/AppWidgetLoader;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    iput-object p3, p0, Lcom/android/settings/AppWidgetLoader;->mItemConstructor:Lcom/android/settings/AppWidgetLoader$ItemConstructor;

    return-void
.end method


# virtual methods
.method protected getItems(Landroid/content/Intent;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List<",
            "TItem;>;"
        }
    .end annotation

    const-string v0, "customSort"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const-string v3, "categoryFilter"

    invoke-virtual {p1, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {p0, v2, v1}, Lcom/android/settings/AppWidgetLoader;->putInstalledAppWidgets(Ljava/util/List;I)V

    if-eqz v0, :cond_0

    invoke-virtual {p0, v2, p1}, Lcom/android/settings/AppWidgetLoader;->putCustomAppWidgets(Ljava/util/List;Landroid/content/Intent;)V

    :cond_0
    new-instance v1, Lcom/android/settings/AppWidgetLoader$1;

    invoke-direct {v1, p0}, Lcom/android/settings/AppWidgetLoader$1;-><init>(Lcom/android/settings/AppWidgetLoader;)V

    invoke-static {v2, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, v0, p1}, Lcom/android/settings/AppWidgetLoader;->putCustomAppWidgets(Ljava/util/List;Landroid/content/Intent;)V

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_1
    return-object v2
.end method

.method putAppWidgetItems(Ljava/util/List;Ljava/util/List;Ljava/util/List;IZ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/appwidget/AppWidgetProviderInfo;",
            ">;",
            "Ljava/util/List<",
            "Landroid/os/Bundle;",
            ">;",
            "Ljava/util/List<",
            "TItem;>;IZ)V"
        }
    .end annotation

    goto/32 :goto_14

    nop

    :goto_0
    const/4 v5, 0x0

    :goto_1
    goto/32 :goto_1b

    nop

    :goto_2
    if-nez p2, :cond_0

    goto/32 :goto_16

    :cond_0
    goto/32 :goto_1f

    nop

    :goto_3
    iget v3, v2, Landroid/appwidget/AppWidgetProviderInfo;->widgetCategory:I

    goto/32 :goto_9

    nop

    :goto_4
    if-eqz v3, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_10

    nop

    :goto_5
    iget-object v3, p0, Lcom/android/settings/AppWidgetLoader;->mItemConstructor:Lcom/android/settings/AppWidgetLoader$ItemConstructor;

    goto/32 :goto_1c

    nop

    :goto_6
    return-void

    :goto_7
    check-cast v2, Landroid/appwidget/AppWidgetProviderInfo;

    goto/32 :goto_18

    nop

    :goto_8
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    goto/32 :goto_1d

    nop

    :goto_9
    and-int/2addr v3, p4

    goto/32 :goto_4

    nop

    :goto_a
    check-cast v5, Landroid/os/Bundle;

    goto/32 :goto_15

    nop

    :goto_b
    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_c
    goto/32 :goto_12

    nop

    :goto_d
    check-cast v2, Lcom/android/settings/AppWidgetLoader$LabelledItem;

    goto/32 :goto_b

    nop

    :goto_e
    goto :goto_1e

    :goto_f
    goto/32 :goto_6

    nop

    :goto_10
    goto :goto_c

    :goto_11
    goto/32 :goto_5

    nop

    :goto_12
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_e

    nop

    :goto_13
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_7

    nop

    :goto_14
    if-eqz p1, :cond_2

    goto/32 :goto_1a

    :cond_2
    goto/32 :goto_19

    nop

    :goto_15
    goto :goto_1

    :goto_16
    goto/32 :goto_0

    nop

    :goto_17
    if-lt v1, v0, :cond_3

    goto/32 :goto_f

    :cond_3
    goto/32 :goto_13

    nop

    :goto_18
    if-eqz p5, :cond_4

    goto/32 :goto_11

    :cond_4
    goto/32 :goto_3

    nop

    :goto_19
    return-void

    :goto_1a
    goto/32 :goto_8

    nop

    :goto_1b
    invoke-interface {v3, v4, v2, v5}, Lcom/android/settings/AppWidgetLoader$ItemConstructor;->createItem(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_d

    nop

    :goto_1c
    iget-object v4, p0, Lcom/android/settings/AppWidgetLoader;->mContext:Landroid/content/Context;

    goto/32 :goto_2

    nop

    :goto_1d
    const/4 v1, 0x0

    :goto_1e
    goto/32 :goto_17

    nop

    :goto_1f
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    goto/32 :goto_a

    nop
.end method

.method putCustomAppWidgets(Ljava/util/List;Landroid/content/Intent;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "TItem;>;",
            "Landroid/content/Intent;",
            ")V"
        }
    .end annotation

    goto/32 :goto_22

    nop

    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    goto/32 :goto_51

    nop

    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_9

    nop

    :goto_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    goto/32 :goto_4d

    nop

    :goto_3
    check-cast v3, Landroid/os/Parcelable;

    goto/32 :goto_31

    nop

    :goto_4
    new-instance p2, Ljava/lang/StringBuilder;

    goto/32 :goto_3e

    nop

    :goto_5
    goto/16 :goto_29

    :goto_6
    goto/32 :goto_39

    nop

    :goto_7
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    goto/32 :goto_21

    nop

    :goto_8
    invoke-static {v1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_23

    nop

    :goto_9
    goto/16 :goto_2c

    :goto_a
    goto/32 :goto_4

    nop

    :goto_b
    goto/16 :goto_5d

    :goto_c
    goto/32 :goto_2a

    nop

    :goto_d
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_5b

    nop

    :goto_e
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_46

    nop

    :goto_f
    instance-of v3, v3, Landroid/os/Bundle;

    goto/32 :goto_35

    nop

    :goto_10
    const-string v0, "error using EXTRA_CUSTOM_INFO index="

    goto/32 :goto_45

    nop

    :goto_11
    return-void

    :goto_12
    move-object v4, v2

    goto/32 :goto_1c

    nop

    :goto_13
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_5a

    nop

    :goto_14
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_48

    nop

    :goto_15
    if-nez v6, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_2f

    nop

    :goto_16
    invoke-virtual {p2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_1f

    nop

    :goto_17
    move-object v4, v2

    goto/32 :goto_19

    nop

    :goto_18
    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_4f

    nop

    :goto_19
    goto/16 :goto_5d

    :goto_1a
    goto/32 :goto_54

    nop

    :goto_1b
    if-lt v4, v5, :cond_1

    goto/32 :goto_2e

    :cond_1
    goto/32 :goto_37

    nop

    :goto_1c
    move-object v5, v4

    goto/32 :goto_b

    nop

    :goto_1d
    goto/16 :goto_4c

    :goto_1e
    goto/32 :goto_2

    nop

    :goto_1f
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_3f

    nop

    :goto_20
    const/4 v8, 0x1

    goto/32 :goto_3a

    nop

    :goto_21
    check-cast v6, Landroid/os/Parcelable;

    goto/32 :goto_15

    nop

    :goto_22
    const-string v0, "customInfo"

    goto/32 :goto_24

    nop

    :goto_23
    move-object v4, v0

    goto/32 :goto_5c

    nop

    :goto_24
    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    goto/32 :goto_55

    nop

    :goto_25
    goto/16 :goto_a

    :goto_26
    goto/32 :goto_1

    nop

    :goto_27
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_52

    nop

    :goto_28
    goto/16 :goto_43

    :goto_29
    goto/32 :goto_36

    nop

    :goto_2a
    const-string v5, "customExtras"

    goto/32 :goto_47

    nop

    :goto_2b
    move v5, v4

    :goto_2c
    goto/32 :goto_4e

    nop

    :goto_2d
    goto :goto_40

    :goto_2e
    goto/32 :goto_50

    nop

    :goto_2f
    instance-of v6, v6, Landroid/appwidget/AppWidgetProviderInfo;

    goto/32 :goto_58

    nop

    :goto_30
    if-nez v0, :cond_2

    goto/32 :goto_4c

    :cond_2
    goto/32 :goto_0

    nop

    :goto_31
    if-nez v3, :cond_3

    goto/32 :goto_29

    :cond_3
    goto/32 :goto_f

    nop

    :goto_32
    invoke-virtual/range {v3 .. v8}, Lcom/android/settings/AppWidgetLoader;->putAppWidgetItems(Ljava/util/List;Ljava/util/List;Ljava/util/List;IZ)V

    goto/32 :goto_11

    nop

    :goto_33
    const-string v0, "EXTRA_CUSTOM_INFO without EXTRA_CUSTOM_EXTRAS"

    goto/32 :goto_e

    nop

    :goto_34
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_3d

    nop

    :goto_35
    if-eqz v3, :cond_4

    goto/32 :goto_6

    :cond_4
    goto/32 :goto_5

    nop

    :goto_36
    new-instance p2, Ljava/lang/StringBuilder;

    goto/32 :goto_34

    nop

    :goto_37
    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_3

    nop

    :goto_38
    const-string p2, "EXTRA_CUSTOM_INFO not present."

    goto/32 :goto_8

    nop

    :goto_39
    add-int/lit8 v4, v4, 0x1

    goto/32 :goto_28

    nop

    :goto_3a
    move-object v3, p0

    goto/32 :goto_3c

    nop

    :goto_3b
    move-object v4, v0

    goto/32 :goto_4b

    nop

    :goto_3c
    move-object v6, p1

    goto/32 :goto_32

    nop

    :goto_3d
    const-string v0, "error using EXTRA_CUSTOM_EXTRAS index="

    goto/32 :goto_27

    nop

    :goto_3e
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_10

    nop

    :goto_3f
    invoke-static {v1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_40
    goto/32 :goto_12

    nop

    :goto_41
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_56

    nop

    :goto_42
    goto :goto_40

    :goto_43
    goto/32 :goto_1b

    nop

    :goto_44
    if-eqz p2, :cond_5

    goto/32 :goto_1a

    :cond_5
    goto/32 :goto_33

    nop

    :goto_45
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_16

    nop

    :goto_46
    move-object v5, p2

    goto/32 :goto_17

    nop

    :goto_47
    invoke-virtual {p2, v5}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p2

    goto/32 :goto_44

    nop

    :goto_48
    const-string v0, "list size mismatch: EXTRA_CUSTOM_INFO: "

    goto/32 :goto_59

    nop

    :goto_49
    new-instance p2, Ljava/lang/StringBuilder;

    goto/32 :goto_14

    nop

    :goto_4a
    const/4 v2, 0x0

    goto/32 :goto_30

    nop

    :goto_4b
    goto :goto_5d

    :goto_4c
    goto/32 :goto_38

    nop

    :goto_4d
    const/4 v4, 0x0

    goto/32 :goto_2b

    nop

    :goto_4e
    if-lt v5, v3, :cond_6

    goto/32 :goto_c

    :cond_6
    goto/32 :goto_7

    nop

    :goto_4f
    const-string v0, " EXTRA_CUSTOM_EXTRAS: "

    goto/32 :goto_d

    nop

    :goto_50
    move-object v5, p2

    goto/32 :goto_3b

    nop

    :goto_51
    if-eqz v3, :cond_7

    goto/32 :goto_1e

    :cond_7
    goto/32 :goto_1d

    nop

    :goto_52
    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_13

    nop

    :goto_53
    const/4 v7, 0x0

    goto/32 :goto_20

    nop

    :goto_54
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v5

    goto/32 :goto_57

    nop

    :goto_55
    const-string v1, "AppWidgetAdapter"

    goto/32 :goto_4a

    nop

    :goto_56
    invoke-static {v1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_42

    nop

    :goto_57
    if-ne v3, v5, :cond_8

    goto/32 :goto_43

    :cond_8
    goto/32 :goto_49

    nop

    :goto_58
    if-eqz v6, :cond_9

    goto/32 :goto_26

    :cond_9
    goto/32 :goto_25

    nop

    :goto_59
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_18

    nop

    :goto_5a
    invoke-static {v1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_2d

    nop

    :goto_5b
    invoke-virtual {p2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_41

    nop

    :goto_5c
    move-object v5, v2

    :goto_5d
    goto/32 :goto_53

    nop
.end method

.method putInstalledAppWidgets(Ljava/util/List;I)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "TItem;>;I)V"
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    const/4 v6, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    move-object v1, p0

    goto/32 :goto_5

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/settings/AppWidgetLoader;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    goto/32 :goto_4

    nop

    :goto_3
    const/4 v3, 0x0

    goto/32 :goto_0

    nop

    :goto_4
    invoke-virtual {v0, p2}, Landroid/appwidget/AppWidgetManager;->getInstalledProviders(I)Ljava/util/List;

    move-result-object v2

    goto/32 :goto_3

    nop

    :goto_5
    move-object v4, p1

    goto/32 :goto_8

    nop

    :goto_6
    return-void

    :goto_7
    invoke-virtual/range {v1 .. v6}, Lcom/android/settings/AppWidgetLoader;->putAppWidgetItems(Ljava/util/List;Ljava/util/List;Ljava/util/List;IZ)V

    goto/32 :goto_6

    nop

    :goto_8
    move v5, p2

    goto/32 :goto_7

    nop
.end method
