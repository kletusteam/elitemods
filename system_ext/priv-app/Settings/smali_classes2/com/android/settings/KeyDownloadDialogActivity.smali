.class public Lcom/android/settings/KeyDownloadDialogActivity;
.super Landroid/app/Activity;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private initView()V
    .locals 10

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_1
    const-string/jumbo v1, "packageName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "title"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v2, "content"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v2, "shortcut"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "launch_noapp_dialog_install"

    invoke-static {v3, v2}, Lcom/android/settings/MiuiShortcut$Key;->getResourceForKey(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "launch_noapp_dialog_replace"

    invoke-static {v3, v2}, Lcom/android/settings/MiuiShortcut$Key;->getResourceForKey(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    new-instance v2, Lcom/android/settings/KeyDownloadDialog;

    new-instance v9, Lcom/android/settings/KeyDownloadDialogActivity$1;

    invoke-direct {v9, p0, v0, v1}, Lcom/android/settings/KeyDownloadDialogActivity$1;-><init>(Lcom/android/settings/KeyDownloadDialogActivity;Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v2

    move-object v4, p0

    invoke-direct/range {v3 .. v9}, Lcom/android/settings/KeyDownloadDialog;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/settings/KeyDownloadDialog$IOnClickListener;)V

    invoke-virtual {v2}, Lcom/android/settings/KeyDownloadDialog;->show()V

    return-void
.end method


# virtual methods
.method gotoBackTapGestureSettings()V
    .locals 3

    goto/32 :goto_c

    nop

    :goto_0
    const-string v1, ":android:show_fragment"

    goto/32 :goto_7

    nop

    :goto_1
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/32 :goto_12

    nop

    :goto_2
    const/4 v2, 0x0

    goto/32 :goto_b

    nop

    :goto_3
    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto/32 :goto_d

    nop

    :goto_4
    sget-object v1, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    goto/32 :goto_3

    nop

    :goto_5
    const-string v1, ":android:show_fragment_short_title"

    goto/32 :goto_13

    nop

    :goto_6
    const-string v2, "com.android.settings.Settings"

    goto/32 :goto_f

    nop

    :goto_7
    const-string v2, "com.android.settings.BackTapSettingsFragment"

    goto/32 :goto_11

    nop

    :goto_8
    const-string v1, ":android:no_headers"

    goto/32 :goto_a

    nop

    :goto_9
    const-string v1, "com.android.settings"

    goto/32 :goto_6

    nop

    :goto_a
    const/4 v2, 0x1

    goto/32 :goto_1

    nop

    :goto_b
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/32 :goto_5

    nop

    :goto_c
    new-instance v0, Landroid/content/Intent;

    goto/32 :goto_14

    nop

    :goto_d
    return-void

    :goto_e
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto/32 :goto_4

    nop

    :goto_f
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/32 :goto_0

    nop

    :goto_10
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto/32 :goto_9

    nop

    :goto_11
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/32 :goto_15

    nop

    :goto_12
    const/high16 v1, 0x10000000

    goto/32 :goto_e

    nop

    :goto_13
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/32 :goto_8

    nop

    :goto_14
    const-string v1, "android.intent.action.MAIN"

    goto/32 :goto_10

    nop

    :goto_15
    const-string v1, ":android:show_fragment_title"

    goto/32 :goto_2

    nop
.end method

.method gotoFingerPrintTapSettings()V
    .locals 3

    goto/32 :goto_d

    nop

    :goto_0
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/32 :goto_4

    nop

    :goto_1
    const-string v1, ":android:show_fragment"

    goto/32 :goto_11

    nop

    :goto_2
    const-string v1, "android.intent.action.MAIN"

    goto/32 :goto_15

    nop

    :goto_3
    sget-object v1, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    goto/32 :goto_e

    nop

    :goto_4
    const-string v1, ":android:no_headers"

    goto/32 :goto_12

    nop

    :goto_5
    const-string v1, "com.android.settings"

    goto/32 :goto_a

    nop

    :goto_6
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/32 :goto_1

    nop

    :goto_7
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/32 :goto_c

    nop

    :goto_8
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto/32 :goto_3

    nop

    :goto_9
    const/high16 v1, 0x10000000

    goto/32 :goto_8

    nop

    :goto_a
    const-string v2, "com.android.settings.Settings"

    goto/32 :goto_6

    nop

    :goto_b
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/32 :goto_9

    nop

    :goto_c
    const-string v1, ":android:show_fragment_short_title"

    goto/32 :goto_0

    nop

    :goto_d
    new-instance v0, Landroid/content/Intent;

    goto/32 :goto_2

    nop

    :goto_e
    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto/32 :goto_10

    nop

    :goto_f
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/32 :goto_14

    nop

    :goto_10
    return-void

    :goto_11
    const-string v2, "com.android.settings.GestureShortcutSettingsFragment"

    goto/32 :goto_f

    nop

    :goto_12
    const/4 v2, 0x1

    goto/32 :goto_b

    nop

    :goto_13
    const/4 v2, 0x0

    goto/32 :goto_7

    nop

    :goto_14
    const-string v1, ":android:show_fragment_title"

    goto/32 :goto_13

    nop

    :goto_15
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto/32 :goto_5

    nop
.end method

.method gotoKnockGestureVSetting()V
    .locals 3

    goto/32 :goto_12

    nop

    :goto_0
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/32 :goto_d

    nop

    :goto_1
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto/32 :goto_3

    nop

    :goto_2
    const-string v2, "com.android.settings.Settings"

    goto/32 :goto_b

    nop

    :goto_3
    const-string v1, "com.android.settings"

    goto/32 :goto_2

    nop

    :goto_4
    const-string v1, "android.intent.action.MAIN"

    goto/32 :goto_1

    nop

    :goto_5
    const-string v1, ":android:show_fragment_title"

    goto/32 :goto_11

    nop

    :goto_6
    const-string v2, "com.android.settings.knock.KnockGestureVSelectFragment"

    goto/32 :goto_e

    nop

    :goto_7
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/32 :goto_10

    nop

    :goto_8
    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto/32 :goto_c

    nop

    :goto_9
    const-string v1, ":android:show_fragment"

    goto/32 :goto_6

    nop

    :goto_a
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto/32 :goto_13

    nop

    :goto_b
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/32 :goto_9

    nop

    :goto_c
    return-void

    :goto_d
    const-string v1, ":android:no_headers"

    goto/32 :goto_14

    nop

    :goto_e
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/32 :goto_5

    nop

    :goto_f
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/32 :goto_15

    nop

    :goto_10
    const/high16 v1, 0x10000000

    goto/32 :goto_a

    nop

    :goto_11
    const/4 v2, 0x0

    goto/32 :goto_f

    nop

    :goto_12
    new-instance v0, Landroid/content/Intent;

    goto/32 :goto_4

    nop

    :goto_13
    sget-object v1, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    goto/32 :goto_8

    nop

    :goto_14
    const/4 v2, 0x1

    goto/32 :goto_7

    nop

    :goto_15
    const-string v1, ":android:show_fragment_short_title"

    goto/32 :goto_0

    nop
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/android/settings/KeyDownloadDialogActivity;->initView()V

    return-void
.end method

.method public openInMarket(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "market://details?id=%s&ref=knock_gesture&back=true"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 p1, 0x10000000

    invoke-virtual {v0, p1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    const-string p1, "KeyDownloadDialogActivity"

    const-string v0, "Exception"

    invoke-static {p1, v0, p0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method
