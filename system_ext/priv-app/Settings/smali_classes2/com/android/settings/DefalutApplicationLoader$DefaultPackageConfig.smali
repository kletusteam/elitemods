.class final enum Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/DefalutApplicationLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "DefaultPackageConfig"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

.field public static final enum BROWSER_ARRAY:Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

.field public static final enum CAMERA_ARRAY:Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

.field public static final enum DIALER_ARRAY:Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

.field public static final enum EMAIL_ARRAY:Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

.field public static final enum GALLERY_ARRAY:Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

.field public static final enum MMS_ARRAY:Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

.field public static final enum MUSIC_ARRAY:Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

.field public static final enum VIDEO_ARRAY:Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;


# instance fields
.field public final arrayID:I

.field public final intentFlag:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    new-instance v0, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

    sget v1, Lcom/android/settings/R$array;->default_mms_package:I

    const-string v2, "MMS_ARRAY"

    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-direct {v0, v2, v3, v1, v4}, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;->MMS_ARRAY:Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

    new-instance v1, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

    sget v2, Lcom/android/settings/R$array;->default_dialer_package:I

    const-string v5, "DIALER_ARRAY"

    const/4 v6, 0x1

    invoke-direct {v1, v5, v6, v2, v4}, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;->DIALER_ARRAY:Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

    new-instance v2, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

    sget v5, Lcom/android/settings/R$array;->defalut_browser_package:I

    const-string v7, "BROWSER_ARRAY"

    const/4 v8, 0x2

    invoke-direct {v2, v7, v8, v5, v4}, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;-><init>(Ljava/lang/String;III)V

    sput-object v2, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;->BROWSER_ARRAY:Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

    new-instance v4, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

    sget v5, Lcom/android/settings/R$array;->default_gallery_package:I

    const-string v7, "GALLERY_ARRAY"

    const/4 v9, 0x3

    const/4 v10, 0x5

    invoke-direct {v4, v7, v9, v5, v10}, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;-><init>(Ljava/lang/String;III)V

    sput-object v4, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;->GALLERY_ARRAY:Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

    new-instance v5, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

    sget v7, Lcom/android/settings/R$array;->default_camera_package:I

    const-string v11, "CAMERA_ARRAY"

    const/4 v12, 0x4

    invoke-direct {v5, v11, v12, v7, v12}, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;-><init>(Ljava/lang/String;III)V

    sput-object v5, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;->CAMERA_ARRAY:Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

    new-instance v7, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

    sget v11, Lcom/android/settings/R$array;->default_music_package:I

    const-string v13, "MUSIC_ARRAY"

    const/4 v14, 0x6

    invoke-direct {v7, v13, v10, v11, v14}, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;-><init>(Ljava/lang/String;III)V

    sput-object v7, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;->MUSIC_ARRAY:Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

    new-instance v11, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

    sget v13, Lcom/android/settings/R$array;->default_email_package:I

    const-string v15, "EMAIL_ARRAY"

    const/4 v10, 0x7

    invoke-direct {v11, v15, v14, v13, v10}, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;-><init>(Ljava/lang/String;III)V

    sput-object v11, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;->EMAIL_ARRAY:Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

    new-instance v13, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

    sget v15, Lcom/android/settings/R$array;->default_video_package:I

    const-string v14, "VIDEO_ARRAY"

    const/16 v12, 0x8

    invoke-direct {v13, v14, v10, v15, v12}, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;-><init>(Ljava/lang/String;III)V

    sput-object v13, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;->VIDEO_ARRAY:Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

    new-array v12, v12, [Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

    aput-object v0, v12, v3

    aput-object v1, v12, v6

    aput-object v2, v12, v8

    aput-object v4, v12, v9

    const/4 v0, 0x4

    aput-object v5, v12, v0

    const/4 v0, 0x5

    aput-object v7, v12, v0

    const/4 v0, 0x6

    aput-object v11, v12, v0

    aput-object v13, v12, v10

    sput-object v12, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;->$VALUES:[Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;->arrayID:I

    iput p4, p0, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;->intentFlag:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;
    .locals 1

    const-class v0, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

    return-object p0
.end method

.method public static values()[Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;
    .locals 1

    sget-object v0, Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;->$VALUES:[Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

    invoke-virtual {v0}, [Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/settings/DefalutApplicationLoader$DefaultPackageConfig;

    return-object v0
.end method
