.class public Lcom/android/settings/accounts/MiuiProviderPreference;
.super Lcom/android/settingslib/miuisettings/preference/Preference;


# instance fields
.field private mAccountType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/android/settingslib/miuisettings/preference/Preference;-><init>(Landroid/content/Context;Z)V

    iput-object p2, p0, Lcom/android/settings/accounts/MiuiProviderPreference;->mAccountType:Ljava/lang/String;

    invoke-virtual {p0, p3}, Landroidx/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setPersistent(Z)V

    invoke-virtual {p0, p4}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public getAccountType()Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/accounts/MiuiProviderPreference;->mAccountType:Ljava/lang/String;

    return-object p0
.end method
