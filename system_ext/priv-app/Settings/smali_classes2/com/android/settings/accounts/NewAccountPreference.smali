.class Lcom/android/settings/accounts/NewAccountPreference;
.super Lcom/android/settingslib/miuisettings/preference/Preference;

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceClickListener;
.implements Lmiuix/preference/FolmeAnimationController;
.implements Landroid/content/SyncStatusObserver;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x5
.end annotation


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mAuthority:Ljava/lang/String;

.field private final mFragment:Ljava/lang/String;

.field private final mFragmentArguments:Landroid/os/Bundle;

.field private mHandler:Landroid/os/Handler;

.field private mIsActive:Z

.field private mIsPending:Z

.field public final mTitle:Ljava/lang/CharSequence;

.field private final mTitleResId:I

.field private final mTitleResPackageName:Ljava/lang/String;

.field private mUpdateUIRunable:Ljava/lang/Runnable;

.field private mUserHandle:Landroid/os/UserHandle;

.field private final miuiAccountSettings:Lcom/android/settings/accounts/MiuiAccountSettings;

.field private objectHandle:Ljava/lang/Object;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAccount(Lcom/android/settings/accounts/NewAccountPreference;)Landroid/accounts/Account;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/accounts/NewAccountPreference;->mAccount:Landroid/accounts/Account;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmAuthority(Lcom/android/settings/accounts/NewAccountPreference;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/accounts/NewAccountPreference;->mAuthority:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFragment(Lcom/android/settings/accounts/NewAccountPreference;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/accounts/NewAccountPreference;->mFragment:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFragmentArguments(Lcom/android/settings/accounts/NewAccountPreference;)Landroid/os/Bundle;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/accounts/NewAccountPreference;->mFragmentArguments:Landroid/os/Bundle;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsActive(Lcom/android/settings/accounts/NewAccountPreference;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/accounts/NewAccountPreference;->mIsActive:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsPending(Lcom/android/settings/accounts/NewAccountPreference;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/accounts/NewAccountPreference;->mIsPending:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmTitleResPackageName(Lcom/android/settings/accounts/NewAccountPreference;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/accounts/NewAccountPreference;->mTitleResPackageName:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmUserHandle(Lcom/android/settings/accounts/NewAccountPreference;)Landroid/os/UserHandle;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/accounts/NewAccountPreference;->mUserHandle:Landroid/os/UserHandle;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmiuiAccountSettings(Lcom/android/settings/accounts/NewAccountPreference;)Lcom/android/settings/accounts/MiuiAccountSettings;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/accounts/NewAccountPreference;->miuiAccountSettings:Lcom/android/settings/accounts/MiuiAccountSettings;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmAccount(Lcom/android/settings/accounts/NewAccountPreference;Landroid/accounts/Account;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/accounts/NewAccountPreference;->mAccount:Landroid/accounts/Account;

    return-void
.end method

.method public constructor <init>(Lcom/android/settings/accounts/MiuiAccountSettings;Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/String;ILjava/lang/String;Landroid/os/Bundle;Landroid/graphics/drawable/Drawable;)V
    .locals 3

    invoke-direct {p0, p2}, Lcom/android/settingslib/miuisettings/preference/Preference;-><init>(Landroid/content/Context;)V

    const/4 p2, 0x0

    iput-object p2, p0, Lcom/android/settings/accounts/NewAccountPreference;->objectHandle:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/accounts/NewAccountPreference;->mIsActive:Z

    iput-boolean v0, p0, Lcom/android/settings/accounts/NewAccountPreference;->mIsPending:Z

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/settings/accounts/NewAccountPreference;->mHandler:Landroid/os/Handler;

    iput-object p2, p0, Lcom/android/settings/accounts/NewAccountPreference;->mUpdateUIRunable:Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/android/settings/accounts/NewAccountPreference;->miuiAccountSettings:Lcom/android/settings/accounts/MiuiAccountSettings;

    iput-object p3, p0, Lcom/android/settings/accounts/NewAccountPreference;->mTitle:Ljava/lang/CharSequence;

    iput-object p4, p0, Lcom/android/settings/accounts/NewAccountPreference;->mTitleResPackageName:Ljava/lang/String;

    iput p5, p0, Lcom/android/settings/accounts/NewAccountPreference;->mTitleResId:I

    iput-object p6, p0, Lcom/android/settings/accounts/NewAccountPreference;->mFragment:Ljava/lang/String;

    iput-object p7, p0, Lcom/android/settings/accounts/NewAccountPreference;->mFragmentArguments:Landroid/os/Bundle;

    const-string p1, "account"

    invoke-virtual {p7, p1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Landroid/accounts/Account;

    iput-object p1, p0, Lcom/android/settings/accounts/NewAccountPreference;->mAccount:Landroid/accounts/Account;

    const-string p1, "android.intent.extra.USER"

    invoke-virtual {p7, p1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Landroid/os/UserHandle;

    iput-object p1, p0, Lcom/android/settings/accounts/NewAccountPreference;->mUserHandle:Landroid/os/UserHandle;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/os/UserHandle;->getIdentifier()I

    move-result p1

    invoke-static {p1}, Landroid/content/ContentResolver;->getSyncAdapterTypesAsUser(I)[Landroid/content/SyncAdapterType;

    move-result-object p1

    :goto_0
    array-length p2, p1

    if-ge v0, p2, :cond_2

    iget-object p2, p0, Lcom/android/settings/accounts/NewAccountPreference;->mAccount:Landroid/accounts/Account;

    if-nez p2, :cond_0

    goto :goto_1

    :cond_0
    aget-object p4, p1, v0

    iget-object p5, p4, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    iget-object p2, p2, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {p5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_1

    goto :goto_1

    :cond_1
    iget-object p2, p4, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/settings/accounts/NewAccountPreference;->mAuthority:Ljava/lang/String;

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    sget p1, Lcom/android/settings/R$layout;->preference_system_app_new:I

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setLayoutResource(I)V

    invoke-virtual {p0, p3}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, p8}, Landroidx/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    return-void
.end method

.method private setIsActive(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/accounts/NewAccountPreference;->mIsActive:Z

    return-void
.end method

.method private setIsPending(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/accounts/NewAccountPreference;->mIsPending:Z

    return-void
.end method

.method private updateEndUI()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/accounts/NewAccountPreference;->mUpdateUIRunable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/accounts/NewAccountPreference;->mHandler:Landroid/os/Handler;

    invoke-virtual {p0, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public isSyncing(Ljava/util/List;Landroid/accounts/Account;Ljava/lang/String;)Z
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x8
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/content/SyncInfo;",
            ">;",
            "Landroid/accounts/Account;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/SyncInfo;

    iget-object v0, p1, Landroid/content/SyncInfo;->account:Landroid/accounts/Account;

    invoke-virtual {v0, p2}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p1, Landroid/content/SyncInfo;->authority:Ljava/lang/String;

    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_1
    const/4 p0, 0x0

    return p0
.end method

.method public isTouchAnimationEnable()Z
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method public onAttached()V
    .locals 1

    invoke-super {p0}, Lcom/android/settingslib/miuisettings/preference/Preference;->onAttached()V

    iget-object v0, p0, Lcom/android/settings/accounts/NewAccountPreference;->objectHandle:Ljava/lang/Object;

    if-nez v0, :cond_0

    const/16 v0, 0xd

    invoke-static {v0, p0}, Landroid/content/ContentResolver;->addStatusChangeListener(ILandroid/content/SyncStatusObserver;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/accounts/NewAccountPreference;->objectHandle:Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
    .locals 7

    invoke-super {p0, p1}, Lcom/android/settingslib/miuisettings/preference/Preference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V

    const/4 v0, 0x1

    new-array v1, v0, [Landroid/view/View;

    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v1}, Lmiuix/animation/Folme;->clean([Ljava/lang/Object;)V

    iget-object v1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->head:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    sget v4, Lcom/android/settings/R$id;->end:I

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-object v4, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    sget v5, Lcom/android/settings/R$id;->value_right:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    sget v5, Lcom/android/settings/R$id;->sync_img:I

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setSelected(Z)V

    iget-object v5, p0, Lcom/android/settings/accounts/NewAccountPreference;->mUpdateUIRunable:Ljava/lang/Runnable;

    if-nez v5, :cond_0

    if-eqz p1, :cond_0

    if-eqz v2, :cond_0

    new-instance v5, Lcom/android/settings/accounts/NewAccountPreference$1;

    invoke-direct {v5, p0, v4, v2, p1}, Lcom/android/settings/accounts/NewAccountPreference$1;-><init>(Lcom/android/settings/accounts/NewAccountPreference;Landroid/widget/TextView;Landroid/view/View;Landroid/widget/ImageView;)V

    iput-object v5, p0, Lcom/android/settings/accounts/NewAccountPreference;->mUpdateUIRunable:Ljava/lang/Runnable;

    :cond_0
    const/high16 p1, 0x3f800000    # 1.0f

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/android/settings/usagestats/utils/CommonUtils;->isRtl()Z

    move-result v4

    if-eqz v4, :cond_1

    sget v4, Lcom/android/settings/R$drawable;->preference_card_head_bg_rtl:I

    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    :cond_1
    sget v4, Lcom/android/settings/R$drawable;->preference_card_head_bg:I

    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundResource(I)V

    :goto_0
    new-array v4, v0, [Landroid/view/View;

    aput-object v1, v4, v3

    invoke-static {v4}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v4

    invoke-interface {v4}, Lmiuix/animation/IFolme;->touch()Lmiuix/animation/ITouchStyle;

    move-result-object v4

    new-array v5, v0, [Lmiuix/animation/ITouchStyle$TouchType;

    sget-object v6, Lmiuix/animation/ITouchStyle$TouchType;->DOWN:Lmiuix/animation/ITouchStyle$TouchType;

    aput-object v6, v5, v3

    invoke-interface {v4, p1, v5}, Lmiuix/animation/ITouchStyle;->setScale(F[Lmiuix/animation/ITouchStyle$TouchType;)Lmiuix/animation/ITouchStyle;

    move-result-object v4

    new-array v5, v3, [Lmiuix/animation/base/AnimConfig;

    invoke-interface {v4, v1, v5}, Lmiuix/animation/ITouchStyle;->handleTouchOf(Landroid/view/View;[Lmiuix/animation/base/AnimConfig;)V

    new-instance v4, Lcom/android/settings/accounts/NewAccountPreference$2;

    invoke-direct {v4, p0}, Lcom/android/settings/accounts/NewAccountPreference$2;-><init>(Lcom/android/settings/accounts/NewAccountPreference;)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    if-eqz v2, :cond_4

    invoke-static {}, Lcom/android/settings/usagestats/utils/CommonUtils;->isRtl()Z

    move-result v1

    if-eqz v1, :cond_3

    sget v1, Lcom/android/settings/R$drawable;->preference_card_end_bg_rtl:I

    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1

    :cond_3
    sget v1, Lcom/android/settings/R$drawable;->preference_card_end_bg:I

    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundResource(I)V

    :goto_1
    new-array v1, v0, [Landroid/view/View;

    aput-object v2, v1, v3

    invoke-static {v1}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/IFolme;->touch()Lmiuix/animation/ITouchStyle;

    move-result-object v1

    new-array v0, v0, [Lmiuix/animation/ITouchStyle$TouchType;

    sget-object v4, Lmiuix/animation/ITouchStyle$TouchType;->DOWN:Lmiuix/animation/ITouchStyle$TouchType;

    aput-object v4, v0, v3

    invoke-interface {v1, p1, v0}, Lmiuix/animation/ITouchStyle;->setScale(F[Lmiuix/animation/ITouchStyle$TouchType;)Lmiuix/animation/ITouchStyle;

    move-result-object p1

    new-array v0, v3, [Lmiuix/animation/base/AnimConfig;

    invoke-interface {p1, v2, v0}, Lmiuix/animation/ITouchStyle;->handleTouchOf(Landroid/view/View;[Lmiuix/animation/base/AnimConfig;)V

    new-instance p1, Lcom/android/settings/accounts/NewAccountPreference$3;

    invoke-direct {p1, p0}, Lcom/android/settings/accounts/NewAccountPreference$3;-><init>(Lcom/android/settings/accounts/NewAccountPreference;)V

    invoke-virtual {v2, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_4
    return-void
.end method

.method public onDetached()V
    .locals 1

    invoke-super {p0}, Lcom/android/settingslib/miuisettings/preference/Preference;->onDetached()V

    iget-object v0, p0, Lcom/android/settings/accounts/NewAccountPreference;->objectHandle:Ljava/lang/Object;

    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/content/ContentResolver;->removeStatusChangeListener(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/accounts/NewAccountPreference;->objectHandle:Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public onPreferenceClick(Landroidx/preference/Preference;)Z
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 0

    invoke-super {p0}, Landroidx/preference/Preference;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object p0

    return-object p0
.end method

.method public onStatusChanged(I)V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/accounts/NewAccountPreference;->onSyncStateUpdate()V

    return-void
.end method

.method public onSyncStateUpdate()V
    .locals 8

    iget-object v0, p0, Lcom/android/settings/accounts/NewAccountPreference;->mUserHandle:Landroid/os/UserHandle;

    invoke-virtual {v0}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/accounts/NewAccountPreference;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/android/settings/accounts/NewAccountPreference;->mAuthority:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/content/ContentResolver;->getSyncStatusAsUser(Landroid/accounts/Account;Ljava/lang/String;I)Landroid/content/SyncStatusInfo;

    move-result-object v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move v3, v2

    goto :goto_0

    :cond_0
    iget-boolean v3, v1, Landroid/content/SyncStatusInfo;->pending:Z

    :goto_0
    if-nez v1, :cond_1

    move v4, v2

    goto :goto_1

    :cond_1
    iget-boolean v4, v1, Landroid/content/SyncStatusInfo;->initialize:Z

    :goto_1
    invoke-static {v0}, Landroid/content/ContentResolver;->getCurrentSyncsAsUser(I)Ljava/util/List;

    move-result-object v5

    iget-object v6, p0, Lcom/android/settings/accounts/NewAccountPreference;->mAccount:Landroid/accounts/Account;

    iget-object v7, p0, Lcom/android/settings/accounts/NewAccountPreference;->mAuthority:Ljava/lang/String;

    invoke-virtual {p0, v5, v6, v7}, Lcom/android/settings/accounts/NewAccountPreference;->isSyncing(Ljava/util/List;Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v5

    iget-object v6, p0, Lcom/android/settings/accounts/NewAccountPreference;->mAccount:Landroid/accounts/Account;

    iget-object v7, p0, Lcom/android/settings/accounts/NewAccountPreference;->mAuthority:Ljava/lang/String;

    invoke-static {v6, v7, v0}, Landroid/content/ContentResolver;->getIsSyncableAsUser(Landroid/accounts/Account;Ljava/lang/String;I)I

    move-result v0

    const/4 v6, 0x1

    if-eqz v5, :cond_2

    if-ltz v0, :cond_2

    if-nez v4, :cond_2

    move v5, v6

    goto :goto_2

    :cond_2
    move v5, v2

    :goto_2
    invoke-direct {p0, v5}, Lcom/android/settings/accounts/NewAccountPreference;->setIsActive(Z)V

    if-eqz v3, :cond_3

    if-ltz v0, :cond_3

    if-nez v4, :cond_3

    move v0, v6

    goto :goto_3

    :cond_3
    move v0, v2

    :goto_3
    invoke-direct {p0, v0}, Lcom/android/settings/accounts/NewAccountPreference;->setIsPending(Z)V

    invoke-static {}, Landroid/content/ContentResolver;->getCurrentSyncs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v6

    if-nez v0, :cond_4

    invoke-direct {p0, v2}, Lcom/android/settings/accounts/NewAccountPreference;->setIsActive(Z)V

    invoke-direct {p0, v2}, Lcom/android/settings/accounts/NewAccountPreference;->setIsPending(Z)V

    :cond_4
    invoke-direct {p0}, Lcom/android/settings/accounts/NewAccountPreference;->updateEndUI()V

    iget-object v0, p0, Lcom/android/settings/accounts/NewAccountPreference;->miuiAccountSettings:Lcom/android/settings/accounts/MiuiAccountSettings;

    iget-object v0, v0, Lcom/android/settings/accounts/MiuiAccountSettings;->mPackageTimeMap:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v2, p0, Lcom/android/settings/accounts/NewAccountPreference;->mAuthority:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/accounts/NewAccountPreference;->miuiAccountSettings:Lcom/android/settings/accounts/MiuiAccountSettings;

    iget-object v0, v0, Lcom/android/settings/accounts/MiuiAccountSettings;->mPackageTimeMap:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object p0, p0, Lcom/android/settings/accounts/NewAccountPreference;->mAuthority:Ljava/lang/String;

    iget-wide v1, v1, Landroid/content/SyncStatusInfo;->lastSuccessTime:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/android/settings/accounts/NewAccountPreference;->miuiAccountSettings:Lcom/android/settings/accounts/MiuiAccountSettings;

    iget-object v0, v0, Lcom/android/settings/accounts/MiuiAccountSettings;->mPackageTimeMap:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v2, p0, Lcom/android/settings/accounts/NewAccountPreference;->mAuthority:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-wide v0, v1, Landroid/content/SyncStatusInfo;->lastSuccessTime:J

    cmp-long v2, v2, v0

    if-gez v2, :cond_6

    iget-object v2, p0, Lcom/android/settings/accounts/NewAccountPreference;->miuiAccountSettings:Lcom/android/settings/accounts/MiuiAccountSettings;

    iget-object v2, v2, Lcom/android/settings/accounts/MiuiAccountSettings;->mPackageTimeMap:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object p0, p0, Lcom/android/settings/accounts/NewAccountPreference;->mAuthority:Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, p0, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    :goto_4
    return-void
.end method
