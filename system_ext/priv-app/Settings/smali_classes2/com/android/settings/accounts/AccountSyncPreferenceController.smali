.class public Lcom/android/settings/accounts/AccountSyncPreferenceController;
.super Lcom/android/settingslib/core/AbstractPreferenceController;

# interfaces
.implements Lcom/android/settings/core/PreferenceControllerMixin;
.implements Lcom/android/settingslib/accounts/AuthenticatorHelper$OnAccountsUpdateListener;


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mPreference:Landroidx/preference/Preference;

.field private mUserHandle:Landroid/os/UserHandle;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settingslib/core/AbstractPreferenceController;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public displayPreference(Landroidx/preference/PreferenceScreen;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settingslib/core/AbstractPreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    invoke-virtual {p0}, Lcom/android/settings/accounts/AccountSyncPreferenceController;->getPreferenceKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/accounts/AccountSyncPreferenceController;->mPreference:Landroidx/preference/Preference;

    return-void
.end method

.method public getPreferenceKey()Ljava/lang/String;
    .locals 0

    const-string p0, "account_sync"

    return-object p0
.end method

.method public handlePreferenceTreeClick(Landroidx/preference/Preference;)Z
    .locals 2

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    const-string v0, "account_sync"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    iget-object v0, p0, Lcom/android/settings/accounts/AccountSyncPreferenceController;->mAccount:Landroid/accounts/Account;

    const-string v1, "account"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/android/settings/accounts/AccountSyncPreferenceController;->mUserHandle:Landroid/os/UserHandle;

    const-string v1, "android.intent.extra.USER"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    new-instance v0, Lcom/android/settings/core/SubSettingLauncher;

    iget-object p0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0}, Lcom/android/settings/core/SubSettingLauncher;-><init>(Landroid/content/Context;)V

    const-class p0, Lcom/android/settings/accounts/AccountSyncSettings;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/android/settings/core/SubSettingLauncher;->setDestination(Ljava/lang/String;)Lcom/android/settings/core/SubSettingLauncher;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/android/settings/core/SubSettingLauncher;->setArguments(Landroid/os/Bundle;)Lcom/android/settings/core/SubSettingLauncher;

    move-result-object p0

    const/16 p1, 0x8

    invoke-virtual {p0, p1}, Lcom/android/settings/core/SubSettingLauncher;->setSourceMetricsCategory(I)Lcom/android/settings/core/SubSettingLauncher;

    move-result-object p0

    sget p1, Lcom/android/settings/R$string;->account_sync_title:I

    invoke-virtual {p0, p1}, Lcom/android/settings/core/SubSettingLauncher;->setTitleRes(I)Lcom/android/settings/core/SubSettingLauncher;

    move-result-object p0

    invoke-virtual {p0}, Lcom/android/settings/core/SubSettingLauncher;->launch()V

    const/4 p0, 0x1

    return p0
.end method

.method public init(Landroid/accounts/Account;Landroid/os/UserHandle;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/accounts/AccountSyncPreferenceController;->mAccount:Landroid/accounts/Account;

    iput-object p2, p0, Lcom/android/settings/accounts/AccountSyncPreferenceController;->mUserHandle:Landroid/os/UserHandle;

    return-void
.end method

.method public isAvailable()Z
    .locals 0

    const/4 p0, 0x1

    return p0
.end method

.method public onAccountsUpdate(Landroid/os/UserHandle;)V
    .locals 0

    iget-object p1, p0, Lcom/android/settings/accounts/AccountSyncPreferenceController;->mPreference:Landroidx/preference/Preference;

    invoke-virtual {p0, p1}, Lcom/android/settings/accounts/AccountSyncPreferenceController;->updateSummary(Landroidx/preference/Preference;)V

    return-void
.end method

.method public updateState(Landroidx/preference/Preference;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/settings/accounts/AccountSyncPreferenceController;->updateSummary(Landroidx/preference/Preference;)V

    return-void
.end method

.method updateSummary(Landroidx/preference/Preference;)V
    .locals 11

    goto/32 :goto_6

    nop

    :goto_0
    invoke-static {v9, v8, v0}, Landroid/content/ContentResolver;->getSyncAutomaticallyAsUser(Landroid/accounts/Account;Ljava/lang/String;I)Z

    move-result v8

    goto/32 :goto_1d

    nop

    :goto_1
    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setSummary(I)V

    goto/32 :goto_13

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/settings/accounts/AccountSyncPreferenceController;->mUserHandle:Landroid/os/UserHandle;

    goto/32 :goto_b

    nop

    :goto_3
    iget-object v9, p0, Lcom/android/settings/accounts/AccountSyncPreferenceController;->mAccount:Landroid/accounts/Account;

    goto/32 :goto_38

    nop

    :goto_4
    if-eqz v7, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_12

    nop

    :goto_5
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_c

    nop

    :goto_6
    iget-object v0, p0, Lcom/android/settings/accounts/AccountSyncPreferenceController;->mAccount:Landroid/accounts/Account;

    goto/32 :goto_1e

    nop

    :goto_7
    invoke-virtual {v8}, Landroid/content/SyncAdapterType;->isUserVisible()Z

    move-result v9

    goto/32 :goto_2e

    nop

    :goto_8
    invoke-static {v0}, Landroid/content/ContentResolver;->getSyncAdapterTypesAsUser(I)[Landroid/content/SyncAdapterType;

    move-result-object v1

    goto/32 :goto_10

    nop

    :goto_9
    move v6, v2

    goto/32 :goto_18

    nop

    :goto_a
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    goto/32 :goto_11

    nop

    :goto_b
    invoke-virtual {v0}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v0

    goto/32 :goto_8

    nop

    :goto_c
    goto/16 :goto_3e

    :goto_d
    goto/32 :goto_9

    nop

    :goto_e
    aput-object v4, v1, v2

    goto/32 :goto_24

    nop

    :goto_f
    sget p0, Lcom/android/settings/R$string;->account_sync_summary_all_on:I

    goto/32 :goto_35

    nop

    :goto_10
    const/4 v2, 0x0

    goto/32 :goto_34

    nop

    :goto_11
    if-nez v9, :cond_1

    goto/32 :goto_43

    :cond_1
    goto/32 :goto_7

    nop

    :goto_12
    sget p0, Lcom/android/settings/R$string;->account_sync_summary_all_off:I

    goto/32 :goto_1

    nop

    :goto_13
    goto :goto_27

    :goto_14
    goto/32 :goto_2f

    nop

    :goto_15
    goto/16 :goto_43

    :goto_16
    goto/32 :goto_3

    nop

    :goto_17
    iget-object v10, v10, Landroid/accounts/Account;->type:Ljava/lang/String;

    goto/32 :goto_a

    nop

    :goto_18
    move v7, v6

    :goto_19
    goto/32 :goto_4

    nop

    :goto_1a
    add-int/lit8 v6, v6, 0x1

    goto/32 :goto_23

    nop

    :goto_1b
    aget-object v8, v1, v5

    goto/32 :goto_29

    nop

    :goto_1c
    if-eqz v9, :cond_2

    goto/32 :goto_3a

    :cond_2
    goto/32 :goto_39

    nop

    :goto_1d
    invoke-static {v0}, Landroid/content/ContentResolver;->getMasterSyncAutomaticallyAsUser(I)Z

    move-result v9

    goto/32 :goto_32

    nop

    :goto_1e
    if-eqz v0, :cond_3

    goto/32 :goto_20

    :cond_3
    goto/32 :goto_1f

    nop

    :goto_1f
    return-void

    :goto_20
    goto/32 :goto_2

    nop

    :goto_21
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_26

    nop

    :goto_22
    iget-object v8, v8, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    goto/32 :goto_0

    nop

    :goto_23
    iget-object v9, p0, Lcom/android/settings/accounts/AccountSyncPreferenceController;->mAccount:Landroid/accounts/Account;

    goto/32 :goto_22

    nop

    :goto_24
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/32 :goto_3f

    nop

    :goto_25
    sget v0, Lcom/android/settings/R$string;->account_sync_summary_some_on:I

    goto/32 :goto_2d

    nop

    :goto_26
    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_27
    goto/32 :goto_31

    nop

    :goto_28
    invoke-static {v9, v10, v0}, Landroid/content/ContentResolver;->getIsSyncableAsUser(Landroid/accounts/Account;Ljava/lang/String;I)I

    move-result v9

    goto/32 :goto_37

    nop

    :goto_29
    iget-object v9, v8, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    goto/32 :goto_33

    nop

    :goto_2a
    if-nez v1, :cond_4

    goto/32 :goto_d

    :cond_4
    goto/32 :goto_2b

    nop

    :goto_2b
    array-length v4, v1

    goto/32 :goto_36

    nop

    :goto_2c
    move v6, v5

    goto/32 :goto_3d

    nop

    :goto_2d
    const/4 v1, 0x2

    goto/32 :goto_44

    nop

    :goto_2e
    if-eqz v9, :cond_5

    goto/32 :goto_16

    :cond_5
    goto/32 :goto_15

    nop

    :goto_2f
    if-eq v7, v6, :cond_6

    goto/32 :goto_41

    :cond_6
    goto/32 :goto_f

    nop

    :goto_30
    if-lt v5, v4, :cond_7

    goto/32 :goto_19

    :cond_7
    goto/32 :goto_1b

    nop

    :goto_31
    return-void

    :goto_32
    xor-int/2addr v9, v3

    goto/32 :goto_1c

    nop

    :goto_33
    iget-object v10, p0, Lcom/android/settings/accounts/AccountSyncPreferenceController;->mAccount:Landroid/accounts/Account;

    goto/32 :goto_17

    nop

    :goto_34
    const/4 v3, 0x1

    goto/32 :goto_2a

    nop

    :goto_35
    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setSummary(I)V

    goto/32 :goto_40

    nop

    :goto_36
    move v5, v2

    goto/32 :goto_2c

    nop

    :goto_37
    if-gtz v9, :cond_8

    goto/32 :goto_43

    :cond_8
    goto/32 :goto_1a

    nop

    :goto_38
    iget-object v10, v8, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    goto/32 :goto_28

    nop

    :goto_39
    if-nez v8, :cond_9

    goto/32 :goto_43

    :cond_9
    :goto_3a
    goto/32 :goto_42

    nop

    :goto_3b
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto/32 :goto_e

    nop

    :goto_3c
    iget-object p0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    goto/32 :goto_25

    nop

    :goto_3d
    move v7, v6

    :goto_3e
    goto/32 :goto_30

    nop

    :goto_3f
    aput-object v2, v1, v3

    goto/32 :goto_21

    nop

    :goto_40
    goto/16 :goto_27

    :goto_41
    goto/32 :goto_3c

    nop

    :goto_42
    add-int/lit8 v7, v7, 0x1

    :goto_43
    goto/32 :goto_5

    nop

    :goto_44
    new-array v1, v1, [Ljava/lang/Object;

    goto/32 :goto_3b

    nop
.end method
