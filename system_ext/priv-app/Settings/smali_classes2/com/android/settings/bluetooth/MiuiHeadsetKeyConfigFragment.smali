.class public final Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;


# instance fields
.field private PRESS_KEY_INIT:Ljava/lang/String;

.field private final mBluetoothA2dpReceiver:Landroid/content/BroadcastReceiver;

.field private mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

.field private mDevice:Landroid/bluetooth/BluetoothDevice;

.field private mDeviceConnected:Z

.field private mDeviceId:Ljava/lang/String;

.field private mDoubleClickLeft:Lmiuix/preference/DropDownPreference;

.field private mDoubleClickRight:Lmiuix/preference/DropDownPreference;

.field public mDropdownLeftKey:I

.field private mDropdownPrefLeft:Lmiuix/preference/DropDownPreference;

.field private mDropdownPrefRight:Lmiuix/preference/DropDownPreference;

.field public mDropdownRightKey:I

.field private mHeadSetAct:Lcom/android/settings/bluetooth/MiuiHeadsetActivity;

.field public mLeftDoubleKey:I

.field public mLeftKey:Z

.field public mLeftTripleKey:I

.field private final mPrefChangeListener:Landroidx/preference/Preference$OnPreferenceChangeListener;

.field public mRightDoubleKey:I

.field public mRightKey:Z

.field public mRightTripleKey:I

.field private mRootView:Landroid/view/View;

.field private mService:Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;

.field private mSupport:Ljava/lang/String;

.field private mTripleClickLeft:Lmiuix/preference/DropDownPreference;

.field private mTripleClickRight:Lmiuix/preference/DropDownPreference;

.field private pref_left:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

.field private pref_right:Lcom/android/settingslib/miuisettings/preference/ValuePreference;


# direct methods
.method static bridge synthetic -$$Nest$fgetmCachedDevice(Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;)Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDevice(Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;)Landroid/bluetooth/BluetoothDevice;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDevice:Landroid/bluetooth/BluetoothDevice;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDeviceConnected(Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDeviceConnected:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmDeviceId(Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDeviceId:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDoubleClickLeft(Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;)Lmiuix/preference/DropDownPreference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDoubleClickLeft:Lmiuix/preference/DropDownPreference;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDoubleClickRight(Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;)Lmiuix/preference/DropDownPreference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDoubleClickRight:Lmiuix/preference/DropDownPreference;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmService(Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;)Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mService:Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTripleClickLeft(Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;)Lmiuix/preference/DropDownPreference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mTripleClickLeft:Lmiuix/preference/DropDownPreference;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTripleClickRight(Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;)Lmiuix/preference/DropDownPreference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mTripleClickRight:Lmiuix/preference/DropDownPreference;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmDeviceConnected(Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDeviceConnected:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$msetPreferenceEnable(Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->setPreferenceEnable(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateKeyConfig(Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->updateKeyConfig()V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mService:Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mSupport:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDeviceId:Ljava/lang/String;

    const-string v0, "000011101110"

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->PRESS_KEY_INIT:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mLeftDoubleKey:I

    iput v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mLeftTripleKey:I

    iput v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mRightDoubleKey:I

    iput v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mRightTripleKey:I

    iput-boolean v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mLeftKey:Z

    iput v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDropdownLeftKey:I

    iput-boolean v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mRightKey:Z

    iput v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDropdownRightKey:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDeviceConnected:Z

    new-instance v0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment$1;

    invoke-direct {v0, p0}, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment$1;-><init>(Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;)V

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mBluetoothA2dpReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment$2;

    invoke-direct {v0, p0}, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment$2;-><init>(Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;)V

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mPrefChangeListener:Landroidx/preference/Preference$OnPreferenceChangeListener;

    return-void
.end method

.method private getRadioButtonConfig()Ljava/lang/String;
    .locals 5

    const-string v0, ""

    :try_start_0
    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mService:Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v2, "MiuiHeadsetKeyConfigFragment"

    if-eqz v1, :cond_3

    :try_start_1
    iget-object v3, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDevice:Landroid/bluetooth/BluetoothDevice;

    if-nez v3, :cond_0

    goto :goto_0

    :cond_0
    const/16 v4, 0x6a

    invoke-interface {v1, v4, v0, v3}, Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;->setCommonCommand(ILjava/lang/String;Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v3, 0xc

    if-eq v0, v3, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mService:Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;

    const/16 v1, 0x69

    iget-object v3, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->PRESS_KEY_INIT:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-interface {v0, v1, v3, v4}, Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;->setCommonCommand(ILjava/lang/String;Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->PRESS_KEY_INIT:Ljava/lang/String;

    :cond_2
    invoke-direct {p0, v1}, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->saveCurrentKeyConfig(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "get radio button is: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :cond_3
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getRadioButtonConfig(): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mService:Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez v1, :cond_4

    move v1, v3

    goto :goto_1

    :cond_4
    move v1, v4

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDevice:Landroid/bluetooth/BluetoothDevice;

    if-nez v1, :cond_5

    goto :goto_2

    :cond_5
    move v3, v4

    :goto_2
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->PRESS_KEY_INIT:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    return-object p0

    :catch_0
    iget-object p0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->PRESS_KEY_INIT:Ljava/lang/String;

    return-object p0
.end method

.method private gotoPressKeyFragment(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_1

    const-string v0, ""

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "left"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "right"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/android/settings/bluetooth/MiuiHeadsetPressKeyFragment;

    invoke-direct {v0}, Lcom/android/settings/bluetooth/MiuiHeadsetPressKeyFragment;-><init>()V

    invoke-virtual {v0, p1}, Lcom/android/settings/bluetooth/MiuiHeadsetPressKeyFragment;->setTitleKey(Ljava/lang/String;)V

    iget-object p0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mHeadSetAct:Lcom/android/settings/bluetooth/MiuiHeadsetActivity;

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/MiuiHeadsetActivity;->changeFragment(Landroidx/fragment/app/Fragment;)V

    return-void

    :cond_1
    :goto_0
    const-string p0, "MiuiHeadsetKeyConfigFragment"

    const-string p1, "go to fragment presskey, it\'s title is null"

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static hexToByteArray(Ljava/lang/String;)[B
    .locals 4

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    new-array v0, v0, [B

    const/4 v1, 0x0

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v2

    goto :goto_0

    :catch_0
    move-exception p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hexToByteArray error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v1, "MiuiHeadsetKeyConfigFragment"

    invoke-static {v1, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-object v0
.end method

.method private initKeyConfig()V
    .locals 5

    invoke-direct {p0}, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->getRadioButtonConfig()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "radio button is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiHeadsetKeyConfigFragment"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v0}, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->hexToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    iput v2, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mLeftDoubleKey:I

    const/4 v2, 0x2

    aget-byte v2, v0, v2

    iput v2, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mLeftTripleKey:I

    const/4 v2, 0x1

    aget-byte v3, v0, v2

    iput v3, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mRightDoubleKey:I

    const/4 v3, 0x3

    aget-byte v3, v0, v3

    iput v3, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mRightTripleKey:I

    const/4 v3, 0x4

    aget-byte v3, v0, v3

    if-eqz v3, :cond_0

    move v4, v2

    goto :goto_0

    :cond_0
    move v4, v1

    :goto_0
    iput-boolean v4, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mLeftKey:Z

    iput v3, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDropdownLeftKey:I

    const/16 v3, 0x8

    aget-byte v0, v0, v3

    if-eqz v0, :cond_1

    move v1, v2

    :cond_1
    iput-boolean v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mRightKey:Z

    iput v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDropdownRightKey:I

    return-void
.end method

.method private initResource()V
    .locals 3

    const-string v0, "left_double"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiuix/preference/DropDownPreference;

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDoubleClickLeft:Lmiuix/preference/DropDownPreference;

    const-string/jumbo v0, "right_double"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiuix/preference/DropDownPreference;

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDoubleClickRight:Lmiuix/preference/DropDownPreference;

    const-string v0, "left_triple"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiuix/preference/DropDownPreference;

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mTripleClickLeft:Lmiuix/preference/DropDownPreference;

    const-string/jumbo v0, "right_triple"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiuix/preference/DropDownPreference;

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mTripleClickRight:Lmiuix/preference/DropDownPreference;

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDoubleClickLeft:Lmiuix/preference/DropDownPreference;

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mLeftDoubleKey:I

    invoke-virtual {v0, v1}, Lmiuix/preference/DropDownPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDoubleClickLeft:Lmiuix/preference/DropDownPreference;

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mPrefChangeListener:Landroidx/preference/Preference$OnPreferenceChangeListener;

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDoubleClickRight:Lmiuix/preference/DropDownPreference;

    if-eqz v0, :cond_1

    iget v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mRightDoubleKey:I

    invoke-virtual {v0, v1}, Lmiuix/preference/DropDownPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDoubleClickRight:Lmiuix/preference/DropDownPreference;

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mPrefChangeListener:Landroidx/preference/Preference$OnPreferenceChangeListener;

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mTripleClickLeft:Lmiuix/preference/DropDownPreference;

    if-eqz v0, :cond_2

    iget v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mLeftTripleKey:I

    invoke-virtual {v0, v1}, Lmiuix/preference/DropDownPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mTripleClickLeft:Lmiuix/preference/DropDownPreference;

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mPrefChangeListener:Landroidx/preference/Preference$OnPreferenceChangeListener;

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mTripleClickRight:Lmiuix/preference/DropDownPreference;

    if-eqz v0, :cond_3

    iget v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mRightTripleKey:I

    invoke-virtual {v0, v1}, Lmiuix/preference/DropDownPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mTripleClickRight:Lmiuix/preference/DropDownPreference;

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mPrefChangeListener:Landroidx/preference/Preference$OnPreferenceChangeListener;

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDeviceId:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/settings/bluetooth/HeadsetIDConstants;->isK77sHeadset(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "long_press_right_headset"

    const-string v2, "long_press_left_headset"

    if-eqz v0, :cond_5

    invoke-virtual {p0, v2}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiuix/preference/DropDownPreference;

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDropdownPrefLeft:Lmiuix/preference/DropDownPreference;

    invoke-virtual {p0, v1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiuix/preference/DropDownPreference;

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDropdownPrefRight:Lmiuix/preference/DropDownPreference;

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDropdownPrefLeft:Lmiuix/preference/DropDownPreference;

    if-eqz v0, :cond_4

    iget v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDropdownLeftKey:I

    invoke-virtual {v0, v1}, Lmiuix/preference/DropDownPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDropdownPrefLeft:Lmiuix/preference/DropDownPreference;

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mPrefChangeListener:Landroidx/preference/Preference$OnPreferenceChangeListener;

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_4
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDropdownPrefRight:Lmiuix/preference/DropDownPreference;

    if-eqz v0, :cond_8

    iget v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDropdownRightKey:I

    invoke-virtual {v0, v1}, Lmiuix/preference/DropDownPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDropdownPrefRight:Lmiuix/preference/DropDownPreference;

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mPrefChangeListener:Landroidx/preference/Preference$OnPreferenceChangeListener;

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_2

    :cond_5
    invoke-virtual {p0, v2}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->pref_left:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    invoke-virtual {p0, v1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->pref_right:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->pref_left:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    iget-boolean v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mLeftKey:Z

    if-eqz v1, :cond_6

    sget v1, Lcom/android/settings/R$string;->miheadset_key_config_noise_control:I

    goto :goto_0

    :cond_6
    sget v1, Lcom/android/settings/R$string;->miheadset_key_config_call_ai:I

    :goto_0
    invoke-virtual {v0, v1}, Lcom/android/settingslib/miuisettings/preference/ValuePreference;->setValue(I)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->pref_right:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    iget-boolean v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mRightKey:Z

    if-eqz v1, :cond_7

    sget v1, Lcom/android/settings/R$string;->miheadset_key_config_noise_control:I

    goto :goto_1

    :cond_7
    sget v1, Lcom/android/settings/R$string;->miheadset_key_config_call_ai:I

    :goto_1
    invoke-virtual {v0, v1}, Lcom/android/settingslib/miuisettings/preference/ValuePreference;->setValue(I)V

    :cond_8
    :goto_2
    iget-boolean v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDeviceConnected:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mService:Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDevice:Landroid/bluetooth/BluetoothDevice;

    iget-object v2, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDeviceId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/android/settings/bluetooth/HeadsetIDConstants;->isBleMmaConnect(Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    goto :goto_3

    :cond_9
    const/4 v0, 0x0

    goto :goto_4

    :cond_a
    :goto_3
    const/4 v0, 0x1

    :goto_4
    invoke-direct {p0, v0}, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->setPreferenceEnable(Z)V

    return-void
.end method

.method private saveCurrentKeyConfig(Ljava/lang/String;)V
    .locals 2

    iput-object p1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->PRESS_KEY_INIT:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mHeadSetAct:Lcom/android/settings/bluetooth/MiuiHeadsetActivity;

    invoke-virtual {v0, p1}, Lcom/android/settings/bluetooth/MiuiHeadsetActivity;->setDeviceConfig(Ljava/lang/String;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "Headset_Key_Init"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDeviceId:Ljava/lang/String;

    const-string v1, "Headset_DeviceId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-void
.end method

.method private setPreferenceEnable(Z)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setPreferenceEnable "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiHeadsetKeyConfigFragment"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDoubleClickLeft:Lmiuix/preference/DropDownPreference;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroidx/preference/Preference;->setEnabled(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDoubleClickRight:Lmiuix/preference/DropDownPreference;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Landroidx/preference/Preference;->setEnabled(Z)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mTripleClickLeft:Lmiuix/preference/DropDownPreference;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Landroidx/preference/Preference;->setEnabled(Z)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mTripleClickRight:Lmiuix/preference/DropDownPreference;

    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Landroidx/preference/Preference;->setEnabled(Z)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDeviceId:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/settings/bluetooth/HeadsetIDConstants;->isK77sHeadset(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDropdownPrefLeft:Lmiuix/preference/DropDownPreference;

    if-eqz v0, :cond_4

    invoke-virtual {v0, p1}, Landroidx/preference/Preference;->setEnabled(Z)V

    :cond_4
    iget-object p0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDropdownPrefRight:Lmiuix/preference/DropDownPreference;

    if-eqz p0, :cond_7

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setEnabled(Z)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->pref_left:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    if-eqz v0, :cond_6

    invoke-virtual {v0, p1}, Landroidx/preference/Preference;->setEnabled(Z)V

    :cond_6
    iget-object p0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->pref_right:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    if-eqz p0, :cond_7

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setEnabled(Z)V

    :cond_7
    :goto_0
    return-void
.end method

.method private updateKeyConfig()V
    .locals 5

    const-string v0, "MiuiHeadsetKeyConfigFragment"

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateKeyConfig:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mService:Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_0

    move v2, v3

    goto :goto_0

    :cond_0
    move v2, v4

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    move v3, v4

    :goto_1
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mService:Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v1, :cond_5

    invoke-direct {p0}, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->getRadioButtonConfig()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mLeftDoubleKey:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mRightDoubleKey:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mLeftTripleKey:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mRightTripleKey:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDeviceId:Ljava/lang/String;

    invoke-static {v3}, Lcom/android/settings/bluetooth/HeadsetIDConstants;->isK77sHeadset(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDropdownLeftKey:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v3, "0000"

    const-string v4, "1000"

    if-nez v2, :cond_2

    move-object v2, v3

    goto :goto_2

    :cond_2
    move-object v2, v4

    :goto_2
    :try_start_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDropdownRightKey:I

    if-nez v2, :cond_3

    goto :goto_3

    :cond_3
    move-object v3, v4

    :goto_3
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_4
    iget-object v2, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mService:Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;

    const/16 v3, 0x69

    iget-object v4, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-interface {v2, v3, v1, v4}, Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;->setCommonCommand(ILjava/lang/String;Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->saveCurrentKeyConfig(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_5

    :catch_0
    move-exception p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Get device load list failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    :goto_5
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 0

    const-class p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method protected getPreferenceScreenResId()I
    .locals 3

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDeviceId:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/settings/bluetooth/HeadsetIDConstants;->isTWS01Headset(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "MiuiHeadsetKeyConfigFragment"

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDeviceId:Ljava/lang/String;

    const-string v2, "gesture"

    invoke-static {v2, v0}, Lcom/android/settings/bluetooth/HeadsetIDConstants;->isSupportZimiAdapter(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDeviceId:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/settings/bluetooth/HeadsetIDConstants;->isK77sHeadset(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p0, "K77s device ID "

    invoke-static {v1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget p0, Lcom/android/settings/R$xml;->headset_key_config_tws_k77s:I

    return p0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDeviceId:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/settings/bluetooth/HeadsetIDConstants;->isK73Headset(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string p0, "K73 device ID "

    invoke-static {v1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget p0, Lcom/android/settings/R$xml;->headset_key_config:I

    return p0

    :cond_2
    iget-object p0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDeviceId:Ljava/lang/String;

    invoke-static {p0}, Lcom/android/settings/bluetooth/HeadsetIDConstants;->isK75Headset(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_3

    const-string p0, "K75 device ID "

    invoke-static {v1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget p0, Lcom/android/settings/R$xml;->headset_key_config:I

    return p0

    :cond_3
    sget p0, Lcom/android/settings/R$xml;->headset_key_config:I

    return p0

    :cond_4
    :goto_0
    const-string p0, "K76 device ID "

    invoke-static {v1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget p0, Lcom/android/settings/R$xml;->headset_key_config_TWS01:I

    return p0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onAttach(Landroid/app/Activity;)V

    check-cast p1, Lcom/android/settings/bluetooth/MiuiHeadsetActivity;

    invoke-virtual {p1}, Lcom/android/settings/bluetooth/MiuiHeadsetActivity;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDevice:Landroid/bluetooth/BluetoothDevice;

    iput-object p1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mHeadSetAct:Lcom/android/settings/bluetooth/MiuiHeadsetActivity;

    invoke-virtual {p1}, Lcom/android/settings/bluetooth/MiuiHeadsetActivity;->getSupport()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mSupport:Ljava/lang/String;

    iget-object p1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mHeadSetAct:Lcom/android/settings/bluetooth/MiuiHeadsetActivity;

    invoke-virtual {p1}, Lcom/android/settings/bluetooth/MiuiHeadsetActivity;->getDeviceID()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDeviceId:Ljava/lang/String;

    iget-object p1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mHeadSetAct:Lcom/android/settings/bluetooth/MiuiHeadsetActivity;

    invoke-virtual {p1}, Lcom/android/settings/bluetooth/MiuiHeadsetActivity;->getDeviceConfig()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->PRESS_KEY_INIT:Ljava/lang/String;

    iget-object p1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mHeadSetAct:Lcom/android/settings/bluetooth/MiuiHeadsetActivity;

    invoke-virtual {p1}, Lcom/android/settings/bluetooth/MiuiHeadsetActivity;->getCachedBluetoothDevice()Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "Headset_Key_Init"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->PRESS_KEY_INIT:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "Headset_DeviceId"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDeviceId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDeviceId:Ljava/lang/String;

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getArguments(), init key:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->PRESS_KEY_INIT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDeviceId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiHeadsetKeyConfigFragment"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p0

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceGroup;->setOrderingAsAdded(Z)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    sget v0, Lcom/android/settings/R$layout;->headset_key_config_layout:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mRootView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->prefs_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/SettingsPreferenceFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object p1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mHeadSetAct:Lcom/android/settings/bluetooth/MiuiHeadsetActivity;

    invoke-virtual {p1}, Lcom/android/settings/bluetooth/MiuiHeadsetActivity;->getService()Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mService:Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    check-cast p1, Lmiuix/appcompat/app/AppCompatActivity;

    invoke-virtual {p1}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/ActionBar;

    move-result-object p1

    if-eqz p1, :cond_0

    sget p2, Lcom/android/settings/R$string;->miheadset_key_config_gesture_control:I

    invoke-virtual {p1, p2}, Landroidx/appcompat/app/ActionBar;->setTitle(I)V

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const/4 p2, 0x1

    const-string p3, "device_connected"

    invoke-virtual {p1, p3, p2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDeviceConnected:Z

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->initKeyConfig()V

    invoke-direct {p0}, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->initResource()V

    iget-object p0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mRootView:Landroid/view/View;

    return-object p0
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Lcom/android/settingslib/core/lifecycle/ObservablePreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onHiddenChanged(Z)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onHiddenChanged(Z)V

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mHeadSetAct:Lcom/android/settings/bluetooth/MiuiHeadsetActivity;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/android/settings/bluetooth/MiuiHeadsetActivity;->getDeviceConfig()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->PRESS_KEY_INIT:Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->initKeyConfig()V

    iget-object p1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->pref_left:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    iget-boolean v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mLeftKey:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/android/settings/R$string;->miheadset_key_config_noise_control:I

    goto :goto_0

    :cond_0
    sget v0, Lcom/android/settings/R$string;->miheadset_key_config_call_ai:I

    :goto_0
    invoke-virtual {p1, v0}, Lcom/android/settingslib/miuisettings/preference/ValuePreference;->setValue(I)V

    iget-object p1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->pref_right:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    iget-boolean v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mRightKey:Z

    if-eqz v0, :cond_1

    sget v0, Lcom/android/settings/R$string;->miheadset_key_config_noise_control:I

    goto :goto_1

    :cond_1
    sget v0, Lcom/android/settings/R$string;->miheadset_key_config_call_ai:I

    :goto_1
    invoke-virtual {p1, v0}, Lcom/android/settingslib/miuisettings/preference/ValuePreference;->setValue(I)V

    iget-object p1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDoubleClickLeft:Lmiuix/preference/DropDownPreference;

    iget v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mLeftDoubleKey:I

    invoke-virtual {p1, v0}, Lmiuix/preference/DropDownPreference;->setValueIndex(I)V

    iget-object p1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mTripleClickLeft:Lmiuix/preference/DropDownPreference;

    iget v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mLeftTripleKey:I

    invoke-virtual {p1, v0}, Lmiuix/preference/DropDownPreference;->setValueIndex(I)V

    iget-object p1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDoubleClickRight:Lmiuix/preference/DropDownPreference;

    iget v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mRightDoubleKey:I

    invoke-virtual {p1, v0}, Lmiuix/preference/DropDownPreference;->setValueIndex(I)V

    iget-object p1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mTripleClickRight:Lmiuix/preference/DropDownPreference;

    iget p0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mRightTripleKey:I

    invoke-virtual {p1, p0}, Lmiuix/preference/DropDownPreference;->setValueIndex(I)V

    :cond_2
    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onPause()V

    return-void
.end method

.method public onPreferenceTreeClick(Landroidx/preference/PreferenceScreen;Landroidx/preference/Preference;)Z
    .locals 0

    invoke-virtual {p2}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    const-string p2, "long_press_right_headset"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_1

    const-string p2, "long_press_left_headset"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, "left"

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->gotoPressKeyFragment(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string/jumbo p1, "right"

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->gotoPressKeyFragment(Ljava/lang/String;)V

    :goto_0
    const/4 p0, 0x0

    return p0
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onResume()V

    return-void
.end method

.method public onServiceConnected()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mHeadSetAct:Lcom/android/settings/bluetooth/MiuiHeadsetActivity;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/android/settings/bluetooth/MiuiHeadsetActivity;

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mHeadSetAct:Lcom/android/settings/bluetooth/MiuiHeadsetActivity;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mHeadSetAct:Lcom/android/settings/bluetooth/MiuiHeadsetActivity;

    invoke-virtual {v0}, Lcom/android/settings/bluetooth/MiuiHeadsetActivity;->getService()Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mService:Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mHeadSetAct:Lcom/android/settings/bluetooth/MiuiHeadsetActivity;

    invoke-virtual {v0}, Lcom/android/settings/bluetooth/MiuiHeadsetActivity;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-direct {p0}, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->initKeyConfig()V

    invoke-direct {p0}, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->initResource()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "activity define service error "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "MiuiHeadsetKeyConfigFragment"

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public onStart()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onStart()V

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "com.xiaomi.bluetooth.ACTION.MMA_STATUS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    iget-object p0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mBluetoothA2dpReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, p0, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Lcom/android/settingslib/core/lifecycle/ObservablePreferenceFragment;->onStop()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget-object p0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->mBluetoothA2dpReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method
