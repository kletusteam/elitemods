.class Lcom/android/settings/bluetooth/MiuiHeadsetActivity$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/bluetooth/MiuiHeadsetActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/bluetooth/MiuiHeadsetActivity;


# direct methods
.method constructor <init>(Lcom/android/settings/bluetooth/MiuiHeadsetActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetActivity$1;->this$0:Lcom/android/settings/bluetooth/MiuiHeadsetActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    const-string p1, "MiuiHeadsetActivity"

    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetActivity$1;->this$0:Lcom/android/settings/bluetooth/MiuiHeadsetActivity;

    invoke-static {p2}, Lcom/android/bluetooth/ble/app/IMiuiHeadsetService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;

    move-result-object p2

    invoke-static {v0, p2}, Lcom/android/settings/bluetooth/MiuiHeadsetActivity;->-$$Nest$fputmService(Lcom/android/settings/bluetooth/MiuiHeadsetActivity;Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;)V

    iget-object p2, p0, Lcom/android/settings/bluetooth/MiuiHeadsetActivity$1;->this$0:Lcom/android/settings/bluetooth/MiuiHeadsetActivity;

    invoke-virtual {p2}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p2

    sget v0, Lcom/android/settings/R$id;->layout_content:I

    invoke-virtual {p2, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentById(I)Landroidx/fragment/app/Fragment;

    move-result-object p2

    instance-of v0, p2, Lcom/android/settings/bluetooth/MiuiHeadsetAntiLostFragment;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/android/settings/bluetooth/MiuiHeadsetAntiLostFragment;

    invoke-virtual {v0}, Lcom/android/settings/bluetooth/MiuiHeadsetAntiLostFragment;->onServiceConnected()V

    goto :goto_0

    :cond_0
    instance-of v0, p2, Lcom/android/settings/bluetooth/MiuiHeadsetFragment;

    if-eqz v0, :cond_1

    move-object v0, p2

    check-cast v0, Lcom/android/settings/bluetooth/MiuiHeadsetFragment;

    invoke-virtual {v0}, Lcom/android/settings/bluetooth/MiuiHeadsetFragment;->onServiceConnected()V

    goto :goto_0

    :cond_1
    instance-of v0, p2, Lcom/android/settings/bluetooth/MiuiHeadsetFitnessFragment;

    if-eqz v0, :cond_2

    move-object v0, p2

    check-cast v0, Lcom/android/settings/bluetooth/MiuiHeadsetFitnessFragment;

    invoke-virtual {v0}, Lcom/android/settings/bluetooth/MiuiHeadsetFitnessFragment;->onServiceConnected()V

    goto :goto_0

    :cond_2
    instance-of v0, p2, Lcom/android/settings/bluetooth/MiuiHeadsetFindDeviceFragment;

    if-eqz v0, :cond_3

    move-object v0, p2

    check-cast v0, Lcom/android/settings/bluetooth/MiuiHeadsetFindDeviceFragment;

    invoke-virtual {v0}, Lcom/android/settings/bluetooth/MiuiHeadsetFindDeviceFragment;->onServiceConnected()V

    goto :goto_0

    :cond_3
    instance-of v0, p2, Lcom/android/settings/bluetooth/MiuiHeadsetPressKeyFragment;

    if-eqz v0, :cond_4

    move-object v0, p2

    check-cast v0, Lcom/android/settings/bluetooth/MiuiHeadsetPressKeyFragment;

    invoke-virtual {v0}, Lcom/android/settings/bluetooth/MiuiHeadsetPressKeyFragment;->onServiceConnected()V

    goto :goto_0

    :cond_4
    instance-of v0, p2, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;

    if-eqz v0, :cond_5

    move-object v0, p2

    check-cast v0, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;

    invoke-virtual {v0}, Lcom/android/settings/bluetooth/MiuiHeadsetKeyConfigFragment;->onServiceConnected()V

    :cond_5
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetActivity$1;->this$0:Lcom/android/settings/bluetooth/MiuiHeadsetActivity;

    invoke-static {v0}, Lcom/android/settings/bluetooth/MiuiHeadsetActivity;->-$$Nest$fgetmService(Lcom/android/settings/bluetooth/MiuiHeadsetActivity;)Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bluetooth/MiuiHeadsetActivity$1;->this$0:Lcom/android/settings/bluetooth/MiuiHeadsetActivity;

    invoke-static {v1}, Lcom/android/settings/bluetooth/MiuiHeadsetActivity;->-$$Nest$fgetmCallBack(Lcom/android/settings/bluetooth/MiuiHeadsetActivity;)Lcom/android/settings/bluetooth/MiuiHeadsetActivity$MiuiHeadsetCallback;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;->register(Lcom/android/bluetooth/ble/app/IMiuiHeadsetCallback;)V

    instance-of p2, p2, Lcom/android/settings/bluetooth/MiuiHeadsetFragment;

    if-nez p2, :cond_6

    iget-object p2, p0, Lcom/android/settings/bluetooth/MiuiHeadsetActivity$1;->this$0:Lcom/android/settings/bluetooth/MiuiHeadsetActivity;

    invoke-static {p2}, Lcom/android/settings/bluetooth/MiuiHeadsetActivity;->-$$Nest$fgetmService(Lcom/android/settings/bluetooth/MiuiHeadsetActivity;)Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;

    move-result-object p2

    iget-object p0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetActivity$1;->this$0:Lcom/android/settings/bluetooth/MiuiHeadsetActivity;

    iget-object p0, p0, Lcom/android/settings/bluetooth/MiuiHeadsetActivity;->mDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-interface {p2, p0}, Lcom/android/bluetooth/ble/app/IMiuiHeadsetService;->connect(Landroid/bluetooth/BluetoothDevice;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "connect the mma failed "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    :goto_1
    const-string/jumbo p0, "onServiceConnected"

    invoke-static {p1, p0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0

    const-string p0, "MiuiHeadsetActivity"

    const-string/jumbo p1, "service disconnected"

    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
