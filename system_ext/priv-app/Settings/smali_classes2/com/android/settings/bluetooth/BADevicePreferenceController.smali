.class public Lcom/android/settings/bluetooth/BADevicePreferenceController;
.super Lcom/android/settings/core/BasePreferenceController;

# interfaces
.implements Lcom/android/settingslib/core/lifecycle/LifecycleObserver;
.implements Lcom/android/settingslib/core/lifecycle/events/OnStart;
.implements Lcom/android/settingslib/core/lifecycle/events/OnStop;
.implements Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreferenceCallback;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation


# static fields
.field private static final MAX_DEVICE_NUM:I = 0x3

.field private static final TAG:Ljava/lang/String; = "BADevicePreferenceController"


# instance fields
.field private mBleSourceInfoUpdater:Lcom/android/settings/bluetooth/BluetoothBroadcastSourceInfoEntries;

.field private mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

.field private mPreferenceGroup:Landroidx/preference/PreferenceGroup;

.field private mPreferenceSize:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settingslib/core/lifecycle/Lifecycle;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p3}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p2, p0}, Lcom/android/settingslib/core/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p1, "constructor: KEY"

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "BADevicePreferenceController"

    invoke-static {p1, p0}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public displayPreference(Landroidx/preference/PreferenceScreen;)V
    .locals 3

    const-string v0, "BADevicePreferenceController"

    const-string v1, "displayPreference"

    invoke-static {v0, v1}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/android/settings/core/BasePreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BADevicePreferenceController;->getPreferenceKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v1

    check-cast v1, Landroidx/preference/PreferenceGroup;

    iput-object v1, p0, Lcom/android/settings/bluetooth/BADevicePreferenceController;->mPreferenceGroup:Landroidx/preference/PreferenceGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroidx/preference/Preference;->setVisible(Z)V

    invoke-virtual {p0}, Lcom/android/settings/core/BasePreferenceController;->isAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "registering wth BleSrcInfo updaters"

    invoke-static {v0, v1}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object p1

    iget-object p0, p0, Lcom/android/settings/bluetooth/BADevicePreferenceController;->mBleSourceInfoUpdater:Lcom/android/settings/bluetooth/BluetoothBroadcastSourceInfoEntries;

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoUpdater;->setPrefContext(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method public getAvailabilityStatus()I
    .locals 1

    iget-object p0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    const-string v0, "android.hardware.bluetooth"

    invoke-virtual {p0, v0}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_0
    const/4 p0, 0x2

    :goto_0
    return p0
.end method

.method public bridge synthetic getBackgroundWorkerClass()Ljava/lang/Class;
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->getBackgroundWorkerClass()Ljava/lang/Class;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic getIntentFilter()Landroid/content/IntentFilter;
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->getIntentFilter()Landroid/content/IntentFilter;

    move-result-object p0

    return-object p0
.end method

.method public getPreferenceKey()Ljava/lang/String;
    .locals 1

    new-instance p0, Ljava/lang/String;

    const-string v0, "added_sources"

    invoke-direct {p0, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    return-object p0
.end method

.method public bridge synthetic getSliceHighlightMenuRes()I
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->getSliceHighlightMenuRes()I

    move-result p0

    return p0
.end method

.method public bridge synthetic hasAsyncUpdate()Z
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->hasAsyncUpdate()Z

    move-result p0

    return p0
.end method

.method public init(Lcom/android/settings/dashboard/DashboardFragment;Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)V
    .locals 2

    const-string v0, "BADevicePreferenceController"

    const-string v1, "Init"

    invoke-static {v0, v1}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/android/settings/bluetooth/BADevicePreferenceController;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    new-instance v0, Lcom/android/settings/bluetooth/BluetoothBroadcastSourceInfoEntries;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1, p0, p2}, Lcom/android/settings/bluetooth/BluetoothBroadcastSourceInfoEntries;-><init>(Landroid/content/Context;Lcom/android/settings/dashboard/DashboardFragment;Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreferenceCallback;Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)V

    iput-object v0, p0, Lcom/android/settings/bluetooth/BADevicePreferenceController;->mBleSourceInfoUpdater:Lcom/android/settings/bluetooth/BluetoothBroadcastSourceInfoEntries;

    const/4 p1, 0x0

    iput p1, p0, Lcom/android/settings/bluetooth/BADevicePreferenceController;->mPreferenceSize:I

    return-void
.end method

.method public bridge synthetic isPublicSlice()Z
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->isPublicSlice()Z

    move-result p0

    return p0
.end method

.method public bridge synthetic isSliceable()Z
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->isSliceable()Z

    move-result p0

    return p0
.end method

.method public onBroadcastSourceInfoAdded(Landroidx/preference/Preference;)V
    .locals 3

    const-string v0, "BADevicePreferenceController"

    const-string/jumbo v1, "onBroadcastSourceInfoAdded"

    invoke-static {v0, v1}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    iget v1, p0, Lcom/android/settings/bluetooth/BADevicePreferenceController;->mPreferenceSize:I

    const/4 v2, 0x3

    if-ge v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/settings/bluetooth/BADevicePreferenceController;->mPreferenceGroup:Landroidx/preference/PreferenceGroup;

    invoke-virtual {v1, p1}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    move-result p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addPreference returns"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    iget p1, p0, Lcom/android/settings/bluetooth/BADevicePreferenceController;->mPreferenceSize:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/android/settings/bluetooth/BADevicePreferenceController;->mPreferenceSize:I

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BADevicePreferenceController;->updatePreferenceVisiblity()V

    return-void
.end method

.method public onBroadcastSourceInfoRemoved(Landroidx/preference/Preference;)V
    .locals 3

    const-string v0, "BADevicePreferenceController"

    const-string/jumbo v1, "onBroadcastSourceInfoRemoved"

    invoke-static {v0, v1}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    iget v1, p0, Lcom/android/settings/bluetooth/BADevicePreferenceController;->mPreferenceSize:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/android/settings/bluetooth/BADevicePreferenceController;->mPreferenceSize:I

    iget-object v1, p0, Lcom/android/settings/bluetooth/BADevicePreferenceController;->mPreferenceGroup:Landroidx/preference/PreferenceGroup;

    invoke-virtual {v1, p1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    move-result p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "removePreference returns "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BADevicePreferenceController;->updatePreferenceVisiblity()V

    return-void
.end method

.method public onStart()V
    .locals 0

    iget-object p0, p0, Lcom/android/settings/bluetooth/BADevicePreferenceController;->mBleSourceInfoUpdater:Lcom/android/settings/bluetooth/BluetoothBroadcastSourceInfoEntries;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoUpdater;->registerCallback()V

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 0

    iget-object p0, p0, Lcom/android/settings/bluetooth/BADevicePreferenceController;->mBleSourceInfoUpdater:Lcom/android/settings/bluetooth/BluetoothBroadcastSourceInfoEntries;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoUpdater;->unregisterCallback()V

    :cond_0
    return-void
.end method

.method setPreferenceGroup(Landroidx/preference/PreferenceGroup;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iput-object p1, p0, Lcom/android/settings/bluetooth/BADevicePreferenceController;->mPreferenceGroup:Landroidx/preference/PreferenceGroup;

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method updatePreferenceVisiblity()V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    const-string v1, "BADevicePreferenceController"

    goto/32 :goto_11

    nop

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_9

    nop

    :goto_2
    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setVisible(Z)V

    goto/32 :goto_e

    nop

    :goto_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_f

    nop

    :goto_4
    goto :goto_8

    :goto_5
    goto/32 :goto_7

    nop

    :goto_6
    iget v1, p0, Lcom/android/settings/bluetooth/BADevicePreferenceController;->mPreferenceSize:I

    goto/32 :goto_3

    nop

    :goto_7
    const/4 p0, 0x0

    :goto_8
    goto/32 :goto_2

    nop

    :goto_9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_10

    nop

    :goto_a
    if-gtz p0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_c

    nop

    :goto_b
    iget-object v0, p0, Lcom/android/settings/bluetooth/BADevicePreferenceController;->mPreferenceGroup:Landroidx/preference/PreferenceGroup;

    goto/32 :goto_12

    nop

    :goto_c
    const/4 p0, 0x1

    goto/32 :goto_4

    nop

    :goto_d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_e
    return-void

    :goto_f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_10
    const-string/jumbo v1, "updatePreferenceVisiblity:"

    goto/32 :goto_d

    nop

    :goto_11
    invoke-static {v1, v0}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_b

    nop

    :goto_12
    iget p0, p0, Lcom/android/settings/bluetooth/BADevicePreferenceController;->mPreferenceSize:I

    goto/32 :goto_a

    nop
.end method

.method public bridge synthetic useDynamicSliceSummary()Z
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->useDynamicSliceSummary()Z

    move-result p0

    return p0
.end method
