.class public Lcom/android/settings/bluetooth/QigsawInstaller;
.super Landroid/app/Activity;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/bluetooth/QigsawInstaller$BluetoothPluginOneTrackHelper;
    }
.end annotation


# static fields
.field private static HEADSETPLUGIN_INITED:I = 0x1


# instance fields
.field private mFirstStartup:Z

.field private mInstallManager:Lcom/google/android/play/core/splitinstall/SplitInstallManager;

.field private mModuleNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSessionId:I

.field private mStatus:I

.field private myListener:Lcom/google/android/play/core/splitinstall/SplitInstallStateUpdatedListener;

.field private startInstallFlag:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetmInstallManager(Lcom/android/settings/bluetooth/QigsawInstaller;)Lcom/google/android/play/core/splitinstall/SplitInstallManager;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->mInstallManager:Lcom/google/android/play/core/splitinstall/SplitInstallManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmModuleNames(Lcom/android/settings/bluetooth/QigsawInstaller;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->mModuleNames:Ljava/util/ArrayList;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSessionId(Lcom/android/settings/bluetooth/QigsawInstaller;)I
    .locals 0

    iget p0, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->mSessionId:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmSessionId(Lcom/android/settings/bluetooth/QigsawInstaller;I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->mSessionId:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmStatus(Lcom/android/settings/bluetooth/QigsawInstaller;I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->mStatus:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputstartInstallFlag(Lcom/android/settings/bluetooth/QigsawInstaller;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->startInstallFlag:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$monDownloaded(Lcom/android/settings/bluetooth/QigsawInstaller;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/bluetooth/QigsawInstaller;->onDownloaded()V

    return-void
.end method

.method static bridge synthetic -$$Nest$monDownloading(Lcom/android/settings/bluetooth/QigsawInstaller;Lcom/google/android/play/core/splitinstall/SplitInstallSessionState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/QigsawInstaller;->onDownloading(Lcom/google/android/play/core/splitinstall/SplitInstallSessionState;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monFailed(Lcom/android/settings/bluetooth/QigsawInstaller;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/bluetooth/QigsawInstaller;->onFailed()V

    return-void
.end method

.method static bridge synthetic -$$Nest$monInstalled(Lcom/android/settings/bluetooth/QigsawInstaller;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/bluetooth/QigsawInstaller;->onInstalled()V

    return-void
.end method

.method static bridge synthetic -$$Nest$monInstalling(Lcom/android/settings/bluetooth/QigsawInstaller;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/bluetooth/QigsawInstaller;->onInstalling()V

    return-void
.end method

.method static bridge synthetic -$$Nest$monPending(Lcom/android/settings/bluetooth/QigsawInstaller;Lcom/google/android/play/core/splitinstall/SplitInstallSessionState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/QigsawInstaller;->onPending(Lcom/google/android/play/core/splitinstall/SplitInstallSessionState;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monRequiresUserConfirmation(Lcom/android/settings/bluetooth/QigsawInstaller;Lcom/google/android/play/core/splitinstall/SplitInstallSessionState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/QigsawInstaller;->onRequiresUserConfirmation(Lcom/google/android/play/core/splitinstall/SplitInstallSessionState;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mstartInstall(Lcom/android/settings/bluetooth/QigsawInstaller;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/bluetooth/QigsawInstaller;->startInstall()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->mFirstStartup:Z

    new-instance v0, Lcom/android/settings/bluetooth/QigsawInstaller$1;

    invoke-direct {v0, p0}, Lcom/android/settings/bluetooth/QigsawInstaller$1;-><init>(Lcom/android/settings/bluetooth/QigsawInstaller;)V

    iput-object v0, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->myListener:Lcom/google/android/play/core/splitinstall/SplitInstallStateUpdatedListener;

    return-void
.end method

.method private onDownloaded()V
    .locals 1

    const-string p0, "QigsawInstaller"

    const-string/jumbo v0, "on downloaded"

    invoke-static {p0, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private onDownloading(Lcom/google/android/play/core/splitinstall/SplitInstallSessionState;)V
    .locals 0
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "StringFormatInvalid"
        }
    .end annotation

    const-string p0, "QigsawInstaller"

    const-string/jumbo p1, "on downloaded message"

    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private onFailed()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method private onInstalled()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/QigsawInstaller;->onInstallOK()V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->mModuleNames:Ljava/util/ArrayList;

    const-string/jumbo v2, "moduleNames"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method private onInstalling()V
    .locals 1

    const-string p0, "QigsawInstaller"

    const-string/jumbo v0, "on installing"

    invoke-static {p0, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private onPending(Lcom/google/android/play/core/splitinstall/SplitInstallSessionState;)V
    .locals 0

    return-void
.end method

.method private onRequiresUserConfirmation(Lcom/google/android/play/core/splitinstall/SplitInstallSessionState;)V
    .locals 7

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/play/core/splitinstall/SplitInstallSessionState;->resolutionIntent()Landroid/app/PendingIntent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v1

    const/16 v2, 0xb

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Landroid/app/Activity;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Landroid/content/IntentSender$SendIntentException;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private startInstall()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->mInstallManager:Lcom/google/android/play/core/splitinstall/SplitInstallManager;

    invoke-interface {v0}, Lcom/google/android/play/core/splitinstall/SplitInstallManager;->getInstalledModules()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->mModuleNames:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/bluetooth/QigsawInstaller;->onInstalled()V

    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/play/core/splitinstall/SplitInstallRequest;->newBuilder()Lcom/google/android/play/core/splitinstall/SplitInstallRequest$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->mModuleNames:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/play/core/splitinstall/SplitInstallRequest$Builder;->addModule(Ljava/lang/String;)Lcom/google/android/play/core/splitinstall/SplitInstallRequest$Builder;

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/play/core/splitinstall/SplitInstallRequest$Builder;->build()Lcom/google/android/play/core/splitinstall/SplitInstallRequest;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->mInstallManager:Lcom/google/android/play/core/splitinstall/SplitInstallManager;

    invoke-interface {v1, v0}, Lcom/google/android/play/core/splitinstall/SplitInstallManager;->startInstall(Lcom/google/android/play/core/splitinstall/SplitInstallRequest;)Lcom/google/android/play/core/tasks/Task;

    move-result-object v0

    new-instance v1, Lcom/android/settings/bluetooth/QigsawInstaller$3;

    invoke-direct {v1, p0}, Lcom/android/settings/bluetooth/QigsawInstaller$3;-><init>(Lcom/android/settings/bluetooth/QigsawInstaller;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/core/tasks/Task;->addOnSuccessListener(Lcom/google/android/play/core/tasks/OnSuccessListener;)Lcom/google/android/play/core/tasks/Task;

    move-result-object v0

    new-instance v1, Lcom/android/settings/bluetooth/QigsawInstaller$2;

    invoke-direct {v1, p0}, Lcom/android/settings/bluetooth/QigsawInstaller$2;-><init>(Lcom/android/settings/bluetooth/QigsawInstaller;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/core/tasks/Task;->addOnFailureListener(Lcom/google/android/play/core/tasks/OnFailureListener;)Lcom/google/android/play/core/tasks/Task;

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 2

    iget v0, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->mStatus:I

    if-nez v0, :cond_0

    const-string v0, "QigsawInstaller"

    const-string v1, "Split download is not started!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    return-void

    :cond_0
    const/16 v1, 0x9

    if-eq v0, v1, :cond_4

    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->startInstallFlag:Z

    if-nez v0, :cond_2

    return-void

    :cond_2
    iget v0, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->mSessionId:I

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->mInstallManager:Lcom/google/android/play/core/splitinstall/SplitInstallManager;

    invoke-interface {v1, v0}, Lcom/google/android/play/core/splitinstall/SplitInstallManager;->cancelInstall(I)Lcom/google/android/play/core/tasks/Task;

    move-result-object v0

    new-instance v1, Lcom/android/settings/bluetooth/QigsawInstaller$6;

    invoke-direct {v1, p0}, Lcom/android/settings/bluetooth/QigsawInstaller$6;-><init>(Lcom/android/settings/bluetooth/QigsawInstaller;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/core/tasks/Task;->addOnSuccessListener(Lcom/google/android/play/core/tasks/OnSuccessListener;)Lcom/google/android/play/core/tasks/Task;

    move-result-object v0

    new-instance v1, Lcom/android/settings/bluetooth/QigsawInstaller$5;

    invoke-direct {v1, p0}, Lcom/android/settings/bluetooth/QigsawInstaller$5;-><init>(Lcom/android/settings/bluetooth/QigsawInstaller;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/core/tasks/Task;->addOnFailureListener(Lcom/google/android/play/core/tasks/OnFailureListener;)Lcom/google/android/play/core/tasks/Task;

    goto :goto_0

    :cond_3
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    :cond_4
    :goto_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    const/16 v0, 0x33

    invoke-virtual {p1, v0}, Landroid/view/Window;->setGravity(I)V

    invoke-virtual {p1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    const/4 v1, 0x1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    const/16 v1, 0x7d2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v1, 0x20

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    invoke-virtual {p1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object p1

    check-cast p1, Lcom/android/settings/SettingsApplication;

    iget p1, p1, Lcom/android/settings/SettingsApplication;->mQigsawStarted:I

    sget v0, Lcom/android/settings/bluetooth/QigsawInstaller;->HEADSETPLUGIN_INITED:I

    if-eq p1, v0, :cond_0

    const-string p1, "QigsawInstaller"

    const-string/jumbo v0, "the qigsaw does not start up"

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_0
    invoke-static {p0}, Lcom/google/android/play/core/splitinstall/SplitInstallManagerFactory;->create(Landroid/content/Context;)Lcom/google/android/play/core/splitinstall/SplitInstallManager;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->mInstallManager:Lcom/google/android/play/core/splitinstall/SplitInstallManager;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string/jumbo v0, "moduleNames"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    iput-object p1, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->mModuleNames:Ljava/util/ArrayList;

    goto :goto_1

    :cond_2
    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_1
    iget-object p1, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->mInstallManager:Lcom/google/android/play/core/splitinstall/SplitInstallManager;

    iget-object p0, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->myListener:Lcom/google/android/play/core/splitinstall/SplitInstallStateUpdatedListener;

    invoke-interface {p1, p0}, Lcom/google/android/play/core/splitinstall/SplitInstallManager;->registerListener(Lcom/google/android/play/core/splitinstall/SplitInstallStateUpdatedListener;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->mInstallManager:Lcom/google/android/play/core/splitinstall/SplitInstallManager;

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->myListener:Lcom/google/android/play/core/splitinstall/SplitInstallStateUpdatedListener;

    invoke-interface {v0, p0}, Lcom/google/android/play/core/splitinstall/SplitInstallManager;->unregisterListener(Lcom/google/android/play/core/splitinstall/SplitInstallStateUpdatedListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method protected onInstallOK()V
    .locals 1

    const-string p0, "QigsawInstaller"

    const-string/jumbo v0, "on install ok"

    invoke-static {p0, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-boolean v0, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->mFirstStartup:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/bluetooth/QigsawInstaller;->startInstall()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/bluetooth/QigsawInstaller;->mFirstStartup:Z

    return-void
.end method
