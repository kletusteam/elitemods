.class public Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;
.super Lmiuix/appcompat/app/AppCompatActivity;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field private mAppLabel:Ljava/lang/CharSequence;

.field private mApplicationName:Ljava/lang/String;

.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mDialog:Lmiuix/appcompat/app/AlertDialog;

.field private mRequest:I

.field private mTimeout:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiuix/appcompat/app/AppCompatActivity;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mTimeout:I

    return-void
.end method

.method private parseIntent()Z
    .locals 5

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    const-string v2, "ApplicationName"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mApplicationName:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.android.settings.bluetooth.ACTION_INTERNAL_REQUEST_BT_ON"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_1

    iput v4, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mRequest:I

    const-string v2, "android.bluetooth.adapter.extra.DISCOVERABLE_DURATION"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v3, 0x78

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mTimeout:I

    goto :goto_0

    :cond_1
    const-string v0, "com.android.settings.bluetooth.ACTION_INTERNAL_REQUEST_BT_OFF"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x3

    iput v0, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mRequest:I

    :cond_2
    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "com.android.settings.bluetooth.extra.APP_LABEL"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mAppLabel:Ljava/lang/CharSequence;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v0, :cond_3

    const-string p0, "RequestPermissionHelperActivity"

    const-string v0, "Error: there\'s a problem starting Bluetooth"

    invoke-static {p0, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_3
    return v4

    :cond_4
    return v1
.end method


# virtual methods
.method createDialog()V
    .locals 7

    goto/32 :goto_90

    nop

    :goto_0
    const/4 v3, 0x1

    goto/32 :goto_71

    nop

    :goto_1
    invoke-virtual {p0, v4, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_69

    nop

    :goto_2
    sget v4, Lcom/android/settings/R$string;->bluetooth_ask_enablement_and_discovery_detected:I

    goto/32 :goto_22

    nop

    :goto_3
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_2

    nop

    :goto_4
    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_5
    goto/32 :goto_1c

    nop

    :goto_6
    invoke-virtual {p0, v4, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_16

    nop

    :goto_7
    new-array v3, v3, [Ljava/lang/Object;

    goto/32 :goto_5f

    nop

    :goto_8
    if-nez v4, :cond_0

    goto/32 :goto_41

    :cond_0
    goto/32 :goto_8f

    nop

    :goto_9
    new-array v3, v3, [Ljava/lang/Object;

    goto/32 :goto_2f

    nop

    :goto_a
    sget v1, Lcom/android/settings/R$string;->bluetooth_ask_enablement_and_lasting_discovery_no_name:I

    goto/32 :goto_89

    nop

    :goto_b
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_3d

    nop

    :goto_c
    invoke-virtual {p0, v5, v6}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_37

    nop

    :goto_d
    goto/16 :goto_5e

    :goto_e
    goto/32 :goto_6b

    nop

    :goto_f
    iget-object v4, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mAppLabel:Ljava/lang/CharSequence;

    goto/32 :goto_1d

    nop

    :goto_10
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_91

    nop

    :goto_11
    sget v4, Lcom/android/settings/R$string;->bluetooth_ask_enablement:I

    goto/32 :goto_3b

    nop

    :goto_12
    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_8d

    nop

    :goto_13
    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_85

    nop

    :goto_14
    if-ne v1, v4, :cond_1

    goto/32 :goto_81

    :cond_1
    goto/32 :goto_80

    nop

    :goto_15
    iget-object v1, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mAppLabel:Ljava/lang/CharSequence;

    goto/32 :goto_5b

    nop

    :goto_16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_21

    nop

    :goto_17
    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    goto/32 :goto_50

    nop

    :goto_18
    aput-object v1, v3, v2

    goto/32 :goto_1

    nop

    :goto_19
    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_49

    nop

    :goto_1a
    invoke-virtual {v0, v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    goto/32 :goto_8c

    nop

    :goto_1b
    aput-object v1, v3, v2

    goto/32 :goto_45

    nop

    :goto_1c
    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    goto/32 :goto_28

    nop

    :goto_1d
    if-nez v4, :cond_2

    goto/32 :goto_38

    :cond_2
    goto/32 :goto_84

    nop

    :goto_1e
    aput-object v1, v6, v3

    goto/32 :goto_c

    nop

    :goto_1f
    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_36

    nop

    :goto_20
    iget-object v1, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mAppLabel:Ljava/lang/CharSequence;

    goto/32 :goto_7d

    nop

    :goto_21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_40

    nop

    :goto_22
    new-array v3, v3, [Ljava/lang/Object;

    goto/32 :goto_48

    nop

    :goto_23
    iput-object v0, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    goto/32 :goto_35

    nop

    :goto_24
    sget v1, Lcom/android/settings/R$string;->bluetooth_ask_enablement_and_lasting_discovery_no_name:I

    goto/32 :goto_4

    nop

    :goto_25
    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    goto/32 :goto_26

    nop

    :goto_26
    goto/16 :goto_5e

    :goto_27
    goto/32 :goto_f

    nop

    :goto_28
    goto/16 :goto_5e

    :goto_29
    goto/32 :goto_93

    nop

    :goto_2a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_7e

    nop

    :goto_2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_56

    nop

    :goto_2c
    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    goto/32 :goto_31

    nop

    :goto_2d
    new-array v3, v3, [Ljava/lang/Object;

    goto/32 :goto_b

    nop

    :goto_2e
    const/4 v2, 0x0

    goto/32 :goto_0

    nop

    :goto_2f
    aput-object v1, v3, v2

    goto/32 :goto_62

    nop

    :goto_30
    sget v1, Lcom/android/settings/R$string;->bluetooth_ask_disablement_no_name:I

    goto/32 :goto_7b

    nop

    :goto_31
    goto/16 :goto_5e

    :goto_32
    goto/32 :goto_77

    nop

    :goto_33
    iget-object v2, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mApplicationName:Ljava/lang/String;

    goto/32 :goto_2b

    nop

    :goto_34
    invoke-virtual {p0, v4, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_3e

    nop

    :goto_35
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto/32 :goto_6f

    nop

    :goto_36
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_47

    nop

    :goto_37
    goto :goto_46

    :goto_38
    goto/32 :goto_4d

    nop

    :goto_39
    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object v0

    goto/32 :goto_23

    nop

    :goto_3a
    sget v4, Lcom/android/settings/R$string;->bluetooth_ask_enablement_and_discovery_no_name:I

    goto/32 :goto_2d

    nop

    :goto_3b
    new-array v3, v3, [Ljava/lang/Object;

    goto/32 :goto_18

    nop

    :goto_3c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_4a

    nop

    :goto_3d
    aput-object v1, v3, v2

    goto/32 :goto_86

    nop

    :goto_3e
    goto/16 :goto_5

    :goto_3f
    goto/32 :goto_24

    nop

    :goto_40
    goto/16 :goto_87

    :goto_41
    goto/32 :goto_3a

    nop

    :goto_42
    if-ltz v1, :cond_3

    goto/32 :goto_51

    :cond_3
    goto/32 :goto_52

    nop

    :goto_43
    iget-object v1, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mApplicationName:Ljava/lang/String;

    goto/32 :goto_57

    nop

    :goto_44
    iget-object v1, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mAppLabel:Ljava/lang/CharSequence;

    goto/32 :goto_6e

    nop

    :goto_45
    invoke-virtual {p0, v4, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_46
    goto/32 :goto_5d

    nop

    :goto_47
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_53

    nop

    :goto_48
    iget v5, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mTimeout:I

    goto/32 :goto_88

    nop

    :goto_49
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_3c

    nop

    :goto_4a
    goto/16 :goto_67

    :goto_4b
    goto/32 :goto_72

    nop

    :goto_4c
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_7a

    nop

    :goto_4d
    sget v4, Lcom/android/settings/R$string;->bluetooth_ask_enablement_and_discovery_no_name:I

    goto/32 :goto_7

    nop

    :goto_4e
    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_4f
    goto/32 :goto_17

    nop

    :goto_50
    goto/16 :goto_5e

    :goto_51
    goto/32 :goto_6c

    nop

    :goto_52
    iget-object v1, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mApplicationName:Ljava/lang/String;

    goto/32 :goto_75

    nop

    :goto_53
    goto/16 :goto_8a

    :goto_54
    goto/32 :goto_a

    nop

    :goto_55
    if-nez v4, :cond_4

    goto/32 :goto_27

    :cond_4
    goto/32 :goto_7f

    nop

    :goto_56
    sget v2, Lcom/android/settings/R$string;->bluetooth_ask_enablement_detected:I

    goto/32 :goto_19

    nop

    :goto_57
    if-nez v1, :cond_5

    goto/32 :goto_32

    :cond_5
    goto/32 :goto_73

    nop

    :goto_58
    aput-object v4, v6, v2

    goto/32 :goto_82

    nop

    :goto_59
    iget v1, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mRequest:I

    goto/32 :goto_2e

    nop

    :goto_5a
    sget v1, Lcom/android/settings/R$string;->allow:I

    goto/32 :goto_13

    nop

    :goto_5b
    if-nez v1, :cond_6

    goto/32 :goto_4b

    :cond_6
    goto/32 :goto_4c

    nop

    :goto_5c
    const/4 v6, 0x2

    goto/32 :goto_95

    nop

    :goto_5d
    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    :goto_5e
    goto/32 :goto_5a

    nop

    :goto_5f
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_1b

    nop

    :goto_60
    goto/16 :goto_5e

    :goto_61
    goto/32 :goto_20

    nop

    :goto_62
    invoke-virtual {p0, v4, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_64

    nop

    :goto_63
    const/4 v4, 0x3

    goto/32 :goto_14

    nop

    :goto_64
    goto/16 :goto_7c

    :goto_65
    goto/32 :goto_30

    nop

    :goto_66
    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_67
    goto/32 :goto_96

    nop

    :goto_68
    aput-object v5, v3, v2

    goto/32 :goto_6

    nop

    :goto_69
    goto/16 :goto_4f

    :goto_6a
    goto/32 :goto_98

    nop

    :goto_6b
    iget v1, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mTimeout:I

    goto/32 :goto_42

    nop

    :goto_6c
    if-eqz v1, :cond_7

    goto/32 :goto_29

    :cond_7
    goto/32 :goto_43

    nop

    :goto_6d
    sget v1, Lcom/android/settings/R$string;->deny:I

    goto/32 :goto_12

    nop

    :goto_6e
    if-nez v1, :cond_8

    goto/32 :goto_65

    :cond_8
    goto/32 :goto_76

    nop

    :goto_6f
    return-void

    :goto_70
    if-nez v1, :cond_9

    goto/32 :goto_3f

    :cond_9
    goto/32 :goto_97

    nop

    :goto_71
    if-ne v1, v3, :cond_a

    goto/32 :goto_e

    :cond_a
    goto/32 :goto_63

    nop

    :goto_72
    sget v1, Lcom/android/settings/R$string;->bluetooth_ask_enablement_no_name:I

    goto/32 :goto_66

    nop

    :goto_73
    iget-object v1, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mAppLabel:Ljava/lang/CharSequence;

    goto/32 :goto_8e

    nop

    :goto_74
    aput-object v1, v3, v2

    goto/32 :goto_34

    nop

    :goto_75
    if-nez v1, :cond_b

    goto/32 :goto_61

    :cond_b
    goto/32 :goto_15

    nop

    :goto_76
    sget v4, Lcom/android/settings/R$string;->bluetooth_ask_disablement:I

    goto/32 :goto_9

    nop

    :goto_77
    iget-object v1, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mAppLabel:Ljava/lang/CharSequence;

    goto/32 :goto_70

    nop

    :goto_78
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_8b

    nop

    :goto_79
    iget-object v2, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mApplicationName:Ljava/lang/String;

    goto/32 :goto_78

    nop

    :goto_7a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_33

    nop

    :goto_7b
    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_7c
    goto/32 :goto_83

    nop

    :goto_7d
    if-nez v1, :cond_c

    goto/32 :goto_6a

    :cond_c
    goto/32 :goto_11

    nop

    :goto_7e
    iget-object v4, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mApplicationName:Ljava/lang/String;

    goto/32 :goto_3

    nop

    :goto_7f
    iget-object v4, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mAppLabel:Ljava/lang/CharSequence;

    goto/32 :goto_8

    nop

    :goto_80
    goto/16 :goto_5e

    :goto_81
    goto/32 :goto_44

    nop

    :goto_82
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_1e

    nop

    :goto_83
    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    goto/32 :goto_d

    nop

    :goto_84
    sget v5, Lcom/android/settings/R$string;->bluetooth_ask_enablement_and_discovery:I

    goto/32 :goto_5c

    nop

    :goto_85
    invoke-virtual {v0, v1, p0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    goto/32 :goto_6d

    nop

    :goto_86
    invoke-virtual {p0, v4, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_87
    goto/32 :goto_25

    nop

    :goto_88
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto/32 :goto_68

    nop

    :goto_89
    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_8a
    goto/32 :goto_2c

    nop

    :goto_8b
    sget v2, Lcom/android/settings/R$string;->bluetooth_ask_enablement_and_lasting_discovery_detected:I

    goto/32 :goto_1f

    nop

    :goto_8c
    invoke-virtual {v0, p0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    goto/32 :goto_39

    nop

    :goto_8d
    const/4 v2, 0x0

    goto/32 :goto_1a

    nop

    :goto_8e
    if-nez v1, :cond_d

    goto/32 :goto_54

    :cond_d
    goto/32 :goto_10

    nop

    :goto_8f
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_2a

    nop

    :goto_90
    new-instance v0, Lmiuix/appcompat/app/AlertDialog$Builder;

    goto/32 :goto_94

    nop

    :goto_91
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_79

    nop

    :goto_92
    new-array v3, v3, [Ljava/lang/Object;

    goto/32 :goto_74

    nop

    :goto_93
    iget-object v4, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mApplicationName:Ljava/lang/String;

    goto/32 :goto_55

    nop

    :goto_94
    invoke-direct {v0, p0}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    goto/32 :goto_59

    nop

    :goto_95
    new-array v6, v6, [Ljava/lang/Object;

    goto/32 :goto_58

    nop

    :goto_96
    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    goto/32 :goto_60

    nop

    :goto_97
    sget v4, Lcom/android/settings/R$string;->bluetooth_ask_enablement_and_lasting_discovery:I

    goto/32 :goto_92

    nop

    :goto_98
    sget v1, Lcom/android/settings/R$string;->bluetooth_ask_enablement_no_name:I

    goto/32 :goto_4e

    nop
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    iget p1, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mRequest:I

    const/4 p2, 0x1

    const/4 v0, -0x1

    if-eq p1, p2, :cond_1

    const/4 p2, 0x2

    if-eq p1, p2, :cond_1

    const/4 p2, 0x3

    if-eq p1, p2, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setResult(I)V

    goto :goto_0

    :cond_1
    const-class p1, Landroid/os/UserManager;

    invoke-virtual {p0, p1}, Landroid/app/Activity;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/os/UserManager;

    const-string/jumbo p2, "no_bluetooth"

    invoke-virtual {p1, p2}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    const-class p1, Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {p0, p1}, Landroid/app/Activity;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {p1, p2}, Landroid/app/admin/DevicePolicyManager;->createAdminSupportIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/android/settingslib/bluetooth/LocalBluetoothAdapter;->getInstance()Lcom/android/settingslib/bluetooth/LocalBluetoothAdapter;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/android/settingslib/bluetooth/LocalBluetoothAdapter;->getBluetoothRestrictState(Landroid/content/Context;)Z

    move-result p2

    if-nez p2, :cond_3

    iget-object p2, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setResult(I)V

    :cond_3
    const/4 p2, 0x0

    invoke-virtual {p1, p0, p2}, Lcom/android/settingslib/bluetooth/LocalBluetoothAdapter;->enableBluetoothRestrict(Landroid/content/Context;Z)Z

    iget-object p1, p0, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setResult(I)V

    :cond_4
    :goto_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/app/Activity;->setResult(I)V

    invoke-direct {p0}, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->parseIntent()Z

    move-result p1

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/android/settings/R$bool;->auto_confirm_bluetooth_activation_dialog:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->onClick(Landroid/content/DialogInterface;I)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/bluetooth/RequestPermissionHelperActivity;->createDialog()V

    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 0

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    return-void
.end method
