.class public Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog;
.super Lcom/android/settings/core/instrumentation/InstrumentedDialogFragment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog$BluetoothProfileConfirmListener;
    }
.end annotation


# instance fields
.field private mGroupId:I

.field private mGroupUtils:Lcom/android/settings/bluetooth/GroupUtils;

.field private mProfileController:Lcom/android/settings/bluetooth/BluetoothDetailsProfilesController;


# direct methods
.method public static synthetic $r8$lambda$spd4SmiRoRBZW9Cbp0BJMyMQc8s(Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog;->lambda$onCreateDialog$0(Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$tsosuhBG2_a-nPRuClAUzWTfwyQ(Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog;->lambda$onCreateDialog$1(Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/core/instrumentation/InstrumentedDialogFragment;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog;->mGroupId:I

    return-void
.end method

.method private synthetic lambda$onCreateDialog$0(Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog;->onPositiveButtonClicked()V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method private synthetic lambda$onCreateDialog$1(Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog;->onNegativeButtonClicked()V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method public static newInstance(I)Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog;
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    const-string v1, "group_id"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance p0, Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog;

    invoke-direct {p0}, Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog;-><init>()V

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object p0
.end method

.method private onNegativeButtonClicked()V
    .locals 0

    iget-object p0, p0, Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog;->mProfileController:Lcom/android/settings/bluetooth/BluetoothDetailsProfilesController;

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothDetailsProfilesController;->onDialogNegativeClick()V

    return-void
.end method

.method private onPositiveButtonClicked()V
    .locals 0

    iget-object p0, p0, Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog;->mProfileController:Lcom/android/settings/bluetooth/BluetoothDetailsProfilesController;

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothDetailsProfilesController;->onDialogPositiveClick()V

    return-void
.end method


# virtual methods
.method getGroupTitle()Ljava/lang/String;
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/GroupUtils;->getGroupTitle(I)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_4

    nop

    :goto_1
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    goto/32 :goto_6

    nop

    :goto_2
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_3
    const-string v1, "group_id"

    goto/32 :goto_1

    nop

    :goto_4
    return-object p0

    :goto_5
    iget-object p0, p0, Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog;->mGroupUtils:Lcom/android/settings/bluetooth/GroupUtils;

    goto/32 :goto_0

    nop

    :goto_6
    iput v0, p0, Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog;->mGroupId:I

    goto/32 :goto_5

    nop
.end method

.method public getMetricsCategory()I
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method isPairingControllerSet()Z
    .locals 0

    goto/32 :goto_4

    nop

    :goto_0
    return p0

    :goto_1
    if-nez p0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_5

    nop

    :goto_2
    const/4 p0, 0x0

    :goto_3
    goto/32 :goto_0

    nop

    :goto_4
    iget-object p0, p0, Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog;->mProfileController:Lcom/android/settings/bluetooth/BluetoothDetailsProfilesController;

    goto/32 :goto_1

    nop

    :goto_5
    const/4 p0, 0x1

    goto/32 :goto_6

    nop

    :goto_6
    goto :goto_3

    :goto_7
    goto/32 :goto_2

    nop
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    new-instance p1, Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog$$ExternalSyntheticLambda0;

    invoke-direct {p1, p0}, Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog;)V

    new-instance v0, Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog$$ExternalSyntheticLambda1;-><init>(Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/android/settings/bluetooth/GroupUtils;

    invoke-direct {v2, v1}, Lcom/android/settings/bluetooth/GroupUtils;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog;->mGroupUtils:Lcom/android/settings/bluetooth/GroupUtils;

    new-instance v2, Landroidx/appcompat/app/AlertDialog$Builder;

    invoke-direct {v2, v1}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v3, Lcom/android/settings/R$string;->group_confirm_dialog_apply_button:I

    invoke-virtual {v2, v3, p1}, Landroidx/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    const/high16 v2, 0x1040000

    invoke-virtual {p1, v2, v0}, Landroidx/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/appcompat/app/AlertDialog$Builder;->create()Landroidx/appcompat/app/AlertDialog;

    move-result-object p1

    sget v0, Lcom/android/settings/R$string;->group_apply_changes_dialog_title:I

    invoke-virtual {p1, v0}, Landroidx/appcompat/app/AppCompatDialog;->setTitle(I)V

    sget v0, Lcom/android/settings/R$string;->group_confirm_dialog_body:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog;->getGroupTitle()Ljava/lang/String;

    move-result-object p0

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroidx/appcompat/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    return-object p1
.end method

.method setPairingController(Lcom/android/settings/bluetooth/BluetoothDetailsProfilesController;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    if-eqz v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_8

    nop

    :goto_1
    invoke-virtual {p0}, Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog;->isPairingControllerSet()Z

    move-result v0

    goto/32 :goto_0

    nop

    :goto_2
    const-string p1, "The controller can only be set once. Forcibly replacing it will lead to undefined behavior"

    goto/32 :goto_4

    nop

    :goto_3
    throw p0

    :goto_4
    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_3

    nop

    :goto_5
    new-instance p0, Ljava/lang/IllegalStateException;

    goto/32 :goto_2

    nop

    :goto_6
    return-void

    :goto_7
    goto/32 :goto_5

    nop

    :goto_8
    iput-object p1, p0, Lcom/android/settings/bluetooth/GroupBluetoothProfileSwitchConfirmDialog;->mProfileController:Lcom/android/settings/bluetooth/BluetoothDetailsProfilesController;

    goto/32 :goto_6

    nop
.end method
