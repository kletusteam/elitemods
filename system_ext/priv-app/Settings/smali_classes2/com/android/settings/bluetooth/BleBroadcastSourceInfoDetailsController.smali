.class public Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;
.super Lcom/android/settings/bluetooth/BluetoothDetailsController;

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceClickListener;
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private final EMPTY_BD_ADDRESS:Ljava/lang/String;

.field private EMPTY_ENTRY:Ljava/lang/String;

.field private isBroadcastPINUpdated:Z

.field private mAudioSyncState:I

.field private mBisIndicies:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/bluetooth/BleBroadcastSourceChannel;",
            ">;"
        }
    .end annotation
.end field

.field private mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

.field private mBroadcastCode:Ljava/lang/String;

.field private mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

.field private mGroupOp:Z

.field private mIsButtonRefreshOnly:Z

.field private mIsValueChanged:Z

.field private mMetadataSyncState:I

.field private mPAsyncCtrlNeeded:Z

.field private mScanAssistGroupOpDialog:Landroidx/appcompat/app/AlertDialog;

.field private mScanAssistanceMgr:Landroid/bluetooth/BleBroadcastAudioScanAssistManager;

.field private mSourceAudioSyncStatusPref:Landroidx/preference/MultiSelectListPreference;

.field private mSourceAudioSyncSwitchPref:Landroidx/preference/SwitchPreference;

.field private mSourceDevicePref:Landroidx/preference/Preference;

.field private mSourceEncStatusPref:Landroidx/preference/Preference;

.field private mSourceIdPref:Landroidx/preference/Preference;

.field private mSourceInfoContainer:Landroidx/preference/PreferenceCategory;

.field private mSourceInfoIndex:I

.field private mSourceMetadataPref:Landroidx/preference/Preference;

.field private mSourceMetadataSyncStatusPref:Landroidx/preference/Preference;

.field private mSourceMetadataSyncSwitchPref:Landroidx/preference/SwitchPreference;

.field private mSourceUpdateBcastCodePref:Landroidx/preference/EditTextPreference;

.field private mSourceUpdateSourceInfoPref:Lcom/android/settingslib/widget/ActionButtonsPreference;

.field private mVendorCachedDevice:Lcom/android/settingslib/bluetooth/VendorCachedBluetoothDevice;


# direct methods
.method public static synthetic $r8$lambda$VUXXwVXORd55aCIvYfJBSDt0SIc(Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->lambda$init$0(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$tAvtxs36q5E_37EQTqS1iJs-UXU(Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->lambda$init$1(Landroid/view/View;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmScanAssistGroupOpDialog(Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;)Landroidx/appcompat/app/AlertDialog;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mScanAssistGroupOpDialog:Landroidx/appcompat/app/AlertDialog;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmGroupOp(Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mGroupOp:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmScanAssistGroupOpDialog(Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;Landroidx/appcompat/app/AlertDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mScanAssistGroupOpDialog:Landroidx/appcompat/app/AlertDialog;

    return-void
.end method

.method static bridge synthetic -$$Nest$mtriggerRemoveBroadcastSource(Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->triggerRemoveBroadcastSource()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mtriggerUpdateBroadcastSource(Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->triggerUpdateBroadcastSource()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroidx/preference/PreferenceFragmentCompat;Landroid/bluetooth/BleBroadcastSourceInfo;Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;ILcom/android/settingslib/core/lifecycle/Lifecycle;)V
    .locals 1

    invoke-direct {p0, p1, p2, p4, p6}, Lcom/android/settings/bluetooth/BluetoothDetailsController;-><init>(Landroid/content/Context;Landroidx/preference/PreferenceFragmentCompat;Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;Lcom/android/settingslib/core/lifecycle/Lifecycle;)V

    const-string p2, "00:00:00:00:00:00"

    iput-object p2, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->EMPTY_BD_ADDRESS:Ljava/lang/String;

    const/4 p2, 0x0

    iput-boolean p2, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mIsValueChanged:Z

    iput-boolean p2, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->isBroadcastPINUpdated:Z

    const-string v0, "EMPTY ENTRY"

    iput-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->EMPTY_ENTRY:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mIsButtonRefreshOnly:Z

    iput-boolean p2, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mGroupOp:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mScanAssistGroupOpDialog:Landroidx/appcompat/app/AlertDialog;

    iput-boolean p2, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mPAsyncCtrlNeeded:Z

    iput-object p3, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    iput-object p4, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    invoke-static {p1}, Lcom/android/settings/bluetooth/Utils;->getLocalBtManager(Landroid/content/Context;)Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    move-result-object p1

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/LocalBluetoothManager;->getProfileManager()Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;

    move-result-object p1

    invoke-static {p4, p1}, Lcom/android/settingslib/bluetooth/VendorCachedBluetoothDevice;->getVendorCachedBluetoothDevice(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;)Lcom/android/settingslib/bluetooth/VendorCachedBluetoothDevice;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mVendorCachedDevice:Lcom/android/settingslib/bluetooth/VendorCachedBluetoothDevice;

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/VendorCachedBluetoothDevice;->getScanAssistManager()Landroid/bluetooth/BleBroadcastAudioScanAssistManager;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mScanAssistanceMgr:Landroid/bluetooth/BleBroadcastAudioScanAssistManager;

    invoke-virtual {p6, p0}, Lcom/android/settingslib/core/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    iput p5, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoIndex:I

    invoke-direct {p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->clearInputs()V

    iput-boolean p2, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mPAsyncCtrlNeeded:Z

    return-void
.end method

.method private clearInputs()V
    .locals 1

    const/4 v0, 0x2

    iput v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mMetadataSyncState:I

    const v0, 0xffff

    iput v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mAudioSyncState:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBroadcastCode:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->isBroadcastPINUpdated:Z

    return-void
.end method

.method private forceUpdateBroadcastSourceToLoseSync()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/bluetooth/BleBroadcastSourceInfo;->getMetadataSyncState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const-string v0, "BleBroadcastSourceInfoDetailsController"

    const-string/jumbo v1, "triggerUpdateBroadcastsource with PA off"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mMetadataSyncState:I

    invoke-direct {p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->triggerUpdateBroadcastSource()V

    const-wide/16 v0, 0xc8

    invoke-static {v0, v1}, Landroid/os/SystemClock;->sleep(J)V

    :cond_0
    return-void
.end method

.method private getSyncState(II)I
    .locals 1

    const/4 p0, 0x1

    const/4 v0, 0x2

    if-ne p2, p0, :cond_0

    if-ne p1, v0, :cond_0

    return v0

    :cond_0
    if-eq p1, v0, :cond_1

    return p0

    :cond_1
    if-eq p2, p0, :cond_2

    if-ne p1, v0, :cond_2

    const/4 p0, 0x0

    return p0

    :cond_2
    const/4 p0, -0x1

    return p0
.end method

.method private isPinUpdatedNeeded()Z
    .locals 3

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {v0}, Landroid/bluetooth/BleBroadcastSourceInfo;->getSourceDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->isLocalDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    const-string v1, "BleBroadcastSourceInfoDetailsController"

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    const-string p0, "Local Device, Dont allow User to update PWD"

    invoke-static {v1, p0}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    return v2

    :cond_0
    iget-object p0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {p0}, Landroid/bluetooth/BleBroadcastSourceInfo;->getEncryptionStatus()I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_1

    move v2, v0

    :cond_1
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "isPinUpdatedNeeded return"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    return v2
.end method

.method private synthetic lambda$init$0(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->onUpdateBroadcastSourceInfoPressed()V

    return-void
.end method

.method private synthetic lambda$init$1(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->onRemoveBroadcastSourceInfoPressed()V

    return-void
.end method

.method private onRemoveBroadcastSourceInfoPressed()V
    .locals 10

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ":onRemoveBroadcastSourceInfoPressed:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BleBroadcastSourceInfoDetailsController"

    invoke-static {v1, v0}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->isGroupDevice()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDetailsController;->mContext:Landroid/content/Context;

    sget v2, Lcom/android/settings/R$string;->bluetooth_device:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothDetailsController;->mContext:Landroid/content/Context;

    sget v3, Lcom/android/settings/R$string;->group_remove_source_message:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothDetailsController;->mContext:Landroid/content/Context;

    sget v3, Lcom/android/settings/R$string;->group_remove_source_title:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-instance v6, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController$1;

    invoke-direct {v6, p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController$1;-><init>(Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;)V

    new-instance v7, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController$2;

    invoke-direct {v7, p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController$2;-><init>(Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;)V

    iput-boolean v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mGroupOp:Z

    iget-object v4, p0, Lcom/android/settings/bluetooth/BluetoothDetailsController;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mScanAssistGroupOpDialog:Landroidx/appcompat/app/AlertDialog;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v9

    invoke-static/range {v4 .. v9}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->showAssistanceGroupOptionsDialog(Landroid/content/Context;Landroidx/appcompat/app/AlertDialog;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroidx/appcompat/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mScanAssistGroupOpDialog:Landroidx/appcompat/app/AlertDialog;

    goto :goto_0

    :cond_1
    iput-boolean v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mGroupOp:Z

    invoke-direct {p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->triggerRemoveBroadcastSource()V

    :goto_0
    return-void
.end method

.method private onUpdateBroadcastSourceInfoPressed()V
    .locals 10

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "onUpdateBroadcastSourceInfoPressed:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BleBroadcastSourceInfoDetailsController"

    invoke-static {v1, v0}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->isGroupDevice()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothDetailsController;->mContext:Landroid/content/Context;

    sget v2, Lcom/android/settings/R$string;->bluetooth_device:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothDetailsController;->mContext:Landroid/content/Context;

    sget v3, Lcom/android/settings/R$string;->group_update_source_message:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothDetailsController;->mContext:Landroid/content/Context;

    sget v3, Lcom/android/settings/R$string;->group_update_source_title:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-instance v6, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController$3;

    invoke-direct {v6, p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController$3;-><init>(Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;)V

    new-instance v7, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController$4;

    invoke-direct {v7, p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController$4;-><init>(Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;)V

    iput-boolean v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mGroupOp:Z

    iget-object v4, p0, Lcom/android/settings/bluetooth/BluetoothDetailsController;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mScanAssistGroupOpDialog:Landroidx/appcompat/app/AlertDialog;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v9

    invoke-static/range {v4 .. v9}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->showAssistanceGroupOptionsDialog(Landroid/content/Context;Landroidx/appcompat/app/AlertDialog;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroidx/appcompat/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mScanAssistGroupOpDialog:Landroidx/appcompat/app/AlertDialog;

    goto :goto_0

    :cond_1
    iput-boolean v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mGroupOp:Z

    invoke-direct {p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->triggerUpdateBroadcastSource()V

    :goto_0
    return-void
.end method

.method private triggerRemoveBroadcastSource()V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->forceUpdateBroadcastSourceToLoseSync()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mScanAssistanceMgr:Landroid/bluetooth/BleBroadcastAudioScanAssistManager;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {v1}, Landroid/bluetooth/BleBroadcastSourceInfo;->getSourceId()B

    move-result v1

    iget-boolean p0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mGroupOp:Z

    invoke-virtual {v0, v1, p0}, Landroid/bluetooth/BleBroadcastAudioScanAssistManager;->removeBroadcastSource(BZ)Z

    :cond_0
    return-void
.end method

.method private triggerUpdateBroadcastSource()V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mScanAssistanceMgr:Landroid/bluetooth/BleBroadcastAudioScanAssistManager;

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mMetadataSyncState:I

    iget v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mAudioSyncState:I

    invoke-direct {p0, v0, v1}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->getSyncState(II)I

    move-result v0

    const/4 v1, -0x1

    const-string v2, "BleBroadcastSourceInfoDetailsController"

    if-ne v0, v1, :cond_0

    const-string/jumbo p0, "triggerUpdateBroadcastSource: Invalid sync Input, Ignore"

    invoke-static {v2, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "triggerUpdateBroadcastSource:  "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mScanAssistanceMgr:Landroid/bluetooth/BleBroadcastAudioScanAssistManager;

    iget-object v2, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {v2}, Landroid/bluetooth/BleBroadcastSourceInfo;->getSourceId()B

    move-result v2

    iget-object v3, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBisIndicies:Ljava/util/List;

    iget-boolean v4, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mGroupOp:Z

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/bluetooth/BleBroadcastAudioScanAssistManager;->updateBroadcastSource(BILjava/util/List;Z)Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mIsValueChanged:Z

    iget-boolean v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->isBroadcastPINUpdated:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mScanAssistanceMgr:Landroid/bluetooth/BleBroadcastAudioScanAssistManager;

    iget-object v2, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {v2}, Landroid/bluetooth/BleBroadcastSourceInfo;->getSourceId()B

    move-result v2

    iget-object v3, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBroadcastCode:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mGroupOp:Z

    invoke-virtual {v1, v2, v3, v4}, Landroid/bluetooth/BleBroadcastAudioScanAssistManager;->setBroadcastCode(BLjava/lang/String;Z)Z

    iput-boolean v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->isBroadcastPINUpdated:Z

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->clearInputs()V

    :cond_2
    return-void
.end method


# virtual methods
.method getAudioSyncStatusString(I)Ljava/lang/String;
    .locals 0

    goto/32 :goto_6

    nop

    :goto_0
    const-string p0, "IN SYNC"

    goto/32 :goto_3

    nop

    :goto_1
    const-string p0, "UNKNOWN"

    goto/32 :goto_8

    nop

    :goto_2
    const/4 p0, 0x1

    goto/32 :goto_5

    nop

    :goto_3
    return-object p0

    :goto_4
    goto/32 :goto_a

    nop

    :goto_5
    if-ne p1, p0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_1

    nop

    :goto_6
    if-nez p1, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_2

    nop

    :goto_7
    return-object p0

    :goto_8
    return-object p0

    :goto_9
    goto/32 :goto_0

    nop

    :goto_a
    const-string p0, "NOT IN SYNC"

    goto/32 :goto_7

    nop
.end method

.method getEncryptionStatusString(I)Ljava/lang/String;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    if-nez p1, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_3

    nop

    :goto_1
    return-object p0

    :goto_2
    goto/32 :goto_f

    nop

    :goto_3
    const/4 p0, 0x1

    goto/32 :goto_13

    nop

    :goto_4
    return-object p0

    :goto_5
    goto/32 :goto_11

    nop

    :goto_6
    return-object p0

    :goto_7
    goto/32 :goto_b

    nop

    :goto_8
    const/4 p0, 0x3

    goto/32 :goto_14

    nop

    :goto_9
    const/4 p0, 0x2

    goto/32 :goto_12

    nop

    :goto_a
    return-object p0

    :goto_b
    const-string p0, "INCORRECT BROADCAST PIN"

    goto/32 :goto_4

    nop

    :goto_c
    const-string p0, "UNENCRYPTED STREAMING"

    goto/32 :goto_a

    nop

    :goto_d
    return-object p0

    :goto_e
    goto/32 :goto_c

    nop

    :goto_f
    const-string p0, "PIN UPDATE NEEDED"

    goto/32 :goto_d

    nop

    :goto_10
    const-string p0, "ENCRYPTION STATE UNKNOWN"

    goto/32 :goto_6

    nop

    :goto_11
    const-string p0, "DECRYPTING SUCCESSFULLY"

    goto/32 :goto_1

    nop

    :goto_12
    if-ne p1, p0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_8

    nop

    :goto_13
    if-ne p1, p0, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_9

    nop

    :goto_14
    if-ne p1, p0, :cond_3

    goto/32 :goto_7

    :cond_3
    goto/32 :goto_10

    nop
.end method

.method getMetadataSyncStatusString(I)Ljava/lang/String;
    .locals 0

    goto/32 :goto_e

    nop

    :goto_0
    const/4 p0, 0x4

    goto/32 :goto_16

    nop

    :goto_1
    return-object p0

    :goto_2
    goto/32 :goto_8

    nop

    :goto_3
    const-string p0, "NO PAST"

    goto/32 :goto_f

    nop

    :goto_4
    return-object p0

    :goto_5
    goto/32 :goto_3

    nop

    :goto_6
    const/4 p0, 0x1

    goto/32 :goto_7

    nop

    :goto_7
    if-ne p1, p0, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_18

    nop

    :goto_8
    const-string p0, "IDLE"

    goto/32 :goto_17

    nop

    :goto_9
    const-string p0, "IN SYNC"

    goto/32 :goto_11

    nop

    :goto_a
    const-string p0, "UNKNOWN"

    goto/32 :goto_4

    nop

    :goto_b
    const/4 p0, 0x3

    goto/32 :goto_d

    nop

    :goto_c
    const-string p0, "SYNCINFO NEEDED"

    goto/32 :goto_1

    nop

    :goto_d
    if-ne p1, p0, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_0

    nop

    :goto_e
    if-nez p1, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_6

    nop

    :goto_f
    return-object p0

    :goto_10
    goto/32 :goto_13

    nop

    :goto_11
    return-object p0

    :goto_12
    goto/32 :goto_c

    nop

    :goto_13
    const-string p0, "SYNC FAIL"

    goto/32 :goto_14

    nop

    :goto_14
    return-object p0

    :goto_15
    goto/32 :goto_9

    nop

    :goto_16
    if-ne p1, p0, :cond_3

    goto/32 :goto_5

    :cond_3
    goto/32 :goto_a

    nop

    :goto_17
    return-object p0

    :goto_18
    const/4 p0, 0x2

    goto/32 :goto_19

    nop

    :goto_19
    if-ne p1, p0, :cond_4

    goto/32 :goto_15

    :cond_4
    goto/32 :goto_b

    nop
.end method

.method public getPreferenceKey()Ljava/lang/String;
    .locals 0

    const-string p0, "broadcast_source_details_category"

    return-object p0
.end method

.method protected init(Landroidx/preference/PreferenceScreen;)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->getPreferenceKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/PreferenceCategory;

    iput-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoContainer:Landroidx/preference/PreferenceCategory;

    const-string v0, "broadcast_si_sourceId"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceIdPref:Landroidx/preference/Preference;

    iget-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoContainer:Landroidx/preference/PreferenceCategory;

    const-string v0, "broadcast_si_source_address"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceDevicePref:Landroidx/preference/Preference;

    iget-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoContainer:Landroidx/preference/PreferenceCategory;

    const-string v0, "broadcast_si_encryption_state"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceEncStatusPref:Landroidx/preference/Preference;

    iget-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoContainer:Landroidx/preference/PreferenceCategory;

    const-string v0, "broadcast_si_metadata"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceMetadataPref:Landroidx/preference/Preference;

    iget-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoContainer:Landroidx/preference/PreferenceCategory;

    const-string v0, "broadcast_si_metadata_state"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceMetadataSyncStatusPref:Landroidx/preference/Preference;

    iget-boolean p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mPAsyncCtrlNeeded:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoContainer:Landroidx/preference/PreferenceCategory;

    const-string v0, "broadcast_si_enable_metadata_sync"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/SwitchPreference;

    iput-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceMetadataSyncSwitchPref:Landroidx/preference/SwitchPreference;

    if-eqz p1, :cond_0

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    :cond_0
    iget-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoContainer:Landroidx/preference/PreferenceCategory;

    const-string v0, "broadcast_si_audio_state"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/MultiSelectListPreference;

    iput-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceAudioSyncStatusPref:Landroidx/preference/MultiSelectListPreference;

    if-eqz p1, :cond_1

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_1
    iget-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoContainer:Landroidx/preference/PreferenceCategory;

    const-string v0, "broadcast_si_enable_audio_sync"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/SwitchPreference;

    iput-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceAudioSyncSwitchPref:Landroidx/preference/SwitchPreference;

    if-eqz p1, :cond_2

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    :cond_2
    iget-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoContainer:Landroidx/preference/PreferenceCategory;

    const-string/jumbo v0, "update_broadcast_code"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/EditTextPreference;

    iput-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceUpdateBcastCodePref:Landroidx/preference/EditTextPreference;

    if-eqz p1, :cond_3

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    iget-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceUpdateBcastCodePref:Landroidx/preference/EditTextPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_3
    iget-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoContainer:Landroidx/preference/PreferenceCategory;

    const-string v0, "bcast_si_update_button"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settingslib/widget/ActionButtonsPreference;

    sget v0, Lcom/android/settings/R$string;->update_sourceinfo_btn_txt:I

    invoke-virtual {p1, v0}, Lcom/android/settingslib/widget/ActionButtonsPreference;->setButton1Text(I)Lcom/android/settingslib/widget/ActionButtonsPreference;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/android/settingslib/widget/ActionButtonsPreference;->setButton1Enabled(Z)Lcom/android/settingslib/widget/ActionButtonsPreference;

    move-result-object p1

    new-instance v1, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;)V

    invoke-virtual {p1, v1}, Lcom/android/settingslib/widget/ActionButtonsPreference;->setButton1OnClickListener(Landroid/view/View$OnClickListener;)Lcom/android/settingslib/widget/ActionButtonsPreference;

    move-result-object p1

    sget v1, Lcom/android/settings/R$string;->remove_sourceinfo_btn_txt:I

    invoke-virtual {p1, v1}, Lcom/android/settingslib/widget/ActionButtonsPreference;->setButton2Text(I)Lcom/android/settingslib/widget/ActionButtonsPreference;

    move-result-object p1

    sget v1, Lcom/android/settings/R$drawable;->ic_settings_close:I

    invoke-virtual {p1, v1}, Lcom/android/settingslib/widget/ActionButtonsPreference;->setButton2Icon(I)Lcom/android/settingslib/widget/ActionButtonsPreference;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/android/settingslib/widget/ActionButtonsPreference;->setButton2Enabled(Z)Lcom/android/settingslib/widget/ActionButtonsPreference;

    move-result-object p1

    new-instance v0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController$$ExternalSyntheticLambda1;-><init>(Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;)V

    invoke-virtual {p1, v0}, Lcom/android/settingslib/widget/ActionButtonsPreference;->setButton2OnClickListener(Landroid/view/View$OnClickListener;)Lcom/android/settingslib/widget/ActionButtonsPreference;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceUpdateSourceInfoPref:Lcom/android/settingslib/widget/ActionButtonsPreference;

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->refresh()V

    return-void
.end method

.method public onDeviceAttributesChanged()V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mVendorCachedDevice:Lcom/android/settingslib/bluetooth/VendorCachedBluetoothDevice;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/VendorCachedBluetoothDevice;->getAllBleBroadcastreceiverStates()Ljava/util/Map;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget v3, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoIndex:I

    const-string v4, "BleBroadcastSourceInfoDetailsController"

    if-ne v2, v3, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoIndex:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ":matching source Info"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/bluetooth/BleBroadcastSourceInfo;->isEmptyEntry()Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoIndex:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ":source info seem to be removed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {v1, v2}, Landroid/bluetooth/BleBroadcastSourceInfo;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_2

    iput-object v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ":Update in Broadcast Source Information"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ":No Update to Source Information values"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ":Ignore this case"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->refresh()V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/bluetooth/BluetoothDetailsController;->onPause()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->unregisterCallback(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice$Callback;)V

    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 9

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ":onPreferenceChange"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BleBroadcastSourceInfoDetailsController"

    invoke-static {v2, v1}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "update_broadcast_code"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v3, 0x1

    if-eqz v1, :cond_0

    check-cast p1, Landroidx/preference/EditTextPreference;

    check-cast p2, Ljava/lang/String;

    iput-boolean v3, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->isBroadcastPINUpdated:Z

    iput-object p2, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBroadcastCode:Ljava/lang/String;

    goto/16 :goto_3

    :cond_0
    const-string v1, "broadcast_si_audio_state"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ">>Checked:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p1, Landroidx/preference/MultiSelectListPreference;

    invoke-virtual {p1}, Landroidx/preference/MultiSelectListPreference;->getEntries()[Ljava/lang/CharSequence;

    invoke-virtual {p1}, Landroidx/preference/MultiSelectListPreference;->getValues()Ljava/util/Set;

    check-cast p2, Ljava/util/Set;

    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result p1

    new-array p1, p1, [Ljava/lang/String;

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    aput-object v4, p1, v1

    move v5, v0

    :goto_1
    iget-object v6, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBisIndicies:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    const-string v7, "]- "

    if-ge v5, v6, :cond_3

    iget-object v6, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBisIndicies:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/bluetooth/BleBroadcastSourceChannel;

    invoke-virtual {v6}, Landroid/bluetooth/BleBroadcastSourceChannel;->getDescription()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Selected: value["

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBisIndicies:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/bluetooth/BleBroadcastSourceChannel;

    invoke-virtual {v6}, Landroid/bluetooth/BleBroadcastSourceChannel;->getStatus()Z

    move-result v6

    if-ne v6, v3, :cond_1

    iget-object v6, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBisIndicies:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/bluetooth/BleBroadcastSourceChannel;

    invoke-virtual {v6, v0}, Landroid/bluetooth/BleBroadcastSourceChannel;->setStatus(Z)V

    goto :goto_2

    :cond_1
    iget-object v6, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBisIndicies:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/bluetooth/BleBroadcastSourceChannel;

    invoke-virtual {v6, v3}, Landroid/bluetooth/BleBroadcastSourceChannel;->setStatus(Z)V

    :cond_2
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "value["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v6

    goto/16 :goto_0

    :cond_4
    iput-boolean v3, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mIsValueChanged:Z

    :cond_5
    :goto_3
    iput-boolean v3, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mIsButtonRefreshOnly:Z

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->refresh()V

    return v3
.end method

.method public onPreferenceClick(Landroidx/preference/Preference;)Z
    .locals 8

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ":onPreferenceClick"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "BleBroadcastSourceInfoDetailsController"

    invoke-static {v3, v1}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mIsValueChanged:Z

    iget-boolean v4, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mPAsyncCtrlNeeded:Z

    const/4 v5, 0x0

    if-eqz v4, :cond_2

    const-string v4, "broadcast_si_enable_metadata_sync"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v4, p1

    check-cast v4, Landroidx/preference/SwitchPreference;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget v7, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoIndex:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v7, ":Meta data sync state: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x2

    iput v4, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mMetadataSyncState:I

    goto :goto_0

    :cond_0
    iput v5, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mMetadataSyncState:I

    :goto_0
    iget-object v4, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceAudioSyncSwitchPref:Landroidx/preference/SwitchPreference;

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_1

    iput v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mAudioSyncState:I

    goto :goto_1

    :cond_1
    iput v5, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mAudioSyncState:I

    :cond_2
    :goto_1
    const-string v4, "broadcast_si_enable_audio_sync"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    check-cast p1, Landroidx/preference/SwitchPreference;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoIndex:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ":Audio sync state:  "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_3

    iput v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mAudioSyncState:I

    goto :goto_2

    :cond_3
    iput v5, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mAudioSyncState:I

    :goto_2
    iget-boolean p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mPAsyncCtrlNeeded:Z

    if-eqz p1, :cond_7

    iget-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceMetadataSyncSwitchPref:Landroidx/preference/SwitchPreference;

    if-eqz p1, :cond_7

    invoke-virtual {p1}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_4

    iput v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mMetadataSyncState:I

    goto :goto_3

    :cond_4
    iput v5, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mMetadataSyncState:I

    goto :goto_3

    :cond_5
    const-string/jumbo v4, "update_broadcast_code"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    check-cast p1, Landroidx/preference/EditTextPreference;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoIndex:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ":>>Pin code updated:  "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroidx/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v5, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mIsValueChanged:Z

    iput-boolean v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->isBroadcastPINUpdated:Z

    invoke-virtual {p1}, Landroidx/preference/EditTextPreference;->getText()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBroadcastCode:Ljava/lang/String;

    goto :goto_3

    :cond_6
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoIndex:I

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ":unhandled preference"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v3, p1}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v5, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mIsValueChanged:Z

    :cond_7
    :goto_3
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoIndex:I

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v3, p1}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mIsButtonRefreshOnly:Z

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->refresh()V

    return v1
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/bluetooth/BluetoothDetailsController;->onResume()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->registerCallback(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice$Callback;)V

    return-void
.end method

.method protected refresh()V
    .locals 9

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ":refresh: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " mSourceIndex"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BleBroadcastSourceInfoDetailsController"

    invoke-static {v1, v0}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceIdPref:Landroidx/preference/Preference;

    iget-object v2, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {v2}, Landroid/bluetooth/BleBroadcastSourceInfo;->getSourceId()B

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {v0}, Landroid/bluetooth/BleBroadcastSourceInfo;->getSourceDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v0, :cond_1

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "(Self)"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAlias()Ljava/lang/String;

    move-result-object v2

    :goto_0
    if-nez v2, :cond_2

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    move-object v2, v3

    :cond_2
    :goto_1
    if-eqz v2, :cond_3

    const-string v0, "00:00:00:00:00:00"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoIndex:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ":NULL source device"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "EMPTY_ENTRY"

    :cond_4
    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceDevicePref:Landroidx/preference/Preference;

    invoke-virtual {v0, v2}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceEncStatusPref:Landroidx/preference/Preference;

    iget-object v2, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {v2}, Landroid/bluetooth/BleBroadcastSourceInfo;->getEncryptionStatus()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->getEncryptionStatusString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {v0}, Landroid/bluetooth/BleBroadcastSourceInfo;->isEmptyEntry()Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoIndex:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ":Source Information seem to be Empty"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mPAsyncCtrlNeeded:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceMetadataSyncSwitchPref:Landroidx/preference/SwitchPreference;

    invoke-virtual {v0, v2}, Landroidx/preference/Preference;->setEnabled(Z)V

    :cond_5
    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceAudioSyncSwitchPref:Landroidx/preference/SwitchPreference;

    invoke-virtual {v0, v2}, Landroidx/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceUpdateBcastCodePref:Landroidx/preference/EditTextPreference;

    invoke-virtual {v0, v2}, Landroidx/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceUpdateSourceInfoPref:Lcom/android/settingslib/widget/ActionButtonsPreference;

    invoke-virtual {v0, v2}, Lcom/android/settingslib/widget/ActionButtonsPreference;->setButton1Enabled(Z)Lcom/android/settingslib/widget/ActionButtonsPreference;

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceUpdateSourceInfoPref:Lcom/android/settingslib/widget/ActionButtonsPreference;

    invoke-virtual {v0, v2}, Lcom/android/settingslib/widget/ActionButtonsPreference;->setButton2Enabled(Z)Lcom/android/settingslib/widget/ActionButtonsPreference;

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceAudioSyncStatusPref:Landroidx/preference/MultiSelectListPreference;

    invoke-virtual {v0, v2}, Landroidx/preference/Preference;->setEnabled(Z)V

    iput-boolean v2, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mIsValueChanged:Z

    goto/16 :goto_a

    :cond_6
    iget-boolean v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mPAsyncCtrlNeeded:Z

    const/4 v4, 0x1

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceMetadataSyncSwitchPref:Landroidx/preference/SwitchPreference;

    invoke-virtual {v0, v4}, Landroidx/preference/Preference;->setEnabled(Z)V

    :cond_7
    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceAudioSyncSwitchPref:Landroidx/preference/SwitchPreference;

    invoke-virtual {v0, v4}, Landroidx/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceUpdateBcastCodePref:Landroidx/preference/EditTextPreference;

    invoke-direct {p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->isPinUpdatedNeeded()Z

    move-result v5

    invoke-virtual {v0, v5}, Landroidx/preference/Preference;->setEnabled(Z)V

    iget-boolean v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mIsButtonRefreshOnly:Z

    if-eq v0, v4, :cond_11

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceMetadataSyncStatusPref:Landroidx/preference/Preference;

    iget-object v5, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {v5}, Landroid/bluetooth/BleBroadcastSourceInfo;->getMetadataSyncState()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->getMetadataSyncStatusString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceAudioSyncStatusPref:Landroidx/preference/MultiSelectListPreference;

    iget-object v5, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {v5}, Landroid/bluetooth/BleBroadcastSourceInfo;->getAudioSyncState()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->getAudioSyncStatusString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {v0}, Landroid/bluetooth/BleBroadcastSourceInfo;->getBroadcastChannelsSyncStatus()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBisIndicies:Ljava/util/List;

    if-eqz v0, :cond_9

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iget-object v5, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBisIndicies:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Z

    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    move v7, v2

    :goto_2
    iget-object v8, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBisIndicies:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge v7, v8, :cond_8

    iget-object v8, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBisIndicies:Ljava/util/List;

    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/bluetooth/BleBroadcastSourceChannel;

    invoke-virtual {v8}, Landroid/bluetooth/BleBroadcastSourceChannel;->getDescription()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v0, v7

    iget-object v8, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBisIndicies:Ljava/util/List;

    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/bluetooth/BleBroadcastSourceChannel;

    invoke-virtual {v8}, Landroid/bluetooth/BleBroadcastSourceChannel;->getStatus()Z

    move-result v8

    aput-boolean v8, v5, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_8
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v6, v5}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v5, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceAudioSyncStatusPref:Landroidx/preference/MultiSelectListPreference;

    invoke-virtual {v5, v0}, Landroidx/preference/MultiSelectListPreference;->setEntries([Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceAudioSyncStatusPref:Landroidx/preference/MultiSelectListPreference;

    invoke-virtual {v5, v0}, Landroidx/preference/MultiSelectListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceAudioSyncStatusPref:Landroidx/preference/MultiSelectListPreference;

    invoke-virtual {v0, v6}, Landroidx/preference/MultiSelectListPreference;->setValues(Ljava/util/Set;)V

    :cond_9
    iget-boolean v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mPAsyncCtrlNeeded:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceMetadataSyncSwitchPref:Landroidx/preference/SwitchPreference;

    iget-object v5, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {v5}, Landroid/bluetooth/BleBroadcastSourceInfo;->getMetadataSyncState()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_a

    move v5, v4

    goto :goto_3

    :cond_a
    move v5, v2

    :goto_3
    invoke-virtual {v0, v5}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :cond_b
    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceAudioSyncSwitchPref:Landroidx/preference/SwitchPreference;

    iget-object v5, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {v5}, Landroid/bluetooth/BleBroadcastSourceInfo;->getAudioSyncState()I

    move-result v5

    if-ne v5, v4, :cond_c

    move v5, v4

    goto :goto_4

    :cond_c
    move v5, v2

    :goto_4
    invoke-virtual {v0, v5}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBisIndicies:Ljava/util/List;

    const/4 v5, -0x1

    if-eqz v0, :cond_e

    move v0, v2

    :goto_5
    iget-object v6, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBisIndicies:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v0, v6, :cond_e

    iget-object v6, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBisIndicies:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/bluetooth/BleBroadcastSourceChannel;

    invoke-virtual {v6}, Landroid/bluetooth/BleBroadcastSourceChannel;->getStatus()Z

    move-result v6

    if-ne v6, v4, :cond_d

    goto :goto_6

    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_e
    move v0, v5

    :goto_6
    if-eq v0, v5, :cond_f

    iget-object v3, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mBisIndicies:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BleBroadcastSourceChannel;

    invoke-virtual {v0}, Landroid/bluetooth/BleBroadcastSourceChannel;->getMetadata()[B

    move-result-object v3

    :cond_f
    if-eqz v3, :cond_10

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>([B)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceInfoIndex:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, ":Metadata:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceMetadataPref:Landroidx/preference/Preference;

    invoke-virtual {v1, v0}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_7

    :cond_10
    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceMetadataPref:Landroidx/preference/Preference;

    const-string v1, "NONE"

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_7
    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceUpdateSourceInfoPref:Lcom/android/settingslib/widget/ActionButtonsPreference;

    invoke-virtual {v0, v4}, Lcom/android/settingslib/widget/ActionButtonsPreference;->setButton2Enabled(Z)Lcom/android/settingslib/widget/ActionButtonsPreference;

    :cond_11
    iget-boolean v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mIsValueChanged:Z

    if-nez v0, :cond_13

    iget-boolean v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->isBroadcastPINUpdated:Z

    if-eqz v0, :cond_12

    goto :goto_8

    :cond_12
    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceUpdateSourceInfoPref:Lcom/android/settingslib/widget/ActionButtonsPreference;

    invoke-virtual {v0, v2}, Lcom/android/settingslib/widget/ActionButtonsPreference;->setButton1Enabled(Z)Lcom/android/settingslib/widget/ActionButtonsPreference;

    goto :goto_9

    :cond_13
    :goto_8
    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mSourceUpdateSourceInfoPref:Lcom/android/settingslib/widget/ActionButtonsPreference;

    invoke-virtual {v0, v4}, Lcom/android/settingslib/widget/ActionButtonsPreference;->setButton1Enabled(Z)Lcom/android/settingslib/widget/ActionButtonsPreference;

    :goto_9
    iput-boolean v2, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;->mIsButtonRefreshOnly:Z

    :goto_a
    return-void
.end method
