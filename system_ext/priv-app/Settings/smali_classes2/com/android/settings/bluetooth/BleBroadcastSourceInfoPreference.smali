.class public final Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;
.super Lcom/android/settings/widget/GearPreference;

# interfaces
.implements Lcom/android/settingslib/bluetooth/CachedBluetoothDevice$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference$SortType;
    }
.end annotation


# static fields
.field private static EMPTY_BD_ADDR:Ljava/lang/String; = "00:00:00:00:00:00"


# instance fields
.field private mBleSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

.field private final mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

.field private final mCurrentTime:J

.field private final mIndex:Ljava/lang/Integer;

.field mResources:Landroid/content/res/Resources;

.field private final mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;Landroid/bluetooth/BleBroadcastSourceInfo;Ljava/lang/Integer;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/widget/GearPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->mResources:Landroid/content/res/Resources;

    iput-object p4, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->mIndex:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    iput-object p3, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->mBleSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {p2, p0}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->registerCallback(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice$Callback;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->mCurrentTime:J

    iput p5, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->mType:I

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->onDeviceAttributesChanged()V

    return-void
.end method


# virtual methods
.method public compareTo(Landroidx/preference/Preference;)I
    .locals 5

    instance-of v0, p1, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroidx/preference/Preference;->compareTo(Landroidx/preference/Preference;)I

    move-result p0

    return p0

    :cond_0
    iget v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->mType:I

    const/4 v1, -0x1

    const/4 v2, 0x1

    if-eq v0, v2, :cond_3

    const/4 v3, 0x2

    if-eq v0, v3, :cond_1

    invoke-super {p0, p1}, Landroidx/preference/Preference;->compareTo(Landroidx/preference/Preference;)I

    move-result p0

    return p0

    :cond_1
    iget-wide v3, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->mCurrentTime:J

    check-cast p1, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;

    iget-wide p0, p1, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->mCurrentTime:J

    cmp-long p0, v3, p0

    if-lez p0, :cond_2

    move v1, v2

    :cond_2
    return v1

    :cond_3
    const-string v0, "BleBroadcastSourceInfoPreference"

    const-string v3, ">>compareTo"

    invoke-static {v0, v3}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->mIndex:Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    check-cast p1, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;

    invoke-virtual {p1}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->getSourceInfoIndex()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-le p0, p1, :cond_4

    move v1, v2

    :cond_4
    return v1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Landroidx/preference/Preference;

    invoke-virtual {p0, p1}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->compareTo(Landroidx/preference/Preference;)I

    move-result p0

    return p0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    const-string v1, "BleBroadcastSourceInfoPreference"

    if-eqz p1, :cond_2

    instance-of v2, p1, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    check-cast p1, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;

    iget-object v2, p1, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->mBleSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Comparing: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->mBleSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "TO: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->mBleSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {v3}, Landroid/bluetooth/BleBroadcastSourceInfo;->getSourceId()B

    move-result v3

    invoke-virtual {v2}, Landroid/bluetooth/BleBroadcastSourceInfo;->getSourceId()B

    move-result v2

    if-ne v3, v2, :cond_1

    iget-object p0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->mIndex:Ljava/lang/Integer;

    invoke-virtual {p1}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->getSourceInfoIndex()Ljava/lang/Integer;

    move-result-object p1

    if-ne p0, p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p1, "equals returns: "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    return v0

    :cond_2
    :goto_0
    const-string p0, "Not an Instance of BleBroadcastSourceInfoPreference:"

    invoke-static {v1, p0}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    return v0
.end method

.method formSyncSummaryString(Landroid/bluetooth/BleBroadcastSourceInfo;)Ljava/lang/String;
    .locals 1

    goto/32 :goto_11

    nop

    :goto_0
    goto :goto_e

    :goto_1
    goto/32 :goto_d

    nop

    :goto_2
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_b

    nop

    :goto_3
    const/4 v0, 0x2

    goto/32 :goto_c

    nop

    :goto_4
    if-eq p1, v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_15

    nop

    :goto_5
    goto :goto_9

    :goto_6
    goto/32 :goto_8

    nop

    :goto_7
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_13

    nop

    :goto_8
    const-string p0, "Metadata not synced"

    :goto_9
    goto/32 :goto_12

    nop

    :goto_a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_7

    nop

    :goto_b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_17

    nop

    :goto_c
    if-eq p0, v0, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_16

    nop

    :goto_d
    const-string p1, "Audio not synced"

    :goto_e
    goto/32 :goto_14

    nop

    :goto_f
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_2

    nop

    :goto_10
    const/4 v0, 0x1

    goto/32 :goto_4

    nop

    :goto_11
    invoke-virtual {p1}, Landroid/bluetooth/BleBroadcastSourceInfo;->getMetadataSyncState()I

    move-result p0

    goto/32 :goto_3

    nop

    :goto_12
    invoke-virtual {p1}, Landroid/bluetooth/BleBroadcastSourceInfo;->getAudioSyncState()I

    move-result p1

    goto/32 :goto_10

    nop

    :goto_13
    const-string p0, ", "

    goto/32 :goto_f

    nop

    :goto_14
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_a

    nop

    :goto_15
    const-string p1, "Audio Synced"

    goto/32 :goto_0

    nop

    :goto_16
    const-string p0, "Metadata Synced"

    goto/32 :goto_5

    nop

    :goto_17
    return-object p0
.end method

.method public getBleBroadcastSourceInfo()Landroid/bluetooth/BleBroadcastSourceInfo;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->mBleSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    return-object p0
.end method

.method protected getSecondTargetResId()I
    .locals 0

    sget p0, Lcom/android/settings/R$layout;->preference_widget_gear:I

    return p0
.end method

.method getSourceInfoIndex()Ljava/lang/Integer;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iget-object p0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->mIndex:Ljava/lang/Integer;

    goto/32 :goto_1

    nop

    :goto_1
    return-object p0
.end method

.method public hashCode()I
    .locals 0

    iget-object p0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->mBleSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {p0}, Landroid/bluetooth/BleBroadcastSourceInfo;->hashCode()I

    move-result p0

    return p0
.end method

.method public onDeviceAttributesChanged()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->mBleSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {v0}, Landroid/bluetooth/BleBroadcastSourceInfo;->getSourceDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "(Self)"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAlias()Ljava/lang/String;

    move-result-object v1

    :goto_0
    if-nez v1, :cond_2

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_1
    if-eqz v1, :cond_3

    sget-object v0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->EMPTY_BD_ADDR:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    const-string v0, "BleBroadcastSourceInfoPreference"

    const-string/jumbo v1, "seem to be an entry source Info"

    invoke-static {v0, v1}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "EMPTY ENTRY"

    :cond_4
    invoke-virtual {p0, v1}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    sget v0, Lcom/android/settings/R$drawable;->ic_media_stream:I

    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setIcon(I)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->mBleSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {v0}, Landroid/bluetooth/BleBroadcastSourceInfo;->isEmptyEntry()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->mBleSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->formSyncSummaryString(Landroid/bluetooth/BleBroadcastSourceInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_5
    const-string v0, ""

    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    invoke-virtual {p0}, Landroidx/preference/Preference;->notifyHierarchyChanged()V

    return-void
.end method

.method protected onPrepareForRemoval()V
    .locals 1

    invoke-super {p0}, Landroidx/preference/Preference;->onPrepareForRemoval()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    invoke-virtual {v0, p0}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->unregisterCallback(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice$Callback;)V

    return-void
.end method

.method public setBleBroadcastSourceInfo(Landroid/bluetooth/BleBroadcastSourceInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->mBleSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->onDeviceAttributesChanged()V

    return-void
.end method

.method protected shouldHideSecondTarget()Z
    .locals 0

    iget-object p0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoPreference;->mBleSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method
