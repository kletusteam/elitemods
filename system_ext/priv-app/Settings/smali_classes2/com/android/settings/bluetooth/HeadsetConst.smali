.class public Lcom/android/settings/bluetooth/HeadsetConst;
.super Ljava/lang/Object;


# instance fields
.field public mNotPluginDeviceId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/bluetooth/HeadsetConst;->mNotPluginDeviceId:Ljava/util/List;

    const-string v1, "01010402"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/bluetooth/HeadsetConst;->mNotPluginDeviceId:Ljava/util/List;

    const-string v1, "01010600"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/bluetooth/HeadsetConst;->mNotPluginDeviceId:Ljava/util/List;

    const-string v1, "01010602"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/bluetooth/HeadsetConst;->mNotPluginDeviceId:Ljava/util/List;

    const-string v1, "01010603"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/bluetooth/HeadsetConst;->mNotPluginDeviceId:Ljava/util/List;

    const-string v1, "01010601"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/bluetooth/HeadsetConst;->mNotPluginDeviceId:Ljava/util/List;

    const-string v1, "01010605"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/bluetooth/HeadsetConst;->mNotPluginDeviceId:Ljava/util/List;

    const-string v1, "01010606"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/bluetooth/HeadsetConst;->mNotPluginDeviceId:Ljava/util/List;

    const-string v1, "01010607"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/bluetooth/HeadsetConst;->mNotPluginDeviceId:Ljava/util/List;

    const-string v1, "01010703"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/bluetooth/HeadsetConst;->mNotPluginDeviceId:Ljava/util/List;

    const-string v1, "01010704"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/bluetooth/HeadsetConst;->mNotPluginDeviceId:Ljava/util/List;

    const-string v1, "01010901"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/bluetooth/HeadsetConst;->mNotPluginDeviceId:Ljava/util/List;

    const-string v1, "01010902"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p0, p0, Lcom/android/settings/bluetooth/HeadsetConst;->mNotPluginDeviceId:Ljava/util/List;

    const-string v0, "0201010001"

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method
