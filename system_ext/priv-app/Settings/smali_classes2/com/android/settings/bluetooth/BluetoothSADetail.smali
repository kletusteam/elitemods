.class public Lcom/android/settings/bluetooth/BluetoothSADetail;
.super Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;


# instance fields
.field clickedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

.field mAvailableDevicesCategory:Lcom/android/settings/bluetooth/BluetoothProgressCategory;

.field mBroadcastPinCode:Ljava/lang/String;

.field private mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

.field mCommonMsgDialog:Landroidx/appcompat/app/AlertDialog;

.field mContext:Landroid/content/Context;

.field mFooterPreference:Lcom/android/settingslib/widget/FooterPreference;

.field mGroupOperation:Z

.field private mInitialScanStarted:Z

.field protected mProfileManager:Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;

.field mScanAssistCallback:Landroid/bluetooth/BleBroadcastAudioScanAssistCallback;

.field private mScanAssistDetailsDialog:Landroidx/appcompat/app/AlertDialog;

.field mScanAssistManager:Landroid/bluetooth/BleBroadcastAudioScanAssistManager;

.field mScanDelegatorName:Landroidx/preference/Preference;

.field mScanning:Z


# direct methods
.method static bridge synthetic -$$Nest$mgetBluetoothName(Lcom/android/settings/bluetooth/BluetoothSADetail;Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/BluetoothSADetail;->getBluetoothName(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetSourceAdditionErrMessage(Lcom/android/settings/bluetooth/BluetoothSADetail;I)I
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/BluetoothSADetail;->getSourceAdditionErrMessage(I)I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mgetSourceRemovalErrMessage(Lcom/android/settings/bluetooth/BluetoothSADetail;I)I
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/BluetoothSADetail;->getSourceRemovalErrMessage(I)I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mgetSourceSelectionErrMessage(Lcom/android/settings/bluetooth/BluetoothSADetail;I)I
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/BluetoothSADetail;->getSourceSelectionErrMessage(I)I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mgetSourceUpdateErrMessage(Lcom/android/settings/bluetooth/BluetoothSADetail;I)I
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/BluetoothSADetail;->getSourceUpdateErrMessage(I)I

    move-result p0

    return p0
.end method

.method public constructor <init>()V
    .locals 2

    const-string/jumbo v0, "no_config_bluetooth"

    invoke-direct {p0, v0}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->clickedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mBroadcastPinCode:Ljava/lang/String;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mScanning:Z

    iput-boolean v1, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mGroupOperation:Z

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mCommonMsgDialog:Landroidx/appcompat/app/AlertDialog;

    new-instance v0, Lcom/android/settings/bluetooth/BluetoothSADetail$1;

    invoke-direct {v0, p0}, Lcom/android/settings/bluetooth/BluetoothSADetail$1;-><init>(Lcom/android/settings/bluetooth/BluetoothSADetail;)V

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mScanAssistCallback:Landroid/bluetooth/BleBroadcastAudioScanAssistCallback;

    return-void
.end method

.method private getBluetoothName(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;
    .locals 2

    const-string p0, "Scan Delegator"

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAlias()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    :cond_1
    if-nez v0, :cond_2

    goto :goto_0

    :cond_2
    move-object p0, v0

    :goto_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "getBluetoothName returns"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "BluetoothSADetail"

    invoke-static {v0, p1}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method private getSourceAdditionErrMessage(I)I
    .locals 1

    sget p0, Lcom/android/settings/R$string;->bluetooth_source_addition_error_message:I

    const/16 v0, 0x8

    if-eq p1, v0, :cond_1

    const/16 v0, 0x9

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    sget p0, Lcom/android/settings/R$string;->bluetooth_source_no_empty_slot_error_message:I

    goto :goto_0

    :cond_1
    sget p0, Lcom/android/settings/R$string;->bluetooth_source_dup_addition_error_message:I

    :goto_0
    return p0
.end method

.method private getSourceRemovalErrMessage(I)I
    .locals 1

    sget p0, Lcom/android/settings/R$string;->bluetooth_source_removal_error_message:I

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/16 v0, 0x10

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    sget p0, Lcom/android/settings/R$string;->bluetooth_source_remove_invalid_group_op:I

    goto :goto_0

    :cond_1
    sget p0, Lcom/android/settings/R$string;->bluetooth_source_remove_invalid_src_id:I

    :cond_2
    :goto_0
    return p0
.end method

.method private getSourceSelectionErrMessage(I)I
    .locals 0

    sget p0, Lcom/android/settings/R$string;->bluetooth_source_selection_error_message:I

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    sget p0, Lcom/android/settings/R$string;->bluetooth_source_no_empty_slot_error_message:I

    goto :goto_0

    :pswitch_1
    sget p0, Lcom/android/settings/R$string;->bluetooth_source_dup_addition_error_message:I

    goto :goto_0

    :pswitch_2
    sget p0, Lcom/android/settings/R$string;->bluetooth_source_selection_error_src_unavail_message:I

    :goto_0
    :pswitch_3
    return p0

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getSourceUpdateErrMessage(I)I
    .locals 1

    sget p0, Lcom/android/settings/R$string;->bluetooth_source_update_error_message:I

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/16 v0, 0x10

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    sget p0, Lcom/android/settings/R$string;->bluetooth_source_update_invalid_group_op:I

    goto :goto_0

    :cond_1
    sget p0, Lcom/android/settings/R$string;->bluetooth_source_update_invalid_src_id:I

    :cond_2
    :goto_0
    return p0
.end method


# virtual methods
.method createDevicePreference(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)V
    .locals 6

    goto/32 :goto_7

    nop

    :goto_0
    new-instance v2, Lcom/android/settings/bluetooth/BluetoothDevicePreference;

    goto/32 :goto_1d

    nop

    :goto_1
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_17

    nop

    :goto_2
    const-string v1, "BluetoothSADetail"

    goto/32 :goto_20

    nop

    :goto_3
    check-cast v2, Lcom/android/settings/bluetooth/BluetoothDevicePreference;

    goto/32 :goto_e

    nop

    :goto_4
    invoke-virtual {v0, v2}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    :goto_5
    goto/32 :goto_f

    nop

    :goto_6
    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->mDevicePreferenceMap:Ljava/util/HashMap;

    goto/32 :goto_23

    nop

    :goto_7
    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->mDeviceListGroup:Landroidx/preference/PreferenceGroup;

    goto/32 :goto_2

    nop

    :goto_8
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_14

    nop

    :goto_9
    iget-boolean p0, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mScanning:Z

    goto/32 :goto_1e

    nop

    :goto_a
    const-string/jumbo v3, "to the Pref map"

    goto/32 :goto_15

    nop

    :goto_b
    return-void

    :goto_c
    const-string v3, "adding"

    goto/32 :goto_1

    nop

    :goto_d
    invoke-direct {v2, v3, p1, v4, v5}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;-><init>(Landroid/content/Context;Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;ZI)V

    goto/32 :goto_24

    nop

    :goto_e
    if-eqz v2, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_0

    nop

    :goto_f
    invoke-virtual {p0, v2}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->initDevicePreference(Lcom/android/settings/bluetooth/BluetoothDevicePreference;)V

    goto/32 :goto_16

    nop

    :goto_10
    iget-object p1, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mAvailableDevicesCategory:Lcom/android/settings/bluetooth/BluetoothProgressCategory;

    goto/32 :goto_9

    nop

    :goto_11
    const-string p0, "Trying to create a device preference before the list group/category exists!"

    goto/32 :goto_1b

    nop

    :goto_12
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_c

    nop

    :goto_13
    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->mDeviceListGroup:Landroidx/preference/PreferenceGroup;

    goto/32 :goto_4

    nop

    :goto_14
    invoke-virtual {p0, v0}, Lcom/android/settings/SettingsPreferenceFragment;->getCachedPreference(Ljava/lang/String;)Landroidx/preference/Preference;

    move-result-object v2

    goto/32 :goto_3

    nop

    :goto_15
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_21

    nop

    :goto_16
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_12

    nop

    :goto_17
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_a

    nop

    :goto_18
    const/4 v4, 0x1

    goto/32 :goto_1c

    nop

    :goto_19
    return-void

    :goto_1a
    goto/32 :goto_22

    nop

    :goto_1b
    invoke-static {v1, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_19

    nop

    :goto_1c
    const/4 v5, 0x2

    goto/32 :goto_d

    nop

    :goto_1d
    invoke-virtual {p0}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->getPrefContext()Landroid/content/Context;

    move-result-object v3

    goto/32 :goto_18

    nop

    :goto_1e
    invoke-virtual {p1, p0}, Lcom/android/settings/MiuiProgressCategory;->setProgress(Z)V

    goto/32 :goto_b

    nop

    :goto_1f
    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_6

    nop

    :goto_20
    if-eqz v0, :cond_1

    goto/32 :goto_1a

    :cond_1
    goto/32 :goto_11

    nop

    :goto_21
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_1f

    nop

    :goto_22
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_23
    invoke-virtual {v0, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_10

    nop

    :goto_24
    invoke-virtual {v2, v0}, Landroidx/preference/Preference;->setKey(Ljava/lang/String;)V

    goto/32 :goto_13

    nop
.end method

.method disableScanning()V
    .locals 2

    goto/32 :goto_6

    nop

    :goto_0
    invoke-virtual {v0}, Landroid/bluetooth/BleBroadcastAudioScanAssistManager;->stopSearchforLeAudioBroadcasters()Z

    goto/32 :goto_8

    nop

    :goto_1
    if-eq v0, v1, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_a

    nop

    :goto_2
    iget-boolean v0, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mScanning:Z

    goto/32 :goto_7

    nop

    :goto_3
    const-string v1, "call stopSearchforLeAudioBroadcasters"

    goto/32 :goto_9

    nop

    :goto_4
    iput-boolean v0, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mScanning:Z

    :goto_5
    goto/32 :goto_d

    nop

    :goto_6
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mScanAssistManager:Landroid/bluetooth/BleBroadcastAudioScanAssistManager;

    goto/32 :goto_b

    nop

    :goto_7
    const/4 v1, 0x1

    goto/32 :goto_1

    nop

    :goto_8
    const/4 v0, 0x0

    goto/32 :goto_4

    nop

    :goto_9
    invoke-static {v0, v1}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_c

    nop

    :goto_a
    const-string v0, "BluetoothSADetail"

    goto/32 :goto_3

    nop

    :goto_b
    if-nez v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_2

    nop

    :goto_c
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mScanAssistManager:Landroid/bluetooth/BleBroadcastAudioScanAssistManager;

    goto/32 :goto_0

    nop

    :goto_d
    return-void
.end method

.method enableScanning()V
    .locals 3

    goto/32 :goto_14

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mScanAssistManager:Landroid/bluetooth/BleBroadcastAudioScanAssistManager;

    goto/32 :goto_a

    nop

    :goto_1
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mScanAssistManager:Landroid/bluetooth/BleBroadcastAudioScanAssistManager;

    goto/32 :goto_11

    nop

    :goto_2
    invoke-virtual {p0}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->removeAllDevices()V

    :goto_3
    goto/32 :goto_d

    nop

    :goto_4
    const-string v0, "BluetoothSADetail"

    goto/32 :goto_b

    nop

    :goto_5
    iput-boolean v1, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mInitialScanStarted:Z

    :goto_6
    goto/32 :goto_0

    nop

    :goto_7
    if-eqz v0, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_4

    nop

    :goto_8
    iget-boolean v0, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mScanning:Z

    goto/32 :goto_7

    nop

    :goto_9
    invoke-static {v0, v2}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_1

    nop

    :goto_a
    if-nez v0, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_8

    nop

    :goto_b
    const-string v2, "call searchforLeAudioBroadcasters"

    goto/32 :goto_9

    nop

    :goto_c
    const/4 v1, 0x1

    goto/32 :goto_13

    nop

    :goto_d
    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->mLocalManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    goto/32 :goto_12

    nop

    :goto_e
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mAvailableDevicesCategory:Lcom/android/settings/bluetooth/BluetoothProgressCategory;

    goto/32 :goto_16

    nop

    :goto_f
    iput-boolean v1, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mScanning:Z

    :goto_10
    goto/32 :goto_15

    nop

    :goto_11
    invoke-virtual {v0}, Landroid/bluetooth/BleBroadcastAudioScanAssistManager;->searchforLeAudioBroadcasters()Z

    goto/32 :goto_f

    nop

    :goto_12
    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/LocalBluetoothManager;->getCachedDeviceManager()Lcom/android/settingslib/bluetooth/CachedBluetoothDeviceManager;

    move-result-object v0

    goto/32 :goto_17

    nop

    :goto_13
    if-eqz v0, :cond_2

    goto/32 :goto_6

    :cond_2
    goto/32 :goto_e

    nop

    :goto_14
    iget-boolean v0, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mInitialScanStarted:Z

    goto/32 :goto_c

    nop

    :goto_15
    return-void

    :goto_16
    if-nez v0, :cond_3

    goto/32 :goto_3

    :cond_3
    goto/32 :goto_2

    nop

    :goto_17
    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/CachedBluetoothDeviceManager;->clearNonBondedDevices()V

    goto/32 :goto_5

    nop
.end method

.method public getDeviceListKey()Ljava/lang/String;
    .locals 0

    const-string p0, "available_audio_sources"

    return-object p0
.end method

.method public getHelpResource()I
    .locals 0

    sget p0, Lcom/android/settings/R$string;->help_url_bluetooth:I

    return p0
.end method

.method protected getLogTag()Ljava/lang/String;
    .locals 0

    const-string p0, "BluetoothSADetail"

    return-object p0
.end method

.method public getMetricsCategory()I
    .locals 0

    const/16 p0, 0x3fa

    return p0
.end method

.method protected getPreferenceScreenResId()I
    .locals 0

    sget p0, Lcom/android/settings/R$xml;->bluetooth_search_bcast_sources:I

    return p0
.end method

.method initPreferencesFromPreferenceScreen()V
    .locals 3

    goto/32 :goto_14

    nop

    :goto_0
    invoke-direct {p0, v0}, Lcom/android/settings/bluetooth/BluetoothSADetail;->getBluetoothName(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_1
    const-string v0, "footer_preference"

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    goto/32 :goto_10

    nop

    :goto_3
    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_4
    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mFooterPreference:Lcom/android/settingslib/widget/FooterPreference;

    goto/32 :goto_13

    nop

    :goto_5
    const-string v2, "Scan Delegator"

    goto/32 :goto_a

    nop

    :goto_6
    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setSelectable(Z)V

    goto/32 :goto_19

    nop

    :goto_7
    check-cast v0, Lcom/android/settings/bluetooth/BluetoothProgressCategory;

    goto/32 :goto_b

    nop

    :goto_8
    invoke-virtual {v2, v0}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_9
    goto/32 :goto_f

    nop

    :goto_a
    invoke-virtual {v0, v2}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/32 :goto_15

    nop

    :goto_b
    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mAvailableDevicesCategory:Lcom/android/settings/bluetooth/BluetoothProgressCategory;

    goto/32 :goto_1

    nop

    :goto_c
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mScanDelegatorName:Landroidx/preference/Preference;

    goto/32 :goto_5

    nop

    :goto_d
    return-void

    :goto_e
    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mScanDelegatorName:Landroidx/preference/Preference;

    goto/32 :goto_11

    nop

    :goto_f
    const-string v0, "available_audio_sources"

    goto/32 :goto_3

    nop

    :goto_10
    check-cast v0, Lcom/android/settingslib/widget/FooterPreference;

    goto/32 :goto_4

    nop

    :goto_11
    const/4 v1, 0x0

    goto/32 :goto_6

    nop

    :goto_12
    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mScanDelegatorName:Landroidx/preference/Preference;

    goto/32 :goto_1a

    nop

    :goto_13
    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setSelectable(Z)V

    goto/32 :goto_d

    nop

    :goto_14
    const-string v0, "bt_bcast_rcvr_device"

    goto/32 :goto_18

    nop

    :goto_15
    goto :goto_9

    :goto_16
    goto/32 :goto_12

    nop

    :goto_17
    if-eqz v0, :cond_0

    goto/32 :goto_16

    :cond_0
    goto/32 :goto_c

    nop

    :goto_18
    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    goto/32 :goto_e

    nop

    :goto_19
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    goto/32 :goto_17

    nop

    :goto_1a
    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    goto/32 :goto_0

    nop
.end method

.method launchSyncAndBroadcastIndexOptions(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/bluetooth/BleBroadcastSourceChannel;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_3c

    nop

    :goto_0
    if-nez v2, :cond_0

    goto/32 :goto_20

    :cond_0
    goto/32 :goto_f

    nop

    :goto_1
    invoke-virtual {p1, v2}, Landroid/widget/EditText;->setVisibility(I)V

    goto/32 :goto_17

    nop

    :goto_2
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_3
    goto/32 :goto_33

    nop

    :goto_4
    return-void

    :goto_5
    sget v2, Lcom/android/settings/R$string;->bluetooth_source_selection_options_detail:I

    goto/32 :goto_43

    nop

    :goto_6
    sget v1, Lcom/android/settings/R$string;->bluetooth_device:I

    goto/32 :goto_2

    nop

    :goto_7
    if-nez p1, :cond_1

    goto/32 :goto_28

    :cond_1
    goto/32 :goto_14

    nop

    :goto_8
    invoke-direct {v9, p0, v6, p1}, Lcom/android/settings/bluetooth/BluetoothSADetail$3;-><init>(Lcom/android/settings/bluetooth/BluetoothSADetail;Landroid/view/View;Ljava/util/List;)V

    goto/32 :goto_35

    nop

    :goto_9
    invoke-virtual {v0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto/32 :goto_38

    nop

    :goto_a
    new-instance v9, Lcom/android/settings/bluetooth/BluetoothSADetail$3;

    goto/32 :goto_8

    nop

    :goto_b
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result v2

    goto/32 :goto_0

    nop

    :goto_c
    goto :goto_11

    :goto_d
    goto/32 :goto_10

    nop

    :goto_e
    invoke-virtual {v6, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    goto/32 :goto_32

    nop

    :goto_f
    if-nez v1, :cond_2

    goto/32 :goto_20

    :cond_2
    goto/32 :goto_15

    nop

    :goto_10
    move-object v1, v3

    :goto_11
    goto/32 :goto_23

    nop

    :goto_12
    const/4 v3, 0x0

    goto/32 :goto_19

    nop

    :goto_13
    sget v2, Lcom/android/settings/R$string;->bluetooth_grp_source_selection_options_detail:I

    goto/32 :goto_34

    nop

    :goto_14
    sget p1, Lcom/android/settings/R$string;->bluetooth_col_grp_source_selection_options_detail:I

    goto/32 :goto_37

    nop

    :goto_15
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    goto/32 :goto_24

    nop

    :goto_16
    invoke-virtual {v10}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v10

    goto/32 :goto_1c

    nop

    :goto_17
    iget-boolean p1, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mGroupOperation:Z

    goto/32 :goto_7

    nop

    :goto_18
    const/4 v5, 0x1

    goto/32 :goto_1a

    nop

    :goto_19
    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    goto/32 :goto_44

    nop

    :goto_1a
    if-nez v2, :cond_3

    goto/32 :goto_1e

    :cond_3
    goto/32 :goto_13

    nop

    :goto_1b
    if-nez v10, :cond_4

    goto/32 :goto_2e

    :cond_4
    goto/32 :goto_48

    nop

    :goto_1c
    iget-object v11, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    goto/32 :goto_45

    nop

    :goto_1d
    goto/16 :goto_4a

    :goto_1e
    goto/32 :goto_5

    nop

    :goto_1f
    iput-object p1, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mScanAssistDetailsDialog:Landroidx/appcompat/app/AlertDialog;

    :goto_20
    goto/32 :goto_4

    nop

    :goto_21
    iput-object v3, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mBroadcastPinCode:Ljava/lang/String;

    goto/32 :goto_3f

    nop

    :goto_22
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    goto/32 :goto_1b

    nop

    :goto_23
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    goto/32 :goto_25

    nop

    :goto_24
    sget v2, Lcom/android/settings/R$layout;->select_source_prompt:I

    goto/32 :goto_12

    nop

    :goto_25
    if-nez v2, :cond_5

    goto/32 :goto_3

    :cond_5
    goto/32 :goto_6

    nop

    :goto_26
    iget-object v1, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mScanAssistDetailsDialog:Landroidx/appcompat/app/AlertDialog;

    goto/32 :goto_4b

    nop

    :goto_27
    goto :goto_2e

    :goto_28
    goto/32 :goto_3a

    nop

    :goto_29
    aput-object v1, v7, v4

    goto/32 :goto_49

    nop

    :goto_2a
    iget-object v10, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->clickedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    goto/32 :goto_42

    nop

    :goto_2b
    move-object v4, v7

    goto/32 :goto_4d

    nop

    :goto_2c
    move-object v3, v8

    goto/32 :goto_2b

    nop

    :goto_2d
    invoke-virtual {v0, p1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_2e
    goto/32 :goto_26

    nop

    :goto_2f
    aput-object v1, v2, v4

    goto/32 :goto_2d

    nop

    :goto_30
    sget v7, Lcom/android/settings/R$string;->bluetooth_source_selection_options_detail_title:I

    goto/32 :goto_9

    nop

    :goto_31
    invoke-virtual {v0, p1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_27

    nop

    :goto_32
    check-cast p1, Landroid/widget/EditText;

    goto/32 :goto_2a

    nop

    :goto_33
    iget-boolean v2, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mGroupOperation:Z

    goto/32 :goto_4f

    nop

    :goto_34
    new-array v7, v5, [Ljava/lang/Object;

    goto/32 :goto_3b

    nop

    :goto_35
    sget p1, Lcom/android/settings/R$id;->broadcastPINcode:I

    goto/32 :goto_e

    nop

    :goto_36
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    goto/32 :goto_b

    nop

    :goto_37
    new-array v2, v5, [Ljava/lang/Object;

    goto/32 :goto_40

    nop

    :goto_38
    new-instance v8, Lcom/android/settings/bluetooth/BluetoothSADetail$2;

    goto/32 :goto_4c

    nop

    :goto_39
    invoke-virtual {v0, v2, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_1d

    nop

    :goto_3a
    sget p1, Lcom/android/settings/R$string;->bluetooth_col_source_selection_options_detail:I

    goto/32 :goto_3e

    nop

    :goto_3b
    aput-object v1, v7, v4

    goto/32 :goto_39

    nop

    :goto_3c
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    goto/32 :goto_36

    nop

    :goto_3d
    if-nez v1, :cond_6

    goto/32 :goto_d

    :cond_6
    goto/32 :goto_41

    nop

    :goto_3e
    new-array v2, v5, [Ljava/lang/Object;

    goto/32 :goto_2f

    nop

    :goto_3f
    const/4 v2, 0x4

    goto/32 :goto_1

    nop

    :goto_40
    aput-object v1, v2, v4

    goto/32 :goto_31

    nop

    :goto_41
    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_c

    nop

    :goto_42
    if-nez v10, :cond_7

    goto/32 :goto_2e

    :cond_7
    goto/32 :goto_16

    nop

    :goto_43
    new-array v7, v5, [Ljava/lang/Object;

    goto/32 :goto_29

    nop

    :goto_44
    iget-object v1, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->clickedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    goto/32 :goto_3d

    nop

    :goto_45
    invoke-virtual {v11}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v11

    goto/32 :goto_22

    nop

    :goto_46
    invoke-static {v2, v10}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_21

    nop

    :goto_47
    move-object v2, v9

    goto/32 :goto_2c

    nop

    :goto_48
    const-string v2, "BluetoothSADetail"

    goto/32 :goto_4e

    nop

    :goto_49
    invoke-virtual {v0, v2, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_4a
    goto/32 :goto_30

    nop

    :goto_4b
    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    goto/32 :goto_47

    nop

    :goto_4c
    invoke-direct {v8, p0}, Lcom/android/settings/bluetooth/BluetoothSADetail$2;-><init>(Lcom/android/settings/bluetooth/BluetoothSADetail;)V

    goto/32 :goto_a

    nop

    :goto_4d
    invoke-static/range {v0 .. v6}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->showScanAssistDetailsDialog(Landroid/content/Context;Landroidx/appcompat/app/AlertDialog;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View;)Landroidx/appcompat/app/AlertDialog;

    move-result-object p1

    goto/32 :goto_1f

    nop

    :goto_4e
    const-string v10, "Local Adapter"

    goto/32 :goto_46

    nop

    :goto_4f
    const/4 v4, 0x0

    goto/32 :goto_18

    nop
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/dashboard/RestrictedDashboardFragment;->onActivityCreated(Landroid/os/Bundle;)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mInitialScanStarted:Z

    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3

    const-string v0, "BluetoothSADetail"

    const-string v1, "OnAttach Called"

    invoke-static {v0, v1}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/android/settings/dashboard/DashboardFragment;->onAttach(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v1, "device_address"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "group_op"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getShort(Ljava/lang/String;)S

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    iput-boolean v2, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mGroupOperation:Z

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object p1

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->mLocalManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    if-nez v1, :cond_1

    const-string v1, "Local mgr is NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/bluetooth/Utils;->getLocalBtManager(Landroid/content/Context;)Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->mLocalManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    if-nez v1, :cond_1

    const-string p0, "Bluetooth is not supported on this device"

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->mLocalManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/LocalBluetoothManager;->getCachedDeviceManager()Lcom/android/settingslib/bluetooth/CachedBluetoothDeviceManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDeviceManager;->findDevice(Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    if-nez p1, :cond_2

    return-void

    :cond_2
    iget-object p1, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->mLocalManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/LocalBluetoothManager;->getProfileManager()Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mProfileManager:Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;->getBCProfile()Lcom/android/settingslib/bluetooth/LocalBluetoothProfile;

    move-result-object p1

    check-cast p1, Lcom/android/settingslib/bluetooth/BCProfile;

    iget-object v1, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mScanAssistCallback:Landroid/bluetooth/BleBroadcastAudioScanAssistCallback;

    invoke-virtual {p1, v1, v2}, Lcom/android/settingslib/bluetooth/BCProfile;->getBSAManager(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BleBroadcastAudioScanAssistCallback;)Landroid/bluetooth/BleBroadcastAudioScanAssistManager;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mScanAssistManager:Landroid/bluetooth/BleBroadcastAudioScanAssistManager;

    if-nez p1, :cond_3

    const-string/jumbo p0, "not able to instantiate scanAssistManager"

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return-void
.end method

.method public onBluetoothStateChanged(I)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->onBluetoothStateChanged(I)V

    invoke-virtual {p0, p1}, Lcom/android/settings/bluetooth/BluetoothSADetail;->updateContent(I)V

    const/16 v0, 0xc

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothSADetail;->showBluetoothTurnedOnToast()V

    :cond_0
    return-void
.end method

.method public onDeviceAdded(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)V
    .locals 0

    return-void
.end method

.method onDevicePreferenceClick(Lcom/android/settings/bluetooth/BluetoothDevicePreference;)V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    const-string v1, "calling selectAudioSource"

    goto/32 :goto_d

    nop

    :goto_1
    return-void

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_7

    nop

    :goto_3
    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothSADetail;->disableScanning()V

    goto/32 :goto_f

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mScanAssistManager:Landroid/bluetooth/BleBroadcastAudioScanAssistManager;

    goto/32 :goto_2

    nop

    :goto_5
    iget-boolean p0, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mGroupOperation:Z

    goto/32 :goto_a

    nop

    :goto_6
    invoke-static {p1, v0}, Lcom/android/settingslib/bluetooth/VendorCachedBluetoothDevice;->getVendorCachedBluetoothDevice(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;)Lcom/android/settingslib/bluetooth/VendorCachedBluetoothDevice;

    move-result-object p1

    goto/32 :goto_4

    nop

    :goto_7
    const-string v0, "BluetoothSADetail"

    goto/32 :goto_0

    nop

    :goto_8
    iput-object p1, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->clickedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    goto/32 :goto_9

    nop

    :goto_9
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mProfileManager:Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;

    goto/32 :goto_6

    nop

    :goto_a
    invoke-virtual {v0, p1, p0}, Landroid/bluetooth/BleBroadcastAudioScanAssistManager;->selectBroadcastSource(Landroid/bluetooth/le/ScanResult;Z)Z

    :goto_b
    goto/32 :goto_1

    nop

    :goto_c
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/VendorCachedBluetoothDevice;->getScanResult()Landroid/bluetooth/le/ScanResult;

    move-result-object p1

    goto/32 :goto_5

    nop

    :goto_d
    invoke-static {v0, v1}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_e

    nop

    :goto_e
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mScanAssistManager:Landroid/bluetooth/BleBroadcastAudioScanAssistManager;

    goto/32 :goto_c

    nop

    :goto_f
    invoke-virtual {p1}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->getBluetoothDevice()Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    move-result-object p1

    goto/32 :goto_8

    nop
.end method

.method public onStart()V
    .locals 4

    const-string v0, "BluetoothSADetail"

    const-string v1, "OnStart Called"

    invoke-static {v0, v1}, Lcom/android/settings/bluetooth/BroadcastScanAssistanceUtils;->debug(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->onStart()V

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->mLocalManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    if-nez v1, :cond_0

    const-string p0, "Bluetooth is not supported on this device"

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/settings/bluetooth/BluetoothSADetail;->updateContent(I)V

    iget-object v1, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mAvailableDevicesCategory:Lcom/android/settings/bluetooth/BluetoothProgressCategory;

    iget-object v2, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->isDiscovering()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/settings/MiuiProgressCategory;->setProgress(Z)V

    iget-object v1, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mScanAssistManager:Landroid/bluetooth/BleBroadcastAudioScanAssistManager;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mProfileManager:Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->mLocalManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/LocalBluetoothManager;->getProfileManager()Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mProfileManager:Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;

    :cond_1
    iget-object v1, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mProfileManager:Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;->getBCProfile()Lcom/android/settingslib/bluetooth/LocalBluetoothProfile;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/bluetooth/BCProfile;

    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    invoke-virtual {v2}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mScanAssistCallback:Landroid/bluetooth/BleBroadcastAudioScanAssistCallback;

    invoke-virtual {v1, v2, v3}, Lcom/android/settingslib/bluetooth/BCProfile;->getBSAManager(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BleBroadcastAudioScanAssistCallback;)Landroid/bluetooth/BleBroadcastAudioScanAssistManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mScanAssistManager:Landroid/bluetooth/BleBroadcastAudioScanAssistManager;

    if-nez v1, :cond_2

    const-string p0, "On Start: not able to instantiate scanAssistManager"

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->onStop()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->mLocalManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    if-nez v0, :cond_0

    const-string p0, "BluetoothSADetail"

    const-string v0, "Bluetooth is not supported on this device"

    invoke-static {p0, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothSADetail;->disableScanning()V

    iget-object v0, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->mDevicePreferenceMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mScanAssistManager:Landroid/bluetooth/BleBroadcastAudioScanAssistManager;

    return-void
.end method

.method showBluetoothTurnedOnToast()V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p0

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {p0}, Landroid/widget/Toast;->show()V

    goto/32 :goto_4

    nop

    :goto_2
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    goto/32 :goto_5

    nop

    :goto_3
    const/4 v1, 0x0

    goto/32 :goto_0

    nop

    :goto_4
    return-void

    :goto_5
    sget v0, Lcom/android/settings/R$string;->connected_device_bluetooth_turned_on_toast:I

    goto/32 :goto_3

    nop
.end method

.method updateContent(I)V
    .locals 3

    goto/32 :goto_0

    nop

    :goto_0
    const/16 v0, 0xa

    goto/32 :goto_7

    nop

    :goto_1
    invoke-virtual {p0, p1}, Lcom/android/settings/bluetooth/BluetoothSADetail;->updateFooterPreference(Landroidx/preference/Preference;)V

    goto/32 :goto_d

    nop

    :goto_2
    const/16 v0, 0xc

    goto/32 :goto_c

    nop

    :goto_3
    sget v0, Lcom/android/settings/R$string;->bluetooth_preference_found_media_devices:I

    goto/32 :goto_14

    nop

    :goto_4
    goto :goto_b

    :goto_5
    goto/32 :goto_a

    nop

    :goto_6
    iget-object p1, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mFooterPreference:Lcom/android/settingslib/widget/FooterPreference;

    goto/32 :goto_1

    nop

    :goto_7
    if-ne p1, v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_2

    nop

    :goto_8
    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->addDeviceCategory(Landroidx/preference/PreferenceGroup;ILcom/android/settingslib/bluetooth/BluetoothDeviceFilter$Filter;Z)V

    goto/32 :goto_6

    nop

    :goto_9
    const/4 v2, 0x0

    goto/32 :goto_8

    nop

    :goto_a
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->finish()V

    :goto_b
    goto/32 :goto_e

    nop

    :goto_c
    if-ne p1, v0, :cond_1

    goto/32 :goto_12

    :cond_1
    goto/32 :goto_11

    nop

    :goto_d
    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothSADetail;->enableScanning()V

    goto/32 :goto_4

    nop

    :goto_e
    return-void

    :goto_f
    iget-object p1, p0, Lcom/android/settings/bluetooth/DeviceListPreferenceFragment;->mDevicePreferenceMap:Ljava/util/HashMap;

    goto/32 :goto_13

    nop

    :goto_10
    iget-object p1, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mAvailableDevicesCategory:Lcom/android/settings/bluetooth/BluetoothProgressCategory;

    goto/32 :goto_3

    nop

    :goto_11
    goto :goto_b

    :goto_12
    goto/32 :goto_f

    nop

    :goto_13
    invoke-virtual {p1}, Ljava/util/HashMap;->clear()V

    goto/32 :goto_10

    nop

    :goto_14
    sget-object v1, Lcom/android/settingslib/bluetooth/BluetoothDeviceFilter;->ALL_FILTER:Lcom/android/settingslib/bluetooth/BluetoothDeviceFilter$Filter;

    goto/32 :goto_9

    nop
.end method

.method updateFooterPreference(Landroidx/preference/Preference;)V
    .locals 4

    goto/32 :goto_b

    nop

    :goto_0
    new-array v2, v2, [Ljava/lang/Object;

    goto/32 :goto_2

    nop

    :goto_1
    invoke-virtual {v3}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_3

    nop

    :goto_2
    iget-object v3, p0, Lcom/android/settings/bluetooth/BluetoothSADetail;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    goto/32 :goto_1

    nop

    :goto_3
    invoke-virtual {v0, v3}, Landroid/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_4
    const/4 v2, 0x1

    goto/32 :goto_0

    nop

    :goto_5
    invoke-virtual {p0, v1, v2}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_6

    nop

    :goto_6
    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    goto/32 :goto_a

    nop

    :goto_7
    sget v1, Lcom/android/settings/R$string;->bluetooth_footer_mac_message:I

    goto/32 :goto_4

    nop

    :goto_8
    const/4 v3, 0x0

    goto/32 :goto_9

    nop

    :goto_9
    aput-object v0, v2, v3

    goto/32 :goto_5

    nop

    :goto_a
    return-void

    :goto_b
    invoke-static {}, Landroid/text/BidiFormatter;->getInstance()Landroid/text/BidiFormatter;

    move-result-object v0

    goto/32 :goto_7

    nop
.end method
