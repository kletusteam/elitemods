.class public Lcom/android/settings/bluetooth/GroupUtils;
.super Ljava/lang/Object;


# static fields
.field private static final D:Z

.field private static final V:Z


# instance fields
.field private mBCProfile:Lcom/android/settingslib/bluetooth/LocalBluetoothProfile;

.field private mCacheDeviceNamanger:Lcom/android/settingslib/bluetooth/CachedBluetoothDeviceManager;

.field private mCtx:Landroid/content/Context;

.field private mDeviceGroup:Landroid/bluetooth/DeviceGroup;

.field private mGroupClientProfile:Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;

.field private mIsGroupEnabled:Z

.field private mLocalBluetoothManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

.field protected mProfileManager:Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget-boolean v0, Lcom/android/settings/connecteddevice/ConnectedDeviceDashboardFragment;->DBG_GROUP:Z

    sput-boolean v0, Lcom/android/settings/bluetooth/GroupUtils;->D:Z

    const-string v0, "GroupUtilss"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/settings/bluetooth/GroupUtils;->V:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/bluetooth/GroupUtils;->mIsGroupEnabled:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/bluetooth/GroupUtils;->mBCProfile:Lcom/android/settingslib/bluetooth/LocalBluetoothProfile;

    iput-object p1, p0, Lcom/android/settings/bluetooth/GroupUtils;->mCtx:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/bluetooth/Utils;->getLocalBtManager(Landroid/content/Context;)Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    move-result-object p1

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/LocalBluetoothManager;->getCachedDeviceManager()Lcom/android/settingslib/bluetooth/CachedBluetoothDeviceManager;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/GroupUtils;->mCacheDeviceNamanger:Lcom/android/settingslib/bluetooth/CachedBluetoothDeviceManager;

    invoke-direct {p0}, Lcom/android/settings/bluetooth/GroupUtils;->isGroupEnabled()V

    iget-object p1, p0, Lcom/android/settings/bluetooth/GroupUtils;->mCtx:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/bluetooth/Utils;->getLocalBtManager(Landroid/content/Context;)Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/GroupUtils;->mLocalBluetoothManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    if-nez p1, :cond_0

    const-string p0, "GroupUtilss"

    const-string p1, "Bluetooth is not supported on this device"

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/LocalBluetoothManager;->getProfileManager()Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/GroupUtils;->mProfileManager:Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;->getBCProfile()Lcom/android/settingslib/bluetooth/LocalBluetoothProfile;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/GroupUtils;->mBCProfile:Lcom/android/settingslib/bluetooth/LocalBluetoothProfile;

    return-void
.end method

.method private getExistingGroup(Ljava/util/ArrayList;Landroidx/preference/Preference;)Lcom/android/settings/widget/GroupPreferenceCategory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/widget/GroupPreferenceCategory;",
            ">;",
            "Landroidx/preference/Preference;",
            ")",
            "Lcom/android/settings/widget/GroupPreferenceCategory;"
        }
    .end annotation

    invoke-direct {p0, p2}, Lcom/android/settings/bluetooth/GroupUtils;->getGroupId(Landroidx/preference/Preference;)I

    move-result p0

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/android/settings/widget/GroupPreferenceCategory;

    invoke-virtual {p2}, Lcom/android/settings/widget/GroupPreferenceCategory;->getGroupId()I

    move-result v0

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    :goto_0
    return-object p2
.end method

.method private getGroupId(Landroidx/preference/Preference;)I
    .locals 3

    instance-of v0, p1, Lcom/android/settings/bluetooth/BluetoothDevicePreference;

    const/4 v1, -0x1

    if-eqz v0, :cond_0

    check-cast p1, Lcom/android/settings/bluetooth/BluetoothDevicePreference;

    invoke-virtual {p1}, Lcom/android/settings/bluetooth/BluetoothDevicePreference;->getBluetoothDevice()Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/android/settings/bluetooth/GroupUtils;->getGroupId(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    move v0, v1

    :goto_0
    if-ne v0, v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "group id not found "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/GroupUtils;->loge(Ljava/lang/String;)V

    :cond_1
    return v0
.end method

.method private getHedaer(ILcom/android/settings/widget/GearPreference$OnGearClickListener;)Lcom/android/settings/bluetooth/GroupBluetoothSettingsPreference;
    .locals 1

    new-instance v0, Lcom/android/settings/bluetooth/GroupBluetoothSettingsPreference;

    iget-object p0, p0, Lcom/android/settings/bluetooth/GroupUtils;->mCtx:Landroid/content/Context;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/bluetooth/GroupBluetoothSettingsPreference;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, p2}, Lcom/android/settings/widget/GearPreference;->setOnGearClickListener(Lcom/android/settings/widget/GearPreference$OnGearClickListener;)V

    return-object v0
.end method

.method private getParentGroup(Ljava/util/ArrayList;Landroidx/preference/Preference;)Lcom/android/settings/widget/GroupPreferenceCategory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/widget/GroupPreferenceCategory;",
            ">;",
            "Landroidx/preference/Preference;",
            ")",
            "Lcom/android/settings/widget/GroupPreferenceCategory;"
        }
    .end annotation

    const/4 p0, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p2

    add-int/lit8 p2, p2, -0x1

    if-ge p0, p2, :cond_1

    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/android/settings/widget/GroupPreferenceCategory;

    invoke-virtual {p2}, Landroidx/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    :goto_1
    return-object p2
.end method

.method private isAllFilled(ILjava/util/ArrayList;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/widget/GroupPreferenceCategory;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    if-ge v1, v2, :cond_2

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/widget/GroupPreferenceCategory;

    if-nez v2, :cond_0

    const-string p1, "isAllFilled"

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/GroupUtils;->loge(Ljava/lang/String;)V

    return v0

    :cond_0
    invoke-virtual {v2}, Lcom/android/settings/widget/GroupPreferenceCategory;->getGroupId()I

    move-result v2

    if-ne p1, v2, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    move v0, v3

    :goto_1
    return v0
.end method

.method private isAllGroupsFilled(Ljava/util/ArrayList;Lcom/android/settings/bluetooth/GroupBluetoothSettingsPreference;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/widget/GroupPreferenceCategory;",
            ">;",
            "Lcom/android/settings/bluetooth/GroupBluetoothSettingsPreference;",
            ")V"
        }
    .end annotation

    const/4 v0, -0x1

    invoke-direct {p0, v0, p1}, Lcom/android/settings/bluetooth/GroupUtils;->isAllFilled(ILjava/util/ArrayList;)Z

    move-result v0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/android/settings/widget/GroupPreferenceCategory;

    if-eqz v0, :cond_6

    if-nez p1, :cond_0

    const-string p1, "isAllGroupsFilled received invalid group"

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/GroupUtils;->loge(Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "remaining"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string p1, "isAllGroupsFilled not last group"

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/GroupUtils;->loge(Ljava/lang/String;)V

    return-void

    :cond_1
    invoke-virtual {p1}, Landroidx/preference/PreferenceGroup;->getPreferenceCount()I

    move-result p0

    sget-boolean v0, Lcom/android/settings/bluetooth/GroupUtils;->D:Z

    const-string v1, "GroupUtilss"

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isAllGroupsFilled size "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, p0, :cond_4

    invoke-virtual {p1, v3}, Landroidx/preference/PreferenceGroup;->getPreference(I)Landroidx/preference/Preference;

    move-result-object v4

    instance-of v4, v4, Lcom/android/settings/bluetooth/GroupBluetoothSettingsPreference;

    if-eqz v4, :cond_3

    invoke-virtual {p1, v3}, Landroidx/preference/PreferenceGroup;->getPreference(I)Landroidx/preference/Preference;

    move-result-object v4

    check-cast v4, Lcom/android/settings/bluetooth/GroupBluetoothSettingsPreference;

    invoke-virtual {p2}, Lcom/android/settings/bluetooth/GroupBluetoothSettingsPreference;->getGroupId()I

    move-result v5

    invoke-virtual {v4}, Lcom/android/settings/bluetooth/GroupBluetoothSettingsPreference;->getGroupId()I

    move-result v6

    if-ne v5, v6, :cond_3

    invoke-virtual {v4}, Lcom/android/settings/bluetooth/GroupBluetoothSettingsPreference;->incrementChildCound()I

    move-result p0

    sget-boolean v0, Lcom/android/settings/bluetooth/GroupUtils;->D:Z

    if-eqz v0, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isAllGroupsFilled updated chCount "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    move v2, v0

    :cond_5
    :goto_1
    if-nez v2, :cond_6

    invoke-virtual {p2}, Lcom/android/settings/bluetooth/GroupBluetoothSettingsPreference;->incrementChildCound()I

    move-result p0

    invoke-virtual {p1, p2}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    sget-boolean p1, Lcom/android/settings/bluetooth/GroupUtils;->D:Z

    if-eqz p1, :cond_6

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "isAllGroupsFilled added chCount "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    return-void
.end method

.method private isGroupDeviceBonded(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)Z
    .locals 2

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getBondState()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/settings/bluetooth/GroupUtils;->isGroupDevice(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method private isGroupDeviceBondedOnly(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)Z
    .locals 2

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getBondState()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/settings/bluetooth/GroupUtils;->isGroupDevice(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method private isGroupEnabled()V
    .locals 5

    const-string v0, "GroupUtilss"

    const/4 v1, 0x0

    :try_start_0
    const-string/jumbo v2, "persist.vendor.service.bt.adv_audio_mask"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v2

    sget-boolean v3, Lcom/android/settings/bluetooth/GroupUtils;->D:Z

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isGroupEnabled advAudioFeatureMask "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eqz v2, :cond_1

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/settings/bluetooth/GroupUtils;->mIsGroupEnabled:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    iput-boolean v1, p0, Lcom/android/settings/bluetooth/GroupUtils;->mIsGroupEnabled:Z

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isGroupEnabled "

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void
.end method

.method private isGroupIdMatch(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;I)Z
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/settings/bluetooth/GroupUtils;->getGroupId(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)I

    move-result p0

    if-ne p2, p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private isNewGroup(ILjava/util/ArrayList;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/widget/GroupPreferenceCategory;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    const-string v4, "GroupUtilss"

    if-ge v1, v2, :cond_3

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/widget/GroupPreferenceCategory;

    if-nez v2, :cond_0

    const-string p1, "isNewGroup  tempGroup null"

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/GroupUtils;->loge(Ljava/lang/String;)V

    return v0

    :cond_0
    invoke-virtual {v2}, Lcom/android/settings/widget/GroupPreferenceCategory;->getGroupId()I

    move-result v3

    sget-boolean v5, Lcom/android/settings/bluetooth/GroupUtils;->D:Z

    if-eqz v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isNewGroup val "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, " key "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    if-ne p1, v3, :cond_2

    goto :goto_1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    move v0, v3

    :goto_1
    sget-boolean p0, Lcom/android/settings/bluetooth/GroupUtils;->D:Z

    if-eqz p0, :cond_4

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "isNewGroup id  "

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "  val "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v4, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    return v0
.end method

.method private isValid()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bluetooth/GroupUtils;->mGroupClientProfile:Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/bluetooth/GroupUtils;->mCtx:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/bluetooth/Utils;->getLocalBtManager(Landroid/content/Context;)Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/LocalBluetoothManager;->getProfileManager()Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;->getDeviceGroupClientProfile()Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/GroupUtils;->mGroupClientProfile:Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;

    :cond_0
    iget-object p0, p0, Lcom/android/settings/bluetooth/GroupUtils;->mGroupClientProfile:Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;

    if-nez p0, :cond_1

    const/4 p0, 0x0

    goto :goto_0

    :cond_1
    const/4 p0, 0x1

    :goto_0
    return p0
.end method

.method private loge(Ljava/lang/String;)V
    .locals 0

    const-string p0, "GroupUtilss"

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private removePreference(Lcom/android/settings/widget/GroupPreferenceCategory;Landroidx/preference/Preference;)V
    .locals 6

    invoke-virtual {p1}, Landroidx/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo p1, "removePreference Header invalid"

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/GroupUtils;->loge(Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-direct {p0, p2}, Lcom/android/settings/bluetooth/GroupUtils;->getGroupId(Landroidx/preference/Preference;)I

    move-result p2

    const/4 v1, -0x1

    if-ne p2, v1, :cond_1

    const-string/jumbo p1, "removePreference Header groupId is invalid"

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/GroupUtils;->loge(Ljava/lang/String;)V

    return-void

    :cond_1
    const/4 p0, 0x0

    :goto_0
    if-ge p0, v0, :cond_5

    invoke-virtual {p1, p0}, Landroidx/preference/PreferenceGroup;->getPreference(I)Landroidx/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/settings/bluetooth/GroupBluetoothSettingsPreference;

    sget-boolean v2, Lcom/android/settings/bluetooth/GroupUtils;->D:Z

    const-string v3, "GroupUtilss"

    if-eqz v2, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "removePreference Header headerPreference "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v5, " header id "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/android/settings/bluetooth/GroupBluetoothSettingsPreference;->getGroupId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, " groupId "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {v1}, Lcom/android/settings/bluetooth/GroupBluetoothSettingsPreference;->getGroupId()I

    move-result v4

    if-ne v4, p2, :cond_4

    invoke-virtual {v1}, Lcom/android/settings/bluetooth/GroupBluetoothSettingsPreference;->decrementChildCount()I

    move-result v4

    if-eqz v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "removePreference Header group id  chCount "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    if-gtz v4, :cond_4

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_4
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    :cond_5
    return-void
.end method

.method private updateGroupStatus(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;I)V
    .locals 2

    invoke-virtual {p1, p2}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->setDeviceType(I)V

    iget-object v0, p0, Lcom/android/settings/bluetooth/GroupUtils;->mCacheDeviceNamanger:Lcom/android/settingslib/bluetooth/CachedBluetoothDeviceManager;

    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDeviceManager;->findDevice(Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->setDeviceType(I)V

    sget-boolean p0, Lcom/android/settings/bluetooth/GroupUtils;->D:Z

    if-eqz p0, :cond_1

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v0, "updateGroupStatus updated "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "GroupUtilss"

    invoke-static {p1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateGroupStatus failed  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " groupId "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/GroupUtils;->loge(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method addDevice(Ljava/util/ArrayList;ILcom/android/settingslib/bluetooth/CachedBluetoothDevice;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;",
            ">;I",
            "Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;",
            ")Z"
        }
    .end annotation

    goto/32 :goto_1e

    nop

    :goto_0
    const-string p1, " is added "

    goto/32 :goto_f

    nop

    :goto_1
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_0

    nop

    :goto_2
    const-string p1, " name "

    goto/32 :goto_d

    nop

    :goto_3
    const-string p1, "GroupUtilss"

    goto/32 :goto_9

    nop

    :goto_4
    if-nez p0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_1f

    nop

    :goto_5
    sget-boolean p0, Lcom/android/settings/bluetooth/GroupUtils;->D:Z

    goto/32 :goto_21

    nop

    :goto_6
    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result p2

    :goto_7
    goto/32 :goto_5

    nop

    :goto_8
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_3

    nop

    :goto_9
    invoke-static {p1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_a
    goto/32 :goto_16

    nop

    :goto_b
    invoke-virtual {p0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_2

    nop

    :goto_c
    const-string p1, "addDevice cachedDevice "

    goto/32 :goto_18

    nop

    :goto_d
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1a

    nop

    :goto_e
    const/4 v0, 0x1

    goto/32 :goto_4

    nop

    :goto_f
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_22

    nop

    :goto_10
    move v0, p2

    :goto_11
    goto/32 :goto_19

    nop

    :goto_12
    if-nez v1, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_1d

    nop

    :goto_13
    if-nez v1, :cond_2

    goto/32 :goto_20

    :cond_2
    goto/32 :goto_10

    nop

    :goto_14
    const/4 p2, 0x0

    goto/32 :goto_e

    nop

    :goto_15
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto/32 :goto_13

    nop

    :goto_16
    return v0

    :goto_17
    invoke-virtual {p3}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_15

    nop

    :goto_18
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_b

    nop

    :goto_19
    if-nez v0, :cond_3

    goto/32 :goto_7

    :cond_3
    goto/32 :goto_6

    nop

    :goto_1a
    invoke-virtual {p3}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getName()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_1b
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_12

    nop

    :goto_1c
    check-cast v1, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    goto/32 :goto_23

    nop

    :goto_1d
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_1c

    nop

    :goto_1e
    invoke-virtual {p0, p2, p3}, Lcom/android/settings/bluetooth/GroupUtils;->isUpdate(ILcom/android/settingslib/bluetooth/CachedBluetoothDevice;)Z

    move-result p0

    goto/32 :goto_14

    nop

    :goto_1f
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_20
    goto/32 :goto_1b

    nop

    :goto_21
    if-nez p0, :cond_4

    goto/32 :goto_a

    :cond_4
    goto/32 :goto_25

    nop

    :goto_22
    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    goto/32 :goto_8

    nop

    :goto_23
    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_17

    nop

    :goto_24
    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_c

    nop

    :goto_25
    new-instance p0, Ljava/lang/StringBuilder;

    goto/32 :goto_24

    nop
.end method

.method public addPreference(Ljava/util/ArrayList;Landroidx/preference/Preference;Lcom/android/settings/widget/GearPreference$OnGearClickListener;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/widget/GroupPreferenceCategory;",
            ">;",
            "Landroidx/preference/Preference;",
            "Lcom/android/settings/widget/GearPreference$OnGearClickListener;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p2}, Lcom/android/settings/bluetooth/GroupUtils;->getGroupId(Landroidx/preference/Preference;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "addPreference groupId is not valid "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/GroupUtils;->loge(Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-direct {p0, v0, p1}, Lcom/android/settings/bluetooth/GroupUtils;->isNewGroup(ILjava/util/ArrayList;)Z

    move-result v1

    sget-boolean v2, Lcom/android/settings/bluetooth/GroupUtils;->D:Z

    const-string v3, "GroupUtilss"

    if-eqz v2, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "addPreference  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v5, " isNewGroup "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    if-eqz v1, :cond_3

    invoke-direct {p0, v0, p3}, Lcom/android/settings/bluetooth/GroupUtils;->getHedaer(ILcom/android/settings/widget/GearPreference$OnGearClickListener;)Lcom/android/settings/bluetooth/GroupBluetoothSettingsPreference;

    move-result-object p3

    invoke-direct {p0, p1, p2}, Lcom/android/settings/bluetooth/GroupUtils;->getParentGroup(Ljava/util/ArrayList;Landroidx/preference/Preference;)Lcom/android/settings/widget/GroupPreferenceCategory;

    move-result-object v1

    if-nez v1, :cond_2

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getParentGroup not found for groupId "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/android/settings/bluetooth/GroupUtils;->loge(Ljava/lang/String;)V

    invoke-direct {p0, p1, p3}, Lcom/android/settings/bluetooth/GroupUtils;->isAllGroupsFilled(Ljava/util/ArrayList;Lcom/android/settings/bluetooth/GroupBluetoothSettingsPreference;)V

    return-void

    :cond_2
    invoke-virtual {v1, v0}, Lcom/android/settings/widget/GroupPreferenceCategory;->setGroupId(I)V

    invoke-virtual {v1, p3}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    invoke-virtual {v1, p2}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, Landroidx/preference/Preference;->setVisible(Z)V

    goto :goto_0

    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/android/settings/bluetooth/GroupUtils;->getExistingGroup(Ljava/util/ArrayList;Landroidx/preference/Preference;)Lcom/android/settings/widget/GroupPreferenceCategory;

    move-result-object v1

    if-nez v1, :cond_4

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "getExistingGroup not found for groupId "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/GroupUtils;->loge(Ljava/lang/String;)V

    return-void

    :cond_4
    invoke-virtual {v1, p2}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    :goto_0
    if-eqz v2, :cond_5

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p1, "addPreference  key "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v3, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    return-void
.end method

.method connectGroup(I)Z
    .locals 1

    goto/32 :goto_5

    nop

    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;->connectGroup(I)Z

    move-result p0

    goto/32 :goto_6

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_2

    nop

    :goto_2
    iget-object p0, p0, Lcom/android/settings/bluetooth/GroupUtils;->mGroupClientProfile:Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;

    goto/32 :goto_0

    nop

    :goto_3
    return p0

    :goto_4
    const/4 p0, 0x0

    goto/32 :goto_3

    nop

    :goto_5
    invoke-direct {p0}, Lcom/android/settings/bluetooth/GroupUtils;->isValid()Z

    move-result v0

    goto/32 :goto_1

    nop

    :goto_6
    return p0

    :goto_7
    goto/32 :goto_4

    nop
.end method

.method disconnectGroup(I)Z
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;->disconnectGroup(I)Z

    move-result p0

    goto/32 :goto_5

    nop

    :goto_1
    invoke-direct {p0}, Lcom/android/settings/bluetooth/GroupUtils;->isValid()Z

    move-result v0

    goto/32 :goto_3

    nop

    :goto_2
    return p0

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_7

    nop

    :goto_4
    const/4 p0, 0x0

    goto/32 :goto_2

    nop

    :goto_5
    return p0

    :goto_6
    goto/32 :goto_4

    nop

    :goto_7
    iget-object p0, p0, Lcom/android/settings/bluetooth/GroupUtils;->mGroupClientProfile:Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;

    goto/32 :goto_0

    nop
.end method

.method forgetGroup(I)Z
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return p0

    :goto_1
    invoke-direct {p0}, Lcom/android/settings/bluetooth/GroupUtils;->isValid()Z

    move-result v0

    goto/32 :goto_4

    nop

    :goto_2
    const/4 p0, 0x0

    goto/32 :goto_0

    nop

    :goto_3
    invoke-virtual {p0, p1}, Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;->forgetGroup(I)Z

    move-result p0

    goto/32 :goto_5

    nop

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_7

    nop

    :goto_5
    return p0

    :goto_6
    goto/32 :goto_2

    nop

    :goto_7
    iget-object p0, p0, Lcom/android/settings/bluetooth/GroupUtils;->mGroupClientProfile:Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;

    goto/32 :goto_3

    nop
.end method

.method public getAnyBCConnectedDevice(I)Landroid/bluetooth/BluetoothDevice;
    .locals 4

    iget-object v0, p0, Lcom/android/settings/bluetooth/GroupUtils;->mGroupClientProfile:Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;->getGroup(I)Landroid/bluetooth/DeviceGroup;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/GroupUtils;->mDeviceGroup:Landroid/bluetooth/DeviceGroup;

    const-string v0, "GroupUtilss"

    const/4 v1, 0x0

    if-nez p1, :cond_0

    const-string p0, "getAnyBCConnectedDevice: dGrp is null"

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :cond_0
    iget-object v2, p0, Lcom/android/settings/bluetooth/GroupUtils;->mBCProfile:Lcom/android/settingslib/bluetooth/LocalBluetoothProfile;

    if-nez v2, :cond_1

    const-string p0, "getAnyBCConnectedDevice: BCProfile is null"

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :cond_1
    invoke-virtual {p1}, Landroid/bluetooth/DeviceGroup;->getDeviceGroupMembers()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    iget-object v2, p0, Lcom/android/settings/bluetooth/GroupUtils;->mBCProfile:Lcom/android/settingslib/bluetooth/LocalBluetoothProfile;

    invoke-interface {v2, v0}, Lcom/android/settingslib/bluetooth/LocalBluetoothProfile;->getConnectionStatus(Landroid/bluetooth/BluetoothDevice;)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    move-object v1, v0

    :cond_3
    return-object v1
.end method

.method getCahcedDevice(I)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList<",
            "Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;",
            ">;"
        }
    .end annotation

    goto/32 :goto_10

    nop

    :goto_0
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_19

    nop

    :goto_1
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_4

    nop

    :goto_2
    invoke-static {p1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    goto/32 :goto_c

    nop

    :goto_4
    const-string p1, " list "

    goto/32 :goto_9

    nop

    :goto_5
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_12

    nop

    :goto_6
    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/CachedBluetoothDeviceManager;->getCachedDevicesCopy()Ljava/util/Collection;

    move-result-object v0

    goto/32 :goto_24

    nop

    :goto_7
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_1a

    nop

    :goto_8
    check-cast v2, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    goto/32 :goto_e

    nop

    :goto_9
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1c

    nop

    :goto_a
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result p1

    goto/32 :goto_21

    nop

    :goto_b
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_a

    nop

    :goto_c
    return-object v1

    :goto_d
    const-string p1, " "

    goto/32 :goto_b

    nop

    :goto_e
    if-nez v2, :cond_0

    goto/32 :goto_13

    :cond_0
    goto/32 :goto_1f

    nop

    :goto_f
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_8

    nop

    :goto_10
    iget-object v0, p0, Lcom/android/settings/bluetooth/GroupUtils;->mCacheDeviceNamanger:Lcom/android/settingslib/bluetooth/CachedBluetoothDeviceManager;

    goto/32 :goto_6

    nop

    :goto_11
    if-nez p0, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_22

    nop

    :goto_12
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_13
    goto/32 :goto_15

    nop

    :goto_14
    sget-boolean p0, Lcom/android/settings/bluetooth/GroupUtils;->D:Z

    goto/32 :goto_11

    nop

    :goto_15
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    goto/32 :goto_20

    nop

    :goto_16
    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_23

    nop

    :goto_17
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop

    :goto_18
    invoke-virtual {p0, v2}, Lcom/android/settings/bluetooth/GroupUtils;->getGroupId(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)I

    move-result v3

    goto/32 :goto_1e

    nop

    :goto_19
    const-string p1, "GroupUtilss"

    goto/32 :goto_2

    nop

    :goto_1a
    goto :goto_13

    :goto_1b
    goto/32 :goto_14

    nop

    :goto_1c
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_d

    nop

    :goto_1d
    if-nez v3, :cond_2

    goto/32 :goto_13

    :cond_2
    goto/32 :goto_18

    nop

    :goto_1e
    if-eq v3, p1, :cond_3

    goto/32 :goto_13

    :cond_3
    goto/32 :goto_7

    nop

    :goto_1f
    invoke-direct {p0, v2}, Lcom/android/settings/bluetooth/GroupUtils;->isGroupDeviceBonded(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)Z

    move-result v3

    goto/32 :goto_1d

    nop

    :goto_20
    if-nez v2, :cond_4

    goto/32 :goto_1b

    :cond_4
    goto/32 :goto_f

    nop

    :goto_21
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_0

    nop

    :goto_22
    new-instance p0, Ljava/lang/StringBuilder;

    goto/32 :goto_16

    nop

    :goto_23
    const-string v0, "getCahcedDevice "

    goto/32 :goto_17

    nop

    :goto_24
    new-instance v1, Ljava/util/ArrayList;

    goto/32 :goto_5

    nop
.end method

.method getGroupId(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)I
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    new-instance p0, Ljava/lang/StringBuilder;

    goto/32 :goto_f

    nop

    :goto_1
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getQGroupId()I

    move-result v0

    goto/32 :goto_10

    nop

    :goto_2
    const-string v1, " device "

    goto/32 :goto_e

    nop

    :goto_3
    const-string p1, "GroupUtilss"

    goto/32 :goto_8

    nop

    :goto_4
    const-string/jumbo v1, "qgroupId "

    goto/32 :goto_13

    nop

    :goto_5
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_3

    nop

    :goto_6
    sget-boolean p0, Lcom/android/settings/bluetooth/GroupUtils;->D:Z

    goto/32 :goto_12

    nop

    :goto_7
    const-string v1, " qgroupId is -1"

    goto/32 :goto_b

    nop

    :goto_8
    invoke-static {p1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_9
    goto/32 :goto_a

    nop

    :goto_a
    return v0

    :goto_b
    invoke-direct {p0, v1}, Lcom/android/settings/bluetooth/GroupUtils;->loge(Ljava/lang/String;)V

    :goto_c
    goto/32 :goto_6

    nop

    :goto_d
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_5

    nop

    :goto_e
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_d

    nop

    :goto_f
    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_4

    nop

    :goto_10
    const/4 v1, -0x1

    goto/32 :goto_11

    nop

    :goto_11
    if-eq v0, v1, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_7

    nop

    :goto_12
    if-nez p0, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_0

    nop

    :goto_13
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_14

    nop

    :goto_14
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_2

    nop
.end method

.method getGroupSize(I)I
    .locals 1

    goto/32 :goto_a

    nop

    :goto_0
    new-instance p1, Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_1
    sget-boolean p1, Lcom/android/settings/bluetooth/GroupUtils;->D:Z

    goto/32 :goto_d

    nop

    :goto_2
    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_9

    nop

    :goto_3
    goto :goto_13

    :goto_4
    goto/32 :goto_12

    nop

    :goto_5
    if-nez p1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_16

    nop

    :goto_6
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_b

    nop

    :goto_7
    iput-object p1, p0, Lcom/android/settings/bluetooth/GroupUtils;->mDeviceGroup:Landroid/bluetooth/DeviceGroup;

    goto/32 :goto_5

    nop

    :goto_8
    iget-object v0, p0, Lcom/android/settings/bluetooth/GroupUtils;->mGroupClientProfile:Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;

    goto/32 :goto_14

    nop

    :goto_9
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_10

    nop

    :goto_a
    invoke-direct {p0}, Lcom/android/settings/bluetooth/GroupUtils;->isValid()Z

    move-result v0

    goto/32 :goto_11

    nop

    :goto_b
    const-string v0, "getDeviceGroupSize size "

    goto/32 :goto_15

    nop

    :goto_c
    return p0

    :goto_d
    if-nez p1, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_0

    nop

    :goto_e
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_f
    goto/32 :goto_c

    nop

    :goto_10
    const-string v0, "GroupUtilss"

    goto/32 :goto_e

    nop

    :goto_11
    if-nez v0, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_8

    nop

    :goto_12
    const/4 p0, 0x0

    :goto_13
    goto/32 :goto_1

    nop

    :goto_14
    invoke-virtual {v0, p1}, Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;->getGroup(I)Landroid/bluetooth/DeviceGroup;

    move-result-object p1

    goto/32 :goto_7

    nop

    :goto_15
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_2

    nop

    :goto_16
    invoke-virtual {p1}, Landroid/bluetooth/DeviceGroup;->getDeviceGroupSize()I

    move-result p0

    goto/32 :goto_3

    nop
.end method

.method getGroupTitle(I)Ljava/lang/String;
    .locals 1

    goto/32 :goto_5

    nop

    :goto_0
    return-object p0

    :goto_1
    const-string v0, " "

    goto/32 :goto_3

    nop

    :goto_2
    add-int/lit8 p1, p1, 0x1

    goto/32 :goto_7

    nop

    :goto_3
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_2

    nop

    :goto_4
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_0

    nop

    :goto_5
    new-instance p0, Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_6
    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1

    nop

    :goto_7
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_4

    nop
.end method

.method isGroupDevice(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)Z
    .locals 1

    goto/32 :goto_b

    nop

    :goto_0
    const-string v0, "isGroupDevice "

    goto/32 :goto_6

    nop

    :goto_1
    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    goto/32 :goto_26

    nop

    :goto_3
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getmType()I

    move-result v0

    goto/32 :goto_1b

    nop

    :goto_4
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_1d

    nop

    :goto_5
    if-eqz v0, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_f

    nop

    :goto_6
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_24

    nop

    :goto_7
    sget-boolean p0, Lcom/android/settings/bluetooth/GroupUtils;->D:Z

    goto/32 :goto_19

    nop

    :goto_8
    if-eqz v0, :cond_1

    goto/32 :goto_23

    :cond_1
    goto/32 :goto_27

    nop

    :goto_9
    const-string v0, "GroupUtilss"

    goto/32 :goto_1

    nop

    :goto_a
    return p0

    :goto_b
    iget-boolean v0, p0, Lcom/android/settings/bluetooth/GroupUtils;->mIsGroupEnabled:Z

    goto/32 :goto_8

    nop

    :goto_c
    const-string v0, " type "

    goto/32 :goto_1e

    nop

    :goto_d
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_9

    nop

    :goto_e
    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_0

    nop

    :goto_f
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->isTypeUnKnown()Z

    move-result v0

    goto/32 :goto_21

    nop

    :goto_10
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getDeviceType()I

    move-result v0

    goto/32 :goto_13

    nop

    :goto_11
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    goto/32 :goto_10

    nop

    :goto_12
    new-instance p0, Ljava/lang/StringBuilder;

    goto/32 :goto_e

    nop

    :goto_13
    invoke-direct {p0, p1, v0}, Lcom/android/settings/bluetooth/GroupUtils;->updateGroupStatus(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;I)V

    :goto_14
    goto/32 :goto_7

    nop

    :goto_15
    invoke-direct {p0, p1}, Lcom/android/settings/bluetooth/GroupUtils;->loge(Ljava/lang/String;)V

    :goto_16
    goto/32 :goto_1c

    nop

    :goto_17
    const-string p1, " GroupProfile not enabled"

    goto/32 :goto_15

    nop

    :goto_18
    const-string v0, " name "

    goto/32 :goto_1f

    nop

    :goto_19
    if-nez p0, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_12

    nop

    :goto_1a
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_18

    nop

    :goto_1b
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_d

    nop

    :goto_1c
    const/4 p0, 0x0

    goto/32 :goto_22

    nop

    :goto_1d
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_c

    nop

    :goto_1e
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_3

    nop

    :goto_1f
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_4

    nop

    :goto_20
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->isGroupDevice()Z

    move-result v0

    goto/32 :goto_5

    nop

    :goto_21
    if-nez v0, :cond_3

    goto/32 :goto_14

    :cond_3
    goto/32 :goto_11

    nop

    :goto_22
    return p0

    :goto_23
    goto/32 :goto_20

    nop

    :goto_24
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->isGroupDevice()Z

    move-result v0

    goto/32 :goto_28

    nop

    :goto_25
    if-nez p1, :cond_4

    goto/32 :goto_16

    :cond_4
    goto/32 :goto_17

    nop

    :goto_26
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->isGroupDevice()Z

    move-result p0

    goto/32 :goto_a

    nop

    :goto_27
    sget-boolean p1, Lcom/android/settings/bluetooth/GroupUtils;->D:Z

    goto/32 :goto_25

    nop

    :goto_28
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    goto/32 :goto_1a

    nop
.end method

.method isGroupDiscoveryInProgress(I)Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    invoke-direct {p0}, Lcom/android/settings/bluetooth/GroupUtils;->isValid()Z

    move-result v0

    goto/32 :goto_7

    nop

    :goto_1
    iget-object p0, p0, Lcom/android/settings/bluetooth/GroupUtils;->mGroupClientProfile:Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;

    goto/32 :goto_5

    nop

    :goto_2
    const/4 p0, 0x0

    goto/32 :goto_3

    nop

    :goto_3
    return p0

    :goto_4
    goto/32 :goto_1

    nop

    :goto_5
    invoke-virtual {p0, p1}, Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;->isGroupDiscoveryInProgress(I)Z

    move-result p0

    goto/32 :goto_6

    nop

    :goto_6
    return p0

    :goto_7
    if-eqz v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_2

    nop
.end method

.method isHideGroupOptions(I)Z
    .locals 3

    goto/32 :goto_19

    nop

    :goto_0
    const/4 p0, 0x0

    goto/32 :goto_d

    nop

    :goto_1
    if-nez v2, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_f

    nop

    :goto_2
    sget-boolean p1, Lcom/android/settings/bluetooth/GroupUtils;->D:Z

    goto/32 :goto_16

    nop

    :goto_3
    const-string v0, "GroupUtilss"

    goto/32 :goto_1a

    nop

    :goto_4
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    goto/32 :goto_12

    nop

    :goto_5
    new-instance p1, Ljava/lang/StringBuilder;

    goto/32 :goto_a

    nop

    :goto_6
    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    goto/32 :goto_1d

    nop

    :goto_7
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_9
    goto/32 :goto_11

    nop

    :goto_a
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_b

    nop

    :goto_b
    const-string v0, "isHideGroupOptions "

    goto/32 :goto_7

    nop

    :goto_c
    check-cast v1, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    goto/32 :goto_18

    nop

    :goto_d
    goto :goto_1f

    :goto_e
    goto/32 :goto_1e

    nop

    :goto_f
    invoke-direct {p0, v1, p1}, Lcom/android/settings/bluetooth/GroupUtils;->isGroupIdMatch(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;I)Z

    move-result v1

    goto/32 :goto_20

    nop

    :goto_10
    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/CachedBluetoothDeviceManager;->getCachedDevicesCopy()Ljava/util/Collection;

    move-result-object v0

    goto/32 :goto_1c

    nop

    :goto_11
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_14

    nop

    :goto_12
    if-gtz v1, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_8

    nop

    :goto_13
    return p0

    :goto_14
    if-nez v1, :cond_2

    goto/32 :goto_e

    :cond_2
    goto/32 :goto_15

    nop

    :goto_15
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_c

    nop

    :goto_16
    if-nez p1, :cond_3

    goto/32 :goto_1b

    :cond_3
    goto/32 :goto_5

    nop

    :goto_17
    invoke-direct {p0, v1}, Lcom/android/settings/bluetooth/GroupUtils;->isGroupDeviceBonded(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)Z

    move-result v2

    goto/32 :goto_1

    nop

    :goto_18
    if-nez v1, :cond_4

    goto/32 :goto_9

    :cond_4
    goto/32 :goto_17

    nop

    :goto_19
    iget-object v0, p0, Lcom/android/settings/bluetooth/GroupUtils;->mCacheDeviceNamanger:Lcom/android/settingslib/bluetooth/CachedBluetoothDeviceManager;

    goto/32 :goto_10

    nop

    :goto_1a
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1b
    goto/32 :goto_13

    nop

    :goto_1c
    if-nez v0, :cond_5

    goto/32 :goto_e

    :cond_5
    goto/32 :goto_4

    nop

    :goto_1d
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_3

    nop

    :goto_1e
    const/4 p0, 0x1

    :goto_1f
    goto/32 :goto_2

    nop

    :goto_20
    if-nez v1, :cond_6

    goto/32 :goto_9

    :cond_6
    goto/32 :goto_0

    nop
.end method

.method public isHidePCGGroups()Z
    .locals 3

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getMostRecentlyConnectedDevices()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    iget-object v2, p0, Lcom/android/settings/bluetooth/GroupUtils;->mCacheDeviceNamanger:Lcom/android/settingslib/bluetooth/CachedBluetoothDeviceManager;

    invoke-virtual {v2, v1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDeviceManager;->findDevice(Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v1}, Lcom/android/settings/bluetooth/GroupUtils;->isGroupDeviceBondedOnly(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_1
    const/4 p0, 0x1

    :goto_0
    sget-boolean v0, Lcom/android/settings/bluetooth/GroupUtils;->D:Z

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isHidePCGGroups "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GroupUtilss"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return p0
.end method

.method isUpdate(ILcom/android/settingslib/bluetooth/CachedBluetoothDevice;)Z
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return p0

    :goto_1
    if-eq p1, p0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_6

    nop

    :goto_2
    invoke-virtual {p0, p2}, Lcom/android/settings/bluetooth/GroupUtils;->isGroupDevice(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)Z

    move-result v0

    goto/32 :goto_9

    nop

    :goto_3
    invoke-virtual {p0, p2}, Lcom/android/settings/bluetooth/GroupUtils;->getGroupId(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)I

    move-result p0

    goto/32 :goto_1

    nop

    :goto_4
    const/4 p0, 0x0

    :goto_5
    goto/32 :goto_0

    nop

    :goto_6
    const/4 p0, 0x1

    goto/32 :goto_7

    nop

    :goto_7
    goto :goto_5

    :goto_8
    goto/32 :goto_4

    nop

    :goto_9
    if-nez v0, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_3

    nop
.end method

.method launchAddSourceGroup(I)V
    .locals 3

    goto/32 :goto_1e

    nop

    :goto_0
    new-instance v1, Landroid/os/Bundle;

    goto/32 :goto_1b

    nop

    :goto_1
    const/4 p1, 0x1

    goto/32 :goto_1c

    nop

    :goto_2
    if-eqz p1, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_d

    nop

    :goto_3
    sget p1, Lcom/android/settings/R$string;->bluetooth_search_broadcasters:I

    goto/32 :goto_17

    nop

    :goto_4
    invoke-virtual {p0, p1}, Lcom/android/settings/core/SubSettingLauncher;->setSourceMetricsCategory(I)Lcom/android/settings/core/SubSettingLauncher;

    move-result-object p0

    goto/32 :goto_9

    nop

    :goto_5
    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_1

    nop

    :goto_6
    const/16 p1, 0x19

    goto/32 :goto_4

    nop

    :goto_7
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_15

    nop

    :goto_8
    if-nez v1, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_11

    nop

    :goto_9
    invoke-virtual {p0}, Lcom/android/settings/core/SubSettingLauncher;->launch()V

    :goto_a
    goto/32 :goto_1a

    nop

    :goto_b
    iget-object p0, p0, Lcom/android/settings/bluetooth/GroupUtils;->mCtx:Landroid/content/Context;

    goto/32 :goto_f

    nop

    :goto_c
    new-instance p1, Lcom/android/settings/core/SubSettingLauncher;

    goto/32 :goto_b

    nop

    :goto_d
    return-void

    :goto_e
    goto/32 :goto_7

    nop

    :goto_f
    invoke-direct {p1, p0}, Lcom/android/settings/core/SubSettingLauncher;-><init>(Landroid/content/Context;)V

    goto/32 :goto_10

    nop

    :goto_10
    invoke-virtual {p1, v0}, Lcom/android/settings/core/SubSettingLauncher;->setDestination(Ljava/lang/String;)Lcom/android/settings/core/SubSettingLauncher;

    move-result-object p0

    goto/32 :goto_1d

    nop

    :goto_11
    invoke-virtual {p0, p1}, Lcom/android/settings/bluetooth/GroupUtils;->getAnyBCConnectedDevice(I)Landroid/bluetooth/BluetoothDevice;

    move-result-object p1

    goto/32 :goto_0

    nop

    :goto_12
    const/4 v1, 0x0

    :goto_13
    goto/32 :goto_8

    nop

    :goto_14
    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putShort(Ljava/lang/String;S)V

    goto/32 :goto_c

    nop

    :goto_15
    const-string v2, "device_address"

    goto/32 :goto_5

    nop

    :goto_16
    const-string v1, "GroupUtilss"

    goto/32 :goto_19

    nop

    :goto_17
    invoke-virtual {p0, p1}, Lcom/android/settings/core/SubSettingLauncher;->setTitleRes(I)Lcom/android/settings/core/SubSettingLauncher;

    move-result-object p0

    goto/32 :goto_6

    nop

    :goto_18
    goto :goto_13

    :catch_0
    goto/32 :goto_16

    nop

    :goto_19
    const-string/jumbo v2, "no SADetail exists"

    goto/32 :goto_1f

    nop

    :goto_1a
    return-void

    :goto_1b
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    goto/32 :goto_2

    nop

    :goto_1c
    const-string v2, "group_op"

    goto/32 :goto_14

    nop

    :goto_1d
    invoke-virtual {p0, v1}, Lcom/android/settings/core/SubSettingLauncher;->setArguments(Landroid/os/Bundle;)Lcom/android/settings/core/SubSettingLauncher;

    move-result-object p0

    goto/32 :goto_3

    nop

    :goto_1e
    const-string v0, "com.android.settings.bluetooth.BluetoothSADetail"

    :try_start_0
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_18

    nop

    :goto_1f
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_12

    nop
.end method

.method removeDevice(Ljava/util/ArrayList;ILcom/android/settingslib/bluetooth/CachedBluetoothDevice;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;",
            ">;I",
            "Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;",
            ")Z"
        }
    .end annotation

    goto/32 :goto_23

    nop

    :goto_0
    move p0, p2

    :goto_1
    goto/32 :goto_1f

    nop

    :goto_2
    sget-boolean p1, Lcom/android/settings/bluetooth/GroupUtils;->D:Z

    goto/32 :goto_20

    nop

    :goto_3
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_9

    nop

    :goto_4
    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1c

    nop

    :goto_5
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    goto/32 :goto_24

    nop

    :goto_6
    const/4 v0, 0x0

    goto/32 :goto_0

    nop

    :goto_7
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_1b

    nop

    :goto_8
    const/4 p0, 0x1

    goto/32 :goto_21

    nop

    :goto_9
    const-string/jumbo v0, "removeDevice cachedDevice "

    goto/32 :goto_27

    nop

    :goto_a
    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_12

    nop

    :goto_b
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_15

    nop

    :goto_c
    move p0, p2

    :goto_d
    goto/32 :goto_2

    nop

    :goto_e
    invoke-static {p2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_f
    goto/32 :goto_14

    nop

    :goto_10
    if-nez v1, :cond_0

    goto/32 :goto_17

    :cond_0
    goto/32 :goto_8

    nop

    :goto_11
    invoke-virtual {p3}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_2b

    nop

    :goto_12
    const-string p3, " isremoved "

    goto/32 :goto_4

    nop

    :goto_13
    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_25

    nop

    :goto_14
    return p0

    :goto_15
    const-string p2, "GroupUtilss"

    goto/32 :goto_e

    nop

    :goto_16
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_17
    goto/32 :goto_5

    nop

    :goto_18
    new-instance p1, Ljava/lang/StringBuilder;

    goto/32 :goto_3

    nop

    :goto_19
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result p2

    goto/32 :goto_1d

    nop

    :goto_1a
    invoke-virtual {p3}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getName()Ljava/lang/String;

    move-result-object p3

    goto/32 :goto_a

    nop

    :goto_1b
    check-cast v0, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    goto/32 :goto_29

    nop

    :goto_1c
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    goto/32 :goto_b

    nop

    :goto_1d
    goto :goto_d

    :goto_1e
    goto/32 :goto_c

    nop

    :goto_1f
    if-nez p0, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_19

    nop

    :goto_20
    if-nez p1, :cond_2

    goto/32 :goto_f

    :cond_2
    goto/32 :goto_18

    nop

    :goto_21
    goto/16 :goto_1

    :goto_22
    goto/32 :goto_6

    nop

    :goto_23
    invoke-virtual {p0, p2, p3}, Lcom/android/settings/bluetooth/GroupUtils;->isUpdate(ILcom/android/settingslib/bluetooth/CachedBluetoothDevice;)Z

    move-result p0

    goto/32 :goto_28

    nop

    :goto_24
    if-nez v0, :cond_3

    goto/32 :goto_22

    :cond_3
    goto/32 :goto_7

    nop

    :goto_25
    const-string v0, " name "

    goto/32 :goto_2a

    nop

    :goto_26
    if-nez p0, :cond_4

    goto/32 :goto_1e

    :cond_4
    goto/32 :goto_16

    nop

    :goto_27
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_13

    nop

    :goto_28
    const/4 p2, 0x0

    goto/32 :goto_26

    nop

    :goto_29
    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_11

    nop

    :goto_2a
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1a

    nop

    :goto_2b
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto/32 :goto_10

    nop
.end method

.method public removePreference(Ljava/util/ArrayList;Landroidx/preference/Preference;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/widget/GroupPreferenceCategory;",
            ">;",
            "Landroidx/preference/Preference;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/android/settings/bluetooth/GroupUtils;->getExistingGroup(Ljava/util/ArrayList;Landroidx/preference/Preference;)Lcom/android/settings/widget/GroupPreferenceCategory;

    move-result-object v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    const-string/jumbo v0, "removePreference group null "

    invoke-direct {p0, v0}, Lcom/android/settings/bluetooth/GroupUtils;->loge(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int/2addr v0, v1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/android/settings/widget/GroupPreferenceCategory;

    invoke-direct {p0, p1, p2}, Lcom/android/settings/bluetooth/GroupUtils;->removePreference(Lcom/android/settings/widget/GroupPreferenceCategory;Landroidx/preference/Preference;)V

    return-void

    :cond_0
    invoke-virtual {v0, p2}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    invoke-virtual {v0}, Landroidx/preference/PreferenceGroup;->getPreferenceCount()I

    move-result p0

    if-ne p0, v1, :cond_1

    const/4 p0, -0x1

    invoke-virtual {v0, p0}, Lcom/android/settings/widget/GroupPreferenceCategory;->setGroupId(I)V

    invoke-virtual {v0}, Landroidx/preference/PreferenceGroup;->removeAll()V

    const/4 p0, 0x0

    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setVisible(Z)V

    :cond_1
    return-void
.end method

.method startGroupDiscovery(I)Z
    .locals 4

    goto/32 :goto_d

    nop

    :goto_0
    return v1

    :goto_1
    goto/32 :goto_a

    nop

    :goto_2
    const/4 p0, 0x1

    goto/32 :goto_8

    nop

    :goto_3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_14

    nop

    :goto_4
    const/4 v1, 0x0

    goto/32 :goto_1b

    nop

    :goto_5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_11

    nop

    :goto_6
    const-string v3, "GroupUtilss"

    goto/32 :goto_17

    nop

    :goto_7
    sget-boolean v2, Lcom/android/settings/bluetooth/GroupUtils;->D:Z

    goto/32 :goto_15

    nop

    :goto_8
    return p0

    :goto_9
    goto/32 :goto_16

    nop

    :goto_a
    iget-object v0, p0, Lcom/android/settings/bluetooth/GroupUtils;->mGroupClientProfile:Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;

    goto/32 :goto_19

    nop

    :goto_b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_1a

    nop

    :goto_c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_6

    nop

    :goto_d
    invoke-direct {p0}, Lcom/android/settings/bluetooth/GroupUtils;->isValid()Z

    move-result v0

    goto/32 :goto_4

    nop

    :goto_e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_b

    nop

    :goto_f
    iget-object p0, p0, Lcom/android/settings/bluetooth/GroupUtils;->mGroupClientProfile:Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;

    goto/32 :goto_13

    nop

    :goto_10
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_5

    nop

    :goto_11
    const-string/jumbo v3, "startGroupDiscovery "

    goto/32 :goto_e

    nop

    :goto_12
    if-eqz v0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_f

    nop

    :goto_13
    invoke-virtual {p0, p1}, Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;->startGroupDiscovery(I)Z

    goto/32 :goto_2

    nop

    :goto_14
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    goto/32 :goto_c

    nop

    :goto_15
    if-nez v2, :cond_1

    goto/32 :goto_18

    :cond_1
    goto/32 :goto_10

    nop

    :goto_16
    return v1

    :goto_17
    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_18
    goto/32 :goto_12

    nop

    :goto_19
    invoke-virtual {v0, p1}, Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;->isGroupDiscoveryInProgress(I)Z

    move-result v0

    goto/32 :goto_7

    nop

    :goto_1a
    const-string v3, "isDiscovering "

    goto/32 :goto_3

    nop

    :goto_1b
    if-eqz v0, :cond_2

    goto/32 :goto_1

    :cond_2
    goto/32 :goto_0

    nop
.end method

.method stopGroupDiscovery(I)Z
    .locals 4

    goto/32 :goto_8

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/GroupUtils;->mGroupClientProfile:Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;

    goto/32 :goto_b

    nop

    :goto_1
    invoke-virtual {p0, p1}, Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;->stopGroupDiscovery(I)Z

    goto/32 :goto_1a

    nop

    :goto_2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    goto/32 :goto_1b

    nop

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_11

    :cond_0
    goto/32 :goto_13

    nop

    :goto_5
    if-nez v2, :cond_1

    goto/32 :goto_18

    :cond_1
    goto/32 :goto_12

    nop

    :goto_6
    const/4 v1, 0x0

    goto/32 :goto_a

    nop

    :goto_7
    const-string v3, "GroupUtilss"

    goto/32 :goto_17

    nop

    :goto_8
    invoke-direct {p0}, Lcom/android/settings/bluetooth/GroupUtils;->isValid()Z

    move-result v0

    goto/32 :goto_6

    nop

    :goto_9
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_19

    nop

    :goto_a
    if-eqz v0, :cond_2

    goto/32 :goto_15

    :cond_2
    goto/32 :goto_14

    nop

    :goto_b
    invoke-virtual {v0, p1}, Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;->isGroupDiscoveryInProgress(I)Z

    move-result v0

    goto/32 :goto_c

    nop

    :goto_c
    sget-boolean v2, Lcom/android/settings/bluetooth/GroupUtils;->D:Z

    goto/32 :goto_5

    nop

    :goto_d
    return v1

    :goto_e
    const-string/jumbo v3, "stopGroupDiscovery "

    goto/32 :goto_16

    nop

    :goto_f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_e

    nop

    :goto_10
    return p0

    :goto_11
    goto/32 :goto_d

    nop

    :goto_12
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_f

    nop

    :goto_13
    iget-object p0, p0, Lcom/android/settings/bluetooth/GroupUtils;->mGroupClientProfile:Lcom/android/settingslib/bluetooth/DeviceGroupClientProfile;

    goto/32 :goto_1

    nop

    :goto_14
    return v1

    :goto_15
    goto/32 :goto_0

    nop

    :goto_16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_9

    nop

    :goto_17
    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_18
    goto/32 :goto_4

    nop

    :goto_19
    const-string v3, "isDiscovering "

    goto/32 :goto_2

    nop

    :goto_1a
    const/4 p0, 0x1

    goto/32 :goto_10

    nop

    :goto_1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_7

    nop
.end method
