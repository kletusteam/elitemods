.class public final Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;
.super Lcom/android/settings/widget/SummaryUpdater;

# interfaces
.implements Lcom/android/settingslib/bluetooth/BluetoothCallback;


# instance fields
.field private final mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;


# virtual methods
.method getConnectedDeviceSummary()Ljava/lang/String;
    .locals 9

    goto/32 :goto_43

    nop

    :goto_0
    new-array v1, v7, [Ljava/lang/Object;

    goto/32 :goto_28

    nop

    :goto_1
    invoke-virtual {v6}, Landroid/bluetooth/BluetoothDevice;->isConnected()Z

    move-result v8

    goto/32 :goto_31

    nop

    :goto_2
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_19

    nop

    :goto_3
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_10

    nop

    :goto_4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_48

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    goto/32 :goto_3a

    nop

    :goto_6
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    goto/32 :goto_36

    nop

    :goto_7
    const-string v0, "getConnectedDeviceSummary, no bonded devices"

    goto/32 :goto_35

    nop

    :goto_8
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_16

    nop

    :goto_9
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_15

    nop

    :goto_a
    return-object p0

    :goto_b
    goto/32 :goto_6

    nop

    :goto_c
    if-nez v6, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_1a

    nop

    :goto_d
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_2d

    nop

    :goto_e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_53

    nop

    :goto_f
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_4b

    nop

    :goto_10
    goto/16 :goto_3d

    :goto_11
    goto/32 :goto_2f

    nop

    :goto_12
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v3

    goto/32 :goto_4

    nop

    :goto_13
    if-gt v5, v7, :cond_1

    goto/32 :goto_20

    :cond_1
    :goto_14
    goto/32 :goto_34

    nop

    :goto_15
    iget-object p0, p0, Lcom/android/settings/widget/SummaryUpdater;->mContext:Landroid/content/Context;

    goto/32 :goto_1e

    nop

    :goto_16
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_33

    nop

    :goto_17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_12

    nop

    :goto_18
    const-string v0, "getConnectedDeviceSummary, bonded devices are null"

    goto/32 :goto_9

    nop

    :goto_19
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_4f

    nop

    :goto_1a
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    goto/32 :goto_3b

    nop

    :goto_1b
    sget v0, Lcom/android/settings/R$string;->disconnected:I

    goto/32 :goto_f

    nop

    :goto_1c
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1d
    goto/32 :goto_5

    nop

    :goto_1e
    sget v0, Lcom/android/settings/R$string;->bluetooth_disabled:I

    goto/32 :goto_25

    nop

    :goto_1f
    move v5, v3

    :goto_20
    goto/32 :goto_2c

    nop

    :goto_21
    goto :goto_1d

    :goto_22
    goto/32 :goto_2b

    nop

    :goto_23
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_26

    nop

    :goto_24
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_32

    nop

    :goto_25
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_a

    nop

    :goto_26
    return-object p0

    :goto_27
    goto/32 :goto_38

    nop

    :goto_28
    aput-object v4, v1, v3

    goto/32 :goto_3c

    nop

    :goto_29
    const/4 v7, 0x1

    goto/32 :goto_c

    nop

    :goto_2a
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_13

    nop

    :goto_2b
    iget-object p0, p0, Lcom/android/settings/widget/SummaryUpdater;->mContext:Landroid/content/Context;

    goto/32 :goto_52

    nop

    :goto_2c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    goto/32 :goto_29

    nop

    :goto_2d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_55

    nop

    :goto_2e
    const/4 v4, 0x0

    goto/32 :goto_1f

    nop

    :goto_2f
    iget-object p0, p0, Lcom/android/settings/widget/SummaryUpdater;->mContext:Landroid/content/Context;

    goto/32 :goto_51

    nop

    :goto_30
    iget-object p0, p0, Lcom/android/settings/widget/SummaryUpdater;->mContext:Landroid/content/Context;

    goto/32 :goto_46

    nop

    :goto_31
    if-nez v8, :cond_2

    goto/32 :goto_20

    :cond_2
    goto/32 :goto_49

    nop

    :goto_32
    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    goto/32 :goto_d

    nop

    :goto_33
    const-string v3, "getConnectedDeviceSummary, deviceName is null, numBondedDevices="

    goto/32 :goto_17

    nop

    :goto_34
    if-eqz v4, :cond_3

    goto/32 :goto_27

    :cond_3
    goto/32 :goto_8

    nop

    :goto_35
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_54

    nop

    :goto_36
    if-nez v2, :cond_4

    goto/32 :goto_4c

    :cond_4
    goto/32 :goto_7

    nop

    :goto_37
    const/4 v3, 0x0

    goto/32 :goto_2e

    nop

    :goto_38
    if-gt v5, v7, :cond_5

    goto/32 :goto_11

    :cond_5
    goto/32 :goto_30

    nop

    :goto_39
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_21

    nop

    :goto_3a
    if-nez v2, :cond_6

    goto/32 :goto_22

    :cond_6
    goto/32 :goto_24

    nop

    :goto_3b
    check-cast v6, Landroid/bluetooth/BluetoothDevice;

    goto/32 :goto_1

    nop

    :goto_3c
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    :goto_3d
    goto/32 :goto_56

    nop

    :goto_3e
    const-string v1, "BluetoothSummaryUpdater"

    goto/32 :goto_47

    nop

    :goto_3f
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    goto/32 :goto_42

    nop

    :goto_40
    const-string v4, "["

    goto/32 :goto_2

    nop

    :goto_41
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_4e

    nop

    :goto_42
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_39

    nop

    :goto_43
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    goto/32 :goto_4d

    nop

    :goto_44
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_1c

    nop

    :goto_45
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    goto/32 :goto_37

    nop

    :goto_46
    sget v0, Lcom/android/settings/R$string;->bluetooth_connected_multiple_devices_summary:I

    goto/32 :goto_3

    nop

    :goto_47
    if-eqz v0, :cond_7

    goto/32 :goto_b

    :cond_7
    goto/32 :goto_18

    nop

    :goto_48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_44

    nop

    :goto_49
    invoke-virtual {v6}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_2a

    nop

    :goto_4a
    const-string v4, "], isConnected="

    goto/32 :goto_e

    nop

    :goto_4b
    return-object p0

    :goto_4c
    goto/32 :goto_45

    nop

    :goto_4d
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v0

    goto/32 :goto_3e

    nop

    :goto_4e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_40

    nop

    :goto_4f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_4a

    nop

    :goto_50
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_41

    nop

    :goto_51
    sget v0, Lcom/android/settings/R$string;->bluetooth_connected_summary:I

    goto/32 :goto_0

    nop

    :goto_52
    sget v0, Lcom/android/settings/R$string;->disconnected:I

    goto/32 :goto_23

    nop

    :goto_53
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->isConnected()Z

    move-result v2

    goto/32 :goto_3f

    nop

    :goto_54
    iget-object p0, p0, Lcom/android/settings/widget/SummaryUpdater;->mContext:Landroid/content/Context;

    goto/32 :goto_1b

    nop

    :goto_55
    const-string v4, "getConnectedDeviceSummary, device="

    goto/32 :goto_50

    nop

    :goto_56
    return-object p0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getConnectionState()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    iget-object p0, p0, Lcom/android/settings/widget/SummaryUpdater;->mContext:Landroid/content/Context;

    sget v0, Lcom/android/settings/R$string;->disconnected:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    iget-object p0, p0, Lcom/android/settings/widget/SummaryUpdater;->mContext:Landroid/content/Context;

    sget v0, Lcom/android/settings/R$string;->bluetooth_disconnecting:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/bluetooth/BluetoothSummaryUpdater;->getConnectedDeviceSummary()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_3
    iget-object p0, p0, Lcom/android/settings/widget/SummaryUpdater;->mContext:Landroid/content/Context;

    sget v0, Lcom/android/settings/R$string;->bluetooth_connecting:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_4
    :goto_0
    iget-object p0, p0, Lcom/android/settings/widget/SummaryUpdater;->mContext:Landroid/content/Context;

    sget v0, Lcom/android/settings/R$string;->bluetooth_disabled:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public onBluetoothStateChanged(I)V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/widget/SummaryUpdater;->notifyChangeIfNeeded()V

    return-void
.end method

.method public onConnectionStateChanged(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;I)V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/widget/SummaryUpdater;->notifyChangeIfNeeded()V

    return-void
.end method
