.class public Lcom/android/settings/bluetooth/GroupForgetDialogFragment;
.super Lcom/android/settings/core/instrumentation/InstrumentedDialogFragment;


# static fields
.field private static final DBG:Z


# instance fields
.field private mGroupId:I

.field private mGroupUtils:Lcom/android/settings/bluetooth/GroupUtils;


# direct methods
.method public static synthetic $r8$lambda$NWB9DZUf6Q_A6bcfKE0Vg4xH7DU(Lcom/android/settings/bluetooth/GroupForgetDialogFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/bluetooth/GroupForgetDialogFragment;->lambda$onCreateDialog$0(Landroid/content/DialogInterface;I)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/android/settings/connecteddevice/ConnectedDeviceDashboardFragment;->DBG_GROUP:Z

    sput-boolean v0, Lcom/android/settings/bluetooth/GroupForgetDialogFragment;->DBG:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/core/instrumentation/InstrumentedDialogFragment;-><init>()V

    return-void
.end method

.method private forget()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bluetooth/GroupForgetDialogFragment;->mGroupUtils:Lcom/android/settings/bluetooth/GroupUtils;

    iget p0, p0, Lcom/android/settings/bluetooth/GroupForgetDialogFragment;->mGroupId:I

    invoke-virtual {v0, p0}, Lcom/android/settings/bluetooth/GroupUtils;->forgetGroup(I)Z

    return-void
.end method

.method private synthetic lambda$onCreateDialog$0(Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/bluetooth/GroupForgetDialogFragment;->forget()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method

.method public static newInstance(I)Lcom/android/settings/bluetooth/GroupForgetDialogFragment;
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    const-string v1, "groupid"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance p0, Lcom/android/settings/bluetooth/GroupForgetDialogFragment;

    invoke-direct {p0}, Lcom/android/settings/bluetooth/GroupForgetDialogFragment;-><init>()V

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object p0
.end method


# virtual methods
.method getGroupTitle()Ljava/lang/String;
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    goto/32 :goto_3

    nop

    :goto_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_2
    const-string v1, "groupid"

    goto/32 :goto_0

    nop

    :goto_3
    iput v0, p0, Lcom/android/settings/bluetooth/GroupForgetDialogFragment;->mGroupId:I

    goto/32 :goto_5

    nop

    :goto_4
    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/GroupUtils;->getGroupTitle(I)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_6

    nop

    :goto_5
    iget-object p0, p0, Lcom/android/settings/bluetooth/GroupForgetDialogFragment;->mGroupUtils:Lcom/android/settings/bluetooth/GroupUtils;

    goto/32 :goto_4

    nop

    :goto_6
    return-object p0
.end method

.method public getMetricsCategory()I
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    new-instance p1, Lcom/android/settings/bluetooth/GroupForgetDialogFragment$$ExternalSyntheticLambda0;

    invoke-direct {p1, p0}, Lcom/android/settings/bluetooth/GroupForgetDialogFragment$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/bluetooth/GroupForgetDialogFragment;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/android/settings/bluetooth/GroupUtils;

    invoke-direct {v1, v0}, Lcom/android/settings/bluetooth/GroupUtils;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/settings/bluetooth/GroupForgetDialogFragment;->mGroupUtils:Lcom/android/settings/bluetooth/GroupUtils;

    new-instance v1, Landroidx/appcompat/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/android/settings/R$string;->groupaudio_unpair_dialog_forget_confirm_button:I

    invoke-virtual {v1, v2, p1}, Landroidx/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    const/high16 v1, 0x1040000

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroidx/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/appcompat/app/AlertDialog$Builder;->create()Landroidx/appcompat/app/AlertDialog;

    move-result-object p1

    sget v1, Lcom/android/settings/R$string;->groupaudio_unpair_dialog_title:I

    invoke-virtual {p1, v1}, Landroidx/appcompat/app/AppCompatDialog;->setTitle(I)V

    sget v1, Lcom/android/settings/R$string;->groupaudio_unpair_dialog_body:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/android/settings/bluetooth/GroupForgetDialogFragment;->getGroupTitle()Ljava/lang/String;

    move-result-object p0

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroidx/appcompat/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    return-object p1
.end method
