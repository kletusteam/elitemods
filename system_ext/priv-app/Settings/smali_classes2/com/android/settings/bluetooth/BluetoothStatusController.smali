.class public Lcom/android/settings/bluetooth/BluetoothStatusController;
.super Lcom/android/settings/BaseSettingsController;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mHasRegister:Z

.field private final mIntentFilter:Landroid/content/IntentFilter;

.field private final mLocalAdapter:Lcom/android/settingslib/bluetooth/LocalBluetoothAdapter;

.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/TextView;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/BaseSettingsController;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    const-string p1, "BluetoothStatusController"

    iput-object p1, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->TAG:Ljava/lang/String;

    new-instance p1, Lcom/android/settings/bluetooth/BluetoothStatusController$1;

    invoke-direct {p1, p0}, Lcom/android/settings/bluetooth/BluetoothStatusController$1;-><init>(Lcom/android/settings/bluetooth/BluetoothStatusController;)V

    iput-object p1, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-static {}, Lcom/android/settingslib/bluetooth/LocalBluetoothAdapter;->getInstance()Lcom/android/settingslib/bluetooth/LocalBluetoothAdapter;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->mLocalAdapter:Lcom/android/settingslib/bluetooth/LocalBluetoothAdapter;

    new-instance p1, Landroid/content/IntentFilter;

    const-string p2, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-direct {p1, p2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->mIntentFilter:Landroid/content/IntentFilter;

    return-void
.end method


# virtual methods
.method handleStateChanged(I)V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/settings/BaseSettingsController;->mStatusView:Landroid/widget/TextView;

    goto/32 :goto_d

    nop

    :goto_1
    const/4 v1, 0x0

    goto/32 :goto_11

    nop

    :goto_2
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(I)V

    goto/32 :goto_8

    nop

    :goto_3
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(I)V

    :goto_4
    goto/32 :goto_7

    nop

    :goto_5
    const/16 v0, 0xb

    goto/32 :goto_13

    nop

    :goto_6
    const/16 v0, 0xc

    goto/32 :goto_b

    nop

    :goto_7
    return-void

    :goto_8
    goto :goto_4

    :goto_9
    goto/32 :goto_a

    nop

    :goto_a
    iget-object p0, p0, Lcom/android/settings/BaseSettingsController;->mStatusView:Landroid/widget/TextView;

    goto/32 :goto_10

    nop

    :goto_b
    if-ne p1, v0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_c

    nop

    :goto_c
    iget-object p0, p0, Lcom/android/settings/BaseSettingsController;->mStatusView:Landroid/widget/TextView;

    goto/32 :goto_12

    nop

    :goto_d
    if-eqz v0, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_e

    nop

    :goto_e
    return-void

    :goto_f
    goto/32 :goto_1

    nop

    :goto_10
    sget p1, Lcom/android/settings/R$string;->wireless_on:I

    goto/32 :goto_3

    nop

    :goto_11
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/32 :goto_5

    nop

    :goto_12
    sget p1, Lcom/android/settings/R$string;->wireless_off:I

    goto/32 :goto_2

    nop

    :goto_13
    if-ne p1, v0, :cond_2

    goto/32 :goto_9

    :cond_2
    goto/32 :goto_6

    nop
.end method

.method public pause()V
    .locals 0

    return-void
.end method

.method public resume()V
    .locals 0

    return-void
.end method

.method public start()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/BaseSettingsController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->mHasRegister:Z

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->mLocalAdapter:Lcom/android/settingslib/bluetooth/LocalBluetoothAdapter;

    const-string v1, "BluetoothStatusController"

    if-nez v0, :cond_0

    const-string/jumbo v0, "start: mLocalAdapter is null"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/BluetoothStatusController;->handleStateChanged(I)V

    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/LocalBluetoothAdapter;->getBluetoothState()I

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BluetoothState: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/BluetoothStatusController;->handleStateChanged(I)V

    return-void
.end method

.method public stop()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->mHasRegister:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/BaseSettingsController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->mHasRegister:Z

    :cond_0
    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->mLocalAdapter:Lcom/android/settingslib/bluetooth/LocalBluetoothAdapter;

    if-nez v0, :cond_1

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/BluetoothStatusController;->handleStateChanged(I)V

    :cond_1
    return-void
.end method

.method protected updateStatus()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/bluetooth/BluetoothStatusController;->mLocalAdapter:Lcom/android/settingslib/bluetooth/LocalBluetoothAdapter;

    if-nez v0, :cond_0

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/BluetoothStatusController;->handleStateChanged(I)V

    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/LocalBluetoothAdapter;->getBluetoothState()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/BluetoothStatusController;->handleStateChanged(I)V

    return-void
.end method
