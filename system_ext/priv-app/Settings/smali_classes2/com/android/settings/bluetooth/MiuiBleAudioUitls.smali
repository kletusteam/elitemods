.class public final Lcom/android/settings/bluetooth/MiuiBleAudioUitls;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/bluetooth/MiuiBleAudioUitls$PasswordDialogEventCallback;,
        Lcom/android/settings/bluetooth/MiuiBleAudioUitls$HintDialogEventCallback;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createHintDialog(Landroid/app/Activity;BIILcom/android/settings/bluetooth/MiuiBleAudioUitls$HintDialogEventCallback;)Lmiuix/appcompat/app/AlertDialog;
    .locals 3

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    packed-switch p2, :pswitch_data_0

    sget p3, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_default_fail:I

    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_default_fail_unknwon:I

    goto :goto_1

    :pswitch_0
    sget p3, Lcom/android/settings/R$string;->bluetooth_ble_audio_stop_synchronize_title:I

    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_stop_synchronize_message:I

    goto :goto_1

    :pswitch_1
    sget p3, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_remove_confirm:I

    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_remove_confirm_content:I

    goto :goto_1

    :pswitch_2
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_remove_fail:I

    invoke-static {p3}, Lcom/android/settings/bluetooth/MiuiBleAudioUitls;->getCallbackStatusString(I)I

    move-result p3

    goto :goto_0

    :pswitch_3
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_update_pin_fail:I

    invoke-static {p3}, Lcom/android/settings/bluetooth/MiuiBleAudioUitls;->getCallbackStatusString(I)I

    move-result p3

    goto :goto_0

    :pswitch_4
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_update_fail:I

    invoke-static {p3}, Lcom/android/settings/bluetooth/MiuiBleAudioUitls;->getCallbackStatusString(I)I

    move-result p3

    goto :goto_0

    :pswitch_5
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_add_fail:I

    invoke-static {p3}, Lcom/android/settings/bluetooth/MiuiBleAudioUitls;->getCallbackStatusString(I)I

    move-result p3

    goto :goto_0

    :pswitch_6
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_select_fail:I

    invoke-static {p3}, Lcom/android/settings/bluetooth/MiuiBleAudioUitls;->getCallbackStatusString(I)I

    move-result p3

    :goto_0
    move v2, v0

    move v0, p3

    move p3, v2

    :goto_1
    new-instance v1, Lmiuix/appcompat/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p3}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setCancelable(Z)Lmiuix/appcompat/app/AlertDialog$Builder;

    invoke-virtual {v1, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    const p0, 0x104000a

    new-instance p3, Lcom/android/settings/bluetooth/MiuiBleAudioUitls$1;

    invoke-direct {p3, p4, p1, p2}, Lcom/android/settings/bluetooth/MiuiBleAudioUitls$1;-><init>(Lcom/android/settings/bluetooth/MiuiBleAudioUitls$HintDialogEventCallback;BI)V

    invoke-virtual {v1, p0, p3}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    const/high16 p0, 0x1040000

    new-instance p3, Lcom/android/settings/bluetooth/MiuiBleAudioUitls$2;

    invoke-direct {p3, p4, p1, p2}, Lcom/android/settings/bluetooth/MiuiBleAudioUitls$2;-><init>(Lcom/android/settings/bluetooth/MiuiBleAudioUitls$HintDialogEventCallback;BI)V

    invoke-virtual {v1, p0, p3}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    invoke-virtual {v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object p0

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static createPasswordDialog(Landroid/app/Activity;Landroid/bluetooth/BluetoothDevice;BLcom/android/settings/bluetooth/MiuiBleAudioUitls$PasswordDialogEventCallback;)Lmiuix/appcompat/app/AlertDialog;
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    new-instance v1, Lmiuix/appcompat/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object p0

    sget v2, Lcom/android/settings/R$layout;->ble_audio_password_dialog:I

    invoke-virtual {p0, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p0

    sget v0, Lcom/android/settings/R$id;->ble_audio_password:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFocusable(Z)V

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setSelection(I)V

    if-nez p1, :cond_1

    sget p1, Lcom/android/settings/R$string;->bluetooth_share_broadcast_update:I

    invoke-virtual {v1, p1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lcom/android/settings/bluetooth/MiuiBleAudioUitls;->getTitleFromBluetoothDevice(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    :goto_0
    invoke-virtual {v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setCancelable(Z)Lmiuix/appcompat/app/AlertDialog$Builder;

    invoke-virtual {v1, p0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setView(Landroid/view/View;)Lmiuix/appcompat/app/AlertDialog$Builder;

    sget p0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_connect:I

    new-instance p1, Lcom/android/settings/bluetooth/MiuiBleAudioUitls$3;

    invoke-direct {p1, v0, p3, p2}, Lcom/android/settings/bluetooth/MiuiBleAudioUitls$3;-><init>(Landroid/widget/EditText;Lcom/android/settings/bluetooth/MiuiBleAudioUitls$PasswordDialogEventCallback;B)V

    invoke-virtual {v1, p0, p1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    const/high16 p0, 0x1040000

    new-instance p1, Lcom/android/settings/bluetooth/MiuiBleAudioUitls$4;

    invoke-direct {p1, p3, p2}, Lcom/android/settings/bluetooth/MiuiBleAudioUitls$4;-><init>(Lcom/android/settings/bluetooth/MiuiBleAudioUitls$PasswordDialogEventCallback;B)V

    invoke-virtual {v1, p0, p1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    invoke-virtual {v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object p0

    return-object p0
.end method

.method public static getAudioSyncStatusString(I)I
    .locals 2

    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_audio_sync_unknown:I

    if-eqz p0, :cond_2

    const/4 v1, 0x1

    if-eq p0, v1, :cond_1

    const v1, 0xffff

    if-eq p0, v1, :cond_0

    goto :goto_0

    :cond_0
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_audio_sync_invalid:I

    goto :goto_0

    :cond_1
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_audio_sync_in_sync:I

    goto :goto_0

    :cond_2
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_audio_sync_not_sync:I

    :goto_0
    return v0
.end method

.method public static getCallbackStatusString(I)I
    .locals 2

    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_callback_fail:I

    const/16 v1, 0x10

    if-eq p0, v1, :cond_0

    packed-switch p0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_callback_no_empty_solt:I

    goto :goto_0

    :pswitch_1
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_callback_duplicate_addition:I

    goto :goto_0

    :pswitch_2
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_callback_source_unavailable:I

    goto :goto_0

    :pswitch_3
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_callback_invalid_source_select:I

    goto :goto_0

    :pswitch_4
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_callback_colocated_src_unavailable:I

    goto :goto_0

    :pswitch_5
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_callback_invalid_source_id:I

    goto :goto_0

    :pswitch_6
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_callback_txn_timeout:I

    goto :goto_0

    :pswitch_7
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_callback_fatal:I

    goto :goto_0

    :pswitch_8
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_callback_success:I

    goto :goto_0

    :cond_0
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_callback_invalid_group_op:I

    :goto_0
    :pswitch_9
    return v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_9
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getEncryptionStatusString(I)I
    .locals 2

    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_broadcast_code_unkonwn:I

    if-eqz p0, :cond_4

    const/4 v1, 0x1

    if-eq p0, v1, :cond_3

    const/4 v1, 0x2

    if-eq p0, v1, :cond_2

    const/4 v1, 0x3

    if-eq p0, v1, :cond_1

    const v1, 0xffff

    if-eq p0, v1, :cond_0

    goto :goto_0

    :cond_0
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_broadcast_code_invalid:I

    goto :goto_0

    :cond_1
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_broadcast_code_incorrect:I

    goto :goto_0

    :cond_2
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_broadcast_code_correct:I

    goto :goto_0

    :cond_3
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_broadcast_code_need_add:I

    goto :goto_0

    :cond_4
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_broadcast_code_not_encrypted:I

    :goto_0
    return v0
.end method

.method public static getMetadataSyncStatusString(I)I
    .locals 2

    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_pa_sync_unknown:I

    if-eqz p0, :cond_5

    const/4 v1, 0x1

    if-eq p0, v1, :cond_4

    const/4 v1, 0x2

    if-eq p0, v1, :cond_3

    const/4 v1, 0x3

    if-eq p0, v1, :cond_2

    const/4 v1, 0x4

    if-eq p0, v1, :cond_1

    const v1, 0xffff

    if-eq p0, v1, :cond_0

    goto :goto_0

    :cond_0
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_pa_sync_invad:I

    goto :goto_0

    :cond_1
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_pa_sync_no_past:I

    goto :goto_0

    :cond_2
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_pa_sync_fail:I

    goto :goto_0

    :cond_3
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_pa_sync_in_sync:I

    goto :goto_0

    :cond_4
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_pa_sync_syncinfo_need:I

    goto :goto_0

    :cond_5
    sget v0, Lcom/android/settings/R$string;->bluetooth_ble_audio_source_pa_sync_idel:I

    :goto_0
    return v0
.end method

.method public static getSyncState(Landroid/bluetooth/BleBroadcastSourceInfo;)I
    .locals 4

    const/4 v0, -0x1

    if-nez p0, :cond_0

    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/bluetooth/BleBroadcastSourceInfo;->getAudioSyncState()I

    move-result v1

    invoke-virtual {p0}, Landroid/bluetooth/BleBroadcastSourceInfo;->getMetadataSyncState()I

    move-result p0

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    if-ne p0, v2, :cond_1

    return v2

    :cond_1
    if-ne v1, v3, :cond_2

    if-eq p0, v2, :cond_2

    return v3

    :cond_2
    if-eq v1, v3, :cond_3

    if-ne p0, v2, :cond_3

    const/4 p0, 0x0

    return p0

    :cond_3
    return v0
.end method

.method public static getTitleFromBluetoothDevice(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;
    .locals 2

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-nez p0, :cond_0

    const-string p0, "NULL DEVICE"

    return-object p0

    :cond_0
    invoke-static {p0}, Lcom/android/settings/bluetooth/MiuiBleAudioUitls;->isLocalDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getName()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_1

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_1
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_0

    :cond_3
    move-object p0, v0

    goto :goto_1

    :cond_4
    :goto_0
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object p0

    :cond_5
    :goto_1
    return-object p0
.end method

.method public static isLocalDevice(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 2

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p0, :cond_1

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 p0, 0x1

    return p0

    :cond_1
    :goto_0
    return v1
.end method

.method public static isSynchronized(Landroid/bluetooth/BleBroadcastSourceInfo;)Z
    .locals 7

    const-string v0, "MiuiBleAudioSourceInfoFragment"

    const/4 v1, 0x0

    if-nez p0, :cond_0

    const-string p0, "isSynchronized srcInfo is null return !"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_0
    invoke-virtual {p0}, Landroid/bluetooth/BleBroadcastSourceInfo;->getMetadataSyncState()I

    move-result v2

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-ne v2, v3, :cond_1

    const-string v2, "Metadata Synced"

    move v3, v4

    goto :goto_0

    :cond_1
    const-string v2, "Metadata not synced"

    move v3, v1

    :goto_0
    invoke-virtual {p0}, Landroid/bluetooth/BleBroadcastSourceInfo;->getAudioSyncState()I

    move-result p0

    if-ne p0, v4, :cond_2

    const-string p0, "Audio Synced"

    move v5, v4

    goto :goto_1

    :cond_2
    const-string p0, "Audio not synced"

    move v5, v1

    :goto_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", "

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v3, :cond_3

    if-eqz v5, :cond_3

    move v1, v4

    :cond_3
    return v1
.end method
