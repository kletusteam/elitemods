.class public Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsFragment;
.super Lcom/android/settings/dashboard/RestrictedDashboardFragment;


# instance fields
.field mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

.field mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

.field mDeviceAddress:Ljava/lang/String;

.field mManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

.field mSourceInfoIndex:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string/jumbo v0, "no_config_bluetooth"

    invoke-direct {p0, v0}, Lcom/android/settings/dashboard/RestrictedDashboardFragment;-><init>(Ljava/lang/String;)V

    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsFragment;->mSourceInfoIndex:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method protected createPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/settingslib/core/AbstractPreferenceController;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsFragment;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsFragment;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settingslib/core/lifecycle/ObservablePreferenceFragment;->getSettingsLifecycle()Lcom/android/settingslib/core/lifecycle/Lifecycle;

    move-result-object v8

    new-instance v1, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;

    iget-object v5, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsFragment;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    iget-object v6, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsFragment;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    iget-object v2, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsFragment;->mSourceInfoIndex:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    move-object v2, v1

    move-object v3, p1

    move-object v4, p0

    invoke-direct/range {v2 .. v8}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsController;-><init>(Landroid/content/Context;Landroidx/preference/PreferenceFragmentCompat;Landroid/bluetooth/BleBroadcastSourceInfo;Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;ILcom/android/settingslib/core/lifecycle/Lifecycle;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v0
.end method

.method getCachedDevice(Ljava/lang/String;)Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;
    .locals 1

    goto/32 :goto_5

    nop

    :goto_0
    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/LocalBluetoothManager;->getBluetoothAdapter()Lcom/android/settingslib/bluetooth/LocalBluetoothAdapter;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_1
    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/LocalBluetoothManager;->getCachedDeviceManager()Lcom/android/settingslib/bluetooth/CachedBluetoothDeviceManager;

    move-result-object p0

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {p0, p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDeviceManager;->findDevice(Landroid/bluetooth/BluetoothDevice;)Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    move-result-object p0

    goto/32 :goto_4

    nop

    :goto_3
    iget-object p0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsFragment;->mManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    goto/32 :goto_1

    nop

    :goto_4
    return-object p0

    :goto_5
    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsFragment;->mManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    goto/32 :goto_0

    nop

    :goto_6
    invoke-virtual {v0, p1}, Lcom/android/settingslib/bluetooth/LocalBluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object p1

    goto/32 :goto_3

    nop
.end method

.method protected getLogTag()Ljava/lang/String;
    .locals 0

    const-string p0, "SourceInfoDetailsFrg"

    return-object p0
.end method

.method public getMetricsCategory()I
    .locals 0

    const/16 p0, 0x3f1

    return p0
.end method

.method protected getPreferenceScreenResId()I
    .locals 0

    sget p0, Lcom/android/settings/R$xml;->bcast_source_info_details_fragment:I

    return p0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "device_address"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsFragment;->mDeviceAddress:Ljava/lang/String;

    invoke-static {p1}, Lcom/android/settings/bluetooth/Utils;->getLocalBtManager(Landroid/content/Context;)Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsFragment;->mManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "broadcast_source_info"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BleBroadcastSourceInfo;

    iput-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsFragment;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    iget-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsFragment;->mDeviceAddress:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsFragment;->getCachedDevice(Ljava/lang/String;)Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsFragment;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "broadcast_source_index"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsFragment;->mSourceInfoIndex:Ljava/lang/Integer;

    invoke-super {p0, p1}, Lcom/android/settings/dashboard/DashboardFragment;->onAttach(Landroid/content/Context;)V

    iget-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsFragment;->mCachedDevice:Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    const-string v0, "SourceInfoDetailsFrg"

    if-nez p1, :cond_0

    const-string/jumbo p1, "onAttach() CachedDevice is null!"

    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->finish()V

    return-void

    :cond_0
    iget-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsFragment;->mBleBroadcastSourceInfo:Landroid/bluetooth/BleBroadcastSourceInfo;

    if-nez p1, :cond_1

    const-string/jumbo p1, "onAttach()  mBleBroadcastSourceInfo null!"

    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->finish()V

    return-void

    :cond_1
    iget-object p1, p0, Lcom/android/settings/bluetooth/BleBroadcastSourceInfoDetailsFragment;->mSourceInfoIndex:Ljava/lang/Integer;

    if-nez p1, :cond_2

    const-string/jumbo p1, "onAttach()  mSourceInfoIndex null!"

    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->finish()V

    :cond_2
    return-void
.end method
