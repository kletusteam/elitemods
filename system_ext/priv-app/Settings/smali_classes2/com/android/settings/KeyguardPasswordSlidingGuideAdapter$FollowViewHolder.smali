.class public Lcom/android/settings/KeyguardPasswordSlidingGuideAdapter$FollowViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/KeyguardPasswordSlidingGuideAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FollowViewHolder"
.end annotation


# instance fields
.field private context:Landroid/widget/TextView;

.field private mLottieView:Lcom/airbnb/lottie/LottieAnimationView;

.field final synthetic this$0:Lcom/android/settings/KeyguardPasswordSlidingGuideAdapter;

.field private title:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/android/settings/KeyguardPasswordSlidingGuideAdapter;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/KeyguardPasswordSlidingGuideAdapter$FollowViewHolder;->this$0:Lcom/android/settings/KeyguardPasswordSlidingGuideAdapter;

    invoke-direct {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    sget p1, Lcom/android/settings/R$id;->guide_title:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/android/settings/KeyguardPasswordSlidingGuideAdapter$FollowViewHolder;->title:Landroid/widget/TextView;

    sget p1, Lcom/android/settings/R$id;->guide_text:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/android/settings/KeyguardPasswordSlidingGuideAdapter$FollowViewHolder;->context:Landroid/widget/TextView;

    sget p1, Lcom/android/settings/R$id;->lottie_view:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/airbnb/lottie/LottieAnimationView;

    iput-object p1, p0, Lcom/android/settings/KeyguardPasswordSlidingGuideAdapter$FollowViewHolder;->mLottieView:Lcom/airbnb/lottie/LottieAnimationView;

    return-void
.end method


# virtual methods
.method bind(Lcom/android/settings/KeyguardPasswordSlidingGuideBean;)V
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    invoke-virtual {v0, p1}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(Ljava/lang/String;)V

    goto/32 :goto_1

    nop

    :goto_1
    iget-object p0, p0, Lcom/android/settings/KeyguardPasswordSlidingGuideAdapter$FollowViewHolder;->mLottieView:Lcom/airbnb/lottie/LottieAnimationView;

    goto/32 :goto_9

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/settings/KeyguardPasswordSlidingGuideAdapter$FollowViewHolder;->context:Landroid/widget/TextView;

    goto/32 :goto_7

    nop

    :goto_3
    invoke-virtual {p1}, Lcom/android/settings/KeyguardPasswordSlidingGuideBean;->getTitle()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_4

    nop

    :goto_4
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_2

    nop

    :goto_5
    iget-object v0, p0, Lcom/android/settings/KeyguardPasswordSlidingGuideAdapter$FollowViewHolder;->title:Landroid/widget/TextView;

    goto/32 :goto_3

    nop

    :goto_6
    invoke-virtual {p1}, Lcom/android/settings/KeyguardPasswordSlidingGuideBean;->getAnimationName()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_0

    nop

    :goto_7
    invoke-virtual {p1}, Lcom/android/settings/KeyguardPasswordSlidingGuideBean;->getContext()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_a

    nop

    :goto_8
    iget-object v0, p0, Lcom/android/settings/KeyguardPasswordSlidingGuideAdapter$FollowViewHolder;->mLottieView:Lcom/airbnb/lottie/LottieAnimationView;

    goto/32 :goto_6

    nop

    :goto_9
    invoke-virtual {p0}, Lcom/airbnb/lottie/LottieAnimationView;->playAnimation()V

    goto/32 :goto_b

    nop

    :goto_a
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_8

    nop

    :goto_b
    return-void
.end method
