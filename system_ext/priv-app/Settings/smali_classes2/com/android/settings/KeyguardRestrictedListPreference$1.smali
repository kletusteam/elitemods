.class Lcom/android/settings/KeyguardRestrictedListPreference$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/KeyguardRestrictedListPreference;->onPrepareDialogBuilder(Lmiuix/appcompat/app/AlertDialog$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/KeyguardRestrictedListPreference;

.field final synthetic val$mListAdapter:Landroid/widget/ListAdapter;


# direct methods
.method constructor <init>(Lcom/android/settings/KeyguardRestrictedListPreference;Landroid/widget/ListAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/KeyguardRestrictedListPreference$1;->this$0:Lcom/android/settings/KeyguardRestrictedListPreference;

    iput-object p2, p0, Lcom/android/settings/KeyguardRestrictedListPreference$1;->val$mListAdapter:Landroid/widget/ListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3

    const/4 p1, 0x0

    move v0, p1

    :goto_0
    iget-object v1, p0, Lcom/android/settings/KeyguardRestrictedListPreference$1;->val$mListAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/KeyguardRestrictedListPreference$1;->val$mListAdapter:Landroid/widget/ListAdapter;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2, v2}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/view/View;

    aput-object v1, v2, p1

    invoke-static {v2}, Lmiuix/animation/Folme;->clean([Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
