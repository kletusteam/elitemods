.class public Lcom/android/settings/search/SearchResultGroupEntity;
.super Ljava/lang/Object;


# instance fields
.field public mAvgScore:D

.field public final mGroup:Ljava/lang/String;

.field private mGroupResultItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/search/SearchResultItem;",
            ">;"
        }
    .end annotation
.end field

.field private mSumScore:D


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/search/SearchResultGroupEntity;->mGroupResultItems:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/android/settings/search/SearchResultGroupEntity;->mGroup:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public addResultItem(Lcom/android/settings/search/SearchResultItem;)Lcom/android/settings/search/SearchResultGroupEntity;
    .locals 4

    iget-wide v0, p0, Lcom/android/settings/search/SearchResultGroupEntity;->mSumScore:D

    iget-wide v2, p1, Lcom/android/settings/search/SearchResultItem;->score:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Lcom/android/settings/search/SearchResultGroupEntity;->mSumScore:D

    iget-object v0, p0, Lcom/android/settings/search/SearchResultGroupEntity;->mGroupResultItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-wide v0, p0, Lcom/android/settings/search/SearchResultGroupEntity;->mSumScore:D

    iget-object p1, p0, Lcom/android/settings/search/SearchResultGroupEntity;->mGroupResultItems:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    int-to-double v2, p1

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/android/settings/search/SearchResultGroupEntity;->mAvgScore:D

    return-object p0
.end method

.method public getGroupResultItems()Ljava/util/ArrayList;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/search/SearchResultItem;",
            ">;"
        }
    .end annotation

    iget-object p0, p0, Lcom/android/settings/search/SearchResultGroupEntity;->mGroupResultItems:Ljava/util/ArrayList;

    return-object p0
.end method
