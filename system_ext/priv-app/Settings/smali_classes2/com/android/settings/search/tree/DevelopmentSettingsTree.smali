.class public Lcom/android/settings/search/tree/DevelopmentSettingsTree;
.super Lcom/android/settingslib/search/SettingsTree;


# static fields
.field private static final A2DP_OFFLOAD_SUPPORTED_PROPERTY:Ljava/lang/String; = "ro.bluetooth.a2dp_offload.supported"

.field private static final ACTUAL_LOGPERSIST_PROPERTY_ENABLE:Ljava/lang/String; = "logd.logpersistd.enable"

.field private static final BLUETOOTH_DISABLE_A2DP_HW_OFFLOAD:Ljava/lang/String; = "bluetooth_disable_a2dp_hw_offload"

.field public static final BLUETOOTH_DISABLE_ABSOLUTE_VOLUME:Ljava/lang/String; = "bluetooth_disable_absolute_volume"

.field public static final BLUETOOTH_ENABLE_OPP_HIGH_SPEED:Ljava/lang/String; = "bluetooth_enable_opp_high_speed"

.field public static final BLUETOOTH_ENABLE_PAGE_SCAN:Ljava/lang/String; = "bluetooth_enable_page_scan"

.field public static final BLUETOOTH_ENABLE_PTS_TEST_KEY:Ljava/lang/String; = "bluetooth_enable_pts_test"

.field private static final BLUETOOTH_MAX_CONNECTED_AUDIO_DEVICES_STRING:Ljava/lang/String; = "bluetooth_max_connected_audio_devices_string"

.field private static final BLUETOOTH_SELECT_A2DP_CODEC_APTXADAPTIVE_MODE:Ljava/lang/String; = "bluetooth_select_a2dp_codec_aptxadaptive_mode"

.field private static final BOOTLOADER_STATUS:Ljava/lang/String; = "bootloader_status"

.field private static final CAMERA_LASER_SENSOR_SWITCH:Ljava/lang/String; = "camera_laser_sensor_switch"

.field private static final CLEAR_ADB_KEYS:Ljava/lang/String; = "clear_adb_keys"

.field private static final COLOR_TEMPERATURE:Ljava/lang/String; = "color_temperature"

.field public static final COM_ANDROID_TRACEUR:Ljava/lang/String; = "com.android.traceur"

.field private static final DARK_UI_MODE:Ljava/lang/String; = "dark_ui_mode"

.field public static final DEBUG_DEBUGGING_CATEGORY:Ljava/lang/String; = "debug_debugging_category"

.field private static final DEVELOPMENT_SETTINGS_TITLE:Ljava/lang/String; = "development_settings_title"

.field private static final DISPLAY_CUTOUT_EMULATION:Ljava/lang/String; = "display_cutout_emulation"

.field private static final ENABLE_ADB:Ljava/lang/String; = "enable_adb"

.field private static final ENABLE_FREEFORM_SUPPORT:Ljava/lang/String; = "enable_freeform_support"

.field private static final ENABLE_TERMINAL_TITLE:Ljava/lang/String; = "enable_terminal_title"

.field private static final FIVEG_NRCA_SETTINGS_TITLE:Ljava/lang/String; = "fiveg_nrca_switch_title"

.field private static final FIVEG_SA_VICE_SWITCH_TITLE:Ljava/lang/String; = "fiveg_sa_vice_switch_title"

.field private static final FIVEG_VONR_SETTINGS_TITLE:Ljava/lang/String; = "fiveg_vonr_switch_title"

.field private static final HDCP_CHECKING_TITLE:Ljava/lang/String; = "hdcp_checking_title"

.field private static final LOW_FLICKER_BACKLIGHT_TITLE:Ljava/lang/String; = "low_dc_light_title"

.field private static final MIUI_EXPERIENCE_OPTIMIZATION:Ljava/lang/String; = "miui_experience_optimization"

.field private static final OEM_UNLOCK_ENABLE:Ljava/lang/String; = "oem_unlock_enable"

.field private static final SECONDSPACE_DELETE_SPACE_TITLE:Ljava/lang/String; = "secondspace_delete_space_title"

.field private static final SECURITY_ENCRYPTION_TITLE:Ljava/lang/String; = "security_encryption_title"

.field private static final SELECT_LOGD_OFF_SIZE_MARKER_VALUE:Ljava/lang/String; = "32768"

.field private static final SELECT_LOGD_SIZE_PROPERTY:Ljava/lang/String; = "persist.logd.size"

.field private static final SELECT_LOGD_TAG_PROPERTY:Ljava/lang/String; = "persist.log.tag"

.field private static final SELECT_LOGD_TAG_SILENCE:Ljava/lang/String; = "Settings"

.field private static final SELECT_LOGPERSIST_TITLE:Ljava/lang/String; = "select_logpersist_title"

.field private static final SHOW_FIRST_CRASH_DIALOG:Ljava/lang/String; = "show_first_crash_dialog"

.field private static final SPEED_MODE_TITLE:Ljava/lang/String; = "speed_mode"

.field public static final SYSTEM_TRACING:Ljava/lang/String; = "system_tracing"

.field private static final UNLOCK_SET_ENTER_SYSTEM:Ljava/lang/String; = "unlock_set_enter_system"

.field private static final USB_ADB_INPUT:Ljava/lang/String; = "usb_adb_input"

.field private static final USB_INSTALL_APP:Ljava/lang/String; = "usb_install_app"

.field private static final VERIFY_APPS_OVER_USB_TITLE:Ljava/lang/String; = "verify_apps_over_usb_title"

.field private static final WAIT_FOR_DEBUGGER:Ljava/lang/String; = "wait_for_debugger"

.field private static final WIFI_CONNECTED_MAC_RANDOMIZATION:Ljava/lang/String; = "wifi_connected_mac_randomization"

.field private static final controllerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/settingslib/core/AbstractPreferenceController;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    sput-object v0, Lcom/android/settings/search/tree/DevelopmentSettingsTree;->controllerMap:Ljava/util/Map;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lorg/json/JSONObject;Lcom/android/settingslib/search/SettingsTree;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settingslib/search/SettingsTree;-><init>(Landroid/content/Context;Lorg/json/JSONObject;Lcom/android/settingslib/search/SettingsTree;Z)V

    const-string/jumbo p3, "title"

    invoke-virtual {p2, p3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/android/settings/search/tree/DevelopmentSettingsTree;->mTitle:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/settings/search/tree/DevelopmentSettingsTree;->buildPreferenceControllersMap(Landroid/content/Context;)V

    return-void
.end method

.method private buildPreferenceControllersMap(Landroid/content/Context;)V
    .locals 3

    sget-object p0, Lcom/android/settings/search/tree/DevelopmentSettingsTree;->controllerMap:Ljava/util/Map;

    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/settings/development/OemUnlockPreferenceController;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1, v1}, Lcom/android/settings/development/OemUnlockPreferenceController;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/android/settings/development/DevelopmentSettingsDashboardFragment;)V

    const-string/jumbo v2, "oem_unlock_enable"

    invoke-interface {p0, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/android/settings/development/AdbInstallPreferenceController;

    invoke-direct {v0, v1}, Lcom/android/settings/development/AdbInstallPreferenceController;-><init>(Landroid/app/Activity;)V

    const-string/jumbo v2, "usb_install_app"

    invoke-interface {p0, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/android/settings/development/AdbInputPreferenceController;

    invoke-direct {v0, v1}, Lcom/android/settings/development/AdbInputPreferenceController;-><init>(Landroid/app/Activity;)V

    const-string/jumbo v2, "usb_adb_input"

    invoke-interface {p0, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/android/settings/development/VerifyAppsOverUsbPreferenceController;

    invoke-direct {v0, p1}, Lcom/android/settings/development/VerifyAppsOverUsbPreferenceController;-><init>(Landroid/content/Context;)V

    const-string/jumbo v2, "verify_apps_over_usb_title"

    invoke-interface {p0, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/android/settings/development/LocalTerminalPreferenceController;

    invoke-direct {v0, p1}, Lcom/android/settings/development/LocalTerminalPreferenceController;-><init>(Landroid/content/Context;)V

    const-string v2, "enable_terminal_title"

    invoke-interface {p0, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/android/settings/development/HdcpCheckingPreferenceController;

    invoke-direct {v0, p1}, Lcom/android/settings/development/HdcpCheckingPreferenceController;-><init>(Landroid/content/Context;)V

    const-string v2, "hdcp_checking_title"

    invoke-interface {p0, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/android/settings/development/CoolColorTemperaturePreferenceController;

    invoke-direct {v0, p1}, Lcom/android/settings/development/CoolColorTemperaturePreferenceController;-><init>(Landroid/content/Context;)V

    const-string v2, "color_temperature"

    invoke-interface {p0, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/android/settings/development/SecondSpaceDeleteController;

    invoke-direct {v0, v1, v1}, Lcom/android/settings/development/SecondSpaceDeleteController;-><init>(Landroid/app/Activity;Lcom/android/settingslib/core/lifecycle/Lifecycle;)V

    const-string/jumbo v2, "secondspace_delete_space_title"

    invoke-interface {p0, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/android/settings/development/ClearAdbKeysPreferenceController;

    invoke-direct {v0, p1, v1}, Lcom/android/settings/development/ClearAdbKeysPreferenceController;-><init>(Landroid/content/Context;Lcom/android/settings/development/DevelopmentSettingsDashboardFragment;)V

    const-string v2, "clear_adb_keys"

    invoke-interface {p0, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/android/settings/development/LogPersistPreferenceController;

    invoke-direct {v0, p1, v1, v1}, Lcom/android/settings/development/LogPersistPreferenceController;-><init>(Landroid/content/Context;Lcom/android/settings/development/DevelopmentSettingsDashboardFragment;Lcom/android/settingslib/core/lifecycle/Lifecycle;)V

    const-string/jumbo v2, "select_logpersist_title"

    invoke-interface {p0, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/android/settings/development/BluetoothAptxAdaptiveModePreferenceController;

    invoke-direct {v0, p1, v1, v1}, Lcom/android/settings/development/BluetoothAptxAdaptiveModePreferenceController;-><init>(Landroid/content/Context;Lcom/android/settingslib/core/lifecycle/Lifecycle;Lcom/android/settings/development/BluetoothA2dpConfigStore;)V

    const-string v2, "bluetooth_select_a2dp_codec_aptxadaptive_mode"

    invoke-interface {p0, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/android/settings/development/CameraLaserSensorPreferenceController;

    invoke-direct {v0, p1}, Lcom/android/settings/development/CameraLaserSensorPreferenceController;-><init>(Landroid/content/Context;)V

    const-string v2, "camera_laser_sensor_switch"

    invoke-interface {p0, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/android/settings/development/DarkUIPreferenceController;

    invoke-direct {v0, p1}, Lcom/android/settings/development/DarkUIPreferenceController;-><init>(Landroid/content/Context;)V

    const-string v2, "dark_ui_mode"

    invoke-interface {p0, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/android/settings/development/MiuiOptimizationController;

    invoke-direct {v0, p1}, Lcom/android/settings/development/MiuiOptimizationController;-><init>(Landroid/content/Context;)V

    const-string/jumbo v2, "miui_experience_optimization"

    invoke-interface {p0, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/android/settings/development/DeviceLockStateController;

    invoke-direct {v0, v1}, Lcom/android/settings/development/DeviceLockStateController;-><init>(Landroid/app/Activity;)V

    const-string v2, "bootloader_status"

    invoke-interface {p0, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/android/settings/development/ShowFirstCrashDialogPreferenceController;

    invoke-direct {v0, p1}, Lcom/android/settings/development/ShowFirstCrashDialogPreferenceController;-><init>(Landroid/content/Context;)V

    const-string/jumbo v2, "show_first_crash_dialog"

    invoke-interface {p0, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/android/settings/development/EmulateDisplayCutoutPreferenceController;

    invoke-direct {v0, p1}, Lcom/android/settings/development/EmulateDisplayCutoutPreferenceController;-><init>(Landroid/content/Context;)V

    const-string v2, "display_cutout_emulation"

    invoke-interface {p0, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/android/settings/development/AdbPreferenceController;

    invoke-direct {v0, p1, v1}, Lcom/android/settings/development/AdbPreferenceController;-><init>(Landroid/content/Context;Lcom/android/settings/development/DevelopmentSettingsDashboardFragment;)V

    const-string v1, "enable_adb"

    invoke-interface {p0, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/android/settings/security/SecurityEncryptionPreferenceController;

    const-string/jumbo v1, "security_encryption"

    invoke-direct {v0, p1, v1}, Lcom/android/settings/security/SecurityEncryptionPreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const-string/jumbo v1, "security_encryption_title"

    invoke-interface {p0, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/android/settings/development/BluetoothAbsoluteVolumePreferenceController;

    invoke-direct {v0, p1}, Lcom/android/settings/development/BluetoothAbsoluteVolumePreferenceController;-><init>(Landroid/content/Context;)V

    const-string v1, "bluetooth_disable_absolute_volume"

    invoke-interface {p0, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/android/settings/development/BluetoothPageScanPreferenceController;

    invoke-direct {v0, p1}, Lcom/android/settings/development/BluetoothPageScanPreferenceController;-><init>(Landroid/content/Context;)V

    const-string v1, "bluetooth_enable_page_scan"

    invoke-interface {p0, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/android/settings/development/BluetoothOppHighSpeedPreferenceController;

    invoke-direct {v0, p1}, Lcom/android/settings/development/BluetoothOppHighSpeedPreferenceController;-><init>(Landroid/content/Context;)V

    const-string v1, "bluetooth_enable_opp_high_speed"

    invoke-interface {p0, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/android/settings/development/LowFlickerBacklightController;

    invoke-direct {v0, p1}, Lcom/android/settings/development/LowFlickerBacklightController;-><init>(Landroid/content/Context;)V

    const-string v1, "low_dc_light_title"

    invoke-interface {p0, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;

    const-string/jumbo v1, "speed_mode"

    invoke-direct {v0, p1, v1}, Lcom/android/settings/development/SpeedModeToolsPreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-interface {p0, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method


# virtual methods
.method public getSons()Ljava/util/LinkedList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList<",
            "Lcom/android/settingslib/search/SettingsTree;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "resource"

    invoke-virtual {p0, v0}, Lcom/android/settingslib/search/SettingsTree;->getColumnValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "development_settings_title"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-super {p0}, Lcom/android/settingslib/search/SettingsTree;->getSons()Ljava/util/LinkedList;

    move-result-object v0

    const-string/jumbo v1, "temporary"

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v3

    sub-int/2addr v3, v2

    :goto_0
    if-ltz v3, :cond_1

    invoke-virtual {v0, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settingslib/search/SettingsTree;

    invoke-virtual {v4, v1}, Lcom/android/settingslib/search/SettingsTree;->getColumnValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Lcom/android/settingslib/search/SettingsTree;->removeSelf()V

    :cond_0
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settingslib/search/SettingsTree;->mContext:Landroid/content/Context;

    const-string v3, "com.android.traceur"

    const-string/jumbo v4, "system_tracing"

    invoke-static {v0, v3, v4}, Lcom/android/settings/MiuiUtils;->getStringFromSpecificPackage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    :try_start_0
    const-string/jumbo v4, "title"

    invoke-virtual {v3, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "category"

    const-string v4, "debug_debugging_category"

    invoke-virtual {v3, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v3, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string/jumbo v0, "preference_key"

    const-string v1, "dashboard_tile_pref_com.android.traceur.MainActivity"

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    iget-object v0, p0, Lcom/android/settingslib/search/SettingsTree;->mContext:Landroid/content/Context;

    invoke-static {v0, v3, p0}, Lcom/android/settingslib/search/SettingsTree;->newInstance(Landroid/content/Context;Lorg/json/JSONObject;Lcom/android/settingslib/search/SettingsTree;)Lcom/android/settingslib/search/SettingsTree;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/search/SettingsTree;->addSon(Lcom/android/settingslib/search/IndexTree;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_2
    invoke-super {p0}, Lcom/android/settingslib/search/SettingsTree;->getSons()Ljava/util/LinkedList;

    move-result-object p0

    return-object p0
.end method

.method protected getStatus()I
    .locals 9

    const-string/jumbo v0, "resource"

    invoke-virtual {p0, v0}, Lcom/android/settingslib/search/SettingsTree;->getColumnValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "miui_experience_optimization"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v1, :cond_0

    return v2

    :cond_0
    sget-object v1, Lcom/android/settings/search/tree/DevelopmentSettingsTree;->controllerMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/settingslib/core/AbstractPreferenceController;

    invoke-virtual {v3}, Lcom/android/settingslib/core/AbstractPreferenceController;->isAvailable()Z

    move-result v3

    if-nez v3, :cond_1

    return v2

    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    const/4 v3, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v4

    const-string v5, "bluetooth_enable_opp_high_speed"

    const-string v6, "bluetooth_disable_absolute_volume"

    const-string v7, "bluetooth_enable_page_scan"

    const/4 v8, 0x1

    sparse-switch v4, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    goto/16 :goto_0

    :cond_2
    const/16 v3, 0x11

    goto/16 :goto_0

    :sswitch_1
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    goto/16 :goto_0

    :cond_3
    const/16 v3, 0x10

    goto/16 :goto_0

    :sswitch_2
    const-string/jumbo v4, "verify_apps_over_usb_title"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    goto/16 :goto_0

    :cond_4
    const/16 v3, 0xf

    goto/16 :goto_0

    :sswitch_3
    const-string/jumbo v4, "wait_for_debugger"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    goto/16 :goto_0

    :cond_5
    const/16 v3, 0xe

    goto/16 :goto_0

    :sswitch_4
    const-string v4, "bluetooth_max_connected_audio_devices_string"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    goto/16 :goto_0

    :cond_6
    const/16 v3, 0xd

    goto/16 :goto_0

    :sswitch_5
    const-string/jumbo v4, "usb_install_app"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    goto/16 :goto_0

    :cond_7
    const/16 v3, 0xc

    goto/16 :goto_0

    :sswitch_6
    const-string/jumbo v4, "oem_unlock_enable"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    goto/16 :goto_0

    :cond_8
    const/16 v3, 0xb

    goto/16 :goto_0

    :sswitch_7
    const-string/jumbo v4, "select_logpersist_title"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_9

    goto/16 :goto_0

    :cond_9
    const/16 v3, 0xa

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_a

    goto/16 :goto_0

    :cond_a
    const/16 v3, 0x9

    goto/16 :goto_0

    :sswitch_9
    const-string v4, "bluetooth_enable_pts_test"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_b

    goto/16 :goto_0

    :cond_b
    const/16 v3, 0x8

    goto/16 :goto_0

    :sswitch_a
    const-string v4, "fiveg_nrca_switch_title"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_c

    goto :goto_0

    :cond_c
    const/4 v3, 0x7

    goto :goto_0

    :sswitch_b
    const-string v4, "development_settings_title"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_d

    goto :goto_0

    :cond_d
    const/4 v3, 0x6

    goto :goto_0

    :sswitch_c
    const-string/jumbo v4, "usb_adb_input"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_e

    goto :goto_0

    :cond_e
    const/4 v3, 0x5

    goto :goto_0

    :sswitch_d
    const-string v4, "fiveg_vonr_switch_title"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_f

    goto :goto_0

    :cond_f
    const/4 v3, 0x4

    goto :goto_0

    :sswitch_e
    const-string/jumbo v4, "unlock_set_enter_system"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_10

    goto :goto_0

    :cond_10
    const/4 v3, 0x3

    goto :goto_0

    :sswitch_f
    const-string v4, "bluetooth_disable_a2dp_hw_offload"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_11

    goto :goto_0

    :cond_11
    const/4 v3, 0x2

    goto :goto_0

    :sswitch_10
    const-string v4, "enable_terminal_title"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_12

    goto :goto_0

    :cond_12
    move v3, v8

    goto :goto_0

    :sswitch_11
    const-string v4, "fiveg_sa_vice_switch_title"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_13

    goto :goto_0

    :cond_13
    move v3, v2

    :goto_0
    packed-switch v3, :pswitch_data_0

    goto/16 :goto_1

    :pswitch_0
    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/development/BluetoothOppHighSpeedPreferenceController;

    if-eqz v0, :cond_16

    invoke-virtual {v0}, Lcom/android/settings/development/BluetoothOppHighSpeedPreferenceController;->isAvailable()Z

    move-result v0

    if-nez v0, :cond_16

    return v2

    :pswitch_1
    invoke-interface {v1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/development/BluetoothAbsoluteVolumePreferenceController;

    if-eqz v0, :cond_16

    invoke-virtual {v0}, Lcom/android/settings/development/BluetoothAbsoluteVolumePreferenceController;->isAvailable()Z

    move-result v0

    if-nez v0, :cond_16

    return v2

    :pswitch_2
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/development/VerifyAppsOverUsbPreferenceController;

    invoke-virtual {v0}, Lcom/android/settings/development/VerifyAppsOverUsbPreferenceController;->shouldBeEnabled()Z

    move-result v0

    if-nez v0, :cond_16

    return v8

    :pswitch_3
    iget-object v0, p0, Lcom/android/settingslib/search/SettingsTree;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "debug_app"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_16

    :pswitch_4
    return v8

    :pswitch_5
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/development/OemUnlockPreferenceController;

    invoke-virtual {v0}, Lcom/android/settings/development/OemUnlockPreferenceController;->enableOemUnlockPreference()Z

    move-result v0

    if-nez v0, :cond_16

    return v8

    :pswitch_6
    const-string v0, "logd.logpersistd.enable"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "persist.log.tag"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "persist.logd.size"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "32768"

    if-eqz v1, :cond_14

    const-string v4, "Settings"

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_14

    move-object v2, v3

    :cond_14
    if-eqz v0, :cond_15

    const-string/jumbo v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    :cond_15
    return v8

    :pswitch_7
    invoke-interface {v1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/development/BluetoothPageScanPreferenceController;

    if-eqz v0, :cond_16

    invoke-virtual {v0}, Lcom/android/settingslib/development/DeveloperOptionsPreferenceController;->isAvailable()Z

    move-result v0

    if-nez v0, :cond_16

    :pswitch_8
    return v2

    :pswitch_9
    iget-object v0, p0, Lcom/android/settingslib/search/SettingsTree;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/development/FiveGNrcaConfigController;->isSearchable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_16

    return v2

    :pswitch_a
    iget-object v0, p0, Lcom/android/settingslib/search/SettingsTree;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/personal/DevelopmentSettingsController;->hideDevelopmentSettings(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_16

    return v2

    :pswitch_b
    iget-object v0, p0, Lcom/android/settingslib/search/SettingsTree;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "adb_enabled"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_16

    return v8

    :pswitch_c
    iget-object v0, p0, Lcom/android/settingslib/search/SettingsTree;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/development/FiveGVonrConfigController;->isSearchable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_16

    return v2

    :pswitch_d
    new-instance v0, Lcom/android/internal/widget/LockPatternUtils;

    iget-object v1, p0, Lcom/android/settingslib/search/SettingsTree;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-static {v1}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/development/MiuiDirectEnterSystemController;->isEnabled(Lcom/android/internal/widget/LockPatternUtils;I)Z

    move-result v0

    if-nez v0, :cond_16

    return v8

    :pswitch_e
    const-string/jumbo v0, "ro.bluetooth.a2dp_offload.supported"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_16

    return v8

    :pswitch_f
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/development/LocalTerminalPreferenceController;

    invoke-virtual {v0}, Lcom/android/settings/development/LocalTerminalPreferenceController;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_16

    return v8

    :pswitch_10
    invoke-static {}, Lcom/android/settings/development/FiveGViceSAPreferenceController;->isViceSaDevelopmentAvailable()Z

    move-result v0

    if-nez v0, :cond_16

    return v2

    :cond_16
    :goto_1
    invoke-super {p0}, Lcom/android/settingslib/search/SettingsTree;->getStatus()I

    move-result p0

    return p0

    :sswitch_data_0
    .sparse-switch
        -0x72b42270 -> :sswitch_11
        -0x71bb82ef -> :sswitch_10
        -0x6d950501 -> :sswitch_f
        -0x6399aa32 -> :sswitch_e
        -0x5197017b -> :sswitch_d
        -0x386554d1 -> :sswitch_c
        -0x29ec5140 -> :sswitch_b
        -0xe584c40 -> :sswitch_a
        0x210538d -> :sswitch_9
        0x189447e2 -> :sswitch_8
        0x2cd6a04c -> :sswitch_7
        0x2e1229f6 -> :sswitch_6
        0x30c68742 -> :sswitch_5
        0x38e8e61e -> :sswitch_4
        0x3c0a8961 -> :sswitch_3
        0x438ca6d9 -> :sswitch_2
        0x5536d8fa -> :sswitch_1
        0x7802d265 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_b
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected getTitle(Z)Ljava/lang/String;
    .locals 2

    const-string/jumbo v0, "resource"

    invoke-virtual {p0, v0}, Lcom/android/settingslib/search/SettingsTree;->getColumnValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "low_dc_light_title"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->isSupportDifferentLight()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object p1, p0, Lcom/android/settingslib/search/SettingsTree;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/utils/SettingsFeatures;->isScreenLayoutLarge(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    const-string/jumbo p1, "support_dc_backlight"

    goto :goto_0

    :cond_0
    const-string/jumbo p1, "support_dc_backlight_sec"

    :goto_0
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    iget-object p0, p0, Lcom/android/settingslib/search/SettingsTree;->mContext:Landroid/content/Context;

    if-eqz p1, :cond_1

    sget p1, Lcom/android/settings/R$string;->dc_light_title:I

    goto :goto_1

    :cond_1
    sget p1, Lcom/android/settings/R$string;->low_dc_light_title:I

    :goto_1
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/search/tree/DevelopmentSettingsTree;->mTitle:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object p0, p0, Lcom/android/settings/search/tree/DevelopmentSettingsTree;->mTitle:Ljava/lang/String;

    return-object p0

    :cond_3
    invoke-super {p0, p1}, Lcom/android/settingslib/search/SettingsTree;->getTitle(Z)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
