.class Lcom/android/settings/search/DisplayUpdateHelper;
.super Lcom/android/settings/search/BaseSearchUpdateHelper;


# static fields
.field private static final AUTOMATIC_BRIGHTNESS_RESOURCE:Ljava/lang/String; = "automatic_brightness"

.field private static final DOZE_RESOURCE:Ljava/lang/String; = "doze_title"

.field private static final INFINITY_DISPLAY_RESOURCE:Ljava/lang/String; = "infinity_display_title"

.field private static final LIFT_TO_WAKE_RESOURCE:Ljava/lang/String; = "lift_to_wake_title"

.field private static final NOTIFICATION_PULSE_RESOURCE:Ljava/lang/String; = "notification_pulse_title"

.field private static final SCREEN_COLOR_AND_OPTIMIZE_RESOURCE:Ljava/lang/String; = "screen_color_and_optimize"

.field private static final SCREEN_COLOR_AND_TEMPERATURE_RESOURCE:Ljava/lang/String; = "screen_color_temperature_and_saturation"

.field private static final SCREEN_EFFECT_RESOURCE:Ljava/lang/String; = "screen_effect"

.field private static final SCREEN_MONOCHROME_MODE_GLOBAL_RESOURCE:Ljava/lang/String; = "screen_monochrome_mode_global_title"

.field private static final SCREEN_MONOCHROME_MODE_LOCAL_RESOURCE:Ljava/lang/String; = "screen_monochrome_mode_local_title"

.field private static final SCREEN_MONOCHROME_MODE_RESOURCE:Ljava/lang/String; = "screen_monochrome_mode_title"

.field private static final SCREEN_PAPER_MODE_RESOURCE:Ljava/lang/String; = "screen_paper_mode_title"

.field private static final TITLE_FONT_CURRENT2_RESOURCE:Ljava/lang/String; = "title_font_current2"

.field private static final TITLE_FONT_SIZE_RESOURCE:Ljava/lang/String; = "title_font_size"

.field private static final TITLE_LAYOUT_CURRENT2_RESOURCE:Ljava/lang/String; = "title_layout_current2"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/search/BaseSearchUpdateHelper;-><init>()V

    return-void
.end method

.method static update(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string/jumbo v2, "screen_effect_supported"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "screen_effect"

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_0
    const-string/jumbo v2, "support_screen_effect"

    invoke-static {v2, v3}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    const-string/jumbo v4, "screen_color_and_optimize"

    if-eqz v2, :cond_1

    const-string/jumbo v2, "support_screen_optimize"

    invoke-static {v2, v3}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "screen_color_temperature_and_saturation"

    invoke-static {v0, v1, v4, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->updatePath(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v4}, Lcom/android/settings/search/BaseSearchUpdateHelper;->getIdWithResource(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/android/settings/R$string;->screen_color_temperature_and_saturation:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "name"

    invoke-static {v0, v1, v4, v6, v5}, Lcom/android/settings/search/BaseSearchUpdateHelper;->updateItemData(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {v0, v1, v4}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_2
    sget-boolean v2, Landroid/provider/MiuiSettings$ScreenEffect;->isScreenPaperModeSupported:Z

    if-nez v2, :cond_3

    const-string/jumbo v2, "screen_paper_mode_title"

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideTreeByRootResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_3
    sget v2, Landroid/provider/MiuiSettings$ScreenEffect;->SCREEN_EFFECT_SUPPORTED:I

    and-int/lit8 v2, v2, 0x8

    if-nez v2, :cond_4

    const-string/jumbo v2, "screen_monochrome_mode_title"

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v2, "screen_monochrome_mode_global_title"

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v2, "screen_monochrome_mode_local_title"

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_4
    const-string v2, "lift_to_wake_title"

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const/4 v2, 0x1

    sget-boolean v4, Lmiui/os/Build;->IS_DEBUGGABLE:Z

    if-eqz v4, :cond_5

    const-string v4, "debug.doze.component"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_5
    const/4 v4, 0x0

    :goto_1
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x1040274

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    :cond_6
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    invoke-static {v4}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v4, v5, v3}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_7

    move v2, v3

    :cond_7
    if-eqz v2, :cond_8

    const-string v2, "doze_title"

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_8
    const-string/jumbo v2, "support_led_light"

    invoke-static {v2, v3}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string/jumbo v2, "notification_pulse_title"

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_9
    sget-boolean v2, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v2, :cond_a

    const-string/jumbo v2, "title_font_current2"

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v2, "title_layout_current2"

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_2

    :cond_a
    const-string/jumbo v2, "title_font_size"

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :goto_2
    const-string/jumbo v2, "window"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/android/settings/R$string;->automatic_brightness:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v11, 0x0

    const-wide/16 v14, 0x0

    const-string v3, "com.android.settings05053"

    const-string/jumbo v9, "zidongtiaozhengliangdu"

    const-string v10, "com.android.settings"

    const-string v12, "com.android.settings.SubSettings"

    const-string/jumbo v13, "settings_label-display_settings-brightness-automatic_brightness"

    const-string v16, "com.android.settings.display.BrightnessFragment"

    move-object/from16 v0, p1

    move-object v1, v3

    move-object v3, v9

    move-object v9, v10

    move-object v10, v12

    move-object v12, v13

    move-object/from16 v13, v16

    invoke-static/range {v0 .. v15}, Lcom/android/settings/search/BaseSearchUpdateHelper;->insertSearchItem(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    return-void
.end method
