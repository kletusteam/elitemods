.class public Lcom/android/settings/search/SearchResult;
.super Ljava/lang/Object;


# static fields
.field public static ENABLED_REDUNDANT_ENTRIES_SET:Ljava/util/Set; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "SearchResult"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/settings/search/SearchResult;->ENABLED_REDUNDANT_ENTRIES_SET:Ljava/util/Set;

    const-string/jumbo v1, "sim_pin_toggle"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/search/SearchResult;->ENABLED_REDUNDANT_ENTRIES_SET:Ljava/util/Set;

    const-string/jumbo v1, "sim_pin_change"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/search/SearchResult;->ENABLED_REDUNDANT_ENTRIES_SET:Ljava/util/Set;

    const-string/jumbo v1, "sim_radio_off"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/search/SearchResult;->ENABLED_REDUNDANT_ENTRIES_SET:Ljava/util/Set;

    const-string/jumbo v1, "wallpaper_setting_title"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/search/SearchResult;->ENABLED_REDUNDANT_ENTRIES_SET:Ljava/util/Set;

    const-string v1, "application_mode_name"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/search/SearchResult;->ENABLED_REDUNDANT_ENTRIES_SET:Ljava/util/Set;

    const-string v1, "fold_game_adaptation"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/search/SearchResult;->ENABLED_REDUNDANT_ENTRIES_SET:Ljava/util/Set;

    const-string v1, "close_lid_display_settings_title"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/search/SearchResult;->ENABLED_REDUNDANT_ENTRIES_SET:Ljava/util/Set;

    const-string v1, "device_ime_keyboard_fold"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/search/SearchResult;->ENABLED_REDUNDANT_ENTRIES_SET:Ljava/util/Set;

    const-string v1, "device_ime_keyboard_pad"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/search/SearchResult;->ENABLED_REDUNDANT_ENTRIES_SET:Ljava/util/Set;

    const-string v1, "freeform_guide_settings"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/android/settings/search/SearchResult;->ENABLED_REDUNDANT_ENTRIES_SET:Ljava/util/Set;

    const-string v1, "beauty_fc_assistant"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static checkBundleEqual(Landroid/os/Bundle;Landroid/os/Bundle;)Z
    .locals 6

    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_1

    return v3

    :cond_1
    invoke-virtual {p0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    instance-of v4, v2, Landroid/os/Bundle;

    if-eqz v4, :cond_2

    instance-of v4, v1, Landroid/os/Bundle;

    if-eqz v4, :cond_2

    move-object v4, v2

    check-cast v4, Landroid/os/Bundle;

    move-object v5, v1

    check-cast v5, Landroid/os/Bundle;

    invoke-static {v4, v5}, Lcom/android/settings/search/SearchResult;->checkBundleEqual(Landroid/os/Bundle;Landroid/os/Bundle;)Z

    move-result v4

    if-nez v4, :cond_2

    return v3

    :cond_2
    invoke-static {v2, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    return v3

    :cond_3
    const/4 p0, 0x1

    return p0
.end method

.method private static checkIntentEqual(Landroid/content/Intent;Landroid/content/Intent;)Z
    .locals 2

    if-nez p0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    if-eqz p0, :cond_2

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/android/settings/search/SearchResult;->checkBundleEqual(Landroid/os/Bundle;Landroid/os/Bundle;)Z

    move-result p0

    if-eqz p0, :cond_2

    :cond_1
    const/4 p0, 0x1

    goto :goto_0

    :cond_2
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static getSearchExcludeMap()Ljava/util/HashSet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "ro.radio.noril"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sget-boolean v2, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    if-eqz v1, :cond_1

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    const-string v1, "com.miui.antispam.ui.activity.MainActivity"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const-string/jumbo v1, "miui.intent.action.SET_FIREWALL"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const-string v1, "com.miui.antispam.ui.activity.AntiSpamNewSettingsActivity"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const-string v1, "com.miui.antispam.action.ANTISPAM_SETTINGS"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public static removeExcludeItem(Ljava/util/HashSet;Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Lcom/android/settings/search/SearchResultItem;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/android/settings/search/SearchResultItem;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/search/SearchResultItem;

    iget-object v2, v1, Lcom/android/settings/search/SearchResultItem;->intent:Landroid/content/Intent;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method


# virtual methods
.method public getSearchResultList(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;
    .locals 6

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p2}, Lcom/android/settings/search/provider/SettingsProvider;->getSearchUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/android/settingslib/search/Function;->FUNCTIONS:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p0

    new-instance p1, Ljava/util/LinkedList;

    invoke-direct {p1}, Ljava/util/LinkedList;-><init>()V

    if-nez p0, :cond_0

    return-object p1

    :cond_0
    check-cast p0, Landroid/database/CursorWrapper;

    invoke-virtual {p0}, Landroid/database/CursorWrapper;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object p0

    check-cast p0, Lcom/android/settingslib/search/RankedCursor;

    const/4 p2, 0x0

    move-object v0, p2

    :goto_0
    invoke-virtual {p0}, Lcom/android/settingslib/search/RankedCursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_4

    const-string/jumbo v1, "resource"

    invoke-virtual {p0, v1}, Lcom/android/settingslib/search/RankedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/settingslib/search/RankedCursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/settingslib/search/RankedCursor;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "intent"

    invoke-virtual {p0, v3}, Lcom/android/settingslib/search/RankedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/android/settingslib/search/RankedCursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->isSplitTabletDevice()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->isFoldDevice()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addMiuiFlags(I)Landroid/content/Intent;

    :cond_2
    invoke-static {p2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {v2, v0}, Lcom/android/settings/search/SearchResult;->checkIntentEqual(Landroid/content/Intent;Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v3, Lcom/android/settings/search/SearchResult;->ENABLED_REDUNDANT_ENTRIES_SET:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    goto :goto_0

    :cond_3
    new-instance p2, Lcom/android/settings/search/SearchResultItem;

    const/4 v0, 0x0

    invoke-direct {p2, v0}, Lcom/android/settings/search/SearchResultItem;-><init>(I)V

    const-string/jumbo v0, "package"

    invoke-virtual {p0, v0}, Lcom/android/settingslib/search/RankedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/search/RankedCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, Lcom/android/settings/search/SearchResultItem;->pkg:Ljava/lang/String;

    const-string/jumbo v0, "preference_key"

    invoke-virtual {p0, v0}, Lcom/android/settingslib/search/RankedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/search/RankedCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, Lcom/android/settings/search/SearchResultItem;->preferenceKey:Ljava/lang/String;

    iput-object v1, p2, Lcom/android/settings/search/SearchResultItem;->resource:Ljava/lang/String;

    const-string/jumbo v0, "title"

    invoke-virtual {p0, v0}, Lcom/android/settingslib/search/RankedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/search/RankedCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, Lcom/android/settings/search/SearchResultItem;->title:Ljava/lang/String;

    const-string v0, "category"

    invoke-virtual {p0, v0}, Lcom/android/settingslib/search/RankedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/search/RankedCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, Lcom/android/settings/search/SearchResultItem;->category:Ljava/lang/String;

    const-string/jumbo v0, "path"

    invoke-virtual {p0, v0}, Lcom/android/settingslib/search/RankedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/search/RankedCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, Lcom/android/settings/search/SearchResultItem;->path:Ljava/lang/String;

    const-string v0, "keywords"

    invoke-virtual {p0, v0}, Lcom/android/settingslib/search/RankedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/search/RankedCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, Lcom/android/settings/search/SearchResultItem;->keywords:Ljava/lang/String;

    const-string/jumbo v0, "summary"

    invoke-virtual {p0, v0}, Lcom/android/settingslib/search/RankedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/search/RankedCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, Lcom/android/settings/search/SearchResultItem;->summary:Ljava/lang/String;

    const-string v0, "icon"

    invoke-virtual {p0, v0}, Lcom/android/settingslib/search/RankedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/search/RankedCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, Lcom/android/settings/search/SearchResultItem;->icon:Ljava/lang/String;

    const-string v0, "is_checkbox"

    invoke-virtual {p0, v0}, Lcom/android/settingslib/search/RankedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/search/RankedCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p2, Lcom/android/settings/search/SearchResultItem;->checkbox:Z

    iput-object v2, p2, Lcom/android/settings/search/SearchResultItem;->intent:Landroid/content/Intent;

    const-string/jumbo v0, "status"

    invoke-virtual {p0, v0}, Lcom/android/settingslib/search/RankedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settingslib/search/RankedCursor;->getInt(I)I

    move-result v0

    iput v0, p2, Lcom/android/settings/search/SearchResultItem;->status:I

    invoke-virtual {p0}, Lcom/android/settingslib/search/RankedCursor;->getScore()D

    move-result-wide v3

    iput-wide v3, p2, Lcom/android/settings/search/SearchResultItem;->score:D

    iget-object v0, p2, Lcom/android/settings/search/SearchResultItem;->path:Ljava/lang/String;

    iget-object v3, p2, Lcom/android/settings/search/SearchResultItem;->summary:Ljava/lang/String;

    invoke-virtual {p2, v0, v3}, Lcom/android/settings/search/SearchResultItem;->getGroupInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, Lcom/android/settings/search/SearchResultItem;->group:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    move-object p2, v1

    move-object v0, v2

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_5

    sget-object p2, Lcom/android/settings/search/SearchResultItem;->EMPTY:Lcom/android/settings/search/SearchResultItem;

    invoke-virtual {p1, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :cond_5
    invoke-virtual {p0}, Lcom/android/settingslib/search/RankedCursor;->close()V

    return-object p1
.end method
