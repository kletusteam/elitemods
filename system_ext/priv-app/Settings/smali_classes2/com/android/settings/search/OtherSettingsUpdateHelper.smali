.class Lcom/android/settings/search/OtherSettingsUpdateHelper;
.super Lcom/android/settings/search/BaseSearchUpdateHelper;


# static fields
.field private static final AI_TOUCH_RESOURCE:Ljava/lang/String; = "ai_button_title"

.field private static final APP_UPDATER_RESOURCE:Ljava/lang/String; = "system_apps_updater"

.field private static final AVOID_UI_RESOURCE:Ljava/lang/String; = "avoid_ui"

.field private static final BATTERY_INDICATOR_STYLE_RESOURCE:Ljava/lang/String; = "battery_indicator_style"

.field private static final CALL_BREATHING_LIGHT_COLOR_RESOURCE:Ljava/lang/String; = "call_breathing_light_color"

.field private static final CALL_BREATHING_LIGHT_FREQ_RESOURCE:Ljava/lang/String; = "call_breathing_light_freq"

.field private static final EMERGENCY_SOS:Ljava/lang/String; = "emergency_sos_title"

.field private static final FAKECELL_SETTINGS_RESOURCE:Ljava/lang/String; = "manage_fakecell_settings"

.field private static final HANDY_MODE_RESOURCE:Ljava/lang/String; = "handy_mode"

.field private static final HOME_XIAOAI_RESOURCE:Ljava/lang/String; = "voice_assist"

.field private static final INFINITY_DISPLAY_RESOURCE:Ljava/lang/String; = "infinity_display_title"

.field private static final LOCKSCREEN_MAGAZINE_RESOURCE:Ljava/lang/String; = "lockscreen_magazine"

.field private static final MIUI_LAB_AI_PRELOAD:Ljava/lang/String; = "miui_lab_ai_preload_title"

.field private static final MIUI_LAB_RESOURCE:Ljava/lang/String; = "miui_lab_settings"

.field private static final MI_SERVICE:Ljava/lang/String; = "mi_service"

.field private static final MI_SERVICE_URL:Ljava/lang/String; = "http://ab.xiaomi.com/d?url=aHR0cDovL20ubWkuY29tL3Nkaz9waWQ9MTI1MDEmY2lkPTIwMDI3LjAwMDAxJnJvb3Q9Y29tLnhpYW9taS5zaG9wLnBsdWdpbi5zeXNzZXR0aW5nLlJvb3RGcmFnbWVudCZtYXNpZD0yMDAyNy4wMDAwMQ=="

.field private static final MMS_BREATHING_LIGHT_COLOR_RESOURCE:Ljava/lang/String; = "mms_breathing_light_color"

.field private static final MMS_BREATHING_LIGHT_FREQ_RESOURCE:Ljava/lang/String; = "mms_breathing_light_freq"

.field private static final NOTCH_FORCE_BLACK_RESOURCE:Ljava/lang/String; = "notch_force_black_title"

.field private static final POWER_MODE_RESOURCE:Ljava/lang/String; = "power_mode"

.field private static final PRINT_RESOURCE:Ljava/lang/String; = "print_settings"

.field private static final SECOND_SPACE_RESOURCE:Ljava/lang/String; = "second_space"

.field private static final SHOW_NOTIFICATION_ICON_RESOURCE:Ljava/lang/String; = "status_bar_settings_show_notification_icon"

.field private static final SPELLCHECKERS_RESOURCE:Ljava/lang/String; = "spellcheckers_settings_title"

.field private static final STATUS_BAR_RESOURCE:Ljava/lang/String; = "status_bar_settings"

.field private static final TAPLUS_SETTINGS_RESOURCE:Ljava/lang/String; = "taplus_title"

.field private static final THEME_RESOURCE:Ljava/lang/String; = "theme_settings_title"

.field private static final USER_DICT_RESOURCE:Ljava/lang/String; = "user_dict_settings_title"

.field private static final VIBRATE_INPUT_DEVICES_RESOURCE:Ljava/lang/String; = "vibrate_input_devices"

.field private static final WALLPAPER_RESOURCE:Ljava/lang/String; = "wallpaper_settings_title"

.field private static final XIAOMI_ACCOUNT:Ljava/lang/String; = "xiaomi_account"

.field private static final XIAOMI_ACCOUNT_INFO:Ljava/lang/String; = "unlogin_account_title"

.field private static final XSPACE_RESOURCE:Ljava/lang/String; = "xspace"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/search/BaseSearchUpdateHelper;-><init>()V

    return-void
.end method

.method static update(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-static {}, Lcom/android/settingslib/OldmanHelper;->isOldmanMode()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lmiui/securityspace/ConfigUtils;->isSupportSecuritySpace()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    const-string/jumbo v3, "second_space"

    invoke-static {v0, v1, v3}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideTreeByRootResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_1
    invoke-static {}, Lcom/android/settingslib/OldmanHelper;->isOldmanMode()Z

    move-result v3

    if-eqz v3, :cond_2

    const-string/jumbo v3, "wallpaper_settings_title"

    invoke-static {v0, v1, v3}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideTreeByRootResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_2
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    if-nez v3, :cond_3

    invoke-static {}, Lcom/android/settingslib/OldmanHelper;->isOldmanMode()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-static {}, Lmiui/securityspace/ConfigUtils;->isSupportSecuritySpace()Z

    move-result v3

    if-nez v3, :cond_4

    :cond_3
    const-string/jumbo v3, "xspace"

    invoke-static {v0, v1, v3}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_4
    invoke-static/range {p0 .. p0}, Lmiui/theme/ThemeManagerHelper;->needDisableTheme(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-static {}, Lcom/android/settingslib/OldmanHelper;->isOldmanMode()Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_5
    const-string/jumbo v3, "theme_settings_title"

    invoke-static {v0, v1, v3}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideTreeByRootResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_6
    const-string/jumbo v3, "support_power_mode"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_7

    const-string/jumbo v3, "power_mode"

    invoke-static {v0, v1, v3}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_7
    invoke-static/range {p0 .. p0}, Lcom/android/settingslib/OldmanHelper;->isStatusBarSettingsHidden(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_8

    const-string/jumbo v3, "status_bar_settings"

    invoke-static {v0, v1, v3}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideTreeByRootResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_8
    sget-boolean v3, Lmiui/util/CustomizeUtil;->HAS_NOTCH:Z

    if-eqz v3, :cond_9

    const-string v3, "battery_indicator_style"

    invoke-static {v0, v1, v3}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v3, "status_bar_settings_show_notification_icon"

    invoke-static {v0, v1, v3}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_0

    :cond_9
    const-string/jumbo v3, "notch_force_black_title"

    invoke-static {v0, v1, v3}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :goto_0
    const-string/jumbo v3, "sensor"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/hardware/SensorManager;

    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v3

    const/4 v5, 0x1

    if-eqz v3, :cond_a

    invoke-virtual {v3}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Elliptic Proximity"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-virtual {v3}, Landroid/hardware/Sensor;->getVendor()Ljava/lang/String;

    move-result-object v3

    const-string v6, "Elliptic Labs"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    move v3, v5

    goto :goto_1

    :cond_a
    move v3, v4

    :goto_1
    const-string v6, "com.miui.sensor.avoid"

    invoke-virtual {v2, v6}, Landroid/content/pm/PackageManager;->isPackageAvailable(Ljava/lang/String;)Z

    move-result v6

    xor-int/2addr v6, v5

    or-int/2addr v3, v6

    if-eqz v3, :cond_b

    const-string v3, "avoid_ui"

    invoke-static {v0, v1, v3}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_b
    invoke-static {}, Lmiui/util/HandyModeUtils;->isFeatureVisible()Z

    move-result v3

    if-nez v3, :cond_c

    const-string v3, "handy_mode"

    invoke-static {v0, v1, v3}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideTreeByRootResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_c
    invoke-static/range {p0 .. p0}, Lcom/android/settings/Utils;->isVoiceCapable(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_d

    const-string v3, "call_breathing_light_color"

    invoke-static {v0, v1, v3}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string v3, "call_breathing_light_freq"

    invoke-static {v0, v1, v3}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v3, "mms_breathing_light_color"

    invoke-static {v0, v1, v3}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    const-string/jumbo v3, "mms_breathing_light_freq"

    invoke-static {v0, v1, v3}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_d
    invoke-static/range {p0 .. p0}, Lcom/android/settings/MiuiUtils;->needRemoveSystemAppsUpdater(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_e

    const-string/jumbo v3, "system_apps_updater"

    invoke-static {v0, v1, v3}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_e
    const-string/jumbo v3, "textservices"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/textservice/TextServicesManager;

    invoke-virtual {v3}, Landroid/view/textservice/TextServicesManager;->getEnabledSpellCheckers()[Landroid/view/textservice/SpellCheckerInfo;

    move-result-object v6

    invoke-virtual {v3}, Landroid/view/textservice/TextServicesManager;->isSpellCheckerEnabled()Z

    move-result v3

    if-eqz v3, :cond_f

    if-eqz v6, :cond_f

    array-length v3, v6

    if-nez v3, :cond_10

    :cond_f
    const-string/jumbo v3, "spellcheckers_settings_title"

    invoke-static {v0, v1, v3}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_10
    const-string v3, "input_method"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodManager;->getEnabledInputMethodList()Ljava/util/List;

    move-result-object v3

    if-nez v3, :cond_11

    move v6, v4

    goto :goto_2

    :cond_11
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    :goto_2
    move v7, v4

    :goto_3
    if-ge v7, v6, :cond_13

    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/inputmethod/InputMethodInfo;

    invoke-virtual {v8, v2}, Landroid/view/inputmethod/InputMethodInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v8

    if-eqz v8, :cond_12

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "AOSP"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_12

    move v3, v5

    goto :goto_4

    :cond_12
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    :cond_13
    move v3, v4

    :goto_4
    if-nez v3, :cond_14

    const-string/jumbo v3, "user_dict_settings_title"

    invoke-static {v0, v1, v3}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_14
    invoke-static {}, Landroid/view/InputDevice;->getDeviceIds()[I

    move-result-object v3

    move v6, v4

    :goto_5
    array-length v7, v3

    if-ge v6, v7, :cond_16

    aget v7, v3, v6

    invoke-static {v7}, Landroid/view/InputDevice;->getDevice(I)Landroid/view/InputDevice;

    move-result-object v7

    if-eqz v7, :cond_15

    invoke-virtual {v7}, Landroid/view/InputDevice;->isVirtual()Z

    move-result v8

    if-nez v8, :cond_15

    invoke-virtual {v7}, Landroid/view/InputDevice;->getVibrator()Landroid/os/Vibrator;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v7

    if-eqz v7, :cond_15

    move v3, v5

    goto :goto_6

    :cond_15
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    :cond_16
    move v3, v4

    :goto_6
    if-nez v3, :cond_17

    const-string/jumbo v3, "vibrate_input_devices"

    invoke-static {v0, v1, v3}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_17
    invoke-static {}, Lcom/android/settings/FakeCellSettings;->supportDetectFakecell()Z

    move-result v3

    if-nez v3, :cond_18

    const-string v3, "manage_fakecell_settings"

    invoke-static {v0, v1, v3}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_18
    new-instance v3, Landroid/content/Intent;

    const-string v6, "android.printservice.PrintService"

    invoke-direct {v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v6, 0x84

    invoke-virtual {v2, v3, v6}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_19

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1a

    :cond_19
    const-string/jumbo v2, "print_settings"

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_1a
    invoke-static/range {p0 .. p0}, Lcom/android/settings/utils/SettingsFeatures;->isMiuiLabNeedHide(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1b

    const-string/jumbo v2, "miui_lab_settings"

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_1b
    invoke-static/range {p0 .. p0}, Lcom/android/settings/utils/SettingsFeatures;->isNeedRemoveSOS(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1c

    const-string v2, "emergency_sos_title"

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_1c
    const-string/jumbo v2, "window"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v2

    :try_start_0
    invoke-interface {v2, v4}, Landroid/view/IWindowManager;->hasNavigationBar(I)Z

    move-result v2

    if-nez v2, :cond_1d

    const-string v2, "infinity_display_title"

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideTreeByRootResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_1d
    sget-boolean v2, Lmiui/os/Build;->IS_TABLET:Z

    const-string/jumbo v3, "mi_service"

    const-string v6, "fragment"

    const-string v7, "dest_class"

    const-string v8, "dest_package"

    const-string v9, "intent_data"

    const-string v10, "intent_action"

    const/4 v11, 0x5

    const-string v12, ""

    if-nez v2, :cond_1f

    sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v2, :cond_1e

    goto :goto_8

    :cond_1e
    new-instance v2, Landroid/content/Intent;

    const-string v13, "http://ab.xiaomi.com/d?url=aHR0cDovL20ubWkuY29tL3Nkaz9waWQ9MTI1MDEmY2lkPTIwMDI3LjAwMDAxJnJvb3Q9Y29tLnhpYW9taS5zaG9wLnBsdWdpbi5zeXNzZXR0aW5nLlJvb3RGcmFnbWVudCZtYXNpZD0yMDAyNy4wMDAwMQ=="

    invoke-static {v13}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    const-string v15, "android.intent.action.VIEW"

    invoke-direct {v2, v15, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {}, Lcom/android/settings/MiuiUtils;->getInstance()Lcom/android/settings/MiuiUtils;

    move-result-object v14

    invoke-virtual {v14, v0, v2}, Lcom/android/settings/MiuiUtils;->canFindActivity(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_20

    invoke-static {v0, v3}, Lcom/android/settings/search/BaseSearchUpdateHelper;->getIdWithResource(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_20

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    filled-new-array {v10, v9, v8, v7, v6}, [Ljava/lang/String;

    move-result-object v5

    filled-new-array {v15, v13, v12, v12, v12}, [Ljava/lang/String;

    move-result-object v14

    invoke-static {v1, v3, v11, v5, v14}, Lcom/android/settings/search/BaseSearchUpdateHelper;->updateSearchItem(Ljava/util/ArrayList;Ljava/lang/String;I[Ljava/lang/String;[Ljava/lang/String;)V

    const/4 v5, 0x1

    goto :goto_7

    :cond_1f
    :goto_8
    invoke-static {v0, v1, v3}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_20
    invoke-static/range {p0 .. p0}, Lcom/android/settings/MiuiUtils;->includeXiaoAi(Landroid/content/Context;)Z

    move-result v2

    const/4 v13, 0x2

    if-eqz v2, :cond_21

    invoke-static {}, Lcom/android/settings/MiuiUtils;->buildXiaoAiSettingsIntent()Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_21

    const-string/jumbo v14, "voice_assist"

    invoke-static {v0, v14}, Lcom/android/settings/search/BaseSearchUpdateHelper;->getIdWithResource(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_9
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_21

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    filled-new-array {v10, v9, v8, v7, v6}, [Ljava/lang/String;

    move-result-object v5

    new-array v15, v11, [Ljava/lang/String;

    aput-object v12, v15, v4

    const/16 v16, 0x1

    aput-object v12, v15, v16

    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v15, v13

    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v19

    const/16 v18, 0x3

    aput-object v19, v15, v18

    const/16 v17, 0x4

    aput-object v12, v15, v17

    invoke-static {v1, v3, v11, v5, v15}, Lcom/android/settings/search/BaseSearchUpdateHelper;->updateSearchItem(Ljava/util/ArrayList;Ljava/lang/String;I[Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_9

    :cond_21
    const-string v2, "lockscreen_magazine"

    invoke-static {v0, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->getIdWithResource(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_23

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static/range {p0 .. p0}, Lcom/android/settings/MiuiSecuritySettings;->getWallpaperIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v14

    if-nez v14, :cond_22

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    move/from16 v19, v13

    const/4 v14, 0x4

    const/16 v16, 0x1

    const/16 v18, 0x3

    goto :goto_b

    :cond_22
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    filled-new-array {v10, v9, v8, v7, v6}, [Ljava/lang/String;

    move-result-object v13

    new-array v15, v11, [Ljava/lang/String;

    aput-object v12, v15, v4

    const/16 v16, 0x1

    aput-object v12, v15, v16

    invoke-virtual {v14}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v20

    const/16 v19, 0x2

    aput-object v20, v15, v19

    invoke-virtual {v14}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v14

    const/16 v18, 0x3

    aput-object v14, v15, v18

    const/4 v14, 0x4

    aput-object v12, v15, v14

    invoke-static {v1, v5, v11, v13, v15}, Lcom/android/settings/search/BaseSearchUpdateHelper;->updateSearchItem(Ljava/util/ArrayList;Ljava/lang/String;I[Ljava/lang/String;[Ljava/lang/String;)V

    :goto_b
    move/from16 v13, v19

    goto :goto_a

    :cond_23
    invoke-static/range {p0 .. p0}, Lcom/android/settings/MiuiUtils;->isAiKeyExist(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_24

    const-string/jumbo v2, "support_ai_task"

    invoke-static {v2, v4}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_25

    :cond_24
    const-string v2, "ai_button_title"

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_25
    invoke-static {}, Lcom/android/settings/lab/MiuiAiPreloadController;->isNotSupported()Z

    move-result v2

    if-eqz v2, :cond_26

    const-string/jumbo v2, "miui_lab_ai_preload_title"

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_26
    sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v2, :cond_27

    sget-boolean v2, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v2, :cond_28

    :cond_27
    const-string/jumbo v2, "taplus_title"

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_28
    sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v2, :cond_29

    const-string/jumbo v2, "unlogin_account_title"

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_c

    :cond_29
    const-string/jumbo v2, "xiaomi_account"

    invoke-static {v0, v1, v2}, Lcom/android/settings/search/BaseSearchUpdateHelper;->hideByResource(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    :goto_c
    return-void
.end method
