.class Lcom/android/settings/dangerousoptions/DangerousOptionsUtil$1;
.super Landroid/util/SparseIntArray;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/dangerousoptions/DangerousOptionsUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/util/SparseIntArray;-><init>()V

    sget v0, Lcom/android/settings/R$string;->switch_access_service_on:I

    const/16 v1, 0x20

    invoke-virtual {p0, v1, v0}, Landroid/util/SparseIntArray;->put(II)V

    sget v0, Lcom/android/settings/R$string;->switch_select_to_speak_on:I

    const/16 v1, 0x40

    invoke-virtual {p0, v1, v0}, Landroid/util/SparseIntArray;->put(II)V

    sget v0, Lcom/android/settings/R$string;->debug_layout:I

    const/16 v1, 0x400

    invoke-virtual {p0, v1, v0}, Landroid/util/SparseIntArray;->put(II)V

    sget v0, Lcom/android/settings/R$string;->show_hw_screen_updates:I

    const/16 v1, 0x800

    invoke-virtual {p0, v1, v0}, Landroid/util/SparseIntArray;->put(II)V

    sget v0, Lcom/android/settings/R$string;->strict_mode:I

    const/16 v1, 0x1000

    invoke-virtual {p0, v1, v0}, Landroid/util/SparseIntArray;->put(II)V

    sget v0, Lcom/android/settings/R$string;->immediately_destroy_activities:I

    const/16 v1, 0x2000

    invoke-virtual {p0, v1, v0}, Landroid/util/SparseIntArray;->put(II)V

    sget v0, Lcom/android/settings/R$string;->show_screen_updates:I

    const/16 v1, 0x4000

    invoke-virtual {p0, v1, v0}, Landroid/util/SparseIntArray;->put(II)V

    sget v0, Lcom/android/settings/R$string;->accessibility_toggle_high_text_contrast_preference_title:I

    const/16 v1, 0x80

    invoke-virtual {p0, v1, v0}, Landroid/util/SparseIntArray;->put(II)V

    return-void
.end method
