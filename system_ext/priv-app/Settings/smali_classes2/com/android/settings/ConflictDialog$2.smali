.class Lcom/android/settings/ConflictDialog$2;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/ConflictDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/ConflictDialog;


# direct methods
.method constructor <init>(Lcom/android/settings/ConflictDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/ConflictDialog$2;->this$0:Lcom/android/settings/ConflictDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    iget-object p1, p0, Lcom/android/settings/ConflictDialog$2;->this$0:Lcom/android/settings/ConflictDialog;

    invoke-static {p1}, Lcom/android/settings/ConflictDialog;->-$$Nest$fgetmCallback(Lcom/android/settings/ConflictDialog;)Lcom/android/settings/CheckedCallback;

    move-result-object p1

    const/4 p2, 0x1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/ConflictDialog$2;->this$0:Lcom/android/settings/ConflictDialog;

    invoke-static {p1}, Lcom/android/settings/ConflictDialog;->-$$Nest$fgetmCallback(Lcom/android/settings/ConflictDialog;)Lcom/android/settings/CheckedCallback;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/android/settings/CheckedCallback;->onCheckResult(Z)V

    :cond_0
    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    const-string v0, "android.intent.action.MAIN"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.android.settings"

    const-string v1, "com.android.settings.SubSettings"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, ":settings:show_fragment"

    const-string v1, "com.android.settings.AodAndLockScreenSettings"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, ":android:no_headers"

    invoke-virtual {p1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object p0, p0, Lcom/android/settings/ConflictDialog$2;->this$0:Lcom/android/settings/ConflictDialog;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
