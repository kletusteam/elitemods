.class public Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;


# instance fields
.field private final mAppIcon:Landroid/widget/ImageView;

.field final mAppName:Landroid/widget/TextView;

.field final mDisabled:Landroid/widget/TextView;

.field final mSummary:Landroid/widget/TextView;

.field final mSwitch:Landroid/widget/Switch;

.field final mWidgetContainer:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    const v0, 0x1020016

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mAppName:Landroid/widget/TextView;

    const v0, 0x1020006

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mAppIcon:Landroid/widget/ImageView;

    const v0, 0x1020010

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mSummary:Landroid/widget/TextView;

    sget v0, Lcom/android/settings/R$id;->appendix:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mDisabled:Landroid/widget/TextView;

    sget v0, Lcom/android/settings/R$id;->switchWidget:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mSwitch:Landroid/widget/Switch;

    const v0, 0x1020018

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mWidgetContainer:Landroid/view/ViewGroup;

    return-void
.end method

.method static newHeader(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 3

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/android/settings/R$layout;->preference_app_header:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/view/ViewGroup;

    sget v0, Lcom/android/settings/R$id;->apps_top_intro_text:I

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-object p0
.end method

.method static newView(Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 5

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/android/settings/R$layout;->preference_app:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x1020018

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    if-eqz p1, :cond_0

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    sget v3, Lcom/android/settings/R$layout;->preference_widget_primary_switch:I

    const/4 v4, 0x1

    invoke-virtual {p1, v3, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p0

    sget p1, Lcom/android/settings/R$layout;->preference_two_target_divider:I

    invoke-virtual {p0, p1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p1

    sub-int/2addr p1, v4

    invoke-virtual {v0, p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_1

    const/16 p0, 0x8

    invoke-virtual {v1, p0}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_1
    :goto_0
    return-object v0
.end method

.method private updateSummaryVisibility()V
    .locals 1

    iget-object p0, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mSummary:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method setEnabled(Z)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iget-object p0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    goto/32 :goto_0

    nop
.end method

.method setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    goto/32 :goto_5

    nop

    :goto_0
    return-void

    :goto_1
    goto/32 :goto_2

    nop

    :goto_2
    iget-object p0, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mAppIcon:Landroid/widget/ImageView;

    goto/32 :goto_4

    nop

    :goto_3
    return-void

    :goto_4
    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/32 :goto_3

    nop

    :goto_5
    if-eqz p1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_0

    nop
.end method

.method setSummary(I)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-direct {p0}, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->updateSummaryVisibility()V

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mSummary:Landroid/widget/TextView;

    goto/32 :goto_3

    nop

    :goto_2
    return-void

    :goto_3
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    goto/32 :goto_0

    nop
.end method

.method setSummary(Ljava/lang/CharSequence;)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-direct {p0}, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->updateSummaryVisibility()V

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    iget-object v0, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mSummary:Landroid/widget/TextView;

    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_0

    nop
.end method

.method setTitle(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    goto/32 :goto_7

    nop

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_1

    nop

    :goto_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    goto/32 :goto_6

    nop

    :goto_2
    invoke-virtual {p0, p2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/32 :goto_3

    nop

    :goto_3
    return-void

    :goto_4
    iget-object p0, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mAppName:Landroid/widget/TextView;

    goto/32 :goto_2

    nop

    :goto_5
    iget-object v0, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mAppName:Landroid/widget/TextView;

    goto/32 :goto_0

    nop

    :goto_6
    if-nez p1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_8

    nop

    :goto_7
    if-eqz p1, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_a

    nop

    :goto_8
    return-void

    :goto_9
    goto/32 :goto_4

    nop

    :goto_a
    return-void

    :goto_b
    goto/32 :goto_5

    nop
.end method

.method updateDisableView(Landroid/content/pm/ApplicationInfo;)V
    .locals 2

    goto/32 :goto_8

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_1

    nop

    :goto_1
    iget p1, p1, Landroid/content/pm/ApplicationInfo;->enabledSetting:I

    goto/32 :goto_7

    nop

    :goto_2
    iget-boolean v0, p1, Landroid/content/pm/ApplicationInfo;->enabled:Z

    goto/32 :goto_0

    nop

    :goto_3
    goto :goto_18

    :goto_4
    goto/32 :goto_5

    nop

    :goto_5
    iget-object p1, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mDisabled:Landroid/widget/TextView;

    goto/32 :goto_15

    nop

    :goto_6
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/32 :goto_3

    nop

    :goto_7
    const/4 v0, 0x4

    goto/32 :goto_d

    nop

    :goto_8
    iget v0, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    goto/32 :goto_e

    nop

    :goto_9
    goto :goto_4

    :goto_a
    goto/32 :goto_11

    nop

    :goto_b
    sget p1, Lcom/android/settings/R$string;->not_installed:I

    goto/32 :goto_16

    nop

    :goto_c
    return-void

    :goto_d
    if-eq p1, v0, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_9

    nop

    :goto_e
    const/high16 v1, 0x800000

    goto/32 :goto_f

    nop

    :goto_f
    and-int/2addr v0, v1

    goto/32 :goto_19

    nop

    :goto_10
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/32 :goto_1c

    nop

    :goto_11
    iget-object p0, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mDisabled:Landroid/widget/TextView;

    goto/32 :goto_1d

    nop

    :goto_12
    iget-object p1, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mDisabled:Landroid/widget/TextView;

    goto/32 :goto_10

    nop

    :goto_13
    if-eqz v0, :cond_2

    goto/32 :goto_1b

    :cond_2
    goto/32 :goto_12

    nop

    :goto_14
    sget p1, Lcom/android/settings/R$string;->disabled:I

    goto/32 :goto_17

    nop

    :goto_15
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/32 :goto_1e

    nop

    :goto_16
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(I)V

    goto/32 :goto_1a

    nop

    :goto_17
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(I)V

    :goto_18
    goto/32 :goto_c

    nop

    :goto_19
    const/4 v1, 0x0

    goto/32 :goto_13

    nop

    :goto_1a
    goto :goto_18

    :goto_1b
    goto/32 :goto_2

    nop

    :goto_1c
    iget-object p0, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mDisabled:Landroid/widget/TextView;

    goto/32 :goto_b

    nop

    :goto_1d
    const/16 p1, 0x8

    goto/32 :goto_6

    nop

    :goto_1e
    iget-object p0, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mDisabled:Landroid/widget/TextView;

    goto/32 :goto_14

    nop
.end method

.method updateSizeText(Lcom/android/settingslib/applications/ApplicationsState$AppEntry;Ljava/lang/CharSequence;I)V
    .locals 4

    goto/32 :goto_d

    nop

    :goto_0
    goto :goto_8

    :goto_1
    goto/32 :goto_29

    nop

    :goto_2
    if-ne p3, p2, :cond_0

    goto/32 :goto_1b

    :cond_0
    goto/32 :goto_26

    nop

    :goto_3
    iget-object v0, p1, Lcom/android/settingslib/applications/ApplicationsState$AppEntry;->sizeStr:Ljava/lang/String;

    goto/32 :goto_4

    nop

    :goto_4
    if-nez v0, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_15

    nop

    :goto_5
    const-string/jumbo v1, "updateSizeText of "

    goto/32 :goto_e

    nop

    :goto_6
    cmp-long p1, v0, v2

    goto/32 :goto_a

    nop

    :goto_7
    invoke-virtual {p0, p2}, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->setSummary(Ljava/lang/CharSequence;)V

    :goto_8
    goto/32 :goto_21

    nop

    :goto_9
    invoke-virtual {p0, p1}, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->setSummary(Ljava/lang/CharSequence;)V

    goto/32 :goto_0

    nop

    :goto_a
    if-eqz p1, :cond_2

    goto/32 :goto_8

    :cond_2
    goto/32 :goto_7

    nop

    :goto_b
    if-nez v0, :cond_3

    goto/32 :goto_1f

    :cond_3
    goto/32 :goto_17

    nop

    :goto_c
    iget-object v1, p1, Lcom/android/settingslib/applications/ApplicationsState$AppEntry;->label:Ljava/lang/String;

    goto/32 :goto_28

    nop

    :goto_d
    sget-boolean v0, Lcom/android/settings/applications/manageapplications/ManageApplications;->DEBUG:Z

    goto/32 :goto_b

    nop

    :goto_e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_c

    nop

    :goto_f
    iget-object p1, p1, Lcom/android/settingslib/applications/ApplicationsState$AppEntry;->internalSizeStr:Ljava/lang/String;

    goto/32 :goto_9

    nop

    :goto_10
    goto :goto_8

    :goto_11
    goto/32 :goto_16

    nop

    :goto_12
    iget-object v1, p1, Lcom/android/settingslib/applications/ApplicationsState$AppEntry;->sizeStr:Ljava/lang/String;

    goto/32 :goto_1c

    nop

    :goto_13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_12

    nop

    :goto_14
    const-string v1, " "

    goto/32 :goto_18

    nop

    :goto_15
    const/4 p2, 0x1

    goto/32 :goto_2

    nop

    :goto_16
    iget-object p1, p1, Lcom/android/settingslib/applications/ApplicationsState$AppEntry;->externalSizeStr:Ljava/lang/String;

    goto/32 :goto_27

    nop

    :goto_17
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_25

    nop

    :goto_18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_19

    nop

    :goto_19
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_22

    nop

    :goto_1a
    goto :goto_8

    :goto_1b
    goto/32 :goto_f

    nop

    :goto_1c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_23

    nop

    :goto_1d
    const-wide/16 v2, -0x2

    goto/32 :goto_6

    nop

    :goto_1e
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1f
    goto/32 :goto_3

    nop

    :goto_20
    const-string v1, "ManageApplications"

    goto/32 :goto_1e

    nop

    :goto_21
    return-void

    :goto_22
    const-string v1, ": "

    goto/32 :goto_13

    nop

    :goto_23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_20

    nop

    :goto_24
    invoke-virtual {p0, v0}, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->setSummary(Ljava/lang/CharSequence;)V

    goto/32 :goto_10

    nop

    :goto_25
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_5

    nop

    :goto_26
    const/4 p2, 0x2

    goto/32 :goto_2a

    nop

    :goto_27
    invoke-virtual {p0, p1}, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->setSummary(Ljava/lang/CharSequence;)V

    goto/32 :goto_1a

    nop

    :goto_28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_14

    nop

    :goto_29
    iget-wide v0, p1, Lcom/android/settingslib/applications/ApplicationsState$AppEntry;->size:J

    goto/32 :goto_1d

    nop

    :goto_2a
    if-ne p3, p2, :cond_4

    goto/32 :goto_11

    :cond_4
    goto/32 :goto_24

    nop
.end method

.method updateSwitch(Landroid/widget/CompoundButton$OnCheckedChangeListener;ZZ)V
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClickable(Z)V

    goto/32 :goto_1

    nop

    :goto_1
    iget-object v0, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mSwitch:Landroid/widget/Switch;

    goto/32 :goto_d

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mWidgetContainer:Landroid/view/ViewGroup;

    goto/32 :goto_11

    nop

    :goto_3
    invoke-virtual {v0, p1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto/32 :goto_12

    nop

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_2

    nop

    :goto_5
    iget-object v0, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mSwitch:Landroid/widget/Switch;

    goto/32 :goto_4

    nop

    :goto_6
    invoke-virtual {p0, p2}, Landroid/widget/Switch;->setEnabled(Z)V

    :goto_7
    goto/32 :goto_e

    nop

    :goto_8
    invoke-virtual {p1, p3}, Landroid/widget/Switch;->setChecked(Z)V

    goto/32 :goto_f

    nop

    :goto_9
    const/4 v1, 0x0

    goto/32 :goto_b

    nop

    :goto_a
    iget-object v0, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mWidgetContainer:Landroid/view/ViewGroup;

    goto/32 :goto_0

    nop

    :goto_b
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setFocusable(Z)V

    goto/32 :goto_a

    nop

    :goto_c
    iget-object v0, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mSwitch:Landroid/widget/Switch;

    goto/32 :goto_13

    nop

    :goto_d
    const/4 v1, 0x1

    goto/32 :goto_10

    nop

    :goto_e
    return-void

    :goto_f
    iget-object p0, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mSwitch:Landroid/widget/Switch;

    goto/32 :goto_6

    nop

    :goto_10
    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setFocusable(Z)V

    goto/32 :goto_c

    nop

    :goto_11
    if-nez v0, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_9

    nop

    :goto_12
    iget-object p1, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mSwitch:Landroid/widget/Switch;

    goto/32 :goto_8

    nop

    :goto_13
    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setClickable(Z)V

    goto/32 :goto_14

    nop

    :goto_14
    iget-object v0, p0, Lcom/android/settings/applications/manageapplications/ApplicationViewHolder;->mSwitch:Landroid/widget/Switch;

    goto/32 :goto_3

    nop
.end method
