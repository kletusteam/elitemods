.class public Lcom/android/settings/applications/OpenSupportedLinks;
.super Lcom/android/settings/applications/AppInfoWithHeader;

# interfaces
.implements Lcom/android/settingslib/widget/SelectorWithWidgetPreference$OnClickListener;


# instance fields
.field mAllowOpening:Lcom/android/settingslib/widget/SelectorWithWidgetPreference;

.field mAskEveryTime:Lcom/android/settingslib/widget/SelectorWithWidgetPreference;

.field private mCurrentIndex:I

.field mNotAllowed:Lcom/android/settingslib/widget/SelectorWithWidgetPreference;

.field mPackageManager:Landroid/content/pm/PackageManager;

.field mPreferenceCategory:Landroidx/preference/PreferenceCategory;

.field private mRadioKeys:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/applications/AppInfoWithHeader;-><init>()V

    const-string v0, "app_link_open_always"

    const-string v1, "app_link_open_ask"

    const-string v2, "app_link_open_never"

    filled-new-array {v0, v1, v2}, [Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/OpenSupportedLinks;->mRadioKeys:[Ljava/lang/String;

    return-void
.end method

.method private indexToLinkState(I)I
    .locals 0

    const/4 p0, 0x2

    if-eqz p1, :cond_1

    if-eq p1, p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x3

    :cond_1
    return p0
.end method

.method private linkStateToIndex(I)I
    .locals 1

    const/4 p0, 0x2

    if-eq p1, p0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 p0, 0x1

    :cond_0
    return p0

    :cond_1
    const/4 p0, 0x0

    return p0
.end method

.method private makeRadioPreference(Ljava/lang/String;I)Lcom/android/settingslib/widget/SelectorWithWidgetPreference;
    .locals 2

    new-instance v0, Lcom/android/settingslib/widget/SelectorWithWidgetPreference;

    iget-object v1, p0, Lcom/android/settings/applications/OpenSupportedLinks;->mPreferenceCategory:Landroidx/preference/PreferenceCategory;

    invoke-virtual {v1}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settingslib/widget/SelectorWithWidgetPreference;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroidx/preference/Preference;->setKey(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Landroidx/preference/Preference;->setTitle(I)V

    invoke-virtual {v0, p0}, Lcom/android/settingslib/widget/SelectorWithWidgetPreference;->setOnClickListener(Lcom/android/settingslib/widget/SelectorWithWidgetPreference$OnClickListener;)V

    iget-object p0, p0, Lcom/android/settings/applications/OpenSupportedLinks;->mPreferenceCategory:Landroidx/preference/PreferenceCategory;

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    return-object v0
.end method

.method private preferenceKeyToIndex(Ljava/lang/String;)I
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/settings/applications/OpenSupportedLinks;->mRadioKeys:[Ljava/lang/String;

    array-length v2, v1

    if-ge v0, v2, :cond_1

    aget-object v1, v1, v0

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x1

    return p0
.end method

.method private setRadioStatus(I)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/applications/OpenSupportedLinks;->mAllowOpening:Lcom/android/settingslib/widget/SelectorWithWidgetPreference;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez p1, :cond_0

    move v3, v2

    goto :goto_0

    :cond_0
    move v3, v1

    :goto_0
    invoke-virtual {v0, v3}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/applications/OpenSupportedLinks;->mAskEveryTime:Lcom/android/settingslib/widget/SelectorWithWidgetPreference;

    if-ne p1, v2, :cond_1

    move v3, v2

    goto :goto_1

    :cond_1
    move v3, v1

    :goto_1
    invoke-virtual {v0, v3}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p0, p0, Lcom/android/settings/applications/OpenSupportedLinks;->mNotAllowed:Lcom/android/settingslib/widget/SelectorWithWidgetPreference;

    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    move v1, v2

    :cond_2
    invoke-virtual {p0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    return-void
.end method

.method private updateAppLinkState(I)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/applications/OpenSupportedLinks;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/android/settings/applications/AppInfoBase;->mPackageName:Ljava/lang/String;

    iget v2, p0, Lcom/android/settings/applications/AppInfoBase;->mUserId:I

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getIntentVerificationStatusAsUser(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/OpenSupportedLinks;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/android/settings/applications/AppInfoBase;->mPackageName:Ljava/lang/String;

    iget v2, p0, Lcom/android/settings/applications/AppInfoBase;->mUserId:I

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/pm/PackageManager;->updateIntentVerificationStatusAsUser(Ljava/lang/String;II)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/android/settings/applications/OpenSupportedLinks;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v0, p0, Lcom/android/settings/applications/AppInfoBase;->mPackageName:Ljava/lang/String;

    iget p0, p0, Lcom/android/settings/applications/AppInfoBase;->mUserId:I

    invoke-virtual {p1, v0, p0}, Landroid/content/pm/PackageManager;->getIntentVerificationStatusAsUser(Ljava/lang/String;I)I

    goto :goto_0

    :cond_1
    const-string p0, "OpenSupportedLinks"

    const-string p1, "Couldn\'t update intent verification status!"

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method private updateFooterPreference()V
    .locals 1

    const-string/jumbo v0, "supported_links_footer"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/widget/FooterPreference;

    if-nez v0, :cond_0

    const-string p0, "OpenSupportedLinks"

    const-string v0, "Can\'t find the footer preference."

    invoke-static {p0, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/settings/applications/OpenSupportedLinks;->addLinksToFooter(Lcom/android/settingslib/widget/FooterPreference;)V

    return-void
.end method


# virtual methods
.method addLinksToFooter(Lcom/android/settingslib/widget/FooterPreference;)V
    .locals 3

    goto/32 :goto_e

    nop

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_3

    nop

    :goto_2
    return-void

    :goto_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_1d

    nop

    :goto_4
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_5
    invoke-virtual {p1}, Landroidx/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    goto/32 :goto_1f

    nop

    :goto_6
    invoke-static {}, Ljava/lang/System;->lineSeparator()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_7
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_16

    nop

    :goto_8
    invoke-static {}, Ljava/lang/System;->lineSeparator()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_7

    nop

    :goto_a
    if-nez v0, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_1b

    nop

    :goto_b
    return-void

    :goto_c
    goto/32 :goto_14

    nop

    :goto_d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_5

    nop

    :goto_e
    iget-object v0, p0, Lcom/android/settings/applications/OpenSupportedLinks;->mPackageManager:Landroid/content/pm/PackageManager;

    goto/32 :goto_12

    nop

    :goto_f
    invoke-static {v0, p0}, Lcom/android/settings/Utils;->getHandledDomains(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/util/ArraySet;

    move-result-object p0

    goto/32 :goto_19

    nop

    :goto_10
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_1c

    nop

    :goto_11
    const-string p1, "Can\'t find any app links."

    goto/32 :goto_13

    nop

    :goto_12
    iget-object p0, p0, Lcom/android/settings/applications/AppInfoBase;->mPackageName:Ljava/lang/String;

    goto/32 :goto_f

    nop

    :goto_13
    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_b

    nop

    :goto_14
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_d

    nop

    :goto_15
    check-cast v1, Ljava/lang/String;

    goto/32 :goto_20

    nop

    :goto_16
    invoke-virtual {p0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_17
    goto/32 :goto_10

    nop

    :goto_18
    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    goto/32 :goto_2

    nop

    :goto_19
    invoke-virtual {p0}, Landroid/util/ArraySet;->isEmpty()Z

    move-result v0

    goto/32 :goto_a

    nop

    :goto_1a
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_15

    nop

    :goto_1b
    const-string p0, "OpenSupportedLinks"

    goto/32 :goto_11

    nop

    :goto_1c
    if-nez v1, :cond_1

    goto/32 :goto_1e

    :cond_1
    goto/32 :goto_1a

    nop

    :goto_1d
    goto :goto_17

    :goto_1e
    goto/32 :goto_18

    nop

    :goto_1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_8

    nop

    :goto_20
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_21

    nop

    :goto_21
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_4

    nop
.end method

.method protected createDialog(II)Lmiuix/appcompat/app/AlertDialog;
    .locals 0

    const/4 p0, 0x0

    return-object p0
.end method

.method getEntriesNo()I
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0}, Landroid/util/ArraySet;->size()I

    move-result p0

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Lcom/android/settings/applications/OpenSupportedLinks;->mPackageManager:Landroid/content/pm/PackageManager;

    goto/32 :goto_3

    nop

    :goto_2
    return p0

    :goto_3
    iget-object p0, p0, Lcom/android/settings/applications/AppInfoBase;->mPackageName:Ljava/lang/String;

    goto/32 :goto_4

    nop

    :goto_4
    invoke-static {v0, p0}, Lcom/android/settings/Utils;->getHandledDomains(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/util/ArraySet;

    move-result-object p0

    goto/32 :goto_0

    nop
.end method

.method public getMetricsCategory()I
    .locals 0

    const/16 p0, 0x720

    return p0
.end method

.method initRadioPreferencesGroup()V
    .locals 7

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    goto/32 :goto_c

    nop

    :goto_1
    const-string/jumbo v0, "supported_links_radio_group"

    goto/32 :goto_7

    nop

    :goto_2
    invoke-direct {p0, v0}, Lcom/android/settings/applications/OpenSupportedLinks;->setRadioStatus(I)V

    goto/32 :goto_6

    nop

    :goto_3
    iput-object v0, p0, Lcom/android/settings/applications/OpenSupportedLinks;->mAllowOpening:Lcom/android/settingslib/widget/SelectorWithWidgetPreference;

    goto/32 :goto_15

    nop

    :goto_4
    check-cast v0, Landroidx/preference/PreferenceCategory;

    goto/32 :goto_1f

    nop

    :goto_5
    const-string v1, "app_link_open_ask"

    goto/32 :goto_20

    nop

    :goto_6
    return-void

    :goto_7
    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_8
    sget v0, Lcom/android/settings/R$string;->app_link_open_always:I

    goto/32 :goto_f

    nop

    :goto_9
    iget-object v1, p0, Lcom/android/settings/applications/OpenSupportedLinks;->mAllowOpening:Lcom/android/settingslib/widget/SelectorWithWidgetPreference;

    goto/32 :goto_0

    nop

    :goto_a
    invoke-direct {p0, v0}, Lcom/android/settings/applications/OpenSupportedLinks;->linkStateToIndex(I)I

    move-result v0

    goto/32 :goto_1c

    nop

    :goto_b
    iget-object v1, p0, Lcom/android/settings/applications/AppInfoBase;->mPackageName:Ljava/lang/String;

    goto/32 :goto_d

    nop

    :goto_c
    sget v3, Lcom/android/settings/R$plurals;->app_link_open_always_summary:I

    goto/32 :goto_16

    nop

    :goto_d
    iget v2, p0, Lcom/android/settings/applications/AppInfoBase;->mUserId:I

    goto/32 :goto_10

    nop

    :goto_e
    aput-object v5, v4, v6

    goto/32 :goto_1d

    nop

    :goto_f
    const-string v1, "app_link_open_always"

    goto/32 :goto_22

    nop

    :goto_10
    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getIntentVerificationStatusAsUser(Ljava/lang/String;I)I

    move-result v0

    goto/32 :goto_a

    nop

    :goto_11
    new-array v4, v4, [Ljava/lang/Object;

    goto/32 :goto_13

    nop

    :goto_12
    sget v0, Lcom/android/settings/R$string;->app_link_open_ask:I

    goto/32 :goto_5

    nop

    :goto_13
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto/32 :goto_25

    nop

    :goto_14
    invoke-virtual {v1, v0}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/32 :goto_12

    nop

    :goto_15
    invoke-virtual {p0}, Lcom/android/settings/applications/OpenSupportedLinks;->getEntriesNo()I

    move-result v0

    goto/32 :goto_19

    nop

    :goto_16
    const/4 v4, 0x1

    goto/32 :goto_11

    nop

    :goto_17
    iput-object v0, p0, Lcom/android/settings/applications/OpenSupportedLinks;->mAskEveryTime:Lcom/android/settingslib/widget/SelectorWithWidgetPreference;

    goto/32 :goto_21

    nop

    :goto_18
    invoke-virtual {v1, v2}, Lcom/android/settingslib/widget/SelectorWithWidgetPreference;->setAppendixVisibility(I)V

    goto/32 :goto_9

    nop

    :goto_19
    iget-object v1, p0, Lcom/android/settings/applications/OpenSupportedLinks;->mAllowOpening:Lcom/android/settingslib/widget/SelectorWithWidgetPreference;

    goto/32 :goto_1a

    nop

    :goto_1a
    const/16 v2, 0x8

    goto/32 :goto_18

    nop

    :goto_1b
    invoke-direct {p0, v1, v0}, Lcom/android/settings/applications/OpenSupportedLinks;->makeRadioPreference(Ljava/lang/String;I)Lcom/android/settingslib/widget/SelectorWithWidgetPreference;

    move-result-object v0

    goto/32 :goto_1e

    nop

    :goto_1c
    iput v0, p0, Lcom/android/settings/applications/OpenSupportedLinks;->mCurrentIndex:I

    goto/32 :goto_2

    nop

    :goto_1d
    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_14

    nop

    :goto_1e
    iput-object v0, p0, Lcom/android/settings/applications/OpenSupportedLinks;->mNotAllowed:Lcom/android/settingslib/widget/SelectorWithWidgetPreference;

    goto/32 :goto_24

    nop

    :goto_1f
    iput-object v0, p0, Lcom/android/settings/applications/OpenSupportedLinks;->mPreferenceCategory:Landroidx/preference/PreferenceCategory;

    goto/32 :goto_8

    nop

    :goto_20
    invoke-direct {p0, v1, v0}, Lcom/android/settings/applications/OpenSupportedLinks;->makeRadioPreference(Ljava/lang/String;I)Lcom/android/settingslib/widget/SelectorWithWidgetPreference;

    move-result-object v0

    goto/32 :goto_17

    nop

    :goto_21
    sget v0, Lcom/android/settings/R$string;->app_link_open_never:I

    goto/32 :goto_23

    nop

    :goto_22
    invoke-direct {p0, v1, v0}, Lcom/android/settings/applications/OpenSupportedLinks;->makeRadioPreference(Ljava/lang/String;I)Lcom/android/settingslib/widget/SelectorWithWidgetPreference;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_23
    const-string v1, "app_link_open_never"

    goto/32 :goto_1b

    nop

    :goto_24
    iget-object v0, p0, Lcom/android/settings/applications/OpenSupportedLinks;->mPackageManager:Landroid/content/pm/PackageManager;

    goto/32 :goto_b

    nop

    :goto_25
    const/4 v6, 0x0

    goto/32 :goto_e

    nop
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/applications/AppInfoBase;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/applications/OpenSupportedLinks;->mPackageManager:Landroid/content/pm/PackageManager;

    sget p1, Lcom/android/settings/R$xml;->open_supported_links:I

    invoke-virtual {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/settings/applications/OpenSupportedLinks;->initRadioPreferencesGroup()V

    invoke-direct {p0}, Lcom/android/settings/applications/OpenSupportedLinks;->updateFooterPreference()V

    return-void
.end method

.method public onRadioButtonClicked(Lcom/android/settingslib/widget/SelectorWithWidgetPreference;)V
    .locals 1

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/android/settings/applications/OpenSupportedLinks;->preferenceKeyToIndex(Ljava/lang/String;)I

    move-result p1

    iget v0, p0, Lcom/android/settings/applications/OpenSupportedLinks;->mCurrentIndex:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/android/settings/applications/OpenSupportedLinks;->mCurrentIndex:I

    invoke-direct {p0, p1}, Lcom/android/settings/applications/OpenSupportedLinks;->setRadioStatus(I)V

    iget p1, p0, Lcom/android/settings/applications/OpenSupportedLinks;->mCurrentIndex:I

    invoke-direct {p0, p1}, Lcom/android/settings/applications/OpenSupportedLinks;->indexToLinkState(I)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/applications/OpenSupportedLinks;->updateAppLinkState(I)V

    :cond_0
    return-void
.end method

.method protected refreshUi()Z
    .locals 0

    const/4 p0, 0x1

    return p0
.end method
