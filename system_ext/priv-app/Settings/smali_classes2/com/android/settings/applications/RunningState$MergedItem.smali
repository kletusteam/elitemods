.class Lcom/android/settings/applications/RunningState$MergedItem;
.super Lcom/android/settings/applications/RunningState$BaseItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/applications/RunningState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MergedItem"
.end annotation


# instance fields
.field final mChildren:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/applications/RunningState$MergedItem;",
            ">;"
        }
    .end annotation
.end field

.field private mLastNumProcesses:I

.field private mLastNumServices:I

.field final mOtherProcesses:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/applications/RunningState$ProcessItem;",
            ">;"
        }
    .end annotation
.end field

.field mProcess:Lcom/android/settings/applications/RunningState$ProcessItem;

.field final mServices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/applications/RunningState$ServiceItem;",
            ">;"
        }
    .end annotation
.end field

.field mUser:Lcom/android/settings/applications/RunningState$UserState;


# direct methods
.method constructor <init>(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/android/settings/applications/RunningState$BaseItem;-><init>(ZI)V

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mOtherProcesses:Ljava/util/ArrayList;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mServices:Ljava/util/ArrayList;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mChildren:Ljava/util/ArrayList;

    const/4 p1, -0x1

    iput p1, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mLastNumProcesses:I

    iput p1, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mLastNumServices:I

    return-void
.end method

.method private setDescription(Landroid/content/Context;II)V
    .locals 4

    iget v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mLastNumProcesses:I

    if-ne v0, p2, :cond_0

    iget v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mLastNumServices:I

    if-eq v0, p3, :cond_4

    :cond_0
    iput p2, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mLastNumProcesses:I

    iput p3, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mLastNumServices:I

    sget v0, Lcom/android/settings/R$string;->running_processes_item_description_s_s:I

    const/4 v1, 0x1

    if-eq p2, v1, :cond_2

    if-eq p3, v1, :cond_1

    sget v0, Lcom/android/settings/R$string;->running_processes_item_description_p_p:I

    goto :goto_0

    :cond_1
    sget v0, Lcom/android/settings/R$string;->running_processes_item_description_p_s:I

    goto :goto_0

    :cond_2
    if-eq p3, v1, :cond_3

    sget v0, Lcom/android/settings/R$string;->running_processes_item_description_s_p:I

    :cond_3
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v2, v3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v2, v1

    invoke-virtual {p1, v0, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mDescription:Ljava/lang/String;

    :cond_4
    return-void
.end method


# virtual methods
.method update(Landroid/content/Context;Z)Z
    .locals 10

    goto/32 :goto_54

    nop

    :goto_0
    iput-wide v6, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mActiveSince:J

    :goto_1
    goto/32 :goto_3c

    nop

    :goto_2
    goto :goto_11

    :goto_3
    goto/32 :goto_1d

    nop

    :goto_4
    if-eqz p2, :cond_0

    goto/32 :goto_11

    :cond_0
    goto/32 :goto_41

    nop

    :goto_5
    iget-wide v6, v4, Lcom/android/settings/applications/RunningState$BaseItem;->mActiveSince:J

    goto/32 :goto_16

    nop

    :goto_6
    iget-wide v8, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mActiveSince:J

    goto/32 :goto_8

    nop

    :goto_7
    add-int/2addr p2, v0

    goto/32 :goto_38

    nop

    :goto_8
    cmp-long v4, v8, v6

    goto/32 :goto_5c

    nop

    :goto_9
    add-int/2addr v0, v6

    goto/32 :goto_18

    nop

    :goto_a
    iget-object v6, v0, Lcom/android/settings/applications/RunningState$BaseItem;->mPackageInfo:Landroid/content/pm/PackageItemInfo;

    goto/32 :goto_53

    nop

    :goto_b
    move v0, p2

    goto/32 :goto_46

    nop

    :goto_c
    check-cast p2, Lcom/android/settings/applications/RunningState$MergedItem;

    goto/32 :goto_45

    nop

    :goto_d
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    goto/32 :goto_37

    nop

    :goto_e
    iget-object p2, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mChildren:Ljava/util/ArrayList;

    goto/32 :goto_42

    nop

    :goto_f
    iput-object p2, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mPackageInfo:Landroid/content/pm/PackageItemInfo;

    goto/32 :goto_5a

    nop

    :goto_10
    goto/16 :goto_4e

    :goto_11
    goto/32 :goto_59

    nop

    :goto_12
    iget-object p2, p2, Lcom/android/settings/applications/RunningState$UserState;->mLabel:Ljava/lang/String;

    goto/32 :goto_3a

    nop

    :goto_13
    iget-object p2, p2, Lcom/android/settings/applications/RunningState$BaseItem;->mPackageInfo:Landroid/content/pm/PackageItemInfo;

    goto/32 :goto_f

    nop

    :goto_14
    iput-wide v3, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mActiveSince:J

    :goto_15
    goto/32 :goto_43

    nop

    :goto_16
    cmp-long v4, v6, v1

    goto/32 :goto_28

    nop

    :goto_17
    iput-object v6, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mDisplayLabel:Ljava/lang/CharSequence;

    goto/32 :goto_22

    nop

    :goto_18
    iget v6, v4, Lcom/android/settings/applications/RunningState$MergedItem;->mLastNumServices:I

    goto/32 :goto_40

    nop

    :goto_19
    if-ltz p2, :cond_1

    goto/32 :goto_15

    :cond_1
    goto/32 :goto_14

    nop

    :goto_1a
    iget-boolean p2, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mBackground:Z

    goto/32 :goto_4

    nop

    :goto_1b
    move p2, v5

    goto/32 :goto_b

    nop

    :goto_1c
    iget-object v4, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mChildren:Ljava/util/ArrayList;

    goto/32 :goto_51

    nop

    :goto_1d
    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mProcess:Lcom/android/settings/applications/RunningState$ProcessItem;

    goto/32 :goto_a

    nop

    :goto_1e
    iput-object p2, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mDisplayLabel:Ljava/lang/CharSequence;

    goto/32 :goto_2d

    nop

    :goto_1f
    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/applications/RunningState$MergedItem;->setDescription(Landroid/content/Context;II)V

    :goto_20
    goto/32 :goto_34

    nop

    :goto_21
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p2

    goto/32 :goto_5d

    nop

    :goto_22
    iget-object v6, v0, Lcom/android/settings/applications/RunningState$BaseItem;->mLabel:Ljava/lang/String;

    goto/32 :goto_33

    nop

    :goto_23
    iget-object v6, v0, Lcom/android/settings/applications/RunningState$BaseItem;->mDisplayLabel:Ljava/lang/CharSequence;

    goto/32 :goto_17

    nop

    :goto_24
    cmp-long p2, v6, v3

    goto/32 :goto_19

    nop

    :goto_25
    const/4 p2, 0x0

    :goto_26
    goto/32 :goto_44

    nop

    :goto_27
    iget p2, v0, Lcom/android/settings/applications/RunningState$ProcessItem;->mPid:I

    goto/32 :goto_49

    nop

    :goto_28
    if-gez v4, :cond_2

    goto/32 :goto_1

    :cond_2
    goto/32 :goto_6

    nop

    :goto_29
    if-lt p2, v4, :cond_3

    goto/32 :goto_4c

    :cond_3
    goto/32 :goto_1c

    nop

    :goto_2a
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_1f

    nop

    :goto_2b
    const/4 p2, 0x1

    goto/32 :goto_55

    nop

    :goto_2c
    const/4 v5, 0x0

    goto/32 :goto_31

    nop

    :goto_2d
    iput-wide v3, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mActiveSince:J

    goto/32 :goto_1b

    nop

    :goto_2e
    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mOtherProcesses:Ljava/util/ArrayList;

    goto/32 :goto_36

    nop

    :goto_2f
    move p2, v5

    :goto_30
    goto/32 :goto_2e

    nop

    :goto_31
    if-nez v0, :cond_4

    goto/32 :goto_3

    :cond_4
    goto/32 :goto_e

    nop

    :goto_32
    iget v6, v4, Lcom/android/settings/applications/RunningState$MergedItem;->mLastNumProcesses:I

    goto/32 :goto_9

    nop

    :goto_33
    iput-object v6, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mLabel:Ljava/lang/String;

    goto/32 :goto_39

    nop

    :goto_34
    iput-wide v3, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mActiveSince:J

    goto/32 :goto_4d

    nop

    :goto_35
    const-wide/16 v3, -0x1

    goto/32 :goto_2c

    nop

    :goto_36
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_7

    nop

    :goto_37
    check-cast p2, Lcom/android/settings/applications/RunningState$ServiceItem;

    goto/32 :goto_3f

    nop

    :goto_38
    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mServices:Ljava/util/ArrayList;

    goto/32 :goto_2a

    nop

    :goto_39
    if-eqz p2, :cond_5

    goto/32 :goto_20

    :cond_5
    goto/32 :goto_27

    nop

    :goto_3a
    goto :goto_26

    :goto_3b
    goto/32 :goto_25

    nop

    :goto_3c
    add-int/lit8 p2, p2, 0x1

    goto/32 :goto_4b

    nop

    :goto_3d
    if-nez p2, :cond_6

    goto/32 :goto_3b

    :cond_6
    goto/32 :goto_12

    nop

    :goto_3e
    iget-wide v6, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mActiveSince:J

    goto/32 :goto_24

    nop

    :goto_3f
    iget-wide v3, p2, Lcom/android/settings/applications/RunningState$BaseItem;->mActiveSince:J

    goto/32 :goto_4f

    nop

    :goto_40
    add-int/2addr v3, v6

    goto/32 :goto_5

    nop

    :goto_41
    invoke-direct {p0, p1, v0, v3}, Lcom/android/settings/applications/RunningState$MergedItem;->setDescription(Landroid/content/Context;II)V

    goto/32 :goto_2

    nop

    :goto_42
    invoke-virtual {p2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    goto/32 :goto_c

    nop

    :goto_43
    add-int/lit8 p1, p1, 0x1

    goto/32 :goto_10

    nop

    :goto_44
    iput-object p2, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mLabel:Ljava/lang/String;

    goto/32 :goto_1e

    nop

    :goto_45
    iget-object p2, p2, Lcom/android/settings/applications/RunningState$MergedItem;->mProcess:Lcom/android/settings/applications/RunningState$ProcessItem;

    goto/32 :goto_13

    nop

    :goto_46
    move v3, v0

    :goto_47
    goto/32 :goto_5b

    nop

    :goto_48
    iget-object p2, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mServices:Ljava/util/ArrayList;

    goto/32 :goto_d

    nop

    :goto_49
    if-gtz p2, :cond_7

    goto/32 :goto_56

    :cond_7
    goto/32 :goto_2b

    nop

    :goto_4a
    const-wide/16 v1, 0x0

    goto/32 :goto_35

    nop

    :goto_4b
    goto :goto_47

    :goto_4c
    goto/32 :goto_1a

    nop

    :goto_4d
    move p1, v5

    :goto_4e
    goto/32 :goto_50

    nop

    :goto_4f
    cmp-long p2, v3, v1

    goto/32 :goto_5e

    nop

    :goto_50
    iget-object p2, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mServices:Ljava/util/ArrayList;

    goto/32 :goto_21

    nop

    :goto_51
    invoke-virtual {v4, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_52

    nop

    :goto_52
    check-cast v4, Lcom/android/settings/applications/RunningState$MergedItem;

    goto/32 :goto_32

    nop

    :goto_53
    iput-object v6, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mPackageInfo:Landroid/content/pm/PackageItemInfo;

    goto/32 :goto_23

    nop

    :goto_54
    iput-boolean p2, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mBackground:Z

    goto/32 :goto_57

    nop

    :goto_55
    goto/16 :goto_30

    :goto_56
    goto/32 :goto_2f

    nop

    :goto_57
    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mUser:Lcom/android/settings/applications/RunningState$UserState;

    goto/32 :goto_4a

    nop

    :goto_58
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    goto/32 :goto_29

    nop

    :goto_59
    return v5

    :goto_5a
    iget-object p2, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mUser:Lcom/android/settings/applications/RunningState$UserState;

    goto/32 :goto_3d

    nop

    :goto_5b
    iget-object v4, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mChildren:Ljava/util/ArrayList;

    goto/32 :goto_58

    nop

    :goto_5c
    if-ltz v4, :cond_8

    goto/32 :goto_1

    :cond_8
    goto/32 :goto_0

    nop

    :goto_5d
    if-lt p1, p2, :cond_9

    goto/32 :goto_11

    :cond_9
    goto/32 :goto_48

    nop

    :goto_5e
    if-gez p2, :cond_a

    goto/32 :goto_15

    :cond_a
    goto/32 :goto_3e

    nop
.end method

.method updateSize(Landroid/content/Context;)Z
    .locals 7

    goto/32 :goto_4

    nop

    :goto_0
    iget-object v2, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mChildren:Ljava/util/ArrayList;

    goto/32 :goto_17

    nop

    :goto_1
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_15

    nop

    :goto_2
    add-long/2addr v3, v5

    goto/32 :goto_11

    nop

    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_24

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mUser:Lcom/android/settings/applications/RunningState$UserState;

    goto/32 :goto_2e

    nop

    :goto_5
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_1f

    nop

    :goto_6
    iget-object v2, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mOtherProcesses:Ljava/util/ArrayList;

    goto/32 :goto_27

    nop

    :goto_7
    iput-wide v2, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mSize:J

    goto/32 :goto_22

    nop

    :goto_8
    if-nez v0, :cond_0

    goto/32 :goto_2d

    :cond_0
    goto/32 :goto_1c

    nop

    :goto_9
    if-lt v0, v2, :cond_1

    goto/32 :goto_25

    :cond_1
    goto/32 :goto_0

    nop

    :goto_a
    iput-object p1, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mSizeStr:Ljava/lang/String;

    :goto_b
    goto/32 :goto_1b

    nop

    :goto_c
    iput-wide v2, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mSize:J

    goto/32 :goto_d

    nop

    :goto_d
    move v0, v1

    :goto_e
    goto/32 :goto_6

    nop

    :goto_f
    invoke-virtual {v2, p1}, Lcom/android/settings/applications/RunningState$MergedItem;->updateSize(Landroid/content/Context;)Z

    goto/32 :goto_14

    nop

    :goto_10
    iget-object v2, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mChildren:Ljava/util/ArrayList;

    goto/32 :goto_2a

    nop

    :goto_11
    iput-wide v3, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mSize:J

    goto/32 :goto_13

    nop

    :goto_12
    if-lt v0, v2, :cond_2

    goto/32 :goto_25

    :cond_2
    goto/32 :goto_1a

    nop

    :goto_13
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_2c

    nop

    :goto_14
    iget-wide v3, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mSize:J

    goto/32 :goto_29

    nop

    :goto_15
    if-eqz v0, :cond_3

    goto/32 :goto_b

    :cond_3
    goto/32 :goto_a

    nop

    :goto_16
    iget-wide v2, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mSize:J

    goto/32 :goto_1e

    nop

    :goto_17
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_26

    nop

    :goto_18
    iget-object v0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mProcess:Lcom/android/settings/applications/RunningState$ProcessItem;

    goto/32 :goto_20

    nop

    :goto_19
    iget-object v4, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mOtherProcesses:Ljava/util/ArrayList;

    goto/32 :goto_5

    nop

    :goto_1a
    iget-wide v2, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mSize:J

    goto/32 :goto_19

    nop

    :goto_1b
    return v1

    :goto_1c
    const-wide/16 v2, 0x0

    goto/32 :goto_7

    nop

    :goto_1d
    iput-wide v2, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mSize:J

    goto/32 :goto_3

    nop

    :goto_1e
    invoke-static {p1, v2, v3}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_28

    nop

    :goto_1f
    check-cast v4, Lcom/android/settings/applications/RunningState$ProcessItem;

    goto/32 :goto_2b

    nop

    :goto_20
    iget-wide v2, v0, Lcom/android/settings/applications/RunningState$BaseItem;->mSize:J

    goto/32 :goto_c

    nop

    :goto_21
    add-long/2addr v2, v4

    goto/32 :goto_1d

    nop

    :goto_22
    move v0, v1

    :goto_23
    goto/32 :goto_10

    nop

    :goto_24
    goto :goto_e

    :goto_25
    goto/32 :goto_16

    nop

    :goto_26
    check-cast v2, Lcom/android/settings/applications/RunningState$MergedItem;

    goto/32 :goto_f

    nop

    :goto_27
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    goto/32 :goto_12

    nop

    :goto_28
    iget-object v0, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mSizeStr:Ljava/lang/String;

    goto/32 :goto_1

    nop

    :goto_29
    iget-wide v5, v2, Lcom/android/settings/applications/RunningState$BaseItem;->mSize:J

    goto/32 :goto_2

    nop

    :goto_2a
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    goto/32 :goto_9

    nop

    :goto_2b
    iget-wide v4, v4, Lcom/android/settings/applications/RunningState$BaseItem;->mSize:J

    goto/32 :goto_21

    nop

    :goto_2c
    goto :goto_23

    :goto_2d
    goto/32 :goto_18

    nop

    :goto_2e
    const/4 v1, 0x0

    goto/32 :goto_8

    nop
.end method
