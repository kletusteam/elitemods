.class public Lcom/android/settings/applications/RunningProcessesView$ActiveItem;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/applications/RunningProcessesView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ActiveItem"
.end annotation


# instance fields
.field mFirstRunTime:J

.field mHolder:Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

.field mItem:Lcom/android/settings/applications/RunningState$BaseItem;

.field mRootView:Landroid/view/View;

.field mSetBackground:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method updateTime(Landroid/content/Context;Ljava/lang/StringBuilder;)V
    .locals 8

    goto/32 :goto_10

    nop

    :goto_0
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_35

    nop

    :goto_1
    iget-object v0, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->mHolder:Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

    goto/32 :goto_1b

    nop

    :goto_2
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_15

    nop

    :goto_3
    return-void

    :goto_4
    instance-of p2, p0, Lcom/android/settings/applications/RunningState$MergedItem;

    goto/32 :goto_a

    nop

    :goto_5
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result p0

    goto/32 :goto_2b

    nop

    :goto_6
    iget-wide v4, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->mFirstRunTime:J

    goto/32 :goto_25

    nop

    :goto_7
    move v2, v1

    :goto_8
    goto/32 :goto_48

    nop

    :goto_9
    iget-object v0, v0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->size:Landroid/widget/TextView;

    goto/32 :goto_2e

    nop

    :goto_a
    if-nez p2, :cond_0

    goto/32 :goto_49

    :cond_0
    goto/32 :goto_24

    nop

    :goto_b
    iget-object v0, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->mHolder:Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

    goto/32 :goto_9

    nop

    :goto_c
    iget-object p0, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->mItem:Lcom/android/settings/applications/RunningState$BaseItem;

    goto/32 :goto_4

    nop

    :goto_d
    instance-of v1, v0, Lcom/android/settings/applications/RunningState$ServiceItem;

    goto/32 :goto_4c

    nop

    :goto_e
    if-nez v0, :cond_1

    goto/32 :goto_31

    :cond_1
    goto/32 :goto_3b

    nop

    :goto_f
    iget-object v0, v0, Lcom/android/settings/applications/RunningState$BaseItem;->mCurSizeStr:Ljava/lang/String;

    goto/32 :goto_13

    nop

    :goto_10
    iget-object v0, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->mItem:Lcom/android/settings/applications/RunningState$BaseItem;

    goto/32 :goto_d

    nop

    :goto_11
    if-eqz v0, :cond_2

    goto/32 :goto_2f

    :cond_2
    goto/32 :goto_1e

    nop

    :goto_12
    invoke-static {p2, v1, v2}, Landroid/text/format/DateUtils;->formatElapsedTime(Ljava/lang/StringBuilder;J)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_14

    nop

    :goto_13
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_11

    nop

    :goto_14
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_4e

    nop

    :goto_15
    goto/16 :goto_31

    :goto_16
    goto/32 :goto_30

    nop

    :goto_17
    iget-object v1, v0, Lcom/android/settings/applications/RunningState$BaseItem;->mSizeStr:Ljava/lang/String;

    goto/32 :goto_3e

    nop

    :goto_18
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    goto/32 :goto_34

    nop

    :goto_19
    goto/16 :goto_38

    :goto_1a
    goto/32 :goto_37

    nop

    :goto_1b
    iget-object v0, v0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->uptime:Landroid/widget/TextView;

    goto/32 :goto_0

    nop

    :goto_1c
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    goto/32 :goto_47

    nop

    :goto_1d
    cmp-long v4, v4, v6

    goto/32 :goto_43

    nop

    :goto_1e
    iget-object v0, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->mItem:Lcom/android/settings/applications/RunningState$BaseItem;

    goto/32 :goto_26

    nop

    :goto_1f
    if-nez v0, :cond_3

    goto/32 :goto_3a

    :cond_3
    goto/32 :goto_3c

    nop

    :goto_20
    iput-boolean v1, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->mSetBackground:Z

    goto/32 :goto_6

    nop

    :goto_21
    iget-object v0, v0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->size:Landroid/widget/TextView;

    goto/32 :goto_41

    nop

    :goto_22
    const-string v3, ""

    goto/32 :goto_4b

    nop

    :goto_23
    if-eqz v0, :cond_4

    goto/32 :goto_3a

    :cond_4
    goto/32 :goto_2a

    nop

    :goto_24
    check-cast p0, Lcom/android/settings/applications/RunningState$MergedItem;

    goto/32 :goto_2d

    nop

    :goto_25
    const-wide/16 v6, 0x0

    goto/32 :goto_1d

    nop

    :goto_26
    iput-object v1, v0, Lcom/android/settings/applications/RunningState$BaseItem;->mCurSizeStr:Ljava/lang/String;

    goto/32 :goto_b

    nop

    :goto_27
    iget-boolean v0, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->mSetBackground:Z

    goto/32 :goto_23

    nop

    :goto_28
    div-long/2addr v1, p0

    goto/32 :goto_12

    nop

    :goto_29
    if-nez v1, :cond_5

    goto/32 :goto_16

    :cond_5
    goto/32 :goto_1c

    nop

    :goto_2a
    iput-boolean v2, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->mSetBackground:Z

    goto/32 :goto_1

    nop

    :goto_2b
    if-gtz p0, :cond_6

    goto/32 :goto_40

    :cond_6
    goto/32 :goto_3f

    nop

    :goto_2c
    iget-object v0, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->mHolder:Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

    goto/32 :goto_21

    nop

    :goto_2d
    iget-object p0, p0, Lcom/android/settings/applications/RunningState$MergedItem;->mServices:Ljava/util/ArrayList;

    goto/32 :goto_5

    nop

    :goto_2e
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2f
    goto/32 :goto_4d

    nop

    :goto_30
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_31
    goto/32 :goto_3

    nop

    :goto_32
    const/4 v0, 0x0

    :goto_33
    goto/32 :goto_e

    nop

    :goto_34
    iget-wide p0, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->mFirstRunTime:J

    goto/32 :goto_4a

    nop

    :goto_35
    goto :goto_3a

    :goto_36
    goto/32 :goto_51

    nop

    :goto_37
    move-object v1, v3

    :goto_38
    goto/32 :goto_f

    nop

    :goto_39
    goto :goto_33

    :goto_3a
    goto/32 :goto_32

    nop

    :goto_3b
    const/4 v1, 0x0

    goto/32 :goto_20

    nop

    :goto_3c
    iget-object v0, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->mHolder:Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

    goto/32 :goto_46

    nop

    :goto_3d
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p0

    goto/32 :goto_2

    nop

    :goto_3e
    if-nez v1, :cond_7

    goto/32 :goto_1a

    :cond_7
    goto/32 :goto_19

    nop

    :goto_3f
    goto/16 :goto_8

    :goto_40
    goto/32 :goto_7

    nop

    :goto_41
    goto :goto_33

    :goto_42
    goto/32 :goto_17

    nop

    :goto_43
    if-gez v4, :cond_8

    goto/32 :goto_4f

    :cond_8
    goto/32 :goto_18

    nop

    :goto_44
    iget-boolean v1, v0, Lcom/android/settings/applications/RunningState$BaseItem;->mBackground:Z

    goto/32 :goto_50

    nop

    :goto_45
    const-wide/16 p0, 0x3e8

    goto/32 :goto_28

    nop

    :goto_46
    iget-object v0, v0, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->uptime:Landroid/widget/TextView;

    goto/32 :goto_39

    nop

    :goto_47
    sget p1, Lcom/android/settings/R$string;->service_restarting:I

    goto/32 :goto_3d

    nop

    :goto_48
    move v1, v2

    :goto_49
    goto/32 :goto_29

    nop

    :goto_4a
    sub-long/2addr v1, p0

    goto/32 :goto_45

    nop

    :goto_4b
    if-nez v1, :cond_9

    goto/32 :goto_42

    :cond_9
    goto/32 :goto_2c

    nop

    :goto_4c
    const/4 v2, 0x1

    goto/32 :goto_22

    nop

    :goto_4d
    iget-object v0, p0, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->mItem:Lcom/android/settings/applications/RunningState$BaseItem;

    goto/32 :goto_44

    nop

    :goto_4e
    goto/16 :goto_31

    :goto_4f
    goto/32 :goto_c

    nop

    :goto_50
    if-nez v1, :cond_a

    goto/32 :goto_36

    :cond_a
    goto/32 :goto_27

    nop

    :goto_51
    instance-of v0, v0, Lcom/android/settings/applications/RunningState$MergedItem;

    goto/32 :goto_1f

    nop
.end method
