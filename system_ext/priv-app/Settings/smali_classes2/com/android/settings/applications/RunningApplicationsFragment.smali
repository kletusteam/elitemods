.class public Lcom/android/settings/applications/RunningApplicationsFragment;
.super Lcom/android/settings/BaseFragment;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AbsListView$RecyclerListener;
.implements Lcom/android/settings/applications/RunningState$OnRefreshUiListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;
    }
.end annotation


# instance fields
.field SECONDARY_SERVER_MEM:J

.field final mActiveItems:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Landroid/view/View;",
            "Lcom/android/settings/applications/RunningProcessesView$ActiveItem;",
            ">;"
        }
    .end annotation
.end field

.field private mAdapter:Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;

.field private mAm:Landroid/app/ActivityManager;

.field private mBuilder:Ljava/lang/StringBuilder;

.field private mContainer:Lcom/android/settings/applications/ApplicationsContainer;

.field private mContext:Landroid/content/Context;

.field mCurSelected:Lcom/android/settings/applications/RunningState$BaseItem;

.field mLastAvailMemory:J

.field mLastBackgroundProcessMemory:J

.field mLastForegroundProcessMemory:J

.field mLastNumBackgroundProcesses:I

.field mLastNumForegroundProcesses:I

.field mLastNumServiceProcesses:I

.field mLastServiceProcessMemory:J

.field private mListType:I

.field private mListView:Landroid/widget/ListView;

.field mMemInfoReader:Lcom/android/internal/util/MemInfoReader;

.field private mRootView:Landroid/view/View;

.field private mShowBackgroundProcess:Z

.field private mState:Lcom/android/settings/applications/RunningState;


# direct methods
.method static bridge synthetic -$$Nest$fgetmBuilder(Lcom/android/settings/applications/RunningApplicationsFragment;)Ljava/lang/StringBuilder;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mBuilder:Ljava/lang/StringBuilder;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/settings/applications/RunningApplicationsFragment;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmShowBackgroundProcess(Lcom/android/settings/applications/RunningApplicationsFragment;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mShowBackgroundProcess:Z

    return p0
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/BaseFragment;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mActiveItems:Ljava/util/HashMap;

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mBuilder:Ljava/lang/StringBuilder;

    new-instance v0, Lcom/android/internal/util/MemInfoReader;

    invoke-direct {v0}, Lcom/android/internal/util/MemInfoReader;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mMemInfoReader:Lcom/android/internal/util/MemInfoReader;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mLastNumBackgroundProcesses:I

    iput v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mLastNumForegroundProcesses:I

    iput v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mLastNumServiceProcesses:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mLastBackgroundProcessMemory:J

    iput-wide v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mLastForegroundProcessMemory:J

    iput-wide v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mLastServiceProcessMemory:J

    iput-wide v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mLastAvailMemory:J

    return-void
.end method

.method private startServiceDetailsActivity(Lcom/android/settings/applications/RunningState$MergedItem;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.android.settings"

    const-string v2, "com.android.settings.applications.RunningServiceDetailsActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p1, Lcom/android/settings/applications/RunningState$MergedItem;->mProcess:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget v1, v1, Lcom/android/settings/applications/RunningState$ProcessItem;->mUid:I

    const-string/jumbo v2, "uid"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p1, Lcom/android/settings/applications/RunningState$MergedItem;->mProcess:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget v1, v1, Lcom/android/settings/applications/RunningState$BaseItem;->mUserId:I

    const-string/jumbo v2, "user_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object p1, p1, Lcom/android/settings/applications/RunningState$MergedItem;->mProcess:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-object p1, p1, Lcom/android/settings/applications/RunningState$ProcessItem;->mProcessName:Ljava/lang/String;

    const-string/jumbo v1, "process"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-boolean p1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mShowBackgroundProcess:Z

    const-string v1, "background"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "filter_app_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mListType:I

    const/4 v0, 0x3

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mShowBackgroundProcess:Z

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    const-class v0, Lcom/android/settings/applications/ApplicationsContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object p1

    check-cast p1, Lcom/android/settings/applications/ApplicationsContainer;

    iput-object p1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mContainer:Lcom/android/settings/applications/ApplicationsContainer;

    if-nez p1, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragment()Landroidx/fragment/app/Fragment;

    move-result-object p1

    check-cast p1, Lcom/android/settings/applications/ApplicationsContainer;

    iput-object p1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mContainer:Lcom/android/settings/applications/ApplicationsContainer;

    :cond_1
    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 0

    iget-object p2, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mRootView:Landroid/view/View;

    if-eqz p2, :cond_0

    return-object p2

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    const-string p3, "activity"

    invoke-virtual {p2, p3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/app/ActivityManager;

    iput-object p2, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mAm:Landroid/app/ActivityManager;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    invoke-static {p2}, Lcom/android/settings/applications/RunningState;->getInstance(Landroid/content/Context;)Lcom/android/settings/applications/RunningState;

    move-result-object p2

    iput-object p2, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mState:Lcom/android/settings/applications/RunningState;

    sget p2, Lcom/android/settings/R$layout;->manage_applications_main:I

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mRootView:Landroid/view/View;

    const p2, 0x102000a

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mListView:Landroid/widget/ListView;

    iget-object p1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mRootView:Landroid/view/View;

    const p2, 0x1020004

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object p2, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {p2, p1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    :cond_1
    iget-object p1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {p1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object p1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {p1, p0}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    new-instance p1, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;

    iget-object p2, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mState:Lcom/android/settings/applications/RunningState;

    invoke-direct {p1, p0, p2}, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;-><init>(Lcom/android/settings/applications/RunningApplicationsFragment;Lcom/android/settings/applications/RunningState;)V

    iput-object p1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mAdapter:Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;

    iget-object p2, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {p2, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance p1, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {p1}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    iget-object p2, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mAm:Landroid/app/ActivityManager;

    invoke-virtual {p2, p1}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    iget-wide p1, p1, Landroid/app/ActivityManager$MemoryInfo;->secondaryServerThreshold:J

    iput-wide p1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->SECONDARY_SERVER_MEM:J

    iget-object p0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mRootView:Landroid/view/View;

    return-object p0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    check-cast p1, Landroid/widget/ListView;

    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object p1

    invoke-interface {p1, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/android/settings/applications/RunningState$MergedItem;

    iput-object p1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mCurSelected:Lcom/android/settings/applications/RunningState$BaseItem;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    instance-of p2, p2, Lcom/android/settings/MiuiSettings;

    if-eqz p2, :cond_0

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    iget-object p2, p1, Lcom/android/settings/applications/RunningState$MergedItem;->mProcess:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget p2, p2, Lcom/android/settings/applications/RunningState$ProcessItem;->mUid:I

    const-string/jumbo p3, "uid"

    invoke-virtual {v4, p3, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object p1, p1, Lcom/android/settings/applications/RunningState$MergedItem;->mProcess:Lcom/android/settings/applications/RunningState$ProcessItem;

    iget-object p1, p1, Lcom/android/settings/applications/RunningState$ProcessItem;->mProcessName:Ljava/lang/String;

    const-string/jumbo p2, "process"

    invoke-virtual {v4, p2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean p1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mShowBackgroundProcess:Z

    const-string p2, "background"

    invoke-virtual {v4, p2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    sget p1, Lcom/android/settings/R$string;->application_info_label:I

    const-string p2, ":android:show_fragment_title"

    invoke-virtual {v4, p2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragment()Landroidx/fragment/app/Fragment;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Lcom/android/settings/applications/ApplicationsContainer;

    const-class p1, Lcom/android/settings/applications/RunningServiceDetails;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    sget v5, Lcom/android/settings/R$string;->runningservicedetails_settings_title:I

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/BaseFragment;->startFragment(Lmiuix/appcompat/app/Fragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/settings/applications/RunningApplicationsFragment;->startServiceDetailsActivity(Lcom/android/settings/applications/RunningState$MergedItem;)V

    :goto_0
    return-void
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 0

    iget-object p0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mActiveItems:Ljava/util/HashMap;

    invoke-virtual {p0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onRefreshUi(I)V
    .locals 2

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/android/settings/applications/RunningApplicationsFragment;->refreshUi(Z)V

    invoke-virtual {p0}, Lcom/android/settings/applications/RunningApplicationsFragment;->updateTimes()V

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/RunningApplicationsFragment;->refreshUi(Z)V

    invoke-virtual {p0}, Lcom/android/settings/applications/RunningApplicationsFragment;->updateTimes()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/applications/RunningApplicationsFragment;->updateTimes()V

    :goto_0
    return-void
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mContainer:Lcom/android/settings/applications/ApplicationsContainer;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/android/settings/applications/ApplicationsContainer;->mCurTab:Lcom/android/settings/applications/ApplicationsContainer$TabInfo;

    if-eqz v0, :cond_0

    iget v0, v0, Lcom/android/settings/applications/ApplicationsContainer$TabInfo;->mListType:I

    iget v1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mListType:I

    if-ne v0, v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "call resume RunningState, tab = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RunningApplicationsFragment"

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/applications/RunningApplicationsFragment;->resumeRunningState()V

    :cond_0
    return-void
.end method

.method refreshUi(Z)V
    .locals 9

    goto/32 :goto_9

    nop

    :goto_0
    const-wide/16 v2, 0x0

    goto/32 :goto_4

    nop

    :goto_1
    invoke-virtual {p1}, Lcom/android/internal/util/MemInfoReader;->readMemInfo()V

    goto/32 :goto_13

    nop

    :goto_2
    invoke-virtual {p1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :goto_3
    goto/32 :goto_d

    nop

    :goto_4
    cmp-long p1, v0, v2

    goto/32 :goto_5

    nop

    :goto_5
    if-ltz p1, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_11

    nop

    :goto_6
    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object p1

    goto/32 :goto_14

    nop

    :goto_7
    invoke-virtual {p1}, Lcom/android/internal/util/MemInfoReader;->getFreeSize()J

    move-result-wide v0

    goto/32 :goto_17

    nop

    :goto_8
    invoke-virtual {p1}, Lcom/android/internal/util/MemInfoReader;->getCachedSize()J

    move-result-wide v2

    goto/32 :goto_f

    nop

    :goto_9
    if-nez p1, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_e

    nop

    :goto_a
    monitor-enter p1

    :try_start_0
    iget v2, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mLastNumBackgroundProcesses:I

    iget-object v3, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mState:Lcom/android/settings/applications/RunningState;

    iget v4, v3, Lcom/android/settings/applications/RunningState;->mNumBackgroundProcesses:I

    if-ne v2, v4, :cond_2

    iget-wide v5, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mLastBackgroundProcessMemory:J

    iget-wide v7, v3, Lcom/android/settings/applications/RunningState;->mBackgroundProcessMemory:J

    cmp-long v2, v5, v7

    if-nez v2, :cond_2

    iget-wide v5, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mLastAvailMemory:J

    cmp-long v2, v5, v0

    if-eqz v2, :cond_3

    :cond_2
    iput v4, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mLastNumBackgroundProcesses:I

    iget-wide v4, v3, Lcom/android/settings/applications/RunningState;->mBackgroundProcessMemory:J

    iput-wide v4, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mLastBackgroundProcessMemory:J

    iput-wide v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mLastAvailMemory:J

    :cond_3
    iget v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mLastNumForegroundProcesses:I

    iget v1, v3, Lcom/android/settings/applications/RunningState;->mNumForegroundProcesses:I

    if-ne v0, v1, :cond_4

    iget-wide v4, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mLastForegroundProcessMemory:J

    iget-wide v6, v3, Lcom/android/settings/applications/RunningState;->mForegroundProcessMemory:J

    cmp-long v0, v4, v6

    if-nez v0, :cond_4

    iget v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mLastNumServiceProcesses:I

    iget v2, v3, Lcom/android/settings/applications/RunningState;->mNumServiceProcesses:I

    if-ne v0, v2, :cond_4

    iget-wide v4, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mLastServiceProcessMemory:J

    iget-wide v6, v3, Lcom/android/settings/applications/RunningState;->mServiceProcessMemory:J

    cmp-long v0, v4, v6

    if-eqz v0, :cond_5

    :cond_4
    iput v1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mLastNumForegroundProcesses:I

    iget-wide v0, v3, Lcom/android/settings/applications/RunningState;->mForegroundProcessMemory:J

    iput-wide v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mLastForegroundProcessMemory:J

    iget v0, v3, Lcom/android/settings/applications/RunningState;->mNumServiceProcesses:I

    iput v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mLastNumServiceProcesses:I

    iget-wide v0, v3, Lcom/android/settings/applications/RunningState;->mServiceProcessMemory:J

    iput-wide v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mLastServiceProcessMemory:J

    :cond_5
    monitor-exit p1

    return-void

    :catchall_0
    move-exception p0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_15

    nop

    :goto_b
    iget-wide v2, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->SECONDARY_SERVER_MEM:J

    goto/32 :goto_18

    nop

    :goto_c
    iget-object p1, p1, Lcom/android/settings/applications/RunningState;->mLock:Ljava/lang/Object;

    goto/32 :goto_a

    nop

    :goto_d
    iget-object p1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mMemInfoReader:Lcom/android/internal/util/MemInfoReader;

    goto/32 :goto_1

    nop

    :goto_e
    iget-object p1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mListView:Landroid/widget/ListView;

    goto/32 :goto_6

    nop

    :goto_f
    add-long/2addr v0, v2

    goto/32 :goto_b

    nop

    :goto_10
    iget-object p1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mState:Lcom/android/settings/applications/RunningState;

    goto/32 :goto_c

    nop

    :goto_11
    move-wide v0, v2

    :goto_12
    goto/32 :goto_10

    nop

    :goto_13
    iget-object p1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mMemInfoReader:Lcom/android/internal/util/MemInfoReader;

    goto/32 :goto_7

    nop

    :goto_14
    check-cast p1, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;

    goto/32 :goto_16

    nop

    :goto_15
    throw p0

    :goto_16
    invoke-virtual {p1}, Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;->refreshItems()V

    goto/32 :goto_2

    nop

    :goto_17
    iget-object p1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mMemInfoReader:Lcom/android/internal/util/MemInfoReader;

    goto/32 :goto_8

    nop

    :goto_18
    sub-long/2addr v0, v2

    goto/32 :goto_0

    nop
.end method

.method public resumeRunningState()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "resume RunningState, tab = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mListType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RunningApplicationsFragment"

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mState:Lcom/android/settings/applications/RunningState;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/applications/RunningState;->getInstance(Landroid/content/Context;)Lcom/android/settings/applications/RunningState;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mState:Lcom/android/settings/applications/RunningState;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mState:Lcom/android/settings/applications/RunningState;

    invoke-virtual {v0, p0}, Lcom/android/settings/applications/RunningState;->resume(Lcom/android/settings/applications/RunningState$OnRefreshUiListener;)V

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mAdapter:Lcom/android/settings/applications/RunningApplicationsFragment$ServiceListAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mState:Lcom/android/settings/applications/RunningState;

    invoke-virtual {v0}, Lcom/android/settings/applications/RunningState;->hasData()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/RunningApplicationsFragment;->refreshUi(Z)V

    :cond_1
    return-void
.end method

.method updateTimes()V
    .locals 4

    goto/32 :goto_9

    nop

    :goto_0
    goto :goto_6

    :goto_1
    goto/32 :goto_12

    nop

    :goto_2
    iget-object v2, v1, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->mRootView:Landroid/view/View;

    goto/32 :goto_8

    nop

    :goto_3
    iget-object v3, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mBuilder:Ljava/lang/StringBuilder;

    goto/32 :goto_4

    nop

    :goto_4
    invoke-virtual {v1, v2, v3}, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->updateTime(Landroid/content/Context;Ljava/lang/StringBuilder;)V

    goto/32 :goto_0

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    goto/32 :goto_b

    nop

    :goto_7
    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_8
    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    goto/32 :goto_e

    nop

    :goto_9
    iget-object v0, p0, Lcom/android/settings/applications/RunningApplicationsFragment;->mActiveItems:Ljava/util/HashMap;

    goto/32 :goto_7

    nop

    :goto_a
    if-nez v1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_11

    nop

    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_a

    nop

    :goto_c
    goto :goto_6

    :goto_d
    goto/32 :goto_f

    nop

    :goto_e
    if-eqz v2, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_10

    nop

    :goto_f
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    goto/32 :goto_3

    nop

    :goto_10
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto/32 :goto_c

    nop

    :goto_11
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_13

    nop

    :goto_12
    return-void

    :goto_13
    check-cast v1, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;

    goto/32 :goto_2

    nop
.end method
