.class public Lcom/android/settings/applications/RunningServiceDetails;
.super Lcom/android/settings/core/InstrumentedFragment;

# interfaces
.implements Lcom/android/settings/applications/RunningState$OnRefreshUiListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/applications/RunningServiceDetails$MyAlertDialogFragment;,
        Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;
    }
.end annotation


# instance fields
.field final mActiveDetails:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;",
            ">;"
        }
    .end annotation
.end field

.field mAllDetails:Landroid/view/ViewGroup;

.field mAm:Landroid/app/ActivityManager;

.field mBuilder:Ljava/lang/StringBuilder;

.field mHaveData:Z

.field mInflater:Landroid/view/LayoutInflater;

.field mMergedItem:Lcom/android/settings/applications/RunningState$MergedItem;

.field mNumProcesses:I

.field mNumServices:I

.field mProcessName:Ljava/lang/String;

.field mProcessesHeader:Landroid/widget/TextView;

.field mRootView:Landroid/view/View;

.field mServicesHeader:Landroid/widget/TextView;

.field mShowBackground:Z

.field mSnippet:Landroid/view/ViewGroup;

.field mSnippetActiveItem:Lcom/android/settings/applications/RunningProcessesView$ActiveItem;

.field mSnippetViewHolder:Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

.field mState:Lcom/android/settings/applications/RunningState;

.field mUid:I

.field mUserId:I


# direct methods
.method public static synthetic $r8$lambda$qwq4HY2xSZ6vW67uRf0zi_cQb5I(Lcom/android/settings/applications/RunningServiceDetails;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/applications/RunningServiceDetails;->lambda$finish$0()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mfinish(Lcom/android/settings/applications/RunningServiceDetails;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/applications/RunningServiceDetails;->finish()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mshowConfirmStopDialog(Lcom/android/settings/applications/RunningServiceDetails;Landroid/content/ComponentName;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/applications/RunningServiceDetails;->showConfirmStopDialog(Landroid/content/ComponentName;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/core/InstrumentedFragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mActiveDetails:Ljava/util/ArrayList;

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mBuilder:Ljava/lang/StringBuilder;

    return-void
.end method

.method private finish()V
    .locals 1

    new-instance v0, Lcom/android/settings/applications/RunningServiceDetails$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/android/settings/applications/RunningServiceDetails$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/applications/RunningServiceDetails;)V

    invoke-static {v0}, Lcom/android/settingslib/utils/ThreadUtils;->postOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private synthetic lambda$finish$0()V
    .locals 0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->onBackPressed()V

    :cond_0
    return-void
.end method

.method private showConfirmStopDialog(Landroid/content/ComponentName;)V
    .locals 1

    const/4 v0, 0x1

    invoke-static {v0, p1}, Lcom/android/settings/applications/RunningServiceDetails$MyAlertDialogFragment;->newConfirmStop(ILandroid/content/ComponentName;)Lcom/android/settings/applications/RunningServiceDetails$MyAlertDialogFragment;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, p0, v0}, Landroidx/fragment/app/Fragment;->setTargetFragment(Landroidx/fragment/app/Fragment;I)V

    :try_start_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p0

    const-string v0, "confirmstop"

    invoke-virtual {p1, p0, v0}, Landroidx/fragment/app/DialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    const-string p1, "RunningServicesDetails"

    const-string v0, "Failed to show confirm stop dialog"

    invoke-static {p1, v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method


# virtual methods
.method activeDetailForService(Landroid/content/ComponentName;)Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;
    .locals 3

    goto/32 :goto_c

    nop

    :goto_0
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_6

    nop

    :goto_1
    if-lt v0, v1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_11

    nop

    :goto_2
    if-nez v2, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_3

    nop

    :goto_3
    iget-object v2, v2, Lcom/android/settings/applications/RunningState$ServiceItem;->mRunningService:Landroid/app/ActivityManager$RunningServiceInfo;

    goto/32 :goto_b

    nop

    :goto_4
    check-cast v1, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;

    goto/32 :goto_15

    nop

    :goto_5
    invoke-virtual {p1, v2}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto/32 :goto_14

    nop

    :goto_6
    goto :goto_d

    :goto_7
    goto/32 :goto_9

    nop

    :goto_8
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_1

    nop

    :goto_9
    const/4 p0, 0x0

    goto/32 :goto_13

    nop

    :goto_a
    iget-object v2, v2, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    goto/32 :goto_5

    nop

    :goto_b
    if-nez v2, :cond_2

    goto/32 :goto_10

    :cond_2
    goto/32 :goto_a

    nop

    :goto_c
    const/4 v0, 0x0

    :goto_d
    goto/32 :goto_e

    nop

    :goto_e
    iget-object v1, p0, Lcom/android/settings/applications/RunningServiceDetails;->mActiveDetails:Ljava/util/ArrayList;

    goto/32 :goto_8

    nop

    :goto_f
    return-object v1

    :goto_10
    goto/32 :goto_0

    nop

    :goto_11
    iget-object v1, p0, Lcom/android/settings/applications/RunningServiceDetails;->mActiveDetails:Ljava/util/ArrayList;

    goto/32 :goto_12

    nop

    :goto_12
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_4

    nop

    :goto_13
    return-object p0

    :goto_14
    if-nez v2, :cond_3

    goto/32 :goto_10

    :cond_3
    goto/32 :goto_f

    nop

    :goto_15
    iget-object v2, v1, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->mServiceItem:Lcom/android/settings/applications/RunningState$ServiceItem;

    goto/32 :goto_2

    nop
.end method

.method addDetailViews()V
    .locals 5

    goto/32 :goto_30

    nop

    :goto_0
    iget-object v3, v3, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->mRootView:Landroid/view/View;

    goto/32 :goto_47

    nop

    :goto_1
    invoke-virtual {p0, v4, v1, v0}, Lcom/android/settings/applications/RunningServiceDetails;->addDetailsViews(Lcom/android/settings/applications/RunningState$MergedItem;ZZ)V

    goto/32 :goto_19

    nop

    :goto_2
    iget-object v3, p0, Lcom/android/settings/applications/RunningServiceDetails;->mAllDetails:Landroid/view/ViewGroup;

    goto/32 :goto_a

    nop

    :goto_3
    const/4 v1, 0x1

    goto/32 :goto_3a

    nop

    :goto_4
    iget-object v3, v3, Lcom/android/settings/applications/RunningState$MergedItem;->mChildren:Ljava/util/ArrayList;

    goto/32 :goto_9

    nop

    :goto_5
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto/32 :goto_18

    nop

    :goto_6
    iget-object v3, p0, Lcom/android/settings/applications/RunningServiceDetails;->mState:Lcom/android/settings/applications/RunningState;

    goto/32 :goto_24

    nop

    :goto_7
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    goto/32 :goto_1c

    nop

    :goto_8
    new-instance v2, Ljava/util/ArrayList;

    goto/32 :goto_16

    nop

    :goto_9
    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto/32 :goto_6

    nop

    :goto_a
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto/32 :goto_1d

    nop

    :goto_b
    return-void

    :goto_c
    if-nez v0, :cond_0

    goto/32 :goto_1e

    :cond_0
    goto/32 :goto_2

    nop

    :goto_d
    iput v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mNumServices:I

    goto/32 :goto_2a

    nop

    :goto_e
    check-cast v4, Lcom/android/settings/applications/RunningState$MergedItem;

    goto/32 :goto_1

    nop

    :goto_f
    check-cast v4, Lcom/android/settings/applications/RunningState$MergedItem;

    goto/32 :goto_41

    nop

    :goto_10
    move v3, v0

    :goto_11
    goto/32 :goto_32

    nop

    :goto_12
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto/32 :goto_35

    nop

    :goto_13
    goto/16 :goto_3b

    :goto_14
    goto/32 :goto_44

    nop

    :goto_15
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_e

    nop

    :goto_16
    iget-object v3, p0, Lcom/android/settings/applications/RunningServiceDetails;->mMergedItem:Lcom/android/settings/applications/RunningState$MergedItem;

    goto/32 :goto_4

    nop

    :goto_17
    if-gez v0, :cond_1

    goto/32 :goto_14

    :cond_1
    goto/32 :goto_23

    nop

    :goto_18
    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mServicesHeader:Landroid/widget/TextView;

    goto/32 :goto_25

    nop

    :goto_19
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_20

    nop

    :goto_1a
    if-nez v0, :cond_2

    goto/32 :goto_36

    :cond_2
    goto/32 :goto_37

    nop

    :goto_1b
    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto/32 :goto_45

    nop

    :goto_1c
    if-lt v3, v4, :cond_3

    goto/32 :goto_21

    :cond_3
    goto/32 :goto_15

    nop

    :goto_1d
    iput-object v2, p0, Lcom/android/settings/applications/RunningServiceDetails;->mServicesHeader:Landroid/widget/TextView;

    :goto_1e
    goto/32 :goto_3c

    nop

    :goto_1f
    check-cast v3, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;

    goto/32 :goto_0

    nop

    :goto_20
    goto/16 :goto_39

    :goto_21
    goto/32 :goto_10

    nop

    :goto_22
    if-lt v3, v4, :cond_4

    goto/32 :goto_2c

    :cond_4
    goto/32 :goto_48

    nop

    :goto_23
    iget-object v2, p0, Lcom/android/settings/applications/RunningServiceDetails;->mAllDetails:Landroid/view/ViewGroup;

    goto/32 :goto_40

    nop

    :goto_24
    iget-object v3, v3, Lcom/android/settings/applications/RunningState;->mBackgroundComparator:Ljava/util/Comparator;

    goto/32 :goto_1b

    nop

    :goto_25
    const/4 v2, 0x0

    goto/32 :goto_c

    nop

    :goto_26
    iget-object v3, v2, Lcom/android/settings/applications/RunningState$MergedItem;->mUser:Lcom/android/settings/applications/RunningState$UserState;

    goto/32 :goto_3e

    nop

    :goto_27
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_1f

    nop

    :goto_28
    if-nez v2, :cond_5

    goto/32 :goto_2c

    :cond_5
    goto/32 :goto_26

    nop

    :goto_29
    if-nez v3, :cond_6

    goto/32 :goto_46

    :cond_6
    goto/32 :goto_8

    nop

    :goto_2a
    iget-object v2, p0, Lcom/android/settings/applications/RunningServiceDetails;->mMergedItem:Lcom/android/settings/applications/RunningState$MergedItem;

    goto/32 :goto_28

    nop

    :goto_2b
    invoke-virtual {p0, v2, v1, v1}, Lcom/android/settings/applications/RunningServiceDetails;->addDetailsViews(Lcom/android/settings/applications/RunningState$MergedItem;ZZ)V

    :goto_2c
    goto/32 :goto_b

    nop

    :goto_2d
    iget-object v2, v2, Lcom/android/settings/applications/RunningState$MergedItem;->mChildren:Ljava/util/ArrayList;

    :goto_2e
    goto/32 :goto_38

    nop

    :goto_2f
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_13

    nop

    :goto_30
    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mActiveDetails:Ljava/util/ArrayList;

    goto/32 :goto_31

    nop

    :goto_31
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_3

    nop

    :goto_32
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    goto/32 :goto_22

    nop

    :goto_33
    const/4 v0, 0x0

    goto/32 :goto_3f

    nop

    :goto_34
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_42

    nop

    :goto_35
    iput-object v2, p0, Lcom/android/settings/applications/RunningServiceDetails;->mProcessesHeader:Landroid/widget/TextView;

    :goto_36
    goto/32 :goto_33

    nop

    :goto_37
    iget-object v3, p0, Lcom/android/settings/applications/RunningServiceDetails;->mAllDetails:Landroid/view/ViewGroup;

    goto/32 :goto_12

    nop

    :goto_38
    move v3, v0

    :goto_39
    goto/32 :goto_7

    nop

    :goto_3a
    sub-int/2addr v0, v1

    :goto_3b
    goto/32 :goto_17

    nop

    :goto_3c
    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mProcessesHeader:Landroid/widget/TextView;

    goto/32 :goto_1a

    nop

    :goto_3d
    iget-boolean v3, p0, Lcom/android/settings/applications/RunningServiceDetails;->mShowBackground:Z

    goto/32 :goto_29

    nop

    :goto_3e
    if-nez v3, :cond_7

    goto/32 :goto_43

    :cond_7
    goto/32 :goto_3d

    nop

    :goto_3f
    iput v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mNumProcesses:I

    goto/32 :goto_d

    nop

    :goto_40
    iget-object v3, p0, Lcom/android/settings/applications/RunningServiceDetails;->mActiveDetails:Ljava/util/ArrayList;

    goto/32 :goto_27

    nop

    :goto_41
    invoke-virtual {p0, v4, v0, v1}, Lcom/android/settings/applications/RunningServiceDetails;->addDetailsViews(Lcom/android/settings/applications/RunningState$MergedItem;ZZ)V

    goto/32 :goto_34

    nop

    :goto_42
    goto/16 :goto_11

    :goto_43
    goto/32 :goto_2b

    nop

    :goto_44
    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mActiveDetails:Ljava/util/ArrayList;

    goto/32 :goto_5

    nop

    :goto_45
    goto :goto_2e

    :goto_46
    goto/32 :goto_2d

    nop

    :goto_47
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto/32 :goto_2f

    nop

    :goto_48
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_f

    nop
.end method

.method addDetailsViews(Lcom/android/settings/applications/RunningState$MergedItem;ZZ)V
    .locals 3

    goto/32 :goto_37

    nop

    :goto_0
    goto/16 :goto_31

    :goto_1
    goto/32 :goto_30

    nop

    :goto_2
    return-void

    :goto_3
    if-ltz p2, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_1d

    nop

    :goto_4
    const/4 p2, -0x1

    :goto_5
    goto/32 :goto_10

    nop

    :goto_6
    const/4 v1, 0x1

    goto/32 :goto_20

    nop

    :goto_7
    invoke-virtual {p3, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p3

    goto/32 :goto_8

    nop

    :goto_8
    check-cast p3, Lcom/android/settings/applications/RunningState$ProcessItem;

    :goto_9
    goto/32 :goto_29

    nop

    :goto_a
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p2

    goto/32 :goto_b

    nop

    :goto_b
    if-lez p2, :cond_1

    goto/32 :goto_34

    :cond_1
    goto/32 :goto_23

    nop

    :goto_c
    move p2, v0

    :goto_d
    goto/32 :goto_3e

    nop

    :goto_e
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    goto/32 :goto_18

    nop

    :goto_f
    invoke-virtual {p0, p2, p1, v0, v1}, Lcom/android/settings/applications/RunningServiceDetails;->addServiceDetailsView(Lcom/android/settings/applications/RunningState$ServiceItem;Lcom/android/settings/applications/RunningState$MergedItem;ZZ)V

    goto/32 :goto_33

    nop

    :goto_10
    iget-object p3, p1, Lcom/android/settings/applications/RunningState$MergedItem;->mOtherProcesses:Ljava/util/ArrayList;

    goto/32 :goto_1b

    nop

    :goto_11
    invoke-virtual {p0, v2, p1, v1, v1}, Lcom/android/settings/applications/RunningServiceDetails;->addServiceDetailsView(Lcom/android/settings/applications/RunningState$ServiceItem;Lcom/android/settings/applications/RunningState$MergedItem;ZZ)V

    goto/32 :goto_22

    nop

    :goto_12
    goto :goto_9

    :goto_13
    goto/32 :goto_38

    nop

    :goto_14
    iget-object p3, p1, Lcom/android/settings/applications/RunningState$MergedItem;->mProcess:Lcom/android/settings/applications/RunningState$ProcessItem;

    goto/32 :goto_12

    nop

    :goto_15
    if-lt p2, p3, :cond_2

    goto/32 :goto_2f

    :cond_2
    goto/32 :goto_1a

    nop

    :goto_16
    iget p3, p1, Lcom/android/settings/applications/RunningState$BaseItem;->mUserId:I

    goto/32 :goto_3f

    nop

    :goto_17
    if-ne p3, v2, :cond_3

    goto/32 :goto_26

    :cond_3
    goto/32 :goto_32

    nop

    :goto_18
    if-lt p2, v2, :cond_4

    goto/32 :goto_1f

    :cond_4
    goto/32 :goto_28

    nop

    :goto_19
    check-cast v2, Lcom/android/settings/applications/RunningState$ServiceItem;

    goto/32 :goto_11

    nop

    :goto_1a
    if-ltz p2, :cond_5

    goto/32 :goto_13

    :cond_5
    goto/32 :goto_14

    nop

    :goto_1b
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result p3

    goto/32 :goto_15

    nop

    :goto_1c
    add-int/lit8 p2, p2, 0x1

    goto/32 :goto_2e

    nop

    :goto_1d
    move v2, v1

    goto/32 :goto_0

    nop

    :goto_1e
    goto :goto_d

    :goto_1f
    goto/32 :goto_24

    nop

    :goto_20
    if-nez p2, :cond_6

    goto/32 :goto_1f

    :cond_6
    goto/32 :goto_c

    nop

    :goto_21
    iget v2, p3, Lcom/android/settings/applications/RunningState$ProcessItem;->mPid:I

    goto/32 :goto_2d

    nop

    :goto_22
    add-int/lit8 p2, p2, 0x1

    goto/32 :goto_1e

    nop

    :goto_23
    const/4 p2, 0x0

    goto/32 :goto_16

    nop

    :goto_24
    if-nez p3, :cond_7

    goto/32 :goto_2f

    :cond_7
    goto/32 :goto_35

    nop

    :goto_25
    goto :goto_3d

    :goto_26
    goto/32 :goto_3c

    nop

    :goto_27
    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_19

    nop

    :goto_28
    iget-object v2, p1, Lcom/android/settings/applications/RunningState$MergedItem;->mServices:Ljava/util/ArrayList;

    goto/32 :goto_27

    nop

    :goto_29
    if-nez p3, :cond_8

    goto/32 :goto_2c

    :cond_8
    goto/32 :goto_21

    nop

    :goto_2a
    const/16 v2, 0x3e7

    goto/32 :goto_36

    nop

    :goto_2b
    goto :goto_3a

    :goto_2c
    goto/32 :goto_3

    nop

    :goto_2d
    if-lez v2, :cond_9

    goto/32 :goto_2c

    :cond_9
    goto/32 :goto_2b

    nop

    :goto_2e
    goto/16 :goto_5

    :goto_2f
    goto/32 :goto_2

    nop

    :goto_30
    move v2, v0

    :goto_31
    goto/32 :goto_39

    nop

    :goto_32
    iget p3, p1, Lcom/android/settings/applications/RunningState$BaseItem;->mUserId:I

    goto/32 :goto_2a

    nop

    :goto_33
    goto :goto_2f

    :goto_34
    goto/32 :goto_4

    nop

    :goto_35
    iget-object p2, p1, Lcom/android/settings/applications/RunningState$MergedItem;->mServices:Ljava/util/ArrayList;

    goto/32 :goto_a

    nop

    :goto_36
    if-ne p3, v2, :cond_a

    goto/32 :goto_26

    :cond_a
    goto/32 :goto_25

    nop

    :goto_37
    if-nez p1, :cond_b

    goto/32 :goto_2f

    :cond_b
    goto/32 :goto_3b

    nop

    :goto_38
    iget-object p3, p1, Lcom/android/settings/applications/RunningState$MergedItem;->mOtherProcesses:Ljava/util/ArrayList;

    goto/32 :goto_7

    nop

    :goto_39
    invoke-virtual {p0, p3, v2}, Lcom/android/settings/applications/RunningServiceDetails;->addProcessDetailsView(Lcom/android/settings/applications/RunningState$ProcessItem;Z)V

    :goto_3a
    goto/32 :goto_1c

    nop

    :goto_3b
    const/4 v0, 0x0

    goto/32 :goto_6

    nop

    :goto_3c
    move v1, v0

    :goto_3d
    goto/32 :goto_f

    nop

    :goto_3e
    iget-object v2, p1, Lcom/android/settings/applications/RunningState$MergedItem;->mServices:Ljava/util/ArrayList;

    goto/32 :goto_e

    nop

    :goto_3f
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    goto/32 :goto_17

    nop
.end method

.method addProcessDetailsView(Lcom/android/settings/applications/RunningState$ProcessItem;Z)V
    .locals 7

    goto/32 :goto_15

    nop

    :goto_0
    iget-object v1, p0, Lcom/android/settings/applications/RunningServiceDetails;->mInflater:Landroid/view/LayoutInflater;

    goto/32 :goto_3d

    nop

    :goto_1
    const/4 p2, 0x0

    goto/32 :goto_1f

    nop

    :goto_2
    new-instance v0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;

    goto/32 :goto_3

    nop

    :goto_3
    invoke-direct {v0, p0}, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;-><init>(Lcom/android/settings/applications/RunningServiceDetails;)V

    goto/32 :goto_0

    nop

    :goto_4
    iget-object p0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mActiveDetails:Ljava/util/ArrayList;

    goto/32 :goto_18

    nop

    :goto_5
    const/16 p1, 0x8

    goto/32 :goto_f

    nop

    :goto_6
    iput-object v1, v0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->mRootView:Landroid/view/View;

    goto/32 :goto_3e

    nop

    :goto_7
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_8
    goto/32 :goto_4

    nop

    :goto_9
    new-array v2, v5, [Ljava/lang/Object;

    goto/32 :goto_11

    nop

    :goto_a
    iget-object v3, p0, Lcom/android/settings/applications/RunningServiceDetails;->mState:Lcom/android/settings/applications/RunningState;

    goto/32 :goto_26

    nop

    :goto_b
    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto/32 :goto_6

    nop

    :goto_c
    goto :goto_8

    :goto_d
    goto/32 :goto_10

    nop

    :goto_e
    iget v2, p1, Lcom/android/settings/applications/RunningState$BaseItem;->mUserId:I

    goto/32 :goto_2c

    nop

    :goto_f
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/32 :goto_c

    nop

    :goto_10
    if-nez p2, :cond_0

    goto/32 :goto_34

    :cond_0
    goto/32 :goto_38

    nop

    :goto_11
    aput-object p2, v2, v4

    goto/32 :goto_21

    nop

    :goto_12
    iput-object v2, v0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->mActiveItem:Lcom/android/settings/applications/RunningProcessesView$ActiveItem;

    goto/32 :goto_32

    nop

    :goto_13
    iget v3, p1, Landroid/app/ActivityManager$RunningAppProcessInfo;->importanceReasonCode:I

    goto/32 :goto_2b

    nop

    :goto_14
    sget v3, Lcom/android/settings/R$string;->process_provider_in_use_description:I

    goto/32 :goto_16

    nop

    :goto_15
    invoke-virtual {p0}, Lcom/android/settings/applications/RunningServiceDetails;->addProcessesHeader()V

    goto/32 :goto_2

    nop

    :goto_16
    if-nez v2, :cond_1

    goto/32 :goto_17

    :cond_1
    :try_start_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object p1, p1, Landroid/app/ActivityManager$RunningAppProcessInfo;->importanceReasonComponent:Landroid/content/ComponentName;

    invoke-virtual {v2, p1, v4}, Landroid/content/pm/PackageManager;->getProviderInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ProviderInfo;

    move-result-object p1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v6, p1, Landroid/content/pm/ProviderInfo;->name:Ljava/lang/String;

    invoke-static {v2, v6, p1}, Lcom/android/settings/applications/RunningState;->makeLabel(Landroid/content/pm/PackageManager;Ljava/lang/String;Landroid/content/pm/PackageItemInfo;)Ljava/lang/CharSequence;

    move-result-object p2
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :goto_17
    goto/32 :goto_1d

    nop

    :goto_18
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_3b

    nop

    :goto_19
    const/4 v6, 0x2

    goto/32 :goto_28

    nop

    :goto_1a
    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto/32 :goto_27

    nop

    :goto_1b
    move v3, v4

    goto/32 :goto_2d

    nop

    :goto_1c
    iget-object v2, p1, Landroid/app/ActivityManager$RunningAppProcessInfo;->importanceReasonComponent:Landroid/content/ComponentName;

    goto/32 :goto_13

    nop

    :goto_1d
    if-nez v3, :cond_2

    goto/32 :goto_8

    :cond_2
    goto/32 :goto_35

    nop

    :goto_1e
    if-nez v2, :cond_3

    goto/32 :goto_17

    :cond_3
    :try_start_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object p1, p1, Landroid/app/ActivityManager$RunningAppProcessInfo;->importanceReasonComponent:Landroid/content/ComponentName;

    invoke-virtual {v2, p1, v4}, Landroid/content/pm/PackageManager;->getServiceInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ServiceInfo;

    move-result-object p1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v6, p1, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-static {v2, v6, p1}, Lcom/android/settings/applications/RunningState;->makeLabel(Landroid/content/pm/PackageManager;Ljava/lang/String;Landroid/content/pm/PackageItemInfo;)Ljava/lang/CharSequence;

    move-result-object p2
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/32 :goto_36

    nop

    :goto_1f
    iget-object p1, p1, Lcom/android/settings/applications/RunningState$ProcessItem;->mRunningProcessInfo:Landroid/app/ActivityManager$RunningAppProcessInfo;

    goto/32 :goto_1c

    nop

    :goto_20
    invoke-virtual {v2, v3, p1, v5}, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->bind(Lcom/android/settings/applications/RunningState;Lcom/android/settings/applications/RunningState$BaseItem;Ljava/lang/StringBuilder;)Lcom/android/settings/applications/RunningProcessesView$ActiveItem;

    move-result-object v2

    goto/32 :goto_12

    nop

    :goto_21
    invoke-virtual {p1, v3, v2}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_7

    nop

    :goto_22
    const/4 v4, 0x0

    goto/32 :goto_1a

    nop

    :goto_23
    if-ne v3, v5, :cond_4

    goto/32 :goto_37

    :cond_4
    goto/32 :goto_19

    nop

    :goto_24
    if-ne v2, v3, :cond_5

    goto/32 :goto_d

    :cond_5
    goto/32 :goto_5

    nop

    :goto_25
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    goto/32 :goto_30

    nop

    :goto_26
    iget-object v5, p0, Lcom/android/settings/applications/RunningServiceDetails;->mBuilder:Ljava/lang/StringBuilder;

    goto/32 :goto_20

    nop

    :goto_27
    iget-object v2, p0, Lcom/android/settings/applications/RunningServiceDetails;->mAllDetails:Landroid/view/ViewGroup;

    goto/32 :goto_b

    nop

    :goto_28
    if-ne v3, v6, :cond_6

    goto/32 :goto_2e

    :cond_6
    goto/32 :goto_1b

    nop

    :goto_29
    if-ne v2, v3, :cond_7

    goto/32 :goto_d

    :cond_7
    goto/32 :goto_3f

    nop

    :goto_2a
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(I)V

    goto/32 :goto_33

    nop

    :goto_2b
    const/4 v5, 0x1

    goto/32 :goto_23

    nop

    :goto_2c
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    goto/32 :goto_29

    nop

    :goto_2d
    goto/16 :goto_17

    :goto_2e
    goto/32 :goto_3a

    nop

    :goto_2f
    iget-object v3, p0, Lcom/android/settings/applications/RunningServiceDetails;->mAllDetails:Landroid/view/ViewGroup;

    goto/32 :goto_22

    nop

    :goto_30
    check-cast v1, Landroid/widget/TextView;

    goto/32 :goto_e

    nop

    :goto_31
    invoke-direct {v2, v1}, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;-><init>(Landroid/view/View;)V

    goto/32 :goto_39

    nop

    :goto_32
    sget v2, Lcom/android/settings/R$id;->comp_description:I

    goto/32 :goto_25

    nop

    :goto_33
    goto/16 :goto_8

    :goto_34
    goto/32 :goto_1

    nop

    :goto_35
    if-nez p2, :cond_8

    goto/32 :goto_8

    :cond_8
    goto/32 :goto_40

    nop

    :goto_36
    goto/16 :goto_17

    :goto_37
    goto/32 :goto_14

    nop

    :goto_38
    sget p1, Lcom/android/settings/R$string;->main_running_process_description:I

    goto/32 :goto_2a

    nop

    :goto_39
    iput-object v2, v0, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->mViewHolder:Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

    goto/32 :goto_a

    nop

    :goto_3a
    sget v3, Lcom/android/settings/R$string;->process_service_in_use_description:I

    goto/32 :goto_1e

    nop

    :goto_3b
    return-void

    :goto_3c
    const/16 v3, 0x3e7

    goto/32 :goto_24

    nop

    :goto_3d
    sget v2, Lcom/android/settings/R$layout;->running_service_details_process:I

    goto/32 :goto_2f

    nop

    :goto_3e
    new-instance v2, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

    goto/32 :goto_31

    nop

    :goto_3f
    iget v2, p1, Lcom/android/settings/applications/RunningState$BaseItem;->mUserId:I

    goto/32 :goto_3c

    nop

    :goto_40
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    goto/32 :goto_9

    nop
.end method

.method addProcessesHeader()V
    .locals 4

    goto/32 :goto_c

    nop

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :goto_1
    goto/32 :goto_d

    nop

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_3

    nop

    :goto_3
    iput v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mNumProcesses:I

    goto/32 :goto_7

    nop

    :goto_4
    iget-object v1, p0, Lcom/android/settings/applications/RunningServiceDetails;->mProcessesHeader:Landroid/widget/TextView;

    goto/32 :goto_0

    nop

    :goto_5
    const/4 v3, 0x0

    goto/32 :goto_a

    nop

    :goto_6
    if-eqz v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_9

    nop

    :goto_7
    return-void

    :goto_8
    iget-object v2, p0, Lcom/android/settings/applications/RunningServiceDetails;->mAllDetails:Landroid/view/ViewGroup;

    goto/32 :goto_5

    nop

    :goto_9
    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mInflater:Landroid/view/LayoutInflater;

    goto/32 :goto_b

    nop

    :goto_a
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto/32 :goto_e

    nop

    :goto_b
    sget v1, Lcom/android/settings/R$layout;->preference_category:I

    goto/32 :goto_8

    nop

    :goto_c
    iget v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mNumProcesses:I

    goto/32 :goto_6

    nop

    :goto_d
    iget v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mNumProcesses:I

    goto/32 :goto_2

    nop

    :goto_e
    check-cast v0, Landroid/widget/TextView;

    goto/32 :goto_10

    nop

    :goto_f
    sget v1, Lcom/android/settings/R$string;->runningservicedetails_processes_title:I

    goto/32 :goto_12

    nop

    :goto_10
    iput-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mProcessesHeader:Landroid/widget/TextView;

    goto/32 :goto_f

    nop

    :goto_11
    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mAllDetails:Landroid/view/ViewGroup;

    goto/32 :goto_4

    nop

    :goto_12
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/32 :goto_11

    nop
.end method

.method addServiceDetailsView(Lcom/android/settings/applications/RunningState$ServiceItem;Lcom/android/settings/applications/RunningState$MergedItem;ZZ)V
    .locals 8

    goto/32 :goto_5a

    nop

    :goto_0
    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    goto/32 :goto_30

    nop

    :goto_1
    move-object v1, p1

    goto/32 :goto_4

    nop

    :goto_2
    iget-object p1, p1, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    goto/32 :goto_d

    nop

    :goto_3
    if-nez p1, :cond_0

    goto/32 :goto_f

    :cond_0
    goto/32 :goto_33

    nop

    :goto_4
    goto :goto_13

    :goto_5
    goto/32 :goto_12

    nop

    :goto_6
    invoke-static {p2, p4, p1}, Landroid/app/ApplicationErrorReport;->getErrorReportReceiver(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/ComponentName;

    move-result-object p1

    goto/32 :goto_41

    nop

    :goto_7
    sget p2, Lcom/android/settings/R$string;->background_process_stop_description:I

    goto/32 :goto_4c

    nop

    :goto_8
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    goto/32 :goto_7a

    nop

    :goto_9
    check-cast p4, Landroid/widget/TextView;

    goto/32 :goto_19

    nop

    :goto_a
    new-instance v4, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

    goto/32 :goto_2d

    nop

    :goto_b
    iget-object p1, v2, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->mReportButton:Landroid/widget/Button;

    goto/32 :goto_42

    nop

    :goto_c
    iput-object v1, v2, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->mActiveItem:Lcom/android/settings/applications/RunningProcessesView$ActiveItem;

    goto/32 :goto_82

    nop

    :goto_d
    iget p1, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    goto/32 :goto_6

    nop

    :goto_e
    goto :goto_20

    :goto_f
    goto/32 :goto_38

    nop

    :goto_10
    iget-object p4, p4, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    goto/32 :goto_24

    nop

    :goto_11
    iget v4, p4, Landroid/app/ActivityManager$RunningServiceInfo;->clientLabel:I

    goto/32 :goto_7b

    nop

    :goto_12
    move-object v1, p2

    :goto_13
    goto/32 :goto_53

    nop

    :goto_14
    invoke-virtual {v3, p4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p4

    goto/32 :goto_9

    nop

    :goto_15
    move v6, p3

    :goto_16
    goto/32 :goto_4f

    nop

    :goto_17
    goto/16 :goto_3f

    :goto_18
    goto/32 :goto_3e

    nop

    :goto_19
    sget v4, Lcom/android/settings/R$id;->left_button:I

    goto/32 :goto_7d

    nop

    :goto_1a
    const-string/jumbo p4, "send_action_app_error"

    goto/32 :goto_7c

    nop

    :goto_1b
    iget-object v5, p0, Lcom/android/settings/applications/RunningServiceDetails;->mState:Lcom/android/settings/applications/RunningState;

    goto/32 :goto_6d

    nop

    :goto_1c
    invoke-direct {v2, p0}, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;-><init>(Lcom/android/settings/applications/RunningServiceDetails;)V

    goto/32 :goto_2f

    nop

    :goto_1d
    sget v0, Lcom/android/settings/R$string;->service_stop_description:I

    goto/32 :goto_17

    nop

    :goto_1e
    invoke-virtual {p2}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p2

    goto/32 :goto_6f

    nop

    :goto_1f
    invoke-virtual {p4, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :catch_0
    :goto_20
    goto/32 :goto_70

    nop

    :goto_21
    goto/16 :goto_43

    :goto_22
    goto/32 :goto_b

    nop

    :goto_23
    check-cast v4, Landroid/widget/Button;

    goto/32 :goto_95

    nop

    :goto_24
    invoke-virtual {v4, p4}, Landroid/app/ActivityManager;->getRunningServiceControlPanel(Landroid/content/ComponentName;)Landroid/app/PendingIntent;

    move-result-object p4

    goto/32 :goto_78

    nop

    :goto_25
    invoke-virtual {p4, v0}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object p4

    goto/32 :goto_51

    nop

    :goto_26
    iput-object v4, v2, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->mStopButton:Landroid/widget/Button;

    goto/32 :goto_69

    nop

    :goto_27
    invoke-virtual {p4, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/32 :goto_59

    nop

    :goto_28
    const/4 v6, 0x0

    goto/32 :goto_3d

    nop

    :goto_29
    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    goto/32 :goto_23

    nop

    :goto_2a
    if-ne p3, v4, :cond_1

    goto/32 :goto_5c

    :cond_1
    goto/32 :goto_75

    nop

    :goto_2b
    if-nez p1, :cond_2

    goto/32 :goto_16

    :cond_2
    goto/32 :goto_15

    nop

    :goto_2c
    if-ne p3, v0, :cond_3

    goto/32 :goto_5c

    :cond_3
    goto/32 :goto_27

    nop

    :goto_2d
    invoke-direct {v4, v3}, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;-><init>(Landroid/view/View;)V

    goto/32 :goto_58

    nop

    :goto_2e
    invoke-virtual {p4, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_e

    nop

    :goto_2f
    iget-object v3, p0, Lcom/android/settings/applications/RunningServiceDetails;->mInflater:Landroid/view/LayoutInflater;

    goto/32 :goto_88

    nop

    :goto_30
    invoke-virtual {p2, v1, v3, v0}, Landroid/content/pm/PackageManager;->getText(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object p2

    goto/32 :goto_2e

    nop

    :goto_31
    iget-object v0, v2, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->mManageIntent:Landroid/app/PendingIntent;

    goto/32 :goto_6c

    nop

    :goto_32
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    goto/32 :goto_5b

    nop

    :goto_33
    iget-object v0, p1, Lcom/android/settings/applications/RunningState$ServiceItem;->mServiceInfo:Landroid/content/pm/ServiceInfo;

    goto/32 :goto_8b

    nop

    :goto_34
    iget-object p4, p1, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    goto/32 :goto_2

    nop

    :goto_35
    if-nez p1, :cond_4

    goto/32 :goto_5

    :cond_4
    goto/32 :goto_1

    nop

    :goto_36
    iput-object v3, v2, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->mRootView:Landroid/view/View;

    goto/32 :goto_66

    nop

    :goto_37
    iget-object p4, p1, Lcom/android/settings/applications/RunningState$ServiceItem;->mRunningService:Landroid/app/ActivityManager$RunningServiceInfo;

    goto/32 :goto_11

    nop

    :goto_38
    iget-boolean p2, p2, Lcom/android/settings/applications/RunningState$BaseItem;->mBackground:Z

    goto/32 :goto_6b

    nop

    :goto_39
    invoke-virtual {v3, p4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p4

    goto/32 :goto_3b

    nop

    :goto_3a
    iget-object p2, v2, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->mStopButton:Landroid/widget/Button;

    goto/32 :goto_6a

    nop

    :goto_3b
    invoke-virtual {p4, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_3c
    goto/32 :goto_87

    nop

    :goto_3d
    invoke-virtual {v3, v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    goto/32 :goto_96

    nop

    :goto_3e
    sget v0, Lcom/android/settings/R$string;->heavy_weight_stop_description:I

    :goto_3f
    goto/32 :goto_8c

    nop

    :goto_40
    iget v1, p2, Lcom/android/settings/applications/RunningState$BaseItem;->mUserId:I

    goto/32 :goto_4b

    nop

    :goto_41
    iput-object p1, v2, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->mInstaller:Landroid/content/ComponentName;

    goto/32 :goto_77

    nop

    :goto_42
    invoke-virtual {p1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_43
    goto/32 :goto_73

    nop

    :goto_44
    invoke-virtual {p2, p4}, Landroid/widget/Button;->setText(I)V

    goto/32 :goto_68

    nop

    :goto_45
    iget-object v5, p0, Lcom/android/settings/applications/RunningServiceDetails;->mAllDetails:Landroid/view/ViewGroup;

    goto/32 :goto_28

    nop

    :goto_46
    sget v0, Lcom/android/settings/R$string;->service_stop:I

    :goto_47
    goto/32 :goto_25

    nop

    :goto_48
    iget v1, p2, Lcom/android/settings/applications/RunningState$BaseItem;->mUserId:I

    goto/32 :goto_8

    nop

    :goto_49
    invoke-virtual {p2, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/32 :goto_65

    nop

    :goto_4a
    iget-object p2, v2, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->mReportButton:Landroid/widget/Button;

    goto/32 :goto_49

    nop

    :goto_4b
    if-ne v1, v0, :cond_5

    goto/32 :goto_7f

    :cond_5
    goto/32 :goto_7e

    nop

    :goto_4c
    invoke-virtual {p4, p2}, Landroid/widget/TextView;->setText(I)V

    goto/32 :goto_8d

    nop

    :goto_4d
    if-eqz p4, :cond_6

    goto/32 :goto_3c

    :cond_6
    goto/32 :goto_8a

    nop

    :goto_4e
    check-cast v4, Landroid/widget/Button;

    goto/32 :goto_26

    nop

    :goto_4f
    invoke-virtual {p2, v6}, Landroid/widget/Button;->setEnabled(Z)V

    goto/32 :goto_21

    nop

    :goto_50
    sget v0, Lcom/android/settings/R$string;->service_manage:I

    goto/32 :goto_93

    nop

    :goto_51
    invoke-virtual {p2, p4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_4a

    nop

    :goto_52
    if-nez p2, :cond_7

    goto/32 :goto_63

    :cond_7
    :try_start_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    invoke-virtual {p2}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p2

    iget-object v0, p1, Lcom/android/settings/applications/RunningState$ServiceItem;->mRunningService:Landroid/app/ActivityManager$RunningServiceInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RunningServiceInfo;->clientPackage:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object p2

    iget-object v0, p1, Lcom/android/settings/applications/RunningState$ServiceItem;->mRunningService:Landroid/app/ActivityManager$RunningServiceInfo;

    iget v0, v0, Landroid/app/ActivityManager$RunningServiceInfo;->clientLabel:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/android/settings/R$string;->service_manage_description:I

    new-array v3, p3, [Ljava/lang/Object;

    aput-object p2, v3, v6

    invoke-virtual {v0, v1, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p4, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_62

    nop

    :goto_53
    new-instance v2, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;

    goto/32 :goto_1c

    nop

    :goto_54
    goto/16 :goto_7f

    :goto_55
    goto/32 :goto_48

    nop

    :goto_56
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_91

    nop

    :goto_57
    if-nez p3, :cond_8

    goto/32 :goto_55

    :cond_8
    goto/32 :goto_5f

    nop

    :goto_58
    iput-object v4, v2, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->mViewHolder:Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

    goto/32 :goto_1b

    nop

    :goto_59
    sget p1, Lcom/android/settings/R$id;->control_buttons_panel:I

    goto/32 :goto_71

    nop

    :goto_5a
    const/16 v0, 0x3e7

    goto/32 :goto_57

    nop

    :goto_5b
    goto/16 :goto_43

    :goto_5c
    goto/32 :goto_92

    nop

    :goto_5d
    iget-object v4, p0, Lcom/android/settings/applications/RunningServiceDetails;->mAm:Landroid/app/ActivityManager;

    goto/32 :goto_10

    nop

    :goto_5e
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    goto/32 :goto_2a

    nop

    :goto_5f
    invoke-virtual {p0}, Lcom/android/settings/applications/RunningServiceDetails;->addServicesHeader()V

    goto/32 :goto_54

    nop

    :goto_60
    if-nez p3, :cond_9

    goto/32 :goto_5c

    :cond_9
    goto/32 :goto_98

    nop

    :goto_61
    sget p4, Lcom/android/settings/R$id;->comp_description:I

    goto/32 :goto_14

    nop

    :goto_62
    goto/16 :goto_20

    :goto_63
    goto/32 :goto_8f

    nop

    :goto_64
    invoke-virtual {p2, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/32 :goto_3a

    nop

    :goto_65
    iget-object p2, v2, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->mReportButton:Landroid/widget/Button;

    goto/32 :goto_86

    nop

    :goto_66
    iput-object p1, v2, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->mServiceItem:Lcom/android/settings/applications/RunningState$ServiceItem;

    goto/32 :goto_a

    nop

    :goto_67
    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto/32 :goto_36

    nop

    :goto_68
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    goto/32 :goto_99

    nop

    :goto_69
    sget v4, Lcom/android/settings/R$id;->right_button:I

    goto/32 :goto_29

    nop

    :goto_6a
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p4

    goto/32 :goto_31

    nop

    :goto_6b
    if-nez p2, :cond_a

    goto/32 :goto_8e

    :cond_a
    goto/32 :goto_7

    nop

    :goto_6c
    if-nez v0, :cond_b

    goto/32 :goto_94

    :cond_b
    goto/32 :goto_50

    nop

    :goto_6d
    iget-object v7, p0, Lcom/android/settings/applications/RunningServiceDetails;->mBuilder:Ljava/lang/StringBuilder;

    goto/32 :goto_81

    nop

    :goto_6e
    iget-object v1, v0, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    goto/32 :goto_80

    nop

    :goto_6f
    iget-object v0, p1, Lcom/android/settings/applications/RunningState$ServiceItem;->mServiceInfo:Landroid/content/pm/ServiceInfo;

    goto/32 :goto_6e

    nop

    :goto_70
    iget-object p2, v2, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->mStopButton:Landroid/widget/Button;

    goto/32 :goto_64

    nop

    :goto_71
    invoke-virtual {v3, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    goto/32 :goto_32

    nop

    :goto_72
    if-nez v0, :cond_c

    goto/32 :goto_f

    :cond_c
    goto/32 :goto_74

    nop

    :goto_73
    iget-object p0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mActiveDetails:Ljava/util/ArrayList;

    goto/32 :goto_56

    nop

    :goto_74
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    goto/32 :goto_1e

    nop

    :goto_75
    iget p3, p2, Lcom/android/settings/applications/RunningState$BaseItem;->mUserId:I

    goto/32 :goto_2c

    nop

    :goto_76
    if-nez p1, :cond_d

    goto/32 :goto_22

    :cond_d
    goto/32 :goto_83

    nop

    :goto_77
    iget-object p2, v2, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->mReportButton:Landroid/widget/Button;

    goto/32 :goto_2b

    nop

    :goto_78
    iput-object p4, v2, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->mManageIntent:Landroid/app/PendingIntent;

    :goto_79
    goto/32 :goto_61

    nop

    :goto_7a
    if-ne v1, v2, :cond_e

    goto/32 :goto_7f

    :cond_e
    goto/32 :goto_40

    nop

    :goto_7b
    if-nez v4, :cond_f

    goto/32 :goto_79

    :cond_f
    goto/32 :goto_5d

    nop

    :goto_7c
    invoke-static {p2, p4, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p2

    goto/32 :goto_97

    nop

    :goto_7d
    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    goto/32 :goto_4e

    nop

    :goto_7e
    invoke-virtual {p0}, Lcom/android/settings/applications/RunningServiceDetails;->addProcessesHeader()V

    :goto_7f
    goto/32 :goto_35

    nop

    :goto_80
    iget v3, v0, Landroid/content/pm/ServiceInfo;->descriptionRes:I

    goto/32 :goto_0

    nop

    :goto_81
    invoke-virtual {v4, v5, v1, v7}, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->bind(Lcom/android/settings/applications/RunningState;Lcom/android/settings/applications/RunningState$BaseItem;Ljava/lang/StringBuilder;)Lcom/android/settings/applications/RunningProcessesView$ActiveItem;

    move-result-object v1

    goto/32 :goto_c

    nop

    :goto_82
    const/16 v1, 0x8

    goto/32 :goto_4d

    nop

    :goto_83
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    goto/32 :goto_89

    nop

    :goto_84
    if-nez p1, :cond_10

    goto/32 :goto_18

    :cond_10
    goto/32 :goto_1d

    nop

    :goto_85
    if-nez p1, :cond_11

    goto/32 :goto_63

    :cond_11
    goto/32 :goto_90

    nop

    :goto_86
    const p4, 0x110f0028

    goto/32 :goto_44

    nop

    :goto_87
    if-nez p1, :cond_12

    goto/32 :goto_79

    :cond_12
    goto/32 :goto_37

    nop

    :goto_88
    sget v4, Lcom/android/settings/R$layout;->running_service_details_service:I

    goto/32 :goto_45

    nop

    :goto_89
    iget-object p1, p1, Lcom/android/settings/applications/RunningState$ServiceItem;->mServiceInfo:Landroid/content/pm/ServiceInfo;

    goto/32 :goto_34

    nop

    :goto_8a
    sget p4, Lcom/android/settings/R$id;->service:I

    goto/32 :goto_39

    nop

    :goto_8b
    iget v0, v0, Landroid/content/pm/ServiceInfo;->descriptionRes:I

    goto/32 :goto_72

    nop

    :goto_8c
    invoke-virtual {p2, v0}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    goto/32 :goto_1f

    nop

    :goto_8d
    goto/16 :goto_20

    :goto_8e
    goto/32 :goto_85

    nop

    :goto_8f
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    goto/32 :goto_84

    nop

    :goto_90
    iget-object p2, v2, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->mManageIntent:Landroid/app/PendingIntent;

    goto/32 :goto_52

    nop

    :goto_91
    return-void

    :goto_92
    const/4 p3, 0x1

    goto/32 :goto_3

    nop

    :goto_93
    goto/16 :goto_47

    :goto_94
    goto/32 :goto_46

    nop

    :goto_95
    iput-object v4, v2, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->mReportButton:Landroid/widget/Button;

    goto/32 :goto_60

    nop

    :goto_96
    iget-object v4, p0, Lcom/android/settings/applications/RunningServiceDetails;->mAllDetails:Landroid/view/ViewGroup;

    goto/32 :goto_67

    nop

    :goto_97
    if-nez p2, :cond_13

    goto/32 :goto_22

    :cond_13
    goto/32 :goto_76

    nop

    :goto_98
    iget p3, p2, Lcom/android/settings/applications/RunningState$BaseItem;->mUserId:I

    goto/32 :goto_5e

    nop

    :goto_99
    invoke-virtual {p2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p2

    goto/32 :goto_1a

    nop
.end method

.method addServicesHeader()V
    .locals 4

    goto/32 :goto_4

    nop

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :goto_1
    goto/32 :goto_3

    nop

    :goto_2
    sget v1, Lcom/android/settings/R$layout;->preference_category:I

    goto/32 :goto_a

    nop

    :goto_3
    iget v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mNumServices:I

    goto/32 :goto_b

    nop

    :goto_4
    iget v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mNumServices:I

    goto/32 :goto_7

    nop

    :goto_5
    check-cast v0, Landroid/widget/TextView;

    goto/32 :goto_9

    nop

    :goto_6
    sget v1, Lcom/android/settings/R$string;->runningservicedetails_services_title:I

    goto/32 :goto_8

    nop

    :goto_7
    if-eqz v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_10

    nop

    :goto_8
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/32 :goto_11

    nop

    :goto_9
    iput-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mServicesHeader:Landroid/widget/TextView;

    goto/32 :goto_6

    nop

    :goto_a
    iget-object v2, p0, Lcom/android/settings/applications/RunningServiceDetails;->mAllDetails:Landroid/view/ViewGroup;

    goto/32 :goto_d

    nop

    :goto_b
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_f

    nop

    :goto_c
    iget-object v1, p0, Lcom/android/settings/applications/RunningServiceDetails;->mServicesHeader:Landroid/widget/TextView;

    goto/32 :goto_0

    nop

    :goto_d
    const/4 v3, 0x0

    goto/32 :goto_e

    nop

    :goto_e
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_f
    iput v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mNumServices:I

    goto/32 :goto_12

    nop

    :goto_10
    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mInflater:Landroid/view/LayoutInflater;

    goto/32 :goto_2

    nop

    :goto_11
    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mAllDetails:Landroid/view/ViewGroup;

    goto/32 :goto_c

    nop

    :goto_12
    return-void
.end method

.method ensureData()V
    .locals 2

    goto/32 :goto_7

    nop

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/settings/applications/RunningServiceDetails;->refreshUi(Z)V

    :goto_1
    goto/32 :goto_a

    nop

    :goto_2
    invoke-virtual {v1}, Lcom/android/settings/applications/RunningState;->waitForData()V

    goto/32 :goto_0

    nop

    :goto_3
    iget-object v1, p0, Lcom/android/settings/applications/RunningServiceDetails;->mState:Lcom/android/settings/applications/RunningState;

    goto/32 :goto_9

    nop

    :goto_4
    iput-boolean v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mHaveData:Z

    goto/32 :goto_3

    nop

    :goto_5
    iget-object v1, p0, Lcom/android/settings/applications/RunningServiceDetails;->mState:Lcom/android/settings/applications/RunningState;

    goto/32 :goto_2

    nop

    :goto_6
    if-eqz v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_8

    nop

    :goto_7
    iget-boolean v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mHaveData:Z

    goto/32 :goto_6

    nop

    :goto_8
    const/4 v0, 0x1

    goto/32 :goto_4

    nop

    :goto_9
    invoke-virtual {v1, p0}, Lcom/android/settings/applications/RunningState;->resume(Lcom/android/settings/applications/RunningState$OnRefreshUiListener;)V

    goto/32 :goto_5

    nop

    :goto_a
    return-void
.end method

.method findMergedItem()Z
    .locals 6

    goto/32 :goto_31

    nop

    :goto_0
    goto/16 :goto_30

    :goto_1
    goto/32 :goto_13

    nop

    :goto_2
    goto/16 :goto_29

    :goto_3
    goto/32 :goto_6

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mMergedItem:Lcom/android/settings/applications/RunningState$MergedItem;

    goto/32 :goto_27

    nop

    :goto_5
    if-nez v4, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_1a

    nop

    :goto_6
    const/4 v3, 0x0

    :goto_7
    goto/32 :goto_4

    nop

    :goto_8
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    goto/32 :goto_26

    nop

    :goto_9
    return p0

    :goto_a
    goto/32 :goto_20

    nop

    :goto_b
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_2

    nop

    :goto_c
    invoke-virtual {v0}, Lcom/android/settings/applications/RunningState;->getCurrentMergedItems()Ljava/util/ArrayList;

    move-result-object v0

    :goto_d
    goto/32 :goto_11

    nop

    :goto_e
    if-ne v5, v4, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_0

    nop

    :goto_f
    check-cast v3, Lcom/android/settings/applications/RunningState$MergedItem;

    goto/32 :goto_2a

    nop

    :goto_10
    if-nez v4, :cond_2

    goto/32 :goto_30

    :cond_2
    goto/32 :goto_2f

    nop

    :goto_11
    const/4 v1, 0x0

    goto/32 :goto_25

    nop

    :goto_12
    if-ne v4, v5, :cond_3

    goto/32 :goto_2d

    :cond_3
    goto/32 :goto_2c

    nop

    :goto_13
    iget-object v4, p0, Lcom/android/settings/applications/RunningServiceDetails;->mProcessName:Ljava/lang/String;

    goto/32 :goto_5

    nop

    :goto_14
    if-nez v5, :cond_4

    goto/32 :goto_30

    :cond_4
    goto/32 :goto_21

    nop

    :goto_15
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    goto/32 :goto_10

    nop

    :goto_16
    invoke-virtual {v0}, Lcom/android/settings/applications/RunningState;->getCurrentBackgroundItems()Ljava/util/ArrayList;

    move-result-object v0

    goto/32 :goto_18

    nop

    :goto_17
    if-nez v0, :cond_5

    goto/32 :goto_19

    :cond_5
    goto/32 :goto_2b

    nop

    :goto_18
    goto :goto_d

    :goto_19
    goto/32 :goto_24

    nop

    :goto_1a
    iget-object v5, v3, Lcom/android/settings/applications/RunningState$MergedItem;->mProcess:Lcom/android/settings/applications/RunningState$ProcessItem;

    goto/32 :goto_14

    nop

    :goto_1b
    iget v5, p0, Lcom/android/settings/applications/RunningServiceDetails;->mUserId:I

    goto/32 :goto_12

    nop

    :goto_1c
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_f

    nop

    :goto_1d
    if-gez v4, :cond_6

    goto/32 :goto_1

    :cond_6
    goto/32 :goto_32

    nop

    :goto_1e
    const/4 p0, 0x1

    goto/32 :goto_9

    nop

    :goto_1f
    iget v5, v5, Lcom/android/settings/applications/RunningState$ProcessItem;->mUid:I

    goto/32 :goto_e

    nop

    :goto_20
    return v1

    :goto_21
    iget-object v5, v5, Lcom/android/settings/applications/RunningState$ProcessItem;->mProcessName:Ljava/lang/String;

    goto/32 :goto_15

    nop

    :goto_22
    iget v4, p0, Lcom/android/settings/applications/RunningServiceDetails;->mUid:I

    goto/32 :goto_1d

    nop

    :goto_23
    if-nez v5, :cond_7

    goto/32 :goto_1

    :cond_7
    goto/32 :goto_1f

    nop

    :goto_24
    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mState:Lcom/android/settings/applications/RunningState;

    goto/32 :goto_c

    nop

    :goto_25
    if-nez v0, :cond_8

    goto/32 :goto_3

    :cond_8
    goto/32 :goto_28

    nop

    :goto_26
    if-lt v2, v3, :cond_9

    goto/32 :goto_3

    :cond_9
    goto/32 :goto_1c

    nop

    :goto_27
    if-ne v0, v3, :cond_a

    goto/32 :goto_a

    :cond_a
    goto/32 :goto_2e

    nop

    :goto_28
    move v2, v1

    :goto_29
    goto/32 :goto_8

    nop

    :goto_2a
    iget v4, v3, Lcom/android/settings/applications/RunningState$BaseItem;->mUserId:I

    goto/32 :goto_1b

    nop

    :goto_2b
    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mState:Lcom/android/settings/applications/RunningState;

    goto/32 :goto_16

    nop

    :goto_2c
    goto :goto_30

    :goto_2d
    goto/32 :goto_22

    nop

    :goto_2e
    iput-object v3, p0, Lcom/android/settings/applications/RunningServiceDetails;->mMergedItem:Lcom/android/settings/applications/RunningState$MergedItem;

    goto/32 :goto_1e

    nop

    :goto_2f
    goto/16 :goto_7

    :goto_30
    goto/32 :goto_b

    nop

    :goto_31
    iget-boolean v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mShowBackground:Z

    goto/32 :goto_17

    nop

    :goto_32
    iget-object v5, v3, Lcom/android/settings/applications/RunningState$MergedItem;->mProcess:Lcom/android/settings/applications/RunningState$ProcessItem;

    goto/32 :goto_23

    nop
.end method

.method public getMetricsCategory()I
    .locals 0

    const/16 p0, 0x55

    return p0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/core/InstrumentedFragment;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/Fragment;->setHasOptionsMenu(Z)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string/jumbo v0, "uid"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/android/settings/applications/RunningServiceDetails;->mUid:I

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string/jumbo v0, "user_id"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/android/settings/applications/RunningServiceDetails;->mUserId:I

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string/jumbo v0, "process"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/applications/RunningServiceDetails;->mProcessName:Ljava/lang/String;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "background"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/settings/applications/RunningServiceDetails;->mShowBackground:Z

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/ActivityManager;

    iput-object p1, p0, Lcom/android/settings/applications/RunningServiceDetails;->mAm:Landroid/app/ActivityManager;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/LayoutInflater;

    iput-object p1, p0, Lcom/android/settings/applications/RunningServiceDetails;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/android/settings/applications/RunningState;->getInstance(Landroid/content/Context;)Lcom/android/settings/applications/RunningState;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/applications/RunningServiceDetails;->mState:Lcom/android/settings/applications/RunningState;

    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    sget p3, Lcom/android/settings/R$layout;->running_service_details:I

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    invoke-static {p2, p1, p1, v0}, Lcom/android/settings/Utils;->prepareCustomPreferencesList(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Z)V

    iput-object p1, p0, Lcom/android/settings/applications/RunningServiceDetails;->mRootView:Landroid/view/View;

    sget p2, Lcom/android/settings/R$id;->all_details:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/view/ViewGroup;

    iput-object p2, p0, Lcom/android/settings/applications/RunningServiceDetails;->mAllDetails:Landroid/view/ViewGroup;

    sget p2, Lcom/android/settings/R$id;->snippet:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/view/ViewGroup;

    iput-object p2, p0, Lcom/android/settings/applications/RunningServiceDetails;->mSnippet:Landroid/view/ViewGroup;

    new-instance p3, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

    invoke-direct {p3, p2}, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;-><init>(Landroid/view/View;)V

    iput-object p3, p0, Lcom/android/settings/applications/RunningServiceDetails;->mSnippetViewHolder:Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

    invoke-virtual {p0}, Lcom/android/settings/applications/RunningServiceDetails;->ensureData()V

    return-object p1
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/android/settingslib/core/lifecycle/ObservableFragment;->onPause()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mHaveData:Z

    iget-object p0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mState:Lcom/android/settings/applications/RunningState;

    invoke-virtual {p0}, Lcom/android/settings/applications/RunningState;->pause()V

    return-void
.end method

.method public onRefreshUi(I)V
    .locals 2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_3

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/android/settings/applications/RunningServiceDetails;->refreshUi(Z)V

    invoke-virtual {p0}, Lcom/android/settings/applications/RunningServiceDetails;->updateTimes()V

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/android/settings/applications/RunningServiceDetails;->refreshUi(Z)V

    invoke-virtual {p0}, Lcom/android/settings/applications/RunningServiceDetails;->updateTimes()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/android/settings/applications/RunningServiceDetails;->updateTimes()V

    :goto_0
    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/core/InstrumentedFragment;->onResume()V

    invoke-virtual {p0}, Lcom/android/settings/applications/RunningServiceDetails;->ensureData()V

    return-void
.end method

.method refreshUi(Z)V
    .locals 3

    goto/32 :goto_1b

    nop

    :goto_0
    invoke-virtual {v0, v1, p1, v2}, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->bind(Lcom/android/settings/applications/RunningState;Lcom/android/settings/applications/RunningState$BaseItem;Ljava/lang/StringBuilder;)Lcom/android/settings/applications/RunningProcessesView$ActiveItem;

    move-result-object p1

    goto/32 :goto_1c

    nop

    :goto_1
    iget-object v2, p0, Lcom/android/settings/applications/RunningServiceDetails;->mBuilder:Ljava/lang/StringBuilder;

    goto/32 :goto_0

    nop

    :goto_2
    iget-object p1, p1, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->description:Landroid/widget/TextView;

    goto/32 :goto_1f

    nop

    :goto_3
    iget-object p1, p0, Lcom/android/settings/applications/RunningServiceDetails;->mSnippetActiveItem:Lcom/android/settings/applications/RunningProcessesView$ActiveItem;

    goto/32 :goto_20

    nop

    :goto_4
    const/4 p1, 0x1

    :goto_5
    goto/32 :goto_a

    nop

    :goto_6
    iget-object p1, p1, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->mHolder:Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

    goto/32 :goto_16

    nop

    :goto_7
    iget-object p1, p0, Lcom/android/settings/applications/RunningServiceDetails;->mSnippetActiveItem:Lcom/android/settings/applications/RunningProcessesView$ActiveItem;

    goto/32 :goto_f

    nop

    :goto_8
    invoke-direct {p0}, Lcom/android/settings/applications/RunningServiceDetails;->finish()V

    :goto_9
    goto/32 :goto_d

    nop

    :goto_a
    if-nez p1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_18

    nop

    :goto_b
    goto/16 :goto_23

    :goto_c
    goto/32 :goto_3

    nop

    :goto_d
    return-void

    :goto_e
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_7

    nop

    :goto_f
    iget-object p1, p1, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->mHolder:Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

    goto/32 :goto_2

    nop

    :goto_10
    iget-object p1, p1, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->uptime:Landroid/widget/TextView;

    goto/32 :goto_e

    nop

    :goto_11
    const-string v0, ""

    goto/32 :goto_21

    nop

    :goto_12
    if-nez v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_4

    nop

    :goto_13
    invoke-virtual {p0}, Lcom/android/settings/applications/RunningServiceDetails;->addDetailViews()V

    goto/32 :goto_19

    nop

    :goto_14
    if-nez p1, :cond_2

    goto/32 :goto_c

    :cond_2
    goto/32 :goto_15

    nop

    :goto_15
    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mSnippetViewHolder:Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

    goto/32 :goto_1e

    nop

    :goto_16
    iget-object p1, p1, Lcom/android/settings/applications/RunningProcessesView$ViewHolder;->size:Landroid/widget/TextView;

    goto/32 :goto_11

    nop

    :goto_17
    iget-object p1, p1, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->mHolder:Lcom/android/settings/applications/RunningProcessesView$ViewHolder;

    goto/32 :goto_10

    nop

    :goto_18
    iget-object p1, p0, Lcom/android/settings/applications/RunningServiceDetails;->mMergedItem:Lcom/android/settings/applications/RunningState$MergedItem;

    goto/32 :goto_14

    nop

    :goto_19
    goto :goto_9

    :goto_1a
    goto/32 :goto_8

    nop

    :goto_1b
    invoke-virtual {p0}, Lcom/android/settings/applications/RunningServiceDetails;->findMergedItem()Z

    move-result v0

    goto/32 :goto_12

    nop

    :goto_1c
    iput-object p1, p0, Lcom/android/settings/applications/RunningServiceDetails;->mSnippetActiveItem:Lcom/android/settings/applications/RunningProcessesView$ActiveItem;

    goto/32 :goto_b

    nop

    :goto_1d
    iget-object p1, p0, Lcom/android/settings/applications/RunningServiceDetails;->mSnippetActiveItem:Lcom/android/settings/applications/RunningProcessesView$ActiveItem;

    goto/32 :goto_17

    nop

    :goto_1e
    iget-object v1, p0, Lcom/android/settings/applications/RunningServiceDetails;->mState:Lcom/android/settings/applications/RunningState;

    goto/32 :goto_1

    nop

    :goto_1f
    sget v0, Lcom/android/settings/R$string;->no_services:I

    goto/32 :goto_22

    nop

    :goto_20
    if-nez p1, :cond_3

    goto/32 :goto_1a

    :cond_3
    goto/32 :goto_6

    nop

    :goto_21
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_1d

    nop

    :goto_22
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    :goto_23
    goto/32 :goto_13

    nop
.end method

.method updateTimes()V
    .locals 4

    goto/32 :goto_6

    nop

    :goto_0
    iget-object v2, p0, Lcom/android/settings/applications/RunningServiceDetails;->mBuilder:Ljava/lang/StringBuilder;

    goto/32 :goto_4

    nop

    :goto_1
    goto :goto_10

    :goto_2
    goto/32 :goto_3

    nop

    :goto_3
    return-void

    :goto_4
    invoke-virtual {v0, v1, v2}, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->updateTime(Landroid/content/Context;Ljava/lang/StringBuilder;)V

    :goto_5
    goto/32 :goto_f

    nop

    :goto_6
    iget-object v0, p0, Lcom/android/settings/applications/RunningServiceDetails;->mSnippetActiveItem:Lcom/android/settings/applications/RunningProcessesView$ActiveItem;

    goto/32 :goto_14

    nop

    :goto_7
    invoke-virtual {v1, v2, v3}, Lcom/android/settings/applications/RunningProcessesView$ActiveItem;->updateTime(Landroid/content/Context;Ljava/lang/StringBuilder;)V

    goto/32 :goto_8

    nop

    :goto_8
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_1

    nop

    :goto_9
    iget-object v3, p0, Lcom/android/settings/applications/RunningServiceDetails;->mBuilder:Ljava/lang/StringBuilder;

    goto/32 :goto_7

    nop

    :goto_a
    check-cast v1, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;

    goto/32 :goto_c

    nop

    :goto_b
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_d

    nop

    :goto_c
    iget-object v1, v1, Lcom/android/settings/applications/RunningServiceDetails$ActiveDetail;->mActiveItem:Lcom/android/settings/applications/RunningProcessesView$ActiveItem;

    goto/32 :goto_15

    nop

    :goto_d
    if-lt v0, v1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_e

    nop

    :goto_e
    iget-object v1, p0, Lcom/android/settings/applications/RunningServiceDetails;->mActiveDetails:Ljava/util/ArrayList;

    goto/32 :goto_11

    nop

    :goto_f
    const/4 v0, 0x0

    :goto_10
    goto/32 :goto_13

    nop

    :goto_11
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_a

    nop

    :goto_12
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_13
    iget-object v1, p0, Lcom/android/settings/applications/RunningServiceDetails;->mActiveDetails:Ljava/util/ArrayList;

    goto/32 :goto_b

    nop

    :goto_14
    if-nez v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_12

    nop

    :goto_15
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    goto/32 :goto_9

    nop
.end method
