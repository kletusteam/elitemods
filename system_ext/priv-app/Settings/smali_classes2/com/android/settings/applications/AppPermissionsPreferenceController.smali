.class public Lcom/android/settings/applications/AppPermissionsPreferenceController;
.super Lcom/android/settings/core/BasePreferenceController;


# static fields
.field private static final NUM_PACKAGE_TO_CHECK:I = 0x4

.field static NUM_PERMISSIONS_TO_SHOW:I = 0x3

.field private static final TAG:Ljava/lang/String; = "AppPermissionPrefCtrl"


# instance fields
.field mNumPackageChecked:I

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private final mPermissionGroups:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private final mPermissionsCallback:Lcom/android/settingslib/applications/PermissionsSummaryHelper$PermissionsResultCallback;

.field private mPreference:Landroidx/preference/Preference;


# direct methods
.method public static synthetic $r8$lambda$A9jYkqHYhwPyfqkhOOWO2l74udY(Landroid/content/pm/PackageInfo;)Z
    .locals 0

    invoke-static {p0}, Lcom/android/settings/applications/AppPermissionsPreferenceController;->lambda$queryPermissionSummary$0(Landroid/content/pm/PackageInfo;)Z

    move-result p0

    return p0
.end method

.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    new-instance p2, Lcom/android/settings/applications/AppPermissionsPreferenceController$1;

    invoke-direct {p2, p0}, Lcom/android/settings/applications/AppPermissionsPreferenceController$1;-><init>(Lcom/android/settings/applications/AppPermissionsPreferenceController;)V

    iput-object p2, p0, Lcom/android/settings/applications/AppPermissionsPreferenceController;->mPermissionsCallback:Lcom/android/settingslib/applications/PermissionsSummaryHelper$PermissionsResultCallback;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/applications/AppPermissionsPreferenceController;->mPackageManager:Landroid/content/pm/PackageManager;

    new-instance p1, Landroid/util/ArraySet;

    invoke-direct {p1}, Landroid/util/ArraySet;-><init>()V

    iput-object p1, p0, Lcom/android/settings/applications/AppPermissionsPreferenceController;->mPermissionGroups:Ljava/util/Set;

    return-void
.end method

.method private static synthetic lambda$queryPermissionSummary$0(Landroid/content/pm/PackageInfo;)Z
    .locals 0

    iget-object p0, p0, Landroid/content/pm/PackageInfo;->permissions:[Landroid/content/pm/PermissionInfo;

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method


# virtual methods
.method public getAvailabilityStatus()I
    .locals 0

    invoke-static {}, Landroid/miui/AppOpsUtils;->isXOptMode()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x3

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public bridge synthetic getBackgroundWorkerClass()Ljava/lang/Class;
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->getBackgroundWorkerClass()Ljava/lang/Class;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic getIntentFilter()Landroid/content/IntentFilter;
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->getIntentFilter()Landroid/content/IntentFilter;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic getSliceHighlightMenuRes()I
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->getSliceHighlightMenuRes()I

    move-result p0

    return p0
.end method

.method public bridge synthetic hasAsyncUpdate()Z
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->hasAsyncUpdate()Z

    move-result p0

    return p0
.end method

.method public bridge synthetic isPublicSlice()Z
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->isPublicSlice()Z

    move-result p0

    return p0
.end method

.method public bridge synthetic isSliceable()Z
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->isSliceable()Z

    move-result p0

    return p0
.end method

.method queryPermissionSummary()V
    .locals 4

    goto/32 :goto_6

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_16

    nop

    :goto_1
    invoke-interface {v0, v1}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v0

    goto/32 :goto_17

    nop

    :goto_2
    check-cast v0, Ljava/util/List;

    goto/32 :goto_a

    nop

    :goto_3
    invoke-interface {v0, v1, v2}, Ljava/util/stream/Stream;->limit(J)Ljava/util/stream/Stream;

    move-result-object v0

    goto/32 :goto_15

    nop

    :goto_4
    new-instance v1, Lcom/android/settings/applications/AppPermissionsPreferenceController$$ExternalSyntheticLambda0;

    goto/32 :goto_18

    nop

    :goto_5
    const/16 v1, 0x1000

    goto/32 :goto_11

    nop

    :goto_6
    iget-object v0, p0, Lcom/android/settings/applications/AppPermissionsPreferenceController;->mPackageManager:Landroid/content/pm/PackageManager;

    goto/32 :goto_5

    nop

    :goto_7
    invoke-interface {v0, v1}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_8
    goto :goto_b

    :goto_9
    goto/32 :goto_14

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_b
    goto/32 :goto_c

    nop

    :goto_c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_e

    nop

    :goto_d
    invoke-static {v2, v1, v3}, Lcom/android/settingslib/applications/PermissionsSummaryHelper;->getPermissionSummary(Landroid/content/Context;Ljava/lang/String;Lcom/android/settingslib/applications/PermissionsSummaryHelper$PermissionsResultCallback;)V

    goto/32 :goto_8

    nop

    :goto_e
    if-nez v1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_0

    nop

    :goto_f
    iget-object v2, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    goto/32 :goto_12

    nop

    :goto_10
    invoke-interface {v0}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_11
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v0

    goto/32 :goto_10

    nop

    :goto_12
    iget-object v1, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    goto/32 :goto_13

    nop

    :goto_13
    iget-object v3, p0, Lcom/android/settings/applications/AppPermissionsPreferenceController;->mPermissionsCallback:Lcom/android/settingslib/applications/PermissionsSummaryHelper$PermissionsResultCallback;

    goto/32 :goto_d

    nop

    :goto_14
    return-void

    :goto_15
    invoke-static {}, Ljava/util/stream/Collectors;->toList()Ljava/util/stream/Collector;

    move-result-object v1

    goto/32 :goto_7

    nop

    :goto_16
    check-cast v1, Landroid/content/pm/PackageInfo;

    goto/32 :goto_f

    nop

    :goto_17
    const-wide/16 v1, 0x4

    goto/32 :goto_3

    nop

    :goto_18
    invoke-direct {v1}, Lcom/android/settings/applications/AppPermissionsPreferenceController$$ExternalSyntheticLambda0;-><init>()V

    goto/32 :goto_1

    nop
.end method

.method public updateState(Landroidx/preference/Preference;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/applications/AppPermissionsPreferenceController;->mPreference:Landroidx/preference/Preference;

    const/4 p1, 0x0

    iput p1, p0, Lcom/android/settings/applications/AppPermissionsPreferenceController;->mNumPackageChecked:I

    invoke-virtual {p0}, Lcom/android/settings/applications/AppPermissionsPreferenceController;->queryPermissionSummary()V

    return-void
.end method

.method updateSummary(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    aput-object p1, v0, v3

    goto/32 :goto_14

    nop

    :goto_1
    const/4 v3, 0x0

    goto/32 :goto_37

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/settings/applications/AppPermissionsPreferenceController;->mPermissionGroups:Ljava/util/Set;

    goto/32 :goto_33

    nop

    :goto_3
    sget v0, Lcom/android/settings/R$string;->runtime_permissions_summary_no_permissions_granted:I

    goto/32 :goto_19

    nop

    :goto_4
    iget-object p1, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    goto/32 :goto_3

    nop

    :goto_5
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_0

    nop

    :goto_6
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_26

    nop

    :goto_7
    check-cast p1, Ljava/util/List;

    goto/32 :goto_28

    nop

    :goto_8
    invoke-interface {p1, v1, v2}, Ljava/util/stream/Stream;->limit(J)Ljava/util/stream/Stream;

    move-result-object p1

    goto/32 :goto_2f

    nop

    :goto_9
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_24

    nop

    :goto_a
    if-nez v1, :cond_0

    goto/32 :goto_25

    :cond_0
    goto/32 :goto_23

    nop

    :goto_b
    if-eqz v2, :cond_1

    goto/32 :goto_16

    :cond_1
    goto/32 :goto_a

    nop

    :goto_c
    iput p1, p0, Lcom/android/settings/applications/AppPermissionsPreferenceController;->mNumPackageChecked:I

    goto/32 :goto_27

    nop

    :goto_d
    invoke-interface {p1, v1}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_7

    nop

    :goto_e
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_29

    nop

    :goto_f
    return-void

    :goto_10
    invoke-interface {p1}, Ljava/util/Set;->stream()Ljava/util/stream/Stream;

    move-result-object p1

    goto/32 :goto_11

    nop

    :goto_11
    sget v1, Lcom/android/settings/applications/AppPermissionsPreferenceController;->NUM_PERMISSIONS_TO_SHOW:I

    goto/32 :goto_17

    nop

    :goto_12
    goto :goto_21

    :goto_13
    goto/32 :goto_20

    nop

    :goto_14
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_15

    nop

    :goto_15
    goto :goto_1a

    :goto_16
    goto/32 :goto_4

    nop

    :goto_17
    int-to-long v1, v1

    goto/32 :goto_8

    nop

    :goto_18
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_38

    nop

    :goto_19
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_1a
    goto/32 :goto_22

    nop

    :goto_1b
    return-void

    :goto_1c
    goto/32 :goto_34

    nop

    :goto_1d
    sget v2, Lcom/android/settings/applications/AppPermissionsPreferenceController;->NUM_PERMISSIONS_TO_SHOW:I

    goto/32 :goto_1

    nop

    :goto_1e
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    goto/32 :goto_1d

    nop

    :goto_1f
    invoke-virtual {v4, p1}, Landroid/icu/text/ListFormatter;->format(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_e

    nop

    :goto_20
    move v1, v3

    :goto_21
    goto/32 :goto_30

    nop

    :goto_22
    iget-object p0, p0, Lcom/android/settings/applications/AppPermissionsPreferenceController;->mPreference:Landroidx/preference/Preference;

    goto/32 :goto_31

    nop

    :goto_23
    iget-object v1, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    goto/32 :goto_36

    nop

    :goto_24
    goto :goto_1a

    :goto_25
    goto/32 :goto_39

    nop

    :goto_26
    invoke-static {}, Landroid/icu/text/ListFormatter;->getInstance()Landroid/icu/text/ListFormatter;

    move-result-object v4

    goto/32 :goto_35

    nop

    :goto_27
    const/4 v1, 0x4

    goto/32 :goto_2e

    nop

    :goto_28
    iget-object v1, p0, Lcom/android/settings/applications/AppPermissionsPreferenceController;->mPermissionGroups:Ljava/util/Set;

    goto/32 :goto_1e

    nop

    :goto_29
    aput-object p1, v0, v3

    goto/32 :goto_9

    nop

    :goto_2a
    iget p1, p0, Lcom/android/settings/applications/AppPermissionsPreferenceController;->mNumPackageChecked:I

    goto/32 :goto_2c

    nop

    :goto_2b
    add-int/2addr p1, v0

    goto/32 :goto_c

    nop

    :goto_2c
    const/4 v0, 0x1

    goto/32 :goto_2b

    nop

    :goto_2d
    move v1, v0

    goto/32 :goto_12

    nop

    :goto_2e
    if-lt p1, v1, :cond_2

    goto/32 :goto_1c

    :cond_2
    goto/32 :goto_1b

    nop

    :goto_2f
    invoke-static {}, Ljava/util/stream/Collectors;->toList()Ljava/util/stream/Collector;

    move-result-object v1

    goto/32 :goto_d

    nop

    :goto_30
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    goto/32 :goto_b

    nop

    :goto_31
    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/32 :goto_f

    nop

    :goto_32
    sget v2, Lcom/android/settings/R$string;->app_permissions_summary:I

    goto/32 :goto_6

    nop

    :goto_33
    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto/32 :goto_2a

    nop

    :goto_34
    iget-object p1, p0, Lcom/android/settings/applications/AppPermissionsPreferenceController;->mPermissionGroups:Ljava/util/Set;

    goto/32 :goto_10

    nop

    :goto_35
    invoke-virtual {v4, p1}, Landroid/icu/text/ListFormatter;->format(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_5

    nop

    :goto_36
    sget v2, Lcom/android/settings/R$string;->app_permissions_summary_more:I

    goto/32 :goto_18

    nop

    :goto_37
    if-gt v1, v2, :cond_3

    goto/32 :goto_13

    :cond_3
    goto/32 :goto_2d

    nop

    :goto_38
    invoke-static {}, Landroid/icu/text/ListFormatter;->getInstance()Landroid/icu/text/ListFormatter;

    move-result-object v4

    goto/32 :goto_1f

    nop

    :goto_39
    iget-object v1, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    goto/32 :goto_32

    nop
.end method

.method public bridge synthetic useDynamicSliceSummary()Z
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->useDynamicSliceSummary()Z

    move-result p0

    return p0
.end method
