.class Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$AppInfoModel;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AppInfoModel"
.end annotation


# instance fields
.field public icon:Landroid/graphics/drawable/Drawable;

.field public packageName:Ljava/lang/String;

.field summary:I

.field public title:Ljava/lang/CharSequence;

.field public uid:I


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;ILjava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$AppInfoModel;->icon:Landroid/graphics/drawable/Drawable;

    iput-object p2, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$AppInfoModel;->title:Ljava/lang/CharSequence;

    iput p3, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$AppInfoModel;->summary:I

    iput-object p4, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$AppInfoModel;->packageName:Ljava/lang/String;

    iput p5, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$AppInfoModel;->uid:I

    return-void
.end method
