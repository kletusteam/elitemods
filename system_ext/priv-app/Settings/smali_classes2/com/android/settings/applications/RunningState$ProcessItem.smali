.class Lcom/android/settings/applications/RunningState$ProcessItem;
.super Lcom/android/settings/applications/RunningState$BaseItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/applications/RunningState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ProcessItem"
.end annotation


# instance fields
.field mActiveSince:J

.field mClient:Lcom/android/settings/applications/RunningState$ProcessItem;

.field final mDependentProcesses:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/android/settings/applications/RunningState$ProcessItem;",
            ">;"
        }
    .end annotation
.end field

.field mInteresting:Z

.field mIsStarted:Z

.field mIsSystem:Z

.field mLastNumDependentProcesses:I

.field mMergedItem:Lcom/android/settings/applications/RunningState$MergedItem;

.field mPid:I

.field final mProcessName:Ljava/lang/String;

.field mRunningProcessInfo:Landroid/app/ActivityManager$RunningAppProcessInfo;

.field mRunningSeq:I

.field final mServices:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Landroid/content/ComponentName;",
            "Lcom/android/settings/applications/RunningState$ServiceItem;",
            ">;"
        }
    .end annotation
.end field

.field final mUid:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .locals 3

    invoke-static {p2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/android/settings/applications/RunningState$BaseItem;-><init>(ZI)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->mServices:Ljava/util/HashMap;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->mDependentProcesses:Landroid/util/SparseArray;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/android/settings/R$string;->service_process_name:I

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mDescription:Ljava/lang/String;

    iput p2, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->mUid:I

    iput-object p3, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->mProcessName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method addDependentProcesses(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/applications/RunningState$BaseItem;",
            ">;",
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/applications/RunningState$ProcessItem;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_3

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    goto/32 :goto_4

    nop

    :goto_3
    iget-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->mDependentProcesses:Landroid/util/SparseArray;

    goto/32 :goto_7

    nop

    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_c

    nop

    :goto_5
    iget-object v2, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->mDependentProcesses:Landroid/util/SparseArray;

    goto/32 :goto_b

    nop

    :goto_6
    if-gtz v3, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_7
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    goto/32 :goto_e

    nop

    :goto_8
    check-cast v2, Lcom/android/settings/applications/RunningState$ProcessItem;

    goto/32 :goto_9

    nop

    :goto_9
    invoke-virtual {v2, p1, p2}, Lcom/android/settings/applications/RunningState$ProcessItem;->addDependentProcesses(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto/32 :goto_11

    nop

    :goto_a
    iget v3, v2, Lcom/android/settings/applications/RunningState$ProcessItem;->mPid:I

    goto/32 :goto_6

    nop

    :goto_b
    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_8

    nop

    :goto_c
    goto :goto_f

    :goto_d
    goto/32 :goto_0

    nop

    :goto_e
    const/4 v1, 0x0

    :goto_f
    goto/32 :goto_10

    nop

    :goto_10
    if-lt v1, v0, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_5

    nop

    :goto_11
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_a

    nop
.end method

.method buildDependencyChain(Landroid/content/Context;Landroid/content/pm/PackageManager;I)Z
    .locals 6

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->mDependentProcesses:Landroid/util/SparseArray;

    goto/32 :goto_1b

    nop

    :goto_1
    iget-object v4, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->mDependentProcesses:Landroid/util/SparseArray;

    goto/32 :goto_16

    nop

    :goto_2
    iget-object p1, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->mDependentProcesses:Landroid/util/SparseArray;

    goto/32 :goto_1a

    nop

    :goto_3
    invoke-virtual {p2}, Landroid/util/SparseArray;->size()I

    move-result p2

    goto/32 :goto_17

    nop

    :goto_4
    move v2, v3

    :goto_5
    goto/32 :goto_19

    nop

    :goto_6
    move v2, v1

    :goto_7
    goto/32 :goto_f

    nop

    :goto_8
    iget-object v5, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->mClient:Lcom/android/settings/applications/RunningState$ProcessItem;

    goto/32 :goto_1d

    nop

    :goto_9
    or-int/2addr v2, v3

    goto/32 :goto_11

    nop

    :goto_a
    if-lt v1, v0, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_1

    nop

    :goto_b
    move v2, v3

    :goto_c
    goto/32 :goto_e

    nop

    :goto_d
    check-cast v4, Lcom/android/settings/applications/RunningState$ProcessItem;

    goto/32 :goto_8

    nop

    :goto_e
    iput p3, v4, Lcom/android/settings/applications/RunningState$BaseItem;->mCurSeq:I

    goto/32 :goto_18

    nop

    :goto_f
    const/4 v3, 0x1

    goto/32 :goto_a

    nop

    :goto_10
    iget-object p2, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->mDependentProcesses:Landroid/util/SparseArray;

    goto/32 :goto_3

    nop

    :goto_11
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_13

    nop

    :goto_12
    invoke-virtual {v4, p1, p2, p3}, Lcom/android/settings/applications/RunningState$ProcessItem;->buildDependencyChain(Landroid/content/Context;Landroid/content/pm/PackageManager;I)Z

    move-result v3

    goto/32 :goto_9

    nop

    :goto_13
    goto :goto_7

    :goto_14
    goto/32 :goto_1f

    nop

    :goto_15
    iput p1, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->mLastNumDependentProcesses:I

    goto/32 :goto_4

    nop

    :goto_16
    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_d

    nop

    :goto_17
    if-ne p1, p2, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_2

    nop

    :goto_18
    invoke-virtual {v4, p2}, Lcom/android/settings/applications/RunningState$ProcessItem;->ensureLabel(Landroid/content/pm/PackageManager;)V

    goto/32 :goto_12

    nop

    :goto_19
    return v2

    :goto_1a
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result p1

    goto/32 :goto_15

    nop

    :goto_1b
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    goto/32 :goto_1c

    nop

    :goto_1c
    const/4 v1, 0x0

    goto/32 :goto_6

    nop

    :goto_1d
    if-ne v5, p0, :cond_2

    goto/32 :goto_c

    :cond_2
    goto/32 :goto_1e

    nop

    :goto_1e
    iput-object p0, v4, Lcom/android/settings/applications/RunningState$ProcessItem;->mClient:Lcom/android/settings/applications/RunningState$ProcessItem;

    goto/32 :goto_b

    nop

    :goto_1f
    iget p1, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->mLastNumDependentProcesses:I

    goto/32 :goto_10

    nop
.end method

.method ensureLabel(Landroid/content/pm/PackageManager;)V
    .locals 10

    goto/32 :goto_2

    nop

    :goto_0
    iget v1, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->mUid:I

    goto/32 :goto_24

    nop

    :goto_1
    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    goto/32 :goto_2b

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mLabel:Ljava/lang/String;

    goto/32 :goto_e

    nop

    :goto_3
    iput-object p1, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mLabel:Ljava/lang/String;

    goto/32 :goto_10

    nop

    :goto_4
    array-length v4, v1

    goto/32 :goto_22

    nop

    :goto_5
    array-length v4, v1

    goto/32 :goto_26

    nop

    :goto_6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_c

    nop

    :goto_7
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_17

    nop

    :goto_8
    if-eqz v1, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_13

    nop

    :goto_9
    invoke-virtual {v0, p1}, Landroid/content/pm/PackageItemInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object p1

    goto/32 :goto_15

    nop

    :goto_a
    new-array v1, v3, [Ljava/lang/String;

    :goto_b
    goto/32 :goto_4

    nop

    :goto_c
    const-string v4, "getPackagesForUid return null for uid: "

    goto/32 :goto_33

    nop

    :goto_d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_2c

    nop

    :goto_e
    if-nez v0, :cond_1

    goto/32 :goto_32

    :cond_1
    goto/32 :goto_31

    nop

    :goto_f
    const/4 v3, 0x0

    goto/32 :goto_8

    nop

    :goto_10
    return-void

    :goto_11
    :try_start_0
    array-length v4, v1

    if-nez v4, :cond_2

    const-string/jumbo p0, "pkgs is empty"

    invoke-static {v2, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_2
    aget-object v1, v1, v3

    invoke-virtual {p1, v1, v0}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mDisplayLabel:Ljava/lang/CharSequence;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mLabel:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mPackageInfo:Landroid/content/pm/PackageItemInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    goto/32 :goto_16

    nop

    :goto_12
    iget-object v0, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->mServices:Ljava/util/HashMap;

    goto/32 :goto_1

    nop

    :goto_13
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_14
    iget v4, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->mUid:I

    goto/32 :goto_1b

    nop

    :goto_15
    iput-object p1, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mDisplayLabel:Ljava/lang/CharSequence;

    goto/32 :goto_1a

    nop

    :goto_16
    return-void

    :goto_17
    check-cast v0, Lcom/android/settings/applications/RunningState$ServiceItem;

    goto/32 :goto_1e

    nop

    :goto_18
    iget-object v4, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->mServices:Ljava/util/HashMap;

    goto/32 :goto_21

    nop

    :goto_19
    if-eq v4, v5, :cond_3

    goto/32 :goto_20

    :cond_3
    :try_start_1
    aget-object v4, v1, v3

    invoke-virtual {p1, v4, v0}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    iput-object v5, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mDisplayLabel:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mLabel:Ljava/lang/String;

    iput-object v4, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mPackageInfo:Landroid/content/pm/PackageItemInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/32 :goto_1f

    nop

    :goto_1a
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_3

    nop

    :goto_1b
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_d

    nop

    :goto_1c
    if-gtz v4, :cond_4

    goto/32 :goto_11

    :cond_4
    goto/32 :goto_12

    nop

    :goto_1d
    const/high16 v0, 0x400000

    :try_start_2
    iget-object v1, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->mProcessName:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v2, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    iget v3, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->mUid:I

    if-ne v2, v3, :cond_5

    invoke-virtual {v1, p1}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mDisplayLabel:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mLabel:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mPackageInfo:Landroid/content/pm/PackageItemInfo;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto/32 :goto_25

    nop

    :goto_1e
    iget-object v0, v0, Lcom/android/settings/applications/RunningState$ServiceItem;->mServiceInfo:Landroid/content/pm/ServiceInfo;

    goto/32 :goto_2a

    nop

    :goto_1f
    return-void

    :catch_1
    :goto_20
    goto/32 :goto_5

    nop

    :goto_21
    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    goto/32 :goto_1c

    nop

    :goto_22
    const/4 v5, 0x1

    goto/32 :goto_19

    nop

    :goto_23
    iput-object v0, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mPackageInfo:Landroid/content/pm/PackageItemInfo;

    goto/32 :goto_9

    nop

    :goto_24
    invoke-virtual {p1, v1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_2f

    nop

    :goto_25
    return-void

    :catch_2
    :cond_5
    goto/32 :goto_0

    nop

    :goto_26
    move v5, v3

    :goto_27
    goto/32 :goto_30

    nop

    :goto_28
    return-void

    :catch_3
    :cond_6
    goto/32 :goto_29

    nop

    :goto_29
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_2d

    nop

    :goto_2a
    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    goto/32 :goto_23

    nop

    :goto_2b
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_2c
    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_a

    nop

    :goto_2d
    goto :goto_27

    :goto_2e
    goto/32 :goto_18

    nop

    :goto_2f
    const-string v2, "RunningState"

    goto/32 :goto_f

    nop

    :goto_30
    if-lt v5, v4, :cond_7

    goto/32 :goto_2e

    :cond_7
    goto/32 :goto_34

    nop

    :goto_31
    return-void

    :goto_32
    goto/32 :goto_1d

    nop

    :goto_33
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_14

    nop

    :goto_34
    aget-object v6, v1, v5

    :try_start_3
    invoke-virtual {p1, v6, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v7

    iget v8, v7, Landroid/content/pm/PackageInfo;->sharedUserLabel:I

    if-eqz v8, :cond_6

    iget-object v9, v7, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {p1, v6, v8, v9}, Landroid/content/pm/PackageManager;->getText(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v6

    if-eqz v6, :cond_6

    iput-object v6, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mDisplayLabel:Ljava/lang/CharSequence;

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mLabel:Ljava/lang/String;

    iget-object v6, v7, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iput-object v6, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mPackageInfo:Landroid/content/pm/PackageItemInfo;
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    goto/32 :goto_28

    nop
.end method

.method updateService(Landroid/content/Context;Landroid/app/ActivityManager$RunningServiceInfo;)Z
    .locals 10

    goto/32 :goto_17

    nop

    :goto_0
    check-cast v1, Lcom/android/settings/applications/RunningState$ServiceItem;

    goto/32 :goto_22

    nop

    :goto_1
    if-nez v6, :cond_0

    goto/32 :goto_1f

    :cond_0
    goto/32 :goto_31

    nop

    :goto_2
    iget v6, p2, Landroid/app/ActivityManager$RunningServiceInfo;->clientLabel:I

    goto/32 :goto_2e

    nop

    :goto_3
    return v3

    :catch_0
    :cond_1
    goto/32 :goto_53

    nop

    :goto_4
    invoke-virtual {v5, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_42

    nop

    :goto_5
    goto/16 :goto_50

    :goto_6
    goto/32 :goto_4f

    nop

    :goto_7
    cmp-long p0, v6, v8

    goto/32 :goto_16

    nop

    :goto_8
    const/4 v3, 0x0

    goto/32 :goto_10

    nop

    :goto_9
    iput-object p0, v1, Lcom/android/settings/applications/RunningState$BaseItem;->mDescription:Ljava/lang/String;

    goto/32 :goto_43

    nop

    :goto_a
    goto/16 :goto_44

    :goto_b
    goto/32 :goto_c

    nop

    :goto_c
    iget-boolean p0, v1, Lcom/android/settings/applications/RunningState$ServiceItem;->mShownAsStarted:Z

    goto/32 :goto_1d

    nop

    :goto_d
    return v3

    :goto_e
    goto/32 :goto_33

    nop

    :goto_f
    iput-object p2, v1, Lcom/android/settings/applications/RunningState$ServiceItem;->mRunningService:Landroid/app/ActivityManager$RunningServiceInfo;

    :try_start_0
    invoke-static {}, Landroid/app/ActivityThread;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v5

    iget-object v6, p2, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    const-wide/32 v7, 0x400000

    iget v9, p2, Landroid/app/ActivityManager$RunningServiceInfo;->uid:I

    invoke-static {v9}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v9

    invoke-interface {v5, v6, v7, v8, v9}, Landroid/content/pm/IPackageManager;->getServiceInfo(Landroid/content/ComponentName;JI)Landroid/content/pm/ServiceInfo;

    move-result-object v5

    iput-object v5, v1, Lcom/android/settings/applications/RunningState$ServiceItem;->mServiceInfo:Landroid/content/pm/ServiceInfo;

    if-nez v5, :cond_1

    const-string v5, "RunningService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getServiceInfo returned null for: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, p2, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_3

    nop

    :goto_10
    const/4 v4, 0x1

    goto/32 :goto_3c

    nop

    :goto_11
    iput-boolean v4, v1, Lcom/android/settings/applications/RunningState$ServiceItem;->mShownAsStarted:Z

    goto/32 :goto_55

    nop

    :goto_12
    move v5, v3

    :goto_13
    goto/32 :goto_3b

    nop

    :goto_14
    iget-object v5, v5, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    goto/32 :goto_2f

    nop

    :goto_15
    iput-object v5, v1, Lcom/android/settings/applications/RunningState$BaseItem;->mDisplayLabel:Ljava/lang/CharSequence;

    goto/32 :goto_2d

    nop

    :goto_16
    if-eqz p0, :cond_2

    goto/32 :goto_4c

    :cond_2
    goto/32 :goto_3d

    nop

    :goto_17
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    goto/32 :goto_21

    nop

    :goto_18
    new-instance v1, Lcom/android/settings/applications/RunningState$ServiceItem;

    goto/32 :goto_36

    nop

    :goto_19
    iget-object v5, v1, Lcom/android/settings/applications/RunningState$ServiceItem;->mServiceInfo:Landroid/content/pm/ServiceInfo;

    goto/32 :goto_14

    nop

    :goto_1a
    const-wide/16 v8, 0x0

    goto/32 :goto_7

    nop

    :goto_1b
    iput p0, v1, Lcom/android/settings/applications/RunningState$BaseItem;->mCurSeq:I

    goto/32 :goto_2c

    nop

    :goto_1c
    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_5

    nop

    :goto_1d
    if-eqz p0, :cond_3

    goto/32 :goto_56

    :cond_3
    goto/32 :goto_11

    nop

    :goto_1e
    move v5, v4

    :goto_1f
    :try_start_1
    invoke-virtual {v0, p0}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object p0

    iget p2, p2, Landroid/app/ActivityManager$RunningServiceInfo;->clientLabel:I

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/android/settings/R$string;->service_client_name:I

    new-array v0, v4, [Ljava/lang/Object;

    aput-object p0, v0, v3

    invoke-virtual {p1, p2, v0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v1, Lcom/android/settings/applications/RunningState$BaseItem;->mDescription:Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/32 :goto_35

    nop

    :goto_20
    iget-boolean v6, v1, Lcom/android/settings/applications/RunningState$ServiceItem;->mShownAsStarted:Z

    goto/32 :goto_1

    nop

    :goto_21
    iget-object v1, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->mServices:Ljava/util/HashMap;

    goto/32 :goto_4d

    nop

    :goto_22
    const/4 v2, 0x0

    goto/32 :goto_8

    nop

    :goto_23
    iget-object v5, v5, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    goto/32 :goto_2a

    nop

    :goto_24
    iget-wide v6, p2, Landroid/app/ActivityManager$RunningServiceInfo;->restarting:J

    goto/32 :goto_1a

    nop

    :goto_25
    invoke-direct {v1, v5}, Lcom/android/settings/applications/RunningState$ServiceItem;-><init>(I)V

    goto/32 :goto_f

    nop

    :goto_26
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    goto/32 :goto_45

    nop

    :goto_27
    move v5, v4

    :goto_28
    goto/32 :goto_46

    nop

    :goto_29
    iget-object v5, p0, Lcom/android/settings/applications/RunningState$ProcessItem;->mServices:Ljava/util/HashMap;

    goto/32 :goto_37

    nop

    :goto_2a
    invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_4e

    nop

    :goto_2b
    iput-object v5, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mLabel:Ljava/lang/String;

    goto/32 :goto_19

    nop

    :goto_2c
    iput-object p2, v1, Lcom/android/settings/applications/RunningState$ServiceItem;->mRunningService:Landroid/app/ActivityManager$RunningServiceInfo;

    goto/32 :goto_24

    nop

    :goto_2d
    iget-object v5, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mDisplayLabel:Ljava/lang/CharSequence;

    goto/32 :goto_57

    nop

    :goto_2e
    if-nez v6, :cond_4

    goto/32 :goto_b

    :cond_4
    goto/32 :goto_20

    nop

    :goto_2f
    iput-object v5, v1, Lcom/android/settings/applications/RunningState$BaseItem;->mPackageInfo:Landroid/content/pm/PackageItemInfo;

    goto/32 :goto_29

    nop

    :goto_30
    cmp-long p0, v8, v6

    goto/32 :goto_52

    nop

    :goto_31
    iput-boolean v3, v1, Lcom/android/settings/applications/RunningState$ServiceItem;->mShownAsStarted:Z

    goto/32 :goto_1e

    nop

    :goto_32
    iput-object v2, v1, Lcom/android/settings/applications/RunningState$BaseItem;->mDescription:Ljava/lang/String;

    goto/32 :goto_a

    nop

    :goto_33
    iget-object v5, v1, Lcom/android/settings/applications/RunningState$ServiceItem;->mRunningService:Landroid/app/ActivityManager$RunningServiceInfo;

    goto/32 :goto_23

    nop

    :goto_34
    iget-wide v8, v1, Lcom/android/settings/applications/RunningState$BaseItem;->mActiveSince:J

    goto/32 :goto_30

    nop

    :goto_35
    goto :goto_44

    :catch_1
    goto/32 :goto_32

    nop

    :goto_36
    iget v5, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mUserId:I

    goto/32 :goto_25

    nop

    :goto_37
    iget-object v6, p2, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    goto/32 :goto_4

    nop

    :goto_38
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_9

    nop

    :goto_39
    if-nez p0, :cond_5

    goto/32 :goto_b

    :cond_5
    goto/32 :goto_2

    nop

    :goto_3a
    invoke-static {v0, v5, v6}, Lcom/android/settings/applications/RunningState;->makeLabel(Landroid/content/pm/PackageManager;Ljava/lang/String;Landroid/content/pm/PackageItemInfo;)Ljava/lang/CharSequence;

    move-result-object v5

    goto/32 :goto_15

    nop

    :goto_3b
    iget p0, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mCurSeq:I

    goto/32 :goto_1b

    nop

    :goto_3c
    if-eqz v1, :cond_6

    goto/32 :goto_48

    :cond_6
    goto/32 :goto_18

    nop

    :goto_3d
    iget-wide v6, p2, Landroid/app/ActivityManager$RunningServiceInfo;->activeSince:J

    goto/32 :goto_4b

    nop

    :goto_3e
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_3f
    const-wide/16 v6, -0x1

    :goto_40
    goto/32 :goto_34

    nop

    :goto_41
    return v5

    :goto_42
    move v5, v4

    goto/32 :goto_47

    nop

    :goto_43
    move v5, v4

    :goto_44
    goto/32 :goto_41

    nop

    :goto_45
    sget p1, Lcom/android/settings/R$string;->service_started_by_app:I

    goto/32 :goto_38

    nop

    :goto_46
    iget-object p0, p2, Landroid/app/ActivityManager$RunningServiceInfo;->clientPackage:Ljava/lang/String;

    goto/32 :goto_39

    nop

    :goto_47
    goto/16 :goto_13

    :goto_48
    goto/32 :goto_12

    nop

    :goto_49
    move v4, v5

    :goto_4a
    goto/32 :goto_26

    nop

    :goto_4b
    goto :goto_40

    :goto_4c
    goto/32 :goto_3f

    nop

    :goto_4d
    iget-object v2, p2, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    goto/32 :goto_3e

    nop

    :goto_4e
    iget-object v6, v1, Lcom/android/settings/applications/RunningState$ServiceItem;->mServiceInfo:Landroid/content/pm/ServiceInfo;

    goto/32 :goto_3a

    nop

    :goto_4f
    move-object v5, v2

    :goto_50
    goto/32 :goto_2b

    nop

    :goto_51
    iput-wide v6, v1, Lcom/android/settings/applications/RunningState$BaseItem;->mActiveSince:J

    goto/32 :goto_27

    nop

    :goto_52
    if-nez p0, :cond_7

    goto/32 :goto_28

    :cond_7
    goto/32 :goto_51

    nop

    :goto_53
    iget-object v5, v1, Lcom/android/settings/applications/RunningState$ServiceItem;->mServiceInfo:Landroid/content/pm/ServiceInfo;

    goto/32 :goto_54

    nop

    :goto_54
    if-eqz v5, :cond_8

    goto/32 :goto_e

    :cond_8
    goto/32 :goto_d

    nop

    :goto_55
    goto :goto_4a

    :goto_56
    goto/32 :goto_49

    nop

    :goto_57
    if-nez v5, :cond_9

    goto/32 :goto_6

    :cond_9
    goto/32 :goto_1c

    nop
.end method

.method updateSize(Landroid/content/Context;JI)Z
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    iput-object p1, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mSizeStr:Ljava/lang/String;

    :goto_1
    goto/32 :goto_b

    nop

    :goto_2
    const-wide/16 v0, 0x400

    goto/32 :goto_9

    nop

    :goto_3
    iget-object p2, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mSizeStr:Ljava/lang/String;

    goto/32 :goto_7

    nop

    :goto_4
    if-eq v0, p4, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_8

    nop

    :goto_5
    iput-wide p2, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mSize:J

    goto/32 :goto_a

    nop

    :goto_6
    if-eqz p2, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_0

    nop

    :goto_7
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    goto/32 :goto_6

    nop

    :goto_8
    invoke-static {p1, p2, p3}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_3

    nop

    :goto_9
    mul-long/2addr p2, v0

    goto/32 :goto_5

    nop

    :goto_a
    iget v0, p0, Lcom/android/settings/applications/RunningState$BaseItem;->mCurSeq:I

    goto/32 :goto_c

    nop

    :goto_b
    return v1

    :goto_c
    const/4 v1, 0x0

    goto/32 :goto_4

    nop
.end method
