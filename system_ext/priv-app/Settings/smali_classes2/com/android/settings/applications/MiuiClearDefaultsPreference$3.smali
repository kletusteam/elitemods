.class Lcom/android/settings/applications/MiuiClearDefaultsPreference$3;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/applications/MiuiClearDefaultsPreference;->updateUI(Landroid/view/View;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/applications/MiuiClearDefaultsPreference;

.field final synthetic val$autoLaunchEnabled:Z

.field final synthetic val$hasBindAppWidgetPermission:Z


# direct methods
.method constructor <init>(Lcom/android/settings/applications/MiuiClearDefaultsPreference;ZZ)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/applications/MiuiClearDefaultsPreference$3;->this$0:Lcom/android/settings/applications/MiuiClearDefaultsPreference;

    iput-boolean p2, p0, Lcom/android/settings/applications/MiuiClearDefaultsPreference$3;->val$hasBindAppWidgetPermission:Z

    iput-boolean p3, p0, Lcom/android/settings/applications/MiuiClearDefaultsPreference$3;->val$autoLaunchEnabled:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    iget-boolean v0, p0, Lcom/android/settings/applications/MiuiClearDefaultsPreference$3;->val$hasBindAppWidgetPermission:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-boolean v3, p0, Lcom/android/settings/applications/MiuiClearDefaultsPreference$3;->val$autoLaunchEnabled:Z

    if-eqz v3, :cond_0

    move v3, v1

    goto :goto_0

    :cond_0
    move v3, v2

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/applications/MiuiClearDefaultsPreference$3;->this$0:Lcom/android/settings/applications/MiuiClearDefaultsPreference;

    sget v4, Lcom/android/settings/R$string;->auto_launch_label_generic:I

    invoke-virtual {v0, v4}, Landroidx/preference/Preference;->setSummary(I)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/android/settings/applications/MiuiClearDefaultsPreference$3;->this$0:Lcom/android/settings/applications/MiuiClearDefaultsPreference;

    sget v4, Lcom/android/settings/R$string;->auto_launch_label:I

    invoke-virtual {v0, v4}, Landroidx/preference/Preference;->setSummary(I)V

    :goto_1
    iget-object v0, p0, Lcom/android/settings/applications/MiuiClearDefaultsPreference$3;->this$0:Lcom/android/settings/applications/MiuiClearDefaultsPreference;

    invoke-virtual {v0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/android/settings/R$dimen;->installed_app_details_bullet_offset:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iget-boolean v6, p0, Lcom/android/settings/applications/MiuiClearDefaultsPreference$3;->val$autoLaunchEnabled:Z

    const/4 v7, 0x2

    const-string v8, "\n"

    if-eqz v6, :cond_3

    sget v4, Lcom/android/settings/R$string;->auto_launch_enable_text:I

    invoke-virtual {v0, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    new-instance v6, Landroid/text/SpannableString;

    invoke-direct {v6, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    if-eqz v3, :cond_2

    new-instance v9, Landroid/text/style/BulletSpan;

    invoke-direct {v9, v5}, Landroid/text/style/BulletSpan;-><init>(I)V

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    invoke-virtual {v6, v9, v2, v4, v2}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    :cond_2
    new-array v4, v7, [Ljava/lang/CharSequence;

    aput-object v6, v4, v2

    aput-object v8, v4, v1

    invoke-static {v4}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    :cond_3
    iget-boolean v6, p0, Lcom/android/settings/applications/MiuiClearDefaultsPreference$3;->val$hasBindAppWidgetPermission:Z

    if-eqz v6, :cond_6

    sget v6, Lcom/android/settings/R$string;->always_allow_bind_appwidgets_text:I

    invoke-virtual {v0, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    new-instance v6, Landroid/text/SpannableString;

    invoke-direct {v6, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    if-eqz v3, :cond_4

    new-instance v3, Landroid/text/style/BulletSpan;

    invoke-direct {v3, v5}, Landroid/text/style/BulletSpan;-><init>(I)V

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-virtual {v6, v3, v2, v0, v2}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    :cond_4
    if-nez v4, :cond_5

    new-array v0, v7, [Ljava/lang/CharSequence;

    aput-object v6, v0, v2

    aput-object v8, v0, v1

    invoke-static {v0}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_2

    :cond_5
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/CharSequence;

    aput-object v4, v0, v2

    aput-object v8, v0, v1

    aput-object v6, v0, v7

    const/4 v2, 0x3

    aput-object v8, v0, v2

    invoke-static {v0}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_2
    move-object v4, v0

    :cond_6
    iget-object v0, p0, Lcom/android/settings/applications/MiuiClearDefaultsPreference$3;->this$0:Lcom/android/settings/applications/MiuiClearDefaultsPreference;

    invoke-virtual {v0, v4}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object p0, p0, Lcom/android/settings/applications/MiuiClearDefaultsPreference$3;->this$0:Lcom/android/settings/applications/MiuiClearDefaultsPreference;

    invoke-static {p0, v1}, Lcom/android/settings/applications/MiuiClearDefaultsPreference;->-$$Nest$fputmHasDefault(Lcom/android/settings/applications/MiuiClearDefaultsPreference;Z)V

    return-void
.end method
