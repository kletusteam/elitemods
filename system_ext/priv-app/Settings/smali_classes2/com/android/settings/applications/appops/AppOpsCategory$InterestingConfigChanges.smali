.class public Lcom/android/settings/applications/appops/AppOpsCategory$InterestingConfigChanges;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/applications/appops/AppOpsCategory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InterestingConfigChanges"
.end annotation


# instance fields
.field final mLastConfiguration:Landroid/content/res/Configuration;

.field mLastDensity:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    iput-object v0, p0, Lcom/android/settings/applications/appops/AppOpsCategory$InterestingConfigChanges;->mLastConfiguration:Landroid/content/res/Configuration;

    return-void
.end method


# virtual methods
.method applyNewConfig(Landroid/content/res/Resources;)Z
    .locals 5

    goto/32 :goto_7

    nop

    :goto_0
    return v4

    :goto_1
    goto/32 :goto_10

    nop

    :goto_2
    move v1, v4

    :goto_3
    goto/32 :goto_15

    nop

    :goto_4
    iget v2, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    goto/32 :goto_8

    nop

    :goto_5
    iget p1, p1, Landroid/util/DisplayMetrics;->densityDpi:I

    goto/32 :goto_9

    nop

    :goto_6
    iget v1, p0, Lcom/android/settings/applications/appops/AppOpsCategory$InterestingConfigChanges;->mLastDensity:I

    goto/32 :goto_b

    nop

    :goto_7
    iget-object v0, p0, Lcom/android/settings/applications/appops/AppOpsCategory$InterestingConfigChanges;->mLastConfiguration:Landroid/content/res/Configuration;

    goto/32 :goto_f

    nop

    :goto_8
    const/4 v3, 0x1

    goto/32 :goto_d

    nop

    :goto_9
    iput p1, p0, Lcom/android/settings/applications/appops/AppOpsCategory$InterestingConfigChanges;->mLastDensity:I

    goto/32 :goto_18

    nop

    :goto_a
    if-nez v0, :cond_0

    goto/32 :goto_17

    :cond_0
    goto/32 :goto_16

    nop

    :goto_b
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    goto/32 :goto_4

    nop

    :goto_c
    invoke-virtual {v0, v1}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I

    move-result v0

    goto/32 :goto_6

    nop

    :goto_d
    const/4 v4, 0x0

    goto/32 :goto_e

    nop

    :goto_e
    if-ne v1, v2, :cond_1

    goto/32 :goto_13

    :cond_1
    goto/32 :goto_14

    nop

    :goto_f
    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    goto/32 :goto_c

    nop

    :goto_10
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    goto/32 :goto_5

    nop

    :goto_11
    and-int/lit16 v0, v0, 0x304

    goto/32 :goto_a

    nop

    :goto_12
    goto :goto_3

    :goto_13
    goto/32 :goto_2

    nop

    :goto_14
    move v1, v3

    goto/32 :goto_12

    nop

    :goto_15
    if-eqz v1, :cond_2

    goto/32 :goto_1

    :cond_2
    goto/32 :goto_11

    nop

    :goto_16
    goto :goto_1

    :goto_17
    goto/32 :goto_0

    nop

    :goto_18
    return v3
.end method
