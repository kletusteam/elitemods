.class public Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;
.super Lcom/android/settings/widget/EmptyTextSettings;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$AppInfoModel;,
        Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$LoadingView;,
        Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$AppComparator;
    }
.end annotation


# static fields
.field static final IGNORE_PACKAGE_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/BaseSearchIndexProvider;


# instance fields
.field private mAppOpsManager:Landroid/app/AppOpsManager;

.field private mContext:Landroid/content/Context;

.field private mExecutorService:Ljava/util/concurrent/ExecutorService;

.field private mIconDrawableFactory:Landroid/util/IconDrawableFactory;

.field private mLoading:Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$LoadingView;

.field private final mMainHandler:Landroid/os/Handler;

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mUserManager:Landroid/os/UserManager;


# direct methods
.method public static synthetic $r8$lambda$EIOcFq-MEkDcTWZTNOv3s0o3zdg(Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;Ljava/util/List;Landroidx/preference/PreferenceScreen;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->lambda$doAsyncTask$0(Ljava/util/List;Landroidx/preference/PreferenceScreen;)V

    return-void
.end method

.method public static synthetic $r8$lambda$KB35TLFHkWYeC4y15m7SZZIaPMg(Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;Ljava/util/List;Landroidx/preference/PreferenceScreen;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->lambda$doAsyncTask$1(Ljava/util/List;Landroidx/preference/PreferenceScreen;)V

    return-void
.end method

.method public static synthetic $r8$lambda$rV-qiv2-XbLBmIZMnCFCY5iuZSE(Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;Landroid/content/Context;Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$AppInfoModel;Landroidx/preference/Preference;)Z
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->lambda$updateUI$2(Landroid/content/Context;Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$AppInfoModel;Landroidx/preference/Preference;)Z

    move-result p0

    return p0
.end method

.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->IGNORE_PACKAGE_LIST:Ljava/util/List;

    const-string v1, "com.android.systemui"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/settings/search/BaseSearchIndexProvider;

    sget v1, Lcom/android/settings/R$xml;->turn_screen_on_settings:I

    invoke-direct {v0, v1}, Lcom/android/settings/search/BaseSearchIndexProvider;-><init>(I)V

    sput-object v0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settings/search/BaseSearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/widget/EmptyTextSettings;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mMainHandler:Landroid/os/Handler;

    return-void
.end method

.method private doAsyncTask(Landroidx/preference/PreferenceScreen;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mLoading:Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$LoadingView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0, v0, p1}, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;Ljava/util/List;Landroidx/preference/PreferenceScreen;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method static hasTurnScreenOnPermission(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
    .locals 2

    sget-object v0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->IGNORE_PACKAGE_LIST:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const-string v0, "android.permission.WAKE_LOCK"

    invoke-virtual {p0, v0, p1}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result p0

    if-nez p0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method private synthetic lambda$doAsyncTask$0(Ljava/util/List;Landroidx/preference/PreferenceScreen;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->updateUI(Ljava/util/List;Landroidx/preference/PreferenceScreen;)V

    return-void
.end method

.method private synthetic lambda$doAsyncTask$1(Ljava/util/List;Landroidx/preference/PreferenceScreen;)V
    .locals 11

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->collectTurnScreenOnApps(I)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$AppComparator;

    iget-object v2, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-direct {v1, v2}, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$AppComparator;-><init>(Landroid/content/pm/PackageManager;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->sort(Ljava/util/Comparator;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object v3

    iget-object v8, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v4}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    new-instance v10, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$AppInfoModel;

    iget-object v5, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mIconDrawableFactory:Landroid/util/IconDrawableFactory;

    invoke-virtual {v5, v2, v1}, Landroid/util/IconDrawableFactory;->getBadgedIcon(Landroid/content/pm/ApplicationInfo;I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iget-object v1, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, v4, v3}, Landroid/content/pm/PackageManager;->getUserBadgedLabel(Ljava/lang/CharSequence;Landroid/os/UserHandle;)Ljava/lang/CharSequence;

    move-result-object v6

    iget-object v1, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mAppOpsManager:Landroid/app/AppOpsManager;

    iget v3, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v1, v3, v8}, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnDetails;->getPreferenceSummary(Landroid/app/AppOpsManager;ILjava/lang/String;)I

    move-result v7

    iget v9, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    move-object v4, v10

    invoke-direct/range {v4 .. v9}, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$AppInfoModel;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;ILjava/lang/String;I)V

    invoke-interface {p1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mMainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$$ExternalSyntheticLambda1;-><init>(Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;Ljava/util/List;Landroidx/preference/PreferenceScreen;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private synthetic lambda$updateUI$2(Landroid/content/Context;Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$AppInfoModel;Landroidx/preference/Preference;)Z
    .locals 7

    const-class v0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnDetails;

    sget p3, Lcom/android/settings/R$string;->turn_screen_on_title:I

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p2, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$AppInfoModel;->packageName:Ljava/lang/String;

    iget v3, p2, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$AppInfoModel;->uid:I

    invoke-virtual {p0}, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->getMetricsCategory()I

    move-result v6

    const/4 v5, -0x1

    move-object v4, p0

    invoke-static/range {v0 .. v6}, Lcom/android/settings/applications/AppInfoBase;->startAppInfoFragment(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;ILandroidx/fragment/app/Fragment;II)V

    const/4 p0, 0x1

    return p0
.end method

.method private updateUI(Ljava/util/List;Landroidx/preference/PreferenceScreen;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$AppInfoModel;",
            ">;",
            "Landroidx/preference/PreferenceScreen;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mLoading:Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$LoadingView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->getPrefContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/android/settings/R$string;->no_applications:I

    invoke-virtual {p0, v1}, Lcom/android/settings/widget/EmptyTextSettings;->setEmptyText(I)V

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$AppInfoModel;

    new-instance v2, Lcom/android/settingslib/widget/AppPreference;

    invoke-direct {v2, v0}, Lcom/android/settingslib/widget/AppPreference;-><init>(Landroid/content/Context;)V

    iget-object v3, v1, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$AppInfoModel;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3}, Landroidx/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    iget-object v3, v1, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$AppInfoModel;->title:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget v3, v1, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$AppInfoModel;->summary:I

    invoke-virtual {v2, v3}, Landroidx/preference/Preference;->setSummary(I)V

    new-instance v3, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$$ExternalSyntheticLambda2;

    invoke-direct {v3, p0, v0, v1}, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$$ExternalSyntheticLambda2;-><init>(Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;Landroid/content/Context;Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$AppInfoModel;)V

    invoke-virtual {v2, v3}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    invoke-virtual {p2, v2}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method collectTurnScreenOnApps(I)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList<",
            "Landroid/util/Pair<",
            "Landroid/content/pm/ApplicationInfo;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    goto/32 :goto_e

    nop

    :goto_0
    check-cast v2, Landroid/content/pm/UserInfo;

    goto/32 :goto_20

    nop

    :goto_1
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_16

    nop

    :goto_2
    if-nez v4, :cond_0

    goto/32 :goto_18

    :cond_0
    goto/32 :goto_6

    nop

    :goto_3
    check-cast v3, Landroid/content/pm/PackageInfo;

    goto/32 :goto_5

    nop

    :goto_4
    if-nez v3, :cond_1

    goto/32 :goto_1a

    :cond_1
    goto/32 :goto_13

    nop

    :goto_5
    iget-object v4, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mPackageManager:Landroid/content/pm/PackageManager;

    goto/32 :goto_29

    nop

    :goto_6
    new-instance v4, Landroid/util/Pair;

    goto/32 :goto_2b

    nop

    :goto_7
    if-nez v1, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_22

    nop

    :goto_8
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto/32 :goto_d

    nop

    :goto_9
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_1f

    nop

    :goto_a
    goto :goto_18

    :goto_b
    goto/32 :goto_1b

    nop

    :goto_c
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto/32 :goto_1d

    nop

    :goto_d
    invoke-direct {v4, v3, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/32 :goto_1c

    nop

    :goto_e
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_1

    nop

    :goto_f
    goto/16 :goto_27

    :goto_10
    goto/32 :goto_19

    nop

    :goto_11
    invoke-virtual {v2, p1}, Landroid/os/UserManager;->getProfiles(I)Ljava/util/List;

    move-result-object p1

    goto/32 :goto_26

    nop

    :goto_12
    invoke-static {v4, v5}, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->hasTurnScreenOnPermission(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v4

    goto/32 :goto_2

    nop

    :goto_13
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_3

    nop

    :goto_14
    check-cast v1, Ljava/lang/Integer;

    goto/32 :goto_c

    nop

    :goto_15
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_0

    nop

    :goto_16
    new-instance v1, Ljava/util/ArrayList;

    goto/32 :goto_9

    nop

    :goto_17
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_18
    goto/32 :goto_21

    nop

    :goto_19
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1a
    goto/32 :goto_28

    nop

    :goto_1b
    return-object v0

    :goto_1c
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_a

    nop

    :goto_1d
    iget-object v2, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mPackageManager:Landroid/content/pm/PackageManager;

    goto/32 :goto_24

    nop

    :goto_1e
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    goto/32 :goto_2c

    nop

    :goto_1f
    iget-object v2, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mUserManager:Landroid/os/UserManager;

    goto/32 :goto_11

    nop

    :goto_20
    iget v2, v2, Landroid/content/pm/UserInfo;->id:I

    goto/32 :goto_25

    nop

    :goto_21
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    goto/32 :goto_4

    nop

    :goto_22
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_14

    nop

    :goto_23
    invoke-virtual {v2, v3, v1}, Landroid/content/pm/PackageManager;->getInstalledPackagesAsUser(II)Ljava/util/List;

    move-result-object v2

    goto/32 :goto_17

    nop

    :goto_24
    const/4 v3, 0x0

    goto/32 :goto_23

    nop

    :goto_25
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/32 :goto_2a

    nop

    :goto_26
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_27
    goto/32 :goto_1e

    nop

    :goto_28
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_7

    nop

    :goto_29
    iget-object v5, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    goto/32 :goto_12

    nop

    :goto_2a
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_f

    nop

    :goto_2b
    iget-object v3, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    goto/32 :goto_8

    nop

    :goto_2c
    if-nez v2, :cond_3

    goto/32 :goto_10

    :cond_3
    goto/32 :goto_15

    nop
.end method

.method public getMetricsCategory()I
    .locals 0

    const/16 p0, 0x782

    return p0
.end method

.method protected getPreferenceScreenResId()I
    .locals 0

    sget p0, Lcom/android/settings/R$xml;->turn_screen_on_settings:I

    return p0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object p1, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mContext:Landroid/content/Context;

    const-class v0, Landroid/os/UserManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/os/UserManager;

    iput-object p1, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mUserManager:Landroid/os/UserManager;

    iget-object p1, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mContext:Landroid/content/Context;

    const-class v0, Landroid/app/AppOpsManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/AppOpsManager;

    iput-object p1, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mAppOpsManager:Landroid/app/AppOpsManager;

    iget-object p1, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mContext:Landroid/content/Context;

    invoke-static {p1}, Landroid/util/IconDrawableFactory;->newInstance(Landroid/content/Context;)Landroid/util/IconDrawableFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mIconDrawableFactory:Landroid/util/IconDrawableFactory;

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public onDetach()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onDetach()V

    iget-object v0, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onResume()V

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/preference/PreferenceGroup;->removeAll()V

    invoke-direct {p0, v0}, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->doAsyncTask(Landroidx/preference/PreferenceScreen;)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/android/settings/widget/EmptyTextSettings;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    new-instance p2, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$LoadingView;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$LoadingView;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mLoading:Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$LoadingView;

    const p2, 0x102003f

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    invoke-interface {p1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iget-object p0, p0, Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings;->mLoading:Lcom/android/settings/applications/specialaccess/turnscreenon/TurnScreenOnSettings$LoadingView;

    new-instance p2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v0, -0x1

    invoke-direct {p2, v0, v0}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p1, p0, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method
