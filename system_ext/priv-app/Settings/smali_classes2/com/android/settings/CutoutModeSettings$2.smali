.class Lcom/android/settings/CutoutModeSettings$2;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/CutoutModeSettings;->prepareCutoutDialogBuild(Lmiuix/appcompat/app/AlertDialog$Builder;Ljava/lang/String;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/CutoutModeSettings;

.field final synthetic val$checkedItem:I

.field final synthetic val$listPos:I

.field final synthetic val$packageName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/settings/CutoutModeSettings;IILjava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/CutoutModeSettings$2;->this$0:Lcom/android/settings/CutoutModeSettings;

    iput p2, p0, Lcom/android/settings/CutoutModeSettings$2;->val$checkedItem:I

    iput p3, p0, Lcom/android/settings/CutoutModeSettings$2;->val$listPos:I

    iput-object p4, p0, Lcom/android/settings/CutoutModeSettings$2;->val$packageName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    if-ltz p2, :cond_2

    iget-object v0, p0, Lcom/android/settings/CutoutModeSettings$2;->this$0:Lcom/android/settings/CutoutModeSettings;

    invoke-static {v0}, Lcom/android/settings/CutoutModeSettings;->-$$Nest$fgetmContentArray(Lcom/android/settings/CutoutModeSettings;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-lt p2, v0, :cond_0

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/android/settings/CutoutModeSettings$2;->val$checkedItem:I

    if-eq p2, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/CutoutModeSettings$2;->this$0:Lcom/android/settings/CutoutModeSettings;

    iget-object v0, v0, Lcom/android/settings/CutoutModeSettings;->mAdapter:Lcom/android/settings/CutoutModeSettings$AppAdapter;

    iget v1, p0, Lcom/android/settings/CutoutModeSettings$2;->val$listPos:I

    invoke-virtual {v0, v1}, Lcom/android/settings/CutoutModeSettings$AppAdapter;->getItem(I)Lcom/android/settings/CutoutModeSettings$AppItem;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/android/settings/CutoutModeSettings$AppItem;->-$$Nest$fputmCutoutMode(Lcom/android/settings/CutoutModeSettings$AppItem;I)V

    iget-object v0, p0, Lcom/android/settings/CutoutModeSettings$2;->val$packageName:Ljava/lang/String;

    invoke-static {v0, p2}, Lmiui/os/MiuiInit;->setCutoutMode(Ljava/lang/String;I)V

    iget-object p0, p0, Lcom/android/settings/CutoutModeSettings$2;->this$0:Lcom/android/settings/CutoutModeSettings;

    iget-object p0, p0, Lcom/android/settings/CutoutModeSettings;->mAdapter:Lcom/android/settings/CutoutModeSettings$AppAdapter;

    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    :cond_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    :cond_2
    :goto_0
    return-void
.end method
