.class public Lcom/android/settings/haptic/utils/ViewUtils;
.super Ljava/lang/Object;


# direct methods
.method public static getTransitionColor(FII)I
    .locals 6

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v0

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result p1

    invoke-static {p2}, Landroid/graphics/Color;->red(I)I

    move-result v3

    invoke-static {p2}, Landroid/graphics/Color;->blue(I)I

    move-result v4

    invoke-static {p2}, Landroid/graphics/Color;->green(I)I

    move-result v5

    invoke-static {p2}, Landroid/graphics/Color;->alpha(I)I

    move-result p2

    sub-int/2addr v3, v0

    sub-int/2addr v4, v1

    sub-int/2addr v5, v2

    sub-int/2addr p2, p1

    int-to-float v0, v0

    int-to-float v3, v3

    mul-float/2addr v3, p0

    add-float/2addr v0, v3

    float-to-int v0, v0

    int-to-float v1, v1

    int-to-float v3, v4

    mul-float/2addr v3, p0

    add-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v2, v2

    int-to-float v3, v5

    mul-float/2addr v3, p0

    add-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float p1, p1

    int-to-float p2, p2

    mul-float/2addr p0, p2

    add-float/2addr p1, p0

    float-to-int p0, p1

    invoke-static {p0, v0, v2, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result p0

    return p0
.end method
