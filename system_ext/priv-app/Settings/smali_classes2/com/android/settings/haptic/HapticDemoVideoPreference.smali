.class public Lcom/android/settings/haptic/HapticDemoVideoPreference;
.super Landroidx/preference/Preference;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/haptic/HapticDemoVideoPreference$IVideoState;,
        Lcom/android/settings/haptic/HapticDemoVideoPreference$BgHandler;
    }
.end annotation


# instance fields
.field private VideoContainer:Landroid/view/View;

.field private mBgHandler:Lcom/android/settings/haptic/HapticDemoVideoPreference$BgHandler;

.field private mBgHandlerThread:Landroid/os/HandlerThread;

.field private mHapticCompat:Lmiui/util/HapticFeedbackUtil;

.field private mIsFinish:Z

.field private mIsFinishRenderingStart:Z

.field private mIsVisible:Z

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mPerformExtHapticFeedback:Ljava/lang/Runnable;

.field private mPlayBtn:Landroid/view/View;

.field private mRootView:Landroid/view/View;

.field private mSharedPrefs:Landroid/content/SharedPreferences;

.field private mSupportLinearMotorVibrate:Z

.field private mSurface:Landroid/view/Surface;

.field private mTextureView:Landroid/view/TextureView;

.field private mVideoBgImgHolder:Landroid/view/View;

.field private mVideoBgImgItem:Landroid/view/View;

.field private mVideoState:Lcom/android/settings/haptic/HapticDemoVideoPreference$IVideoState;


# direct methods
.method public static synthetic $r8$lambda$36MCKAE4nbNZPHvyVRLB0PHKRO0(Lcom/android/settings/haptic/HapticDemoVideoPreference;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference;->lambda$stopPlayingVideo$3()V

    return-void
.end method

.method public static synthetic $r8$lambda$AYM5u3cCCNM9RsVLyorCVrbRgs8(Lcom/android/settings/haptic/HapticDemoVideoPreference;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference;->lambda$showVideoBgImgHolder$2()V

    return-void
.end method

.method public static synthetic $r8$lambda$InRkcLxQ575rAOrb7FwgOxhMqMM(Lcom/android/settings/haptic/HapticDemoVideoPreference;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference;->lambda$performHapticFeedback$6()V

    return-void
.end method

.method public static synthetic $r8$lambda$Q-Q1-fCSyPoM0oB8C9XbyzegaIA(Lcom/android/settings/haptic/HapticDemoVideoPreference;Landroid/media/MediaPlayer;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/haptic/HapticDemoVideoPreference;->lambda$playMedia$5(Landroid/media/MediaPlayer;)V

    return-void
.end method

.method public static synthetic $r8$lambda$R2EZ2kPZ_w28ImqXrre6B7Er-zo(Lcom/android/settings/haptic/HapticDemoVideoPreference;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference;->lambda$playMedia$4()V

    return-void
.end method

.method public static synthetic $r8$lambda$ToswUc6HwYBw8NH3RdOnlE7UinE(Lcom/android/settings/haptic/HapticDemoVideoPreference;Landroid/media/MediaPlayer;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/haptic/HapticDemoVideoPreference;->lambda$initMedia$1(Landroid/media/MediaPlayer;)V

    return-void
.end method

.method public static synthetic $r8$lambda$rJ2zgnP0Ad0cbNtDD91xfu8utA0(Lcom/android/settings/haptic/HapticDemoVideoPreference;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference;->lambda$new$0()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmBgHandler(Lcom/android/settings/haptic/HapticDemoVideoPreference;)Lcom/android/settings/haptic/HapticDemoVideoPreference$BgHandler;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mBgHandler:Lcom/android/settings/haptic/HapticDemoVideoPreference$BgHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsFinishRenderingStart(Lcom/android/settings/haptic/HapticDemoVideoPreference;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mIsFinishRenderingStart:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsVisible(Lcom/android/settings/haptic/HapticDemoVideoPreference;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mIsVisible:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmVideoBgImgHolder(Lcom/android/settings/haptic/HapticDemoVideoPreference;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mVideoBgImgHolder:Landroid/view/View;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmIsFinishRenderingStart(Lcom/android/settings/haptic/HapticDemoVideoPreference;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mIsFinishRenderingStart:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmSurface(Lcom/android/settings/haptic/HapticDemoVideoPreference;Landroid/view/Surface;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mSurface:Landroid/view/Surface;

    return-void
.end method

.method static bridge synthetic -$$Nest$minitMedia(Lcom/android/settings/haptic/HapticDemoVideoPreference;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference;->initMedia()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mperformHapticFeedback(Lcom/android/settings/haptic/HapticDemoVideoPreference;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference;->performHapticFeedback()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mplayMedia(Lcom/android/settings/haptic/HapticDemoVideoPreference;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference;->playMedia()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreleaseMedia(Lcom/android/settings/haptic/HapticDemoVideoPreference;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference;->releaseMedia()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mstopPlayingVideo(Lcom/android/settings/haptic/HapticDemoVideoPreference;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference;->stopPlayingVideo()Z

    move-result p0

    return p0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroidx/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance p1, Landroid/media/MediaPlayer;

    invoke-direct {p1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "IS_FIRST_START_HAPTIC_SP"

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mSharedPrefs:Landroid/content/SharedPreferences;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mIsFinish:Z

    new-instance p2, Lcom/android/settings/haptic/HapticDemoVideoPreference$$ExternalSyntheticLambda0;

    invoke-direct {p2, p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/haptic/HapticDemoVideoPreference;)V

    iput-object p2, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mPerformExtHapticFeedback:Ljava/lang/Runnable;

    sget p2, Lcom/android/settings/R$layout;->haptic_demo_main_video_layout:I

    invoke-virtual {p0, p2}, Landroidx/preference/Preference;->setLayoutResource(I)V

    new-instance p2, Lmiui/util/HapticFeedbackUtil;

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0, p1}, Lmiui/util/HapticFeedbackUtil;-><init>(Landroid/content/Context;Z)V

    iput-object p2, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mHapticCompat:Lmiui/util/HapticFeedbackUtil;

    invoke-static {}, Lcom/android/settings/haptic/utils/UiUtils;->isSupportLinearMotorVibrate()Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mSupportLinearMotorVibrate:Z

    new-instance p1, Landroid/os/HandlerThread;

    const-string p2, "haptic_video"

    const/4 v0, 0x5

    invoke-direct {p1, p2, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mBgHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {p1}, Landroid/os/HandlerThread;->start()V

    new-instance p1, Lcom/android/settings/haptic/HapticDemoVideoPreference$BgHandler;

    iget-object p2, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mBgHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {p2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object p2

    invoke-direct {p1, p0, p2}, Lcom/android/settings/haptic/HapticDemoVideoPreference$BgHandler;-><init>(Lcom/android/settings/haptic/HapticDemoVideoPreference;Landroid/os/Looper;)V

    iput-object p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mBgHandler:Lcom/android/settings/haptic/HapticDemoVideoPreference$BgHandler;

    return-void
.end method

.method private initMedia()V
    .locals 3

    const-string v0, "HapticDemoVideoPreferen"

    const-string v1, "initMedia"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mSurface:Landroid/view/Surface;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "android.resource://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v1, Lcom/android/settings/R$raw;->main_haptic_video:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/android/settings/haptic/HapticDemoVideoPreference$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference$$ExternalSyntheticLambda2;-><init>(Lcom/android/settings/haptic/HapticDemoVideoPreference;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    :try_start_1
    iget-object p0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_1
    return-void
.end method

.method private synthetic lambda$initMedia$1(Landroid/media/MediaPlayer;)V
    .locals 1

    iget-object p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v0, "IS_FIRST_START_HAPTIC"

    invoke-interface {p1, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result p1

    const-string v0, "HapticDemoVideoPreferen"

    if-eqz p1, :cond_0

    const-string/jumbo p0, "no need to play video"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mVideoBgImgHolder:Landroid/view/View;

    if-eqz p1, :cond_1

    const-string p1, "first play video"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference;->playMedia()V

    :cond_1
    return-void
.end method

.method private synthetic lambda$new$0()V
    .locals 1

    const/16 v0, 0xc0

    invoke-virtual {p0, v0}, Lcom/android/settings/haptic/HapticDemoVideoPreference;->playExtPatternById(I)V

    return-void
.end method

.method private synthetic lambda$performHapticFeedback$6()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mPlayBtn:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mVideoState:Lcom/android/settings/haptic/HapticDemoVideoPreference$IVideoState;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/android/settings/haptic/HapticDemoVideoPreference$IVideoState;->onHapticVideoStateChange(Z)V

    :cond_0
    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getMainThreadHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mPerformExtHapticFeedback:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getMainThreadHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object p0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mPerformExtHapticFeedback:Ljava/lang/Runnable;

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private synthetic lambda$playMedia$4()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mPlayBtn:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mVideoState:Lcom/android/settings/haptic/HapticDemoVideoPreference$IVideoState;

    if-eqz v0, :cond_0

    invoke-interface {v0, v1}, Lcom/android/settings/haptic/HapticDemoVideoPreference$IVideoState;->onHapticVideoStateChange(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mVideoBgImgItem:Landroid/view/View;

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v1, Lcom/android/settings/R$drawable;->img_main_video_bg:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private synthetic lambda$playMedia$5(Landroid/media/MediaPlayer;)V
    .locals 1

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mIsFinish:Z

    iget-object p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->seekTo(I)V

    iget-object p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mPlayBtn:Landroid/view/View;

    new-instance v0, Lcom/android/settings/haptic/HapticDemoVideoPreference$$ExternalSyntheticLambda6;

    invoke-direct {v0, p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference$$ExternalSyntheticLambda6;-><init>(Lcom/android/settings/haptic/HapticDemoVideoPreference;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private synthetic lambda$showVideoBgImgHolder$2()V
    .locals 1

    iget-object p0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mVideoBgImgHolder:Landroid/view/View;

    if-eqz p0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private synthetic lambda$stopPlayingVideo$3()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mPlayBtn:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mVideoState:Lcom/android/settings/haptic/HapticDemoVideoPreference$IVideoState;

    if-eqz v0, :cond_0

    invoke-interface {v0, v1}, Lcom/android/settings/haptic/HapticDemoVideoPreference$IVideoState;->onHapticVideoStateChange(Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mVideoBgImgItem:Landroid/view/View;

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v1, Lcom/android/settings/R$drawable;->img_main_video_bg:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private performHapticFeedback()V
    .locals 3

    iget-boolean v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mIsFinishRenderingStart:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mSharedPrefs:Landroid/content/SharedPreferences;

    const-string v1, "IS_FIRST_START_HAPTIC"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mSharedPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getMainThreadHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/settings/haptic/HapticDemoVideoPreference$$ExternalSyntheticLambda5;

    invoke-direct {v1, p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference$$ExternalSyntheticLambda5;-><init>(Lcom/android/settings/haptic/HapticDemoVideoPreference;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const-class p0, Lcom/android/settings/haptic/HapticFragment;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    const-string v0, "haptic_main_video"

    invoke-static {p0, v0}, Lcom/android/settingslib/util/MiStatInterfaceUtils;->trackPreferenceClick(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private playMedia()V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference;->stopPlayingVideo()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mIsFinish:Z

    if-nez v0, :cond_1

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_2

    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/android/settings/haptic/HapticDemoVideoPreference$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference$$ExternalSyntheticLambda3;-><init>(Lcom/android/settings/haptic/HapticDemoVideoPreference;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mIsFinish:Z

    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/android/settings/haptic/HapticDemoVideoPreference$3;

    invoke-direct {v1, p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference$3;-><init>(Lcom/android/settings/haptic/HapticDemoVideoPreference;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    invoke-direct {p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference;->performHapticFeedback()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private releaseMedia()V
    .locals 2

    :try_start_0
    invoke-direct {p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference;->showVideoBgImgHolder()V

    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    iput-object v1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mIsFinishRenderingStart:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "releaseMedia error "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "HapticDemoVideoPreferen"

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void
.end method

.method private showVideoBgImgHolder()V
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getMainThreadHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/android/settings/haptic/HapticDemoVideoPreference$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference$$ExternalSyntheticLambda1;-><init>(Lcom/android/settings/haptic/HapticDemoVideoPreference;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private stopPlayingVideo()Z
    .locals 3

    const-string v0, "HapticDemoVideoPreferen"

    const-string/jumbo v1, "stopPlayingVideo"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mPlayBtn:Landroid/view/View;

    new-instance v2, Lcom/android/settings/haptic/HapticDemoVideoPreference$$ExternalSyntheticLambda4;

    invoke-direct {v2, p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference$$ExternalSyntheticLambda4;-><init>(Lcom/android/settings/haptic/HapticDemoVideoPreference;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getMainThreadHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mPerformExtHapticFeedback:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    iget-object v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mIsFinish:Z

    iget-object p0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mHapticCompat:Lmiui/util/HapticFeedbackUtil;

    invoke-virtual {p0}, Lmiui/util/HapticFeedbackUtil;->release()V

    return v0

    :cond_0
    return v1
.end method


# virtual methods
.method public onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
    .locals 2

    invoke-super {p0, p1}, Landroidx/preference/Preference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    iput-object p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mRootView:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0, v0, v0, v0}, Landroid/view/View;->setPadding(IIII)V

    iget-object p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mRootView:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mRootView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->video_card_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->VideoContainer:Landroid/view/View;

    iget-object p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mRootView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->tv_item:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/TextureView;

    iput-object p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mTextureView:Landroid/view/TextureView;

    iget-object p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mRootView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->img_item:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mVideoBgImgItem:Landroid/view/View;

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/settings/R$drawable;->img_main_video_bg:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mRootView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->img_holder:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mVideoBgImgHolder:Landroid/view/View;

    iget-object p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mTextureView:Landroid/view/TextureView;

    new-instance v0, Lcom/android/settings/haptic/HapticDemoVideoPreference$1;

    invoke-direct {v0, p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference$1;-><init>(Lcom/android/settings/haptic/HapticDemoVideoPreference;)V

    invoke-virtual {p1, v0}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    iget-object p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mRootView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->play_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mPlayBtn:Landroid/view/View;

    iget-object p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->VideoContainer:Landroid/view/View;

    new-instance v0, Lcom/android/settings/haptic/HapticDemoVideoPreference$2;

    invoke-direct {v0, p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference$2;-><init>(Lcom/android/settings/haptic/HapticDemoVideoPreference;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onStart()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/haptic/HapticDemoVideoPreference;->showVideoBgImgHolder()V

    return-void
.end method

.method public onStop()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mIsVisible:Z

    const-string v0, "HapticDemoVideoPreferen"

    const-string/jumbo v1, "onStop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mBgHandler:Lcom/android/settings/haptic/HapticDemoVideoPreference$BgHandler;

    if-eqz p0, :cond_0

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method public onVisible()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mIsVisible:Z

    return-void
.end method

.method onVisible(Z)V
    .locals 1

    goto/32 :goto_d

    nop

    :goto_0
    const-string p1, "HapticDemoVideoPreferen"

    goto/32 :goto_b

    nop

    :goto_1
    iget-object p0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mBgHandler:Lcom/android/settings/haptic/HapticDemoVideoPreference$BgHandler;

    goto/32 :goto_7

    nop

    :goto_2
    invoke-virtual {p0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_3
    goto/32 :goto_f

    nop

    :goto_4
    if-nez p0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_8

    nop

    :goto_5
    goto :goto_3

    :goto_6
    goto/32 :goto_0

    nop

    :goto_7
    if-nez p0, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_c

    nop

    :goto_8
    const/4 p1, 0x4

    goto/32 :goto_2

    nop

    :goto_9
    invoke-virtual {p0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/32 :goto_5

    nop

    :goto_a
    if-nez p1, :cond_2

    goto/32 :goto_6

    :cond_2
    goto/32 :goto_1

    nop

    :goto_b
    const-string/jumbo v0, "onVisible false"

    goto/32 :goto_10

    nop

    :goto_c
    const/4 p1, 0x1

    goto/32 :goto_9

    nop

    :goto_d
    iput-boolean p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mIsVisible:Z

    goto/32 :goto_a

    nop

    :goto_e
    iget-object p0, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mBgHandler:Lcom/android/settings/haptic/HapticDemoVideoPreference$BgHandler;

    goto/32 :goto_4

    nop

    :goto_f
    return-void

    :goto_10
    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_e

    nop
.end method

.method public playExtPatternById(I)V
    .locals 4

    const-string v0, "HapticDemoVideoPreferen"

    :try_start_0
    invoke-static {}, Lmiui/util/HapticFeedbackUtil;->isSupportLinearMotorVibrate()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mHapticCompat:Lmiui/util/HapticFeedbackUtil;

    invoke-virtual {v1, p1}, Lmiui/util/HapticFeedbackUtil;->isSupportExtHapticFeedback(I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "performExtHapticFeedback id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mHapticCompat:Lmiui/util/HapticFeedbackUtil;

    const/4 v2, 0x4

    const/4 v3, 0x1

    invoke-virtual {v1, v2, p1, v3}, Lmiui/util/HapticFeedbackUtil;->performExtHapticFeedback(IIZ)Z

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "haptic_feedback_infinite_intensity"

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0}, Lcom/android/settings/sound/VibratorFeatureUtil;->getInstance(Landroid/content/Context;)Lcom/android/settings/sound/VibratorFeatureUtil;

    move-result-object p0

    invoke-virtual {p0, v1}, Lcom/android/settings/sound/VibratorFeatureUtil;->setAmplitude(F)V

    goto :goto_0

    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Not support this rtp:$rtpEffectId! id:"

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Not support linearMotor! id:"

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not support this id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " exception:"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public setVideoState(Lcom/android/settings/haptic/HapticDemoVideoPreference$IVideoState;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/haptic/HapticDemoVideoPreference;->mVideoState:Lcom/android/settings/haptic/HapticDemoVideoPreference$IVideoState;

    return-void
.end method
