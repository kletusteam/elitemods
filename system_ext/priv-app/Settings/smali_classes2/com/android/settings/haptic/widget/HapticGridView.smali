.class public Lcom/android/settings/haptic/widget/HapticGridView;
.super Landroid/widget/GridLayout;


# instance fields
.field private mAlphaAnimation:Landroid/animation/ValueAnimator;

.field private mAnimatorSet:Landroid/animation/AnimatorSet;

.field private mChildView:[Landroid/view/View;

.field private mData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/haptic/data/HapticResource;",
            ">;"
        }
    .end annotation
.end field

.field private mFirstRowMarginBottom:I

.field private mFirstRowMarginTop:I

.field private mHapticCompat:Lmiuix/util/HapticFeedbackCompat;

.field private mHapticUtil:Lmiui/util/HapticFeedbackUtil;

.field mIsFinishRenderingStart:[Z

.field private mItemWidth:I

.field private mLastPlayingIndex:I

.field private mLeftItemMarginEnd:I

.field private mLeftItemMarginStart:I

.field private mMediaPlayerList:[Landroid/media/MediaPlayer;

.field private mPlayingIndex:I

.field private mRightItemMarginEnd:I

.field private mRightItemMarginStart:I

.field private mSurfaceList:[Landroid/view/Surface;


# direct methods
.method public static synthetic $r8$lambda$4MMSOVN5hHz92_8kifGgcJwa5Uk(Landroid/view/View;Landroid/animation/ValueAnimator;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/android/settings/haptic/widget/HapticGridView;->lambda$startAlphaAnimation$0(Landroid/view/View;Landroid/animation/ValueAnimator;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmAnimatorSet(Lcom/android/settings/haptic/widget/HapticGridView;)Landroid/animation/AnimatorSet;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmChildView(Lcom/android/settings/haptic/widget/HapticGridView;)[Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mChildView:[Landroid/view/View;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmData(Lcom/android/settings/haptic/widget/HapticGridView;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mData:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastPlayingIndex(Lcom/android/settings/haptic/widget/HapticGridView;)I
    .locals 0

    iget p0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mLastPlayingIndex:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmMediaPlayerList(Lcom/android/settings/haptic/widget/HapticGridView;)[Landroid/media/MediaPlayer;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mMediaPlayerList:[Landroid/media/MediaPlayer;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPlayingIndex(Lcom/android/settings/haptic/widget/HapticGridView;)I
    .locals 0

    iget p0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mPlayingIndex:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSurfaceList(Lcom/android/settings/haptic/widget/HapticGridView;)[Landroid/view/Surface;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mSurfaceList:[Landroid/view/Surface;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmAnimatorSet(Lcom/android/settings/haptic/widget/HapticGridView;Landroid/animation/AnimatorSet;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastPlayingIndex(Lcom/android/settings/haptic/widget/HapticGridView;I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mLastPlayingIndex:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmPlayingIndex(Lcom/android/settings/haptic/widget/HapticGridView;I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mPlayingIndex:I

    return-void
.end method

.method static bridge synthetic -$$Nest$mhideImgHolder(Lcom/android/settings/haptic/widget/HapticGridView;ILandroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/haptic/widget/HapticGridView;->hideImgHolder(ILandroid/view/View;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mplayVideo(Lcom/android/settings/haptic/widget/HapticGridView;Landroid/view/View;ILandroid/view/View;ZI)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/android/settings/haptic/widget/HapticGridView;->playVideo(Landroid/view/View;ILandroid/view/View;ZI)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mstartAlphaAnimation(Lcom/android/settings/haptic/widget/HapticGridView;Landroid/view/View;FFJ)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/android/settings/haptic/widget/HapticGridView;->startAlphaAnimation(Landroid/view/View;FFJ)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/GridLayout;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x4

    new-array v0, p1, [Landroid/media/MediaPlayer;

    iput-object v0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mMediaPlayerList:[Landroid/media/MediaPlayer;

    new-array v0, p1, [Landroid/view/Surface;

    iput-object v0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mSurfaceList:[Landroid/view/Surface;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mPlayingIndex:I

    iput v0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mLastPlayingIndex:I

    new-array p1, p1, [Z

    iput-object p1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mIsFinishRenderingStart:[Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/GridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x4

    new-array p2, p1, [Landroid/media/MediaPlayer;

    iput-object p2, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mMediaPlayerList:[Landroid/media/MediaPlayer;

    new-array p2, p1, [Landroid/view/Surface;

    iput-object p2, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mSurfaceList:[Landroid/view/Surface;

    const/4 p2, -0x1

    iput p2, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mPlayingIndex:I

    iput p2, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mLastPlayingIndex:I

    new-array p1, p1, [Z

    iput-object p1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mIsFinishRenderingStart:[Z

    new-instance p1, Lmiuix/util/HapticFeedbackCompat;

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Lmiuix/util/HapticFeedbackCompat;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mHapticCompat:Lmiuix/util/HapticFeedbackCompat;

    new-instance p1, Lmiui/util/HapticFeedbackUtil;

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object p2

    const/4 v0, 0x1

    invoke-direct {p1, p2, v0}, Lmiui/util/HapticFeedbackUtil;-><init>(Landroid/content/Context;Z)V

    iput-object p1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mHapticUtil:Lmiui/util/HapticFeedbackUtil;

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/android/settings/R$dimen;->left_video_margin_start:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/android/settings/R$dimen;->left_video_margin_end:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    invoke-static {}, Lcom/android/settings/usagestats/utils/CommonUtils;->isRtl()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, p2

    goto :goto_0

    :cond_0
    move v0, p1

    :goto_0
    iput v0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mLeftItemMarginStart:I

    invoke-static {}, Lcom/android/settings/usagestats/utils/CommonUtils;->isRtl()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, p1

    goto :goto_1

    :cond_1
    move v0, p2

    :goto_1
    iput v0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mLeftItemMarginEnd:I

    invoke-static {}, Lcom/android/settings/usagestats/utils/CommonUtils;->isRtl()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, p1

    goto :goto_2

    :cond_2
    move v0, p2

    :goto_2
    iput v0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mRightItemMarginStart:I

    invoke-static {}, Lcom/android/settings/usagestats/utils/CommonUtils;->isRtl()Z

    move-result v0

    if-eqz v0, :cond_3

    move p1, p2

    :cond_3
    iput p1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mRightItemMarginEnd:I

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/android/settings/R$dimen;->first_row_video_margin_top:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mFirstRowMarginTop:I

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/android/settings/R$dimen;->first_row_video_margin_bottom:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mFirstRowMarginBottom:I

    return-void
.end method

.method private hideImgHolder(ILandroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mIsFinishRenderingStart:[Z

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mChildView:[Landroid/view/View;

    aget-object v0, v0, p1

    sget v1, Lcom/android/settings/R$id;->vv_item:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object p2

    iget-object p0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mData:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/android/settings/haptic/data/HapticResource;

    invoke-virtual {p0}, Lcom/android/settings/haptic/data/HapticResource;->getContentDescription()I

    move-result p0

    invoke-virtual {p2, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    const-class p1, Lcom/android/settings/haptic/HapticDetailActivity;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, p0}, Lcom/android/settingslib/util/MiStatInterfaceUtils;->trackPreferenceClick(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private initNormalView()V
    .locals 9

    iget-object v0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Landroid/view/View;

    iput-object v0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mChildView:[Landroid/view/View;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v2, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_4

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    sget v3, Lcom/android/settings/R$layout;->haptic_demo_video_layout:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mChildView:[Landroid/view/View;

    aput-object v2, v3, v1

    div-int/lit8 v3, v1, 0x2

    rem-int/lit8 v4, v1, 0x2

    const/4 v5, 0x1

    invoke-static {v3, v5}, Landroid/widget/GridLayout;->spec(II)Landroid/widget/GridLayout$Spec;

    move-result-object v3

    invoke-static {v4, v5}, Landroid/widget/GridLayout;->spec(II)Landroid/widget/GridLayout$Spec;

    move-result-object v4

    new-instance v6, Landroid/widget/GridLayout$LayoutParams;

    invoke-direct {v6, v3, v4}, Landroid/widget/GridLayout$LayoutParams;-><init>(Landroid/widget/GridLayout$Spec;Landroid/widget/GridLayout$Spec;)V

    iget v3, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mItemWidth:I

    iput v3, v6, Landroid/widget/GridLayout$LayoutParams;->width:I

    if-eqz v1, :cond_3

    if-eq v1, v5, :cond_2

    const/4 v3, 0x2

    if-eq v1, v3, :cond_1

    const/4 v3, 0x3

    if-eq v1, v3, :cond_0

    goto :goto_1

    :cond_0
    iget v3, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mRightItemMarginStart:I

    iget v4, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mRightItemMarginEnd:I

    iget v5, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mFirstRowMarginTop:I

    invoke-virtual {v6, v3, v0, v4, v5}, Landroid/widget/GridLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_1

    :cond_1
    iget v3, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mLeftItemMarginStart:I

    iget v4, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mLeftItemMarginEnd:I

    iget v5, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mFirstRowMarginTop:I

    invoke-virtual {v6, v3, v0, v4, v5}, Landroid/widget/GridLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_1

    :cond_2
    iget v3, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mRightItemMarginStart:I

    iget v4, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mFirstRowMarginTop:I

    iget v5, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mRightItemMarginEnd:I

    iget v7, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mFirstRowMarginBottom:I

    invoke-virtual {v6, v3, v4, v5, v7}, Landroid/widget/GridLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_1

    :cond_3
    iget v3, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mLeftItemMarginStart:I

    iget v4, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mFirstRowMarginTop:I

    iget v5, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mLeftItemMarginEnd:I

    iget v7, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mFirstRowMarginBottom:I

    invoke-virtual {v6, v3, v4, v5, v7}, Landroid/widget/GridLayout$LayoutParams;->setMargins(IIII)V

    :goto_1
    invoke-virtual {p0, v2, v6}, Landroid/widget/GridLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    sget v3, Lcom/android/settings/R$id;->vv_item:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/TextureView;

    iget-object v4, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mData:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/haptic/data/HapticResource;

    invoke-virtual {v4}, Lcom/android/settings/haptic/data/HapticResource;->getShowRes()I

    move-result v4

    iget-object v5, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mData:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/settings/haptic/data/HapticResource;

    invoke-virtual {v5}, Lcom/android/settings/haptic/data/HapticResource;->getVideoBgRes()I

    move-result v5

    iget-object v6, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mData:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/settings/haptic/data/HapticResource;

    invoke-virtual {v6}, Lcom/android/settings/haptic/data/HapticResource;->getContentDescription()I

    move-result v6

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    sget v6, Lcom/android/settings/R$id;->img_holder:I

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    sget v7, Lcom/android/settings/R$id;->img_item:I

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v7, v5}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    new-instance v5, Lcom/android/settings/haptic/widget/HapticGridView$1;

    invoke-direct {v5, p0, v1, v4}, Lcom/android/settings/haptic/widget/HapticGridView$1;-><init>(Lcom/android/settings/haptic/widget/HapticGridView;II)V

    invoke-virtual {v3, v5}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    new-instance v3, Lcom/android/settings/haptic/widget/HapticGridView$2;

    invoke-direct {v3, p0, v1, v6}, Lcom/android/settings/haptic/widget/HapticGridView$2;-><init>(Lcom/android/settings/haptic/widget/HapticGridView;ILandroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_4
    return-void
.end method

.method private static synthetic lambda$startAlphaAnimation$0(Landroid/view/View;Landroid/animation/ValueAnimator;)V
    .locals 0

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    invoke-virtual {p0, p1}, Landroid/view/View;->setAlpha(F)V

    return-void
.end method

.method private playVideo(Landroid/view/View;ILandroid/view/View;ZI)V
    .locals 1

    if-eqz p4, :cond_0

    iget-object p3, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mMediaPlayerList:[Landroid/media/MediaPlayer;

    aget-object p3, p3, p2

    invoke-virtual {p3}, Landroid/media/MediaPlayer;->pause()V

    iget-object p3, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mMediaPlayerList:[Landroid/media/MediaPlayer;

    aget-object p3, p3, p2

    const/4 p4, 0x0

    invoke-virtual {p3, p4}, Landroid/media/MediaPlayer;->seekTo(I)V

    new-instance p3, Landroid/animation/AnimatorSet;

    invoke-direct {p3}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object p3, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    new-instance p4, Lcom/android/settings/haptic/widget/HapticGridView$3;

    invoke-direct {p4, p0, p2}, Lcom/android/settings/haptic/widget/HapticGridView$3;-><init>(Lcom/android/settings/haptic/widget/HapticGridView;I)V

    invoke-virtual {p3, p4}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    const/4 p2, 0x2

    new-array p3, p2, [F

    fill-array-data p3, :array_0

    const-string/jumbo p4, "scaleX"

    invoke-static {p1, p4, p3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object p3

    new-array p2, p2, [F

    fill-array-data p2, :array_1

    const-string/jumbo p4, "scaleY"

    invoke-static {p1, p4, p2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object p1

    iget-object p2, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    const-wide/16 p4, 0x12c

    invoke-virtual {p2, p4, p5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    iget-object p2, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    new-instance p4, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {p4}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {p2, p4}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object p2, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {p2, p3}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    iget-object p1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {p1}, Landroid/animation/AnimatorSet;->start()V

    const/4 p1, -0x1

    iput p1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mPlayingIndex:I

    return-void

    :cond_0
    iget-object p4, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mMediaPlayerList:[Landroid/media/MediaPlayer;

    aget-object p5, p4, p2

    if-nez p5, :cond_1

    new-instance p5, Landroid/media/MediaPlayer;

    invoke-direct {p5}, Landroid/media/MediaPlayer;-><init>()V

    aput-object p5, p4, p2

    :cond_1
    :try_start_0
    iget-object p4, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mMediaPlayerList:[Landroid/media/MediaPlayer;

    aget-object p4, p4, p2

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object p5

    iget-object v0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mData:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/haptic/data/HapticResource;

    invoke-virtual {v0}, Lcom/android/settings/haptic/data/HapticResource;->getSubTitleRes()I

    move-result v0

    invoke-static {p5, v0}, Lcom/android/settings/haptic/SubtitleProcessor;->getSubtitleFile(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p5

    const-string v0, "application/x-subrip"

    invoke-virtual {p4, p5, v0}, Landroid/media/MediaPlayer;->addTimedTextSource(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p4, 0x3

    iget-object p5, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mMediaPlayerList:[Landroid/media/MediaPlayer;

    aget-object p5, p5, p2

    invoke-virtual {p5}, Landroid/media/MediaPlayer;->getTrackInfo()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object p5

    invoke-static {p4, p5}, Lcom/android/settings/haptic/SubtitleProcessor;->findTrackIndexFor(I[Landroid/media/MediaPlayer$TrackInfo;)I

    move-result p4

    if-ltz p4, :cond_2

    iget-object p5, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mMediaPlayerList:[Landroid/media/MediaPlayer;

    aget-object p5, p5, p2

    invoke-virtual {p5, p4}, Landroid/media/MediaPlayer;->selectTrack(I)V

    goto :goto_0

    :cond_2
    const-string p4, "HapticGridView"

    const-string p5, "Cannot find text track!"

    invoke-static {p4, p5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object p4, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mMediaPlayerList:[Landroid/media/MediaPlayer;

    aget-object p4, p4, p2

    new-instance p5, Lcom/android/settings/haptic/widget/HapticGridView$4;

    invoke-direct {p5, p0}, Lcom/android/settings/haptic/widget/HapticGridView$4;-><init>(Lcom/android/settings/haptic/widget/HapticGridView;)V

    invoke-virtual {p4, p5}, Landroid/media/MediaPlayer;->setOnTimedTextListener(Landroid/media/MediaPlayer$OnTimedTextListener;)V

    iget-object p4, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mMediaPlayerList:[Landroid/media/MediaPlayer;

    aget-object p4, p4, p2

    new-instance p5, Lcom/android/settings/haptic/widget/HapticGridView$5;

    invoke-direct {p5, p0, p2, p3, p1}, Lcom/android/settings/haptic/widget/HapticGridView$5;-><init>(Lcom/android/settings/haptic/widget/HapticGridView;ILandroid/view/View;Landroid/view/View;)V

    invoke-virtual {p4, p5}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object p1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mMediaPlayerList:[Landroid/media/MediaPlayer;

    aget-object p1, p1, p2

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->start()V

    iget-object p1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mMediaPlayerList:[Landroid/media/MediaPlayer;

    aget-object p1, p1, p2

    new-instance p4, Lcom/android/settings/haptic/widget/HapticGridView$6;

    invoke-direct {p4, p0, p2, p3}, Lcom/android/settings/haptic/widget/HapticGridView$6;-><init>(Lcom/android/settings/haptic/widget/HapticGridView;ILandroid/view/View;)V

    invoke-virtual {p1, p4}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    invoke-direct {p0, p2, p3}, Lcom/android/settings/haptic/widget/HapticGridView;->hideImgHolder(ILandroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_1
    return-void

    :array_0
    .array-data 4
        0x3f8b851f    # 1.09f
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x3f8b851f    # 1.09f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private startAlphaAnimation(Landroid/view/View;FFJ)V
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput p2, v0, v1

    const/4 p2, 0x1

    aput p3, v0, p2

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object p2

    invoke-virtual {p2, p4, p5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object p2

    iput-object p2, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mAlphaAnimation:Landroid/animation/ValueAnimator;

    new-instance p3, Lcom/android/settings/haptic/widget/HapticGridView$$ExternalSyntheticLambda0;

    invoke-direct {p3, p1}, Lcom/android/settings/haptic/widget/HapticGridView$$ExternalSyntheticLambda0;-><init>(Landroid/view/View;)V

    invoke-virtual {p2, p3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object p1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mAlphaAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {p1, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    iget-object p0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mAlphaAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {p0}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method

.method private stopPlayingVideo()V
    .locals 7

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mData:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mMediaPlayerList:[Landroid/media/MediaPlayer;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mChildView:[Landroid/view/View;

    aget-object v1, v1, v0

    sget v2, Lcom/android/settings/R$id;->img_holder:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iget-object v1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mChildView:[Landroid/view/View;

    aget-object v2, v1, v0

    const/4 v5, 0x1

    const/4 v6, -0x1

    move-object v1, p0

    move v3, v0

    invoke-direct/range {v1 .. v6}, Lcom/android/settings/haptic/widget/HapticGridView;->playVideo(Landroid/view/View;ILandroid/view/View;ZI)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/haptic/widget/HapticGridView;->cancelAnimation()V

    return-void
.end method


# virtual methods
.method cancelAnimation()V
    .locals 2

    goto/32 :goto_7

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    goto/32 :goto_3

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mAlphaAnimation:Landroid/animation/ValueAnimator;

    goto/32 :goto_8

    nop

    :goto_3
    iget-object v0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    goto/32 :goto_4

    nop

    :goto_4
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    goto/32 :goto_a

    nop

    :goto_5
    iget-object v0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mAlphaAnimation:Landroid/animation/ValueAnimator;

    goto/32 :goto_9

    nop

    :goto_6
    if-nez v0, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_1

    nop

    :goto_7
    iget-object v0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    goto/32 :goto_f

    nop

    :goto_8
    if-nez v0, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_c

    nop

    :goto_9
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    goto/32 :goto_d

    nop

    :goto_a
    iput-object v1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    :goto_b
    goto/32 :goto_2

    nop

    :goto_c
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    goto/32 :goto_5

    nop

    :goto_d
    iput-object v1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mAlphaAnimation:Landroid/animation/ValueAnimator;

    :goto_e
    goto/32 :goto_0

    nop

    :goto_f
    const/4 v1, 0x0

    goto/32 :goto_6

    nop
.end method

.method public onDestroy()V
    .locals 0

    return-void
.end method

.method public onPageChange()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/haptic/widget/HapticGridView;->stopPlayingVideo()V

    return-void
.end method

.method public onStop()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/haptic/widget/HapticGridView;->stopPlayingVideo()V

    return-void
.end method

.method public playExtPatternById(I)V
    .locals 3

    invoke-static {}, Lmiui/util/HapticFeedbackUtil;->isSupportLinearMotorVibrate()Z

    move-result v0

    const-string v1, "HapticGridView"

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mHapticUtil:Lmiui/util/HapticFeedbackUtil;

    invoke-virtual {v0, p1}, Lmiui/util/HapticFeedbackUtil;->isSupportExtHapticFeedback(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "performExtHapticFeedback id:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mHapticUtil:Lmiui/util/HapticFeedbackUtil;

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lmiui/util/HapticFeedbackUtil;->performExtHapticFeedback(IZ)Z

    goto :goto_0

    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Not support this rtp:$rtpEffectId! id:"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Not support linearMotor! id:"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public playPatternById(I)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mHapticCompat:Lmiuix/util/HapticFeedbackCompat;

    invoke-virtual {v0}, Lmiuix/util/HapticFeedbackCompat;->supportLinearMotor()Z

    move-result v0

    const-string v1, "HapticGridView"

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "performHapticFeedback id:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p0, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mHapticCompat:Lmiuix/util/HapticFeedbackCompat;

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lmiuix/util/HapticFeedbackCompat;->performHapticFeedback(IZ)Z

    goto :goto_0

    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Not support linearMotor! id:"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public setType(I)V
    .locals 2

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/settings/haptic/data/ResourceWrapper;->loadResource(Landroid/content/Context;I)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mData:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/4 v0, 0x2

    div-int/2addr p1, v0

    add-int/lit8 p1, p1, 0x1

    invoke-virtual {p0, p1}, Landroid/widget/GridLayout;->setRowCount(I)V

    invoke-virtual {p0, v0}, Landroid/widget/GridLayout;->setColumnCount(I)V

    invoke-virtual {p0}, Landroid/widget/GridLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iget p1, p1, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mLeftItemMarginStart:I

    mul-int/2addr v1, v0

    sub-int/2addr p1, v1

    iget v1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mLeftItemMarginEnd:I

    mul-int/2addr v1, v0

    sub-int/2addr p1, v1

    div-int/2addr p1, v0

    iput p1, p0, Lcom/android/settings/haptic/widget/HapticGridView;->mItemWidth:I

    invoke-direct {p0}, Lcom/android/settings/haptic/widget/HapticGridView;->initNormalView()V

    return-void
.end method
