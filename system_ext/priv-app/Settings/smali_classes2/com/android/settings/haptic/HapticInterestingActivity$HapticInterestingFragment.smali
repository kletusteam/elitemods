.class public Lcom/android/settings/haptic/HapticInterestingActivity$HapticInterestingFragment;
.super Landroidx/fragment/app/Fragment;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/haptic/HapticInterestingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HapticInterestingFragment"
.end annotation


# instance fields
.field mGridView:Lcom/android/settings/haptic/widget/HapticGridView;

.field mTv:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 0

    sget p0, Lcom/android/settings/R$layout;->fragment_haptic_detail_base:I

    const/4 p3, 0x0

    invoke-virtual {p1, p0, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1, p2}, Landroidx/fragment/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    sget p2, Lcom/android/settings/R$id;->ringtone_grid:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/android/settings/haptic/widget/HapticGridView;

    iput-object p2, p0, Lcom/android/settings/haptic/HapticInterestingActivity$HapticInterestingFragment;->mGridView:Lcom/android/settings/haptic/widget/HapticGridView;

    sget p2, Lcom/android/settings/R$id;->haptic_text:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/android/settings/haptic/HapticInterestingActivity$HapticInterestingFragment;->mTv:Landroid/widget/TextView;

    sget p2, Lcom/android/settings/R$string;->interesting_text:I

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    iget-object p0, p0, Lcom/android/settings/haptic/HapticInterestingActivity$HapticInterestingFragment;->mGridView:Lcom/android/settings/haptic/widget/HapticGridView;

    if-eqz p0, :cond_0

    const/4 p1, 0x6

    invoke-virtual {p0, p1}, Lcom/android/settings/haptic/widget/HapticGridView;->setType(I)V

    :cond_0
    return-void
.end method
