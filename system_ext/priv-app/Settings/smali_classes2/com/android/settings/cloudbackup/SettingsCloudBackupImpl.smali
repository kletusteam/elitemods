.class public Lcom/android/settings/cloudbackup/SettingsCloudBackupImpl;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/xiaomi/settingsdk/backup/ICloudBackup;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getWeiboAccount(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    const-string p0, "account"

    invoke-virtual {p1, p0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/accounts/AccountManager;

    invoke-virtual {p0}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object p0

    array-length p1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_1

    aget-object v1, p0, v0

    iget-object v2, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v3, "com.sina.weibo.account"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object p0, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    return-object p0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string p0, ""

    return-object p0
.end method

.method private static logJSON(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    sget-boolean v0, Lmiui/os/Build;->IS_OFFICIAL_VERSION:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SettingsCloudBackup"

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-object p0
.end method


# virtual methods
.method public getCurrentVersion(Landroid/content/Context;)I
    .locals 0

    const/4 p0, 0x1

    return p0
.end method

.method public onBackupSettings(Landroid/content/Context;Lcom/xiaomi/settingsdk/backup/data/DataPackage;)V
    .locals 3

    const-string v0, "SettingsCloudBackup"

    const-string/jumbo v1, "start settings backup. "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Lcom/android/settings/cloudbackup/ConnectionCloudBackupHelper;->saveToCloud(Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/cloudbackup/SettingsCloudBackupImpl;->logJSON(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "Connection"

    invoke-virtual {p2, v2, v1}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->addKeyJson(Ljava/lang/String;Lorg/json/JSONObject;)V

    invoke-direct {p0, p1}, Lcom/android/settings/cloudbackup/SettingsCloudBackupImpl;->getWeiboAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    const-string/jumbo v1, "weiboAccount"

    invoke-virtual {p2, v1, p0}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->addKeyValue(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/android/settings/cloudbackup/NotificationCloudBackupHelper;->saveToCloud(Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object p0

    invoke-static {p0}, Lcom/android/settings/cloudbackup/SettingsCloudBackupImpl;->logJSON(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p0

    const-string v1, "NotificationFilter"

    invoke-virtual {p2, v1, p0}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->addKeyJson(Ljava/lang/String;Lorg/json/JSONObject;)V

    invoke-static {p1}, Lcom/android/settings/cloudbackup/StatusBarCloudBackupHelper;->saveToCloud(Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object p0

    invoke-static {p0}, Lcom/android/settings/cloudbackup/SettingsCloudBackupImpl;->logJSON(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p0

    const-string v1, "StatusBar"

    invoke-virtual {p2, v1, p0}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->addKeyJson(Ljava/lang/String;Lorg/json/JSONObject;)V

    invoke-static {p1}, Lcom/android/settings/cloudbackup/AdvancedSettingsCloudBackupHelper;->saveToCloud(Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object p0

    invoke-static {p0}, Lcom/android/settings/cloudbackup/SettingsCloudBackupImpl;->logJSON(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p0

    const-string v1, "AdvanceSettings"

    invoke-virtual {p2, v1, p0}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->addKeyJson(Ljava/lang/String;Lorg/json/JSONObject;)V

    invoke-static {p1}, Lcom/android/settings/cloudbackup/KeySettingsCloudBackupHelper;->saveToCloud(Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object p0

    invoke-static {p0}, Lcom/android/settings/cloudbackup/SettingsCloudBackupImpl;->logJSON(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p0

    const-string v1, "ScreenKeySettings"

    invoke-virtual {p2, v1, p0}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->addKeyJson(Ljava/lang/String;Lorg/json/JSONObject;)V

    invoke-static {p1}, Lcom/android/settings/cloudbackup/SoundAndVibrateCloudBackupHelper;->saveToCloud(Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object p0

    invoke-static {p0}, Lcom/android/settings/cloudbackup/SettingsCloudBackupImpl;->logJSON(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p0

    const-string v1, "SoundAndVibrateSettings"

    invoke-virtual {p2, v1, p0}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->addKeyJson(Ljava/lang/String;Lorg/json/JSONObject;)V

    invoke-static {p1}, Lcom/android/settings/cloudbackup/DisplaySettingsCloudBackupHelper;->saveToCloud(Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object p0

    invoke-static {p0}, Lcom/android/settings/cloudbackup/SettingsCloudBackupImpl;->logJSON(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p0

    const-string v1, "DisplaySettings"

    invoke-virtual {p2, v1, p0}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->addKeyJson(Ljava/lang/String;Lorg/json/JSONObject;)V

    invoke-static {p1}, Lcom/android/settings/cloudbackup/AccessibilityCloudBackupHelper;->saveToCloud(Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object p0

    invoke-static {p0}, Lcom/android/settings/cloudbackup/SettingsCloudBackupImpl;->logJSON(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p0

    const-string v1, "Accessibility"

    invoke-virtual {p2, v1, p0}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->addKeyJson(Ljava/lang/String;Lorg/json/JSONObject;)V

    invoke-static {p1}, Lcom/android/settings/cloudbackup/LockScreenSettingsCloudBackupHelper;->saveToCloud(Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object p0

    invoke-static {p0}, Lcom/android/settings/cloudbackup/SettingsCloudBackupImpl;->logJSON(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p0

    const-string v1, "LockScreen"

    invoke-virtual {p2, v1, p0}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->addKeyJson(Ljava/lang/String;Lorg/json/JSONObject;)V

    invoke-static {p1}, Lcom/android/settings/cloudbackup/SilentSettingsCloudBackupHelper;->saveToCloud(Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object p0

    invoke-static {p0}, Lcom/android/settings/cloudbackup/SettingsCloudBackupImpl;->logJSON(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p0

    const-string v1, "SilentSettings"

    invoke-virtual {p2, v1, p0}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->addKeyJson(Ljava/lang/String;Lorg/json/JSONObject;)V

    invoke-static {p1}, Lcom/android/settings/cloudbackup/DefaultAppSettingsCloudBackupHelper;->saveToCloud(Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object p0

    invoke-static {p0}, Lcom/android/settings/cloudbackup/SettingsCloudBackupImpl;->logJSON(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p0

    const-string v1, "DefaultAppSettings"

    invoke-virtual {p2, v1, p0}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->addKeyJson(Ljava/lang/String;Lorg/json/JSONObject;)V

    invoke-static {p1, p2}, Lcom/android/settings/cloudbackup/RingtoneCloudBackupHelper;->backupRingtones(Landroid/content/Context;Lcom/xiaomi/settingsdk/backup/data/DataPackage;)V

    const-string p0, "end settings backup. "

    invoke-static {v0, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onRestoreSettings(Landroid/content/Context;Lcom/xiaomi/settingsdk/backup/data/DataPackage;I)V
    .locals 11

    const-string p0, "DefaultAppSettings"

    const-string p3, "SilentSettings"

    const-string v0, "LockScreen"

    const-string v1, "Accessibility"

    const-string v2, "DisplaySettings"

    const-string v3, "SoundAndVibrateSettings"

    const-string v4, "ScreenKeySettings"

    const-string v5, "AdvanceSettings"

    const-string v6, "StatusBar"

    const-string v7, "NotificationFilter"

    const-string v8, "Connection"

    const-string v9, "SettingsCloudBackup"

    const-string/jumbo v10, "start settings restore. "

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-virtual {p2, v8}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object v10

    if-eqz v10, :cond_0

    invoke-virtual {p2, v8}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object v8

    invoke-virtual {v8}, Lcom/xiaomi/settingsdk/backup/data/SettingItem;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/json/JSONObject;

    invoke-static {v8}, Lcom/android/settings/cloudbackup/SettingsCloudBackupImpl;->logJSON(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v8

    invoke-static {p1, v8}, Lcom/android/settings/cloudbackup/ConnectionCloudBackupHelper;->restoreFromCloud(Landroid/content/Context;Lorg/json/JSONObject;)V

    :cond_0
    invoke-virtual {p2, v7}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object v8

    if-eqz v8, :cond_1

    invoke-virtual {p2, v7}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object v7

    invoke-virtual {v7}, Lcom/xiaomi/settingsdk/backup/data/SettingItem;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/json/JSONObject;

    invoke-static {v7}, Lcom/android/settings/cloudbackup/SettingsCloudBackupImpl;->logJSON(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v7

    invoke-static {p1, v7}, Lcom/android/settings/cloudbackup/NotificationCloudBackupHelper;->restoreFromCloud(Landroid/content/Context;Lorg/json/JSONObject;)V

    :cond_1
    invoke-virtual {p2, v6}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {p2, v6}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object v6

    invoke-virtual {v6}, Lcom/xiaomi/settingsdk/backup/data/SettingItem;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/json/JSONObject;

    invoke-static {v6}, Lcom/android/settings/cloudbackup/SettingsCloudBackupImpl;->logJSON(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v6

    invoke-static {p1, v6}, Lcom/android/settings/cloudbackup/StatusBarCloudBackupHelper;->restoreFromCloud(Landroid/content/Context;Lorg/json/JSONObject;)V

    :cond_2
    invoke-virtual {p2, v5}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {p2, v5}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object v5

    invoke-virtual {v5}, Lcom/xiaomi/settingsdk/backup/data/SettingItem;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/json/JSONObject;

    invoke-static {v5}, Lcom/android/settings/cloudbackup/SettingsCloudBackupImpl;->logJSON(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v5

    invoke-static {p1, v5}, Lcom/android/settings/cloudbackup/AdvancedSettingsCloudBackupHelper;->restoreFromCloud(Landroid/content/Context;Lorg/json/JSONObject;)V

    :cond_3
    invoke-virtual {p2, v4}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {p2, v4}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/xiaomi/settingsdk/backup/data/SettingItem;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/json/JSONObject;

    invoke-static {v4}, Lcom/android/settings/cloudbackup/SettingsCloudBackupImpl;->logJSON(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/android/settings/cloudbackup/KeySettingsCloudBackupHelper;->restoreFromCloud(Landroid/content/Context;Lorg/json/JSONObject;)V

    :cond_4
    invoke-virtual {p2, v3}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {p2, v3}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/xiaomi/settingsdk/backup/data/SettingItem;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/json/JSONObject;

    invoke-static {v3}, Lcom/android/settings/cloudbackup/SettingsCloudBackupImpl;->logJSON(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-static {p1, v3}, Lcom/android/settings/cloudbackup/SoundAndVibrateCloudBackupHelper;->restoreFromCloud(Landroid/content/Context;Lorg/json/JSONObject;)V

    :cond_5
    invoke-virtual {p2, v2}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {p2, v2}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/xiaomi/settingsdk/backup/data/SettingItem;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/json/JSONObject;

    invoke-static {v2}, Lcom/android/settings/cloudbackup/SettingsCloudBackupImpl;->logJSON(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/android/settings/cloudbackup/DisplaySettingsCloudBackupHelper;->restoreFromCloud(Landroid/content/Context;Lorg/json/JSONObject;)V

    :cond_6
    invoke-virtual {p2, v1}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {p2, v1}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/xiaomi/settingsdk/backup/data/SettingItem;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/json/JSONObject;

    invoke-static {v1}, Lcom/android/settings/cloudbackup/SettingsCloudBackupImpl;->logJSON(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/android/settings/cloudbackup/AccessibilityCloudBackupHelper;->restoreFromCloud(Landroid/content/Context;Lorg/json/JSONObject;)V

    :cond_7
    invoke-virtual {p2, v0}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual {p2, v0}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/xiaomi/settingsdk/backup/data/SettingItem;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    invoke-static {v0}, Lcom/android/settings/cloudbackup/SettingsCloudBackupImpl;->logJSON(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/android/settings/cloudbackup/LockScreenSettingsCloudBackupHelper;->restoreFromCloud(Landroid/content/Context;Lorg/json/JSONObject;)V

    :cond_8
    invoke-virtual {p2, p3}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {p2, p3}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object p3

    invoke-virtual {p3}, Lcom/xiaomi/settingsdk/backup/data/SettingItem;->getValue()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lorg/json/JSONObject;

    invoke-static {p3}, Lcom/android/settings/cloudbackup/SettingsCloudBackupImpl;->logJSON(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p3

    invoke-static {p1, p3}, Lcom/android/settings/cloudbackup/SilentSettingsCloudBackupHelper;->restoreFromCloud(Landroid/content/Context;Lorg/json/JSONObject;)V

    :cond_9
    invoke-virtual {p2, p0}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object p3

    if-eqz p3, :cond_a

    invoke-virtual {p2, p0}, Lcom/xiaomi/settingsdk/backup/data/DataPackage;->get(Ljava/lang/String;)Lcom/xiaomi/settingsdk/backup/data/SettingItem;

    move-result-object p0

    invoke-virtual {p0}, Lcom/xiaomi/settingsdk/backup/data/SettingItem;->getValue()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lorg/json/JSONObject;

    invoke-static {p0}, Lcom/android/settings/cloudbackup/SettingsCloudBackupImpl;->logJSON(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object p0

    invoke-static {p1, p0}, Lcom/android/settings/cloudbackup/DefaultAppSettingsCloudBackupHelper;->restoreFromCloud(Landroid/content/Context;Lorg/json/JSONObject;)V

    :cond_a
    invoke-static {p1, p2}, Lcom/android/settings/cloudbackup/RingtoneCloudBackupHelper;->restoreRingtones(Landroid/content/Context;Lcom/xiaomi/settingsdk/backup/data/DataPackage;)V

    invoke-static {p2}, Lcom/xiaomi/settingsdk/backup/SettingsBackupHelper;->restoreFiles(Lcom/xiaomi/settingsdk/backup/data/DataPackage;)V

    const-string p0, "end settings restore. "

    invoke-static {v9, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string/jumbo p0, "settings restore exception."

    invoke-static {v9, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string p0, "CloudBackupRestoreException"

    invoke-static {p0}, Lcom/android/settings/cloudbackup/CloudBackupException;->trackException(Ljava/lang/String;)V

    :goto_0
    return-void
.end method
