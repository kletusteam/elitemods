.class Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/MiuiTetherSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TetherChangeReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/MiuiTetherSettings;


# direct methods
.method private constructor <init>(Lcom/android/settings/MiuiTetherSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/MiuiTetherSettings;Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;-><init>(Lcom/android/settings/MiuiTetherSettings;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string v0, "android.net.conn.TETHER_STATE_CHANGED"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-wide/16 v1, 0x1f4

    const/4 v3, 0x2

    if-eqz v0, :cond_1

    const-string p1, "availableArray"

    invoke-virtual {p2, p1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    const-string/jumbo v0, "tetherArray"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    const-string v4, "erroredArray"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    iget-object v5, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {v5}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fgetisClickUsb(Lcom/android/settings/MiuiTetherSettings;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object p1, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p1}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$mclearDelayMsg(Lcom/android/settings/MiuiTetherSettings;)V

    iget-object p1, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p1}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fgetmDelayHandler(Lcom/android/settings/MiuiTetherSettings;)Landroid/os/Handler;

    move-result-object p1

    if-eqz p1, :cond_c

    iget-object p1, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p1}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fgetmDelayHandler(Lcom/android/settings/MiuiTetherSettings;)Landroid/os/Handler;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object p1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v3, "intent"

    invoke-virtual {v0, v3, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {p1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object p0, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p0}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fgetmDelayHandler(Lcom/android/settings/MiuiTetherSettings;)Landroid/os/Handler;

    move-result-object p0

    invoke-virtual {p0, p1, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_2

    :cond_0
    iget-object p0, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p2

    new-array p2, p2, [Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/String;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p2

    new-array p2, p2, [Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [Ljava/lang/String;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {p0, p1, p2, v0}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$mupdateState(Lcom/android/settings/MiuiTetherSettings;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_1
    const-string v0, "android.intent.action.MEDIA_SHARED"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v4, 0x1

    if-eqz v0, :cond_2

    iget-object p1, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p1, v4}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fputmMassStorageActive(Lcom/android/settings/MiuiTetherSettings;Z)V

    iget-object p0, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p0}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fgetisClickUsb(Lcom/android/settings/MiuiTetherSettings;)Z

    move-result p1

    xor-int/2addr p1, v4

    invoke-static {p0, p1}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$mupdateState(Lcom/android/settings/MiuiTetherSettings;Z)V

    goto/16 :goto_2

    :cond_2
    const-string v0, "android.intent.action.MEDIA_UNSHARED"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v5, 0x0

    if-eqz v0, :cond_3

    iget-object p1, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p1, v5}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fputmMassStorageActive(Lcom/android/settings/MiuiTetherSettings;Z)V

    iget-object p0, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p0}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fgetisClickUsb(Lcom/android/settings/MiuiTetherSettings;)Z

    move-result p1

    xor-int/2addr p1, v4

    invoke-static {p0, p1}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$mupdateState(Lcom/android/settings/MiuiTetherSettings;Z)V

    goto/16 :goto_2

    :cond_3
    const-string v0, "android.hardware.usb.action.USB_STATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object p1, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p1}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fgetisClickUsb(Lcom/android/settings/MiuiTetherSettings;)Z

    move-result p1

    const-string v0, "connected"

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p2

    invoke-static {p1, p2}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fputtmpUsbConnected(Lcom/android/settings/MiuiTetherSettings;Z)V

    iget-object p1, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p1}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$mclearDelayMsg(Lcom/android/settings/MiuiTetherSettings;)V

    iget-object p0, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p0}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fgetmDelayHandler(Lcom/android/settings/MiuiTetherSettings;)Landroid/os/Handler;

    move-result-object p0

    invoke-virtual {p0, v4, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_2

    :cond_4
    iget-object p1, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-virtual {p2, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p2

    invoke-static {p1, p2}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fputmUsbConnected(Lcom/android/settings/MiuiTetherSettings;Z)V

    iget-object p0, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p0}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$mupdateState(Lcom/android/settings/MiuiTetherSettings;)V

    goto/16 :goto_2

    :cond_5
    const-string v0, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "MiuiTetherSettings"

    if-eqz v0, :cond_a

    iget-object p1, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p1}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fgetmBluetoothEnableForTether(Lcom/android/settings/MiuiTetherSettings;)Z

    move-result p1

    if-eqz p1, :cond_9

    const-string p1, "android.bluetooth.adapter.extra.STATE"

    const/high16 v0, -0x80000000

    invoke-virtual {p2, p1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    if-eq p1, v0, :cond_8

    const/16 p2, 0xa

    if-eq p1, p2, :cond_8

    const/16 p2, 0xc

    if-eq p1, p2, :cond_6

    goto :goto_1

    :cond_6
    iget-object p1, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p1}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fgetmBluetoothPan(Lcom/android/settings/MiuiTetherSettings;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/bluetooth/BluetoothPan;

    if-eqz p1, :cond_9

    iget-object p1, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p1}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fgetmTetheringProvisionNeeded(Lcom/android/settings/MiuiTetherSettings;)Z

    move-result p1

    if-eqz p1, :cond_7

    const-string p1, "Start Bluetooth Tethering!"

    invoke-static {v1, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p1, v5}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fputmTetheringProvisionNeeded(Lcom/android/settings/MiuiTetherSettings;Z)V

    iget-object p1, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p1, v3}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$mstartTethering(Lcom/android/settings/MiuiTetherSettings;I)V

    goto :goto_0

    :cond_7
    iget-object p1, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p1}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fgetmCm(Lcom/android/settings/MiuiTetherSettings;)Landroid/net/ConnectivityManager;

    move-result-object p1

    iget-object p2, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p2}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fgetmTetherChoice(Lcom/android/settings/MiuiTetherSettings;)I

    move-result p2

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {v0}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fgetmStartTetheringCallback(Lcom/android/settings/MiuiTetherSettings;)Lcom/android/settings/MiuiTetherSettings$OnStartTetheringCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {v1}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fgetmHandler(Lcom/android/settings/MiuiTetherSettings;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {p1, p2, v4, v0, v1}, Landroid/net/ConnectivityManager;->startTethering(IZLandroid/net/ConnectivityManager$OnStartTetheringCallback;Landroid/os/Handler;)V

    :goto_0
    iget-object p1, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p1, v5}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fputmBluetoothEnableForTether(Lcom/android/settings/MiuiTetherSettings;Z)V

    goto :goto_1

    :cond_8
    iget-object p1, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p1, v5}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fputmBluetoothEnableForTether(Lcom/android/settings/MiuiTetherSettings;Z)V

    :cond_9
    :goto_1
    iget-object p0, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p0}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fgetisClickUsb(Lcom/android/settings/MiuiTetherSettings;)Z

    move-result p1

    xor-int/2addr p1, v4

    invoke-static {p0, p1}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$mupdateState(Lcom/android/settings/MiuiTetherSettings;Z)V

    goto :goto_2

    :cond_a
    const-string p2, "android.bluetooth.action.STATE_CHANGED"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_b

    iget-object p0, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p0}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fgetisClickUsb(Lcom/android/settings/MiuiTetherSettings;)Z

    move-result p1

    xor-int/2addr p1, v4

    invoke-static {p0, p1}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$mupdateState(Lcom/android/settings/MiuiTetherSettings;Z)V

    goto :goto_2

    :cond_b
    const-string p2, "android.bluetooth.pan.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_c

    const-string/jumbo p1, "update statue when receive bluetoothPan state changed!"

    invoke-static {v1, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p0, p0, Lcom/android/settings/MiuiTetherSettings$TetherChangeReceiver;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p0}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fgetisClickUsb(Lcom/android/settings/MiuiTetherSettings;)Z

    move-result p1

    xor-int/2addr p1, v4

    invoke-static {p0, p1}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$mupdateState(Lcom/android/settings/MiuiTetherSettings;Z)V

    :cond_c
    :goto_2
    return-void
.end method
