.class Lcom/android/settings/MiuiMasterClearApplyActivity$1;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/MiuiMasterClearApplyActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/MiuiMasterClearApplyActivity;


# direct methods
.method constructor <init>(Lcom/android/settings/MiuiMasterClearApplyActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity$1;->this$0:Lcom/android/settings/MiuiMasterClearApplyActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    iget-object p1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity$1;->this$0:Lcom/android/settings/MiuiMasterClearApplyActivity;

    invoke-static {p1}, Lcom/android/settings/MiuiMasterClearApplyActivity;->-$$Nest$fgetmAutoNextStepTime(Lcom/android/settings/MiuiMasterClearApplyActivity;)I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    invoke-static {p1, v0}, Lcom/android/settings/MiuiMasterClearApplyActivity;->-$$Nest$fputmAutoNextStepTime(Lcom/android/settings/MiuiMasterClearApplyActivity;I)V

    iget-object p1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity$1;->this$0:Lcom/android/settings/MiuiMasterClearApplyActivity;

    invoke-static {p1}, Lcom/android/settings/MiuiMasterClearApplyActivity;->-$$Nest$fgetmCurrentStep(Lcom/android/settings/MiuiMasterClearApplyActivity;)I

    move-result p1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity$1;->this$0:Lcom/android/settings/MiuiMasterClearApplyActivity;

    invoke-static {p1}, Lcom/android/settings/MiuiMasterClearApplyActivity;->-$$Nest$fgetmAutoNextStepTime(Lcom/android/settings/MiuiMasterClearApplyActivity;)I

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity$1;->this$0:Lcom/android/settings/MiuiMasterClearApplyActivity;

    invoke-static {p1}, Lcom/android/settings/MiuiMasterClearApplyActivity;->-$$Nest$fgetmAcceptButton(Lcom/android/settings/MiuiMasterClearApplyActivity;)Landroid/widget/Button;

    move-result-object p1

    sget v0, Lcom/android/settings/R$string;->button_text_ok:I

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(I)V

    iget-object p0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity$1;->this$0:Lcom/android/settings/MiuiMasterClearApplyActivity;

    invoke-static {p0}, Lcom/android/settings/MiuiMasterClearApplyActivity;->-$$Nest$fgetmAcceptButton(Lcom/android/settings/MiuiMasterClearApplyActivity;)Landroid/widget/Button;

    move-result-object p0

    invoke-virtual {p0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_1

    :cond_0
    iget-object p1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity$1;->this$0:Lcom/android/settings/MiuiMasterClearApplyActivity;

    invoke-static {p1}, Lcom/android/settings/MiuiMasterClearApplyActivity;->-$$Nest$fgetmAutoNextStepTime(Lcom/android/settings/MiuiMasterClearApplyActivity;)I

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity$1;->this$0:Lcom/android/settings/MiuiMasterClearApplyActivity;

    invoke-static {p1}, Lcom/android/settings/MiuiMasterClearApplyActivity;->-$$Nest$fgetmAcceptButton(Lcom/android/settings/MiuiMasterClearApplyActivity;)Landroid/widget/Button;

    move-result-object p1

    sget v0, Lcom/android/settings/R$string;->button_text_next_step:I

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(I)V

    iget-object p0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity$1;->this$0:Lcom/android/settings/MiuiMasterClearApplyActivity;

    invoke-static {p0}, Lcom/android/settings/MiuiMasterClearApplyActivity;->-$$Nest$fgetmAcceptButton(Lcom/android/settings/MiuiMasterClearApplyActivity;)Landroid/widget/Button;

    move-result-object p0

    invoke-virtual {p0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity$1;->this$0:Lcom/android/settings/MiuiMasterClearApplyActivity;

    invoke-static {p1}, Lcom/android/settings/MiuiMasterClearApplyActivity;->-$$Nest$fgetmCurrentStep(Lcom/android/settings/MiuiMasterClearApplyActivity;)I

    move-result p1

    const/4 v2, 0x0

    if-ne p1, v0, :cond_2

    iget-object p1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity$1;->this$0:Lcom/android/settings/MiuiMasterClearApplyActivity;

    invoke-static {p1}, Lcom/android/settings/MiuiMasterClearApplyActivity;->-$$Nest$fgetmAcceptButton(Lcom/android/settings/MiuiMasterClearApplyActivity;)Landroid/widget/Button;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity$1;->this$0:Lcom/android/settings/MiuiMasterClearApplyActivity;

    sget v3, Lcom/android/settings/R$string;->button_text_ok_timer:I

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/android/settings/MiuiMasterClearApplyActivity;->-$$Nest$fgetmAutoNextStepTime(Lcom/android/settings/MiuiMasterClearApplyActivity;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {v0, v3, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity$1;->this$0:Lcom/android/settings/MiuiMasterClearApplyActivity;

    invoke-static {p1}, Lcom/android/settings/MiuiMasterClearApplyActivity;->-$$Nest$fgetmAcceptButton(Lcom/android/settings/MiuiMasterClearApplyActivity;)Landroid/widget/Button;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity$1;->this$0:Lcom/android/settings/MiuiMasterClearApplyActivity;

    sget v3, Lcom/android/settings/R$string;->button_text_next_step_timer:I

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/android/settings/MiuiMasterClearApplyActivity;->-$$Nest$fgetmAutoNextStepTime(Lcom/android/settings/MiuiMasterClearApplyActivity;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {v0, v3, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object p1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity$1;->this$0:Lcom/android/settings/MiuiMasterClearApplyActivity;

    invoke-static {p1}, Lcom/android/settings/MiuiMasterClearApplyActivity;->-$$Nest$fgetmHandler(Lcom/android/settings/MiuiMasterClearApplyActivity;)Landroid/os/Handler;

    move-result-object p1

    const/16 v0, 0x64

    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeMessages(I)V

    iget-object p0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity$1;->this$0:Lcom/android/settings/MiuiMasterClearApplyActivity;

    invoke-static {p0}, Lcom/android/settings/MiuiMasterClearApplyActivity;->-$$Nest$fgetmHandler(Lcom/android/settings/MiuiMasterClearApplyActivity;)Landroid/os/Handler;

    move-result-object p0

    const-wide/16 v1, 0x3e8

    invoke-virtual {p0, v0, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :goto_1
    return-void
.end method
