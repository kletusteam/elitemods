.class public Lcom/android/settings/ProvisionSetUpMiuiSecurityChooseUnlock;
.super Lmiuix/provision/ProvisionBaseActivity;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/ProvisionSetUpMiuiSecurityChooseUnlock$ProvisionSetUpMiuiSecurityChooseUnlockFragment;,
        Lcom/android/settings/ProvisionSetUpMiuiSecurityChooseUnlock$InternalActivity;
    }
.end annotation


# instance fields
.field private mSetupMiuiChooseLockFragment:Landroidx/preference/PreferenceFragmentCompat;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiuix/provision/ProvisionBaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public hasPreview()Z
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lmiuix/provision/ProvisionBaseActivity;->onCreate(Landroid/os/Bundle;)V

    sget v0, Lcom/android/settings/R$string;->password_entrance_title:I

    invoke-virtual {p0, v0}, Lmiuix/provision/ProvisionBaseActivity;->setTitle(I)V

    iget-object v0, p0, Lmiuix/provision/ProvisionBaseActivity;->mSubTitle:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "add_keyguard_password_then_add_fingerprint"

    invoke-static {p1, v0, v1}, Lcom/android/settings/MiuiKeyguardSettingsUtils;->getBooolExtra(Landroid/os/Bundle;Landroid/content/Intent;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget p1, Lcom/android/settings/R$string;->choose_unlock_fingerprint_msg:I

    invoke-virtual {p0, p1}, Lmiuix/provision/ProvisionBaseActivity;->setSubTitle(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "add_keyguard_password_then_add_face_recoginition"

    invoke-static {p1, v0, v1}, Lcom/android/settings/MiuiKeyguardSettingsUtils;->getBooolExtra(Landroid/os/Bundle;Landroid/content/Intent;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget p1, Lcom/android/settings/R$string;->choose_unlock_face_msg:I

    invoke-virtual {p0, p1}, Lmiuix/provision/ProvisionBaseActivity;->setSubTitle(I)V

    goto :goto_0

    :cond_1
    sget p1, Lcom/android/settings/R$string;->turn_on_keyguard_password_alert:I

    invoke-virtual {p0, p1}, Lmiuix/provision/ProvisionBaseActivity;->setSubTitle(I)V

    :goto_0
    iget-object p1, p0, Lcom/android/settings/ProvisionSetUpMiuiSecurityChooseUnlock;->mSetupMiuiChooseLockFragment:Landroidx/preference/PreferenceFragmentCompat;

    if-nez p1, :cond_2

    new-instance p1, Lcom/android/settings/ProvisionSetUpMiuiSecurityChooseUnlock$ProvisionSetUpMiuiSecurityChooseUnlockFragment;

    invoke-direct {p1}, Lcom/android/settings/ProvisionSetUpMiuiSecurityChooseUnlock$ProvisionSetUpMiuiSecurityChooseUnlockFragment;-><init>()V

    iput-object p1, p0, Lcom/android/settings/ProvisionSetUpMiuiSecurityChooseUnlock;->mSetupMiuiChooseLockFragment:Landroidx/preference/PreferenceFragmentCompat;

    :cond_2
    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    sget v0, Lcom/android/settings/R$id;->provision_container:I

    iget-object v1, p0, Lcom/android/settings/ProvisionSetUpMiuiSecurityChooseUnlock;->mSetupMiuiChooseLockFragment:Landroidx/preference/PreferenceFragmentCompat;

    invoke-virtual {p1, v0, v1}, Landroidx/fragment/app/FragmentTransaction;->replace(ILandroidx/fragment/app/Fragment;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object p0

    invoke-virtual {p0}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    return-void
.end method
