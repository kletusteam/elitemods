.class public interface abstract Lcom/android/settings/VerticalSeekBar$OnSeekBarChangeListener;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/VerticalSeekBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnSeekBarChangeListener"
.end annotation


# virtual methods
.method public abstract onProgressChanged(Lcom/android/settings/VerticalSeekBar;IZ)V
.end method

.method public abstract onStartTrackingTouch(Lcom/android/settings/VerticalSeekBar;)V
.end method

.method public abstract onStopTrackingTouch(Lcom/android/settings/VerticalSeekBar;)V
.end method
