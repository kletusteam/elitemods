.class public Lcom/android/settings/connecteddevice/usb/MiUsbService;
.super Landroid/app/Service;


# instance fields
.field private mShown:Z

.field private screenUnlockReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static bridge synthetic -$$Nest$fgetmShown(Lcom/android/settings/connecteddevice/usb/MiUsbService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/connecteddevice/usb/MiUsbService;->mShown:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmShown(Lcom/android/settings/connecteddevice/usb/MiUsbService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/connecteddevice/usb/MiUsbService;->mShown:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$misUsbConnected(Lcom/android/settings/connecteddevice/usb/MiUsbService;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/connecteddevice/usb/MiUsbService;->isUsbConnected()Z

    move-result p0

    return p0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/connecteddevice/usb/MiUsbService;->mShown:Z

    new-instance v0, Lcom/android/settings/connecteddevice/usb/MiUsbService$1;

    invoke-direct {v0, p0}, Lcom/android/settings/connecteddevice/usb/MiUsbService$1;-><init>(Lcom/android/settings/connecteddevice/usb/MiUsbService;)V

    iput-object v0, p0, Lcom/android/settings/connecteddevice/usb/MiUsbService;->screenUnlockReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private isUsbConnected()Z
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.hardware.usb.action.USB_STATE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Landroid/app/Service;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object p0

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    const-string v1, "connected"

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    return v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 0

    const/4 p0, 0x0

    return-object p0
.end method

.method public onCreate()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/connecteddevice/usb/MiUsbService;->screenUnlockReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.USER_PRESENT"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Landroid/app/Service;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/connecteddevice/usb/MiUsbService;->mShown:Z

    iget-object v0, p0, Lcom/android/settings/connecteddevice/usb/MiUsbService;->screenUnlockReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/app/Service;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method
