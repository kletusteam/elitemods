.class Lcom/android/settings/connecteddevice/usb/MiUsbService$1;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/connecteddevice/usb/MiUsbService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/connecteddevice/usb/MiUsbService;


# direct methods
.method constructor <init>(Lcom/android/settings/connecteddevice/usb/MiUsbService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/connecteddevice/usb/MiUsbService$1;->this$0:Lcom/android/settings/connecteddevice/usb/MiUsbService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    sget-boolean p2, Lcom/android/settings/connecteddevice/usb/UsbModeChooserReceiver;->mSoftSwitch:Z

    if-nez p2, :cond_0

    iget-object p2, p0, Lcom/android/settings/connecteddevice/usb/MiUsbService$1;->this$0:Lcom/android/settings/connecteddevice/usb/MiUsbService;

    invoke-static {p2}, Lcom/android/settings/connecteddevice/usb/MiUsbService;->-$$Nest$fgetmShown(Lcom/android/settings/connecteddevice/usb/MiUsbService;)Z

    move-result p2

    if-nez p2, :cond_0

    iget-object p2, p0, Lcom/android/settings/connecteddevice/usb/MiUsbService$1;->this$0:Lcom/android/settings/connecteddevice/usb/MiUsbService;

    invoke-static {p2}, Lcom/android/settings/connecteddevice/usb/MiUsbService;->-$$Nest$misUsbConnected(Lcom/android/settings/connecteddevice/usb/MiUsbService;)Z

    move-result p2

    if-eqz p2, :cond_0

    new-instance p2, Landroid/content/Intent;

    invoke-direct {p2}, Landroid/content/Intent;-><init>()V

    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.android.settings"

    const-string v2, "com.android.settings.Settings$UsbDetailsActivity"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const/high16 v0, 0x10000000

    invoke-virtual {p2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p1, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    iget-object p0, p0, Lcom/android/settings/connecteddevice/usb/MiUsbService$1;->this$0:Lcom/android/settings/connecteddevice/usb/MiUsbService;

    const/4 p1, 0x1

    invoke-static {p0, p1}, Lcom/android/settings/connecteddevice/usb/MiUsbService;->-$$Nest$fputmShown(Lcom/android/settings/connecteddevice/usb/MiUsbService;Z)V

    :cond_0
    return-void
.end method
