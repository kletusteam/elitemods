.class Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity$2;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;->initDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;


# direct methods
.method constructor <init>(Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity$2;->this$0:Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity$2;->this$0:Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;

    invoke-static {v0}, Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;->-$$Nest$fgetmCurrentModesList(Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p2, v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity$2;->this$0:Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;

    invoke-static {v0}, Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;->-$$Nest$fgetmCurrentModesList(Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x80

    cmp-long v0, v0, v2

    const/4 v1, 0x1

    if-nez v0, :cond_0

    iget-object p2, p0, Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity$2;->this$0:Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;

    invoke-static {p2}, Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;->-$$Nest$fgetmBackend(Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;)Lcom/android/settings/connecteddevice/usb/UsbBackend;

    move-result-object p2

    invoke-virtual {p2, v1}, Lcom/android/settings/connecteddevice/usb/UsbBackend;->setPowerRole(I)V

    const-string/jumbo p2, "ro.product.device"

    const-string v0, ""

    invoke-static {p2, v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    sget-object v0, Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;->MIUI_REVERSE_CHARGE_SWAP_DR:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    iget-object p2, p0, Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity$2;->this$0:Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;

    invoke-static {p2}, Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;->-$$Nest$fgetmBackend(Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;)Lcom/android/settings/connecteddevice/usb/UsbBackend;

    move-result-object p2

    invoke-virtual {p2, v1}, Lcom/android/settings/connecteddevice/usb/UsbBackend;->setDataRole(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity$2;->this$0:Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;

    invoke-static {v0}, Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;->-$$Nest$fgetmBackend(Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;)Lcom/android/settings/connecteddevice/usb/UsbBackend;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/connecteddevice/usb/UsbBackend;->getPowerRole()I

    move-result v0

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity$2;->this$0:Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;

    invoke-static {v0}, Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;->-$$Nest$fgetmBackend(Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;)Lcom/android/settings/connecteddevice/usb/UsbBackend;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/settings/connecteddevice/usb/UsbBackend;->setPowerRole(I)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity$2;->this$0:Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;

    invoke-static {v0}, Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;->-$$Nest$fgetmBackend(Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;)Lcom/android/settings/connecteddevice/usb/UsbBackend;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity$2;->this$0:Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;

    invoke-static {v1}, Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;->-$$Nest$fgetmCurrentModesList(Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/connecteddevice/usb/UsbBackend;->setCurrentFunctions(J)V

    :cond_2
    :goto_0
    iget-object p2, p0, Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity$2;->this$0:Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;

    invoke-virtual {p2}, Landroid/app/Activity;->isDestroyed()Z

    move-result p2

    if-nez p2, :cond_3

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    :cond_3
    iget-object p0, p0, Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity$2;->this$0:Lcom/android/settings/connecteddevice/usb/UsbModeChooserActivity;

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method
