.class public Lcom/android/settings/emergency/ui/SosExitAlertActivity;
.super Lmiuix/appcompat/app/AppCompatActivity;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/emergency/ui/SosExitAlertActivity$MyTransitionListener;
    }
.end annotation


# instance fields
.field private listNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private listPhoneNunbers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mBtnHideSos:Landroid/widget/Button;

.field private mFingerVerifyDialog:Lcom/android/settings/emergency/ui/view/FullScreenDialog;

.field private mGpContactSecond:Landroid/widget/RelativeLayout;

.field private mGpContactThird:Landroid/widget/RelativeLayout;

.field private mGroupCn110Call:Landroid/widget/LinearLayout;

.field private mGroupCn119Call:Landroid/widget/LinearLayout;

.field private mGroupCn120Call:Landroid/widget/LinearLayout;

.field private mGroupCnSosCommonCall:Landroid/widget/LinearLayout;

.field private mGroupCnSosEl:Landroid/widget/RelativeLayout;

.field private mIvSosContactCallFirst:Landroid/widget/ImageView;

.field private mIvSosContactCallSecond:Landroid/widget/ImageView;

.field private mIvSosContactCallThird:Landroid/widget/ImageView;

.field private mSosDialogLayout:Landroid/widget/RelativeLayout;

.field private mTvSosContactNameFirst:Landroid/widget/TextView;

.field private mTvSosContactNameSecond:Landroid/widget/TextView;

.field private mTvSosContactNameThird:Landroid/widget/TextView;

.field private mTvSosContactPhoneFirst:Landroid/widget/TextView;

.field private mTvSosContactPhoneSecond:Landroid/widget/TextView;

.field private mTvSosContactPhoneThird:Landroid/widget/TextView;

.field private mView:Landroid/view/View;


# direct methods
.method static bridge synthetic -$$Nest$fgetlistPhoneNunbers(Lcom/android/settings/emergency/ui/SosExitAlertActivity;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->listPhoneNunbers:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFingerVerifyDialog(Lcom/android/settings/emergency/ui/SosExitAlertActivity;)Lcom/android/settings/emergency/ui/view/FullScreenDialog;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mFingerVerifyDialog:Lcom/android/settings/emergency/ui/view/FullScreenDialog;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmView(Lcom/android/settings/emergency/ui/SosExitAlertActivity;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mView:Landroid/view/View;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mcall(Lcom/android/settings/emergency/ui/SosExitAlertActivity;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->call(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiuix/appcompat/app/AppCompatActivity;-><init>()V

    return-void
.end method

.method private call(Ljava/lang/String;)V
    .locals 4

    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CALL_PRIVILEGED"

    const-string/jumbo v2, "tel"

    const/4 v3, 0x0

    invoke-static {v2, p1, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/4 p1, 0x1

    invoke-static {v0, p1}, Lmiui/telephony/SubscriptionManager;->putSlotIdExtra(Landroid/content/Intent;I)V

    const-string v1, "com.android.phone.extra.slot"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string p1, "com.android.server.telecom"

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 p1, 0x14000000

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "SOS-ExitAlertActivity"

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method private static encryptPhone(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const-string v0, " "

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x6

    if-gt v0, v1, :cond_0

    return-object p0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p0

    div-int/lit8 p0, p0, 0x2

    add-int/lit8 p0, p0, 0x2

    const-string v2, " **** "

    invoke-virtual {v0, v1, p0, v2}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private initDatas()V
    .locals 4

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    invoke-static {p0}, Lcom/android/settings/emergency/util/Config;->getSosEmergencyContacts(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/android/settings/emergency/util/Config;->getSosEmergencyContactNames(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    const-string v3, ";"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->listPhoneNunbers:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->listNames:Ljava/util/List;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->listNames:Ljava/util/List;

    :cond_0
    return-void
.end method

.method private initListeners()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mBtnHideSos:Landroid/widget/Button;

    new-instance v1, Lcom/android/settings/emergency/ui/SosExitAlertActivity$1;

    invoke-direct {v1, p0}, Lcom/android/settings/emergency/ui/SosExitAlertActivity$1;-><init>(Lcom/android/settings/emergency/ui/SosExitAlertActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mSosDialogLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/android/settings/emergency/ui/SosExitAlertActivity$2;

    invoke-direct {v1, p0}, Lcom/android/settings/emergency/ui/SosExitAlertActivity$2;-><init>(Lcom/android/settings/emergency/ui/SosExitAlertActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mTvSosContactNameFirst:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->listNames:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mTvSosContactPhoneFirst:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->listPhoneNunbers:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->encryptPhone(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mIvSosContactCallFirst:Landroid/widget/ImageView;

    new-instance v1, Lcom/android/settings/emergency/ui/SosExitAlertActivity$3;

    invoke-direct {v1, p0}, Lcom/android/settings/emergency/ui/SosExitAlertActivity$3;-><init>(Lcom/android/settings/emergency/ui/SosExitAlertActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->listPhoneNunbers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x8

    const/4 v2, 0x1

    if-le v0, v2, :cond_1

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->listNames:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v2, :cond_0

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mTvSosContactNameSecond:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->listNames:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mTvSosContactPhoneSecond:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->listPhoneNunbers:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->encryptPhone(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mIvSosContactCallSecond:Landroid/widget/ImageView;

    new-instance v2, Lcom/android/settings/emergency/ui/SosExitAlertActivity$4;

    invoke-direct {v2, p0}, Lcom/android/settings/emergency/ui/SosExitAlertActivity$4;-><init>(Lcom/android/settings/emergency/ui/SosExitAlertActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mGpContactSecond:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->listPhoneNunbers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x2

    if-le v0, v2, :cond_3

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->listNames:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v2, :cond_2

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mTvSosContactNameThird:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->listNames:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mTvSosContactPhoneThird:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->listPhoneNunbers:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->encryptPhone(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mIvSosContactCallThird:Landroid/widget/ImageView;

    new-instance v1, Lcom/android/settings/emergency/ui/SosExitAlertActivity$5;

    invoke-direct {v1, p0}, Lcom/android/settings/emergency/ui/SosExitAlertActivity$5;-><init>(Lcom/android/settings/emergency/ui/SosExitAlertActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mGpContactThird:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    :goto_1
    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mGroupCn110Call:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/android/settings/emergency/ui/SosExitAlertActivity$6;

    invoke-direct {v1, p0}, Lcom/android/settings/emergency/ui/SosExitAlertActivity$6;-><init>(Lcom/android/settings/emergency/ui/SosExitAlertActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mGroupCn120Call:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/android/settings/emergency/ui/SosExitAlertActivity$7;

    invoke-direct {v1, p0}, Lcom/android/settings/emergency/ui/SosExitAlertActivity$7;-><init>(Lcom/android/settings/emergency/ui/SosExitAlertActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mGroupCn119Call:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/android/settings/emergency/ui/SosExitAlertActivity$8;

    invoke-direct {v1, p0}, Lcom/android/settings/emergency/ui/SosExitAlertActivity$8;-><init>(Lcom/android/settings/emergency/ui/SosExitAlertActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mGroupCnSosEl:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/android/settings/emergency/ui/SosExitAlertActivity$9;

    invoke-direct {v1, p0}, Lcom/android/settings/emergency/ui/SosExitAlertActivity$9;-><init>(Lcom/android/settings/emergency/ui/SosExitAlertActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private initViews()V
    .locals 7

    new-instance v0, Lcom/android/settings/emergency/ui/view/FullScreenDialog;

    sget v1, Lcom/android/settings/R$style;->Fod_Dialog_Fullscreen:I

    invoke-direct {v0, p0, v1}, Lcom/android/settings/emergency/ui/view/FullScreenDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mFingerVerifyDialog:Lcom/android/settings/emergency/ui/view/FullScreenDialog;

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/android/settings/R$layout;->sos_dialog_view:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mFingerVerifyDialog:Lcom/android/settings/emergency/ui/view/FullScreenDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mFingerVerifyDialog:Lcom/android/settings/emergency/ui/view/FullScreenDialog;

    iget-object v1, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/AppCompatDialog;->setContentView(Landroid/view/View;)V

    new-instance v0, Lmiuix/animation/controller/AnimState;

    const-string v1, "from"

    invoke-direct {v0, v1}, Lmiuix/animation/controller/AnimState;-><init>(Ljava/lang/Object;)V

    sget-object v1, Lmiuix/animation/property/ViewProperty;->SCALE_X:Lmiuix/animation/property/ViewProperty;

    const-wide v2, 0x3fee666660000000L    # 0.949999988079071

    invoke-virtual {v0, v1, v2, v3}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;

    move-result-object v0

    sget-object v4, Lmiuix/animation/property/ViewProperty;->SCALE_Y:Lmiuix/animation/property/ViewProperty;

    invoke-virtual {v0, v4, v2, v3}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;

    move-result-object v0

    sget-object v2, Lmiuix/animation/property/ViewProperty;->ALPHA:Lmiuix/animation/property/ViewProperty;

    const-wide/16 v5, 0x0

    invoke-virtual {v0, v2, v5, v6}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;

    move-result-object v0

    new-instance v3, Lmiuix/animation/controller/AnimState;

    const-string/jumbo v5, "to"

    invoke-direct {v3, v5}, Lmiuix/animation/controller/AnimState;-><init>(Ljava/lang/Object;)V

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v3, v1, v5, v6}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;

    move-result-object v1

    invoke-virtual {v1, v4, v5, v6}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;

    move-result-object v1

    invoke-virtual {v1, v2, v5, v6}, Lmiuix/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiuix/animation/controller/AnimState;

    move-result-object v1

    new-instance v2, Lmiuix/animation/base/AnimConfig;

    invoke-direct {v2}, Lmiuix/animation/base/AnimConfig;-><init>()V

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    const/4 v4, -0x2

    invoke-virtual {v2, v4, v3}, Lmiuix/animation/base/AnimConfig;->setEase(I[F)Lmiuix/animation/base/AnimConfig;

    move-result-object v2

    const-wide/16 v3, 0x1f4

    invoke-virtual {v2, v3, v4}, Lmiuix/animation/base/AnimConfig;->setMinDuration(J)Lmiuix/animation/base/AnimConfig;

    move-result-object v2

    const/4 v3, 0x1

    new-array v4, v3, [Landroid/view/View;

    iget-object v5, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mView:Landroid/view/View;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-static {v4}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v4

    invoke-interface {v4}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v4

    invoke-interface {v4, v0}, Lmiuix/animation/IStateStyle;->setTo(Ljava/lang/Object;)Lmiuix/animation/IStateStyle;

    move-result-object v0

    new-array v3, v3, [Lmiuix/animation/base/AnimConfig;

    aput-object v2, v3, v6

    invoke-interface {v0, v1, v3}, Lmiuix/animation/IStateStyle;->to(Ljava/lang/Object;[Lmiuix/animation/base/AnimConfig;)Lmiuix/animation/IStateStyle;

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->sos_dialog_layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mSosDialogLayout:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->group_cn_sos_el:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mGroupCnSosEl:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->group_cn_sos_common_call:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mGroupCnSosCommonCall:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->iv_sos_contact_call_first:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mIvSosContactCallFirst:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->iv_sos_contact_call_second:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mIvSosContactCallSecond:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->iv_sos_contact_call_third:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mIvSosContactCallThird:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->tv_sos_contact_name_first:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mTvSosContactNameFirst:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->tv_sos_contact_name_second:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mTvSosContactNameSecond:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->tv_sos_contact_name_third:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mTvSosContactNameThird:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->tv_sos_contact_phone_first:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mTvSosContactPhoneFirst:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->tv_sos_contact_phone_second:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mTvSosContactPhoneSecond:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->tv_sos_contact_phone_third:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mTvSosContactPhoneThird:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->group_cn_119_call:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mGroupCn119Call:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->group_cn_120_call:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mGroupCn120Call:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->group_cn_110_call:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mGroupCn110Call:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->gp_contact_second:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mGpContactSecond:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->gp_contact_third:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mGpContactThird:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->btn_hide_sos:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mBtnHideSos:Landroid/widget/Button;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "zh"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mBtnHideSos:Landroid/widget/Button;

    const/high16 v1, 0x41500000    # 13.0f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextSize(F)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mBtnHideSos:Landroid/widget/Button;

    const/high16 v1, 0x41400000    # 12.0f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextSize(F)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->sos_dec:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->sos_json_anim:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/airbnb/lottie/LottieAnimationView;

    sget v2, Lcom/android/settings/R$raw;->sos:I

    invoke-virtual {v1, v2}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(I)V

    const-string v2, "images"

    invoke-virtual {v1, v2}, Lcom/airbnb/lottie/LottieAnimationView;->setImageAssetsFolder(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v3, Lcom/android/settings/emergency/ui/SosExitAlertActivity$10;

    invoke-direct {v3, p0, v0, v1}, Lcom/android/settings/emergency/ui/SosExitAlertActivity$10;-><init>(Lcom/android/settings/emergency/ui/SosExitAlertActivity;Landroid/widget/TextView;Lcom/airbnb/lottie/LottieAnimationView;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void

    :array_0
    .array-data 4
        0x3f666666    # 0.9f
        0x3e99999a    # 0.3f
    .end array-data
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->initDatas()V

    invoke-direct {p0}, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->initViews()V

    invoke-direct {p0}, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->initListeners()V

    sget-boolean p1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mGroupCnSosEl:Landroid/widget/RelativeLayout;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object p0, p0, Lcom/android/settings/emergency/ui/SosExitAlertActivity;->mGroupCnSosCommonCall:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_0
    return-void
.end method
