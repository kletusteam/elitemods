.class Lcom/android/settings/KeyShortcutSettingsFragment$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/KeyShortcutSettingsFragment;->bringUpActionChooseDlg(Ljava/lang/String;Ljava/lang/String;Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/KeyShortcutSettingsFragment;

.field final synthetic val$action:Ljava/lang/String;

.field final synthetic val$function:Ljava/lang/String;

.field final synthetic val$preference:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;


# direct methods
.method constructor <init>(Lcom/android/settings/KeyShortcutSettingsFragment;Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/KeyShortcutSettingsFragment$1;->this$0:Lcom/android/settings/KeyShortcutSettingsFragment;

    iput-object p2, p0, Lcom/android/settings/KeyShortcutSettingsFragment$1;->val$preference:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    iput-object p3, p0, Lcom/android/settings/KeyShortcutSettingsFragment$1;->val$function:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/settings/KeyShortcutSettingsFragment$1;->val$action:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    const-string/jumbo p1, "none"

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    iget-object p2, p0, Lcom/android/settings/KeyShortcutSettingsFragment$1;->this$0:Lcom/android/settings/KeyShortcutSettingsFragment;

    invoke-static {p2}, Lcom/android/settings/KeyShortcutSettingsFragment;->-$$Nest$fgetmContext(Lcom/android/settings/KeyShortcutSettingsFragment;)Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p2

    iget-object v0, p0, Lcom/android/settings/KeyShortcutSettingsFragment$1;->val$preference:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    invoke-virtual {v0}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/KeyShortcutSettingsFragment$1;->val$function:Ljava/lang/String;

    const/4 v2, -0x2

    invoke-static {p2, v0, v1, v2}, Landroid/provider/MiuiSettings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    iget-object p2, p0, Lcom/android/settings/KeyShortcutSettingsFragment$1;->val$preference:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    iget-object v0, p0, Lcom/android/settings/KeyShortcutSettingsFragment$1;->val$function:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lmiuix/preference/DropDownPreference;->setValue(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/android/settings/KeyShortcutSettingsFragment$1;->this$0:Lcom/android/settings/KeyShortcutSettingsFragment;

    invoke-static {p2}, Lcom/android/settings/KeyShortcutSettingsFragment;->-$$Nest$fgetmContext(Lcom/android/settings/KeyShortcutSettingsFragment;)Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p2

    iget-object v0, p0, Lcom/android/settings/KeyShortcutSettingsFragment$1;->val$action:Ljava/lang/String;

    invoke-static {p2, v0, p1, v2}, Landroid/provider/MiuiSettings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    iget-object p2, p0, Lcom/android/settings/KeyShortcutSettingsFragment$1;->this$0:Lcom/android/settings/KeyShortcutSettingsFragment;

    invoke-static {p2}, Lcom/android/settings/KeyShortcutSettingsFragment;->-$$Nest$fgetmShortcutPreferenceMap(Lcom/android/settings/KeyShortcutSettingsFragment;)Ljava/util/Map;

    move-result-object p2

    iget-object v0, p0, Lcom/android/settings/KeyShortcutSettingsFragment$1;->val$action:Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    invoke-virtual {p2, p1}, Lmiuix/preference/DropDownPreference;->setValue(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    iget-object p2, p0, Lcom/android/settings/KeyShortcutSettingsFragment$1;->this$0:Lcom/android/settings/KeyShortcutSettingsFragment;

    invoke-static {p2}, Lcom/android/settings/KeyShortcutSettingsFragment;->-$$Nest$fgetmContext(Lcom/android/settings/KeyShortcutSettingsFragment;)Landroid/content/Context;

    move-result-object p2

    iget-object v0, p0, Lcom/android/settings/KeyShortcutSettingsFragment$1;->val$preference:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    invoke-virtual {v0}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/android/settings/MiuiShortcut$Key;->getKeyAndGestureShortcutSetFunction(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/android/settings/KeyShortcutSettingsFragment$1;->val$preference:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    if-nez p2, :cond_1

    goto :goto_0

    :cond_1
    move-object p1, p2

    :goto_0
    invoke-virtual {v0, p1}, Lmiuix/preference/DropDownPreference;->setValue(Ljava/lang/String;)V

    :goto_1
    iget-object p1, p0, Lcom/android/settings/KeyShortcutSettingsFragment$1;->this$0:Lcom/android/settings/KeyShortcutSettingsFragment;

    invoke-static {p1}, Lcom/android/settings/KeyShortcutSettingsFragment;->-$$Nest$fgetmFunctionChangeDialog(Lcom/android/settings/KeyShortcutSettingsFragment;)Lmiuix/appcompat/app/AlertDialog;

    move-result-object p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/android/settings/KeyShortcutSettingsFragment$1;->this$0:Lcom/android/settings/KeyShortcutSettingsFragment;

    invoke-static {p1}, Lcom/android/settings/KeyShortcutSettingsFragment;->-$$Nest$fgetmFunctionChangeDialog(Lcom/android/settings/KeyShortcutSettingsFragment;)Lmiuix/appcompat/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p1}, Lmiuix/appcompat/app/AlertDialog;->dismiss()V

    iget-object p0, p0, Lcom/android/settings/KeyShortcutSettingsFragment$1;->this$0:Lcom/android/settings/KeyShortcutSettingsFragment;

    const/4 p1, 0x0

    invoke-static {p0, p1}, Lcom/android/settings/KeyShortcutSettingsFragment;->-$$Nest$fputmFunctionChangeDialog(Lcom/android/settings/KeyShortcutSettingsFragment;Lmiuix/appcompat/app/AlertDialog;)V

    :cond_2
    return-void
.end method
