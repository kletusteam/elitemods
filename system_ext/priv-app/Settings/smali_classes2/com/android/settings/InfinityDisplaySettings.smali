.class public Lcom/android/settings/InfinityDisplaySettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;


# instance fields
.field private mAppGuide:Landroidx/preference/Preference;

.field private mAppSwitchFeature:Landroidx/preference/CheckBoxPreference;

.field private mAppSwitchGuide:Landroidx/preference/Preference;

.field private mAspectRatio:Landroidx/preference/Preference;

.field private mBackGuide:Landroidx/preference/Preference;

.field private mCutoutMode:Landroidx/preference/Preference;

.field private mCutoutType:Landroidx/preference/CheckBoxPreference;

.field private mDemoExistes:Z

.field private mGuideCategory:Landroidx/preference/PreferenceCategory;

.field private mHasCheckedDemo:Z

.field private mHomeGuide:Landroidx/preference/Preference;

.field private mMistakeTouch:Landroidx/preference/CheckBoxPreference;

.field private mNotchForceBlack:Landroidx/preference/CheckBoxPreference;

.field private mRecentGuide:Landroidx/preference/Preference;

.field private mScreenButtonHide:Landroidx/preference/CheckBoxPreference;

.field private mScreenButtonHideListener:Landroid/database/ContentObserver;

.field private mSettingCategory:Landroidx/preference/PreferenceCategory;

.field private mSwitchScreenButtonOrder:Landroidx/preference/CheckBoxPreference;


# direct methods
.method static bridge synthetic -$$Nest$fgetmScreenButtonHide(Lcom/android/settings/InfinityDisplaySettings;)Landroidx/preference/CheckBoxPreference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/InfinityDisplaySettings;->mScreenButtonHide:Landroidx/preference/CheckBoxPreference;

    return-object p0
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Lcom/android/settings/InfinityDisplaySettings$2;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, v1}, Lcom/android/settings/InfinityDisplaySettings$2;-><init>(Lcom/android/settings/InfinityDisplaySettings;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mScreenButtonHideListener:Landroid/database/ContentObserver;

    return-void
.end method

.method private checkDemoExist()Z
    .locals 5

    iget-boolean v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mHasCheckedDemo:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mHasCheckedDemo:Z

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.android.systemui"

    const-string v4, "com.android.systemui.fsgesture.HomeDemoAct"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v2, "DEMO_TYPE"

    const-string v3, "DEMO_TO_HOME"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    iput-boolean v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mDemoExistes:Z

    :cond_0
    iget-boolean p0, p0, Lcom/android/settings/InfinityDisplaySettings;->mDemoExistes:Z

    return p0
.end method

.method private isAppSwitchFeatureEnable()Z
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "show_gesture_appswitch_feature"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    if-eqz p0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method private isDripType()Z
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "overlay_drip"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    if-ne p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private isForceBlack()Z
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/InfinityDisplaySettings;->supportForceBlackV2()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "force_black_v2"

    invoke-static {p0, v0}, Landroid/provider/MiuiSettings$Global;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result p0

    return p0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "force_black"

    invoke-static {p0, v0}, Landroid/provider/MiuiSettings$Global;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method private isMistakeTouchEnable()Z
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "show_mistake_touch_toast"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private isScreenButtonHidden()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "force_fsg_nav_bar"

    invoke-static {p0, v0}, Landroid/provider/MiuiSettings$Global;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method private setAppSwitchFeatureEnable(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "show_gesture_appswitch_feature"

    invoke-static {p0, v0, p1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method private setDripType(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "overlay_drip"

    invoke-static {p0, v0, p1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method private setForceBlack(Z)V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/InfinityDisplaySettings;->supportForceBlackV2()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "force_black_v2"

    invoke-static {p0, v0, p1}, Landroid/provider/MiuiSettings$Global;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "force_black"

    invoke-static {p0, v0, p1}, Landroid/provider/MiuiSettings$Global;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    :goto_0
    return-void
.end method

.method private setMistakeTouchEnable(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "show_mistake_touch_toast"

    invoke-static {p0, v0, p1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method private setupForceImmersiveHintDialog(Lcom/android/settings/SimpleDialogFragment;)V
    .locals 2

    sget v0, Lcom/android/settings/R$string;->force_immersive_compatibility_dont_hide:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/android/settings/SimpleDialogFragment;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    sget v0, Lcom/android/settings/R$string;->force_immersive_compatibility_hide:I

    new-instance v1, Lcom/android/settings/InfinityDisplaySettings$1;

    invoke-direct {v1, p0}, Lcom/android/settings/InfinityDisplaySettings$1;-><init>(Lcom/android/settings/InfinityDisplaySettings;)V

    invoke-virtual {p1, v0, v1}, Lcom/android/settings/SimpleDialogFragment;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method

.method private showForceImmersiveHintDialog()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mScreenButtonHide:Landroidx/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    new-instance v0, Lcom/android/settings/SimpleDialogFragment$AlertDialogFragmentBuilder;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/android/settings/SimpleDialogFragment$AlertDialogFragmentBuilder;-><init>(I)V

    sget v1, Lcom/android/settings/R$string;->force_immersive_compatibility_hint_title:I

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/SimpleDialogFragment$AlertDialogFragmentBuilder;->setTitle(Ljava/lang/String;)Lcom/android/settings/SimpleDialogFragment$AlertDialogFragmentBuilder;

    move-result-object v0

    sget v1, Lcom/android/settings/R$string;->force_immersive_compatibility_hint_message:I

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/SimpleDialogFragment$AlertDialogFragmentBuilder;->setMessage(Ljava/lang/String;)Lcom/android/settings/SimpleDialogFragment$AlertDialogFragmentBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/SimpleDialogFragment$AlertDialogFragmentBuilder;->create()Lcom/android/settings/SimpleDialogFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/InfinityDisplaySettings;->setupForceImmersiveHintDialog(Lcom/android/settings/SimpleDialogFragment;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p0

    const-string v1, "fragment_force_immersive_dialog"

    invoke-virtual {v0, p0, v1}, Landroidx/fragment/app/DialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private supportForceBlackV2()Z
    .locals 0

    const/4 p0, 0x1

    return p0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 0

    const-class p0, Lcom/android/settings/InfinityDisplaySettings;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method isRightHand()Z
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    const/4 v0, 0x1

    :goto_1
    goto/32 :goto_a

    nop

    :goto_2
    if-eq p0, v1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_0

    nop

    :goto_3
    const/4 v1, 0x2

    goto/32 :goto_2

    nop

    :goto_4
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    goto/32 :goto_3

    nop

    :goto_5
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    goto/32 :goto_c

    nop

    :goto_6
    if-nez p0, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_b

    nop

    :goto_7
    const/4 v0, 0x0

    goto/32 :goto_6

    nop

    :goto_8
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p0

    goto/32 :goto_d

    nop

    :goto_9
    if-gtz v1, :cond_2

    goto/32 :goto_1

    :cond_2
    goto/32 :goto_8

    nop

    :goto_a
    return v0

    :goto_b
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_9

    nop

    :goto_c
    invoke-static {p0}, Landroid/provider/MiuiSettings$System;->getScreenKeyOrder(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object p0

    goto/32 :goto_7

    nop

    :goto_d
    check-cast p0, Ljava/lang/Integer;

    goto/32 :goto_4

    nop
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const-string/jumbo p1, "window"

    invoke-static {p1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object p1

    invoke-static {p1}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object p1

    :try_start_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContextCompat;->getDisplayId(Landroid/content/Context;)I

    move-result v0

    invoke-static {p1, v0}, Landroid/view/IWindowManagerCompat;->hasNavigationBar(Landroid/view/IWindowManager;I)Z

    move-result p1

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->finish()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    const-string v0, "fragment_force_immersive_dialog"

    invoke-virtual {p1, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object p1

    check-cast p1, Lcom/android/settings/SimpleDialogFragment;

    invoke-direct {p0, p1}, Lcom/android/settings/InfinityDisplaySettings;->setupForceImmersiveHintDialog(Lcom/android/settings/SimpleDialogFragment;)V

    :cond_1
    sget p1, Lcom/android/settings/R$xml;->infinity_display_settings:I

    invoke-virtual {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    const-string/jumbo v0, "screen_button_hide"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/InfinityDisplaySettings;->mScreenButtonHide:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    const-string/jumbo v0, "switch_screen_button_order"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/InfinityDisplaySettings;->mSwitchScreenButtonOrder:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    const-string/jumbo v0, "notch_force_black"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/InfinityDisplaySettings;->mNotchForceBlack:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    const-string v0, "fsg_mistake_touch"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/InfinityDisplaySettings;->mMistakeTouch:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    const-string/jumbo v0, "navigation_appswitch_anim"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/InfinityDisplaySettings;->mAppSwitchFeature:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    const-string v0, "cutout_type"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/InfinityDisplaySettings;->mCutoutType:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    const-string v0, "cutout_mode"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/InfinityDisplaySettings;->mCutoutMode:Landroidx/preference/Preference;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    const-string/jumbo v0, "navigation_guide_home"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/InfinityDisplaySettings;->mHomeGuide:Landroidx/preference/Preference;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    const-string/jumbo v0, "navigation_guide_recent"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/InfinityDisplaySettings;->mRecentGuide:Landroidx/preference/Preference;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    const-string/jumbo v0, "navigation_guide_back"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/InfinityDisplaySettings;->mBackGuide:Landroidx/preference/Preference;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    const-string/jumbo v0, "navigation_guide_app"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/InfinityDisplaySettings;->mAppGuide:Landroidx/preference/Preference;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    const-string/jumbo v0, "screen_max_aspect_ratio"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/InfinityDisplaySettings;->mAspectRatio:Landroidx/preference/Preference;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    const-string/jumbo v0, "navigation_guide_category"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/PreferenceCategory;

    iput-object p1, p0, Lcom/android/settings/InfinityDisplaySettings;->mGuideCategory:Landroidx/preference/PreferenceCategory;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    const-string/jumbo v0, "navigation_setting_category"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/PreferenceCategory;

    iput-object p1, p0, Lcom/android/settings/InfinityDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    const-string/jumbo v0, "navigation_guide_appswitch"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/InfinityDisplaySettings;->mAppSwitchGuide:Landroidx/preference/Preference;

    invoke-direct {p0}, Lcom/android/settings/InfinityDisplaySettings;->checkDemoExist()Z

    iget-boolean p1, p0, Lcom/android/settings/InfinityDisplaySettings;->mDemoExistes:Z

    if-nez p1, :cond_2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object p0, p0, Lcom/android/settings/InfinityDisplaySettings;->mGuideCategory:Landroidx/preference/PreferenceCategory;

    invoke-virtual {p1, p0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_2
    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onPause()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    sget v1, Lcom/android/settings/R$id;->navigation_guide:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/view/NavigationBarGuideView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/view/NavigationBarGuideView;->onPause()V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object p0, p0, Lcom/android/settings/InfinityDisplaySettings;->mScreenButtonHideListener:Landroid/database/ContentObserver;

    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method public onPreferenceTreeClick(Landroidx/preference/PreferenceScreen;Landroidx/preference/Preference;)Z
    .locals 6

    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mScreenButtonHide:Landroidx/preference/CheckBoxPreference;

    const/4 v1, 0x0

    if-ne p2, v0, :cond_1

    invoke-virtual {v0}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/InfinityDisplaySettings;->showForceImmersiveHintDialog()V

    goto/16 :goto_0

    :cond_0
    invoke-virtual {p0, v1}, Lcom/android/settings/InfinityDisplaySettings;->setScreenButtonHidden(Z)V

    goto/16 :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mSwitchScreenButtonOrder:Landroidx/preference/CheckBoxPreference;

    if-ne p2, v0, :cond_2

    invoke-virtual {v0}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/InfinityDisplaySettings;->setRightHand(Z)V

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mNotchForceBlack:Landroidx/preference/CheckBoxPreference;

    const/4 v2, 0x1

    if-ne p2, v0, :cond_4

    invoke-virtual {v0}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/InfinityDisplaySettings;->setForceBlack(Z)V

    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mCutoutMode:Landroidx/preference/Preference;

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/android/settings/InfinityDisplaySettings;->isForceBlack()Z

    move-result v1

    xor-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setEnabled(Z)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mCutoutType:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_c

    invoke-direct {p0}, Lcom/android/settings/InfinityDisplaySettings;->isForceBlack()Z

    move-result v1

    xor-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mCutoutType:Landroidx/preference/CheckBoxPreference;

    if-ne p2, v0, :cond_5

    invoke-virtual {v0}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/InfinityDisplaySettings;->setDripType(Z)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mHomeGuide:Landroidx/preference/Preference;

    const-string v3, "com.android.systemui.fsgesture.HomeDemoAct"

    const-string v4, "DEMO_TYPE"

    const-string v5, "com.android.systemui"

    if-ne p2, v0, :cond_6

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "home"

    invoke-static {v0, v1}, Lcom/android/settings/utils/AnalyticsUtils;->trackClickSingleTurorialEvent(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    new-instance v1, Landroid/content/ComponentName;

    invoke-direct {v1, v5, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v1, "DEMO_TO_HOME"

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mRecentGuide:Landroidx/preference/Preference;

    if-ne p2, v0, :cond_7

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "recents"

    invoke-static {v0, v1}, Lcom/android/settings/utils/AnalyticsUtils;->trackClickSingleTurorialEvent(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    new-instance v1, Landroid/content/ComponentName;

    invoke-direct {v1, v5, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v1, "DEMO_TO_RECENTTASK"

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mBackGuide:Landroidx/preference/Preference;

    if-ne p2, v0, :cond_8

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "back"

    invoke-static {v0, v1}, Lcom/android/settings/utils/AnalyticsUtils;->trackClickSingleTurorialEvent(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    new-instance v1, Landroid/content/ComponentName;

    const-string v3, "com.android.systemui.fsgesture.FsGestureBackDemoActivity"

    invoke-direct {v1, v5, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v1, "FSG_BACK_GESTURE"

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "DEMO_STEP"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_8
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mAppGuide:Landroidx/preference/Preference;

    if-ne p2, v0, :cond_9

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "in_app_function"

    invoke-static {v0, v1}, Lcom/android/settings/utils/AnalyticsUtils;->trackClickSingleTurorialEvent(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.android.systemui.fsgesture.DrawerDemoAct"

    invoke-direct {v1, v5, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_9
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mMistakeTouch:Landroidx/preference/CheckBoxPreference;

    if-ne p2, v0, :cond_a

    invoke-virtual {v0}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/InfinityDisplaySettings;->setMistakeTouchEnable(Z)V

    goto :goto_0

    :cond_a
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mAppSwitchFeature:Landroidx/preference/CheckBoxPreference;

    if-ne p2, v0, :cond_b

    invoke-virtual {v0}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/InfinityDisplaySettings;->setAppSwitchFeatureEnable(Z)V

    goto :goto_0

    :cond_b
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mAppSwitchGuide:Landroidx/preference/Preference;

    if-ne p2, v0, :cond_c

    invoke-direct {p0}, Lcom/android/settings/InfinityDisplaySettings;->isAppSwitchFeatureEnable()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/android/settings/R$string;->navigation_guide_appswitch_click:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_c
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/android/settings/SettingsPreferenceFragment;->onPreferenceTreeClick(Landroidx/preference/PreferenceScreen;Landroidx/preference/Preference;)Z

    move-result p0

    return p0
.end method

.method public onResume()V
    .locals 5

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onResume()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mScreenButtonHide:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/InfinityDisplaySettings;->isScreenButtonHidden()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mSwitchScreenButtonOrder:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/InfinityDisplaySettings;->isRightHand()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mNotchForceBlack:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/android/settings/InfinityDisplaySettings;->isForceBlack()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mCutoutMode:Landroidx/preference/Preference;

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/android/settings/InfinityDisplaySettings;->isForceBlack()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setEnabled(Z)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mCutoutType:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/android/settings/InfinityDisplaySettings;->isForceBlack()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setEnabled(Z)V

    :cond_4
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mCutoutType:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lcom/android/settings/InfinityDisplaySettings;->isDripType()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :cond_5
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mMistakeTouch:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcom/android/settings/InfinityDisplaySettings;->isMistakeTouchEnable()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :cond_6
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mAppSwitchFeature:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_7

    invoke-direct {p0}, Lcom/android/settings/InfinityDisplaySettings;->isAppSwitchFeatureEnable()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :cond_7
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_8

    sget v1, Lcom/android/settings/R$id;->navigation_guide:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/view/NavigationBarGuideView;

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lcom/android/settings/view/NavigationBarGuideView;->onResume()V

    :cond_8
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "force_fsg_nav_bar"

    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/settings/InfinityDisplaySettings;->mScreenButtonHideListener:Landroid/database/ContentObserver;

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    invoke-virtual {p0}, Lcom/android/settings/InfinityDisplaySettings;->updatePrefence()V

    return-void
.end method

.method setRightHand(Z)V
    .locals 4

    goto/32 :goto_11

    nop

    :goto_0
    const/4 v2, 0x1

    goto/32 :goto_10

    nop

    :goto_1
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_12

    nop

    :goto_2
    return-void

    :goto_3
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_c

    nop

    :goto_4
    const/4 v3, 0x3

    goto/32 :goto_f

    nop

    :goto_5
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_6
    goto :goto_9

    :goto_7
    goto/32 :goto_1

    nop

    :goto_8
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_9
    goto/32 :goto_e

    nop

    :goto_a
    if-nez p1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_d

    nop

    :goto_b
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_13

    nop

    :goto_c
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_6

    nop

    :goto_d
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_3

    nop

    :goto_e
    invoke-virtual {p0, v0}, Lcom/android/settings/InfinityDisplaySettings;->setScreenKeyOrder(Ljava/util/List;)V

    goto/32 :goto_2

    nop

    :goto_f
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/32 :goto_a

    nop

    :goto_10
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/32 :goto_4

    nop

    :goto_11
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_b

    nop

    :goto_12
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_8

    nop

    :goto_13
    const/4 v1, 0x2

    goto/32 :goto_5

    nop
.end method

.method setScreenButtonHidden(Z)V
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    const-string v1, "force_fsg_nav_bar"

    goto/32 :goto_7

    nop

    :goto_1
    const-string v0, "immersive.preconfirms=*"

    goto/32 :goto_2

    nop

    :goto_2
    invoke-static {p0, p1, v0}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto/32 :goto_6

    nop

    :goto_3
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    goto/32 :goto_4

    nop

    :goto_4
    const-string/jumbo p1, "policy_control"

    goto/32 :goto_1

    nop

    :goto_5
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_6
    return-void

    :goto_7
    invoke-static {v0, v1, p1}, Landroid/provider/MiuiSettings$Global;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    goto/32 :goto_3

    nop
.end method

.method public setScreenKeyOrder(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0xc

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string/jumbo v0, "screen_key_order"

    invoke-static {p0, v0, p1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method updatePrefence()V
    .locals 3

    goto/32 :goto_25

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v1, p0, Lcom/android/settings/InfinityDisplaySettings;->mAppSwitchFeature:Landroidx/preference/CheckBoxPreference;

    goto/32 :goto_1e

    nop

    :goto_2
    sget-boolean v0, Lmiui/util/CustomizeUtil;->HAS_NOTCH:Z

    goto/32 :goto_8

    nop

    :goto_3
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    goto/32 :goto_1d

    nop

    :goto_4
    if-eqz v0, :cond_0

    goto/32 :goto_24

    :cond_0
    goto/32 :goto_38

    nop

    :goto_5
    iget-object v1, p0, Lcom/android/settings/InfinityDisplaySettings;->mMistakeTouch:Landroidx/preference/CheckBoxPreference;

    goto/32 :goto_30

    nop

    :goto_6
    invoke-virtual {v0, p0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :goto_7
    goto/32 :goto_0

    nop

    :goto_8
    if-eqz v0, :cond_1

    goto/32 :goto_18

    :cond_1
    goto/32 :goto_20

    nop

    :goto_9
    invoke-static {}, Lcom/android/settings/utils/Utils;->supportCutoutMode()Z

    move-result v0

    goto/32 :goto_4

    nop

    :goto_a
    iget-object v1, p0, Lcom/android/settings/InfinityDisplaySettings;->mSwitchScreenButtonOrder:Landroidx/preference/CheckBoxPreference;

    goto/32 :goto_34

    nop

    :goto_b
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    goto/32 :goto_14

    nop

    :goto_c
    if-nez v0, :cond_2

    goto/32 :goto_24

    :cond_2
    goto/32 :goto_12

    nop

    :goto_d
    if-nez v1, :cond_3

    goto/32 :goto_f

    :cond_3
    goto/32 :goto_13

    nop

    :goto_e
    invoke-virtual {v1, v2}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :goto_f
    goto/32 :goto_39

    nop

    :goto_10
    if-eqz v0, :cond_4

    goto/32 :goto_7

    :cond_4
    goto/32 :goto_1c

    nop

    :goto_11
    iget-object v1, p0, Lcom/android/settings/InfinityDisplaySettings;->mAppSwitchFeature:Landroidx/preference/CheckBoxPreference;

    goto/32 :goto_37

    nop

    :goto_12
    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    goto/32 :goto_35

    nop

    :goto_13
    iget-object v1, p0, Lcom/android/settings/InfinityDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    goto/32 :goto_1f

    nop

    :goto_14
    iget-object v1, p0, Lcom/android/settings/InfinityDisplaySettings;->mSwitchScreenButtonOrder:Landroidx/preference/CheckBoxPreference;

    goto/32 :goto_28

    nop

    :goto_15
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    goto/32 :goto_a

    nop

    :goto_16
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    goto/32 :goto_1

    nop

    :goto_17
    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :goto_18
    goto/32 :goto_9

    nop

    :goto_19
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    goto/32 :goto_11

    nop

    :goto_1a
    sget-boolean v1, Lmiui/os/Build;->IS_TABLET:Z

    goto/32 :goto_d

    nop

    :goto_1b
    invoke-static {}, Lcom/android/settings/utils/Utils;->supportOverlayRoundedCorner()Z

    move-result v0

    goto/32 :goto_10

    nop

    :goto_1c
    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    goto/32 :goto_27

    nop

    :goto_1d
    iget-object v1, p0, Lcom/android/settings/InfinityDisplaySettings;->mMistakeTouch:Landroidx/preference/CheckBoxPreference;

    goto/32 :goto_26

    nop

    :goto_1e
    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    goto/32 :goto_15

    nop

    :goto_1f
    iget-object v2, p0, Lcom/android/settings/InfinityDisplaySettings;->mAspectRatio:Landroidx/preference/Preference;

    goto/32 :goto_e

    nop

    :goto_20
    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    goto/32 :goto_3a

    nop

    :goto_21
    iget-boolean v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mDemoExistes:Z

    goto/32 :goto_2c

    nop

    :goto_22
    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    goto/32 :goto_32

    nop

    :goto_23
    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :goto_24
    goto/32 :goto_1b

    nop

    :goto_25
    invoke-direct {p0}, Lcom/android/settings/InfinityDisplaySettings;->isScreenButtonHidden()Z

    move-result v0

    goto/32 :goto_1a

    nop

    :goto_26
    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    goto/32 :goto_16

    nop

    :goto_27
    iget-object p0, p0, Lcom/android/settings/InfinityDisplaySettings;->mCutoutType:Landroidx/preference/CheckBoxPreference;

    goto/32 :goto_6

    nop

    :goto_28
    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    goto/32 :goto_19

    nop

    :goto_29
    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    goto/32 :goto_b

    nop

    :goto_2a
    goto :goto_31

    :goto_2b
    goto/32 :goto_33

    nop

    :goto_2c
    if-nez v0, :cond_5

    goto/32 :goto_2f

    :cond_5
    goto/32 :goto_22

    nop

    :goto_2d
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    goto/32 :goto_5

    nop

    :goto_2e
    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    :goto_2f
    goto/32 :goto_3

    nop

    :goto_30
    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :goto_31
    goto/32 :goto_2

    nop

    :goto_32
    iget-object v1, p0, Lcom/android/settings/InfinityDisplaySettings;->mGuideCategory:Landroidx/preference/PreferenceCategory;

    goto/32 :goto_2e

    nop

    :goto_33
    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    goto/32 :goto_36

    nop

    :goto_34
    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    goto/32 :goto_2a

    nop

    :goto_35
    iget-object v1, p0, Lcom/android/settings/InfinityDisplaySettings;->mCutoutMode:Landroidx/preference/Preference;

    goto/32 :goto_23

    nop

    :goto_36
    iget-object v1, p0, Lcom/android/settings/InfinityDisplaySettings;->mGuideCategory:Landroidx/preference/PreferenceCategory;

    goto/32 :goto_29

    nop

    :goto_37
    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    goto/32 :goto_2d

    nop

    :goto_38
    iget-object v0, p0, Lcom/android/settings/InfinityDisplaySettings;->mCutoutMode:Landroidx/preference/Preference;

    goto/32 :goto_c

    nop

    :goto_39
    if-nez v0, :cond_6

    goto/32 :goto_2b

    :cond_6
    goto/32 :goto_21

    nop

    :goto_3a
    iget-object v1, p0, Lcom/android/settings/InfinityDisplaySettings;->mNotchForceBlack:Landroidx/preference/CheckBoxPreference;

    goto/32 :goto_17

    nop
.end method
