.class public Lcom/android/settings/MiuiSoundSettings;
.super Lcom/android/settings/MiuiSoundSettingsBase;

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/MiuiSoundSettings$SoundUIHandler;,
        Lcom/android/settings/MiuiSoundSettings$UpdateInfoCallback;,
        Lcom/android/settings/MiuiSoundSettings$RemoteServiceConn;,
        Lcom/android/settings/MiuiSoundSettings$SoundSettingsObserver;
    }
.end annotation


# static fields
.field private static final mRestrictedKeyList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAlarmRingtonePreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mCalendarSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

.field private final mContentHandler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mControllers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/utils/MiuiBaseController;",
            ">;"
        }
    .end annotation
.end field

.field private mDeviceInfoCallback:Lcom/android/settings/MiuiSoundSettings$UpdateInfoCallback;

.field private mHandler:Landroid/os/Handler;

.field private mHelper:Lcom/android/settings/device/DeviceParamsInitHelper;

.field private mMiuiAlarmRingtoneController:Lcom/android/settings/sound/MiuiAlarmRingtonePreferenceController;

.field private mMuteMediaSoundPref:Landroidx/preference/CheckBoxPreference;

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mNotificationSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

.field private final mObserver:Lcom/android/settings/MiuiSoundSettings$SoundSettingsObserver;

.field private mRemoteService:Lcom/android/settings/aidl/IRemoteGetDeviceInfoService;

.field private mRemoteServiceConn:Lcom/android/settings/MiuiSoundSettings$RemoteServiceConn;

.field private mRequestPreference:Lcom/android/settings/sound/MiuiWorkRingtonePreference;

.field private final mRingerModeReceiver:Landroid/content/BroadcastReceiver;

.field private mRingerModeSettingPref:Landroidx/preference/CheckBoxPreference;

.field private mRingtonePreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

.field private mSilentModePref:Landroidx/preference/CheckBoxPreference;

.field private mSmsDeliveredSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

.field private mSmsReceivedSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

.field private mSoundModeCategory:Landroidx/preference/PreferenceCategory;

.field private mSoundSpeakerCategory:Landroidx/preference/PreferenceCategory;

.field private mSoundSpeakerPreference:Lcom/android/settings/soundsettings/SoundSpeakerDescPreference;

.field protected mUserId:I

.field private final mVibrateSettingsObserver:Landroid/database/ContentObserver;

.field private mVibrateWhenRingingPref:Landroidx/preference/CheckBoxPreference;

.field private mVibrateWhenSilentPref:Landroidx/preference/CheckBoxPreference;

.field private mVolumeDownPressed:Z

.field private mVolumePrefs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/sound/VolumeSeekBarPreference;",
            ">;"
        }
    .end annotation
.end field

.field private mZenModePef:Lcom/android/settings/dndmode/LabelPreference;

.field private workSoundController:Lcom/android/settings/sound/MiuiWorkSoundPreferenceController;


# direct methods
.method public static synthetic $r8$lambda$gQDhRHqpzSsDc6Vd5mAfAE10q98(Lcom/android/settings/MiuiSoundSettings;Landroidx/preference/Preference;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiSoundSettings;->lambda$initRingToYouPreference$0(Landroidx/preference/Preference;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$xJZr0vjSvtA1RxGMQETs5iSkZz8(Lcom/android/settings/MiuiSoundSettings;Landroidx/preference/CheckBoxPreference;Landroidx/preference/Preference;Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settings/MiuiSoundSettings;->lambda$initRingToYouPreference$1(Landroidx/preference/CheckBoxPreference;Landroidx/preference/Preference;Landroidx/preference/Preference;Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmDeviceInfoCallback(Lcom/android/settings/MiuiSoundSettings;)Lcom/android/settings/MiuiSoundSettings$UpdateInfoCallback;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettings;->mDeviceInfoCallback:Lcom/android/settings/MiuiSoundSettings$UpdateInfoCallback;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/settings/MiuiSoundSettings;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettings;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHelper(Lcom/android/settings/MiuiSoundSettings;)Lcom/android/settings/device/DeviceParamsInitHelper;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettings;->mHelper:Lcom/android/settings/device/DeviceParamsInitHelper;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmRemoteService(Lcom/android/settings/MiuiSoundSettings;)Lcom/android/settings/aidl/IRemoteGetDeviceInfoService;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettings;->mRemoteService:Lcom/android/settings/aidl/IRemoteGetDeviceInfoService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmHelper(Lcom/android/settings/MiuiSoundSettings;Lcom/android/settings/device/DeviceParamsInitHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mHelper:Lcom/android/settings/device/DeviceParamsInitHelper;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmRemoteService(Lcom/android/settings/MiuiSoundSettings;Lcom/android/settings/aidl/IRemoteGetDeviceInfoService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mRemoteService:Lcom/android/settings/aidl/IRemoteGetDeviceInfoService;

    return-void
.end method

.method static bridge synthetic -$$Nest$minitSoundParams(Lcom/android/settings/MiuiSoundSettings;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiSoundSettings;->initSoundParams(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mrefreshZenModeSetting(Lcom/android/settings/MiuiSoundSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiSoundSettings;->refreshZenModeSetting()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msendRefreshMsg(Lcom/android/settings/MiuiSoundSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiSoundSettings;->sendRefreshMsg()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/settings/MiuiSoundSettings;->mRestrictedKeyList:Ljava/util/List;

    const-string/jumbo v1, "ring_volume"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string/jumbo v1, "media_volume"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "alarm_volume"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/MiuiSoundSettingsBase;-><init>()V

    new-instance v0, Lcom/android/settings/MiuiSoundSettings$SoundUIHandler;

    invoke-direct {v0, p0}, Lcom/android/settings/MiuiSoundSettings$SoundUIHandler;-><init>(Lcom/android/settings/MiuiSoundSettings;)V

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mHandler:Landroid/os/Handler;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mControllers:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mVolumePrefs:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/MiuiSoundSettings;->mVolumeDownPressed:Z

    new-instance v0, Lcom/android/settings/MiuiSoundSettings$SoundSettingsObserver;

    invoke-direct {v0, p0}, Lcom/android/settings/MiuiSoundSettings$SoundSettingsObserver;-><init>(Lcom/android/settings/MiuiSoundSettings;)V

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mObserver:Lcom/android/settings/MiuiSoundSettings$SoundSettingsObserver;

    new-instance v0, Lcom/android/settings/MiuiSoundSettings$1;

    invoke-direct {v0, p0}, Lcom/android/settings/MiuiSoundSettings$1;-><init>(Lcom/android/settings/MiuiSoundSettings;)V

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mRingerModeReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mContentHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/settings/MiuiSoundSettings$2;

    invoke-direct {v1, p0, v0}, Lcom/android/settings/MiuiSoundSettings$2;-><init>(Lcom/android/settings/MiuiSoundSettings;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->mVibrateSettingsObserver:Landroid/database/ContentObserver;

    return-void
.end method

.method public static getHapticFeedbackLevelValue(Landroid/content/Context;)I
    .locals 3

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "haptic_feedback_enabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    const/4 p0, -0x1

    return p0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    sget v0, Landroid/provider/MiuiSettings$System;->HAPTIC_FEEDBACK_LEVEL_DEFAULT:I

    const-string v1, "haptic_feedback_level"

    invoke-static {p0, v1, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-static {v1, p0}, Ljava/lang/Math;->max(II)I

    move-result p0

    invoke-static {v0, p0}, Ljava/lang/Math;->min(II)I

    move-result p0

    return p0
.end method

.method public static hideRingtonePreference(Landroid/content/Context;)Z
    .locals 1

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/android/settingslib/Utils;->isWifiOnly(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/android/settings/Utils;->isVoiceCapable(Landroid/content/Context;)Z

    move-result p0

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private initRingToYouPreference()V
    .locals 5

    sget-boolean v0, Lcom/android/settings/utils/SettingsFeatures;->IS_NEED_ADD_RINGTOYOU:Z

    const-string/jumbo v1, "ring_toyou_check"

    const-string/jumbo v2, "ring_toyou"

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lcom/android/settings/SettingsPreferenceFragment;->removePreference(Ljava/lang/String;)Z

    invoke-virtual {p0, v1}, Lcom/android/settings/SettingsPreferenceFragment;->removePreference(Ljava/lang/String;)Z

    return-void

    :cond_0
    invoke-virtual {p0, v1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p0, v2}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v1

    if-eqz v0, :cond_3

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v3, v2, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    new-instance v0, Lcom/android/settings/MiuiSoundSettings$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/android/settings/MiuiSoundSettings$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/MiuiSoundSettings;)V

    invoke-virtual {v1, v0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    return-void

    :cond_2
    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    new-instance v2, Lcom/android/settings/MiuiSoundSettings$$ExternalSyntheticLambda1;

    invoke-direct {v2, p0, v0, v1}, Lcom/android/settings/MiuiSoundSettings$$ExternalSyntheticLambda1;-><init>(Lcom/android/settings/MiuiSoundSettings;Landroidx/preference/CheckBoxPreference;Landroidx/preference/Preference;)V

    invoke-virtual {v0, v2}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_3
    :goto_0
    return-void
.end method

.method private initRingtoneType()V
    .locals 2

    const-string v0, "MiuiSoundSettings"

    const-string v1, "init all ringtone type"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/settings/MiuiSoundSettingsBase;->mSupportCoolSound:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mRingtonePreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/MiuiDefaultRingtonePreference;->setRingtoneType(I)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mSmsReceivedSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz v0, :cond_2

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/android/settings/MiuiDefaultRingtonePreference;->setRingtoneType(I)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mSmsDeliveredSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz v0, :cond_3

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/settings/MiuiDefaultRingtonePreference;->setRingtoneType(I)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mCalendarSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz v0, :cond_4

    const/16 v1, 0x1000

    invoke-virtual {v0, v1}, Lcom/android/settings/MiuiDefaultRingtonePreference;->setRingtoneType(I)V

    :cond_4
    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettings;->mNotificationSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz p0, :cond_5

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiDefaultRingtonePreference;->setRingtoneType(I)V

    :cond_5
    return-void
.end method

.method private initSoundParams(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Lcom/android/settings/device/ParseMiShopDataUtils;->showBasicItems(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettings;->mHandler:Landroid/os/Handler;

    invoke-virtual {p0, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    :goto_0
    return-void
.end method

.method private initVolumePreference(Ljava/lang/String;II)V
    .locals 0

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/sound/VolumeSeekBarPreference;

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1, p2}, Lcom/android/settings/sound/VolumeSeekBarPreference;->setStream(I)V

    invoke-virtual {p1, p3}, Landroidx/preference/Preference;->setIcon(I)V

    new-instance p2, Lcom/android/settings/sound/SeekBarVolumizer;

    invoke-direct {p2, p1}, Lcom/android/settings/sound/SeekBarVolumizer;-><init>(Lcom/android/settings/sound/VolumeSeekBarPreference;)V

    invoke-virtual {p1, p2}, Lcom/android/settings/sound/VolumeSeekBarPreference;->setSeekBarVolumizer(Lcom/android/settings/sound/SeekBarVolumizer;)V

    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettings;->mVolumePrefs:Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private isInCommunication()Z
    .locals 2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "telecom"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    invoke-virtual {v0}, Landroid/telecom/TelecomManager;->isInCall()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettings;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {p0}, Landroid/media/AudioManager;->getMode()I

    move-result p0

    const/4 v0, 0x3

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static isSystemHapticEnable(Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "haptic_feedback_enabled"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    if-ne p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private synthetic lambda$initRingToYouPreference$0(Landroidx/preference/Preference;)Z
    .locals 1

    invoke-virtual {p1}, Landroidx/preference/Preference;->getFragment()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Landroidx/preference/PreferenceFragmentCompat$OnPreferenceStartFragmentCallback;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Landroidx/preference/PreferenceFragmentCompat$OnPreferenceStartFragmentCallback;

    invoke-interface {v0, p0, p1}, Landroidx/preference/PreferenceFragmentCompat$OnPreferenceStartFragmentCallback;->onPreferenceStartFragment(Landroidx/preference/PreferenceFragmentCompat;Landroidx/preference/Preference;)Z

    move-result p0

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method private synthetic lambda$initRingToYouPreference$1(Landroidx/preference/CheckBoxPreference;Landroidx/preference/Preference;Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 0

    invoke-virtual {p1}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result p3

    if-nez p3, :cond_0

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p3

    invoke-virtual {p3, p2}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    invoke-virtual {p1}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result p1

    const-string/jumbo p2, "ring_toyou"

    invoke-static {p0, p2, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_0
    const/4 p0, 0x1

    return p0
.end method

.method private refreshMuteModeSetting()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mRingerModeSettingPref:Landroidx/preference/CheckBoxPreference;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/provider/MiuiSettings$SoundMode;->isSilenceModeOn(Landroid/content/Context;)Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->mRingerModeSettingPref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x1

    const/4 v2, -0x3

    const-string/jumbo v3, "mute_music_at_silent"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "refreshMuteModeSetting(), muteMediaValue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiSoundSettings"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mMuteMediaSoundPref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mMuteMediaSoundPref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mMuteMediaSoundPref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :goto_0
    invoke-direct {p0}, Lcom/android/settings/MiuiSoundSettings;->refreshVolumePrefDrawable()V

    return-void
.end method

.method private refreshVolumePrefDrawable()V
    .locals 1

    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettings;->mVolumePrefs:Ljava/util/ArrayList;

    if-nez p0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/sound/VolumeSeekBarPreference;

    invoke-virtual {v0}, Lcom/android/settings/sound/VolumeSeekBarPreference;->updateSeekBarDrawable()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private refreshZenModeSetting()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mZenModePef:Lcom/android/settings/dndmode/LabelPreference;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v0}, Landroid/app/NotificationManager;->getZenMode()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/settings/R$string;->mode_enable:I

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/settings/R$string;->mode_disable:I

    :goto_0
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettings;->mZenModePef:Lcom/android/settings/dndmode/LabelPreference;

    invoke-virtual {p0, v0}, Lcom/android/settings/dndmode/LabelPreference;->setLabel(Ljava/lang/String;)V

    return-void
.end method

.method private sendRefreshMsg()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettings;->mHandler:Landroid/os/Handler;

    invoke-virtual {p0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method private setFragmentTitle()V
    .locals 3

    sget v0, Lcom/android/settings/R$string;->sound_settings:I

    const-string/jumbo v1, "vibrator"

    invoke-virtual {p0, v1}, Lcom/android/settings/SettingsPreferenceFragment;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/android/settings/utils/SettingsFeatures;->isSupportSettingsHaptic(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget v0, Lcom/android/settings/R$string;->sound_haptic_settings:I

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v1

    if-eqz v1, :cond_1

    sget v0, Lcom/android/settings/R$string;->sound_vibrate_settings:I

    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/android/settingslib/miuisettings/preference/PreferenceFragment;->getAppCompatActionBar()Lmiuix/appcompat/app/ActionBar;

    move-result-object p0

    if-eqz p0, :cond_2

    if-lez v0, :cond_2

    invoke-virtual {p0, v0}, Landroidx/appcompat/app/ActionBar;->setTitle(I)V

    :cond_2
    return-void
.end method

.method private setHapticFeedbackLevelValue(I)V
    .locals 5

    const/4 v0, 0x0

    const-string v1, "haptic_feedback_enabled"

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    const-class v2, Landroid/os/Vibrator;

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/16 v4, 0x12

    invoke-virtual {v1, v4}, Landroid/os/Vibrator;->getDefaultVibrationIntensity(I)I

    move-result v1

    const-string v4, "haptic_feedback_intensity"

    invoke-static {v2, v4, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "haptic_feedback_level"

    invoke-static {v1, v2, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    new-instance v1, Lmiui/util/HapticFeedbackUtil;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2, v3}, Lmiui/util/HapticFeedbackUtil;-><init>(Landroid/content/Context;Z)V

    invoke-virtual {v1, v3, v0}, Lmiui/util/HapticFeedbackUtil;->performHapticFeedback(IZ)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, v1, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :goto_0
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettingsBase;->mHapticFeedbackLevel:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lmiuix/preference/DropDownPreference;->setValue(Ljava/lang/String;)V

    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettingsBase;->mHapticFeedbackLevel:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    invoke-virtual {p0}, Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static setSystemHapticEnable(Landroid/content/Context;Z)V
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "haptic_feedback_enabled"

    invoke-static {p0, v0, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method private updateMuteCheckPref(Z)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x3

    const-string/jumbo v2, "mute_music_at_silent"

    invoke-static {v0, v2, p1, v1}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    invoke-direct {p0}, Lcom/android/settings/MiuiSoundSettings;->refreshVolumePrefDrawable()V

    invoke-direct {p0}, Lcom/android/settings/MiuiSoundSettings;->sendRefreshMsg()V

    return-void
.end method

.method private updateRingerModeSettingPref(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Landroid/provider/MiuiSettings$SoundMode;->setSilenceModeOn(Landroid/content/Context;Z)V

    invoke-direct {p0}, Lcom/android/settings/MiuiSoundSettings;->refreshVolumePrefDrawable()V

    invoke-direct {p0}, Lcom/android/settings/MiuiSoundSettings;->sendRefreshMsg()V

    return-void
.end method

.method private updateVibrateInNormalPref(Z)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "vibrate_in_normal"

    const/4 v2, -0x3

    invoke-static {v0, v1, p1, v2}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettings;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "vibrate_when_ringing"

    invoke-static {p0, v0, p1, v2}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    return-void
.end method

.method private updateVibrateInSilentPref(Z)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x3

    const-string/jumbo v2, "vibrate_in_silent"

    invoke-static {v0, v2, p1, v1}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerModeInternal()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettings;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {p0, p1}, Landroid/media/AudioManager;->setRingerModeInternal(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public DrawableToID(Ljava/lang/String;)I
    .locals 3

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "drawable"

    const-string v2, "com.android.settings"

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public enableWorkSync()V
    .locals 0

    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettings;->workSoundController:Lcom/android/settings/sound/MiuiWorkSoundPreferenceController;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/sound/MiuiWorkSoundPreferenceController;->enableWorkSync()V

    :cond_0
    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 0

    const-string p0, "MiuiSoundSettings"

    return-object p0
.end method

.method protected handleOthersSummery(Landroid/os/Message;)V
    .locals 2

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettings;->mCalendarSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz p0, :cond_3

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p0, p1}, Lcom/android/settings/MiuiDefaultRingtonePreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettings;->mSmsDeliveredSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz p0, :cond_3

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p0, p1}, Lcom/android/settings/MiuiDefaultRingtonePreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettings;->mSmsReceivedSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz p0, :cond_3

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p0, p1}, Lcom/android/settings/MiuiDefaultRingtonePreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_3
    :goto_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSoundSettingsBase;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mContext:Landroid/content/Context;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result p1

    iput p1, p0, Lcom/android/settings/MiuiSoundSettings;->mUserId:I

    iget-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mContext:Landroid/content/Context;

    invoke-static {p1}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mNotificationManager:Landroid/app/NotificationManager;

    iget-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/utils/SettingsFeatures;->isHideRingtoneCall(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x5

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    :goto_0
    sget v0, Lcom/android/settings/R$drawable;->ring_volume_icon:I

    const-string/jumbo v1, "ring_volume"

    invoke-direct {p0, v1, p1, v0}, Lcom/android/settings/MiuiSoundSettings;->initVolumePreference(Ljava/lang/String;II)V

    const-string/jumbo p1, "notification_volume"

    const/4 v0, 0x5

    const-string/jumbo v1, "notification_volume_icon"

    invoke-virtual {p0, v1}, Lcom/android/settings/MiuiSoundSettings;->DrawableToID(Ljava/lang/String;)I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, Lcom/android/settings/MiuiSoundSettings;->initVolumePreference(Ljava/lang/String;II)V

    const/4 p1, 0x4

    sget v0, Lcom/android/settings/R$drawable;->alarm_volume_icon:I

    const-string v1, "alarm_volume"

    invoke-direct {p0, v1, p1, v0}, Lcom/android/settings/MiuiSoundSettings;->initVolumePreference(Ljava/lang/String;II)V

    const/4 p1, 0x3

    sget v0, Lcom/android/settings/R$drawable;->media_volume_icon:I

    const-string/jumbo v1, "media_volume"

    invoke-direct {p0, v1, p1, v0}, Lcom/android/settings/MiuiSoundSettings;->initVolumePreference(Ljava/lang/String;II)V

    iget-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/MiuiUtils;->includeXiaoAi(Landroid/content/Context;)Z

    move-result p1

    const-string/jumbo v0, "voice_assist_volume"

    if-eqz p1, :cond_1

    const/16 p1, 0xb

    sget v1, Lcom/android/settings/R$drawable;->xiaoai_volume_icon:I

    invoke-direct {p0, v0, p1, v1}, Lcom/android/settings/MiuiSoundSettings;->initVolumePreference(Ljava/lang/String;II)V

    sget-object p1, Lcom/android/settings/MiuiSoundSettings;->mRestrictedKeyList:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :goto_1
    const-string/jumbo p1, "sound_mode_category"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/PreferenceCategory;

    iput-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mSoundModeCategory:Landroidx/preference/PreferenceCategory;

    const-string/jumbo p1, "ringer_mode_setting"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mRingerModeSettingPref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo p1, "mute_media_sound"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mMuteMediaSoundPref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    invoke-direct {p0}, Lcom/android/settings/MiuiSoundSettings;->refreshMuteModeSetting()V

    const-string/jumbo p1, "zen_mode_category_label"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/dndmode/LabelPreference;

    iput-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mZenModePef:Lcom/android/settings/dndmode/LabelPreference;

    invoke-direct {p0}, Lcom/android/settings/MiuiSoundSettings;->refreshZenModeSetting()V

    const-string/jumbo p1, "ring_toyou_check"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    const-string/jumbo p1, "ring_toyou"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    invoke-direct {p0}, Lcom/android/settings/MiuiSoundSettings;->initRingToYouPreference()V

    iget-object p1, p0, Lcom/android/settings/MiuiSoundSettingsBase;->mHapticFeedbackLevel:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    if-eqz p1, :cond_2

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-object p1, p0, Lcom/android/settings/MiuiSoundSettingsBase;->mHapticFeedbackLevel:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/MiuiSoundSettings;->getHapticFeedbackLevelValue(Landroid/content/Context;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmiuix/preference/DropDownPreference;->setValue(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/settings/MiuiSoundSettingsBase;->mHapticFeedbackLevel:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    invoke-virtual {p1}, Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_2
    const-string/jumbo p1, "sms_received_sound"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/MiuiDefaultRingtonePreference;

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mSmsReceivedSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    const-string/jumbo v0, "sms_delivered_sound"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/settings/MiuiDefaultRingtonePreference;

    iput-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->mSmsDeliveredSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    const-string v1, "calendar_sound"

    invoke-virtual {p0, v1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v2

    check-cast v2, Lcom/android/settings/MiuiDefaultRingtonePreference;

    iput-object v2, p0, Lcom/android/settings/MiuiSoundSettings;->mCalendarSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    const-string/jumbo v2, "notification_sound"

    invoke-virtual {p0, v2}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v3

    check-cast v3, Lcom/android/settings/MiuiDefaultRingtonePreference;

    iput-object v3, p0, Lcom/android/settings/MiuiSoundSettings;->mNotificationSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    const-string v3, "alarm_ringtone"

    invoke-virtual {p0, v3}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v3

    check-cast v3, Lcom/android/settings/MiuiDefaultRingtonePreference;

    iput-object v3, p0, Lcom/android/settings/MiuiSoundSettings;->mAlarmRingtonePreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    iget-boolean v3, p0, Lcom/android/settings/MiuiSoundSettingsBase;->mSupportCoolSound:Z

    const/4 v4, 0x0

    if-eqz v3, :cond_3

    invoke-virtual {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->removePreference(Ljava/lang/String;)Z

    invoke-virtual {p0, v0}, Lcom/android/settings/SettingsPreferenceFragment;->removePreference(Ljava/lang/String;)Z

    invoke-virtual {p0, v1}, Lcom/android/settings/SettingsPreferenceFragment;->removePreference(Ljava/lang/String;)Z

    invoke-virtual {p0, v2}, Lcom/android/settings/SettingsPreferenceFragment;->removePreference(Ljava/lang/String;)Z

    iput-object v4, p0, Lcom/android/settings/MiuiSoundSettings;->mSmsReceivedSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    iput-object v4, p0, Lcom/android/settings/MiuiSoundSettings;->mSmsDeliveredSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    iput-object v4, p0, Lcom/android/settings/MiuiSoundSettings;->mCalendarSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    iput-object v4, p0, Lcom/android/settings/MiuiSoundSettings;->mNotificationSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    :cond_3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/android/settingslib/Utils;->isWifiOnly(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mSmsDeliveredSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz p1, :cond_4

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mSmsDeliveredSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    iput-object v4, p0, Lcom/android/settings/MiuiSoundSettings;->mSmsDeliveredSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    :cond_4
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/android/settings/utils/SettingsFeatures;->isNeedRemoveSmsReceivedSound(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mSmsReceivedSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz p1, :cond_5

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mSmsReceivedSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    iput-object v4, p0, Lcom/android/settings/MiuiSoundSettings;->mSmsReceivedSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    :cond_5
    const-string p1, "audio"

    invoke-virtual {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/media/AudioManager;

    iput-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mAudioManager:Landroid/media/AudioManager;

    const-string/jumbo p1, "silent_mode"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mSilentModePref:Landroidx/preference/CheckBoxPreference;

    const-string p1, "key_vibrate_when_silent"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mVibrateWhenSilentPref:Landroidx/preference/CheckBoxPreference;

    const-string p1, "key_vibrate_when_ringing"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mVibrateWhenRingingPref:Landroidx/preference/CheckBoxPreference;

    iget-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mSilentModePref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mVibrateWhenSilentPref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mVibrateWhenRingingPref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    sget-boolean p1, Landroid/provider/MiuiSettings$SilenceMode;->isSupported:Z

    if-eqz p1, :cond_6

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mSilentModePref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    iput-object v4, p0, Lcom/android/settings/MiuiSoundSettings;->mSilentModePref:Landroidx/preference/CheckBoxPreference;

    goto :goto_2

    :cond_6
    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mSoundModeCategory:Landroidx/preference/PreferenceCategory;

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    iput-object v4, p0, Lcom/android/settings/MiuiSoundSettings;->mSoundModeCategory:Landroidx/preference/PreferenceCategory;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mZenModePef:Lcom/android/settings/dndmode/LabelPreference;

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    iput-object v4, p0, Lcom/android/settings/MiuiSoundSettings;->mZenModePef:Lcom/android/settings/dndmode/LabelPreference;

    :goto_2
    const-string/jumbo p1, "vibrator"

    invoke-virtual {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/os/Vibrator;

    invoke-virtual {p1}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result p1

    if-nez p1, :cond_a

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mVibrateWhenSilentPref:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_7

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_7
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mVibrateWhenRingingPref:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_8

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_8
    const-string/jumbo v0, "miui_vibrate_category"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_9
    iput-object v4, p0, Lcom/android/settings/MiuiSoundSettings;->mVibrateWhenRingingPref:Landroidx/preference/CheckBoxPreference;

    iput-object v4, p0, Lcom/android/settings/MiuiSoundSettings;->mVibrateWhenSilentPref:Landroidx/preference/CheckBoxPreference;

    :cond_a
    sget-boolean p1, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz p1, :cond_b

    iget-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mVibrateWhenRingingPref:Landroidx/preference/CheckBoxPreference;

    if-eqz p1, :cond_b

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mVibrateWhenRingingPref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    iput-object v4, p0, Lcom/android/settings/MiuiSoundSettings;->mVibrateWhenRingingPref:Landroidx/preference/CheckBoxPreference;

    :cond_b
    iget-object p1, p0, Lcom/android/settings/MiuiSoundSettingsBase;->mSystemHapticPreference:Landroidx/preference/CheckBoxPreference;

    if-eqz p1, :cond_c

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/MiuiSoundSettings;->isSystemHapticEnable(Landroid/content/Context;)Z

    move-result v0

    invoke-virtual {p1, v0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p1, p0, Lcom/android/settings/MiuiSoundSettingsBase;->mSystemHapticPreference:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_c
    const-string/jumbo p1, "ringtone"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/MiuiDefaultRingtonePreference;

    iput-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mRingtonePreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/android/settings/MiuiSoundSettings;->hideRingtonePreference(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_d

    iget-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mRingtonePreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz p1, :cond_d

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mRingtonePreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    iput-object v4, p0, Lcom/android/settings/MiuiSoundSettings;->mRingtonePreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    :cond_d
    invoke-direct {p0}, Lcom/android/settings/MiuiSoundSettings;->initRingtoneType()V

    iget-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mObserver:Lcom/android/settings/MiuiSoundSettings$SoundSettingsObserver;

    invoke-virtual {p1}, Lcom/android/settings/MiuiSoundSettings$SoundSettingsObserver;->register()V

    new-instance p1, Lcom/android/settings/sound/MiuiWorkSoundPreferenceController;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    invoke-direct {p1, v0, p0}, Lcom/android/settings/sound/MiuiWorkSoundPreferenceController;-><init>(Landroidx/preference/PreferenceScreen;Lcom/android/settings/MiuiSoundSettings;)V

    iput-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->workSoundController:Lcom/android/settings/sound/MiuiWorkSoundPreferenceController;

    new-instance p1, Lcom/android/settings/sound/MiuiAlarmRingtonePreferenceController;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/android/settings/sound/MiuiAlarmRingtonePreferenceController;-><init>(Landroidx/preference/PreferenceScreen;)V

    iput-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mMiuiAlarmRingtoneController:Lcom/android/settings/sound/MiuiAlarmRingtonePreferenceController;

    iget-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mControllers:Ljava/util/List;

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->workSoundController:Lcom/android/settings/sound/MiuiWorkSoundPreferenceController;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mControllers:Ljava/util/List;

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mMiuiAlarmRingtoneController:Lcom/android/settings/sound/MiuiAlarmRingtonePreferenceController;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/android/settings/utils/SettingsFeatures;->isIncallShowNeeded(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_e

    const-string p1, "incall_show"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    if-eqz p1, :cond_e

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_e
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/android/settings/utils/SettingsFeatures;->isMisoundShowNeeded(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_10

    const-string p1, "headset_settings"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    if-eqz p1, :cond_f

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_f
    const-string/jumbo p1, "sound_assist_settings"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    if-eqz p1, :cond_10

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_10
    iget-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/MiuiSoundSettings;->hideRingtonePreference(Landroid/content/Context;)Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_11

    iget-object p1, p0, Lcom/android/settings/MiuiSoundSettingsBase;->mRingtoneCardPreference:Lcom/android/settings/sound/RingtoneCardPreference;

    if-eqz p1, :cond_11

    invoke-virtual {p1, v0}, Lcom/android/settings/sound/RingtoneCardPreference;->setDisable(I)V

    :cond_11
    const-string/jumbo p1, "sound_speaker_category"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/PreferenceCategory;

    iput-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mSoundSpeakerCategory:Landroidx/preference/PreferenceCategory;

    const-string/jumbo p1, "sound_speaker_preference"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/soundsettings/SoundSpeakerDescPreference;

    iput-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mSoundSpeakerPreference:Lcom/android/settings/soundsettings/SoundSpeakerDescPreference;

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mSoundSpeakerCategory:Landroidx/preference/PreferenceCategory;

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    sget-boolean p1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez p1, :cond_12

    new-instance p1, Lcom/android/settings/MiuiSoundSettings$UpdateInfoCallback;

    invoke-direct {p1, p0}, Lcom/android/settings/MiuiSoundSettings$UpdateInfoCallback;-><init>(Lcom/android/settings/MiuiSoundSettings;)V

    iput-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mDeviceInfoCallback:Lcom/android/settings/MiuiSoundSettings$UpdateInfoCallback;

    new-instance p1, Lcom/android/settings/MiuiSoundSettings$RemoteServiceConn;

    invoke-direct {p1, p0, v4}, Lcom/android/settings/MiuiSoundSettings$RemoteServiceConn;-><init>(Lcom/android/settings/MiuiSoundSettings;Lcom/android/settings/MiuiSoundSettings$RemoteServiceConn-IA;)V

    iput-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mRemoteServiceConn:Lcom/android/settings/MiuiSoundSettings$RemoteServiceConn;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettings;->mRemoteServiceConn:Lcom/android/settings/MiuiSoundSettings$RemoteServiceConn;

    invoke-static {p1, p0}, Lcom/android/settings/device/RemoteServiceUtil;->bindRemoteService(Landroid/content/Context;Landroid/content/ServiceConnection;)Z

    :cond_12
    return-void
.end method

.method public onDestroy()V
    .locals 3

    invoke-super {p0}, Lcom/android/settingslib/core/lifecycle/ObservablePreferenceFragment;->onDestroy()V

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mControllers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/utils/MiuiBaseController;

    invoke-virtual {v1}, Lcom/android/settings/utils/MiuiBaseController;->destroy()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mVolumePrefs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/sound/VolumeSeekBarPreference;

    invoke-virtual {v1}, Lcom/android/settings/sound/VolumeSeekBarPreference;->getSeekBarVolumizer()Lcom/android/settings/sound/SeekBarVolumizer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/sound/SeekBarVolumizer;->stop()V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mSilentModePref:Landroidx/preference/CheckBoxPreference;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mVibrateWhenSilentPref:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_3

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mVibrateWhenRingingPref:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_4

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_4
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mObserver:Lcom/android/settings/MiuiSoundSettings$SoundSettingsObserver;

    invoke-virtual {v0}, Lcom/android/settings/MiuiSoundSettings$SoundSettingsObserver;->unregister()V

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mRemoteService:Lcom/android/settings/aidl/IRemoteGetDeviceInfoService;

    if-eqz v0, :cond_6

    :try_start_0
    iget-object v2, p0, Lcom/android/settings/MiuiSoundSettings;->mDeviceInfoCallback:Lcom/android/settings/MiuiSoundSettings$UpdateInfoCallback;

    if-eqz v2, :cond_5

    invoke-interface {v0, v2}, Lcom/android/settings/aidl/IRemoteGetDeviceInfoService;->unregisteCallback(Lcom/android/settings/aidl/IRequestCallback;)V

    iput-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->mDeviceInfoCallback:Lcom/android/settings/MiuiSoundSettings$UpdateInfoCallback;

    :cond_5
    iput-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->mRemoteService:Lcom/android/settings/aidl/IRemoteGetDeviceInfoService;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_6
    :goto_2
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mRemoteServiceConn:Lcom/android/settings/MiuiSoundSettings$RemoteServiceConn;

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/MiuiSoundSettings;->mRemoteServiceConn:Lcom/android/settings/MiuiSoundSettings$RemoteServiceConn;

    invoke-static {v0, v2}, Lcom/android/settings/device/RemoteServiceUtil;->unBindRemoteService(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    :cond_7
    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettings;->mHandler:Landroid/os/Handler;

    if-eqz p0, :cond_8

    invoke-virtual {p0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_8
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5

    invoke-direct {p0}, Lcom/android/settings/MiuiSoundSettings;->isInCommunication()Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    return v0

    :cond_0
    const/4 p1, 0x2

    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/android/settings/SettingsPreferenceFragment;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 p1, 0x3

    :cond_1
    const/16 v1, 0x18

    const/16 v2, 0x19

    const/4 v3, 0x1

    if-eq p2, v2, :cond_2

    if-ne p2, v1, :cond_5

    :cond_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_5

    if-ne p2, v2, :cond_3

    iput-boolean v3, p0, Lcom/android/settings/MiuiSoundSettings;->mVolumeDownPressed:Z

    :cond_3
    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettings;->mAudioManager:Landroid/media/AudioManager;

    if-ne p2, v1, :cond_4

    move p2, v3

    goto :goto_0

    :cond_4
    const/4 p2, -0x1

    :goto_0
    const p3, 0x100400

    invoke-virtual {p0, p1, p2, p3}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    return v3

    :cond_5
    if-ne p2, v2, :cond_6

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result p1

    if-ne p1, v3, :cond_6

    iput-boolean v0, p0, Lcom/android/settings/MiuiSoundSettings;->mVolumeDownPressed:Z

    :cond_6
    return v0
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mControllers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/utils/MiuiBaseController;

    invoke-virtual {v1}, Lcom/android/settings/utils/MiuiBaseController;->pause()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->mVibrateSettingsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->mRingerModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-super {p0}, Lcom/android/settings/MiuiSoundSettingsBase;->onPause()V

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mVolumePrefs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/sound/VolumeSeekBarPreference;

    invoke-virtual {v1}, Lcom/android/settings/sound/VolumeSeekBarPreference;->getSeekBarVolumizer()Lcom/android/settings/sound/SeekBarVolumizer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/sound/SeekBarVolumizer;->pause()V

    goto :goto_1

    :cond_1
    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettings;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getListView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->isComputingLayout()Z

    move-result v0

    const-string v1, "MiuiSoundSettings"

    if-eqz v0, :cond_0

    const-string p0, "isComputingLayout"

    invoke-static {v1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p0, 0x0

    return p0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettingsBase;->mHapticFeedbackLevel:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    if-ne p1, v0, :cond_1

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiSoundSettings;->setHapticFeedbackLevelValue(I)V

    goto/16 :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettingsBase;->mSystemHapticPreference:Landroidx/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_2

    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettings;->mContext:Landroid/content/Context;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-static {p0, p1}, Lcom/android/settings/MiuiSoundSettings;->setSystemHapticEnable(Landroid/content/Context;Z)V

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mSilentModePref:Landroidx/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_3

    sget-boolean v0, Landroid/provider/MiuiSettings$SilenceMode;->isSupported:Z

    if-nez v0, :cond_3

    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettings;->mContext:Landroid/content/Context;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-static {p0, p1}, Landroid/provider/MiuiSettings$SoundMode;->setSilenceModeOn(Landroid/content/Context;Z)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mVibrateWhenRingingPref:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_4

    if-ne p1, v0, :cond_4

    const-string/jumbo p1, "setting_sound_sring"

    invoke-static {p1, p2}, Lcom/android/settings/report/InternationalCompat;->trackReportSwitchStatus(Ljava/lang/String;Ljava/lang/Object;)V

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiSoundSettings;->updateVibrateInNormalPref(Z)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mVibrateWhenSilentPref:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_5

    if-ne p1, v0, :cond_5

    const-string/jumbo p1, "setting_sound_smute"

    invoke-static {p1, p2}, Lcom/android/settings/report/InternationalCompat;->trackReportSwitchStatus(Ljava/lang/String;Ljava/lang/Object;)V

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiSoundSettings;->updateVibrateInSilentPref(Z)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mRingerModeSettingPref:Landroidx/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_6

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "mRingerModeSettingPref change, objValue : "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiSoundSettings;->updateRingerModeSettingPref(Z)V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mMuteMediaSoundPref:Landroidx/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_7

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "mMediaSoundSettingPref change, objValue : "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiSoundSettings;->updateMuteCheckPref(Z)V

    :cond_7
    :goto_0
    const/4 p0, 0x1

    return p0
.end method

.method public onPreferenceTreeClick(Landroidx/preference/PreferenceScreen;Landroidx/preference/Preference;)Z
    .locals 3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiSoundSettings;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p2}, Landroidx/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/settings/MiuiUtils;->getResourceName(Landroid/content/Context;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settingslib/util/MiStatInterfaceUtils;->trackPreferenceClick(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    instance-of v0, p2, Lcom/android/settings/sound/MiuiWorkRingtonePreference;

    if-eqz v0, :cond_2

    check-cast p2, Lcom/android/settings/sound/MiuiWorkRingtonePreference;

    iput-object p2, p0, Lcom/android/settings/MiuiSoundSettings;->mRequestPreference:Lcom/android/settings/sound/MiuiWorkRingtonePreference;

    invoke-virtual {p2}, Lcom/android/settings/RingtonePreference;->getUserId()I

    move-result p1

    iget-object p2, p0, Lcom/android/settings/MiuiSoundSettings;->mRequestPreference:Lcom/android/settings/sound/MiuiWorkRingtonePreference;

    invoke-virtual {p2}, Landroidx/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object p2

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mContext:Landroid/content/Context;

    invoke-static {v0, p2, p1}, Lcom/android/settings/MiuiUtils;->isIntentActivityExistAsUser(Landroid/content/Context;Landroid/content/Intent;I)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    const/16 v0, 0xc8

    invoke-static {p1}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object p1

    invoke-virtual {p0, p2, v0, v1, p1}, Landroid/app/Activity;->startActivityForResultAsUser(Landroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)V

    goto :goto_0

    :cond_1
    new-instance p1, Lmiuix/appcompat/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    invoke-direct {p1, p0}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p0, Lcom/android/settings/R$string;->work_sound_permission_dialog_title:I

    invoke-virtual {p1, p0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p0

    sget p1, Lcom/android/settings/R$string;->work_sound_permission_dialog_message:I

    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p0

    sget p1, Lcom/android/settings/R$string;->work_sound_permission_dialog_button_text_known:I

    invoke-virtual {p0, p1, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object p0

    invoke-virtual {p0}, Landroid/app/Dialog;->show()V

    :goto_0
    const/4 p0, 0x1

    return p0

    :cond_2
    invoke-super {p0, p1, p2}, Lcom/android/settings/MiuiSoundSettingsBase;->onPreferenceTreeClick(Landroidx/preference/PreferenceScreen;Landroidx/preference/Preference;)Z

    move-result p0

    return p0
.end method

.method public onResume()V
    .locals 5

    invoke-super {p0}, Lcom/android/settings/MiuiSoundSettingsBase;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/MiuiSoundSettings;->setFragmentTitle()V

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mControllers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/utils/MiuiBaseController;

    invoke-virtual {v1}, Lcom/android/settings/utils/MiuiBaseController;->resume()V

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.media.INTERNAL_RINGER_MODE_CHANGED_ACTION"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "android.media.VOLUME_CHANGED_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/MiuiSoundSettings;->mRingerModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "vibrate_in_silent"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/MiuiSoundSettings;->mVibrateSettingsObserver:Landroid/database/ContentObserver;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    const-string/jumbo v1, "vibrate_in_normal"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/MiuiSoundSettings;->mVibrateSettingsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mVolumePrefs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/sound/VolumeSeekBarPreference;

    invoke-virtual {v1}, Lcom/android/settings/sound/VolumeSeekBarPreference;->getSeekBarVolumizer()Lcom/android/settings/sound/SeekBarVolumizer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/sound/SeekBarVolumizer;->resume()V

    goto :goto_1

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/MiuiSoundSettings;->sendRefreshMsg()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/MiuiSoundSettings;->mUserId:I

    const-string/jumbo v2, "no_adjust_volume"

    invoke-static {v0, v2, v1}, Lcom/android/settingslib/RestrictedLockUtilsInternal;->checkIfRestrictionEnforced(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    iget v3, p0, Lcom/android/settings/MiuiSoundSettings;->mUserId:I

    invoke-static {v1, v2, v3}, Lcom/android/settingslib/RestrictedLockUtilsInternal;->hasBaseUserRestriction(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v1

    sget-object v2, Lcom/android/settings/MiuiSoundSettings;->mRestrictedKeyList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p0, v3}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v3

    if-eqz v3, :cond_3

    xor-int/lit8 v4, v1, 0x1

    invoke-virtual {v3, v4}, Landroidx/preference/Preference;->setEnabled(Z)V

    :cond_3
    instance-of v4, v3, Lcom/android/settingslib/RestrictedPreference;

    if-eqz v4, :cond_2

    if-nez v1, :cond_2

    check-cast v3, Lcom/android/settingslib/RestrictedPreference;

    invoke-virtual {v3, v0}, Lcom/android/settingslib/RestrictedPreference;->setDisabledByAdmin(Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)V

    goto :goto_2

    :cond_4
    const-string v0, "cell_broadcast_settings"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p0

    check-cast p0, Lcom/android/settingslib/RestrictedPreference;

    if-eqz p0, :cond_5

    const-string/jumbo v0, "no_config_cell_broadcasts"

    invoke-virtual {p0, v0}, Lcom/android/settingslib/RestrictedPreference;->checkRestrictionAndSetDisabled(Ljava/lang/String;)V

    :cond_5
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/android/settings/SettingsPreferenceFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    const/4 p0, 0x1

    invoke-virtual {p1, p0}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    return-void
.end method

.method refreshVolumeAndVibrate()V
    .locals 6

    goto/32 :goto_4

    nop

    :goto_0
    if-nez v1, :cond_0

    goto/32 :goto_16

    :cond_0
    goto/32 :goto_11

    nop

    :goto_1
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_2c

    nop

    :goto_2
    iget-object v2, p0, Lcom/android/settings/MiuiSoundSettings;->mContext:Landroid/content/Context;

    goto/32 :goto_6

    nop

    :goto_3
    const/4 v3, -0x3

    goto/32 :goto_17

    nop

    :goto_4
    const-string v0, "MiuiSoundSettings"

    goto/32 :goto_1c

    nop

    :goto_5
    invoke-static {v0, v5, v1, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    goto/32 :goto_10

    nop

    :goto_6
    invoke-static {v2}, Landroid/provider/MiuiSettings$SoundMode;->isSilenceModeOn(Landroid/content/Context;)Z

    move-result v2

    goto/32 :goto_1d

    nop

    :goto_7
    invoke-direct {p0}, Lcom/android/settings/MiuiSoundSettings;->refreshMuteModeSetting()V

    goto/32 :goto_1b

    nop

    :goto_8
    if-eq v0, v4, :cond_1

    goto/32 :goto_24

    :cond_1
    goto/32 :goto_23

    nop

    :goto_9
    iget-object v5, p0, Lcom/android/settings/MiuiSoundSettings;->mVibrateWhenSilentPref:Landroidx/preference/CheckBoxPreference;

    goto/32 :goto_14

    nop

    :goto_a
    if-nez v1, :cond_2

    goto/32 :goto_1e

    :cond_2
    goto/32 :goto_2

    nop

    :goto_b
    if-eqz v0, :cond_3

    goto/32 :goto_19

    :cond_3
    goto/32 :goto_18

    nop

    :goto_c
    invoke-virtual {v5, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :goto_d
    goto/32 :goto_12

    nop

    :goto_e
    move v1, v4

    goto/32 :goto_29

    nop

    :goto_f
    sget-boolean v1, Landroid/provider/MiuiSettings$System;->VIBRATE_IN_NORMAL_DEFAULT:Z

    goto/32 :goto_21

    nop

    :goto_10
    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->mVibrateWhenRingingPref:Landroidx/preference/CheckBoxPreference;

    goto/32 :goto_8

    nop

    :goto_11
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    goto/32 :goto_f

    nop

    :goto_12
    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->mVibrateWhenRingingPref:Landroidx/preference/CheckBoxPreference;

    goto/32 :goto_0

    nop

    :goto_13
    return-void

    :goto_14
    if-eq v1, v4, :cond_4

    goto/32 :goto_2a

    :cond_4
    goto/32 :goto_e

    nop

    :goto_15
    invoke-virtual {v1, v2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :goto_16
    goto/32 :goto_7

    nop

    :goto_17
    const/4 v4, 0x1

    goto/32 :goto_22

    nop

    :goto_18
    return-void

    :goto_19
    goto/32 :goto_1f

    nop

    :goto_1a
    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->mVibrateWhenSilentPref:Landroidx/preference/CheckBoxPreference;

    goto/32 :goto_20

    nop

    :goto_1b
    invoke-direct {p0}, Lcom/android/settings/MiuiSoundSettings;->refreshZenModeSetting()V

    goto/32 :goto_13

    nop

    :goto_1c
    const-string/jumbo v1, "refreshVolumeAndVibrate"

    goto/32 :goto_1

    nop

    :goto_1d
    invoke-virtual {v1, v2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :goto_1e
    goto/32 :goto_1a

    nop

    :goto_1f
    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettings;->mSilentModePref:Landroidx/preference/CheckBoxPreference;

    goto/32 :goto_a

    nop

    :goto_20
    const/4 v2, 0x0

    goto/32 :goto_3

    nop

    :goto_21
    const-string/jumbo v5, "vibrate_in_normal"

    goto/32 :goto_5

    nop

    :goto_22
    if-nez v1, :cond_5

    goto/32 :goto_d

    :cond_5
    goto/32 :goto_25

    nop

    :goto_23
    move v2, v4

    :goto_24
    goto/32 :goto_15

    nop

    :goto_25
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    goto/32 :goto_26

    nop

    :goto_26
    const-string/jumbo v5, "vibrate_in_silent"

    goto/32 :goto_2b

    nop

    :goto_27
    move v1, v2

    :goto_28
    goto/32 :goto_c

    nop

    :goto_29
    goto :goto_28

    :goto_2a
    goto/32 :goto_27

    nop

    :goto_2b
    invoke-static {v1, v5, v4, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    goto/32 :goto_9

    nop

    :goto_2c
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    goto/32 :goto_b

    nop
.end method

.method protected ringtoneLookupOthers()V
    .locals 2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mSmsReceivedSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/settings/DefaultRingtonePreference;->getUri()Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/MiuiSoundSettingsBase;->updateRingtoneName(Landroid/net/Uri;I)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mSmsDeliveredSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/settings/DefaultRingtonePreference;->getUri()Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/MiuiSoundSettingsBase;->updateRingtoneName(Landroid/net/Uri;I)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mCalendarSoundPreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/android/settings/DefaultRingtonePreference;->getUri()Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/MiuiSoundSettingsBase;->updateRingtoneName(Landroid/net/Uri;I)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettings;->mAlarmRingtonePreference:Lcom/android/settings/MiuiDefaultRingtonePreference;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/android/settings/DefaultRingtonePreference;->getUri()Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/MiuiSoundSettingsBase;->updateRingtoneName(Landroid/net/Uri;I)V

    :cond_4
    return-void
.end method

.method protected updateOthers()V
    .locals 2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSoundSettingsBase;->isRingtoneViewEnable(I)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettingsBase;->mRingtoneCardPreference:Lcom/android/settings/sound/RingtoneCardPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/sound/RingtoneCardPreference;->getUri(I)Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/MiuiSoundSettingsBase;->updateRingtoneName(Landroid/net/Uri;I)V

    :cond_1
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSoundSettingsBase;->isRingtoneViewEnable(I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettingsBase;->mRingtoneCardPreference:Lcom/android/settings/sound/RingtoneCardPreference;

    invoke-virtual {v1, v0}, Lcom/android/settings/sound/RingtoneCardPreference;->getUri(I)Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/MiuiSoundSettingsBase;->updateRingtoneName(Landroid/net/Uri;I)V

    :cond_2
    return-void
.end method

.method public updateSoundDesc(Ljava/lang/String;)V
    .locals 6

    invoke-static {p1}, Lcom/android/settings/device/ParseMiShopDataUtils;->getBasicItemsArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-lez v3, :cond_3

    move-object v3, v2

    move v2, v0

    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v0, v4, :cond_2

    invoke-static {p1, v0}, Lcom/android/settings/device/JSONUtils;->getJSONObject(Lorg/json/JSONArray;I)Lorg/json/JSONObject;

    move-result-object v4

    invoke-static {v4}, Lcom/android/settings/device/ParseMiShopDataUtils;->getItemIndex(Lorg/json/JSONObject;)I

    move-result v5

    if-eqz v5, :cond_1

    if-eq v5, v1, :cond_0

    goto :goto_1

    :cond_0
    invoke-static {v4}, Lcom/android/settings/device/ParseMiShopDataUtils;->getItemBooleanSummary(Lorg/json/JSONObject;)Z

    move-result v2

    goto :goto_1

    :cond_1
    invoke-static {v4}, Lcom/android/settings/device/ParseMiShopDataUtils;->getItemSummary(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v3

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    move-object v2, v3

    :cond_3
    iget-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mSoundSpeakerCategory:Landroidx/preference/PreferenceCategory;

    if-eqz p1, :cond_4

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    xor-int/2addr v3, v1

    invoke-virtual {p1, v3}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mSoundSpeakerPreference:Lcom/android/settings/soundsettings/SoundSpeakerDescPreference;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    xor-int/2addr v1, v3

    invoke-virtual {p1, v1}, Landroidx/preference/Preference;->setVisible(Z)V

    iget-object p1, p0, Lcom/android/settings/MiuiSoundSettings;->mSoundSpeakerPreference:Lcom/android/settings/soundsettings/SoundSpeakerDescPreference;

    invoke-virtual {p1, v0}, Lcom/android/settings/soundsettings/SoundSpeakerDescPreference;->setHarman(Z)V

    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettings;->mSoundSpeakerPreference:Lcom/android/settings/soundsettings/SoundSpeakerDescPreference;

    invoke-virtual {p0, v2}, Lcom/android/settings/soundsettings/SoundSpeakerDescPreference;->setSummary(Ljava/lang/String;)V

    :cond_4
    return-void
.end method

.method protected updateValue(Landroid/os/Message;)V
    .locals 2

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_1

    const/4 v1, 0x7

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x3

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p0, v0, p1}, Lcom/android/settings/MiuiSoundSettingsBase;->setRingtoneValue(ILjava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p0, v0, p1}, Lcom/android/settings/MiuiSoundSettingsBase;->setRingtoneValue(ILjava/lang/CharSequence;)V

    :goto_0
    return-void
.end method
