.class public Lcom/android/settings/FullScreenDisplaySettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/FullScreenDisplaySettings$LauncherPackageMonitor;
    }
.end annotation


# instance fields
.field private mAlertDialog:Lmiuix/appcompat/app/AlertDialog;

.field private mAppGuide:Landroidx/preference/Preference;

.field private mAppQuickSwitchGuide:Landroidx/preference/Preference;

.field private mAppSwitchFeature:Landroidx/preference/CheckBoxPreference;

.field private mAppSwitchGuide:Landroidx/preference/Preference;

.field private mAutoDisableScreenButtons:Landroidx/preference/Preference;

.field private mBackGuide:Landroidx/preference/Preference;

.field private mClickOnDialog:Z

.field private mContext:Landroid/content/Context;

.field private mDemoExistes:Z

.field private mGuideCategory:Landroidx/preference/PreferenceCategory;

.field private mHasCheckedDemo:Z

.field private mHideGestureLine:Landroidx/preference/CheckBoxPreference;

.field private mHomeGuide:Landroidx/preference/Preference;

.field private mIsRecentsWithinLauncher:Z

.field private mKeyShortcutSettings:Landroidx/preference/Preference;

.field private mMistakeTouch:Landroidx/preference/CheckBoxPreference;

.field private mNeedShowDialog:Z

.field private mPackageMonitor:Lcom/android/settings/FullScreenDisplaySettings$LauncherPackageMonitor;

.field private mRecentGuide:Landroidx/preference/Preference;

.field private mScreenButtonHide:Landroidx/preference/CheckBoxPreference;

.field private mScreenButtonHideListener:Landroid/database/ContentObserver;

.field private mSettingCategory:Landroidx/preference/PreferenceCategory;

.field private mSharedPreferences:Landroid/content/SharedPreferences;

.field private mSwitchScreenButtonOrder:Landroidx/preference/CheckBoxPreference;

.field private mUseMiuiHomeAsDefaultHome:Z

.field private final mUserPreferenceChangeReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAlertDialog(Lcom/android/settings/FullScreenDisplaySettings;)Lmiuix/appcompat/app/AlertDialog;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mAlertDialog:Lmiuix/appcompat/app/AlertDialog;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmClickOnDialog(Lcom/android/settings/FullScreenDisplaySettings;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mClickOnDialog:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/settings/FullScreenDisplaySettings;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHideGestureLine(Lcom/android/settings/FullScreenDisplaySettings;)Landroidx/preference/CheckBoxPreference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mHideGestureLine:Landroidx/preference/CheckBoxPreference;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsRecentsWithinLauncher(Lcom/android/settings/FullScreenDisplaySettings;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mIsRecentsWithinLauncher:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmNeedShowDialog(Lcom/android/settings/FullScreenDisplaySettings;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mNeedShowDialog:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmScreenButtonHide(Lcom/android/settings/FullScreenDisplaySettings;)Landroidx/preference/CheckBoxPreference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mScreenButtonHide:Landroidx/preference/CheckBoxPreference;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSharedPreferences(Lcom/android/settings/FullScreenDisplaySettings;)Landroid/content/SharedPreferences;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mSharedPreferences:Landroid/content/SharedPreferences;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmUseMiuiHomeAsDefaultHome(Lcom/android/settings/FullScreenDisplaySettings;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mUseMiuiHomeAsDefaultHome:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmAlertDialog(Lcom/android/settings/FullScreenDisplaySettings;Lmiuix/appcompat/app/AlertDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mAlertDialog:Lmiuix/appcompat/app/AlertDialog;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmClickOnDialog(Lcom/android/settings/FullScreenDisplaySettings;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mClickOnDialog:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsRecentsWithinLauncher(Lcom/android/settings/FullScreenDisplaySettings;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mIsRecentsWithinLauncher:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmNeedShowDialog(Lcom/android/settings/FullScreenDisplaySettings;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mNeedShowDialog:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmUseMiuiHomeAsDefaultHome(Lcom/android/settings/FullScreenDisplaySettings;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mUseMiuiHomeAsDefaultHome:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mcheckDemoExist(Lcom/android/settings/FullScreenDisplaySettings;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->checkDemoExist()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mcreateDialog(Lcom/android/settings/FullScreenDisplaySettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->createDialog()V

    return-void
.end method

.method static bridge synthetic -$$Nest$misAppSwitchFeatureEnable(Lcom/android/settings/FullScreenDisplaySettings;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->isAppSwitchFeatureEnable()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misMatchDefaultHome(Lcom/android/settings/FullScreenDisplaySettings;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/FullScreenDisplaySettings;->isMatchDefaultHome(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$msetAppSwitchFeatureEnable(Lcom/android/settings/FullScreenDisplaySettings;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/FullScreenDisplaySettings;->setAppSwitchFeatureEnable(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetHideGestureLine(Lcom/android/settings/FullScreenDisplaySettings;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/FullScreenDisplaySettings;->setHideGestureLine(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetMistakeTouchEnable(Lcom/android/settings/FullScreenDisplaySettings;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/FullScreenDisplaySettings;->setMistakeTouchEnable(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mshowForceImmersiveHintDialog(Lcom/android/settings/FullScreenDisplaySettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->showForceImmersiveHintDialog()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateGestureLineOfNavBarGuideView(Lcom/android/settings/FullScreenDisplaySettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->updateGestureLineOfNavBarGuideView()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateHideGesturePreference(Lcom/android/settings/FullScreenDisplaySettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->updateHideGesturePreference()V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    new-instance v0, Lcom/android/settings/FullScreenDisplaySettings$12;

    invoke-direct {v0, p0}, Lcom/android/settings/FullScreenDisplaySettings$12;-><init>(Lcom/android/settings/FullScreenDisplaySettings;)V

    iput-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mUserPreferenceChangeReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/settings/FullScreenDisplaySettings$14;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, v1}, Lcom/android/settings/FullScreenDisplaySettings$14;-><init>(Lcom/android/settings/FullScreenDisplaySettings;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mScreenButtonHideListener:Landroid/database/ContentObserver;

    return-void
.end method

.method private addHideGesturePreference()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mHideGestureLine:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mAppSwitchFeature:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mGuideCategory:Landroidx/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mAppQuickSwitchGuide:Landroidx/preference/Preference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->updateAppQuickSwitchGuide()V

    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mGuideCategory:Landroidx/preference/PreferenceCategory;

    iget-object p0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mAppSwitchGuide:Landroidx/preference/Preference;

    invoke-virtual {v0, p0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    return-void
.end method

.method private checkDemoExist()Z
    .locals 5

    iget-boolean v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mHasCheckedDemo:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mHasCheckedDemo:Z

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.android.systemui"

    const-string v4, "com.android.systemui.fsgesture.HomeDemoAct"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v2, "DEMO_TYPE"

    const-string v3, "DEMO_TO_HOME"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    iput-boolean v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mDemoExistes:Z

    :cond_0
    iget-boolean p0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mDemoExistes:Z

    return p0
.end method

.method private createDialog()V
    .locals 4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mClickOnDialog:Z

    new-instance v1, Lmiuix/appcompat/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/android/settings/FullScreenDisplaySettings;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/android/settings/R$string;->navigation_guide_gesture_line_dialog_title:I

    invoke-virtual {v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/android/settings/R$string;->navigation_guide_gesture_line_dialog_summary:I

    invoke-virtual {v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/FullScreenDisplaySettings;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/android/settings/R$string;->navigation_guide_dialog_dont_show_again:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setCheckBox(ZLjava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/android/settings/R$string;->navigation_guide_dialog_ok:I

    new-instance v2, Lcom/android/settings/FullScreenDisplaySettings$16;

    invoke-direct {v2, p0}, Lcom/android/settings/FullScreenDisplaySettings$16;-><init>(Lcom/android/settings/FullScreenDisplaySettings;)V

    invoke-virtual {v0, v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/android/settings/R$string;->navigation_guide_dialog_skip:I

    new-instance v2, Lcom/android/settings/FullScreenDisplaySettings$15;

    invoke-direct {v2, p0}, Lcom/android/settings/FullScreenDisplaySettings$15;-><init>(Lcom/android/settings/FullScreenDisplaySettings;)V

    invoke-virtual {v0, v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mAlertDialog:Lmiuix/appcompat/app/AlertDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mAlertDialog:Lmiuix/appcompat/app/AlertDialog;

    new-instance v1, Lcom/android/settings/FullScreenDisplaySettings$17;

    invoke-direct {v1, p0}, Lcom/android/settings/FullScreenDisplaySettings$17;-><init>(Lcom/android/settings/FullScreenDisplaySettings;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    return-void
.end method

.method private initAppGuide()V
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "navigation_guide_app"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mAppGuide:Landroidx/preference/Preference;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/settings/FullScreenDisplaySettings$4;

    invoke-direct {v1, p0}, Lcom/android/settings/FullScreenDisplaySettings$4;-><init>(Lcom/android/settings/FullScreenDisplaySettings;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    :cond_0
    return-void
.end method

.method private initAppQuickSwitchGuide()V
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "navigation_guide_app_quick_switch"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mAppQuickSwitchGuide:Landroidx/preference/Preference;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/settings/FullScreenDisplaySettings$6;

    invoke-direct {v1, p0}, Lcom/android/settings/FullScreenDisplaySettings$6;-><init>(Lcom/android/settings/FullScreenDisplaySettings;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    :cond_0
    return-void
.end method

.method private initAppSwitchFeature()V
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "navigation_appswitch_anim"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mAppSwitchFeature:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/settings/FullScreenDisplaySettings$10;

    invoke-direct {v1, p0}, Lcom/android/settings/FullScreenDisplaySettings$10;-><init>(Lcom/android/settings/FullScreenDisplaySettings;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_0
    return-void
.end method

.method private initAppSwitchGuide()V
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "navigation_guide_appswitch"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mAppSwitchGuide:Landroidx/preference/Preference;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/settings/FullScreenDisplaySettings$5;

    invoke-direct {v1, p0}, Lcom/android/settings/FullScreenDisplaySettings$5;-><init>(Lcom/android/settings/FullScreenDisplaySettings;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    :cond_0
    return-void
.end method

.method private initAutoDisableScreenButtons()V
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    const-string v1, "audo_disable_screen_buttons_settings"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mAutoDisableScreenButtons:Landroidx/preference/Preference;

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->isSupportGestureSettings()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    iget-object p0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mAutoDisableScreenButtons:Landroidx/preference/Preference;

    invoke-virtual {v0, p0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_0
    return-void
.end method

.method private initBackGuide()V
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "navigation_guide_back"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mBackGuide:Landroidx/preference/Preference;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/settings/FullScreenDisplaySettings$3;

    invoke-direct {v1, p0}, Lcom/android/settings/FullScreenDisplaySettings$3;-><init>(Lcom/android/settings/FullScreenDisplaySettings;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    :cond_0
    return-void
.end method

.method private initHideGestureLine()V
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "navigation_hide_gesture_line"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mHideGestureLine:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/settings/FullScreenDisplaySettings$11;

    invoke-direct {v1, p0}, Lcom/android/settings/FullScreenDisplaySettings$11;-><init>(Lcom/android/settings/FullScreenDisplaySettings;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_0
    return-void
.end method

.method private initHomeGuide()V
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "navigation_guide_home"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mHomeGuide:Landroidx/preference/Preference;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/settings/FullScreenDisplaySettings$1;

    invoke-direct {v1, p0}, Lcom/android/settings/FullScreenDisplaySettings$1;-><init>(Lcom/android/settings/FullScreenDisplaySettings;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    :cond_0
    return-void
.end method

.method private initKeyShortcutSettings()V
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    const-string v1, "key_shortcut_settings"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mKeyShortcutSettings:Landroidx/preference/Preference;

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->isSupportGestureSettings()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    iget-object p0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mKeyShortcutSettings:Landroidx/preference/Preference;

    invoke-virtual {v0, p0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_0
    return-void
.end method

.method private initMistakeTouch()V
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    const-string v1, "fsg_mistake_touch"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mMistakeTouch:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/settings/FullScreenDisplaySettings$9;

    invoke-direct {v1, p0}, Lcom/android/settings/FullScreenDisplaySettings$9;-><init>(Lcom/android/settings/FullScreenDisplaySettings;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_0
    return-void
.end method

.method private initRecentGuide()V
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "navigation_guide_recent"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mRecentGuide:Landroidx/preference/Preference;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/settings/FullScreenDisplaySettings$2;

    invoke-direct {v1, p0}, Lcom/android/settings/FullScreenDisplaySettings$2;-><init>(Lcom/android/settings/FullScreenDisplaySettings;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    :cond_0
    return-void
.end method

.method private initScreenButtonHide()V
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "screen_button_hide"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mScreenButtonHide:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/settings/FullScreenDisplaySettings$7;

    invoke-direct {v1, p0}, Lcom/android/settings/FullScreenDisplaySettings$7;-><init>(Lcom/android/settings/FullScreenDisplaySettings;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_0
    return-void
.end method

.method private initSwitchScreenButtonOrder()V
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    const-string/jumbo v1, "switch_screen_button_order"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mSwitchScreenButtonOrder:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/settings/FullScreenDisplaySettings$8;

    invoke-direct {v1, p0}, Lcom/android/settings/FullScreenDisplaySettings$8;-><init>(Lcom/android/settings/FullScreenDisplaySettings;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_0
    return-void
.end method

.method private isAppSwitchFeatureEnable()Z
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "show_gesture_appswitch_feature"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    if-eqz p0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method private isHideGestureLine()Z
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "hide_gesture_line"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    if-eqz p0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method private isMatchDefaultHome(Ljava/lang/String;)Z
    .locals 0

    const-string p0, "com.miui.home"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1

    const-string p0, "com.mi.android.globallauncher"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private isMistakeTouchEnable()Z
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "show_mistake_touch_toast"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private isScreenButtonHidden()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "force_fsg_nav_bar"

    invoke-static {p0, v0}, Landroid/provider/MiuiSettings$Global;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method private isSupportGestureSettings()Z
    .locals 2

    iget-object p0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "show_key_shortcuts_entry_in_full_screen_settings"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    move v1, v0

    :cond_0
    return v1
.end method

.method private isUseFsVersionThree()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mIsRecentsWithinLauncher:Z

    if-eqz v0, :cond_0

    iget-boolean p0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mUseMiuiHomeAsDefaultHome:Z

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private removeHideGesturePreference()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mHideGestureLine:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mAppSwitchFeature:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mGuideCategory:Landroidx/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mAppQuickSwitchGuide:Landroidx/preference/Preference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mGuideCategory:Landroidx/preference/PreferenceCategory;

    iget-object p0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mAppSwitchGuide:Landroidx/preference/Preference;

    invoke-virtual {v0, p0}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    return-void
.end method

.method private setAppSwitchFeatureEnable(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "show_gesture_appswitch_feature"

    invoke-static {p0, v0, p1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method private setHideGestureLine(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "hide_gesture_line"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->updateAppQuickSwitchGuide()V

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->updateGestureLineOfNavBarGuideView()V

    return-void
.end method

.method private setMistakeTouchEnable(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "show_mistake_touch_toast"

    invoke-static {p0, v0, p1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method private setupForceImmersiveHintDialog(Lcom/android/settings/SimpleDialogFragment;)V
    .locals 2

    sget v0, Lcom/android/settings/R$string;->force_immersive_compatibility_dont_hide:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/android/settings/SimpleDialogFragment;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)V

    sget v0, Lcom/android/settings/R$string;->force_immersive_compatibility_hide:I

    new-instance v1, Lcom/android/settings/FullScreenDisplaySettings$13;

    invoke-direct {v1, p0}, Lcom/android/settings/FullScreenDisplaySettings$13;-><init>(Lcom/android/settings/FullScreenDisplaySettings;)V

    invoke-virtual {p1, v0, v1}, Lcom/android/settings/SimpleDialogFragment;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method

.method private showForceImmersiveHintDialog()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mScreenButtonHide:Landroidx/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    new-instance v0, Lcom/android/settings/SimpleDialogFragment$AlertDialogFragmentBuilder;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/android/settings/SimpleDialogFragment$AlertDialogFragmentBuilder;-><init>(I)V

    sget v1, Lcom/android/settings/R$string;->force_immersive_compatibility_hint_title:I

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/SimpleDialogFragment$AlertDialogFragmentBuilder;->setTitle(Ljava/lang/String;)Lcom/android/settings/SimpleDialogFragment$AlertDialogFragmentBuilder;

    move-result-object v0

    sget v1, Lcom/android/settings/R$string;->force_immersive_compatibility_hint_message:I

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/SimpleDialogFragment$AlertDialogFragmentBuilder;->setMessage(Ljava/lang/String;)Lcom/android/settings/SimpleDialogFragment$AlertDialogFragmentBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/SimpleDialogFragment$AlertDialogFragmentBuilder;->create()Lcom/android/settings/SimpleDialogFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/FullScreenDisplaySettings;->setupForceImmersiveHintDialog(Lcom/android/settings/SimpleDialogFragment;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p0

    const-string v1, "fragment_force_immersive_dialog"

    invoke-virtual {v0, p0, v1}, Landroidx/fragment/app/DialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private updateAppQuickSwitchGuide()V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->isHideGestureLine()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mAppQuickSwitchGuide:Landroidx/preference/Preference;

    sget v1, Lcom/android/settings/R$string;->navigation_guide_app_quick_switch_hide_line_summary:I

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setSummary(I)V

    iget-object p0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mAppQuickSwitchGuide:Landroidx/preference/Preference;

    sget v0, Lcom/android/settings/R$drawable;->navigation_bar_guide_new_appswitch_hide_gesture_line:I

    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setIcon(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mAppQuickSwitchGuide:Landroidx/preference/Preference;

    sget v1, Lcom/android/settings/R$string;->navigation_guide_app_quick_switch_summary:I

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setSummary(I)V

    iget-object p0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mAppQuickSwitchGuide:Landroidx/preference/Preference;

    sget v0, Lcom/android/settings/R$drawable;->navigation_bar_guide_new_appswitch:I

    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setIcon(I)V

    :goto_0
    return-void
.end method

.method private updateGestureLineOfNavBarGuideView()V
    .locals 2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    sget v1, Lcom/android/settings/R$id;->navigation_guide:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/view/NavigationBarGuideView;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->isUseFsVersionThree()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->isHideGestureLine()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v0, p0}, Lcom/android/settings/view/NavigationBarGuideView;->setIsShowGestureLine(Z)V

    :cond_1
    return-void
.end method

.method private updateHideGesturePreference()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->isUseFsVersionThree()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->addHideGesturePreference()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->removeHideGesturePreference()V

    :goto_0
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 0

    const-class p0, Lcom/android/settings/FullScreenDisplaySettings;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public getPageIndex()I
    .locals 0

    const/16 p0, 0x3e9

    return p0
.end method

.method isRightHand()Z
    .locals 2

    goto/32 :goto_a

    nop

    :goto_0
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    goto/32 :goto_9

    nop

    :goto_1
    if-gtz v1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_d

    nop

    :goto_2
    const/4 v0, 0x1

    :goto_3
    goto/32 :goto_c

    nop

    :goto_4
    if-nez p0, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_6

    nop

    :goto_5
    check-cast p0, Ljava/lang/Integer;

    goto/32 :goto_0

    nop

    :goto_6
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_1

    nop

    :goto_7
    const/4 v0, 0x0

    goto/32 :goto_4

    nop

    :goto_8
    invoke-static {p0}, Landroid/provider/MiuiSettings$System;->getScreenKeyOrder(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object p0

    goto/32 :goto_7

    nop

    :goto_9
    const/4 v1, 0x2

    goto/32 :goto_b

    nop

    :goto_a
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    goto/32 :goto_8

    nop

    :goto_b
    if-eq p0, v1, :cond_2

    goto/32 :goto_3

    :cond_2
    goto/32 :goto_2

    nop

    :goto_c
    return v0

    :goto_d
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p0

    goto/32 :goto_5

    nop
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mContext:Landroid/content/Context;

    const-string/jumbo p1, "window"

    invoke-static {p1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object p1

    invoke-static {p1}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object p1

    :try_start_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContextCompat;->getDisplayId(Landroid/content/Context;)I

    move-result v0

    invoke-static {p1, v0}, Landroid/view/IWindowManagerCompat;->hasNavigationBar(Landroid/view/IWindowManager;I)Z

    move-result p1

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->finish()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    const-string v0, "fragment_force_immersive_dialog"

    invoke-virtual {p1, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object p1

    check-cast p1, Lcom/android/settings/SimpleDialogFragment;

    invoke-direct {p0, p1}, Lcom/android/settings/FullScreenDisplaySettings;->setupForceImmersiveHintDialog(Lcom/android/settings/SimpleDialogFragment;)V

    :cond_1
    sget p1, Lcom/android/settings/R$xml;->fullscreen_display_settings:I

    invoke-virtual {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->addPreferencesFromResource(I)V

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->initScreenButtonHide()V

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->initSwitchScreenButtonOrder()V

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->initMistakeTouch()V

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->initAppSwitchFeature()V

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->initHideGestureLine()V

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->initHomeGuide()V

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->initRecentGuide()V

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->initBackGuide()V

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->initAppGuide()V

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->initAppSwitchGuide()V

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->initAppQuickSwitchGuide()V

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    const-string/jumbo v0, "navigation_guide_category"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/PreferenceCategory;

    iput-object p1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mGuideCategory:Landroidx/preference/PreferenceCategory;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    const-string/jumbo v0, "navigation_setting_category"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/PreferenceCategory;

    iput-object p1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->initKeyShortcutSettings()V

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->initAutoDisableScreenButtons()V

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->checkDemoExist()Z

    iget-boolean p1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mDemoExistes:Z

    if-nez p1, :cond_2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mGuideCategory:Landroidx/preference/PreferenceCategory;

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_2
    iget-object p1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroidx/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v0, "need_show_gesture_line_guide"

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mNeedShowDialog:Z

    iget-object p1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/utils/Utils;->isRecentsWithinLauncher(Landroid/content/Context;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mIsRecentsWithinLauncher:Z

    iget-object p1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/utils/Utils;->useMiuiHomeAsDefaultHome(Landroid/content/Context;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mUseMiuiHomeAsDefaultHome:Z

    new-instance p1, Lcom/android/settings/FullScreenDisplaySettings$LauncherPackageMonitor;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0}, Lcom/android/settings/FullScreenDisplaySettings$LauncherPackageMonitor;-><init>(Lcom/android/settings/FullScreenDisplaySettings;Lcom/android/settings/FullScreenDisplaySettings$LauncherPackageMonitor-IA;)V

    iput-object p1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mPackageMonitor:Lcom/android/settings/FullScreenDisplaySettings$LauncherPackageMonitor;

    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {p1, v0, v2, v3, v1}, Lcom/android/internal/content/PackageMonitor;->register(Landroid/content/Context;Landroid/os/Looper;Landroid/os/UserHandle;Z)V

    iget-object p1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mContext:Landroid/content/Context;

    iget-object p0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mUserPreferenceChangeReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.ACTION_PREFERRED_ACTIVITY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Lcom/android/settingslib/core/lifecycle/ObservablePreferenceFragment;->onDestroy()V

    :try_start_0
    iget-object p0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mPackageMonitor:Lcom/android/settings/FullScreenDisplaySettings$LauncherPackageMonitor;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/android/internal/content/PackageMonitor;->unregister()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onPause()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    sget v1, Lcom/android/settings/R$id;->navigation_guide:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/view/NavigationBarGuideView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/view/NavigationBarGuideView;->onPause()V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object p0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mScreenButtonHideListener:Landroid/database/ContentObserver;

    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method public onResume()V
    .locals 5

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onResume()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mScreenButtonHide:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->isScreenButtonHidden()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mSwitchScreenButtonOrder:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/FullScreenDisplaySettings;->isRightHand()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mMistakeTouch:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->isMistakeTouchEnable()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mAppSwitchFeature:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->isAppSwitchFeatureEnable()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :cond_4
    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mHideGestureLine:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->isHideGestureLine()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :cond_5
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_6

    sget v1, Lcom/android/settings/R$id;->navigation_guide:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/view/NavigationBarGuideView;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/android/settings/view/NavigationBarGuideView;->onResume()V

    :cond_6
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "force_fsg_nav_bar"

    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/settings/FullScreenDisplaySettings;->mScreenButtonHideListener:Landroid/database/ContentObserver;

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    invoke-virtual {p0}, Lcom/android/settings/FullScreenDisplaySettings;->updatePrefence()V

    return-void
.end method

.method setRightHand(Z)V
    .locals 4

    goto/32 :goto_10

    nop

    :goto_0
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_6

    nop

    :goto_1
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_8

    nop

    :goto_2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/32 :goto_f

    nop

    :goto_3
    return-void

    :goto_4
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_5
    goto/32 :goto_c

    nop

    :goto_6
    const/4 v1, 0x2

    goto/32 :goto_b

    nop

    :goto_7
    const/4 v2, 0x1

    goto/32 :goto_2

    nop

    :goto_8
    goto :goto_5

    :goto_9
    goto/32 :goto_a

    nop

    :goto_a
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_e

    nop

    :goto_b
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_7

    nop

    :goto_c
    invoke-virtual {p0, v0}, Lcom/android/settings/FullScreenDisplaySettings;->setScreenKeyOrder(Ljava/util/List;)V

    goto/32 :goto_3

    nop

    :goto_d
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/32 :goto_13

    nop

    :goto_e
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_4

    nop

    :goto_f
    const/4 v3, 0x3

    goto/32 :goto_d

    nop

    :goto_10
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_0

    nop

    :goto_11
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_12

    nop

    :goto_12
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_1

    nop

    :goto_13
    if-nez p1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_11

    nop
.end method

.method setScreenButtonHidden(Z)V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    invoke-static {p0, p1, v0}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto/32 :goto_6

    nop

    :goto_1
    invoke-static {v0, v1, p1}, Landroid/provider/MiuiSettings$Global;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    goto/32 :goto_7

    nop

    :goto_2
    const-string v0, "immersive.preconfirms=*"

    goto/32 :goto_0

    nop

    :goto_3
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_4
    const-string v1, "force_fsg_nav_bar"

    goto/32 :goto_1

    nop

    :goto_5
    const-string/jumbo p1, "policy_control"

    goto/32 :goto_2

    nop

    :goto_6
    return-void

    :goto_7
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    goto/32 :goto_5

    nop
.end method

.method public setScreenKeyOrder(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0xc

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string/jumbo v0, "screen_key_order"

    invoke-static {p0, v0, p1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method updatePrefence()V
    .locals 2

    goto/32 :goto_11

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_1f

    :cond_0
    goto/32 :goto_26

    nop

    :goto_1
    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    goto/32 :goto_4

    nop

    :goto_2
    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    goto/32 :goto_2c

    nop

    :goto_3
    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    goto/32 :goto_b

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    goto/32 :goto_2e

    nop

    :goto_5
    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    goto/32 :goto_10

    nop

    :goto_6
    iget-object v1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mKeyShortcutSettings:Landroidx/preference/Preference;

    goto/32 :goto_25

    nop

    :goto_7
    invoke-virtual {v0, p0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :goto_8
    goto/32 :goto_e

    nop

    :goto_9
    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->isSupportGestureSettings()Z

    move-result v0

    goto/32 :goto_0

    nop

    :goto_a
    invoke-virtual {v0, p0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    goto/32 :goto_27

    nop

    :goto_b
    iget-object v1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mSwitchScreenButtonOrder:Landroidx/preference/CheckBoxPreference;

    goto/32 :goto_f

    nop

    :goto_c
    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    goto/32 :goto_13

    nop

    :goto_d
    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    goto/32 :goto_14

    nop

    :goto_e
    return-void

    :goto_f
    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    goto/32 :goto_2d

    nop

    :goto_10
    iget-object v1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mGuideCategory:Landroidx/preference/PreferenceCategory;

    goto/32 :goto_29

    nop

    :goto_11
    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->isScreenButtonHidden()Z

    move-result v0

    goto/32 :goto_1a

    nop

    :goto_12
    iget-object v1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mHideGestureLine:Landroidx/preference/CheckBoxPreference;

    goto/32 :goto_1

    nop

    :goto_13
    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    goto/32 :goto_16

    nop

    :goto_14
    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    goto/32 :goto_12

    nop

    :goto_15
    invoke-direct {p0}, Lcom/android/settings/FullScreenDisplaySettings;->updateHideGesturePreference()V

    goto/32 :goto_3

    nop

    :goto_16
    iget-object v1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mAutoDisableScreenButtons:Landroidx/preference/Preference;

    goto/32 :goto_1e

    nop

    :goto_17
    iget-object v1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mKeyShortcutSettings:Landroidx/preference/Preference;

    goto/32 :goto_c

    nop

    :goto_18
    iget-object v1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mSwitchScreenButtonOrder:Landroidx/preference/CheckBoxPreference;

    goto/32 :goto_30

    nop

    :goto_19
    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    goto/32 :goto_1d

    nop

    :goto_1a
    if-nez v0, :cond_1

    goto/32 :goto_28

    :cond_1
    goto/32 :goto_1c

    nop

    :goto_1b
    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    goto/32 :goto_20

    nop

    :goto_1c
    iget-boolean v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mDemoExistes:Z

    goto/32 :goto_2b

    nop

    :goto_1d
    iget-object v1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mGuideCategory:Landroidx/preference/PreferenceCategory;

    goto/32 :goto_2

    nop

    :goto_1e
    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    :goto_1f
    goto/32 :goto_19

    nop

    :goto_20
    iget-object p0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mAutoDisableScreenButtons:Landroidx/preference/Preference;

    goto/32 :goto_a

    nop

    :goto_21
    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    goto/32 :goto_23

    nop

    :goto_22
    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    goto/32 :goto_15

    nop

    :goto_23
    iget-object v1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mMistakeTouch:Landroidx/preference/CheckBoxPreference;

    goto/32 :goto_22

    nop

    :goto_24
    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    goto/32 :goto_2f

    nop

    :goto_25
    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    goto/32 :goto_1b

    nop

    :goto_26
    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    goto/32 :goto_17

    nop

    :goto_27
    goto/16 :goto_8

    :goto_28
    goto/32 :goto_9

    nop

    :goto_29
    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    :goto_2a
    goto/32 :goto_21

    nop

    :goto_2b
    if-nez v0, :cond_2

    goto/32 :goto_2a

    :cond_2
    goto/32 :goto_5

    nop

    :goto_2c
    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    goto/32 :goto_18

    nop

    :goto_2d
    iget-object v0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mSettingCategory:Landroidx/preference/PreferenceCategory;

    goto/32 :goto_6

    nop

    :goto_2e
    iget-object p0, p0, Lcom/android/settings/FullScreenDisplaySettings;->mMistakeTouch:Landroidx/preference/CheckBoxPreference;

    goto/32 :goto_7

    nop

    :goto_2f
    iget-object v1, p0, Lcom/android/settings/FullScreenDisplaySettings;->mAppSwitchFeature:Landroidx/preference/CheckBoxPreference;

    goto/32 :goto_d

    nop

    :goto_30
    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    goto/32 :goto_24

    nop
.end method
