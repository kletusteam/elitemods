.class Lcom/android/settings/display/BrightnessFragment$3;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/display/BrightnessFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/display/BrightnessFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/display/BrightnessFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/BrightnessFragment$3;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment$3;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {v0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmIsVrModeEnabled(Lcom/android/settings/display/BrightnessFragment;)Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment$3;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {v1}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmContext(Lcom/android/settings/display/BrightnessFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getBrightnessInfo()Landroid/hardware/display/BrightnessInfo;

    move-result-object v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/settings/display/BrightnessFragment$3;->this$0:Lcom/android/settings/display/BrightnessFragment;

    iget v3, v1, Landroid/hardware/display/BrightnessInfo;->brightnessMaximum:F

    invoke-static {v2, v3}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fputmMaximumBrightness(Lcom/android/settings/display/BrightnessFragment;F)V

    iget-object v2, p0, Lcom/android/settings/display/BrightnessFragment$3;->this$0:Lcom/android/settings/display/BrightnessFragment;

    iget v3, v1, Landroid/hardware/display/BrightnessInfo;->brightnessMinimum:F

    invoke-static {v2, v3}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fputmMinimumBrightness(Lcom/android/settings/display/BrightnessFragment;F)V

    iget v1, v1, Landroid/hardware/display/BrightnessInfo;->brightness:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment$3;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {p0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmHandler(Lcom/android/settings/display/BrightnessFragment;)Landroid/os/Handler;

    move-result-object p0

    const/4 v2, 0x1

    invoke-virtual {p0, v2, v1, v0}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object p0

    invoke-virtual {p0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method
