.class Lcom/android/settings/display/DarkModeAppsSettingFragment$1$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/settings/display/DarkModeAppsListAdapter$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/display/DarkModeAppsSettingFragment$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/settings/display/DarkModeAppsSettingFragment$1;


# direct methods
.method constructor <init>(Lcom/android/settings/display/DarkModeAppsSettingFragment$1;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment$1$1;->this$1:Lcom/android/settings/display/DarkModeAppsSettingFragment$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Lmiuix/slidingwidget/widget/SlidingButton;I)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment$1$1;->this$1:Lcom/android/settings/display/DarkModeAppsSettingFragment$1;

    iget-object v0, v0, Lcom/android/settings/display/DarkModeAppsSettingFragment$1;->this$0:Lcom/android/settings/display/DarkModeAppsSettingFragment;

    invoke-static {v0}, Lcom/android/settings/display/DarkModeAppsSettingFragment;->-$$Nest$fgetmDarkModeAppsListAdapter(Lcom/android/settings/display/DarkModeAppsSettingFragment;)Lcom/android/settings/display/DarkModeAppsListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/display/DarkModeAppsListAdapter;->getData()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/android/settings/display/DarkModeAppInfo;

    if-eqz p2, :cond_0

    iget-object p0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment$1$1;->this$1:Lcom/android/settings/display/DarkModeAppsSettingFragment$1;

    iget-object p0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment$1;->this$0:Lcom/android/settings/display/DarkModeAppsSettingFragment;

    invoke-virtual {p2}, Lcom/android/settings/display/DarkModeAppInfo;->isEnabled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, p1, v0, p2}, Lcom/android/settings/display/DarkModeAppsSettingFragment;->onAppChecked(Landroid/widget/CompoundButton;ZLcom/android/settings/display/DarkModeAppInfo;)V

    :cond_0
    return-void
.end method
