.class public Lcom/android/settings/display/DarkModeFragment;
.super Lcom/android/settings/SettingsPreferenceFragment;

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;
.implements Landroidx/preference/Preference$OnPreferenceClickListener;
.implements Lmiuix/appcompat/app/TimePickerDialog$OnTimeSetListener;


# instance fields
.field private DARK_MODE_OPEN_BEFORE_TIME_MODE:Ljava/lang/String;

.field private isSetStartTime:Z

.field private mContext:Landroid/content/Context;

.field private mDarkModeAutoTimePref:Lcom/android/settings/display/DarkModePreference;

.field private mDarkModeSunTimeModePref:Lcom/android/settings/display/DarkModePreference;

.field private mDarkModeTimeEnablePref:Landroidx/preference/CheckBoxPreference;

.field private mDarkModeTimeGroup:Landroidx/preference/PreferenceGroup;

.field private mDarkModeTimeRadioGroup:Landroidx/preference/PreferenceGroup;

.field private mEndTime:I

.field private mEndTimePref:Lcom/android/settings/dndmode/LabelPreference;

.field private mHandler:Landroid/os/Handler;

.field private mStartTime:I

.field private mStartTimePref:Lcom/android/settings/dndmode/LabelPreference;

.field private mTimePickerDialog:Lmiuix/appcompat/app/TimePickerDialog;

.field private mToast:Landroid/widget/Toast;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/SettingsPreferenceFragment;-><init>()V

    const-string v0, "dark_mode_open_before_time_mode"

    iput-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->DARK_MODE_OPEN_BEFORE_TIME_MODE:Ljava/lang/String;

    return-void
.end method

.method private findPreferenceImpl(Ljava/lang/String;)Landroidx/preference/Preference;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/preference/Preference;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    invoke-super {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p0

    return-object p0
.end method

.method private formatChooseTime(II)Ljava/lang/String;
    .locals 2

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->set(II)V

    const/16 p1, 0xc

    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {p0, v0, v1, p1}, Lmiuix/pickerwidget/date/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private getActivityImpl()Landroid/app/Activity;
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    return-object p0
.end method

.method private getDarkModeTimeType()I
    .locals 2

    iget-object p0, p0, Lcom/android/settings/display/DarkModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "dark_mode_time_type"

    const/4 v1, 0x2

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    return p0
.end method

.method private initPreference()V
    .locals 3

    const-string v0, "dark_mode_time_group"

    invoke-direct {p0, v0}, Lcom/android/settings/display/DarkModeFragment;->findPreferenceImpl(Ljava/lang/String;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/PreferenceGroup;

    iput-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeTimeGroup:Landroidx/preference/PreferenceGroup;

    const-string v0, "dark_mode_time_radio_group"

    invoke-direct {p0, v0}, Lcom/android/settings/display/DarkModeFragment;->findPreferenceImpl(Ljava/lang/String;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/PreferenceGroup;

    iput-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeTimeRadioGroup:Landroidx/preference/PreferenceGroup;

    const-string v0, "dark_mode_time_enable"

    invoke-direct {p0, v0}, Lcom/android/settings/display/DarkModeFragment;->findPreferenceImpl(Ljava/lang/String;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeTimeEnablePref:Landroidx/preference/CheckBoxPreference;

    const-string v0, "dark_mode_sun_time_mode"

    invoke-direct {p0, v0}, Lcom/android/settings/display/DarkModeFragment;->findPreferenceImpl(Ljava/lang/String;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/display/DarkModePreference;

    iput-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeSunTimeModePref:Lcom/android/settings/display/DarkModePreference;

    const-string v0, "dark_mode_auto_time_mode"

    invoke-direct {p0, v0}, Lcom/android/settings/display/DarkModeFragment;->findPreferenceImpl(Ljava/lang/String;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/display/DarkModePreference;

    iput-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeAutoTimePref:Lcom/android/settings/display/DarkModePreference;

    const-string v0, "dark_mode_start_time"

    invoke-direct {p0, v0}, Lcom/android/settings/display/DarkModeFragment;->findPreferenceImpl(Ljava/lang/String;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dndmode/LabelPreference;

    iput-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mStartTimePref:Lcom/android/settings/dndmode/LabelPreference;

    const-string v0, "dark_mode_end_time"

    invoke-direct {p0, v0}, Lcom/android/settings/display/DarkModeFragment;->findPreferenceImpl(Ljava/lang/String;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dndmode/LabelPreference;

    iput-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mEndTimePref:Lcom/android/settings/dndmode/LabelPreference;

    iget-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeTimeEnablePref:Landroidx/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/android/settings/display/DarkModeFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/settings/display/DarkModeTimeModeUtil;->isDarkModeTimeEnable(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeSunTimeModePref:Lcom/android/settings/display/DarkModePreference;

    iget-object v1, p0, Lcom/android/settings/display/DarkModeFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/settings/display/DarkModeTimeModeUtil;->isSunRiseSunSetMode(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeAutoTimePref:Lcom/android/settings/display/DarkModePreference;

    iget-object v1, p0, Lcom/android/settings/display/DarkModeFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/settings/display/DarkModeTimeModeUtil;->isDarkModeAutoTimeEnable(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mStartTimePref:Lcom/android/settings/dndmode/LabelPreference;

    iget v1, p0, Lcom/android/settings/display/DarkModeFragment;->mStartTime:I

    div-int/lit8 v2, v1, 0x3c

    rem-int/lit8 v1, v1, 0x3c

    invoke-direct {p0, v2, v1}, Lcom/android/settings/display/DarkModeFragment;->formatChooseTime(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/LabelPreference;->setLabel(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mEndTimePref:Lcom/android/settings/dndmode/LabelPreference;

    iget v1, p0, Lcom/android/settings/display/DarkModeFragment;->mEndTime:I

    div-int/lit8 v2, v1, 0x3c

    rem-int/lit8 v1, v1, 0x3c

    invoke-direct {p0, v2, v1}, Lcom/android/settings/display/DarkModeFragment;->formatChooseTime(II)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/android/settings/dndmode/LabelPreference;->setLabel(Ljava/lang/String;)V

    return-void
.end method

.method private initPreferenceListener()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeTimeEnablePref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeSunTimeModePref:Lcom/android/settings/display/DarkModePreference;

    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeAutoTimePref:Lcom/android/settings/display/DarkModePreference;

    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mStartTimePref:Lcom/android/settings/dndmode/LabelPreference;

    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mEndTimePref:Lcom/android/settings/dndmode/LabelPreference;

    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    return-void
.end method

.method private setDarkModeTimeType(I)V
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/display/DarkModeFragment;->getDarkModeTimeType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "open_sun_time_channel"

    const-string/jumbo v2, "\u8bbe\u7f6e"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_0
    iget-object p0, p0, Lcom/android/settings/display/DarkModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "dark_mode_time_type"

    invoke-static {p0, v0, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method private showTimePickerDelay(I)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/android/settings/display/DarkModeFragment$1;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/display/DarkModeFragment$1;-><init>(Lcom/android/settings/display/DarkModeFragment;I)V

    const-wide/16 p0, 0x96

    invoke-virtual {v0, v1, p0, p1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method private showWarnToast()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/android/settings/R$string;->screen_dark_mode_select_time_error_summary:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private updateDarkModeTimeGroup(Ljava/lang/Boolean;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeTimeGroup:Landroidx/preference/PreferenceGroup;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeTimeRadioGroup:Landroidx/preference/PreferenceGroup;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeTimeGroup:Landroidx/preference/PreferenceGroup;

    iget-object p0, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeTimeRadioGroup:Landroidx/preference/PreferenceGroup;

    invoke-virtual {p1, p0}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeTimeGroup:Landroidx/preference/PreferenceGroup;

    const-string v0, "dark_mode_time_radio_group"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeTimeGroup:Landroidx/preference/PreferenceGroup;

    iget-object p0, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeTimeRadioGroup:Landroidx/preference/PreferenceGroup;

    invoke-virtual {p1, p0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_2
    :goto_0
    return-void
.end method

.method private updateDarkModeTimeGroupStatus()V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeSunTimeModePref:Lcom/android/settings/display/DarkModePreference;

    invoke-direct {p0}, Lcom/android/settings/display/DarkModeFragment;->getDarkModeTimeType()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    move v1, v3

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeAutoTimePref:Lcom/android/settings/display/DarkModePreference;

    invoke-direct {p0}, Lcom/android/settings/display/DarkModeFragment;->getDarkModeTimeType()I

    move-result p0

    if-ne p0, v3, :cond_1

    move v2, v3

    :cond_1
    invoke-virtual {v0, v2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    sget p1, Lcom/android/settings/R$xml;->dark_mode_settings:I

    invoke-virtual {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->addPreferencesFromResource(I)V

    invoke-direct {p0}, Lcom/android/settings/display/DarkModeFragment;->getActivityImpl()Landroid/app/Activity;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/DarkModeFragment;->mContext:Landroid/content/Context;

    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lcom/android/settings/display/DarkModeFragment;->mHandler:Landroid/os/Handler;

    iget-object p1, p0, Lcom/android/settings/display/DarkModeFragment;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/display/DarkModeTimeModeUtil;->getDarkModeStartTime(Landroid/content/Context;)I

    move-result p1

    iput p1, p0, Lcom/android/settings/display/DarkModeFragment;->mStartTime:I

    iget-object p1, p0, Lcom/android/settings/display/DarkModeFragment;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/display/DarkModeTimeModeUtil;->getDarkModeEndTime(Landroid/content/Context;)I

    move-result p1

    iput p1, p0, Lcom/android/settings/display/DarkModeFragment;->mEndTime:I

    invoke-direct {p0}, Lcom/android/settings/display/DarkModeFragment;->initPreference()V

    invoke-direct {p0}, Lcom/android/settings/display/DarkModeFragment;->initPreferenceListener()V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Lcom/android/settingslib/core/lifecycle/ObservablePreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onPause()V

    iget-object p0, p0, Lcom/android/settings/display/DarkModeFragment;->mToast:Landroid/widget/Toast;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/widget/Toast;->cancel()V

    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, -0x1

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v0, "dark_mode_sun_time_mode"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    move v4, v1

    goto :goto_0

    :sswitch_1
    const-string v0, "dark_mode_time_enable"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    move v4, v2

    goto :goto_0

    :sswitch_2
    const-string v0, "dark_mode_auto_time_mode"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    goto :goto_0

    :cond_2
    move v4, v3

    :goto_0
    const-string p1, "DarkModeFragment"

    packed-switch v4, :pswitch_data_0

    goto/16 :goto_1

    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mDarkModeSunTimeModePref isCheck = "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeSunTimeModePref:Lcom/android/settings/display/DarkModePreference;

    invoke-virtual {p1, p2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p1, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeAutoTimePref:Lcom/android/settings/display/DarkModePreference;

    invoke-virtual {p1, v3}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    invoke-direct {p0, v1}, Lcom/android/settings/display/DarkModeFragment;->setDarkModeTimeType(I)V

    iget-object p0, p0, Lcom/android/settings/display/DarkModeFragment;->mContext:Landroid/content/Context;

    invoke-static {p0, p2}, Lcom/android/settings/display/DarkModeTimeModeUtil;->setSunRiseSunSetMode(Landroid/content/Context;Z)V

    goto :goto_1

    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mDarkModeTimeEnablePref isCheck = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeTimeEnablePref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/android/settings/display/DarkModeFragment;->updateDarkModeTimeGroup(Ljava/lang/Boolean;)V

    invoke-direct {p0}, Lcom/android/settings/display/DarkModeFragment;->updateDarkModeTimeGroupStatus()V

    invoke-direct {p0}, Lcom/android/settings/display/DarkModeFragment;->getDarkModeTimeType()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/display/DarkModeFragment;->setDarkModeTimeType(I)V

    iget-object p1, p0, Lcom/android/settings/display/DarkModeFragment;->mContext:Landroid/content/Context;

    invoke-static {p1, p2}, Lcom/android/settings/display/DarkModeTimeModeUtil;->setDarkModeTimeEnable(Landroid/content/Context;Z)V

    if-nez p2, :cond_3

    iget-object p0, p0, Lcom/android/settings/display/DarkModeFragment;->mContext:Landroid/content/Context;

    invoke-static {p0}, Lcom/android/settings/display/DarkModeTimeModeUtil;->isDarkModeEnable(Landroid/content/Context;)Z

    move-result p1

    invoke-static {p0, p1, v3}, Lcom/android/settings/display/DarkModeTimeModeUtil;->setDarkModeEnable(Landroid/content/Context;ZZ)V

    goto :goto_1

    :cond_3
    if-eqz p2, :cond_4

    iget-object p0, p0, Lcom/android/settings/display/DarkModeFragment;->mContext:Landroid/content/Context;

    invoke-static {p0, p2}, Lcom/android/settings/display/DarkModeTimeModeUtil;->startDarkModeAutoTime(Landroid/content/Context;Z)V

    goto :goto_1

    :pswitch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mDarkModeAutoTimePref isCheck = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeAutoTimePref:Lcom/android/settings/display/DarkModePreference;

    invoke-virtual {p1, p2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p1, p0, Lcom/android/settings/display/DarkModeFragment;->mDarkModeSunTimeModePref:Lcom/android/settings/display/DarkModePreference;

    invoke-virtual {p1, v3}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    invoke-direct {p0, v2}, Lcom/android/settings/display/DarkModeFragment;->setDarkModeTimeType(I)V

    iget-object p1, p0, Lcom/android/settings/display/DarkModeFragment;->mContext:Landroid/content/Context;

    invoke-static {p1, p2}, Lcom/android/settings/display/DarkModeTimeModeUtil;->setDarkModeAutoTimeEnable(Landroid/content/Context;Z)V

    iget-object p0, p0, Lcom/android/settings/display/DarkModeFragment;->mContext:Landroid/content/Context;

    invoke-static {p0, p2}, Lcom/android/settings/display/DarkModeTimeModeUtil;->startDarkModeAutoTime(Landroid/content/Context;Z)V

    :cond_4
    :goto_1
    return v2

    :sswitch_data_0
    .sparse-switch
        -0x5fed5ee8 -> :sswitch_2
        -0x56ccb7be -> :sswitch_1
        0xd645b6f -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onPreferenceClick(Landroidx/preference/Preference;)Z
    .locals 7

    new-instance v6, Lmiuix/appcompat/app/TimePickerDialog;

    iget-object v1, p0, Lcom/android/settings/display/DarkModeFragment;->mContext:Landroid/content/Context;

    iget v0, p0, Lcom/android/settings/display/DarkModeFragment;->mStartTime:I

    div-int/lit8 v3, v0, 0x3c

    rem-int/lit8 v4, v0, 0x3c

    invoke-static {v1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v5

    move-object v0, v6

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lmiuix/appcompat/app/TimePickerDialog;-><init>(Landroid/content/Context;Lmiuix/appcompat/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    iput-object v6, p0, Lcom/android/settings/display/DarkModeFragment;->mTimePickerDialog:Lmiuix/appcompat/app/TimePickerDialog;

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    const-string v0, "dark_mode_start_time"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    const-string v0, "dark_mode_end_time"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    iput-boolean v1, p0, Lcom/android/settings/display/DarkModeFragment;->isSetStartTime:Z

    iget p1, p0, Lcom/android/settings/display/DarkModeFragment;->mEndTime:I

    invoke-virtual {p0, p1}, Lcom/android/settings/display/DarkModeFragment;->showTimePicker(I)V

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/android/settings/display/DarkModeFragment;->isSetStartTime:Z

    iget p1, p0, Lcom/android/settings/display/DarkModeFragment;->mStartTime:I

    invoke-virtual {p0, p1}, Lcom/android/settings/display/DarkModeFragment;->showTimePicker(I)V

    :goto_0
    return v1
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onResume()V

    return-void
.end method

.method public onTimeSet(Lmiuix/pickerwidget/widget/TimePicker;II)V
    .locals 1

    mul-int/lit8 p1, p2, 0x3c

    add-int/2addr p1, p3

    iget-boolean v0, p0, Lcom/android/settings/display/DarkModeFragment;->isSetStartTime:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/settings/display/DarkModeFragment;->mEndTime:I

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/display/DarkModeFragment;->showWarnToast()V

    iget p1, p0, Lcom/android/settings/display/DarkModeFragment;->mStartTime:I

    invoke-direct {p0, p1}, Lcom/android/settings/display/DarkModeFragment;->showTimePickerDelay(I)V

    return-void

    :cond_0
    iput p1, p0, Lcom/android/settings/display/DarkModeFragment;->mStartTime:I

    iget-object p1, p0, Lcom/android/settings/display/DarkModeFragment;->mStartTimePref:Lcom/android/settings/dndmode/LabelPreference;

    invoke-direct {p0, p2, p3}, Lcom/android/settings/display/DarkModeFragment;->formatChooseTime(II)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/android/settings/dndmode/LabelPreference;->setLabel(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/settings/display/DarkModeFragment;->mContext:Landroid/content/Context;

    iget p2, p0, Lcom/android/settings/display/DarkModeFragment;->mStartTime:I

    invoke-static {p1, p2}, Lcom/android/settings/display/DarkModeTimeModeUtil;->setDarkModeStartTime(Landroid/content/Context;I)V

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/android/settings/display/DarkModeFragment;->mStartTime:I

    if-ne p1, v0, :cond_2

    invoke-direct {p0}, Lcom/android/settings/display/DarkModeFragment;->showWarnToast()V

    iget p1, p0, Lcom/android/settings/display/DarkModeFragment;->mEndTime:I

    invoke-direct {p0, p1}, Lcom/android/settings/display/DarkModeFragment;->showTimePickerDelay(I)V

    return-void

    :cond_2
    iput p1, p0, Lcom/android/settings/display/DarkModeFragment;->mEndTime:I

    iget-object p1, p0, Lcom/android/settings/display/DarkModeFragment;->mEndTimePref:Lcom/android/settings/dndmode/LabelPreference;

    invoke-direct {p0, p2, p3}, Lcom/android/settings/display/DarkModeFragment;->formatChooseTime(II)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/android/settings/dndmode/LabelPreference;->setLabel(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/settings/display/DarkModeFragment;->mContext:Landroid/content/Context;

    iget p2, p0, Lcom/android/settings/display/DarkModeFragment;->mEndTime:I

    invoke-static {p1, p2}, Lcom/android/settings/display/DarkModeTimeModeUtil;->setDarkModeEndTime(Landroid/content/Context;I)V

    :goto_0
    iget-object p1, p0, Lcom/android/settings/display/DarkModeFragment;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/display/DarkModeTimeModeUtil;->isDarkModeTimeEnable(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p0, p0, Lcom/android/settings/display/DarkModeFragment;->mContext:Landroid/content/Context;

    const/4 p1, 0x1

    invoke-static {p0, p1}, Lcom/android/settings/display/DarkModeTimeModeUtil;->startDarkModeAutoTime(Landroid/content/Context;Z)V

    :cond_3
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/android/settings/SettingsPreferenceFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iget-object p1, p0, Lcom/android/settings/display/DarkModeFragment;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/display/DarkModeTimeModeUtil;->isDarkModeTimeEnable(Landroid/content/Context;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/android/settings/display/DarkModeFragment;->updateDarkModeTimeGroup(Ljava/lang/Boolean;)V

    invoke-direct {p0}, Lcom/android/settings/display/DarkModeFragment;->updateDarkModeTimeGroupStatus()V

    return-void
.end method

.method public showTimePicker(I)V
    .locals 2

    if-lez p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/DarkModeFragment;->mTimePickerDialog:Lmiuix/appcompat/app/TimePickerDialog;

    div-int/lit8 v1, p1, 0x3c

    rem-int/lit8 p1, p1, 0x3c

    invoke-virtual {v0, v1, p1}, Lmiuix/appcompat/app/TimePickerDialog;->updateTime(II)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/android/settings/display/DarkModeFragment;->mTimePickerDialog:Lmiuix/appcompat/app/TimePickerDialog;

    const/4 v0, 0x0

    invoke-virtual {p1, v0, v0}, Lmiuix/appcompat/app/TimePickerDialog;->updateTime(II)V

    :goto_0
    iget-object p1, p0, Lcom/android/settings/display/DarkModeFragment;->mTimePickerDialog:Lmiuix/appcompat/app/TimePickerDialog;

    iget-boolean v0, p0, Lcom/android/settings/display/DarkModeFragment;->isSetStartTime:Z

    if-eqz v0, :cond_1

    sget v0, Lcom/android/settings/R$string;->paper_mode_start_time_title:I

    goto :goto_1

    :cond_1
    sget v0, Lcom/android/settings/R$string;->paper_mode_end_time_title:I

    :goto_1
    invoke-virtual {p1, v0}, Landroidx/appcompat/app/AppCompatDialog;->setTitle(I)V

    iget-object p0, p0, Lcom/android/settings/display/DarkModeFragment;->mTimePickerDialog:Lmiuix/appcompat/app/TimePickerDialog;

    invoke-virtual {p0}, Landroid/app/Dialog;->show()V

    return-void
.end method
