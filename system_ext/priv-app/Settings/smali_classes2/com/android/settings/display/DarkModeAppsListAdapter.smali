.class public Lcom/android/settings/display/DarkModeAppsListAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/display/DarkModeAppsListAdapter$ItemViewHolder;,
        Lcom/android/settings/display/DarkModeAppsListAdapter$HeaderViewHolder;,
        Lcom/android/settings/display/DarkModeAppsListAdapter$OnAppCheckedListener;,
        Lcom/android/settings/display/DarkModeAppsListAdapter$OnItemClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private mAppInfo:Landroid/content/pm/ApplicationInfo;

.field private mContext:Landroid/content/Context;

.field mData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/display/DarkModeAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mHeaderCount:I

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mSearchInput:Ljava/lang/String;

.field private onAppCheckedListener:Lcom/android/settings/display/DarkModeAppsListAdapter$OnAppCheckedListener;

.field private onItemClickListener:Lcom/android/settings/display/DarkModeAppsListAdapter$OnItemClickListener;

.field userId:I


# direct methods
.method static bridge synthetic -$$Nest$fgetonAppCheckedListener(Lcom/android/settings/display/DarkModeAppsListAdapter;)Lcom/android/settings/display/DarkModeAppsListAdapter$OnAppCheckedListener;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->onAppCheckedListener:Lcom/android/settings/display/DarkModeAppsListAdapter$OnAppCheckedListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetonItemClickListener(Lcom/android/settings/display/DarkModeAppsListAdapter;)Lcom/android/settings/display/DarkModeAppsListAdapter$OnItemClickListener;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->onItemClickListener:Lcom/android/settings/display/DarkModeAppsListAdapter$OnItemClickListener;

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Lcom/android/settings/display/DarkModeAppsListAdapter$OnAppCheckedListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/android/settings/display/DarkModeAppInfo;",
            ">;",
            "Lcom/android/settings/display/DarkModeAppsListAdapter$OnAppCheckedListener;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    invoke-static {}, Lmiui/os/UserHandle;->myUserId()I

    move-result v0

    iput v0, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->userId:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->mHeaderCount:I

    iput-object p1, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->onAppCheckedListener:Lcom/android/settings/display/DarkModeAppsListAdapter$OnAppCheckedListener;

    invoke-direct {p0, p2}, Lcom/android/settings/display/DarkModeAppsListAdapter;->updateData(Ljava/util/List;)V

    const-string/jumbo p0, "uimode"

    invoke-static {p0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    return-void
.end method

.method private setLabelTextView(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result p3

    add-int/2addr p3, v0

    invoke-virtual {p2, v0, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p3

    iget-object p0, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->mContext:Landroid/content/Context;

    sget v0, Lcom/android/settings/R$string;->search_input_txt_na:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p3, v0, v1

    invoke-static {p0, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, p3, p0}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private updateData(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/display/DarkModeAppInfo;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->mData:Ljava/util/List;

    if-eqz p1, :cond_0

    new-instance v0, Lcom/android/settings/display/DarkModeAppsListAdapter$3;

    invoke-direct {v0, p0}, Lcom/android/settings/display/DarkModeAppsListAdapter$3;-><init>(Lcom/android/settings/display/DarkModeAppsListAdapter;)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public getContentItemCount()I
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->mData:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    return p0
.end method

.method public getData()Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/android/settings/display/DarkModeAppInfo;",
            ">;"
        }
    .end annotation

    iget-object p0, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->mData:Ljava/util/List;

    return-object p0
.end method

.method public getItemCount()I
    .locals 1

    iget v0, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->mHeaderCount:I

    invoke-virtual {p0}, Lcom/android/settings/display/DarkModeAppsListAdapter;->getContentItemCount()I

    move-result p0

    add-int/2addr v0, p0

    return v0
.end method

.method public getItemId(I)J
    .locals 0

    int-to-long p0, p1

    return-wide p0
.end method

.method public getItemViewType(I)I
    .locals 0

    if-nez p1, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    const/4 p0, 0x1

    return p0
.end method

.method public onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 6

    instance-of v0, p1, Lcom/android/settings/display/DarkModeAppsListAdapter$HeaderViewHolder;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->mData:Ljava/util/List;

    if-nez v0, :cond_0

    return-void

    :cond_0
    move-object v1, p1

    check-cast v1, Lcom/android/settings/display/DarkModeAppsListAdapter$HeaderViewHolder;

    iget-object v2, v1, Lcom/android/settings/display/DarkModeAppsListAdapter$HeaderViewHolder;->mAppListTitleLine:Landroid/view/View;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/android/settings/display/DarkModeAppsListAdapter$HeaderViewHolder;->mAppListTitle:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, v1, Lcom/android/settings/display/DarkModeAppsListAdapter$HeaderViewHolder;->mAppListTitleLine:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, v1, Lcom/android/settings/display/DarkModeAppsListAdapter$HeaderViewHolder;->mAppListTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v0, v1, Lcom/android/settings/display/DarkModeAppsListAdapter$HeaderViewHolder;->mAppListTitleLine:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, v1, Lcom/android/settings/display/DarkModeAppsListAdapter$HeaderViewHolder;->mAppListTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2
    :goto_0
    instance-of v0, p1, Lcom/android/settings/display/DarkModeAppsListAdapter$ItemViewHolder;

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->mHeaderCount:I

    sub-int/2addr p2, v0

    iget-object v0, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->mData:Ljava/util/List;

    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p2, v0, :cond_3

    goto :goto_1

    :cond_3
    check-cast p1, Lcom/android/settings/display/DarkModeAppsListAdapter$ItemViewHolder;

    iget-object v0, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->onItemClickListener:Lcom/android/settings/display/DarkModeAppsListAdapter$OnItemClickListener;

    if-eqz v0, :cond_4

    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    new-instance v1, Lcom/android/settings/display/DarkModeAppsListAdapter$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/android/settings/display/DarkModeAppsListAdapter$1;-><init>(Lcom/android/settings/display/DarkModeAppsListAdapter;Lcom/android/settings/display/DarkModeAppsListAdapter$ItemViewHolder;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_4
    iget-object v0, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->mData:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/android/settings/display/DarkModeAppInfo;

    invoke-virtual {p2}, Lcom/android/settings/display/DarkModeAppInfo;->getPkgName()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/display/util/DarkModeAppCacheManager;->getInstance(Landroid/content/Context;)Lcom/android/settings/display/util/DarkModeAppCacheManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->mContext:Landroid/content/Context;

    iget v3, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->userId:I

    iget-object v4, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->mAppInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v5, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/display/util/DarkModeAppCacheManager;->loadAppIcon(Landroid/content/Context;Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p1, Lcom/android/settings/display/DarkModeAppsListAdapter$ItemViewHolder;->icon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p1, Lcom/android/settings/display/DarkModeAppsListAdapter$ItemViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/android/settings/display/DarkModeAppInfo;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p1, Lcom/android/settings/display/DarkModeAppsListAdapter$ItemViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/android/settings/display/DarkModeAppInfo;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->mSearchInput:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/android/settings/display/DarkModeAppsListAdapter;->setLabelTextView(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p1, Lcom/android/settings/display/DarkModeAppsListAdapter$ItemViewHolder;->sliding_button:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {p2}, Lcom/android/settings/display/DarkModeAppInfo;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    iget-object p1, p1, Lcom/android/settings/display/DarkModeAppsListAdapter$ItemViewHolder;->sliding_button:Lmiuix/slidingwidget/widget/SlidingButton;

    new-instance v0, Lcom/android/settings/display/DarkModeAppsListAdapter$2;

    invoke-direct {v0, p0, p2}, Lcom/android/settings/display/DarkModeAppsListAdapter$2;-><init>(Lcom/android/settings/display/DarkModeAppsListAdapter;Lcom/android/settings/display/DarkModeAppInfo;)V

    invoke-virtual {p1, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setOnPerformCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    nop

    :cond_5
    :goto_1
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 1

    const/4 v0, 0x0

    if-nez p2, :cond_0

    iget-object p0, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->mContext:Landroid/content/Context;

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p0

    sget p2, Lcom/android/settings/R$layout;->dark_mode_apps_header:I

    invoke-virtual {p0, p2, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    new-instance p1, Lcom/android/settings/display/DarkModeAppsListAdapter$HeaderViewHolder;

    invoke-direct {p1, p0}, Lcom/android/settings/display/DarkModeAppsListAdapter$HeaderViewHolder;-><init>(Landroid/view/View;)V

    return-object p1

    :cond_0
    iget-object p0, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->mContext:Landroid/content/Context;

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p0

    sget p2, Lcom/android/settings/R$layout;->listitem_darkmode_app:I

    invoke-virtual {p0, p2, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    new-instance p1, Lcom/android/settings/display/DarkModeAppsListAdapter$ItemViewHolder;

    invoke-direct {p1, p0}, Lcom/android/settings/display/DarkModeAppsListAdapter$ItemViewHolder;-><init>(Landroid/view/View;)V

    return-object p1
.end method

.method public refreshAppList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/display/DarkModeAppInfo;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/android/settings/display/DarkModeAppsListAdapter;->updateData(Ljava/util/List;)V

    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.method setAppDarkMode(Lcom/android/settings/display/DarkModeAppInfo;Z)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p1, p2}, Lcom/android/settings/display/DarkModeAppInfo;->setEnabled(Z)V

    goto/32 :goto_2

    nop

    :goto_1
    invoke-static {p0}, Lcom/android/settings/display/util/DarkModeAppCacheManager;->getInstance(Landroid/content/Context;)Lcom/android/settings/display/util/DarkModeAppCacheManager;

    move-result-object p0

    goto/32 :goto_3

    nop

    :goto_2
    iget-object p0, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->mContext:Landroid/content/Context;

    goto/32 :goto_1

    nop

    :goto_3
    invoke-virtual {p1}, Lcom/android/settings/display/DarkModeAppInfo;->getPkgName()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_4

    nop

    :goto_4
    invoke-virtual {p0, p1, p2}, Lcom/android/settings/display/util/DarkModeAppCacheManager;->setAppDarkMode(Ljava/lang/String;Z)V

    goto/32 :goto_5

    nop

    :goto_5
    return-void
.end method

.method public setOnItemClickListener(Lcom/android/settings/display/DarkModeAppsListAdapter$OnItemClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/DarkModeAppsListAdapter;->onItemClickListener:Lcom/android/settings/display/DarkModeAppsListAdapter$OnItemClickListener;

    return-void
.end method
