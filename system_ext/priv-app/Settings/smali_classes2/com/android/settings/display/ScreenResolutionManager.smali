.class public Lcom/android/settings/display/ScreenResolutionManager;
.super Lcom/android/settings/SettingsPreferenceFragment;

# interfaces
.implements Lmiuix/visual/check/VisualCheckGroup$OnCheckedChangeListener;
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private mDisplay:Landroid/view/Display;

.field private mFHDHeight:I

.field private mFHDWidth:I

.field private mInitalDensity:I

.field private mInitalPoint:Landroid/graphics/Point;

.field private mQHDHeight:I

.field private mQHDWidth:I

.field mSaveBatteryCategory:Landroidx/preference/PreferenceCategory;

.field mSaveBatteryMode:Landroidx/preference/CheckBoxPreference;

.field private mScreenResolutionSwitching:Z

.field mSelectResolution:Lcom/android/settings/display/ScreenResolutionPreference;

.field private mWindowManager:Landroid/view/IWindowManager;


# direct methods
.method public static synthetic $r8$lambda$1r4RiuCmz8codr6oAwHsoBYWNCE(Lcom/android/settings/display/ScreenResolutionManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/display/ScreenResolutionManager;->lambda$onResume$0()V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/SettingsPreferenceFragment;-><init>()V

    const/16 v0, 0x438

    iput v0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mFHDWidth:I

    const/16 v0, 0x960

    iput v0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mFHDHeight:I

    const/16 v0, 0x5a0

    iput v0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mQHDWidth:I

    const/16 v0, 0xc80

    iput v0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mQHDHeight:I

    return-void
.end method

.method private calculateHeightFromWidth(I)I
    .locals 2

    iget-object p0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mDisplay:Landroid/view/Display;

    invoke-virtual {p0}, Landroid/view/Display;->getMode()Landroid/view/Display$Mode;

    move-result-object p0

    invoke-virtual {p0}, Landroid/view/Display$Mode;->getPhysicalHeight()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    mul-float/2addr v0, v1

    invoke-virtual {p0}, Landroid/view/Display$Mode;->getPhysicalWidth()I

    move-result p0

    int-to-float p0, p0

    div-float/2addr v0, p0

    int-to-float p0, p1

    mul-float/2addr v0, p0

    float-to-int p0, v0

    return p0
.end method

.method private initSupportSolution()V
    .locals 3

    const-string/jumbo v0, "screen_resolution_supported"

    invoke-static {v0}, Lmiui/util/FeatureParser;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    const/4 v1, 0x0

    aget v1, v0, v1

    iput v1, p0, Lcom/android/settings/display/ScreenResolutionManager;->mQHDWidth:I

    invoke-direct {p0, v1}, Lcom/android/settings/display/ScreenResolutionManager;->calculateHeightFromWidth(I)I

    move-result v1

    iput v1, p0, Lcom/android/settings/display/ScreenResolutionManager;->mQHDHeight:I

    aget v0, v0, v2

    iput v0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mFHDWidth:I

    invoke-direct {p0, v0}, Lcom/android/settings/display/ScreenResolutionManager;->calculateHeightFromWidth(I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mFHDHeight:I

    :cond_0
    return-void
.end method

.method private isCompatMode()Z
    .locals 2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "miui_screen_compat"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    if-ne p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private isQhdMode()Z
    .locals 3

    const-string/jumbo v0, "persist.sys.miui_resolution"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const-string v2, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    aget-object v0, v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iget p0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mFHDWidth:I

    if-ne v0, p0, :cond_0

    move v1, v2

    :cond_0
    return v1
.end method

.method private synthetic lambda$onResume$0()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->finishFragment()V

    return-void
.end method

.method private switchResolution(I)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/android/settings/display/ScreenResolutionManager;->calculateHeightFromWidth(I)I

    move-result v0

    iget v1, p0, Lcom/android/settings/display/ScreenResolutionManager;->mInitalDensity:I

    mul-int/2addr v1, p1

    int-to-float v1, v1

    const/high16 v2, 0x3f800000    # 1.0f

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/android/settings/display/ScreenResolutionManager;->mInitalPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    const/4 v2, 0x0

    invoke-direct {p0, v2, p1, v0, v1}, Lcom/android/settings/display/ScreenResolutionManager;->switchResolution(IIII)V

    return-void
.end method

.method private switchResolution(IIII)V
    .locals 6

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    :try_start_0
    const-string v2, "android.view.IWindowManager"

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    const/16 v2, 0xff

    const-string v3, "ScreenResolutionManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "switchResolution [ displayId:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ", width:"

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ", height:"

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ", density:"

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "]"

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v3, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mWindowManager:Landroid/view/IWindowManager;

    invoke-interface {p0}, Landroid/view/IWindowManager;->asBinder()Landroid/os/IBinder;

    move-result-object p0

    const/4 p1, 0x0

    invoke-interface {p0, v2, v0, v1, p1}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw p0

    :catch_0
    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Lmiuix/visual/check/VisualCheckGroup;I)V
    .locals 1

    iget-boolean p1, p0, Lcom/android/settings/display/ScreenResolutionManager;->mScreenResolutionSwitching:Z

    if-nez p1, :cond_2

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/android/settings/display/ScreenResolutionManager;->mScreenResolutionSwitching:Z

    iget-object p1, p0, Lcom/android/settings/display/ScreenResolutionManager;->mSelectResolution:Lcom/android/settings/display/ScreenResolutionPreference;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/android/settings/display/ScreenResolutionPreference;->setSwitchEnabled(Z)V

    sget p1, Lcom/android/settings/R$id;->resolution_fhd:I

    if-ne p2, p1, :cond_0

    iget p1, p0, Lcom/android/settings/display/ScreenResolutionManager;->mFHDWidth:I

    invoke-direct {p0, p1}, Lcom/android/settings/display/ScreenResolutionManager;->switchResolution(I)V

    goto :goto_0

    :cond_0
    sget p1, Lcom/android/settings/R$id;->resolution_qhd:I

    if-ne p2, p1, :cond_1

    iget p1, p0, Lcom/android/settings/display/ScreenResolutionManager;->mQHDWidth:I

    invoke-direct {p0, p1}, Lcom/android/settings/display/ScreenResolutionManager;->switchResolution(I)V

    goto :goto_0

    :cond_1
    const-string p0, "ScreenResolutionManager"

    const-string p1, "Switch resolution error."

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "ScreenResolutionManager"

    const-string v1, "Settings onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget v1, Lcom/android/settings/R$xml;->full_screen_resolution_settings:I

    invoke-virtual {p0, v1}, Lcom/android/settings/SettingsPreferenceFragment;->addPreferencesFromResource(I)V

    if-eqz p1, :cond_0

    const-string/jumbo v1, "screen_resolution_switching"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/settings/display/ScreenResolutionManager;->mScreenResolutionSwitching:Z

    :cond_0
    invoke-static {}, Landroid/hardware/display/DisplayManagerGlobal;->getInstance()Landroid/hardware/display/DisplayManagerGlobal;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/hardware/display/DisplayManagerGlobal;->getRealDisplay(I)Landroid/view/Display;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/ScreenResolutionManager;->mDisplay:Landroid/view/Display;

    const-string/jumbo p1, "window"

    invoke-static {p1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object p1

    invoke-static {p1}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/ScreenResolutionManager;->mWindowManager:Landroid/view/IWindowManager;

    new-instance p1, Landroid/graphics/Point;

    invoke-direct {p1}, Landroid/graphics/Point;-><init>()V

    iput-object p1, p0, Lcom/android/settings/display/ScreenResolutionManager;->mInitalPoint:Landroid/graphics/Point;

    :try_start_0
    iget-object p1, p0, Lcom/android/settings/display/ScreenResolutionManager;->mWindowManager:Landroid/view/IWindowManager;

    invoke-interface {p1, v1}, Landroid/view/IWindowManager;->getInitialDisplayDensity(I)I

    move-result p1

    iput p1, p0, Lcom/android/settings/display/ScreenResolutionManager;->mInitalDensity:I

    iget-object p1, p0, Lcom/android/settings/display/ScreenResolutionManager;->mWindowManager:Landroid/view/IWindowManager;

    iget-object v2, p0, Lcom/android/settings/display/ScreenResolutionManager;->mInitalPoint:Landroid/graphics/Point;

    invoke-interface {p1, v1, v2}, Landroid/view/IWindowManager;->getInitialDisplaySize(ILandroid/graphics/Point;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v2, "Exception: "

    invoke-static {v0, v2, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    invoke-direct {p0}, Lcom/android/settings/display/ScreenResolutionManager;->initSupportSolution()V

    const-string p1, "full_screen_resolution_selection"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/display/ScreenResolutionPreference;

    iput-object p1, p0, Lcom/android/settings/display/ScreenResolutionManager;->mSelectResolution:Lcom/android/settings/display/ScreenResolutionPreference;

    invoke-direct {p0}, Lcom/android/settings/display/ScreenResolutionManager;->isQhdMode()Z

    move-result p1

    iget-object v0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mSelectResolution:Lcom/android/settings/display/ScreenResolutionPreference;

    iget v2, p0, Lcom/android/settings/display/ScreenResolutionManager;->mQHDWidth:I

    iget v3, p0, Lcom/android/settings/display/ScreenResolutionManager;->mQHDHeight:I

    invoke-virtual {v0, v2, v3}, Lcom/android/settings/display/ScreenResolutionPreference;->setQHDSolution(II)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mSelectResolution:Lcom/android/settings/display/ScreenResolutionPreference;

    iget v2, p0, Lcom/android/settings/display/ScreenResolutionManager;->mFHDWidth:I

    iget v3, p0, Lcom/android/settings/display/ScreenResolutionManager;->mFHDHeight:I

    invoke-virtual {v0, v2, v3}, Lcom/android/settings/display/ScreenResolutionPreference;->setFHDSolution(II)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mSelectResolution:Lcom/android/settings/display/ScreenResolutionPreference;

    invoke-virtual {v0, p1}, Lcom/android/settings/display/ScreenResolutionPreference;->setQhdChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mSelectResolution:Lcom/android/settings/display/ScreenResolutionPreference;

    sget v2, Lcom/android/settings/R$drawable;->qhd_image:I

    invoke-virtual {v0, v2}, Lcom/android/settings/display/ScreenResolutionPreference;->setQhdImage(I)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mSelectResolution:Lcom/android/settings/display/ScreenResolutionPreference;

    sget v2, Lcom/android/settings/R$drawable;->fhd_image:I

    invoke-virtual {v0, v2}, Lcom/android/settings/display/ScreenResolutionPreference;->setFhdImage(I)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mSelectResolution:Lcom/android/settings/display/ScreenResolutionPreference;

    sget v2, Lcom/android/settings/R$string;->resolution_qhd:I

    invoke-virtual {v0, v2}, Lcom/android/settings/display/ScreenResolutionPreference;->setQhdText(I)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mSelectResolution:Lcom/android/settings/display/ScreenResolutionPreference;

    sget v2, Lcom/android/settings/R$string;->resolution_fhd:I

    invoke-virtual {v0, v2}, Lcom/android/settings/display/ScreenResolutionPreference;->setFhdText(I)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mSelectResolution:Lcom/android/settings/display/ScreenResolutionPreference;

    sget v2, Lcom/android/settings/R$string;->resolution_qhd_summary:I

    invoke-virtual {v0, v2}, Lcom/android/settings/display/ScreenResolutionPreference;->setQhdTextSummary(I)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mSelectResolution:Lcom/android/settings/display/ScreenResolutionPreference;

    sget v2, Lcom/android/settings/R$string;->resolution_fhd_summary:I

    invoke-virtual {v0, v2}, Lcom/android/settings/display/ScreenResolutionPreference;->setFhdTextSummary(I)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mSelectResolution:Lcom/android/settings/display/ScreenResolutionPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/display/ScreenResolutionPreference;->setOnCheckedChangeListener(Lmiuix/visual/check/VisualCheckGroup$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mSelectResolution:Lcom/android/settings/display/ScreenResolutionPreference;

    iget-boolean v2, p0, Lcom/android/settings/display/ScreenResolutionManager;->mScreenResolutionSwitching:Z

    xor-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Lcom/android/settings/display/ScreenResolutionPreference;->setSwitchEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mSelectResolution:Lcom/android/settings/display/ScreenResolutionPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setEnabled(Z)V

    const-string/jumbo v0, "save_battery_mode_category"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mSaveBatteryCategory:Landroidx/preference/PreferenceCategory;

    const-string/jumbo v0, "save_battery_mode"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mSaveBatteryMode:Landroidx/preference/CheckBoxPreference;

    invoke-direct {p0}, Lcom/android/settings/display/ScreenResolutionManager;->isCompatMode()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mSaveBatteryMode:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    if-nez p1, :cond_1

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object p0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mSaveBatteryCategory:Landroidx/preference/PreferenceCategory;

    invoke-virtual {p1, p0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_1
    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 1

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const/4 p2, 0x1

    const-string/jumbo v0, "miui_screen_compat"

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    invoke-static {p0, v0, p2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const/4 p1, 0x0

    invoke-static {p0, v0, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :goto_0
    return p2
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onResume()V

    invoke-virtual {p0}, Lcom/android/settingslib/miuisettings/preference/PreferenceFragment;->getAppCompatActionBar()Lmiuix/appcompat/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settingslib/miuisettings/preference/PreferenceFragment;->getAppCompatActionBar()Lmiuix/appcompat/app/ActionBar;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$string;->screen_resolution_title:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/settingslib/miuisettings/preference/PreferenceFragment;->getAppCompatActionBar()Lmiuix/appcompat/app/ActionBar;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$string;->resolution_sub_title:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isInMultiWindowMode()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->finishFragment()V

    :cond_1
    iget-boolean v0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mScreenResolutionSwitching:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mScreenResolutionSwitching:Z

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/android/settings/display/ScreenResolutionManager$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/settings/display/ScreenResolutionManager$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/display/ScreenResolutionManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_2
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-boolean p0, p0, Lcom/android/settings/display/ScreenResolutionManager;->mScreenResolutionSwitching:Z

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    const-string/jumbo v0, "screen_resolution_switching"

    invoke-virtual {p1, v0, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method
