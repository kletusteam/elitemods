.class public Lcom/android/settings/display/PaperModeFragment;
.super Lcom/android/settings/SettingsPreferenceFragment;

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;
.implements Landroidx/preference/Preference$OnPreferenceClickListener;


# static fields
.field private static final PAPER_MODE_MAX_LEVEL:I

.field private static final PAPER_MODE_MIN_LEVEL:F

.field private static final PER_LEVEL:F

.field private static mEndTime:I

.field private static mStartTime:I


# instance fields
.field private classicPreference:Lcom/android/settings/display/PaperRadioButtonPreference;

.field dialogListener:Landroid/content/DialogInterface$OnClickListener;

.field private isSupportHDRMode:Z

.field private mAutoAdjustPreference:Landroidx/preference/CheckBoxPreference;

.field private mAutoTwilightPref:Lcom/android/settings/display/PaperModePreference;

.field private mContext:Landroid/content/Context;

.field private mCustomizedTimePref:Lcom/android/settings/display/PaperModePreference;

.field mLocationGetDialog:Lmiuix/appcompat/app/AlertDialog;

.field private mOnTimeSetListener:Lmiuix/appcompat/app/TimePickerDialog$OnTimeSetListener;

.field private mPaperEffectGroup:Landroidx/preference/PreferenceGroup;

.field private mPaperModeCustomizeTimeGroup:Landroidx/preference/PreferenceGroup;

.field private mPaperModeEnableObserver:Landroid/database/ContentObserver;

.field private mPaperModeScheduleObserver:Landroid/database/ContentObserver;

.field private mPaperModeTimeGroup:Landroidx/preference/PreferenceGroup;

.field private mPaperModeTimeRadioGroup:Landroidx/preference/PreferenceGroup;

.field private mTimeFlag:Z

.field private mTimePickerDialog:Lmiuix/appcompat/app/TimePickerDialog;

.field private mTimeZoneOffset:I

.field private paperModeEnable:Landroidx/preference/CheckBoxPreference;

.field private paperModeEndTime:Lcom/android/settings/dndmode/LabelPreference;

.field private paperModeStartTime:Lcom/android/settings/dndmode/LabelPreference;

.field private paperModeTimeEnable:Landroidx/preference/CheckBoxPreference;

.field private paperPreference:Lcom/android/settings/display/PaperRadioButtonPreference;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/settings/display/PaperModeFragment;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTimeFlag(Lcom/android/settings/display/PaperModeFragment;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/display/PaperModeFragment;->mTimeFlag:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetpaperModeEnable(Lcom/android/settings/display/PaperModeFragment;)Landroidx/preference/CheckBoxPreference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/PaperModeFragment;->paperModeEnable:Landroidx/preference/CheckBoxPreference;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetpaperModeEndTime(Lcom/android/settings/display/PaperModeFragment;)Lcom/android/settings/dndmode/LabelPreference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/PaperModeFragment;->paperModeEndTime:Lcom/android/settings/dndmode/LabelPreference;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetpaperModeStartTime(Lcom/android/settings/display/PaperModeFragment;)Lcom/android/settings/dndmode/LabelPreference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/PaperModeFragment;->paperModeStartTime:Lcom/android/settings/dndmode/LabelPreference;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mformatChoosenTime(Lcom/android/settings/display/PaperModeFragment;II)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/display/PaperModeFragment;->formatChoosenTime(II)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetPaperModeSchedulerType(Lcom/android/settings/display/PaperModeFragment;)I
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/display/PaperModeFragment;->getPaperModeSchedulerType()I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mupdatePaperEffectGroup(Lcom/android/settings/display/PaperModeFragment;Ljava/lang/Boolean;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/display/PaperModeFragment;->updatePaperEffectGroup(Ljava/lang/Boolean;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetmEndTime()I
    .locals 1

    sget v0, Lcom/android/settings/display/PaperModeFragment;->mEndTime:I

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetmStartTime()I
    .locals 1

    sget v0, Lcom/android/settings/display/PaperModeFragment;->mStartTime:I

    return v0
.end method

.method static bridge synthetic -$$Nest$sfputmEndTime(I)V
    .locals 0

    sput p0, Lcom/android/settings/display/PaperModeFragment;->mEndTime:I

    return-void
.end method

.method static bridge synthetic -$$Nest$sfputmStartTime(I)V
    .locals 0

    sput p0, Lcom/android/settings/display/PaperModeFragment;->mStartTime:I

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    sget v0, Landroid/provider/MiuiSettings$ScreenEffect;->PAPER_MODE_MAX_LEVEL:I

    sput v0, Lcom/android/settings/display/PaperModeFragment;->PAPER_MODE_MAX_LEVEL:I

    const-string/jumbo v1, "paper_mode_min_level"

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v1, v2}, Lmiui/util/FeatureParser;->getFloat(Ljava/lang/String;F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    sput v1, Lcom/android/settings/display/PaperModeFragment;->PAPER_MODE_MIN_LEVEL:F

    int-to-float v0, v0

    sub-float/2addr v0, v1

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    sput v0, Lcom/android/settings/display/PaperModeFragment;->PER_LEVEL:F

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/SettingsPreferenceFragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/display/PaperModeFragment;->mTimeFlag:Z

    new-instance v0, Lcom/android/settings/display/PaperModeFragment$1;

    invoke-direct {v0, p0}, Lcom/android/settings/display/PaperModeFragment$1;-><init>(Lcom/android/settings/display/PaperModeFragment;)V

    iput-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mOnTimeSetListener:Lmiuix/appcompat/app/TimePickerDialog$OnTimeSetListener;

    new-instance v0, Lcom/android/settings/display/PaperModeFragment$2;

    invoke-direct {v0, p0}, Lcom/android/settings/display/PaperModeFragment$2;-><init>(Lcom/android/settings/display/PaperModeFragment;)V

    iput-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->dialogListener:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method public static autoAdjustState(Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "screen_auto_adjust"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    if-ne p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private formatChoosenTime(II)Ljava/lang/String;
    .locals 2

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->set(II)V

    const/16 p1, 0xc

    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {p0, v0, v1, p1}, Lmiuix/pickerwidget/date/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private getPaperModeLevel()I
    .locals 2

    iget-object p0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    sget v0, Landroid/provider/MiuiSettings$ScreenEffect;->DEFAULT_PAPER_MODE_LEVEL:I

    const-string/jumbo v1, "screen_paper_mode_level"

    invoke-static {p0, v1, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    return p0
.end method

.method private getPaperModeSchedulerType()I
    .locals 2

    iget-object p0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "paper_mode_scheduler_type"

    const/4 v1, 0x2

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    return p0
.end method

.method public static isPaperModeEnable(Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "screen_paper_mode_enabled"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/provider/MiuiSettings$System;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result p0

    return p0
.end method

.method public static isPaperModeTimeEnable(Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "screen_paper_mode_time_enabled"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/provider/MiuiSettings$System;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result p0

    return p0
.end method

.method private setAutoAdjustLevel(Z)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setAutoAdjustLevel enable : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PaperModeFragment"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "screen_auto_adjust"

    invoke-static {p0, v0, p1}, Landroid/provider/MiuiSettings$System;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    return-void
.end method

.method private setPaperModeEnable(Z)V
    .locals 1

    iget-object p0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "screen_paper_mode_enabled"

    invoke-static {p0, v0, p1}, Landroid/provider/MiuiSettings$System;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    return-void
.end method

.method private setPaperModeLevel(I)V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/display/PaperModeFragment;->getPaperModeLevel()I

    move-result v0

    if-eq p1, v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "screen_paper_mode_level"

    invoke-static {p0, v0, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_0
    return-void
.end method

.method private setPaperModeSchedulerType(I)V
    .locals 1

    iget-object p0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "paper_mode_scheduler_type"

    invoke-static {p0, v0, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method private setPaperModeTimeEnable(Z)V
    .locals 1

    iget-object p0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "screen_paper_mode_time_enabled"

    invoke-static {p0, v0, p1}, Landroid/provider/MiuiSettings$System;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    return-void
.end method

.method private updatePaperEffectGroup(Ljava/lang/Boolean;)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mPaperEffectGroup:Landroidx/preference/PreferenceGroup;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_4

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object v1, p0, Lcom/android/settings/display/PaperModeFragment;->mPaperEffectGroup:Landroidx/preference/PreferenceGroup;

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    invoke-static {}, Lcom/android/settings/MiuiUtils;->supportSmartEyeCare()Z

    move-result p1

    const/4 v1, 0x1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mAutoAdjustPreference:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, v1}, Landroidx/preference/Preference;->setVisible(Z)V

    :cond_1
    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string/jumbo v2, "screen_mode_type"

    invoke-static {p1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p1

    iget-object v2, p0, Lcom/android/settings/display/PaperModeFragment;->classicPreference:Lcom/android/settings/display/PaperRadioButtonPreference;

    if-nez p1, :cond_2

    move v3, v1

    goto :goto_0

    :cond_2
    move v3, v0

    :goto_0
    invoke-virtual {v2, v3}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p0, p0, Lcom/android/settings/display/PaperModeFragment;->paperPreference:Lcom/android/settings/display/PaperRadioButtonPreference;

    if-ne p1, v1, :cond_3

    move v0, v1

    :cond_3
    invoke-virtual {p0, v0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    goto :goto_1

    :cond_4
    const-string/jumbo p1, "paper_effect"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    if-eqz p1, :cond_5

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object v1, p0, Lcom/android/settings/display/PaperModeFragment;->mPaperEffectGroup:Landroidx/preference/PreferenceGroup;

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_5
    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mAutoAdjustPreference:Landroidx/preference/CheckBoxPreference;

    if-eqz p1, :cond_6

    invoke-static {}, Lcom/android/settings/MiuiUtils;->supportSmartEyeCare()Z

    move-result p1

    if-eqz p1, :cond_6

    iget-object p0, p0, Lcom/android/settings/display/PaperModeFragment;->mAutoAdjustPreference:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    :cond_6
    :goto_1
    return-void
.end method

.method private updatePaperModeTimeGroup(Ljava/lang/Boolean;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mPaperModeTimeGroup:Landroidx/preference/PreferenceGroup;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mPaperModeTimeRadioGroup:Landroidx/preference/PreferenceGroup;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mPaperModeTimeGroup:Landroidx/preference/PreferenceGroup;

    iget-object p0, p0, Lcom/android/settings/display/PaperModeFragment;->mPaperModeTimeRadioGroup:Landroidx/preference/PreferenceGroup;

    invoke-virtual {p1, p0}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mPaperModeTimeGroup:Landroidx/preference/PreferenceGroup;

    const-string/jumbo v0, "paper_mode_time_radio_group"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mPaperModeTimeGroup:Landroidx/preference/PreferenceGroup;

    iget-object p0, p0, Lcom/android/settings/display/PaperModeFragment;->mPaperModeTimeRadioGroup:Landroidx/preference/PreferenceGroup;

    invoke-virtual {p1, p0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_2
    :goto_0
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 0

    const-class p0, Lcom/android/settings/display/PaperModeFragment;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public initCallingLock()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mLocationGetDialog:Lmiuix/appcompat/app/AlertDialog;

    if-nez v0, :cond_0

    new-instance v0, Lmiuix/appcompat/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/android/settings/R$string;->paper_mode_get_location_dlg_title:I

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/android/settings/R$string;->paper_mode_get_location_dlg_msg:I

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/android/settings/R$string;->paper_mode_get_location_dlg_positive_btn_text:I

    iget-object v2, p0, Lcom/android/settings/display/PaperModeFragment;->dialogListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/android/settings/R$string;->paper_mode_get_location_dlg_negative_btn_text:I

    iget-object v2, p0, Lcom/android/settings/display/PaperModeFragment;->dialogListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setCancelable(Z)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mLocationGetDialog:Lmiuix/appcompat/app/AlertDialog;

    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lmiuix/preference/PreferenceFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    const-string/jumbo p1, "top_image"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p0

    sget p1, Lcom/android/settings/R$layout;->display_top_image_layout:I

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setLayoutResource(I)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    sget p1, Lcom/android/settings/R$xml;->paper_mode_settings:I

    invoke-virtual {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/android/settings/MiuiUtils;->isSupportHDRMode()Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/settings/display/PaperModeFragment;->isSupportHDRMode:Z

    const-string/jumbo p1, "paper_mode_enable"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->paperModeEnable:Landroidx/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/display/PaperModeFragment;->isPaperModeEnable(Landroid/content/Context;)Z

    move-result v0

    invoke-virtual {p1, v0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->paperModeEnable:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo p1, "paper_mode_time_radio_group"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/PreferenceGroup;

    iput-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mPaperModeTimeRadioGroup:Landroidx/preference/PreferenceGroup;

    const-string/jumbo p1, "paper_mode_time_group"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/PreferenceGroup;

    iput-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mPaperModeTimeGroup:Landroidx/preference/PreferenceGroup;

    const-string/jumbo p1, "paper_mode_customize_time_group"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/PreferenceGroup;

    iput-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mPaperModeCustomizeTimeGroup:Landroidx/preference/PreferenceGroup;

    const-string/jumbo p1, "paper_mode_time_enable"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->paperModeTimeEnable:Landroidx/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/display/PaperModeFragment;->isPaperModeTimeEnable(Landroid/content/Context;)Z

    move-result v0

    invoke-virtual {p1, v0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->paperModeTimeEnable:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo p1, "paper_effect"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/PreferenceGroup;

    iput-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mPaperEffectGroup:Landroidx/preference/PreferenceGroup;

    const-string p1, "classic_mode"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/display/PaperRadioButtonPreference;

    iput-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->classicPreference:Lcom/android/settings/display/PaperRadioButtonPreference;

    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.android.settings"

    const-string v2, "com.android.settings.display.ClassicProtectionActivity"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->classicPreference:Lcom/android/settings/display/PaperRadioButtonPreference;

    iget-object v2, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v0, v2, p1}, Lcom/android/settings/display/PaperRadioButtonPreference;->setTargetIntent(Landroid/app/Activity;Landroid/content/Intent;)V

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->classicPreference:Lcom/android/settings/display/PaperRadioButtonPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    const-string/jumbo p1, "paper_mode"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/display/PaperRadioButtonPreference;

    iput-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->paperPreference:Lcom/android/settings/display/PaperRadioButtonPreference;

    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    new-instance v0, Landroid/content/ComponentName;

    const-string v2, "com.android.settings.display.PaperProtectionActivity"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->paperPreference:Lcom/android/settings/display/PaperRadioButtonPreference;

    iget-object v1, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v0, v1, p1}, Lcom/android/settings/display/PaperRadioButtonPreference;->setTargetIntent(Landroid/app/Activity;Landroid/content/Intent;)V

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->paperPreference:Lcom/android/settings/display/PaperRadioButtonPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    const-string p1, "auto_adjust_effect"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mAutoAdjustPreference:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mAutoAdjustPreference:Landroidx/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/display/PaperModeFragment;->autoAdjustState(Landroid/content/Context;)Z

    move-result v0

    invoke-virtual {p1, v0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    const-string/jumbo p1, "paper_mode_auto_twilight"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/display/PaperModePreference;

    iput-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mAutoTwilightPref:Lcom/android/settings/display/PaperModePreference;

    invoke-direct {p0}, Lcom/android/settings/display/PaperModeFragment;->getPaperModeSchedulerType()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    invoke-virtual {p1, v0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mAutoTwilightPref:Lcom/android/settings/display/PaperModePreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    const-string/jumbo p1, "paper_mode_customize_time"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/display/PaperModePreference;

    iput-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mCustomizedTimePref:Lcom/android/settings/display/PaperModePreference;

    invoke-direct {p0}, Lcom/android/settings/display/PaperModeFragment;->getPaperModeSchedulerType()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    goto :goto_1

    :cond_1
    move v1, v2

    :goto_1
    invoke-virtual {p1, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mCustomizedTimePref:Lcom/android/settings/display/PaperModePreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/TimeZone;->getRawOffset()I

    move-result p1

    iput p1, p0, Lcom/android/settings/display/PaperModeFragment;->mTimeZoneOffset:I

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/display/PaperModeTimeModeUtil;->getPaperModeStartTime(Landroid/content/Context;)I

    move-result p1

    sput p1, Lcom/android/settings/display/PaperModeFragment;->mStartTime:I

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/display/PaperModeTimeModeUtil;->getPaperModeEndTime(Landroid/content/Context;)I

    move-result p1

    sput p1, Lcom/android/settings/display/PaperModeFragment;->mEndTime:I

    const-string/jumbo p1, "paper_mode_start_time"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/dndmode/LabelPreference;

    iput-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->paperModeStartTime:Lcom/android/settings/dndmode/LabelPreference;

    const-string/jumbo p1, "paper_mode_end_time"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/dndmode/LabelPreference;

    iput-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->paperModeEndTime:Lcom/android/settings/dndmode/LabelPreference;

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->paperModeStartTime:Lcom/android/settings/dndmode/LabelPreference;

    sget v0, Lcom/android/settings/display/PaperModeFragment;->mStartTime:I

    div-int/lit8 v1, v0, 0x3c

    rem-int/lit8 v0, v0, 0x3c

    invoke-direct {p0, v1, v0}, Lcom/android/settings/display/PaperModeFragment;->formatChoosenTime(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/settings/dndmode/LabelPreference;->setLabel(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->paperModeEndTime:Lcom/android/settings/dndmode/LabelPreference;

    sget v0, Lcom/android/settings/display/PaperModeFragment;->mEndTime:I

    div-int/lit8 v1, v0, 0x3c

    rem-int/lit8 v0, v0, 0x3c

    invoke-direct {p0, v1, v0}, Lcom/android/settings/display/PaperModeFragment;->formatChoosenTime(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/settings/dndmode/LabelPreference;->setLabel(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->paperModeStartTime:Lcom/android/settings/dndmode/LabelPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->paperModeEndTime:Lcom/android/settings/dndmode/LabelPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/device/MiuiAboutPhoneUtils;->getInstance(Landroid/content/Context;)Lcom/android/settings/device/MiuiAboutPhoneUtils;

    move-result-object p1

    invoke-virtual {p1}, Lcom/android/settings/device/MiuiAboutPhoneUtils;->isMIUILite()Z

    move-result p1

    if-eqz p1, :cond_2

    const-string/jumbo p1, "top_image"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_2
    new-instance p1, Lmiuix/appcompat/app/TimePickerDialog;

    iget-object v4, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/settings/display/PaperModeFragment;->mOnTimeSetListener:Lmiuix/appcompat/app/TimePickerDialog$OnTimeSetListener;

    sget v0, Lcom/android/settings/display/PaperModeFragment;->mStartTime:I

    div-int/lit8 v6, v0, 0x3c

    rem-int/lit8 v7, v0, 0x3c

    invoke-static {v4}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v8

    move-object v3, p1

    invoke-direct/range {v3 .. v8}, Lmiuix/appcompat/app/TimePickerDialog;-><init>(Landroid/content/Context;Lmiuix/appcompat/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    iput-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mTimePickerDialog:Lmiuix/appcompat/app/TimePickerDialog;

    new-instance p1, Lcom/android/settings/display/PaperModeFragment$3;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p1, p0, v0}, Lcom/android/settings/display/PaperModeFragment$3;-><init>(Lcom/android/settings/display/PaperModeFragment;Landroid/os/Handler;)V

    iput-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mPaperModeEnableObserver:Landroid/database/ContentObserver;

    new-instance p1, Lcom/android/settings/display/PaperModeFragment$4;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p1, p0, v0}, Lcom/android/settings/display/PaperModeFragment$4;-><init>(Lcom/android/settings/display/PaperModeFragment;Landroid/os/Handler;)V

    iput-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mPaperModeScheduleObserver:Landroid/database/ContentObserver;

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string/jumbo v0, "paper_mode_scheduler_type"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/PaperModeFragment;->mPaperModeScheduleObserver:Landroid/database/ContentObserver;

    invoke-virtual {p1, v0, v2, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string/jumbo v0, "screen_paper_mode_enabled"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object p0, p0, Lcom/android/settings/display/PaperModeFragment;->mPaperModeEnableObserver:Landroid/database/ContentObserver;

    invoke-virtual {p1, v0, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/PaperModeFragment;->mPaperModeEnableObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v1, p0, Lcom/android/settings/display/PaperModeFragment;->mPaperModeScheduleObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    invoke-super {p0}, Lcom/android/settingslib/core/lifecycle/ObservablePreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getListView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->isComputingLayout()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    const-string/jumbo v0, "paper_mode_enable"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string/jumbo p1, "setting_Display_PE"

    invoke-static {p1, p2}, Lcom/android/settings/report/InternationalCompat;->trackReportSwitchStatus(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->paperModeEnable:Landroidx/preference/CheckBoxPreference;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/display/PaperModeFragment;->setPaperModeEnable(Z)V

    invoke-direct {p0, p2}, Lcom/android/settings/display/PaperModeFragment;->updatePaperEffectGroup(Ljava/lang/Boolean;)V

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    invoke-static {v0, p0}, Lcom/android/settingslib/util/OneTrackInterfaceUtils;->trackSwitchEvent(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "paper_mode_time_enable"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo p1, "setting_Display_PET"

    invoke-static {p1, p2}, Lcom/android/settings/report/InternationalCompat;->trackReportSwitchStatus(Ljava/lang/String;Ljava/lang/Object;)V

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/display/PaperModeFragment;->setPaperModeTimeEnable(Z)V

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->paperModeTimeEnable:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/android/settings/display/PaperModeFragment;->getPaperModeSchedulerType()I

    move-result v1

    :cond_2
    invoke-static {p1, v1}, Lcom/android/settings/display/PaperModeTimeModeUtil;->startPaperModeAutoTime(Landroid/content/Context;I)V

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const-string v0, "ScreenEffect_PapermodeTimeControl"

    invoke-static {v0, p1}, Lcom/android/settingslib/util/OneTrackInterfaceUtils;->trackSwitchEvent(Ljava/lang/String;Z)V

    invoke-direct {p0, p2}, Lcom/android/settings/display/PaperModeFragment;->updatePaperModeTimeGroup(Ljava/lang/Boolean;)V

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "paper_mode_adjust_level"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-float p1, p1

    sget p2, Lcom/android/settings/display/PaperModeFragment;->PER_LEVEL:F

    mul-float/2addr p1, p2

    sget p2, Lcom/android/settings/display/PaperModeFragment;->PAPER_MODE_MIN_LEVEL:F

    add-float/2addr p1, p2

    float-to-int p1, p1

    invoke-direct {p0, p1}, Lcom/android/settings/display/PaperModeFragment;->setPaperModeLevel(I)V

    goto :goto_0

    :cond_4
    const-string v0, "auto_adjust_effect"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/display/PaperModeFragment;->setAutoAdjustLevel(Z)V

    :cond_5
    :goto_0
    const/4 p0, 0x1

    return p0
.end method

.method public onPreferenceClick(Landroidx/preference/Preference;)Z
    .locals 10

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string/jumbo v1, "paper_mode_auto_twilight"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/location/LocationManager;

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string v0, "location_mode"

    invoke-static {p1, v0, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p1

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/display/PaperModeFragment;->initCallingLock()V

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mLocationGetDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    :cond_0
    invoke-direct {p0, v2}, Lcom/android/settings/display/PaperModeFragment;->setPaperModeSchedulerType(I)V

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mAutoTwilightPref:Lcom/android/settings/display/PaperModePreference;

    invoke-virtual {p1, v2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p0, p0, Lcom/android/settings/display/PaperModeFragment;->mCustomizedTimePref:Lcom/android/settings/display/PaperModePreference;

    invoke-virtual {p0, v3}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    goto/16 :goto_2

    :cond_1
    const-string/jumbo v1, "paper_mode_customize_time"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lcom/android/settings/display/PaperModeFragment;->setPaperModeSchedulerType(I)V

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mAutoTwilightPref:Lcom/android/settings/display/PaperModePreference;

    invoke-virtual {p1, v3}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p0, p0, Lcom/android/settings/display/PaperModeFragment;->mCustomizedTimePref:Lcom/android/settings/display/PaperModePreference;

    invoke-virtual {p0, v2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    goto/16 :goto_2

    :cond_2
    const-string/jumbo v1, "paper_mode_start_time"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iput-boolean v3, p0, Lcom/android/settings/display/PaperModeFragment;->mTimeFlag:Z

    new-instance p1, Lmiuix/appcompat/app/TimePickerDialog;

    iget-object v5, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/android/settings/display/PaperModeFragment;->mOnTimeSetListener:Lmiuix/appcompat/app/TimePickerDialog$OnTimeSetListener;

    sget v0, Lcom/android/settings/display/PaperModeFragment;->mStartTime:I

    div-int/lit8 v7, v0, 0x3c

    rem-int/lit8 v8, v0, 0x3c

    invoke-static {v5}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v9

    move-object v4, p1

    invoke-direct/range {v4 .. v9}, Lmiuix/appcompat/app/TimePickerDialog;-><init>(Landroid/content/Context;Lmiuix/appcompat/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    iput-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mTimePickerDialog:Lmiuix/appcompat/app/TimePickerDialog;

    sget v0, Lcom/android/settings/display/PaperModeFragment;->mStartTime:I

    if-lez v0, :cond_3

    div-int/lit8 v1, v0, 0x3c

    rem-int/lit8 v0, v0, 0x3c

    invoke-virtual {p1, v1, v0}, Lmiuix/appcompat/app/TimePickerDialog;->updateTime(II)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1, v3, v3}, Lmiuix/appcompat/app/TimePickerDialog;->updateTime(II)V

    :goto_0
    iget-object p0, p0, Lcom/android/settings/display/PaperModeFragment;->mTimePickerDialog:Lmiuix/appcompat/app/TimePickerDialog;

    invoke-virtual {p0}, Landroid/app/Dialog;->show()V

    goto :goto_2

    :cond_4
    const-string/jumbo v1, "paper_mode_end_time"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    iput-boolean v2, p0, Lcom/android/settings/display/PaperModeFragment;->mTimeFlag:Z

    new-instance p1, Lmiuix/appcompat/app/TimePickerDialog;

    iget-object v5, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/android/settings/display/PaperModeFragment;->mOnTimeSetListener:Lmiuix/appcompat/app/TimePickerDialog$OnTimeSetListener;

    sget v0, Lcom/android/settings/display/PaperModeFragment;->mEndTime:I

    div-int/lit8 v7, v0, 0x3c

    rem-int/lit8 v8, v0, 0x3c

    invoke-static {v5}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v9

    move-object v4, p1

    invoke-direct/range {v4 .. v9}, Lmiuix/appcompat/app/TimePickerDialog;-><init>(Landroid/content/Context;Lmiuix/appcompat/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    iput-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mTimePickerDialog:Lmiuix/appcompat/app/TimePickerDialog;

    sget v0, Lcom/android/settings/display/PaperModeFragment;->mEndTime:I

    if-lez v0, :cond_5

    div-int/lit8 v1, v0, 0x3c

    rem-int/lit8 v0, v0, 0x3c

    invoke-virtual {p1, v1, v0}, Lmiuix/appcompat/app/TimePickerDialog;->updateTime(II)V

    goto :goto_1

    :cond_5
    invoke-virtual {p1, v3, v3}, Lmiuix/appcompat/app/TimePickerDialog;->updateTime(II)V

    :goto_1
    iget-object p0, p0, Lcom/android/settings/display/PaperModeFragment;->mTimePickerDialog:Lmiuix/appcompat/app/TimePickerDialog;

    invoke-virtual {p0}, Landroid/app/Dialog;->show()V

    goto :goto_2

    :cond_6
    const-string v1, "classic_mode"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string/jumbo v4, "screen_mode_type"

    if-eqz v1, :cond_7

    iget-object p0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    invoke-static {p0, v4, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_2

    :cond_7
    const-string/jumbo v1, "paper_mode"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_8

    iget-object p0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    invoke-static {p0, v4, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    sget-boolean p0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez p0, :cond_8

    sget-object p0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    const-string/jumbo p1, "status"

    invoke-virtual {v0, p1, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo p0, "texture_eyecare_status"

    invoke-static {p0, v0}, Lcom/android/settingslib/util/OneTrackInterfaceUtils;->track(Ljava/lang/String;Ljava/util/Map;)V

    :cond_8
    :goto_2
    return v2
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->paperModeEnable:Landroidx/preference/CheckBoxPreference;

    iget-object p0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-static {p0}, Lcom/android/settings/display/PaperModeFragment;->isPaperModeEnable(Landroid/content/Context;)Z

    move-result p0

    invoke-virtual {v0, p0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    return-void
.end method

.method public onStopLocated()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    const-string v1, "jobscheduler"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    const v1, 0xabe9

    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->cancel(I)V

    iget-object v0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object p0, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    const-class v2, Lcom/android/settings/display/PaperModeSunTimeService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/android/settings/SettingsPreferenceFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-static {}, Lcom/android/settings/MiuiUtils;->supportPaperEyeCare()Z

    move-result p1

    const/4 p2, 0x0

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->paperPreference:Lcom/android/settings/display/PaperRadioButtonPreference;

    invoke-virtual {p1, p2}, Landroidx/preference/Preference;->setVisible(Z)V

    :cond_0
    invoke-static {}, Lcom/android/settings/MiuiUtils;->supportSmartEyeCare()Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mAutoAdjustPreference:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p2}, Landroidx/preference/Preference;->setVisible(Z)V

    :cond_1
    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/display/PaperModeFragment;->isPaperModeTimeEnable(Landroid/content/Context;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/android/settings/display/PaperModeFragment;->updatePaperModeTimeGroup(Ljava/lang/Boolean;)V

    iget-object p1, p0, Lcom/android/settings/display/PaperModeFragment;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/display/PaperModeFragment;->isPaperModeEnable(Landroid/content/Context;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/android/settings/display/PaperModeFragment;->updatePaperEffectGroup(Ljava/lang/Boolean;)V

    return-void
.end method
