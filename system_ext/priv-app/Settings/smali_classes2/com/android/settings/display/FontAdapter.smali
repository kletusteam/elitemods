.class public Lcom/android/settings/display/FontAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/display/FontAdapter$FontViewHolder;,
        Lcom/android/settings/display/FontAdapter$FontSelectListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/android/settings/display/FontAdapter$FontViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field public static EMPTY:I = 0x0

.field public static FONT:I = 0x1

.field private static TAG:Ljava/lang/String; = "FontAdapter"


# instance fields
.field private contextWeakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private downIndex:J

.field private mCurrentIndex:J

.field public mLastIndex:J

.field private mList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/display/LocalFontModel;",
            ">;"
        }
    .end annotation
.end field

.field mListener:Lcom/android/settings/display/FontAdapter$FontSelectListener;


# direct methods
.method public static synthetic $r8$lambda$muwgBhfJW-g_prFuCoZ1SYWIDp4(Landroid/widget/TextView;Landroid/graphics/Typeface;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/android/settings/display/FontAdapter;->lambda$setFont$0(Landroid/widget/TextView;Landroid/graphics/Typeface;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetdownIndex(Lcom/android/settings/display/FontAdapter;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/settings/display/FontAdapter;->downIndex:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmList(Lcom/android/settings/display/FontAdapter;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/FontAdapter;->mList:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputdownIndex(Lcom/android/settings/display/FontAdapter;J)V
    .locals 0

    iput-wide p1, p0, Lcom/android/settings/display/FontAdapter;->downIndex:J

    return-void
.end method

.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/settings/display/FontAdapter;->downIndex:J

    iput-wide v0, p0, Lcom/android/settings/display/FontAdapter;->mCurrentIndex:J

    iput-wide v0, p0, Lcom/android/settings/display/FontAdapter;->mLastIndex:J

    return-void
.end method

.method private getMiddleScaleFromWeightList(Lcom/android/settings/display/LocalFontModel;)I
    .locals 3

    sget-object p0, Lcom/android/settings/display/PageLayoutFragment;->MIUI_WGHT:[I

    const/4 v0, 0x5

    aget p0, p0, v0

    invoke-virtual {p1}, Lcom/android/settings/display/LocalFontModel;->getFontWeight()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    return p0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p0

    rem-int/lit8 v1, p0, 0x2

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    div-int/lit8 p0, p0, 0x2

    invoke-interface {v0, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/android/settings/display/LocalFontModel;->getFontWeight()Ljava/util/List;

    move-result-object v0

    div-int/lit8 p0, p0, 0x2

    invoke-interface {v0, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1}, Lcom/android/settings/display/LocalFontModel;->getFontWeight()Ljava/util/List;

    move-result-object p1

    sub-int/2addr p0, v2

    invoke-interface {p1, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    add-int/2addr v0, p0

    div-int/lit8 p0, v0, 0x2

    :goto_0
    return p0
.end method

.method private static synthetic lambda$setFont$0(Landroid/widget/TextView;Landroid/graphics/Typeface;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    return-void
.end method

.method private tryGetFontAssertFile(Lcom/android/settings/display/LocalFontModel;)Ljava/io/File;
    .locals 4

    :try_start_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/android/settings/display/FontAdapter;->contextWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/android/settings/display/LocalFontModel;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ".ttf"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    iget-object p0, p0, Lcom/android/settings/display/FontAdapter;->contextWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    invoke-virtual {p1}, Lcom/android/settings/display/LocalFontModel;->getContentUri()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object p0

    invoke-static {p0, v0}, Lcom/android/settings/usagestats/utils/FileUtils;->InputStreamToFile(Ljava/io/InputStream;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method public getItemCount()I
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/FontAdapter;->mList:Ljava/util/List;

    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public getItemId(I)J
    .locals 0

    int-to-long p0, p1

    return-wide p0
.end method

.method public getItemViewType(I)I
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/display/FontAdapter;->getItemCount()I

    move-result p0

    add-int/lit8 p0, p0, -0x1

    if-ne p1, p0, :cond_0

    sget p0, Lcom/android/settings/display/FontAdapter;->EMPTY:I

    return p0

    :cond_0
    sget p0, Lcom/android/settings/display/FontAdapter;->FONT:I

    return p0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    check-cast p1, Lcom/android/settings/display/FontAdapter$FontViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/display/FontAdapter;->onBindViewHolder(Lcom/android/settings/display/FontAdapter$FontViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/android/settings/display/FontAdapter$FontViewHolder;I)V
    .locals 7

    iget-object v0, p0, Lcom/android/settings/display/FontAdapter;->mList:Ljava/util/List;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    const/4 v2, 0x0

    if-ne p2, v0, :cond_1

    iget-object v0, p1, Lcom/android/settings/display/FontAdapter$FontViewHolder;->checkBox:Lmiuix/visual/check/VisualCheckBox;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p1, Lcom/android/settings/display/FontAdapter$FontViewHolder;->checkbox3points:Lmiuix/visual/check/VisualCheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object p1, p1, Lcom/android/settings/display/FontAdapter$FontViewHolder;->checkbox3points:Lmiuix/visual/check/VisualCheckBox;

    iget-wide v3, p0, Lcom/android/settings/display/FontAdapter;->mCurrentIndex:J

    int-to-long v5, p2

    cmp-long p0, v3, v5

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    invoke-virtual {p1, v1}, Lmiuix/visual/check/VisualCheckBox;->setChecked(Z)V

    goto :goto_2

    :cond_1
    iget-object v0, p0, Lcom/android/settings/display/FontAdapter;->mList:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/display/LocalFontModel;

    iget-object v3, p1, Lcom/android/settings/display/FontAdapter$FontViewHolder;->textView:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/android/settings/display/LocalFontModel;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p1, Lcom/android/settings/display/FontAdapter$FontViewHolder;->textView:Landroid/widget/TextView;

    invoke-virtual {p0, v3, v0}, Lcom/android/settings/display/FontAdapter;->setFontFamily(Landroid/widget/TextView;Lcom/android/settings/display/LocalFontModel;)V

    iget-object p1, p1, Lcom/android/settings/display/FontAdapter$FontViewHolder;->checkBox:Lmiuix/visual/check/VisualCheckBox;

    iget-wide v3, p0, Lcom/android/settings/display/FontAdapter;->mCurrentIndex:J

    int-to-long v5, p2

    cmp-long p0, v3, v5

    if-nez p0, :cond_2

    goto :goto_1

    :cond_2
    move v1, v2

    :goto_1
    invoke-virtual {p1, v1}, Lmiuix/visual/check/VisualCheckBox;->setChecked(Z)V

    :cond_3
    :goto_2
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/display/FontAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/android/settings/display/FontAdapter$FontViewHolder;

    move-result-object p0

    return-object p0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/android/settings/display/FontAdapter$FontViewHolder;
    .locals 2

    iget-object p2, p0, Lcom/android/settings/display/FontAdapter;->mList:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    const/4 v0, 0x0

    const/4 v1, 0x3

    if-lt p2, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    sget v1, Lcom/android/settings/R$layout;->font_item_normal:I

    invoke-virtual {p2, v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    sget v1, Lcom/android/settings/R$layout;->font_item_big:I

    invoke-virtual {p2, v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    :goto_0
    new-instance p2, Lcom/android/settings/display/FontAdapter$FontViewHolder;

    invoke-direct {p2, p1}, Lcom/android/settings/display/FontAdapter$FontViewHolder;-><init>(Landroid/view/View;)V

    new-instance p1, Lcom/android/settings/display/FontAdapter$1;

    invoke-direct {p1, p0}, Lcom/android/settings/display/FontAdapter$1;-><init>(Lcom/android/settings/display/FontAdapter;)V

    invoke-virtual {p2, p1}, Lcom/android/settings/display/FontAdapter$FontViewHolder;->setItemTouchListener(Lcom/android/settings/display/FontAdapter$FontViewHolder$OnItemTouchListener;)V

    return-object p2
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 1

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/display/FontAdapter;->contextWeakReference:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public setCurrentFontId(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/android/settings/display/FontAdapter;->mList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/android/settings/display/FontAdapter;->mList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/display/LocalFontModel;

    invoke-virtual {v1}, Lcom/android/settings/display/LocalFontModel;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/android/settings/display/FontAdapter;->getItemId(I)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/display/FontAdapter;->setCurrentIndex(J)V

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method public setCurrentIndex(J)V
    .locals 2

    iget-wide v0, p0, Lcom/android/settings/display/FontAdapter;->mCurrentIndex:J

    iput-wide v0, p0, Lcom/android/settings/display/FontAdapter;->mLastIndex:J

    iput-wide p1, p0, Lcom/android/settings/display/FontAdapter;->mCurrentIndex:J

    long-to-int p1, v0

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    iget-wide p1, p0, Lcom/android/settings/display/FontAdapter;->mCurrentIndex:J

    long-to-int p1, p1

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    return-void
.end method

.method public setDataList(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/display/LocalFontModel;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/settings/display/FontAdapter;->mList:Ljava/util/List;

    const/4 p1, 0x0

    :goto_0
    iget-object v0, p0, Lcom/android/settings/display/FontAdapter;->mList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/display/FontAdapter;->mList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/display/LocalFontModel;

    invoke-virtual {v0}, Lcom/android/settings/display/LocalFontModel;->getId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/display/LocalFontModel;->getId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "10"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/display/LocalFontModel;->getId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "b004d74e-5c49-430c-bb6a-18ed5d2d33e4"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/display/LocalFontModel;->getId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "-100"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/display/LocalFontModel;->getFontAssert()Ljava/io/File;

    move-result-object v1

    if-nez v1, :cond_0

    :try_start_0
    invoke-direct {p0, v0}, Lcom/android/settings/display/FontAdapter;->tryGetFontAssertFile(Lcom/android/settings/display/LocalFontModel;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/display/LocalFontModel;->setFontAssert(Ljava/io/File;)V

    if-eqz v1, :cond_0

    invoke-static {v1}, Landroid/graphics/Typeface;->createFromFile(Ljava/io/File;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/display/LocalFontModel;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v1, p0, Lcom/android/settings/display/FontAdapter;->mList:Ljava/util/List;

    invoke-interface {v1, p1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    sget-object v2, Lcom/android/settings/display/FontAdapter;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setDataList file io error, font is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/android/settings/display/LocalFontModel;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ","

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    :goto_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setFont(Landroid/widget/TextView;Landroid/graphics/Typeface;)V
    .locals 0

    new-instance p0, Lcom/android/settings/display/FontAdapter$$ExternalSyntheticLambda0;

    invoke-direct {p0, p1, p2}, Lcom/android/settings/display/FontAdapter$$ExternalSyntheticLambda0;-><init>(Landroid/widget/TextView;Landroid/graphics/Typeface;)V

    invoke-static {p0}, Lcom/android/settingslib/utils/ThreadUtils;->postOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setFontFamily(Landroid/widget/TextView;Lcom/android/settings/display/LocalFontModel;)V
    .locals 3

    :try_start_0
    invoke-virtual {p2}, Lcom/android/settings/display/LocalFontModel;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p2}, Lcom/android/settings/display/LocalFontModel;->getId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "10"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object p2, Lcom/android/settings/display/PageLayoutFragment;->MIUI_WGHT:[I

    const/4 v0, 0x5

    aget p2, p2, v0

    const/4 v0, 0x0

    invoke-static {p2, v0}, Lcom/android/settings/display/font/FontWeightUtils;->getVarTypeface(II)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_1

    :cond_0
    invoke-virtual {p2}, Lcom/android/settings/display/LocalFontModel;->isVariable()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, p2}, Lcom/android/settings/display/FontAdapter;->getMiddleScaleFromWeightList(Lcom/android/settings/display/LocalFontModel;)I

    move-result v0

    invoke-virtual {p2}, Lcom/android/settings/display/LocalFontModel;->getId()Ljava/lang/String;

    move-result-object p2

    const-string v1, "b004d74e-5c49-430c-bb6a-18ed5d2d33e4"

    invoke-static {p2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_1

    const/4 p2, 0x3

    goto :goto_0

    :cond_1
    const/4 p2, 0x4

    :goto_0
    invoke-static {v0, p2}, Lcom/android/settings/display/font/FontWeightUtils;->getVarTypeface(II)Landroid/graphics/Typeface;

    move-result-object v0

    :cond_2
    :goto_1
    if-eqz v0, :cond_3

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/display/FontAdapter;->setFont(Landroid/widget/TextView;Landroid/graphics/Typeface;)V

    goto :goto_2

    :cond_3
    sget-object p0, Lcom/android/settings/display/FontAdapter;->TAG:Ljava/lang/String;

    const-string/jumbo p1, "setFontFamily error fontTypeFace, content uri is null! "

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p0

    sget-object p1, Lcom/android/settings/display/FontAdapter;->TAG:Ljava/lang/String;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v0, "set FontFamily fail, "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    return-void
.end method

.method public setFontSelectListener(Lcom/android/settings/display/FontAdapter$FontSelectListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/FontAdapter;->mListener:Lcom/android/settings/display/FontAdapter$FontSelectListener;

    return-void
.end method
