.class Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;
.super Landroid/database/ContentObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/display/BrightnessFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BrightnessObserver"
.end annotation


# instance fields
.field private final BRIGHTNESS_FLOAT_URI:Landroid/net/Uri;

.field private final BRIGHTNESS_FOR_VR_FLOAT_URI:Landroid/net/Uri;

.field private final BRIGHTNESS_MODE_URI:Landroid/net/Uri;

.field private final BRIGHTNESS_URI:Landroid/net/Uri;

.field private final SCREEN_SUNLIGHT_MODE_URI:Landroid/net/Uri;

.field final synthetic this$0:Lcom/android/settings/display/BrightnessFragment;


# direct methods
.method public constructor <init>(Lcom/android/settings/display/BrightnessFragment;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    const-string/jumbo p1, "screen_brightness_mode"

    invoke-static {p1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->BRIGHTNESS_MODE_URI:Landroid/net/Uri;

    const-string/jumbo p1, "screen_brightness"

    invoke-static {p1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->BRIGHTNESS_URI:Landroid/net/Uri;

    const-string/jumbo p1, "screen_brightness_float"

    invoke-static {p1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->BRIGHTNESS_FLOAT_URI:Landroid/net/Uri;

    const-string/jumbo p1, "screen_brightness_for_vr_float"

    invoke-static {p1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->BRIGHTNESS_FOR_VR_FLOAT_URI:Landroid/net/Uri;

    const-string/jumbo p1, "sunlight_mode"

    invoke-static {p1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->SCREEN_SUNLIGHT_MODE_URI:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->onChange(ZLandroid/net/Uri;)V

    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 0

    if-eqz p1, :cond_0

    return-void

    :cond_0
    iget-object p1, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->BRIGHTNESS_MODE_URI:Landroid/net/Uri;

    invoke-virtual {p1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {p1}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmBackgroundHandler(Lcom/android/settings/display/BrightnessFragment;)Lcom/android/settings/display/BrightnessFragment$BackgroundHandler;

    move-result-object p1

    iget-object p2, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {p2}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmUpdateModeRunnable(Lcom/android/settings/display/BrightnessFragment;)Ljava/lang/Runnable;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object p1, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {p1}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmBackgroundHandler(Lcom/android/settings/display/BrightnessFragment;)Lcom/android/settings/display/BrightnessFragment$BackgroundHandler;

    move-result-object p1

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {p0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmUpdateBrightnessSeekBarRunnable(Lcom/android/settings/display/BrightnessFragment;)Ljava/lang/Runnable;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->BRIGHTNESS_FOR_VR_FLOAT_URI:Landroid/net/Uri;

    invoke-virtual {p1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {p1}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmBackgroundHandler(Lcom/android/settings/display/BrightnessFragment;)Lcom/android/settings/display/BrightnessFragment$BackgroundHandler;

    move-result-object p1

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {p0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmUpdateBrightnessSeekBarRunnable(Lcom/android/settings/display/BrightnessFragment;)Ljava/lang/Runnable;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->SCREEN_SUNLIGHT_MODE_URI:Landroid/net/Uri;

    invoke-virtual {p1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {p0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$misSunlightModeEnabled(Lcom/android/settings/display/BrightnessFragment;)Z

    move-result p1

    invoke-static {p0, p1}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$mupdateSunlightMode(Lcom/android/settings/display/BrightnessFragment;Z)V

    goto :goto_0

    :cond_3
    iget-object p1, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {p1}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmBackgroundHandler(Lcom/android/settings/display/BrightnessFragment;)Lcom/android/settings/display/BrightnessFragment$BackgroundHandler;

    move-result-object p1

    iget-object p2, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {p2}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmUpdateModeRunnable(Lcom/android/settings/display/BrightnessFragment;)Ljava/lang/Runnable;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object p1, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {p1}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmBackgroundHandler(Lcom/android/settings/display/BrightnessFragment;)Lcom/android/settings/display/BrightnessFragment$BackgroundHandler;

    move-result-object p1

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {p0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmUpdateBrightnessSeekBarRunnable(Lcom/android/settings/display/BrightnessFragment;)Ljava/lang/Runnable;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void
.end method

.method public startObserving()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {v0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmContext(Lcom/android/settings/display/BrightnessFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->BRIGHTNESS_MODE_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->BRIGHTNESS_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->BRIGHTNESS_FLOAT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->BRIGHTNESS_FOR_VR_FLOAT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->SCREEN_SUNLIGHT_MODE_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {v0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmDisplayManager(Lcom/android/settings/display/BrightnessFragment;)Landroid/hardware/display/DisplayManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {v1}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmDisplayListener(Lcom/android/settings/display/BrightnessFragment;)Landroid/hardware/display/DisplayManager$DisplayListener;

    move-result-object v1

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {p0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmHandler(Lcom/android/settings/display/BrightnessFragment;)Landroid/os/Handler;

    move-result-object p0

    const-wide/16 v2, 0x8

    invoke-virtual {v0, v1, p0, v2, v3}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;J)V

    return-void
.end method

.method public stopObserving()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {v0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmContext(Lcom/android/settings/display/BrightnessFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {v0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmDisplayManager(Lcom/android/settings/display/BrightnessFragment;)Landroid/hardware/display/DisplayManager;

    move-result-object v0

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {p0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmDisplayListener(Lcom/android/settings/display/BrightnessFragment;)Landroid/hardware/display/DisplayManager$DisplayListener;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/hardware/display/DisplayManager;->unregisterDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;)V

    return-void
.end method
