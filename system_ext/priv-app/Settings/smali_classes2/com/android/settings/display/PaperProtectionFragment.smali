.class public Lcom/android/settings/display/PaperProtectionFragment;
.super Lcom/android/settings/SettingsPreferenceFragment;

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;
.implements Landroidx/preference/Preference$OnPreferenceClickListener;


# static fields
.field private static final EYECARE_MAX_LEVEL:I

.field private static final EYECARE_MIN_LEVEL:F

.field private static final PAPER_MODE_MAX_LEVEL:I

.field private static final PAPER_MODE_MIN_LEVEL:F

.field private static final PER_LEVEL:F

.field private static final PER_TEXTURE_LEVEL:F


# instance fields
.field private colorTypePreference:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

.field private hintPreference:Lcom/android/settings/widget/PaperModeTipPreference;

.field private mAtuoAdjustState:Z

.field private paperTempPreference:Lcom/android/settings/display/TemperatureSeekBarPreference;

.field private resetPreference:Landroidx/preference/Preference;

.field private texturePreference:Lcom/android/settings/widget/MiuiSeekBarPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    sget v0, Landroid/provider/MiuiSettings$ScreenEffect;->PAPER_MODE_MAX_LEVEL:I

    sput v0, Lcom/android/settings/display/PaperProtectionFragment;->PAPER_MODE_MAX_LEVEL:I

    const-string/jumbo v1, "paper_mode_min_level"

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v1, v2}, Lmiui/util/FeatureParser;->getFloat(Ljava/lang/String;F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    sput v1, Lcom/android/settings/display/PaperProtectionFragment;->PAPER_MODE_MIN_LEVEL:F

    int-to-float v0, v0

    sub-float/2addr v0, v1

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    sput v0, Lcom/android/settings/display/PaperProtectionFragment;->PER_LEVEL:F

    const-string/jumbo v0, "paper_eyecare_max_texture"

    const/16 v3, 0x19

    invoke-static {v0, v3}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/settings/display/PaperProtectionFragment;->EYECARE_MAX_LEVEL:I

    const-string/jumbo v3, "paper_eyecare_min_texture"

    invoke-static {v3, v2}, Lmiui/util/FeatureParser;->getFloat(Ljava/lang/String;F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    sput v2, Lcom/android/settings/display/PaperProtectionFragment;->EYECARE_MIN_LEVEL:F

    int-to-float v0, v0

    sub-float/2addr v0, v2

    div-float/2addr v0, v1

    sput v0, Lcom/android/settings/display/PaperProtectionFragment;->PER_TEXTURE_LEVEL:F

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/SettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method private getPaperEyeCareLevel()I
    .locals 2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    sget v0, Lcom/android/settings/display/util/PaperConstants;->DEFAULT_TEXTURE_EYECARE_LEVEL:I

    const-string/jumbo v1, "screen_texture_eyecare_level"

    invoke-static {p0, v1, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    return p0
.end method

.method private getPaperModeLevel()I
    .locals 2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    sget v0, Lcom/android/settings/display/util/PaperConstants;->DEFAULT_TEXTURE_MODE_LEVEL:F

    float-to-int v0, v0

    const-string/jumbo v1, "screen_paper_texture_level"

    invoke-static {p0, v1, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    return p0
.end method

.method private getTextureColorType()I
    .locals 3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_texture_color_type"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v1, Lcom/android/settings/R$array;->paper_color_index:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object p0

    array-length p0, p0

    if-ge v0, p0, :cond_1

    if-gez v0, :cond_0

    goto :goto_0

    :cond_0
    move v2, v0

    goto :goto_1

    :cond_1
    :goto_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getTextureColorType: ArrayIndexOutOfBounds index="

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "PaperProtectionFragment"

    invoke-static {v0, p0}, Lmiuix/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return v2
.end method

.method private resetDefault()V
    .locals 4

    invoke-static {}, Lcom/android/settings/MiuiUtils;->supportSmartEyeCare()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/display/PaperProtectionFragment;->mAtuoAdjustState:Z

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/display/PaperProtectionFragment;->paperTempPreference:Lcom/android/settings/display/TemperatureSeekBarPreference;

    sget v1, Lcom/android/settings/display/util/PaperConstants;->DEFAULT_TEXTURE_MODE_LEVEL:F

    sget v2, Lcom/android/settings/display/PaperProtectionFragment;->PAPER_MODE_MIN_LEVEL:F

    sub-float v2, v1, v2

    sget v3, Lcom/android/settings/display/PaperProtectionFragment;->PER_LEVEL:F

    div-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/SeekBarPreference;->setProgress(I)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    float-to-int v1, v1

    const-string/jumbo v2, "screen_paper_texture_level"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_1
    iget-object v0, p0, Lcom/android/settings/display/PaperProtectionFragment;->texturePreference:Lcom/android/settings/widget/MiuiSeekBarPreference;

    sget v1, Lcom/android/settings/display/util/PaperConstants;->DEFAULT_TEXTURE_EYECARE_LEVEL:I

    int-to-float v2, v1

    sget v3, Lcom/android/settings/display/PaperProtectionFragment;->EYECARE_MIN_LEVEL:F

    sub-float/2addr v2, v3

    sget v3, Lcom/android/settings/display/PaperProtectionFragment;->PER_TEXTURE_LEVEL:F

    div-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/SeekBarPreference;->setProgress(I)V

    iget-object v0, p0, Lcom/android/settings/display/PaperProtectionFragment;->colorTypePreference:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lmiuix/preference/DropDownPreference;->setValueIndex(I)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v3, "screen_texture_eyecare_level"

    invoke-static {v0, v3, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "screen_texture_color_type"

    invoke-static {p0, v0, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method private setEyeCareLevel(I)V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/display/PaperProtectionFragment;->getPaperEyeCareLevel()I

    move-result v0

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "previous level "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/android/settings/display/PaperProtectionFragment;->getPaperEyeCareLevel()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", new level "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "setEyeCareLevel "

    invoke-static {v1, v0}, Lmiuix/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "screen_texture_eyecare_level"

    invoke-static {p0, v0, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_0
    return-void
.end method

.method private setPaperModeLevel(I)V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/display/PaperProtectionFragment;->getPaperModeLevel()I

    move-result v0

    if-eq p1, v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "screen_paper_texture_level"

    invoke-static {p0, v0, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_0
    return-void
.end method

.method private setTextureColorType(I)V
    .locals 1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "screen_texture_color_type"

    invoke-static {p0, v0, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method private updatePreference()V
    .locals 2

    invoke-static {}, Lcom/android/settings/MiuiUtils;->supportSmartEyeCare()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/display/PaperProtectionFragment;->mAtuoAdjustState:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/PaperProtectionFragment;->paperTempPreference:Lcom/android/settings/display/TemperatureSeekBarPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/settingslib/RestrictedPreference;->setEnabled(Z)V

    :cond_0
    invoke-static {}, Lcom/android/settings/MiuiUtils;->supportSmartEyeCare()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/display/PaperProtectionFragment;->hintPreference:Lcom/android/settings/widget/PaperModeTipPreference;

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/android/settings/display/PaperProtectionFragment;->mAtuoAdjustState:Z

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setVisible(Z)V

    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/MiuiUtils;->isSecondSpace(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/display/PaperProtectionFragment;->texturePreference:Lcom/android/settings/widget/MiuiSeekBarPreference;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object p0, p0, Lcom/android/settings/display/PaperProtectionFragment;->texturePreference:Lcom/android/settings/widget/MiuiSeekBarPreference;

    invoke-virtual {v0, p0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_2
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    sget p1, Lcom/android/settings/R$xml;->paper_protection_mode:I

    invoke-virtual {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->addPreferencesFromResource(I)V

    const-string p1, "adjust_paper_mode"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/display/TemperatureSeekBarPreference;

    iput-object p1, p0, Lcom/android/settings/display/PaperProtectionFragment;->paperTempPreference:Lcom/android/settings/display/TemperatureSeekBarPreference;

    const/16 v0, 0x3e8

    invoke-virtual {p1, v0}, Lcom/android/settings/widget/SeekBarPreference;->setMax(I)V

    iget-object p1, p0, Lcom/android/settings/display/PaperProtectionFragment;->paperTempPreference:Lcom/android/settings/display/TemperatureSeekBarPreference;

    invoke-direct {p0}, Lcom/android/settings/display/PaperProtectionFragment;->getPaperModeLevel()I

    move-result v1

    int-to-float v1, v1

    sget v2, Lcom/android/settings/display/PaperProtectionFragment;->PAPER_MODE_MIN_LEVEL:F

    sub-float/2addr v1, v2

    sget v2, Lcom/android/settings/display/PaperProtectionFragment;->PER_LEVEL:F

    div-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {p1, v1}, Lcom/android/settings/widget/SeekBarPreference;->setProgress(I)V

    iget-object p1, p0, Lcom/android/settings/display/PaperProtectionFragment;->paperTempPreference:Lcom/android/settings/display/TemperatureSeekBarPreference;

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lcom/android/settings/widget/SeekBarPreference;->setContinuousUpdates(Z)V

    iget-object p1, p0, Lcom/android/settings/display/PaperProtectionFragment;->paperTempPreference:Lcom/android/settings/display/TemperatureSeekBarPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-object p1, p0, Lcom/android/settings/display/PaperProtectionFragment;->paperTempPreference:Lcom/android/settings/display/TemperatureSeekBarPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    const-string p1, "adjust_paper_texture"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/widget/MiuiSeekBarPreference;

    iput-object p1, p0, Lcom/android/settings/display/PaperProtectionFragment;->texturePreference:Lcom/android/settings/widget/MiuiSeekBarPreference;

    invoke-virtual {p1, v0}, Lcom/android/settings/widget/SeekBarPreference;->setMax(I)V

    iget-object p1, p0, Lcom/android/settings/display/PaperProtectionFragment;->texturePreference:Lcom/android/settings/widget/MiuiSeekBarPreference;

    invoke-direct {p0}, Lcom/android/settings/display/PaperProtectionFragment;->getPaperEyeCareLevel()I

    move-result v0

    int-to-float v0, v0

    sget v2, Lcom/android/settings/display/PaperProtectionFragment;->EYECARE_MIN_LEVEL:F

    sub-float/2addr v0, v2

    sget v2, Lcom/android/settings/display/PaperProtectionFragment;->PER_TEXTURE_LEVEL:F

    div-float/2addr v0, v2

    float-to-int v0, v0

    invoke-virtual {p1, v0}, Lcom/android/settings/widget/SeekBarPreference;->setProgress(I)V

    iget-object p1, p0, Lcom/android/settings/display/PaperProtectionFragment;->texturePreference:Lcom/android/settings/widget/MiuiSeekBarPreference;

    invoke-virtual {p1, v1}, Lcom/android/settings/widget/SeekBarPreference;->setContinuousUpdates(Z)V

    iget-object p1, p0, Lcom/android/settings/display/PaperProtectionFragment;->texturePreference:Lcom/android/settings/widget/MiuiSeekBarPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-object p1, p0, Lcom/android/settings/display/PaperProtectionFragment;->texturePreference:Lcom/android/settings/widget/MiuiSeekBarPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    const-string/jumbo p1, "paper_color"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    iput-object p1, p0, Lcom/android/settings/display/PaperProtectionFragment;->colorTypePreference:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-object p1, p0, Lcom/android/settings/display/PaperProtectionFragment;->colorTypePreference:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    invoke-direct {p0}, Lcom/android/settings/display/PaperProtectionFragment;->getTextureColorType()I

    move-result v0

    invoke-virtual {p1, v0}, Lmiuix/preference/DropDownPreference;->setValueIndex(I)V

    const-string/jumbo p1, "paper_reset"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/PaperProtectionFragment;->resetPreference:Landroidx/preference/Preference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    const-string p1, "hint_unadjustable"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/widget/PaperModeTipPreference;

    iput-object p1, p0, Lcom/android/settings/display/PaperProtectionFragment;->hintPreference:Lcom/android/settings/widget/PaperModeTipPreference;

    sget v0, Lcom/android/settings/R$string;->hint_unadjustable_text:I

    invoke-virtual {p1, v0}, Lcom/android/settingslib/miuisettings/preference/Preference;->setTitle(I)V

    iget-object p1, p0, Lcom/android/settings/display/PaperProtectionFragment;->hintPreference:Lcom/android/settings/widget/PaperModeTipPreference;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string/jumbo v2, "screen_auto_adjust"

    invoke-static {p1, v2, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p1

    if-ne p1, v1, :cond_0

    goto :goto_0

    :cond_0
    move v1, v0

    :goto_0
    iput-boolean v1, p0, Lcom/android/settings/display/PaperProtectionFragment;->mAtuoAdjustState:Z

    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    const-string v0, "adjust_paper_texture"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_0

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "eyecare_level"

    invoke-static {v0, p2}, Lmiuix/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    int-to-float p1, p1

    sget p2, Lcom/android/settings/display/PaperProtectionFragment;->PER_TEXTURE_LEVEL:F

    mul-float/2addr p1, p2

    sget p2, Lcom/android/settings/display/PaperProtectionFragment;->EYECARE_MIN_LEVEL:F

    add-float/2addr p1, p2

    float-to-int p1, p1

    invoke-direct {p0, p1}, Lcom/android/settings/display/PaperProtectionFragment;->setEyeCareLevel(I)V

    goto :goto_0

    :cond_0
    const-string v0, "adjust_paper_mode"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string/jumbo v0, "temp_mode_level"

    invoke-static {v0, p2}, Lmiuix/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    int-to-float p1, p1

    sget p2, Lcom/android/settings/display/PaperProtectionFragment;->PER_LEVEL:F

    mul-float/2addr p1, p2

    sget p2, Lcom/android/settings/display/PaperProtectionFragment;->PAPER_MODE_MIN_LEVEL:F

    add-float/2addr p1, p2

    float-to-int p1, p1

    invoke-direct {p0, p1}, Lcom/android/settings/display/PaperProtectionFragment;->setPaperModeLevel(I)V

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "paper_color"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/display/PaperProtectionFragment;->setTextureColorType(I)V

    :cond_2
    :goto_0
    const/4 p0, 0x1

    return p0
.end method

.method public onPreferenceClick(Landroidx/preference/Preference;)Z
    .locals 1

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    const-string/jumbo v0, "paper_reset"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/display/PaperProtectionFragment;->resetDefault()V

    :cond_0
    const/4 p0, 0x1

    return p0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/android/settings/SettingsPreferenceFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/android/settings/display/PaperProtectionFragment;->updatePreference()V

    return-void
.end method
