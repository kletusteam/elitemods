.class public Lcom/android/settings/display/KeepScreenOnAfterFoldingFragment;
.super Lcom/android/settings/SettingsPreferenceFragment;

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mKeepScreenOnAfterFoldingEnable:Landroidx/preference/CheckBoxPreference;

.field private mLargeImage:Landroidx/preference/Preference;

.field private mSmallImage:Landroidx/preference/Preference;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/SettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method private static isKeepScreenOnAfterFoldingEnable(Landroid/content/Context;)Z
    .locals 3

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    const-string v1, "lock_screen_after_fold_screen"

    const/4 v2, 0x0

    invoke-static {p0, v1, v2, v0}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    return p0
.end method

.method private setKeepScreenOnAfterFoldingEnable(Z)V
    .locals 2

    iget-object p0, p0, Lcom/android/settings/display/KeepScreenOnAfterFoldingFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    xor-int/lit8 p1, p1, 0x1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    const-string v1, "lock_screen_after_fold_screen"

    invoke-static {p0, v1, p1, v0}, Landroid/provider/MiuiSettings$System;->putBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/KeepScreenOnAfterFoldingFragment;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$xml;->keep_screen_on_after_folding:I

    invoke-virtual {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->addPreferencesFromResource(I)V

    const-string p1, "lock_screen_after_fold_screen"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/display/KeepScreenOnAfterFoldingFragment;->mKeepScreenOnAfterFoldingEnable:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-object p1, p0, Lcom/android/settings/display/KeepScreenOnAfterFoldingFragment;->mKeepScreenOnAfterFoldingEnable:Landroidx/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/android/settings/display/KeepScreenOnAfterFoldingFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/display/KeepScreenOnAfterFoldingFragment;->isKeepScreenOnAfterFoldingEnable(Landroid/content/Context;)Z

    move-result v0

    invoke-virtual {p1, v0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p1, p0, Lcom/android/settings/display/KeepScreenOnAfterFoldingFragment;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/display/KeepScreenOnAfterFoldingFragment;->isKeepScreenOnAfterFoldingEnable(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/display/KeepScreenOnAfterFoldingFragment;->mKeepScreenOnAfterFoldingEnable:Landroidx/preference/CheckBoxPreference;

    sget v0, Lcom/android/settings/R$string;->keep_screen_on_after_folding_on_summary:I

    invoke-virtual {p1, v0}, Landroidx/preference/TwoStatePreference;->setSummaryOn(I)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/android/settings/display/KeepScreenOnAfterFoldingFragment;->mKeepScreenOnAfterFoldingEnable:Landroidx/preference/CheckBoxPreference;

    sget v0, Lcom/android/settings/R$string;->keep_screen_on_after_folding_off_summary:I

    invoke-virtual {p1, v0}, Landroidx/preference/TwoStatePreference;->setSummaryOff(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const/4 v0, 0x0

    const-string v1, "device_posture"

    invoke-static {p1, v1, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p1

    const-string/jumbo v0, "small_screen_image"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/KeepScreenOnAfterFoldingFragment;->mSmallImage:Landroidx/preference/Preference;

    const-string v0, "large_screen_image"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/KeepScreenOnAfterFoldingFragment;->mLargeImage:Landroidx/preference/Preference;

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object p0, p0, Lcom/android/settings/display/KeepScreenOnAfterFoldingFragment;->mLargeImage:Landroidx/preference/Preference;

    invoke-virtual {p1, p0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object p0, p0, Lcom/android/settings/display/KeepScreenOnAfterFoldingFragment;->mSmallImage:Landroidx/preference/Preference;

    invoke-virtual {p1, p0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :goto_1
    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/KeepScreenOnAfterFoldingFragment;->mKeepScreenOnAfterFoldingEnable:Landroidx/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    invoke-super {p0}, Lcom/android/settingslib/core/lifecycle/ObservablePreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 1

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getListView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->isComputingLayout()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    const-string v0, "lock_screen_after_fold_screen"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/android/settings/display/KeepScreenOnAfterFoldingFragment;->mKeepScreenOnAfterFoldingEnable:Landroidx/preference/CheckBoxPreference;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/android/settings/display/KeepScreenOnAfterFoldingFragment;->mKeepScreenOnAfterFoldingEnable:Landroidx/preference/CheckBoxPreference;

    sget v0, Lcom/android/settings/R$string;->keep_screen_on_after_folding_on_summary:I

    invoke-virtual {p1, v0}, Landroidx/preference/TwoStatePreference;->setSummaryOn(I)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/android/settings/display/KeepScreenOnAfterFoldingFragment;->mKeepScreenOnAfterFoldingEnable:Landroidx/preference/CheckBoxPreference;

    sget v0, Lcom/android/settings/R$string;->keep_screen_on_after_folding_off_summary:I

    invoke-virtual {p1, v0}, Landroidx/preference/TwoStatePreference;->setSummaryOff(I)V

    :goto_0
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/display/KeepScreenOnAfterFoldingFragment;->setKeepScreenOnAfterFoldingEnable(Z)V

    :cond_2
    const/4 p0, 0x1

    return p0
.end method
