.class public Lcom/android/settings/display/DarkUISettingsRadioButtonsController;
.super Ljava/lang/Object;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFooter:Landroidx/preference/Preference;

.field mManager:Landroid/app/UiModeManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroidx/preference/Preference;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-class v0, Landroid/app/UiModeManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/UiModeManager;

    iput-object v0, p0, Lcom/android/settings/display/DarkUISettingsRadioButtonsController;->mManager:Landroid/app/UiModeManager;

    iput-object p2, p0, Lcom/android/settings/display/DarkUISettingsRadioButtonsController;->mFooter:Landroidx/preference/Preference;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/DarkUISettingsRadioButtonsController;->mContext:Landroid/content/Context;

    return-void
.end method

.method public static modeToDescription(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v0, Lcom/android/settings/R$array;->dark_ui_mode_entries:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 p1, 0x1

    aget-object p0, p0, p1

    return-object p0

    :cond_0
    const/4 p1, 0x0

    aget-object p0, p0, p1

    return-object p0
.end method


# virtual methods
.method public getDefaultKey()Ljava/lang/String;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/display/DarkUISettingsRadioButtonsController;->updateFooter()V

    iget-object p0, p0, Lcom/android/settings/display/DarkUISettingsRadioButtonsController;->mContext:Landroid/content/Context;

    invoke-static {p0}, Lcom/android/settings/Utils;->isNightMode(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_0

    const-string p0, "key_dark_ui_settings_dark"

    goto :goto_0

    :cond_0
    const-string p0, "key_dark_ui_settings_light"

    :goto_0
    return-object p0
.end method

.method public setDefaultKey(Ljava/lang/String;)Z
    .locals 3

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    const-string v0, "key_dark_ui_settings_dark"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    const-string v0, "key_dark_ui_settings_light"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/android/settings/display/DarkUISettingsRadioButtonsController;->mManager:Landroid/app/UiModeManager;

    invoke-virtual {p1, v1}, Landroid/app/UiModeManager;->setNightMode(I)V

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not a valid key for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ": "

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object p1, p0, Lcom/android/settings/display/DarkUISettingsRadioButtonsController;->mManager:Landroid/app/UiModeManager;

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/app/UiModeManager;->setNightMode(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/display/DarkUISettingsRadioButtonsController;->updateFooter()V

    return v1
.end method

.method public updateFooter()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/DarkUISettingsRadioButtonsController;->mManager:Landroid/app/UiModeManager;

    invoke-virtual {v0}, Landroid/app/UiModeManager;->getNightMode()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget-object p0, p0, Lcom/android/settings/display/DarkUISettingsRadioButtonsController;->mFooter:Landroidx/preference/Preference;

    sget v0, Lcom/android/settings/R$string;->dark_ui_settings_light_summary:I

    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setSummary(I)V

    :cond_0
    return-void
.end method
