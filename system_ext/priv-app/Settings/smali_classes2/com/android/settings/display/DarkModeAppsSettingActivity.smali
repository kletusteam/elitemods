.class public Lcom/android/settings/display/DarkModeAppsSettingActivity;
.super Lmiuix/appcompat/app/AppCompatActivity;


# instance fields
.field private transaction:Landroidx/fragment/app/FragmentTransaction;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiuix/appcompat/app/AppCompatActivity;-><init>()V

    return-void
.end method

.method private onCreateFragment()Landroidx/fragment/app/Fragment;
    .locals 0

    new-instance p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;

    invoke-direct {p0}, Lcom/android/settings/display/DarkModeAppsSettingFragment;-><init>()V

    return-object p0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    sget p1, Lcom/android/settings/R$string;->more_dark_mode_settings:I

    invoke-virtual {p0, p1}, Landroid/app/Activity;->setTitle(I)V

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onDestroy()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onResume()V

    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/DarkModeAppsSettingActivity;->transaction:Landroidx/fragment/app/FragmentTransaction;

    invoke-direct {p0}, Lcom/android/settings/display/DarkModeAppsSettingActivity;->onCreateFragment()Landroidx/fragment/app/Fragment;

    move-result-object p0

    const v1, 0x1020002

    invoke-virtual {v0, v1, p0}, Landroidx/fragment/app/FragmentTransaction;->replace(ILandroidx/fragment/app/Fragment;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object p0

    invoke-virtual {p0}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    return-void
.end method
