.class public Lcom/android/settings/display/DarkModeAppsSettingFragment;
.super Lcom/android/settings/display/util/BaseFragment;

# interfaces
.implements Lcom/android/settings/display/DarkModeAppsListAdapter$OnAppCheckedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/display/DarkModeAppsSettingFragment$MainHandler;
    }
.end annotation


# instance fields
.field private mAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/display/DarkModeAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mConfirmDialog:Lmiuix/appcompat/app/AlertDialog;

.field private mContext:Landroid/content/Context;

.field private mDarkModeAppCacheManager:Lcom/android/settings/display/util/DarkModeAppCacheManager;

.field private mDarkModeAppsListAdapter:Lcom/android/settings/display/DarkModeAppsListAdapter;

.field private mDarkModeAppsRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private mEmptyView:Landroid/widget/TextView;

.field private mShouldShowConfirmDialog:Z

.field private mStats:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/app/usage/UsageStats;",
            ">;"
        }
    .end annotation
.end field

.field private mUIHandler:Landroid/os/Handler;

.field private mUsageStatsManager:Landroid/app/usage/UsageStatsManager;


# direct methods
.method public static synthetic $r8$lambda$GR_gi-f_qW8EoJopztm-UwbMt8M(Lcom/android/settings/display/DarkModeAppsSettingFragment;Landroid/widget/CompoundButton;Lcom/android/settings/display/DarkModeAppInfo;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settings/display/DarkModeAppsSettingFragment;->lambda$onAppChecked$0(Landroid/widget/CompoundButton;Lcom/android/settings/display/DarkModeAppInfo;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$SMLVoEnDmKHc3MedK3-LlNQjYAs(Lcom/android/settings/display/DarkModeAppsSettingFragment;Landroid/widget/CompoundButton;Lcom/android/settings/display/DarkModeAppInfo;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settings/display/DarkModeAppsSettingFragment;->lambda$onAppChecked$1(Landroid/widget/CompoundButton;Lcom/android/settings/display/DarkModeAppInfo;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$crTdRJ6BWWSbHAaZsHqn2g7eS9c(Lcom/android/settings/display/DarkModeAppsSettingFragment;Landroid/content/DialogInterface;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/display/DarkModeAppsSettingFragment;->lambda$onAppChecked$2(Landroid/content/DialogInterface;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmAppList(Lcom/android/settings/display/DarkModeAppsSettingFragment;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mAppList:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmArray(Lcom/android/settings/display/DarkModeAppsSettingFragment;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mArray:Ljava/util/ArrayList;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDarkModeAppCacheManager(Lcom/android/settings/display/DarkModeAppsSettingFragment;)Lcom/android/settings/display/util/DarkModeAppCacheManager;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mDarkModeAppCacheManager:Lcom/android/settings/display/util/DarkModeAppCacheManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDarkModeAppsListAdapter(Lcom/android/settings/display/DarkModeAppsSettingFragment;)Lcom/android/settings/display/DarkModeAppsListAdapter;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mDarkModeAppsListAdapter:Lcom/android/settings/display/DarkModeAppsListAdapter;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmStats(Lcom/android/settings/display/DarkModeAppsSettingFragment;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mStats:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmArray(Lcom/android/settings/display/DarkModeAppsSettingFragment;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mArray:Ljava/util/ArrayList;

    return-void
.end method

.method static bridge synthetic -$$Nest$mremoveGameFromList(Lcom/android/settings/display/DarkModeAppsSettingFragment;Ljava/util/ArrayList;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/display/DarkModeAppsSettingFragment;->removeGameFromList(Ljava/util/ArrayList;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/display/util/BaseFragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mAppList:Ljava/util/List;

    return-void
.end method

.method private getUsageStats()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/app/usage/UsageStats;",
            ">;"
        }
    .end annotation

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mContext:Landroid/content/Context;

    const-class v1, Landroid/app/usage/UsageStatsManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/usage/UsageStatsManager;

    iput-object v0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mUsageStatsManager:Landroid/app/usage/UsageStatsManager;

    const-wide/32 v1, 0x36ee80

    sub-long v2, v4, v1

    const/4 v1, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/app/usage/UsageStatsManager;->queryUsageStats(IJJ)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private initAppsList()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mUIHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/display/DarkModeAppsSettingFragment$MainHandler;

    invoke-direct {v0, p0}, Lcom/android/settings/display/DarkModeAppsSettingFragment$MainHandler;-><init>(Lcom/android/settings/display/DarkModeAppsSettingFragment;)V

    iput-object v0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mUIHandler:Landroid/os/Handler;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mUIHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/settings/display/DarkModeAppsSettingFragment$1;

    invoke-direct {v1, p0}, Lcom/android/settings/display/DarkModeAppsSettingFragment$1;-><init>(Lcom/android/settings/display/DarkModeAppsSettingFragment;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private synthetic lambda$onAppChecked$0(Landroid/widget/CompoundButton;Lcom/android/settings/display/DarkModeAppInfo;Landroid/content/DialogInterface;I)V
    .locals 0

    const/4 p3, 0x0

    invoke-virtual {p1, p3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object p0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mDarkModeAppsListAdapter:Lcom/android/settings/display/DarkModeAppsListAdapter;

    invoke-virtual {p0, p2, p3}, Lcom/android/settings/display/DarkModeAppsListAdapter;->setAppDarkMode(Lcom/android/settings/display/DarkModeAppInfo;Z)V

    return-void
.end method

.method private synthetic lambda$onAppChecked$1(Landroid/widget/CompoundButton;Lcom/android/settings/display/DarkModeAppInfo;Landroid/content/DialogInterface;I)V
    .locals 0

    const/4 p3, 0x1

    invoke-virtual {p1, p3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object p0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mDarkModeAppsListAdapter:Lcom/android/settings/display/DarkModeAppsListAdapter;

    invoke-virtual {p0, p2, p3}, Lcom/android/settings/display/DarkModeAppsListAdapter;->setAppDarkMode(Lcom/android/settings/display/DarkModeAppInfo;Z)V

    return-void
.end method

.method private synthetic lambda$onAppChecked$2(Landroid/content/DialogInterface;)V
    .locals 3

    iget-object p1, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mConfirmDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {p1}, Lmiuix/appcompat/app/AlertDialog;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const/4 v0, 0x1

    const/4 v1, -0x2

    const-string v2, "dark_mode_show_confirm_dialog"

    invoke-static {p1, v2, v0, v1}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mShouldShowConfirmDialog:Z

    :cond_0
    return-void
.end method

.method private removeGameFromList(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    :goto_0
    if-ltz v0, :cond_1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "removeGameFromList App: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mAppList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/settings/display/DarkModeAppInfo;

    invoke-virtual {v3}, Lcom/android/settings/display/DarkModeAppInfo;->getPkgName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "DarkModeAppsSettingFragment"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mAppList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mDarkModeAppsListAdapter:Lcom/android/settings/display/DarkModeAppsListAdapter;

    iget-object p0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mAppList:Ljava/util/List;

    invoke-virtual {p1, p0}, Lcom/android/settings/display/DarkModeAppsListAdapter;->refreshAppList(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method protected initView()V
    .locals 4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mContext:Landroid/content/Context;

    const v0, 0x1020004

    invoke-virtual {p0, v0}, Lcom/android/settings/display/util/BaseFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mEmptyView:Landroid/widget/TextView;

    sget v0, Lcom/android/settings/R$id;->apps_list:I

    invoke-virtual {p0, v0}, Lcom/android/settings/display/util/BaseFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mDarkModeAppsRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/display/util/DarkModeAppCacheManager;->getInstance(Landroid/content/Context;)Lcom/android/settings/display/util/DarkModeAppCacheManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mDarkModeAppCacheManager:Lcom/android/settings/display/util/DarkModeAppCacheManager;

    invoke-direct {p0}, Lcom/android/settings/display/DarkModeAppsSettingFragment;->getUsageStats()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mStats:Ljava/util/List;

    invoke-direct {p0}, Lcom/android/settings/display/DarkModeAppsSettingFragment;->initAppsList()V

    new-instance v0, Lcom/android/settings/display/DarkModeAppsListAdapter;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mAppList:Ljava/util/List;

    invoke-direct {v0, v1, v2, p0}, Lcom/android/settings/display/DarkModeAppsListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/android/settings/display/DarkModeAppsListAdapter$OnAppCheckedListener;)V

    iput-object v0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mDarkModeAppsListAdapter:Lcom/android/settings/display/DarkModeAppsListAdapter;

    iget-object v1, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mDarkModeAppsRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    iget-object v0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "dark_mode_show_confirm_dialog"

    const/4 v2, 0x0

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v2, 0x1

    :cond_0
    iput-boolean v2, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mShouldShowConfirmDialog:Z

    return-void
.end method

.method public onAppChecked(Landroid/widget/CompoundButton;ZLcom/android/settings/display/DarkModeAppInfo;)V
    .locals 3

    if-eqz p1, :cond_3

    if-nez p3, :cond_0

    goto/16 :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onAppChecked, appInfo:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Lcom/android/settings/display/DarkModeAppInfo;->getPkgName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", isChecked:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DarkModeAppsSettingFragment"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mShouldShowConfirmDialog:Z

    if-eqz v0, :cond_2

    if-eqz p2, :cond_2

    iget-object p2, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mConfirmDialog:Lmiuix/appcompat/app/AlertDialog;

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Landroid/app/Dialog;->isShowing()Z

    move-result p2

    if-eqz p2, :cond_1

    iget-object p2, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mConfirmDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {p2}, Lmiuix/appcompat/app/AlertDialog;->dismiss()V

    :cond_1
    new-instance p2, Lmiuix/appcompat/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mContext:Landroid/content/Context;

    invoke-direct {p2, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setCancelable(Z)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p2

    sget v1, Lcom/android/settings/R$string;->dark_mode_alert_dialog_title:I

    invoke-virtual {p2, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p2

    sget v1, Lcom/android/settings/R$string;->dark_mode_alert_dialog_message:I

    invoke-virtual {p2, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p2

    iget-object v1, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$string;->dark_mode_alert_dialog_checkbox:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setCheckBox(ZLjava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p2

    sget v0, Lcom/android/settings/R$string;->dark_mode_alert_dialog_cancel:I

    new-instance v1, Lcom/android/settings/display/DarkModeAppsSettingFragment$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1, p3}, Lcom/android/settings/display/DarkModeAppsSettingFragment$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/display/DarkModeAppsSettingFragment;Landroid/widget/CompoundButton;Lcom/android/settings/display/DarkModeAppInfo;)V

    invoke-virtual {p2, v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p2

    sget v0, Lcom/android/settings/R$string;->dark_mode_alert_dialog_confirm:I

    new-instance v1, Lcom/android/settings/display/DarkModeAppsSettingFragment$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, p1, p3}, Lcom/android/settings/display/DarkModeAppsSettingFragment$$ExternalSyntheticLambda1;-><init>(Lcom/android/settings/display/DarkModeAppsSettingFragment;Landroid/widget/CompoundButton;Lcom/android/settings/display/DarkModeAppInfo;)V

    invoke-virtual {p2, v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    new-instance p2, Lcom/android/settings/display/DarkModeAppsSettingFragment$$ExternalSyntheticLambda2;

    invoke-direct {p2, p0}, Lcom/android/settings/display/DarkModeAppsSettingFragment$$ExternalSyntheticLambda2;-><init>(Lcom/android/settings/display/DarkModeAppsSettingFragment;)V

    invoke-virtual {p1, p2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mConfirmDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    goto :goto_0

    :cond_2
    invoke-virtual {p1, p2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object p0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mDarkModeAppsListAdapter:Lcom/android/settings/display/DarkModeAppsListAdapter;

    invoke-virtual {p0, p3, p2}, Lcom/android/settings/display/DarkModeAppsListAdapter;->setAppDarkMode(Lcom/android/settings/display/DarkModeAppInfo;Z)V

    :cond_3
    :goto_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/display/util/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onCreateViewLayout()I
    .locals 0

    sget p0, Lcom/android/settings/R$layout;->dark_mode_apps_setting:I

    return p0
.end method

.method protected onCustomizeActionBar(Landroid/app/ActionBar;)I
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onDestroy()V

    iget-object v0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mUIHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;->mUIHandler:Landroid/os/Handler;

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onResume()V

    return-void
.end method
