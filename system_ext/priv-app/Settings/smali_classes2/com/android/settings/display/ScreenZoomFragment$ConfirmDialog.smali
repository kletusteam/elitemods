.class public Lcom/android/settings/display/ScreenZoomFragment$ConfirmDialog;
.super Landroidx/fragment/app/DialogFragment;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/display/ScreenZoomFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ConfirmDialog"
.end annotation


# static fields
.field private static final FRAG_TAG:Ljava/lang/String;


# instance fields
.field private mType:I

.field private mUiMode:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmUiMode(Lcom/android/settings/display/ScreenZoomFragment$ConfirmDialog;)I
    .locals 0

    iget p0, p0, Lcom/android/settings/display/ScreenZoomFragment$ConfirmDialog;->mUiMode:I

    return p0
.end method

.method static bridge synthetic -$$Nest$sfgetFRAG_TAG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/settings/display/ScreenZoomFragment$ConfirmDialog;->FRAG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/settings/display/ScreenZoomFragment$ConfirmDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/display/ScreenZoomFragment$ConfirmDialog;->FRAG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroidx/fragment/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "com.android.settings.display.ScreenZoomFragment:STATE_SCREEN_ZOOM"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/android/settings/display/ScreenZoomFragment$ConfirmDialog;->mUiMode:I

    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    new-instance p1, Lmiuix/appcompat/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p1, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget v0, p0, Lcom/android/settings/display/ScreenZoomFragment$ConfirmDialog;->mType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    sget v0, Lcom/android/settings/R$string;->page_layout_confirm_title_enlarge:I

    goto :goto_0

    :cond_0
    sget v0, Lcom/android/settings/R$string;->page_layout_confirm_title_narrow:I

    :goto_0
    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    iget v0, p0, Lcom/android/settings/display/ScreenZoomFragment$ConfirmDialog;->mType:I

    if-ne v0, v1, :cond_1

    sget v0, Lcom/android/settings/R$string;->page_layout_confirm_message_enlarge:I

    goto :goto_1

    :cond_1
    sget v0, Lcom/android/settings/R$string;->page_layout_confirm_message_narrow:I

    :goto_1
    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    const v0, 0x104000a

    new-instance v1, Lcom/android/settings/display/ScreenZoomFragment$ConfirmDialog$1;

    invoke-direct {v1, p0}, Lcom/android/settings/display/ScreenZoomFragment$ConfirmDialog$1;-><init>(Lcom/android/settings/display/ScreenZoomFragment$ConfirmDialog;)V

    invoke-virtual {p1, v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    const/high16 p0, 0x1040000

    const/4 v0, 0x0

    invoke-virtual {p1, p0, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    invoke-virtual {p1}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object p0

    return-object p0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget p0, p0, Lcom/android/settings/display/ScreenZoomFragment$ConfirmDialog;->mUiMode:I

    const-string v0, "com.android.settings.display.ScreenZoomFragment:STATE_SCREEN_ZOOM"

    invoke-virtual {p1, v0, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public setCurrentUiMode(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/display/ScreenZoomFragment$ConfirmDialog;->mUiMode:I

    return-void
.end method

.method public setType(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/display/ScreenZoomFragment$ConfirmDialog;->mType:I

    return-void
.end method
