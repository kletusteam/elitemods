.class Lcom/android/settings/display/DarkModeAppsListAdapter$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/display/DarkModeAppsListAdapter;->onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/display/DarkModeAppsListAdapter;

.field final synthetic val$pos:I

.field final synthetic val$viewHolder:Lcom/android/settings/display/DarkModeAppsListAdapter$ItemViewHolder;


# direct methods
.method constructor <init>(Lcom/android/settings/display/DarkModeAppsListAdapter;Lcom/android/settings/display/DarkModeAppsListAdapter$ItemViewHolder;I)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/DarkModeAppsListAdapter$1;->this$0:Lcom/android/settings/display/DarkModeAppsListAdapter;

    iput-object p2, p0, Lcom/android/settings/display/DarkModeAppsListAdapter$1;->val$viewHolder:Lcom/android/settings/display/DarkModeAppsListAdapter$ItemViewHolder;

    iput p3, p0, Lcom/android/settings/display/DarkModeAppsListAdapter$1;->val$pos:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lcom/android/settings/display/DarkModeAppsListAdapter$1;->this$0:Lcom/android/settings/display/DarkModeAppsListAdapter;

    invoke-static {p1}, Lcom/android/settings/display/DarkModeAppsListAdapter;->-$$Nest$fgetonItemClickListener(Lcom/android/settings/display/DarkModeAppsListAdapter;)Lcom/android/settings/display/DarkModeAppsListAdapter$OnItemClickListener;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/display/DarkModeAppsListAdapter$1;->val$viewHolder:Lcom/android/settings/display/DarkModeAppsListAdapter$ItemViewHolder;

    iget-object v0, v0, Lcom/android/settings/display/DarkModeAppsListAdapter$ItemViewHolder;->sliding_button:Lmiuix/slidingwidget/widget/SlidingButton;

    iget p0, p0, Lcom/android/settings/display/DarkModeAppsListAdapter$1;->val$pos:I

    invoke-interface {p1, v0, p0}, Lcom/android/settings/display/DarkModeAppsListAdapter$OnItemClickListener;->onItemClick(Lmiuix/slidingwidget/widget/SlidingButton;I)V

    return-void
.end method
