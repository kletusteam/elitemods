.class Lcom/android/settings/display/BrightnessFragment$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/hardware/display/DisplayManager$DisplayListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/display/BrightnessFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/display/BrightnessFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/display/BrightnessFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/BrightnessFragment$1;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDisplayAdded(I)V
    .locals 0

    return-void
.end method

.method public onDisplayChanged(I)V
    .locals 0

    iget-object p1, p0, Lcom/android/settings/display/BrightnessFragment$1;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {p1}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmBackgroundHandler(Lcom/android/settings/display/BrightnessFragment;)Lcom/android/settings/display/BrightnessFragment$BackgroundHandler;

    move-result-object p1

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment$1;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {p0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmUpdateBrightnessSeekBarRunnable(Lcom/android/settings/display/BrightnessFragment;)Ljava/lang/Runnable;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onDisplayRemoved(I)V
    .locals 0

    return-void
.end method
