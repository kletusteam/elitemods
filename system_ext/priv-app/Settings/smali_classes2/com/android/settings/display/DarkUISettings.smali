.class public Lcom/android/settings/display/DarkUISettings;
.super Lcom/android/settings/widget/RadioButtonPickerFragment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/display/DarkUISettings$DarkUISettingsCandidateInfo;
    }
.end annotation


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settingslib/search/Indexable$SearchIndexProvider;


# instance fields
.field private mController:Lcom/android/settings/display/DarkUISettingsRadioButtonsController;

.field private mFooter:Landroidx/preference/Preference;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/display/DarkUISettings$1;

    invoke-direct {v0}, Lcom/android/settings/display/DarkUISettings$1;-><init>()V

    sput-object v0, Lcom/android/settings/display/DarkUISettings;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settingslib/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/widget/RadioButtonPickerFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected addStaticPreferences(Landroidx/preference/PreferenceScreen;)V
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/DarkUISettings;->mFooter:Landroidx/preference/Preference;

    invoke-virtual {p1, p0}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    return-void
.end method

.method protected getCandidates()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "+",
            "Lcom/android/settingslib/widget/CandidateInfo;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/android/settings/display/DarkUISettings$DarkUISettingsCandidateInfo;

    const/4 v2, 0x2

    invoke-static {p0, v2}, Lcom/android/settings/display/DarkUISettingsRadioButtonsController;->modeToDescription(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const-string v4, "key_dark_ui_settings_dark"

    const/4 v5, 0x1

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/android/settings/display/DarkUISettings$DarkUISettingsCandidateInfo;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/display/DarkUISettings$DarkUISettingsCandidateInfo;

    invoke-static {p0, v5}, Lcom/android/settings/display/DarkUISettingsRadioButtonsController;->modeToDescription(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object p0

    const-string v2, "key_dark_ui_settings_light"

    invoke-direct {v1, p0, v3, v2, v5}, Lcom/android/settings/display/DarkUISettings$DarkUISettingsCandidateInfo;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method protected getDefaultKey()Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/DarkUISettings;->mController:Lcom/android/settings/display/DarkUISettingsRadioButtonsController;

    invoke-virtual {p0}, Lcom/android/settings/display/DarkUISettingsRadioButtonsController;->getDefaultKey()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public getMetricsCategory()I
    .locals 0

    const/16 p0, 0x6a2

    return p0
.end method

.method protected getPreferenceScreenResId()I
    .locals 0

    sget p0, Lcom/android/settings/R$xml;->dark_ui_settings:I

    return p0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/widget/RadioButtonPickerFragment;->onAttach(Landroid/content/Context;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0}, Lcom/android/settings/widget/RadioButtonPickerFragment;->setIllustration(II)V

    new-instance v0, Lcom/android/settingslib/widget/FooterPreference;

    invoke-direct {v0, p1}, Lcom/android/settingslib/widget/FooterPreference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/display/DarkUISettings;->mFooter:Landroidx/preference/Preference;

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setIcon(I)V

    new-instance v0, Lcom/android/settings/display/DarkUISettingsRadioButtonsController;

    iget-object v1, p0, Lcom/android/settings/display/DarkUISettings;->mFooter:Landroidx/preference/Preference;

    invoke-direct {v0, p1, v1}, Lcom/android/settings/display/DarkUISettingsRadioButtonsController;-><init>(Landroid/content/Context;Landroidx/preference/Preference;)V

    iput-object v0, p0, Lcom/android/settings/display/DarkUISettings;->mController:Lcom/android/settings/display/DarkUISettingsRadioButtonsController;

    return-void
.end method

.method protected setDefaultKey(Ljava/lang/String;)Z
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/DarkUISettings;->mController:Lcom/android/settings/display/DarkUISettingsRadioButtonsController;

    invoke-virtual {p0, p1}, Lcom/android/settings/display/DarkUISettingsRadioButtonsController;->setDefaultKey(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method
