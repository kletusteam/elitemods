.class public Lcom/android/settings/display/BrightnessSeekBarPreference;
.super Lcom/android/settings/widget/SeekBarPreference;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDisplayId:I

.field private mDisplayManager:Landroid/hardware/display/DisplayManager;

.field private mIsVrModeEnabled:Z

.field mListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mMaximumBrightness:F

.field private mMaximumBrightnessForVr:F

.field private mMinimumBrightness:F

.field private mMinimumBrightnessForVr:F

.field private mSeekBar:Landroid/widget/SeekBar;

.field private mSeekBarAnimator:Landroid/animation/ValueAnimator;

.field private mVrManager:Landroid/service/vr/IVrManager;


# direct methods
.method public static synthetic $r8$lambda$X9You57Js8s6S3Y84oM03kO_68Y(Lcom/android/settings/display/BrightnessSeekBarPreference;Landroid/animation/ValueAnimator;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/display/BrightnessSeekBarPreference;->lambda$animateSeekBarTo$0(Landroid/animation/ValueAnimator;)V

    return-void
.end method

.method public static synthetic $r8$lambda$eSGzO1QNzITw4RiLr_AKK6QSpIw(Lcom/android/settings/display/BrightnessSeekBarPreference;Landroid/animation/ValueAnimator;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/display/BrightnessSeekBarPreference;->lambda$animateSeekBarTo$1(Landroid/animation/ValueAnimator;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmSeekBarAnimator(Lcom/android/settings/display/BrightnessSeekBarPreference;)Landroid/animation/ValueAnimator;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mSeekBarAnimator:Landroid/animation/ValueAnimator;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$msetBrightness(Lcom/android/settings/display/BrightnessSeekBarPreference;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/display/BrightnessSeekBarPreference;->setBrightness(IZ)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/settings/display/BrightnessSeekBarPreference;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/display/BrightnessSeekBarPreference;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/display/BrightnessSeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/display/BrightnessSeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/widget/SeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance p2, Lcom/android/settings/display/BrightnessSeekBarPreference$1;

    invoke-direct {p2, p0}, Lcom/android/settings/display/BrightnessSeekBarPreference$1;-><init>(Lcom/android/settings/display/BrightnessSeekBarPreference;)V

    iput-object p2, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    sget p2, Lcom/android/settingslib/display/BrightnessUtils;->GAMMA_SPACE_MAX:I

    invoke-virtual {p0, p2}, Lcom/android/settings/widget/SeekBarPreference;->setMax(I)V

    sget p2, Lcom/android/settings/R$layout;->preference_brightness_seekbar:I

    invoke-virtual {p0, p2}, Landroidx/preference/Preference;->setLayoutResource(I)V

    iput-object p1, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/android/settings/display/BrightnessSeekBarPreference;->initValue()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/display/BrightnessSeekBarPreference;IZ)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/widget/SeekBarPreference;->setProgress(IZ)V

    return-void
.end method

.method private animateSeekBarTo(I)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mSeekBarAnimator:Landroid/animation/ValueAnimator;

    const/4 v1, 0x1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mSeekBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1, v1}, Lcom/android/settings/widget/SeekBarPreference;->setProgress(IZ)V

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mSeekBarAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mSeekBarAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mSeekBar:Landroid/widget/SeekBar;

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-eqz v0, :cond_3

    new-array v3, v3, [I

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    aput v0, v3, v2

    aput p1, v3, v1

    invoke-static {v3}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mSeekBarAnimator:Landroid/animation/ValueAnimator;

    new-instance v0, Lcom/android/settings/display/BrightnessSeekBarPreference$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/android/settings/display/BrightnessSeekBarPreference$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/display/BrightnessSeekBarPreference;)V

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    goto :goto_1

    :cond_3
    new-array v0, v3, [I

    invoke-virtual {p0}, Lcom/android/settings/widget/SeekBarPreference;->getProgress()I

    move-result v3

    aput v3, v0, v2

    aput p1, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mSeekBarAnimator:Landroid/animation/ValueAnimator;

    new-instance v0, Lcom/android/settings/display/BrightnessSeekBarPreference$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/android/settings/display/BrightnessSeekBarPreference$$ExternalSyntheticLambda1;-><init>(Lcom/android/settings/display/BrightnessSeekBarPreference;)V

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    :goto_1
    iget-object p1, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mSeekBarAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v0, 0x5dc

    invoke-virtual {p1, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object p0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mSeekBarAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {p0}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method

.method private initValue()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mContext:Landroid/content/Context;

    const-class v1, Landroid/os/PowerManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mMinimumBrightness:F

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mMaximumBrightness:F

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->getBrightnessConstraint(I)F

    move-result v1

    iput v1, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mMinimumBrightnessForVr:F

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->getBrightnessConstraint(I)F

    move-result v0

    iput v0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mMaximumBrightnessForVr:F

    iget-object v0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "display"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    iput-object v0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    const-string/jumbo v0, "vrmanager"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/service/vr/IVrManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/service/vr/IVrManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mVrManager:Landroid/service/vr/IVrManager;

    iget-object v0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getDisplayId()I

    move-result v0

    iput v0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mDisplayId:I

    iget-object v0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mVrManager:Landroid/service/vr/IVrManager;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0}, Landroid/service/vr/IVrManager;->getVrModeState()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mIsVrModeEnabled:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    sget-object v0, Lcom/android/settings/display/BrightnessSeekBarPreference;->TAG:Ljava/lang/String;

    const-string v1, "Failed to register VR mode state listener: "

    invoke-static {v0, v1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    return-void
.end method

.method private synthetic lambda$animateSeekBarTo$0(Landroid/animation/ValueAnimator;)V
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    return-void
.end method

.method private synthetic lambda$animateSeekBarTo$1(Landroid/animation/ValueAnimator;)V
    .locals 0

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/android/settings/widget/SeekBarPreference;->setProgress(I)V

    return-void
.end method

.method private setBrightness(IZ)V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mIsVrModeEnabled:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mMinimumBrightnessForVr:F

    iget v1, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mMaximumBrightnessForVr:F

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mMinimumBrightness:F

    iget v1, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mMaximumBrightness:F

    :goto_0
    invoke-static {p1, v0, v1}, Lcom/android/settingslib/display/BrightnessUtils;->convertGammaToLinearFloat(IFF)F

    move-result p1

    invoke-static {p1, v1}, Landroid/util/MathUtils;->min(FF)F

    move-result p1

    if-nez p2, :cond_1

    iget-object p2, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    iget p0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mDisplayId:I

    invoke-virtual {p2, p0, p1}, Landroid/hardware/display/DisplayManager;->setTemporaryBrightness(IF)V

    goto :goto_1

    :cond_1
    iget-object p2, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    iget v0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mDisplayId:I

    invoke-virtual {p2, v0}, Landroid/hardware/display/DisplayManager;->getBrightness(I)F

    move-result p2

    cmpl-float p2, p1, p2

    if-nez p2, :cond_2

    iget-object p1, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    iget p0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mDisplayId:I

    const/high16 p2, 0x7fc00000    # Float.NaN

    invoke-virtual {p1, p0, p2}, Landroid/hardware/display/DisplayManager;->setTemporaryBrightness(IF)V

    goto :goto_1

    :cond_2
    iget-object p2, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    iget p0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mDisplayId:I

    invoke-virtual {p2, p0, p1}, Landroid/hardware/display/DisplayManager;->setBrightness(IF)V

    :goto_1
    return-void
.end method


# virtual methods
.method public hasIcon()Z
    .locals 0

    const/4 p0, 0x1

    return p0
.end method

.method public onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/android/settings/widget/SeekBarPreference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0, v2, v1}, Landroid/view/View;->setPaddingRelative(IIII)V

    sget v0, Lcom/android/settings/R$id;->seekbar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mSeekBar:Landroid/widget/SeekBar;

    sget v0, Lcom/android/settings/R$id;->title_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getPaddingStart()I

    move-result p1

    iget-object v0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getPaddingStart()I

    move-result v0

    sub-int/2addr p1, v0

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/android/settings/R$dimen;->brightness_seekbar_margin_bottom:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, p1, v2, p1, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMarginsRelative(IIII)V

    iget-object p1, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p1, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mSeekBar:Landroid/widget/SeekBar;

    sget v0, Lcom/android/settingslib/display/BrightnessUtils;->GAMMA_SPACE_MAX:I

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object p1, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mSeekBar:Landroid/widget/SeekBar;

    iget-object p0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {p1, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    return-void
.end method

.method public updateBrightnessSeekBar(FZFF)V
    .locals 0

    iput p3, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mMinimumBrightness:F

    iput p4, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mMaximumBrightness:F

    if-eqz p2, :cond_0

    iget p3, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mMinimumBrightnessForVr:F

    :cond_0
    if-eqz p2, :cond_1

    iget p4, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mMaximumBrightnessForVr:F

    :cond_1
    iget-object p2, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mSeekBarAnimator:Landroid/animation/ValueAnimator;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/widget/SeekBarPreference;->getProgress()I

    move-result p2

    invoke-static {p2, p3, p4}, Lcom/android/settingslib/display/BrightnessUtils;->convertGammaToLinearFloat(IFF)F

    move-result p2

    invoke-static {p1, p2}, Lcom/android/internal/display/BrightnessSynchronizer;->floatEquals(FF)Z

    move-result p2

    if-eqz p2, :cond_3

    iget-object p2, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mSeekBar:Landroid/widget/SeekBar;

    if-eqz p2, :cond_3

    return-void

    :cond_3
    invoke-static {p1, p3, p4}, Lcom/android/settingslib/display/BrightnessUtils;->convertLinearToGammaFloat(FFF)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/display/BrightnessSeekBarPreference;->animateSeekBarTo(I)V

    return-void
.end method

.method public updateVrMode(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mIsVrModeEnabled:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/android/settings/display/BrightnessSeekBarPreference;->mIsVrModeEnabled:Z

    :cond_0
    return-void
.end method
