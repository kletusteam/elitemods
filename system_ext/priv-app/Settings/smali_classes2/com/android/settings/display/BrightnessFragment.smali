.class public Lcom/android/settings/display/BrightnessFragment;
.super Lcom/android/settings/SettingsPreferenceFragment;

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/display/BrightnessFragment$BackgroundHandler;,
        Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;
    }
.end annotation


# instance fields
.field private mAutoAdjustModeEnable:Landroidx/preference/CheckBoxPreference;

.field private mAutomaticAvailable:Z

.field private volatile mAutomaticBrightnessEnabled:Z

.field private mBackgroundHandler:Lcom/android/settings/display/BrightnessFragment$BackgroundHandler;

.field private mBinder:Landroid/os/IBinder;

.field private mBrightnessModeGroup:Landroidx/preference/PreferenceGroup;

.field private mBrightnessObserver:Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;

.field private mBrightnessSeekBarPreference:Lcom/android/settings/display/BrightnessSeekBarPreference;

.field private mContext:Landroid/content/Context;

.field private final mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

.field private mDisplayManager:Landroid/hardware/display/DisplayManager;

.field private final mHandler:Landroid/os/Handler;

.field private mHighBrightnessHintSummary:Landroidx/preference/Preference;

.field private mIsOdinInternal:Z

.field private volatile mIsVrModeEnabled:Z

.field private mMaximumBrightness:F

.field private mMaximumBrightnessForVr:F

.field private mMinimumBrightness:F

.field private mMinimumBrightnessForVr:F

.field private mSmoothLightModeAvailable:Z

.field private mSmoothLightModeEnable:Landroidx/preference/CheckBoxPreference;

.field private final mStartListeningRunnable:Ljava/lang/Runnable;

.field private final mStopListeningRunnable:Ljava/lang/Runnable;

.field private mSunlightModeAvailable:Z

.field private mSunlightModeEnable:Landroidx/preference/CheckBoxPreference;

.field private mToast:Landroid/widget/Toast;

.field private final mUpdateBrightnessSeekBarRunnable:Ljava/lang/Runnable;

.field private final mUpdateModeRunnable:Ljava/lang/Runnable;

.field private mVrManager:Landroid/service/vr/IVrManager;

.field private final mVrStateCallbacks:Landroid/service/vr/IVrStateCallbacks;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAutomaticAvailable(Lcom/android/settings/display/BrightnessFragment;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/display/BrightnessFragment;->mAutomaticAvailable:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmAutomaticBrightnessEnabled(Lcom/android/settings/display/BrightnessFragment;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/display/BrightnessFragment;->mAutomaticBrightnessEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmBackgroundHandler(Lcom/android/settings/display/BrightnessFragment;)Lcom/android/settings/display/BrightnessFragment$BackgroundHandler;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment;->mBackgroundHandler:Lcom/android/settings/display/BrightnessFragment$BackgroundHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmBrightnessObserver(Lcom/android/settings/display/BrightnessFragment;)Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment;->mBrightnessObserver:Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmBrightnessSeekBarPreference(Lcom/android/settings/display/BrightnessFragment;)Lcom/android/settings/display/BrightnessSeekBarPreference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment;->mBrightnessSeekBarPreference:Lcom/android/settings/display/BrightnessSeekBarPreference;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/settings/display/BrightnessFragment;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDisplayListener(Lcom/android/settings/display/BrightnessFragment;)Landroid/hardware/display/DisplayManager$DisplayListener;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDisplayManager(Lcom/android/settings/display/BrightnessFragment;)Landroid/hardware/display/DisplayManager;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/settings/display/BrightnessFragment;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsVrModeEnabled(Lcom/android/settings/display/BrightnessFragment;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/display/BrightnessFragment;->mIsVrModeEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmMaximumBrightness(Lcom/android/settings/display/BrightnessFragment;)F
    .locals 0

    iget p0, p0, Lcom/android/settings/display/BrightnessFragment;->mMaximumBrightness:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmMinimumBrightness(Lcom/android/settings/display/BrightnessFragment;)F
    .locals 0

    iget p0, p0, Lcom/android/settings/display/BrightnessFragment;->mMinimumBrightness:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmUpdateBrightnessSeekBarRunnable(Lcom/android/settings/display/BrightnessFragment;)Ljava/lang/Runnable;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment;->mUpdateBrightnessSeekBarRunnable:Ljava/lang/Runnable;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmUpdateModeRunnable(Lcom/android/settings/display/BrightnessFragment;)Ljava/lang/Runnable;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment;->mUpdateModeRunnable:Ljava/lang/Runnable;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmVrManager(Lcom/android/settings/display/BrightnessFragment;)Landroid/service/vr/IVrManager;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment;->mVrManager:Landroid/service/vr/IVrManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmVrStateCallbacks(Lcom/android/settings/display/BrightnessFragment;)Landroid/service/vr/IVrStateCallbacks;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment;->mVrStateCallbacks:Landroid/service/vr/IVrStateCallbacks;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmAutomaticBrightnessEnabled(Lcom/android/settings/display/BrightnessFragment;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/display/BrightnessFragment;->mAutomaticBrightnessEnabled:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsVrModeEnabled(Lcom/android/settings/display/BrightnessFragment;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/display/BrightnessFragment;->mIsVrModeEnabled:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmMaximumBrightness(Lcom/android/settings/display/BrightnessFragment;F)V
    .locals 0

    iput p1, p0, Lcom/android/settings/display/BrightnessFragment;->mMaximumBrightness:F

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmMinimumBrightness(Lcom/android/settings/display/BrightnessFragment;F)V
    .locals 0

    iput p1, p0, Lcom/android/settings/display/BrightnessFragment;->mMinimumBrightness:F

    return-void
.end method

.method static bridge synthetic -$$Nest$misAutoBrightnessEnabled(Lcom/android/settings/display/BrightnessFragment;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/display/BrightnessFragment;->isAutoBrightnessEnabled()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$misSunlightModeEnabled(Lcom/android/settings/display/BrightnessFragment;)Z
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/display/BrightnessFragment;->isSunlightModeEnabled()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mupdateAutomaticBrightnessMode(Lcom/android/settings/display/BrightnessFragment;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/display/BrightnessFragment;->updateAutomaticBrightnessMode(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateSunlightMode(Lcom/android/settings/display/BrightnessFragment;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/display/BrightnessFragment;->updateSunlightMode(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateVrMode(Lcom/android/settings/display/BrightnessFragment;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/display/BrightnessFragment;->updateVrMode(Z)V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/SettingsPreferenceFragment;-><init>()V

    const-string/jumbo v0, "support_backlight_bit_switch"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/display/BrightnessFragment;->mSmoothLightModeAvailable:Z

    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string/jumbo v2, "odin"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    iput-boolean v1, p0, Lcom/android/settings/display/BrightnessFragment;->mIsOdinInternal:Z

    new-instance v0, Lcom/android/settings/display/BrightnessFragment$1;

    invoke-direct {v0, p0}, Lcom/android/settings/display/BrightnessFragment$1;-><init>(Lcom/android/settings/display/BrightnessFragment;)V

    iput-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    new-instance v0, Lcom/android/settings/display/BrightnessFragment$2;

    invoke-direct {v0, p0}, Lcom/android/settings/display/BrightnessFragment$2;-><init>(Lcom/android/settings/display/BrightnessFragment;)V

    iput-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mUpdateModeRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/settings/display/BrightnessFragment$3;

    invoke-direct {v0, p0}, Lcom/android/settings/display/BrightnessFragment$3;-><init>(Lcom/android/settings/display/BrightnessFragment;)V

    iput-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mUpdateBrightnessSeekBarRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/settings/display/BrightnessFragment$4;

    invoke-direct {v0, p0}, Lcom/android/settings/display/BrightnessFragment$4;-><init>(Lcom/android/settings/display/BrightnessFragment;)V

    iput-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mVrStateCallbacks:Landroid/service/vr/IVrStateCallbacks;

    new-instance v0, Lcom/android/settings/display/BrightnessFragment$5;

    invoke-direct {v0, p0}, Lcom/android/settings/display/BrightnessFragment$5;-><init>(Lcom/android/settings/display/BrightnessFragment;)V

    iput-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mStartListeningRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/settings/display/BrightnessFragment$6;

    invoke-direct {v0, p0}, Lcom/android/settings/display/BrightnessFragment$6;-><init>(Lcom/android/settings/display/BrightnessFragment;)V

    iput-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mStopListeningRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/settings/display/BrightnessFragment$7;

    invoke-direct {v0, p0}, Lcom/android/settings/display/BrightnessFragment$7;-><init>(Lcom/android/settings/display/BrightnessFragment;)V

    iput-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private isAutoBrightnessEnabled()Z
    .locals 2

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "screen_brightness_mode"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    if-eqz p0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method private isLowPowerLevel()Z
    .locals 2

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "low_power_level_state"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    if-eqz p0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method private isSunlightModeEnabled()Z
    .locals 2

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "sunlight_mode"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    if-eqz p0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method private resetAutoBrightnessShortModel()V
    .locals 4

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    :try_start_0
    const-string v2, "android.view.android.hardware.display.IDisplayManager"

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment;->mBinder:Landroid/os/IBinder;

    const v2, 0xfffffe

    const/4 v3, 0x0

    invoke-interface {p0, v2, v0, v1, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    goto :goto_1

    :catchall_0
    move-exception p0

    goto :goto_2

    :catch_0
    :try_start_1
    const-string p0, "BrightnessFragment"

    const-string v2, "RemoteException!"

    invoke-static {p0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :goto_1
    return-void

    :goto_2
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw p0
.end method

.method private setAutomaticBrightnessMode(I)V
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/display/BrightnessFragment;->resetAutoBrightnessShortModel()V

    :cond_0
    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "screen_brightness_mode"

    invoke-static {p0, v0, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method private updateAutomaticBrightnessMode(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mAutoAdjustModeEnable:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-boolean v0, p0, Lcom/android/settings/display/BrightnessFragment;->mSunlightModeAvailable:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mBrightnessModeGroup:Landroidx/preference/PreferenceGroup;

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->mSunlightModeEnable:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/settings/display/BrightnessFragment;->updateHighBrightnessHintSummary(Z)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mBrightnessModeGroup:Landroidx/preference/PreferenceGroup;

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->mSunlightModeEnable:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    invoke-direct {p0}, Lcom/android/settings/display/BrightnessFragment;->isSunlightModeEnabled()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/display/BrightnessFragment;->updateSunlightMode(Z)V

    :cond_1
    :goto_0
    invoke-direct {p0, p1}, Lcom/android/settings/display/BrightnessFragment;->setAutomaticBrightnessMode(I)V

    return-void
.end method

.method private updateHighBrightnessHintSummary(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/display/BrightnessFragment;->mIsOdinInternal:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment;->mHighBrightnessHintSummary:Landroidx/preference/Preference;

    invoke-virtual {p1, p0}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment;->mHighBrightnessHintSummary:Landroidx/preference/Preference;

    invoke-virtual {p1, p0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_1
    :goto_0
    return-void
.end method

.method private updateSmoothLightMode(Z)V
    .locals 2

    const-string/jumbo v0, "persist.vendor.light.bit.switch"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment;->mSmoothLightModeEnable:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p0, p1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateSmoothLightMode: isChecked: "

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", current status: "

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "BrightnessFragment"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lmiui/hardware/display/DisplayFeatureManager;->getInstance()Lmiui/hardware/display/DisplayFeatureManager;

    move-result-object p0

    const/16 v0, 0x2a

    invoke-virtual {p0, v0, p1}, Lmiui/hardware/display/DisplayFeatureManager;->setScreenEffect(II)V

    return-void
.end method

.method private updateSunlightMode(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mSunlightModeEnable:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v0, p1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    invoke-direct {p0, p1}, Lcom/android/settings/display/BrightnessFragment;->updateHighBrightnessHintSummary(Z)V

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "sunlight_mode"

    invoke-static {p0, v0, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method private updateVrMode(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/display/BrightnessFragment;->mIsVrModeEnabled:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/android/settings/display/BrightnessFragment;->mIsVrModeEnabled:Z

    iget-object p1, p0, Lcom/android/settings/display/BrightnessFragment;->mBackgroundHandler:Lcom/android/settings/display/BrightnessFragment$BackgroundHandler;

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment;->mUpdateBrightnessSeekBarRunnable:Ljava/lang/Runnable;

    invoke-virtual {p1, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$xml;->brightness_settings:I

    invoke-virtual {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->addPreferencesFromResource(I)V

    new-instance p1, Landroid/os/HandlerThread;

    const-string v0, "BrightnessFragment"

    const/16 v1, 0xa

    invoke-direct {p1, v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p1}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Lcom/android/settings/display/BrightnessFragment$BackgroundHandler;

    invoke-virtual {p1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object p1

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/android/settings/display/BrightnessFragment$BackgroundHandler;-><init>(Lcom/android/settings/display/BrightnessFragment;Landroid/os/Looper;Lcom/android/settings/display/BrightnessFragment$BackgroundHandler-IA;)V

    iput-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mBackgroundHandler:Lcom/android/settings/display/BrightnessFragment$BackgroundHandler;

    const-string p1, "brightness"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/display/BrightnessSeekBarPreference;

    iput-object p1, p0, Lcom/android/settings/display/BrightnessFragment;->mBrightnessSeekBarPreference:Lcom/android/settings/display/BrightnessSeekBarPreference;

    sget v0, Lcom/android/settings/R$drawable;->sun_brightness_icon:I

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setIcon(I)V

    const-string p1, "brightness_mode_group"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/PreferenceGroup;

    iput-object p1, p0, Lcom/android/settings/display/BrightnessFragment;->mBrightnessModeGroup:Landroidx/preference/PreferenceGroup;

    const-string p1, "brightness_auto_mode_enable"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/display/BrightnessFragment;->mAutoAdjustModeEnable:Landroidx/preference/CheckBoxPreference;

    const-string p1, "brightness_sunlight_mode_enable"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/display/BrightnessFragment;->mSunlightModeEnable:Landroidx/preference/CheckBoxPreference;

    const-string p1, "brightness_smooth_mode_enable"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/display/BrightnessFragment;->mSmoothLightModeEnable:Landroidx/preference/CheckBoxPreference;

    const-string p1, "high_brightness_hint_summary"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/BrightnessFragment;->mHighBrightnessHintSummary:Landroidx/preference/Preference;

    const-string p1, "config_sunlight_mode_available"

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/settings/display/BrightnessFragment;->mSunlightModeAvailable:Z

    iget-object p1, p0, Lcom/android/settings/display/BrightnessFragment;->mAutoAdjustModeEnable:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-object p1, p0, Lcom/android/settings/display/BrightnessFragment;->mSunlightModeEnable:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-boolean p1, p0, Lcom/android/settings/display/BrightnessFragment;->mSmoothLightModeAvailable:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/display/BrightnessFragment;->mSmoothLightModeEnable:Landroidx/preference/CheckBoxPreference;

    const/4 v0, 0x0

    const-string/jumbo v1, "persist.vendor.light.bit.switch"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p1, v0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p1, p0, Lcom/android/settings/display/BrightnessFragment;->mSmoothLightModeEnable:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/android/settings/display/BrightnessFragment;->mBrightnessModeGroup:Landroidx/preference/PreferenceGroup;

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mSmoothLightModeEnable:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :goto_0
    iget-boolean p1, p0, Lcom/android/settings/display/BrightnessFragment;->mSunlightModeAvailable:Z

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/android/settings/display/BrightnessFragment;->mBrightnessModeGroup:Landroidx/preference/PreferenceGroup;

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mSunlightModeEnable:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_1
    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mHighBrightnessHintSummary:Landroidx/preference/Preference;

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    iget-object p1, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    const-class v0, Landroid/os/PowerManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/os/PowerManager;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/display/BrightnessFragment;->mMinimumBrightness:F

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/android/settings/display/BrightnessFragment;->mMaximumBrightness:F

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Landroid/os/PowerManager;->getBrightnessConstraint(I)F

    move-result v0

    iput v0, p0, Lcom/android/settings/display/BrightnessFragment;->mMinimumBrightnessForVr:F

    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Landroid/os/PowerManager;->getBrightnessConstraint(I)F

    move-result p1

    iput p1, p0, Lcom/android/settings/display/BrightnessFragment;->mMaximumBrightnessForVr:F

    const-string/jumbo p1, "vrmanager"

    invoke-static {p1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object p1

    invoke-static {p1}, Landroid/service/vr/IVrManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/service/vr/IVrManager;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/BrightnessFragment;->mVrManager:Landroid/service/vr/IVrManager;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x11050001

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/settings/display/BrightnessFragment;->mAutomaticAvailable:Z

    new-instance p1, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mHandler:Landroid/os/Handler;

    invoke-direct {p1, p0, v0}, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;-><init>(Lcom/android/settings/display/BrightnessFragment;Landroid/os/Handler;)V

    iput-object p1, p0, Lcom/android/settings/display/BrightnessFragment;->mBrightnessObserver:Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;

    iget-object p1, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    const-class v0, Landroid/hardware/display/DisplayManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/hardware/display/DisplayManager;

    iput-object p1, p0, Lcom/android/settings/display/BrightnessFragment;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    const-string p1, "display"

    invoke-static {p1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/BrightnessFragment;->mBinder:Landroid/os/IBinder;

    invoke-direct {p0}, Lcom/android/settings/display/BrightnessFragment;->isAutoBrightnessEnabled()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/display/BrightnessFragment;->updateAutomaticBrightnessMode(Z)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mAutoAdjustModeEnable:Landroidx/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mSunlightModeEnable:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mBackgroundHandler:Lcom/android/settings/display/BrightnessFragment$BackgroundHandler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    invoke-super {p0}, Lcom/android/settingslib/core/lifecycle/ObservablePreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mBackgroundHandler:Lcom/android/settings/display/BrightnessFragment$BackgroundHandler;

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->mStopListeningRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mBackgroundHandler:Lcom/android/settings/display/BrightnessFragment$BackgroundHandler;

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->mStopListeningRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onPause()V

    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getListView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->isComputingLayout()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    const-string v0, "brightness_auto_mode_enable"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/display/BrightnessFragment;->updateAutomaticBrightnessMode(Z)V

    goto :goto_0

    :cond_1
    const-string v0, "brightness_sunlight_mode_enable"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-direct {p0}, Lcom/android/settings/display/BrightnessFragment;->isLowPowerLevel()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/android/settings/display/BrightnessFragment;->mToast:Landroid/widget/Toast;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/widget/Toast;->cancel()V

    :cond_2
    iget-object p1, p0, Lcom/android/settings/display/BrightnessFragment;->mContext:Landroid/content/Context;

    sget p2, Lcom/android/settings/R$string;->sunlight_low_power_notification:I

    invoke-static {p1, p2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/BrightnessFragment;->mToast:Landroid/widget/Toast;

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return v1

    :cond_3
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/display/BrightnessFragment;->updateSunlightMode(Z)V

    goto :goto_0

    :cond_4
    const-string v0, "brightness_smooth_mode_enable"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/display/BrightnessFragment;->updateSmoothLightMode(Z)V

    :cond_5
    :goto_0
    const/4 p0, 0x1

    return p0
.end method

.method public onResume()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mBackgroundHandler:Lcom/android/settings/display/BrightnessFragment$BackgroundHandler;

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->mStartListeningRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment;->mBackgroundHandler:Lcom/android/settings/display/BrightnessFragment$BackgroundHandler;

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment;->mStartListeningRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onResume()V

    return-void
.end method
