.class Lcom/android/settings/display/DarkModeAppsSettingFragment$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/display/DarkModeAppsSettingFragment;->initAppsList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/display/DarkModeAppsSettingFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/display/DarkModeAppsSettingFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment$1;->this$0:Lcom/android/settings/display/DarkModeAppsSettingFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    iget-object v0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment$1;->this$0:Lcom/android/settings/display/DarkModeAppsSettingFragment;

    invoke-static {v0}, Lcom/android/settings/display/DarkModeAppsSettingFragment;->-$$Nest$fgetmDarkModeAppCacheManager(Lcom/android/settings/display/DarkModeAppsSettingFragment;)Lcom/android/settings/display/util/DarkModeAppCacheManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/display/util/DarkModeAppCacheManager;->getDarkModeAppInfoList()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment$1;->this$0:Lcom/android/settings/display/DarkModeAppsSettingFragment;

    invoke-static {v1}, Lcom/android/settings/display/DarkModeAppsSettingFragment;->-$$Nest$fgetmAppList(Lcom/android/settings/display/DarkModeAppsSettingFragment;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/darkmode/DarkModeAppDetailInfo;

    new-instance v2, Lcom/android/settings/display/DarkModeAppInfo;

    invoke-direct {v2, v1}, Lcom/android/settings/display/DarkModeAppInfo;-><init>(Lcom/miui/darkmode/DarkModeAppDetailInfo;)V

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment$1;->this$0:Lcom/android/settings/display/DarkModeAppsSettingFragment;

    invoke-static {v3}, Lcom/android/settings/display/DarkModeAppsSettingFragment;->-$$Nest$fgetmStats(Lcom/android/settings/display/DarkModeAppsSettingFragment;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/usage/UsageStats;

    invoke-virtual {v4}, Landroid/app/usage/UsageStats;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/android/settings/display/DarkModeAppInfo;->getPkgName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Landroid/app/usage/UsageStats;->getLastTimeUsed()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/android/settings/display/DarkModeAppInfo;->setLastTimeUsed(J)V

    iget-object v3, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment$1;->this$0:Lcom/android/settings/display/DarkModeAppsSettingFragment;

    invoke-static {v3}, Lcom/android/settings/display/DarkModeAppsSettingFragment;->-$$Nest$fgetmStats(Lcom/android/settings/display/DarkModeAppsSettingFragment;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    :goto_2
    iget-object v1, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment$1;->this$0:Lcom/android/settings/display/DarkModeAppsSettingFragment;

    invoke-static {v1}, Lcom/android/settings/display/DarkModeAppsSettingFragment;->-$$Nest$fgetmAppList(Lcom/android/settings/display/DarkModeAppsSettingFragment;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment$1;->this$0:Lcom/android/settings/display/DarkModeAppsSettingFragment;

    invoke-static {v0}, Lcom/android/settings/display/DarkModeAppsSettingFragment;->-$$Nest$fgetmDarkModeAppsListAdapter(Lcom/android/settings/display/DarkModeAppsSettingFragment;)Lcom/android/settings/display/DarkModeAppsListAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment$1;->this$0:Lcom/android/settings/display/DarkModeAppsSettingFragment;

    invoke-static {v1}, Lcom/android/settings/display/DarkModeAppsSettingFragment;->-$$Nest$fgetmAppList(Lcom/android/settings/display/DarkModeAppsSettingFragment;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/display/DarkModeAppsListAdapter;->refreshAppList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment$1;->this$0:Lcom/android/settings/display/DarkModeAppsSettingFragment;

    invoke-static {v0}, Lcom/android/settings/display/DarkModeAppsSettingFragment;->-$$Nest$fgetmDarkModeAppsListAdapter(Lcom/android/settings/display/DarkModeAppsSettingFragment;)Lcom/android/settings/display/DarkModeAppsListAdapter;

    move-result-object v0

    new-instance v1, Lcom/android/settings/display/DarkModeAppsSettingFragment$1$1;

    invoke-direct {v1, p0}, Lcom/android/settings/display/DarkModeAppsSettingFragment$1$1;-><init>(Lcom/android/settings/display/DarkModeAppsSettingFragment$1;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/display/DarkModeAppsListAdapter;->setOnItemClickListener(Lcom/android/settings/display/DarkModeAppsListAdapter$OnItemClickListener;)V

    return-void
.end method
