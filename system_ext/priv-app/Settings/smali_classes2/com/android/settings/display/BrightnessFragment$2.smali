.class Lcom/android/settings/display/BrightnessFragment$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/display/BrightnessFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/display/BrightnessFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/display/BrightnessFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/BrightnessFragment$2;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment$2;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {v0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmAutomaticAvailable(Lcom/android/settings/display/BrightnessFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment$2;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {v0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$misAutoBrightnessEnabled(Lcom/android/settings/display/BrightnessFragment;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fputmAutomaticBrightnessEnabled(Lcom/android/settings/display/BrightnessFragment;Z)V

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment$2;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {v0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmHandler(Lcom/android/settings/display/BrightnessFragment;)Landroid/os/Handler;

    move-result-object v0

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment$2;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {p0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmAutomaticBrightnessEnabled(Lcom/android/settings/display/BrightnessFragment;)Z

    move-result p0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p0, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object p0

    invoke-virtual {p0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method
