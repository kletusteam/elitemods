.class Lcom/android/settings/display/BrightnessFragment$7;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/display/BrightnessFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/display/BrightnessFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/display/BrightnessFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/BrightnessFragment$7;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_5

    if-eq v0, v2, :cond_3

    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment$7;->this$0:Lcom/android/settings/display/BrightnessFragment;

    iget v3, p1, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_1

    move v3, v2

    goto :goto_0

    :cond_1
    move v3, v1

    :goto_0
    invoke-static {v0, v3}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$mupdateVrMode(Lcom/android/settings/display/BrightnessFragment;Z)V

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment$7;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {p0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmBrightnessSeekBarPreference(Lcom/android/settings/display/BrightnessFragment;)Lcom/android/settings/display/BrightnessSeekBarPreference;

    move-result-object p0

    iget p1, p1, Landroid/os/Message;->arg1:I

    if-eqz p1, :cond_2

    move v1, v2

    :cond_2
    invoke-virtual {p0, v1}, Lcom/android/settings/display/BrightnessSeekBarPreference;->updateVrMode(Z)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment$7;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {v0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmBrightnessSeekBarPreference(Lcom/android/settings/display/BrightnessFragment;)Lcom/android/settings/display/BrightnessSeekBarPreference;

    move-result-object v0

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    iget p1, p1, Landroid/os/Message;->arg2:I

    if-eqz p1, :cond_4

    move v1, v2

    :cond_4
    iget-object p1, p0, Lcom/android/settings/display/BrightnessFragment$7;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {p1}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmMinimumBrightness(Lcom/android/settings/display/BrightnessFragment;)F

    move-result p1

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment$7;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {p0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmMaximumBrightness(Lcom/android/settings/display/BrightnessFragment;)F

    move-result p0

    invoke-virtual {v0, v3, v1, p1, p0}, Lcom/android/settings/display/BrightnessSeekBarPreference;->updateBrightnessSeekBar(FZFF)V

    goto :goto_1

    :cond_5
    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment$7;->this$0:Lcom/android/settings/display/BrightnessFragment;

    iget p1, p1, Landroid/os/Message;->arg1:I

    if-eqz p1, :cond_6

    move v1, v2

    :cond_6
    invoke-static {p0, v1}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$mupdateAutomaticBrightnessMode(Lcom/android/settings/display/BrightnessFragment;Z)V

    :goto_1
    return-void
.end method
