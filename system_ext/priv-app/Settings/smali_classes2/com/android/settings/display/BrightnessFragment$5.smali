.class Lcom/android/settings/display/BrightnessFragment$5;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/display/BrightnessFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/display/BrightnessFragment;


# direct methods
.method constructor <init>(Lcom/android/settings/display/BrightnessFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/BrightnessFragment$5;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment$5;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {v0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmVrManager(Lcom/android/settings/display/BrightnessFragment;)Landroid/service/vr/IVrManager;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment$5;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {v0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmVrManager(Lcom/android/settings/display/BrightnessFragment;)Landroid/service/vr/IVrManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/BrightnessFragment$5;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {v1}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmVrStateCallbacks(Lcom/android/settings/display/BrightnessFragment;)Landroid/service/vr/IVrStateCallbacks;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/service/vr/IVrManager;->registerListener(Landroid/service/vr/IVrStateCallbacks;)V

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment$5;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {v0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmVrManager(Lcom/android/settings/display/BrightnessFragment;)Landroid/service/vr/IVrManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/service/vr/IVrManager;->getVrModeState()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fputmIsVrModeEnabled(Lcom/android/settings/display/BrightnessFragment;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "BrightnessFragment"

    const-string v2, "Failed to register VR mode state listener: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment$5;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {v0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmBrightnessObserver(Lcom/android/settings/display/BrightnessFragment;)Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/display/BrightnessFragment$BrightnessObserver;->startObserving()V

    iget-object v0, p0, Lcom/android/settings/display/BrightnessFragment$5;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {v0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmUpdateModeRunnable(Lcom/android/settings/display/BrightnessFragment;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    iget-object p0, p0, Lcom/android/settings/display/BrightnessFragment$5;->this$0:Lcom/android/settings/display/BrightnessFragment;

    invoke-static {p0}, Lcom/android/settings/display/BrightnessFragment;->-$$Nest$fgetmUpdateBrightnessSeekBarRunnable(Lcom/android/settings/display/BrightnessFragment;)Ljava/lang/Runnable;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    return-void
.end method
