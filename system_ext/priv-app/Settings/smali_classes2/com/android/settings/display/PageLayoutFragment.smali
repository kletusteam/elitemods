.class public Lcom/android/settings/display/PageLayoutFragment;
.super Lcom/android/settings/BaseFragment;

# interfaces
.implements Lcom/android/settings/display/FontSizeAdjustView$FontSizeChangeListener;
.implements Lcom/android/settings/display/FontWeightAdjustView$FontWeightChangeListener;
.implements Lcom/android/settings/display/FontSizeAdjustView$RecommendListener;
.implements Lcom/android/settings/display/FontAdapter$FontSelectListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/display/PageLayoutFragment$DownloadRunnable;,
        Lcom/android/settings/display/PageLayoutFragment$FontUpdateHandler;
    }
.end annotation


# static fields
.field private static LOCAL_FONT_SP:Ljava/lang/String;

.field private static MAX_FONT_COUNT:I

.field private static final MIUI_VERSION_CODE:I

.field public static final MIUI_WGHT:[I

.field protected static final PAGE_LAYOUT_SIZE:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final PAGE_LAYOUT_TITLE:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static RECOMMEND_HIDE:I

.field private static RECOMMEND_SHOW:I

.field public static final SYSTEM_FONTS_MIUI_EX_REGULAR_TTF:Ljava/lang/String;


# instance fields
.field private fontAdapter:Lcom/android/settings/display/FontAdapter;

.field private fontWeightLinearLayout:Landroid/view/View;

.field final isPrimaryUser:Z

.field private isTalkbackMode:Z

.field private localFontModelList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/display/LocalFontModel;",
            ">;"
        }
    .end annotation
.end field

.field private mAdjustView:Lcom/android/settings/display/FontSizeAdjustView;

.field private mCacheResTitle:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCurrentFont:Lcom/android/settings/display/LocalFontModel;

.field private mCurrentFontId:Ljava/lang/String;

.field private mCurrentFontPos:I

.field protected mCurrentLevel:I

.field private mFontBubbleLeftTv:Landroid/widget/TextView;

.field private mFontBubbleRightTv:Landroid/widget/TextView;

.field private mFontHintTv:Landroid/widget/TextView;

.field protected mFontWeightAdjustView:Lcom/android/settings/display/FontWeightAdjustView;

.field private mHander:Lcom/android/settings/display/PageLayoutFragment$FontUpdateHandler;

.field private mLanProMiui13FontIsExists:Z

.field private mLastFontId:Ljava/lang/String;

.field private mLastFontPos:I

.field private mLastFontWeight:I

.field private mLastProgress:I

.field private mRecommendLayout:Landroid/view/View;

.field private mRootView:Landroid/view/View;

.field private mTypefaceCache:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Landroid/graphics/Typeface;",
            ">;"
        }
    .end annotation
.end field

.field final myUserId:I

.field private originFontModelList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/display/LocalFontModel;",
            ">;"
        }
    .end annotation
.end field

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private scrollViewCard:Lcom/android/settings/display/FontSettingsScrollView;


# direct methods
.method public static synthetic $r8$lambda$5zHr6tBm3HPIo_qAIe3Bn3JtyQY(Lcom/android/settings/display/PageLayoutFragment;[ZLandroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/display/PageLayoutFragment;->lambda$applyFont$6([ZLandroid/content/Context;)V

    return-void
.end method

.method public static synthetic $r8$lambda$RYEKtCb-N9FhbUCVmchntj-kTZA(Lcom/android/settings/display/PageLayoutFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/display/PageLayoutFragment;->lambda$tryBuildRecommendLayout$1()V

    return-void
.end method

.method public static synthetic $r8$lambda$W2qOhJwj5uZXVWdDDZRX8c2Q3_g(Lcom/android/settings/display/PageLayoutFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/display/PageLayoutFragment;->lambda$onActivityResult$4()V

    return-void
.end method

.method public static synthetic $r8$lambda$Y67E9LdAItJHPOgXG4g37ZljjEY(Lcom/android/settings/display/PageLayoutFragment;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/display/PageLayoutFragment;->lambda$getFonts$5(Landroid/content/Context;)V

    return-void
.end method

.method public static synthetic $r8$lambda$lqYWi1cZ2u53nK0M9dCWL02ajLM(Lcom/android/settings/display/PageLayoutFragment;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/display/PageLayoutFragment;->lambda$tryBuildRecommendLayout$0(Ljava/util/List;)V

    return-void
.end method

.method public static synthetic $r8$lambda$nGKyWRoQHbR4pEjJyL6tkGBk7w4(Lcom/android/settings/display/PageLayoutFragment;Ljava/lang/Exception;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/display/PageLayoutFragment;->lambda$applyFont$7(Ljava/lang/Exception;Landroid/content/Context;)V

    return-void
.end method

.method public static synthetic $r8$lambda$sPbb-M88A6P0WR5AlFc4D0Am2hA(Lcom/android/settings/display/PageLayoutFragment;Ljava/lang/String;IILandroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settings/display/PageLayoutFragment;->lambda$applyFont$8(Ljava/lang/String;IILandroid/content/Context;)V

    return-void
.end method

.method public static synthetic $r8$lambda$tHeciEZ6cGX6g5dh6wSzWKfg3VA(Lcom/android/settings/display/PageLayoutFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/display/PageLayoutFragment;->lambda$showThemeCtaComfirmDialog$3(Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$vQw9fHPNmGbXd9BOiXHba0uO6No(Lcom/android/settings/display/PageLayoutFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/display/PageLayoutFragment;->lambda$showThemeCtaComfirmDialog$2(Landroid/content/DialogInterface;I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetfontAdapter(Lcom/android/settings/display/PageLayoutFragment;)Lcom/android/settings/display/FontAdapter;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->fontAdapter:Lcom/android/settings/display/FontAdapter;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetfontWeightLinearLayout(Lcom/android/settings/display/PageLayoutFragment;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->fontWeightLinearLayout:Landroid/view/View;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetlocalFontModelList(Lcom/android/settings/display/PageLayoutFragment;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->localFontModelList:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/settings/display/PageLayoutFragment;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCurrentFont(Lcom/android/settings/display/PageLayoutFragment;)Lcom/android/settings/display/LocalFontModel;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFont:Lcom/android/settings/display/LocalFontModel;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCurrentFontId(Lcom/android/settings/display/PageLayoutFragment;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFontId:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLanProMiui13FontIsExists(Lcom/android/settings/display/PageLayoutFragment;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/display/PageLayoutFragment;->mLanProMiui13FontIsExists:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastFontWeight(Lcom/android/settings/display/PageLayoutFragment;)I
    .locals 0

    iget p0, p0, Lcom/android/settings/display/PageLayoutFragment;->mLastFontWeight:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmRecommendLayout(Lcom/android/settings/display/PageLayoutFragment;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->mRecommendLayout:Landroid/view/View;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetoriginFontModelList(Lcom/android/settings/display/PageLayoutFragment;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->originFontModelList:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetrecyclerView(Lcom/android/settings/display/PageLayoutFragment;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetscrollViewCard(Lcom/android/settings/display/PageLayoutFragment;)Lcom/android/settings/display/FontSettingsScrollView;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->scrollViewCard:Lcom/android/settings/display/FontSettingsScrollView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputfontAdapter(Lcom/android/settings/display/PageLayoutFragment;Lcom/android/settings/display/FontAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->fontAdapter:Lcom/android/settings/display/FontAdapter;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputlocalFontModelList(Lcom/android/settings/display/PageLayoutFragment;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->localFontModelList:Ljava/util/List;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmCurrentFont(Lcom/android/settings/display/PageLayoutFragment;Lcom/android/settings/display/LocalFontModel;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFont:Lcom/android/settings/display/LocalFontModel;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmCurrentFontId(Lcom/android/settings/display/PageLayoutFragment;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFontId:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLanProMiui13FontIsExists(Lcom/android/settings/display/PageLayoutFragment;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mLanProMiui13FontIsExists:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastFontId(Lcom/android/settings/display/PageLayoutFragment;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mLastFontId:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputoriginFontModelList(Lcom/android/settings/display/PageLayoutFragment;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->originFontModelList:Ljava/util/List;

    return-void
.end method

.method static bridge synthetic -$$Nest$mcompareOldAndNewFontList(Lcom/android/settings/display/PageLayoutFragment;Ljava/util/List;Ljava/util/List;)Z
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/display/PageLayoutFragment;->compareOldAndNewFontList(Ljava/util/List;Ljava/util/List;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$msetAllTextByCustomSize(Lcom/android/settings/display/PageLayoutFragment;Landroid/view/View;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/display/PageLayoutFragment;->setAllTextByCustomSize(Landroid/view/View;I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetLocalFontModelListCacahe(Lcom/android/settings/display/PageLayoutFragment;Ljava/util/List;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/display/PageLayoutFragment;->setLocalFontModelListCacahe(Ljava/util/List;Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateBubbleAndHintText(Lcom/android/settings/display/PageLayoutFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/display/PageLayoutFragment;->updateBubbleAndHintText()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetMAX_FONT_COUNT()I
    .locals 1

    sget v0, Lcom/android/settings/display/PageLayoutFragment;->MAX_FONT_COUNT:I

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetRECOMMEND_HIDE()I
    .locals 1

    sget v0, Lcom/android/settings/display/PageLayoutFragment;->RECOMMEND_HIDE:I

    return v0
.end method

.method static bridge synthetic -$$Nest$smgetFontTitle()I
    .locals 1

    invoke-static {}, Lcom/android/settings/display/PageLayoutFragment;->getFontTitle()I

    move-result v0

    return v0
.end method

.method static constructor <clinit>()V
    .locals 12

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lcom/android/settings/display/PageLayoutFragment;->PAGE_LAYOUT_TITLE:Ljava/util/LinkedHashMap;

    sget-boolean v1, Lcom/android/settings/RegionUtils;->IS_JP_KDDI:Z

    const/16 v2, 0xd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0xb

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/16 v4, 0xf

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/16 v5, 0xe

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/16 v7, 0xc

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/16 v8, 0xa

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    if-eqz v1, :cond_0

    sget v1, Lcom/android/settings/R$string;->layout_size_small:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v9, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v1, Lcom/android/settings/R$string;->layout_size_normal:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v7, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v1, Lcom/android/settings/R$string;->layout_size_medium:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v6, v10}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v1, Lcom/android/settings/R$string;->layout_size_large:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v1, Lcom/android/settings/R$string;->layout_size_huge:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v1, Lcom/android/settings/R$string;->layout_size_exhuge:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    sget v1, Lcom/android/settings/R$string;->layout_size_extral_small:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v9, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v1, Lcom/android/settings/R$string;->layout_size_small:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v7, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v1, Lcom/android/settings/R$string;->layout_size_normal:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v1, Lcom/android/settings/R$string;->layout_size_large:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v1, Lcom/android/settings/R$string;->layout_size_huge:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v1, Lcom/android/settings/R$string;->layout_size_exhuge:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    const/4 v0, 0x0

    sput v0, Lcom/android/settings/display/PageLayoutFragment;->RECOMMEND_SHOW:I

    const/4 v1, 0x4

    sput v1, Lcom/android/settings/display/PageLayoutFragment;->RECOMMEND_HIDE:I

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/android/settings/display/PageLayoutFragment;->PAGE_LAYOUT_SIZE:Ljava/util/HashMap;

    const-string/jumbo v10, "ro.miui.ui.font.mi_font_path"

    const-string v11, "/system/fonts/MiLanProVF.ttf"

    invoke-static {v10, v11}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    sput-object v10, Lcom/android/settings/display/PageLayoutFragment;->SYSTEM_FONTS_MIUI_EX_REGULAR_TTF:Ljava/lang/String;

    new-array v8, v8, [I

    fill-array-data v8, :array_0

    sput-object v8, Lcom/android/settings/display/PageLayoutFragment;->MIUI_WGHT:[I

    sget v8, Lcom/android/settings/R$dimen;->page_layout_extral_small_size:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v1, v9, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v8, Lcom/android/settings/R$dimen;->page_layout_small_size:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v7, Lcom/android/settings/R$dimen;->page_layout_normal_size:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v6, Lcom/android/settings/R$dimen;->page_layout_medium_size:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v2, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v2, Lcom/android/settings/R$dimen;->page_layout_large_size:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v2, Lcom/android/settings/R$dimen;->page_layout_huge_size:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v2, Lcom/android/settings/R$dimen;->page_layout_godzilla_size:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x9

    sput v1, Lcom/android/settings/display/PageLayoutFragment;->MAX_FONT_COUNT:I

    const-string/jumbo v1, "ro.miui.ui.version.code"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/settings/display/PageLayoutFragment;->MIUI_VERSION_CODE:I

    const-string v0, "LOCAL_FONT_SP"

    sput-object v0, Lcom/android/settings/display/PageLayoutFragment;->LOCAL_FONT_SP:Ljava/lang/String;

    return-void

    :array_0
    .array-data 4
        0x96
        0xc8
        0xfa
        0x131
        0x154
        0x190
        0x1e0
        0x21c
        0x276
        0x2bc
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/BaseFragment;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mCacheResTitle:Ljava/util/HashMap;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/display/PageLayoutFragment;->isTalkbackMode:Z

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mLastProgress:I

    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mTypefaceCache:Landroid/util/SparseArray;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    iput v1, p0, Lcom/android/settings/display/PageLayoutFragment;->myUserId:I

    const/4 v2, 0x1

    if-nez v1, :cond_0

    move v0, v2

    :cond_0
    iput-boolean v0, p0, Lcom/android/settings/display/PageLayoutFragment;->isPrimaryUser:Z

    iput-boolean v2, p0, Lcom/android/settings/display/PageLayoutFragment;->mLanProMiui13FontIsExists:Z

    new-instance v0, Lcom/android/settings/display/PageLayoutFragment$FontUpdateHandler;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {v0, v1}, Lcom/android/settings/display/PageLayoutFragment$FontUpdateHandler;-><init>(Ljava/lang/ref/WeakReference;)V

    iput-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mHander:Lcom/android/settings/display/PageLayoutFragment$FontUpdateHandler;

    return-void
.end method

.method private addChildViewForRecommendLayout(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/recommend/bean/RecommendItem;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mRootView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->line_layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    invoke-virtual {v0, v2, v1}, Landroid/widget/LinearLayout;->removeViews(II)V

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/settings/recommend/bean/RecommendItem;

    invoke-virtual {v3}, Lcom/android/settings/recommend/bean/RecommendItem;->getTargetPageTitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v3}, Lcom/android/settings/recommend/bean/RecommendItem;->getTargetPageTitle()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/settings/display/PageLayoutFragment;->mCacheResTitle:Ljava/util/HashMap;

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    const-string/jumbo v6, "tryBuildRecommendLayout: Uri parse fail or recommendLayout addVew fail"

    const-string v7, "PageLayoutFragment"

    if-nez v5, :cond_0

    iget-object v4, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v3}, Lcom/android/settings/recommend/bean/RecommendItem;->getTargetPageTitle()Ljava/lang/String;

    move-result-object v5

    iget-object v8, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v9, "string"

    invoke-virtual {v4, v5, v9, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    iget-object v5, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    :try_start_0
    invoke-virtual {v3}, Lcom/android/settings/recommend/bean/RecommendItem;->getIntent()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v1}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/android/settings/display/PageLayoutFragment;->addRecommendView(Ljava/lang/CharSequence;Landroid/content/Intent;)Landroid/widget/RelativeLayout;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v5, p0, Lcom/android/settings/display/PageLayoutFragment;->mCacheResTitle:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/android/settings/recommend/bean/RecommendItem;->getTargetPageTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    invoke-static {v7, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_0
    :try_start_1
    iget-object v5, p0, Lcom/android/settings/display/PageLayoutFragment;->mCacheResTitle:Ljava/util/HashMap;

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v3}, Lcom/android/settings/recommend/bean/RecommendItem;->getIntent()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v4, v3}, Lcom/android/settings/display/PageLayoutFragment;->addRecommendView(Ljava/lang/CharSequence;Landroid/content/Intent;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    invoke-static {v7, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_2
    return-void
.end method

.method private adjustCurrentLevelIfNeed()Z
    .locals 3

    sget-object v0, Lcom/android/settings/display/PageLayoutFragment;->PAGE_LAYOUT_SIZE:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v2, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentLevel:I

    if-ne v2, v1, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_1
    const/4 p0, 0x1

    :goto_0
    return p0
.end method

.method private compareOldAndNewFont(Lcom/android/settings/display/LocalFontModel;Lcom/android/settings/display/LocalFontModel;)Z
    .locals 3

    const/4 p0, 0x1

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/android/settings/display/LocalFontModel;->getId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/android/settings/display/LocalFontModel;->getContentUri()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/android/settings/display/LocalFontModel;->getTitle()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Lcom/android/settings/display/LocalFontModel;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/android/settings/display/LocalFontModel;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/android/settings/display/LocalFontModel;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/android/settings/display/LocalFontModel;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/android/settings/display/LocalFontModel;->getContentUri()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/android/settings/display/LocalFontModel;->getContentUri()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/android/settings/display/LocalFontModel;->isUsing()Z

    move-result v0

    invoke-virtual {p2}, Lcom/android/settings/display/LocalFontModel;->isUsing()Z

    move-result v2

    if-ne v0, v2, :cond_2

    invoke-virtual {p1}, Lcom/android/settings/display/LocalFontModel;->isVariable()Z

    move-result v0

    invoke-virtual {p2}, Lcom/android/settings/display/LocalFontModel;->isVariable()Z

    move-result v2

    if-ne v0, v2, :cond_2

    invoke-virtual {p1}, Lcom/android/settings/display/LocalFontModel;->getFontWeight()Ljava/util/List;

    move-result-object p1

    if-nez p1, :cond_1

    invoke-virtual {p2}, Lcom/android/settings/display/LocalFontModel;->getFontWeight()Ljava/util/List;

    move-result-object p1

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    move p0, v1

    :goto_0
    return p0

    :cond_2
    return v1

    :cond_3
    :goto_1
    return p0
.end method

.method private compareOldAndNewFontList(Ljava/util/List;Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/display/LocalFontModel;",
            ">;",
            "Ljava/util/List<",
            "Lcom/android/settings/display/LocalFontModel;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p1, :cond_4

    if-nez p2, :cond_0

    goto :goto_1

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-eq v1, v2, :cond_1

    return v0

    :cond_1
    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_3

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/display/LocalFontModel;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/settings/display/LocalFontModel;

    invoke-direct {p0, v2, v3}, Lcom/android/settings/display/PageLayoutFragment;->compareOldAndNewFont(Lcom/android/settings/display/LocalFontModel;Lcom/android/settings/display/LocalFontModel;)Z

    move-result v2

    if-nez v2, :cond_2

    return v0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    const/4 p0, 0x1

    return p0

    :cond_4
    :goto_1
    return v0
.end method

.method private completeHintText(Landroid/widget/TextView;II)V
    .locals 3

    if-eqz p1, :cond_4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/settings/display/PageLayoutFragment;->PAGE_LAYOUT_TITLE:Ljava/util/LinkedHashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    return-void

    :cond_1
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v1, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-boolean p2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez p2, :cond_3

    const/16 p2, 0x32

    if-eq p3, p2, :cond_3

    if-ge p3, p2, :cond_2

    sget p2, Lcom/android/settings/R$string;->weight_light:I

    goto :goto_0

    :cond_2
    sget p2, Lcom/android/settings/R$string;->weight_heavy:I

    :goto_0
    const-string p3, " "

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    :goto_1
    return-void
.end method

.method private deleteRecommendFile()V
    .locals 1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object p0

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "/"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo p0, "recommend.json"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_0
    return-void
.end method

.method private getCurrentFontId()Ljava/lang/String;
    .locals 2

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    sget-object v0, Lcom/android/settings/display/PageLayoutFragment;->LOCAL_FONT_SP:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p0

    const-string v0, "current_font_id"

    const-string v1, "10"

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getFontList(Landroid/content/Context;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/settings/display/LocalFontModel;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v1, "content://com.android.thememanager.theme_provider"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "getFonts"

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v2, v3, v3}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object p0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    const-string/jumbo v1, "result"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getFonts json:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PageLayoutFragment"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p0, :cond_1

    return-object v0

    :cond_1
    invoke-static {p0}, Lcom/android/settings/display/PageLayoutFragment;->getFontsResult(Ljava/lang/String;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private static getFontTitle()I
    .locals 2

    sget v0, Lcom/android/settings/display/PageLayoutFragment;->MIUI_VERSION_CODE:I

    const/16 v1, 0xd

    if-lt v0, v1, :cond_0

    sget v0, Lcom/android/settings/R$string;->MiSans_title:I

    goto :goto_0

    :cond_0
    sget v0, Lcom/android/settings/R$string;->xiaomi_lanting_title:I

    :goto_0
    return v0
.end method

.method private static getFontsResult(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/settings/display/LocalFontModel;",
            ">;"
        }
    .end annotation

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-ge p0, v2, :cond_1

    :try_start_1
    invoke-virtual {v1, p0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {v2}, Lcom/android/settings/display/font/FontModel2JsonUtils;->Json2LocalFont(Lorg/json/JSONObject;)Lcom/android/settings/display/LocalFontModel;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    :try_start_2
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :goto_1
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    :catch_1
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    return-object v0
.end method

.method private getLocalFontModelListCacahe()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/android/settings/display/LocalFontModel;",
            ">;"
        }
    .end annotation

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    sget-object v0, Lcom/android/settings/display/PageLayoutFragment;->LOCAL_FONT_SP:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p0

    const-string v0, "local_font_list"

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/android/settings/display/PageLayoutFragment;->getFontsResult(Ljava/lang/String;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private synthetic lambda$applyFont$6([ZLandroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    aget-boolean v0, p1, v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object p2, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFontId:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/settings/display/PageLayoutFragment;->mLastFontId:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->originFontModelList:Ljava/util/List;

    invoke-direct {p0, v0, p2}, Lcom/android/settings/display/PageLayoutFragment;->setLocalFontModelListCacahe(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/display/PageLayoutFragment;->modifyFontSizeAndWeight()V

    sget v0, Lcom/android/settings/R$string;->toast_apply_font_fail:I

    invoke-static {p2, v0, v1}, Lcom/android/settingslib/util/ToastUtil;->show(Landroid/content/Context;II)V

    :goto_0
    aget-boolean p2, p1, v1

    if-eqz p2, :cond_1

    const/4 p2, 0x2

    aget-boolean p1, p1, p2

    if-nez p1, :cond_2

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/display/PageLayoutFragment;->modifyFontSizeAndWeight()V

    :cond_2
    return-void
.end method

.method private synthetic lambda$applyFont$7(Ljava/lang/Exception;Landroid/content/Context;)V
    .locals 2

    const-string v0, "PageLayoutFragment"

    const-string v1, "applyFont: "

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    sget p1, Lcom/android/settings/R$string;->toast_apply_font_fail:I

    const/4 v0, 0x1

    invoke-static {p2, p1, v0}, Lcom/android/settingslib/util/ToastUtil;->show(Landroid/content/Context;II)V

    invoke-direct {p0}, Lcom/android/settings/display/PageLayoutFragment;->modifyFontSizeAndWeight()V

    return-void
.end method

.method private synthetic lambda$applyFont$8(Ljava/lang/String;IILandroid/content/Context;)V
    .locals 8

    const-string v0, "PageLayoutFragment"

    const/4 v1, 0x3

    :try_start_0
    new-array v2, v1, [Z

    const/4 v3, 0x0

    aput-boolean v3, v2, v3

    const/4 v4, 0x1

    aput-boolean v3, v2, v4

    const/4 v5, 0x2

    aput-boolean v3, v2, v5

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    const-string v7, "fontId"

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "fontScale"

    invoke-virtual {v6, p1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p1, "fontWeight"

    invoke-virtual {v6, p1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-virtual {p4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string p2, "content://com.android.thememanager.theme_provider"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    const-string p3, "applyFont"

    const/4 v7, 0x0

    invoke-virtual {p1, p2, p3, v7, v6}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object p1

    const-string p2, "applyResult"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p2

    aput-boolean p2, v2, v3

    const-string p2, "applyFontScale"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p2

    aput-boolean p2, v2, v4

    const-string p2, "applyFontWeight"

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    aput-boolean p1, v2, v5

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "applyFont: applyResult -- "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-boolean p2, v2, v3

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p2, "; applyFontScale -- "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-boolean p2, v2, v4

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p2, "; applyFontWeight -- "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-boolean p2, v2, v5

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    move p2, v3

    :goto_0
    if-ge p2, v1, :cond_0

    :try_start_2
    aput-boolean v3, v2, p2

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    const-string p2, "applyFont call: "

    invoke-static {v0, p2, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_1
    new-instance p1, Lcom/android/settings/display/PageLayoutFragment$$ExternalSyntheticLambda6;

    invoke-direct {p1, p0, v2, p4}, Lcom/android/settings/display/PageLayoutFragment$$ExternalSyntheticLambda6;-><init>(Lcom/android/settings/display/PageLayoutFragment;[ZLandroid/content/Context;)V

    invoke-static {p1}, Lcom/android/settingslib/utils/ThreadUtils;->postOnMainThread(Ljava/lang/Runnable;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :catch_1
    move-exception p1

    new-instance p2, Lcom/android/settings/display/PageLayoutFragment$$ExternalSyntheticLambda7;

    invoke-direct {p2, p0, p1, p4}, Lcom/android/settings/display/PageLayoutFragment$$ExternalSyntheticLambda7;-><init>(Lcom/android/settings/display/PageLayoutFragment;Ljava/lang/Exception;Landroid/content/Context;)V

    invoke-static {p2}, Lcom/android/settingslib/utils/ThreadUtils;->postOnMainThread(Ljava/lang/Runnable;)V

    :goto_2
    return-void
.end method

.method private synthetic lambda$getFonts$5(Landroid/content/Context;)V
    .locals 3

    :try_start_0
    invoke-static {p1}, Lcom/android/settings/display/PageLayoutFragment;->getFontList(Landroid/content/Context;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v2, "fontList"

    invoke-virtual {v1, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->mHander:Lcom/android/settings/display/PageLayoutFragment$FontUpdateHandler;

    invoke-virtual {p0, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method private synthetic lambda$onActivityResult$4()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->localFontModelList:Ljava/util/List;

    iget v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFontPos:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/display/LocalFontModel;

    invoke-direct {p0, v0}, Lcom/android/settings/display/PageLayoutFragment;->onFontChange(Lcom/android/settings/display/LocalFontModel;)V

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->fontAdapter:Lcom/android/settings/display/FontAdapter;

    iget p0, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFontPos:I

    int-to-long v1, p0

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/display/FontAdapter;->setCurrentIndex(J)V

    return-void
.end method

.method private synthetic lambda$showThemeCtaComfirmDialog$2(Landroid/content/DialogInterface;I)V
    .locals 0

    :try_start_0
    new-instance p1, Landroid/content/Intent;

    const-string/jumbo p2, "miui.thememanager.SHOW_CTA"

    invoke-direct {p1, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string p2, "com.android.thememanager"

    invoke-virtual {p1, p2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/16 p2, 0xc8

    invoke-virtual {p0, p1, p2}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "jump to theme error: "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "PageLayoutFragment"

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method private synthetic lambda$showThemeCtaComfirmDialog$3(Landroid/content/DialogInterface;I)V
    .locals 2

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->fontAdapter:Lcom/android/settings/display/FontAdapter;

    iget-wide v0, p0, Lcom/android/settings/display/FontAdapter;->mLastIndex:J

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/display/FontAdapter;->setCurrentIndex(J)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method private synthetic lambda$tryBuildRecommendLayout$0(Ljava/util/List;)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/android/settings/display/PageLayoutFragment;->addChildViewForRecommendLayout(Ljava/util/List;)V

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mRecommendLayout:Landroid/view/View;

    if-eqz p1, :cond_0

    check-cast p1, Landroid/view/ViewGroup;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/android/settings/display/PageLayoutFragment;->setAllTextByCustomSize(Landroid/view/View;I)V

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->mRecommendLayout:Landroid/view/View;

    sget p1, Lcom/android/settings/R$id;->line_layout:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/widget/LinearLayout;

    new-array p1, v0, [Landroid/view/View;

    const/4 v0, 0x0

    aput-object p0, p1, v0

    invoke-static {p1}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/IFolme;->touch()Lmiuix/animation/ITouchStyle;

    move-result-object p1

    const/high16 v1, 0x3f800000    # 1.0f

    new-array v2, v0, [Lmiuix/animation/ITouchStyle$TouchType;

    invoke-interface {p1, v1, v2}, Lmiuix/animation/ITouchStyle;->setScale(F[Lmiuix/animation/ITouchStyle$TouchType;)Lmiuix/animation/ITouchStyle;

    move-result-object p1

    const v1, 0x3da3d70a    # 0.08f

    const/4 v2, 0x0

    invoke-interface {p1, v1, v2, v2, v2}, Lmiuix/animation/ITouchStyle;->setTint(FFFF)Lmiuix/animation/ITouchStyle;

    move-result-object p1

    invoke-interface {p1, v0}, Lmiuix/animation/ITouchStyle;->setTintMode(I)Lmiuix/animation/ITouchStyle;

    move-result-object p1

    new-array v0, v0, [Lmiuix/animation/base/AnimConfig;

    invoke-interface {p1, p0, v0}, Lmiuix/animation/ITouchStyle;->handleTouchOf(Landroid/view/View;[Lmiuix/animation/base/AnimConfig;)V

    :cond_0
    return-void
.end method

.method private synthetic lambda$tryBuildRecommendLayout$1()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/recommend/RecommendManager;->getInstance(Landroid/content/Context;)Lcom/android/settings/recommend/RecommendManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/recommend/RecommendManager;->loadRecommendList()V

    invoke-static {}, Lcom/android/settings/recommend/RecommendManager;->isLoadComplete()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lcom/android/settings/recommend/RecommendFilter;

    invoke-direct {v1}, Lcom/android/settings/recommend/RecommendFilter;-><init>()V

    invoke-virtual {p0}, Lcom/android/settings/display/PageLayoutFragment;->getPageIndex()I

    move-result v2

    iget-object v3, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v3, v2}, Lcom/android/settings/recommend/RecommendFilter;->getListByPageIndex(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-direct {p0}, Lcom/android/settings/display/PageLayoutFragment;->deleteRecommendFile()V

    invoke-virtual {v0}, Lcom/android/settings/recommend/RecommendManager;->loadRecommendList()V

    invoke-static {}, Lcom/android/settings/recommend/RecommendManager;->isLoadComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0, v2}, Lcom/android/settings/recommend/RecommendFilter;->getListByPageIndex(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v3

    if-nez v3, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/android/settings/display/PageLayoutFragment$$ExternalSyntheticLambda5;

    invoke-direct {v0, p0, v3}, Lcom/android/settings/display/PageLayoutFragment$$ExternalSyntheticLambda5;-><init>(Lcom/android/settings/display/PageLayoutFragment;Ljava/util/List;)V

    invoke-static {v0}, Lcom/android/settingslib/utils/ThreadUtils;->postOnMainThread(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_1
    const-string p0, "PageLayoutFragment"

    const-string/jumbo v0, "recommend items not load complete."

    invoke-static {p0, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method private modifyFontSizeAndWeight()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentLevel:I

    invoke-static {v0, v1}, Lcom/android/settings/display/PageLayoutFragment;->setUiMode(Landroid/content/Context;I)V

    sget-boolean v0, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mLastFontWeight:I

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/display/LargeFontUtils;->getFontWeight(Landroid/content/Context;)I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/display/PageLayoutFragment;->notifyFontWeightChanged()V

    :cond_0
    return-void
.end method

.method private notNeedCache(I)Z
    .locals 1

    const/16 v0, 0x32

    if-ne p1, v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFont:Lcom/android/settings/display/LocalFontModel;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/display/LocalFontModel;->getId()Ljava/lang/String;

    move-result-object p0

    const-string p1, "b004d74e-5c49-430c-bb6a-18ed5d2d33e4"

    invoke-static {p0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private notifyFontWeightChanged()V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0}, Lcom/android/settings/display/LargeFontUtils;->getFontWeight(Landroid/content/Context;)I

    move-result p0

    const-string v1, "key_var_font_scale"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-wide/32 v1, 0x20000000

    invoke-static {v1, v2, v0}, Landroid/content/res/MiuiConfiguration;->sendThemeConfigurationChangeMsg(JLandroid/os/Bundle;)V

    return-void
.end method

.method private onFontChange(Lcom/android/settings/display/LocalFontModel;)V
    .locals 2

    invoke-virtual {p1}, Lcom/android/settings/display/LocalFontModel;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFontId:Ljava/lang/String;

    iput-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFont:Lcom/android/settings/display/LocalFontModel;

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->fontAdapter:Lcom/android/settings/display/FontAdapter;

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontBubbleLeftTv:Landroid/widget/TextView;

    invoke-virtual {v0, v1, p1}, Lcom/android/settings/display/FontAdapter;->setFontFamily(Landroid/widget/TextView;Lcom/android/settings/display/LocalFontModel;)V

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->fontAdapter:Lcom/android/settings/display/FontAdapter;

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontBubbleRightTv:Landroid/widget/TextView;

    invoke-virtual {v0, v1, p1}, Lcom/android/settings/display/FontAdapter;->setFontFamily(Landroid/widget/TextView;Lcom/android/settings/display/LocalFontModel;)V

    iget-boolean p1, p0, Lcom/android/settings/display/PageLayoutFragment;->isPrimaryUser:Z

    const/16 v0, 0x32

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFontId:Ljava/lang/String;

    const-string v1, "10"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFont:Lcom/android/settings/display/LocalFontModel;

    invoke-virtual {p1}, Lcom/android/settings/display/LocalFontModel;->isVariable()Z

    move-result p1

    if-eqz p1, :cond_2

    :cond_0
    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFont:Lcom/android/settings/display/LocalFontModel;

    invoke-virtual {p1}, Lcom/android/settings/display/LocalFontModel;->isVariable()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-static {}, Lcom/android/settings/display/font/FontWeightUtils;->updateVarFont()V

    :cond_1
    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->fontWeightLinearLayout:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontWeightAdjustView:Lcom/android/settings/display/FontWeightAdjustView;

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontWeightAdjustView:Lcom/android/settings/display/FontWeightAdjustView;

    invoke-virtual {p1, p0}, Lcom/android/settings/display/FontWeightAdjustView;->setFontWeightChangeListener(Lcom/android/settings/display/FontWeightAdjustView$FontWeightChangeListener;)V

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontWeightAdjustView:Lcom/android/settings/display/FontWeightAdjustView;

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    invoke-static {p1, v0}, Lcom/android/settings/display/LargeFontUtils;->setFontWeight(Landroid/content/Context;I)V

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->mTypefaceCache:Landroid/util/SparseArray;

    invoke-virtual {p0}, Landroid/util/SparseArray;->clear()V

    goto :goto_0

    :cond_2
    iget-boolean p1, p0, Lcom/android/settings/display/PageLayoutFragment;->isPrimaryUser:Z

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->fontWeightLinearLayout:Landroid/view/View;

    const v1, 0x3e99999a    # 0.3f

    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    :cond_3
    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontWeightAdjustView:Lcom/android/settings/display/FontWeightAdjustView;

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontWeightAdjustView:Lcom/android/settings/display/FontWeightAdjustView;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    :goto_0
    return-void
.end method

.method private relayoutItems()V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mRootView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->font_hint_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentLevel:I

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/settings/display/LargeFontUtils;->getFontWeight(Landroid/content/Context;)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/android/settings/display/PageLayoutFragment;->completeHintText(Landroid/widget/TextView;II)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget-object v2, Lcom/android/settings/display/PageLayoutFragment;->PAGE_LAYOUT_SIZE:Ljava/util/HashMap;

    iget v3, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentLevel:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/android/settings/display/PageLayoutFragment;->setAllTextSize(Landroid/view/View;F)V

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mRootView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->font_bubble_right:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mRootView:Landroid/view/View;

    sget v3, Lcom/android/settings/R$id;->font_bubble_left:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentLevel:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-direct {p0, v0, v3}, Lcom/android/settings/display/PageLayoutFragment;->setAllTextSize(Landroid/view/View;F)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v3, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentLevel:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-direct {p0, v1, v0}, Lcom/android/settings/display/PageLayoutFragment;->setAllTextSize(Landroid/view/View;F)V

    return-void
.end method

.method private setAllTextByCustomSize(Landroid/view/View;I)V
    .locals 3

    instance-of v0, p1, Landroid/widget/TextView;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v2, Lcom/android/settings/display/PageLayoutFragment;->PAGE_LAYOUT_SIZE:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p2

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/display/PageLayoutFragment;->getCurrentZoomRadio()F

    move-result p0

    mul-float/2addr p2, p0

    invoke-virtual {p1, v1, p2}, Landroid/widget/TextView;->setTextSize(IF)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    check-cast p1, Landroid/view/ViewGroup;

    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/android/settings/display/PageLayoutFragment;->setAllTextByCustomSize(Landroid/view/View;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method private setAllTextSize(Landroid/view/View;F)V
    .locals 3

    instance-of v0, p1, Landroid/widget/TextView;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/display/PageLayoutFragment;->getCurrentZoomRadio()F

    move-result p0

    mul-float/2addr p2, p0

    invoke-virtual {p1, v1, p2}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_1

    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    check-cast p1, Landroid/view/ViewGroup;

    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/display/PageLayoutFragment;->getCurrentZoomRadio()F

    move-result v2

    mul-float/2addr v2, p2

    invoke-direct {p0, v0, v2}, Lcom/android/settings/display/PageLayoutFragment;->setAllTextSize(Landroid/view/View;F)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method private setLocalFontModelListCacahe(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    sget-object v0, Lcom/android/settings/display/PageLayoutFragment;->LOCAL_FONT_SP:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p0

    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    const-string v0, "local_font_list"

    invoke-interface {p0, v0, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string p1, "current_font_id"

    invoke-interface {p0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private setLocalFontModelListCacahe(Ljava/util/List;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/display/LocalFontModel;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    sget v2, Lcom/android/settings/display/PageLayoutFragment;->MAX_FONT_COUNT:I

    const/4 v3, 0x0

    if-le v1, v2, :cond_0

    invoke-interface {p1, v3, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_1

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/display/LocalFontModel;

    invoke-static {v1}, Lcom/android/settings/display/font/FontModel2JsonUtils;->LocalFont2Json(Lcom/android/settings/display/LocalFontModel;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/display/PageLayoutFragment;->setLocalFontModelListCacahe(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private setTextViewFont(Landroid/widget/TextView;I)V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/widget/TextView;->getTextSize()F

    move-result v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    const/4 v3, 0x5

    const/4 v4, 0x0

    invoke-static {v2, v3, v0, v4}, Lcom/android/settings/display/font/FontWeightUtils;->getScaleWght(Landroid/content/Context;IFI)I

    move-result v0

    iget-object v2, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFont:Lcom/android/settings/display/LocalFontModel;

    const-string v3, "10"

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/android/settings/display/LocalFontModel;->isVariable()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFont:Lcom/android/settings/display/LocalFontModel;

    invoke-virtual {v2}, Lcom/android/settings/display/LocalFontModel;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFont:Lcom/android/settings/display/LocalFontModel;

    invoke-virtual {v2}, Lcom/android/settings/display/LocalFontModel;->getFontWeight()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFont:Lcom/android/settings/display/LocalFontModel;

    invoke-virtual {v2}, Lcom/android/settings/display/LocalFontModel;->getFontWeight()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v5, 0x1

    if-lt v2, v5, :cond_2

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFont:Lcom/android/settings/display/LocalFontModel;

    invoke-virtual {v0}, Lcom/android/settings/display/LocalFontModel;->getFontWeight()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFont:Lcom/android/settings/display/LocalFontModel;

    invoke-virtual {v1}, Lcom/android/settings/display/LocalFontModel;->getFontWeight()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v1, v5

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFont:Lcom/android/settings/display/LocalFontModel;

    invoke-virtual {v1}, Lcom/android/settings/display/LocalFontModel;->getFontWeight()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v2, p2

    const/high16 v3, 0x42c80000    # 100.0f

    div-float/2addr v2, v3

    sub-int/2addr v0, v1

    int-to-float v0, v0

    mul-float/2addr v2, v0

    float-to-int v0, v2

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mTypefaceCache:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Typeface;

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFont:Lcom/android/settings/display/LocalFontModel;

    invoke-virtual {v1}, Lcom/android/settings/display/LocalFontModel;->getId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "b004d74e-5c49-430c-bb6a-18ed5d2d33e4"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    goto :goto_0

    :cond_1
    const/4 v1, 0x4

    :goto_0
    invoke-static {v0, v1}, Lcom/android/settings/display/font/FontWeightUtils;->getVarTypeface(II)Landroid/graphics/Typeface;

    move-result-object v1

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFontId:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mTypefaceCache:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Typeface;

    if-nez v1, :cond_3

    invoke-static {v0, v4}, Lcom/android/settings/display/font/FontWeightUtils;->getVarTypeface(II)Landroid/graphics/Typeface;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-direct {p0, p2}, Lcom/android/settings/display/PageLayoutFragment;->notNeedCache(I)Z

    move-result p2

    if-nez p2, :cond_4

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->mTypefaceCache:Landroid/util/SparseArray;

    invoke-virtual {p0, v0, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_4
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    return-void
.end method

.method private static setUiMode(Landroid/content/Context;I)V
    .locals 0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/android/settings/display/LargeFontUtils;->sendUiModeChangeMessage(Landroid/content/Context;I)Z

    :cond_0
    return-void
.end method

.method private showThemeCtaComfirmDialog(Landroid/content/Context;)V
    .locals 3

    new-instance v0, Lmiuix/appcompat/app/AlertDialog$Builder;

    sget v1, Lcom/android/settings/R$style;->AlertDialog_Theme_DayNight:I

    invoke-direct {v0, p1, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    sget p1, Lcom/android/settings/R$string;->font_cta_alert_title:I

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/android/settings/R$string;->font_cta_alert_message:I

    invoke-virtual {p1, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/android/settings/R$string;->font_cta_alert_btn_positive:I

    new-instance v2, Lcom/android/settings/display/PageLayoutFragment$$ExternalSyntheticLambda8;

    invoke-direct {v2, p0}, Lcom/android/settings/display/PageLayoutFragment$$ExternalSyntheticLambda8;-><init>(Lcom/android/settings/display/PageLayoutFragment;)V

    invoke-virtual {p1, v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/android/settings/R$string;->font_cta_alert_btn_negative:I

    new-instance v2, Lcom/android/settings/display/PageLayoutFragment$$ExternalSyntheticLambda9;

    invoke-direct {v2, p0}, Lcom/android/settings/display/PageLayoutFragment$$ExternalSyntheticLambda9;-><init>(Lcom/android/settings/display/PageLayoutFragment;)V

    invoke-virtual {p1, v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object p0

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    :try_start_0
    invoke-virtual {p0}, Landroid/app/Dialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private tryBuildRecommendLayout()V
    .locals 1

    new-instance v0, Lcom/android/settings/display/PageLayoutFragment$$ExternalSyntheticLambda4;

    invoke-direct {v0, p0}, Lcom/android/settings/display/PageLayoutFragment$$ExternalSyntheticLambda4;-><init>(Lcom/android/settings/display/PageLayoutFragment;)V

    invoke-static {v0}, Lcom/android/settingslib/utils/ThreadUtils;->postOnBackgroundThread(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method private updateBubbleAndHintText()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFont:Lcom/android/settings/display/LocalFontModel;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->fontAdapter:Lcom/android/settings/display/FontAdapter;

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {v0}, Lcom/android/settings/display/LocalFontModel;->isVariable()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFont:Lcom/android/settings/display/LocalFontModel;

    invoke-virtual {v0}, Lcom/android/settings/display/LocalFontModel;->getId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "10"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->fontAdapter:Lcom/android/settings/display/FontAdapter;

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontHintTv:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFont:Lcom/android/settings/display/LocalFontModel;

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/display/FontAdapter;->setFontFamily(Landroid/widget/TextView;Lcom/android/settings/display/LocalFontModel;)V

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->fontAdapter:Lcom/android/settings/display/FontAdapter;

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontBubbleLeftTv:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFont:Lcom/android/settings/display/LocalFontModel;

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/display/FontAdapter;->setFontFamily(Landroid/widget/TextView;Lcom/android/settings/display/LocalFontModel;)V

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->fontAdapter:Lcom/android/settings/display/FontAdapter;

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontBubbleLeftTv:Landroid/widget/TextView;

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFont:Lcom/android/settings/display/LocalFontModel;

    invoke-virtual {v0, v1, p0}, Lcom/android/settings/display/FontAdapter;->setFontFamily(Landroid/widget/TextView;Lcom/android/settings/display/LocalFontModel;)V

    goto :goto_1

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontHintTv:Landroid/widget/TextView;

    iget v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mLastFontWeight:I

    invoke-direct {p0, v0, v1}, Lcom/android/settings/display/PageLayoutFragment;->setTextViewFont(Landroid/widget/TextView;I)V

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontBubbleLeftTv:Landroid/widget/TextView;

    iget v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mLastFontWeight:I

    invoke-direct {p0, v0, v1}, Lcom/android/settings/display/PageLayoutFragment;->setTextViewFont(Landroid/widget/TextView;I)V

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontBubbleRightTv:Landroid/widget/TextView;

    iget v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mLastFontWeight:I

    invoke-direct {p0, v0, v1}, Lcom/android/settings/display/PageLayoutFragment;->setTextViewFont(Landroid/widget/TextView;I)V

    :cond_3
    :goto_1
    return-void
.end method

.method private updateLocalFontModelListCache(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/display/LocalFontModel;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/display/LocalFontModel;

    invoke-virtual {v0}, Lcom/android/settings/display/LocalFontModel;->getId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "10"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {}, Lcom/android/settings/display/PageLayoutFragment;->getFontTitle()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateLocalFontModelListCache: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "PageLayoutFragment"

    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0, v1}, Lcom/android/settings/display/LocalFontModel;->setTitle(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    :goto_1
    return-void
.end method


# virtual methods
.method public addRecommendView(Ljava/lang/CharSequence;Landroid/content/Intent;)Landroid/widget/RelativeLayout;
    .locals 3

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/android/settings/R$layout;->font_recommend_item:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    sget v1, Lcom/android/settings/R$id;->item_view:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance p1, Lcom/android/settings/display/PageLayoutFragment$1;

    invoke-direct {p1, p0, p2}, Lcom/android/settings/display/PageLayoutFragment$1;-><init>(Lcom/android/settings/display/PageLayoutFragment;Landroid/content/Intent;)V

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 p0, 0x1

    new-array p0, p0, [Landroid/view/View;

    const/4 p1, 0x0

    aput-object v1, p0, p1

    invoke-static {p0}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object p0

    invoke-interface {p0}, Lmiuix/animation/IFolme;->touch()Lmiuix/animation/ITouchStyle;

    move-result-object p0

    new-array p2, p1, [Lmiuix/animation/ITouchStyle$TouchType;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {p0, v2, p2}, Lmiuix/animation/ITouchStyle;->setScale(F[Lmiuix/animation/ITouchStyle$TouchType;)Lmiuix/animation/ITouchStyle;

    move-result-object p0

    new-array p2, p1, [Lmiuix/animation/ITouchStyle$TouchType;

    const v2, 0x3f19999a    # 0.6f

    invoke-interface {p0, v2, p2}, Lmiuix/animation/ITouchStyle;->setAlpha(F[Lmiuix/animation/ITouchStyle$TouchType;)Lmiuix/animation/ITouchStyle;

    move-result-object p0

    new-array p1, p1, [Lmiuix/animation/base/AnimConfig;

    invoke-interface {p0, v1, p1}, Lmiuix/animation/ITouchStyle;->handleTouchOf(Landroid/view/View;[Lmiuix/animation/base/AnimConfig;)V

    return-object v0
.end method

.method public applyFont(Landroid/content/Context;Ljava/lang/String;II)V
    .locals 8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "applyFont: fontId -- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", fontScale -- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", fontWeight -- "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PageLayoutFragment"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/android/settings/display/PageLayoutFragment$$ExternalSyntheticLambda2;

    move-object v2, v0

    move-object v3, p0

    move-object v4, p2

    move v5, p3

    move v6, p4

    move-object v7, p1

    invoke-direct/range {v2 .. v7}, Lcom/android/settings/display/PageLayoutFragment$$ExternalSyntheticLambda2;-><init>(Lcom/android/settings/display/PageLayoutFragment;Ljava/lang/String;IILandroid/content/Context;)V

    invoke-static {v0}, Lcom/android/settingslib/utils/ThreadUtils;->postOnBackgroundThread(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method public fontSelected(IZ)V
    .locals 1

    if-eqz p2, :cond_2

    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->isFoldDevice()Z

    move-result p2

    const-string v0, "com.android.thememanager"

    if-nez p2, :cond_1

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->isSplitTabletDevice()Z

    move-result p2

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const-string p2, "com.android.thememanager.ThemeResourceTabActivity"

    invoke-virtual {p1, v0, p2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p2, "EXTRA_TAB_ID"

    const-string v0, "fonts"

    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :cond_1
    :goto_0
    const-string p2, "com.setting.pad.font"

    invoke-virtual {p1, p2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    :goto_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2, p1}, Lcom/android/settings/MiuiUtils;->canFindActivityStatic(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result p2

    if-eqz p2, :cond_4

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    :cond_2
    iget-object p2, p0, Lcom/android/settings/display/PageLayoutFragment;->localFontModelList:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/android/settings/display/LocalFontModel;

    iget v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFontPos:I

    iput v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mLastFontPos:I

    iput p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFontPos:I

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "fontSelected: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/android/settings/display/LocalFontModel;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " -- "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/android/settings/display/LocalFontModel;->getRightFileUnaccessable()Z

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "PageLayoutFragment"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Lcom/android/settings/display/LocalFontModel;->getRightFileUnaccessable()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/android/settings/display/PageLayoutFragment;->showThemeCtaComfirmDialog(Landroid/content/Context;)V

    return-void

    :cond_3
    invoke-direct {p0, p2}, Lcom/android/settings/display/PageLayoutFragment;->onFontChange(Lcom/android/settings/display/LocalFontModel;)V

    :cond_4
    :goto_2
    return-void
.end method

.method protected getCurrentZoomRadio()F
    .locals 1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/display/ScreenZoomUtils;->getLastZoomLevel(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/display/PageLayoutFragment;->getZoomRadioByLevel(I)F

    move-result p0

    return p0
.end method

.method public getFonts(Landroid/content/Context;)V
    .locals 1

    new-instance v0, Lcom/android/settings/display/PageLayoutFragment$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/display/PageLayoutFragment$$ExternalSyntheticLambda3;-><init>(Lcom/android/settings/display/PageLayoutFragment;Landroid/content/Context;)V

    invoke-static {v0}, Lcom/android/settingslib/utils/ThreadUtils;->postOnBackgroundThread(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method public getPageIndex()I
    .locals 0

    const/16 p0, 0x7d3

    return p0
.end method

.method protected getZoomRadioByLevel(I)F
    .locals 0

    if-eqz p1, :cond_2

    const/4 p0, 0x1

    if-eq p1, p0, :cond_1

    const/4 p0, 0x2

    if-eq p1, p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_0
    const p0, 0x3f866666    # 1.05f

    goto :goto_0

    :cond_1
    const/high16 p0, 0x3f800000    # 1.0f

    goto :goto_0

    :cond_2
    sget p0, Lcom/android/settings/display/ScreenZoomUtils;->SCREEN_ZOOM_SMALL_RATIO:F

    :goto_0
    return p0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    new-instance p1, Lcom/android/settings/display/FontAdapter;

    invoke-direct {p1}, Lcom/android/settings/display/FontAdapter;-><init>()V

    iput-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->fontAdapter:Lcom/android/settings/display/FontAdapter;

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v1}, Lcom/android/settings/display/FontAdapter;->setContext(Landroid/content/Context;)V

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->fontAdapter:Lcom/android/settings/display/FontAdapter;

    new-instance v1, Lcom/android/settings/display/PageLayoutFragment$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/settings/display/PageLayoutFragment$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/display/PageLayoutFragment;)V

    invoke-virtual {p1, v1}, Lcom/android/settings/display/FontAdapter;->setFontSelectListener(Lcom/android/settings/display/FontAdapter$FontSelectListener;)V

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->fontAdapter:Lcom/android/settings/display/FontAdapter;

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->localFontModelList:Ljava/util/List;

    invoke-virtual {p1, v1}, Lcom/android/settings/display/FontAdapter;->setDataList(Ljava/util/List;)V

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->fontAdapter:Lcom/android/settings/display/FontAdapter;

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFontId:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/android/settings/display/FontAdapter;->setCurrentFontId(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->fontAdapter:Lcom/android/settings/display/FontAdapter;

    invoke-virtual {p1, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    new-instance p1, Landroidx/recyclerview/widget/LinearLayoutManager;

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    invoke-direct {p1, v1, v0, v0}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, p1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mRootView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->recommend_layout:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mRecommendLayout:Landroid/view/View;

    invoke-direct {p0}, Lcom/android/settings/display/PageLayoutFragment;->tryBuildRecommendLayout()V

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mRootView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->font_bubble_right_layout:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mRootView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->font_bubble_left_layout:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/settings/stat/commonswitch/TalkbackSwitch;->isTalkbackEnable(Landroid/content/Context;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/settings/display/PageLayoutFragment;->isTalkbackMode:Z

    if-nez v2, :cond_0

    const/4 v2, 0x1

    new-array v3, v2, [Landroid/view/View;

    aput-object p1, v3, v0

    invoke-static {v3}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v3

    invoke-interface {v3}, Lmiuix/animation/IFolme;->touch()Lmiuix/animation/ITouchStyle;

    move-result-object v3

    new-array v4, v0, [Lmiuix/animation/ITouchStyle$TouchType;

    const v5, 0x3f666666    # 0.9f

    invoke-interface {v3, v5, v4}, Lmiuix/animation/ITouchStyle;->setScale(F[Lmiuix/animation/ITouchStyle$TouchType;)Lmiuix/animation/ITouchStyle;

    move-result-object v3

    new-array v4, v0, [Lmiuix/animation/base/AnimConfig;

    invoke-interface {v3, p1, v4}, Lmiuix/animation/ITouchStyle;->handleTouchOf(Landroid/view/View;[Lmiuix/animation/base/AnimConfig;)V

    new-array p1, v2, [Landroid/view/View;

    aput-object v1, p1, v0

    invoke-static {p1}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/IFolme;->touch()Lmiuix/animation/ITouchStyle;

    move-result-object p1

    new-array v2, v0, [Lmiuix/animation/ITouchStyle$TouchType;

    invoke-interface {p1, v5, v2}, Lmiuix/animation/ITouchStyle;->setScale(F[Lmiuix/animation/ITouchStyle$TouchType;)Lmiuix/animation/ITouchStyle;

    move-result-object p1

    const v2, 0x3da3d70a    # 0.08f

    const/4 v3, 0x0

    invoke-interface {p1, v2, v3, v3, v3}, Lmiuix/animation/ITouchStyle;->setTint(FFFF)Lmiuix/animation/ITouchStyle;

    move-result-object p1

    invoke-interface {p1, v0}, Lmiuix/animation/ITouchStyle;->setTintMode(I)Lmiuix/animation/ITouchStyle;

    move-result-object p1

    new-array v0, v0, [Lmiuix/animation/base/AnimConfig;

    invoke-interface {p1, v1, v0}, Lmiuix/animation/ITouchStyle;->handleTouchOf(Landroid/view/View;[Lmiuix/animation/base/AnimConfig;)V

    :cond_0
    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mRootView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->font_hint_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontHintTv:Landroid/widget/TextView;

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mRootView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->font_bubble_right:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontBubbleRightTv:Landroid/widget/TextView;

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mRootView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->font_bubble_left:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontBubbleLeftTv:Landroid/widget/TextView;

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v0, "onActivityResult: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " , result: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    const-string v0, "PageLayoutFragment"

    invoke-static {v0, p3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 p3, 0xc8

    if-ne p1, p3, :cond_2

    if-eqz p2, :cond_1

    const/4 p1, 0x1

    if-eq p2, p1, :cond_0

    goto :goto_0

    :cond_0
    const-string/jumbo p1, "onActivityResult: login success"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/android/settings/display/PageLayoutFragment;->getFonts(Landroid/content/Context;)V

    invoke-static {}, Lcom/android/settingslib/utils/ThreadUtils;->getUiThreadHandler()Landroid/os/Handler;

    move-result-object p1

    new-instance p2, Lcom/android/settings/display/PageLayoutFragment$$ExternalSyntheticLambda1;

    invoke-direct {p2, p0}, Lcom/android/settings/display/PageLayoutFragment$$ExternalSyntheticLambda1;-><init>(Lcom/android/settings/display/PageLayoutFragment;)V

    const-wide/16 v0, 0xc8

    invoke-virtual {p1, p2, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_1
    const-string/jumbo p1, "onActivityResult: login error"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mLastFontPos:I

    iput p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFontPos:I

    :cond_2
    :goto_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    new-instance p1, Lcom/android/settings/display/PageLayoutFragment$DownloadRunnable;

    invoke-direct {p1, p0}, Lcom/android/settings/display/PageLayoutFragment$DownloadRunnable;-><init>(Lcom/android/settings/display/PageLayoutFragment;)V

    invoke-static {p1}, Lcom/android/settingslib/utils/ThreadUtils;->postOnBackgroundThread(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/display/LargeFontUtils;->getVariableFontChange(Landroid/content/Context;)Z

    invoke-static {}, Lcom/android/settings/display/LargeFontUtils;->getCurrentUIModeType()I

    move-result p1

    iput p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentLevel:I

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "mCurrentLevel:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentLevel:I

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "PageLayoutFragment"

    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/settings/display/PageLayoutFragment;->adjustCurrentLevelIfNeed()Z

    move-result p1

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    iput v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentLevel:I

    :cond_0
    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/display/LargeFontUtils;->getFontWeight(Landroid/content/Context;)I

    move-result p1

    iput p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mLastFontWeight:I

    iget-boolean p1, p0, Lcom/android/settings/display/PageLayoutFragment;->isPrimaryUser:Z

    const-string v2, "10"

    const/4 v3, 0x0

    const/4 v4, 0x0

    if-eqz p1, :cond_4

    invoke-direct {p0}, Lcom/android/settings/display/PageLayoutFragment;->getLocalFontModelListCacahe()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/android/settings/display/PageLayoutFragment;->updateLocalFontModelListCache(Ljava/util/List;)V

    invoke-direct {p0}, Lcom/android/settings/display/PageLayoutFragment;->getCurrentFontId()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFontId:Ljava/lang/String;

    iput-object v5, p0, Lcom/android/settings/display/PageLayoutFragment;->mLastFontId:Ljava/lang/String;

    if-nez p1, :cond_1

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iget-object v5, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    if-eqz v5, :cond_1

    new-instance v5, Lcom/android/settings/display/LocalFontModel;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {}, Lcom/android/settings/display/PageLayoutFragment;->getFontTitle()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v2, v6, v3, v4}, Lcom/android/settings/display/LocalFontModel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {p1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    iput-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->localFontModelList:Ljava/util/List;

    move p1, v4

    move v2, p1

    :goto_0
    iget-object v5, p0, Lcom/android/settings/display/PageLayoutFragment;->localFontModelList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge p1, v5, :cond_3

    iget-object v5, p0, Lcom/android/settings/display/PageLayoutFragment;->localFontModelList:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/settings/display/LocalFontModel;

    invoke-virtual {v5}, Lcom/android/settings/display/LocalFontModel;->getId()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFontId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v2, p0, Lcom/android/settings/display/PageLayoutFragment;->localFontModelList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/display/LocalFontModel;

    invoke-virtual {v2}, Lcom/android/settings/display/LocalFontModel;->getId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFontId:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/settings/display/PageLayoutFragment;->localFontModelList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/display/LocalFontModel;

    iput-object v2, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFont:Lcom/android/settings/display/LocalFontModel;

    iget-object v2, p0, Lcom/android/settings/display/PageLayoutFragment;->localFontModelList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/display/LocalFontModel;

    invoke-virtual {v2, v1}, Lcom/android/settings/display/LocalFontModel;->setUsing(Z)V

    move v2, p1

    goto :goto_1

    :cond_2
    iget-object v5, p0, Lcom/android/settings/display/PageLayoutFragment;->localFontModelList:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/settings/display/LocalFontModel;

    invoke-virtual {v5, v4}, Lcom/android/settings/display/LocalFontModel;->setUsing(Z)V

    :goto_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_3
    const/4 p1, 0x2

    if-lt v2, p1, :cond_5

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->localFontModelList:Ljava/util/List;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/android/settings/display/LocalFontModel;

    iget-object v5, p0, Lcom/android/settings/display/PageLayoutFragment;->localFontModelList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget-object v2, p0, Lcom/android/settings/display/PageLayoutFragment;->localFontModelList:Ljava/util/List;

    invoke-interface {v2, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_2

    :cond_4
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->localFontModelList:Ljava/util/List;

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    if-eqz p1, :cond_5

    new-instance p1, Lcom/android/settings/display/LocalFontModel;

    iget-object v5, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {}, Lcom/android/settings/display/PageLayoutFragment;->getFontTitle()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p1, v2, v5, v3, v1}, Lcom/android/settings/display/LocalFontModel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v2, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFontId:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/settings/display/PageLayoutFragment;->mLastFontId:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->localFontModelList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFont:Lcom/android/settings/display/LocalFontModel;

    :cond_5
    :goto_2
    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->localFontModelList:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    sget v1, Lcom/android/settings/display/PageLayoutFragment;->MAX_FONT_COUNT:I

    if-le p1, v1, :cond_6

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "the size of the current font list: "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->localFontModelList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->localFontModelList:Ljava/util/List;

    sget v0, Lcom/android/settings/display/PageLayoutFragment;->MAX_FONT_COUNT:I

    invoke-interface {p1, v4, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->localFontModelList:Ljava/util/List;

    :cond_6
    new-instance p1, Lcom/android/settings/display/LocalFontModel;

    const-string v0, "-1000"

    invoke-direct {p1, v0, v3, v3, v4}, Lcom/android/settings/display/LocalFontModel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->localFontModelList:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string/jumbo p0, "setting_font_click_size"

    invoke-static {p0}, Lcom/android/settings/report/InternationalCompat;->trackReportEvent(Ljava/lang/String;)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mHander:Lcom/android/settings/display/PageLayoutFragment$FontUpdateHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onDestroyView()V
    .locals 0

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onDestroyView()V

    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 0

    sget p2, Lcom/android/settings/R$layout;->font_settings_fragment:I

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mRootView:Landroid/view/View;

    new-instance p1, Landroid/preference/PreferenceFrameLayout$LayoutParams;

    const/4 p2, -0x1

    invoke-direct {p1, p2, p2}, Landroid/preference/PreferenceFrameLayout$LayoutParams;-><init>(II)V

    const/4 p2, 0x1

    iput-boolean p2, p1, Landroid/preference/PreferenceFrameLayout$LayoutParams;->removeBorders:Z

    iget-object p2, p0, Lcom/android/settings/display/PageLayoutFragment;->mRootView:Landroid/view/View;

    invoke-virtual {p2, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->mRootView:Landroid/view/View;

    return-object p0
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/display/PageLayoutFragment;->isPrimaryUser:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/android/settings/display/PageLayoutFragment;->getFonts(Landroid/content/Context;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontWeightAdjustView:Lcom/android/settings/display/FontWeightAdjustView;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    :goto_0
    invoke-direct {p0}, Lcom/android/settings/display/PageLayoutFragment;->relayoutItems()V

    iget v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentLevel:I

    const/16 v2, 0xb

    if-eq v0, v2, :cond_2

    iget-boolean v0, p0, Lcom/android/settings/display/PageLayoutFragment;->isTalkbackMode:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mRecommendLayout:Landroid/view/View;

    sget v2, Lcom/android/settings/display/PageLayoutFragment;->RECOMMEND_HIDE:I

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->scrollViewCard:Lcom/android/settings/display/FontSettingsScrollView;

    const/16 v2, 0x21

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->fullScroll(I)Z

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->scrollViewCard:Lcom/android/settings/display/FontSettingsScrollView;

    invoke-virtual {v0, v1}, Lcom/android/settings/display/FontSettingsScrollView;->setCanScroll(Z)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mRecommendLayout:Landroid/view/View;

    sget v1, Lcom/android/settings/display/PageLayoutFragment;->RECOMMEND_SHOW:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->scrollViewCard:Lcom/android/settings/display/FontSettingsScrollView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/display/FontSettingsScrollView;->setCanScroll(Z)V

    :goto_1
    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->fontAdapter:Lcom/android/settings/display/FontAdapter;

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFontId:Ljava/lang/String;

    invoke-virtual {v0, p0}, Lcom/android/settings/display/FontAdapter;->setCurrentFontId(Ljava/lang/String;)V

    return-void
.end method

.method public onSizeChange(I)V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "fontSize"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v1, "setting_Display_fontsize"

    invoke-static {v1, v0}, Lcom/android/settings/report/InternationalCompat;->trackReportObjectEvent(Ljava/lang/String;Ljava/util/Map;)V

    if-eqz p1, :cond_5

    const/4 v0, 0x1

    if-eq p1, v0, :cond_4

    const/4 v1, 0x2

    if-eq p1, v1, :cond_3

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    return-void

    :cond_0
    const/16 p1, 0xb

    iput p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentLevel:I

    goto :goto_0

    :cond_1
    const/16 p1, 0xf

    iput p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentLevel:I

    goto :goto_0

    :cond_2
    const/16 p1, 0xe

    iput p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentLevel:I

    goto :goto_0

    :cond_3
    iput v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentLevel:I

    goto :goto_0

    :cond_4
    const/16 p1, 0xc

    iput p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentLevel:I

    goto :goto_0

    :cond_5
    const/16 p1, 0xa

    iput p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentLevel:I

    :goto_0
    invoke-direct {p0}, Lcom/android/settings/display/PageLayoutFragment;->relayoutItems()V

    return-void
.end method

.method public onStart()V
    .locals 3

    invoke-super {p0}, Lcom/android/settings/BaseFragment;->onStart()V

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->getAppCompatActivity()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v1, :cond_0

    sget v1, Lcom/android/settings/R$string;->title_layout_current2:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/android/settings/R$string;->title_font_settings:I

    :goto_0
    invoke-virtual {v0, v1}, Landroidx/appcompat/app/ActionBar;->setTitle(I)V

    :try_start_0
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v2, Lcom/android/settings/R$color;->font_settings_bg_color:I

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    invoke-direct {v1, p0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    :goto_1
    return-void
.end method

.method public onStop()V
    .locals 4

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onStop()V

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mLastFontId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFontId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFontId:Ljava/lang/String;

    iget v2, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentLevel:I

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/android/settings/display/LargeFontUtils;->getFontWeight(Landroid/content/Context;)I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/settings/display/PageLayoutFragment;->applyFont(Landroid/content/Context;Ljava/lang/String;II)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/display/PageLayoutFragment;->modifyFontSizeAndWeight()V

    :goto_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/android/settings/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mRootView:Landroid/view/View;

    sget p2, Lcom/android/settings/R$id;->fontweight_view:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/android/settings/display/FontWeightAdjustView;

    iput-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontWeightAdjustView:Lcom/android/settings/display/FontWeightAdjustView;

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mRootView:Landroid/view/View;

    sget p2, Lcom/android/settings/R$id;->ll_font_weight:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->fontWeightLinearLayout:Landroid/view/View;

    iget p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentLevel:I

    const/4 p2, 0x0

    const/4 v0, 0x1

    const/16 v1, 0xd

    if-ne p1, v1, :cond_0

    iput v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentLevel:I

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/android/settings/display/LargeFontUtils;->isSupportVarintFont()Z

    move-result p1

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentFont:Lcom/android/settings/display/LocalFontModel;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/android/settings/display/LocalFontModel;->isVariable()Z

    move-result p1

    if-nez p1, :cond_2

    iget-boolean p1, p0, Lcom/android/settings/display/PageLayoutFragment;->isPrimaryUser:Z

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->fontWeightLinearLayout:Landroid/view/View;

    const v1, 0x3e99999a    # 0.3f

    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    :cond_1
    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontWeightAdjustView:Lcom/android/settings/display/FontWeightAdjustView;

    invoke-virtual {p1, p2}, Landroid/widget/SeekBar;->setEnabled(Z)V

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->fontWeightLinearLayout:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontWeightAdjustView:Lcom/android/settings/display/FontWeightAdjustView;

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setEnabled(Z)V

    :goto_0
    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontWeightAdjustView:Lcom/android/settings/display/FontWeightAdjustView;

    invoke-virtual {p1, p0}, Lcom/android/settings/display/FontWeightAdjustView;->setFontWeightChangeListener(Lcom/android/settings/display/FontWeightAdjustView$FontWeightChangeListener;)V

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mRootView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->font_recycler_view:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mRootView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->font_bubble_left:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->fontWeightLinearLayout:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/settings/display/PageLayoutFragment;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$string;->adjust_font_size_answer_global:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$string;->adjust_font_size_answer_cn:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mRootView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->font_view:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/android/settings/display/FontSizeAdjustView;

    iput-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mAdjustView:Lcom/android/settings/display/FontSizeAdjustView;

    invoke-virtual {p1, p0}, Lcom/android/settings/display/FontSizeAdjustView;->setFontSizeChangeListener(Lcom/android/settings/display/FontSizeAdjustView$FontSizeChangeListener;)V

    iget p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentLevel:I

    if-eq p1, v0, :cond_8

    const/16 v1, 0xb

    if-eq p1, v1, :cond_7

    const/16 v1, 0xc

    if-eq p1, v1, :cond_6

    const/16 v0, 0xe

    if-eq p1, v0, :cond_5

    const/16 v0, 0xf

    if-eq p1, v0, :cond_4

    goto :goto_2

    :cond_4
    const/4 p2, 0x4

    goto :goto_2

    :cond_5
    const/4 p2, 0x3

    goto :goto_2

    :cond_6
    move p2, v0

    goto :goto_2

    :cond_7
    const/4 p2, 0x5

    goto :goto_2

    :cond_8
    const/4 p2, 0x2

    :goto_2
    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mAdjustView:Lcom/android/settings/display/FontSizeAdjustView;

    invoke-virtual {p1, p2}, Lcom/android/settings/display/FontSizeAdjustView;->setCurrentPointIndex(I)V

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mAdjustView:Lcom/android/settings/display/FontSizeAdjustView;

    invoke-virtual {p1, p2}, Lcom/android/settings/display/FontSizeAdjustView;->setLastCurrentPointIndex(I)V

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mAdjustView:Lcom/android/settings/display/FontSizeAdjustView;

    invoke-virtual {p1, p0}, Lcom/android/settings/display/FontSizeAdjustView;->setRecommendListener(Lcom/android/settings/display/FontSizeAdjustView$RecommendListener;)V

    iget-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mRootView:Landroid/view/View;

    sget p2, Lcom/android/settings/R$id;->bottom_scroll_view:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/android/settings/display/FontSettingsScrollView;

    iput-object p1, p0, Lcom/android/settings/display/PageLayoutFragment;->scrollViewCard:Lcom/android/settings/display/FontSettingsScrollView;

    return-void
.end method

.method public onWeightChange(I)V
    .locals 2

    rem-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mLastProgress:I

    sub-int v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mLastFontWeight:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "ignore weight change, progress:"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "PageLayoutFragment"

    invoke-static {p1, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iput p1, p0, Lcom/android/settings/display/PageLayoutFragment;->mLastProgress:I

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontHintTv:Landroid/widget/TextView;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mRootView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->font_hint_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontHintTv:Landroid/widget/TextView;

    :cond_1
    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontBubbleRightTv:Landroid/widget/TextView;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mRootView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->font_bubble_right:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontBubbleRightTv:Landroid/widget/TextView;

    :cond_2
    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontBubbleLeftTv:Landroid/widget/TextView;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mRootView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->font_bubble_left:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontBubbleLeftTv:Landroid/widget/TextView;

    :cond_3
    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontHintTv:Landroid/widget/TextView;

    iget v1, p0, Lcom/android/settings/display/PageLayoutFragment;->mCurrentLevel:I

    invoke-direct {p0, v0, v1, p1}, Lcom/android/settings/display/PageLayoutFragment;->completeHintText(Landroid/widget/TextView;II)V

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontHintTv:Landroid/widget/TextView;

    invoke-direct {p0, v0, p1}, Lcom/android/settings/display/PageLayoutFragment;->setTextViewFont(Landroid/widget/TextView;I)V

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontBubbleLeftTv:Landroid/widget/TextView;

    invoke-direct {p0, v0, p1}, Lcom/android/settings/display/PageLayoutFragment;->setTextViewFont(Landroid/widget/TextView;I)V

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mFontBubbleRightTv:Landroid/widget/TextView;

    invoke-direct {p0, v0, p1}, Lcom/android/settings/display/PageLayoutFragment;->setTextViewFont(Landroid/widget/TextView;I)V

    return-void
.end method

.method public scrollToPosition(II)V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->scrollViewCard:Lcom/android/settings/display/FontSettingsScrollView;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v3, 0x0

    aput p1, v2, v3

    const-string/jumbo p1, "scrollX"

    invoke-static {v0, p1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object p1

    iget-object p0, p0, Lcom/android/settings/display/PageLayoutFragment;->scrollViewCard:Lcom/android/settings/display/FontSettingsScrollView;

    new-array v0, v1, [I

    aput p2, v0, v3

    const-string/jumbo p2, "scrollY"

    invoke-static {p0, p2, v0}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object p0

    new-instance p2, Landroid/animation/AnimatorSet;

    invoke-direct {p2}, Landroid/animation/AnimatorSet;-><init>()V

    const-wide/16 v4, 0x3e8

    invoke-virtual {p2, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/animation/Animator;

    aput-object p1, v0, v3

    aput-object p0, v0, v1

    invoke-virtual {p2, v0}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    invoke-virtual {p2}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method public scrollViewToHideRecommend()V
    .locals 4

    iget-boolean v0, p0, Lcom/android/settings/display/PageLayoutFragment;->isTalkbackMode:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mRecommendLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    sget v2, Lcom/android/settings/display/PageLayoutFragment;->RECOMMEND_HIDE:I

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->scrollViewCard:Lcom/android/settings/display/FontSettingsScrollView;

    invoke-virtual {v0, v1}, Lcom/android/settings/display/FontSettingsScrollView;->setCanScroll(Z)V

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v2, 0x78

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v0

    new-instance v2, Lcom/android/settings/display/PageLayoutFragment$4;

    invoke-direct {v2, p0}, Lcom/android/settings/display/PageLayoutFragment$4;-><init>(Lcom/android/settings/display/PageLayoutFragment;)V

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v2, Lcom/android/settings/display/PageLayoutFragment$5;

    invoke-direct {v2, p0}, Lcom/android/settings/display/PageLayoutFragment$5;-><init>(Lcom/android/settings/display/PageLayoutFragment;)V

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    :cond_0
    invoke-virtual {p0, v1, v1}, Lcom/android/settings/display/PageLayoutFragment;->scrollToPosition(II)V

    return-void

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public showRecommendLayout()V
    .locals 3

    iget-boolean v0, p0, Lcom/android/settings/display/PageLayoutFragment;->isTalkbackMode:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mRecommendLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    sget v1, Lcom/android/settings/display/PageLayoutFragment;->RECOMMEND_SHOW:I

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mRecommendLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/display/PageLayoutFragment;->mRecommendLayout:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v0

    new-instance v1, Lcom/android/settings/display/PageLayoutFragment$2;

    invoke-direct {v1, p0}, Lcom/android/settings/display/PageLayoutFragment$2;-><init>(Lcom/android/settings/display/PageLayoutFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v1, Lcom/android/settings/display/PageLayoutFragment$3;

    invoke-direct {v1, p0}, Lcom/android/settings/display/PageLayoutFragment$3;-><init>(Lcom/android/settings/display/PageLayoutFragment;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    :cond_0
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method
