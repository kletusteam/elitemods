.class Lcom/android/settings/display/DarkModeAppsListAdapter$2;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/display/DarkModeAppsListAdapter;->onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/display/DarkModeAppsListAdapter;

.field final synthetic val$item:Lcom/android/settings/display/DarkModeAppInfo;


# direct methods
.method constructor <init>(Lcom/android/settings/display/DarkModeAppsListAdapter;Lcom/android/settings/display/DarkModeAppInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/display/DarkModeAppsListAdapter$2;->this$0:Lcom/android/settings/display/DarkModeAppsListAdapter;

    iput-object p2, p0, Lcom/android/settings/display/DarkModeAppsListAdapter$2;->val$item:Lcom/android/settings/display/DarkModeAppInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/display/DarkModeAppsListAdapter$2;->this$0:Lcom/android/settings/display/DarkModeAppsListAdapter;

    invoke-static {v0}, Lcom/android/settings/display/DarkModeAppsListAdapter;->-$$Nest$fgetonAppCheckedListener(Lcom/android/settings/display/DarkModeAppsListAdapter;)Lcom/android/settings/display/DarkModeAppsListAdapter$OnAppCheckedListener;

    move-result-object v0

    iget-object p0, p0, Lcom/android/settings/display/DarkModeAppsListAdapter$2;->val$item:Lcom/android/settings/display/DarkModeAppInfo;

    invoke-interface {v0, p1, p2, p0}, Lcom/android/settings/display/DarkModeAppsListAdapter$OnAppCheckedListener;->onAppChecked(Landroid/widget/CompoundButton;ZLcom/android/settings/display/DarkModeAppInfo;)V

    return-void
.end method
