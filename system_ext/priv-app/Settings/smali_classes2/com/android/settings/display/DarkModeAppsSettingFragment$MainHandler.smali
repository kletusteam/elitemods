.class Lcom/android/settings/display/DarkModeAppsSettingFragment$MainHandler;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/display/DarkModeAppsSettingFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MainHandler"
.end annotation


# instance fields
.field private mOuterRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/android/settings/display/DarkModeAppsSettingFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/android/settings/display/DarkModeAppsSettingFragment;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment$MainHandler;->mOuterRef:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget-object p0, p0, Lcom/android/settings/display/DarkModeAppsSettingFragment$MainHandler;->mOuterRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/android/settings/display/DarkModeAppsSettingFragment;

    if-nez p0, :cond_0

    return-void

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "array"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/android/settings/display/DarkModeAppsSettingFragment;->-$$Nest$fputmArray(Lcom/android/settings/display/DarkModeAppsSettingFragment;Ljava/util/ArrayList;)V

    invoke-static {p0}, Lcom/android/settings/display/DarkModeAppsSettingFragment;->-$$Nest$fgetmArray(Lcom/android/settings/display/DarkModeAppsSettingFragment;)Ljava/util/ArrayList;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/android/settings/display/DarkModeAppsSettingFragment;->-$$Nest$mremoveGameFromList(Lcom/android/settings/display/DarkModeAppsSettingFragment;Ljava/util/ArrayList;)V

    :goto_0
    return-void
.end method
