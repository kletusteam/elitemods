.class public Lcom/android/settings/PlatformUtils;
.super Ljava/lang/Object;


# direct methods
.method public static getDefaultStreamVolume(I)I
    .locals 0

    invoke-static {p0}, Landroid/media/AudioSystem;->getDefaultStreamVolume(I)I

    move-result p0

    return p0
.end method

.method public static getTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-static {p1, p0, p2}, Landroid/telephony/TelephonyManager;->getTelephonyProperty(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
