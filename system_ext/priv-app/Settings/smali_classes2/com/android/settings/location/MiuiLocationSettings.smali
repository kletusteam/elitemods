.class public Lcom/android/settings/location/MiuiLocationSettings;
.super Lcom/android/settings/dashboard/DashboardFragment;

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/settingslib/widget/RadioButtonPreference$OnClickListener;


# instance fields
.field private mAGpsParas:Landroidx/preference/Preference;

.field private mActive:Z

.field private mAgpsEnabled:Z

.field private mAgpsRoaming:Landroidx/preference/CheckBoxPreference;

.field private mAssistedGps:Landroidx/preference/CheckBoxPreference;

.field private mBatterySaving:Lcom/android/settingslib/widget/RadioButtonPreference;

.field private mCurrentMode:I

.field private mEditor:Landroid/content/SharedPreferences$Editor;

.field private mHasGpsFeature:Z

.field private mHighAccuracy:Lcom/android/settingslib/widget/RadioButtonPreference;

.field private mLocationManager:Landroid/location/LocationManager;

.field private mLocationMode:Landroidx/preference/Preference;

.field private mManagedProfile:Landroid/os/UserHandle;

.field private mManagedProfileSwitch:Lcom/android/settingslib/RestrictedSwitchPreference;

.field private mManagedProfileSwitchClickListener:Landroidx/preference/Preference$OnPreferenceClickListener;

.field private mModeChangeReceiver:Landroid/content/BroadcastReceiver;

.field private mSensorsOnly:Lcom/android/settingslib/widget/RadioButtonPreference;

.field private mSharedSP:Landroid/content/SharedPreferences;

.field private mSwitch:Lcom/android/settingslib/RestrictedSwitchPreference;

.field private mUm:Landroid/os/UserManager;

.field private mValidListener:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetmAgpsRoaming(Lcom/android/settings/location/MiuiLocationSettings;)Landroidx/preference/CheckBoxPreference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mAgpsRoaming:Landroidx/preference/CheckBoxPreference;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLocationManager(Lcom/android/settings/location/MiuiLocationSettings;)Landroid/location/LocationManager;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mLocationManager:Landroid/location/LocationManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmManagedProfile(Lcom/android/settings/location/MiuiLocationSettings;)Landroid/os/UserHandle;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mManagedProfile:Landroid/os/UserHandle;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmManagedProfileSwitch(Lcom/android/settings/location/MiuiLocationSettings;)Lcom/android/settingslib/RestrictedSwitchPreference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mManagedProfileSwitch:Lcom/android/settingslib/RestrictedSwitchPreference;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmUm(Lcom/android/settings/location/MiuiLocationSettings;)Landroid/os/UserManager;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mUm:Landroid/os/UserManager;

    return-object p0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/dashboard/DashboardFragment;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mActive:Z

    new-instance v0, Lcom/android/settings/location/MiuiLocationSettings$5;

    invoke-direct {v0, p0}, Lcom/android/settings/location/MiuiLocationSettings$5;-><init>(Lcom/android/settings/location/MiuiLocationSettings;)V

    iput-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mManagedProfileSwitchClickListener:Landroidx/preference/Preference$OnPreferenceClickListener;

    return-void
.end method

.method private addLocationServices(Landroid/content/Context;Landroidx/preference/PreferenceScreen;Z)V
    .locals 0

    return-void
.end method

.method private changeManagedProfileLocationAccessStatus(Z)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mManagedProfileSwitch:Lcom/android/settingslib/RestrictedSwitchPreference;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mManagedProfile:Landroid/os/UserHandle;

    invoke-virtual {v0}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/location/MiuiLocationSettings;->getShareLocationEnforcedAdmin(I)Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/settings/location/MiuiLocationSettings;->isManagedProfileRestrictedByBase()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mManagedProfileSwitch:Lcom/android/settingslib/RestrictedSwitchPreference;

    invoke-virtual {p1, v0}, Lcom/android/settingslib/RestrictedSwitchPreference;->setDisabledByAdmin(Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)V

    iget-object p0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mManagedProfileSwitch:Lcom/android/settingslib/RestrictedSwitchPreference;

    invoke-virtual {p0, v2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    goto :goto_2

    :cond_1
    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mManagedProfileSwitch:Lcom/android/settingslib/RestrictedSwitchPreference;

    invoke-virtual {v0, p1}, Lcom/android/settingslib/RestrictedSwitchPreference;->setEnabled(Z)V

    sget v0, Lcom/android/settings/R$string;->switch_off_text:I

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mManagedProfileSwitch:Lcom/android/settingslib/RestrictedSwitchPreference;

    invoke-virtual {p1, v2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mManagedProfileSwitch:Lcom/android/settingslib/RestrictedSwitchPreference;

    xor-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    if-eqz v1, :cond_3

    goto :goto_0

    :cond_3
    sget p1, Lcom/android/settings/R$string;->switch_on_text:I

    move v0, p1

    :goto_0
    iget-object p1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mManagedProfileSwitch:Lcom/android/settingslib/RestrictedSwitchPreference;

    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mManagedProfileSwitchClickListener:Landroidx/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {p1, v1}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    :goto_1
    iget-object p0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mManagedProfileSwitch:Lcom/android/settingslib/RestrictedSwitchPreference;

    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setSummary(I)V

    :goto_2
    return-void
.end method

.method private createPreferenceHierarchy()Landroidx/preference/PreferenceScreen;
    .locals 6

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroidx/preference/PreferenceGroup;->removeAll()V

    :cond_0
    sget v0, Lcom/android/settings/R$xml;->miui_location_settings:I

    invoke-virtual {p0, v0}, Lcom/android/settings/SettingsPreferenceFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    const-class v1, Lcom/android/settings/location/RecentLocationAccessPreferenceController;

    invoke-virtual {p0, v1}, Lcom/android/settings/dashboard/DashboardFragment;->use(Ljava/lang/Class;)Lcom/android/settingslib/core/AbstractPreferenceController;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1, v0}, Lcom/android/settingslib/core/AbstractPreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    invoke-virtual {v1}, Lcom/android/settingslib/core/AbstractPreferenceController;->isAvailable()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Lcom/android/settingslib/core/AbstractPreferenceController;->getPreferenceKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/settings/SettingsPreferenceFragment;->removePreference(Ljava/lang/String;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lcom/android/settingslib/core/AbstractPreferenceController;->getPreferenceKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/settingslib/core/AbstractPreferenceController;->updateState(Landroidx/preference/Preference;)V

    :cond_2
    :goto_0
    invoke-direct {p0, v0}, Lcom/android/settings/location/MiuiLocationSettings;->setupManagedProfileCategory(Landroidx/preference/PreferenceScreen;)V

    const-string v1, "location_toggle"

    invoke-virtual {p0, v1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/RestrictedSwitchPreference;

    iput-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mSwitch:Lcom/android/settingslib/RestrictedSwitchPreference;

    invoke-virtual {v1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    const-string v1, "location_mode"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mLocationMode:Landroidx/preference/Preference;

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "android.hardware.location.gps"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mHasGpsFeature:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mLocationMode:Landroidx/preference/Preference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mLocationMode:Landroidx/preference/Preference;

    goto :goto_1

    :cond_3
    const-string v1, "high_accuracy"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/widget/RadioButtonPreference;

    iput-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mHighAccuracy:Lcom/android/settingslib/widget/RadioButtonPreference;

    const-string v1, "battery_saving"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/widget/RadioButtonPreference;

    iput-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mBatterySaving:Lcom/android/settingslib/widget/RadioButtonPreference;

    const-string/jumbo v1, "sensors_only"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/widget/RadioButtonPreference;

    iput-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mSensorsOnly:Lcom/android/settingslib/widget/RadioButtonPreference;

    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mHighAccuracy:Lcom/android/settingslib/widget/RadioButtonPreference;

    invoke-virtual {v1, p0}, Lcom/android/settingslib/widget/RadioButtonPreference;->setOnClickListener(Lcom/android/settingslib/widget/RadioButtonPreference$OnClickListener;)V

    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mBatterySaving:Lcom/android/settingslib/widget/RadioButtonPreference;

    invoke-virtual {v1, p0}, Lcom/android/settingslib/widget/RadioButtonPreference;->setOnClickListener(Lcom/android/settingslib/widget/RadioButtonPreference$OnClickListener;)V

    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mSensorsOnly:Lcom/android/settingslib/widget/RadioButtonPreference;

    invoke-virtual {v1, p0}, Lcom/android/settingslib/widget/RadioButtonPreference;->setOnClickListener(Lcom/android/settingslib/widget/RadioButtonPreference$OnClickListener;)V

    :goto_1
    invoke-static {}, Lcom/android/settings/AgpsSettings;->isAgpsEnabled()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mAgpsEnabled:Z

    const-string v1, "assisted_gps"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v1

    check-cast v1, Landroidx/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mAssistedGps:Landroidx/preference/CheckBoxPreference;

    const-string v1, "agps_roaming"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v1

    check-cast v1, Landroidx/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mAgpsRoaming:Landroidx/preference/CheckBoxPreference;

    invoke-static {}, Lcom/android/settings/MiuiUtils;->getInstance()Lcom/android/settings/MiuiUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/location/MiuiLocationSettings;->mLocationManager:Landroid/location/LocationManager;

    invoke-virtual {v2, v3}, Lcom/android/settings/MiuiUtils;->getAgpsRoaming(Landroid/location/LocationManager;)I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-ne v2, v4, :cond_4

    move v2, v4

    goto :goto_2

    :cond_4
    move v2, v3

    :goto_2
    invoke-virtual {v1, v2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    const-string v1, "assisted_gps_params"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mAGpsParas:Landroidx/preference/Preference;

    iget-boolean v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mAgpsEnabled:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mAssistedGps:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mAGpsParas:Landroidx/preference/Preference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mAgpsRoaming:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    goto :goto_3

    :cond_5
    const-string/jumbo v1, "support_agps_paras"

    invoke-static {v1, v3}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mAgpsRoaming:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    goto :goto_3

    :cond_6
    const-string/jumbo v1, "support_agps_roaming"

    invoke-static {v1, v3}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mAGpsParas:Landroidx/preference/Preference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_7
    :goto_3
    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mAssistedGps:Landroidx/preference/CheckBoxPreference;

    if-eqz v1, :cond_9

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v5, "assisted_gps_enabled"

    invoke-static {v2, v5, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v4, :cond_8

    move v2, v4

    goto :goto_4

    :cond_8
    move v2, v3

    :goto_4
    invoke-virtual {v1, v2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :cond_9
    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mManagedProfile:Landroid/os/UserHandle;

    if-eqz v1, :cond_a

    iget-object v2, p0, Lcom/android/settings/location/MiuiLocationSettings;->mUm:Landroid/os/UserManager;

    const-string/jumbo v5, "no_share_location"

    invoke-virtual {v2, v5, v1}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;Landroid/os/UserHandle;)Z

    move-result v1

    if-eqz v1, :cond_a

    move v3, v4

    :cond_a
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p0, v1, v0, v3}, Lcom/android/settings/location/MiuiLocationSettings;->addLocationServices(Landroid/content/Context;Landroidx/preference/PreferenceScreen;Z)V

    invoke-virtual {p0, v4}, Landroidx/fragment/app/Fragment;->setHasOptionsMenu(Z)V

    invoke-virtual {p0}, Lcom/android/settings/location/MiuiLocationSettings;->refreshLocationMode()V

    return-object v0
.end method

.method private isManagedProfileRestrictedByBase()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mManagedProfile:Landroid/os/UserHandle;

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    iget-object p0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mUm:Landroid/os/UserManager;

    const-string/jumbo v1, "no_share_location"

    invoke-virtual {p0, v1, v0}, Landroid/os/UserManager;->hasBaseUserRestriction(Ljava/lang/String;Landroid/os/UserHandle;)Z

    move-result p0

    return p0
.end method

.method private isRestricted()Z
    .locals 1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    const-string/jumbo v0, "user"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/os/UserManager;

    const-string/jumbo v0, "no_share_location"

    invoke-virtual {p0, v0}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method private setupManagedProfileCategory(Landroidx/preference/PreferenceScreen;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mUm:Landroid/os/UserManager;

    invoke-static {v0}, Lcom/android/settings/Utils;->getManagedProfile(Landroid/os/UserManager;)Landroid/os/UserHandle;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mManagedProfile:Landroid/os/UserHandle;

    const/4 v1, 0x0

    const-string v2, "managed_profile_location_switch"

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lcom/android/settings/SettingsPreferenceFragment;->removePreference(Ljava/lang/String;)Z

    iput-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mManagedProfileSwitch:Lcom/android/settingslib/RestrictedSwitchPreference;

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v2}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settingslib/RestrictedSwitchPreference;

    iput-object p1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mManagedProfileSwitch:Lcom/android/settingslib/RestrictedSwitchPreference;

    invoke-virtual {p1, v1}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    :goto_0
    return-void
.end method

.method private updateRadioButtons(Lcom/android/settingslib/widget/RadioButtonPreference;)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mHighAccuracy:Lcom/android/settingslib/widget/RadioButtonPreference;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    if-nez p1, :cond_1

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mBatterySaving:Lcom/android/settingslib/widget/RadioButtonPreference;

    invoke-virtual {p1, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mSensorsOnly:Lcom/android/settingslib/widget/RadioButtonPreference;

    invoke-virtual {p0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    if-ne p1, v0, :cond_2

    invoke-virtual {v0, v2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mBatterySaving:Lcom/android/settingslib/widget/RadioButtonPreference;

    invoke-virtual {p1, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mSensorsOnly:Lcom/android/settingslib/widget/RadioButtonPreference;

    invoke-virtual {p0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/android/settings/location/MiuiLocationSettings;->mBatterySaving:Lcom/android/settingslib/widget/RadioButtonPreference;

    if-ne p1, v3, :cond_3

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mBatterySaving:Lcom/android/settingslib/widget/RadioButtonPreference;

    invoke-virtual {p1, v2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mSensorsOnly:Lcom/android/settingslib/widget/RadioButtonPreference;

    invoke-virtual {p0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/android/settings/location/MiuiLocationSettings;->mSensorsOnly:Lcom/android/settingslib/widget/RadioButtonPreference;

    if-ne p1, v3, :cond_4

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mBatterySaving:Lcom/android/settingslib/widget/RadioButtonPreference;

    invoke-virtual {p1, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mSensorsOnly:Lcom/android/settingslib/widget/RadioButtonPreference;

    invoke-virtual {p0, v2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :cond_4
    :goto_0
    return-void
.end method


# virtual methods
.method protected createPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/settingslib/core/AbstractPreferenceController;",
            ">;"
        }
    .end annotation

    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    return-object p0
.end method

.method protected getLogTag()Ljava/lang/String;
    .locals 0

    const-string p0, "MiuiLocationSettings"

    return-object p0
.end method

.method protected getPreferenceScreenResId()I
    .locals 0

    sget p0, Lcom/android/settings/R$xml;->miui_location_settings:I

    return p0
.end method

.method getShareLocationEnforcedAdmin(I)Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;
    .locals 2

    goto/32 :goto_8

    nop

    :goto_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    goto/32 :goto_1

    nop

    :goto_1
    const-string/jumbo v0, "no_config_location"

    goto/32 :goto_5

    nop

    :goto_2
    invoke-static {v0, v1, p1}, Lcom/android/settingslib/RestrictedLockUtilsInternal;->checkIfRestrictionEnforced(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_3
    if-eqz v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_0

    nop

    :goto_4
    const-string/jumbo v1, "no_share_location"

    goto/32 :goto_2

    nop

    :goto_5
    invoke-static {p0, v0, p1}, Lcom/android/settingslib/RestrictedLockUtilsInternal;->checkIfRestrictionEnforced(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    move-result-object v0

    :goto_6
    goto/32 :goto_7

    nop

    :goto_7
    return-object v0

    :goto_8
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    goto/32 :goto_4

    nop
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/dashboard/DashboardFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Lcom/android/settings/location/MiuiLocationSettings$1;

    invoke-direct {p1, p0}, Lcom/android/settings/location/MiuiLocationSettings$1;-><init>(Lcom/android/settings/location/MiuiLocationSettings;)V

    iput-object p1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mModeChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/location/LocationManager;

    iput-object p1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mLocationManager:Landroid/location/LocationManager;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    const-string v0, "location_last_mode"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mSharedSP:Landroid/content/SharedPreferences;

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mEditor:Landroid/content/SharedPreferences$Editor;

    const-string/jumbo p1, "user"

    invoke-virtual {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/os/UserManager;

    iput-object p1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mUm:Landroid/os/UserManager;

    invoke-static {p1}, Lcom/android/settings/Utils;->getManagedProfile(Landroid/os/UserManager;)Landroid/os/UserHandle;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mManagedProfile:Landroid/os/UserHandle;

    return-void
.end method

.method public onModeChanged(IZ)V
    .locals 6

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/location/MiuiLocationSettings;->getShareLocationEnforcedAdmin(I)Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    const-string/jumbo v3, "no_share_location"

    invoke-static {v1, v3, v2}, Lcom/android/settingslib/RestrictedLockUtilsInternal;->hasBaseUserRestriction(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz p1, :cond_0

    move v4, v3

    goto :goto_0

    :cond_0
    move v4, v2

    :goto_0
    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mSwitch:Lcom/android/settingslib/RestrictedSwitchPreference;

    invoke-virtual {v1, v0}, Lcom/android/settingslib/RestrictedSwitchPreference;->setDisabledByAdmin(Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mSwitch:Lcom/android/settingslib/RestrictedSwitchPreference;

    xor-int/lit8 v1, p2, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/RestrictedSwitchPreference;->setEnabled(Z)V

    :goto_1
    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mSwitch:Lcom/android/settingslib/RestrictedSwitchPreference;

    invoke-virtual {v0}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v0

    const/4 v1, 0x0

    if-eq v4, v0, :cond_3

    iget-boolean v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mValidListener:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mSwitch:Lcom/android/settingslib/RestrictedSwitchPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mSwitch:Lcom/android/settingslib/RestrictedSwitchPreference;

    invoke-virtual {v0, v4}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-boolean v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mValidListener:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mSwitch:Lcom/android/settingslib/RestrictedSwitchPreference;

    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mLocationMode:Landroidx/preference/Preference;

    if-nez v0, :cond_4

    return-void

    :cond_4
    if-eqz p1, :cond_8

    if-eq p1, v3, :cond_7

    const/4 v1, 0x2

    if-eq p1, v1, :cond_6

    const/4 v1, 0x3

    if-eq p1, v1, :cond_5

    goto :goto_2

    :cond_5
    sget v1, Lcom/android/settings/R$string;->location_mode_high_accuracy_title:I

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setSummary(I)V

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mHighAccuracy:Lcom/android/settingslib/widget/RadioButtonPreference;

    invoke-direct {p0, v0}, Lcom/android/settings/location/MiuiLocationSettings;->updateRadioButtons(Lcom/android/settingslib/widget/RadioButtonPreference;)V

    goto :goto_2

    :cond_6
    sget v1, Lcom/android/settings/R$string;->location_mode_battery_saving_title:I

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setSummary(I)V

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mBatterySaving:Lcom/android/settingslib/widget/RadioButtonPreference;

    invoke-direct {p0, v0}, Lcom/android/settings/location/MiuiLocationSettings;->updateRadioButtons(Lcom/android/settingslib/widget/RadioButtonPreference;)V

    goto :goto_2

    :cond_7
    sget v1, Lcom/android/settings/R$string;->location_mode_sensors_only_title:I

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setSummary(I)V

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mSensorsOnly:Lcom/android/settingslib/widget/RadioButtonPreference;

    invoke-direct {p0, v0}, Lcom/android/settings/location/MiuiLocationSettings;->updateRadioButtons(Lcom/android/settingslib/widget/RadioButtonPreference;)V

    goto :goto_2

    :cond_8
    sget v5, Lcom/android/settings/R$string;->location_mode_location_off_title:I

    invoke-virtual {v0, v5}, Landroidx/preference/Preference;->setSummary(I)V

    invoke-direct {p0, v1}, Lcom/android/settings/location/MiuiLocationSettings;->updateRadioButtons(Lcom/android/settingslib/widget/RadioButtonPreference;)V

    :goto_2
    if-eqz p1, :cond_9

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mEditor:Landroid/content/SharedPreferences$Editor;

    const-string v1, "last_mode"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_9
    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mLocationMode:Landroidx/preference/Preference;

    if-eqz v4, :cond_a

    if-nez p2, :cond_a

    move v1, v3

    goto :goto_3

    :cond_a
    move v1, v2

    :goto_3
    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setEnabled(Z)V

    if-eqz p1, :cond_b

    if-nez p2, :cond_b

    move v2, v3

    :cond_b
    iget-object p1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mHighAccuracy:Lcom/android/settingslib/widget/RadioButtonPreference;

    invoke-virtual {p1, v2}, Landroidx/preference/Preference;->setEnabled(Z)V

    iget-object p1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mBatterySaving:Lcom/android/settingslib/widget/RadioButtonPreference;

    invoke-virtual {p1, v2}, Landroidx/preference/Preference;->setEnabled(Z)V

    iget-object p1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mSensorsOnly:Lcom/android/settingslib/widget/RadioButtonPreference;

    invoke-virtual {p1, v2}, Landroidx/preference/Preference;->setEnabled(Z)V

    invoke-direct {p0, v2}, Lcom/android/settings/location/MiuiLocationSettings;->changeManagedProfileLocationAccessStatus(Z)V

    return-void
.end method

.method public onPause()V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mModeChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "MiuiLocationSettings"

    const-string v2, "Error"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onPause()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mActive:Z

    iput-boolean v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mValidListener:Z

    iget-object p0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mSwitch:Lcom/android/settingslib/RestrictedSwitchPreference;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    const-string v0, "location_toggle"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mSharedSP:Landroid/content/SharedPreferences;

    iget-boolean p2, p0, Lcom/android/settings/location/MiuiLocationSettings;->mHasGpsFeature:Z

    if-eqz p2, :cond_1

    const/4 p2, 0x3

    goto :goto_0

    :cond_1
    const/4 p2, 0x2

    :goto_0
    const-string v1, "last_mode"

    invoke-interface {p1, v1, p2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/android/settings/location/MiuiLocationSettings;->setLocationMode(I)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0, v0}, Lcom/android/settings/location/MiuiLocationSettings;->setLocationMode(I)V

    :goto_1
    return v0
.end method

.method public onPreferenceTreeClick(Landroidx/preference/PreferenceScreen;Landroidx/preference/Preference;)Z
    .locals 5

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/location/MiuiLocationSettings;->mAssistedGps:Landroidx/preference/CheckBoxPreference;

    if-ne p2, v1, :cond_3

    invoke-virtual {v1}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_2

    const-string p1, "assisted_gps_supl_host"

    invoke-static {v0, p1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string v1, "assisted_gps_supl_port"

    if-eqz p2, :cond_0

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    if-nez p2, :cond_2

    :cond_0
    const/4 p2, 0x0

    :try_start_0
    new-instance v2, Ljava/util/Properties;

    invoke-direct {v2}, Ljava/util/Properties;-><init>()V

    new-instance v3, Ljava/io/File;

    const-string v4, "/etc/gps.conf"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v2, v4}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    const-string v3, "SUPL_HOST"

    invoke-virtual {v2, v3, p2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, p1, v3}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    const-string p1, "SUPL_PORT"

    invoke-virtual {v2, p1, p2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_2

    :catchall_0
    move-exception p0

    move-object p2, v4

    goto :goto_1

    :catch_0
    move-exception p1

    move-object p2, v4

    goto :goto_0

    :catchall_1
    move-exception p0

    goto :goto_1

    :catch_1
    move-exception p1

    :goto_0
    :try_start_3
    const-string v1, "LocationSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not open GPS configuration file /etc/gps.conf, e="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz p2, :cond_2

    :try_start_4
    invoke-virtual {p2}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_2

    :goto_1
    if-eqz p2, :cond_1

    :try_start_5
    invoke-virtual {p2}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    :catch_2
    :cond_1
    throw p0

    :catch_3
    :cond_2
    :goto_2
    iget-object p0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mAssistedGps:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p0}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result p0

    const-string p1, "assisted_gps_enabled"

    invoke-static {v0, p1, p0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_3

    :cond_3
    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mAgpsRoaming:Landroidx/preference/CheckBoxPreference;

    if-ne p2, v0, :cond_5

    invoke-virtual {v0}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_4

    new-instance p1, Lmiuix/appcompat/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    invoke-direct {p1, p2}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/high16 p2, 0x1040000

    new-instance v0, Lcom/android/settings/location/MiuiLocationSettings$3;

    invoke-direct {v0, p0}, Lcom/android/settings/location/MiuiLocationSettings$3;-><init>(Lcom/android/settings/location/MiuiLocationSettings;)V

    invoke-virtual {p1, p2, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    const p2, 0x104000a

    new-instance v0, Lcom/android/settings/location/MiuiLocationSettings$2;

    invoke-direct {v0, p0}, Lcom/android/settings/location/MiuiLocationSettings$2;-><init>(Lcom/android/settings/location/MiuiLocationSettings;)V

    invoke-virtual {p1, p2, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    sget p2, Lcom/android/settings/R$string;->agps_roaming_dia_title:I

    invoke-virtual {p0, p2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    sget p2, Lcom/android/settings/R$string;->agps_roaming_dia:I

    invoke-virtual {p0, p2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object p0

    invoke-virtual {p0}, Landroid/app/Dialog;->show()V

    goto :goto_3

    :cond_4
    invoke-static {}, Lcom/android/settings/MiuiUtils;->getInstance()Lcom/android/settings/MiuiUtils;

    move-result-object p1

    iget-object p0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mLocationManager:Landroid/location/LocationManager;

    const/4 p2, 0x0

    invoke-virtual {p1, p0, p2}, Lcom/android/settings/MiuiUtils;->setAgpsRoaming(Landroid/location/LocationManager;I)V

    :goto_3
    const/4 p0, 0x1

    return p0

    :cond_5
    invoke-super {p0, p1, p2}, Lcom/android/settings/SettingsPreferenceFragment;->onPreferenceTreeClick(Landroidx/preference/PreferenceScreen;Landroidx/preference/Preference;)Z

    move-result p0

    return p0
.end method

.method public onRadioButtonClicked(Lcom/android/settingslib/widget/RadioButtonPreference;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mHighAccuracy:Lcom/android/settingslib/widget/RadioButtonPreference;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x3

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mBatterySaving:Lcom/android/settingslib/widget/RadioButtonPreference;

    if-ne p1, v0, :cond_1

    const/4 p1, 0x2

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mSensorsOnly:Lcom/android/settingslib/widget/RadioButtonPreference;

    if-ne p1, v0, :cond_2

    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/settings/location/MiuiLocationSettings;->setLocationMode(I)V

    return-void
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Lcom/android/settings/dashboard/DashboardFragment;->onResume()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mActive:Z

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.location.MODE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/location/MiuiLocationSettings;->mModeChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iput-boolean v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mValidListener:Z

    invoke-direct {p0}, Lcom/android/settings/location/MiuiLocationSettings;->createPreferenceHierarchy()Landroidx/preference/PreferenceScreen;

    return-void
.end method

.method public refreshLocationMode()V
    .locals 3

    iget-boolean v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mActive:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "location_mode"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mCurrentMode:I

    const/4 v1, 0x4

    const-string v2, "MiuiLocationSettings"

    invoke-static {v2, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Location mode has been changed"

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/location/MiuiLocationSettings;->isRestricted()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/settings/location/MiuiLocationSettings;->onModeChanged(IZ)V

    :cond_1
    return-void
.end method

.method public setLocationMode(I)V
    .locals 4

    invoke-direct {p0}, Lcom/android/settings/location/MiuiLocationSettings;->isRestricted()Z

    move-result v0

    const-string v1, "location_mode"

    if-eqz v0, :cond_2

    const/4 p1, 0x4

    const-string v0, "MiuiLocationSettings"

    invoke-static {v0, p1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "Restricted user, not setting location mode"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const/4 v0, 0x0

    invoke-static {p1, v1, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p1

    iget-boolean v0, p0, Lcom/android/settings/location/MiuiLocationSettings;->mActive:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/location/MiuiLocationSettings;->onModeChanged(IZ)V

    :cond_1
    return-void

    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.android.settings.location.MODE_CHANGING"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/android/settings/location/MiuiLocationSettings;->mCurrentMode:I

    const-string v3, "CURRENT_MODE"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "NEW_MODE"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    const-string v3, "android.permission.WRITE_SECURE_SETTINGS"

    invoke-virtual {v2, v0, v3}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-virtual {p0}, Lcom/android/settings/location/MiuiLocationSettings;->refreshLocationMode()V

    return-void
.end method
