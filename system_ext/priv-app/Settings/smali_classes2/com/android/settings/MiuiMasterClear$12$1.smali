.class Lcom/android/settings/MiuiMasterClear$12$1;
.super Lcom/miui/tsmclient/service/ICallback$Stub;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/MiuiMasterClear$12;->lambda$onWipeFinished$1()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/settings/MiuiMasterClear$12;


# direct methods
.method public static synthetic $r8$lambda$-Wz9szHyXyxeYW5ok6KnkX64Ij4(Lcom/android/settings/MiuiMasterClear;)V
    .locals 0

    invoke-static {p0}, Lcom/android/settings/MiuiMasterClear$12$1;->lambda$onError$1(Lcom/android/settings/MiuiMasterClear;)V

    return-void
.end method

.method public static synthetic $r8$lambda$Xdw2wzfsVKHNvukh-xzu8km09WQ(Lcom/android/settings/MiuiMasterClear;)V
    .locals 0

    invoke-static {p0}, Lcom/android/settings/MiuiMasterClear$12$1;->lambda$onSuccess$0(Lcom/android/settings/MiuiMasterClear;)V

    return-void
.end method

.method constructor <init>(Lcom/android/settings/MiuiMasterClear$12;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/MiuiMasterClear$12$1;->this$1:Lcom/android/settings/MiuiMasterClear$12;

    invoke-direct {p0}, Lcom/miui/tsmclient/service/ICallback$Stub;-><init>()V

    return-void
.end method

.method private static synthetic lambda$onError$1(Lcom/android/settings/MiuiMasterClear;)V
    .locals 0

    invoke-static {p0}, Lcom/android/settings/MiuiMasterClear;->-$$Nest$mdoMasterClear(Lcom/android/settings/MiuiMasterClear;)V

    return-void
.end method

.method private static synthetic lambda$onSuccess$0(Lcom/android/settings/MiuiMasterClear;)V
    .locals 0

    invoke-static {p0}, Lcom/android/settings/MiuiMasterClear;->-$$Nest$mdoMasterClear(Lcom/android/settings/MiuiMasterClear;)V

    return-void
.end method


# virtual methods
.method public onError(ILjava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "clean car key fail. error code : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ", message : "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "MiuiMasterClear"

    invoke-static {p2, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p0, p0, Lcom/android/settings/MiuiMasterClear$12$1;->this$1:Lcom/android/settings/MiuiMasterClear$12;

    iget-object p0, p0, Lcom/android/settings/MiuiMasterClear$12;->this$0:Lcom/android/settings/MiuiMasterClear;

    new-instance p1, Lcom/android/settings/MiuiMasterClear$12$1$$ExternalSyntheticLambda0;

    invoke-direct {p1, p0}, Lcom/android/settings/MiuiMasterClear$12$1$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/MiuiMasterClear;)V

    invoke-static {p1}, Lcom/android/settingslib/utils/ThreadUtils;->postOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onSuccess(Landroid/os/Bundle;)V
    .locals 1

    const-string p1, "MiuiMasterClear"

    const-string v0, "clean car key data success"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p0, p0, Lcom/android/settings/MiuiMasterClear$12$1;->this$1:Lcom/android/settings/MiuiMasterClear$12;

    iget-object p0, p0, Lcom/android/settings/MiuiMasterClear$12;->this$0:Lcom/android/settings/MiuiMasterClear;

    new-instance p1, Lcom/android/settings/MiuiMasterClear$12$1$$ExternalSyntheticLambda1;

    invoke-direct {p1, p0}, Lcom/android/settings/MiuiMasterClear$12$1$$ExternalSyntheticLambda1;-><init>(Lcom/android/settings/MiuiMasterClear;)V

    invoke-static {p1}, Lcom/android/settingslib/utils/ThreadUtils;->postOnMainThread(Ljava/lang/Runnable;)V

    return-void
.end method
