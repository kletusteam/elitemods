.class public Lcom/android/settings/special/ExternalRamFragment;
.super Lcom/android/settings/dashboard/DashboardFragment;

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private mRamMapping:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mRange:[F

.field private mSelectedKey:Ljava/lang/String;

.field private mShowMultiLevel:Z


# direct methods
.method public static synthetic $r8$lambda$CdzhsN_YgVhMPNe-6ONf-NeOSC4(Landroid/content/Context;)V
    .locals 0

    invoke-static {p0}, Lcom/android/settings/special/ExternalRamFragment;->lambda$buildAlertDialog$0(Landroid/content/Context;)V

    return-void
.end method

.method public static synthetic $r8$lambda$oLMXdpdaCBxsFVzTl26_SXqwSzQ(Lcom/android/settings/special/ExternalRamFragment;Lmiuix/preference/RadioButtonPreference;Landroid/content/Context;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settings/special/ExternalRamFragment;->lambda$buildAlertDialog$1(Lmiuix/preference/RadioButtonPreference;Landroid/content/Context;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$ycbv2gBwNpHfCNiyfBMiVVmwSzM(Lcom/android/settings/special/ExternalRamFragment;Lmiuix/preference/RadioButtonPreference;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/special/ExternalRamFragment;->lambda$buildAlertDialog$2(Lmiuix/preference/RadioButtonPreference;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/dashboard/DashboardFragment;-><init>()V

    return-void
.end method

.method private buildAlertDialog(Lmiuix/preference/RadioButtonPreference;)V
    .locals 6

    sget v0, Lcom/android/settings/R$string;->external_ram_dialog_title:I

    sget v1, Lcom/android/settings/R$string;->external_ram_dialog_message:I

    sget v2, Lcom/android/settings/R$string;->external_ram_dialog_btn_ok:I

    sget v3, Lcom/android/settings/R$string;->external_ram_dialog_btn_cancel:I

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    new-instance v5, Lmiuix/appcompat/app/AlertDialog$Builder;

    invoke-direct {v5, v4}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    invoke-virtual {v5, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    new-instance v0, Lcom/android/settings/special/ExternalRamFragment$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1, v4}, Lcom/android/settings/special/ExternalRamFragment$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/special/ExternalRamFragment;Lmiuix/preference/RadioButtonPreference;Landroid/content/Context;)V

    invoke-virtual {v5, v2, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    new-instance v0, Lcom/android/settings/special/ExternalRamFragment$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/special/ExternalRamFragment$$ExternalSyntheticLambda1;-><init>(Lcom/android/settings/special/ExternalRamFragment;Lmiuix/preference/RadioButtonPreference;)V

    invoke-virtual {v5, v3, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    const/4 p0, 0x0

    invoke-virtual {v5, p0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setCancelable(Z)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object p0

    invoke-virtual {p0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private initPreferenceState()V
    .locals 15

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v1

    invoke-static {v0}, Lcom/android/settings/special/ExternalRamController;->isChecked(Landroid/content/Context;)Z

    move-result v2

    invoke-static {v0}, Lcom/android/settings/special/ExternalRamController;->getBdSize(Landroid/content/Context;)F

    move-result v3

    const/4 v4, 0x0

    move v5, v4

    move v6, v5

    :goto_0
    invoke-virtual {v1}, Landroidx/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v7

    if-ge v5, v7, :cond_8

    invoke-virtual {v1, v5}, Landroidx/preference/PreferenceGroup;->getPreference(I)Landroidx/preference/Preference;

    move-result-object v7

    instance-of v8, v7, Lmiuix/preference/RadioButtonPreference;

    if-nez v8, :cond_0

    goto/16 :goto_4

    :cond_0
    iget-object v8, p0, Lcom/android/settings/special/ExternalRamFragment;->mRange:[F

    array-length v8, v8

    if-ne v6, v8, :cond_1

    invoke-virtual {v7, v4}, Landroidx/preference/Preference;->setVisible(Z)V

    goto/16 :goto_4

    :cond_1
    invoke-virtual {v7}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    const-string v9, "key_close_external_ram"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    const/4 v10, 0x1

    if-eqz v9, :cond_4

    move-object v9, v7

    check-cast v9, Lmiuix/preference/RadioButtonPreference;

    if-eqz v2, :cond_3

    const/4 v11, 0x0

    cmpl-float v11, v3, v11

    if-nez v11, :cond_2

    goto :goto_1

    :cond_2
    move v10, v4

    :cond_3
    :goto_1
    invoke-virtual {v9, v10}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    goto :goto_3

    :cond_4
    iget-object v9, p0, Lcom/android/settings/special/ExternalRamFragment;->mRange:[F

    add-int/lit8 v11, v6, 0x1

    aget v6, v9, v6

    const/high16 v9, 0x44800000    # 1024.0f

    mul-float/2addr v9, v6

    float-to-long v12, v9

    invoke-static {v12, v13, v2, v0}, Lcom/android/settings/special/ExternalRamController;->isSupportExternalRam(JZLandroid/content/Context;)Z

    move-result v9

    if-nez v9, :cond_5

    invoke-virtual {v7, v4}, Landroidx/preference/Preference;->setEnabled(Z)V

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "initPreferenceState: "

    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v14, "setEnabled false"

    invoke-virtual {v9, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v14, "ExternalRamFragment"

    invoke-static {v14, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget-object v9, p0, Lcom/android/settings/special/ExternalRamFragment;->mRamMapping:Ljava/util/Map;

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-interface {v9, v8, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9, v6}, Lcom/android/settings/special/ExternalRamController;->getBdSizeString(Landroid/content/Context;F)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    move-object v9, v7

    check-cast v9, Lmiuix/preference/RadioButtonPreference;

    if-eqz v2, :cond_6

    cmpl-float v6, v3, v6

    if-nez v6, :cond_6

    goto :goto_2

    :cond_6
    move v10, v4

    :goto_2
    invoke-virtual {v9, v10}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    move v6, v11

    :goto_3
    check-cast v7, Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {v7}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v7

    if-eqz v7, :cond_7

    iput-object v8, p0, Lcom/android/settings/special/ExternalRamFragment;->mSelectedKey:Ljava/lang/String;

    :cond_7
    :goto_4
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    :cond_8
    return-void
.end method

.method private static synthetic lambda$buildAlertDialog$0(Landroid/content/Context;)V
    .locals 2

    const-wide/16 v0, 0x1f4

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    invoke-static {p0}, Lcom/android/settings/special/ExternalRamController;->rebootPhone(Landroid/content/Context;)V

    return-void
.end method

.method private synthetic lambda$buildAlertDialog$1(Lmiuix/preference/RadioButtonPreference;Landroid/content/Context;Landroid/content/DialogInterface;I)V
    .locals 0

    const/4 p3, -0x1

    if-ne p4, p3, :cond_0

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p3

    iget-object p4, p0, Lcom/android/settings/special/ExternalRamFragment;->mSelectedKey:Ljava/lang/String;

    invoke-virtual {p3, p4}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p3

    check-cast p3, Lmiuix/preference/RadioButtonPreference;

    const/4 p4, 0x0

    invoke-virtual {p3, p4}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    const/4 p3, 0x1

    invoke-virtual {p1, p3}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/special/ExternalRamFragment;->mSelectedKey:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/settings/special/ExternalRamFragment;->updateExmState(Ljava/lang/String;)V

    new-instance p0, Lcom/android/settings/special/ExternalRamFragment$$ExternalSyntheticLambda2;

    invoke-direct {p0, p2}, Lcom/android/settings/special/ExternalRamFragment$$ExternalSyntheticLambda2;-><init>(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/android/settingslib/utils/ThreadUtils;->postOnBackgroundThread(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    :cond_0
    return-void
.end method

.method private synthetic lambda$buildAlertDialog$2(Lmiuix/preference/RadioButtonPreference;Landroid/content/DialogInterface;I)V
    .locals 0

    const/4 p2, -0x2

    if-ne p3, p2, :cond_0

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/android/settings/special/ExternalRamFragment;->updateExmState(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private updateExmState(Ljava/lang/String;)V
    .locals 2

    const-string v0, "key_close_external_ram"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v1, v0, 0x1

    invoke-static {v1}, Lcom/android/settings/special/ExternalRamController;->setChecked(Z)V

    if-nez v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/special/ExternalRamFragment;->mRamMapping:Ljava/util/Map;

    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide p0

    invoke-static {p0, p1}, Lcom/android/settings/special/ExternalRamController;->setBdSize(J)Z

    :cond_0
    return-void
.end method

.method private updateRamPreferenceState(Landroidx/preference/Preference;)V
    .locals 0

    check-cast p1, Lmiuix/preference/RadioButtonPreference;

    invoke-direct {p0, p1}, Lcom/android/settings/special/ExternalRamFragment;->buildAlertDialog(Lmiuix/preference/RadioButtonPreference;)V

    return-void
.end method


# virtual methods
.method protected getLogTag()Ljava/lang/String;
    .locals 0

    const-string p0, "ExternalRamFragment"

    return-object p0
.end method

.method protected getPreferenceScreenResId()I
    .locals 1

    invoke-static {}, Lcom/android/settings/special/ExternalRamController;->getExternalAdjustableRange()[F

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/special/ExternalRamFragment;->mRange:[F

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->isSupportMultiLevelExtm()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/special/ExternalRamFragment;->mRange:[F

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/android/settings/special/ExternalRamFragment;->mShowMultiLevel:Z

    if-eqz v0, :cond_1

    sget p0, Lcom/android/settings/R$xml;->external_ram_settings_global:I

    goto :goto_1

    :cond_1
    sget p0, Lcom/android/settings/R$xml;->external_ram_settings:I

    :goto_1
    return p0
.end method

.method public onPreferenceClick(Landroidx/preference/Preference;)Z
    .locals 6

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/special/ExternalRamFragment;->mSelectedKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    check-cast p1, Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {p1, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    return v1

    :cond_0
    move-object v0, p1

    check-cast v0, Lmiuix/preference/RadioButtonPreference;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "key_close_external_ram"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/android/settings/special/ExternalRamFragment;->mRamMapping:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v3}, Lcom/android/settings/special/ExternalRamController;->isChecked(Landroid/content/Context;)Z

    move-result v0

    invoke-static {v4, v5, v0, v3}, Lcom/android/settings/special/ExternalRamController;->isSupportExternalRam(JZLandroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1, v2}, Landroidx/preference/Preference;->setEnabled(Z)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    sget p1, Lcom/android/settings/R$string;->external_ram_toast:I

    invoke-static {p0, p1, v1}, Lcom/android/settingslib/util/ToastUtil;->show(Landroid/content/Context;II)V

    return v1

    :cond_1
    invoke-direct {p0, p1}, Lcom/android/settings/special/ExternalRamFragment;->updateRamPreferenceState(Landroidx/preference/Preference;)V

    return v1
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/dashboard/DashboardFragment;->onStart()V

    iget-boolean v0, p0, Lcom/android/settings/special/ExternalRamFragment;->mShowMultiLevel:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/special/ExternalRamFragment;->mRamMapping:Ljava/util/Map;

    invoke-direct {p0}, Lcom/android/settings/special/ExternalRamFragment;->initPreferenceState()V

    :cond_0
    return-void
.end method
