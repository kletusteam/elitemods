.class public Lcom/android/settings/MyCallsSettings;
.super Lcom/android/settings/MiuiSettingsPreferenceFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiSettingsPreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public CreateXml(Ljava/lang/String;)I
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/MyCallsSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string/jumbo v1, "xml"

    const-string/jumbo v2, "com.android.settings"

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-class v0, Lcom/android/settings/MyCallsSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    invoke-super {p0, p1}, Lcom/android/settings/MiuiSettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const-string/jumbo v0, "calls_settings"

    invoke-virtual {p0, v0}, Lcom/android/settings/MyCallsSettings;->CreateXml(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/MyCallsSettings;->addPreferencesFromResource(I)V

    return-void
.end method
