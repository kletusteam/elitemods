.class public Lcom/android/settings/widget/CommonNotification;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/widget/CommonNotification$Builder;
    }
.end annotation


# instance fields
.field private actionPendingIntent:Landroid/app/PendingIntent;

.field private actionText:Ljava/lang/String;

.field private channelId:Ljava/lang/String;

.field private channelName:Ljava/lang/String;

.field private contentText:Ljava/lang/CharSequence;

.field private contentTitle:Ljava/lang/CharSequence;

.field private context:Landroid/content/Context;

.field private enableFloat:Z

.field private enableKeyguard:Z

.field private importance:I

.field private isResident:Z

.field private largeIcon:I

.field private messageCount:I

.field private notificationIcon:I

.field private notifyId:I

.field private requestCode:I

.field private resultIntent:Landroid/content/Intent;

.field private smallIcon:I


# direct methods
.method static bridge synthetic -$$Nest$fputactionPendingIntent(Lcom/android/settings/widget/CommonNotification;Landroid/app/PendingIntent;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/CommonNotification;->actionPendingIntent:Landroid/app/PendingIntent;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputactionText(Lcom/android/settings/widget/CommonNotification;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/CommonNotification;->actionText:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputchannelId(Lcom/android/settings/widget/CommonNotification;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/CommonNotification;->channelId:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputchannelName(Lcom/android/settings/widget/CommonNotification;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/CommonNotification;->channelName:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputcontentText(Lcom/android/settings/widget/CommonNotification;Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/CommonNotification;->contentText:Ljava/lang/CharSequence;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputcontentTitle(Lcom/android/settings/widget/CommonNotification;Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/CommonNotification;->contentTitle:Ljava/lang/CharSequence;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputcontext(Lcom/android/settings/widget/CommonNotification;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/CommonNotification;->context:Landroid/content/Context;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputenableFloat(Lcom/android/settings/widget/CommonNotification;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/widget/CommonNotification;->enableFloat:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputenableKeyguard(Lcom/android/settings/widget/CommonNotification;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/widget/CommonNotification;->enableKeyguard:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputimportance(Lcom/android/settings/widget/CommonNotification;I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/widget/CommonNotification;->importance:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputisResident(Lcom/android/settings/widget/CommonNotification;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/widget/CommonNotification;->isResident:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputnotificationIcon(Lcom/android/settings/widget/CommonNotification;I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/widget/CommonNotification;->notificationIcon:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputnotifyId(Lcom/android/settings/widget/CommonNotification;I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/widget/CommonNotification;->notifyId:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputrequestCode(Lcom/android/settings/widget/CommonNotification;I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/widget/CommonNotification;->requestCode:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputresultIntent(Lcom/android/settings/widget/CommonNotification;Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/CommonNotification;->resultIntent:Landroid/content/Intent;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputsmallIcon(Lcom/android/settings/widget/CommonNotification;I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/widget/CommonNotification;->smallIcon:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/widget/CommonNotification;->messageCount:I

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    :cond_1
    check-cast p1, Lcom/android/settings/widget/CommonNotification;

    iget v2, p0, Lcom/android/settings/widget/CommonNotification;->notifyId:I

    iget v3, p1, Lcom/android/settings/widget/CommonNotification;->notifyId:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/android/settings/widget/CommonNotification;->channelId:Ljava/lang/String;

    iget-object v3, p1, Lcom/android/settings/widget/CommonNotification;->channelId:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object p0, p0, Lcom/android/settings/widget/CommonNotification;->channelName:Ljava/lang/String;

    iget-object p1, p1, Lcom/android/settings/widget/CommonNotification;->channelName:Ljava/lang/String;

    invoke-static {p0, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    goto :goto_0

    :cond_2
    move v0, v1

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    iget v1, p0, Lcom/android/settings/widget/CommonNotification;->notifyId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/android/settings/widget/CommonNotification;->channelId:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object p0, p0, Lcom/android/settings/widget/CommonNotification;->channelName:Ljava/lang/String;

    const/4 v1, 0x2

    aput-object p0, v0, v1

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result p0

    return p0
.end method

.method public sendCommonNotification(Z)V
    .locals 7

    iget-object v0, p0, Lcom/android/settings/widget/CommonNotification;->contentTitle:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/android/settings/widget/CommonNotification;->channelId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/android/settings/widget/CommonNotification;->channelName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_2

    :cond_0
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/android/settings/widget/CommonNotification;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/android/settings/R$layout;->notification_common_layout:I

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iget v1, p0, Lcom/android/settings/widget/CommonNotification;->notificationIcon:I

    if-eqz v1, :cond_1

    sget v2, Lcom/android/settings/R$id;->notification_icon:I

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    :cond_1
    iget-object v1, p0, Lcom/android/settings/widget/CommonNotification;->contentText:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/16 v2, 0x8

    if-eqz v1, :cond_2

    sget v1, Lcom/android/settings/R$id;->notification_summary:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0

    :cond_2
    sget v1, Lcom/android/settings/R$id;->notification_summary:I

    iget-object v3, p0, Lcom/android/settings/widget/CommonNotification;->contentText:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    :goto_0
    sget v1, Lcom/android/settings/R$id;->notification_text:I

    iget-object v3, p0, Lcom/android/settings/widget/CommonNotification;->contentTitle:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/widget/CommonNotification;->actionText:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget v1, Lcom/android/settings/R$id;->notification_button:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    iget v1, p0, Lcom/android/settings/widget/CommonNotification;->largeIcon:I

    if-eqz v1, :cond_4

    sget v1, Lcom/android/settings/R$id;->large_icon:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    iget v2, p0, Lcom/android/settings/widget/CommonNotification;->largeIcon:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_1

    :cond_3
    sget v1, Lcom/android/settings/R$id;->notification_button:I

    iget-object v2, p0, Lcom/android/settings/widget/CommonNotification;->actionText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/android/settings/widget/CommonNotification;->context:Landroid/content/Context;

    const-string/jumbo v2, "notification"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    iget-object v2, p0, Lcom/android/settings/widget/CommonNotification;->channelId:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/settings/widget/CommonNotification;->channelName:Ljava/lang/String;

    iget v4, p0, Lcom/android/settings/widget/CommonNotification;->importance:I

    invoke-static {v1, v2, v3, v4}, Lcom/android/settings/utils/NotificationUtils;->createNotificationChannel(Landroid/app/NotificationManager;Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/android/settings/widget/CommonNotification;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/settings/widget/CommonNotification;->channelId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/android/settings/utils/NotificationUtils;->createNotificationBuilder(Landroid/content/Context;Ljava/lang/String;)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    iget v3, p0, Lcom/android/settings/widget/CommonNotification;->importance:I

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    iget v3, p0, Lcom/android/settings/widget/CommonNotification;->smallIcon:I

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    iget-object v3, p0, Lcom/android/settings/widget/CommonNotification;->resultIntent:Landroid/content/Intent;

    if-eqz v3, :cond_5

    iget-object v4, p0, Lcom/android/settings/widget/CommonNotification;->context:Landroid/content/Context;

    iget v5, p0, Lcom/android/settings/widget/CommonNotification;->requestCode:I

    const/high16 v6, 0x4000000

    invoke-static {v4, v5, v3, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    :cond_5
    iget-object v3, p0, Lcom/android/settings/widget/CommonNotification;->actionPendingIntent:Landroid/app/PendingIntent;

    if-eqz v3, :cond_6

    sget v4, Lcom/android/settings/R$id;->notification_button:I

    invoke-virtual {v0, v4, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    :cond_6
    invoke-virtual {v2, v0}, Landroid/app/Notification$Builder;->setContent(Landroid/widget/RemoteViews;)Landroid/app/Notification$Builder;

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    iget v2, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v2, v2, 0x10

    iput v2, v0, Landroid/app/Notification;->flags:I

    iget-boolean v3, p0, Lcom/android/settings/widget/CommonNotification;->isResident:Z

    if-eqz v3, :cond_7

    or-int/lit8 v2, v2, 0x20

    iput v2, v0, Landroid/app/Notification;->flags:I

    :cond_7
    invoke-static {v0, p1}, Lcom/android/settings/utils/NotificationUtils;->setEnableFloat(Landroid/app/Notification;Z)V

    iget-boolean p1, p0, Lcom/android/settings/widget/CommonNotification;->enableKeyguard:Z

    invoke-static {v0, p1}, Lcom/android/settings/utils/NotificationUtils;->setEnableKeyguard(Landroid/app/Notification;Z)V

    const/4 p1, 0x1

    invoke-static {v0, p1}, Lcom/android/settings/utils/NotificationUtils;->setCustomizedIcon(Landroid/app/Notification;Z)V

    iget p1, p0, Lcom/android/settings/widget/CommonNotification;->messageCount:I

    if-ltz p1, :cond_8

    invoke-static {v0, p1}, Lcom/android/settings/utils/NotificationUtils;->setMessageCount(Landroid/app/Notification;I)V

    :cond_8
    iget p0, p0, Lcom/android/settings/widget/CommonNotification;->notifyId:I

    invoke-virtual {v1, p0, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void

    :cond_9
    :goto_2
    const-string p0, "CommonNotification"

    const-string p1, "Params not support!"

    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public show()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/CommonNotification;->contentTitle:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/widget/CommonNotification;->channelId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/widget/CommonNotification;->channelName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/widget/CommonNotification;->enableFloat:Z

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/CommonNotification;->sendCommonNotification(Z)V

    return-void

    :cond_1
    :goto_0
    const-string p0, "CommonNotification"

    const-string v0, "Params not support!"

    invoke-static {p0, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
