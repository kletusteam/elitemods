.class public Lcom/android/settings/widget/CommonNotification$Builder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/widget/CommonNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private cN:Lcom/android/settings/widget/CommonNotification;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/settings/widget/CommonNotification;

    invoke-direct {v0}, Lcom/android/settings/widget/CommonNotification;-><init>()V

    iput-object v0, p0, Lcom/android/settings/widget/CommonNotification$Builder;->cN:Lcom/android/settings/widget/CommonNotification;

    invoke-static {v0, p1}, Lcom/android/settings/widget/CommonNotification;->-$$Nest$fputcontext(Lcom/android/settings/widget/CommonNotification;Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public build()Lcom/android/settings/widget/CommonNotification;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/widget/CommonNotification$Builder;->cN:Lcom/android/settings/widget/CommonNotification;

    return-object p0
.end method

.method public setActionPendingIntent(Landroid/app/PendingIntent;)Lcom/android/settings/widget/CommonNotification$Builder;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/CommonNotification$Builder;->cN:Lcom/android/settings/widget/CommonNotification;

    invoke-static {v0, p1}, Lcom/android/settings/widget/CommonNotification;->-$$Nest$fputactionPendingIntent(Lcom/android/settings/widget/CommonNotification;Landroid/app/PendingIntent;)V

    return-object p0
.end method

.method public setActionText(Ljava/lang/String;)Lcom/android/settings/widget/CommonNotification$Builder;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/CommonNotification$Builder;->cN:Lcom/android/settings/widget/CommonNotification;

    invoke-static {v0, p1}, Lcom/android/settings/widget/CommonNotification;->-$$Nest$fputactionText(Lcom/android/settings/widget/CommonNotification;Ljava/lang/String;)V

    return-object p0
.end method

.method public setChannel(Ljava/lang/String;Ljava/lang/String;)Lcom/android/settings/widget/CommonNotification$Builder;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/CommonNotification$Builder;->cN:Lcom/android/settings/widget/CommonNotification;

    invoke-static {v0, p1}, Lcom/android/settings/widget/CommonNotification;->-$$Nest$fputchannelId(Lcom/android/settings/widget/CommonNotification;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/settings/widget/CommonNotification$Builder;->cN:Lcom/android/settings/widget/CommonNotification;

    invoke-static {p1, p2}, Lcom/android/settings/widget/CommonNotification;->-$$Nest$fputchannelName(Lcom/android/settings/widget/CommonNotification;Ljava/lang/String;)V

    return-object p0
.end method

.method public setContentText(Ljava/lang/CharSequence;)Lcom/android/settings/widget/CommonNotification$Builder;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/CommonNotification$Builder;->cN:Lcom/android/settings/widget/CommonNotification;

    invoke-static {v0, p1}, Lcom/android/settings/widget/CommonNotification;->-$$Nest$fputcontentText(Lcom/android/settings/widget/CommonNotification;Ljava/lang/CharSequence;)V

    return-object p0
.end method

.method public setContentTitle(Ljava/lang/CharSequence;)Lcom/android/settings/widget/CommonNotification$Builder;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/CommonNotification$Builder;->cN:Lcom/android/settings/widget/CommonNotification;

    invoke-static {v0, p1}, Lcom/android/settings/widget/CommonNotification;->-$$Nest$fputcontentTitle(Lcom/android/settings/widget/CommonNotification;Ljava/lang/CharSequence;)V

    return-object p0
.end method

.method public setEnableFloat(Z)Lcom/android/settings/widget/CommonNotification$Builder;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/CommonNotification$Builder;->cN:Lcom/android/settings/widget/CommonNotification;

    invoke-static {v0, p1}, Lcom/android/settings/widget/CommonNotification;->-$$Nest$fputenableFloat(Lcom/android/settings/widget/CommonNotification;Z)V

    return-object p0
.end method

.method public setEnableKeyguard(Z)Lcom/android/settings/widget/CommonNotification$Builder;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/CommonNotification$Builder;->cN:Lcom/android/settings/widget/CommonNotification;

    invoke-static {v0, p1}, Lcom/android/settings/widget/CommonNotification;->-$$Nest$fputenableKeyguard(Lcom/android/settings/widget/CommonNotification;Z)V

    return-object p0
.end method

.method public setImportance(I)Lcom/android/settings/widget/CommonNotification$Builder;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/CommonNotification$Builder;->cN:Lcom/android/settings/widget/CommonNotification;

    invoke-static {v0, p1}, Lcom/android/settings/widget/CommonNotification;->-$$Nest$fputimportance(Lcom/android/settings/widget/CommonNotification;I)V

    return-object p0
.end method

.method public setNotificationIcon(I)Lcom/android/settings/widget/CommonNotification$Builder;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/CommonNotification$Builder;->cN:Lcom/android/settings/widget/CommonNotification;

    invoke-static {v0, p1}, Lcom/android/settings/widget/CommonNotification;->-$$Nest$fputnotificationIcon(Lcom/android/settings/widget/CommonNotification;I)V

    return-object p0
.end method

.method public setNotifyId(I)Lcom/android/settings/widget/CommonNotification$Builder;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/CommonNotification$Builder;->cN:Lcom/android/settings/widget/CommonNotification;

    invoke-static {v0, p1}, Lcom/android/settings/widget/CommonNotification;->-$$Nest$fputnotifyId(Lcom/android/settings/widget/CommonNotification;I)V

    return-object p0
.end method

.method public setResident(Z)Lcom/android/settings/widget/CommonNotification$Builder;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/CommonNotification$Builder;->cN:Lcom/android/settings/widget/CommonNotification;

    invoke-static {v0, p1}, Lcom/android/settings/widget/CommonNotification;->-$$Nest$fputisResident(Lcom/android/settings/widget/CommonNotification;Z)V

    return-object p0
.end method

.method public setResultIntent(Landroid/content/Intent;I)Lcom/android/settings/widget/CommonNotification$Builder;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/CommonNotification$Builder;->cN:Lcom/android/settings/widget/CommonNotification;

    invoke-static {v0, p1}, Lcom/android/settings/widget/CommonNotification;->-$$Nest$fputresultIntent(Lcom/android/settings/widget/CommonNotification;Landroid/content/Intent;)V

    iget-object p1, p0, Lcom/android/settings/widget/CommonNotification$Builder;->cN:Lcom/android/settings/widget/CommonNotification;

    invoke-static {p1, p2}, Lcom/android/settings/widget/CommonNotification;->-$$Nest$fputrequestCode(Lcom/android/settings/widget/CommonNotification;I)V

    return-object p0
.end method

.method public setSmallIcon(I)Lcom/android/settings/widget/CommonNotification$Builder;
    .locals 1

    iget-object v0, p0, Lcom/android/settings/widget/CommonNotification$Builder;->cN:Lcom/android/settings/widget/CommonNotification;

    invoke-static {v0, p1}, Lcom/android/settings/widget/CommonNotification;->-$$Nest$fputsmallIcon(Lcom/android/settings/widget/CommonNotification;I)V

    return-object p0
.end method
