.class public Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;
.super Landroid/view/ViewGroup;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;
    }
.end annotation


# instance fields
.field private mBaselineAligned:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field

.field private mBaselineAlignedChildIndex:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field

.field private mBaselineChildTop:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "measurement"
    .end annotation
.end field

.field private mDivider:Landroid/graphics/drawable/Drawable;

.field private mDividerHeight:I

.field private mDividerPadding:I

.field private mDividerWidth:I

.field private mGravity:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "measurement"
        flagMapping = {
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = -0x1
                mask = -0x1
                name = "NONE"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x0
                mask = 0x0
                name = "NONE"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x30
                mask = 0x30
                name = "TOP"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x50
                mask = 0x50
                name = "BOTTOM"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x3
                mask = 0x3
                name = "LEFT"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x5
                mask = 0x5
                name = "RIGHT"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x800003
                mask = 0x800003
                name = "START"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x800005
                mask = 0x800005
                name = "END"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x10
                mask = 0x10
                name = "CENTER_VERTICAL"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x70
                mask = 0x70
                name = "FILL_VERTICAL"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x1
                mask = 0x1
                name = "CENTER_HORIZONTAL"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x7
                mask = 0x7
                name = "FILL_HORIZONTAL"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x11
                mask = 0x11
                name = "CENTER"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x77
                mask = 0x77
                name = "FILL"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$FlagToString;
                equals = 0x800000
                mask = 0x800000
                name = "RELATIVE"
            .end subannotation
        }
        formatToHexString = true
    .end annotation
.end field

.field private mLayoutDirection:I

.field private mMaxAscent:[I

.field private mMaxDescent:[I

.field private mOrientation:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "measurement"
    .end annotation
.end field

.field private mShowDividers:I

.field private mTotalLength:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "measurement"
    .end annotation
.end field

.field private mUseLargestChild:Z
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field

.field private mWeightSum:F
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 4

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mBaselineAligned:Z

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mBaselineAlignedChildIndex:I

    const/4 v2, 0x0

    iput v2, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mBaselineChildTop:I

    const v3, 0x800033

    iput v3, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mGravity:I

    iput v1, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mLayoutDirection:I

    sget-object v3, Lcom/android/internal/R$styleable;->LinearLayout:[I

    invoke-virtual {p1, p2, v3, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    if-ltz p2, :cond_0

    invoke-virtual {p0, p2}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->setOrientation(I)V

    :cond_0
    invoke-virtual {p1, v2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    if-ltz p2, :cond_1

    invoke-virtual {p0, p2}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->setGravity(I)V

    :cond_1
    const/4 p2, 0x2

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    if-nez p2, :cond_2

    invoke-virtual {p0, p2}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->setBaselineAligned(Z)V

    :cond_2
    const/4 p2, 0x4

    const/high16 p3, -0x40800000    # -1.0f

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result p2

    iput p2, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mWeightSum:F

    const/4 p2, 0x3

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    iput p2, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mBaselineAlignedChildIndex:I

    const/4 p2, 0x6

    invoke-virtual {p1, p2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mUseLargestChild:Z

    const/4 p2, 0x5

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 p2, 0x7

    invoke-virtual {p1, p2, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    iput p2, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mShowDividers:I

    const/16 p2, 0x8

    invoke-virtual {p1, p2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDividerPadding:I

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private forceUniformWidth(II)V
    .locals 10

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_1

    invoke-virtual {p0, v1}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v4, 0x8

    if-eq v2, v4, :cond_0

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;

    iget v2, v8, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_0

    iget v9, v8, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iput v2, v8, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v2, p0

    move v4, v0

    move v6, p2

    invoke-virtual/range {v2 .. v7}, Landroid/view/ViewGroup;->measureChildWithMargins(Landroid/view/View;IIII)V

    iput v9, v8, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private setChildFrame(Landroid/view/View;IIII)V
    .locals 0

    add-int/2addr p4, p2

    add-int/2addr p5, p3

    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/view/View;->layout(IIII)V

    return-void
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 0

    instance-of p0, p1, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;

    return p0
.end method

.method drawDividersHorizontal(Landroid/graphics/Canvas;)V
    .locals 6

    goto/32 :goto_13

    nop

    :goto_0
    add-int/2addr v3, v4

    goto/32 :goto_2f

    nop

    :goto_1
    goto :goto_9

    :goto_2
    goto/32 :goto_3f

    nop

    :goto_3
    if-nez v1, :cond_0

    goto/32 :goto_30

    :cond_0
    goto/32 :goto_38

    nop

    :goto_4
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    goto/32 :goto_2c

    nop

    :goto_5
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_24

    nop

    :goto_6
    goto :goto_9

    :goto_7
    goto/32 :goto_39

    nop

    :goto_8
    add-int/2addr v0, v1

    :goto_9
    goto/32 :goto_3b

    nop

    :goto_a
    invoke-virtual {p0, v2}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->hasDividerBeforeChildAt(I)Z

    move-result v4

    goto/32 :goto_16

    nop

    :goto_b
    iget v1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto/32 :goto_8

    nop

    :goto_c
    sub-int/2addr v3, v4

    goto/32 :goto_2e

    nop

    :goto_d
    check-cast v4, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;

    goto/32 :goto_3

    nop

    :goto_e
    iget v4, v4, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto/32 :goto_0

    nop

    :goto_f
    const/4 v2, 0x0

    :goto_10
    goto/32 :goto_15

    nop

    :goto_11
    iget v4, v4, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto/32 :goto_c

    nop

    :goto_12
    if-nez v3, :cond_1

    goto/32 :goto_36

    :cond_1
    goto/32 :goto_1f

    nop

    :goto_13
    invoke-virtual {p0}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getVirtualChildCount()I

    move-result v0

    goto/32 :goto_28

    nop

    :goto_14
    if-nez v1, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_32

    nop

    :goto_15
    if-lt v2, v0, :cond_3

    goto/32 :goto_25

    :cond_3
    goto/32 :goto_31

    nop

    :goto_16
    if-nez v4, :cond_4

    goto/32 :goto_36

    :cond_4
    goto/32 :goto_23

    nop

    :goto_17
    iget v1, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDividerWidth:I

    :goto_18
    goto/32 :goto_29

    nop

    :goto_19
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    goto/32 :goto_11

    nop

    :goto_1a
    invoke-virtual {p0, v0}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    move-result-object v0

    goto/32 :goto_3e

    nop

    :goto_1b
    invoke-virtual {p0, v0}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->hasDividerBeforeChildAt(I)Z

    move-result v2

    goto/32 :goto_3d

    nop

    :goto_1c
    sub-int/2addr v0, v1

    goto/32 :goto_17

    nop

    :goto_1d
    const/16 v5, 0x8

    goto/32 :goto_3a

    nop

    :goto_1e
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v1

    goto/32 :goto_26

    nop

    :goto_1f
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    goto/32 :goto_1d

    nop

    :goto_20
    if-nez v1, :cond_5

    goto/32 :goto_7

    :cond_5
    goto/32 :goto_33

    nop

    :goto_21
    sub-int/2addr v3, v4

    :goto_22
    goto/32 :goto_35

    nop

    :goto_23
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    goto/32 :goto_d

    nop

    :goto_24
    goto/16 :goto_10

    :goto_25
    goto/32 :goto_1b

    nop

    :goto_26
    sub-int/2addr v0, v1

    goto/32 :goto_34

    nop

    :goto_27
    return-void

    :goto_28
    invoke-virtual {p0}, Landroid/view/ViewGroup;->isLayoutRtl()Z

    move-result v1

    goto/32 :goto_f

    nop

    :goto_29
    sub-int/2addr v0, v1

    goto/32 :goto_1

    nop

    :goto_2a
    goto :goto_18

    :goto_2b
    goto/32 :goto_4

    nop

    :goto_2c
    check-cast v2, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;

    goto/32 :goto_14

    nop

    :goto_2d
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_1a

    nop

    :goto_2e
    iget v4, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDividerWidth:I

    goto/32 :goto_21

    nop

    :goto_2f
    goto :goto_22

    :goto_30
    goto/32 :goto_19

    nop

    :goto_31
    invoke-virtual {p0, v2}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    move-result-object v3

    goto/32 :goto_12

    nop

    :goto_32
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    goto/32 :goto_37

    nop

    :goto_33
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v0

    goto/32 :goto_6

    nop

    :goto_34
    iget v1, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDividerWidth:I

    goto/32 :goto_2a

    nop

    :goto_35
    invoke-virtual {p0, p1, v3}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->drawVerticalDivider(Landroid/graphics/Canvas;I)V

    :goto_36
    goto/32 :goto_5

    nop

    :goto_37
    iget v1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto/32 :goto_1c

    nop

    :goto_38
    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v3

    goto/32 :goto_e

    nop

    :goto_39
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    goto/32 :goto_1e

    nop

    :goto_3a
    if-ne v4, v5, :cond_6

    goto/32 :goto_36

    :cond_6
    goto/32 :goto_a

    nop

    :goto_3b
    invoke-virtual {p0, p1, v0}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->drawVerticalDivider(Landroid/graphics/Canvas;I)V

    :goto_3c
    goto/32 :goto_27

    nop

    :goto_3d
    if-nez v2, :cond_7

    goto/32 :goto_3c

    :cond_7
    goto/32 :goto_2d

    nop

    :goto_3e
    if-eqz v0, :cond_8

    goto/32 :goto_2b

    :cond_8
    goto/32 :goto_20

    nop

    :goto_3f
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    goto/32 :goto_b

    nop
.end method

.method drawDividersVertical(Landroid/graphics/Canvas;)V
    .locals 5

    goto/32 :goto_19

    nop

    :goto_0
    if-lt v1, v0, :cond_0

    goto/32 :goto_18

    :cond_0
    goto/32 :goto_6

    nop

    :goto_1
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    goto/32 :goto_d

    nop

    :goto_2
    iget v3, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDividerHeight:I

    goto/32 :goto_26

    nop

    :goto_3
    const/16 v4, 0x8

    goto/32 :goto_a

    nop

    :goto_4
    if-nez v1, :cond_1

    goto/32 :goto_2a

    :cond_1
    goto/32 :goto_13

    nop

    :goto_5
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    goto/32 :goto_16

    nop

    :goto_6
    invoke-virtual {p0, v1}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    move-result-object v2

    goto/32 :goto_1f

    nop

    :goto_7
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    goto/32 :goto_12

    nop

    :goto_8
    invoke-virtual {p0, v0}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    move-result-object v0

    goto/32 :goto_2b

    nop

    :goto_9
    invoke-virtual {p0, v1}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->hasDividerBeforeChildAt(I)Z

    move-result v3

    goto/32 :goto_c

    nop

    :goto_a
    if-ne v3, v4, :cond_2

    goto/32 :goto_10

    :cond_2
    goto/32 :goto_9

    nop

    :goto_b
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v1

    goto/32 :goto_24

    nop

    :goto_c
    if-nez v3, :cond_3

    goto/32 :goto_10

    :cond_3
    goto/32 :goto_5

    nop

    :goto_d
    check-cast v1, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;

    goto/32 :goto_23

    nop

    :goto_e
    return-void

    :goto_f
    invoke-virtual {p0, p1, v2}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->drawHorizontalDivider(Landroid/graphics/Canvas;I)V

    :goto_10
    goto/32 :goto_11

    nop

    :goto_11
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_17

    nop

    :goto_12
    iget v3, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto/32 :goto_1c

    nop

    :goto_13
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_8

    nop

    :goto_14
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    goto/32 :goto_b

    nop

    :goto_15
    invoke-virtual {p0, v0}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->hasDividerBeforeChildAt(I)Z

    move-result v1

    goto/32 :goto_4

    nop

    :goto_16
    check-cast v3, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;

    goto/32 :goto_7

    nop

    :goto_17
    goto :goto_28

    :goto_18
    goto/32 :goto_15

    nop

    :goto_19
    invoke-virtual {p0}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getVirtualChildCount()I

    move-result v0

    goto/32 :goto_27

    nop

    :goto_1a
    goto :goto_22

    :goto_1b
    goto/32 :goto_1

    nop

    :goto_1c
    sub-int/2addr v2, v3

    goto/32 :goto_2

    nop

    :goto_1d
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    goto/32 :goto_3

    nop

    :goto_1e
    sub-int/2addr v0, v1

    goto/32 :goto_1a

    nop

    :goto_1f
    if-nez v2, :cond_4

    goto/32 :goto_10

    :cond_4
    goto/32 :goto_1d

    nop

    :goto_20
    iget v1, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDividerHeight:I

    goto/32 :goto_1e

    nop

    :goto_21
    add-int/2addr v0, v1

    :goto_22
    goto/32 :goto_29

    nop

    :goto_23
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    goto/32 :goto_25

    nop

    :goto_24
    sub-int/2addr v0, v1

    goto/32 :goto_20

    nop

    :goto_25
    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto/32 :goto_21

    nop

    :goto_26
    sub-int/2addr v2, v3

    goto/32 :goto_f

    nop

    :goto_27
    const/4 v1, 0x0

    :goto_28
    goto/32 :goto_0

    nop

    :goto_29
    invoke-virtual {p0, p1, v0}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->drawHorizontalDivider(Landroid/graphics/Canvas;I)V

    :goto_2a
    goto/32 :goto_e

    nop

    :goto_2b
    if-eqz v0, :cond_5

    goto/32 :goto_1b

    :cond_5
    goto/32 :goto_14

    nop
.end method

.method drawHorizontalDivider(Landroid/graphics/Canvas;I)V
    .locals 4

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {v0, v1, p2, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto/32 :goto_d

    nop

    :goto_1
    iget v3, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDividerPadding:I

    goto/32 :goto_b

    nop

    :goto_2
    add-int/2addr v3, p2

    goto/32 :goto_0

    nop

    :goto_3
    iget-object v0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDivider:Landroid/graphics/drawable/Drawable;

    goto/32 :goto_e

    nop

    :goto_4
    add-int/2addr v1, v2

    goto/32 :goto_5

    nop

    :goto_5
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    goto/32 :goto_c

    nop

    :goto_6
    sub-int/2addr v2, v3

    goto/32 :goto_1

    nop

    :goto_7
    return-void

    :goto_8
    iget v3, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDividerHeight:I

    goto/32 :goto_2

    nop

    :goto_9
    iget v2, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDividerPadding:I

    goto/32 :goto_4

    nop

    :goto_a
    invoke-virtual {p0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/32 :goto_7

    nop

    :goto_b
    sub-int/2addr v2, v3

    goto/32 :goto_8

    nop

    :goto_c
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v3

    goto/32 :goto_6

    nop

    :goto_d
    iget-object p0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDivider:Landroid/graphics/drawable/Drawable;

    goto/32 :goto_a

    nop

    :goto_e
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v1

    goto/32 :goto_9

    nop
.end method

.method drawVerticalDivider(Landroid/graphics/Canvas;I)V
    .locals 5

    goto/32 :goto_a

    nop

    :goto_0
    invoke-virtual {p0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/32 :goto_d

    nop

    :goto_1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    goto/32 :goto_8

    nop

    :goto_2
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v1

    goto/32 :goto_6

    nop

    :goto_3
    add-int/2addr v2, p2

    goto/32 :goto_1

    nop

    :goto_4
    sub-int/2addr v3, v4

    goto/32 :goto_e

    nop

    :goto_5
    add-int/2addr v1, v2

    goto/32 :goto_7

    nop

    :goto_6
    iget v2, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDividerPadding:I

    goto/32 :goto_5

    nop

    :goto_7
    iget v2, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDividerWidth:I

    goto/32 :goto_3

    nop

    :goto_8
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v4

    goto/32 :goto_9

    nop

    :goto_9
    sub-int/2addr v3, v4

    goto/32 :goto_c

    nop

    :goto_a
    iget-object v0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDivider:Landroid/graphics/drawable/Drawable;

    goto/32 :goto_2

    nop

    :goto_b
    iget-object p0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDivider:Landroid/graphics/drawable/Drawable;

    goto/32 :goto_0

    nop

    :goto_c
    iget v4, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDividerPadding:I

    goto/32 :goto_4

    nop

    :goto_d
    return-void

    :goto_e
    invoke-virtual {v0, p2, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto/32 :goto_b

    nop
.end method

.method protected encodeProperties(Landroid/view/ViewHierarchyEncoder;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->encodeProperties(Landroid/view/ViewHierarchyEncoder;)V

    iget-boolean v0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mBaselineAligned:Z

    const-string v1, "layout:baselineAligned"

    invoke-virtual {p1, v1, v0}, Landroid/view/ViewHierarchyEncoder;->addProperty(Ljava/lang/String;Z)V

    iget v0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mBaselineAlignedChildIndex:I

    const-string v1, "layout:baselineAlignedChildIndex"

    invoke-virtual {p1, v1, v0}, Landroid/view/ViewHierarchyEncoder;->addProperty(Ljava/lang/String;I)V

    iget v0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mBaselineChildTop:I

    const-string/jumbo v1, "measurement:baselineChildTop"

    invoke-virtual {p1, v1, v0}, Landroid/view/ViewHierarchyEncoder;->addProperty(Ljava/lang/String;I)V

    iget v0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mOrientation:I

    const-string/jumbo v1, "measurement:orientation"

    invoke-virtual {p1, v1, v0}, Landroid/view/ViewHierarchyEncoder;->addProperty(Ljava/lang/String;I)V

    iget v0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mGravity:I

    const-string/jumbo v1, "measurement:gravity"

    invoke-virtual {p1, v1, v0}, Landroid/view/ViewHierarchyEncoder;->addProperty(Ljava/lang/String;I)V

    iget v0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    const-string/jumbo v1, "measurement:totalLength"

    invoke-virtual {p1, v1, v0}, Landroid/view/ViewHierarchyEncoder;->addProperty(Ljava/lang/String;I)V

    iget v0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    const-string v1, "layout:totalLength"

    invoke-virtual {p1, v1, v0}, Landroid/view/ViewHierarchyEncoder;->addProperty(Ljava/lang/String;I)V

    iget-boolean p0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mUseLargestChild:Z

    const-string v0, "layout:useLargestChild"

    invoke-virtual {p1, v0, p0}, Landroid/view/ViewHierarchyEncoder;->addProperty(Ljava/lang/String;Z)V

    return-void
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->generateDefaultLayoutParams()Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;

    move-result-object p0

    return-object p0
.end method

.method protected generateDefaultLayoutParams()Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;
    .locals 2

    iget p0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mOrientation:I

    const/4 v0, -0x2

    if-nez p0, :cond_0

    new-instance p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;

    invoke-direct {p0, v0, v0}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;-><init>(II)V

    return-object p0

    :cond_0
    const/4 v1, 0x1

    if-ne p0, v1, :cond_1

    new-instance p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {p0, v1, v0}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;-><init>(II)V

    return-object p0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->generateLayoutParams(Landroid/util/AttributeSet;)Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;

    move-result-object p0

    return-object p0
.end method

.method protected bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;

    move-result-object p0

    return-object p0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;
    .locals 1

    new-instance v0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v0, p0, p1}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;
    .locals 0

    new-instance p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;

    invoke-direct {p0, p1}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object p0
.end method

.method public getAccessibilityClassName()Ljava/lang/CharSequence;
    .locals 0

    const-class p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public getBaseline()I
    .locals 5

    iget v0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mBaselineAlignedChildIndex:I

    if-gez v0, :cond_0

    invoke-super {p0}, Landroid/view/ViewGroup;->getBaseline()I

    move-result p0

    return p0

    :cond_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    iget v1, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mBaselineAlignedChildIndex:I

    if-le v0, v1, :cond_6

    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBaseline()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    iget p0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mBaselineAlignedChildIndex:I

    if-nez p0, :cond_1

    return v2

    :cond_1
    new-instance p0, Ljava/lang/RuntimeException;

    const-string v0, "mBaselineAlignedChildIndex of LinearLayout points to a View that doesn\'t know how to get its baseline."

    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_2
    iget v2, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mBaselineChildTop:I

    iget v3, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mOrientation:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_5

    iget v3, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mGravity:I

    and-int/lit8 v3, v3, 0x70

    const/16 v4, 0x30

    if-eq v3, v4, :cond_5

    const/16 v4, 0x10

    if-eq v3, v4, :cond_4

    const/16 v4, 0x50

    if-eq v3, v4, :cond_3

    goto :goto_0

    :cond_3
    iget v2, p0, Landroid/view/ViewGroup;->mBottom:I

    iget v3, p0, Landroid/view/ViewGroup;->mTop:I

    sub-int/2addr v2, v3

    iget v3, p0, Landroid/view/ViewGroup;->mPaddingBottom:I

    sub-int/2addr v2, v3

    iget p0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    sub-int/2addr v2, p0

    goto :goto_0

    :cond_4
    iget v3, p0, Landroid/view/ViewGroup;->mBottom:I

    iget v4, p0, Landroid/view/ViewGroup;->mTop:I

    sub-int/2addr v3, v4

    iget v4, p0, Landroid/view/ViewGroup;->mPaddingTop:I

    sub-int/2addr v3, v4

    iget v4, p0, Landroid/view/ViewGroup;->mPaddingBottom:I

    sub-int/2addr v3, v4

    iget p0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    sub-int/2addr v3, p0

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    :cond_5
    :goto_0
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p0

    check-cast p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;

    iget p0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v2, p0

    add-int/2addr v2, v1

    return v2

    :cond_6
    new-instance p0, Ljava/lang/RuntimeException;

    const-string v0, "mBaselineAlignedChildIndex of LinearLayout set to an index that is out of bounds."

    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public getBaselineAlignedChildIndex()I
    .locals 0

    iget p0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mBaselineAlignedChildIndex:I

    return p0
.end method

.method getChildrenSkipCount(Landroid/view/View;I)I
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    const/4 p0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    return p0
.end method

.method public getDividerDrawable()Landroid/graphics/drawable/Drawable;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDivider:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method public getDividerPadding()I
    .locals 0

    iget p0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDividerPadding:I

    return p0
.end method

.method public getDividerWidth()I
    .locals 0

    iget p0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDividerWidth:I

    return p0
.end method

.method getLocationOffset(Landroid/view/View;)I
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return p0

    :goto_1
    const/4 p0, 0x0

    goto/32 :goto_0

    nop
.end method

.method getNextLocationOffset(Landroid/view/View;)I
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    const/4 p0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    return p0
.end method

.method public getOrientation()I
    .locals 0

    iget p0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mOrientation:I

    return p0
.end method

.method public getShowDividers()I
    .locals 0

    iget p0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mShowDividers:I

    return p0
.end method

.method getVirtualChildAt(I)Landroid/view/View;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p0

    goto/32 :goto_1

    nop

    :goto_1
    return-object p0
.end method

.method getVirtualChildCount()I
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p0

    goto/32 :goto_1

    nop

    :goto_1
    return p0
.end method

.method public getWeightSum()F
    .locals 0

    iget p0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mWeightSum:F

    return p0
.end method

.method protected hasDividerBeforeChildAt(I)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-nez p1, :cond_1

    iget p0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mShowDividers:I

    and-int/2addr p0, v1

    if-eqz p0, :cond_0

    move v0, v1

    :cond_0
    return v0

    :cond_1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ne p1, v2, :cond_3

    iget p0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mShowDividers:I

    and-int/lit8 p0, p0, 0x4

    if-eqz p0, :cond_2

    move v0, v1

    :cond_2
    return v0

    :cond_3
    iget v2, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mShowDividers:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_5

    sub-int/2addr p1, v1

    :goto_0
    if-ltz p1, :cond_5

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_1

    :cond_4
    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    :cond_5
    :goto_1
    return v0
.end method

.method layoutHorizontal(IIII)V
    .locals 24

    goto/32 :goto_35

    nop

    :goto_0
    move-object/from16 v0, p1

    goto/32 :goto_38

    nop

    :goto_1
    move v4, v15

    goto/32 :goto_7a

    nop

    :goto_2
    invoke-static {v2, v1}, Landroid/view/Gravity;->getAbsoluteGravity(II)I

    move-result v1

    goto/32 :goto_2f

    nop

    :goto_3
    sub-int v22, v22, v10

    goto/32 :goto_4b

    nop

    :goto_4
    move-object v7, v4

    goto/32 :goto_1

    nop

    :goto_5
    goto/16 :goto_24

    :goto_6
    goto/32 :goto_1c

    nop

    :goto_7
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    goto/32 :goto_55

    nop

    :goto_8
    iget v1, v6, Landroid/view/ViewGroup;->mPaddingLeft:I

    goto/32 :goto_85

    nop

    :goto_9
    sub-int v1, p4, p2

    goto/32 :goto_8b

    nop

    :goto_a
    add-int v2, v16, v0

    goto/32 :goto_1b

    nop

    :goto_b
    goto/16 :goto_42

    :goto_c
    goto/32 :goto_84

    nop

    :goto_d
    mul-int v0, v17, v3

    goto/32 :goto_a

    nop

    :goto_e
    const/16 v11, 0x50

    goto/32 :goto_48

    nop

    :goto_f
    add-int/2addr v3, v7

    goto/32 :goto_8f

    nop

    :goto_10
    sub-int v1, v1, p1

    goto/32 :goto_28

    nop

    :goto_11
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v5

    goto/32 :goto_14

    nop

    :goto_12
    if-lt v3, v10, :cond_0

    goto/32 :goto_74

    :cond_0
    goto/32 :goto_d

    nop

    :goto_13
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_6a

    nop

    :goto_14
    const/16 v15, 0x8

    goto/32 :goto_54

    nop

    :goto_15
    const/4 v2, 0x0

    goto/32 :goto_20

    nop

    :goto_16
    iget v11, v4, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto/32 :goto_25

    nop

    :goto_17
    add-int/2addr v3, v7

    goto/32 :goto_79

    nop

    :goto_18
    iget-object v14, v6, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mMaxDescent:[I

    goto/32 :goto_6c

    nop

    :goto_19
    and-int/2addr v2, v1

    goto/32 :goto_a7

    nop

    :goto_1a
    if-ne v3, v11, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_7b

    nop

    :goto_1b
    invoke-virtual {v6, v2}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    move-result-object v0

    goto/32 :goto_a4

    nop

    :goto_1c
    const/4 v11, -0x1

    goto/32 :goto_2e

    nop

    :goto_1d
    move/from16 v5, v21

    goto/32 :goto_b7

    nop

    :goto_1e
    goto/16 :goto_ad

    :goto_1f
    goto/32 :goto_50

    nop

    :goto_20
    if-nez v0, :cond_2

    goto/32 :goto_1f

    :cond_2
    goto/32 :goto_69

    nop

    :goto_21
    if-ne v10, v11, :cond_3

    goto/32 :goto_42

    :cond_3
    goto/32 :goto_58

    nop

    :goto_22
    move/from16 v20, v11

    goto/32 :goto_75

    nop

    :goto_23
    sub-int/2addr v3, v10

    :goto_24
    goto/32 :goto_40

    nop

    :goto_25
    sub-int/2addr v3, v11

    goto/32 :goto_3d

    nop

    :goto_26
    move-object/from16 p1, v0

    goto/32 :goto_7c

    nop

    :goto_27
    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getVirtualChildCount()I

    move-result v10

    goto/32 :goto_94

    nop

    :goto_28
    iget v2, v6, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_88

    nop

    :goto_29
    const/16 v23, -0x1

    :goto_2a
    goto/32 :goto_13

    nop

    :goto_2b
    move/from16 v22, v7

    goto/32 :goto_8c

    nop

    :goto_2c
    sub-int/2addr v1, v7

    goto/32 :goto_76

    nop

    :goto_2d
    move-object/from16 v4, v18

    goto/32 :goto_a9

    nop

    :goto_2e
    const/16 v21, 0x1

    goto/32 :goto_5f

    nop

    :goto_2f
    const/4 v15, 0x2

    goto/32 :goto_95

    nop

    :goto_30
    goto/16 :goto_b0

    :goto_31
    goto/32 :goto_4c

    nop

    :goto_32
    const/4 v10, -0x1

    goto/32 :goto_a1

    nop

    :goto_33
    move v3, v11

    :goto_34
    goto/32 :goto_5b

    nop

    :goto_35
    move-object/from16 v6, p0

    goto/32 :goto_3c

    nop

    :goto_36
    iget v10, v4, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto/32 :goto_a3

    nop

    :goto_37
    const/16 v17, -0x1

    goto/32 :goto_1e

    nop

    :goto_38
    invoke-virtual {v6, v0}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getNextLocationOffset(Landroid/view/View;)I

    move-result v1

    goto/32 :goto_b9

    nop

    :goto_39
    const/4 v11, -0x1

    goto/32 :goto_77

    nop

    :goto_3a
    iget v3, v6, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_4e

    nop

    :goto_3b
    invoke-virtual {v6, v0}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getLocationOffset(Landroid/view/View;)I

    move-result v1

    goto/32 :goto_b3

    nop

    :goto_3c
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->isLayoutRtl()Z

    move-result v0

    goto/32 :goto_7f

    nop

    :goto_3d
    const/4 v11, -0x1

    goto/32 :goto_21

    nop

    :goto_3e
    iget v10, v4, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto/32 :goto_23

    nop

    :goto_3f
    move/from16 v18, v3

    goto/32 :goto_60

    nop

    :goto_40
    invoke-virtual {v6, v2}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->hasDividerBeforeChildAt(I)Z

    move-result v10

    goto/32 :goto_70

    nop

    :goto_41
    goto/16 :goto_24

    :goto_42
    goto/32 :goto_93

    nop

    :goto_43
    move/from16 v21, v5

    goto/32 :goto_62

    nop

    :goto_44
    const/4 v10, 0x2

    goto/32 :goto_47

    nop

    :goto_45
    invoke-virtual {v6, v0, v11}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getChildrenSkipCount(Landroid/view/View;I)I

    move-result v0

    goto/32 :goto_b8

    nop

    :goto_46
    sub-int v22, v22, v21

    goto/32 :goto_9e

    nop

    :goto_47
    aget v22, v14, v10

    goto/32 :goto_46

    nop

    :goto_48
    if-ne v3, v11, :cond_4

    goto/32 :goto_78

    :cond_4
    goto/32 :goto_a8

    nop

    :goto_49
    invoke-virtual {v6, v2}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->measureNullChild(I)I

    move-result v0

    goto/32 :goto_6e

    nop

    :goto_4a
    sub-int v8, v1, v2

    goto/32 :goto_2c

    nop

    :goto_4b
    add-int v3, v3, v22

    goto/32 :goto_41

    nop

    :goto_4c
    move/from16 v19, v10

    :goto_4d
    goto/32 :goto_af

    nop

    :goto_4e
    sub-int/2addr v2, v3

    goto/32 :goto_87

    nop

    :goto_4f
    add-int/2addr v3, v10

    goto/32 :goto_3e

    nop

    :goto_50
    move/from16 v16, v2

    goto/32 :goto_ac

    nop

    :goto_51
    move/from16 v22, v7

    goto/32 :goto_91

    nop

    :goto_52
    const/4 v10, 0x2

    goto/32 :goto_61

    nop

    :goto_53
    move v11, v2

    goto/32 :goto_ab

    nop

    :goto_54
    if-ne v5, v15, :cond_5

    goto/32 :goto_65

    :cond_5
    goto/32 :goto_8d

    nop

    :goto_55
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v18

    goto/32 :goto_2d

    nop

    :goto_56
    move/from16 v19, v10

    goto/32 :goto_32

    nop

    :goto_57
    const/4 v2, 0x5

    goto/32 :goto_7e

    nop

    :goto_58
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v21

    goto/32 :goto_b6

    nop

    :goto_59
    add-int/2addr v10, v15

    goto/32 :goto_45

    nop

    :goto_5a
    if-ltz v3, :cond_6

    goto/32 :goto_34

    :cond_6
    goto/32 :goto_33

    nop

    :goto_5b
    and-int/lit8 v3, v3, 0x70

    goto/32 :goto_22

    nop

    :goto_5c
    const/16 v21, 0x1

    goto/32 :goto_89

    nop

    :goto_5d
    sub-int v3, v8, v5

    goto/32 :goto_16

    nop

    :goto_5e
    iget v3, v4, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    goto/32 :goto_56

    nop

    :goto_5f
    sub-int v3, v9, v5

    goto/32 :goto_52

    nop

    :goto_60
    if-nez v12, :cond_7

    goto/32 :goto_31

    :cond_7
    goto/32 :goto_5e

    nop

    :goto_61
    div-int/2addr v3, v10

    goto/32 :goto_17

    nop

    :goto_62
    move/from16 v22, v7

    goto/32 :goto_ae

    nop

    :goto_63
    iget v1, v6, Landroid/view/ViewGroup;->mPaddingLeft:I

    goto/32 :goto_80

    nop

    :goto_64
    goto/16 :goto_2a

    :goto_65
    goto/32 :goto_90

    nop

    :goto_66
    goto/16 :goto_97

    :goto_67
    goto/32 :goto_63

    nop

    :goto_68
    add-int/2addr v15, v0

    goto/32 :goto_0

    nop

    :goto_69
    add-int/lit8 v0, v10, -0x1

    goto/32 :goto_a0

    nop

    :goto_6a
    move/from16 v10, v19

    goto/32 :goto_6d

    nop

    :goto_6b
    iget v10, v6, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDividerWidth:I

    goto/32 :goto_a5

    nop

    :goto_6c
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getLayoutDirection()I

    move-result v1

    goto/32 :goto_2

    nop

    :goto_6d
    move/from16 v11, v20

    goto/32 :goto_1d

    nop

    :goto_6e
    add-int/2addr v1, v0

    goto/32 :goto_43

    nop

    :goto_6f
    const v2, 0x800007

    goto/32 :goto_19

    nop

    :goto_70
    if-nez v10, :cond_8

    goto/32 :goto_a6

    :cond_8
    goto/32 :goto_6b

    nop

    :goto_71
    goto/16 :goto_82

    :goto_72
    goto/32 :goto_11

    nop

    :goto_73
    goto/16 :goto_b2

    :goto_74
    goto/32 :goto_aa

    nop

    :goto_75
    const/16 v11, 0x10

    goto/32 :goto_1a

    nop

    :goto_76
    sub-int v9, v1, v2

    goto/32 :goto_27

    nop

    :goto_77
    goto/16 :goto_42

    :goto_78
    goto/32 :goto_5d

    nop

    :goto_79
    iget v10, v4, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto/32 :goto_4f

    nop

    :goto_7a
    invoke-direct/range {v0 .. v5}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->setChildFrame(Landroid/view/View;IIII)V

    goto/32 :goto_83

    nop

    :goto_7b
    const/16 v11, 0x30

    goto/32 :goto_9a

    nop

    :goto_7c
    move-object/from16 v0, p0

    goto/32 :goto_9f

    nop

    :goto_7d
    if-ne v1, v5, :cond_9

    goto/32 :goto_67

    :cond_9
    goto/32 :goto_57

    nop

    :goto_7e
    if-ne v1, v2, :cond_a

    goto/32 :goto_9d

    :cond_a
    goto/32 :goto_8a

    nop

    :goto_7f
    iget v7, v6, Landroid/view/ViewGroup;->mPaddingTop:I

    goto/32 :goto_9

    nop

    :goto_80
    sub-int v2, p3, p1

    goto/32 :goto_3a

    nop

    :goto_81
    const/16 v21, 0x1

    :goto_82
    goto/32 :goto_29

    nop

    :goto_83
    iget v0, v7, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto/32 :goto_68

    nop

    :goto_84
    const/4 v11, -0x1

    goto/32 :goto_98

    nop

    :goto_85
    add-int v1, v1, p3

    goto/32 :goto_10

    nop

    :goto_86
    move/from16 v20, v11

    goto/32 :goto_81

    nop

    :goto_87
    div-int/2addr v2, v15

    goto/32 :goto_96

    nop

    :goto_88
    sub-int/2addr v1, v2

    goto/32 :goto_66

    nop

    :goto_89
    aget v22, v13, v21

    goto/32 :goto_3

    nop

    :goto_8a
    iget v1, v6, Landroid/view/ViewGroup;->mPaddingLeft:I

    goto/32 :goto_9c

    nop

    :goto_8b
    iget v2, v6, Landroid/view/ViewGroup;->mPaddingBottom:I

    goto/32 :goto_4a

    nop

    :goto_8c
    const/16 v23, -0x1

    goto/32 :goto_4

    nop

    :goto_8d
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v15

    goto/32 :goto_7

    nop

    :goto_8e
    move/from16 v20, v11

    goto/32 :goto_71

    nop

    :goto_8f
    if-ne v10, v11, :cond_b

    goto/32 :goto_42

    :cond_b
    goto/32 :goto_5c

    nop

    :goto_90
    move/from16 v18, v3

    goto/32 :goto_51

    nop

    :goto_91
    move/from16 v19, v10

    goto/32 :goto_86

    nop

    :goto_92
    invoke-virtual {v0}, Landroid/view/View;->getBaseline()I

    move-result v10

    goto/32 :goto_30

    nop

    :goto_93
    const/16 v21, 0x1

    goto/32 :goto_5

    nop

    :goto_94
    iget v1, v6, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mGravity:I

    goto/32 :goto_6f

    nop

    :goto_95
    const/4 v5, 0x1

    goto/32 :goto_7d

    nop

    :goto_96
    add-int/2addr v1, v2

    :goto_97
    goto/32 :goto_15

    nop

    :goto_98
    iget v3, v4, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto/32 :goto_f

    nop

    :goto_99
    const/4 v15, 0x2

    goto/32 :goto_73

    nop

    :goto_9a
    if-ne v3, v11, :cond_c

    goto/32 :goto_c

    :cond_c
    goto/32 :goto_e

    nop

    :goto_9b
    iget-object v13, v6, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mMaxAscent:[I

    goto/32 :goto_18

    nop

    :goto_9c
    goto :goto_97

    :goto_9d
    goto/32 :goto_8

    nop

    :goto_9e
    sub-int v3, v3, v22

    goto/32 :goto_b

    nop

    :goto_9f
    move-object/from16 v1, p1

    goto/32 :goto_53

    nop

    :goto_a0
    move/from16 v16, v0

    goto/32 :goto_37

    nop

    :goto_a1
    if-ne v3, v10, :cond_d

    goto/32 :goto_4d

    :cond_d
    goto/32 :goto_92

    nop

    :goto_a2
    move v1, v10

    goto/32 :goto_64

    nop

    :goto_a3
    add-int/2addr v10, v1

    goto/32 :goto_3b

    nop

    :goto_a4
    if-eqz v0, :cond_e

    goto/32 :goto_72

    :cond_e
    goto/32 :goto_49

    nop

    :goto_a5
    add-int/2addr v1, v10

    :goto_a6
    goto/32 :goto_36

    nop

    :goto_a7
    and-int/lit8 v11, v1, 0x70

    goto/32 :goto_b5

    nop

    :goto_a8
    move v3, v7

    goto/32 :goto_39

    nop

    :goto_a9
    check-cast v4, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;

    goto/32 :goto_3f

    nop

    :goto_aa
    return-void

    :goto_ab
    move/from16 v2, v22

    goto/32 :goto_2b

    nop

    :goto_ac
    move/from16 v17, v5

    :goto_ad
    goto/32 :goto_b1

    nop

    :goto_ae
    move/from16 v19, v10

    goto/32 :goto_8e

    nop

    :goto_af
    const/4 v10, -0x1

    :goto_b0
    goto/32 :goto_b4

    nop

    :goto_b1
    move v3, v2

    :goto_b2
    goto/32 :goto_12

    nop

    :goto_b3
    add-int v22, v10, v1

    goto/32 :goto_26

    nop

    :goto_b4
    iget v3, v4, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;->gravity:I

    goto/32 :goto_5a

    nop

    :goto_b5
    iget-boolean v12, v6, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mBaselineAligned:Z

    goto/32 :goto_9b

    nop

    :goto_b6
    sub-int v21, v21, v10

    goto/32 :goto_44

    nop

    :goto_b7
    move/from16 v7, v22

    goto/32 :goto_99

    nop

    :goto_b8
    add-int v3, v18, v0

    goto/32 :goto_a2

    nop

    :goto_b9
    add-int/2addr v15, v1

    goto/32 :goto_59

    nop
.end method

.method layoutVertical(IIII)V
    .locals 17

    goto/32 :goto_1f

    nop

    :goto_0
    sub-int v1, v8, v4

    goto/32 :goto_66

    nop

    :goto_1
    if-ne v1, v2, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_44

    nop

    :goto_2
    iget v0, v14, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto/32 :goto_57

    nop

    :goto_3
    invoke-virtual {v6, v12}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->measureNullChild(I)I

    move-result v1

    goto/32 :goto_4

    nop

    :goto_4
    add-int/2addr v0, v1

    goto/32 :goto_30

    nop

    :goto_5
    invoke-virtual {v13}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    goto/32 :goto_1b

    nop

    :goto_6
    invoke-static {v1, v2}, Landroid/view/Gravity;->getAbsoluteGravity(II)I

    move-result v1

    goto/32 :goto_1e

    nop

    :goto_7
    const/4 v14, 0x1

    goto/32 :goto_39

    nop

    :goto_8
    iget v1, v6, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_43

    nop

    :goto_9
    goto/16 :goto_63

    :goto_a
    goto/32 :goto_62

    nop

    :goto_b
    and-int v11, v0, v2

    goto/32 :goto_17

    nop

    :goto_c
    add-int/2addr v0, v1

    :goto_d
    goto/32 :goto_56

    nop

    :goto_e
    add-int/2addr v1, v2

    goto/32 :goto_3e

    nop

    :goto_f
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    goto/32 :goto_5

    nop

    :goto_10
    const/16 v2, 0x8

    goto/32 :goto_1

    nop

    :goto_11
    const/16 v0, 0x50

    goto/32 :goto_2e

    nop

    :goto_12
    add-int/2addr v15, v0

    goto/32 :goto_2d

    nop

    :goto_13
    move-object/from16 v0, p0

    goto/32 :goto_32

    nop

    :goto_14
    iget v0, v6, Landroid/view/ViewGroup;->mPaddingTop:I

    goto/32 :goto_53

    nop

    :goto_15
    add-int/2addr v0, v1

    :goto_16
    goto/32 :goto_1c

    nop

    :goto_17
    const/16 v0, 0x10

    goto/32 :goto_64

    nop

    :goto_18
    invoke-virtual {v6, v12}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    move-result-object v13

    goto/32 :goto_7

    nop

    :goto_19
    const/4 v1, 0x1

    goto/32 :goto_9

    nop

    :goto_1a
    move-object v14, v5

    goto/32 :goto_49

    nop

    :goto_1b
    move-object v5, v1

    goto/32 :goto_5d

    nop

    :goto_1c
    iget v1, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto/32 :goto_6a

    nop

    :goto_1d
    add-int/2addr v1, v7

    goto/32 :goto_26

    nop

    :goto_1e
    and-int/lit8 v1, v1, 0x7

    goto/32 :goto_55

    nop

    :goto_1f
    move-object/from16 v6, p0

    goto/32 :goto_69

    nop

    :goto_20
    add-int/2addr v1, v7

    goto/32 :goto_50

    nop

    :goto_21
    sub-int/2addr v0, v7

    goto/32 :goto_5c

    nop

    :goto_22
    if-ltz v1, :cond_1

    goto/32 :goto_4e

    :cond_1
    goto/32 :goto_4d

    nop

    :goto_23
    sub-int/2addr v1, v2

    :goto_24
    goto/32 :goto_5f

    nop

    :goto_25
    iget v1, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto/32 :goto_20

    nop

    :goto_26
    iget v2, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto/32 :goto_e

    nop

    :goto_27
    iget v1, v5, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;->gravity:I

    goto/32 :goto_22

    nop

    :goto_28
    goto/16 :goto_37

    :goto_29
    goto/32 :goto_38

    nop

    :goto_2a
    if-ne v1, v2, :cond_2

    goto/32 :goto_51

    :cond_2
    goto/32 :goto_25

    nop

    :goto_2b
    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getVirtualChildCount()I

    move-result v10

    goto/32 :goto_54

    nop

    :goto_2c
    div-int/lit8 v1, v1, 0x2

    goto/32 :goto_c

    nop

    :goto_2d
    add-int v16, v16, v15

    goto/32 :goto_4a

    nop

    :goto_2e
    if-ne v1, v0, :cond_3

    goto/32 :goto_61

    :cond_3
    goto/32 :goto_42

    nop

    :goto_2f
    sub-int v1, v9, v4

    goto/32 :goto_52

    nop

    :goto_30
    goto/16 :goto_a

    :goto_31
    goto/32 :goto_4c

    nop

    :goto_32
    move-object v1, v13

    goto/32 :goto_1a

    nop

    :goto_33
    invoke-virtual {v6, v12}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->hasDividerBeforeChildAt(I)Z

    move-result v1

    goto/32 :goto_40

    nop

    :goto_34
    add-int/2addr v12, v1

    goto/32 :goto_28

    nop

    :goto_35
    iget v1, v6, Landroid/view/ViewGroup;->mPaddingRight:I

    goto/32 :goto_65

    nop

    :goto_36
    move v12, v1

    :goto_37
    goto/32 :goto_4b

    nop

    :goto_38
    return-void

    :goto_39
    if-eqz v13, :cond_4

    goto/32 :goto_31

    :cond_4
    goto/32 :goto_3

    nop

    :goto_3a
    sub-int/2addr v1, v2

    goto/32 :goto_2c

    nop

    :goto_3b
    iget v2, v6, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_3a

    nop

    :goto_3c
    goto/16 :goto_d

    :goto_3d
    goto/32 :goto_14

    nop

    :goto_3e
    iget v2, v5, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    :goto_3f
    goto/32 :goto_23

    nop

    :goto_40
    if-nez v1, :cond_5

    goto/32 :goto_16

    :cond_5
    goto/32 :goto_6e

    nop

    :goto_41
    add-int v0, v0, p4

    goto/32 :goto_58

    nop

    :goto_42
    iget v0, v6, Landroid/view/ViewGroup;->mPaddingTop:I

    goto/32 :goto_60

    nop

    :goto_43
    sub-int/2addr v0, v1

    goto/32 :goto_3c

    nop

    :goto_44
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    goto/32 :goto_f

    nop

    :goto_45
    sub-int v0, p3, p1

    goto/32 :goto_35

    nop

    :goto_46
    const v2, 0x800007

    goto/32 :goto_b

    nop

    :goto_47
    invoke-virtual {v6, v13}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getNextLocationOffset(Landroid/view/View;)I

    move-result v0

    goto/32 :goto_12

    nop

    :goto_48
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getLayoutDirection()I

    move-result v2

    goto/32 :goto_6

    nop

    :goto_49
    move v5, v15

    goto/32 :goto_5e

    nop

    :goto_4a
    invoke-virtual {v6, v13, v12}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getChildrenSkipCount(Landroid/view/View;I)I

    move-result v0

    goto/32 :goto_6c

    nop

    :goto_4b
    if-lt v12, v10, :cond_6

    goto/32 :goto_29

    :cond_6
    goto/32 :goto_18

    nop

    :goto_4c
    invoke-virtual {v13}, Landroid/view/View;->getVisibility()I

    move-result v1

    goto/32 :goto_10

    nop

    :goto_4d
    move v1, v11

    :goto_4e
    goto/32 :goto_48

    nop

    :goto_4f
    add-int v3, v16, v0

    goto/32 :goto_13

    nop

    :goto_50
    goto/16 :goto_24

    :goto_51
    goto/32 :goto_0

    nop

    :goto_52
    div-int/lit8 v1, v1, 0x2

    goto/32 :goto_1d

    nop

    :goto_53
    sub-int v1, p4, p2

    goto/32 :goto_3b

    nop

    :goto_54
    iget v0, v6, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mGravity:I

    goto/32 :goto_59

    nop

    :goto_55
    if-ne v1, v14, :cond_7

    goto/32 :goto_5b

    :cond_7
    goto/32 :goto_6d

    nop

    :goto_56
    const/4 v1, 0x0

    goto/32 :goto_36

    nop

    :goto_57
    add-int/2addr v15, v0

    goto/32 :goto_47

    nop

    :goto_58
    sub-int v0, v0, p2

    goto/32 :goto_8

    nop

    :goto_59
    and-int/lit8 v1, v0, 0x70

    goto/32 :goto_46

    nop

    :goto_5a
    goto/16 :goto_3f

    :goto_5b
    goto/32 :goto_2f

    nop

    :goto_5c
    sub-int v9, v0, v1

    goto/32 :goto_2b

    nop

    :goto_5d
    check-cast v5, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;

    goto/32 :goto_27

    nop

    :goto_5e
    invoke-direct/range {v0 .. v5}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->setChildFrame(Landroid/view/View;IIII)V

    goto/32 :goto_2

    nop

    :goto_5f
    move v2, v1

    goto/32 :goto_33

    nop

    :goto_60
    goto/16 :goto_d

    :goto_61
    goto/32 :goto_68

    nop

    :goto_62
    move v1, v14

    :goto_63
    goto/32 :goto_34

    nop

    :goto_64
    if-ne v1, v0, :cond_8

    goto/32 :goto_3d

    :cond_8
    goto/32 :goto_11

    nop

    :goto_65
    sub-int v8, v0, v1

    goto/32 :goto_21

    nop

    :goto_66
    iget v2, v5, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto/32 :goto_5a

    nop

    :goto_67
    invoke-virtual {v6, v13}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getLocationOffset(Landroid/view/View;)I

    move-result v0

    goto/32 :goto_4f

    nop

    :goto_68
    iget v0, v6, Landroid/view/ViewGroup;->mPaddingTop:I

    goto/32 :goto_41

    nop

    :goto_69
    iget v7, v6, Landroid/view/ViewGroup;->mPaddingLeft:I

    goto/32 :goto_45

    nop

    :goto_6a
    add-int v16, v0, v1

    goto/32 :goto_67

    nop

    :goto_6b
    move/from16 v0, v16

    goto/32 :goto_19

    nop

    :goto_6c
    add-int/2addr v12, v0

    goto/32 :goto_6b

    nop

    :goto_6d
    const/4 v2, 0x5

    goto/32 :goto_2a

    nop

    :goto_6e
    iget v1, v6, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDividerHeight:I

    goto/32 :goto_15

    nop
.end method

.method measureChildBeforeLayout(Landroid/view/View;IIIII)V
    .locals 6

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual/range {v0 .. v5}, Landroid/view/ViewGroup;->measureChildWithMargins(Landroid/view/View;IIII)V

    goto/32 :goto_7

    nop

    :goto_1
    move v4, p5

    goto/32 :goto_3

    nop

    :goto_2
    move-object v0, p0

    goto/32 :goto_6

    nop

    :goto_3
    move v5, p6

    goto/32 :goto_0

    nop

    :goto_4
    move v3, p4

    goto/32 :goto_1

    nop

    :goto_5
    move v2, p3

    goto/32 :goto_4

    nop

    :goto_6
    move-object v1, p1

    goto/32 :goto_5

    nop

    :goto_7
    return-void
.end method

.method measureHorizontal(II)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    new-instance p0, Ljava/lang/IllegalStateException;

    goto/32 :goto_1

    nop

    :goto_1
    const-string p1, "horizontal mode not supported."

    goto/32 :goto_2

    nop

    :goto_2
    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_3

    nop

    :goto_3
    throw p0
.end method

.method measureNullChild(I)I
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return p0

    :goto_1
    const/4 p0, 0x0

    goto/32 :goto_0

    nop
.end method

.method measureVertical(II)V
    .locals 32

    goto/32 :goto_33

    nop

    :goto_0
    if-gtz v0, :cond_0

    goto/32 :goto_1fd

    :cond_0
    goto/32 :goto_110

    nop

    :goto_1
    goto/16 :goto_11b

    :goto_2
    goto/32 :goto_11a

    nop

    :goto_3
    if-ne v12, v0, :cond_1

    goto/32 :goto_201

    :cond_1
    goto/32 :goto_209

    nop

    :goto_4
    move/from16 v0, v18

    :goto_5
    goto/32 :goto_f0

    nop

    :goto_6
    move/from16 v21, v3

    goto/32 :goto_94

    nop

    :goto_7
    if-lt v11, v14, :cond_2

    goto/32 :goto_32

    :cond_2
    goto/32 :goto_1d0

    nop

    :goto_8
    move-object/from16 v3, v22

    goto/32 :goto_165

    nop

    :goto_9
    const/4 v15, 0x0

    :goto_a
    goto/32 :goto_1c4

    nop

    :goto_b
    add-int/2addr v1, v15

    goto/32 :goto_35

    nop

    :goto_c
    iget v9, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_160

    nop

    :goto_d
    const/high16 v0, 0x40000000    # 2.0f

    goto/32 :goto_14b

    nop

    :goto_e
    move/from16 v0, v31

    goto/32 :goto_40

    nop

    :goto_f
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v19

    goto/32 :goto_5d

    nop

    :goto_10
    iget v3, v13, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto/32 :goto_1b5

    nop

    :goto_11
    invoke-static {v2, v1, v0}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v0

    goto/32 :goto_1eb

    nop

    :goto_12
    if-nez v11, :cond_3

    goto/32 :goto_d7

    :cond_3
    goto/32 :goto_148

    nop

    :goto_13
    move/from16 v25, v13

    goto/32 :goto_5b

    nop

    :goto_14
    add-int/2addr v5, v1

    goto/32 :goto_93

    nop

    :goto_15
    move/from16 v19, v4

    goto/32 :goto_c1

    nop

    :goto_16
    move v1, v4

    :goto_17
    goto/32 :goto_b1

    nop

    :goto_18
    move/from16 v31, v2

    goto/32 :goto_97

    nop

    :goto_19
    if-nez v15, :cond_4

    goto/32 :goto_9c

    :cond_4
    goto/32 :goto_fe

    nop

    :goto_1a
    if-gtz v9, :cond_5

    goto/32 :goto_1e0

    :cond_5
    goto/32 :goto_20d

    nop

    :goto_1b
    move/from16 v4, v22

    goto/32 :goto_21d

    nop

    :goto_1c
    add-int/2addr v0, v1

    goto/32 :goto_21b

    nop

    :goto_1d
    move/from16 v18, v0

    goto/32 :goto_7e

    nop

    :goto_1e
    move v5, v0

    goto/32 :goto_155

    nop

    :goto_1f
    iget v2, v6, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto/32 :goto_72

    nop

    :goto_20
    move/from16 v25, v13

    goto/32 :goto_19b

    nop

    :goto_21
    move/from16 v4, v22

    goto/32 :goto_1ab

    nop

    :goto_22
    const/4 v15, 0x0

    :goto_23
    goto/32 :goto_10b

    nop

    :goto_24
    if-gtz v9, :cond_6

    goto/32 :goto_126

    :cond_6
    goto/32 :goto_125

    nop

    :goto_25
    iget v6, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDividerHeight:I

    goto/32 :goto_26

    nop

    :goto_26
    add-int/2addr v1, v6

    goto/32 :goto_192

    nop

    :goto_27
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto/32 :goto_66

    nop

    :goto_28
    invoke-virtual {v7, v5}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->measureNullChild(I)I

    move-result v6

    goto/32 :goto_166

    nop

    :goto_29
    const/16 v28, 0x0

    goto/32 :goto_60

    nop

    :goto_2a
    add-int v2, v1, v0

    goto/32 :goto_105

    nop

    :goto_2b
    move v9, v3

    goto/32 :goto_7f

    nop

    :goto_2c
    add-int/2addr v1, v0

    goto/32 :goto_106

    nop

    :goto_2d
    move/from16 v13, v22

    goto/32 :goto_198

    nop

    :goto_2e
    add-int/2addr v4, v1

    goto/32 :goto_189

    nop

    :goto_2f
    add-int v1, v19, v1

    goto/32 :goto_49

    nop

    :goto_30
    if-eqz v4, :cond_7

    goto/32 :goto_206

    :cond_7
    goto/32 :goto_82

    nop

    :goto_31
    throw v0

    :goto_32
    goto/32 :goto_1e7

    nop

    :goto_33
    move-object/from16 v7, p0

    goto/32 :goto_13c

    nop

    :goto_34
    if-gtz v11, :cond_8

    goto/32 :goto_180

    :cond_8
    goto/32 :goto_74

    nop

    :goto_35
    iget v15, v14, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto/32 :goto_e7

    nop

    :goto_36
    and-int/2addr v11, v5

    goto/32 :goto_62

    nop

    :goto_37
    if-ne v3, v4, :cond_9

    goto/32 :goto_c9

    :cond_9
    goto/32 :goto_b3

    nop

    :goto_38
    goto/16 :goto_123

    :goto_39
    goto/32 :goto_122

    nop

    :goto_3a
    iget v9, v9, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;->weight:F

    goto/32 :goto_1ee

    nop

    :goto_3b
    iput v9, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_215

    nop

    :goto_3c
    add-int/2addr v1, v0

    goto/32 :goto_1fe

    nop

    :goto_3d
    goto/16 :goto_141

    :goto_3e
    goto/32 :goto_211

    nop

    :goto_3f
    iget v10, v13, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;->weight:F

    goto/32 :goto_1af

    nop

    :goto_40
    if-ne v0, v9, :cond_a

    goto/32 :goto_178

    :cond_a
    goto/32 :goto_177

    nop

    :goto_41
    move/from16 v2, v27

    goto/32 :goto_12a

    nop

    :goto_42
    goto/16 :goto_1e5

    :goto_43
    goto/32 :goto_1e4

    nop

    :goto_44
    move v9, v13

    goto/32 :goto_f2

    nop

    :goto_45
    if-nez v20, :cond_b

    goto/32 :goto_1a6

    :cond_b
    goto/32 :goto_101

    nop

    :goto_46
    move/from16 v6, v24

    goto/32 :goto_1b4

    nop

    :goto_47
    if-lt v5, v11, :cond_c

    goto/32 :goto_3e

    :cond_c
    goto/32 :goto_1a3

    nop

    :goto_48
    move v10, v6

    goto/32 :goto_b5

    nop

    :goto_49
    invoke-static {v15, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto/32 :goto_133

    nop

    :goto_4a
    add-int v19, v19, v1

    goto/32 :goto_121

    nop

    :goto_4b
    move/from16 v25, v13

    goto/32 :goto_21

    nop

    :goto_4c
    iget v1, v7, Landroid/view/ViewGroup;->mPaddingLeft:I

    goto/32 :goto_208

    nop

    :goto_4d
    const/4 v1, 0x0

    goto/32 :goto_116

    nop

    :goto_4e
    invoke-static {v10, v0}, Landroid/view/ViewGroup;->combineMeasuredStates(II)I

    move-result v10

    goto/32 :goto_df

    nop

    :goto_4f
    add-int v21, v21, v6

    goto/32 :goto_158

    nop

    :goto_50
    iget v6, v11, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto/32 :goto_4f

    nop

    :goto_51
    if-ne v12, v3, :cond_d

    goto/32 :goto_f6

    :cond_d
    goto/32 :goto_f5

    nop

    :goto_52
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v1

    goto/32 :goto_e4

    nop

    :goto_53
    goto/16 :goto_13a

    :goto_54
    goto/32 :goto_139

    nop

    :goto_55
    move/from16 v6, p2

    goto/32 :goto_44

    nop

    :goto_56
    move/from16 v18, v0

    goto/32 :goto_d

    nop

    :goto_57
    sub-int v18, v15, v18

    goto/32 :goto_162

    nop

    :goto_58
    invoke-virtual {v13}, Landroid/view/View;->getVisibility()I

    move-result v14

    goto/32 :goto_1bd

    nop

    :goto_59
    move/from16 v0, v17

    goto/32 :goto_f1

    nop

    :goto_5a
    move/from16 v23, v11

    goto/32 :goto_170

    nop

    :goto_5b
    move-object v13, v6

    goto/32 :goto_1fc

    nop

    :goto_5c
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    goto/32 :goto_1e3

    nop

    :goto_5d
    add-int v19, v15, v19

    goto/32 :goto_78

    nop

    :goto_5e
    const/4 v5, 0x0

    :goto_5f
    goto/32 :goto_129

    nop

    :goto_60
    cmpl-float v0, v26, v16

    goto/32 :goto_1a0

    nop

    :goto_61
    iget v0, v6, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    goto/32 :goto_15d

    nop

    :goto_62
    iget v13, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_a1

    nop

    :goto_63
    if-eq v4, v1, :cond_e

    goto/32 :goto_2

    :cond_e
    goto/32 :goto_136

    nop

    :goto_64
    cmpl-float v9, v1, v16

    goto/32 :goto_24

    nop

    :goto_65
    move/from16 v28, v13

    goto/32 :goto_195

    nop

    :goto_66
    iput v1, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_12b

    nop

    :goto_67
    move/from16 v0, v17

    goto/32 :goto_200

    nop

    :goto_68
    iput v5, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_a8

    nop

    :goto_69
    invoke-virtual {v7, v5}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    move-result-object v9

    goto/32 :goto_1c8

    nop

    :goto_6a
    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getVirtualChildCount()I

    move-result v11

    goto/32 :goto_173

    nop

    :goto_6b
    move v5, v4

    goto/32 :goto_188

    nop

    :goto_6c
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_31

    nop

    :goto_6d
    and-int/lit16 v0, v0, -0x100

    goto/32 :goto_16e

    nop

    :goto_6e
    move-object v6, v1

    goto/32 :goto_1f0

    nop

    :goto_6f
    move/from16 v15, v21

    :goto_70
    goto/32 :goto_1ed

    nop

    :goto_71
    iget v9, v7, Landroid/view/ViewGroup;->mPaddingBottom:I

    goto/32 :goto_1b6

    nop

    :goto_72
    add-int/2addr v1, v2

    goto/32 :goto_1de

    nop

    :goto_73
    const/4 v5, 0x0

    goto/32 :goto_8a

    nop

    :goto_74
    int-to-float v15, v11

    goto/32 :goto_ee

    nop

    :goto_75
    if-ltz v11, :cond_f

    goto/32 :goto_e0

    :cond_f
    goto/32 :goto_bd

    nop

    :goto_76
    goto/16 :goto_d2

    :goto_77
    goto/32 :goto_199

    nop

    :goto_78
    iget v1, v14, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto/32 :goto_4a

    nop

    :goto_79
    invoke-virtual {v13, v0, v15}, Landroid/view/View;->measure(II)V

    goto/32 :goto_1dc

    nop

    :goto_7a
    const/16 v6, 0x8

    goto/32 :goto_f9

    nop

    :goto_7b
    const/4 v0, 0x0

    :goto_7c
    goto/32 :goto_152

    nop

    :goto_7d
    move/from16 v25, v13

    goto/32 :goto_2d

    nop

    :goto_7e
    const/high16 v0, 0x40000000    # 2.0f

    goto/32 :goto_3

    nop

    :goto_7f
    move/from16 v3, p1

    goto/32 :goto_207

    nop

    :goto_80
    move v0, v1

    :goto_81
    goto/32 :goto_4c

    nop

    :goto_82
    iget v4, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_28

    nop

    :goto_83
    add-int/2addr v1, v4

    goto/32 :goto_10f

    nop

    :goto_84
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_119

    nop

    :goto_85
    iget v0, v7, Landroid/view/ViewGroup;->mPaddingRight:I

    goto/32 :goto_138

    nop

    :goto_86
    move v2, v5

    goto/32 :goto_2b

    nop

    :goto_87
    if-eq v9, v11, :cond_10

    goto/32 :goto_214

    :cond_10
    goto/32 :goto_213

    nop

    :goto_88
    invoke-virtual {v7, v5}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->hasDividerBeforeChildAt(I)Z

    move-result v1

    goto/32 :goto_db

    nop

    :goto_89
    iget v14, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_b9

    nop

    :goto_8a
    iput v5, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_5e

    nop

    :goto_8b
    goto/16 :goto_32

    :goto_8c
    goto/32 :goto_af

    nop

    :goto_8d
    if-eqz v1, :cond_11

    goto/32 :goto_1dd

    :cond_11
    goto/32 :goto_14e

    nop

    :goto_8e
    const/high16 v9, -0x80000000

    goto/32 :goto_9e

    nop

    :goto_8f
    const v11, 0xffffff

    goto/32 :goto_36

    nop

    :goto_90
    if-gtz v18, :cond_12

    goto/32 :goto_180

    :cond_12
    goto/32 :goto_34

    nop

    :goto_91
    if-ne v12, v0, :cond_13

    goto/32 :goto_43

    :cond_13
    goto/32 :goto_179

    nop

    :goto_92
    add-int v19, v19, v1

    goto/32 :goto_1a1

    nop

    :goto_93
    move/from16 v23, v11

    goto/32 :goto_1b

    nop

    :goto_94
    move-object v3, v4

    goto/32 :goto_212

    nop

    :goto_95
    iget v1, v7, Landroid/view/ViewGroup;->mPaddingTop:I

    goto/32 :goto_cf

    nop

    :goto_96
    move/from16 v0, v16

    goto/32 :goto_ef

    nop

    :goto_97
    move/from16 v30, v27

    goto/32 :goto_86

    nop

    :goto_98
    add-int v6, v21, v6

    goto/32 :goto_184

    nop

    :goto_99
    const/4 v1, -0x1

    :goto_9a
    goto/32 :goto_7b

    nop

    :goto_9b
    goto/16 :goto_21f

    :goto_9c
    goto/32 :goto_21e

    nop

    :goto_9d
    move/from16 v8, v25

    goto/32 :goto_41

    nop

    :goto_9e
    move v13, v3

    goto/32 :goto_112

    nop

    :goto_9f
    move/from16 v21, v9

    goto/32 :goto_65

    nop

    :goto_a0
    move v3, v1

    goto/32 :goto_1ea

    nop

    :goto_a1
    sub-int/2addr v11, v13

    goto/32 :goto_aa

    nop

    :goto_a2
    iput v4, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_cd

    nop

    :goto_a3
    move v13, v0

    goto/32 :goto_202

    nop

    :goto_a4
    if-eq v1, v15, :cond_14

    goto/32 :goto_e0

    :cond_14
    goto/32 :goto_164

    nop

    :goto_a5
    const/high16 v9, -0x80000000

    goto/32 :goto_20

    nop

    :goto_a6
    invoke-static {v0, v2, v10}, Landroid/view/ViewGroup;->resolveSizeAndState(III)I

    move-result v0

    goto/32 :goto_1cf

    nop

    :goto_a7
    if-lt v3, v2, :cond_15

    goto/32 :goto_c9

    :cond_15
    goto/32 :goto_1bf

    nop

    :goto_a8
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getSuggestedMinimumHeight()I

    move-result v6

    goto/32 :goto_1b7

    nop

    :goto_a9
    move/from16 v21, v1

    goto/32 :goto_20b

    nop

    :goto_aa
    if-eqz v18, :cond_16

    goto/32 :goto_219

    :cond_16
    goto/32 :goto_12

    nop

    :goto_ab
    move v9, v1

    :goto_ac
    goto/32 :goto_ff

    nop

    :goto_ad
    iget v1, v7, Landroid/view/ViewGroup;->mPaddingLeft:I

    goto/32 :goto_1b0

    nop

    :goto_ae
    iget v15, v14, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    goto/32 :goto_151

    nop

    :goto_af
    new-instance v0, Ljava/lang/RuntimeException;

    goto/32 :goto_1fb

    nop

    :goto_b0
    cmpl-float v0, v1, v16

    goto/32 :goto_104

    nop

    :goto_b1
    move/from16 v13, v21

    goto/32 :goto_1d1

    nop

    :goto_b2
    move v1, v5

    goto/32 :goto_48

    nop

    :goto_b3
    const/4 v3, 0x0

    :goto_b4
    goto/32 :goto_a7

    nop

    :goto_b5
    move/from16 v19, v8

    goto/32 :goto_159

    nop

    :goto_b6
    add-int/2addr v0, v11

    goto/32 :goto_b2

    nop

    :goto_b7
    move/from16 v23, v2

    goto/32 :goto_14c

    nop

    :goto_b8
    move/from16 v2, p1

    goto/32 :goto_75

    nop

    :goto_b9
    add-int v21, v14, v1

    goto/32 :goto_103

    nop

    :goto_ba
    if-nez v15, :cond_17

    goto/32 :goto_10a

    :cond_17
    goto/32 :goto_da

    nop

    :goto_bb
    move/from16 v2, p1

    goto/32 :goto_218

    nop

    :goto_bc
    add-int/2addr v1, v15

    goto/32 :goto_1ff

    nop

    :goto_bd
    iget v1, v14, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    goto/32 :goto_1ac

    nop

    :goto_be
    const/16 v6, 0x8

    goto/32 :goto_109

    nop

    :goto_bf
    const/4 v2, 0x0

    goto/32 :goto_38

    nop

    :goto_c0
    add-int/2addr v1, v0

    goto/32 :goto_14f

    nop

    :goto_c1
    move v4, v0

    goto/32 :goto_4

    nop

    :goto_c2
    sub-float/2addr v0, v1

    goto/32 :goto_f3

    nop

    :goto_c3
    const/high16 v11, 0x40000000    # 2.0f

    goto/32 :goto_1e1

    nop

    :goto_c4
    invoke-virtual {v13}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    goto/32 :goto_10e

    nop

    :goto_c5
    move-object v13, v6

    goto/32 :goto_46

    nop

    :goto_c6
    const/4 v15, 0x0

    :goto_c7
    goto/32 :goto_1f1

    nop

    :goto_c8
    goto :goto_b4

    :goto_c9
    goto/32 :goto_11e

    nop

    :goto_ca
    iput v1, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_ab

    nop

    :goto_cb
    move/from16 v2, v23

    goto/32 :goto_4d

    nop

    :goto_cc
    invoke-virtual {v7, v9}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    move-result-object v13

    goto/32 :goto_58

    nop

    :goto_cd
    move/from16 v23, v11

    goto/32 :goto_4b

    nop

    :goto_ce
    iget v1, v14, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;->weight:F

    goto/32 :goto_17c

    nop

    :goto_cf
    iget v3, v7, Landroid/view/ViewGroup;->mPaddingBottom:I

    goto/32 :goto_1c7

    nop

    :goto_d0
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredState()I

    move-result v0

    goto/32 :goto_167

    nop

    :goto_d1
    iput v6, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    :goto_d2
    goto/32 :goto_172

    nop

    :goto_d3
    invoke-virtual {v9}, Landroid/view/View;->getVisibility()I

    move-result v11

    goto/32 :goto_1d2

    nop

    :goto_d4
    const/high16 v4, 0x40000000    # 2.0f

    goto/32 :goto_37

    nop

    :goto_d5
    move/from16 v9, p2

    goto/32 :goto_fb

    nop

    :goto_d6
    goto/16 :goto_219

    :goto_d7
    goto/32 :goto_221

    nop

    :goto_d8
    if-gtz v2, :cond_18

    goto/32 :goto_146

    :cond_18
    goto/32 :goto_15e

    nop

    :goto_d9
    iget v1, v6, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;->weight:F

    goto/32 :goto_13f

    nop

    :goto_da
    if-ne v3, v9, :cond_19

    goto/32 :goto_115

    :cond_19
    goto/32 :goto_114

    nop

    :goto_db
    if-nez v1, :cond_1a

    goto/32 :goto_193

    :cond_1a
    goto/32 :goto_142

    nop

    :goto_dc
    goto/16 :goto_1a8

    :goto_dd
    goto/32 :goto_1a7

    nop

    :goto_de
    move/from16 v5, p2

    goto/32 :goto_9f

    nop

    :goto_df
    goto/16 :goto_18f

    :goto_e0
    goto/32 :goto_18e

    nop

    :goto_e1
    move v1, v8

    :goto_e2
    goto/32 :goto_144

    nop

    :goto_e3
    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    goto/32 :goto_15f

    nop

    :goto_e4
    if-eq v1, v6, :cond_1b

    goto/32 :goto_19c

    :cond_1b
    goto/32 :goto_1d9

    nop

    :goto_e5
    move/from16 v3, v25

    goto/32 :goto_ba

    nop

    :goto_e6
    move v1, v10

    goto/32 :goto_a0

    nop

    :goto_e7
    add-int/2addr v1, v15

    goto/32 :goto_ae

    nop

    :goto_e8
    const/high16 v0, 0x40000000    # 2.0f

    goto/32 :goto_183

    nop

    :goto_e9
    iget v0, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_130

    nop

    :goto_ea
    move-object/from16 v22, v4

    goto/32 :goto_132

    nop

    :goto_eb
    if-nez v0, :cond_1c

    goto/32 :goto_186

    :cond_1c
    goto/32 :goto_185

    nop

    :goto_ec
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_c8

    nop

    :goto_ed
    move/from16 v23, v2

    goto/32 :goto_176

    nop

    :goto_ee
    mul-float/2addr v15, v1

    goto/32 :goto_19d

    nop

    :goto_ef
    move/from16 v19, v17

    goto/32 :goto_140

    nop

    :goto_f0
    add-int/lit8 v9, v9, 0x1

    goto/32 :goto_cb

    nop

    :goto_f1
    move/from16 v20, v0

    goto/32 :goto_42

    nop

    :goto_f2
    const/4 v11, 0x0

    goto/32 :goto_120

    nop

    :goto_f3
    sub-int/2addr v11, v15

    goto/32 :goto_ad

    nop

    :goto_f4
    if-nez v0, :cond_1d

    goto/32 :goto_54

    :cond_1d
    goto/32 :goto_53

    nop

    :goto_f5
    goto/16 :goto_81

    :goto_f6
    goto/32 :goto_80

    nop

    :goto_f7
    if-eq v8, v10, :cond_1e

    goto/32 :goto_dd

    :cond_1e
    goto/32 :goto_1c0

    nop

    :goto_f8
    move/from16 v18, v17

    goto/32 :goto_15a

    nop

    :goto_f9
    move/from16 v22, v4

    goto/32 :goto_47

    nop

    :goto_fa
    if-lez v0, :cond_1f

    goto/32 :goto_8c

    :cond_1f
    goto/32 :goto_8b

    nop

    :goto_fb
    move/from16 v11, v23

    goto/32 :goto_1e8

    nop

    :goto_fc
    add-int/2addr v1, v3

    goto/32 :goto_102

    nop

    :goto_fd
    iput v0, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_6

    nop

    :goto_fe
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto/32 :goto_9b

    nop

    :goto_ff
    if-lt v9, v2, :cond_20

    goto/32 :goto_117

    :cond_20
    goto/32 :goto_cc

    nop

    :goto_100
    iget v1, v6, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto/32 :goto_c0

    nop

    :goto_101
    move/from16 v0, v23

    goto/32 :goto_1a5

    nop

    :goto_102
    add-int/2addr v0, v1

    goto/32 :goto_187

    nop

    :goto_103
    iget v6, v11, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto/32 :goto_118

    nop

    :goto_104
    if-gtz v0, :cond_21

    goto/32 :goto_39

    :cond_21
    goto/32 :goto_153

    nop

    :goto_105
    iget v3, v13, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto/32 :goto_134

    nop

    :goto_106
    iget v0, v14, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    goto/32 :goto_ed

    nop

    :goto_107
    iget v15, v14, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto/32 :goto_bc

    nop

    :goto_108
    move v0, v4

    goto/32 :goto_e1

    nop

    :goto_109
    goto/16 :goto_5f

    :goto_10a
    goto/32 :goto_11c

    nop

    :goto_10b
    invoke-static {v15, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    goto/32 :goto_79

    nop

    :goto_10c
    if-nez v3, :cond_22

    goto/32 :goto_17b

    :cond_22
    goto/32 :goto_196

    nop

    :goto_10d
    if-nez v19, :cond_23

    goto/32 :goto_dd

    :cond_23
    goto/32 :goto_1a2

    nop

    :goto_10e
    check-cast v14, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;

    goto/32 :goto_ce

    nop

    :goto_10f
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    goto/32 :goto_2e

    nop

    :goto_110
    iget v0, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_100

    nop

    :goto_111
    iget v0, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_1be

    nop

    :goto_112
    iget v2, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_d8

    nop

    :goto_113
    if-gtz v13, :cond_24

    goto/32 :goto_d7

    :cond_24
    goto/32 :goto_d6

    nop

    :goto_114
    if-eqz v3, :cond_25

    goto/32 :goto_10a

    :cond_25
    :goto_115
    goto/32 :goto_73

    nop

    :goto_116
    goto/16 :goto_ac

    :goto_117
    goto/32 :goto_1c1

    nop

    :goto_118
    add-int v21, v21, v6

    goto/32 :goto_50

    nop

    :goto_119
    move/from16 v8, p1

    goto/32 :goto_d5

    nop

    :goto_11a
    const/4 v4, 0x0

    :goto_11b
    goto/32 :goto_17e

    nop

    :goto_11c
    iget v5, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_194

    nop

    :goto_11d
    add-int/2addr v3, v5

    goto/32 :goto_18d

    nop

    :goto_11e
    move/from16 v23, v2

    goto/32 :goto_1c5

    nop

    :goto_11f
    if-nez v15, :cond_26

    goto/32 :goto_c9

    :cond_26
    goto/32 :goto_d4

    nop

    :goto_120
    invoke-static {v5, v6, v11}, Landroid/view/ViewGroup;->resolveSizeAndState(III)I

    move-result v5

    goto/32 :goto_8f

    nop

    :goto_121
    iget v1, v14, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto/32 :goto_92

    nop

    :goto_122
    const/high16 v2, -0x80000000

    :goto_123
    goto/32 :goto_29

    nop

    :goto_124
    iget v14, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mBaselineAlignedChildIndex:I

    goto/32 :goto_1b9

    nop

    :goto_125
    move v0, v1

    :goto_126
    goto/32 :goto_154

    nop

    :goto_127
    invoke-static {v10, v6}, Landroid/view/ViewGroup;->combineMeasuredStates(II)I

    move-result v6

    goto/32 :goto_10d

    nop

    :goto_128
    const/high16 v3, 0x40000000    # 2.0f

    goto/32 :goto_51

    nop

    :goto_129
    if-lt v5, v2, :cond_27

    goto/32 :goto_10a

    :cond_27
    goto/32 :goto_69

    nop

    :goto_12a
    const/high16 v9, -0x80000000

    goto/32 :goto_12d

    nop

    :goto_12b
    move/from16 v1, v30

    goto/32 :goto_19

    nop

    :goto_12c
    iget v15, v14, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto/32 :goto_b

    nop

    :goto_12d
    move v11, v5

    goto/32 :goto_13

    nop

    :goto_12e
    goto/16 :goto_5

    :goto_12f
    goto/32 :goto_c4

    nop

    :goto_130
    iput v0, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mBaselineChildTop:I

    :goto_131
    goto/32 :goto_7

    nop

    :goto_132
    move/from16 v4, v28

    goto/32 :goto_168

    nop

    :goto_133
    iput v1, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_15

    nop

    :goto_134
    add-int/2addr v2, v3

    goto/32 :goto_10

    nop

    :goto_135
    iget v0, v6, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    goto/32 :goto_182

    nop

    :goto_136
    move/from16 v4, v17

    goto/32 :goto_1

    nop

    :goto_137
    move/from16 v2, p1

    goto/32 :goto_1fa

    nop

    :goto_138
    add-int/2addr v1, v0

    goto/32 :goto_143

    nop

    :goto_139
    move v1, v4

    :goto_13a
    goto/32 :goto_20c

    nop

    :goto_13b
    move/from16 v27, v2

    goto/32 :goto_135

    nop

    :goto_13c
    move/from16 v8, p1

    goto/32 :goto_150

    nop

    :goto_13d
    if-eq v0, v1, :cond_28

    goto/32 :goto_9a

    :cond_28
    goto/32 :goto_67

    nop

    :goto_13e
    add-int/2addr v15, v1

    goto/32 :goto_1ba

    nop

    :goto_13f
    add-float v26, v0, v1

    goto/32 :goto_e8

    nop

    :goto_140
    const/high16 v2, -0x80000000

    :goto_141
    goto/32 :goto_7a

    nop

    :goto_142
    iget v1, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_25

    nop

    :goto_143
    iget v0, v14, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto/32 :goto_3c

    nop

    :goto_144
    if-eqz v19, :cond_29

    goto/32 :goto_f6

    :cond_29
    goto/32 :goto_128

    nop

    :goto_145
    goto/16 :goto_17b

    :goto_146
    goto/32 :goto_17a

    nop

    :goto_147
    add-int v15, v21, v15

    goto/32 :goto_1e2

    nop

    :goto_148
    cmpl-float v13, v0, v16

    goto/32 :goto_113

    nop

    :goto_149
    add-int/2addr v15, v11

    goto/32 :goto_1cb

    nop

    :goto_14a
    invoke-static {v1, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    goto/32 :goto_1df

    nop

    :goto_14b
    invoke-static {v15, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    goto/32 :goto_210

    nop

    :goto_14c
    move/from16 v2, p1

    goto/32 :goto_12e

    nop

    :goto_14d
    add-int/2addr v5, v6

    goto/32 :goto_68

    nop

    :goto_14e
    const/high16 v1, 0x40000000    # 2.0f

    goto/32 :goto_1f9

    nop

    :goto_14f
    move/from16 v27, v2

    goto/32 :goto_1f

    nop

    :goto_150
    move/from16 v9, p2

    goto/32 :goto_1ef

    nop

    :goto_151
    invoke-static {v2, v1, v15}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v1

    goto/32 :goto_17d

    nop

    :goto_152
    if-nez v0, :cond_2a

    goto/32 :goto_70

    :cond_2a
    goto/32 :goto_6f

    nop

    :goto_153
    const/4 v0, -0x2

    goto/32 :goto_21c

    nop

    :goto_154
    const/4 v1, 0x0

    goto/32 :goto_ca

    nop

    :goto_155
    move/from16 v0, v26

    :goto_156
    goto/32 :goto_84

    nop

    :goto_157
    iget v15, v7, Landroid/view/ViewGroup;->mPaddingRight:I

    goto/32 :goto_204

    nop

    :goto_158
    invoke-virtual {v7, v9}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getNextLocationOffset(Landroid/view/View;)I

    move-result v6

    goto/32 :goto_98

    nop

    :goto_159
    move v3, v13

    goto/32 :goto_1e

    nop

    :goto_15a
    move/from16 v28, v22

    goto/32 :goto_9d

    nop

    :goto_15b
    cmpl-float v0, v1, v16

    goto/32 :goto_0

    nop

    :goto_15c
    iget v1, v13, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto/32 :goto_1bb

    nop

    :goto_15d
    if-eqz v0, :cond_2b

    goto/32 :goto_1fd

    :cond_2b
    goto/32 :goto_15b

    nop

    :goto_15e
    move/from16 v2, v23

    goto/32 :goto_1f2

    nop

    :goto_15f
    iget v1, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_2a

    nop

    :goto_160
    invoke-virtual {v7, v5}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->measureNullChild(I)I

    move-result v11

    goto/32 :goto_1cd

    nop

    :goto_161
    move/from16 v25, v1

    goto/32 :goto_52

    nop

    :goto_162
    sub-int v11, v11, v18

    goto/32 :goto_56

    nop

    :goto_163
    if-gez v14, :cond_2c

    goto/32 :goto_131

    :cond_2c
    goto/32 :goto_19f

    nop

    :goto_164
    iget v1, v7, Landroid/view/ViewGroup;->mPaddingLeft:I

    goto/32 :goto_157

    nop

    :goto_165
    invoke-virtual {v7, v3}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getNextLocationOffset(Landroid/view/View;)I

    move-result v4

    goto/32 :goto_18b

    nop

    :goto_166
    add-int/2addr v4, v6

    goto/32 :goto_a2

    nop

    :goto_167
    and-int/lit16 v0, v0, -0x100

    goto/32 :goto_4e

    nop

    :goto_168
    move v11, v5

    goto/32 :goto_de

    nop

    :goto_169
    if-gtz v15, :cond_2d

    goto/32 :goto_1ca

    :cond_2d
    goto/32 :goto_1c9

    nop

    :goto_16a
    goto/16 :goto_1f5

    :goto_16b
    goto/32 :goto_169

    nop

    :goto_16c
    goto/16 :goto_1d6

    :goto_16d
    goto/32 :goto_1d5

    nop

    :goto_16e
    invoke-static {v10, v0}, Landroid/view/ViewGroup;->combineMeasuredStates(II)I

    move-result v10

    goto/32 :goto_17f

    nop

    :goto_16f
    const/4 v10, -0x1

    goto/32 :goto_f7

    nop

    :goto_170
    move/from16 v25, v13

    goto/32 :goto_1e9

    nop

    :goto_171
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v21

    goto/32 :goto_147

    nop

    :goto_172
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_be

    nop

    :goto_173
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v12

    goto/32 :goto_1ce

    nop

    :goto_174
    const/high16 v29, 0x40000000    # 2.0f

    goto/32 :goto_220

    nop

    :goto_175
    iget v1, v14, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto/32 :goto_107

    nop

    :goto_176
    move/from16 v2, p1

    goto/32 :goto_11

    nop

    :goto_177
    iput v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    :goto_178
    goto/32 :goto_e3

    nop

    :goto_179
    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    goto/32 :goto_1f3

    nop

    :goto_17a
    move/from16 v2, v23

    :goto_17b
    goto/32 :goto_e5

    nop

    :goto_17c
    cmpl-float v18, v1, v16

    goto/32 :goto_90

    nop

    :goto_17d
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    goto/32 :goto_149

    nop

    :goto_17e
    iget v15, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_f

    nop

    :goto_17f
    goto :goto_18f

    :goto_180
    goto/32 :goto_1f6

    nop

    :goto_181
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredState()I

    move-result v6

    goto/32 :goto_127

    nop

    :goto_182
    if-eqz v0, :cond_2e

    goto/32 :goto_39

    :cond_2e
    goto/32 :goto_b0

    nop

    :goto_183
    if-eq v13, v0, :cond_2f

    goto/32 :goto_1fd

    :cond_2f
    goto/32 :goto_61

    nop

    :goto_184
    invoke-static {v14, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    goto/32 :goto_d1

    nop

    :goto_185
    goto/16 :goto_17

    :goto_186
    goto/32 :goto_16

    nop

    :goto_187
    invoke-virtual/range {p0 .. p0}, Landroid/view/ViewGroup;->getSuggestedMinimumWidth()I

    move-result v1

    goto/32 :goto_20f

    nop

    :goto_188
    move/from16 v18, v5

    goto/32 :goto_21a

    nop

    :goto_189
    invoke-static {v8, v4}, Ljava/lang/Math;->max(II)I

    move-result v5

    goto/32 :goto_181

    nop

    :goto_18a
    invoke-virtual {v7, v3, v11}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getChildrenSkipCount(Landroid/view/View;I)I

    move-result v0

    goto/32 :goto_b6

    nop

    :goto_18b
    add-int/2addr v2, v4

    goto/32 :goto_27

    nop

    :goto_18c
    iget v4, v14, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    goto/32 :goto_63

    nop

    :goto_18d
    iput v3, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_145

    nop

    :goto_18e
    move/from16 v18, v0

    :goto_18f
    goto/32 :goto_1da

    nop

    :goto_190
    goto/16 :goto_1d4

    :goto_191
    goto/32 :goto_217

    nop

    :goto_192
    iput v1, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    :goto_193
    goto/32 :goto_1b8

    nop

    :goto_194
    iget v6, v7, Landroid/view/ViewGroup;->mPaddingTop:I

    goto/32 :goto_71

    nop

    :goto_195
    const/high16 v9, -0x80000000

    goto/32 :goto_c5

    nop

    :goto_196
    iget v3, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_20e

    nop

    :goto_197
    move v1, v2

    goto/32 :goto_5a

    nop

    :goto_198
    move/from16 v11, v29

    goto/32 :goto_ea

    nop

    :goto_199
    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    goto/32 :goto_1ae

    nop

    :goto_19a
    move-object v1, v4

    goto/32 :goto_18

    nop

    :goto_19b
    goto/16 :goto_156

    :goto_19c
    goto/32 :goto_88

    nop

    :goto_19d
    div-float/2addr v15, v0

    goto/32 :goto_1a4

    nop

    :goto_19e
    if-eq v14, v15, :cond_30

    goto/32 :goto_12f

    :cond_30
    goto/32 :goto_b7

    nop

    :goto_19f
    add-int/lit8 v5, v11, 0x1

    goto/32 :goto_1c3

    nop

    :goto_1a0
    if-eqz v0, :cond_31

    goto/32 :goto_16d

    :cond_31
    goto/32 :goto_111

    nop

    :goto_1a1
    invoke-virtual {v7, v13}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getNextLocationOffset(Landroid/view/View;)I

    move-result v1

    goto/32 :goto_2f

    nop

    :goto_1a2
    iget v8, v13, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    goto/32 :goto_16f

    nop

    :goto_1a3
    invoke-virtual {v7, v5}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    move-result-object v4

    goto/32 :goto_30

    nop

    :goto_1a4
    float-to-int v15, v15

    goto/32 :goto_c2

    nop

    :goto_1a5
    invoke-direct {v7, v0, v6}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->forceUniformWidth(II)V

    :goto_1a6
    goto/32 :goto_203

    nop

    :goto_1a7
    const/4 v8, 0x0

    :goto_1a8
    goto/32 :goto_3f

    nop

    :goto_1a9
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredState()I

    move-result v0

    goto/32 :goto_6d

    nop

    :goto_1aa
    move/from16 v8, v25

    goto/32 :goto_19a

    nop

    :goto_1ab
    const/high16 v9, -0x80000000

    goto/32 :goto_205

    nop

    :goto_1ac
    const/4 v15, -0x1

    goto/32 :goto_a4

    nop

    :goto_1ad
    if-gtz v10, :cond_32

    goto/32 :goto_191

    :cond_32
    goto/32 :goto_eb

    nop

    :goto_1ae
    check-cast v11, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;

    goto/32 :goto_89

    nop

    :goto_1af
    cmpl-float v10, v10, v16

    goto/32 :goto_1ad

    nop

    :goto_1b0
    move/from16 v18, v0

    goto/32 :goto_85

    nop

    :goto_1b1
    iget v1, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mWeightSum:F

    goto/32 :goto_64

    nop

    :goto_1b2
    invoke-virtual {v7, v9, v5}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getChildrenSkipCount(Landroid/view/View;I)I

    move-result v9

    goto/32 :goto_1c6

    nop

    :goto_1b3
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v9

    goto/32 :goto_1d8

    nop

    :goto_1b4
    invoke-virtual/range {v0 .. v6}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->measureChildBeforeLayout(Landroid/view/View;IIIII)V

    goto/32 :goto_e

    nop

    :goto_1b5
    add-int/2addr v2, v3

    goto/32 :goto_8

    nop

    :goto_1b6
    add-int/2addr v6, v9

    goto/32 :goto_14d

    nop

    :goto_1b7
    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    goto/32 :goto_55

    nop

    :goto_1b8
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    goto/32 :goto_6e

    nop

    :goto_1b9
    iget-boolean v15, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mUseLargestChild:Z

    goto/32 :goto_1d7

    nop

    :goto_1ba
    invoke-static {v8, v15}, Ljava/lang/Math;->max(II)I

    move-result v8

    goto/32 :goto_1d

    nop

    :goto_1bb
    iget v4, v13, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto/32 :goto_83

    nop

    :goto_1bc
    move/from16 v21, v1

    goto/32 :goto_99

    nop

    :goto_1bd
    const/16 v15, 0x8

    goto/32 :goto_19e

    nop

    :goto_1be
    move/from16 v24, v0

    goto/32 :goto_16c

    nop

    :goto_1bf
    invoke-virtual {v7, v3}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getVirtualChildAt(I)Landroid/view/View;

    move-result-object v4

    goto/32 :goto_1cc

    nop

    :goto_1c0
    move/from16 v8, v17

    goto/32 :goto_dc

    nop

    :goto_1c1
    move/from16 v23, v2

    goto/32 :goto_137

    nop

    :goto_1c2
    cmpl-float v0, v0, v16

    goto/32 :goto_fa

    nop

    :goto_1c3
    if-eq v14, v5, :cond_33

    goto/32 :goto_131

    :cond_33
    goto/32 :goto_e9

    nop

    :goto_1c4
    invoke-static {v15, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    goto/32 :goto_1f7

    nop

    :goto_1c5
    move v1, v8

    goto/32 :goto_bb

    nop

    :goto_1c6
    add-int/2addr v5, v9

    goto/32 :goto_76

    nop

    :goto_1c7
    add-int/2addr v1, v3

    goto/32 :goto_1c

    nop

    :goto_1c8
    if-eqz v9, :cond_34

    goto/32 :goto_216

    :cond_34
    goto/32 :goto_c

    nop

    :goto_1c9
    goto/16 :goto_23

    :goto_1ca
    goto/32 :goto_22

    nop

    :goto_1cb
    if-ltz v15, :cond_35

    goto/32 :goto_c7

    :cond_35
    goto/32 :goto_c6

    nop

    :goto_1cc
    if-nez v4, :cond_36

    goto/32 :goto_1e0

    :cond_36
    goto/32 :goto_1b3

    nop

    :goto_1cd
    add-int/2addr v9, v11

    goto/32 :goto_3b

    nop

    :goto_1ce
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v13

    goto/32 :goto_124

    nop

    :goto_1cf
    invoke-virtual {v7, v0, v5}, Landroid/view/ViewGroup;->setMeasuredDimension(II)V

    goto/32 :goto_45

    nop

    :goto_1d0
    iget v0, v13, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;->weight:F

    goto/32 :goto_1c2

    nop

    :goto_1d1
    invoke-static {v13, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto/32 :goto_a3

    nop

    :goto_1d2
    if-eq v11, v6, :cond_37

    goto/32 :goto_77

    :cond_37
    goto/32 :goto_1b2

    nop

    :goto_1d3
    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    :goto_1d4
    goto/32 :goto_18a

    nop

    :goto_1d5
    const/16 v24, 0x0

    :goto_1d6
    goto/32 :goto_174

    nop

    :goto_1d7
    const/16 v16, 0x0

    goto/32 :goto_1db

    nop

    :goto_1d8
    const/16 v11, 0x8

    goto/32 :goto_87

    nop

    :goto_1d9
    invoke-virtual {v7, v4, v5}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->getChildrenSkipCount(Landroid/view/View;I)I

    move-result v1

    goto/32 :goto_14

    nop

    :goto_1da
    move/from16 v0, v18

    goto/32 :goto_175

    nop

    :goto_1db
    const/16 v17, 0x1

    goto/32 :goto_e6

    nop

    :goto_1dc
    goto/16 :goto_1f8

    :goto_1dd
    goto/32 :goto_1f4

    nop

    :goto_1de
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto/32 :goto_fd

    nop

    :goto_1df
    invoke-virtual {v4, v9, v13}, Landroid/view/View;->measure(II)V

    :goto_1e0
    goto/32 :goto_ec

    nop

    :goto_1e1
    invoke-static {v9, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    goto/32 :goto_14a

    nop

    :goto_1e2
    if-ltz v15, :cond_38

    goto/32 :goto_a

    :cond_38
    goto/32 :goto_9

    nop

    :goto_1e3
    check-cast v9, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;

    goto/32 :goto_3a

    nop

    :goto_1e4
    const/4 v0, 0x0

    :goto_1e5
    goto/32 :goto_15c

    nop

    :goto_1e6
    if-eq v0, v1, :cond_39

    goto/32 :goto_43

    :cond_39
    goto/32 :goto_59

    nop

    :goto_1e7
    const/high16 v0, 0x40000000    # 2.0f

    goto/32 :goto_91

    nop

    :goto_1e8
    move/from16 v13, v25

    goto/32 :goto_3d

    nop

    :goto_1e9
    move/from16 v4, v22

    goto/32 :goto_8e

    nop

    :goto_1ea
    move v4, v3

    goto/32 :goto_6b

    nop

    :goto_1eb
    iget v1, v14, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    goto/32 :goto_8d

    nop

    :goto_1ec
    if-nez v19, :cond_3a

    goto/32 :goto_2

    :cond_3a
    goto/32 :goto_18c

    nop

    :goto_1ed
    invoke-static {v4, v15}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto/32 :goto_1ec

    nop

    :goto_1ee
    cmpl-float v9, v9, v16

    goto/32 :goto_1a

    nop

    :goto_1ef
    const/4 v10, 0x0

    goto/32 :goto_20a

    nop

    :goto_1f0
    check-cast v6, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout$LayoutParams;

    goto/32 :goto_d9

    nop

    :goto_1f1
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v18

    goto/32 :goto_57

    nop

    :goto_1f2
    invoke-virtual {v7, v2}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->hasDividerBeforeChildAt(I)Z

    move-result v3

    goto/32 :goto_10c

    nop

    :goto_1f3
    const/4 v1, -0x1

    goto/32 :goto_1e6

    nop

    :goto_1f4
    const/high16 v1, 0x40000000    # 2.0f

    :goto_1f5
    goto/32 :goto_171

    nop

    :goto_1f6
    move/from16 v23, v2

    goto/32 :goto_b8

    nop

    :goto_1f7
    invoke-virtual {v13, v0, v15}, Landroid/view/View;->measure(II)V

    :goto_1f8
    goto/32 :goto_1a9

    nop

    :goto_1f9
    if-ne v3, v1, :cond_3b

    goto/32 :goto_16b

    :cond_3b
    goto/32 :goto_16a

    nop

    :goto_1fa
    iget v0, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_95

    nop

    :goto_1fb
    const-string v1, "A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won\'t work.  Either remove the weight, or don\'t set mBaselineAlignedChildIndex."

    goto/32 :goto_6c

    nop

    :goto_1fc
    goto/16 :goto_21f

    :goto_1fd
    goto/32 :goto_13b

    nop

    :goto_1fe
    iget v0, v14, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto/32 :goto_2c

    nop

    :goto_1ff
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v15

    goto/32 :goto_13e

    nop

    :goto_200
    goto/16 :goto_7c

    :goto_201
    goto/32 :goto_1bc

    nop

    :goto_202
    move/from16 v4, v28

    goto/32 :goto_190

    nop

    :goto_203
    return-void

    :goto_204
    add-int/2addr v1, v15

    goto/32 :goto_12c

    nop

    :goto_205
    goto/16 :goto_156

    :goto_206
    goto/32 :goto_161

    nop

    :goto_207
    move/from16 v23, v11

    goto/32 :goto_7d

    nop

    :goto_208
    iget v3, v7, Landroid/view/ViewGroup;->mPaddingRight:I

    goto/32 :goto_fc

    nop

    :goto_209
    iget v0, v14, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    goto/32 :goto_a9

    nop

    :goto_20a
    iput v10, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_6a

    nop

    :goto_20b
    const/4 v1, -0x1

    goto/32 :goto_13d

    nop

    :goto_20c
    move/from16 v4, v28

    goto/32 :goto_1d3

    nop

    :goto_20d
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    goto/32 :goto_c3

    nop

    :goto_20e
    iget v5, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDividerHeight:I

    goto/32 :goto_11d

    nop

    :goto_20f
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto/32 :goto_a6

    nop

    :goto_210
    invoke-virtual {v13, v1, v15}, Landroid/view/View;->measure(II)V

    goto/32 :goto_d0

    nop

    :goto_211
    move v8, v1

    goto/32 :goto_197

    nop

    :goto_212
    move/from16 v23, v11

    goto/32 :goto_f8

    nop

    :goto_213
    goto/16 :goto_1e0

    :goto_214
    goto/32 :goto_5c

    nop

    :goto_215
    goto/16 :goto_d2

    :goto_216
    goto/32 :goto_d3

    nop

    :goto_217
    move/from16 v13, v21

    goto/32 :goto_f4

    nop

    :goto_218
    goto/16 :goto_e2

    :goto_219
    goto/32 :goto_1b1

    nop

    :goto_21a
    move/from16 v20, v18

    goto/32 :goto_96

    nop

    :goto_21b
    iput v0, v7, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mTotalLength:I

    goto/32 :goto_108

    nop

    :goto_21c
    iput v0, v6, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    goto/32 :goto_bf

    nop

    :goto_21d
    move/from16 v1, v25

    goto/32 :goto_a5

    nop

    :goto_21e
    move v2, v1

    :goto_21f
    goto/32 :goto_163

    nop

    :goto_220
    move-object/from16 v0, p0

    goto/32 :goto_1aa

    nop

    :goto_221
    invoke-static {v4, v9}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto/32 :goto_11f

    nop
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDivider:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget v0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mOrientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-virtual {p0, p1}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->drawDividersVertical(Landroid/graphics/Canvas;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->drawDividersHorizontal(Landroid/graphics/Canvas;)V

    :goto_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    iget p1, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mOrientation:I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->layoutVertical(IIII)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->layoutHorizontal(IIII)V

    :goto_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    iget v0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mOrientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->measureVertical(II)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->measureHorizontal(II)V

    :goto_0
    return-void
.end method

.method public onRtlPropertiesChanged(I)V
    .locals 1

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onRtlPropertiesChanged(I)V

    iget v0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mLayoutDirection:I

    if-eq p1, v0, :cond_0

    iput p1, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mLayoutDirection:I

    iget p1, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mOrientation:I

    if-nez p1, :cond_0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    :cond_0
    return-void
.end method

.method public setBaselineAligned(Z)V
    .locals 0
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    iput-boolean p1, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mBaselineAligned:Z

    return-void
.end method

.method public setBaselineAlignedChildIndex(I)V
    .locals 2
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    iput p1, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mBaselineAlignedChildIndex:I

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "base aligned child index out of range (0, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, ")"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setDividerDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDivider:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    iput-object p1, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDivider:Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iput v1, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDividerWidth:I

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    iput v1, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDividerHeight:I

    goto :goto_0

    :cond_1
    iput v0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDividerWidth:I

    iput v0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDividerHeight:I

    :goto_0
    if-nez p1, :cond_2

    const/4 v0, 0x1

    :cond_2
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->setWillNotDraw(Z)V

    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    return-void
.end method

.method public setDividerPadding(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mDividerPadding:I

    return-void
.end method

.method public setGravity(I)V
    .locals 1
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    iget v0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mGravity:I

    if-eq v0, p1, :cond_2

    const v0, 0x800007

    and-int/2addr v0, p1

    if-nez v0, :cond_0

    const v0, 0x800003

    or-int/2addr p1, v0

    :cond_0
    and-int/lit8 v0, p1, 0x70

    if-nez v0, :cond_1

    or-int/lit8 p1, p1, 0x30

    :cond_1
    iput p1, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mGravity:I

    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    :cond_2
    return-void
.end method

.method public setHorizontalGravity(I)V
    .locals 2
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    const v0, 0x800007

    and-int/2addr p1, v0

    iget v1, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mGravity:I

    and-int/2addr v0, v1

    if-eq v0, p1, :cond_0

    const v0, -0x800008

    and-int/2addr v0, v1

    or-int/2addr p1, v0

    iput p1, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mGravity:I

    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    :cond_0
    return-void
.end method

.method public setMeasureWithLargestChildEnabled(Z)V
    .locals 0
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    iput-boolean p1, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mUseLargestChild:Z

    return-void
.end method

.method public setOrientation(I)V
    .locals 1

    iget v0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mOrientation:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mOrientation:I

    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    :cond_0
    return-void
.end method

.method public setShowDividers(I)V
    .locals 1

    iget v0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mShowDividers:I

    if-eq p1, v0, :cond_0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    :cond_0
    iput p1, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mShowDividers:I

    return-void
.end method

.method public setVerticalGravity(I)V
    .locals 2
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    and-int/lit8 p1, p1, 0x70

    iget v0, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mGravity:I

    and-int/lit8 v1, v0, 0x70

    if-eq v1, p1, :cond_0

    and-int/lit8 v0, v0, -0x71

    or-int/2addr p1, v0

    iput p1, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mGravity:I

    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    :cond_0
    return-void
.end method

.method public setWeightSum(F)V
    .locals 1
    .annotation runtime Landroid/view/RemotableViewMethod;
    .end annotation

    const/4 v0, 0x0

    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    iput p1, p0, Lcom/android/settings/widget/MatchParentShrinkingLinearLayout;->mWeightSum:F

    return-void
.end method

.method public shouldDelayChildPressedState()Z
    .locals 0

    const/4 p0, 0x0

    return p0
.end method
