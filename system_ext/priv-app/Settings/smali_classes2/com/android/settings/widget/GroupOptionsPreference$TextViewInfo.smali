.class Lcom/android/settings/widget/GroupOptionsPreference$TextViewInfo;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/widget/GroupOptionsPreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TextViewInfo"
.end annotation


# instance fields
.field private mIsVisible:Z

.field private mText:Ljava/lang/CharSequence;

.field private mTextView:Landroid/widget/TextView;


# direct methods
.method static bridge synthetic -$$Nest$fgetmIsVisible(Lcom/android/settings/widget/GroupOptionsPreference$TextViewInfo;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/widget/GroupOptionsPreference$TextViewInfo;->mIsVisible:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmText(Lcom/android/settings/widget/GroupOptionsPreference$TextViewInfo;)Ljava/lang/CharSequence;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/widget/GroupOptionsPreference$TextViewInfo;->mText:Ljava/lang/CharSequence;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmIsVisible(Lcom/android/settings/widget/GroupOptionsPreference$TextViewInfo;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/widget/GroupOptionsPreference$TextViewInfo;->mIsVisible:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmText(Lcom/android/settings/widget/GroupOptionsPreference$TextViewInfo;Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/GroupOptionsPreference$TextViewInfo;->mText:Ljava/lang/CharSequence;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmTextView(Lcom/android/settings/widget/GroupOptionsPreference$TextViewInfo;Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/GroupOptionsPreference$TextViewInfo;->mTextView:Landroid/widget/TextView;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/widget/GroupOptionsPreference$TextViewInfo;->mIsVisible:Z

    return-void
.end method

.method private shouldBeVisible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/widget/GroupOptionsPreference$TextViewInfo;->mIsVisible:Z

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/widget/GroupOptionsPreference$TextViewInfo;->mText:Ljava/lang/CharSequence;

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method


# virtual methods
.method setUpTextView()V
    .locals 3

    goto/32 :goto_4

    nop

    :goto_0
    iget-object v1, p0, Lcom/android/settings/widget/GroupOptionsPreference$TextViewInfo;->mText:Ljava/lang/CharSequence;

    goto/32 :goto_8

    nop

    :goto_1
    goto :goto_6

    :goto_2
    goto/32 :goto_11

    nop

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_10

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/settings/widget/GroupOptionsPreference$TextViewInfo;->mTextView:Landroid/widget/TextView;

    goto/32 :goto_0

    nop

    :goto_5
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_6
    goto/32 :goto_e

    nop

    :goto_7
    invoke-direct {p0}, Lcom/android/settings/widget/GroupOptionsPreference$TextViewInfo;->shouldBeVisible()Z

    move-result v0

    goto/32 :goto_3

    nop

    :goto_8
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_c

    nop

    :goto_9
    const/4 v1, 0x0

    goto/32 :goto_12

    nop

    :goto_a
    const/4 v0, 0x4

    goto/32 :goto_5

    nop

    :goto_b
    const/4 v0, 0x0

    goto/32 :goto_d

    nop

    :goto_c
    iget-object v0, p0, Lcom/android/settings/widget/GroupOptionsPreference$TextViewInfo;->mTextView:Landroid/widget/TextView;

    goto/32 :goto_9

    nop

    :goto_d
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto/32 :goto_1

    nop

    :goto_e
    return-void

    :goto_f
    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    goto/32 :goto_7

    nop

    :goto_10
    iget-object p0, p0, Lcom/android/settings/widget/GroupOptionsPreference$TextViewInfo;->mTextView:Landroid/widget/TextView;

    goto/32 :goto_b

    nop

    :goto_11
    iget-object p0, p0, Lcom/android/settings/widget/GroupOptionsPreference$TextViewInfo;->mTextView:Landroid/widget/TextView;

    goto/32 :goto_a

    nop

    :goto_12
    const/4 v2, 0x1

    goto/32 :goto_f

    nop
.end method
