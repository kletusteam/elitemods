.class Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/widget/GroupOptionsPreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ButtonInfo"
.end annotation


# instance fields
.field private mButton:Landroid/widget/Button;

.field private mIsEnabled:Z

.field private mIsVisible:Z

.field private mListener:Landroid/view/View$OnClickListener;

.field private mText:Ljava/lang/CharSequence;


# direct methods
.method static bridge synthetic -$$Nest$fgetmIsEnabled(Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;->mIsEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsVisible(Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;->mIsVisible:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmListener(Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;)Landroid/view/View$OnClickListener;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;->mListener:Landroid/view/View$OnClickListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmText(Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;)Ljava/lang/CharSequence;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;->mText:Ljava/lang/CharSequence;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmButton(Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;Landroid/widget/Button;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;->mButton:Landroid/widget/Button;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsEnabled(Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;->mIsEnabled:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsVisible(Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;->mIsVisible:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmListener(Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;Landroid/view/View$OnClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;->mListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmText(Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;->mText:Ljava/lang/CharSequence;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;->mIsEnabled:Z

    iput-boolean v0, p0, Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;->mIsVisible:Z

    return-void
.end method

.method private shouldBeVisible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;->mIsVisible:Z

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;->mText:Ljava/lang/CharSequence;

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method


# virtual methods
.method setUpButton()V
    .locals 3

    goto/32 :goto_9

    nop

    :goto_0
    invoke-virtual {v0, v1, v2}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;I)V

    goto/32 :goto_17

    nop

    :goto_1
    return-void

    :goto_2
    iget-object p0, p0, Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;->mButton:Landroid/widget/Button;

    goto/32 :goto_8

    nop

    :goto_3
    goto :goto_11

    :goto_4
    goto/32 :goto_2

    nop

    :goto_5
    iget-object v1, p0, Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;->mListener:Landroid/view/View$OnClickListener;

    goto/32 :goto_13

    nop

    :goto_6
    invoke-virtual {p0, v0}, Landroid/widget/Button;->setVisibility(I)V

    goto/32 :goto_3

    nop

    :goto_7
    const/4 v0, 0x0

    goto/32 :goto_6

    nop

    :goto_8
    const/16 v0, 0x8

    goto/32 :goto_10

    nop

    :goto_9
    iget-object v0, p0, Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;->mButton:Landroid/widget/Button;

    goto/32 :goto_c

    nop

    :goto_a
    iget-object p0, p0, Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;->mButton:Landroid/widget/Button;

    goto/32 :goto_7

    nop

    :goto_b
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_a

    nop

    :goto_c
    iget-object v1, p0, Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;->mText:Ljava/lang/CharSequence;

    goto/32 :goto_f

    nop

    :goto_d
    const/4 v1, 0x0

    goto/32 :goto_e

    nop

    :goto_e
    const/4 v2, 0x1

    goto/32 :goto_0

    nop

    :goto_f
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_12

    nop

    :goto_10
    invoke-virtual {p0, v0}, Landroid/widget/Button;->setVisibility(I)V

    :goto_11
    goto/32 :goto_1

    nop

    :goto_12
    iget-object v0, p0, Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;->mButton:Landroid/widget/Button;

    goto/32 :goto_5

    nop

    :goto_13
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/32 :goto_16

    nop

    :goto_14
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto/32 :goto_15

    nop

    :goto_15
    iget-object v0, p0, Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;->mButton:Landroid/widget/Button;

    goto/32 :goto_d

    nop

    :goto_16
    iget-object v0, p0, Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;->mButton:Landroid/widget/Button;

    goto/32 :goto_18

    nop

    :goto_17
    invoke-direct {p0}, Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;->shouldBeVisible()Z

    move-result v0

    goto/32 :goto_b

    nop

    :goto_18
    iget-boolean v1, p0, Lcom/android/settings/widget/GroupOptionsPreference$ButtonInfo;->mIsEnabled:Z

    goto/32 :goto_14

    nop
.end method
