.class Lcom/android/settings/widget/DarkModeAppsGuideView$2;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/widget/DarkModeAppsGuideView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/widget/DarkModeAppsGuideView;


# direct methods
.method constructor <init>(Lcom/android/settings/widget/DarkModeAppsGuideView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/DarkModeAppsGuideView$2;->this$0:Lcom/android/settings/widget/DarkModeAppsGuideView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0

    iget-object p1, p0, Lcom/android/settings/widget/DarkModeAppsGuideView$2;->this$0:Lcom/android/settings/widget/DarkModeAppsGuideView;

    iget-object p1, p1, Lcom/android/settings/widget/DarkModeAppsGuideView;->mBackgroundSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {p1, p2}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    iget-object p0, p0, Lcom/android/settings/widget/DarkModeAppsGuideView$2;->this$0:Lcom/android/settings/widget/DarkModeAppsGuideView;

    invoke-static {p0}, Lcom/android/settings/widget/DarkModeAppsGuideView;->-$$Nest$fgetmContext(Lcom/android/settings/widget/DarkModeAppsGuideView;)Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0, p2}, Lcom/android/settings/display/DarkModeTimeModeUtil;->setDarkModeContrastEnable(Landroid/content/Context;Z)V

    return-void
.end method
