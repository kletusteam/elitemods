.class public Lcom/android/settings/widget/SettingsStatusCard;
.super Lcom/android/settings/widget/BaseSettingsCard;


# instance fields
.field private mCardBackground:I

.field private mCardIcon:I

.field private mCardImageView:Landroid/widget/ImageView;

.field private mCardTitle:Ljava/lang/String;

.field private mCardTitleColor:I

.field private mCardTitleTextView:Lcom/android/settings/widget/CustomMarqueeTextView;

.field private mCardValue:Ljava/lang/String;

.field private mCardValueColor:I

.field private mCardValueTextView:Lcom/android/settings/widget/CustomMarqueeTextView;

.field private mIsChecked:Z

.field private mIsDisable:Z

.field private mRootLayout:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/widget/BaseSettingsCard;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/android/settings/widget/SettingsStatusCard;->initView()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/widget/BaseSettingsCard;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1, p2}, Lcom/android/settings/widget/SettingsStatusCard;->initTypedArray(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/android/settings/widget/SettingsStatusCard;->initView()V

    return-void
.end method

.method private initTypedArray(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    sget-object v0, Lcom/android/settings/R$styleable;->SettingsStatusCard:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    sget p2, Lcom/android/settings/R$styleable;->SettingsStatusCard_cardTitle:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardTitle:Ljava/lang/String;

    sget p2, Lcom/android/settings/R$styleable;->SettingsStatusCard_cardTitleColor:I

    iget-object v0, p0, Lcom/android/settings/widget/BaseSettingsCard;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/settings/R$color;->card_view_title_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p2

    iput p2, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardTitleColor:I

    sget p2, Lcom/android/settings/R$styleable;->SettingsStatusCard_cardValue:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardValue:Ljava/lang/String;

    sget p2, Lcom/android/settings/R$styleable;->SettingsStatusCard_cardValueColor:I

    iget-object v0, p0, Lcom/android/settings/widget/BaseSettingsCard;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/settings/R$color;->card_view_value_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p2

    iput p2, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardValueColor:I

    sget p2, Lcom/android/settings/R$styleable;->SettingsStatusCard_cardIcon:I

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p2

    iput p2, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardIcon:I

    sget p2, Lcom/android/settings/R$styleable;->SettingsStatusCard_cardBackground:I

    sget v0, Lcom/android/settings/R$drawable;->card_shape_corner:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p2

    iput p2, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardBackground:I

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private initView()V
    .locals 3

    sget v0, Lcom/android/settings/R$layout;->settings_status_card:I

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/BaseSettingsCard;->addLayout(I)V

    sget v0, Lcom/android/settings/R$id;->settings_card:I

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/settings/widget/SettingsStatusCard;->mRootLayout:Landroid/widget/LinearLayout;

    sget v0, Lcom/android/settings/R$id;->card_title:I

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/CustomMarqueeTextView;

    iput-object v0, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardTitleTextView:Lcom/android/settings/widget/CustomMarqueeTextView;

    sget v0, Lcom/android/settings/R$id;->card_value:I

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/CustomMarqueeTextView;

    iput-object v0, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardValueTextView:Lcom/android/settings/widget/CustomMarqueeTextView;

    sget v0, Lcom/android/settings/R$id;->card_icon:I

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardImageView:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/settings/widget/SettingsStatusCard;->mRootLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/android/settings/widget/BaseSettingsCard;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardBackground:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardTitleTextView:Lcom/android/settings/widget/CustomMarqueeTextView;

    iget-object v1, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardTitleTextView:Lcom/android/settings/widget/CustomMarqueeTextView;

    iget v1, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardTitleColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardValueTextView:Lcom/android/settings/widget/CustomMarqueeTextView;

    iget-object v1, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardValueTextView:Lcom/android/settings/widget/CustomMarqueeTextView;

    iget v1, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardValueColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardImageView:Landroid/widget/ImageView;

    iget p0, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardIcon:I

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method


# virtual methods
.method public getTitleTextView()Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardTitleTextView:Lcom/android/settings/widget/CustomMarqueeTextView;

    return-object p0
.end method

.method public getValueTextView()Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardValueTextView:Lcom/android/settings/widget/CustomMarqueeTextView;

    return-object p0
.end method

.method public setCardImageView(I)V
    .locals 0

    if-lez p1, :cond_0

    iget-object p0, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardImageView:Landroid/widget/ImageView;

    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    return-void
.end method

.method public setCardTitle(I)V
    .locals 1

    if-lez p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/BaseSettingsCard;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object p0, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardTitleTextView:Lcom/android/settings/widget/CustomMarqueeTextView;

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setCardTitle(Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardTitleTextView:Lcom/android/settings/widget/CustomMarqueeTextView;

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setCardValue(I)V
    .locals 1

    if-lez p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/BaseSettingsCard;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object p0, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardValueTextView:Lcom/android/settings/widget/CustomMarqueeTextView;

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setCardValue(Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardValueTextView:Lcom/android/settings/widget/CustomMarqueeTextView;

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setChecked(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/android/settings/widget/SettingsStatusCard;->mIsChecked:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/widget/SettingsStatusCard;->mRootLayout:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/settings/widget/BaseSettingsCard;->mContext:Landroid/content/Context;

    sget v1, Lcom/android/settings/R$drawable;->card_checked_corner:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardTitleTextView:Lcom/android/settings/widget/CustomMarqueeTextView;

    iget-object v0, p0, Lcom/android/settings/widget/BaseSettingsCard;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/settings/R$color;->card_view_title_checked_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object p1, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardValueTextView:Lcom/android/settings/widget/CustomMarqueeTextView;

    iget-object p0, p0, Lcom/android/settings/widget/BaseSettingsCard;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v0, Lcom/android/settings/R$color;->card_view_value_checked_color:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/android/settings/widget/SettingsStatusCard;->mRootLayout:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/settings/widget/BaseSettingsCard;->mContext:Landroid/content/Context;

    sget v1, Lcom/android/settings/R$drawable;->card_shape_corner:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardTitleTextView:Lcom/android/settings/widget/CustomMarqueeTextView;

    iget-object v0, p0, Lcom/android/settings/widget/BaseSettingsCard;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/settings/R$color;->card_view_title_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object p1, p0, Lcom/android/settings/widget/SettingsStatusCard;->mCardValueTextView:Lcom/android/settings/widget/CustomMarqueeTextView;

    iget-object p0, p0, Lcom/android/settings/widget/BaseSettingsCard;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v0, Lcom/android/settings/R$color;->card_view_value_color:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    return-void
.end method

.method public setDisable(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/widget/SettingsStatusCard;->mIsDisable:Z

    if-eqz p1, :cond_0

    iget-object p0, p0, Lcom/android/settings/widget/SettingsStatusCard;->mRootLayout:Landroid/widget/LinearLayout;

    const p1, 0x3e99999a    # 0.3f

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    goto :goto_0

    :cond_0
    iget-object p0, p0, Lcom/android/settings/widget/SettingsStatusCard;->mRootLayout:Landroid/widget/LinearLayout;

    const/high16 p1, 0x3f800000    # 1.0f

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    :goto_0
    return-void
.end method
