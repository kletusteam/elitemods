.class public Lcom/android/settings/widget/HomepagePreferenceLayoutHelper;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/widget/HomepagePreferenceLayoutHelper$HomepagePreferenceLayout;
    }
.end annotation


# instance fields
.field private mIcon:Landroid/view/View;

.field private mIconPaddingStart:I

.field private mIconVisible:Z

.field private mText:Landroid/view/View;

.field private mTextPaddingStart:I


# direct methods
.method public constructor <init>(Landroidx/preference/Preference;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/widget/HomepagePreferenceLayoutHelper;->mIconVisible:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/widget/HomepagePreferenceLayoutHelper;->mIconPaddingStart:I

    iput v0, p0, Lcom/android/settings/widget/HomepagePreferenceLayoutHelper;->mTextPaddingStart:I

    sget p0, Lcom/android/settings/R$layout;->homepage_preference:I

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setLayoutResource(I)V

    return-void
.end method


# virtual methods
.method onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    iget p1, p0, Lcom/android/settings/widget/HomepagePreferenceLayoutHelper;->mTextPaddingStart:I

    goto/32 :goto_8

    nop

    :goto_1
    iput-object p1, p0, Lcom/android/settings/widget/HomepagePreferenceLayoutHelper;->mText:Landroid/view/View;

    goto/32 :goto_c

    nop

    :goto_2
    sget v0, Lcom/android/settings/R$id;->icon_frame:I

    goto/32 :goto_6

    nop

    :goto_3
    iget p1, p0, Lcom/android/settings/widget/HomepagePreferenceLayoutHelper;->mIconPaddingStart:I

    goto/32 :goto_b

    nop

    :goto_4
    sget v0, Lcom/android/settings/R$id;->text_frame:I

    goto/32 :goto_a

    nop

    :goto_5
    return-void

    :goto_6
    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_7
    invoke-virtual {p0, p1}, Lcom/android/settings/widget/HomepagePreferenceLayoutHelper;->setIconVisible(Z)V

    goto/32 :goto_3

    nop

    :goto_8
    invoke-virtual {p0, p1}, Lcom/android/settings/widget/HomepagePreferenceLayoutHelper;->setTextPaddingStart(I)V

    goto/32 :goto_5

    nop

    :goto_9
    iput-object v0, p0, Lcom/android/settings/widget/HomepagePreferenceLayoutHelper;->mIcon:Landroid/view/View;

    goto/32 :goto_4

    nop

    :goto_a
    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_b
    invoke-virtual {p0, p1}, Lcom/android/settings/widget/HomepagePreferenceLayoutHelper;->setIconPaddingStart(I)V

    goto/32 :goto_0

    nop

    :goto_c
    iget-boolean p1, p0, Lcom/android/settings/widget/HomepagePreferenceLayoutHelper;->mIconVisible:Z

    goto/32 :goto_7

    nop
.end method

.method public setIconPaddingStart(I)V
    .locals 3

    iput p1, p0, Lcom/android/settings/widget/HomepagePreferenceLayoutHelper;->mIconPaddingStart:I

    iget-object v0, p0, Lcom/android/settings/widget/HomepagePreferenceLayoutHelper;->mIcon:Landroid/view/View;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/widget/HomepagePreferenceLayoutHelper;->mIcon:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingEnd()I

    move-result v2

    iget-object p0, p0, Lcom/android/settings/widget/HomepagePreferenceLayoutHelper;->mIcon:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result p0

    invoke-virtual {v0, p1, v1, v2, p0}, Landroid/view/View;->setPaddingRelative(IIII)V

    :cond_0
    return-void
.end method

.method public setIconVisible(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/widget/HomepagePreferenceLayoutHelper;->mIconVisible:Z

    iget-object p0, p0, Lcom/android/settings/widget/HomepagePreferenceLayoutHelper;->mIcon:Landroid/view/View;

    if-eqz p0, :cond_1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method public setTextPaddingStart(I)V
    .locals 3

    iput p1, p0, Lcom/android/settings/widget/HomepagePreferenceLayoutHelper;->mTextPaddingStart:I

    iget-object v0, p0, Lcom/android/settings/widget/HomepagePreferenceLayoutHelper;->mText:Landroid/view/View;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/widget/HomepagePreferenceLayoutHelper;->mText:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingEnd()I

    move-result v2

    iget-object p0, p0, Lcom/android/settings/widget/HomepagePreferenceLayoutHelper;->mText:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result p0

    invoke-virtual {v0, p1, v1, v2, p0}, Landroid/view/View;->setPaddingRelative(IIII)V

    :cond_0
    return-void
.end method
