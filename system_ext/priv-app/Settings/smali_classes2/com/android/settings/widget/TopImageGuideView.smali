.class public Lcom/android/settings/widget/TopImageGuideView;
.super Landroid/widget/RelativeLayout;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDarkModeEnable:Landroid/view/View;

.field private mDarkModeOuterView:Landroid/view/View;

.field private mLightModeEnable:Landroid/view/View;

.field private mLightModeOuterView:Landroid/view/View;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/settings/widget/TopImageGuideView;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/widget/TopImageGuideView;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDarkModeOuterView(Lcom/android/settings/widget/TopImageGuideView;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/widget/TopImageGuideView;->mDarkModeOuterView:Landroid/view/View;

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/widget/TopImageGuideView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/widget/TopImageGuideView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/widget/TopImageGuideView;->mContext:Landroid/content/Context;

    sget v0, Lcom/android/settings/R$id;->dark_mode_enable:I

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/widget/TopImageGuideView;->mDarkModeEnable:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->light_mode_enable:I

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/widget/TopImageGuideView;->mLightModeEnable:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->dark_mode_outer_view:I

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/widget/TopImageGuideView;->mDarkModeOuterView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->light_mode_outer_view:I

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/widget/TopImageGuideView;->mLightModeOuterView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/widget/TopImageGuideView;->mDarkModeEnable:Landroid/view/View;

    new-instance v1, Lcom/android/settings/widget/TopImageGuideView$1;

    invoke-direct {v1, p0}, Lcom/android/settings/widget/TopImageGuideView$1;-><init>(Lcom/android/settings/widget/TopImageGuideView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/widget/TopImageGuideView;->mLightModeEnable:Landroid/view/View;

    new-instance v1, Lcom/android/settings/widget/TopImageGuideView$2;

    invoke-direct {v1, p0}, Lcom/android/settings/widget/TopImageGuideView$2;-><init>(Lcom/android/settings/widget/TopImageGuideView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/widget/TopImageGuideView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/display/DarkModeTimeModeUtil;->isDarkModeEnable(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/widget/TopImageGuideView;->mLightModeOuterView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/settings/widget/TopImageGuideView;->mDarkModeOuterView:Landroid/view/View;

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v1, Lcom/android/settings/R$drawable;->light_dark_mode_outer:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/widget/TopImageGuideView;->mDarkModeOuterView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/settings/widget/TopImageGuideView;->mLightModeOuterView:Landroid/view/View;

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v1, Lcom/android/settings/R$drawable;->light_dark_mode_outer:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void
.end method
