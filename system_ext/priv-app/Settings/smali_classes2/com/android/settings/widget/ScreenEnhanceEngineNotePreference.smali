.class public Lcom/android/settings/widget/ScreenEnhanceEngineNotePreference;
.super Lcom/android/settingslib/miuisettings/preference/Preference;


# instance fields
.field private noteInfoString:Ljava/lang/String;

.field private noteInfoTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settingslib/miuisettings/preference/Preference;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/android/settings/widget/ScreenEnhanceEngineNotePreference;->noteInfoTextView:Landroid/widget/TextView;

    iput-object p1, p0, Lcom/android/settings/widget/ScreenEnhanceEngineNotePreference;->noteInfoString:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settingslib/miuisettings/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/android/settings/widget/ScreenEnhanceEngineNotePreference;->noteInfoTextView:Landroid/widget/TextView;

    iput-object p1, p0, Lcom/android/settings/widget/ScreenEnhanceEngineNotePreference;->noteInfoString:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settingslib/miuisettings/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/android/settings/widget/ScreenEnhanceEngineNotePreference;->noteInfoTextView:Landroid/widget/TextView;

    iput-object p1, p0, Lcom/android/settings/widget/ScreenEnhanceEngineNotePreference;->noteInfoString:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settingslib/miuisettings/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/android/settings/widget/ScreenEnhanceEngineNotePreference;->noteInfoTextView:Landroid/widget/TextView;

    iput-object p1, p0, Lcom/android/settings/widget/ScreenEnhanceEngineNotePreference;->noteInfoString:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onBindView(Landroid/view/View;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settingslib/miuisettings/preference/Preference;->onBindView(Landroid/view/View;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/android/settings/widget/ScreenEnhanceEngineNotePreference;->noteInfoTextView:Landroid/widget/TextView;

    iget-object p0, p0, Lcom/android/settings/widget/ScreenEnhanceEngineNotePreference;->noteInfoString:Ljava/lang/String;

    if-eqz p0, :cond_0

    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setNoteInfo(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/ScreenEnhanceEngineNotePreference;->noteInfoString:Ljava/lang/String;

    iget-object p0, p0, Lcom/android/settings/widget/ScreenEnhanceEngineNotePreference;->noteInfoTextView:Landroid/widget/TextView;

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
