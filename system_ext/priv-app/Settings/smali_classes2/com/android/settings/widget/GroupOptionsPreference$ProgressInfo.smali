.class Lcom/android/settings/widget/GroupOptionsPreference$ProgressInfo;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/widget/GroupOptionsPreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ProgressInfo"
.end annotation


# instance fields
.field private mIsVisible:Z

.field private mProgress:Landroid/widget/ProgressBar;


# direct methods
.method static bridge synthetic -$$Nest$fgetmIsVisible(Lcom/android/settings/widget/GroupOptionsPreference$ProgressInfo;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/widget/GroupOptionsPreference$ProgressInfo;->mIsVisible:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmIsVisible(Lcom/android/settings/widget/GroupOptionsPreference$ProgressInfo;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/widget/GroupOptionsPreference$ProgressInfo;->mIsVisible:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmProgress(Lcom/android/settings/widget/GroupOptionsPreference$ProgressInfo;Landroid/widget/ProgressBar;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/GroupOptionsPreference$ProgressInfo;->mProgress:Landroid/widget/ProgressBar;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/widget/GroupOptionsPreference$ProgressInfo;->mIsVisible:Z

    return-void
.end method

.method private shouldBeVisible()Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/widget/GroupOptionsPreference$ProgressInfo;->mIsVisible:Z

    return p0
.end method


# virtual methods
.method setUpProgress()V
    .locals 1

    goto/32 :goto_8

    nop

    :goto_0
    const/4 v0, 0x4

    goto/32 :goto_5

    nop

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {p0, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/32 :goto_3

    nop

    :goto_3
    goto :goto_6

    :goto_4
    goto/32 :goto_a

    nop

    :goto_5
    invoke-virtual {p0, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_6
    goto/32 :goto_b

    nop

    :goto_7
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_9

    nop

    :goto_8
    invoke-direct {p0}, Lcom/android/settings/widget/GroupOptionsPreference$ProgressInfo;->shouldBeVisible()Z

    move-result v0

    goto/32 :goto_7

    nop

    :goto_9
    iget-object p0, p0, Lcom/android/settings/widget/GroupOptionsPreference$ProgressInfo;->mProgress:Landroid/widget/ProgressBar;

    goto/32 :goto_1

    nop

    :goto_a
    iget-object p0, p0, Lcom/android/settings/widget/GroupOptionsPreference$ProgressInfo;->mProgress:Landroid/widget/ProgressBar;

    goto/32 :goto_0

    nop

    :goto_b
    return-void
.end method
