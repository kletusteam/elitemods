.class public Lcom/android/settings/widget/BottomLabelLayout;
.super Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private isStacked()Z
    .locals 1

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getOrientation()I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 5

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-direct {p0}, Lcom/android/settings/widget/BottomLabelLayout;->isStacked()Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    if-ne v3, v4, :cond_0

    const/high16 v3, -0x80000000

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    move v3, v2

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    move v3, v0

    move v0, p1

    :goto_0
    invoke-super {p0, v0, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getMeasuredWidthAndState()I

    move-result v0

    const/high16 v1, -0x1000000

    and-int/2addr v0, v1

    const/high16 v1, 0x1000000

    if-ne v0, v1, :cond_1

    invoke-virtual {p0, v2}, Lcom/android/settings/widget/BottomLabelLayout;->setStacked(Z)V

    goto :goto_1

    :cond_1
    move v2, v3

    :goto_1
    if-eqz v2, :cond_2

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    :cond_2
    return-void
.end method

.method setStacked(Z)V
    .locals 1

    goto/32 :goto_10

    nop

    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p0

    goto/32 :goto_11

    nop

    :goto_1
    goto :goto_9

    :goto_2
    goto/32 :goto_8

    nop

    :goto_3
    return-void

    :goto_4
    if-nez p1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_f

    nop

    :goto_5
    if-nez p1, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_e

    nop

    :goto_6
    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->setGravity(I)V

    goto/32 :goto_7

    nop

    :goto_7
    sget v0, Lcom/android/settingslib/R$id;->spacer:I

    goto/32 :goto_0

    nop

    :goto_8
    const/16 v0, 0x50

    :goto_9
    goto/32 :goto_6

    nop

    :goto_a
    goto :goto_13

    :goto_b
    goto/32 :goto_12

    nop

    :goto_c
    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    :goto_d
    goto/32 :goto_3

    nop

    :goto_e
    const/16 p1, 0x8

    goto/32 :goto_a

    nop

    :goto_f
    const v0, 0x800003

    goto/32 :goto_1

    nop

    :goto_10
    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    goto/32 :goto_4

    nop

    :goto_11
    if-nez p0, :cond_2

    goto/32 :goto_d

    :cond_2
    goto/32 :goto_5

    nop

    :goto_12
    const/4 p1, 0x0

    :goto_13
    goto/32 :goto_c

    nop
.end method
