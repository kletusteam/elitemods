.class public Lcom/android/settings/widget/DotsPageIndicator$RightwardStartPredicate;
.super Lcom/android/settings/widget/DotsPageIndicator$StartPredicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/widget/DotsPageIndicator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RightwardStartPredicate"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/widget/DotsPageIndicator;


# direct methods
.method public constructor <init>(Lcom/android/settings/widget/DotsPageIndicator;F)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/widget/DotsPageIndicator$RightwardStartPredicate;->this$0:Lcom/android/settings/widget/DotsPageIndicator;

    invoke-direct {p0, p1, p2}, Lcom/android/settings/widget/DotsPageIndicator$StartPredicate;-><init>(Lcom/android/settings/widget/DotsPageIndicator;F)V

    return-void
.end method


# virtual methods
.method shouldStart(F)Z
    .locals 0

    goto/32 :goto_7

    nop

    :goto_0
    const/4 p0, 0x1

    goto/32 :goto_4

    nop

    :goto_1
    const/4 p0, 0x0

    :goto_2
    goto/32 :goto_6

    nop

    :goto_3
    cmpl-float p0, p1, p0

    goto/32 :goto_8

    nop

    :goto_4
    goto :goto_2

    :goto_5
    goto/32 :goto_1

    nop

    :goto_6
    return p0

    :goto_7
    iget p0, p0, Lcom/android/settings/widget/DotsPageIndicator$StartPredicate;->thresholdValue:F

    goto/32 :goto_3

    nop

    :goto_8
    if-gtz p0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_0

    nop
.end method
