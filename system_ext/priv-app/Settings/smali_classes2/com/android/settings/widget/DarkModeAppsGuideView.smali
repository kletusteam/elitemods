.class public Lcom/android/settings/widget/DarkModeAppsGuideView;
.super Landroid/widget/LinearLayout;


# instance fields
.field private mBackgroundLayout:Landroid/widget/LinearLayout;

.field public mBackgroundSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

.field private mContext:Landroid/content/Context;

.field private mWallPaperLayout:Landroid/widget/LinearLayout;

.field public mWallPaperSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/settings/widget/DarkModeAppsGuideView;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/widget/DarkModeAppsGuideView;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/widget/DarkModeAppsGuideView;->mContext:Landroid/content/Context;

    sget v0, Lcom/android/settings/R$id;->sliding_button_wallpaper:I

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object v0, p0, Lcom/android/settings/widget/DarkModeAppsGuideView;->mWallPaperSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    sget v0, Lcom/android/settings/R$id;->sliding_button_background:I

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object v0, p0, Lcom/android/settings/widget/DarkModeAppsGuideView;->mBackgroundSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    sget v0, Lcom/android/settings/R$id;->wallpaper:I

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/settings/widget/DarkModeAppsGuideView;->mWallPaperLayout:Landroid/widget/LinearLayout;

    sget v0, Lcom/android/settings/R$id;->background:I

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/settings/widget/DarkModeAppsGuideView;->mBackgroundLayout:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/settings/widget/DarkModeAppsGuideView;->mWallPaperSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    iget-object v1, p0, Lcom/android/settings/widget/DarkModeAppsGuideView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/display/DarkModeTimeModeUtil;->isDarkWallpaperModeEnable(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/widget/DarkModeAppsGuideView;->mBackgroundSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    iget-object v1, p0, Lcom/android/settings/widget/DarkModeAppsGuideView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/display/DarkModeTimeModeUtil;->isDarkModeContrastEnable(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/widget/DarkModeAppsGuideView;->mWallPaperSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    new-instance v1, Lcom/android/settings/widget/DarkModeAppsGuideView$1;

    invoke-direct {v1, p0}, Lcom/android/settings/widget/DarkModeAppsGuideView$1;-><init>(Lcom/android/settings/widget/DarkModeAppsGuideView;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/widget/DarkModeAppsGuideView;->mBackgroundSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    new-instance v1, Lcom/android/settings/widget/DarkModeAppsGuideView$2;

    invoke-direct {v1, p0}, Lcom/android/settings/widget/DarkModeAppsGuideView$2;-><init>(Lcom/android/settings/widget/DarkModeAppsGuideView;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/widget/DarkModeAppsGuideView;->mWallPaperLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/android/settings/widget/DarkModeAppsGuideView$3;

    invoke-direct {v1, p0}, Lcom/android/settings/widget/DarkModeAppsGuideView$3;-><init>(Lcom/android/settings/widget/DarkModeAppsGuideView;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/widget/DarkModeAppsGuideView;->mBackgroundLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/android/settings/widget/DarkModeAppsGuideView$4;

    invoke-direct {v1, p0}, Lcom/android/settings/widget/DarkModeAppsGuideView$4;-><init>(Lcom/android/settings/widget/DarkModeAppsGuideView;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
