.class public Lcom/android/settings/vpn2/MiuiVpnEditFragment;
.super Lcom/android/settings/BaseEditFragment;

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field private mAddVpn:Z

.field private mAllowedTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mArgs:Landroid/os/Bundle;

.field private mChoice:Z

.field private mContext:Landroid/content/Context;

.field private mDnsServers:Lmiuix/androidbasewidget/widget/StateEditText;

.field private mEditing:Z

.field private mIpsecCaCert:Lcom/android/settings/vpn2/VpnSpinner;

.field private mIpsecIdentifier:Lmiuix/androidbasewidget/widget/StateEditText;

.field private mIpsecSecret:Lmiuix/androidbasewidget/widget/StateEditText;

.field private mIpsecServerCert:Lcom/android/settings/vpn2/VpnSpinner;

.field private mIpsecUserCert:Lcom/android/settings/vpn2/VpnSpinner;

.field private final mKeyStore:Landroid/security/KeyStore;

.field private mKeyboardHelper:Lcom/android/settings/utils/KeyboardHelper;

.field private mL2tpSecret:Lmiuix/androidbasewidget/widget/StateEditText;

.field private mMppe:Lcom/android/settings/vpn2/VpnCheckBox;

.field private mName:Lmiuix/androidbasewidget/widget/StateEditText;

.field private mOptions:Lcom/android/settings/vpn2/VpnCheckBox;

.field private mPassword:Lmiuix/androidbasewidget/widget/StateEditText;

.field private mProfile:Lcom/android/internal/net/VpnProfile;

.field private mRoutes:Lmiuix/androidbasewidget/widget/StateEditText;

.field private mSearchDomains:Lmiuix/androidbasewidget/widget/StateEditText;

.field private mServer:Lmiuix/androidbasewidget/widget/StateEditText;

.field private mTotalTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mType:Lcom/android/settings/vpn2/VpnSpinner;

.field private mUsername:Lmiuix/androidbasewidget/widget/StateEditText;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/settings/vpn2/MiuiVpnEditFragment;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmOptions(Lcom/android/settings/vpn2/MiuiVpnEditFragment;)Lcom/android/settings/vpn2/VpnCheckBox;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mOptions:Lcom/android/settings/vpn2/VpnCheckBox;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmChoice(Lcom/android/settings/vpn2/MiuiVpnEditFragment;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mChoice:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/BaseEditFragment;-><init>()V

    invoke-static {}, Landroid/security/KeyStore;->getInstance()Landroid/security/KeyStore;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mKeyStore:Landroid/security/KeyStore;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mAddVpn:Z

    return-void
.end method

.method private addAnim(Landroid/view/View;)V
    .locals 3

    const/4 p0, 0x1

    new-array p0, p0, [Landroid/view/View;

    const/4 v0, 0x0

    aput-object p1, p0, v0

    invoke-static {p0}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object p0

    invoke-interface {p0}, Lmiuix/animation/IFolme;->touch()Lmiuix/animation/ITouchStyle;

    move-result-object p0

    const v1, 0x3da3d70a    # 0.08f

    const/4 v2, 0x0

    invoke-interface {p0, v1, v2, v2, v2}, Lmiuix/animation/ITouchStyle;->setBackgroundColor(FFFF)Lmiuix/animation/ITouchStyle;

    move-result-object p0

    new-array v1, v0, [Lmiuix/animation/ITouchStyle$TouchType;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {p0, v2, v1}, Lmiuix/animation/ITouchStyle;->setScale(F[Lmiuix/animation/ITouchStyle$TouchType;)Lmiuix/animation/ITouchStyle;

    move-result-object p0

    new-array v0, v0, [Lmiuix/animation/base/AnimConfig;

    invoke-interface {p0, p1, v0}, Lmiuix/animation/ITouchStyle;->handleTouchOf(Landroid/view/View;[Lmiuix/animation/base/AnimConfig;)V

    return-void
.end method

.method private changeType(I)V
    .locals 10

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mMppe:Lcom/android/settings/vpn2/VpnCheckBox;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    sget v1, Lcom/android/settings/R$id;->l2tp:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    sget v3, Lcom/android/settings/R$id;->ipsec_psk:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    sget v4, Lcom/android/settings/R$id;->ipsec_user_cert:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/view/View;->setVisibility(I)V

    sget v5, Lcom/android/settings/R$id;->ipsec_identifier_bg:I

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/view/View;->setVisibility(I)V

    sget v6, Lcom/android/settings/R$id;->ipsec_secret_bg:I

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/view/View;->setVisibility(I)V

    sget v7, Lcom/android/settings/R$id;->ipsec_ca_cert:I

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/view/View;->setVisibility(I)V

    sget v8, Lcom/android/settings/R$id;->ipsec_server_cert:I

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, p1}, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->setUsernamePasswordVisibility(I)V

    invoke-static {p1}, Lcom/android/internal/net/VpnProfile;->isLegacyType(I)Z

    move-result v2

    const/4 v9, 0x0

    if-nez v2, :cond_0

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    invoke-virtual {p0, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    invoke-virtual {p0, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    invoke-virtual {p0, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    invoke-virtual {p0, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    invoke-virtual {p0, v9}, Landroid/view/View;->setVisibility(I)V

    :pswitch_2
    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    invoke-virtual {p0, v9}, Landroid/view/View;->setVisibility(I)V

    :pswitch_3
    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    invoke-virtual {p0, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    invoke-virtual {p0, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    invoke-virtual {p0, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    invoke-virtual {p0, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    invoke-virtual {p0, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    invoke-virtual {p0, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_5
    iget-object p0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mMppe:Lcom/android/settings/vpn2/VpnCheckBox;

    invoke-virtual {p0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private convertAllowedIndexToProfileType(I)I
    .locals 2

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mAllowedTypes:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mTotalTypes:Ljava/util/List;

    if-eqz v1, :cond_0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iget-object p0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mTotalTypes:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p0

    return p0

    :cond_0
    const-string p0, "MiuiVpnEditFragment"

    const-string v0, "Allowed or Total vpn types not initialized when converting protileType"

    invoke-static {p0, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return p1
.end method

.method private getSelectedVpnType()I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mType:Lcom/android/settings/vpn2/VpnSpinner;

    invoke-virtual {v0}, Lcom/android/settings/vpn2/VpnSpinner;->getSelectedItemPosition()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->convertAllowedIndexToProfileType(I)I

    move-result p0

    return p0
.end method

.method private loadCertificates(Lcom/android/settings/vpn2/VpnSpinner;Ljava/util/Collection;ILjava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/settings/vpn2/VpnSpinner;",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez p3, :cond_0

    const-string p3, ""

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p3

    :goto_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v2

    if-nez v2, :cond_1

    goto :goto_2

    :cond_1
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v2

    add-int/2addr v2, v1

    new-array v2, v2, [Ljava/lang/String;

    aput-object p3, v2, v0

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    move p3, v1

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    add-int/lit8 v3, p3, 0x1

    aput-object v0, v2, p3

    move p3, v3

    goto :goto_1

    :cond_2
    :goto_2
    new-array v2, v1, [Ljava/lang/String;

    aput-object p3, v2, v0

    :cond_3
    new-instance p2, Landroid/widget/ArrayAdapter;

    iget-object p0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mContext:Landroid/content/Context;

    sget p3, Lcom/android/settings/R$layout;->miuix_appcompat_simple_spinner_layout_integrated:I

    const v0, 0x1020014

    invoke-direct {p2, p0, p3, v0, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    sget p0, Lcom/android/settings/R$layout;->miuix_appcompat_simple_spinner_dropdown_item:I

    invoke-virtual {p2, p0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {p1}, Lcom/android/settings/vpn2/VpnSpinner;->getSpinner()Lmiuix/appcompat/widget/Spinner;

    move-result-object p0

    invoke-virtual {p0, p2}, Lmiuix/appcompat/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-virtual {p1}, Lcom/android/settings/vpn2/VpnSpinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-virtual {p1, p0}, Lcom/android/settings/vpn2/VpnSpinner;->setPrompt(Ljava/lang/CharSequence;)V

    :goto_3
    array-length p0, v2

    if-ge v1, p0, :cond_5

    aget-object p0, v2, v1

    invoke-virtual {p0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    invoke-virtual {p1, v1}, Lcom/android/settings/vpn2/VpnSpinner;->setSelection(I)V

    goto :goto_4

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_5
    :goto_4
    return-void
.end method

.method private requiresUsernamePassword(I)Z
    .locals 0

    const/4 p0, 0x7

    if-eq p1, p0, :cond_0

    const/16 p0, 0x8

    if-eq p1, p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method private setTypesByFeature(Landroid/widget/Spinner;)V
    .locals 4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/settings/R$array;->vpn_types:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mTotalTypes:Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mAllowedTypes:Ljava/util/List;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "android.software.ipsec_tunnels"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "MiuiVpnEditFragment"

    const-string v2, "FEATURE_IPSEC_TUNNELS missing from system"

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mAddVpn:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    const/4 v2, 0x6

    iput v2, v1, Lcom/android/internal/net/VpnProfile;->type:I

    :cond_1
    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    iget v1, v1, Lcom/android/internal/net/VpnProfile;->type:I

    invoke-static {v1}, Lcom/android/internal/net/VpnProfile;->isLegacyType(I)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mAllowedTypes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_3

    invoke-static {v0}, Lcom/android/internal/net/VpnProfile;->isLegacyType(I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mAllowedTypes:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mAllowedTypes:Ljava/util/List;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    :cond_4
    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object p0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mContext:Landroid/content/Context;

    sget v2, Lcom/android/settings/R$layout;->miuix_appcompat_simple_spinner_layout_integrated:I

    const v3, 0x1020014

    invoke-direct {v1, p0, v2, v3, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    sget p0, Lcom/android/settings/R$layout;->miuix_appcompat_simple_spinner_dropdown_item:I

    invoke-virtual {v1, p0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {p1, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    return-void
.end method

.method private setUsernamePasswordVisibility(I)V
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/android/settings/R$id;->login:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->requiresUsernamePassword(I)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_0
    const/16 p0, 0x8

    :goto_0
    invoke-virtual {v0, p0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private validate(Z)Z
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->getSelectedVpnType()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_1

    invoke-direct {p0, v0}, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->requiresUsernamePassword(I)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mUsername:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {p1}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result p1

    if-eqz p1, :cond_0

    iget-object p0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mPassword:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {p0}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object p0

    invoke-interface {p0}, Landroid/text/Editable;->length()I

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    return v1

    :cond_1
    iget-object p1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mName:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {p1}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result p1

    if-eqz p1, :cond_6

    iget-object p1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mServer:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {p1}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result p1

    if-eqz p1, :cond_6

    iget-object p1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mDnsServers:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {p1}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1, v2}, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->validateAddresses(Ljava/lang/String;Z)Z

    move-result p1

    if-eqz p1, :cond_6

    iget-object p1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mRoutes:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {p1}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1, v1}, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->validateAddresses(Ljava/lang/String;Z)Z

    move-result p1

    if-nez p1, :cond_2

    goto :goto_3

    :cond_2
    iget-object p1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    iget p1, p1, Lcom/android/internal/net/VpnProfile;->type:I

    invoke-static {p1}, Lcom/android/internal/net/VpnProfile;->isLegacyType(I)Z

    move-result p1

    if-nez p1, :cond_3

    iget-object p1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecIdentifier:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {p1}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result p1

    if-nez p1, :cond_3

    return v2

    :cond_3
    packed-switch v0, :pswitch_data_0

    return v2

    :pswitch_0
    iget-object p0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecUserCert:Lcom/android/settings/vpn2/VpnSpinner;

    invoke-virtual {p0}, Lcom/android/settings/vpn2/VpnSpinner;->getSelectedItemPosition()I

    move-result p0

    if-eqz p0, :cond_4

    goto :goto_1

    :cond_4
    move v1, v2

    :goto_1
    return v1

    :pswitch_1
    iget-object p0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecSecret:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {p0}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object p0

    invoke-interface {p0}, Landroid/text/Editable;->length()I

    move-result p0

    if-eqz p0, :cond_5

    goto :goto_2

    :cond_5
    move v1, v2

    :goto_2
    :pswitch_2
    return v1

    :cond_6
    :goto_3
    return v2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private validateAddresses(Ljava/lang/String;Z)Z
    .locals 9

    const/4 p0, 0x0

    :try_start_0
    const-string v0, " "

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    array-length v0, p1

    move v1, p0

    :goto_0
    const/4 v2, 0x1

    if-ge v1, v0, :cond_4

    aget-object v3, p1, v1

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_2

    :cond_0
    const/4 v4, 0x2

    const/16 v5, 0x20

    if-eqz p2, :cond_1

    const-string v6, "/"

    invoke-virtual {v3, v6, v4}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v3

    aget-object v6, v3, p0

    aget-object v3, v3, v2

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    move-object v8, v6

    move v6, v3

    move-object v3, v8

    goto :goto_1

    :cond_1
    move v6, v5

    :goto_1
    invoke-static {v3}, Ljava/net/InetAddress;->parseNumericAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v3

    const/4 v7, 0x3

    aget-byte v7, v3, v7

    and-int/lit16 v7, v7, 0xff

    aget-byte v4, v3, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x8

    or-int/2addr v4, v7

    aget-byte v2, v3, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v2, v4

    aget-byte v4, v3, p0

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x18

    or-int/2addr v2, v4

    array-length v3, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v4, 0x4

    if-ne v3, v4, :cond_3

    if-ltz v6, :cond_3

    if-gt v6, v5, :cond_3

    if-ge v6, v5, :cond_2

    shl-int/2addr v2, v6

    if-eqz v2, :cond_2

    goto :goto_3

    :cond_2
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    :goto_3
    return p0

    :cond_4
    return v2

    :catch_0
    return p0
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    iget-boolean p1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mEditing:Z

    invoke-direct {p0, p1}, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->validate(Z)Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/android/settings/BaseEditFragment;->onEditStateChange(Z)V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public doInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p3

    if-eqz p3, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p3

    iput-object p3, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mContext:Landroid/content/Context;

    :cond_0
    sget p0, Lcom/android/settings/R$layout;->vpn_edit_layout:I

    const/4 p3, 0x0

    invoke-virtual {p1, p0, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method getProfile()Lcom/android/internal/net/VpnProfile;
    .locals 2

    goto/32 :goto_45

    nop

    :goto_0
    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mDnsServers:Lmiuix/androidbasewidget/widget/StateEditText;

    goto/32 :goto_55

    nop

    :goto_1
    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mRoutes:Lmiuix/androidbasewidget/widget/StateEditText;

    goto/32 :goto_10

    nop

    :goto_2
    iput v1, v0, Lcom/android/internal/net/VpnProfile;->type:I

    goto/32 :goto_13

    nop

    :goto_3
    iput-object v1, v0, Lcom/android/internal/net/VpnProfile;->username:Ljava/lang/String;

    goto/32 :goto_4b

    nop

    :goto_4
    invoke-virtual {v1}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    goto/32 :goto_e

    nop

    :goto_5
    invoke-virtual {p0}, Lcom/android/settings/vpn2/VpnCheckBox;->isChecked()Z

    move-result p0

    goto/32 :goto_16

    nop

    :goto_6
    iput-object v1, v0, Lcom/android/internal/net/VpnProfile;->searchDomains:Ljava/lang/String;

    goto/32 :goto_0

    nop

    :goto_7
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch

    :goto_8
    invoke-virtual {v1}, Lcom/android/settings/vpn2/VpnSpinner;->getSelectedItemPosition()I

    move-result v1

    goto/32 :goto_28

    nop

    :goto_9
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_2b

    nop

    :goto_a
    if-nez v1, :cond_0

    goto/32 :goto_17

    :cond_0
    goto/32 :goto_2e

    nop

    :goto_b
    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecServerCert:Lcom/android/settings/vpn2/VpnSpinner;

    goto/32 :goto_43

    nop

    :goto_c
    iput-object p0, v0, Lcom/android/internal/net/VpnProfile;->ipsecServerCert:Ljava/lang/String;

    goto/32 :goto_57

    nop

    :goto_d
    iput-object v1, v0, Lcom/android/internal/net/VpnProfile;->l2tpSecret:Ljava/lang/String;

    :pswitch_0
    goto/32 :goto_5a

    nop

    :goto_e
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_d

    nop

    :goto_f
    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecIdentifier:Lmiuix/androidbasewidget/widget/StateEditText;

    goto/32 :goto_2f

    nop

    :goto_10
    invoke-virtual {v1}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    goto/32 :goto_53

    nop

    :goto_11
    if-nez v1, :cond_1

    goto/32 :goto_24

    :cond_1
    goto/32 :goto_1f

    nop

    :goto_12
    iget v1, v0, Lcom/android/internal/net/VpnProfile;->type:I

    goto/32 :goto_5e

    nop

    :goto_13
    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mServer:Lmiuix/androidbasewidget/widget/StateEditText;

    goto/32 :goto_2a

    nop

    :goto_14
    iget-object v1, v1, Lcom/android/internal/net/VpnProfile;->key:Ljava/lang/String;

    goto/32 :goto_18

    nop

    :goto_15
    invoke-virtual {v1}, Lcom/android/settings/vpn2/VpnSpinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_42

    nop

    :goto_16
    iput-boolean p0, v0, Lcom/android/internal/net/VpnProfile;->mppe:Z

    :goto_17
    goto/32 :goto_26

    nop

    :goto_18
    invoke-direct {v0, v1}, Lcom/android/internal/net/VpnProfile;-><init>(Ljava/lang/String;)V

    goto/32 :goto_1e

    nop

    :goto_19
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_37

    nop

    :goto_1a
    iget-object p0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecSecret:Lmiuix/androidbasewidget/widget/StateEditText;

    goto/32 :goto_49

    nop

    :goto_1b
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_31

    nop

    :goto_1c
    check-cast p0, Ljava/lang/String;

    goto/32 :goto_c

    nop

    :goto_1d
    invoke-virtual {v1}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    goto/32 :goto_4d

    nop

    :goto_1e
    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mName:Lmiuix/androidbasewidget/widget/StateEditText;

    goto/32 :goto_46

    nop

    :goto_1f
    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecUserCert:Lcom/android/settings/vpn2/VpnSpinner;

    goto/32 :goto_51

    nop

    :goto_20
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_21
    goto :goto_17

    :pswitch_1
    goto/32 :goto_2c

    nop

    :goto_22
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_61

    nop

    :goto_23
    iput-object v1, v0, Lcom/android/internal/net/VpnProfile;->ipsecUserCert:Ljava/lang/String;

    :goto_24
    :pswitch_2
    goto/32 :goto_58

    nop

    :goto_25
    invoke-virtual {v1}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    goto/32 :goto_5b

    nop

    :goto_26
    const/4 p0, 0x1

    goto/32 :goto_54

    nop

    :goto_27
    iput-object v1, v0, Lcom/android/internal/net/VpnProfile;->password:Ljava/lang/String;

    goto/32 :goto_12

    nop

    :goto_28
    if-nez v1, :cond_2

    goto/32 :goto_3e

    :cond_2
    goto/32 :goto_40

    nop

    :goto_29
    invoke-virtual {v1}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    goto/32 :goto_39

    nop

    :goto_2a
    invoke-virtual {v1}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    goto/32 :goto_44

    nop

    :goto_2b
    iput-object v1, v0, Lcom/android/internal/net/VpnProfile;->dnsServers:Ljava/lang/String;

    goto/32 :goto_1

    nop

    :goto_2c
    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mL2tpSecret:Lmiuix/androidbasewidget/widget/StateEditText;

    goto/32 :goto_4

    nop

    :goto_2d
    invoke-virtual {p0}, Lcom/android/settings/vpn2/VpnSpinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object p0

    goto/32 :goto_1c

    nop

    :goto_2e
    iget-object p0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecServerCert:Lcom/android/settings/vpn2/VpnSpinner;

    goto/32 :goto_2d

    nop

    :goto_2f
    invoke-virtual {v1}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    goto/32 :goto_3b

    nop

    :goto_30
    iget v1, v0, Lcom/android/internal/net/VpnProfile;->type:I

    packed-switch v1, :pswitch_data_0

    goto/32 :goto_21

    nop

    :goto_31
    iput-object v1, v0, Lcom/android/internal/net/VpnProfile;->name:Ljava/lang/String;

    goto/32 :goto_56

    nop

    :goto_32
    iput-object v1, v0, Lcom/android/internal/net/VpnProfile;->ipsecIdentifier:Ljava/lang/String;

    :goto_33
    goto/32 :goto_30

    nop

    :goto_34
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_5d

    nop

    :goto_35
    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecIdentifier:Lmiuix/androidbasewidget/widget/StateEditText;

    goto/32 :goto_36

    nop

    :goto_36
    invoke-virtual {v1}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    goto/32 :goto_4a

    nop

    :goto_37
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_38
    check-cast v1, Ljava/lang/String;

    goto/32 :goto_23

    nop

    :goto_39
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_27

    nop

    :goto_3a
    iput-object v1, v0, Lcom/android/internal/net/VpnProfile;->ipsecIdentifier:Ljava/lang/String;

    goto/32 :goto_1a

    nop

    :goto_3b
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_32

    nop

    :goto_3c
    iput-object v1, v0, Lcom/android/internal/net/VpnProfile;->l2tpSecret:Ljava/lang/String;

    :pswitch_3
    goto/32 :goto_35

    nop

    :goto_3d
    iput-object v1, v0, Lcom/android/internal/net/VpnProfile;->ipsecCaCert:Ljava/lang/String;

    :goto_3e
    goto/32 :goto_b

    nop

    :goto_3f
    if-nez v1, :cond_3

    goto/32 :goto_60

    :cond_3
    goto/32 :goto_50

    nop

    :goto_40
    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecCaCert:Lcom/android/settings/vpn2/VpnSpinner;

    goto/32 :goto_15

    nop

    :goto_41
    invoke-virtual {v1}, Lcom/android/settings/vpn2/VpnSpinner;->getSelectedItemPosition()I

    move-result v1

    goto/32 :goto_11

    nop

    :goto_42
    check-cast v1, Ljava/lang/String;

    goto/32 :goto_3d

    nop

    :goto_43
    invoke-virtual {v1}, Lcom/android/settings/vpn2/VpnSpinner;->getSelectedItemPosition()I

    move-result v1

    goto/32 :goto_a

    nop

    :goto_44
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_48

    nop

    :goto_45
    new-instance v0, Lcom/android/internal/net/VpnProfile;

    goto/32 :goto_52

    nop

    :goto_46
    invoke-virtual {v1}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    goto/32 :goto_1b

    nop

    :goto_47
    iput-object v1, v0, Lcom/android/internal/net/VpnProfile;->server:Ljava/lang/String;

    goto/32 :goto_4f

    nop

    :goto_48
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_47

    nop

    :goto_49
    invoke-virtual {p0}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object p0

    goto/32 :goto_22

    nop

    :goto_4a
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_3a

    nop

    :goto_4b
    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mPassword:Lmiuix/androidbasewidget/widget/StateEditText;

    goto/32 :goto_29

    nop

    :goto_4c
    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mL2tpSecret:Lmiuix/androidbasewidget/widget/StateEditText;

    goto/32 :goto_25

    nop

    :goto_4d
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_3

    nop

    :goto_4e
    goto/16 :goto_17

    :pswitch_4
    goto/32 :goto_59

    nop

    :goto_4f
    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mUsername:Lmiuix/androidbasewidget/widget/StateEditText;

    goto/32 :goto_1d

    nop

    :goto_50
    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mSearchDomains:Lmiuix/androidbasewidget/widget/StateEditText;

    goto/32 :goto_5c

    nop

    :goto_51
    invoke-virtual {v1}, Lcom/android/settings/vpn2/VpnSpinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_38

    nop

    :goto_52
    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    goto/32 :goto_14

    nop

    :goto_53
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_34

    nop

    :goto_54
    iput-boolean p0, v0, Lcom/android/internal/net/VpnProfile;->saveLogin:Z

    goto/32 :goto_7

    nop

    :goto_55
    invoke-virtual {v1}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    goto/32 :goto_20

    nop

    :goto_56
    invoke-direct {p0}, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->getSelectedVpnType()I

    move-result v1

    goto/32 :goto_2

    nop

    :goto_57
    goto/16 :goto_17

    :pswitch_5
    goto/32 :goto_4c

    nop

    :goto_58
    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecCaCert:Lcom/android/settings/vpn2/VpnSpinner;

    goto/32 :goto_8

    nop

    :goto_59
    iget-object p0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mMppe:Lcom/android/settings/vpn2/VpnCheckBox;

    goto/32 :goto_5

    nop

    :goto_5a
    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecUserCert:Lcom/android/settings/vpn2/VpnSpinner;

    goto/32 :goto_41

    nop

    :goto_5b
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_3c

    nop

    :goto_5c
    invoke-virtual {v1}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    goto/32 :goto_19

    nop

    :goto_5d
    iput-object v1, v0, Lcom/android/internal/net/VpnProfile;->routes:Ljava/lang/String;

    goto/32 :goto_5f

    nop

    :goto_5e
    invoke-static {v1}, Lcom/android/internal/net/VpnProfile;->isLegacyType(I)Z

    move-result v1

    goto/32 :goto_3f

    nop

    :goto_5f
    goto/16 :goto_33

    :goto_60
    goto/32 :goto_f

    nop

    :goto_61
    iput-object p0, v0, Lcom/android/internal/net/VpnProfile;->ipsecSecret:Ljava/lang/String;

    goto/32 :goto_4e

    nop
.end method

.method public getTitle()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mArgs:Landroid/os/Bundle;

    const-string/jumbo v1, "profile_add"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/android/settings/R$string;->vpn_create:I

    goto :goto_0

    :cond_0
    sget v0, Lcom/android/settings/R$string;->vpn_edit:I

    :goto_0
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string/jumbo v0, "show_options_flag"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mChoice:Z

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mArgs:Landroid/os/Bundle;

    const-string/jumbo v0, "profile_add"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mAddVpn:Z

    iget-object p1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mArgs:Landroid/os/Bundle;

    const-string/jumbo v0, "profile"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mArgs:Landroid/os/Bundle;

    const-string/jumbo v2, "profile_key"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    return-void

    :cond_1
    if-eqz p1, :cond_3

    array-length v2, p1

    if-nez v2, :cond_2

    goto :goto_0

    :cond_2
    invoke-static {v0, p1}, Lcom/android/internal/net/VpnProfile;->decode(Ljava/lang/String;[B)Lcom/android/internal/net/VpnProfile;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    goto :goto_1

    :cond_3
    :goto_0
    new-instance p1, Lcom/android/internal/net/VpnProfile;

    invoke-direct {p1, v0}, Lcom/android/internal/net/VpnProfile;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    :goto_1
    iput-boolean v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mEditing:Z

    return-void
.end method

.method public onDestroyView()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/BaseEditFragment;->onDestroyView()V

    iget-object p0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mKeyboardHelper:Lcom/android/settings/utils/KeyboardHelper;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/utils/KeyboardHelper;->destroy()V

    :cond_0
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mType:Lcom/android/settings/vpn2/VpnSpinner;

    invoke-virtual {p2}, Lcom/android/settings/vpn2/VpnSpinner;->getSpinner()Lmiuix/appcompat/widget/Spinner;

    move-result-object p2

    if-ne p1, p2, :cond_0

    invoke-direct {p0, p3}, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->convertAllowedIndexToProfileType(I)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->changeType(I)V

    :cond_0
    iget-boolean p1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mEditing:Z

    invoke-direct {p0, p1}, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->validate(Z)Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/android/settings/BaseEditFragment;->onEditStateChange(Z)V

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;)V"
        }
    .end annotation

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onResume()V

    iget-boolean v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mEditing:Z

    invoke-direct {p0, v0}, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->validate(Z)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/settings/BaseEditFragment;->onEditStateChange(Z)V

    return-void
.end method

.method public onSave(Z)V
    .locals 4

    invoke-virtual {p0}, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->getProfile()Lcom/android/internal/net/VpnProfile;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0}, Lcom/android/internal/net/VpnProfile;->encode()[B

    move-result-object v2

    const-string/jumbo v3, "profile"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    iget-object v0, v0, Lcom/android/internal/net/VpnProfile;->key:Ljava/lang/String;

    const-string/jumbo v2, "profile_key"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "profile_delete"

    invoke-virtual {v1, v0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {p0, v1}, Lcom/android/settings/BaseEditFragment;->onSave(Landroid/os/Bundle;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-boolean p0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mChoice:Z

    const-string/jumbo v0, "show_options_flag"

    invoke-virtual {p1, v0, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    invoke-super {p0, p1, p2}, Lcom/android/settings/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mArgs:Landroid/os/Bundle;

    const-string/jumbo v0, "profile_key"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/BaseFragment;->finish()V

    return-void

    :cond_0
    sget p2, Lcom/android/settings/R$id;->name:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lmiuix/androidbasewidget/widget/StateEditText;

    iput-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mName:Lmiuix/androidbasewidget/widget/StateEditText;

    sget p2, Lcom/android/settings/R$id;->server:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lmiuix/androidbasewidget/widget/StateEditText;

    iput-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mServer:Lmiuix/androidbasewidget/widget/StateEditText;

    sget p2, Lcom/android/settings/R$id;->username:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lmiuix/androidbasewidget/widget/StateEditText;

    iput-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mUsername:Lmiuix/androidbasewidget/widget/StateEditText;

    sget p2, Lcom/android/settings/R$id;->password:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lmiuix/androidbasewidget/widget/StateEditText;

    iput-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mPassword:Lmiuix/androidbasewidget/widget/StateEditText;

    sget p2, Lcom/android/settings/R$id;->search_domains:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lmiuix/androidbasewidget/widget/StateEditText;

    iput-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mSearchDomains:Lmiuix/androidbasewidget/widget/StateEditText;

    sget p2, Lcom/android/settings/R$id;->dns_servers:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lmiuix/androidbasewidget/widget/StateEditText;

    iput-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mDnsServers:Lmiuix/androidbasewidget/widget/StateEditText;

    sget p2, Lcom/android/settings/R$id;->routes:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lmiuix/androidbasewidget/widget/StateEditText;

    iput-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mRoutes:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    iput-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mContext:Landroid/content/Context;

    sget p2, Lcom/android/settings/R$id;->show_options:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/android/settings/vpn2/VpnCheckBox;

    iput-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mOptions:Lcom/android/settings/vpn2/VpnCheckBox;

    sget p2, Lcom/android/settings/R$id;->mppe:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/android/settings/vpn2/VpnCheckBox;

    iput-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mMppe:Lcom/android/settings/vpn2/VpnCheckBox;

    sget p2, Lcom/android/settings/R$id;->type:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/android/settings/vpn2/VpnSpinner;

    iput-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mType:Lcom/android/settings/vpn2/VpnSpinner;

    sget p2, Lcom/android/settings/R$id;->l2tp_secret:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lmiuix/androidbasewidget/widget/StateEditText;

    iput-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mL2tpSecret:Lmiuix/androidbasewidget/widget/StateEditText;

    sget p2, Lcom/android/settings/R$id;->ipsec_identifier:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lmiuix/androidbasewidget/widget/StateEditText;

    iput-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecIdentifier:Lmiuix/androidbasewidget/widget/StateEditText;

    sget p2, Lcom/android/settings/R$id;->ipsec_secret:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lmiuix/androidbasewidget/widget/StateEditText;

    iput-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecSecret:Lmiuix/androidbasewidget/widget/StateEditText;

    sget p2, Lcom/android/settings/R$id;->ipsec_user_cert:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/android/settings/vpn2/VpnSpinner;

    iput-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecUserCert:Lcom/android/settings/vpn2/VpnSpinner;

    sget p2, Lcom/android/settings/R$id;->ipsec_ca_cert:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/android/settings/vpn2/VpnSpinner;

    iput-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecCaCert:Lcom/android/settings/vpn2/VpnSpinner;

    sget p2, Lcom/android/settings/R$id;->ipsec_server_cert:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/android/settings/vpn2/VpnSpinner;

    iput-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecServerCert:Lcom/android/settings/vpn2/VpnSpinner;

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mServer:Lmiuix/androidbasewidget/widget/StateEditText;

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/settings/R$string;->proxy_hostname_hint:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    new-instance p2, Lcom/android/settings/utils/AndroidKeystoreAliasLoader;

    const/4 v0, 0x0

    invoke-direct {p2, v0}, Lcom/android/settings/utils/AndroidKeystoreAliasLoader;-><init>(Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecUserCert:Lcom/android/settings/vpn2/VpnSpinner;

    invoke-virtual {p2}, Lcom/android/settings/utils/AndroidKeystoreAliasLoader;->getKeyCertAliases()Ljava/util/Collection;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    iget-object v2, v2, Lcom/android/internal/net/VpnProfile;->ipsecUserCert:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v3, v2}, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->loadCertificates(Lcom/android/settings/vpn2/VpnSpinner;Ljava/util/Collection;ILjava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecCaCert:Lcom/android/settings/vpn2/VpnSpinner;

    invoke-virtual {p2}, Lcom/android/settings/utils/AndroidKeystoreAliasLoader;->getCaCertAliases()Ljava/util/Collection;

    move-result-object v1

    sget v2, Lcom/android/settings/R$string;->vpn_no_ca_cert:I

    iget-object v4, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    iget-object v4, v4, Lcom/android/internal/net/VpnProfile;->ipsecCaCert:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v4}, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->loadCertificates(Lcom/android/settings/vpn2/VpnSpinner;Ljava/util/Collection;ILjava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecServerCert:Lcom/android/settings/vpn2/VpnSpinner;

    invoke-virtual {p2}, Lcom/android/settings/utils/AndroidKeystoreAliasLoader;->getKeyCertAliases()Ljava/util/Collection;

    move-result-object p2

    sget v1, Lcom/android/settings/R$string;->vpn_no_server_cert:I

    iget-object v2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    iget-object v2, v2, Lcom/android/internal/net/VpnProfile;->ipsecServerCert:Ljava/lang/String;

    invoke-direct {p0, v0, p2, v1, v2}, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->loadCertificates(Lcom/android/settings/vpn2/VpnSpinner;Ljava/util/Collection;ILjava/lang/String;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mName:Lmiuix/androidbasewidget/widget/StateEditText;

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    iget-object v0, v0, Lcom/android/internal/net/VpnProfile;->name:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mType:Lcom/android/settings/vpn2/VpnSpinner;

    invoke-virtual {p2}, Lcom/android/settings/vpn2/VpnSpinner;->getSpinner()Lmiuix/appcompat/widget/Spinner;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->setTypesByFeature(Landroid/widget/Spinner;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mAllowedTypes:Ljava/util/List;

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mTotalTypes:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mType:Lcom/android/settings/vpn2/VpnSpinner;

    iget-object v2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    iget v2, v2, Lcom/android/internal/net/VpnProfile;->type:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p2

    invoke-virtual {v1, p2}, Lcom/android/settings/vpn2/VpnSpinner;->setSelection(I)V

    goto :goto_0

    :cond_1
    const-string p2, "MiuiVpnEditFragment"

    const-string v0, "Allowed or Total vpn types not initialized when setting initial selection"

    invoke-static {p2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mServer:Lmiuix/androidbasewidget/widget/StateEditText;

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    iget-object v0, v0, Lcom/android/internal/net/VpnProfile;->server:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mUsername:Lmiuix/androidbasewidget/widget/StateEditText;

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    iget-object v0, v0, Lcom/android/internal/net/VpnProfile;->username:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mPassword:Lmiuix/androidbasewidget/widget/StateEditText;

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    iget-object v0, v0, Lcom/android/internal/net/VpnProfile;->password:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mSearchDomains:Lmiuix/androidbasewidget/widget/StateEditText;

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    iget-object v0, v0, Lcom/android/internal/net/VpnProfile;->searchDomains:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mDnsServers:Lmiuix/androidbasewidget/widget/StateEditText;

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    iget-object v0, v0, Lcom/android/internal/net/VpnProfile;->dnsServers:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mRoutes:Lmiuix/androidbasewidget/widget/StateEditText;

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    iget-object v0, v0, Lcom/android/internal/net/VpnProfile;->routes:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mMppe:Lcom/android/settings/vpn2/VpnCheckBox;

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    iget-boolean v0, v0, Lcom/android/internal/net/VpnProfile;->mppe:Z

    invoke-virtual {p2, v0}, Lcom/android/settings/vpn2/VpnCheckBox;->setChecked(Z)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mL2tpSecret:Lmiuix/androidbasewidget/widget/StateEditText;

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    iget-object v0, v0, Lcom/android/internal/net/VpnProfile;->l2tpSecret:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecIdentifier:Lmiuix/androidbasewidget/widget/StateEditText;

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    iget-object v0, v0, Lcom/android/internal/net/VpnProfile;->ipsecIdentifier:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecSecret:Lmiuix/androidbasewidget/widget/StateEditText;

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    iget-object v0, v0, Lcom/android/internal/net/VpnProfile;->ipsecSecret:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mType:Lcom/android/settings/vpn2/VpnSpinner;

    invoke-virtual {p2}, Lcom/android/settings/vpn2/VpnSpinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/android/settings/vpn2/VpnSpinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecUserCert:Lcom/android/settings/vpn2/VpnSpinner;

    invoke-virtual {p2}, Lcom/android/settings/vpn2/VpnSpinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/android/settings/vpn2/VpnSpinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecCaCert:Lcom/android/settings/vpn2/VpnSpinner;

    invoke-virtual {p2}, Lcom/android/settings/vpn2/VpnSpinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/android/settings/vpn2/VpnSpinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecServerCert:Lcom/android/settings/vpn2/VpnSpinner;

    invoke-virtual {p2}, Lcom/android/settings/vpn2/VpnSpinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/android/settings/vpn2/VpnSpinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mName:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {p2, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mType:Lcom/android/settings/vpn2/VpnSpinner;

    invoke-virtual {p2}, Lcom/android/settings/vpn2/VpnSpinner;->getSpinner()Lmiuix/appcompat/widget/Spinner;

    move-result-object p2

    invoke-virtual {p2, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mServer:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {p2, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mUsername:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {p2, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mPassword:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {p2, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mDnsServers:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {p2, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mRoutes:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {p2, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecIdentifier:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {p2, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecSecret:Lmiuix/androidbasewidget/widget/StateEditText;

    invoke-virtual {p2, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object p2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mIpsecUserCert:Lcom/android/settings/vpn2/VpnSpinner;

    invoke-virtual {p2}, Lcom/android/settings/vpn2/VpnSpinner;->getSpinner()Lmiuix/appcompat/widget/Spinner;

    move-result-object p2

    invoke-virtual {p2, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    const/4 p2, 0x1

    invoke-direct {p0, p2}, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->validate(Z)Z

    move-result v0

    iget-boolean v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mEditing:Z

    if-nez v1, :cond_3

    if-nez v0, :cond_2

    goto :goto_1

    :cond_2
    move v0, v3

    goto :goto_2

    :cond_3
    :goto_1
    move v0, p2

    :goto_2
    iput-boolean v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mEditing:Z

    if-eqz v0, :cond_6

    sget v0, Lcom/android/settings/R$id;->editor:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/android/settings/R$id;->options:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mChoice:Z

    if-eqz v2, :cond_4

    move v2, v3

    goto :goto_3

    :cond_4
    const/16 v2, 0x8

    :goto_3
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    iget v1, v1, Lcom/android/internal/net/VpnProfile;->type:I

    invoke-direct {p0, v1}, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->changeType(I)V

    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mOptions:Lcom/android/settings/vpn2/VpnCheckBox;

    new-instance v2, Lcom/android/settings/vpn2/MiuiVpnEditFragment$1;

    invoke-direct {v2, p0}, Lcom/android/settings/vpn2/MiuiVpnEditFragment$1;-><init>(Lcom/android/settings/vpn2/MiuiVpnEditFragment;)V

    invoke-virtual {v1, v2}, Lcom/android/settings/vpn2/VpnCheckBox;->setCheckListener(Lcom/android/settings/vpn2/VpnCheckBox$CheckListener;)V

    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    iget-object v1, v1, Lcom/android/internal/net/VpnProfile;->searchDomains:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    iget-object v1, v1, Lcom/android/internal/net/VpnProfile;->dnsServers:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mProfile:Lcom/android/internal/net/VpnProfile;

    iget-object v1, v1, Lcom/android/internal/net/VpnProfile;->routes:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    :cond_5
    iget-object v1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mOptions:Lcom/android/settings/vpn2/VpnCheckBox;

    invoke-virtual {v1}, Lcom/android/settings/vpn2/VpnCheckBox;->performClick()Z

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mOptions:Lcom/android/settings/vpn2/VpnCheckBox;

    invoke-virtual {v0}, Lcom/android/settings/vpn2/VpnCheckBox;->isChecked()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mChoice:Z

    :cond_6
    sget v0, Lcom/android/settings/R$id;->button_delete:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    invoke-direct {p0, p1}, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->addAnim(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mArgs:Landroid/os/Bundle;

    const-string/jumbo v1, "profile_add"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result p2

    if-nez p2, :cond_7

    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    new-instance p2, Lcom/android/settings/vpn2/MiuiVpnEditFragment$2;

    invoke-direct {p2, p0}, Lcom/android/settings/vpn2/MiuiVpnEditFragment$2;-><init>(Lcom/android/settings/vpn2/MiuiVpnEditFragment;)V

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_7
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/android/settings/utils/KeyboardHelper;->assistActivity(Landroid/app/Activity;)Lcom/android/settings/utils/KeyboardHelper;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/vpn2/MiuiVpnEditFragment;->mKeyboardHelper:Lcom/android/settings/utils/KeyboardHelper;

    return-void
.end method
