.class Lcom/android/settings/MutedVideoView$7;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/MutedVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/MutedVideoView;


# direct methods
.method constructor <init>(Lcom/android/settings/MutedVideoView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/MutedVideoView$7;->this$0:Lcom/android/settings/MutedVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 2

    iget-object p1, p0, Lcom/android/settings/MutedVideoView$7;->this$0:Lcom/android/settings/MutedVideoView;

    invoke-static {p1, p3}, Lcom/android/settings/MutedVideoView;->-$$Nest$fputmSurfaceWidth(Lcom/android/settings/MutedVideoView;I)V

    iget-object p1, p0, Lcom/android/settings/MutedVideoView$7;->this$0:Lcom/android/settings/MutedVideoView;

    invoke-static {p1, p4}, Lcom/android/settings/MutedVideoView;->-$$Nest$fputmSurfaceHeight(Lcom/android/settings/MutedVideoView;I)V

    iget-object p1, p0, Lcom/android/settings/MutedVideoView$7;->this$0:Lcom/android/settings/MutedVideoView;

    invoke-static {p1}, Lcom/android/settings/MutedVideoView;->-$$Nest$fgetmTargetState(Lcom/android/settings/MutedVideoView;)I

    move-result p1

    const/4 p2, 0x1

    const/4 v0, 0x0

    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    move p1, p2

    goto :goto_0

    :cond_0
    move p1, v0

    :goto_0
    iget-object v1, p0, Lcom/android/settings/MutedVideoView$7;->this$0:Lcom/android/settings/MutedVideoView;

    invoke-static {v1}, Lcom/android/settings/MutedVideoView;->-$$Nest$fgetmVideoWidth(Lcom/android/settings/MutedVideoView;)I

    move-result v1

    if-ne v1, p3, :cond_1

    iget-object p3, p0, Lcom/android/settings/MutedVideoView$7;->this$0:Lcom/android/settings/MutedVideoView;

    invoke-static {p3}, Lcom/android/settings/MutedVideoView;->-$$Nest$fgetmVideoHeight(Lcom/android/settings/MutedVideoView;)I

    move-result p3

    if-ne p3, p4, :cond_1

    goto :goto_1

    :cond_1
    move p2, v0

    :goto_1
    iget-object p3, p0, Lcom/android/settings/MutedVideoView$7;->this$0:Lcom/android/settings/MutedVideoView;

    invoke-static {p3}, Lcom/android/settings/MutedVideoView;->-$$Nest$fgetmMediaPlayer(Lcom/android/settings/MutedVideoView;)Landroid/media/MediaPlayer;

    move-result-object p3

    if-eqz p3, :cond_3

    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    iget-object p1, p0, Lcom/android/settings/MutedVideoView$7;->this$0:Lcom/android/settings/MutedVideoView;

    invoke-static {p1}, Lcom/android/settings/MutedVideoView;->-$$Nest$fgetmSeekWhenPrepared(Lcom/android/settings/MutedVideoView;)I

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/android/settings/MutedVideoView$7;->this$0:Lcom/android/settings/MutedVideoView;

    invoke-static {p1}, Lcom/android/settings/MutedVideoView;->-$$Nest$fgetmSeekWhenPrepared(Lcom/android/settings/MutedVideoView;)I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/android/settings/MutedVideoView;->seekTo(I)V

    :cond_2
    iget-object p0, p0, Lcom/android/settings/MutedVideoView$7;->this$0:Lcom/android/settings/MutedVideoView;

    invoke-virtual {p0}, Lcom/android/settings/MutedVideoView;->start()V

    :cond_3
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MutedVideoView$7;->this$0:Lcom/android/settings/MutedVideoView;

    invoke-static {v0, p1}, Lcom/android/settings/MutedVideoView;->-$$Nest$fputmSurfaceHolder(Lcom/android/settings/MutedVideoView;Landroid/view/SurfaceHolder;)V

    iget-object p0, p0, Lcom/android/settings/MutedVideoView$7;->this$0:Lcom/android/settings/MutedVideoView;

    invoke-static {p0}, Lcom/android/settings/MutedVideoView;->-$$Nest$mopenVideo(Lcom/android/settings/MutedVideoView;)V

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1

    iget-object p1, p0, Lcom/android/settings/MutedVideoView$7;->this$0:Lcom/android/settings/MutedVideoView;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/android/settings/MutedVideoView;->-$$Nest$fputmSurfaceHolder(Lcom/android/settings/MutedVideoView;Landroid/view/SurfaceHolder;)V

    iget-object p1, p0, Lcom/android/settings/MutedVideoView$7;->this$0:Lcom/android/settings/MutedVideoView;

    invoke-static {p1}, Lcom/android/settings/MutedVideoView;->-$$Nest$fgetmMediaController(Lcom/android/settings/MutedVideoView;)Landroid/widget/MediaController;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/MutedVideoView$7;->this$0:Lcom/android/settings/MutedVideoView;

    invoke-static {p1}, Lcom/android/settings/MutedVideoView;->-$$Nest$fgetmMediaController(Lcom/android/settings/MutedVideoView;)Landroid/widget/MediaController;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/MediaController;->hide()V

    :cond_0
    iget-object p0, p0, Lcom/android/settings/MutedVideoView$7;->this$0:Lcom/android/settings/MutedVideoView;

    const/4 p1, 0x1

    invoke-static {p0, p1}, Lcom/android/settings/MutedVideoView;->-$$Nest$mrelease(Lcom/android/settings/MutedVideoView;Z)V

    return-void
.end method
