.class public Lcom/android/settings/AodAndLockScreenSettings;
.super Lcom/android/settings/KeyguardSettingsPreferenceFragment;

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private mAodModePref:Lcom/android/settings/KeyguardRestrictedPreference;

.field private mAodModePrefController:Lcom/android/settings/AodModePreferenceController;

.field private mAodNotificationModePref:Lcom/android/settings/KeyguardRestrictedPreference;

.field private mAodNotificationPrefController:Lcom/android/settings/AodNotificationPrefController;

.field private mAodSettingPrefController:Lcom/android/settings/AodSettingPreferenceController;

.field private mAodSettingsCategory:Landroidx/preference/PreferenceCategory;

.field private mAodSettingsSwitchPref:Landroidx/preference/Preference;

.field private mAodShowModePref:Lcom/android/settings/KeyguardRestrictedPreference;

.field private mAodShowModePrefController:Lcom/android/settings/AodShowModePreferenceController;

.field private mAodShowModeStyleSelectAvaliable:Z

.field mAodStateObserver:Landroid/database/ContentObserver;

.field private mAodStyleListSupportSetMode:Z

.field private mAodStylePref:Lcom/android/settings/AodStylePreference;

.field private mAodStylePrefController:Lcom/android/settings/AodStylePreferenceController;

.field private mEyeGazePref:Landroidx/preference/CheckBoxPreference;

.field private mFoldLockScreenCbp:Landroidx/preference/CheckBoxPreference;

.field private mGestureWakeupPref:Landroidx/preference/CheckBoxPreference;

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mLockScreenDisplayCategory:Landroidx/preference/PreferenceCategory;

.field private mLockScreenMagazine:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

.field private mNotificationStyleSelectAvaliable:Z

.field private mOtherCategory:Landroidx/preference/PreferenceCategory;

.field private mPickupWakeupPref:Landroidx/preference/CheckBoxPreference;

.field private mPowerMenuUnderKeyguard:Landroidx/preference/CheckBoxPreference;

.field private mScreenOnProximitySensor:Landroidx/preference/CheckBoxPreference;

.field private mScreenTimeout:Lcom/android/settings/KeyguardTimeoutListPreference;

.field private mShowChargingInNonLockscreen:Landroidx/preference/CheckBoxPreference;

.field private mSmartCoverSensitiveCbp:Landroidx/preference/CheckBoxPreference;

.field private mVolumeKeyLaunchCamera:Landroidx/preference/CheckBoxPreference;

.field private mWakeupAndSleepCategory:Landroidx/preference/PreferenceCategory;

.field private mWakeupForKeyguardNotificationPref:Landroidx/preference/CheckBoxPreference;


# direct methods
.method static bridge synthetic -$$Nest$fgetmLockScreenDisplayCategory(Lcom/android/settings/AodAndLockScreenSettings;)Landroidx/preference/PreferenceCategory;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mLockScreenDisplayCategory:Landroidx/preference/PreferenceCategory;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLockScreenMagazine(Lcom/android/settings/AodAndLockScreenSettings;)Lcom/android/settingslib/miuisettings/preference/ValuePreference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mLockScreenMagazine:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mupdateAodState(Lcom/android/settings/AodAndLockScreenSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/AodAndLockScreenSettings;->updateAodState()V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/KeyguardSettingsPreferenceFragment;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mNotificationStyleSelectAvaliable:Z

    iput-boolean v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodShowModeStyleSelectAvaliable:Z

    iput-boolean v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodStyleListSupportSetMode:Z

    new-instance v0, Lcom/android/settings/AodAndLockScreenSettings$2;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/settings/AodAndLockScreenSettings$2;-><init>(Lcom/android/settings/AodAndLockScreenSettings;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodStateObserver:Landroid/database/ContentObserver;

    return-void
.end method

.method public static checkoutActivityExist(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    const/4 p1, 0x0

    invoke-virtual {p0, v0, p1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p1, 0x1

    :cond_0
    return p1
.end method

.method private enableScreenOnProximitySensor(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "enable_screen_on_proximity_sensor"

    invoke-static {p0, v0, p1}, Landroid/provider/MiuiSettings$Global;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    return-void
.end method

.method public static getAodIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    new-instance p0, Landroid/content/Intent;

    invoke-direct {p0}, Landroid/content/Intent;-><init>()V

    invoke-static {}, Lcom/android/settings/utils/AodUtils;->isFoldDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    const/high16 v0, 0x10000000

    invoke-virtual {p0, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_0
    const-string v0, "com.miui.aod"

    const-string v1, "com.miui.aod.settings.AodStyleCategoriesActivity"

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "android.intent.action.MAIN"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public static getKeyguardClockIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.android.systemui"

    const-string v2, "com.android.keyguard.settings.ChooseKeyguardClockActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    const-string v2, "extra_user_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0}, Lcom/android/settings/MiuiKeyguardSettingsUtils;->isLargeScreen(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-static {v0}, Lcom/android/settings/MiuiKeyguardSettingsUtils;->setCancelSettingsSplit(Landroid/content/Intent;)V

    :cond_0
    return-object v0
.end method

.method private static getSettingsComponent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/ComponentName;
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "content://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    const-string v1, "getSettingsComponent"

    invoke-virtual {p0, p1, v1, v0, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object p0

    const-string/jumbo p1, "result_string"

    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-object v0

    :cond_0
    invoke-static {p0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    return-object v0
.end method

.method public static getWallpaperIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 7

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "miui.intent.action.LOCKWALLPAPER_PROVIDER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentContentProviders(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->providerInfo:Landroid/content/pm/ProviderInfo;

    iget-object v1, v1, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    :try_start_0
    invoke-static {p0, v1}, Lcom/android/settings/AodAndLockScreenSettings;->isProviderEnabled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_0

    :cond_0
    const-string v3, "com.xiaomi.tv.gallerylockscreen.lockscreen_magazine_provider"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    const-string/jumbo v5, "mifg://fashiongallery/jump_setting"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_1

    :cond_1
    const-string v3, "IN"

    invoke-static {}, Lmiui/os/Build;->getRegion()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.miui.android.fashiongallery.setting.SETTING"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v3

    const-class v4, Lcom/android/settings/MiuiSecuritySettings;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "call lockscreen magazine provider  throw an exception"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move-object v3, v2

    :goto_1
    const/16 v4, 0x40

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    :cond_3
    if-nez v2, :cond_4

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    invoke-static {p0, v1}, Lcom/android/settings/AodAndLockScreenSettings;->getSettingsComponent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-nez v1, :cond_4

    goto/16 :goto_0

    :cond_4
    return-object v3

    :cond_5
    return-object v2
.end method

.method private initKeyguardNotificationPref()V
    .locals 2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/MiuiKeyguardSettingsUtils;->isSupportAodAnimateDevice(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mNotificationStyleSelectAvaliable:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mWakeupForKeyguardNotificationPref:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    iget-object p0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mWakeupAndSleepCategory:Landroidx/preference/PreferenceCategory;

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/settings/MiuiKeyguardSettingsUtils;->isWakeupForNotification(Landroid/content/Context;Landroid/content/ContentResolver;)Z

    move-result v0

    iget-object p0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mWakeupForKeyguardNotificationPref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p0, v0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :cond_1
    :goto_0
    return-void
.end method

.method private initLockScreenMagazine()V
    .locals 2

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mLockScreenDisplayCategory:Landroidx/preference/PreferenceCategory;

    iget-object p0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mLockScreenMagazine:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    invoke-virtual {v0, p0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    return-void

    :cond_0
    new-instance v0, Lcom/android/settings/AodAndLockScreenSettings$1;

    invoke-direct {v0, p0}, Lcom/android/settings/AodAndLockScreenSettings$1;-><init>(Lcom/android/settings/AodAndLockScreenSettings;)V

    sget-object p0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, p0, v1}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public static isAdaptiveSleepSupported(Landroid/content/Context;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x111000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/android/settings/AodAndLockScreenSettings;->isAttentionServiceAvailable(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private static isAttentionServiceAvailable(Landroid/content/Context;)Z
    .locals 4

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/pm/PackageManager;->getAttentionServicePackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    return v2

    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.service.attention.AttentionService"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x100000

    invoke-virtual {p0, v0, v1}, Landroid/content/pm/PackageManager;->resolveService(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object p0

    if-eqz p0, :cond_1

    iget-object p0, p0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    if-eqz p0, :cond_1

    const/4 v2, 0x1

    :cond_1
    return v2
.end method

.method public static isEllipticProximity(Landroid/content/Context;)Z
    .locals 1

    const-string/jumbo p0, "ro.vendor.audio.us.proximity"

    const/4 v0, 0x0

    invoke-static {p0, v0}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result p0

    return p0
.end method

.method public static isLockScreenMagazineAvailable(Landroid/content/Context;)Z
    .locals 1

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/android/settings/AodAndLockScreenSettings;->getWallpaperIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method private static isProviderEnabled(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 0

    const-string p0, "com.xiaomi.tv.gallerylockscreen.lockscreen_magazine_provider"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_0

    const-string p0, "com.miui.android.fashiongallery.lockscreen_magazine_provider"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    :cond_0
    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private isShowChangingInNonLockscreenEnable()Z
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "show_charging_in_non_lockscreen"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    if-ne p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public static isSupportAntiMisTouch(Landroid/content/Context;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    const-string/jumbo v2, "sensor"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/SensorManager;

    if-eqz v2, :cond_1

    const v1, 0x1fa2697

    invoke-virtual {v2, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    if-nez v1, :cond_2

    const-string v1, "android.hardware.sensor.proximity"

    invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {p0}, Lcom/android/settings/AodAndLockScreenSettings;->isEllipticProximity(Landroid/content/Context;)Z

    move-result p0

    if-nez p0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :cond_3
    return v0
.end method

.method public static isSupportPickupWakeup(Landroid/content/Context;)Z
    .locals 6

    const/4 v0, 0x0

    if-eqz p0, :cond_2

    const-string/jumbo v1, "sensor"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    const v2, 0x1fa265c

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "oem7 Pick Up Gesture"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v2}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v4, "pickup  Wakeup"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    return v3

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v2, Lcom/android/settings/R$array;->device_support_pickup_by_MTK:I

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    sget-object v2, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-interface {p0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    const/16 p0, 0x16

    invoke-virtual {v1, p0, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(IZ)Landroid/hardware/Sensor;

    move-result-object p0

    if-eqz p0, :cond_2

    move v0, v3

    :cond_2
    return v0
.end method

.method private setupPowerMenuUnderKeyguard()V
    .locals 2

    const-string/jumbo v0, "power_menu_under_keyguard"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mPowerMenuUnderKeyguard:Landroidx/preference/CheckBoxPreference;

    new-instance v1, Lcom/android/settings/AodAndLockScreenSettings$5;

    invoke-direct {v1, p0}, Lcom/android/settings/AodAndLockScreenSettings$5;-><init>(Lcom/android/settings/AodAndLockScreenSettings;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method

.method private setupTimeoutPreference()V
    .locals 4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "screen_off_timeout"

    const-wide/16 v2, 0x7530

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/settings/AodAndLockScreenSettings;->mScreenTimeout:Lcom/android/settings/KeyguardTimeoutListPreference;

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroidx/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object p0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mScreenTimeout:Lcom/android/settings/KeyguardTimeoutListPreference;

    invoke-virtual {p0}, Lcom/android/settings/KeyguardTimeoutListPreference;->disableUnusableTimeouts()V

    return-void
.end method

.method private updateAodState()V
    .locals 4

    iget-boolean v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodStyleListSupportSetMode:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodSettingPrefController:Lcom/android/settings/AodSettingPreferenceController;

    iget-object v1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodSettingsSwitchPref:Landroidx/preference/Preference;

    invoke-virtual {v0, v1}, Lcom/android/settings/core/TogglePreferenceController;->updateState(Landroidx/preference/Preference;)V

    iget-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodStylePrefController:Lcom/android/settings/AodStylePreferenceController;

    iget-object v1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodStylePref:Lcom/android/settings/AodStylePreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/AodStylePreferenceController;->updateState(Landroidx/preference/Preference;)V

    iget-boolean v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodShowModeStyleSelectAvaliable:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/AodStylePreferenceController;->isDualClock(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.miui.aod"

    const-string v3, "com.miui.aod.settings.AODStyleActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodStylePref:Lcom/android/settings/AodStylePreference;

    invoke-virtual {v1, v0}, Landroidx/preference/Preference;->setIntent(Landroid/content/Intent;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodShowModePrefController:Lcom/android/settings/AodShowModePreferenceController;

    iget-object v1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodShowModePref:Lcom/android/settings/KeyguardRestrictedPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/AodShowModePreferenceController;->updateState(Landroidx/preference/Preference;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodModePrefController:Lcom/android/settings/AodModePreferenceController;

    iget-object v1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodModePref:Lcom/android/settings/KeyguardRestrictedPreference;

    invoke-virtual {v0, v1}, Lcom/android/settings/AodModePreferenceController;->updateState(Landroidx/preference/Preference;)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodNotificationPrefController:Lcom/android/settings/AodNotificationPrefController;

    iget-object p0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodNotificationModePref:Lcom/android/settings/KeyguardRestrictedPreference;

    invoke-virtual {v0, p0}, Lcom/android/settings/AodNotificationPrefController;->updateState(Landroidx/preference/Preference;)V

    return-void
.end method

.method private updatePowerMenuUnderKeyguard()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mPowerMenuUnderKeyguard:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v1, "power_menu_under_keyguard"

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Landroid/provider/MiuiSettings$System;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result p0

    invoke-virtual {v0, p0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    return-void
.end method

.method private updateProximitySensorStatus()V
    .locals 4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/AodAndLockScreenSettings;->isSupportAntiMisTouch(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mScreenOnProximitySensor:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/android/settings/AodAndLockScreenSettings;->mOtherCategory:Landroidx/preference/PreferenceCategory;

    invoke-virtual {v2, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mScreenOnProximitySensor:Landroidx/preference/CheckBoxPreference;

    invoke-direct {p0, v1}, Lcom/android/settings/AodAndLockScreenSettings;->enableScreenOnProximitySensor(Z)V

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "enable_screen_on_proximity_sensor"

    const/4 v3, -0x1

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v3, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x1105004d

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    invoke-static {v0, v2, v1}, Landroid/provider/MiuiSettings$System;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v2, v0}, Landroid/provider/MiuiSettings$Global;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    move v0, v1

    :goto_0
    iget-object p0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mScreenOnProximitySensor:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p0, v0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :cond_3
    :goto_1
    return-void
.end method

.method private updateTimeoutPreferenceState()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mScreenTimeout:Lcom/android/settings/KeyguardTimeoutListPreference;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/AodAndLockScreenSettings;->setupTimeoutPreference()V

    iget-object p0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mScreenTimeout:Lcom/android/settings/KeyguardTimeoutListPreference;

    invoke-virtual {p0}, Lcom/android/settings/KeyguardTimeoutListPreference;->updateTimeoutPreferenceSummary()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getMetricsCategory()I
    .locals 0

    const/16 p0, 0x57

    return p0
.end method

.method public getName()Ljava/lang/String;
    .locals 0

    const-class p0, Lcom/android/settings/AodAndLockScreenSettings;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public getPageIndex()I
    .locals 0

    const/4 p0, 0x2

    return p0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroidx/preference/PreferenceGroup;->removeAll()V

    :cond_0
    sget p1, Lcom/android/settings/R$xml;->aod_and_lockscreen_settings:I

    invoke-virtual {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    const-string v0, "com.miui.aod"

    const-string v1, "com.miui.aod.settings.NotificationAnimationSelectActivity"

    invoke-static {p1, v0, v1}, Lcom/android/settings/AodAndLockScreenSettings;->checkoutActivityExist(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mNotificationStyleSelectAvaliable:Z

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    const-string v1, "com.miui.aod.settings.AodShowModeSettingActivity"

    invoke-static {p1, v0, v1}, Lcom/android/settings/AodAndLockScreenSettings;->checkoutActivityExist(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodShowModeStyleSelectAvaliable:Z

    const-string p1, "aod_settings_category"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/PreferenceCategory;

    iput-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodSettingsCategory:Landroidx/preference/PreferenceCategory;

    const-string p1, "aod_settings_switch"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodSettingsSwitchPref:Landroidx/preference/Preference;

    new-instance p1, Lcom/android/settings/AodSettingPreferenceController;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/android/settings/AodSettingPreferenceController;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodSettingPrefController:Lcom/android/settings/AodSettingPreferenceController;

    iget-boolean v1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodShowModeStyleSelectAvaliable:Z

    invoke-virtual {p1, v1}, Lcom/android/settings/AodSettingPreferenceController;->setAodShowModeStyleSelectAvaliable(Z)V

    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodSettingPrefController:Lcom/android/settings/AodSettingPreferenceController;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/android/settings/AodSettingPreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    const-string p1, "aod_show_style"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/AodStylePreference;

    iput-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodStylePref:Lcom/android/settings/AodStylePreference;

    new-instance p1, Lcom/android/settings/AodStylePreferenceController;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/android/settings/AodStylePreferenceController;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodStylePrefController:Lcom/android/settings/AodStylePreferenceController;

    iget-boolean v1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodShowModeStyleSelectAvaliable:Z

    invoke-virtual {p1, v1}, Lcom/android/settings/AodStylePreferenceController;->setAodShowModeStyleSelectAvaliable(Z)V

    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodStylePrefController:Lcom/android/settings/AodStylePreferenceController;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/android/settings/AodStylePreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodSettingPrefController:Lcom/android/settings/AodSettingPreferenceController;

    iget-object v1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodStylePrefController:Lcom/android/settings/AodStylePreferenceController;

    invoke-virtual {p1, v1}, Lcom/android/settings/AodSettingPreferenceController;->addController(Lcom/android/settings/core/AodPreferenceController;)V

    const-string p1, "aod_show_mode"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/KeyguardRestrictedPreference;

    iput-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodShowModePref:Lcom/android/settings/KeyguardRestrictedPreference;

    new-instance p1, Lcom/android/settings/AodShowModePreferenceController;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/android/settings/AodShowModePreferenceController;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodShowModePrefController:Lcom/android/settings/AodShowModePreferenceController;

    iget-boolean v1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodShowModeStyleSelectAvaliable:Z

    invoke-virtual {p1, v1}, Lcom/android/settings/AodShowModePreferenceController;->setAodShowModeStyleSelectAvaliable(Z)V

    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodShowModePrefController:Lcom/android/settings/AodShowModePreferenceController;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/android/settings/AodShowModePreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodSettingPrefController:Lcom/android/settings/AodSettingPreferenceController;

    iget-object v1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodShowModePrefController:Lcom/android/settings/AodShowModePreferenceController;

    invoke-virtual {p1, v1}, Lcom/android/settings/AodSettingPreferenceController;->addController(Lcom/android/settings/core/AodPreferenceController;)V

    const-string p1, "aod_mode_value_preference"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/KeyguardRestrictedPreference;

    iput-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodModePref:Lcom/android/settings/KeyguardRestrictedPreference;

    new-instance p1, Lcom/android/settings/AodModePreferenceController;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/android/settings/AodModePreferenceController;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodModePrefController:Lcom/android/settings/AodModePreferenceController;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/android/settings/core/BasePreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodSettingPrefController:Lcom/android/settings/AodSettingPreferenceController;

    iget-object v1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodModePrefController:Lcom/android/settings/AodModePreferenceController;

    invoke-virtual {p1, v1}, Lcom/android/settings/AodSettingPreferenceController;->addController(Lcom/android/settings/core/AodPreferenceController;)V

    const-string p1, "aod_notification_status"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/KeyguardRestrictedPreference;

    iput-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodNotificationModePref:Lcom/android/settings/KeyguardRestrictedPreference;

    new-instance p1, Lcom/android/settings/AodNotificationPrefController;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/android/settings/AodNotificationPrefController;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodNotificationPrefController:Lcom/android/settings/AodNotificationPrefController;

    iget-boolean v1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodShowModeStyleSelectAvaliable:Z

    invoke-virtual {p1, v1}, Lcom/android/settings/AodNotificationPrefController;->setAodShowModeStyleSelectAvaliable(Z)V

    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodNotificationPrefController:Lcom/android/settings/AodNotificationPrefController;

    iget-boolean v1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mNotificationStyleSelectAvaliable:Z

    invoke-virtual {p1, v1}, Lcom/android/settings/AodNotificationPrefController;->setNotificationStyleSelectAvaliable(Z)V

    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodNotificationPrefController:Lcom/android/settings/AodNotificationPrefController;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/android/settings/AodNotificationPrefController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/android/settings/utils/AodUtils;->isAodAvailable(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_1

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodSettingsCategory:Landroidx/preference/PreferenceCategory;

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    goto :goto_2

    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    const-string v1, "com.miui.aod.settings.AODSettingActivity"

    invoke-static {p1, v0, v1}, Lcom/android/settings/AodAndLockScreenSettings;->checkoutActivityExist(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_2

    new-instance p1, Landroid/content/Intent;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    const-class v1, Lcom/android/settings/AODSettingActivity;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "android.intent.action.MAIN"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodStylePref:Lcom/android/settings/AodStylePreference;

    invoke-virtual {v0, p1}, Landroidx/preference/Preference;->setIntent(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodShowModePref:Lcom/android/settings/KeyguardRestrictedPreference;

    invoke-virtual {v0, p1}, Landroidx/preference/Preference;->setIntent(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodNotificationModePref:Lcom/android/settings/KeyguardRestrictedPreference;

    invoke-virtual {v0, p1}, Landroidx/preference/Preference;->setIntent(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/android/settings/utils/AodUtils;->actionAvailable(Landroid/content/Context;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodStyleListSupportSetMode:Z

    :goto_0
    iget-boolean p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodStyleListSupportSetMode:Z

    if-nez p1, :cond_3

    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodSettingsCategory:Landroidx/preference/PreferenceCategory;

    iget-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodModePref:Lcom/android/settings/KeyguardRestrictedPreference;

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    goto :goto_1

    :cond_3
    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodSettingsCategory:Landroidx/preference/PreferenceCategory;

    iget-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodSettingsSwitchPref:Landroidx/preference/Preference;

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodSettingsCategory:Landroidx/preference/PreferenceCategory;

    iget-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodShowModePref:Lcom/android/settings/KeyguardRestrictedPreference;

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodSettingsCategory:Landroidx/preference/PreferenceCategory;

    iget-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodStylePref:Lcom/android/settings/AodStylePreference;

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :goto_1
    invoke-direct {p0}, Lcom/android/settings/AodAndLockScreenSettings;->updateAodState()V

    :goto_2
    const-string/jumbo p1, "wakeup_and_sleep_settings_category"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/PreferenceCategory;

    iput-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mWakeupAndSleepCategory:Landroidx/preference/PreferenceCategory;

    const-string/jumbo p1, "screen_timeout"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/KeyguardTimeoutListPreference;

    iput-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mScreenTimeout:Lcom/android/settings/KeyguardTimeoutListPreference;

    const-string p1, "eye_gaze"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mEyeGazePref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/android/settings/AodAndLockScreenSettings;->isAdaptiveSleepSupported(Landroid/content/Context;)Z

    move-result p1

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_4

    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mWakeupAndSleepCategory:Landroidx/preference/PreferenceCategory;

    iget-object v2, p0, Lcom/android/settings/AodAndLockScreenSettings;->mEyeGazePref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, v2}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    goto :goto_4

    :cond_4
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    if-eqz p1, :cond_5

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const/4 v2, -0x2

    const-string v3, "adaptive_sleep"

    invoke-static {p1, v3, v1, v2}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result p1

    if-eqz p1, :cond_5

    move p1, v0

    goto :goto_3

    :cond_5
    move p1, v1

    :goto_3
    iget-object v2, p0, Lcom/android/settings/AodAndLockScreenSettings;->mEyeGazePref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v2, p1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :goto_4
    const-string p1, "gesture_wakeup"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v2

    check-cast v2, Landroidx/preference/CheckBoxPreference;

    iput-object v2, p0, Lcom/android/settings/AodAndLockScreenSettings;->mGestureWakeupPref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v2, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v2, "support_gesture_wakeup"

    invoke-static {v2, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mWakeupAndSleepCategory:Landroidx/preference/PreferenceCategory;

    iget-object v2, p0, Lcom/android/settings/AodAndLockScreenSettings;->mGestureWakeupPref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, v2}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    goto :goto_5

    :cond_6
    invoke-static {}, Lcom/android/settings/MiuiKeyguardSettingsUtils;->isPad()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/android/settings/AodAndLockScreenSettings;->mGestureWakeupPref:Landroidx/preference/CheckBoxPreference;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_7
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    invoke-static {v2, p1, v1, v3}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result p1

    if-eqz p1, :cond_8

    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mGestureWakeupPref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, v0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    goto :goto_5

    :cond_8
    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mGestureWakeupPref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :goto_5
    const-string/jumbo p1, "pick_up_gesture_wakeup"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mPickupWakeupPref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/android/settings/AodAndLockScreenSettings;->isSupportPickupWakeup(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_9

    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mWakeupAndSleepCategory:Landroidx/preference/PreferenceCategory;

    iget-object v2, p0, Lcom/android/settings/AodAndLockScreenSettings;->mPickupWakeupPref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, v2}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    goto :goto_6

    :cond_9
    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mPickupWakeupPref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    const-string/jumbo v4, "pick_up_gesture_wakeup_mode"

    invoke-static {v2, v4, v1, v3}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v2

    invoke-virtual {p1, v2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :goto_6
    const-string/jumbo p1, "wakeup_for_keyguard_notification"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mWakeupForKeyguardNotificationPref:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    invoke-direct {p0}, Lcom/android/settings/AodAndLockScreenSettings;->initKeyguardNotificationPref()V

    const-string/jumbo p1, "smartcover_lock_or_unlock_screen"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mSmartCoverSensitiveCbp:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object p1

    const-string v2, "config_smartCoverEnabled"

    const-string v3, "bool"

    const-string v4, "android.miui"

    invoke-virtual {p1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    const-string v2, "AodAndLockScreenSetting"

    if-lez p1, :cond_a

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result p1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isSupportSmartCover: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    :cond_a
    const-string p1, "The device is old smart cover."

    invoke-static {v2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move p1, v1

    :goto_7
    if-eqz p1, :cond_c

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string/jumbo v2, "miui_smart_cover_mode"

    invoke-static {p1, v2, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p1

    if-ne p1, v0, :cond_b

    move p1, v0

    goto :goto_8

    :cond_b
    move p1, v1

    :goto_8
    iget-object v2, p0, Lcom/android/settings/AodAndLockScreenSettings;->mSmartCoverSensitiveCbp:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v2, p1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    goto :goto_9

    :cond_c
    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mWakeupAndSleepCategory:Landroidx/preference/PreferenceCategory;

    iget-object v2, p0, Lcom/android/settings/AodAndLockScreenSettings;->mSmartCoverSensitiveCbp:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, v2}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :goto_9
    const-string p1, "lock_screen_after_fold_screen"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v2

    check-cast v2, Landroidx/preference/CheckBoxPreference;

    iput-object v2, p0, Lcom/android/settings/AodAndLockScreenSettings;->mFoldLockScreenCbp:Landroidx/preference/CheckBoxPreference;

    sget-object v2, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    const-string v3, "cetus"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/android/settings/AodAndLockScreenSettings;->mFoldLockScreenCbp:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    invoke-static {v3, p1, v0, v4}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result p1

    invoke-virtual {v2, p1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mFoldLockScreenCbp:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_a

    :cond_d
    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mWakeupAndSleepCategory:Landroidx/preference/PreferenceCategory;

    iget-object v2, p0, Lcom/android/settings/AodAndLockScreenSettings;->mFoldLockScreenCbp:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, v2}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :goto_a
    const-string p1, "lock_screen_display_category"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/PreferenceCategory;

    iput-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mLockScreenDisplayCategory:Landroidx/preference/PreferenceCategory;

    const-string p1, "lockscreen_magazine"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    iput-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mLockScreenMagazine:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    invoke-static {}, Lmiui/os/Build;->getRegion()Ljava/lang/String;

    move-result-object p1

    const-string v2, "IN"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_e

    sget-boolean p1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz p1, :cond_e

    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mLockScreenMagazine:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/android/settings/R$string;->lockscreen_magazine_india:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    :cond_e
    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mLockScreenMagazine:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    invoke-virtual {p1, v0}, Lcom/android/settingslib/miuisettings/preference/ValuePreference;->setShowRightArrow(Z)V

    invoke-direct {p0}, Lcom/android/settings/AodAndLockScreenSettings;->initLockScreenMagazine()V

    const-string p1, "choose_keyguard_clock"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    invoke-virtual {p1, v0}, Lcom/android/settingslib/miuisettings/preference/ValuePreference;->setShowRightArrow(Z)V

    const-string/jumbo p1, "others_category"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/PreferenceCategory;

    iput-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mOtherCategory:Landroidx/preference/PreferenceCategory;

    const-string/jumbo p1, "volume_launch_camera"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mVolumeKeyLaunchCamera:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo p1, "support_edge_touch_volume"

    invoke-static {p1, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    if-nez p1, :cond_10

    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mVolumeKeyLaunchCamera:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "volumekey_launch_camera"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_f

    goto :goto_b

    :cond_f
    move v0, v1

    :goto_b
    invoke-virtual {p1, v0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    goto :goto_c

    :cond_10
    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mVolumeKeyLaunchCamera:Landroidx/preference/CheckBoxPreference;

    if-eqz p1, :cond_11

    iget-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mOtherCategory:Landroidx/preference/PreferenceCategory;

    invoke-virtual {v0, p1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_11
    :goto_c
    const-string/jumbo p1, "screen_on_proximity_sensor"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mScreenOnProximitySensor:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo p1, "show_charging_in_non_lockscreen"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mShowChargingInNonLockscreen:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-object p1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mShowChargingInNonLockscreen:Landroidx/preference/CheckBoxPreference;

    invoke-direct {p0}, Lcom/android/settings/AodAndLockScreenSettings;->isShowChangingInNonLockscreenEnable()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    invoke-direct {p0}, Lcom/android/settings/AodAndLockScreenSettings;->updateProximitySensorStatus()V

    invoke-direct {p0}, Lcom/android/settings/AodAndLockScreenSettings;->setupPowerMenuUnderKeyguard()V

    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/android/settings/MiuiPasswordGuardActivity;

    if-eqz v0, :cond_0

    sget p0, Lcom/android/settings/R$layout;->password_guard_activity:I

    const/4 p3, 0x0

    invoke-virtual {p1, p0, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/android/settingslib/miuisettings/preference/PreferenceFragment;->onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onPause()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/utils/AodUtils;->isAodAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodStateObserver:Landroid/database/ContentObserver;

    invoke-static {v0, v1}, Lcom/android/settings/utils/AodUtils;->unregisterAodStateObserver(Landroid/content/Context;Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodShowModePrefController:Lcom/android/settings/AodShowModePreferenceController;

    invoke-virtual {v0}, Lcom/android/settings/AodShowModePreferenceController;->cancelTask()V

    iget-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodStylePrefController:Lcom/android/settings/AodStylePreferenceController;

    invoke-virtual {v0}, Lcom/android/settings/AodStylePreferenceController;->cancelTask()V

    iget-object v0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodModePrefController:Lcom/android/settings/AodModePreferenceController;

    invoke-virtual {v0}, Lcom/android/settings/AodModePreferenceController;->cancelTask()V

    :cond_0
    iget-object p0, p0, Lcom/android/settings/AodAndLockScreenSettings;->mScreenTimeout:Lcom/android/settings/KeyguardTimeoutListPreference;

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/KeyguardTimeoutListPreference;->hideListView()V

    :cond_1
    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    iget-object v1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mGestureWakeupPref:Landroidx/preference/CheckBoxPreference;

    if-ne p1, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result p1

    const-string v0, "gesture_wakeup"

    invoke-static {p0, v0, p2, p1}, Landroid/provider/MiuiSettings$System;->putBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    goto/16 :goto_0

    :cond_0
    iget-object v1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mPickupWakeupPref:Landroidx/preference/CheckBoxPreference;

    if-ne p1, v1, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result p1

    const-string/jumbo v0, "pick_up_gesture_wakeup_mode"

    invoke-static {p0, v0, p2, p1}, Landroid/provider/MiuiSettings$System;->putBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mWakeupForKeyguardNotificationPref:Landroidx/preference/CheckBoxPreference;

    if-ne p1, v1, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo p1, "wakeup_for_keyguard_notification"

    invoke-static {p0, p1, p2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    :cond_2
    const-string/jumbo v1, "smartcover_lock_or_unlock_screen"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo p1, "miui_smart_cover_mode"

    invoke-static {p0, p1, p2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mVolumeKeyLaunchCamera:Landroidx/preference/CheckBoxPreference;

    if-ne p1, v1, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo p1, "volumekey_launch_camera"

    invoke-static {p0, p1, p2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mScreenOnProximitySensor:Landroidx/preference/CheckBoxPreference;

    if-ne p1, v1, :cond_5

    invoke-direct {p0, p2}, Lcom/android/settings/AodAndLockScreenSettings;->enableScreenOnProximitySensor(Z)V

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mShowChargingInNonLockscreen:Landroidx/preference/CheckBoxPreference;

    if-ne p1, v1, :cond_6

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo p1, "show_charging_in_non_lockscreen"

    invoke-static {p0, p1, p2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mEyeGazePref:Landroidx/preference/CheckBoxPreference;

    if-ne p1, v1, :cond_7

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string p1, "adaptive_sleep"

    invoke-static {p0, p1, p2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    :cond_7
    const-string p1, "lock_screen_after_fold_screen"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-static {p0, p1, p2, v0}, Landroid/provider/MiuiSettings$System;->putBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    :cond_8
    :goto_0
    const/4 p0, 0x1

    return p0
.end method

.method public onPreferenceTreeClick(Landroidx/preference/PreferenceScreen;Landroidx/preference/Preference;)Z
    .locals 3

    invoke-virtual {p2}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "choose_keyguard_clock"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.android.systemui"

    const-string v2, "com.android.keyguard.settings.ChooseKeyguardClockActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    const-string v2, "extra_user_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/MiuiKeyguardSettingsUtils;->isLargeScreen(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/android/settings/MiuiKeyguardSettingsUtils;->setCancelSettingsSplit(Landroid/content/Intent;)V

    :cond_0
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    :cond_1
    invoke-super {p0, p1, p2}, Lcom/android/settings/SettingsPreferenceFragment;->onPreferenceTreeClick(Landroidx/preference/PreferenceScreen;Landroidx/preference/Preference;)Z

    move-result p0

    return p0
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onResume()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/utils/AodUtils;->isAodAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/AodAndLockScreenSettings;->mAodStateObserver:Landroid/database/ContentObserver;

    invoke-static {v0, v1}, Lcom/android/settings/utils/AodUtils;->registerAodStateObserver(Landroid/content/Context;Landroid/database/ContentObserver;)V

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/AodAndLockScreenSettings;->updateAodState()V

    invoke-direct {p0}, Lcom/android/settings/AodAndLockScreenSettings;->updateTimeoutPreferenceState()V

    invoke-direct {p0}, Lcom/android/settings/AodAndLockScreenSettings;->updatePowerMenuUnderKeyguard()V

    return-void
.end method
