.class public Lcom/android/settings/sound/RingerVolumeFragment;
.super Lcom/android/settings/BaseFragment;

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;
    }
.end annotation


# static fields
.field static final SECTION_ID:[I

.field private static final SEEKBAR_MUTED_RES_ID:[I

.field private static final SEEKBAR_TYPE:[I

.field private static final SEEKBAR_UNMUTED_RES_ID:[I


# instance fields
.field private final DESCPTION_ID:[I

.field private mAudioManager:Landroid/media/AudioManager;

.field private mCheckBoxes:[Landroid/widget/ImageView;

.field private mHandler:Landroid/os/Handler;

.field private mRingModeChangedReceiver:Landroid/content/BroadcastReceiver;

.field private mSeekBarVolumizer:[Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;

.field private mSeekBars:[Lmiuix/androidbasewidget/widget/SeekBar;

.field private mVolumeChangedReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/settings/sound/RingerVolumeFragment;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetMediaVolumeUri(Lcom/android/settings/sound/RingerVolumeFragment;)Landroid/net/Uri;
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/sound/RingerVolumeFragment;->getMediaVolumeUri()Landroid/net/Uri;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mupdateSlidersAndMutedStates(Lcom/android/settings/sound/RingerVolumeFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/sound/RingerVolumeFragment;->updateSlidersAndMutedStates()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateSlidersAndMutedStates(Lcom/android/settings/sound/RingerVolumeFragment;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/sound/RingerVolumeFragment;->updateSlidersAndMutedStates(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetSEEKBAR_TYPE()[I
    .locals 1

    sget-object v0, Lcom/android/settings/sound/RingerVolumeFragment;->SEEKBAR_TYPE:[I

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 11

    const/4 v0, 0x6

    new-array v1, v0, [I

    sget v2, Lcom/android/settings/R$id;->ringer_section:I

    const/4 v3, 0x0

    aput v2, v1, v3

    sget v2, Lcom/android/settings/R$id;->notification_section:I

    const/4 v4, 0x1

    aput v2, v1, v4

    sget v2, Lcom/android/settings/R$id;->alarm_section:I

    const/4 v5, 0x2

    aput v2, v1, v5

    sget v2, Lcom/android/settings/R$id;->voice_section:I

    const/4 v6, 0x3

    aput v2, v1, v6

    sget v2, Lcom/android/settings/R$id;->media_section:I

    const/4 v7, 0x4

    aput v2, v1, v7

    sget v2, Lcom/android/settings/R$id;->bluetooth_section:I

    const/4 v8, 0x5

    aput v2, v1, v8

    sput-object v1, Lcom/android/settings/sound/RingerVolumeFragment;->SECTION_ID:[I

    new-array v1, v0, [I

    fill-array-data v1, :array_0

    sput-object v1, Lcom/android/settings/sound/RingerVolumeFragment;->SEEKBAR_TYPE:[I

    new-array v1, v0, [I

    sget v2, Lcom/android/settings/R$drawable;->ic_audio_ring_notif_mute:I

    aput v2, v1, v3

    sget v2, Lcom/android/settings/R$drawable;->ic_audio_notification_mute:I

    aput v2, v1, v4

    sget v2, Lcom/android/settings/R$drawable;->ic_audio_alarm_mute:I

    aput v2, v1, v5

    sget v2, Lcom/android/settings/R$drawable;->ic_audio_phone:I

    aput v2, v1, v6

    sget v9, Lcom/android/settings/R$drawable;->ic_audio_media:I

    aput v9, v1, v7

    sget v10, Lcom/android/settings/R$drawable;->ic_audio_bt_mute:I

    aput v10, v1, v8

    sput-object v1, Lcom/android/settings/sound/RingerVolumeFragment;->SEEKBAR_MUTED_RES_ID:[I

    new-array v0, v0, [I

    sget v1, Lcom/android/settings/R$drawable;->ic_audio_ring_notif:I

    aput v1, v0, v3

    sget v1, Lcom/android/settings/R$drawable;->ic_audio_notification:I

    aput v1, v0, v4

    sget v1, Lcom/android/settings/R$drawable;->ic_audio_alarm:I

    aput v1, v0, v5

    aput v2, v0, v6

    aput v9, v0, v7

    sget v1, Lcom/android/settings/R$drawable;->ic_audio_bt:I

    aput v1, v0, v8

    sput-object v0, Lcom/android/settings/sound/RingerVolumeFragment;->SEEKBAR_UNMUTED_RES_ID:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x2
        0x5
        0x4
        0x0
        0x3
        0x6
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/BaseFragment;-><init>()V

    const/4 v0, 0x6

    new-array v0, v0, [I

    sget v1, Lcom/android/settings/R$string;->volume_ring_description:I

    const/4 v2, 0x0

    aput v1, v0, v2

    sget v1, Lcom/android/settings/R$string;->volume_notification_description:I

    const/4 v2, 0x1

    aput v1, v0, v2

    sget v1, Lcom/android/settings/R$string;->volume_alarm_description:I

    const/4 v2, 0x2

    aput v1, v0, v2

    sget v1, Lcom/android/settings/R$string;->volume_voice_description:I

    const/4 v2, 0x3

    aput v1, v0, v2

    sget v1, Lcom/android/settings/R$string;->volume_media_description:I

    const/4 v2, 0x4

    aput v1, v0, v2

    sget v1, Lcom/android/settings/R$string;->volume_bluetooth_description:I

    const/4 v2, 0x5

    aput v1, v0, v2

    iput-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->DESCPTION_ID:[I

    sget-object v0, Lcom/android/settings/sound/RingerVolumeFragment;->SEEKBAR_MUTED_RES_ID:[I

    array-length v0, v0

    new-array v0, v0, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mCheckBoxes:[Landroid/widget/ImageView;

    sget-object v0, Lcom/android/settings/sound/RingerVolumeFragment;->SECTION_ID:[I

    array-length v0, v0

    new-array v0, v0, [Lmiuix/androidbasewidget/widget/SeekBar;

    iput-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mSeekBars:[Lmiuix/androidbasewidget/widget/SeekBar;

    new-instance v0, Lcom/android/settings/sound/RingerVolumeFragment$1;

    invoke-direct {v0, p0}, Lcom/android/settings/sound/RingerVolumeFragment$1;-><init>(Lcom/android/settings/sound/RingerVolumeFragment;)V

    iput-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private cleanup()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/android/settings/sound/RingerVolumeFragment;->SECTION_ID:[I

    array-length v1, v1

    const/4 v2, 0x0

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mSeekBarVolumizer:[Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;->stop()V

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mSeekBarVolumizer:[Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;

    aput-object v2, v1, v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mRingModeChangedReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mRingModeChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v2, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mRingModeChangedReceiver:Landroid/content/BroadcastReceiver;

    :cond_2
    iget-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mVolumeChangedReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mVolumeChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v2, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mVolumeChangedReceiver:Landroid/content/BroadcastReceiver;

    :cond_3
    iget-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object p0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mHandler:Landroid/os/Handler;

    const/16 v0, 0x66

    invoke-virtual {p0, v0}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method private getMediaVolumeUri()Landroid/net/Uri;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "android.resource://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "/"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget p0, Lcom/android/settings/R$raw;->media_volume:I

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    return-object p0
.end method

.method private updateSlidersAndMutedStates()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/android/settings/sound/RingerVolumeFragment;->SEEKBAR_TYPE:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    invoke-direct {p0, v0}, Lcom/android/settings/sound/RingerVolumeFragment;->updateSlidersAndMutedStates(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private updateSlidersAndMutedStates(I)V
    .locals 4

    sget-object v0, Lcom/android/settings/sound/RingerVolumeFragment;->SEEKBAR_TYPE:[I

    aget v0, v0, p1

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v0}, Landroid/media/AudioManager;->isStreamMute(I)Z

    move-result v1

    iget-object v2, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mCheckBoxes:[Landroid/widget/ImageView;

    aget-object v2, v2, p1

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mAudioManager:Landroid/media/AudioManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->shouldVibrate(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mCheckBoxes:[Landroid/widget/ImageView;

    aget-object v1, v1, p1

    sget v2, Lcom/android/settings/R$drawable;->ic_audio_ring_notif_vibrate:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_0
    iget-object v2, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mCheckBoxes:[Landroid/widget/ImageView;

    aget-object v2, v2, p1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/android/settings/sound/RingerVolumeFragment;->SEEKBAR_MUTED_RES_ID:[I

    aget v1, v1, p1

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/android/settings/sound/RingerVolumeFragment;->SEEKBAR_UNMUTED_RES_ID:[I

    aget v1, v1, p1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mSeekBars:[Lmiuix/androidbasewidget/widget/SeekBar;

    aget-object v1, v1, p1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v0}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mSeekBarVolumizer:[Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;

    aget-object v1, v1, p1

    iget-object v2, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mSeekBars:[Lmiuix/androidbasewidget/widget/SeekBar;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;->getVolume(I)I

    move-result v1

    if-eq v0, v1, :cond_3

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mSeekBars:[Lmiuix/androidbasewidget/widget/SeekBar;

    aget-object v1, v1, p1

    iget-object p0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mSeekBarVolumizer:[Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;

    aget-object p0, p0, p1

    invoke-virtual {p0, v0}, Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;->getProgress(I)I

    move-result p0

    invoke-virtual {v1, p0}, Landroid/widget/SeekBar;->setProgress(I)V

    :cond_3
    return-void
.end method


# virtual methods
.method public doInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 0

    sget p0, Lcom/android/settings/R$layout;->preference_dialog_ringervolume_miui:I

    const/4 p3, 0x0

    invoke-virtual {p1, p0, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method doRestoreAllVolumes()V
    .locals 2

    goto/32 :goto_f

    nop

    :goto_0
    return-void

    :goto_1
    sget-object v1, Lcom/android/settings/sound/RingerVolumeFragment;->SECTION_ID:[I

    goto/32 :goto_c

    nop

    :goto_2
    aget-object v1, v1, v0

    goto/32 :goto_d

    nop

    :goto_3
    aget-object v1, v1, v0

    goto/32 :goto_9

    nop

    :goto_4
    invoke-direct {p0}, Lcom/android/settings/sound/RingerVolumeFragment;->updateSlidersAndMutedStates()V

    goto/32 :goto_0

    nop

    :goto_5
    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mSeekBarVolumizer:[Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;

    goto/32 :goto_3

    nop

    :goto_6
    goto :goto_10

    :goto_7
    goto/32 :goto_4

    nop

    :goto_8
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_6

    nop

    :goto_9
    invoke-virtual {v1}, Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;->revertStreamVolume()V

    :goto_a
    goto/32 :goto_8

    nop

    :goto_b
    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mSeekBarVolumizer:[Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;

    goto/32 :goto_2

    nop

    :goto_c
    array-length v1, v1

    goto/32 :goto_11

    nop

    :goto_d
    if-nez v1, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_e

    nop

    :goto_e
    invoke-virtual {v1}, Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;->stopSample()V

    goto/32 :goto_5

    nop

    :goto_f
    const/4 v0, 0x0

    :goto_10
    goto/32 :goto_1

    nop

    :goto_11
    if-lt v0, v1, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_b

    nop
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    sget p0, Lcom/android/settings/R$string;->volume_restore:I

    const/4 p2, 0x0

    const/4 v0, 0x1

    invoke-interface {p1, p2, v0, p2, p0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object p0

    sget p1, Lcom/android/settings/R$drawable;->action_button_reset_volume:I

    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const/4 p1, 0x5

    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/sound/RingerVolumeFragment;->cleanup()V

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/16 p0, 0x18

    if-eq p2, p0, :cond_0

    const/16 p0, 0x19

    if-eq p2, p0, :cond_0

    const/16 p0, 0xa4

    if-eq p2, p0, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    const/4 p0, 0x1

    return p0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p0

    return p0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/sound/RingerVolumeFragment;->restoreAllVolumes()V

    return v1
.end method

.method public onPause()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mSeekBarVolumizer:[Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    invoke-virtual {v3}, Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;->stopSample()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onPause()V

    return-void
.end method

.method protected onSampleStarting(Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;)V
    .locals 3

    iget-object p0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mSeekBarVolumizer:[Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;

    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget-object v2, p0, v1

    if-eqz v2, :cond_0

    if-eq v2, p1, :cond_0

    invoke-virtual {v2}, Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;->stopSample()V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 10

    invoke-super {p0, p1, p2}, Lcom/android/settings/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    sget-object p2, Lcom/android/settings/sound/RingerVolumeFragment;->SECTION_ID:[I

    array-length p2, p2

    new-array p2, p2, [Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;

    iput-object p2, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mSeekBarVolumizer:[Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    const-string v0, "audio"

    invoke-virtual {p2, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/media/AudioManager;

    iput-object p2, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mAudioManager:Landroid/media/AudioManager;

    const/4 p2, 0x0

    :goto_0
    sget-object v0, Lcom/android/settings/sound/RingerVolumeFragment;->SECTION_ID:[I

    array-length v1, v0

    if-ge p2, v1, :cond_1

    aget v0, v0, p2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/android/settings/R$id;->volume_seekbar:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lmiuix/androidbasewidget/widget/SeekBar;

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mSeekBars:[Lmiuix/androidbasewidget/widget/SeekBar;

    aput-object v5, v1, p2

    sget-object v1, Lcom/android/settings/sound/RingerVolumeFragment;->SEEKBAR_TYPE:[I

    aget v2, v1, p2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    iget-object v8, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mSeekBarVolumizer:[Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;

    new-instance v9, Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v4

    aget v6, v1, p2

    invoke-direct {p0}, Lcom/android/settings/sound/RingerVolumeFragment;->getMediaVolumeUri()Landroid/net/Uri;

    move-result-object v7

    move-object v2, v9

    move-object v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;-><init>(Lcom/android/settings/sound/RingerVolumeFragment;Landroid/content/Context;Lmiuix/androidbasewidget/widget/SeekBar;ILandroid/net/Uri;)V

    aput-object v9, v8, p2

    goto :goto_1

    :cond_0
    iget-object v2, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mSeekBarVolumizer:[Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;

    new-instance v3, Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v4

    aget v1, v1, p2

    invoke-direct {v3, p0, v4, v5, v1}, Lcom/android/settings/sound/RingerVolumeFragment$SeekBarVolumizer;-><init>(Lcom/android/settings/sound/RingerVolumeFragment;Landroid/content/Context;Lmiuix/androidbasewidget/widget/SeekBar;I)V

    aput-object v3, v2, p2

    :goto_1
    sget v1, Lcom/android/settings/R$id;->mute_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mCheckBoxes:[Landroid/widget/ImageView;

    aput-object v1, v2, p2

    sget v1, Lcom/android/settings/R$id;->description_text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->DESCPTION_ID:[I

    aget v1, v1, p2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/sound/RingerVolumeFragment;->updateSlidersAndMutedStates()V

    iget-object p2, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mRingModeChangedReceiver:Landroid/content/BroadcastReceiver;

    if-nez p2, :cond_2

    new-instance p2, Landroid/content/IntentFilter;

    invoke-direct {p2}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {p2, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v0, Lcom/android/settings/sound/RingerVolumeFragment$2;

    invoke-direct {v0, p0}, Lcom/android/settings/sound/RingerVolumeFragment$2;-><init>(Lcom/android/settings/sound/RingerVolumeFragment;)V

    iput-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mRingModeChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mRingModeChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1, p2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_2
    iget-object p2, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mVolumeChangedReceiver:Landroid/content/BroadcastReceiver;

    if-nez p2, :cond_3

    new-instance p2, Landroid/content/IntentFilter;

    invoke-direct {p2}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "android.media.VOLUME_CHANGED_ACTION"

    invoke-virtual {p2, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v0, Lcom/android/settings/sound/RingerVolumeFragment$3;

    invoke-direct {v0, p0}, Lcom/android/settings/sound/RingerVolumeFragment$3;-><init>(Lcom/android/settings/sound/RingerVolumeFragment;)V

    iput-object v0, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mVolumeChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/sound/RingerVolumeFragment;->mVolumeChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1, p2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    invoke-static {p2}, Lcom/android/settings/Utils;->isVoiceCapable(Landroid/content/Context;)Z

    move-result p2

    const/16 v0, 0x8

    if-nez p2, :cond_4

    sget p2, Lcom/android/settings/R$id;->ringer_section:I

    sget v1, Lcom/android/settings/R$id;->voice_section:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    sget v1, Lcom/android/settings/R$id;->bluetooth_section:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    sget v1, Lcom/android/settings/R$id;->alarm_section:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_4
    sget p2, Lcom/android/settings/R$id;->notification_section:I

    :goto_2
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    const/4 p0, 0x1

    invoke-virtual {p1, p0}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    return-void
.end method

.method restoreAllVolumes()V
    .locals 3

    goto/32 :goto_6

    nop

    :goto_0
    invoke-virtual {v0, p0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p0

    goto/32 :goto_9

    nop

    :goto_1
    const/4 v1, 0x1

    goto/32 :goto_3

    nop

    :goto_2
    const/4 v2, 0x0

    goto/32 :goto_d

    nop

    :goto_3
    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setCancelable(Z)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    goto/32 :goto_c

    nop

    :goto_4
    new-instance v1, Lcom/android/settings/sound/RingerVolumeFragment$4;

    goto/32 :goto_5

    nop

    :goto_5
    invoke-direct {v1, p0}, Lcom/android/settings/sound/RingerVolumeFragment$4;-><init>(Lcom/android/settings/sound/RingerVolumeFragment;)V

    goto/32 :goto_11

    nop

    :goto_6
    new-instance v0, Lmiuix/appcompat/app/AlertDialog$Builder;

    goto/32 :goto_7

    nop

    :goto_7
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_8
    invoke-direct {v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    goto/32 :goto_a

    nop

    :goto_9
    sget v0, Lcom/android/settings/R$string;->volume_restore_alert:I

    goto/32 :goto_b

    nop

    :goto_a
    sget v1, Lcom/android/settings/R$string;->volume_restore:I

    goto/32 :goto_10

    nop

    :goto_b
    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p0

    goto/32 :goto_f

    nop

    :goto_c
    const/high16 v1, 0x1040000

    goto/32 :goto_2

    nop

    :goto_d
    invoke-virtual {v0, v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_e
    return-void

    :goto_f
    invoke-virtual {p0}, Lmiuix/appcompat/app/AlertDialog$Builder;->show()Lmiuix/appcompat/app/AlertDialog;

    goto/32 :goto_e

    nop

    :goto_10
    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_11
    const p0, 0x104000a

    goto/32 :goto_0

    nop
.end method
