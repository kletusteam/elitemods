.class public Lcom/android/settings/sound/HapticMiddleEnabledSeekBarPreference;
.super Lcom/android/settingslib/miuisettings/preference/Preference;

# interfaces
.implements Lcom/android/settingslib/miuisettings/preference/PreferenceFeature;
.implements Lcom/android/settings/widget/MiuiHapticInfinitySeekBar$SeekBarProgressChangeListener;
.implements Lcom/android/settings/sound/IHapticVideoPlaying;


# static fields
.field private static final VIBRATION_ATTRIBUTES:Landroid/os/VibrationAttributes;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHapticFeedbackUtil:Lmiui/util/HapticFeedbackUtil;

.field private mIsHapticVideoPlaying:Z

.field private mSeekBar:Landroid/widget/SeekBar;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/os/VibrationAttributes$Builder;

    invoke-direct {v0}, Landroid/os/VibrationAttributes$Builder;-><init>()V

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/os/VibrationAttributes$Builder;->setUsage(I)Landroid/os/VibrationAttributes$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/VibrationAttributes$Builder;->build()Landroid/os/VibrationAttributes;

    move-result-object v0

    sput-object v0, Lcom/android/settings/sound/HapticMiddleEnabledSeekBarPreference;->VIBRATION_ATTRIBUTES:Landroid/os/VibrationAttributes;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/sound/HapticMiddleEnabledSeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/sound/HapticMiddleEnabledSeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settingslib/miuisettings/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget p1, Lcom/android/settings/R$layout;->preference_haptic_middle_enabled_seekbar:I

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setLayoutResource(I)V

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/sound/HapticMiddleEnabledSeekBarPreference;->mContext:Landroid/content/Context;

    new-instance p1, Lmiui/util/HapticFeedbackUtil;

    iget-object p2, p0, Lcom/android/settings/sound/HapticMiddleEnabledSeekBarPreference;->mContext:Landroid/content/Context;

    const/4 p3, 0x1

    invoke-direct {p1, p2, p3}, Lmiui/util/HapticFeedbackUtil;-><init>(Landroid/content/Context;Z)V

    iput-object p1, p0, Lcom/android/settings/sound/HapticMiddleEnabledSeekBarPreference;->mHapticFeedbackUtil:Lmiui/util/HapticFeedbackUtil;

    return-void
.end method

.method public static getHapticLevel(Landroid/content/Context;)F
    .locals 2

    const/high16 v0, 0x3f800000    # 1.0f

    if-nez p0, :cond_0

    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v1, "haptic_feedback_infinite_intensity"

    invoke-static {p0, v1, v0}, Landroid/provider/Settings$System;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result p0

    return p0
.end method

.method private performHaptic()V
    .locals 3

    iget-boolean v0, p0, Lcom/android/settings/sound/HapticMiddleEnabledSeekBarPreference;->mIsHapticVideoPlaying:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object p0, p0, Lcom/android/settings/sound/HapticMiddleEnabledSeekBarPreference;->mHapticFeedbackUtil:Lmiui/util/HapticFeedbackUtil;

    sget-object v0, Lcom/android/settings/sound/HapticMiddleEnabledSeekBarPreference;->VIBRATION_ATTRIBUTES:Landroid/os/VibrationAttributes;

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lmiui/util/HapticFeedbackUtil;->performExtHapticFeedback(Landroid/os/VibrationAttributes;IZ)Z

    return-void
.end method


# virtual methods
.method public hasIcon()Z
    .locals 0

    const/4 p0, 0x1

    return p0
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/android/settingslib/miuisettings/preference/Preference;->onBindView(Landroid/view/View;)V

    invoke-static {}, Lcom/android/settings/MiuiUtils;->isMiuiSdkSupportFolme()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    new-array v0, v1, [Landroid/view/View;

    aput-object p1, v0, v2

    invoke-static {v0}, Lmiuix/animation/Folme;->clean([Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p1, v2, v0, v2, v3}, Landroid/view/View;->setPaddingRelative(IIII)V

    sget v0, Lcom/android/settings/R$id;->seekbar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/MiuiHapticInfinitySeekBar;

    invoke-virtual {v0, v2, v2}, Lmiuix/androidbasewidget/widget/SeekBar;->setForegroundPrimaryColor(II)V

    const/16 v2, 0x64

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setMax(I)V

    invoke-virtual {v0, p0}, Lcom/android/settings/widget/MiuiHapticInfinitySeekBar;->setFontWeightChangeListener(Lcom/android/settings/widget/MiuiHapticInfinitySeekBar$SeekBarProgressChangeListener;)V

    const v2, 0x1020006

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/android/settings/sound/VolumeStreamStateView;

    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getAlpha()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :cond_1
    invoke-virtual {p0, v1}, Landroidx/preference/Preference;->setEnabled(Z)V

    iput-object v0, p0, Lcom/android/settings/sound/HapticMiddleEnabledSeekBarPreference;->mSeekBar:Landroid/widget/SeekBar;

    return-void
.end method

.method public onSeekBarProgressChange(I)V
    .locals 1

    iget-boolean p1, p0, Lcom/android/settings/sound/HapticMiddleEnabledSeekBarPreference;->mIsHapticVideoPlaying:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/sound/HapticMiddleEnabledSeekBarPreference;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/sound/VibratorFeatureUtil;->getInstance(Landroid/content/Context;)Lcom/android/settings/sound/VibratorFeatureUtil;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/sound/HapticMiddleEnabledSeekBarPreference;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/sound/HapticMiddleEnabledSeekBarPreference;->getHapticLevel(Landroid/content/Context;)F

    move-result v0

    invoke-virtual {p1, v0}, Lcom/android/settings/sound/VibratorFeatureUtil;->setAmplitude(F)V

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/sound/HapticMiddleEnabledSeekBarPreference;->performHaptic()V

    return-void
.end method

.method public onSeekBarStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/sound/HapticMiddleEnabledSeekBarPreference;->performHaptic()V

    return-void
.end method

.method public onSeekBarStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/sound/HapticMiddleEnabledSeekBarPreference;->performHaptic()V

    return-void
.end method

.method public setIsHapticVideoPlaying(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/sound/HapticMiddleEnabledSeekBarPreference;->mIsHapticVideoPlaying:Z

    iget-object p0, p0, Lcom/android/settings/sound/HapticMiddleEnabledSeekBarPreference;->mSeekBar:Landroid/widget/SeekBar;

    if-nez p0, :cond_0

    return-void

    :cond_0
    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {p0, p1}, Landroid/widget/SeekBar;->setHapticFeedbackEnabled(Z)V

    return-void
.end method
