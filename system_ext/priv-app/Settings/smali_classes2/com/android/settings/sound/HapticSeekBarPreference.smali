.class public Lcom/android/settings/sound/HapticSeekBarPreference;
.super Lcom/android/settings/widget/SeekBarPreference;

# interfaces
.implements Lcom/android/settings/sound/IHapticVideoPlaying;


# instance fields
.field private disableForegroundColor:I

.field private mContext:Landroid/content/Context;

.field private mDegreePerGear:F

.field private mHapticFeedbackUtil:Lmiui/util/HapticFeedbackUtil;

.field private mIsHapticVideoPlaying:Z

.field private mLastLevel:F

.field private mMinProgress:I

.field private mSeekBar:Landroid/widget/SeekBar;

.field private primaryForegroundColor:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/settings/sound/HapticSeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/sound/HapticSeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/widget/SeekBarPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/high16 p1, -0x40800000    # -1.0f

    iput p1, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->mLastLevel:F

    const/4 p1, 0x0

    iput p1, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->mMinProgress:I

    sget p1, Lcom/android/settings/R$layout;->preference_volume_seekbar:I

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setLayoutResource(I)V

    const/16 p1, 0x3e8

    invoke-virtual {p0, p1}, Lcom/android/settings/widget/SeekBarPreference;->setMax(I)V

    const p1, 0x3ca3d70a    # 0.02f

    invoke-direct {p0, p1}, Lcom/android/settings/sound/HapticSeekBarPreference;->toFloaWith2Bit(F)F

    move-result p1

    iput p1, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->mDegreePerGear:F

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->mContext:Landroid/content/Context;

    new-instance p1, Lmiui/util/HapticFeedbackUtil;

    iget-object p2, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->mContext:Landroid/content/Context;

    const/4 p3, 0x1

    invoke-direct {p1, p2, p3}, Lmiui/util/HapticFeedbackUtil;-><init>(Landroid/content/Context;Z)V

    iput-object p1, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->mHapticFeedbackUtil:Lmiui/util/HapticFeedbackUtil;

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object p1

    sget p2, Lcom/android/settings/R$color;->miuix_appcompat_progress_disable_color_light:I

    invoke-virtual {p1, p2}, Landroid/content/Context;->getColor(I)I

    move-result p1

    iput p1, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->disableForegroundColor:I

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object p1

    sget p2, Lcom/android/settings/R$color;->miuix_appcompat_progress_primary_colors_light:I

    invoke-virtual {p1, p2}, Landroid/content/Context;->getColor(I)I

    move-result p1

    iput p1, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->primaryForegroundColor:I

    return-void
.end method

.method private getHapticLevel()F
    .locals 2

    iget-object p0, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "haptic_feedback_infinite_intensity"

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$System;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result p0

    return p0
.end method

.method private performHaptic()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->mIsHapticVideoPlaying:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object p0, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->mHapticFeedbackUtil:Lmiui/util/HapticFeedbackUtil;

    const/4 v0, 0x0

    const-string/jumbo v1, "mesh_light"

    invoke-virtual {p0, v1, v0}, Lmiui/util/HapticFeedbackUtil;->performHapticFeedback(Ljava/lang/String;Z)Z

    return-void
.end method

.method private progressToLevel(I)F
    .locals 2

    div-int/lit8 v0, p1, 0x14

    int-to-float v0, v0

    iget v1, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->mDegreePerGear:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/android/settings/sound/HapticSeekBarPreference;->toFloaWith2Bit(F)F

    move-result v0

    cmpl-float v1, v0, v1

    if-nez v1, :cond_0

    if-eqz p1, :cond_0

    iget p0, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->mDegreePerGear:F

    add-float/2addr v0, p0

    :cond_0
    return v0
.end method

.method private setHapticLevel(F)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->mIsHapticVideoPlaying:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/sound/VibratorFeatureUtil;->getInstance(Landroid/content/Context;)Lcom/android/settings/sound/VibratorFeatureUtil;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/settings/sound/VibratorFeatureUtil;->setAmplitude(F)V

    :cond_0
    iget-object p0, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "haptic_feedback_infinite_intensity"

    invoke-static {p0, v0, p1}, Landroid/provider/Settings$System;->putFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)Z

    return-void
.end method

.method private toFloaWith2Bit(F)F
    .locals 0

    const/high16 p0, 0x42c80000    # 100.0f

    mul-float/2addr p1, p0

    float-to-int p1, p1

    int-to-float p1, p1

    div-float/2addr p1, p0

    return p1
.end method


# virtual methods
.method public hasIcon()Z
    .locals 0

    const/4 p0, 0x1

    return p0
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 7

    invoke-super {p0, p1}, Lcom/android/settingslib/miuisettings/preference/Preference;->onBindView(Landroid/view/View;)V

    invoke-static {}, Lcom/android/settings/MiuiUtils;->isMiuiSdkSupportFolme()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    new-array v0, v1, [Landroid/view/View;

    aput-object p1, v0, v2

    invoke-static {v0}, Lmiuix/animation/Folme;->clean([Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p1, v2, v0, v2, v3}, Landroid/view/View;->setPaddingRelative(IIII)V

    sget v0, Lcom/android/settings/R$id;->seekbar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/androidbasewidget/widget/SeekBar;

    iput-object v0, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->mSeekBar:Landroid/widget/SeekBar;

    sget v3, Lcom/android/settings/R$id;->title_view:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getPaddingStart()I

    move-result v3

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getPaddingStart()I

    move-result v4

    sub-int/2addr v3, v4

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/android/settings/R$dimen;->volume_seekbar_margin_bottom:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v4, v3, v2, v3, v5}, Landroid/widget/LinearLayout$LayoutParams;->setMarginsRelative(IIII)V

    iget-object v3, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/settings/utils/SettingsFeatures;->isSplitTablet(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    const v3, 0x1020006

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/settings/sound/VolumeStreamStateView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getAlpha()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :cond_2
    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v3

    if-nez v3, :cond_3

    invoke-direct {p0}, Lcom/android/settings/sound/HapticSeekBarPreference;->getHapticLevel()F

    move-result v3

    const/high16 v4, 0x3f000000    # 0.5f

    sub-float/2addr v3, v4

    const/high16 v4, 0x447a0000    # 1000.0f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    invoke-virtual {p0, v3, v2}, Lcom/android/settings/widget/SeekBarPreference;->setProgress(IZ)V

    :cond_3
    invoke-virtual {v0, p0}, Lmiuix/androidbasewidget/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/settings/utils/SettingsFeatures;->isSupportSettingsHaptic(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v2, Lcom/android/settings/sound/HapticSeekBarPreference$1;

    invoke-direct {v2, p0}, Lcom/android/settings/sound/HapticSeekBarPreference$1;-><init>(Lcom/android/settings/sound/HapticSeekBarPreference;)V

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_4
    invoke-virtual {p0}, Lcom/android/settingslib/RestrictedPreference;->isDisabledByAdmin()Z

    move-result v2

    if-eqz v2, :cond_5

    new-instance v2, Lcom/android/settings/sound/HapticSeekBarPreference$2;

    invoke-direct {v2, p0}, Lcom/android/settings/sound/HapticSeekBarPreference$2;-><init>(Lcom/android/settings/sound/HapticSeekBarPreference;)V

    invoke-virtual {p1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_5
    invoke-virtual {p0, v1}, Lcom/android/settingslib/RestrictedPreference;->setEnabled(Z)V

    iget p1, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->primaryForegroundColor:I

    iget p0, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->disableForegroundColor:I

    invoke-virtual {v0, p1, p0}, Lmiuix/androidbasewidget/widget/SeekBar;->setForegroundPrimaryColor(II)V

    return-void
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 1

    if-eqz p3, :cond_2

    iget p3, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->mMinProgress:I

    const/4 v0, 0x0

    if-ge p2, p3, :cond_0

    invoke-virtual {p1, p3, v0}, Landroid/widget/SeekBar;->setProgress(IZ)V

    return-void

    :cond_0
    invoke-direct {p0, p2}, Lcom/android/settings/sound/HapticSeekBarPreference;->progressToLevel(I)F

    move-result p1

    iget p3, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->mLastLevel:F

    cmpl-float p3, p3, p1

    if-eqz p3, :cond_1

    iput p1, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->mLastLevel:F

    invoke-direct {p0, p1}, Lcom/android/settings/sound/HapticSeekBarPreference;->setHapticLevel(F)V

    invoke-direct {p0}, Lcom/android/settings/sound/HapticSeekBarPreference;->performHaptic()V

    :cond_1
    invoke-virtual {p0, p2, v0}, Lcom/android/settings/widget/SeekBarPreference;->setProgress(IZ)V

    :cond_2
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/sound/HapticSeekBarPreference;->performHaptic()V

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/sound/HapticSeekBarPreference;->performHaptic()V

    return-void
.end method

.method public setIsHapticVideoPlaying(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->mIsHapticVideoPlaying:Z

    iget-object p0, p0, Lcom/android/settings/sound/HapticSeekBarPreference;->mSeekBar:Landroid/widget/SeekBar;

    if-nez p0, :cond_0

    return-void

    :cond_0
    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {p0, p1}, Landroid/widget/SeekBar;->setHapticFeedbackEnabled(Z)V

    return-void
.end method
