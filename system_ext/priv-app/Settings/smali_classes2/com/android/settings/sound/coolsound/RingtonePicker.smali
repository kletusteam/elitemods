.class public Lcom/android/settings/sound/coolsound/RingtonePicker;
.super Ljava/lang/Object;


# instance fields
.field private drawableId:I

.field private isDisable:Z

.field private resType:I

.field private ringtoneTitle:Ljava/lang/String;

.field private ringtoneValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(IILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/settings/sound/coolsound/RingtonePicker;->resType:I

    iput p2, p0, Lcom/android/settings/sound/coolsound/RingtonePicker;->drawableId:I

    iput-object p3, p0, Lcom/android/settings/sound/coolsound/RingtonePicker;->ringtoneTitle:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getDrawableId()I
    .locals 0

    iget p0, p0, Lcom/android/settings/sound/coolsound/RingtonePicker;->drawableId:I

    return p0
.end method

.method public getResType()I
    .locals 0

    iget p0, p0, Lcom/android/settings/sound/coolsound/RingtonePicker;->resType:I

    return p0
.end method

.method public getRingtoneTitle()Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/sound/coolsound/RingtonePicker;->ringtoneTitle:Ljava/lang/String;

    return-object p0
.end method

.method public getRingtoneValue()Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/sound/coolsound/RingtonePicker;->ringtoneValue:Ljava/lang/String;

    return-object p0
.end method

.method public isDisable()Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/sound/coolsound/RingtonePicker;->isDisable:Z

    return p0
.end method

.method public setDisable(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/sound/coolsound/RingtonePicker;->isDisable:Z

    return-void
.end method

.method public setRingtoneValue(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/sound/coolsound/RingtonePicker;->ringtoneValue:Ljava/lang/String;

    return-void
.end method
