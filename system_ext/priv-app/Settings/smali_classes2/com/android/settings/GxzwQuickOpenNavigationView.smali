.class public Lcom/android/settings/GxzwQuickOpenNavigationView;
.super Landroid/widget/RelativeLayout;


# instance fields
.field private mFastTitle:Landroid/widget/TextView;

.field private mPlayCount:I

.field private mSlowTitle:Landroid/widget/TextView;

.field private mVideoView:Lcom/android/settings/MutedVideoView;


# direct methods
.method static bridge synthetic -$$Nest$fgetmVideoView(Lcom/android/settings/GxzwQuickOpenNavigationView;)Lcom/android/settings/MutedVideoView;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mVideoView:Lcom/android/settings/MutedVideoView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmPlayCount(Lcom/android/settings/GxzwQuickOpenNavigationView;I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mPlayCount:I

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdatePlayState(Lcom/android/settings/GxzwQuickOpenNavigationView;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/GxzwQuickOpenNavigationView;->updatePlayState()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    iput p1, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mPlayCount:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    iput p1, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mPlayCount:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x0

    iput p1, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mPlayCount:I

    return-void
.end method

.method private setVideoURI(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mVideoView:Lcom/android/settings/MutedVideoView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "android.resource://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "/raw/"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/android/settings/MutedVideoView;->setVideoURI(Landroid/net/Uri;)V

    return-void
.end method

.method private updatePlayState()V
    .locals 3

    iget v0, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mPlayCount:I

    rem-int/lit8 v0, v0, 0x6

    iput v0, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mPlayCount:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    const-string v0, "gxzw_quick_open_navi_slow"

    invoke-direct {p0, v0}, Lcom/android/settings/GxzwQuickOpenNavigationView;->setVideoURI(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mSlowTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$color;->gxzw_quick_open_title_select:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mFastTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$color;->gxzw_quick_open_title_unselect:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :cond_0
    const-string v0, "gxzw_quick_open_navi_fast"

    invoke-direct {p0, v0}, Lcom/android/settings/GxzwQuickOpenNavigationView;->setVideoURI(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mSlowTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$color;->gxzw_quick_open_title_unselect:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mFastTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$color;->gxzw_quick_open_title_select:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    iget v0, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mPlayCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mPlayCount:I

    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    iget-object p0, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mVideoView:Lcom/android/settings/MutedVideoView;

    invoke-virtual {p0}, Lcom/android/settings/MutedVideoView;->suspend()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    sget v0, Lcom/android/settings/R$id;->gxzw_quick_open_preview:I

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/MutedVideoView;

    iput-object v0, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mVideoView:Lcom/android/settings/MutedVideoView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setZOrderOnTop(Z)V

    iget-object v0, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mVideoView:Lcom/android/settings/MutedVideoView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, -0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    sget v0, Lcom/android/settings/R$id;->gxzw_quick_open_slow_title:I

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mSlowTitle:Landroid/widget/TextView;

    sget v0, Lcom/android/settings/R$id;->gxzw_quick_open_fast_title:I

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mFastTitle:Landroid/widget/TextView;

    sget v0, Lcom/android/settings/R$id;->gxzw_quick_open_slow_container:I

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/android/settings/GxzwQuickOpenNavigationView$1;

    invoke-direct {v1, p0}, Lcom/android/settings/GxzwQuickOpenNavigationView$1;-><init>(Lcom/android/settings/GxzwQuickOpenNavigationView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/android/settings/R$id;->gxzw_quick_open_fast_container:I

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/android/settings/GxzwQuickOpenNavigationView$2;

    invoke-direct {v1, p0}, Lcom/android/settings/GxzwQuickOpenNavigationView$2;-><init>(Lcom/android/settings/GxzwQuickOpenNavigationView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mVideoView:Lcom/android/settings/MutedVideoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mVideoView:Lcom/android/settings/MutedVideoView;

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setClickable(Z)V

    iget-object v0, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mVideoView:Lcom/android/settings/MutedVideoView;

    new-instance v1, Lcom/android/settings/GxzwQuickOpenNavigationView$3;

    invoke-direct {v1, p0}, Lcom/android/settings/GxzwQuickOpenNavigationView$3;-><init>(Lcom/android/settings/GxzwQuickOpenNavigationView;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/MutedVideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mVideoView:Lcom/android/settings/MutedVideoView;

    new-instance v1, Lcom/android/settings/GxzwQuickOpenNavigationView$4;

    invoke-direct {v1, p0}, Lcom/android/settings/GxzwQuickOpenNavigationView$4;-><init>(Lcom/android/settings/GxzwQuickOpenNavigationView;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/MutedVideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    invoke-direct {p0}, Lcom/android/settings/GxzwQuickOpenNavigationView;->updatePlayState()V

    return-void
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mVideoView:Lcom/android/settings/MutedVideoView;

    invoke-virtual {v0}, Lcom/android/settings/MutedVideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mVideoView:Lcom/android/settings/MutedVideoView;

    invoke-virtual {p0}, Lcom/android/settings/MutedVideoView;->pause()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mVideoView:Lcom/android/settings/MutedVideoView;

    invoke-virtual {v0}, Lcom/android/settings/MutedVideoView;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/GxzwQuickOpenNavigationView;->mVideoView:Lcom/android/settings/MutedVideoView;

    invoke-virtual {p0}, Lcom/android/settings/MutedVideoView;->start()V

    :cond_0
    return-void
.end method
