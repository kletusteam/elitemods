.class public abstract Lcom/android/settings/BaseCardViewController;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/settingslib/core/lifecycle/events/OnResume;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field protected mCard:Lcom/android/settings/CardInfo;

.field protected mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/CardInfo;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/BaseCardViewController;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/BaseCardViewController;->mCard:Lcom/android/settings/CardInfo;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public onResume()V
    .locals 0

    return-void
.end method
