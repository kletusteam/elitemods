.class public Lcom/android/settings/MiuiSoundSettingsActivity;
.super Lcom/android/settings/SettingsActivity;


# instance fields
.field private mActionBar:Lmiuix/appcompat/app/ActionBar;

.field private mBackView:Landroid/widget/ImageView;

.field private mCurrentPosition:I

.field private mHapticFragment:Ljava/lang/Object;

.field private mTitles:[Ljava/lang/String;


# direct methods
.method static bridge synthetic -$$Nest$fgetmCurrentPosition(Lcom/android/settings/MiuiSoundSettingsActivity;)I
    .locals 0

    iget p0, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mCurrentPosition:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmHapticFragment(Lcom/android/settings/MiuiSoundSettingsActivity;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mHapticFragment:Ljava/lang/Object;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTitles(Lcom/android/settings/MiuiSoundSettingsActivity;)[Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mTitles:[Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmCurrentPosition(Lcom/android/settings/MiuiSoundSettingsActivity;I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mCurrentPosition:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmHapticFragment(Lcom/android/settings/MiuiSoundSettingsActivity;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mHapticFragment:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/SettingsActivity;-><init>()V

    return-void
.end method

.method public static varargs callObjectMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Class<",
            "*>;[",
            "Ljava/lang/Object;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NoSuchMethodException;,
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/reflect/InvocationTargetException;
        }
    .end annotation

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object p1

    invoke-virtual {p1, p0, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private initActionBar()V
    .locals 9

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mActionBar:Lmiuix/appcompat/app/ActionBar;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    sget v1, Lcom/android/settings/R$string;->sound_settings_tab_sound:I

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget v1, Lcom/android/settings/R$string;->sound_settings_tab_haptic:I

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mTitles:[Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/settings/MiuiSoundSettingsActivity;->initActionBarBackView()V

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mActionBar:Lmiuix/appcompat/app/ActionBar;

    invoke-virtual {v0, p0, v2}, Lmiuix/appcompat/app/ActionBar;->setFragmentViewPagerMode(Landroidx/fragment/app/FragmentActivity;Z)V

    iget-object v3, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mActionBar:Lmiuix/appcompat/app/ActionBar;

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mTitles:[Ljava/lang/String;

    aget-object v4, v0, v2

    invoke-virtual {v3}, Landroidx/appcompat/app/ActionBar;->newTab()Landroidx/appcompat/app/ActionBar$Tab;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mTitles:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroidx/appcompat/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroidx/appcompat/app/ActionBar$Tab;

    move-result-object v5

    const-class v6, Lcom/android/settings/MiuiSoundSettings;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Lmiuix/appcompat/app/ActionBar;->addFragmentTab(Ljava/lang/String;Landroidx/appcompat/app/ActionBar$Tab;Ljava/lang/Class;Landroid/os/Bundle;Z)I

    invoke-direct {p0}, Lcom/android/settings/MiuiSoundSettingsActivity;->initHapticFragment()V

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mActionBar:Lmiuix/appcompat/app/ActionBar;

    new-instance v1, Lcom/android/settings/MiuiSoundSettingsActivity$1;

    invoke-direct {v1, p0}, Lcom/android/settings/MiuiSoundSettingsActivity$1;-><init>(Lcom/android/settings/MiuiSoundSettingsActivity;)V

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/ActionBar;->addOnFragmentViewPagerChangeListener(Lmiuix/appcompat/app/ActionBar$FragmentViewPagerChangeListener;)V

    invoke-virtual {p0}, Lcom/android/settings/SettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mActionBar:Lmiuix/appcompat/app/ActionBar;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_tab_position"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mActionBar:Lmiuix/appcompat/app/ActionBar;

    invoke-virtual {v1}, Lmiuix/appcompat/app/ActionBar;->getFragmentTabCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mActionBar:Lmiuix/appcompat/app/ActionBar;

    invoke-virtual {p0, v0}, Landroidx/appcompat/app/ActionBar;->setSelectedNavigationItem(I)V

    :cond_0
    return-void
.end method

.method private initActionBarBackView()V
    .locals 2

    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mBackView:Landroid/widget/ImageView;

    sget v1, Lcom/android/settings/R$string;->back_button:I

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mBackView:Landroid/widget/ImageView;

    new-instance v1, Lcom/android/settings/MiuiSoundSettingsActivity$2;

    invoke-direct {v1, p0}, Lcom/android/settings/MiuiSoundSettingsActivity$2;-><init>(Lcom/android/settings/MiuiSoundSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {p0}, Lcom/android/settings/display/DarkModeTimeModeUtil;->isDarkModeEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mBackView:Landroid/widget/ImageView;

    sget v1, Lcom/android/settings/R$drawable;->miuix_appcompat_action_bar_back_dark:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mBackView:Landroid/widget/ImageView;

    sget v1, Lcom/android/settings/R$drawable;->miuix_appcompat_action_bar_back_light:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mActionBar:Lmiuix/appcompat/app/ActionBar;

    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mBackView:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Lmiuix/appcompat/app/ActionBar;->setStartView(Landroid/view/View;)V

    return-void
.end method

.method private initHapticFragment()V
    .locals 7

    :try_start_0
    const-string v0, "com.android.settings.haptic.HapticFragment"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    const/4 v0, 0x0

    :goto_0
    move-object v4, v0

    if-eqz v4, :cond_0

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mActionBar:Lmiuix/appcompat/app/ActionBar;

    iget-object v0, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mTitles:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v0, v0, v2

    invoke-virtual {v1}, Landroidx/appcompat/app/ActionBar;->newTab()Landroidx/appcompat/app/ActionBar$Tab;

    move-result-object v3

    iget-object p0, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mTitles:[Ljava/lang/String;

    aget-object p0, p0, v2

    invoke-virtual {v3, p0}, Landroidx/appcompat/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroidx/appcompat/app/ActionBar$Tab;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, v0

    invoke-virtual/range {v1 .. v6}, Lmiuix/appcompat/app/ActionBar;->addFragmentTab(Ljava/lang/String;Landroidx/appcompat/app/ActionBar$Tab;Ljava/lang/Class;Landroid/os/Bundle;Z)I

    :cond_0
    return-void
.end method


# virtual methods
.method public getCurrentPage()I
    .locals 0

    iget p0, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mCurrentPosition:I

    return p0
.end method

.method protected needToLaunchSettingsFragment()Z
    .locals 0

    invoke-static {p0}, Lcom/android/settings/utils/SettingsFeatures;->isSupportSettingsHaptic(Landroid/content/Context;)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    return p0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-static {p0}, Lcom/android/settings/utils/SettingsFeatures;->isSupportSettingsHaptic(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/android/settings/R$string;->sound_haptic_settings:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/core/SettingsBaseActivity;->setTitle(Ljava/lang/CharSequence;)V

    invoke-super {p0, p1}, Lcom/android/settings/SettingsActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/android/settings/MiuiSoundSettingsActivity;->initActionBar()V

    goto :goto_1

    :cond_0
    const-string/jumbo v0, "vibrator"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lcom/android/settings/R$string;->sound_vibrate_settings:I

    goto :goto_0

    :cond_1
    sget v0, Lcom/android/settings/R$string;->sound_settings:I

    :goto_0
    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/core/SettingsBaseActivity;->setTitle(Ljava/lang/CharSequence;)V

    invoke-super {p0, p1}, Lcom/android/settings/SettingsActivity;->onCreate(Landroid/os/Bundle;)V

    :goto_1
    return-void
.end method

.method protected onResume()V
    .locals 5

    invoke-super {p0}, Lcom/android/settings/SettingsActivity;->onResume()V

    invoke-static {p0}, Lcom/android/settings/utils/SettingsFeatures;->isSupportSettingsHaptic(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mTitles:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mHapticFragment:Ljava/lang/Object;

    if-nez v0, :cond_1

    return-void

    :cond_1
    iget p0, p0, Lcom/android/settings/MiuiSoundSettingsActivity;->mCurrentPosition:I

    if-ne p0, v2, :cond_2

    :try_start_0
    const-string/jumbo p0, "onVisible"

    new-array v1, v2, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v3, v1, v4

    new-array v2, v2, [Ljava/lang/Object;

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    aput-object v3, v2, v4

    invoke-static {v0, p0, v1, v2}, Lcom/android/settings/MiuiSoundSettingsActivity;->callObjectMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_2
    :goto_0
    return-void
.end method

.method public setTheme(I)V
    .locals 1

    invoke-static {p0}, Lcom/android/settings/utils/SettingsFeatures;->isSupportSettingsHaptic(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget p1, Lcom/android/settings/R$style;->MiuiAccessibility:I

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->setTheme(I)V

    return-void
.end method
