.class public Lcom/android/settings/MiuiDisplaySettings;
.super Lcom/android/settings/DisplaySettings;

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static sUiModeOrder:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAODObserver:Landroid/database/ContentObserver;

.field private mAdvancedPaperModePref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

.field private mAlertDialog:Lmiuix/appcompat/app/AlertDialog;

.field private mContext:Landroid/content/Context;

.field private final mCurConfig:Landroid/content/res/Configuration;

.field private mDarkModeRadioTimePref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

.field private mDarkModeTiming:Landroidx/preference/PreferenceCategory;

.field private mDisableDclightAndHighFps:Z

.field private mFontSettingsPref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

.field private mFontStatusController:Lcom/android/settings/BaseSettingsController;

.field private mHandler:Landroid/os/Handler;

.field private mIsFontSettingEnable:Z

.field private mLineBreaking:Landroidx/preference/CheckBoxPreference;

.field private mMiuiUtils:Lcom/android/settings/MiuiUtils;

.field private mMonochromeModeEnabledObserver:Landroid/database/ContentObserver;

.field private mMoreDarkModeSettings:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

.field private mPageLayoutStatusController:Lcom/android/settings/BaseSettingsController;

.field private mPageLayoutStatusPref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

.field private mPaperModeEnabledObserver:Landroid/database/ContentObserver;

.field private mPaperModePref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

.field private mRotatePreference:Landroidx/preference/CheckBoxPreference;

.field private final mRotationPolicyListener:Lcom/android/internal/view/RotationPolicy$RotationPolicyListener;

.field private mScreenDcPreference:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

.field private mScreenFpsPreference:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

.field private mScreenMonochromeModePref:Landroidx/preference/Preference;

.field private mScreenResolutionPref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

.field private mTouchSensitive:Landroidx/preference/CheckBoxPreference;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAdvancedPaperModePref(Lcom/android/settings/MiuiDisplaySettings;)Lcom/android/settingslib/miuisettings/preference/ValuePreference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/MiuiDisplaySettings;->mAdvancedPaperModePref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/settings/MiuiDisplaySettings;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/MiuiDisplaySettings;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFontSettingsPref(Lcom/android/settings/MiuiDisplaySettings;)Lcom/android/settingslib/miuisettings/preference/ValuePreference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/MiuiDisplaySettings;->mFontSettingsPref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFontStatusController(Lcom/android/settings/MiuiDisplaySettings;)Lcom/android/settings/BaseSettingsController;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/MiuiDisplaySettings;->mFontStatusController:Lcom/android/settings/BaseSettingsController;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/settings/MiuiDisplaySettings;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/MiuiDisplaySettings;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsFontSettingEnable(Lcom/android/settings/MiuiDisplaySettings;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/MiuiDisplaySettings;->mIsFontSettingEnable:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmPageLayoutStatusPref(Lcom/android/settings/MiuiDisplaySettings;)Lcom/android/settingslib/miuisettings/preference/ValuePreference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/MiuiDisplaySettings;->mPageLayoutStatusPref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPaperModePref(Lcom/android/settings/MiuiDisplaySettings;)Lcom/android/settingslib/miuisettings/preference/ValuePreference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/MiuiDisplaySettings;->mPaperModePref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmIsFontSettingEnable(Lcom/android/settings/MiuiDisplaySettings;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mIsFontSettingEnable:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$menableScreenOnProximitySensor(Lcom/android/settings/MiuiDisplaySettings;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiDisplaySettings;->enableScreenOnProximitySensor(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateAccelerometerRotationCheckbox(Lcom/android/settings/MiuiDisplaySettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiDisplaySettings;->updateAccelerometerRotationCheckbox()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateMonochromeMode(Lcom/android/settings/MiuiDisplaySettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/MiuiDisplaySettings;->updateMonochromeMode()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdatePaperMode(Lcom/android/settings/MiuiDisplaySettings;Lcom/android/settingslib/miuisettings/preference/ValuePreference;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiDisplaySettings;->updatePaperMode(Lcom/android/settingslib/miuisettings/preference/ValuePreference;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 4

    const-class v0, Lcom/android/settings/MiuiDisplaySettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/MiuiDisplaySettings;->TAG:Ljava/lang/String;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/android/settings/MiuiDisplaySettings;->sUiModeOrder:Landroid/util/SparseArray;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/android/settings/MiuiDisplaySettings;->sUiModeOrder:Landroid/util/SparseArray;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0xc

    invoke-virtual {v0, v3, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/android/settings/MiuiDisplaySettings;->sUiModeOrder:Landroid/util/SparseArray;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/android/settings/MiuiDisplaySettings;->sUiModeOrder:Landroid/util/SparseArray;

    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/android/settings/MiuiDisplaySettings;->sUiModeOrder:Landroid/util/SparseArray;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xe

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/android/settings/MiuiDisplaySettings;->sUiModeOrder:Landroid/util/SparseArray;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xf

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/android/settings/MiuiDisplaySettings;->sUiModeOrder:Landroid/util/SparseArray;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xb

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/DisplaySettings;-><init>()V

    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    iput-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mCurConfig:Landroid/content/res/Configuration;

    new-instance v0, Lcom/android/settings/MiuiDisplaySettings$1;

    invoke-direct {v0, p0}, Lcom/android/settings/MiuiDisplaySettings$1;-><init>(Lcom/android/settings/MiuiDisplaySettings;)V

    iput-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/settings/MiuiDisplaySettings$2;

    invoke-direct {v0, p0}, Lcom/android/settings/MiuiDisplaySettings$2;-><init>(Lcom/android/settings/MiuiDisplaySettings;)V

    iput-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mRotationPolicyListener:Lcom/android/internal/view/RotationPolicy$RotationPolicyListener;

    return-void
.end method

.method private enableScreenOnProximitySensor(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "enable_screen_on_proximity_sensor"

    invoke-static {p0, v0, p1}, Landroid/provider/MiuiSettings$Global;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    return-void
.end method

.method private getAnimateStatus()Z
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "default_close_unlock_animator"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    const-string v2, "animate_settings_status"

    invoke-static {p0, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    move v1, v0

    :cond_0
    return v1
.end method

.method private isQhdMode()Ljava/lang/String;
    .locals 2

    const-string/jumbo p0, "persist.sys.miui_resolution"

    const/4 v0, 0x0

    invoke-static {p0, v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "WQHD+"

    if-eqz p0, :cond_0

    const-string v1, ""

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ","

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    const/4 v1, 0x0

    aget-object p0, p0, v1

    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p0

    const/16 v1, 0x438

    if-ne p0, v1, :cond_0

    const-string v0, "FHD+"

    :cond_0
    return-object v0
.end method

.method private updateAccelerometerRotationCheckbox()V
    .locals 1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mRotatePreference:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    invoke-static {p0}, Lcom/android/internal/view/RotationPolicy;->isRotationLocked(Landroid/content/Context;)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    invoke-virtual {v0, p0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :cond_1
    return-void
.end method

.method private updateAnimateStatus(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "animate_settings_status"

    invoke-static {p0, v0, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method private updateDarkMode(Lcom/android/settingslib/miuisettings/preference/ValuePreference;)V
    .locals 3

    sget-object v0, Lcom/android/settings/MiuiDisplaySettings;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "updateDarkMode"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    if-eqz p1, :cond_2

    if-eqz v1, :cond_2

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Lcom/android/settingslib/miuisettings/preference/ValuePreference;->setShowRightArrow(Z)V

    invoke-static {v1}, Lcom/android/settings/display/DarkModeTimeModeUtil;->isDarkModeTimeEnable(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_1

    const-string/jumbo p1, "updateDarkMode DarkModeTimeEnable"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v1}, Lcom/android/settings/display/DarkModeTimeModeUtil;->getDarkModeTimeType(Landroid/content/Context;)I

    move-result p1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/MiuiDisplaySettings;->mDarkModeRadioTimePref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    sget p1, Lcom/android/settings/R$string;->dark_mode_day_night_mode_title:I

    invoke-virtual {p0, p1}, Lcom/android/settingslib/miuisettings/preference/ValuePreference;->setValue(I)V

    goto :goto_0

    :cond_0
    iget-object p0, p0, Lcom/android/settings/MiuiDisplaySettings;->mDarkModeRadioTimePref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    sget p1, Lcom/android/settings/R$string;->dark_mode_auto_time_title:I

    invoke-virtual {p0, p1}, Lcom/android/settingslib/miuisettings/preference/ValuePreference;->setValue(I)V

    goto :goto_0

    :cond_1
    iget-object p0, p0, Lcom/android/settings/MiuiDisplaySettings;->mDarkModeRadioTimePref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    sget p1, Lcom/android/settings/R$string;->screen_paper_mode_turn_off:I

    invoke-virtual {p0, p1}, Lcom/android/settingslib/miuisettings/preference/ValuePreference;->setValue(I)V

    :cond_2
    :goto_0
    return-void
.end method

.method private updateDarkModeMoreSettings()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/display/DarkModeTimeModeUtil;->isDarkModeEnable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/display/DarkModeTimeModeUtil;->isDarkModeTimeEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object p0, p0, Lcom/android/settings/MiuiDisplaySettings;->mMoreDarkModeSettings:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    goto :goto_1

    :cond_1
    :goto_0
    iget-object p0, p0, Lcom/android/settings/MiuiDisplaySettings;->mMoreDarkModeSettings:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setVisible(Z)V

    :goto_1
    return-void
.end method

.method private updateFontSettings()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mFontStatusController:Lcom/android/settings/BaseSettingsController;

    invoke-virtual {v0}, Lcom/android/settings/BaseSettingsController;->updateStatus()V

    iget-object p0, p0, Lcom/android/settings/MiuiDisplaySettings;->mPageLayoutStatusController:Lcom/android/settings/BaseSettingsController;

    invoke-virtual {p0}, Lcom/android/settings/BaseSettingsController;->updateStatus()V

    return-void
.end method

.method private updateLineBreakingPreference(Z)V
    .locals 0

    if-eqz p1, :cond_0

    const-string/jumbo p0, "true"

    goto :goto_0

    :cond_0
    const-string p0, "false"

    :goto_0
    const-string/jumbo p1, "persist.sys.line_breaking"

    invoke-static {p1, p0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private updateMonochromeMode()V
    .locals 2

    const-string/jumbo v0, "screen_monochrome_mode"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    if-eqz v0, :cond_1

    if-eqz p0, :cond_1

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/miuisettings/preference/ValuePreference;->setShowRightArrow(Z)V

    invoke-static {p0}, Lcom/android/settings/display/MonochromeModeFragment;->isMonochromeModeEnable(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_0

    sget p0, Lcom/android/settings/R$string;->screen_paper_mode_turn_on:I

    goto :goto_0

    :cond_0
    sget p0, Lcom/android/settings/R$string;->screen_paper_mode_turn_off:I

    :goto_0
    invoke-virtual {v0, p0}, Lcom/android/settingslib/miuisettings/preference/ValuePreference;->setValue(I)V

    :cond_1
    return-void
.end method

.method private updatePaperMode(Lcom/android/settingslib/miuisettings/preference/ValuePreference;)V
    .locals 1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    if-eqz p1, :cond_1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/android/settingslib/miuisettings/preference/ValuePreference;->setShowRightArrow(Z)V

    invoke-static {p0}, Lcom/android/settings/MiuiUtils;->isPaperModeEnable(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_0

    sget p0, Lcom/android/settings/R$string;->screen_paper_mode_turn_on:I

    goto :goto_0

    :cond_0
    sget p0, Lcom/android/settings/R$string;->screen_paper_mode_turn_off:I

    :goto_0
    invoke-virtual {p1, p0}, Lcom/android/settingslib/miuisettings/preference/ValuePreference;->setValue(I)V

    :cond_1
    return-void
.end method

.method private updateRotatePreference(Z)V
    .locals 0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    xor-int/lit8 p1, p1, 0x1

    invoke-static {p0, p1}, Lcom/android/internal/view/RotationPolicy;->setRotationLockForAccessibility(Landroid/content/Context;Z)V

    return-void
.end method

.method private updateTouchSensitivePreference(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mMiuiUtils:Lcom/android/settings/MiuiUtils;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/android/settings/MiuiUtils;->enableTouchSensitive(Landroid/content/Context;Z)V

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiDisplaySettings;->enableScreenOnProximitySensor(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 0

    const-class p0, Lcom/android/settings/MiuiDisplaySettings;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public getPageIndex()I
    .locals 0

    const/4 p0, 0x1

    return p0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    invoke-super {p0, p1}, Lcom/android/settings/DisplaySettings;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    const-string/jumbo v0, "screen_effect"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    invoke-static {}, Lcom/android/settings/MiuiUtils;->getInstance()Lcom/android/settings/MiuiUtils;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mMiuiUtils:Lcom/android/settings/MiuiUtils;

    const-string/jumbo p1, "touch_category"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/PreferenceCategory;

    const-string/jumbo v1, "touch_sensitive"

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v1

    check-cast v1, Landroidx/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->mTouchSensitive:Landroidx/preference/CheckBoxPreference;

    if-eqz v1, :cond_0

    invoke-virtual {v1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_0
    const-string/jumbo v1, "support_touch_sensitive"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    const/4 v3, 0x0

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->mTouchSensitive:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    iput-object v3, p0, Lcom/android/settings/MiuiDisplaySettings;->mTouchSensitive:Landroidx/preference/CheckBoxPreference;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->mTouchSensitive:Landroidx/preference/CheckBoxPreference;

    iget-object v4, p0, Lcom/android/settings/MiuiDisplaySettings;->mMiuiUtils:Lcom/android/settings/MiuiUtils;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/settings/MiuiUtils;->isTouchSensitive(Landroid/content/Context;)Z

    move-result v4

    invoke-virtual {v1, v4}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :goto_0
    invoke-virtual {p1}, Landroidx/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_2
    const-string p1, "line_breaking"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mLineBreaking:Landroidx/preference/CheckBoxPreference;

    if-eqz p1, :cond_3

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_3
    const-string/jumbo p1, "screen_monochrome_mode"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mScreenMonochromeModePref:Landroidx/preference/Preference;

    if-eqz p1, :cond_4

    sget p1, Landroid/provider/MiuiSettings$ScreenEffect;->SCREEN_EFFECT_SUPPORTED:I

    and-int/lit8 p1, p1, 0x8

    if-nez p1, :cond_4

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->mScreenMonochromeModePref:Landroidx/preference/Preference;

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    iput-object v3, p0, Lcom/android/settings/MiuiDisplaySettings;->mScreenMonochromeModePref:Landroidx/preference/Preference;

    goto :goto_1

    :cond_4
    new-instance p1, Lcom/android/settings/MiuiDisplaySettings$3;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {p1, p0, v1}, Lcom/android/settings/MiuiDisplaySettings$3;-><init>(Lcom/android/settings/MiuiDisplaySettings;Landroid/os/Handler;)V

    iput-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mMonochromeModeEnabledObserver:Landroid/database/ContentObserver;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string/jumbo v1, "screen_monochrome_mode_enabled"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v4, p0, Lcom/android/settings/MiuiDisplaySettings;->mMonochromeModeEnabledObserver:Landroid/database/ContentObserver;

    invoke-virtual {p1, v1, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    :goto_1
    const-string/jumbo p1, "screen_resolution"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    iput-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mScreenResolutionPref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    const-string/jumbo p1, "screen_resolution_supported"

    invoke-static {p1}, Lmiui/util/FeatureParser;->getIntArray(Ljava/lang/String;)[I

    move-result-object p1

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->mScreenResolutionPref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    const/4 v4, 0x1

    if-eqz v1, :cond_7

    if-eqz p1, :cond_6

    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result p1

    if-nez p1, :cond_6

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->isInMultiWindowMode()Z

    move-result p1

    if-eqz p1, :cond_5

    goto :goto_2

    :cond_5
    iget-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mScreenResolutionPref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    invoke-direct {p0}, Lcom/android/settings/MiuiDisplaySettings;->isQhdMode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/android/settingslib/miuisettings/preference/ValuePreference;->setValue(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mScreenResolutionPref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    invoke-virtual {p1, v4}, Lcom/android/settingslib/miuisettings/preference/ValuePreference;->setShowRightArrow(Z)V

    goto :goto_3

    :cond_6
    :goto_2
    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->mScreenResolutionPref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    iput-object v3, p0, Lcom/android/settings/MiuiDisplaySettings;->mScreenResolutionPref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    :cond_7
    :goto_3
    sget p1, Landroid/provider/MiuiSettings$ScreenEffect;->SCREEN_EFFECT_SUPPORTED:I

    and-int/lit8 p1, p1, 0x7

    if-nez p1, :cond_8

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    if-eqz p1, :cond_8

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_8
    const-string/jumbo p1, "screen_paper_mode"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    iput-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mPaperModePref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    const-string p1, "advanced_screen_paper_mode"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    iput-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mAdvancedPaperModePref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    sget-boolean v0, Landroid/provider/MiuiSettings$ScreenEffect;->isScreenPaperModeSupported:Z

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mPaperModePref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    if-eqz v0, :cond_b

    if-eqz p1, :cond_b

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mPaperModePref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mAdvancedPaperModePref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    goto :goto_5

    :cond_9
    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    invoke-static {}, Lcom/android/settings/MiuiUtils;->supportPaperEyeCare()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mPaperModePref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    goto :goto_4

    :cond_a
    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mAdvancedPaperModePref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    :goto_4
    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    new-instance p1, Lcom/android/settings/MiuiDisplaySettings$4;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p1, p0, v0}, Lcom/android/settings/MiuiDisplaySettings$4;-><init>(Lcom/android/settings/MiuiDisplaySettings;Landroid/os/Handler;)V

    iput-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mPaperModeEnabledObserver:Landroid/database/ContentObserver;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string/jumbo v0, "screen_paper_mode_enabled"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->mPaperModeEnabledObserver:Landroid/database/ContentObserver;

    invoke-virtual {p1, v0, v2, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    :cond_b
    :goto_5
    new-instance p1, Lcom/android/settings/display/FontStatusController;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p1, v0, v3}, Lcom/android/settings/display/FontStatusController;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    iput-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mFontStatusController:Lcom/android/settings/BaseSettingsController;

    const-string p1, "font_settings"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    iput-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mFontSettingsPref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    if-eqz p1, :cond_c

    invoke-virtual {p1, v4}, Lcom/android/settingslib/miuisettings/preference/ValuePreference;->setShowRightArrow(Z)V

    iget-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mFontStatusController:Lcom/android/settings/BaseSettingsController;

    new-instance v0, Lcom/android/settings/MiuiDisplaySettings$5;

    invoke-direct {v0, p0}, Lcom/android/settings/MiuiDisplaySettings$5;-><init>(Lcom/android/settings/MiuiDisplaySettings;)V

    invoke-virtual {p1, v0}, Lcom/android/settings/BaseSettingsController;->setUpdateCallback(Lcom/android/settings/BaseSettingsController$UpdateCallback;)V

    :cond_c
    new-instance p1, Lcom/android/settings/display/PageLayoutStatusController;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p1, v0, v3}, Lcom/android/settings/display/PageLayoutStatusController;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    iput-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mPageLayoutStatusController:Lcom/android/settings/BaseSettingsController;

    const-string/jumbo p1, "page_layout_settings"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    iput-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mPageLayoutStatusPref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    if-eqz p1, :cond_d

    invoke-virtual {p1, v4}, Lcom/android/settingslib/miuisettings/preference/ValuePreference;->setShowRightArrow(Z)V

    iget-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mPageLayoutStatusController:Lcom/android/settings/BaseSettingsController;

    new-instance v0, Lcom/android/settings/MiuiDisplaySettings$6;

    invoke-direct {v0, p0}, Lcom/android/settings/MiuiDisplaySettings$6;-><init>(Lcom/android/settings/MiuiDisplaySettings;)V

    invoke-virtual {p1, v0}, Lcom/android/settings/BaseSettingsController;->setUpdateCallback(Lcom/android/settings/BaseSettingsController$UpdateCallback;)V

    :cond_d
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mPageLayoutStatusPref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    if-eqz v0, :cond_12

    invoke-static {p1}, Lcom/android/settings/MiuiUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_e

    iget-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mPageLayoutStatusPref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    invoke-virtual {p1, v2}, Landroidx/preference/Preference;->setEnabled(Z)V

    :cond_e
    sget-boolean p1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz p1, :cond_10

    iget-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mFontSettingsPref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    if-eqz p1, :cond_f

    invoke-virtual {p1, v2}, Landroidx/preference/Preference;->setVisible(Z)V

    :cond_f
    iget-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mPageLayoutStatusPref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    sget v0, Lcom/android/settings/R$string;->title_layout_current2:I

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setTitle(I)V

    iget-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mPageLayoutStatusPref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    invoke-virtual {p1, v3}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    new-instance p1, Lcom/android/settings/MiuiDisplaySettings$7;

    invoke-direct {p1, p0}, Lcom/android/settings/MiuiDisplaySettings$7;-><init>(Lcom/android/settings/MiuiDisplaySettings;)V

    invoke-static {p1}, Lcom/android/settingslib/utils/ThreadUtils;->postOnBackgroundThread(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_6

    :cond_10
    iget-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mFontSettingsPref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    if-eqz p1, :cond_11

    invoke-virtual {p1, v2}, Landroidx/preference/Preference;->setVisible(Z)V

    :cond_11
    iget-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mPageLayoutStatusPref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    sget v0, Lcom/android/settings/R$string;->title_font_settings:I

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setTitle(I)V

    iget-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mPageLayoutStatusPref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    sget v0, Lcom/android/settings/R$string;->font_settings_summary_cn:I

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setSummary(I)V

    :cond_12
    :goto_6
    const-string p1, "font_settings_cat"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/PreferenceCategory;

    if-eqz p1, :cond_13

    invoke-virtual {p1}, Landroidx/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    if-nez v0, :cond_13

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_13
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-nez v0, :cond_14

    move v0, v4

    goto :goto_7

    :cond_14
    move v0, v2

    :goto_7
    if-eqz p1, :cond_15

    if-nez v0, :cond_15

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_15
    const-string p1, "auto_rotate"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mRotatePreference:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mRotatePreference:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, v2}, Landroidx/preference/Preference;->setPersistent(Z)V

    const-string p1, "dark_mode_apps_setting"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    iput-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mMoreDarkModeSettings:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    const-string p1, "dark_mode_timing"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/PreferenceCategory;

    iput-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mDarkModeTiming:Landroidx/preference/PreferenceCategory;

    iget-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    invoke-static {p1}, Landroid/provider/MiuiSettings$Secure;->isSecureSpace(Landroid/content/ContentResolver;)Z

    move-result p1

    if-eqz p1, :cond_16

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mDarkModeTiming:Landroidx/preference/PreferenceCategory;

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_16
    invoke-static {}, Lcom/android/settings/MiuiUtils;->supportAnimateCheck()Z

    move-result p1

    if-nez p1, :cond_17

    const-string p1, "animate_settings_cat"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    goto :goto_8

    :cond_17
    const-string p1, "animate_settings_key"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    invoke-direct {p0}, Lcom/android/settings/MiuiDisplaySettings;->getAnimateStatus()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :goto_8
    const-string p1, "dark_mode_time_settings"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    iput-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mDarkModeRadioTimePref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    const-string p1, "dc_light"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    iput-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mScreenDcPreference:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    const-string/jumbo p1, "screen_fps"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    iput-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mScreenFpsPreference:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    iget p1, p1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 p1, p1, 0xf

    const/4 v0, 0x3

    if-ne p1, v0, :cond_18

    sget-object p1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    const-string v0, "cetus"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_18

    move v2, v4

    :cond_18
    iput-boolean v2, p0, Lcom/android/settings/MiuiDisplaySettings;->mDisableDclightAndHighFps:Z

    if-eqz v2, :cond_19

    iget-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mScreenDcPreference:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    sget v0, Lcom/android/settings/R$layout;->set_preference_title_gray:I

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setLayoutResource(I)V

    iget-object p0, p0, Lcom/android/settings/MiuiDisplaySettings;->mScreenFpsPreference:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setLayoutResource(I)V

    :cond_19
    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 2

    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    new-instance p1, Lmiuix/appcompat/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p1, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v0, Lcom/android/settings/R$string;->touch_sensitive_turn_off_title:I

    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/android/settings/R$string;->touch_sensitive_turn_off_summary:I

    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/android/settings/R$string;->touch_sensitive_turn_off_confirm:I

    new-instance v1, Lcom/android/settings/MiuiDisplaySettings$8;

    invoke-direct {v1, p0}, Lcom/android/settings/MiuiDisplaySettings$8;-><init>(Lcom/android/settings/MiuiDisplaySettings;)V

    invoke-virtual {p1, v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p0

    sget p1, Lcom/android/settings/R$string;->touch_sensitive_turn_off_cancel:I

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object p0

    return-object p0
.end method

.method public onDestroy()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mPaperModeEnabledObserver:Landroid/database/ContentObserver;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/MiuiDisplaySettings;->mPaperModeEnabledObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iput-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->mPaperModeEnabledObserver:Landroid/database/ContentObserver;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mMonochromeModeEnabledObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/MiuiDisplaySettings;->mMonochromeModeEnabledObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iput-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->mMonochromeModeEnabledObserver:Landroid/database/ContentObserver;

    :cond_1
    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mAODObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/MiuiDisplaySettings;->mAODObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iput-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->mAODObserver:Landroid/database/ContentObserver;

    :cond_2
    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mTouchSensitive:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_3

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mLineBreaking:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_4

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_4
    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mRotatePreference:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_5

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_5
    invoke-super {p0}, Lcom/android/settingslib/core/lifecycle/ObservablePreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onPause()V
    .locals 2

    sget-boolean v0, Lcom/android/settings/utils/SettingsFeatures;->IS_SUPPORT_TWO_AUTO_ROTATE:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->mRotationPolicyListener:Lcom/android/internal/view/RotationPolicy$RotationPolicyListener;

    invoke-static {v0, v1}, Lcom/android/internal/view/RotationPolicy;->unregisterRotationPolicyListener(Landroid/content/Context;Lcom/android/internal/view/RotationPolicy$RotationPolicyListener;)V

    :cond_0
    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onPause()V

    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 1

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    const-string/jumbo v0, "touch_sensitive"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiDisplaySettings;->updateTouchSensitivePreference(Z)V

    goto :goto_0

    :cond_0
    const-string v0, "line_breaking"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiDisplaySettings;->updateLineBreakingPreference(Z)V

    goto :goto_0

    :cond_1
    const-string v0, "auto_rotate"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiDisplaySettings;->updateRotatePreference(Z)V

    goto :goto_0

    :cond_2
    const-string v0, "animate_settings_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiDisplaySettings;->updateAnimateStatus(Z)V

    :cond_3
    :goto_0
    const/4 p0, 0x1

    return p0
.end method

.method public onPreferenceTreeClick(Landroidx/preference/PreferenceScreen;Landroidx/preference/Preference;)Z
    .locals 5

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mFontSettingsPref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    const-string v1, "android.intent.category.DEFAULT"

    if-ne p2, v0, :cond_2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "android.intent.category.BROWSABLE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    sget-boolean v2, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/android/settings/MiuiDisplaySettings;->mIsFontSettingEnable:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "theme://zhuti.xiaomi.com/settingsfont?miback=true&miref="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/android/settings/MiuiDisplaySettings;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "theme://zhuti.xiaomi.com/list?S.REQUEST_RESOURCE_CODE=fonts&miback=true&miref="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/android/settings/MiuiDisplaySettings;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v2, ":miui:starting_window_label"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->isFoldDevice()Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.android.thememanager"

    const-string v4, "com.android.thememanager.activity.ThemeTabActivity"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    :cond_1
    :try_start_0
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mContext:Landroid/content/Context;

    sget v2, Lcom/android/settings/R$string;->thememanager_not_found:I

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    :goto_1
    const-string/jumbo v0, "setting_Display"

    invoke-static {v0}, Lcom/android/settings/report/InternationalCompat;->trackReportEvent(Ljava/lang/String;)V

    :cond_2
    sget-boolean v0, Lcom/android/settings/RegionUtils;->IS_JP_KDDI:Z

    const/4 v2, 0x1

    if-nez v0, :cond_3

    sget-boolean v0, Lcom/android/settings/RegionUtils;->IS_JP_SB:Z

    if-eqz v0, :cond_4

    :cond_3
    invoke-virtual {p2}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "page_layout_settings"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    const-string p2, "android.settings.ACCESSIBILITY_SETTINGS_FOR_SUW"

    invoke-virtual {p1, p2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string p2, "isSetupFlow"

    invoke-virtual {p1, p2, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    new-instance p2, Landroid/content/ComponentName;

    const-string v0, "com.android.settings"

    const-string v1, "com.android.settings.FontSizeSettingsForSetupWizardActivity"

    invoke-direct {p2, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    return v2

    :cond_4
    iget-boolean v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mDisableDclightAndHighFps:Z

    if-eqz v0, :cond_9

    invoke-virtual {p2}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "dc_light"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string/jumbo v3, "screen_fps"

    if-nez v0, :cond_5

    invoke-virtual {p2}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_5
    iget-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mAlertDialog:Lmiuix/appcompat/app/AlertDialog;

    if-nez p1, :cond_6

    new-instance p1, Lmiuix/appcompat/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    sget v4, Lcom/android/settings/R$style;->AlertDialog_Theme_DayNight:I

    invoke-direct {p1, v0, v4}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    sget v0, Lcom/android/settings/R$string;->display_prompt_confirm:I

    const/4 v4, 0x0

    invoke-virtual {p1, v0, v4}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mAlertDialog:Lmiuix/appcompat/app/AlertDialog;

    :cond_6
    invoke-virtual {p2}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_7

    iget-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mAlertDialog:Lmiuix/appcompat/app/AlertDialog;

    sget p2, Lcom/android/settings/R$string;->dclight_unavailable_prompt:I

    invoke-virtual {p1, p2}, Landroidx/appcompat/app/AppCompatDialog;->setTitle(I)V

    goto :goto_2

    :cond_7
    invoke-virtual {p2}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_8

    iget-object p1, p0, Lcom/android/settings/MiuiDisplaySettings;->mAlertDialog:Lmiuix/appcompat/app/AlertDialog;

    sget p2, Lcom/android/settings/R$string;->screen_refresh_unavailable_prompt:I

    invoke-virtual {p1, p2}, Landroidx/appcompat/app/AppCompatDialog;->setTitle(I)V

    :cond_8
    :goto_2
    iget-object p0, p0, Lcom/android/settings/MiuiDisplaySettings;->mAlertDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {p0}, Landroid/app/Dialog;->show()V

    return v2

    :cond_9
    invoke-super {p0, p1, p2}, Lcom/android/settings/SettingsPreferenceFragment;->onPreferenceTreeClick(Landroidx/preference/PreferenceScreen;Landroidx/preference/Preference;)Z

    move-result p0

    return p0
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/dashboard/DashboardFragment;->onResume()V

    invoke-static {}, Lcom/android/settings/MiuiUtils;->supportPaperEyeCare()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mAdvancedPaperModePref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mPaperModePref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    :goto_0
    invoke-direct {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->updatePaperMode(Lcom/android/settingslib/miuisettings/preference/ValuePreference;)V

    invoke-direct {p0}, Lcom/android/settings/MiuiDisplaySettings;->updateFontSettings()V

    invoke-direct {p0}, Lcom/android/settings/MiuiDisplaySettings;->updateMonochromeMode()V

    sget-boolean v0, Lcom/android/settings/utils/SettingsFeatures;->IS_SUPPORT_TWO_AUTO_ROTATE:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/MiuiDisplaySettings;->mRotationPolicyListener:Lcom/android/internal/view/RotationPolicy$RotationPolicyListener;

    invoke-static {v0, v1}, Lcom/android/internal/view/RotationPolicy;->registerRotationPolicyListener(Landroid/content/Context;Lcom/android/internal/view/RotationPolicy$RotationPolicyListener;)V

    invoke-direct {p0}, Lcom/android/settings/MiuiDisplaySettings;->updateAccelerometerRotationCheckbox()V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mRotatePreference:Landroidx/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setVisible(Z)V

    :goto_1
    iget-object v0, p0, Lcom/android/settings/MiuiDisplaySettings;->mDarkModeRadioTimePref:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    invoke-direct {p0, v0}, Lcom/android/settings/MiuiDisplaySettings;->updateDarkMode(Lcom/android/settingslib/miuisettings/preference/ValuePreference;)V

    invoke-direct {p0}, Lcom/android/settings/MiuiDisplaySettings;->updateDarkModeMoreSettings()V

    return-void
.end method
