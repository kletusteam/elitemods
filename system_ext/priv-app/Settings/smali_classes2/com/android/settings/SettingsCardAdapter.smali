.class public Lcom/android/settings/SettingsCardAdapter;
.super Landroid/widget/BaseAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/SettingsCardAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private cardList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/CardInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Lcom/android/settings/CardInfo;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/android/settings/SettingsCardAdapter;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/SettingsCardAdapter;->cardList:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsCardAdapter;->cardList:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    return p0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsCardAdapter;->cardList:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public getItemId(I)J
    .locals 0

    const-wide/16 p0, 0x0

    return-wide p0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    if-nez p2, :cond_0

    iget-object p2, p0, Lcom/android/settings/SettingsCardAdapter;->mContext:Landroid/content/Context;

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    sget p3, Lcom/android/settings/R$layout;->normal_grid_view:I

    const/4 v0, 0x0

    invoke-virtual {p2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance p3, Lcom/android/settings/SettingsCardAdapter$ViewHolder;

    invoke-direct {p3, p0}, Lcom/android/settings/SettingsCardAdapter$ViewHolder;-><init>(Lcom/android/settings/SettingsCardAdapter;)V

    sget v0, Lcom/android/settings/R$id;->card:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/SettingsStatusCard;

    iput-object v0, p3, Lcom/android/settings/SettingsCardAdapter$ViewHolder;->card:Lcom/android/settings/widget/SettingsStatusCard;

    invoke-virtual {p2, p3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/android/settings/SettingsCardAdapter$ViewHolder;

    :goto_0
    iget-object p0, p0, Lcom/android/settings/SettingsCardAdapter;->cardList:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/android/settings/CardInfo;

    const/4 p1, 0x1

    const/4 v0, 0x0

    if-eqz p0, :cond_2

    iget-object v1, p3, Lcom/android/settings/SettingsCardAdapter$ViewHolder;->card:Lcom/android/settings/widget/SettingsStatusCard;

    invoke-virtual {p0}, Lcom/android/settings/CardInfo;->getTitleResId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/settings/widget/SettingsStatusCard;->setCardTitle(I)V

    iget-object v1, p3, Lcom/android/settings/SettingsCardAdapter$ViewHolder;->card:Lcom/android/settings/widget/SettingsStatusCard;

    invoke-virtual {p0}, Lcom/android/settings/CardInfo;->getValueResId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/settings/widget/SettingsStatusCard;->setCardValue(I)V

    invoke-virtual {p0}, Lcom/android/settings/CardInfo;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p3, Lcom/android/settings/SettingsCardAdapter$ViewHolder;->card:Lcom/android/settings/widget/SettingsStatusCard;

    invoke-virtual {v1, p1}, Lcom/android/settings/widget/SettingsStatusCard;->setChecked(Z)V

    iget-object v1, p3, Lcom/android/settings/SettingsCardAdapter$ViewHolder;->card:Lcom/android/settings/widget/SettingsStatusCard;

    invoke-virtual {p0}, Lcom/android/settings/CardInfo;->getCheckedIconResId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/settings/widget/SettingsStatusCard;->setCardImageView(I)V

    goto :goto_1

    :cond_1
    iget-object v1, p3, Lcom/android/settings/SettingsCardAdapter$ViewHolder;->card:Lcom/android/settings/widget/SettingsStatusCard;

    invoke-virtual {v1, v0}, Lcom/android/settings/widget/SettingsStatusCard;->setChecked(Z)V

    iget-object v1, p3, Lcom/android/settings/SettingsCardAdapter$ViewHolder;->card:Lcom/android/settings/widget/SettingsStatusCard;

    invoke-virtual {p0}, Lcom/android/settings/CardInfo;->getIconResId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/settings/widget/SettingsStatusCard;->setCardImageView(I)V

    :goto_1
    iget-object p3, p3, Lcom/android/settings/SettingsCardAdapter$ViewHolder;->card:Lcom/android/settings/widget/SettingsStatusCard;

    invoke-virtual {p0}, Lcom/android/settings/CardInfo;->isDisable()Z

    move-result p0

    invoke-virtual {p3, p0}, Lcom/android/settings/widget/SettingsStatusCard;->setDisable(Z)V

    :cond_2
    new-array p0, p1, [Landroid/view/View;

    aput-object p2, p0, v0

    invoke-static {p0}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object p0

    invoke-interface {p0}, Lmiuix/animation/IFolme;->touch()Lmiuix/animation/ITouchStyle;

    move-result-object p0

    new-array p1, v0, [Lmiuix/animation/base/AnimConfig;

    invoke-interface {p0, p2, p1}, Lmiuix/animation/ITouchStyle;->handleTouchOf(Landroid/view/View;[Lmiuix/animation/base/AnimConfig;)V

    return-object p2
.end method
