.class Lcom/android/settings/development/FiveGNrcaPreferenceController$1;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/development/FiveGNrcaPreferenceController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/development/FiveGNrcaPreferenceController;


# direct methods
.method constructor <init>(Lcom/android/settings/development/FiveGNrcaPreferenceController;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController$1;->this$0:Lcom/android/settings/development/FiveGNrcaPreferenceController;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string v0, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController$1;->this$0:Lcom/android/settings/development/FiveGNrcaPreferenceController;

    const/4 v0, 0x0

    const-string/jumbo v1, "state"

    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p2

    invoke-static {p1, p2}, Lcom/android/settings/development/FiveGNrcaPreferenceController;->-$$Nest$fputmAirplaneMode(Lcom/android/settings/development/FiveGNrcaPreferenceController;Z)V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "ACTION_AIRPLANE_MODE_CHANGED: "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController$1;->this$0:Lcom/android/settings/development/FiveGNrcaPreferenceController;

    invoke-static {p2}, Lcom/android/settings/development/FiveGNrcaPreferenceController;->-$$Nest$fgetmAirplaneMode(Lcom/android/settings/development/FiveGNrcaPreferenceController;)Z

    move-result p2

    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "FiveGNrcaPreferenceController"

    invoke-static {p2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p0, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController$1;->this$0:Lcom/android/settings/development/FiveGNrcaPreferenceController;

    invoke-static {p0}, Lcom/android/settings/development/FiveGNrcaPreferenceController;->-$$Nest$mupdatePreference(Lcom/android/settings/development/FiveGNrcaPreferenceController;)V

    goto :goto_0

    :cond_0
    const-string p2, "android.telephony.action.CARRIER_CONFIG_CHANGED"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p0, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController$1;->this$0:Lcom/android/settings/development/FiveGNrcaPreferenceController;

    invoke-static {p0}, Lcom/android/settings/development/FiveGNrcaPreferenceController;->-$$Nest$mupdatePreference(Lcom/android/settings/development/FiveGNrcaPreferenceController;)V

    :cond_1
    :goto_0
    return-void
.end method
