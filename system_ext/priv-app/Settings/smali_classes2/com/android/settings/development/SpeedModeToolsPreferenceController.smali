.class public Lcom/android/settings/development/SpeedModeToolsPreferenceController;
.super Lcom/android/settings/core/BasePreferenceController;

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;


# static fields
.field public static final SPEED_MODE_ENABLE:Ljava/lang/String; = "speed_mode_enable"

.field public static final SPEED_MODE_KEY:Ljava/lang/String; = "speed_mode"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mPreference:Landroidx/preference/SwitchPreference;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mContext:Landroid/content/Context;

    return-void
.end method

.method private hideSpeedModeActivatedNotification()V
    .locals 2

    iget-object p0, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mContext:Landroid/content/Context;

    const-string/jumbo v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/app/NotificationManager;

    sget v0, Lcom/android/settings/R$string;->speed_mode_noti_title:I

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    return-void
.end method

.method private showSpeedModeActivatedNotification()V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mContext:Landroid/content/Context;

    sget v1, Lcom/android/settings/R$string;->auto_task_operation_close:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/android/settings/widget/CommonNotification$Builder;

    iget-object v2, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/android/settings/widget/CommonNotification$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/android/settings/R$string;->speed_mode_noti_title:I

    invoke-virtual {v1, v2}, Lcom/android/settings/widget/CommonNotification$Builder;->setNotifyId(I)Lcom/android/settings/widget/CommonNotification$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/android/settings/R$string;->menu_item_notification_power_text:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "speed_mode"

    invoke-virtual {v3, v5, v4}, Lcom/android/settings/widget/CommonNotification$Builder;->setChannel(Ljava/lang/String;Ljava/lang/String;)Lcom/android/settings/widget/CommonNotification$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/android/settings/widget/CommonNotification$Builder;->setActionText(Ljava/lang/String;)Lcom/android/settings/widget/CommonNotification$Builder;

    move-result-object v0

    sget v3, Lcom/android/settings/R$drawable;->ic_performance_notification:I

    invoke-virtual {v0, v3}, Lcom/android/settings/widget/CommonNotification$Builder;->setNotificationIcon(I)Lcom/android/settings/widget/CommonNotification$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/settings/widget/CommonNotification$Builder;->setSmallIcon(I)Lcom/android/settings/widget/CommonNotification$Builder;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/CommonNotification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Lcom/android/settings/widget/CommonNotification$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mContext:Landroid/content/Context;

    sget v3, Lcom/android/settings/R$string;->speed_mode_noti_summary:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/CommonNotification$Builder;->setContentText(Ljava/lang/CharSequence;)Lcom/android/settings/widget/CommonNotification$Builder;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/CommonNotification$Builder;->setImportance(I)Lcom/android/settings/widget/CommonNotification$Builder;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/CommonNotification$Builder;->setEnableKeyguard(Z)Lcom/android/settings/widget/CommonNotification$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/CommonNotification$Builder;->setEnableFloat(Z)Lcom/android/settings/widget/CommonNotification$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/settings/widget/CommonNotification$Builder;->setResident(Z)Lcom/android/settings/widget/CommonNotification$Builder;

    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mContext:Landroid/content/Context;

    const-class v3, Lcom/android/settings/SubSettings;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "com.android.settings"

    const-string v3, "com.android.settings.SubSettings"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-class v2, Lcom/android/settings/development/DevelopmentSettingsDashboardFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, ":settings:show_fragment"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/android/settings/widget/CommonNotification$Builder;->setResultIntent(Landroid/content/Intent;I)Lcom/android/settings/widget/CommonNotification$Builder;

    new-instance v0, Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mContext:Landroid/content/Context;

    const-class v4, Lcom/android/settings/MiuiSettingsReceiver;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "miui.intent.action.settings.SPEED_MODE_CLOSED"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object p0, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mContext:Landroid/content/Context;

    const/high16 v3, 0x4000000

    invoke-static {p0, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/android/settings/widget/CommonNotification$Builder;->setActionPendingIntent(Landroid/app/PendingIntent;)Lcom/android/settings/widget/CommonNotification$Builder;

    invoke-virtual {v1}, Lcom/android/settings/widget/CommonNotification$Builder;->build()Lcom/android/settings/widget/CommonNotification;

    move-result-object p0

    invoke-virtual {p0}, Lcom/android/settings/widget/CommonNotification;->show()V

    return-void
.end method


# virtual methods
.method public displayPreference(Landroidx/preference/PreferenceScreen;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/core/BasePreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    invoke-virtual {p0}, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->getPreferenceKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/SwitchPreference;

    iput-object p1, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mPreference:Landroidx/preference/SwitchPreference;

    return-void
.end method

.method public getAvailabilityStatus()I
    .locals 2

    iget-object p0, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "speed_mode_enable"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    return v1

    :cond_0
    const/4 p0, 0x3

    return p0
.end method

.method public bridge synthetic getBackgroundWorkerClass()Ljava/lang/Class;
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->getBackgroundWorkerClass()Ljava/lang/Class;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic getIntentFilter()Landroid/content/IntentFilter;
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->getIntentFilter()Landroid/content/IntentFilter;

    move-result-object p0

    return-object p0
.end method

.method public getPreferenceKey()Ljava/lang/String;
    .locals 0

    const-string/jumbo p0, "speed_mode"

    return-object p0
.end method

.method public bridge synthetic getSliceHighlightMenuRes()I
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->getSliceHighlightMenuRes()I

    move-result p0

    return p0
.end method

.method public bridge synthetic hasAsyncUpdate()Z
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->hasAsyncUpdate()Z

    move-result p0

    return p0
.end method

.method public bridge synthetic isPublicSlice()Z
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->isPublicSlice()Z

    move-result p0

    return p0
.end method

.method public bridge synthetic isSliceable()Z
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->isSliceable()Z

    move-result p0

    return p0
.end method

.method public onDeveloperOptionsSwitchDisabled()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mPreference:Landroidx/preference/SwitchPreference;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->hideSpeedModeActivatedNotification()V

    iget-object v0, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mPreference:Landroidx/preference/SwitchPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mPreference:Landroidx/preference/SwitchPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v2, "speed_mode"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object p0, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_0
    return-void
.end method

.method public onDeveloperOptionsSwitchEnabled()V
    .locals 1

    iget-object p0, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mPreference:Landroidx/preference/SwitchPreference;

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const/4 p2, 0x1

    const-string/jumbo v0, "speed_mode"

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->showSpeedModeActivatedNotification()V

    iget-object p1, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mPreference:Landroidx/preference/SwitchPreference;

    invoke-virtual {p1, p2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p1, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    invoke-static {p1, v0, p2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->hideSpeedModeActivatedNotification()V

    iget-object p1, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mPreference:Landroidx/preference/SwitchPreference;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object p1, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    invoke-static {p1, v0, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :goto_0
    iget-object p0, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    return p2
.end method

.method public updateState(Landroidx/preference/Preference;)V
    .locals 1

    :try_start_0
    iget-object p1, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string/jumbo v0, "speed_mode"

    invoke-static {p1, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result p1

    if-nez p1, :cond_0

    iget-object p0, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mPreference:Landroidx/preference/SwitchPreference;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    goto :goto_0

    :cond_0
    iget-object p0, p0, Lcom/android/settings/development/SpeedModeToolsPreferenceController;->mPreference:Landroidx/preference/SwitchPreference;

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public bridge synthetic useDynamicSliceSummary()Z
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->useDynamicSliceSummary()Z

    move-result p0

    return p0
.end method
