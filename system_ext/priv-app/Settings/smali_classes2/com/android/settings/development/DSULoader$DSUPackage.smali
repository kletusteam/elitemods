.class Lcom/android/settings/development/DSULoader$DSUPackage;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/development/DSULoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DSUPackage"
.end annotation


# instance fields
.field mCpuAbi:Ljava/lang/String;

.field mDetails:Ljava/lang/String;

.field mName:Ljava/lang/String;

.field mOsVersion:I

.field mPubKey:Ljava/lang/String;

.field mSPL:Ljava/util/Date;

.field mTosUrl:Ljava/net/URL;

.field mUri:Ljava/net/URL;

.field mVndk:[I

.field final synthetic this$0:Lcom/android/settings/development/DSULoader;


# direct methods
.method constructor <init>(Lcom/android/settings/development/DSULoader;Lorg/json/JSONObject;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Ljava/net/MalformedURLException;,
            Ljava/text/ParseException;
        }
    .end annotation

    iput-object p1, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->this$0:Lcom/android/settings/development/DSULoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mName:Ljava/lang/String;

    iput-object p1, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mDetails:Ljava/lang/String;

    iput-object p1, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mCpuAbi:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mOsVersion:I

    iput-object p1, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mVndk:[I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mPubKey:Ljava/lang/String;

    iput-object p1, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mSPL:Ljava/util/Date;

    iput-object p1, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mTosUrl:Ljava/net/URL;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "DSUPackage: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "DSULOADER"

    invoke-static {v0, p1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo p1, "name"

    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mName:Ljava/lang/String;

    const-string p1, "details"

    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mDetails:Ljava/lang/String;

    const-string p1, "cpu_abi"

    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mCpuAbi:Ljava/lang/String;

    new-instance p1, Ljava/net/URL;

    const-string/jumbo v0, "uri"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mUri:Ljava/net/URL;

    const-string/jumbo p1, "os_version"

    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, Lcom/android/settings/development/DSULoader$DSUPackage;->dessertNumber(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mOsVersion:I

    :cond_0
    const-string/jumbo p1, "vndk"

    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mVndk:[I

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mVndk:[I

    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->getInt(I)I

    move-result v2

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string/jumbo p1, "pubkey"

    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mPubKey:Ljava/lang/String;

    :cond_2
    const-string/jumbo p1, "tos"

    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/net/URL;

    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mTosUrl:Ljava/net/URL;

    :cond_3
    const-string/jumbo p1, "spl"

    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mSPL:Ljava/util/Date;

    :cond_4
    return-void
.end method


# virtual methods
.method dessertNumber(Ljava/lang/String;I)I
    .locals 1

    goto/32 :goto_a

    nop

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_b

    nop

    :goto_1
    const/4 p0, -0x1

    goto/32 :goto_9

    nop

    :goto_2
    goto :goto_5

    :goto_3
    goto/32 :goto_10

    nop

    :goto_4
    return p0

    :goto_5
    goto/32 :goto_1

    nop

    :goto_6
    if-nez v0, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_f

    nop

    :goto_7
    add-int/2addr p0, p2

    goto/32 :goto_4

    nop

    :goto_8
    add-int/lit8 p0, p0, -0x51

    goto/32 :goto_7

    nop

    :goto_9
    return p0

    :goto_a
    if-nez p1, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_e

    nop

    :goto_b
    invoke-virtual {p1, p0}, Ljava/lang/String;->charAt(I)C

    move-result p0

    goto/32 :goto_8

    nop

    :goto_c
    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v0

    goto/32 :goto_6

    nop

    :goto_d
    invoke-virtual {p1, p0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto/32 :goto_c

    nop

    :goto_e
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result p0

    goto/32 :goto_13

    nop

    :goto_f
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p0

    goto/32 :goto_11

    nop

    :goto_10
    const/4 p0, 0x0

    goto/32 :goto_d

    nop

    :goto_11
    return p0

    :goto_12
    goto/32 :goto_0

    nop

    :goto_13
    if-nez p0, :cond_2

    goto/32 :goto_3

    :cond_2
    goto/32 :goto_2

    nop
.end method

.method getDeviceCpu()Ljava/lang/String;
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    const-string v0, "aarch64"

    goto/32 :goto_4

    nop

    :goto_1
    invoke-static {p0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_0

    nop

    :goto_3
    const-string/jumbo p0, "ro.product.cpu.abi"

    goto/32 :goto_1

    nop

    :goto_4
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    goto/32 :goto_8

    nop

    :goto_5
    return-object p0

    :goto_6
    const-string p0, "arm64-v8a"

    :goto_7
    goto/32 :goto_5

    nop

    :goto_8
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_6

    nop
.end method

.method getDeviceOs()I
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    return p0

    :goto_1
    const-string/jumbo v0, "ro.system.build.version.release"

    goto/32 :goto_4

    nop

    :goto_2
    const/16 v1, 0xa

    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {p0, v0, v1}, Lcom/android/settings/development/DSULoader$DSUPackage;->dessertNumber(Ljava/lang/String;I)I

    move-result p0

    goto/32 :goto_0

    nop

    :goto_4
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_2

    nop
.end method

.method getDeviceSPL()Ljava/util/Date;
    .locals 3

    goto/32 :goto_3

    nop

    :goto_0
    const/4 v1, 0x0

    goto/32 :goto_7

    nop

    :goto_1
    invoke-static {p0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_6

    nop

    :goto_2
    return-object p0

    :catch_0
    goto/32 :goto_8

    nop

    :goto_3
    const-string/jumbo p0, "ro.build.version.security_patch"

    goto/32 :goto_1

    nop

    :goto_4
    return-object v1

    :goto_5
    :try_start_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyy-MM-dd"

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_2

    nop

    :goto_6
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    goto/32 :goto_0

    nop

    :goto_7
    if-nez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_4

    nop

    :goto_8
    return-object v1
.end method

.method getDeviceVndk()I
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p0, v0, v1}, Lcom/android/settings/development/DSULoader$DSUPackage;->dessertNumber(Ljava/lang/String;I)I

    move-result p0

    goto/32 :goto_4

    nop

    :goto_1
    const/16 v1, 0x1c

    goto/32 :goto_0

    nop

    :goto_2
    const-string/jumbo v0, "ro.vndk.version"

    goto/32 :goto_3

    nop

    :goto_3
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_4
    return p0
.end method

.method isSupported()Z
    .locals 9

    goto/32 :goto_40

    nop

    :goto_0
    goto/16 :goto_34

    :goto_1
    goto/32 :goto_33

    nop

    :goto_2
    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_73

    nop

    :goto_3
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_3e

    nop

    :goto_4
    cmp-long v2, v5, v7

    goto/32 :goto_71

    nop

    :goto_5
    iget-object v1, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mVndk:[I

    goto/32 :goto_15

    nop

    :goto_6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_14

    nop

    :goto_7
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    goto/32 :goto_5f

    nop

    :goto_8
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_5b

    nop

    :goto_9
    iget v5, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mOsVersion:I

    goto/32 :goto_3

    nop

    :goto_a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_2b

    nop

    :goto_b
    const-string p0, " isSupported "

    goto/32 :goto_3c

    nop

    :goto_c
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_62

    nop

    :goto_d
    move v0, v4

    goto/32 :goto_57

    nop

    :goto_e
    const-string v0, "Failed to getDeviceVndk"

    goto/32 :goto_38

    nop

    :goto_f
    array-length v7, v6

    goto/32 :goto_5c

    nop

    :goto_10
    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_65

    nop

    :goto_11
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_22

    nop

    :goto_12
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_41

    nop

    :goto_13
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    goto/32 :goto_37

    nop

    :goto_14
    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_3b

    nop

    :goto_15
    if-nez v1, :cond_0

    goto/32 :goto_2e

    :cond_0
    goto/32 :goto_72

    nop

    :goto_16
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_4f

    nop

    :goto_17
    iget v5, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mOsVersion:I

    goto/32 :goto_2a

    nop

    :goto_18
    invoke-virtual {v1}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_a

    nop

    :goto_19
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_64

    nop

    :goto_1a
    iget v1, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mOsVersion:I

    goto/32 :goto_5e

    nop

    :goto_1b
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_6b

    nop

    :goto_1c
    invoke-static {v3, p0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_68

    nop

    :goto_1d
    const-string v0, "Failed to getDeviceSPL"

    goto/32 :goto_2

    nop

    :goto_1e
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_20

    nop

    :goto_1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_6a

    nop

    :goto_20
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_9

    nop

    :goto_21
    if-eq v6, v1, :cond_1

    goto/32 :goto_61

    :cond_1
    goto/32 :goto_60

    nop

    :goto_22
    iget-object p0, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mName:Ljava/lang/String;

    goto/32 :goto_69

    nop

    :goto_23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_36

    nop

    :goto_24
    const-string/jumbo v2, "vndk:"

    goto/32 :goto_47

    nop

    :goto_25
    if-eqz v1, :cond_2

    goto/32 :goto_27

    :cond_2
    goto/32 :goto_35

    nop

    :goto_26
    goto/16 :goto_49

    :goto_27
    goto/32 :goto_48

    nop

    :goto_28
    const-string v3, "DSULOADER"

    goto/32 :goto_77

    nop

    :goto_29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_10

    nop

    :goto_2a
    if-lt v5, v1, :cond_3

    goto/32 :goto_66

    :cond_3
    goto/32 :goto_1e

    nop

    :goto_2b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_4a

    nop

    :goto_2c
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_70

    nop

    :goto_2d
    goto :goto_39

    :goto_2e
    goto/32 :goto_75

    nop

    :goto_2f
    iget-object v1, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mCpuAbi:Ljava/lang/String;

    goto/32 :goto_4d

    nop

    :goto_30
    invoke-virtual {p0}, Lcom/android/settings/development/DSULoader$DSUPackage;->getDeviceOs()I

    move-result v1

    goto/32 :goto_56

    nop

    :goto_31
    if-ltz v1, :cond_4

    goto/32 :goto_55

    :cond_4
    goto/32 :goto_e

    nop

    :goto_32
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_11

    nop

    :goto_33
    move v4, v0

    :goto_34
    goto/32 :goto_32

    nop

    :goto_35
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_19

    nop

    :goto_36
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_46

    nop

    :goto_37
    iget-object v2, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mSPL:Ljava/util/Date;

    goto/32 :goto_50

    nop

    :goto_38
    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_39
    goto/32 :goto_44

    nop

    :goto_3a
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_43

    nop

    :goto_3b
    move v0, v4

    goto/32 :goto_26

    nop

    :goto_3c
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_7

    nop

    :goto_3d
    const-string v2, "Device SPL:"

    goto/32 :goto_16

    nop

    :goto_3e
    const-string v5, " < "

    goto/32 :goto_3a

    nop

    :goto_3f
    invoke-virtual {p0}, Lcom/android/settings/development/DSULoader$DSUPackage;->getDeviceSPL()Ljava/util/Date;

    move-result-object v1

    goto/32 :goto_52

    nop

    :goto_40
    invoke-virtual {p0}, Lcom/android/settings/development/DSULoader$DSUPackage;->getDeviceCpu()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_2f

    nop

    :goto_41
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_24

    nop

    :goto_42
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_6f

    nop

    :goto_43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_29

    nop

    :goto_44
    move v0, v4

    goto/32 :goto_54

    nop

    :goto_45
    if-eqz v2, :cond_5

    goto/32 :goto_2e

    :cond_5
    goto/32 :goto_12

    nop

    :goto_46
    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_2d

    nop

    :goto_47
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_67

    nop

    :goto_48
    move v0, v2

    :goto_49
    goto/32 :goto_1a

    nop

    :goto_4a
    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_0

    nop

    :goto_4b
    move v5, v4

    :goto_4c
    goto/32 :goto_4e

    nop

    :goto_4d
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto/32 :goto_51

    nop

    :goto_4e
    iget-object v6, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mVndk:[I

    goto/32 :goto_f

    nop

    :goto_4f
    invoke-virtual {v1}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_42

    nop

    :goto_50
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    goto/32 :goto_4

    nop

    :goto_51
    const/4 v2, 0x1

    goto/32 :goto_28

    nop

    :goto_52
    if-eqz v1, :cond_6

    goto/32 :goto_74

    :cond_6
    goto/32 :goto_1d

    nop

    :goto_53
    const-string v1, " not found"

    goto/32 :goto_23

    nop

    :goto_54
    goto/16 :goto_2e

    :goto_55
    goto/32 :goto_4b

    nop

    :goto_56
    if-ltz v1, :cond_7

    goto/32 :goto_58

    :cond_7
    goto/32 :goto_5d

    nop

    :goto_57
    goto :goto_66

    :goto_58
    goto/32 :goto_17

    nop

    :goto_59
    move v2, v4

    :goto_5a
    goto/32 :goto_45

    nop

    :goto_5b
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_5c
    if-lt v5, v7, :cond_8

    goto/32 :goto_63

    :cond_8
    goto/32 :goto_6e

    nop

    :goto_5d
    const-string v0, "Failed to getDeviceOs"

    goto/32 :goto_6c

    nop

    :goto_5e
    if-gtz v1, :cond_9

    goto/32 :goto_66

    :cond_9
    goto/32 :goto_30

    nop

    :goto_5f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_1c

    nop

    :goto_60
    goto :goto_5a

    :goto_61
    goto/32 :goto_c

    nop

    :goto_62
    goto :goto_4c

    :goto_63
    goto/32 :goto_59

    nop

    :goto_64
    iget-object v5, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mCpuAbi:Ljava/lang/String;

    goto/32 :goto_2c

    nop

    :goto_65
    goto :goto_6d

    :goto_66
    goto/32 :goto_5

    nop

    :goto_67
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_53

    nop

    :goto_68
    return v4

    :goto_69
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_b

    nop

    :goto_6a
    iget-object v1, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mSPL:Ljava/util/Date;

    goto/32 :goto_18

    nop

    :goto_6b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_3d

    nop

    :goto_6c
    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_6d
    goto/32 :goto_d

    nop

    :goto_6e
    aget v6, v6, v5

    goto/32 :goto_21

    nop

    :goto_6f
    const-string v1, " > "

    goto/32 :goto_1f

    nop

    :goto_70
    const-string v5, " != "

    goto/32 :goto_8

    nop

    :goto_71
    if-gtz v2, :cond_a

    goto/32 :goto_1

    :cond_a
    goto/32 :goto_1b

    nop

    :goto_72
    invoke-virtual {p0}, Lcom/android/settings/development/DSULoader$DSUPackage;->getDeviceVndk()I

    move-result v1

    goto/32 :goto_31

    nop

    :goto_73
    goto/16 :goto_34

    :goto_74
    goto/32 :goto_13

    nop

    :goto_75
    iget-object v1, p0, Lcom/android/settings/development/DSULoader$DSUPackage;->mSPL:Ljava/util/Date;

    goto/32 :goto_76

    nop

    :goto_76
    if-nez v1, :cond_b

    goto/32 :goto_1

    :cond_b
    goto/32 :goto_3f

    nop

    :goto_77
    const/4 v4, 0x0

    goto/32 :goto_25

    nop
.end method
