.class public Lcom/android/settings/development/FiveGNrcaPreferenceController;
.super Lcom/android/settings/core/BasePreferenceController;

# interfaces
.implements Lcom/android/settingslib/core/lifecycle/LifecycleObserver;
.implements Lcom/android/settingslib/core/lifecycle/events/OnResume;
.implements Lcom/android/settingslib/core/lifecycle/events/OnPause;


# static fields
.field private static final NRCA_SWITCH_KEY:Ljava/lang/String; = "nrca_switch"

.field private static final PHONE_COUNT_MAX:I = 0x2

.field private static final SIM_NRCA_CATEGORY_KEY:Ljava/lang/String; = "_nrca_category_key"

.field private static final TAG:Ljava/lang/String; = "FiveGNrcaPreferenceController"


# instance fields
.field private mAirplaneMode:Z

.field private mContext:Landroid/content/Context;

.field private final mNrcaReceiver:Landroid/content/BroadcastReceiver;

.field private mNrcaSwitchView:[Lcom/android/settings/development/NrcaSwitchView;

.field private mSwitchCategory:[Landroidx/preference/PreferenceCategory;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAirplaneMode(Lcom/android/settings/development/FiveGNrcaPreferenceController;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController;->mAirplaneMode:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmAirplaneMode(Lcom/android/settings/development/FiveGNrcaPreferenceController;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController;->mAirplaneMode:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdatePreference(Lcom/android/settings/development/FiveGNrcaPreferenceController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/development/FiveGNrcaPreferenceController;->updatePreference()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const/4 p2, 0x2

    new-array v0, p2, [Landroidx/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController;->mSwitchCategory:[Landroidx/preference/PreferenceCategory;

    new-array p2, p2, [Lcom/android/settings/development/NrcaSwitchView;

    iput-object p2, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController;->mNrcaSwitchView:[Lcom/android/settings/development/NrcaSwitchView;

    const/4 p2, 0x0

    iput-boolean p2, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController;->mAirplaneMode:Z

    new-instance v0, Lcom/android/settings/development/FiveGNrcaPreferenceController$1;

    invoke-direct {v0, p0}, Lcom/android/settings/development/FiveGNrcaPreferenceController$1;-><init>(Lcom/android/settings/development/FiveGNrcaPreferenceController;)V

    iput-object v0, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController;->mNrcaReceiver:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string v0, "airplane_mode_on"

    const/4 v1, -0x1

    invoke-static {p1, v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    move p2, v0

    :cond_0
    iput-boolean p2, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController;->mAirplaneMode:Z

    return-void
.end method

.method private getPhoneCount()I
    .locals 0

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object p0

    invoke-virtual {p0}, Lmiui/telephony/TelephonyManager;->getPhoneCount()I

    move-result p0

    return p0
.end method

.method private getSubscriptionInfoBySlot(I)Lmiui/telephony/SubscriptionInfo;
    .locals 3

    invoke-static {}, Lmiui/telephony/SubscriptionManager;->getDefault()Lmiui/telephony/SubscriptionManager;

    move-result-object p0

    invoke-virtual {p0}, Lmiui/telephony/SubscriptionManager;->getAvailableSubscriptionInfoList()Ljava/util/List;

    move-result-object p0

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/telephony/SubscriptionInfo;

    invoke-virtual {v1}, Lmiui/telephony/SubscriptionInfo;->getSlotId()I

    move-result v2

    if-ne v2, p1, :cond_1

    return-object v1

    :cond_2
    return-object v0
.end method

.method private registerBroadcastReceiver()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.telephony.action.CARRIER_CONFIG_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController;->mContext:Landroid/content/Context;

    iget-object p0, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController;->mNrcaReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const-string p0, "FiveGNrcaPreferenceController"

    const-string/jumbo v0, "register broadcastreceiver"

    invoke-static {p0, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private updatePreference()V
    .locals 8

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-direct {p0}, Lcom/android/settings/development/FiveGNrcaPreferenceController;->getPhoneCount()I

    move-result v2

    if-ge v1, v2, :cond_4

    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lmiui/telephony/TelephonyManager;->hasIccCard(I)Z

    move-result v2

    iget-object v3, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController;->mSwitchCategory:[Landroidx/preference/PreferenceCategory;

    aget-object v3, v3, v1

    invoke-virtual {v3, v2}, Landroidx/preference/Preference;->setVisible(Z)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "updatePreference: isSimInsert = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "FiveGNrcaPreferenceController"

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v2, :cond_3

    invoke-direct {p0, v1}, Lcom/android/settings/development/FiveGNrcaPreferenceController;->getSubscriptionInfoBySlot(I)Lmiui/telephony/SubscriptionInfo;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/android/settings/development/FiveGNrcaPreferenceController;->getPhoneCount()I

    move-result v3

    const/4 v5, 0x1

    if-ne v3, v5, :cond_0

    invoke-virtual {v2}, Lmiui/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_0
    iget-object v3, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController;->mContext:Landroid/content/Context;

    sget v6, Lcom/android/settings/R$string;->sim_nrca_title:I

    invoke-virtual {v3, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    add-int/lit8 v7, v1, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v0

    invoke-virtual {v2}, Lmiui/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v5

    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    iget-object v3, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController;->mSwitchCategory:[Landroidx/preference/PreferenceCategory;

    aget-object v3, v3, v1

    invoke-virtual {v3, v2}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mSwitchCategoryTitle: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v2, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController;->mNrcaSwitchView:[Lcom/android/settings/development/NrcaSwitchView;

    aget-object v3, v2, v1

    if-nez v3, :cond_2

    new-instance v3, Lcom/android/settings/development/NrcaSwitchView;

    iget-object v4, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController;->mSwitchCategory:[Landroidx/preference/PreferenceCategory;

    aget-object v4, v4, v1

    iget-object v5, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4, v5, v1}, Lcom/android/settings/development/NrcaSwitchView;-><init>(Landroidx/preference/PreferenceGroup;Landroid/content/Context;I)V

    aput-object v3, v2, v1

    :cond_2
    iget-object v2, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController;->mNrcaSwitchView:[Lcom/android/settings/development/NrcaSwitchView;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/android/settings/development/NrcaSwitchView;->updateNrcaButtonUI()V

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_4
    return-void
.end method


# virtual methods
.method public displayPreference(Landroidx/preference/PreferenceScreen;)V
    .locals 5

    invoke-super {p0, p1}, Lcom/android/settings/core/BasePreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "sim"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "_nrca_category_key"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v1

    check-cast v1, Landroidx/preference/PreferenceCategory;

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    new-instance v2, Landroidx/preference/SwitchPreference;

    iget-object v3, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroidx/preference/SwitchPreference;-><init>(Landroid/content/Context;)V

    sget v3, Lcom/android/settings/R$string;->nrca_switch_title:I

    invoke-virtual {v2, v3}, Landroidx/preference/Preference;->setTitle(I)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "nrca_switch"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroidx/preference/Preference;->setKey(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    iget-object v2, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController;->mSwitchCategory:[Landroidx/preference/PreferenceCategory;

    aput-object v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public getAvailabilityStatus()I
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method public bridge synthetic getBackgroundWorkerClass()Ljava/lang/Class;
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->getBackgroundWorkerClass()Ljava/lang/Class;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic getIntentFilter()Landroid/content/IntentFilter;
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->getIntentFilter()Landroid/content/IntentFilter;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic getSliceHighlightMenuRes()I
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->getSliceHighlightMenuRes()I

    move-result p0

    return p0
.end method

.method public bridge synthetic hasAsyncUpdate()Z
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->hasAsyncUpdate()Z

    move-result p0

    return p0
.end method

.method public bridge synthetic isPublicSlice()Z
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->isPublicSlice()Z

    move-result p0

    return p0
.end method

.method public bridge synthetic isSliceable()Z
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->isSliceable()Z

    move-result p0

    return p0
.end method

.method public onPause()V
    .locals 2

    const-string v0, "FiveGNrcaPreferenceController"

    const-string/jumbo v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController;->mContext:Landroid/content/Context;

    iget-object p0, p0, Lcom/android/settings/development/FiveGNrcaPreferenceController;->mNrcaReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onResume()V
    .locals 2

    const-string v0, "FiveGNrcaPreferenceController"

    const-string/jumbo v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/settings/development/FiveGNrcaPreferenceController;->registerBroadcastReceiver()V

    invoke-direct {p0}, Lcom/android/settings/development/FiveGNrcaPreferenceController;->updatePreference()V

    return-void
.end method

.method public bridge synthetic useDynamicSliceSummary()Z
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->useDynamicSliceSummary()Z

    move-result p0

    return p0
.end method
