.class Lcom/android/settings/CutoutModeSettings$AppAdapter$ItemViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/CutoutModeSettings$AppAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ItemViewHolder"
.end annotation


# instance fields
.field public icon:Landroid/widget/ImageView;

.field public summary:Landroid/widget/TextView;

.field final synthetic this$1:Lcom/android/settings/CutoutModeSettings$AppAdapter;

.field public title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/android/settings/CutoutModeSettings$AppAdapter;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/CutoutModeSettings$AppAdapter$ItemViewHolder;->this$1:Lcom/android/settings/CutoutModeSettings$AppAdapter;

    invoke-direct {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    const p1, 0x1020006

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/android/settings/CutoutModeSettings$AppAdapter$ItemViewHolder;->icon:Landroid/widget/ImageView;

    const p1, 0x1020016

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/android/settings/CutoutModeSettings$AppAdapter$ItemViewHolder;->title:Landroid/widget/TextView;

    const p1, 0x1020010

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/android/settings/CutoutModeSettings$AppAdapter$ItemViewHolder;->summary:Landroid/widget/TextView;

    return-void
.end method
