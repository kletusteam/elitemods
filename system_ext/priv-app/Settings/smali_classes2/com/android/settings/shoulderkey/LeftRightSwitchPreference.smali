.class public Lcom/android/settings/shoulderkey/LeftRightSwitchPreference;
.super Lcom/android/settingslib/miuisettings/preference/Preference;

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lmiuix/preference/FolmeAnimationController;


# instance fields
.field private mFilterSortView:Lmiuix/miuixbasewidget/widget/FilterSortView;

.field private mHandler:Landroid/os/Handler;

.field private mLastCheckId:I

.field private mLeftShoulderKeyView:Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;

.field private mRightShoulderKeyView:Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/settings/shoulderkey/LeftRightSwitchPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object p2, p0, Lcom/android/settings/shoulderkey/LeftRightSwitchPreference;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settingslib/miuisettings/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget p1, Lcom/android/settings/R$layout;->left_right_switch_preference:I

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setLayoutResource(I)V

    return-void
.end method


# virtual methods
.method public isTouchAnimationEnable()Z
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settingslib/miuisettings/preference/Preference;->onBindView(Landroid/view/View;)V

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, Lmiuix/animation/Folme;->clean([Ljava/lang/Object;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundColor(I)V

    sget v0, Lcom/android/settings/R$id;->filter_sort_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/miuixbasewidget/widget/FilterSortView;

    iput-object v0, p0, Lcom/android/settings/shoulderkey/LeftRightSwitchPreference;->mFilterSortView:Lmiuix/miuixbasewidget/widget/FilterSortView;

    sget v0, Lcom/android/settings/R$id;->left_shoulder_key:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;

    iput-object v0, p0, Lcom/android/settings/shoulderkey/LeftRightSwitchPreference;->mLeftShoulderKeyView:Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;

    sget v0, Lcom/android/settings/R$id;->right_shoulder_key:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;

    iput-object p1, p0, Lcom/android/settings/shoulderkey/LeftRightSwitchPreference;->mRightShoulderKeyView:Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;

    iget-object p1, p0, Lcom/android/settings/shoulderkey/LeftRightSwitchPreference;->mFilterSortView:Lmiuix/miuixbasewidget/widget/FilterSortView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lmiuix/miuixbasewidget/widget/FilterSortView;->setTabIncatorVisibility(I)V

    iget-object p1, p0, Lcom/android/settings/shoulderkey/LeftRightSwitchPreference;->mFilterSortView:Lmiuix/miuixbasewidget/widget/FilterSortView;

    iget-object v0, p0, Lcom/android/settings/shoulderkey/LeftRightSwitchPreference;->mLeftShoulderKeyView:Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;

    invoke-virtual {p1, v0}, Lmiuix/miuixbasewidget/widget/FilterSortView;->setFilteredTab(Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;)V

    iget-object p1, p0, Lcom/android/settings/shoulderkey/LeftRightSwitchPreference;->mLeftShoulderKeyView:Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;

    invoke-virtual {p1, p0}, Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/android/settings/shoulderkey/LeftRightSwitchPreference;->mRightShoulderKeyView:Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;

    invoke-virtual {p1, p0}, Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    iget v0, p0, Lcom/android/settings/shoulderkey/LeftRightSwitchPreference;->mLastCheckId:I

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    sget v0, Lcom/android/settings/R$id;->left_shoulder_key:I

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/shoulderkey/LeftRightSwitchPreference;->mFilterSortView:Lmiuix/miuixbasewidget/widget/FilterSortView;

    iget-object v1, p0, Lcom/android/settings/shoulderkey/LeftRightSwitchPreference;->mLeftShoulderKeyView:Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;

    invoke-virtual {v0, v1}, Lmiuix/miuixbasewidget/widget/FilterSortView;->setFilteredTab(Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;)V

    iget-object v0, p0, Lcom/android/settings/shoulderkey/LeftRightSwitchPreference;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/shoulderkey/LeftRightSwitchPreference;->mFilterSortView:Lmiuix/miuixbasewidget/widget/FilterSortView;

    iget-object v1, p0, Lcom/android/settings/shoulderkey/LeftRightSwitchPreference;->mRightShoulderKeyView:Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;

    invoke-virtual {v0, v1}, Lmiuix/miuixbasewidget/widget/FilterSortView;->setFilteredTab(Lmiuix/miuixbasewidget/widget/FilterSortView$TabView;)V

    iget-object v0, p0, Lcom/android/settings/shoulderkey/LeftRightSwitchPreference;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    iput p1, p0, Lcom/android/settings/shoulderkey/LeftRightSwitchPreference;->mLastCheckId:I

    return-void
.end method
