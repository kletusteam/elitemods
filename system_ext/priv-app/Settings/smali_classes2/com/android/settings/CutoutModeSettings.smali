.class public Lcom/android/settings/CutoutModeSettings;
.super Lcom/android/settings/BaseFragment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/CutoutModeSettings$AppAdapter;,
        Lcom/android/settings/CutoutModeSettings$AppItem;,
        Lcom/android/settings/CutoutModeSettings$AppDialogAdapter;
    }
.end annotation


# static fields
.field public static final TAG_APP_ITEM:I


# instance fields
.field public mAdapter:Lcom/android/settings/CutoutModeSettings$AppAdapter;

.field private mApkIconLoader:Lcom/android/settings/ApkIconLoader;

.field private mAppDialogAdapter:Lcom/android/settings/CutoutModeSettings$AppDialogAdapter;

.field private mContentArray:[Ljava/lang/String;

.field public mContext:Landroid/content/Context;

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private mListView:Lmiuix/recyclerview/widget/RecyclerView;

.field private mPackageChangeReceiver:Landroid/content/BroadcastReceiver;

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mPkgAsyncTaskWithProgress:Lcom/android/settings/CutoutModeAsyncTaskWithProgress;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/settings/CutoutModeAsyncTaskWithProgress<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public mSupportApps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/CutoutModeSettings$AppItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetmApkIconLoader(Lcom/android/settings/CutoutModeSettings;)Lcom/android/settings/ApkIconLoader;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/CutoutModeSettings;->mApkIconLoader:Lcom/android/settings/ApkIconLoader;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContentArray(Lcom/android/settings/CutoutModeSettings;)[Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/CutoutModeSettings;->mContentArray:[Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPackageManager(Lcom/android/settings/CutoutModeSettings;)Landroid/content/pm/PackageManager;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/CutoutModeSettings;->mPackageManager:Landroid/content/pm/PackageManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mloadPackages(Lcom/android/settings/CutoutModeSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/CutoutModeSettings;->loadPackages()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mprepareCutoutDialogBuild(Lcom/android/settings/CutoutModeSettings;Lmiuix/appcompat/app/AlertDialog$Builder;Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settings/CutoutModeSettings;->prepareCutoutDialogBuild(Lmiuix/appcompat/app/AlertDialog$Builder;Ljava/lang/String;II)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    sget v0, Lcom/android/settings/R$layout;->cutout_mode_app_list:I

    sput v0, Lcom/android/settings/CutoutModeSettings;->TAG_APP_ITEM:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/BaseFragment;-><init>()V

    new-instance v0, Lcom/android/settings/CutoutModeSettings$3;

    invoke-direct {v0, p0}, Lcom/android/settings/CutoutModeSettings$3;-><init>(Lcom/android/settings/CutoutModeSettings;)V

    iput-object v0, p0, Lcom/android/settings/CutoutModeSettings;->mPackageChangeReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/CutoutModeSettings;->mSupportApps:Ljava/util/List;

    return-void
.end method

.method private isAttatched()Z
    .locals 0

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->getAppCompatActivity()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private loadPackages()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/CutoutModeSettings;->mPkgAsyncTaskWithProgress:Lcom/android/settings/CutoutModeAsyncTaskWithProgress;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/CutoutModeSettings;->mPkgAsyncTaskWithProgress:Lcom/android/settings/CutoutModeAsyncTaskWithProgress;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_0
    new-instance v0, Lcom/android/settings/CutoutModeAsyncTaskWithProgress;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/CutoutModeAsyncTaskWithProgress;-><init>(Landroidx/fragment/app/FragmentManager;)V

    iput-object v0, p0, Lcom/android/settings/CutoutModeSettings;->mPkgAsyncTaskWithProgress:Lcom/android/settings/CutoutModeAsyncTaskWithProgress;

    invoke-virtual {v0, p0}, Lcom/android/settings/CutoutModeAsyncTaskWithProgress;->setContext(Lcom/android/settings/CutoutModeSettings;)V

    iget-object p0, p0, Lcom/android/settings/CutoutModeSettings;->mPkgAsyncTaskWithProgress:Lcom/android/settings/CutoutModeAsyncTaskWithProgress;

    sget v0, Lcom/android/settings/R$string;->max_aspect_settings_all_app_display_loading:I

    invoke-virtual {p0, v0}, Lmiuix/os/AsyncTaskWithProgress;->setMessage(I)Lmiuix/os/AsyncTaskWithProgress;

    move-result-object p0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiuix/os/AsyncTaskWithProgress;->setCancelable(Z)Lmiuix/os/AsyncTaskWithProgress;

    move-result-object p0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p0, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private prepareCutoutDialogBuild(Lmiuix/appcompat/app/AlertDialog$Builder;Ljava/lang/String;II)V
    .locals 3

    new-instance v0, Lcom/android/settings/CutoutModeSettings$AppDialogAdapter;

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->getAppCompatActivity()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/CutoutModeSettings;->mContentArray:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, p3}, Lcom/android/settings/CutoutModeSettings$AppDialogAdapter;-><init>(Landroid/content/Context;[Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/android/settings/CutoutModeSettings;->mAppDialogAdapter:Lcom/android/settings/CutoutModeSettings$AppDialogAdapter;

    new-instance v1, Lcom/android/settings/CutoutModeSettings$2;

    invoke-direct {v1, p0, p3, p4, p2}, Lcom/android/settings/CutoutModeSettings$2;-><init>(Lcom/android/settings/CutoutModeSettings;IILjava/lang/String;)V

    invoke-virtual {p1, v0, p3, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    const/4 p0, 0x0

    invoke-virtual {p1, p0, p0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    invoke-virtual {p1, p0, p0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    return-void
.end method


# virtual methods
.method public getAppItem(Landroid/content/pm/ApplicationInfo;I)Lcom/android/settings/CutoutModeSettings$AppItem;
    .locals 1

    new-instance v0, Lcom/android/settings/CutoutModeSettings$AppItem;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/settings/CutoutModeSettings$AppItem;-><init>(Lcom/android/settings/CutoutModeSettings;Landroid/content/pm/ApplicationInfo;I)V

    return-object v0
.end method

.method public getAppItem(Ljava/lang/String;)Lcom/android/settings/CutoutModeSettings$AppItem;
    .locals 1

    new-instance v0, Lcom/android/settings/CutoutModeSettings$AppItem;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/CutoutModeSettings$AppItem;-><init>(Lcom/android/settings/CutoutModeSettings;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->getAppCompatActivity()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/CutoutModeSettings;->mContext:Landroid/content/Context;

    new-instance p1, Lcom/android/settings/ApkIconLoader;

    iget-object v0, p0, Lcom/android/settings/CutoutModeSettings;->mContext:Landroid/content/Context;

    invoke-direct {p1, v0}, Lcom/android/settings/ApkIconLoader;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/android/settings/CutoutModeSettings;->mApkIconLoader:Lcom/android/settings/ApkIconLoader;

    invoke-virtual {p0}, Lcom/android/settings/BaseFragment;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/CutoutModeSettings;->mPackageManager:Landroid/content/pm/PackageManager;

    new-instance p1, Landroid/content/IntentFilter;

    invoke-direct {p1}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.PACKAGE_FULLY_REMOVED"

    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/CutoutModeSettings;->mContext:Landroid/content/Context;

    iget-object p0, p0, Lcom/android/settings/CutoutModeSettings;->mPackageChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, p0, p1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/CutoutModeSettings;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/CutoutModeSettings;->mPackageChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/settings/CutoutModeSettings;->mApkIconLoader:Lcom/android/settings/ApkIconLoader;

    invoke-virtual {v0}, Lcom/android/settings/ApkIconLoader;->stop()V

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Lcom/android/settings/CutoutModeSettings;->mLayoutInflater:Landroid/view/LayoutInflater;

    sget p0, Lcom/android/settings/R$layout;->cutout_mode_app_list:I

    const/4 p3, 0x0

    invoke-virtual {p1, p0, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    instance-of p2, p1, Landroid/preference/PreferenceFrameLayout$LayoutParams;

    if-eqz p2, :cond_0

    check-cast p1, Landroid/preference/PreferenceFrameLayout$LayoutParams;

    const/4 p2, 0x1

    iput-boolean p2, p1, Landroid/preference/PreferenceFrameLayout$LayoutParams;->removeBorders:Z

    :cond_0
    return-object p0
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onPause()V

    iget-object v0, p0, Lcom/android/settings/CutoutModeSettings;->mPkgAsyncTaskWithProgress:Lcom/android/settings/CutoutModeAsyncTaskWithProgress;

    invoke-virtual {v0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/CutoutModeSettings;->mPkgAsyncTaskWithProgress:Lcom/android/settings/CutoutModeAsyncTaskWithProgress;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/CutoutModeSettings;->loadPackages()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/android/settings/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    sget p2, Lcom/android/settings/R$id;->listview:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lmiuix/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/android/settings/CutoutModeSettings;->mListView:Lmiuix/recyclerview/widget/RecyclerView;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/android/settings/R$array;->cutout_mode_entries:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/CutoutModeSettings;->mContentArray:[Ljava/lang/String;

    iget-object p1, p0, Lcom/android/settings/CutoutModeSettings;->mListView:Lmiuix/recyclerview/widget/RecyclerView;

    new-instance p2, Landroidx/recyclerview/widget/LinearLayoutManager;

    iget-object v0, p0, Lcom/android/settings/CutoutModeSettings;->mContext:Landroid/content/Context;

    invoke-direct {p2, v0}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    new-instance p1, Lcom/android/settings/CutoutModeSettings$AppAdapter;

    invoke-direct {p1, p0}, Lcom/android/settings/CutoutModeSettings$AppAdapter;-><init>(Lcom/android/settings/CutoutModeSettings;)V

    iput-object p1, p0, Lcom/android/settings/CutoutModeSettings;->mAdapter:Lcom/android/settings/CutoutModeSettings$AppAdapter;

    iget-object p0, p0, Lcom/android/settings/CutoutModeSettings;->mListView:Lmiuix/recyclerview/widget/RecyclerView;

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void
.end method

.method showCutoutModeDialog(Lcom/android/settings/CutoutModeSettings$AppItem;I)V
    .locals 7

    goto/32 :goto_b

    nop

    :goto_0
    if-eqz v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop

    :goto_1
    return-void

    :goto_2
    move v6, p2

    goto/32 :goto_8

    nop

    :goto_3
    return-void

    :goto_4
    goto/32 :goto_7

    nop

    :goto_5
    move-object v2, p0

    goto/32 :goto_f

    nop

    :goto_6
    invoke-virtual {v0, p0}, Lcom/android/settings/CommonDialog;->setTitle(Ljava/lang/String;)V

    goto/32 :goto_a

    nop

    :goto_7
    new-instance v0, Lcom/android/settings/CutoutModeSettings$1;

    goto/32 :goto_c

    nop

    :goto_8
    invoke-direct/range {v1 .. v6}, Lcom/android/settings/CutoutModeSettings$1;-><init>(Lcom/android/settings/CutoutModeSettings;Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;Lcom/android/settings/CutoutModeSettings$AppItem;I)V

    goto/32 :goto_e

    nop

    :goto_9
    move-object v1, v0

    goto/32 :goto_5

    nop

    :goto_a
    invoke-virtual {v0}, Lcom/android/settings/CommonDialog;->show()V

    goto/32 :goto_1

    nop

    :goto_b
    invoke-direct {p0}, Lcom/android/settings/CutoutModeSettings;->isAttatched()Z

    move-result v0

    goto/32 :goto_0

    nop

    :goto_c
    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->getAppCompatActivity()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v3

    goto/32 :goto_d

    nop

    :goto_d
    const/4 v4, 0x0

    goto/32 :goto_9

    nop

    :goto_e
    iget-object p0, p1, Lcom/android/settings/CutoutModeSettings$AppItem;->mLabel:Ljava/lang/String;

    goto/32 :goto_6

    nop

    :goto_f
    move-object v5, p1

    goto/32 :goto_2

    nop
.end method
