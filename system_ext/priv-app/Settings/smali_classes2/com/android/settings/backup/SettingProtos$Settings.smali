.class public final Lcom/android/settings/backup/SettingProtos$Settings;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/backup/SettingProtos$Settings$Builder;
    }
.end annotation


# static fields
.field public static final LOCK_FIELD_NUMBER:I = 0x3

.field public static final SECURE_FIELD_NUMBER:I = 0x2

.field public static final SYSTEM_FIELD_NUMBER:I = 0x1

.field private static final defaultInstance:Lcom/android/settings/backup/SettingProtos$Settings;

.field private static final serialVersionUID:J


# instance fields
.field private lock_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/backup/SettingProtos$LockSetting;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private secure_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/backup/SettingProtos$SecureSetting;",
            ">;"
        }
    .end annotation
.end field

.field private system_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/backup/SettingProtos$SystemSetting;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetlock_(Lcom/android/settings/backup/SettingProtos$Settings;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->lock_:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetsecure_(Lcom/android/settings/backup/SettingProtos$Settings;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->secure_:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetsystem_(Lcom/android/settings/backup/SettingProtos$Settings;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->system_:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputlock_(Lcom/android/settings/backup/SettingProtos$Settings;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/backup/SettingProtos$Settings;->lock_:Ljava/util/List;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputsecure_(Lcom/android/settings/backup/SettingProtos$Settings;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/backup/SettingProtos$Settings;->secure_:Ljava/util/List;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputsystem_(Lcom/android/settings/backup/SettingProtos$Settings;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/backup/SettingProtos$Settings;->system_:Ljava/util/List;

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/android/settings/backup/SettingProtos$Settings;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/android/settings/backup/SettingProtos$Settings;-><init>(Z)V

    sput-object v0, Lcom/android/settings/backup/SettingProtos$Settings;->defaultInstance:Lcom/android/settings/backup/SettingProtos$Settings;

    invoke-direct {v0}, Lcom/android/settings/backup/SettingProtos$Settings;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/android/settings/backup/SettingProtos$Settings$Builder;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    const/4 p1, -0x1

    iput-byte p1, p0, Lcom/android/settings/backup/SettingProtos$Settings;->memoizedIsInitialized:B

    iput p1, p0, Lcom/android/settings/backup/SettingProtos$Settings;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/backup/SettingProtos$Settings$Builder;Lcom/android/settings/backup/SettingProtos$Settings-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/backup/SettingProtos$Settings;-><init>(Lcom/android/settings/backup/SettingProtos$Settings$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const/4 p1, -0x1

    iput-byte p1, p0, Lcom/android/settings/backup/SettingProtos$Settings;->memoizedIsInitialized:B

    iput p1, p0, Lcom/android/settings/backup/SettingProtos$Settings;->memoizedSerializedSize:I

    return-void
.end method

.method public static getDefaultInstance()Lcom/android/settings/backup/SettingProtos$Settings;
    .locals 1

    sget-object v0, Lcom/android/settings/backup/SettingProtos$Settings;->defaultInstance:Lcom/android/settings/backup/SettingProtos$Settings;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->system_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->secure_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->lock_:Ljava/util/List;

    return-void
.end method

.method public static newBuilder()Lcom/android/settings/backup/SettingProtos$Settings$Builder;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->-$$Nest$smcreate()Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/SettingProtos$Settings$Builder;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$Settings;->newBuilder()Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->mergeFrom(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getDefaultInstanceForType()Lcom/android/settings/backup/SettingProtos$Settings;
    .locals 0

    sget-object p0, Lcom/android/settings/backup/SettingProtos$Settings;->defaultInstance:Lcom/android/settings/backup/SettingProtos$Settings;

    return-object p0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$Settings;->getDefaultInstanceForType()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object p0

    return-object p0
.end method

.method public getLockList()Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/android/settings/backup/SettingProtos$LockSetting;",
            ">;"
        }
    .end annotation

    iget-object p0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->lock_:Ljava/util/List;

    return-object p0
.end method

.method public getSecureList()Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/android/settings/backup/SettingProtos$SecureSetting;",
            ">;"
        }
    .end annotation

    iget-object p0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->secure_:Ljava/util/List;

    return-object p0
.end method

.method public getSerializedSize()I
    .locals 5

    iget v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    return v0

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    move v2, v1

    :goto_0
    iget-object v3, p0, Lcom/android/settings/backup/SettingProtos$Settings;->system_:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    iget-object v3, p0, Lcom/android/settings/backup/SettingProtos$Settings;->system_:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/protobuf/MessageLite;

    const/4 v4, 0x1

    invoke-static {v4, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v1, v0

    :goto_1
    iget-object v3, p0, Lcom/android/settings/backup/SettingProtos$Settings;->secure_:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/android/settings/backup/SettingProtos$Settings;->secure_:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    :goto_2
    iget-object v1, p0, Lcom/android/settings/backup/SettingProtos$Settings;->lock_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    const/4 v1, 0x3

    iget-object v3, p0, Lcom/android/settings/backup/SettingProtos$Settings;->lock_:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/protobuf/MessageLite;

    invoke-static {v1, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v2, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iput v2, p0, Lcom/android/settings/backup/SettingProtos$Settings;->memoizedSerializedSize:I

    return v2
.end method

.method public getSystemList()Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/android/settings/backup/SettingProtos$SystemSetting;",
            ">;"
        }
    .end annotation

    iget-object p0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->system_:Ljava/util/List;

    return-object p0
.end method

.method public final isInitialized()Z
    .locals 3

    iget-byte v0, p0, Lcom/android/settings/backup/SettingProtos$Settings;->memoizedIsInitialized:B

    const/4 v1, 0x1

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_1
    iput-byte v1, p0, Lcom/android/settings/backup/SettingProtos$Settings;->memoizedIsInitialized:B

    return v1
.end method

.method public newBuilderForType()Lcom/android/settings/backup/SettingProtos$Settings$Builder;
    .locals 0

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$Settings;->newBuilder()Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$Settings;->newBuilderForType()Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    move-result-object p0

    return-object p0
.end method

.method public toBuilder()Lcom/android/settings/backup/SettingProtos$Settings$Builder;
    .locals 0

    invoke-static {p0}, Lcom/android/settings/backup/SettingProtos$Settings;->newBuilder(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$Settings;->toBuilder()Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    move-result-object p0

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$Settings;->getSerializedSize()I

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v2, p0, Lcom/android/settings/backup/SettingProtos$Settings;->system_:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/backup/SettingProtos$Settings;->system_:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/protobuf/MessageLite;

    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    move v1, v0

    :goto_1
    iget-object v2, p0, Lcom/android/settings/backup/SettingProtos$Settings;->secure_:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/android/settings/backup/SettingProtos$Settings;->secure_:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    :goto_2
    iget-object v1, p0, Lcom/android/settings/backup/SettingProtos$Settings;->lock_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/android/settings/backup/SettingProtos$Settings;->lock_:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    return-void
.end method
