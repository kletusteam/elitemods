.class public Lcom/android/settings/backup/SettingsBackupAgent;
.super Lmiui/app/backup/FullBackupAgent;


# instance fields
.field private mAgent:Lcom/android/settings/backup/AgentBase;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiui/app/backup/FullBackupAgent;-><init>()V

    return-void
.end method

.method private initAgent(I)V
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/android/settings/backup/AccountAgent;

    invoke-direct {p1, p0}, Lcom/android/settings/backup/AccountAgent;-><init>(Lmiui/app/backup/FullBackupAgent;)V

    iput-object p1, p0, Lcom/android/settings/backup/SettingsBackupAgent;->mAgent:Lcom/android/settings/backup/AgentBase;

    goto :goto_0

    :cond_1
    new-instance p1, Lcom/android/settings/backup/WifiAgent;

    invoke-direct {p1, p0}, Lcom/android/settings/backup/WifiAgent;-><init>(Lmiui/app/backup/FullBackupAgent;)V

    iput-object p1, p0, Lcom/android/settings/backup/SettingsBackupAgent;->mAgent:Lcom/android/settings/backup/AgentBase;

    goto :goto_0

    :cond_2
    new-instance p1, Lcom/android/settings/backup/SettingsAgent;

    invoke-direct {p1, p0}, Lcom/android/settings/backup/SettingsAgent;-><init>(Lmiui/app/backup/FullBackupAgent;)V

    iput-object p1, p0, Lcom/android/settings/backup/SettingsBackupAgent;->mAgent:Lcom/android/settings/backup/AgentBase;

    :goto_0
    return-void
.end method


# virtual methods
.method protected getVersion(I)I
    .locals 1

    iget-object v0, p0, Lcom/android/settings/backup/SettingsBackupAgent;->mAgent:Lcom/android/settings/backup/AgentBase;

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/settings/backup/SettingsBackupAgent;->initAgent(I)V

    :cond_0
    iget-object p0, p0, Lcom/android/settings/backup/SettingsBackupAgent;->mAgent:Lcom/android/settings/backup/AgentBase;

    invoke-virtual {p0}, Lcom/android/settings/backup/AgentBase;->getBackupVersion()I

    move-result p0

    return p0
.end method

.method protected onAttachRestore(Lmiui/app/backup/BackupMeta;Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)I
    .locals 0

    iget-object p0, p0, Lcom/android/settings/backup/SettingsBackupAgent;->mAgent:Lcom/android/settings/backup/AgentBase;

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1, p2, p3}, Lcom/android/settings/backup/AgentBase;->restoreAttaches(Lmiui/app/backup/BackupMeta;Landroid/os/ParcelFileDescriptor;Ljava/lang/String;)I

    move-result p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x1

    :goto_0
    return p0
.end method

.method protected onDataRestore(Lmiui/app/backup/BackupMeta;Landroid/os/ParcelFileDescriptor;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/settings/backup/SettingsBackupAgent;->mAgent:Lcom/android/settings/backup/AgentBase;

    if-nez v0, :cond_0

    iget v0, p1, Lmiui/app/backup/BackupMeta;->feature:I

    invoke-direct {p0, v0}, Lcom/android/settings/backup/SettingsBackupAgent;->initAgent(I)V

    :cond_0
    iget-object p0, p0, Lcom/android/settings/backup/SettingsBackupAgent;->mAgent:Lcom/android/settings/backup/AgentBase;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/backup/AgentBase;->restoreData(Lmiui/app/backup/BackupMeta;Landroid/os/ParcelFileDescriptor;)I

    move-result p0

    return p0
.end method

.method protected onFullBackup(Landroid/os/ParcelFileDescriptor;I)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/settings/backup/SettingsBackupAgent;->mAgent:Lcom/android/settings/backup/AgentBase;

    if-nez v0, :cond_0

    invoke-direct {p0, p2}, Lcom/android/settings/backup/SettingsBackupAgent;->initAgent(I)V

    :cond_0
    iget-object p0, p0, Lcom/android/settings/backup/SettingsBackupAgent;->mAgent:Lcom/android/settings/backup/AgentBase;

    invoke-virtual {p0, p1}, Lcom/android/settings/backup/AgentBase;->fullBackup(Landroid/os/ParcelFileDescriptor;)I

    move-result p0

    return p0
.end method

.method protected onRestoreEnd(Lmiui/app/backup/BackupMeta;)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object p0, p0, Lcom/android/settings/backup/SettingsBackupAgent;->mAgent:Lcom/android/settings/backup/AgentBase;

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/settings/backup/AgentBase;->endRestore(Lmiui/app/backup/BackupMeta;)I

    move-result p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x1

    :goto_0
    return p0
.end method

.method protected tarAttaches(Ljava/lang/String;Landroid/app/backup/FullBackupDataOutput;I)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-super {p0, p1, p2, p3}, Lmiui/app/backup/FullBackupAgent;->tarAttaches(Ljava/lang/String;Landroid/app/backup/FullBackupDataOutput;I)I

    move-result p3

    if-nez p3, :cond_1

    iget-object p0, p0, Lcom/android/settings/backup/SettingsBackupAgent;->mAgent:Lcom/android/settings/backup/AgentBase;

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/backup/AgentBase;->tarAttaches(Ljava/lang/String;Landroid/app/backup/FullBackupDataOutput;)I

    move-result p3

    goto :goto_0

    :cond_0
    const/4 p3, 0x1

    :cond_1
    :goto_0
    return p3
.end method
