.class public final Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/backup/SyncRootProtos$SyncRoot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder<",
        "Lcom/android/settings/backup/SyncRootProtos$SyncRoot;",
        "Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;",
        ">;",
        "Lcom/google/protobuf/MessageLiteOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private setting_:Lcom/android/settings/backup/SettingProtos$Settings;


# direct methods
.method static bridge synthetic -$$Nest$mbuildParsed(Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;)Lcom/android/settings/backup/SyncRootProtos$SyncRoot;
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->buildParsed()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$smcreate()Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->create()Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;

    move-result-object v0

    return-object v0
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$Settings;->getDefaultInstance()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    invoke-direct {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method private buildParsed()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->buildPartial()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object p0

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    :cond_0
    invoke-static {p0}, Lcom/google/protobuf/AbstractMessageLite$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object p0

    invoke-virtual {p0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object p0

    throw p0
.end method

.method private static create()Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;
    .locals 1

    new-instance v0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;

    invoke-direct {v0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    return-void
.end method


# virtual methods
.method public build()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->buildPartial()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object p0

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    :cond_0
    invoke-static {p0}, Lcom/google/protobuf/AbstractMessageLite$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object p0

    throw p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->build()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object p0

    return-object p0
.end method

.method public buildPartial()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;
    .locals 3

    new-instance v0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;-><init>(Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;Lcom/android/settings/backup/SyncRootProtos$SyncRoot-IA;)V

    iget v1, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->bitField0_:I

    const/4 v2, 0x1

    and-int/2addr v1, v2

    if-ne v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    iget-object p0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    invoke-static {v0, p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->-$$Nest$fputsetting_(Lcom/android/settings/backup/SyncRootProtos$SyncRoot;Lcom/android/settings/backup/SettingProtos$Settings;)V

    invoke-static {v0, v2}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->-$$Nest$fputbitField0_(Lcom/android/settings/backup/SyncRootProtos$SyncRoot;I)V

    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->buildPartial()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object p0

    return-object p0
.end method

.method public clear()Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$Settings;->getDefaultInstance()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    iget v0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->bitField0_:I

    return-object p0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->clear()Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->clear()Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;

    move-result-object p0

    return-object p0
.end method

.method public clone()Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->create()Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->buildPartial()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->mergeFrom(Lcom/android/settings/backup/SyncRootProtos$SyncRoot;)Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->clone()Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->clone()Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->clone()Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->clone()Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;

    move-result-object p0

    return-object p0
.end method

.method public getDefaultInstanceForType()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;
    .locals 0

    invoke-static {}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->getDefaultInstance()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->getDefaultInstanceForType()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->getDefaultInstanceForType()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object p0

    return-object p0
.end method

.method public getSetting()Lcom/android/settings/backup/SettingProtos$Settings;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    return-object p0
.end method

.method public hasSetting()Z
    .locals 1

    iget p0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->bitField0_:I

    const/4 v0, 0x1

    and-int/2addr p0, v0

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isInitialized()Z
    .locals 0

    const/4 p0, 0x1

    return p0
.end method

.method public mergeFrom(Lcom/android/settings/backup/SyncRootProtos$SyncRoot;)Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->getDefaultInstance()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object v0

    if-ne p1, v0, :cond_0

    return-object p0

    :cond_0
    invoke-virtual {p1}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->hasSetting()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->getSetting()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->mergeSetting(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;

    :cond_1
    return-object p0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    if-eqz v0, :cond_3

    const/16 v1, 0x3a

    if-eq v0, v1, :cond_1

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p0

    :cond_1
    invoke-static {}, Lcom/android/settings/backup/SettingProtos$Settings;->newBuilder()Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->hasSetting()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->getSetting()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->mergeFrom(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->buildPartial()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->setSetting(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;

    goto :goto_0

    :cond_3
    return-object p0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 0

    check-cast p1, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    invoke-virtual {p0, p1}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->mergeFrom(Lcom/android/settings/backup/SyncRootProtos$SyncRoot;)Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;

    move-result-object p0

    return-object p0
.end method

.method public mergeSetting(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;
    .locals 3

    iget v0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->bitField0_:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$Settings;->getDefaultInstance()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v2

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    invoke-static {v0}, Lcom/android/settings/backup/SettingProtos$Settings;->newBuilder(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->mergeFrom(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->buildPartial()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    goto :goto_0

    :cond_0
    iput-object p1, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    :goto_0
    iget p1, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->bitField0_:I

    or-int/2addr p1, v1

    iput p1, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->bitField0_:I

    return-object p0
.end method

.method public setSetting(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;
    .locals 0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iput-object p1, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    iget p1, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->bitField0_:I

    or-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->bitField0_:I

    return-object p0
.end method
