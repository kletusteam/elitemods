.class public final Lcom/android/settings/backup/SettingProtos$Settings$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/backup/SettingProtos$Settings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder<",
        "Lcom/android/settings/backup/SettingProtos$Settings;",
        "Lcom/android/settings/backup/SettingProtos$Settings$Builder;",
        ">;",
        "Lcom/google/protobuf/MessageLiteOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private lock_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/backup/SettingProtos$LockSetting;",
            ">;"
        }
    .end annotation
.end field

.field private secure_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/backup/SettingProtos$SecureSetting;",
            ">;"
        }
    .end annotation
.end field

.field private system_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/backup/SettingProtos$SystemSetting;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$smcreate()Lcom/android/settings/backup/SettingProtos$Settings$Builder;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->create()Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    move-result-object v0

    return-object v0
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->system_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->secure_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->lock_:Ljava/util/List;

    invoke-direct {p0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method private static create()Lcom/android/settings/backup/SettingProtos$Settings$Builder;
    .locals 1

    new-instance v0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    invoke-direct {v0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;-><init>()V

    return-object v0
.end method

.method private ensureLockIsMutable()V
    .locals 3

    iget v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    const/4 v1, 0x4

    and-int/2addr v0, v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->lock_:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->lock_:Ljava/util/List;

    iget v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    or-int/2addr v0, v1

    iput v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private ensureSecureIsMutable()V
    .locals 3

    iget v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    const/4 v1, 0x2

    and-int/2addr v0, v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->secure_:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->secure_:Ljava/util/List;

    iget v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    or-int/2addr v0, v1

    iput v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private ensureSystemIsMutable()V
    .locals 3

    iget v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->system_:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->system_:Ljava/util/List;

    iget v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    or-int/2addr v0, v1

    iput v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    return-void
.end method


# virtual methods
.method public addLock(Lcom/android/settings/backup/SettingProtos$LockSetting;)Lcom/android/settings/backup/SettingProtos$Settings$Builder;
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {p0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->ensureLockIsMutable()V

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->lock_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addSecure(Lcom/android/settings/backup/SettingProtos$SecureSetting;)Lcom/android/settings/backup/SettingProtos$Settings$Builder;
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {p0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->ensureSecureIsMutable()V

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->secure_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addSystem(Lcom/android/settings/backup/SettingProtos$SystemSetting;)Lcom/android/settings/backup/SettingProtos$Settings$Builder;
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {p0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->ensureSystemIsMutable()V

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->system_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/android/settings/backup/SettingProtos$Settings;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->buildPartial()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object p0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$Settings;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    :cond_0
    invoke-static {p0}, Lcom/google/protobuf/AbstractMessageLite$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object p0

    throw p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->build()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object p0

    return-object p0
.end method

.method public buildPartial()Lcom/android/settings/backup/SettingProtos$Settings;
    .locals 3

    new-instance v0, Lcom/android/settings/backup/SettingProtos$Settings;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/settings/backup/SettingProtos$Settings;-><init>(Lcom/android/settings/backup/SettingProtos$Settings$Builder;Lcom/android/settings/backup/SettingProtos$Settings-IA;)V

    iget v1, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    const/4 v2, 0x1

    and-int/2addr v1, v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->system_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->system_:Ljava/util/List;

    iget v1, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    :cond_0
    iget-object v1, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->system_:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/android/settings/backup/SettingProtos$Settings;->-$$Nest$fputsystem_(Lcom/android/settings/backup/SettingProtos$Settings;Ljava/util/List;)V

    iget v1, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    const/4 v2, 0x2

    and-int/2addr v1, v2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->secure_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->secure_:Ljava/util/List;

    iget v1, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    :cond_1
    iget-object v1, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->secure_:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/android/settings/backup/SettingProtos$Settings;->-$$Nest$fputsecure_(Lcom/android/settings/backup/SettingProtos$Settings;Ljava/util/List;)V

    iget v1, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    const/4 v2, 0x4

    and-int/2addr v1, v2

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->lock_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->lock_:Ljava/util/List;

    iget v1, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    :cond_2
    iget-object p0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->lock_:Ljava/util/List;

    invoke-static {v0, p0}, Lcom/android/settings/backup/SettingProtos$Settings;->-$$Nest$fputlock_(Lcom/android/settings/backup/SettingProtos$Settings;Ljava/util/List;)V

    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->buildPartial()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object p0

    return-object p0
.end method

.method public clear()Lcom/android/settings/backup/SettingProtos$Settings$Builder;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->system_:Ljava/util/List;

    iget v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->secure_:Ljava/util/List;

    iget v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->lock_:Ljava/util/List;

    iget v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    return-object p0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->clear()Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->clear()Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    move-result-object p0

    return-object p0
.end method

.method public clone()Lcom/android/settings/backup/SettingProtos$Settings$Builder;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->create()Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->buildPartial()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->mergeFrom(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->clone()Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->clone()Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->clone()Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->clone()Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    move-result-object p0

    return-object p0
.end method

.method public getDefaultInstanceForType()Lcom/android/settings/backup/SettingProtos$Settings;
    .locals 0

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$Settings;->getDefaultInstance()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->getDefaultInstanceForType()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->getDefaultInstanceForType()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object p0

    return-object p0
.end method

.method public final isInitialized()Z
    .locals 0

    const/4 p0, 0x1

    return p0
.end method

.method public mergeFrom(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/SettingProtos$Settings$Builder;
    .locals 2

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$Settings;->getDefaultInstance()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v0

    if-ne p1, v0, :cond_0

    return-object p0

    :cond_0
    invoke-static {p1}, Lcom/android/settings/backup/SettingProtos$Settings;->-$$Nest$fgetsystem_(Lcom/android/settings/backup/SettingProtos$Settings;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->system_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/android/settings/backup/SettingProtos$Settings;->-$$Nest$fgetsystem_(Lcom/android/settings/backup/SettingProtos$Settings;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->system_:Ljava/util/List;

    iget v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->ensureSystemIsMutable()V

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->system_:Ljava/util/List;

    invoke-static {p1}, Lcom/android/settings/backup/SettingProtos$Settings;->-$$Nest$fgetsystem_(Lcom/android/settings/backup/SettingProtos$Settings;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_2
    :goto_0
    invoke-static {p1}, Lcom/android/settings/backup/SettingProtos$Settings;->-$$Nest$fgetsecure_(Lcom/android/settings/backup/SettingProtos$Settings;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->secure_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p1}, Lcom/android/settings/backup/SettingProtos$Settings;->-$$Nest$fgetsecure_(Lcom/android/settings/backup/SettingProtos$Settings;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->secure_:Ljava/util/List;

    iget v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->ensureSecureIsMutable()V

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->secure_:Ljava/util/List;

    invoke-static {p1}, Lcom/android/settings/backup/SettingProtos$Settings;->-$$Nest$fgetsecure_(Lcom/android/settings/backup/SettingProtos$Settings;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_4
    :goto_1
    invoke-static {p1}, Lcom/android/settings/backup/SettingProtos$Settings;->-$$Nest$fgetlock_(Lcom/android/settings/backup/SettingProtos$Settings;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->lock_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {p1}, Lcom/android/settings/backup/SettingProtos$Settings;->-$$Nest$fgetlock_(Lcom/android/settings/backup/SettingProtos$Settings;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->lock_:Ljava/util/List;

    iget p1, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    and-int/lit8 p1, p1, -0x5

    iput p1, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->bitField0_:I

    goto :goto_2

    :cond_5
    invoke-direct {p0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->ensureLockIsMutable()V

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->lock_:Ljava/util/List;

    invoke-static {p1}, Lcom/android/settings/backup/SettingProtos$Settings;->-$$Nest$fgetlock_(Lcom/android/settings/backup/SettingProtos$Settings;)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_6
    :goto_2
    return-object p0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/android/settings/backup/SettingProtos$Settings$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    if-eqz v0, :cond_4

    const/16 v1, 0xa

    if-eq v0, v1, :cond_3

    const/16 v1, 0x12

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1a

    if-eq v0, v1, :cond_1

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p0

    :cond_1
    invoke-static {}, Lcom/android/settings/backup/SettingProtos$LockSetting;->newBuilder()Lcom/android/settings/backup/SettingProtos$LockSetting$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/android/settings/backup/SettingProtos$LockSetting$Builder;->buildPartial()Lcom/android/settings/backup/SettingProtos$LockSetting;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->addLock(Lcom/android/settings/backup/SettingProtos$LockSetting;)Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->newBuilder()Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->buildPartial()Lcom/android/settings/backup/SettingProtos$SecureSetting;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->addSecure(Lcom/android/settings/backup/SettingProtos$SecureSetting;)Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/android/settings/backup/SettingProtos$SystemSetting;->newBuilder()Lcom/android/settings/backup/SettingProtos$SystemSetting$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/android/settings/backup/SettingProtos$SystemSetting$Builder;->buildPartial()Lcom/android/settings/backup/SettingProtos$SystemSetting;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->addSystem(Lcom/android/settings/backup/SettingProtos$SystemSetting;)Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    goto :goto_0

    :cond_4
    return-object p0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 0

    check-cast p1, Lcom/android/settings/backup/SettingProtos$Settings;

    invoke-virtual {p0, p1}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->mergeFrom(Lcom/android/settings/backup/SettingProtos$Settings;)Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/backup/SettingProtos$Settings$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/android/settings/backup/SettingProtos$Settings$Builder;

    move-result-object p0

    return-object p0
.end method
