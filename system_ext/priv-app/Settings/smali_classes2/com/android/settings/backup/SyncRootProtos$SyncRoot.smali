.class public final Lcom/android/settings/backup/SyncRootProtos$SyncRoot;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;
    }
.end annotation


# static fields
.field public static final SETTING_FIELD_NUMBER:I = 0x7

.field private static final defaultInstance:Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private setting_:Lcom/android/settings/backup/SettingProtos$Settings;


# direct methods
.method static bridge synthetic -$$Nest$fputbitField0_(Lcom/android/settings/backup/SyncRootProtos$SyncRoot;I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->bitField0_:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputsetting_(Lcom/android/settings/backup/SyncRootProtos$SyncRoot;Lcom/android/settings/backup/SettingProtos$Settings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;-><init>(Z)V

    sput-object v0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->defaultInstance:Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    invoke-direct {v0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    const/4 p1, -0x1

    iput-byte p1, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->memoizedIsInitialized:B

    iput p1, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;Lcom/android/settings/backup/SyncRootProtos$SyncRoot-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;-><init>(Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const/4 p1, -0x1

    iput-byte p1, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->memoizedIsInitialized:B

    iput p1, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->memoizedSerializedSize:I

    return-void
.end method

.method public static getDefaultInstance()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;
    .locals 1

    sget-object v0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->defaultInstance:Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$Settings;->getDefaultInstance()Lcom/android/settings/backup/SettingProtos$Settings;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    return-void
.end method

.method public static newBuilder()Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->-$$Nest$smcreate()Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/android/settings/backup/SyncRootProtos$SyncRoot;)Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->newBuilder()Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->mergeFrom(Lcom/android/settings/backup/SyncRootProtos$SyncRoot;)Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;

    move-result-object p0

    return-object p0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/android/settings/backup/SyncRootProtos$SyncRoot;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->newBuilder()Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/protobuf/AbstractMessageLite$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object p0

    check-cast p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;

    invoke-static {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;->-$$Nest$mbuildParsed(Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;)Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getDefaultInstanceForType()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;
    .locals 0

    sget-object p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->defaultInstance:Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    return-object p0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->getDefaultInstanceForType()Lcom/android/settings/backup/SyncRootProtos$SyncRoot;

    move-result-object p0

    return-object p0
.end method

.method public getSerializedSize()I
    .locals 3

    iget v0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->bitField0_:I

    const/4 v2, 0x1

    and-int/2addr v1, v2

    if-ne v1, v2, :cond_1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->memoizedSerializedSize:I

    return v0
.end method

.method public getSetting()Lcom/android/settings/backup/SettingProtos$Settings;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    return-object p0
.end method

.method public hasSetting()Z
    .locals 1

    iget p0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->bitField0_:I

    const/4 v0, 0x1

    and-int/2addr p0, v0

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isInitialized()Z
    .locals 3

    iget-byte v0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->memoizedIsInitialized:B

    const/4 v1, 0x1

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_1
    iput-byte v1, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->memoizedIsInitialized:B

    return v1
.end method

.method public newBuilderForType()Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;
    .locals 0

    invoke-static {}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->newBuilder()Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->newBuilderForType()Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;

    move-result-object p0

    return-object p0
.end method

.method public toBuilder()Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;
    .locals 0

    invoke-static {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->newBuilder(Lcom/android/settings/backup/SyncRootProtos$SyncRoot;)Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->toBuilder()Lcom/android/settings/backup/SyncRootProtos$SyncRoot$Builder;

    move-result-object p0

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->getSerializedSize()I

    iget v0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->bitField0_:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x7

    iget-object p0, p0, Lcom/android/settings/backup/SyncRootProtos$SyncRoot;->setting_:Lcom/android/settings/backup/SettingProtos$Settings;

    invoke-virtual {p1, v0, p0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    return-void
.end method
