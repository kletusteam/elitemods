.class public Lcom/android/settings/backup/Customization;
.super Ljava/lang/Object;


# static fields
.field public static final DATA_SYSTEM_PARTITION:[Lcom/android/settings/backup/SystemData;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/android/settings/backup/SystemData;

    new-instance v1, Lcom/android/settings/backup/SystemData;

    const-string v2, ""

    const-string v3, "gesture.key"

    const-string v4, "/data/system/gesture.key"

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/android/settings/backup/SystemData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/android/settings/backup/SystemData;

    const-string/jumbo v3, "password.key"

    const-string v4, "/data/system/password.key"

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/android/settings/backup/SystemData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    const/4 v6, 0x1

    aput-object v1, v0, v6

    new-instance v1, Lcom/android/settings/backup/SystemData;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/android/settings/backup/SystemData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    const/4 v3, 0x2

    aput-object v1, v0, v3

    new-instance v1, Lcom/android/settings/backup/SystemData;

    const-string/jumbo v3, "netpolicy.xml"

    const-string v4, "/data/system/netpolicy.xml"

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/android/settings/backup/SystemData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    const/4 v3, 0x3

    aput-object v1, v0, v3

    new-instance v1, Lcom/android/settings/backup/SystemData;

    const-string/jumbo v3, "netstats.bin"

    const-string v4, "/data/system/netstats.bin"

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/android/settings/backup/SystemData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    const/4 v3, 0x4

    aput-object v1, v0, v3

    new-instance v1, Lcom/android/settings/backup/SystemData;

    const-string/jumbo v3, "netstats_uid.bin"

    const-string v4, "/data/system/netstats_uid.bin"

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/android/settings/backup/SystemData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    const/4 v3, 0x5

    aput-object v1, v0, v3

    new-instance v1, Lcom/android/settings/backup/SystemData;

    const-string/jumbo v3, "netstats_xt.bin"

    const-string v4, "/data/system/netstats_xt.bin"

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/android/settings/backup/SystemData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    const/4 v3, 0x6

    aput-object v1, v0, v3

    new-instance v1, Lcom/android/settings/backup/SystemData;

    const-string v3, "access_control.key"

    const-string v4, "/data/system/access_control.key"

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/android/settings/backup/SystemData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/settings/backup/Customization;->DATA_SYSTEM_PARTITION:[Lcom/android/settings/backup/SystemData;

    return-void
.end method
