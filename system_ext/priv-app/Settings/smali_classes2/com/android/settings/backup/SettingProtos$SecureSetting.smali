.class public final Lcom/android/settings/backup/SettingProtos$SecureSetting;
.super Lcom/google/protobuf/GeneratedMessageLite;

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;
    }
.end annotation


# static fields
.field public static final GUID_FIELD_NUMBER:I = 0x1

.field public static final LUID_FIELD_NUMBER:I = 0x2

.field public static final NAME_FIELD_NUMBER:I = 0x3

.field public static final VALUE_FIELD_NUMBER:I = 0x4

.field private static final defaultInstance:Lcom/android/settings/backup/SettingProtos$SecureSetting;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private guid_:Ljava/lang/Object;

.field private luid_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/Object;

.field private value_:Ljava/lang/Object;


# direct methods
.method static bridge synthetic -$$Nest$fputbitField0_(Lcom/android/settings/backup/SettingProtos$SecureSetting;I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->bitField0_:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputguid_(Lcom/android/settings/backup/SettingProtos$SecureSetting;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->guid_:Ljava/lang/Object;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputluid_(Lcom/android/settings/backup/SettingProtos$SecureSetting;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->luid_:Ljava/lang/Object;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputname_(Lcom/android/settings/backup/SettingProtos$SecureSetting;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->name_:Ljava/lang/Object;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputvalue_(Lcom/android/settings/backup/SettingProtos$SecureSetting;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->value_:Ljava/lang/Object;

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/android/settings/backup/SettingProtos$SecureSetting;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/android/settings/backup/SettingProtos$SecureSetting;-><init>(Z)V

    sput-object v0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->defaultInstance:Lcom/android/settings/backup/SettingProtos$SecureSetting;

    invoke-direct {v0}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    const/4 p1, -0x1

    iput-byte p1, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->memoizedIsInitialized:B

    iput p1, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;Lcom/android/settings/backup/SettingProtos$SecureSetting-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/backup/SettingProtos$SecureSetting;-><init>(Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const/4 p1, -0x1

    iput-byte p1, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->memoizedIsInitialized:B

    iput p1, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->memoizedSerializedSize:I

    return-void
.end method

.method public static getDefaultInstance()Lcom/android/settings/backup/SettingProtos$SecureSetting;
    .locals 1

    sget-object v0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->defaultInstance:Lcom/android/settings/backup/SettingProtos$SecureSetting;

    return-object v0
.end method

.method private getGuidBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->guid_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->guid_:Ljava/lang/Object;

    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method private getLuidBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->luid_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->luid_:Ljava/lang/Object;

    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method private getNameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->name_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->name_:Ljava/lang/Object;

    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method private getValueBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->value_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->value_:Ljava/lang/Object;

    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    const-string v0, ""

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->guid_:Ljava/lang/Object;

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->luid_:Ljava/lang/Object;

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->name_:Ljava/lang/Object;

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->value_:Ljava/lang/Object;

    return-void
.end method

.method public static newBuilder()Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->-$$Nest$smcreate()Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/android/settings/backup/SettingProtos$SecureSetting;)Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->newBuilder()Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->mergeFrom(Lcom/android/settings/backup/SettingProtos$SecureSetting;)Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getDefaultInstanceForType()Lcom/android/settings/backup/SettingProtos$SecureSetting;
    .locals 0

    sget-object p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->defaultInstance:Lcom/android/settings/backup/SettingProtos$SecureSetting;

    return-object p0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->getDefaultInstanceForType()Lcom/android/settings/backup/SettingProtos$SecureSetting;

    move-result-object p0

    return-object p0
.end method

.method public getGuid()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->guid_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->guid_:Ljava/lang/Object;

    :cond_1
    return-object v1
.end method

.method public getLuid()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->luid_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->luid_:Ljava/lang/Object;

    :cond_1
    return-object v1
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->name_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->name_:Ljava/lang/Object;

    :cond_1
    return-object v1
.end method

.method public getSerializedSize()I
    .locals 4

    iget v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->bitField0_:I

    const/4 v2, 0x1

    and-int/2addr v1, v2

    if-ne v1, v2, :cond_1

    invoke-direct {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->getGuidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->bitField0_:I

    const/4 v2, 0x2

    and-int/2addr v1, v2

    if-ne v1, v2, :cond_2

    invoke-direct {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->getLuidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->bitField0_:I

    const/4 v2, 0x4

    and-int/2addr v1, v2

    if-ne v1, v2, :cond_3

    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->getNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->bitField0_:I

    const/16 v3, 0x8

    and-int/2addr v1, v3

    if-ne v1, v3, :cond_4

    invoke-direct {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->getValueBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->memoizedSerializedSize:I

    return v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->value_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->value_:Ljava/lang/Object;

    :cond_1
    return-object v1
.end method

.method public hasGuid()Z
    .locals 1

    iget p0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->bitField0_:I

    const/4 v0, 0x1

    and-int/2addr p0, v0

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasLuid()Z
    .locals 1

    iget p0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->bitField0_:I

    const/4 v0, 0x2

    and-int/2addr p0, v0

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public hasName()Z
    .locals 1

    iget p0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->bitField0_:I

    const/4 v0, 0x4

    and-int/2addr p0, v0

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public hasValue()Z
    .locals 1

    iget p0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->bitField0_:I

    const/16 v0, 0x8

    and-int/2addr p0, v0

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public final isInitialized()Z
    .locals 3

    iget-byte v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->memoizedIsInitialized:B

    const/4 v1, 0x1

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_1
    iput-byte v1, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->memoizedIsInitialized:B

    return v1
.end method

.method public newBuilderForType()Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;
    .locals 0

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->newBuilder()Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->newBuilderForType()Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    move-result-object p0

    return-object p0
.end method

.method public toBuilder()Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;
    .locals 0

    invoke-static {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->newBuilder(Lcom/android/settings/backup/SettingProtos$SecureSetting;)Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->toBuilder()Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    move-result-object p0

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->getSerializedSize()I

    iget v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->bitField0_:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->getGuidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_0
    iget v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->bitField0_:I

    const/4 v1, 0x2

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->getLuidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_1
    iget v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->bitField0_:I

    const/4 v1, 0x4

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_2

    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->getNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_2
    iget v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting;->bitField0_:I

    const/16 v2, 0x8

    and-int/2addr v0, v2

    if-ne v0, v2, :cond_3

    invoke-direct {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->getValueBytes()Lcom/google/protobuf/ByteString;

    move-result-object p0

    invoke-virtual {p1, v1, p0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_3
    return-void
.end method
