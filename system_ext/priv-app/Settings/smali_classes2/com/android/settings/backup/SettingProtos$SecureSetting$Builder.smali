.class public final Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/backup/SettingProtos$SecureSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder<",
        "Lcom/android/settings/backup/SettingProtos$SecureSetting;",
        "Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;",
        ">;",
        "Lcom/google/protobuf/MessageLiteOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private guid_:Ljava/lang/Object;

.field private luid_:Ljava/lang/Object;

.field private name_:Ljava/lang/Object;

.field private value_:Ljava/lang/Object;


# direct methods
.method static bridge synthetic -$$Nest$smcreate()Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->create()Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    move-result-object v0

    return-object v0
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->guid_:Ljava/lang/Object;

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->luid_:Ljava/lang/Object;

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->name_:Ljava/lang/Object;

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->value_:Ljava/lang/Object;

    invoke-direct {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method private static create()Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;
    .locals 1

    new-instance v0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    invoke-direct {v0}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    return-void
.end method


# virtual methods
.method public build()Lcom/android/settings/backup/SettingProtos$SecureSetting;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->buildPartial()Lcom/android/settings/backup/SettingProtos$SecureSetting;

    move-result-object p0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    :cond_0
    invoke-static {p0}, Lcom/google/protobuf/AbstractMessageLite$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object p0

    throw p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->build()Lcom/android/settings/backup/SettingProtos$SecureSetting;

    move-result-object p0

    return-object p0
.end method

.method public buildPartial()Lcom/android/settings/backup/SettingProtos$SecureSetting;
    .locals 5

    new-instance v0, Lcom/android/settings/backup/SettingProtos$SecureSetting;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/settings/backup/SettingProtos$SecureSetting;-><init>(Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;Lcom/android/settings/backup/SettingProtos$SecureSetting-IA;)V

    iget v1, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->bitField0_:I

    and-int/lit8 v2, v1, 0x1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    iget-object v2, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->guid_:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->-$$Nest$fputguid_(Lcom/android/settings/backup/SettingProtos$SecureSetting;Ljava/lang/Object;)V

    and-int/lit8 v2, v1, 0x2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_1

    or-int/lit8 v3, v3, 0x2

    :cond_1
    iget-object v2, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->luid_:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->-$$Nest$fputluid_(Lcom/android/settings/backup/SettingProtos$SecureSetting;Ljava/lang/Object;)V

    and-int/lit8 v2, v1, 0x4

    const/4 v4, 0x4

    if-ne v2, v4, :cond_2

    or-int/lit8 v3, v3, 0x4

    :cond_2
    iget-object v2, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->name_:Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->-$$Nest$fputname_(Lcom/android/settings/backup/SettingProtos$SecureSetting;Ljava/lang/Object;)V

    const/16 v2, 0x8

    and-int/2addr v1, v2

    if-ne v1, v2, :cond_3

    or-int/lit8 v3, v3, 0x8

    :cond_3
    iget-object p0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->value_:Ljava/lang/Object;

    invoke-static {v0, p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->-$$Nest$fputvalue_(Lcom/android/settings/backup/SettingProtos$SecureSetting;Ljava/lang/Object;)V

    invoke-static {v0, v3}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->-$$Nest$fputbitField0_(Lcom/android/settings/backup/SettingProtos$SecureSetting;I)V

    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->buildPartial()Lcom/android/settings/backup/SettingProtos$SecureSetting;

    move-result-object p0

    return-object p0
.end method

.method public clear()Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;
    .locals 2

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->guid_:Ljava/lang/Object;

    iget v1, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->bitField0_:I

    and-int/lit8 v1, v1, -0x2

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->luid_:Ljava/lang/Object;

    and-int/lit8 v1, v1, -0x3

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->name_:Ljava/lang/Object;

    and-int/lit8 v1, v1, -0x5

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->value_:Ljava/lang/Object;

    and-int/lit8 v0, v1, -0x9

    iput v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->bitField0_:I

    return-object p0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->clear()Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->clear()Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    move-result-object p0

    return-object p0
.end method

.method public clone()Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->create()Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->buildPartial()Lcom/android/settings/backup/SettingProtos$SecureSetting;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->mergeFrom(Lcom/android/settings/backup/SettingProtos$SecureSetting;)Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->clone()Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->clone()Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->clone()Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->clone()Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    move-result-object p0

    return-object p0
.end method

.method public getDefaultInstanceForType()Lcom/android/settings/backup/SettingProtos$SecureSetting;
    .locals 0

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->getDefaultInstance()Lcom/android/settings/backup/SettingProtos$SecureSetting;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->getDefaultInstanceForType()Lcom/android/settings/backup/SettingProtos$SecureSetting;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->getDefaultInstanceForType()Lcom/android/settings/backup/SettingProtos$SecureSetting;

    move-result-object p0

    return-object p0
.end method

.method public final isInitialized()Z
    .locals 0

    const/4 p0, 0x1

    return p0
.end method

.method public mergeFrom(Lcom/android/settings/backup/SettingProtos$SecureSetting;)Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;
    .locals 1

    invoke-static {}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->getDefaultInstance()Lcom/android/settings/backup/SettingProtos$SecureSetting;

    move-result-object v0

    if-ne p1, v0, :cond_0

    return-object p0

    :cond_0
    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->hasGuid()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->getGuid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->setGuid(Ljava/lang/String;)Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    :cond_1
    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->hasLuid()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->getLuid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->setLuid(Ljava/lang/String;)Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->hasName()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->setName(Ljava/lang/String;)Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/android/settings/backup/SettingProtos$SecureSetting;->getValue()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->setValue(Ljava/lang/String;)Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    :cond_4
    return-object p0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    if-eqz v0, :cond_5

    const/16 v1, 0xa

    if-eq v0, v1, :cond_4

    const/16 v1, 0x12

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x22

    if-eq v0, v1, :cond_1

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p0

    :cond_1
    iget v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->value_:Ljava/lang/Object;

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->name_:Ljava/lang/Object;

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->luid_:Ljava/lang/Object;

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->guid_:Ljava/lang/Object;

    goto :goto_0

    :cond_5
    return-object p0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 0

    check-cast p1, Lcom/android/settings/backup/SettingProtos$SecureSetting;

    invoke-virtual {p0, p1}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->mergeFrom(Lcom/android/settings/backup/SettingProtos$SecureSetting;)Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;

    move-result-object p0

    return-object p0
.end method

.method public setGuid(Ljava/lang/String;)Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->guid_:Ljava/lang/Object;

    return-object p0
.end method

.method public setLuid(Ljava/lang/String;)Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->luid_:Ljava/lang/Object;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->name_:Ljava/lang/Object;

    return-object p0
.end method

.method public setValue(Ljava/lang/String;)Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/android/settings/backup/SettingProtos$SecureSetting$Builder;->value_:Ljava/lang/Object;

    return-object p0
.end method
