.class Lcom/android/settings/SettingsFragment$8$3;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/SettingsFragment$8;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/settings/SettingsFragment$8;


# direct methods
.method constructor <init>(Lcom/android/settings/SettingsFragment$8;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/SettingsFragment$8$3;->this$1:Lcom/android/settings/SettingsFragment$8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x1

    const/4 p3, 0x5

    if-ne p2, p3, :cond_0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment$8$3;->this$1:Lcom/android/settings/SettingsFragment$8;

    iget-object p0, p0, Lcom/android/settings/SettingsFragment$8;->this$0:Lcom/android/settings/SettingsFragment;

    invoke-static {p0}, Lcom/android/settings/SettingsFragment;->-$$Nest$mhideSoftKeyboard(Lcom/android/settings/SettingsFragment;)V

    return p1

    :cond_0
    const/4 p3, 0x3

    if-ne p2, p3, :cond_1

    iget-object p2, p0, Lcom/android/settings/SettingsFragment$8$3;->this$1:Lcom/android/settings/SettingsFragment$8;

    iget-object p2, p2, Lcom/android/settings/SettingsFragment$8;->this$0:Lcom/android/settings/SettingsFragment;

    invoke-static {p2}, Lcom/android/settings/SettingsFragment;->-$$Nest$fgetmSearchHistoryText(Lcom/android/settings/SettingsFragment;)Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p3}, Lcom/android/settings/SettingsFragment;->-$$Nest$mprocessSearchHistory(Lcom/android/settings/SettingsFragment;Ljava/lang/String;)V

    iget-object p0, p0, Lcom/android/settings/SettingsFragment$8$3;->this$1:Lcom/android/settings/SettingsFragment$8;

    iget-object p0, p0, Lcom/android/settings/SettingsFragment$8;->this$0:Lcom/android/settings/SettingsFragment;

    invoke-static {p0}, Lcom/android/settings/SettingsFragment;->-$$Nest$mhideSoftKeyboard(Lcom/android/settings/SettingsFragment;)V

    return p1

    :cond_1
    const/4 p0, 0x0

    return p0
.end method
