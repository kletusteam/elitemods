.class public final Lcom/android/settings/accessibility/AccessibilityStatsLogUtils;
.super Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static convertToEntryPoint(I)I
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method static convertToItemKeyName(Ljava/lang/String;)I
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method static logAccessibilityServiceEnabled(Landroid/content/ComponentName;Z)V
    .locals 0

    return-void
.end method

.method static logDisableNonA11yCategoryService(Ljava/lang/String;J)V
    .locals 1

    sget v0, Lcom/android/internal/accessibility/util/AccessibilityStatsLogUtils;->ACCESSIBILITY_PRIVACY_WARNING_STATUS_SERVICE_DISABLED:I

    invoke-static {p0, v0, p1, p2}, Lcom/android/internal/accessibility/util/AccessibilityStatsLogUtils;->logNonA11yToolServiceWarningReported(Ljava/lang/String;IJ)V

    return-void
.end method
