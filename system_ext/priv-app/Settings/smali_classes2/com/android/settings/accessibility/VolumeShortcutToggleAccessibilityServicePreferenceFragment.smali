.class public Lcom/android/settings/accessibility/VolumeShortcutToggleAccessibilityServicePreferenceFragment;
.super Lcom/android/settings/accessibility/ToggleAccessibilityServicePreferenceFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/accessibility/ToggleAccessibilityServicePreferenceFragment;-><init>()V

    return-void
.end method

.method private setAllowedPreferredShortcutType(I)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/accessibility/ToggleFeaturePreferenceFragment;->mComponentName:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/android/settings/accessibility/PreferredShortcut;

    invoke-direct {v1, v0, p1}, Lcom/android/settings/accessibility/PreferredShortcut;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->getPrefContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0, v1}, Lcom/android/settings/accessibility/PreferredShortcuts;->saveUserShortcutType(Landroid/content/Context;Lcom/android/settings/accessibility/PreferredShortcut;)V

    return-void
.end method


# virtual methods
.method getUserShortcutTypes()I
    .locals 3

    goto/32 :goto_1

    nop

    :goto_0
    if-nez v1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_8

    nop

    :goto_1
    invoke-super {p0}, Lcom/android/settings/accessibility/ToggleAccessibilityServicePreferenceFragment;->getUserShortcutTypes()I

    move-result v0

    goto/32 :goto_14

    nop

    :goto_2
    return p0

    :goto_3
    invoke-virtual {p0}, Lcom/android/settings/accessibility/ToggleAccessibilityServicePreferenceFragment;->getAccessibilityServiceInfo()Landroid/accessibilityservice/AccessibilityServiceInfo;

    move-result-object p0

    goto/32 :goto_10

    nop

    :goto_4
    if-nez p0, :cond_1

    goto/32 :goto_12

    :cond_1
    goto/32 :goto_a

    nop

    :goto_5
    if-nez p0, :cond_2

    goto/32 :goto_7

    :cond_2
    goto/32 :goto_0

    nop

    :goto_6
    goto :goto_e

    :goto_7
    goto/32 :goto_d

    nop

    :goto_8
    or-int/lit8 p0, v0, 0x1

    goto/32 :goto_6

    nop

    :goto_9
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    goto/32 :goto_3

    nop

    :goto_a
    move p0, v2

    goto/32 :goto_11

    nop

    :goto_b
    const/4 p0, 0x0

    :goto_c
    goto/32 :goto_5

    nop

    :goto_d
    and-int/lit8 p0, v0, -0x2

    :goto_e
    goto/32 :goto_2

    nop

    :goto_f
    const-string v2, "checked"

    goto/32 :goto_9

    nop

    :goto_10
    iget p0, p0, Landroid/accessibilityservice/AccessibilityServiceInfo;->flags:I

    goto/32 :goto_13

    nop

    :goto_11
    goto :goto_c

    :goto_12
    goto/32 :goto_b

    nop

    :goto_13
    and-int/lit16 p0, p0, 0x100

    goto/32 :goto_15

    nop

    :goto_14
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    goto/32 :goto_f

    nop

    :goto_15
    const/4 v2, 0x1

    goto/32 :goto_4

    nop
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/android/settings/accessibility/ToggleAccessibilityServicePreferenceFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->getPrefContext()Landroid/content/Context;

    move-result-object p1

    sget p2, Lcom/android/settings/R$string;->accessibility_shortcut_edit_dialog_title_hardware:I

    invoke-virtual {p1, p2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    iget-object p2, p0, Lcom/android/settings/accessibility/ToggleFeaturePreferenceFragment;->mShortcutPreference:Lcom/android/settings/accessibility/ShortcutPreference;

    invoke-virtual {p2, p1}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/android/settings/accessibility/ToggleFeaturePreferenceFragment;->mShortcutPreference:Lcom/android/settings/accessibility/ShortcutPreference;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/android/settings/accessibility/ShortcutPreference;->setSettingsEditable(Z)V

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lcom/android/settings/accessibility/VolumeShortcutToggleAccessibilityServicePreferenceFragment;->setAllowedPreferredShortcutType(I)V

    return-void
.end method
