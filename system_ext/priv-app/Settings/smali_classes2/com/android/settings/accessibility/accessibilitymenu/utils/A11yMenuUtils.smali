.class public final Lcom/android/settings/accessibility/accessibilitymenu/utils/A11yMenuUtils;
.super Ljava/lang/Object;


# static fields
.field private static final GOOGLE_ASSISTANT_RESOURCE:[I

.field private static final SHORTCUT_RESOURCE:[[I

.field private static final VOICE_ASSIST_RESOURCE:[I

.field private static final XIAOAI_ASSISTANT_RESOURCE:[I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v0, 0x3

    new-array v1, v0, [I

    sget v2, Lcom/android/settings/R$drawable;->button_accessibility_menu_google_assistant_selector:I

    const/4 v3, 0x0

    aput v2, v1, v3

    sget v2, Lcom/android/settings/R$string;->google_assistant_label:I

    const/4 v4, 0x1

    aput v2, v1, v4

    const/4 v5, 0x2

    aput v2, v1, v5

    sput-object v1, Lcom/android/settings/accessibility/accessibilitymenu/utils/A11yMenuUtils;->GOOGLE_ASSISTANT_RESOURCE:[I

    new-array v2, v0, [I

    sget v6, Lcom/android/settings/R$drawable;->button_accessibility_menu_assistant_selector:I

    aput v6, v2, v3

    sget v6, Lcom/android/settings/R$string;->voice_assist:I

    aput v6, v2, v4

    aput v6, v2, v5

    sput-object v2, Lcom/android/settings/accessibility/accessibilitymenu/utils/A11yMenuUtils;->XIAOAI_ASSISTANT_RESOURCE:[I

    sget-boolean v6, Lmiui/os/Build;->IS_GLOBAL_BUILD:Z

    if-eqz v6, :cond_0

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    sput-object v1, Lcom/android/settings/accessibility/accessibilitymenu/utils/A11yMenuUtils;->VOICE_ASSIST_RESOURCE:[I

    const/16 v2, 0x9

    new-array v2, v2, [[I

    aput-object v1, v2, v3

    new-array v1, v0, [I

    sget v6, Lcom/android/settings/R$drawable;->button_accessibility_menu_settings_accessibility_selector:I

    aput v6, v1, v3

    sget v6, Lcom/android/settings/R$string;->a11y_settings_label:I

    aput v6, v1, v4

    aput v6, v1, v5

    aput-object v1, v2, v4

    new-array v1, v0, [I

    sget v6, Lcom/android/settings/R$drawable;->button_accessibility_menu_power_settings_selector:I

    aput v6, v1, v3

    sget v6, Lcom/android/settings/R$string;->power_label:I

    aput v6, v1, v4

    aput v6, v1, v5

    aput-object v1, v2, v5

    new-array v1, v0, [I

    sget v6, Lcom/android/settings/R$drawable;->button_accessibility_menu_quick_settings_selector:I

    aput v6, v1, v3

    sget v6, Lcom/android/settings/R$string;->quick_settings_label:I

    aput v6, v1, v4

    aput v6, v1, v5

    aput-object v1, v2, v0

    const/4 v1, 0x4

    new-array v6, v0, [I

    sget v7, Lcom/android/settings/R$drawable;->button_accessibility_menu_notifications_selector:I

    aput v7, v6, v3

    sget v7, Lcom/android/settings/R$string;->notifications_label:I

    aput v7, v6, v4

    aput v7, v6, v5

    aput-object v6, v2, v1

    const/4 v1, 0x5

    new-array v6, v0, [I

    sget v7, Lcom/android/settings/R$drawable;->button_accessibility_menu_screenshot_selector:I

    aput v7, v6, v3

    sget v7, Lcom/android/settings/R$string;->screenshot_label:I

    aput v7, v6, v4

    aput v7, v6, v5

    aput-object v6, v2, v1

    const/4 v1, 0x6

    new-array v6, v0, [I

    sget v7, Lcom/android/settings/R$drawable;->button_accessibility_menu_lock_settings_selector:I

    aput v7, v6, v3

    sget v7, Lcom/android/settings/R$string;->lockscreen_label:I

    aput v7, v6, v4

    aput v7, v6, v5

    aput-object v6, v2, v1

    const/4 v1, 0x7

    new-array v6, v0, [I

    sget v7, Lcom/android/settings/R$drawable;->button_accessibility_menu_view_carousel_selector:I

    aput v7, v6, v3

    sget v7, Lcom/android/settings/R$string;->recent_apps_label:I

    aput v7, v6, v4

    aput v7, v6, v5

    aput-object v6, v2, v1

    const/16 v1, 0x8

    new-array v0, v0, [I

    sget v6, Lcom/android/settings/R$drawable;->button_accessibility_menu_volume_settings_selector:I

    aput v6, v0, v3

    sget v3, Lcom/android/settings/R$string;->volume_label:I

    aput v3, v0, v4

    aput v3, v0, v5

    aput-object v0, v2, v1

    sput-object v2, Lcom/android/settings/accessibility/accessibilitymenu/utils/A11yMenuUtils;->SHORTCUT_RESOURCE:[[I

    return-void
.end method

.method public static setShortcutResByShortcutId(ILcom/android/settings/accessibility/accessibilitymenu/model/A11yMenuShortcut;)V
    .locals 1

    sget-object v0, Lcom/android/settings/accessibility/accessibilitymenu/utils/A11yMenuUtils;->SHORTCUT_RESOURCE:[[I

    aget-object p0, v0, p0

    const/4 v0, 0x0

    aget v0, p0, v0

    iput v0, p1, Lcom/android/settings/accessibility/accessibilitymenu/model/A11yMenuShortcut;->imageSrc:I

    const/4 v0, 0x1

    aget v0, p0, v0

    iput v0, p1, Lcom/android/settings/accessibility/accessibilitymenu/model/A11yMenuShortcut;->imgContentDescription:I

    const/4 v0, 0x2

    aget p0, p0, v0

    iput p0, p1, Lcom/android/settings/accessibility/accessibilitymenu/model/A11yMenuShortcut;->labelText:I

    return-void
.end method
