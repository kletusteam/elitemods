.class public Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;
.super Lcom/android/settings/SubSettings;


# static fields
.field private static final a11ySettingsClass:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class<",
            "+",
            "Landroidx/fragment/app/Fragment;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mActionBar:Lmiuix/appcompat/app/ActionBar;

.field private mBackView:Landroid/widget/ImageView;

.field private mCurrentPosition:I

.field private mTitles:[Ljava/lang/String;


# direct methods
.method static bridge synthetic -$$Nest$fputmCurrentPosition(Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;->mCurrentPosition:I

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Lcom/android/settings/accessibility/GeneralAccessibilitySettings;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcom/android/settings/accessibility/VisualAccessibilitySettings;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Lcom/android/settings/accessibility/HearingAccessibilitySettings;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Lcom/android/settings/accessibility/PhysicalAccessibilitySettings;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;->a11ySettingsClass:[Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/SubSettings;-><init>()V

    return-void
.end method

.method private initActionBar()V
    .locals 9

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;->mActionBar:Lmiuix/appcompat/app/ActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lmiuix/appcompat/app/ActionBar;->setFragmentViewPagerMode(Landroidx/fragment/app/FragmentActivity;Z)V

    invoke-direct {p0}, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;->initActionBarBackView()V

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    sget v2, Lcom/android/settings/R$string;->accessibility_settings_tabs_general:I

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    sget v2, Lcom/android/settings/R$string;->accessibility_settings_tabs_visual:I

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v0, v3

    sget v2, Lcom/android/settings/R$string;->accessibility_settings_tabs_hearing:I

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v0, v3

    sget v2, Lcom/android/settings/R$string;->accessibility_settings_tabs_physical:I

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v0, v3

    iput-object v0, p0, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;->mTitles:[Ljava/lang/String;

    :goto_0
    sget-object v0, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;->a11ySettingsClass:[Ljava/lang/Class;

    array-length v2, v0

    if-ge v1, v2, :cond_0

    iget-object v3, p0, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;->mActionBar:Lmiuix/appcompat/app/ActionBar;

    iget-object v2, p0, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;->mTitles:[Ljava/lang/String;

    aget-object v4, v2, v1

    invoke-virtual {v3}, Landroidx/appcompat/app/ActionBar;->newTab()Landroidx/appcompat/app/ActionBar$Tab;

    move-result-object v2

    iget-object v5, p0, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;->mTitles:[Ljava/lang/String;

    aget-object v5, v5, v1

    invoke-virtual {v2, v5}, Landroidx/appcompat/app/ActionBar$Tab;->setText(Ljava/lang/CharSequence;)Landroidx/appcompat/app/ActionBar$Tab;

    move-result-object v5

    aget-object v6, v0, v1

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Lmiuix/appcompat/app/ActionBar;->addFragmentTab(Ljava/lang/String;Landroidx/appcompat/app/ActionBar$Tab;Ljava/lang/Class;Landroid/os/Bundle;Z)I

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;->mActionBar:Lmiuix/appcompat/app/ActionBar;

    new-instance v1, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity$1;

    invoke-direct {v1, p0}, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity$1;-><init>(Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;)V

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/ActionBar;->addOnFragmentViewPagerChangeListener(Lmiuix/appcompat/app/ActionBar$FragmentViewPagerChangeListener;)V

    return-void
.end method

.method private initActionBarBackView()V
    .locals 2

    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;->mBackView:Landroid/widget/ImageView;

    sget v1, Lcom/android/settings/R$string;->back_button:I

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;->mBackView:Landroid/widget/ImageView;

    new-instance v1, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity$2;

    invoke-direct {v1, p0}, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity$2;-><init>(Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {p0}, Lcom/android/settings/display/DarkModeTimeModeUtil;->isDarkModeEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;->mBackView:Landroid/widget/ImageView;

    sget v1, Lcom/android/settings/R$drawable;->miuix_appcompat_action_bar_back_dark:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;->mBackView:Landroid/widget/ImageView;

    sget v1, Lcom/android/settings/R$drawable;->miuix_appcompat_action_bar_back_light:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;->mActionBar:Lmiuix/appcompat/app/ActionBar;

    iget-object p0, p0, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;->mBackView:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Lmiuix/appcompat/app/ActionBar;->setStartView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method protected needToLaunchSettingsFragment()Z
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/SettingsActivity;->onCreate(Landroid/os/Bundle;)V

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->isFoldDevice()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;->initActionBar()V

    if-eqz p1, :cond_1

    const-string v0, "current_position"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;->mCurrentPosition:I

    iget-object p0, p0, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;->mActionBar:Lmiuix/appcompat/app/ActionBar;

    invoke-virtual {p0, p1}, Landroidx/appcompat/app/ActionBar;->setSelectedNavigationItem(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/SettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;->mActionBar:Lmiuix/appcompat/app/ActionBar;

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/SettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const/4 v0, 0x0

    const-string v1, "extra_tab_position"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    iget-object p0, p0, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;->mActionBar:Lmiuix/appcompat/app/ActionBar;

    invoke-virtual {p0, p1}, Landroidx/appcompat/app/ActionBar;->setSelectedNavigationItem(I)V

    :cond_2
    :goto_0
    const-string/jumbo p0, "setting_Additional_settings_talkback"

    invoke-static {p0}, Lcom/android/settings/report/InternationalCompat;->trackReportEvent(Ljava/lang/String;)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/SettingsActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget v0, p0, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;->mCurrentPosition:I

    const-string v1, "current_position"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "mCurrentPosition:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p0, p0, Lcom/android/settings/accessibility/MiuiAccessibilitySettingsActivity;->mCurrentPosition:I

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "MiuiA11ySettingsActivity"

    invoke-static {p1, p0}, Landroid/view/textclassifier/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setTheme(I)V
    .locals 0

    sget p1, Lcom/android/settings/R$style;->MiuiAccessibility:I

    invoke-super {p0, p1}, Landroid/app/Activity;->setTheme(I)V

    return-void
.end method
