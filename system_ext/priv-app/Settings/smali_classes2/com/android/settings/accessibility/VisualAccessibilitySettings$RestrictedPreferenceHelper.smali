.class Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/accessibility/VisualAccessibilitySettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RestrictedPreferenceHelper"
.end annotation


# instance fields
.field private final mConfigedServiceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mDpm:Landroid/app/admin/DevicePolicyManager;

.field private final mPm:Landroid/content/pm/PackageManager;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mContext:Landroid/content/Context;

    const-class v0, Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    iput-object v0, p0, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mDpm:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mPm:Landroid/content/pm/PackageManager;

    invoke-static {p1}, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->getConfigedServices(Landroid/content/Context;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mConfigedServiceList:Ljava/util/List;

    return-void
.end method

.method private createCustomRestrictedPreference(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/android/settingslib/RestrictedPreference;
    .locals 2

    new-instance v0, Lcom/android/settings/accessibility/CustomRestrictedPreference;

    iget-object v1, p0, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/settings/accessibility/CustomRestrictedPreference;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroidx/preference/Preference;->setKey(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    iget-object p0, p0, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->accessibility_summary_source:I

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p3, p2, v1

    invoke-virtual {p0, p1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, p5}, Landroidx/preference/Preference;->setFragment(Ljava/lang/String;)V

    invoke-virtual {v0, p4}, Lcom/android/settingslib/RestrictedPreference;->setValue(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setPersistent(Z)V

    return-object v0
.end method

.method private createRestrictedPreference(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/android/settingslib/RestrictedPreference;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "createRestrictedPreference: title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ",fragment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "VisualAccessibilitySettings"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/android/settingslib/RestrictedPreference;

    iget-object p0, p0, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0}, Lcom/android/settingslib/RestrictedPreference;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroidx/preference/Preference;->setKey(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, p4}, Landroidx/preference/Preference;->setFragment(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    const/4 p0, 0x0

    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setPersistent(Z)V

    return-object v0
.end method

.method private getAccessibilityServiceFragmentTypeName(Landroid/accessibilityservice/AccessibilityServiceInfo;)Ljava/lang/String;
    .locals 0

    invoke-static {p1}, Lcom/android/settings/accessibility/AccessibilityServiceUtils;->getAccessibilityServiceFragmentTypeName(Landroid/accessibilityservice/AccessibilityServiceInfo;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getConfigedServices(Landroid/content/Context;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lcom/android/settings/accessibility/AccessibilitySettings;->PRE_CONFIGED_SERVICES_LIST:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    :goto_0
    array-length v4, v2

    if-ge v3, v4, :cond_0

    aget-object v4, v2, v3

    invoke-static {v4}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private putBasicExtras(Lcom/android/settingslib/RestrictedPreference;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/String;Landroid/content/ComponentName;)V
    .locals 0

    invoke-virtual {p1}, Landroidx/preference/Preference;->getExtras()Landroid/os/Bundle;

    move-result-object p0

    const-string/jumbo p1, "preference_key"

    invoke-virtual {p0, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo p1, "title"

    invoke-virtual {p0, p1, p3}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    const-string/jumbo p1, "summary"

    invoke-virtual {p0, p1, p4}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    const-string p1, "component_name"

    invoke-virtual {p0, p1, p7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string p1, "animated_image_res"

    invoke-virtual {p0, p1, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string p1, "html_description"

    invoke-virtual {p0, p1, p6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private putServiceExtras(Lcom/android/settingslib/RestrictedPreference;Landroid/content/pm/ResolveInfo;Ljava/lang/Boolean;)V
    .locals 0

    invoke-virtual {p1}, Landroidx/preference/Preference;->getExtras()Landroid/os/Bundle;

    move-result-object p0

    const-string/jumbo p1, "resolve_info"

    invoke-virtual {p0, p1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const-string p2, "checked"

    invoke-virtual {p0, p2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method private putSettingsExtras(Lcom/android/settingslib/RestrictedPreference;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p1}, Landroidx/preference/Preference;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mContext:Landroid/content/Context;

    sget v0, Lcom/android/settings/R$string;->accessibility_menu_item_settings:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    const-string/jumbo v0, "settings_title"

    invoke-virtual {p1, v0, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance p0, Landroid/content/ComponentName;

    invoke-direct {p0, p2, p3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object p0

    const-string/jumbo p2, "settings_component_name"

    invoke-virtual {p1, p2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private setRestrictedPreferenceEnabled(Lcom/android/settingslib/RestrictedPreference;Ljava/lang/String;ZZ)V
    .locals 0

    if-nez p3, :cond_2

    if-eqz p4, :cond_0

    goto :goto_0

    :cond_0
    iget-object p0, p0, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mContext:Landroid/content/Context;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result p3

    invoke-static {p0, p2, p3}, Lcom/android/settingslib/RestrictedLockUtilsInternal;->checkIfAccessibilityServiceDisallowed(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    move-result-object p0

    if-eqz p0, :cond_1

    invoke-virtual {p1, p0}, Lcom/android/settingslib/RestrictedPreference;->setDisabledByAdmin(Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)V

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    invoke-virtual {p1, p0}, Lcom/android/settingslib/RestrictedPreference;->setEnabled(Z)V

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p0, 0x1

    invoke-virtual {p1, p0}, Lcom/android/settingslib/RestrictedPreference;->setEnabled(Z)V

    :goto_1
    return-void
.end method


# virtual methods
.method createAccessibilityActivityPreferenceList(Ljava/util/List;Z)Ljava/util/List;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/accessibilityservice/AccessibilityShortcutInfo;",
            ">;Z)",
            "Ljava/util/List<",
            "Lcom/android/settingslib/RestrictedPreference;",
            ">;"
        }
    .end annotation

    goto/32 :goto_3f

    nop

    :goto_0
    if-nez v10, :cond_0

    goto/32 :goto_49

    :cond_0
    goto/32 :goto_7

    nop

    :goto_1
    if-eqz v2, :cond_1

    goto/32 :goto_21

    :cond_1
    goto/32 :goto_35

    nop

    :goto_2
    iget-object v6, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    goto/32 :goto_39

    nop

    :goto_3
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    goto/32 :goto_2f

    nop

    :goto_4
    iget-object v1, v8, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mPm:Landroid/content/pm/PackageManager;

    goto/32 :goto_52

    nop

    :goto_5
    goto/16 :goto_21

    :goto_6
    goto/32 :goto_b

    nop

    :goto_7
    invoke-interface {v10, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    goto/32 :goto_4e

    nop

    :goto_8
    const/4 v1, 0x0

    goto/32 :goto_48

    nop

    :goto_9
    goto/16 :goto_49

    :goto_a
    goto/32 :goto_8

    nop

    :goto_b
    invoke-virtual {v7}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_1d

    nop

    :goto_c
    move-object/from16 v10, v16

    goto/32 :goto_1f

    nop

    :goto_d
    move-object/from16 v15, p1

    goto/32 :goto_45

    nop

    :goto_e
    invoke-direct {v12, v11}, Ljava/util/ArrayList;-><init>(I)V

    goto/32 :goto_1a

    nop

    :goto_f
    move-object/from16 v6, v17

    goto/32 :goto_2a

    nop

    :goto_10
    invoke-interface {v9, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    goto/32 :goto_4b

    nop

    :goto_11
    invoke-static {v2, v6, v3}, Lcom/android/settings/accessibility/VisualAccessibilitySettings;->isHideServices(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    goto/32 :goto_20

    nop

    :goto_12
    invoke-interface {v2, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    goto/32 :goto_4d

    nop

    :goto_13
    new-instance v12, Ljava/util/ArrayList;

    goto/32 :goto_e

    nop

    :goto_14
    invoke-virtual {v0, v1}, Landroid/accessibilityservice/AccessibilityShortcutInfo;->loadHtmlDescription(Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v17

    goto/32 :goto_3c

    nop

    :goto_15
    iget-object v1, v8, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mPm:Landroid/content/pm/PackageManager;

    goto/32 :goto_14

    nop

    :goto_16
    check-cast v0, Landroid/accessibilityservice/AccessibilityShortcutInfo;

    goto/32 :goto_25

    nop

    :goto_17
    move-object v13, v1

    goto/32 :goto_24

    nop

    :goto_18
    invoke-interface {v2, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    goto/32 :goto_44

    nop

    :goto_19
    move-object v9, v5

    goto/32 :goto_47

    nop

    :goto_1a
    const/4 v14, 0x0

    :goto_1b
    goto/32 :goto_54

    nop

    :goto_1c
    invoke-virtual {v0}, Landroid/accessibilityservice/AccessibilityShortcutInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v7

    goto/32 :goto_2

    nop

    :goto_1d
    iget-object v3, v8, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mPm:Landroid/content/pm/PackageManager;

    goto/32 :goto_1e

    nop

    :goto_1e
    invoke-virtual {v1, v3}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    goto/32 :goto_23

    nop

    :goto_1f
    move-object/from16 v9, v18

    goto/32 :goto_36

    nop

    :goto_20
    if-nez v2, :cond_2

    goto/32 :goto_2d

    :cond_2
    :goto_21
    goto/32 :goto_51

    nop

    :goto_22
    iget-object v0, v8, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mContext:Landroid/content/Context;

    goto/32 :goto_56

    nop

    :goto_23
    iget-object v1, v8, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mPm:Landroid/content/pm/PackageManager;

    goto/32 :goto_3e

    nop

    :goto_24
    move-object v1, v5

    goto/32 :goto_27

    nop

    :goto_25
    invoke-virtual {v0}, Landroid/accessibilityservice/AccessibilityShortcutInfo;->getActivityInfo()Landroid/content/pm/ActivityInfo;

    move-result-object v1

    goto/32 :goto_1c

    nop

    :goto_26
    iget-object v0, v8, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mDpm:Landroid/app/admin/DevicePolicyManager;

    goto/32 :goto_3

    nop

    :goto_27
    move-object/from16 v18, v9

    goto/32 :goto_19

    nop

    :goto_28
    invoke-interface {v12, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_29
    goto/32 :goto_32

    nop

    :goto_2a
    invoke-direct/range {v0 .. v7}, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->putBasicExtras(Lcom/android/settingslib/RestrictedPreference;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/String;Landroid/content/ComponentName;)V

    goto/32 :goto_41

    nop

    :goto_2b
    const-class v4, Lcom/android/settings/accessibility/LaunchAccessibilityActivityPreferenceFragment;

    goto/32 :goto_2e

    nop

    :goto_2c
    goto/16 :goto_29

    :goto_2d
    goto/32 :goto_33

    nop

    :goto_2e
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_42

    nop

    :goto_2f
    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getPermittedAccessibilityServices(I)Ljava/util/List;

    move-result-object v10

    goto/32 :goto_3d

    nop

    :goto_30
    const/4 v1, 0x1

    :goto_31
    goto/32 :goto_10

    nop

    :goto_32
    add-int/lit8 v14, v14, 0x1

    goto/32 :goto_c

    nop

    :goto_33
    if-eqz p2, :cond_3

    goto/32 :goto_50

    :cond_3
    goto/32 :goto_43

    nop

    :goto_34
    move-object/from16 v0, p0

    goto/32 :goto_17

    nop

    :goto_35
    iget-object v2, v8, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mConfigedServiceList:Ljava/util/List;

    goto/32 :goto_18

    nop

    :goto_36
    goto/16 :goto_1b

    :goto_37
    goto/32 :goto_55

    nop

    :goto_38
    move-object v10, v6

    goto/32 :goto_f

    nop

    :goto_39
    iget-object v2, v8, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mContext:Landroid/content/Context;

    goto/32 :goto_4a

    nop

    :goto_3a
    invoke-interface {v2, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    goto/32 :goto_1

    nop

    :goto_3b
    invoke-virtual {v5}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_4

    nop

    :goto_3c
    invoke-virtual {v0}, Landroid/accessibilityservice/AccessibilityShortcutInfo;->getSettingsActivityName()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_34

    nop

    :goto_3d
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v11

    goto/32 :goto_13

    nop

    :goto_3e
    invoke-virtual {v0, v1}, Landroid/accessibilityservice/AccessibilityShortcutInfo;->loadSummary(Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_2b

    nop

    :goto_3f
    move-object/from16 v8, p0

    goto/32 :goto_22

    nop

    :goto_40
    move-object/from16 v16, v10

    goto/32 :goto_2c

    nop

    :goto_41
    invoke-direct {v8, v9, v10, v13}, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->putSettingsExtras(Lcom/android/settingslib/RestrictedPreference;Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_28

    nop

    :goto_42
    invoke-direct {v8, v2, v3, v1, v4}, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->createRestrictedPreference(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/android/settingslib/RestrictedPreference;

    move-result-object v5

    goto/32 :goto_0

    nop

    :goto_43
    sget-object v2, Lcom/android/settings/accessibility/VisualAccessibilitySettings;->COMMON_SERVICES_LIST:Ljava/util/List;

    goto/32 :goto_12

    nop

    :goto_44
    if-nez v2, :cond_4

    goto/32 :goto_6

    :cond_4
    goto/32 :goto_5

    nop

    :goto_45
    invoke-interface {v15, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_16

    nop

    :goto_46
    sget-object v2, Lcom/android/settings/accessibility/VisualAccessibilitySettings;->COMMON_SERVICES_LIST:Ljava/util/List;

    goto/32 :goto_3a

    nop

    :goto_47
    move/from16 v5, v16

    goto/32 :goto_4c

    nop

    :goto_48
    goto/16 :goto_31

    :goto_49
    goto/32 :goto_30

    nop

    :goto_4a
    iget-object v3, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    goto/32 :goto_11

    nop

    :goto_4b
    invoke-direct {v8, v5, v6, v1, v2}, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->setRestrictedPreferenceEnabled(Lcom/android/settingslib/RestrictedPreference;Ljava/lang/String;ZZ)V

    goto/32 :goto_3b

    nop

    :goto_4c
    move-object/from16 v16, v10

    goto/32 :goto_38

    nop

    :goto_4d
    if-eqz v2, :cond_5

    goto/32 :goto_6

    :cond_5
    goto/32 :goto_4f

    nop

    :goto_4e
    if-nez v1, :cond_6

    goto/32 :goto_a

    :cond_6
    goto/32 :goto_9

    nop

    :goto_4f
    goto/16 :goto_21

    :goto_50
    goto/32 :goto_46

    nop

    :goto_51
    move-object/from16 v18, v9

    goto/32 :goto_40

    nop

    :goto_52
    invoke-virtual {v0, v1}, Landroid/accessibilityservice/AccessibilityShortcutInfo;->loadDescription(Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_53

    nop

    :goto_53
    invoke-virtual {v0}, Landroid/accessibilityservice/AccessibilityShortcutInfo;->getAnimatedImageRes()I

    move-result v16

    goto/32 :goto_15

    nop

    :goto_54
    if-lt v14, v11, :cond_7

    goto/32 :goto_37

    :cond_7
    goto/32 :goto_d

    nop

    :goto_55
    return-object v12

    :goto_56
    invoke-static {v0}, Lcom/android/settingslib/accessibility/AccessibilityUtils;->getEnabledServicesFromSettings(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v9

    goto/32 :goto_26

    nop
.end method

.method createAccessibilityServicePreferenceList(Ljava/util/List;Z)Ljava/util/List;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/accessibilityservice/AccessibilityServiceInfo;",
            ">;Z)",
            "Ljava/util/List<",
            "Lcom/android/settingslib/RestrictedPreference;",
            ">;"
        }
    .end annotation

    goto/32 :goto_3e

    nop

    :goto_0
    sget-object v0, Lcom/android/settings/accessibility/VisualAccessibilitySettings;->COMMON_SERVICES_LIST:Ljava/util/List;

    goto/32 :goto_24

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_41

    :cond_0
    goto/32 :goto_40

    nop

    :goto_2
    move/from16 v18, v11

    goto/32 :goto_37

    nop

    :goto_3
    invoke-static {v1, v5, v0}, Lcom/android/settings/accessibility/VisualAccessibilitySettings;->isHideServices(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto/32 :goto_26

    nop

    :goto_4
    move-object v15, v7

    goto/32 :goto_c

    nop

    :goto_5
    move-object/from16 v4, v18

    goto/32 :goto_5e

    nop

    :goto_6
    move-object/from16 v15, p1

    goto/32 :goto_28

    nop

    :goto_7
    invoke-interface {v9, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    goto/32 :goto_33

    nop

    :goto_8
    iget-object v0, v8, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mConfigedServiceList:Ljava/util/List;

    goto/32 :goto_11

    nop

    :goto_9
    invoke-interface {v12, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_a
    goto/32 :goto_23

    nop

    :goto_b
    move-object v2, v3

    goto/32 :goto_77

    nop

    :goto_c
    move-object/from16 v7, v17

    goto/32 :goto_81

    nop

    :goto_d
    invoke-direct {v8, v6}, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->getAccessibilityServiceFragmentTypeName(Landroid/accessibilityservice/AccessibilityServiceInfo;)Ljava/lang/String;

    move-result-object v13

    goto/32 :goto_54

    nop

    :goto_e
    iget-object v0, v8, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mContext:Landroid/content/Context;

    goto/32 :goto_3f

    nop

    :goto_f
    iget-object v5, v0, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    goto/32 :goto_62

    nop

    :goto_10
    move-object v6, v0

    goto/32 :goto_68

    nop

    :goto_11
    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_1

    nop

    :goto_12
    if-nez v0, :cond_1

    goto/32 :goto_57

    :cond_1
    goto/32 :goto_56

    nop

    :goto_13
    invoke-virtual {v7, v0}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    goto/32 :goto_7

    nop

    :goto_14
    invoke-static {v0, v6, v9}, Lcom/android/settings/accessibility/VisualAccessibilitySettings;->getServiceDescription(Landroid/content/Context;Landroid/accessibilityservice/AccessibilityServiceInfo;Z)Ljava/lang/CharSequence;

    move-result-object v4

    goto/32 :goto_36

    nop

    :goto_15
    invoke-virtual {v6}, Landroid/accessibilityservice/AccessibilityServiceInfo;->getSettingsActivityName()Ljava/lang/String;

    move-result-object v6

    goto/32 :goto_25

    nop

    :goto_16
    invoke-static {v0, v6, v2}, Lcom/android/settings/accessibility/VisualAccessibilitySettings;->getServiceSummary(Landroid/content/Context;Landroid/accessibilityservice/AccessibilityServiceInfo;Z)Ljava/lang/CharSequence;

    move-result-object v0

    goto/32 :goto_d

    nop

    :goto_17
    move-object/from16 v16, v10

    goto/32 :goto_2

    nop

    :goto_18
    iget-object v4, v4, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    goto/32 :goto_1e

    nop

    :goto_19
    iget-object v0, v8, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mPm:Landroid/content/pm/PackageManager;

    goto/32 :goto_13

    nop

    :goto_1a
    move-object/from16 v9, v19

    goto/32 :goto_4b

    nop

    :goto_1b
    move/from16 v9, v16

    goto/32 :goto_b

    nop

    :goto_1c
    move-object/from16 v16, v10

    goto/32 :goto_46

    nop

    :goto_1d
    iget-object v1, v7, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    goto/32 :goto_2d

    nop

    :goto_1e
    invoke-virtual {v2, v4}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v2

    goto/32 :goto_5c

    nop

    :goto_1f
    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_21

    nop

    :goto_20
    move-object/from16 v19, v9

    goto/32 :goto_17

    nop

    :goto_21
    if-eqz v0, :cond_2

    goto/32 :goto_41

    :cond_2
    :goto_22
    goto/32 :goto_70

    nop

    :goto_23
    add-int/lit8 v14, v14, 0x1

    goto/32 :goto_47

    nop

    :goto_24
    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_61

    nop

    :goto_25
    move-object/from16 v0, p0

    goto/32 :goto_5b

    nop

    :goto_26
    if-nez v0, :cond_3

    goto/32 :goto_38

    :cond_3
    :goto_27
    goto/32 :goto_20

    nop

    :goto_28
    invoke-interface {v15, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_10

    nop

    :goto_29
    invoke-virtual {v4}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_19

    nop

    :goto_2a
    iget-object v4, v7, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    goto/32 :goto_18

    nop

    :goto_2b
    move/from16 v9, v16

    goto/32 :goto_30

    nop

    :goto_2c
    iget-object v0, v8, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mContext:Landroid/content/Context;

    goto/32 :goto_14

    nop

    :goto_2d
    iget-object v1, v1, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    goto/32 :goto_63

    nop

    :goto_2e
    move-object v13, v0

    :goto_2f
    goto/32 :goto_58

    nop

    :goto_30
    move-object/from16 v16, v3

    goto/32 :goto_35

    nop

    :goto_31
    invoke-virtual {v6, v0}, Landroid/accessibilityservice/AccessibilityServiceInfo;->loadHtmlDescription(Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v20

    goto/32 :goto_15

    nop

    :goto_32
    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    goto/32 :goto_3

    nop

    :goto_33
    iget-object v0, v8, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mContext:Landroid/content/Context;

    goto/32 :goto_16

    nop

    :goto_34
    move-object v11, v5

    goto/32 :goto_5a

    nop

    :goto_35
    move-object v11, v5

    goto/32 :goto_48

    nop

    :goto_36
    iget-object v0, v8, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mPm:Landroid/content/pm/PackageManager;

    goto/32 :goto_31

    nop

    :goto_37
    goto/16 :goto_a

    :goto_38
    goto/32 :goto_76

    nop

    :goto_39
    move-object/from16 v17, v4

    goto/32 :goto_2a

    nop

    :goto_3a
    move/from16 v18, v11

    goto/32 :goto_2b

    nop

    :goto_3b
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/32 :goto_5d

    nop

    :goto_3c
    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getPermittedAccessibilityServices(I)Ljava/util/List;

    move-result-object v10

    goto/32 :goto_53

    nop

    :goto_3d
    move-object/from16 v6, v20

    goto/32 :goto_4

    nop

    :goto_3e
    move-object/from16 v8, p0

    goto/32 :goto_4f

    nop

    :goto_3f
    invoke-static {v0}, Lcom/android/settings/utils/SettingsFeatures;->isSupportAccessibilityHaptic(Landroid/content/Context;)Z

    move-result v0

    goto/32 :goto_60

    nop

    :goto_40
    goto/16 :goto_27

    :goto_41
    goto/32 :goto_29

    nop

    :goto_42
    if-eqz p2, :cond_4

    goto/32 :goto_49

    :cond_4
    goto/32 :goto_44

    nop

    :goto_43
    invoke-interface {v10, v11}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_12

    nop

    :goto_44
    invoke-direct {v8, v1, v3, v0, v13}, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->createRestrictedPreference(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/android/settingslib/RestrictedPreference;

    move-result-object v0

    goto/32 :goto_66

    nop

    :goto_45
    invoke-virtual {v13}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_78

    nop

    :goto_46
    move-object v10, v6

    goto/32 :goto_3d

    nop

    :goto_47
    move-object/from16 v10, v16

    goto/32 :goto_80

    nop

    :goto_48
    goto/16 :goto_2f

    :goto_49
    goto/32 :goto_59

    nop

    :goto_4a
    return-object v12

    :goto_4b
    goto/16 :goto_74

    :goto_4c
    goto/32 :goto_4a

    nop

    :goto_4d
    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_7e

    nop

    :goto_4e
    invoke-virtual {v6}, Landroid/accessibilityservice/AccessibilityServiceInfo;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v7

    goto/32 :goto_67

    nop

    :goto_4f
    iget-object v0, v8, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mContext:Landroid/content/Context;

    goto/32 :goto_55

    nop

    :goto_50
    invoke-direct {v8, v13, v11, v0, v9}, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->setRestrictedPreferenceEnabled(Lcom/android/settingslib/RestrictedPreference;Ljava/lang/String;ZZ)V

    goto/32 :goto_45

    nop

    :goto_51
    goto/16 :goto_27

    :goto_52
    goto/32 :goto_0

    nop

    :goto_53
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v11

    goto/32 :goto_6a

    nop

    :goto_54
    move/from16 v16, v2

    goto/32 :goto_7f

    nop

    :goto_55
    invoke-static {v0}, Lcom/android/settingslib/accessibility/AccessibilityUtils;->getEnabledServicesFromSettings(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v9

    goto/32 :goto_75

    nop

    :goto_56
    goto/16 :goto_7a

    :goto_57
    goto/32 :goto_6b

    nop

    :goto_58
    if-nez v10, :cond_5

    goto/32 :goto_7a

    :cond_5
    goto/32 :goto_43

    nop

    :goto_59
    move-object/from16 v18, v0

    goto/32 :goto_6f

    nop

    :goto_5a
    move-object v5, v13

    goto/32 :goto_7b

    nop

    :goto_5b
    move-object v1, v13

    goto/32 :goto_7d

    nop

    :goto_5c
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_42

    nop

    :goto_5d
    invoke-direct {v8, v13, v15, v0}, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->putServiceExtras(Lcom/android/settingslib/RestrictedPreference;Landroid/content/pm/ResolveInfo;Ljava/lang/Boolean;)V

    goto/32 :goto_64

    nop

    :goto_5e
    move/from16 v18, v11

    goto/32 :goto_34

    nop

    :goto_5f
    move-object/from16 v19, v9

    goto/32 :goto_1b

    nop

    :goto_60
    if-eqz v0, :cond_6

    goto/32 :goto_22

    :cond_6
    goto/32 :goto_82

    nop

    :goto_61
    if-eqz v0, :cond_7

    goto/32 :goto_27

    :cond_7
    goto/32 :goto_8

    nop

    :goto_62
    new-instance v4, Landroid/content/ComponentName;

    goto/32 :goto_1d

    nop

    :goto_63
    invoke-direct {v4, v5, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_65

    nop

    :goto_64
    invoke-direct {v8, v13, v11, v10}, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->putSettingsExtras(Lcom/android/settingslib/RestrictedPreference;Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_9

    nop

    :goto_65
    iget-object v1, v8, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mContext:Landroid/content/Context;

    goto/32 :goto_32

    nop

    :goto_66
    move-object v13, v0

    goto/32 :goto_6e

    nop

    :goto_67
    iget-object v0, v7, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    goto/32 :goto_f

    nop

    :goto_68
    check-cast v6, Landroid/accessibilityservice/AccessibilityServiceInfo;

    goto/32 :goto_4e

    nop

    :goto_69
    move-object v3, v4

    goto/32 :goto_5

    nop

    :goto_6a
    new-instance v12, Ljava/util/ArrayList;

    goto/32 :goto_6d

    nop

    :goto_6b
    const/4 v0, 0x0

    goto/32 :goto_79

    nop

    :goto_6c
    if-lt v14, v11, :cond_8

    goto/32 :goto_4c

    :cond_8
    goto/32 :goto_6

    nop

    :goto_6d
    invoke-direct {v12, v11}, Ljava/util/ArrayList;-><init>(I)V

    goto/32 :goto_73

    nop

    :goto_6e
    move-object/from16 v19, v9

    goto/32 :goto_3a

    nop

    :goto_6f
    move-object/from16 v0, p0

    goto/32 :goto_5f

    nop

    :goto_70
    iget-object v0, v8, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mConfigedServiceList:Ljava/util/List;

    goto/32 :goto_4d

    nop

    :goto_71
    const/4 v0, 0x1

    :goto_72
    goto/32 :goto_50

    nop

    :goto_73
    const/4 v14, 0x0

    :goto_74
    goto/32 :goto_6c

    nop

    :goto_75
    iget-object v0, v8, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mDpm:Landroid/app/admin/DevicePolicyManager;

    goto/32 :goto_7c

    nop

    :goto_76
    if-eqz p2, :cond_9

    goto/32 :goto_52

    :cond_9
    goto/32 :goto_e

    nop

    :goto_77
    move-object/from16 v16, v3

    goto/32 :goto_69

    nop

    :goto_78
    invoke-virtual {v6}, Landroid/accessibilityservice/AccessibilityServiceInfo;->getAnimatedImageRes()I

    move-result v5

    goto/32 :goto_2c

    nop

    :goto_79
    goto :goto_72

    :goto_7a
    goto/32 :goto_71

    nop

    :goto_7b
    invoke-direct/range {v0 .. v5}, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->createCustomRestrictedPreference(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/android/settingslib/RestrictedPreference;

    move-result-object v0

    goto/32 :goto_2e

    nop

    :goto_7c
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    goto/32 :goto_3c

    nop

    :goto_7d
    move-object/from16 v3, v16

    goto/32 :goto_1c

    nop

    :goto_7e
    if-eqz v0, :cond_a

    goto/32 :goto_41

    :cond_a
    goto/32 :goto_51

    nop

    :goto_7f
    iget-object v2, v8, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->mPm:Landroid/content/pm/PackageManager;

    goto/32 :goto_39

    nop

    :goto_80
    move/from16 v11, v18

    goto/32 :goto_1a

    nop

    :goto_81
    invoke-direct/range {v0 .. v7}, Lcom/android/settings/accessibility/VisualAccessibilitySettings$RestrictedPreferenceHelper;->putBasicExtras(Lcom/android/settingslib/RestrictedPreference;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/String;Landroid/content/ComponentName;)V

    goto/32 :goto_3b

    nop

    :goto_82
    sget-object v0, Lcom/android/settings/accessibility/VisualAccessibilitySettings;->COMMON_SERVICES_LIST:Ljava/util/List;

    goto/32 :goto_1f

    nop
.end method
