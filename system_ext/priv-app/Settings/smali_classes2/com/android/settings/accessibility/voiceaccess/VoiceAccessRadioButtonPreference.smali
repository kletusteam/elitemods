.class public Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;
.super Lcom/android/settings/backup/CustomRadioButtonPreference;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDetailArrow:Landroid/widget/ImageView;

.field private mRadioButton:Landroid/widget/RadioButton;

.field private mRootView:Landroid/view/View;

.field private mSummaryView:Landroid/widget/TextView;

.field private mTitleView:Landroid/widget/TextView;


# direct methods
.method static bridge synthetic -$$Nest$mlaunchVoiceAccess(Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->launchVoiceAccess()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Lmiuix/preference/R$attr;->radioButtonPreferenceStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/backup/CustomRadioButtonPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object p1, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$layout;->preference_widget_detail:I

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setWidgetLayoutResource(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/backup/CustomRadioButtonPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object p1, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$layout;->preference_widget_detail:I

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setWidgetLayoutResource(I)V

    return-void
.end method

.method private launchVoiceAccess()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.miui.accessibility"

    const-string v2, "com.miui.accessibility.voiceaccess.settings.VoiceAccessSettings"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object p0, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/backup/CustomRadioButtonPreference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    iput-object p1, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mRootView:Landroid/view/View;

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/android/settings/R$dimen;->voice_access_preference_container_padding:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iget-object v0, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mRootView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p1, v1}, Landroid/view/View;->setPadding(IIII)V

    iget-object v0, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mRootView:Landroid/view/View;

    const v1, 0x1020001

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    iget-object p1, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {p1, v0}, Landroid/widget/RadioButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p1, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mRootView:Landroid/view/View;

    const v0, 0x1020016

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mTitleView:Landroid/widget/TextView;

    iget-object p1, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mRootView:Landroid/view/View;

    const v0, 0x1020010

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mSummaryView:Landroid/widget/TextView;

    iget-object p1, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mRootView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->detail_arrow:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mDetailArrow:Landroid/widget/ImageView;

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImportantForAccessibility(I)V

    iget-object p1, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mDetailArrow:Landroid/widget/ImageView;

    new-instance v0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference$1;

    invoke-direct {v0, p0}, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference$1;-><init>(Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance p1, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference$2;

    invoke-direct {p1, p0}, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference$2;-><init>(Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;)V

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    iget-object p1, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessController;->isVoiceAccessOn(Landroid/content/Context;)Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->setPreferenceState(Z)V

    return-void
.end method

.method public setPreferenceState(Z)V
    .locals 4

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/display/DarkModeTimeModeUtil;->isDarkModeEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget v0, Lcom/android/settings/R$drawable;->ic_voice_access_normal:I

    goto :goto_1

    :cond_1
    :goto_0
    sget v0, Lcom/android/settings/R$drawable;->ic_voice_access_selected:I

    :goto_1
    iget-object v1, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/backup/CustomRadioButtonPreference;->setCustomItemIcon(Landroid/graphics/drawable/Drawable;)V

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mContext:Landroid/content/Context;

    sget v1, Lcom/android/settings/R$color;->voice_access_radio_preference_bg_selected_color:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getColor(I)I

    move-result v0

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mContext:Landroid/content/Context;

    sget v1, Lcom/android/settings/R$color;->voice_access_radio_preference_bg_unselected_color:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getColor(I)I

    move-result v0

    :goto_2
    iget-object v1, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mRootView:Landroid/view/View;

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mRootView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    iget-object v3, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mRootView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3, v2}, Landroid/view/View;->setPadding(IIII)V

    :cond_3
    if-eqz p1, :cond_4

    iget-object v0, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mContext:Landroid/content/Context;

    sget v1, Lcom/android/settings/R$color;->voice_access_radio_preference_title_selected_color:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getColor(I)I

    move-result v0

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mContext:Landroid/content/Context;

    sget v1, Lcom/android/settings/R$color;->voice_access_radio_preference_title_unselected_color:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getColor(I)I

    move-result v0

    :goto_3
    iget-object v1, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mTitleView:Landroid/widget/TextView;

    if-eqz v1, :cond_5

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_5
    if-eqz p1, :cond_6

    iget-object v0, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mContext:Landroid/content/Context;

    sget v1, Lcom/android/settings/R$color;->voice_access_radio_preference_summary_selected_color:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getColor(I)I

    move-result v0

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mContext:Landroid/content/Context;

    sget v1, Lcom/android/settings/R$color;->voice_access_radio_preference_summary_unselected_color:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getColor(I)I

    move-result v0

    :goto_4
    if-eqz p1, :cond_7

    iget-object v1, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mContext:Landroid/content/Context;

    sget v3, Lcom/android/settings/R$string;->vpn_on:I

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    :cond_7
    iget-object v1, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mContext:Landroid/content/Context;

    sget v3, Lcom/android/settings/R$string;->vpn_off:I

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_5
    iget-object v3, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mSummaryView:Landroid/widget/TextView;

    if-eqz v3, :cond_8

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mSummaryView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mSummaryView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_8
    if-eqz p1, :cond_9

    iget-object p1, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mContext:Landroid/content/Context;

    sget v0, Lcom/android/settings/R$drawable;->ic_arrow_detail_selected:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    goto :goto_6

    :cond_9
    iget-object p1, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mContext:Landroid/content/Context;

    sget v0, Lcom/android/settings/R$drawable;->ic_arrow_detail_normal:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    :goto_6
    iget-object p0, p0, Lcom/android/settings/accessibility/voiceaccess/VoiceAccessRadioButtonPreference;->mDetailArrow:Landroid/widget/ImageView;

    if-eqz p0, :cond_a

    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_a
    return-void
.end method
