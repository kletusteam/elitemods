.class Lcom/android/settings/CutoutModeSettings$1;
.super Lcom/android/settings/CommonDialog;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/CutoutModeSettings;->showCutoutModeDialog(Lcom/android/settings/CutoutModeSettings$AppItem;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/CutoutModeSettings;

.field final synthetic val$item:Lcom/android/settings/CutoutModeSettings$AppItem;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/android/settings/CutoutModeSettings;Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;Lcom/android/settings/CutoutModeSettings$AppItem;I)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/CutoutModeSettings$1;->this$0:Lcom/android/settings/CutoutModeSettings;

    iput-object p4, p0, Lcom/android/settings/CutoutModeSettings$1;->val$item:Lcom/android/settings/CutoutModeSettings$AppItem;

    iput p5, p0, Lcom/android/settings/CutoutModeSettings$1;->val$position:I

    invoke-direct {p0, p2, p3}, Lcom/android/settings/CommonDialog;-><init>(Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method


# virtual methods
.method protected onPrepareBuild(Lmiuix/appcompat/app/AlertDialog$Builder;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/CutoutModeSettings$1;->this$0:Lcom/android/settings/CutoutModeSettings;

    iget-object v1, p0, Lcom/android/settings/CutoutModeSettings$1;->val$item:Lcom/android/settings/CutoutModeSettings$AppItem;

    invoke-virtual {v1}, Lcom/android/settings/CutoutModeSettings$AppItem;->getPkg()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/CutoutModeSettings$1;->val$item:Lcom/android/settings/CutoutModeSettings$AppItem;

    invoke-static {v2}, Lcom/android/settings/CutoutModeSettings$AppItem;->-$$Nest$fgetmCutoutMode(Lcom/android/settings/CutoutModeSettings$AppItem;)I

    move-result v2

    iget p0, p0, Lcom/android/settings/CutoutModeSettings$1;->val$position:I

    invoke-static {v0, p1, v1, v2, p0}, Lcom/android/settings/CutoutModeSettings;->-$$Nest$mprepareCutoutDialogBuild(Lcom/android/settings/CutoutModeSettings;Lmiuix/appcompat/app/AlertDialog$Builder;Ljava/lang/String;II)V

    return-void
.end method
