.class Lcom/android/settings/device/MiuiMemoryCard$MemoryInfoCallback;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/settings/device/MemoryInfoHelper$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/device/MiuiMemoryCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MemoryInfoCallback"
.end annotation


# instance fields
.field private mOuterRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/android/settings/device/MiuiMemoryCard;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/android/settings/device/MiuiMemoryCard;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/device/MiuiMemoryCard$MemoryInfoCallback;->mOuterRef:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public handleTaskResult(J)V
    .locals 8

    iget-object v0, p0, Lcom/android/settings/device/MiuiMemoryCard$MemoryInfoCallback;->mOuterRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/device/MiuiMemoryCard;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    if-nez v1, :cond_1

    return-void

    :cond_1
    invoke-static {v1}, Lcom/android/settings/device/MiuiAboutPhoneUtils;->getInstance(Landroid/content/Context;)Lcom/android/settings/device/MiuiAboutPhoneUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/settings/device/MiuiAboutPhoneUtils;->getTotalMemoryBytes()J

    move-result-wide v2

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v5, "%.0f"

    invoke-static {v1, v2, v3, v5, v4}, Lcom/android/settings/MiuiUtils;->formatShortSize(Landroid/content/Context;JLjava/lang/String;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-static {v0}, Lcom/android/settings/device/MiuiMemoryCard;->-$$Nest$fgettotalText(Lcom/android/settings/device/MiuiMemoryCard;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    sub-long p1, v2, p1

    const-string v5, "%.1f"

    invoke-static {v1, p1, p2, v5, v4}, Lcom/android/settings/MiuiUtils;->formatShortSize(Landroid/content/Context;JLjava/lang/String;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-static {v0}, Lcom/android/settings/device/MiuiMemoryCard;->-$$Nest$fgetusedText(Lcom/android/settings/device/MiuiMemoryCard;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    invoke-static {}, Lcom/android/settings/MiuiUtils;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_4

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    long-to-float p1, p1

    long-to-float p2, v2

    div-float/2addr p1, p2

    float-to-double p1, p1

    sub-double/2addr v4, p1

    double-to-float p1, v4

    invoke-static {v0}, Lcom/android/settings/device/MiuiMemoryCard;->-$$Nest$fgettotalText(Lcom/android/settings/device/MiuiMemoryCard;)Landroid/widget/TextView;

    move-result-object p2

    new-instance v1, Lcom/android/settings/device/MiuiMemoryCard$MemoryInfoCallback$1;

    invoke-direct {v1, p0, v0, p1}, Lcom/android/settings/device/MiuiMemoryCard$MemoryInfoCallback$1;-><init>(Lcom/android/settings/device/MiuiMemoryCard$MemoryInfoCallback;Lcom/android/settings/device/MiuiMemoryCard;F)V

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_4
    long-to-float p0, p1

    long-to-float p1, v2

    div-float/2addr p0, p1

    invoke-virtual {v0, p0}, Lcom/android/settings/device/MiuiMemoryCard;->setPercent(F)V

    :goto_0
    invoke-static {v0}, Lcom/android/settings/device/MiuiMemoryCard;->-$$Nest$fgetusedText(Lcom/android/settings/device/MiuiMemoryCard;)Landroid/widget/TextView;

    move-result-object p0

    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p0

    const-string p1, ""

    if-eq p0, p1, :cond_5

    new-instance p0, Lmiuix/animation/base/AnimConfig;

    invoke-direct {p0}, Lmiuix/animation/base/AnimConfig;-><init>()V

    const-wide/16 p1, 0x78

    invoke-virtual {p0, p1, p2}, Lmiuix/animation/base/AnimConfig;->setDelay(J)Lmiuix/animation/base/AnimConfig;

    move-result-object p0

    const/4 p1, 0x3

    new-array p2, p1, [F

    fill-array-data p2, :array_0

    const/4 v1, 0x0

    invoke-virtual {p0, v1, p2}, Lmiuix/animation/base/AnimConfig;->setEase(I[F)Lmiuix/animation/base/AnimConfig;

    move-result-object p0

    new-instance p2, Lmiuix/animation/base/AnimConfig;

    invoke-direct {p2}, Lmiuix/animation/base/AnimConfig;-><init>()V

    new-array p1, p1, [F

    fill-array-data p1, :array_1

    invoke-virtual {p2, v1, p1}, Lmiuix/animation/base/AnimConfig;->setEase(I[F)Lmiuix/animation/base/AnimConfig;

    move-result-object p1

    const/4 p2, 0x1

    new-array v2, p2, [Landroid/view/View;

    invoke-static {v0}, Lcom/android/settings/device/MiuiMemoryCard;->-$$Nest$fgetstorageView(Lcom/android/settings/device/MiuiMemoryCard;)Landroid/view/View;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v2}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/IFolme;->visible()Lmiuix/animation/IVisibleStyle;

    move-result-object v2

    new-array v3, p2, [Lmiuix/animation/IVisibleStyle$VisibleType;

    sget-object v4, Lmiuix/animation/IVisibleStyle$VisibleType;->HIDE:Lmiuix/animation/IVisibleStyle$VisibleType;

    aput-object v4, v3, v1

    const/4 v5, 0x0

    invoke-interface {v2, v5, v3}, Lmiuix/animation/IVisibleStyle;->setAlpha(F[Lmiuix/animation/IVisibleStyle$VisibleType;)Lmiuix/animation/IVisibleStyle;

    move-result-object v2

    new-array v3, p2, [Lmiuix/animation/IVisibleStyle$VisibleType;

    sget-object v6, Lmiuix/animation/IVisibleStyle$VisibleType;->SHOW:Lmiuix/animation/IVisibleStyle$VisibleType;

    aput-object v6, v3, v1

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-interface {v2, v7, v3}, Lmiuix/animation/IVisibleStyle;->setAlpha(F[Lmiuix/animation/IVisibleStyle$VisibleType;)Lmiuix/animation/IVisibleStyle;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/IVisibleStyle;->setHide()Lmiuix/animation/IVisibleStyle;

    move-result-object v2

    new-array v3, p2, [Lmiuix/animation/base/AnimConfig;

    aput-object p0, v3, v1

    invoke-interface {v2, v3}, Lmiuix/animation/IVisibleStyle;->show([Lmiuix/animation/base/AnimConfig;)V

    new-array p0, p2, [Landroid/view/View;

    invoke-static {v0}, Lcom/android/settings/device/MiuiMemoryCard;->-$$Nest$fgetcalculatingView(Lcom/android/settings/device/MiuiMemoryCard;)Landroid/view/View;

    move-result-object v0

    aput-object v0, p0, v1

    invoke-static {p0}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object p0

    invoke-interface {p0}, Lmiuix/animation/IFolme;->visible()Lmiuix/animation/IVisibleStyle;

    move-result-object p0

    new-array v0, p2, [Lmiuix/animation/IVisibleStyle$VisibleType;

    aput-object v6, v0, v1

    invoke-interface {p0, v7, v0}, Lmiuix/animation/IVisibleStyle;->setAlpha(F[Lmiuix/animation/IVisibleStyle$VisibleType;)Lmiuix/animation/IVisibleStyle;

    move-result-object p0

    new-array v0, p2, [Lmiuix/animation/IVisibleStyle$VisibleType;

    aput-object v4, v0, v1

    invoke-interface {p0, v5, v0}, Lmiuix/animation/IVisibleStyle;->setAlpha(F[Lmiuix/animation/IVisibleStyle$VisibleType;)Lmiuix/animation/IVisibleStyle;

    move-result-object p0

    invoke-interface {p0}, Lmiuix/animation/IVisibleStyle;->setShow()Lmiuix/animation/IVisibleStyle;

    move-result-object p0

    new-array p2, p2, [Lmiuix/animation/base/AnimConfig;

    aput-object p1, p2, v1

    invoke-interface {p0, p2}, Lmiuix/animation/IVisibleStyle;->hide([Lmiuix/animation/base/AnimConfig;)V

    :cond_5
    return-void

    :array_0
    .array-data 4
        0x43fa0000    # 500.0f
        0x3f666666    # 0.9f
        0x3e99999a    # 0.3f
    .end array-data

    :array_1
    .array-data 4
        0x43fa0000    # 500.0f
        0x3f666666    # 0.9f
        0x3e99999a    # 0.3f
    .end array-data
.end method
