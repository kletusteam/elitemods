.class public Lcom/android/settings/device/DeviceBasicInfoPresenter;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/device/DeviceBasicInfoPresenter$SpaceItemDecoration;
    }
.end annotation


# static fields
.field public static final ICON_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final INDEX_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAdapter:Lcom/android/settings/device/DeviceInfoAdapter;

.field private mBasicInfoCards:[Lcom/android/settings/device/DeviceCardInfo;

.field private mCards:[Lcom/android/settings/device/DeviceCardInfo;

.field private mContext:Landroid/content/Context;

.field private mDisplayCards:[Lcom/android/settings/device/DeviceCardInfo;

.field private mIsCardsInitCompleted:Ljava/lang/Boolean;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/settings/device/DeviceBasicInfoPresenter;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDisplayCards(Lcom/android/settings/device/DeviceBasicInfoPresenter;)[Lcom/android/settings/device/DeviceCardInfo;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mDisplayCards:[Lcom/android/settings/device/DeviceCardInfo;

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/device/DeviceBasicInfoPresenter$1;

    invoke-direct {v0}, Lcom/android/settings/device/DeviceBasicInfoPresenter$1;-><init>()V

    sput-object v0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->INDEX_MAP:Ljava/util/Map;

    new-instance v0, Lcom/android/settings/device/DeviceBasicInfoPresenter$2;

    invoke-direct {v0}, Lcom/android/settings/device/DeviceBasicInfoPresenter$2;-><init>()V

    sput-object v0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->ICON_MAP:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mIsCardsInitCompleted:Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mContext:Landroid/content/Context;

    const/4 p1, 0x7

    new-array p1, p1, [Lcom/android/settings/device/DeviceCardInfo;

    iput-object p1, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mCards:[Lcom/android/settings/device/DeviceCardInfo;

    const/4 p1, 0x2

    new-array p1, p1, [Lcom/android/settings/device/DeviceCardInfo;

    iput-object p1, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mBasicInfoCards:[Lcom/android/settings/device/DeviceCardInfo;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Lcom/android/settings/device/DeviceCardInfo;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mCards:[Lcom/android/settings/device/DeviceCardInfo;

    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mIsCardsInitCompleted:Ljava/lang/Boolean;

    return-void
.end method

.method private buildGridView(Landroid/view/View;)V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/MiuiUtils;->isLandScape(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->isSplitTabletDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/android/settings/R$id;->disclaimer:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    sget v1, Lcom/android/settings/R$id;->description_grid:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Lcom/android/settings/device/DeviceInfoAdapter;

    iget-object v3, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Lcom/android/settings/device/DeviceInfoAdapter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mAdapter:Lcom/android/settings/device/DeviceInfoAdapter;

    new-instance v1, Landroidx/recyclerview/widget/GridLayoutManager;

    iget-object v3, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3, v0}, Landroidx/recyclerview/widget/GridLayoutManager;-><init>(Landroid/content/Context;I)V

    iget-object v3, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mIsCardsInitCompleted:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mCards:[Lcom/android/settings/device/DeviceCardInfo;

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mBasicInfoCards:[Lcom/android/settings/device/DeviceCardInfo;

    :goto_1
    iput-object v3, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mDisplayCards:[Lcom/android/settings/device/DeviceCardInfo;

    new-instance v3, Lcom/android/settings/device/DeviceBasicInfoPresenter$3;

    invoke-direct {v3, p0, v0}, Lcom/android/settings/device/DeviceBasicInfoPresenter$3;-><init>(Lcom/android/settings/device/DeviceBasicInfoPresenter;I)V

    invoke-virtual {v1, v3}, Landroidx/recyclerview/widget/GridLayoutManager;->setSpanSizeLookup(Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;)V

    iget-object v0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mAdapter:Lcom/android/settings/device/DeviceInfoAdapter;

    iget-object v3, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mDisplayCards:[Lcom/android/settings/device/DeviceCardInfo;

    invoke-virtual {v0, v3}, Lcom/android/settings/device/DeviceInfoAdapter;->setDataList([Lcom/android/settings/device/DeviceCardInfo;)V

    iget-object v0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mAdapter:Lcom/android/settings/device/DeviceInfoAdapter;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getItemDecorationCount()I

    move-result v0

    if-lez v0, :cond_3

    invoke-virtual {p1, v2}, Landroidx/recyclerview/widget/RecyclerView;->getItemDecorationAt(I)Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    move-result-object v0

    if-nez v0, :cond_4

    :cond_3
    new-instance v0, Lcom/android/settings/device/DeviceBasicInfoPresenter$SpaceItemDecoration;

    invoke-direct {v0, p0}, Lcom/android/settings/device/DeviceBasicInfoPresenter$SpaceItemDecoration;-><init>(Lcom/android/settings/device/DeviceBasicInfoPresenter;)V

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    :cond_4
    invoke-virtual {p1, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    return-void
.end method

.method private createDeviceModelCard(Landroid/view/View$OnClickListener;)Lcom/android/settings/device/DeviceCardInfo;
    .locals 2

    new-instance v0, Lcom/android/settings/device/DeviceCardInfo;

    invoke-direct {v0}, Lcom/android/settings/device/DeviceCardInfo;-><init>()V

    iget-object p0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->isSplitTabletDevice()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/android/settings/R$string;->model_number_pad:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/android/settings/R$string;->model_number:I

    :goto_0
    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/android/settings/device/DeviceCardInfo;->setTitle(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/settings/device/MiuiAboutPhoneUtils;->getWrapModelNumber()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/android/settings/device/DeviceCardInfo;->setValue(Ljava/lang/String;)V

    sget-object p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->ICON_MAP:Ljava/util/Map;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/android/settings/device/DeviceCardInfo;->setIconResId(I)V

    invoke-virtual {v0, p1}, Lcom/android/settings/device/DeviceCardInfo;->setListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method private createMemoryCard(Landroid/view/View$OnClickListener;)Lcom/android/settings/device/DeviceCardInfo;
    .locals 3

    new-instance v0, Lcom/android/settings/device/DeviceCardInfo;

    invoke-direct {v0}, Lcom/android/settings/device/DeviceCardInfo;-><init>()V

    iget-object v1, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$string;->device_memory:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/device/DeviceCardInfo;->setTitle(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/device/DeviceBasicInfoPresenter;->setMemoryValueAndIndex(Lcom/android/settings/device/DeviceCardInfo;)V

    sget-object p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->ICON_MAP:Ljava/util/Map;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/android/settings/device/DeviceCardInfo;->setIconResId(I)V

    invoke-virtual {v0, p1}, Lcom/android/settings/device/DeviceCardInfo;->setListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method private getItemTitle(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "device_description_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/android/settings/device/ParseMiShopDataUtils;->getItemTitle(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object p0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p0

    const-string/jumbo v1, "string"

    invoke-virtual {v0, p1, v1, p0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p0

    if-eqz p0, :cond_0

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    const-string p0, ""

    :goto_0
    return-object p0
.end method

.method private initBasicInfo(Landroid/view/View$OnClickListener;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mBasicInfoCards:[Lcom/android/settings/device/DeviceCardInfo;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mBasicInfoCards:[Lcom/android/settings/device/DeviceCardInfo;

    invoke-direct {p0, p1}, Lcom/android/settings/device/DeviceBasicInfoPresenter;->createDeviceModelCard(Landroid/view/View$OnClickListener;)Lcom/android/settings/device/DeviceCardInfo;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mBasicInfoCards:[Lcom/android/settings/device/DeviceCardInfo;

    const/4 v1, 0x1

    invoke-direct {p0, p1}, Lcom/android/settings/device/DeviceBasicInfoPresenter;->createMemoryCard(Landroid/view/View$OnClickListener;)Lcom/android/settings/device/DeviceCardInfo;

    move-result-object p0

    aput-object p0, v0, v1

    return-void
.end method

.method private initData(Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 8

    iget-object v0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mCards:[Lcom/android/settings/device/DeviceCardInfo;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {p1}, Lcom/android/settings/device/ParseMiShopDataUtils;->getBasicItemsArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    invoke-direct {p0, p2}, Lcom/android/settings/device/DeviceBasicInfoPresenter;->createDeviceModelCard(Landroid/view/View$OnClickListener;)Lcom/android/settings/device/DeviceCardInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mCards:[Lcom/android/settings/device/DeviceCardInfo;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    if-eqz p1, :cond_a

    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_a

    move v0, v2

    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_9

    invoke-static {p1, v0}, Lcom/android/settings/device/JSONUtils;->getJSONObject(Lorg/json/JSONArray;I)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/device/ParseMiShopDataUtils;->getItemIndex(Lorg/json/JSONObject;)I

    move-result v3

    if-ltz v3, :cond_8

    const/4 v4, 0x7

    if-ge v3, v4, :cond_8

    invoke-direct {p0, v1}, Lcom/android/settings/device/DeviceBasicInfoPresenter;->getItemTitle(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/android/settings/device/DeviceCardInfo;

    invoke-direct {v5}, Lcom/android/settings/device/DeviceCardInfo;-><init>()V

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v5, v4}, Lcom/android/settings/device/DeviceCardInfo;->setTitle(Ljava/lang/String;)V

    :cond_1
    invoke-static {v1}, Lcom/android/settings/device/ParseMiShopDataUtils;->getItemSummary(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v5, v1}, Lcom/android/settings/device/DeviceCardInfo;->setValue(Ljava/lang/String;)V

    :cond_2
    sget-object v4, Lcom/android/settings/device/DeviceBasicInfoPresenter;->ICON_MAP:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v5, v4}, Lcom/android/settings/device/DeviceCardInfo;->setIconResId(I)V

    if-eqz v3, :cond_6

    const/4 v4, 0x2

    const/4 v6, 0x1

    if-eq v3, v4, :cond_5

    const/4 v1, 0x5

    if-eq v3, v1, :cond_3

    goto :goto_1

    :cond_3
    invoke-virtual {v5}, Lcom/android/settings/device/DeviceCardInfo;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "DeviceBasicInfoPresenter"

    const-string v4, "initData: MemoryTitle is empty"

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v4, Lcom/android/settings/R$string;->device_memory:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/android/settings/device/DeviceCardInfo;->setTitle(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p0, v5}, Lcom/android/settings/device/DeviceBasicInfoPresenter;->setMemoryValueAndIndex(Lcom/android/settings/device/DeviceCardInfo;)V

    goto :goto_1

    :cond_5
    invoke-virtual {v5, v6}, Lcom/android/settings/device/DeviceCardInfo;->setType(I)V

    iget-object v4, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v7, Lcom/android/settings/R$string;->device_camera:I

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/android/settings/device/DeviceCardInfo;->setTitle(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    const-string v4, "\\n"

    const-string v7, ""

    invoke-virtual {v1, v4, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v7, Lcom/android/settings/R$string;->camera_rear:I

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v1, v6, v2

    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/android/settings/device/DeviceCardInfo;->setValue(Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    invoke-static {v1}, Lcom/android/settings/device/ParseMiShopDataUtils;->setCpuInfo(Ljava/lang/String;)V

    const-string v1, "cpu_item"

    invoke-virtual {v5, v1}, Lcom/android/settings/device/DeviceCardInfo;->setKey(Ljava/lang/String;)V

    :cond_7
    :goto_1
    invoke-virtual {v5, p2}, Lcom/android/settings/device/DeviceCardInfo;->setListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mCards:[Lcom/android/settings/device/DeviceCardInfo;

    sget-object v4, Lcom/android/settings/device/DeviceBasicInfoPresenter;->INDEX_MAP:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput-object v5, v1, v3

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_9
    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mIsCardsInitCompleted:Ljava/lang/Boolean;

    :cond_a
    return-void
.end method


# virtual methods
.method public addBasicInfoCard(Lcom/android/settings/device/DeviceCardInfo;)V
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mBasicInfoCards:[Lcom/android/settings/device/DeviceCardInfo;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    new-array p1, p1, [Lcom/android/settings/device/DeviceCardInfo;

    invoke-interface {v0, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/android/settings/device/DeviceCardInfo;

    iput-object p1, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mBasicInfoCards:[Lcom/android/settings/device/DeviceCardInfo;

    invoke-virtual {p0}, Lcom/android/settings/device/DeviceBasicInfoPresenter;->isCardsInitComplete()Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mBasicInfoCards:[Lcom/android/settings/device/DeviceCardInfo;

    iput-object p1, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mDisplayCards:[Lcom/android/settings/device/DeviceCardInfo;

    iget-object v0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mAdapter:Lcom/android/settings/device/DeviceInfoAdapter;

    invoke-virtual {v0, p1}, Lcom/android/settings/device/DeviceInfoAdapter;->setDataList([Lcom/android/settings/device/DeviceCardInfo;)V

    :cond_0
    iget-object p0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mAdapter:Lcom/android/settings/device/DeviceInfoAdapter;

    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.method public getCardByIndex(I)Lcom/android/settings/device/DeviceCardInfo;
    .locals 1

    iget-object p0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mCards:[Lcom/android/settings/device/DeviceCardInfo;

    sget-object v0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->INDEX_MAP:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    aget-object p0, p0, p1

    return-object p0
.end method

.method public getCards()[Lcom/android/settings/device/DeviceCardInfo;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mCards:[Lcom/android/settings/device/DeviceCardInfo;

    return-object p0
.end method

.method public getRamInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/res/Configuration;->getLocales()Landroid/os/LocaleList;

    move-result-object p0

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/os/LocaleList;->get(I)Ljava/util/Locale;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object p0

    const-string v0, "ja"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    const-string p0, "G"

    invoke-virtual {p2, p0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result p0

    invoke-virtual {p2, p1, p0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    const-string p0, "\\s"

    invoke-virtual {p2, p0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    aget-object p0, p0, p1

    return-object p0
.end method

.method public isCardsInitComplete()Z
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mIsCardsInitCompleted:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method public setMemoryValueAndIndex(Lcom/android/settings/device/DeviceCardInfo;)V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/device/MiuiAboutPhoneUtils;->getInstance(Landroid/content/Context;)Lcom/android/settings/device/MiuiAboutPhoneUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/device/MiuiAboutPhoneUtils;->getTotaolRam()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/settings/special/ExternalRamController;->isChecked(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v2, v0}, Lcom/android/settings/device/DeviceBasicInfoPresenter;->getRamInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "+"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mContext:Landroid/content/Context;

    invoke-static {p0}, Lcom/android/settings/special/ExternalRamController;->getBdSizeInfo(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 p0, 0x5

    invoke-virtual {p1, p0}, Lcom/android/settings/device/DeviceCardInfo;->setIndex(I)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Lcom/android/settings/device/DeviceCardInfo;->setValue(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v0}, Lcom/android/settings/device/DeviceCardInfo;->setValue(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public showBasicInfoGridView(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mIsCardsInitCompleted:Ljava/lang/Boolean;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, p1}, Lcom/android/settings/device/DeviceBasicInfoPresenter;->buildGridView(Landroid/view/View;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public showBasicInfoGridView(Landroid/view/View;Ljava/lang/String;ZLandroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_4

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p2}, Lcom/android/settings/device/ParseMiShopDataUtils;->showBasicItems(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p2, p4}, Lcom/android/settings/device/DeviceBasicInfoPresenter;->initData(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    :cond_1
    iget-object p2, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mIsCardsInitCompleted:Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-nez p2, :cond_2

    if-nez p3, :cond_2

    const-string p0, "DeviceBasicInfoPresenter"

    const-string p2, "card init not complete"

    invoke-static {p0, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/16 p0, 0x8

    invoke-virtual {p1, p0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_2
    iget-object p2, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mIsCardsInitCompleted:Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-nez p2, :cond_3

    invoke-direct {p0, p4}, Lcom/android/settings/device/DeviceBasicInfoPresenter;->initBasicInfo(Landroid/view/View$OnClickListener;)V

    :cond_3
    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, p1}, Lcom/android/settings/device/DeviceBasicInfoPresenter;->buildGridView(Landroid/view/View;)V

    :cond_4
    :goto_0
    return-void
.end method

.method public updateCameraInfo(Ljava/lang/String;)V
    .locals 7

    iget-object v0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mIsCardsInitCompleted:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {p1}, Lcom/android/settings/device/ParseMiShopDataUtils;->getDataSuccess(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    goto/16 :goto_0

    :cond_1
    invoke-static {p1}, Lcom/android/settings/device/ParseMiShopDataUtils;->getAllParamData(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/device/ParseMiShopDataUtils;->showBasicItems(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lcom/android/settings/device/ParseMiShopDataUtils;->getFrontCameraPixel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/device/ParseMiShopDataUtils;->isCameraPixelEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    return-void

    :cond_3
    invoke-static {p1}, Lcom/android/settings/device/ParseMiShopDataUtils;->getRearCameraPixel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget v3, Lcom/android/settings/R$string;->camera_front:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v0, Lcom/android/settings/R$string;->camera_rear:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p1, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mCards:[Lcom/android/settings/device/DeviceCardInfo;

    sget-object v1, Lcom/android/settings/device/DeviceBasicInfoPresenter;->INDEX_MAP:Ljava/util/Map;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aget-object v0, v0, v1

    if-eqz v0, :cond_4

    invoke-virtual {v0, p1}, Lcom/android/settings/device/DeviceCardInfo;->setValue(Ljava/lang/String;)V

    iget-object p0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mAdapter:Lcom/android/settings/device/DeviceInfoAdapter;

    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    :cond_4
    :goto_0
    return-void
.end method

.method public updateCardByIndex(ILcom/android/settings/device/DeviceCardInfo;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mCards:[Lcom/android/settings/device/DeviceCardInfo;

    sget-object v1, Lcom/android/settings/device/DeviceBasicInfoPresenter;->INDEX_MAP:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    aput-object p2, v0, p1

    iget-object p0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter;->mAdapter:Lcom/android/settings/device/DeviceInfoAdapter;

    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method
