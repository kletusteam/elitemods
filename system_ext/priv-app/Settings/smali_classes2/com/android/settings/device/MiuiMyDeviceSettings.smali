.class public Lcom/android/settings/device/MiuiMyDeviceSettings;
.super Lcom/android/settings/dashboard/DashboardFragment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/device/MiuiMyDeviceSettings$ReadVersionTask;,
        Lcom/android/settings/device/MiuiMyDeviceSettings$UpdateInfoCallback;,
        Lcom/android/settings/device/MiuiMyDeviceSettings$RemoteServiceConn;
    }
.end annotation


# static fields
.field public static final DEVICE_MODEL_ORDER:I

.field public static final DEVICE_NAME_ORDER:I


# instance fields
.field private mCards:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field private mDeviceBasicInfoPresenter:Lcom/android/settings/device/DeviceBasicInfoPresenter;

.field private mDeviceInfoCallback:Lcom/android/settings/device/MiuiMyDeviceSettings$UpdateInfoCallback;

.field mDeviceNameCardView:Lcom/android/settings/device/MiuiDeviceNameCard;

.field private mGridViewRoot:Landroid/view/View;

.field private mHandler:Landroid/os/Handler;

.field private mHelper:Lcom/android/settings/device/DeviceParamsInitHelper;

.field private mIsNeedShowAndroid12:Z

.field private mIsOwnerUser:Z

.field mMemoryCardView:Lcom/android/settings/device/MiuiMemoryCard;

.field private mMoreDeviceParams:Landroid/view/View;

.field private mRemoteService:Lcom/android/settings/aidl/IRemoteGetDeviceInfoService;

.field private mRemoteServiceConn:Lcom/android/settings/device/MiuiMyDeviceSettings$RemoteServiceConn;

.field private mRootView:Landroid/view/View;

.field mVersionCardView:Lcom/android/settings/device/MiuiVersionCard;


# direct methods
.method public static synthetic $r8$lambda$-du3_PW9QsbF5cfdwVTqPPjgZLU(Lcom/android/settings/device/MiuiMyDeviceSettings;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/device/MiuiMyDeviceSettings;->lambda$initMallCard$2(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$18ufA0boU3WxE50XwZq3V5paAu4(Lcom/android/settings/device/MiuiMyDeviceSettings;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/device/MiuiMyDeviceSettings;->lambda$onCreateView$0(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$IXSF34C4tlztOwJCY7LT3nIIMgs(Lcom/android/settings/device/MiuiMyDeviceSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->lambda$updateCpuIconIfNeed$1()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmDeviceBasicInfoPresenter(Lcom/android/settings/device/MiuiMyDeviceSettings;)Lcom/android/settings/device/DeviceBasicInfoPresenter;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mDeviceBasicInfoPresenter:Lcom/android/settings/device/DeviceBasicInfoPresenter;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDeviceInfoCallback(Lcom/android/settings/device/MiuiMyDeviceSettings;)Lcom/android/settings/device/MiuiMyDeviceSettings$UpdateInfoCallback;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mDeviceInfoCallback:Lcom/android/settings/device/MiuiMyDeviceSettings$UpdateInfoCallback;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmGridViewRoot(Lcom/android/settings/device/MiuiMyDeviceSettings;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mGridViewRoot:Landroid/view/View;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHelper(Lcom/android/settings/device/MiuiMyDeviceSettings;)Lcom/android/settings/device/DeviceParamsInitHelper;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mHelper:Lcom/android/settings/device/DeviceParamsInitHelper;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmRemoteService(Lcom/android/settings/device/MiuiMyDeviceSettings;)Lcom/android/settings/aidl/IRemoteGetDeviceInfoService;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mRemoteService:Lcom/android/settings/aidl/IRemoteGetDeviceInfoService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmHelper(Lcom/android/settings/device/MiuiMyDeviceSettings;Lcom/android/settings/device/DeviceParamsInitHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mHelper:Lcom/android/settings/device/DeviceParamsInitHelper;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsNeedShowAndroid12(Lcom/android/settings/device/MiuiMyDeviceSettings;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mIsNeedShowAndroid12:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmRemoteService(Lcom/android/settings/device/MiuiMyDeviceSettings;Lcom/android/settings/aidl/IRemoteGetDeviceInfoService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mRemoteService:Lcom/android/settings/aidl/IRemoteGetDeviceInfoService;

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetHandler(Lcom/android/settings/device/MiuiMyDeviceSettings;)Landroid/os/Handler;
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->getHandler()Landroid/os/Handler;

    move-result-object p0

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->hasMarketName()Z

    move-result v0

    const/4 v1, 0x2

    const/4 v2, 0x3

    if-nez v0, :cond_1

    const/4 v0, 0x0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v2

    :goto_1
    sput v0, Lcom/android/settings/device/MiuiMyDeviceSettings;->DEVICE_MODEL_ORDER:I

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->hasMarketName()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    move v1, v2

    :cond_3
    :goto_2
    sput v1, Lcom/android/settings/device/MiuiMyDeviceSettings;->DEVICE_NAME_ORDER:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/dashboard/DashboardFragment;-><init>()V

    sget-boolean v0, Lmiui/os/Build;->IS_STABLE_VERSION:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    if-nez v0, :cond_0

    const-string/jumbo v0, "ro.miui.ui.version.code"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/16 v2, 0xb

    if-ne v0, v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    iput-boolean v1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mIsNeedShowAndroid12:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mCards:Ljava/util/List;

    new-instance v0, Lcom/android/settings/device/MiuiMyDeviceSettings$1;

    invoke-direct {v0, p0}, Lcom/android/settings/device/MiuiMyDeviceSettings$1;-><init>(Lcom/android/settings/device/MiuiMyDeviceSettings;)V

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private getHandler()Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method private initCardView()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mVersionCardView:Lcom/android/settings/device/MiuiVersionCard;

    invoke-virtual {v0, p0}, Lcom/android/settings/device/MiuiVersionCard;->setFragment(Lcom/android/settings/dashboard/DashboardFragment;)V

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mDeviceNameCardView:Lcom/android/settings/device/MiuiDeviceNameCard;

    invoke-virtual {v0, p0}, Lcom/android/settings/device/MiuiDeviceNameCard;->setFragment(Lcom/android/settings/dashboard/DashboardFragment;)V

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mMemoryCardView:Lcom/android/settings/device/MiuiMemoryCard;

    invoke-virtual {v0, p0}, Lcom/android/settings/device/MiuiMemoryCard;->setFragment(Lcom/android/settings/dashboard/DashboardFragment;)V

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mCards:Ljava/util/List;

    iget-object v1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mVersionCardView:Lcom/android/settings/device/MiuiVersionCard;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mCards:Ljava/util/List;

    iget-object v1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mDeviceNameCardView:Lcom/android/settings/device/MiuiDeviceNameCard;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mCards:Ljava/util/List;

    iget-object v1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mMemoryCardView:Lcom/android/settings/device/MiuiMemoryCard;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mCards:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/view/View;

    aput-object v1, v3, v2

    invoke-static {v3}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v3

    invoke-interface {v3}, Lmiuix/animation/IFolme;->touch()Lmiuix/animation/ITouchStyle;

    move-result-object v3

    new-array v2, v2, [Lmiuix/animation/base/AnimConfig;

    invoke-interface {v3, v1, v2}, Lmiuix/animation/ITouchStyle;->handleTouchOf(Landroid/view/View;[Lmiuix/animation/base/AnimConfig;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mVersionCardView:Lcom/android/settings/device/MiuiVersionCard;

    invoke-virtual {v0}, Lcom/android/settings/device/MiuiVersionCard;->refreshUpdateStatus()V

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mDeviceNameCardView:Lcom/android/settings/device/MiuiDeviceNameCard;

    invoke-virtual {v0}, Lcom/android/settings/device/MiuiDeviceNameCard;->refreshDeviceName()V

    iget-boolean v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mIsNeedShowAndroid12:Z

    if-eqz v0, :cond_1

    new-instance v0, Lcom/android/settings/device/MiuiMyDeviceSettings$ReadVersionTask;

    invoke-direct {v0, p0}, Lcom/android/settings/device/MiuiMyDeviceSettings$ReadVersionTask;-><init>(Lcom/android/settings/device/MiuiMyDeviceSettings;)V

    sget-object p0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, p0, v1}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_1
    return-void
.end method

.method private initMallCard()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mRootView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->mall_card:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-wide v2, 0x9fa52400L

    invoke-static {v1, v2, v3}, Lcom/android/settings/utils/SettingsFeatures;->isNeedHideShopEntrance(Landroid/content/Context;J)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 p0, 0x8

    invoke-virtual {v0, p0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/view/View;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-static {v1}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/IFolme;->touch()Lmiuix/animation/ITouchStyle;

    move-result-object v1

    new-array v2, v2, [Lmiuix/animation/base/AnimConfig;

    invoke-interface {v1, v0, v2}, Lmiuix/animation/ITouchStyle;->handleTouchOf(Landroid/view/View;[Lmiuix/animation/base/AnimConfig;)V

    new-instance v1, Lcom/android/settings/device/MiuiMyDeviceSettings$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/settings/device/MiuiMyDeviceSettings$$ExternalSyntheticLambda1;-><init>(Lcom/android/settings/device/MiuiMyDeviceSettings;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private synthetic lambda$initMallCard$2(Landroid/view/View;)V
    .locals 2

    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.xiaomi.shop"

    invoke-static {v0, v1}, Lcom/android/settings/MiuiUtils;->isAppInstalledAndEnabled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "https://m.mi.com/p?pid=111&root=com.xiaomi.shop2.plugin.webview.RootFragment&cid=3007.0001&url=https%3A%2F%2Fm.mi.com%2Fw%2Fmishop_activity%3F_rt%3Dweex%26pageid%3D556%26sign%3D8aa44926bc0707f203c7ed7aeb606e78%26pdl%3Djianyu&fallback=https%3A%2F%2Fm.mi.com%2Fw%2Fmishop_activity%3F_rt%3Dweex%26pageid%3D556%26sign%3D8aa44926bc0707f203c7ed7aeb606e78%26pdl%3Djianyu&masid=3007.0001"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string/jumbo v0, "my_device"

    const-string/jumbo v1, "native_equity"

    invoke-static {v0, v1}, Lcom/android/settingslib/util/OneTrackInterfaceUtils;->trackPreferenceClick(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "mimarket://details/detailcard?id=com.xiaomi.shop&ref=shezhi&launchWhenInstalled=true"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    :goto_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/high16 v1, 0x10000

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "initMallCard: can not find Activity "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "MiuiMyDeviceSettings"

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private synthetic lambda$onCreateView$0(Landroid/view/View;)V
    .locals 0

    const-class p1, Lcom/android/settings/device/controller/MiuiAllSpecsController;

    invoke-virtual {p0, p1}, Lcom/android/settings/dashboard/DashboardFragment;->use(Ljava/lang/Class;)Lcom/android/settingslib/core/AbstractPreferenceController;

    move-result-object p0

    check-cast p0, Lcom/android/settings/device/controller/MiuiAllSpecsController;

    invoke-virtual {p0}, Lcom/android/settings/device/controller/MiuiAllSpecsController;->gotoAllSpecsFragment()V

    return-void
.end method

.method private synthetic lambda$updateCpuIconIfNeed$1()V
    .locals 0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0}, Lcom/android/settings/MiuiUtils;->queryAndUpdateCpuIcon(Landroid/content/Context;)V

    return-void
.end method

.method private updateCpuIconIfNeed()V
    .locals 1

    new-instance v0, Lcom/android/settings/device/MiuiMyDeviceSettings$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0}, Lcom/android/settings/device/MiuiMyDeviceSettings$$ExternalSyntheticLambda2;-><init>(Lcom/android/settings/device/MiuiMyDeviceSettings;)V

    invoke-static {v0}, Lcom/android/settingslib/utils/ThreadUtils;->postOnBackgroundThread(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method


# virtual methods
.method protected createPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/settingslib/core/AbstractPreferenceController;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/android/settings/device/controller/MiuiVersionController;

    invoke-direct {v1, p1}, Lcom/android/settings/device/controller/MiuiVersionController;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/settings/device/controller/BaseDeviceInfoController;->setIsAvailable(Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/device/controller/MiuiFirmwareVersionController;

    invoke-direct {v1, p1}, Lcom/android/settings/device/controller/MiuiFirmwareVersionController;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/settings/device/controller/BaseDeviceInfoController;->setIsAvailable(Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/device/controller/MiuiDeviceSecurityPatchController;

    invoke-direct {v1, p1}, Lcom/android/settings/device/controller/MiuiDeviceSecurityPatchController;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/settings/device/controller/BaseDeviceInfoController;->setIsAvailable(Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/device/controller/MiuiAllSpecsController;

    invoke-direct {v1, p1, p0}, Lcom/android/settings/device/controller/MiuiAllSpecsController;-><init>(Landroid/content/Context;Lcom/android/settings/device/MiuiMyDeviceSettings;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/device/controller/MiuiBackupController;

    invoke-direct {v1, p1}, Lcom/android/settings/device/controller/MiuiBackupController;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/android/settings/device/controller/MiuiOneKeyMirgrateController;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    invoke-direct {v1, p1, p0}, Lcom/android/settings/device/controller/MiuiOneKeyMirgrateController;-><init>(Landroid/content/Context;Landroid/app/Activity;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance p0, Lcom/android/settings/device/controller/MiuiFactoryResetController;

    invoke-direct {p0, p1}, Lcom/android/settings/device/controller/MiuiFactoryResetController;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance p0, Lcom/android/settings/device/controller/MiuiTransferRecordController;

    invoke-direct {p0, p1}, Lcom/android/settings/device/controller/MiuiTransferRecordController;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance p0, Lcom/android/settings/device/controller/MaintenanceModeController;

    invoke-direct {p0, p1}, Lcom/android/settings/device/controller/MaintenanceModeController;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance p0, Lcom/android/settings/device/controller/MiuiCredentialsController;

    invoke-direct {p0, p1}, Lcom/android/settings/device/controller/MiuiCredentialsController;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public getInitHelper()Lcom/android/settings/device/DeviceParamsInitHelper;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mHelper:Lcom/android/settings/device/DeviceParamsInitHelper;

    return-object p0
.end method

.method protected getLogTag()Ljava/lang/String;
    .locals 0

    const-class p0, Lcom/android/settings/device/MiuiMyDeviceSettings;

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public getName()Ljava/lang/String;
    .locals 0

    const-class p0, Lcom/android/settings/device/MiuiMyDeviceSettings;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method protected getPreferenceScreenResId()I
    .locals 0

    sget p0, Lcom/android/settings/R$xml;->my_device_settings:I

    return p0
.end method

.method public getPresenter()Lcom/android/settings/device/DeviceBasicInfoPresenter;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mDeviceBasicInfoPresenter:Lcom/android/settings/device/DeviceBasicInfoPresenter;

    return-object p0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/dashboard/DashboardFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->updateCpuIconIfNeed()V

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mIsOwnerUser:Z

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->setHasOptionsMenu(Z)V

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->isShowMyDevice()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    sget v0, Lcom/android/settings/R$string;->my_device:I

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setTitle(I)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    sget v0, Lcom/android/settings/R$string;->about_settings:I

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setTitle(I)V

    :goto_1
    const/4 p1, 0x0

    if-nez p1, :cond_2

    new-instance p1, Lcom/android/settings/device/DeviceBasicInfoPresenter;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/android/settings/device/DeviceBasicInfoPresenter;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mDeviceBasicInfoPresenter:Lcom/android/settings/device/DeviceBasicInfoPresenter;

    new-instance p1, Lcom/android/settings/device/MiuiMyDeviceSettings$UpdateInfoCallback;

    invoke-direct {p1, p0}, Lcom/android/settings/device/MiuiMyDeviceSettings$UpdateInfoCallback;-><init>(Lcom/android/settings/device/MiuiMyDeviceSettings;)V

    iput-object p1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mDeviceInfoCallback:Lcom/android/settings/device/MiuiMyDeviceSettings$UpdateInfoCallback;

    new-instance p1, Lcom/android/settings/device/MiuiMyDeviceSettings$RemoteServiceConn;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0}, Lcom/android/settings/device/MiuiMyDeviceSettings$RemoteServiceConn;-><init>(Lcom/android/settings/device/MiuiMyDeviceSettings;Lcom/android/settings/device/MiuiMyDeviceSettings$RemoteServiceConn-IA;)V

    iput-object p1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mRemoteServiceConn:Lcom/android/settings/device/MiuiMyDeviceSettings$RemoteServiceConn;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mRemoteServiceConn:Lcom/android/settings/device/MiuiMyDeviceSettings$RemoteServiceConn;

    invoke-static {p1, p0}, Lcom/android/settings/device/RemoteServiceUtil;->bindRemoteService(Landroid/content/Context;Landroid/content/ServiceConnection;)Z

    :cond_2
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/android/settings/MiuiUtils;->updateFragmentView(Landroid/app/Activity;Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mRootView:Landroid/view/View;

    if-nez v0, :cond_2

    sget v0, Lcom/android/settings/R$layout;->my_device_settings:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mRootView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->prefs_container:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/SettingsPreferenceFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getListView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    instance-of p2, p1, Lmiuix/springback/view/SpringBackLayout;

    if-eqz p2, :cond_1

    invoke-virtual {p1, v1}, Landroid/view/View;->setEnabled(Z)V

    :cond_1
    iget-object p1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mRootView:Landroid/view/View;

    sget p2, Lcom/android/settings/R$id;->device_params:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mGridViewRoot:Landroid/view/View;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mRootView:Landroid/view/View;

    sget p2, Lcom/android/settings/R$id;->miui_version_card_view:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/android/settings/device/MiuiVersionCard;

    iput-object p1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mVersionCardView:Lcom/android/settings/device/MiuiVersionCard;

    iget-object p1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mRootView:Landroid/view/View;

    sget p2, Lcom/android/settings/R$id;->device_name_card_view:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/android/settings/device/MiuiDeviceNameCard;

    iput-object p1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mDeviceNameCardView:Lcom/android/settings/device/MiuiDeviceNameCard;

    iget-object p1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mRootView:Landroid/view/View;

    sget p2, Lcom/android/settings/R$id;->device_memory_card_view:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/android/settings/device/MiuiMemoryCard;

    iput-object p1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mMemoryCardView:Lcom/android/settings/device/MiuiMemoryCard;

    iget-object p1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mRootView:Landroid/view/View;

    sget p2, Lcom/android/settings/R$id;->device_more_parameter:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mMoreDeviceParams:Landroid/view/View;

    const/4 p2, 0x1

    new-array p3, p2, [Landroid/view/View;

    aput-object p1, p3, v1

    invoke-static {p3}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/IFolme;->touch()Lmiuix/animation/ITouchStyle;

    move-result-object p1

    const/high16 p3, 0x3f800000    # 1.0f

    new-array v0, v1, [Lmiuix/animation/ITouchStyle$TouchType;

    invoke-interface {p1, p3, v0}, Lmiuix/animation/ITouchStyle;->setScale(F[Lmiuix/animation/ITouchStyle$TouchType;)Lmiuix/animation/ITouchStyle;

    move-result-object p1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget v0, Lcom/android/settings/R$color;->miuisettings_item_touch_color:I

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    invoke-virtual {p3, v0, v2}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result p3

    invoke-interface {p1, p3}, Lmiuix/animation/ITouchStyle;->setBackgroundColor(I)Lmiuix/animation/ITouchStyle;

    move-result-object p1

    invoke-interface {p1, p2}, Lmiuix/animation/ITouchStyle;->setTintMode(I)Lmiuix/animation/ITouchStyle;

    move-result-object p1

    iget-object p2, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mMoreDeviceParams:Landroid/view/View;

    new-array p3, v1, [Lmiuix/animation/base/AnimConfig;

    invoke-interface {p1, p2, p3}, Lmiuix/animation/ITouchStyle;->handleTouchOf(Landroid/view/View;[Lmiuix/animation/base/AnimConfig;)V

    iget-object p1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mMoreDeviceParams:Landroid/view/View;

    new-instance p2, Lcom/android/settings/device/MiuiMyDeviceSettings$$ExternalSyntheticLambda0;

    invoke-direct {p2, p0}, Lcom/android/settings/device/MiuiMyDeviceSettings$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/device/MiuiMyDeviceSettings;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mRootView:Landroid/view/View;

    return-object p0
.end method

.method public onDestroy()V
    .locals 3

    invoke-super {p0}, Lcom/android/settingslib/core/lifecycle/ObservablePreferenceFragment;->onDestroy()V

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mRemoteService:Lcom/android/settings/aidl/IRemoteGetDeviceInfoService;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mDeviceInfoCallback:Lcom/android/settings/device/MiuiMyDeviceSettings$UpdateInfoCallback;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/android/settings/aidl/IRemoteGetDeviceInfoService;->unregisteCallback(Lcom/android/settings/aidl/IRequestCallback;)V

    iput-object v2, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mDeviceInfoCallback:Lcom/android/settings/device/MiuiMyDeviceSettings$UpdateInfoCallback;

    :cond_0
    iput-object v2, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mRemoteService:Lcom/android/settings/aidl/IRemoteGetDeviceInfoService;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    :goto_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mRemoteServiceConn:Lcom/android/settings/device/MiuiMyDeviceSettings$RemoteServiceConn;

    if-eqz p0, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_2
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    invoke-super {p0}, Lcom/android/settingslib/miuisettings/preference/PreferenceFragment;->onDestroyView()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings;->mRootView:Landroid/view/View;

    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onPause()V

    const-string/jumbo p0, "provision_about_page_v85x"

    invoke-static {p0}, Lcom/android/settingslib/util/MiStatInterfaceUtils;->trackPageEnd(Ljava/lang/String;)V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/dashboard/DashboardFragment;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->initCardView()V

    const-string/jumbo p0, "provision_about_page_v85x"

    invoke-static {p0}, Lcom/android/settingslib/util/MiStatInterfaceUtils;->trackPageStart(Ljava/lang/String;)V

    return-void
.end method

.method public onStart()V
    .locals 0

    invoke-super {p0}, Lcom/android/settings/dashboard/DashboardFragment;->onStart()V

    invoke-direct {p0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->initMallCard()V

    return-void
.end method
