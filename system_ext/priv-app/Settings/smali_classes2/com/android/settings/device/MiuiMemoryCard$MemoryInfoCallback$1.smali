.class Lcom/android/settings/device/MiuiMemoryCard$MemoryInfoCallback$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/device/MiuiMemoryCard$MemoryInfoCallback;->handleTaskResult(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/device/MiuiMemoryCard$MemoryInfoCallback;

.field final synthetic val$card:Lcom/android/settings/device/MiuiMemoryCard;

.field final synthetic val$mPercent:F


# direct methods
.method constructor <init>(Lcom/android/settings/device/MiuiMemoryCard$MemoryInfoCallback;Lcom/android/settings/device/MiuiMemoryCard;F)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/device/MiuiMemoryCard$MemoryInfoCallback$1;->this$0:Lcom/android/settings/device/MiuiMemoryCard$MemoryInfoCallback;

    iput-object p2, p0, Lcom/android/settings/device/MiuiMemoryCard$MemoryInfoCallback$1;->val$card:Lcom/android/settings/device/MiuiMemoryCard;

    iput p3, p0, Lcom/android/settings/device/MiuiMemoryCard$MemoryInfoCallback$1;->val$mPercent:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    iget-object v0, p0, Lcom/android/settings/device/MiuiMemoryCard$MemoryInfoCallback$1;->val$card:Lcom/android/settings/device/MiuiMemoryCard;

    invoke-static {v0}, Lcom/android/settings/device/MiuiMemoryCard;->-$$Nest$fgetmView(Lcom/android/settings/device/MiuiMemoryCard;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/android/settings/R$id;->storage_size_bg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/device/MiuiMemoryCard$MemoryInfoCallback$1;->val$card:Lcom/android/settings/device/MiuiMemoryCard;

    invoke-static {v1}, Lcom/android/settings/device/MiuiMemoryCard;->-$$Nest$fgetmView(Lcom/android/settings/device/MiuiMemoryCard;)Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/android/settings/R$id;->water_box_view:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroidx/cardview/widget/CardView;

    invoke-virtual {v1}, Landroidx/cardview/widget/CardView;->getRadius()F

    move-result v1

    new-instance v3, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v3}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    const/16 v2, 0x8

    new-array v2, v2, [F

    const/4 v4, 0x0

    const/4 v5, 0x0

    aput v5, v2, v4

    const/4 v4, 0x1

    aput v5, v2, v4

    const/4 v4, 0x2

    aput v5, v2, v4

    const/4 v4, 0x3

    aput v5, v2, v4

    const/4 v4, 0x4

    aput v1, v2, v4

    const/4 v4, 0x5

    aput v1, v2, v4

    const/4 v4, 0x6

    aput v1, v2, v4

    const/4 v4, 0x7

    aput v1, v2, v4

    invoke-virtual {v3, v2}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadii([F)V

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$color;->storage_size_bg:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget p0, p0, Lcom/android/settings/device/MiuiMemoryCard$MemoryInfoCallback$1;->val$mPercent:F

    mul-float/2addr v1, p0

    float-to-int v5, v1

    new-instance p0, Landroid/graphics/drawable/InsetDrawable;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p0

    invoke-direct/range {v2 .. v7}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
