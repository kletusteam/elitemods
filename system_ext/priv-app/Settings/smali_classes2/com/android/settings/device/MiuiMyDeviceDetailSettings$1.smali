.class Lcom/android/settings/device/MiuiMyDeviceDetailSettings$1;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/device/MiuiMyDeviceDetailSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/device/MiuiMyDeviceDetailSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$1;->this$0:Lcom/android/settings/device/MiuiMyDeviceDetailSettings;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$1;->this$0:Lcom/android/settings/device/MiuiMyDeviceDetailSettings;

    invoke-static {v0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->-$$Nest$fgetmPresenter(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)Lcom/android/settings/device/DeviceBasicInfoPresenter;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$1;->this$0:Lcom/android/settings/device/MiuiMyDeviceDetailSettings;

    invoke-static {v0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->-$$Nest$fgetmGridView(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    if-eq v0, v1, :cond_1

    goto :goto_0

    :cond_1
    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$1;->this$0:Lcom/android/settings/device/MiuiMyDeviceDetailSettings;

    invoke-static {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->-$$Nest$fgetmPresenter(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)Lcom/android/settings/device/DeviceBasicInfoPresenter;

    move-result-object p0

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/settings/device/DeviceBasicInfoPresenter;->updateCameraInfo(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object p1

    const-string/jumbo v0, "result"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "needUpdateCpu"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    iget-object v2, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$1;->this$0:Lcom/android/settings/device/MiuiMyDeviceDetailSettings;

    invoke-static {v2}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->-$$Nest$fgetmPresenter(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)Lcom/android/settings/device/DeviceBasicInfoPresenter;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$1;->this$0:Lcom/android/settings/device/MiuiMyDeviceDetailSettings;

    invoke-static {v3}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->-$$Nest$fgetmGridView(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)Landroid/view/View;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$1;->this$0:Lcom/android/settings/device/MiuiMyDeviceDetailSettings;

    invoke-static {v4}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->-$$Nest$fgetmClickListener(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v2, v3, v0, v1, v4}, Lcom/android/settings/device/DeviceBasicInfoPresenter;->showBasicInfoGridView(Landroid/view/View;Ljava/lang/String;ZLandroid/view/View$OnClickListener;)V

    if-eqz p1, :cond_3

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$1;->this$0:Lcom/android/settings/device/MiuiMyDeviceDetailSettings;

    invoke-static {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->-$$Nest$mstartUpdateInfoAsync(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)V

    :cond_3
    :goto_0
    return-void

    :cond_4
    :goto_1
    const-string p0, "MiuiMyDeviceDetail"

    const-string p1, "Presenter or RootView is null"

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
