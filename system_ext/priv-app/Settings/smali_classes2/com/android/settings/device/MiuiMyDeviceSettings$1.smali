.class Lcom/android/settings/device/MiuiMyDeviceSettings$1;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/device/MiuiMyDeviceSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/device/MiuiMyDeviceSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/device/MiuiMyDeviceSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings$1;->this$0:Lcom/android/settings/device/MiuiMyDeviceSettings;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings$1;->this$0:Lcom/android/settings/device/MiuiMyDeviceSettings;

    invoke-static {v0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->-$$Nest$fgetmDeviceBasicInfoPresenter(Lcom/android/settings/device/MiuiMyDeviceSettings;)Lcom/android/settings/device/DeviceBasicInfoPresenter;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings$1;->this$0:Lcom/android/settings/device/MiuiMyDeviceSettings;

    invoke-static {v0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->-$$Nest$fgetmGridViewRoot(Lcom/android/settings/device/MiuiMyDeviceSettings;)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings$1;->this$0:Lcom/android/settings/device/MiuiMyDeviceSettings;

    invoke-static {v0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->-$$Nest$fgetmDeviceBasicInfoPresenter(Lcom/android/settings/device/MiuiMyDeviceSettings;)Lcom/android/settings/device/DeviceBasicInfoPresenter;

    move-result-object v0

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/android/settings/device/DeviceBasicInfoPresenter;->updateCameraInfo(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings$1;->this$0:Lcom/android/settings/device/MiuiMyDeviceSettings;

    invoke-static {v0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->-$$Nest$fgetmDeviceBasicInfoPresenter(Lcom/android/settings/device/MiuiMyDeviceSettings;)Lcom/android/settings/device/DeviceBasicInfoPresenter;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings$1;->this$0:Lcom/android/settings/device/MiuiMyDeviceSettings;

    invoke-static {v1}, Lcom/android/settings/device/MiuiMyDeviceSettings;->-$$Nest$fgetmGridViewRoot(Lcom/android/settings/device/MiuiMyDeviceSettings;)Landroid/view/View;

    move-result-object v1

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/android/settings/device/DeviceBasicInfoPresenter;->showBasicInfoGridView(Landroid/view/View;Ljava/lang/String;ZLandroid/view/View$OnClickListener;)V

    :goto_0
    iget-object p1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings$1;->this$0:Lcom/android/settings/device/MiuiMyDeviceSettings;

    invoke-static {p1}, Lcom/android/settings/device/MiuiMyDeviceSettings;->-$$Nest$fgetmGridViewRoot(Lcom/android/settings/device/MiuiMyDeviceSettings;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    if-nez p1, :cond_3

    iget-object p1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings$1;->this$0:Lcom/android/settings/device/MiuiMyDeviceSettings;

    const-string v0, "device_more_parameter"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    if-eqz p1, :cond_3

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings$1;->this$0:Lcom/android/settings/device/MiuiMyDeviceSettings;

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_3
    return-void

    :cond_4
    :goto_1
    const-string p0, "MiuiMyDeviceSettings"

    const-string p1, "Presenter or RootView is null"

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
