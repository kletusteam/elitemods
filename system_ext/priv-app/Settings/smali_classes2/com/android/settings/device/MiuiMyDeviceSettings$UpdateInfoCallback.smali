.class Lcom/android/settings/device/MiuiMyDeviceSettings$UpdateInfoCallback;
.super Lcom/android/settings/aidl/IRequestCallback$Stub;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/device/MiuiMyDeviceSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UpdateInfoCallback"
.end annotation


# instance fields
.field private mFragmentRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/android/settings/device/MiuiMyDeviceSettings;",
            ">;"
        }
    .end annotation
.end field

.field private mIsInitCameraUseModel:Z

.field private mIsInitDeviceUseModel:Z


# direct methods
.method public constructor <init>(Lcom/android/settings/device/MiuiMyDeviceSettings;)V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/aidl/IRequestCallback$Stub;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings$UpdateInfoCallback;->mIsInitDeviceUseModel:Z

    iput-boolean v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings$UpdateInfoCallback;->mIsInitCameraUseModel:Z

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings$UpdateInfoCallback;->mFragmentRef:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public onRequestComplete(ILjava/lang/String;)V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceSettings$UpdateInfoCallback;->mFragmentRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/device/MiuiMyDeviceSettings;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->getInitHelper()Lcom/android/settings/device/DeviceParamsInitHelper;

    move-result-object v2

    invoke-static {v0}, Lcom/android/settings/device/MiuiMyDeviceSettings;->-$$Nest$mgetHandler(Lcom/android/settings/device/MiuiMyDeviceSettings;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v2, :cond_6

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz p1, :cond_3

    if-eq p1, v3, :cond_1

    goto :goto_2

    :cond_1
    iput-object p2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iput v3, v1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_2

    invoke-static {p2}, Lcom/android/settings/device/ParseMiShopDataUtils;->getDataSuccess(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_7

    :cond_2
    iget-boolean p1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings$UpdateInfoCallback;->mIsInitCameraUseModel:Z

    if-eqz p1, :cond_7

    iput-boolean v4, p0, Lcom/android/settings/device/MiuiMyDeviceSettings$UpdateInfoCallback;->mIsInitCameraUseModel:Z

    invoke-virtual {v2, v4}, Lcom/android/settings/device/DeviceParamsInitHelper;->initDeviceParams(Z)V

    goto :goto_2

    :cond_3
    iput-object p2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iput v4, v1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_5

    invoke-static {p2}, Lcom/android/settings/device/ParseMiShopDataUtils;->showBasicItems(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_4

    goto :goto_0

    :cond_4
    invoke-virtual {v2, v3}, Lcom/android/settings/device/DeviceParamsInitHelper;->initCameraParams(Z)V

    goto :goto_2

    :cond_5
    :goto_0
    iget-boolean p1, p0, Lcom/android/settings/device/MiuiMyDeviceSettings$UpdateInfoCallback;->mIsInitDeviceUseModel:Z

    if-eqz p1, :cond_7

    iput-boolean v4, p0, Lcom/android/settings/device/MiuiMyDeviceSettings$UpdateInfoCallback;->mIsInitDeviceUseModel:Z

    invoke-virtual {v2, v4}, Lcom/android/settings/device/DeviceParamsInitHelper;->initDeviceParams(Z)V

    goto :goto_2

    :cond_6
    :goto_1
    const-string p0, "MiuiMyDeviceSettings"

    const-string p1, "deal response error"

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    :goto_2
    return-void
.end method
