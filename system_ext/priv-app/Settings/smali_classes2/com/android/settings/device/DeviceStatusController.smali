.class public Lcom/android/settings/device/DeviceStatusController;
.super Lcom/android/settings/BaseSettingsController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/device/DeviceStatusController$UpdateTask;
    }
.end annotation


# instance fields
.field private isLoad:Z

.field private mArrowRight:Landroid/widget/ImageView;

.field private mHolder:Lcom/android/settings/MiuiSettings$HeaderViewHolder;

.field private mRightValue:Landroid/widget/TextView;

.field private mUpdateTask:Lcom/android/settings/device/DeviceStatusController$UpdateTask;

.field private mVersionClick:Landroid/view/View$OnClickListener;

.field private queryResult:Ljava/lang/String;


# direct methods
.method public static synthetic $r8$lambda$UZk8gHniZdDmjsPe5B7HTr5-l6c(Lcom/android/settings/device/DeviceStatusController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/device/DeviceStatusController;->lambda$setUpTextView$1()V

    return-void
.end method

.method public static synthetic $r8$lambda$_ipLteuQq4qCKQrYXriY07eL_hU(Lcom/android/settings/device/DeviceStatusController;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/device/DeviceStatusController;->lambda$setUpTextView$0(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateResult(Lcom/android/settings/device/DeviceStatusController;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/device/DeviceStatusController;->updateResult(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/TextView;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/BaseSettingsController;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/android/settings/device/DeviceStatusController;->queryResult:Ljava/lang/String;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/settings/device/DeviceStatusController;->isLoad:Z

    new-instance p1, Lcom/android/settings/device/DeviceStatusController$1;

    invoke-direct {p1, p0}, Lcom/android/settings/device/DeviceStatusController$1;-><init>(Lcom/android/settings/device/DeviceStatusController;)V

    iput-object p1, p0, Lcom/android/settings/device/DeviceStatusController;->mVersionClick:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/device/DeviceStatusController;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/BaseSettingsController;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$100(Lcom/android/settings/device/DeviceStatusController;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/BaseSettingsController;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$200(Lcom/android/settings/device/DeviceStatusController;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/BaseSettingsController;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method private isShowVerticalSummary()Z
    .locals 0

    iget-object p0, p0, Lcom/android/settings/BaseSettingsController;->mContext:Landroid/content/Context;

    invoke-static {p0}, Lcom/android/settings/MiuiSettings;->isDeviceAdapterVerticalSummary(Landroid/content/Context;)Z

    move-result p0

    return p0
.end method

.method private synthetic lambda$setUpTextView$0(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/device/DeviceStatusController;->updateResult(Ljava/lang/String;)V

    return-void
.end method

.method private synthetic lambda$setUpTextView$1()V
    .locals 3

    invoke-static {}, Landroid/app/AppGlobals;->getInitialApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/device/MiuiAboutPhoneUtils;->getUpdateInfo(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/BaseSettingsController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainThreadHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/android/settings/device/DeviceStatusController$$ExternalSyntheticLambda1;

    invoke-direct {v2, p0, v0}, Lcom/android/settings/device/DeviceStatusController$$ExternalSyntheticLambda1;-><init>(Lcom/android/settings/device/DeviceStatusController;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private updateResult(Ljava/lang/String;)V
    .locals 6

    iput-object p1, p0, Lcom/android/settings/device/DeviceStatusController;->queryResult:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/device/DeviceStatusController;->mRightValue:Landroid/widget/TextView;

    if-eqz v0, :cond_b

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object p1, v1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/device/DeviceStatusController;->mArrowRight:Landroid/widget/ImageView;

    const/4 v2, 0x1

    const/16 v3, 0x8

    const/4 v4, 0x0

    if-eqz v0, :cond_3

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v5, v4

    goto :goto_0

    :cond_1
    move v5, v3

    :goto_0
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->isSplitTabletDevice()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/device/DeviceStatusController;->mArrowRight:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/device/DeviceStatusController;->mRightValue:Landroid/widget/TextView;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    xor-int/2addr v5, v2

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setClickable(Z)V

    :cond_3
    sget-boolean v0, Lcom/android/settings/utils/TabletUtils;->IS_TABLET:Z

    if-eqz v0, :cond_4

    iget-object p0, p0, Lcom/android/settings/device/DeviceStatusController;->mRightValue:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_4
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_6

    invoke-direct {p0}, Lcom/android/settings/device/DeviceStatusController;->isShowVerticalSummary()Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/android/settings/device/DeviceStatusController;->mHolder:Lcom/android/settings/MiuiSettings$HeaderViewHolder;

    if-eqz p1, :cond_5

    iget-object p1, p1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->value:Landroid/widget/TextView;

    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object p1, p0, Lcom/android/settings/device/DeviceStatusController;->mHolder:Lcom/android/settings/MiuiSettings$HeaderViewHolder;

    iget-object p1, p1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->summary:Landroid/widget/TextView;

    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object p1, p0, Lcom/android/settings/device/DeviceStatusController;->mHolder:Lcom/android/settings/MiuiSettings$HeaderViewHolder;

    iget-object p1, p1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->value:Landroid/widget/TextView;

    iput-object p1, p0, Lcom/android/settings/device/DeviceStatusController;->mRightValue:Landroid/widget/TextView;

    :cond_5
    iget-object p1, p0, Lcom/android/settings/BaseSettingsController;->mContext:Landroid/content/Context;

    sget v0, Lcom/android/settings/R$string;->settings_new_version_btn:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/device/DeviceStatusController;->mRightValue:Landroid/widget/TextView;

    sget v1, Lcom/android/settings/R$drawable;->new_version_button:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/android/settings/device/DeviceStatusController;->mRightValue:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/BaseSettingsController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/android/settings/R$color;->new_version_text_color:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/android/settings/device/DeviceStatusController;->mRightValue:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/device/DeviceStatusController;->mVersionClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v0, "about_phone_pv"

    invoke-static {v0}, Lcom/android/settings/report/InternationalCompat;->trackReportEvent(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/settings/MiuiUtils;->isMiuiSdkSupportFolme()Z

    move-result v0

    if-eqz v0, :cond_9

    new-array v0, v2, [Landroid/view/View;

    iget-object v1, p0, Lcom/android/settings/device/DeviceStatusController;->mRightValue:Landroid/widget/TextView;

    aput-object v1, v0, v4

    invoke-static {v0}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/IFolme;->touch()Lmiuix/animation/ITouchStyle;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/device/DeviceStatusController;->mRightValue:Landroid/widget/TextView;

    new-array v2, v4, [Lmiuix/animation/base/AnimConfig;

    invoke-interface {v0, v1, v2}, Lmiuix/animation/ITouchStyle;->handleTouchOf(Landroid/view/View;[Lmiuix/animation/base/AnimConfig;)V

    goto :goto_3

    :cond_6
    iget-object p1, p0, Lcom/android/settings/BaseSettingsController;->mContext:Landroid/content/Context;

    invoke-static {p1, v4}, Lcom/android/settings/device/MiuiAboutPhoneUtils;->getMiuiVersionWithoutBuildType(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/device/DeviceStatusController;->mRightValue:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    invoke-direct {p0}, Lcom/android/settings/device/DeviceStatusController;->isShowVerticalSummary()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v4

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/android/settings/BaseSettingsController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/settings/R$dimen;->preference_value_padding_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    :goto_1
    invoke-direct {p0}, Lcom/android/settings/device/DeviceStatusController;->isShowVerticalSummary()Z

    move-result v1

    if-eqz v1, :cond_8

    move v1, v4

    goto :goto_2

    :cond_8
    iget-object v1, p0, Lcom/android/settings/BaseSettingsController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/android/settings/R$dimen;->preference_value_padding_bottom:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    :goto_2
    iget-object v3, p0, Lcom/android/settings/device/DeviceStatusController;->mRightValue:Landroid/widget/TextView;

    invoke-virtual {v3, v4, v0, v4, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/android/settings/device/DeviceStatusController;->mRightValue:Landroid/widget/TextView;

    sget v1, Lcom/android/settings/R$style;->Miuix_AppCompat_TextAppearance_PreferenceRight:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextAppearance(I)V

    invoke-static {}, Lcom/android/settings/MiuiUtils;->isMiuiSdkSupportFolme()Z

    move-result v0

    if-eqz v0, :cond_9

    new-array v0, v2, [Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/device/DeviceStatusController;->mRightValue:Landroid/widget/TextView;

    aput-object v1, v0, v4

    invoke-static {v0}, Lmiuix/animation/Folme;->clean([Ljava/lang/Object;)V

    :cond_9
    :goto_3
    iget-object v0, p0, Lcom/android/settings/device/DeviceStatusController;->mRightValue:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/android/settings/device/DeviceStatusController;->mRightValue:Landroid/widget/TextView;

    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/settings/device/DeviceStatusController;->isShowVerticalSummary()Z

    move-result p1

    if-eqz p1, :cond_a

    iget-object p0, p0, Lcom/android/settings/device/DeviceStatusController;->mRightValue:Landroid/widget/TextView;

    const/16 p1, 0x10

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_4

    :cond_a
    iget-object p0, p0, Lcom/android/settings/device/DeviceStatusController;->mRightValue:Landroid/widget/TextView;

    const p1, 0x800015

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setGravity(I)V

    :cond_b
    :goto_4
    return-void
.end method


# virtual methods
.method public pause()V
    .locals 1

    iget-object p0, p0, Lcom/android/settings/device/DeviceStatusController;->mUpdateTask:Lcom/android/settings/device/DeviceStatusController$UpdateTask;

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_0
    return-void
.end method

.method public resume()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/device/DeviceStatusController;->updateState()V

    return-void
.end method

.method public setUpTextView(Landroid/widget/TextView;Landroid/widget/ImageView;Lcom/android/settings/MiuiSettings$HeaderViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/device/DeviceStatusController;->mRightValue:Landroid/widget/TextView;

    iput-object p2, p0, Lcom/android/settings/device/DeviceStatusController;->mArrowRight:Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/android/settings/device/DeviceStatusController;->mHolder:Lcom/android/settings/MiuiSettings$HeaderViewHolder;

    invoke-virtual {p0}, Lcom/android/settings/device/DeviceStatusController;->updateState()V

    iget-object p1, p0, Lcom/android/settings/device/DeviceStatusController;->queryResult:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-boolean p1, p0, Lcom/android/settings/device/DeviceStatusController;->isLoad:Z

    if-nez p1, :cond_0

    new-instance p1, Ljava/lang/Thread;

    new-instance p2, Lcom/android/settings/device/DeviceStatusController$$ExternalSyntheticLambda0;

    invoke-direct {p2, p0}, Lcom/android/settings/device/DeviceStatusController$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/device/DeviceStatusController;)V

    invoke-direct {p1, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/android/settings/device/DeviceStatusController;->isLoad:Z

    :cond_0
    return-void
.end method

.method public updateState()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/device/DeviceStatusController;->mUpdateTask:Lcom/android/settings/device/DeviceStatusController$UpdateTask;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/device/DeviceStatusController;->mUpdateTask:Lcom/android/settings/device/DeviceStatusController$UpdateTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_0
    new-instance v0, Lcom/android/settings/device/DeviceStatusController$UpdateTask;

    invoke-direct {v0, p0}, Lcom/android/settings/device/DeviceStatusController$UpdateTask;-><init>(Lcom/android/settings/device/DeviceStatusController;)V

    iput-object v0, p0, Lcom/android/settings/device/DeviceStatusController;->mUpdateTask:Lcom/android/settings/device/DeviceStatusController$UpdateTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v0, p0, Lcom/android/settings/device/DeviceStatusController;->queryResult:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/device/DeviceStatusController;->queryResult:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/settings/device/DeviceStatusController;->updateResult(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method protected updateStatus()V
    .locals 0

    return-void
.end method
