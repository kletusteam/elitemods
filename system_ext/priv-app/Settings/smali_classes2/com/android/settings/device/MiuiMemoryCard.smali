.class public Lcom/android/settings/device/MiuiMemoryCard;
.super Landroid/widget/FrameLayout;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/device/MiuiMemoryCard$MemoryInfoCallback;
    }
.end annotation


# instance fields
.field private calculatingView:Landroid/view/View;

.field private mCallback:Lcom/android/settings/device/MemoryInfoHelper$Callback;

.field private mFragment:Lcom/android/settings/dashboard/DashboardFragment;

.field private mView:Landroid/view/View;

.field private progressCardView:Lmiuix/waterbox/WaterBox;

.field private storageView:Landroid/view/View;

.field private totalText:Landroid/widget/TextView;

.field private usedText:Landroid/widget/TextView;


# direct methods
.method static bridge synthetic -$$Nest$fgetcalculatingView(Lcom/android/settings/device/MiuiMemoryCard;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMemoryCard;->calculatingView:Landroid/view/View;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmView(Lcom/android/settings/device/MiuiMemoryCard;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMemoryCard;->mView:Landroid/view/View;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetstorageView(Lcom/android/settings/device/MiuiMemoryCard;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMemoryCard;->storageView:Landroid/view/View;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgettotalText(Lcom/android/settings/device/MiuiMemoryCard;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMemoryCard;->totalText:Landroid/widget/TextView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetusedText(Lcom/android/settings/device/MiuiMemoryCard;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMemoryCard;->usedText:Landroid/widget/TextView;

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/android/settings/device/MiuiMemoryCard;->initView()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/android/settings/device/MiuiMemoryCard;->initView()V

    return-void
.end method

.method private initView()V
    .locals 4

    invoke-static {}, Lcom/android/settings/MiuiUtils;->isTablet()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/widget/FrameLayout;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lcom/android/settings/R$layout;->miui_pad_memory_card_layout:I

    invoke-virtual {v0, v2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/device/MiuiMemoryCard;->mView:Landroid/view/View;

    goto :goto_2

    :cond_0
    iget-object v0, p0, Landroid/widget/FrameLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    const/4 v2, 0x3

    if-lt v0, v2, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    sget-object v2, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    const-string v3, "cetus"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-nez v0, :cond_2

    iget-object v0, p0, Landroid/widget/FrameLayout;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lcom/android/settings/R$layout;->miui_memory_card_out_layout:I

    invoke-virtual {v0, v2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Landroid/widget/FrameLayout;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lcom/android/settings/R$layout;->miui_memory_card_layout:I

    invoke-virtual {v0, v2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    :goto_1
    sget v0, Lcom/android/settings/R$id;->water_box_view:I

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/waterbox/WaterBox;

    iput-object v0, p0, Lcom/android/settings/device/MiuiMemoryCard;->progressCardView:Lmiuix/waterbox/WaterBox;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$color;->progress_paint_color:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lmiuix/waterbox/WaterBox;->setColor(I)V

    iget-object v0, p0, Lcom/android/settings/device/MiuiMemoryCard;->progressCardView:Lmiuix/waterbox/WaterBox;

    const/high16 v1, 0x42400000    # 48.0f

    invoke-virtual {v0, v1}, Lmiuix/waterbox/WaterBox;->setCornerRadius(F)V

    :goto_2
    invoke-virtual {p0, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/android/settings/R$id;->total_storage:I

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/device/MiuiMemoryCard;->totalText:Landroid/widget/TextView;

    sget v0, Lcom/android/settings/R$id;->used_storage:I

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/device/MiuiMemoryCard;->usedText:Landroid/widget/TextView;

    sget v0, Lcom/android/settings/R$id;->calculating_view:I

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/device/MiuiMemoryCard;->calculatingView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->storage_view:I

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/device/MiuiMemoryCard;->storageView:Landroid/view/View;

    new-instance v0, Lcom/android/settings/device/MiuiMemoryCard$MemoryInfoCallback;

    invoke-direct {v0, p0}, Lcom/android/settings/device/MiuiMemoryCard$MemoryInfoCallback;-><init>(Lcom/android/settings/device/MiuiMemoryCard;)V

    iput-object v0, p0, Lcom/android/settings/device/MiuiMemoryCard;->mCallback:Lcom/android/settings/device/MemoryInfoHelper$Callback;

    invoke-static {v0}, Lcom/android/settings/device/MemoryInfoHelper;->getAvailableMemorySize(Lcom/android/settings/device/MemoryInfoHelper$Callback;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    new-instance p1, Landroid/content/Intent;

    const-string v0, "com.miui.cleanmaster.action.STORAGE_MANAGE"

    invoke-direct {p1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v0, "key_channel"

    const-string/jumbo v1, "miui_settings"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object p0, p0, Lcom/android/settings/device/MiuiMemoryCard;->mFragment:Lcom/android/settings/dashboard/DashboardFragment;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    const-string/jumbo p0, "setting_About_phone_storage"

    invoke-static {p0}, Lcom/android/settings/report/InternationalCompat;->trackReportEvent(Ljava/lang/String;)V

    return-void
.end method

.method public setFragment(Lcom/android/settings/dashboard/DashboardFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/device/MiuiMemoryCard;->mFragment:Lcom/android/settings/dashboard/DashboardFragment;

    return-void
.end method

.method public setPercent(F)V
    .locals 1

    invoke-static {}, Lcom/android/settings/MiuiUtils;->isTablet()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMemoryCard;->progressCardView:Lmiuix/waterbox/WaterBox;

    invoke-virtual {p0, p1}, Lmiuix/waterbox/WaterBox;->setValue(F)V

    :cond_0
    return-void
.end method
