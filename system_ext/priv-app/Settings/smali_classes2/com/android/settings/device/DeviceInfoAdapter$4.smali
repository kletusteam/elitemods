.class Lcom/android/settings/device/DeviceInfoAdapter$4;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/device/DeviceInfoAdapter;->updateCardHeight(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/device/DeviceInfoAdapter;

.field final synthetic val$boardLayout:Landroid/widget/RelativeLayout;

.field final synthetic val$cards:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/android/settings/device/DeviceInfoAdapter;Landroid/widget/RelativeLayout;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/device/DeviceInfoAdapter$4;->this$0:Lcom/android/settings/device/DeviceInfoAdapter;

    iput-object p2, p0, Lcom/android/settings/device/DeviceInfoAdapter$4;->val$boardLayout:Landroid/widget/RelativeLayout;

    iput-object p3, p0, Lcom/android/settings/device/DeviceInfoAdapter$4;->val$cards:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/device/DeviceInfoAdapter$4;->val$boardLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/device/DeviceInfoAdapter$4;->val$boardLayout:Landroid/widget/RelativeLayout;

    sget v2, Lcom/android/settings/R$id;->card_value_layout:I

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/device/DeviceInfoAdapter$4;->val$boardLayout:Landroid/widget/RelativeLayout;

    sget v3, Lcom/android/settings/R$id;->card_value:I

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    const/16 v3, 0x50

    if-ge v1, v3, :cond_1

    add-int/2addr v0, v3

    sub-int/2addr v0, v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/device/DeviceInfoAdapter$4;->val$boardLayout:Landroid/widget/RelativeLayout;

    sget v1, Lcom/android/settings/R$id;->card_title:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int/2addr v0, v2

    iget-object v1, p0, Lcom/android/settings/device/DeviceInfoAdapter$4;->val$boardLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/android/settings/device/DeviceInfoAdapter$4;->val$boardLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v0, v3

    :cond_0
    iget-object v1, p0, Lcom/android/settings/device/DeviceInfoAdapter$4;->val$cards:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/device/BorderedBaseDeviceCardItem;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getPaddingTop()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v5

    add-int/2addr v4, v5

    iput v4, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/device/DeviceInfoAdapter$4;->val$boardLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void
.end method
