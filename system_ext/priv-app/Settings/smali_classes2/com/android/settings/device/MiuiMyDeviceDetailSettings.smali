.class public Lcom/android/settings/device/MiuiMyDeviceDetailSettings;
.super Lcom/android/settings/dashboard/DashboardFragment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/device/MiuiMyDeviceDetailSettings$ReadCpuInfoTask;,
        Lcom/android/settings/device/MiuiMyDeviceDetailSettings$UpdateMemoryCallBack;,
        Lcom/android/settings/device/MiuiMyDeviceDetailSettings$UpdateCpuCallBack;,
        Lcom/android/settings/device/MiuiMyDeviceDetailSettings$UpdateInfoCallback;,
        Lcom/android/settings/device/MiuiMyDeviceDetailSettings$VerisonSpaceItemDecoration;,
        Lcom/android/settings/device/MiuiMyDeviceDetailSettings$SpaceItemDecoration;,
        Lcom/android/settings/device/MiuiMyDeviceDetailSettings$RemoteServiceConn;
    }
.end annotation


# static fields
.field private static final CPU_CLOUD_CONFIG_KEY:Ljava/lang/String;


# instance fields
.field private mCallback:Lcom/android/settings/device/MemoryInfoHelper$Callback;

.field private mClickListener:Landroid/view/View$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private mDeviceInfoCallback:Lcom/android/settings/device/MiuiMyDeviceDetailSettings$UpdateInfoCallback;

.field private mGridView:Landroid/view/View;

.field private mHandler:Landroid/os/Handler;

.field private mHardwareList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/device/DeviceCardInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mHelper:Lcom/android/settings/device/DeviceParamsInitHelper;

.field private mIsNeedAddBasicCpu:Z

.field private mIsNeedUpdateCpu:Z

.field private mMemoryCardItem:Lcom/android/settings/device/BorderedBaseDeviceCardItem;

.field private mPresenter:Lcom/android/settings/device/DeviceBasicInfoPresenter;

.field private mRemoteService:Lcom/android/settings/aidl/IRemoteGetDeviceInfoService;

.field private mRemoteServiceConn:Lcom/android/settings/device/MiuiMyDeviceDetailSettings$RemoteServiceConn;

.field private mRootView:Landroid/view/View;

.field private mVersionlist:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/device/DeviceCardInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetmClickListener(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)Landroid/view/View$OnClickListener;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mClickListener:Landroid/view/View$OnClickListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDeviceInfoCallback(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)Lcom/android/settings/device/MiuiMyDeviceDetailSettings$UpdateInfoCallback;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mDeviceInfoCallback:Lcom/android/settings/device/MiuiMyDeviceDetailSettings$UpdateInfoCallback;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmGridView(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mGridView:Landroid/view/View;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHelper(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)Lcom/android/settings/device/DeviceParamsInitHelper;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mHelper:Lcom/android/settings/device/DeviceParamsInitHelper;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsNeedAddBasicCpu(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mIsNeedAddBasicCpu:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsNeedUpdateCpu(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mIsNeedUpdateCpu:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmMemoryCardItem(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)Lcom/android/settings/device/BorderedBaseDeviceCardItem;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mMemoryCardItem:Lcom/android/settings/device/BorderedBaseDeviceCardItem;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPresenter(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)Lcom/android/settings/device/DeviceBasicInfoPresenter;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mPresenter:Lcom/android/settings/device/DeviceBasicInfoPresenter;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmRemoteService(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)Lcom/android/settings/aidl/IRemoteGetDeviceInfoService;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mRemoteService:Lcom/android/settings/aidl/IRemoteGetDeviceInfoService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmVersionlist(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mVersionlist:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmHelper(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;Lcom/android/settings/device/DeviceParamsInitHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mHelper:Lcom/android/settings/device/DeviceParamsInitHelper;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsNeedAddBasicCpu(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mIsNeedAddBasicCpu:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsNeedUpdateCpu(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mIsNeedUpdateCpu:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmRemoteService(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;Lcom/android/settings/aidl/IRemoteGetDeviceInfoService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mRemoteService:Lcom/android/settings/aidl/IRemoteGetDeviceInfoService;

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetHandler(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)Landroid/os/Handler;
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->getHandler()Landroid/os/Handler;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mstartUpdateInfoAsync(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->startUpdateInfoAsync()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetCPU_CLOUD_CONFIG_KEY()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->CPU_CLOUD_CONFIG_KEY:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    sput-object v0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->CPU_CLOUD_CONFIG_KEY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/dashboard/DashboardFragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mIsNeedUpdateCpu:Z

    iput-boolean v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mIsNeedAddBasicCpu:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mVersionlist:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mHardwareList:Ljava/util/List;

    new-instance v0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$1;

    invoke-direct {v0, p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$1;-><init>(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)V

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private disableRecyclerViewScrollDispatch()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mRootView:Landroid/view/View;

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget v1, Lcom/android/settings/R$id;->description_grid:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mRootView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->verison_info:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView;

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mRootView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->hardware_info_list:I

    invoke-virtual {p0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroidx/recyclerview/widget/RecyclerView;

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    :cond_2
    if-eqz p0, :cond_3

    invoke-virtual {p0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    :cond_3
    return-void
.end method

.method private getHandler()Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method private initGridView()V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mRootView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->device_params:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mGridView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    sget-boolean v0, Lmiui/os/Build;->IS_ALPHA_BUILD:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/settings/device/DeviceBasicInfoPresenter;

    iget-object v1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/settings/device/DeviceBasicInfoPresenter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mPresenter:Lcom/android/settings/device/DeviceBasicInfoPresenter;

    iget-object v1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mGridView:Landroid/view/View;

    iget-object v2, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mClickListener:Landroid/view/View$OnClickListener;

    const-string v3, ""

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v3, v4, v2}, Lcom/android/settings/device/DeviceBasicInfoPresenter;->showBasicInfoGridView(Landroid/view/View;Ljava/lang/String;ZLandroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->startUpdateInfoAsync()V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, ":settings:show_fragment_args"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    const-string v2, "cards_data"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_1

    array-length v2, v0

    const-class v3, [Lcom/android/settings/device/DeviceCardInfo;

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;ILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/settings/device/DeviceCardInfo;

    goto :goto_0

    :cond_1
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_3

    array-length v2, v0

    if-lez v2, :cond_3

    array-length v1, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v1, :cond_2

    aget-object v3, v0, v2

    iget-object v4, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Lcom/android/settings/device/DeviceCardInfo;->setListener(Landroid/view/View$OnClickListener;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    new-instance v1, Lcom/android/settings/device/DeviceBasicInfoPresenter;

    iget-object v2, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/android/settings/device/DeviceBasicInfoPresenter;-><init>(Landroid/content/Context;[Lcom/android/settings/device/DeviceCardInfo;)V

    iput-object v1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mPresenter:Lcom/android/settings/device/DeviceBasicInfoPresenter;

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mGridView:Landroid/view/View;

    invoke-virtual {v1, v0}, Lcom/android/settings/device/DeviceBasicInfoPresenter;->showBasicInfoGridView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->startUpdateInfoAsync()V

    goto :goto_2

    :cond_3
    new-instance v0, Lcom/android/settings/device/DeviceBasicInfoPresenter;

    iget-object v2, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/android/settings/device/DeviceBasicInfoPresenter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mPresenter:Lcom/android/settings/device/DeviceBasicInfoPresenter;

    new-instance v0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$UpdateInfoCallback;

    invoke-direct {v0, p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$UpdateInfoCallback;-><init>(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)V

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mDeviceInfoCallback:Lcom/android/settings/device/MiuiMyDeviceDetailSettings$UpdateInfoCallback;

    new-instance v0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$RemoteServiceConn;

    invoke-direct {v0, p0, v1}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$RemoteServiceConn;-><init>(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;Lcom/android/settings/device/MiuiMyDeviceDetailSettings$RemoteServiceConn-IA;)V

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mRemoteServiceConn:Lcom/android/settings/device/MiuiMyDeviceDetailSettings$RemoteServiceConn;

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    invoke-static {p0, v0}, Lcom/android/settings/device/RemoteServiceUtil;->bindRemoteService(Landroid/content/Context;Landroid/content/ServiceConnection;)Z

    :goto_2
    return-void
.end method

.method private initHardWareVersion()V
    .locals 6

    sget-boolean v0, Lmiui/os/Build;->IS_ALPHA_BUILD:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/android/settings/device/DeviceCardInfo;

    invoke-direct {v0}, Lcom/android/settings/device/DeviceCardInfo;-><init>()V

    iget-object v3, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/android/settings/R$string;->model_name:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/android/settings/device/DeviceCardInfo;->setTitle(Ljava/lang/String;)V

    sget-object v3, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    const-string v4, "diting"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/android/settings/credentials/MiuiCredentialsUpdater;->getGlobalCertNumber()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/android/settings/credentials/MiuiCredentialsUpdater;->getGlobalCertNumber()Ljava/lang/String;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    const/4 v5, 0x2

    if-ne v3, v5, :cond_0

    invoke-static {}, Lcom/android/settings/credentials/MiuiCredentialsUpdater;->getGlobalCertNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v1

    const-string v5, "art"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/android/settings/credentials/MiuiCredentialsUpdater;->getGlobalCertNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v2

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/android/settings/device/DeviceCardInfo;->setValue(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/android/settings/credentials/MiuiCredentialsUpdater;->getGlobalCertNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/android/settings/device/DeviceCardInfo;->setValue(Ljava/lang/String;)V

    :goto_0
    iget-object v3, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mHardwareList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-static {}, Lcom/android/settings/device/MiuiAboutPhoneUtils;->getCTANumble()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Lcom/android/settings/device/DeviceCardInfo;

    invoke-direct {v0}, Lcom/android/settings/device/DeviceCardInfo;-><init>()V

    iget-object v3, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/android/settings/R$string;->model_name:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/android/settings/device/DeviceCardInfo;->setTitle(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/settings/device/MiuiAboutPhoneUtils;->getCTANumble()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/android/settings/device/DeviceCardInfo;->setValue(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mHardwareList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settingslib/Utils;->isWifiOnly(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Lcom/android/settings/device/DeviceCardInfo;

    invoke-direct {v0}, Lcom/android/settings/device/DeviceCardInfo;-><init>()V

    iget-object v3, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/android/settings/R$string;->device_info_default:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "gsm.version.baseband"

    invoke-static {v4, v2, v3}, Lcom/android/settings/PlatformUtils;->getTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/android/settings/device/DeviceCardInfo;->setValue(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/android/settings/R$string;->baseband_version:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/android/settings/device/DeviceCardInfo;->setTitle(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mHardwareList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    new-instance v0, Lcom/android/settings/device/DeviceCardInfo;

    invoke-direct {v0}, Lcom/android/settings/device/DeviceCardInfo;-><init>()V

    invoke-static {}, Lcom/android/settings/device/MiuiAboutPhoneUtils;->getFormattedKernelVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/android/settings/device/DeviceCardInfo;->setValue(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/android/settings/R$string;->kernel_version:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/android/settings/device/DeviceCardInfo;->setTitle(Ljava/lang/String;)V

    const-string v3, "kernel_version"

    invoke-virtual {v0, v3}, Lcom/android/settings/device/DeviceCardInfo;->setKey(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mHardwareList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string/jumbo v0, "ro.miui.cust_hardware"

    const-string v3, ""

    invoke-static {v0, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    new-instance v3, Lcom/android/settings/device/DeviceCardInfo;

    invoke-direct {v3}, Lcom/android/settings/device/DeviceCardInfo;-><init>()V

    iget-object v4, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/android/settings/R$string;->hardware_version:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/settings/device/DeviceCardInfo;->setTitle(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Lcom/android/settings/device/DeviceCardInfo;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mHardwareList:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mHardwareList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/settings/device/DeviceCardInfo;

    iget-object v4, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Lcom/android/settings/device/DeviceCardInfo;->setListener(Landroid/view/View$OnClickListener;)V

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/android/settings/device/DeviceCardInfo;->setType(I)V

    goto :goto_2

    :cond_5
    new-instance v0, Lcom/android/settings/device/DeviceInfoAdapter;

    iget-object v3, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/android/settings/device/DeviceInfoAdapter;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/android/settings/device/DeviceInfoAdapter;->closeValueTextLineLimit()V

    invoke-virtual {v0, v1}, Lcom/android/settings/device/DeviceInfoAdapter;->setType(I)V

    iget-object v3, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mRootView:Landroid/view/View;

    sget v4, Lcom/android/settings/R$id;->hardware_info_list:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroidx/recyclerview/widget/RecyclerView;

    new-instance v4, Landroidx/recyclerview/widget/LinearLayoutManager;

    iget-object v5, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->setOrientation(I)V

    iget-object v1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mHardwareList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Lcom/android/settings/device/DeviceCardInfo;

    invoke-interface {v1, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/android/settings/device/DeviceCardInfo;

    invoke-virtual {v0, v1}, Lcom/android/settings/device/DeviceInfoAdapter;->setDataList([Lcom/android/settings/device/DeviceCardInfo;)V

    invoke-virtual {v3, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    invoke-virtual {v3, v4}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    invoke-virtual {v3}, Landroidx/recyclerview/widget/RecyclerView;->getItemDecorationCount()I

    move-result v0

    if-lez v0, :cond_6

    invoke-virtual {v3, v2}, Landroidx/recyclerview/widget/RecyclerView;->getItemDecorationAt(I)Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    move-result-object v0

    if-nez v0, :cond_7

    :cond_6
    new-instance v0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$SpaceItemDecoration;

    invoke-direct {v0, p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$SpaceItemDecoration;-><init>(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)V

    invoke-virtual {v3, v0}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    :cond_7
    return-void
.end method

.method private initMemoryInfo()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mRootView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->memory:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/device/BorderedBaseDeviceCardItem;

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mMemoryCardItem:Lcom/android/settings/device/BorderedBaseDeviceCardItem;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$string;->device_total_memory:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/settings/device/MiuiAboutPhoneUtils;->getInstance(Landroid/content/Context;)Lcom/android/settings/device/MiuiAboutPhoneUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/settings/device/MiuiAboutPhoneUtils;->getTotalMemory()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mMemoryCardItem:Lcom/android/settings/device/BorderedBaseDeviceCardItem;

    invoke-virtual {v1, v0}, Lcom/android/settings/device/BaseDeviceCardItem;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mMemoryCardItem:Lcom/android/settings/device/BorderedBaseDeviceCardItem;

    iget-object v1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$string;->device_internal_memory:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/device/BaseDeviceCardItem;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mMemoryCardItem:Lcom/android/settings/device/BorderedBaseDeviceCardItem;

    const-string v1, "device_internal_memory"

    invoke-virtual {v0, v1}, Lcom/android/settings/device/BaseDeviceCardItem;->setKey(Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mMemoryCardItem:Lcom/android/settings/device/BorderedBaseDeviceCardItem;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/IFolme;->touch()Lmiuix/animation/ITouchStyle;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mMemoryCardItem:Lcom/android/settings/device/BorderedBaseDeviceCardItem;

    new-array v2, v2, [Lmiuix/animation/base/AnimConfig;

    invoke-interface {v0, v1, v2}, Lmiuix/animation/ITouchStyle;->handleTouchOf(Landroid/view/View;[Lmiuix/animation/base/AnimConfig;)V

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mMemoryCardItem:Lcom/android/settings/device/BorderedBaseDeviceCardItem;

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private initSoftWareVersion()V
    .locals 8

    new-instance v0, Lcom/android/settings/device/DeviceCardInfo;

    invoke-direct {v0}, Lcom/android/settings/device/DeviceCardInfo;-><init>()V

    new-instance v1, Lcom/android/settings/device/DeviceCardInfo;

    invoke-direct {v1}, Lcom/android/settings/device/DeviceCardInfo;-><init>()V

    new-instance v2, Lcom/android/settings/device/DeviceCardInfo;

    invoke-direct {v2}, Lcom/android/settings/device/DeviceCardInfo;-><init>()V

    sget-boolean v3, Lmiui/os/Build;->IS_CU_CUSTOMIZATION_TEST:Z

    if-nez v3, :cond_1

    sget-boolean v3, Lmiui/os/Build;->IS_ALPHA_BUILD:Z

    if-nez v3, :cond_0

    goto :goto_0

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v4, Lmiui/os/Build;->ID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/android/settings/device/DeviceCardInfo;->setValue(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    :goto_0
    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/android/settings/device/DeviceCardInfo;->setValue(Ljava/lang/String;)V

    :goto_1
    iget-object v3, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/android/settings/R$string;->firmware_version:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/android/settings/device/DeviceCardInfo;->setTitle(Ljava/lang/String;)V

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/android/settings/device/DeviceCardInfo;->setType(I)V

    const-string v4, "firmware_version"

    invoke-virtual {v0, v4}, Lcom/android/settings/device/DeviceCardInfo;->setKey(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/android/settings/R$string;->security_patch:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/android/settings/device/DeviceCardInfo;->setTitle(Ljava/lang/String;)V

    sget-object v4, Landroid/os/Build$VERSION;->SECURITY_PATCH:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/android/settings/device/DeviceCardInfo;->setValue(Ljava/lang/String;)V

    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Lcom/android/settings/device/DeviceCardInfo;->setType(I)V

    const-string v5, "Android security patch"

    invoke-virtual {v1, v5}, Lcom/android/settings/device/DeviceCardInfo;->setKey(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/android/settings/R$string;->device_miui_version:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/android/settings/device/DeviceCardInfo;->setTitle(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    const/4 v6, 0x0

    invoke-static {v5, v6, v3}, Lcom/android/settings/device/MiuiAboutPhoneUtils;->getMiuiVersionInCard(Landroid/content/Context;ZZ)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/android/settings/device/DeviceCardInfo;->setValue(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Lcom/android/settings/device/DeviceCardInfo;->setType(I)V

    const-string/jumbo v5, "miui_version"

    invoke-virtual {v2, v5}, Lcom/android/settings/device/DeviceCardInfo;->setKey(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mVersionlist:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mVersionlist:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mVersionlist:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-boolean v0, Lcom/android/settings/utils/SettingsFeatures;->IS_NEED_OPCUST_VERSION:Z

    if-eqz v0, :cond_3

    new-instance v0, Lcom/android/settings/device/DeviceCardInfo;

    invoke-direct {v0}, Lcom/android/settings/device/DeviceCardInfo;-><init>()V

    iget-object v1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$string;->device_opcust_version:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/device/DeviceCardInfo;->setTitle(Ljava/lang/String;)V

    const-string/jumbo v1, "ro.miui.opcust.version"

    const-string v2, ""

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/android/settings/device/MiuiAboutPhoneUtils;->getOpconfigVersion()Ljava/lang/String;

    move-result-object v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v5, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_2
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/device/DeviceCardInfo;->setValue(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lcom/android/settings/device/DeviceCardInfo;->setType(I)V

    const-string v1, "device_opcust_version"

    invoke-virtual {v0, v1}, Lcom/android/settings/device/DeviceCardInfo;->setKey(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mVersionlist:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mVersionlist:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/device/DeviceCardInfo;

    iget-object v2, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/android/settings/device/DeviceCardInfo;->setListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mRootView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->verison_info:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Landroidx/recyclerview/widget/GridLayoutManager;

    iget-object v2, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, v4}, Landroidx/recyclerview/widget/GridLayoutManager;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v3}, Landroidx/recyclerview/widget/LinearLayoutManager;->setOrientation(I)V

    new-instance v2, Lcom/android/settings/device/DeviceInfoAdapter;

    iget-object v4, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    invoke-direct {v2, v4}, Lcom/android/settings/device/DeviceInfoAdapter;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Lcom/android/settings/device/DeviceInfoAdapter;->setType(I)V

    new-instance v3, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$2;

    invoke-direct {v3, p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$2;-><init>(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)V

    invoke-virtual {v1, v3}, Landroidx/recyclerview/widget/GridLayoutManager;->setSpanSizeLookup(Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;)V

    iget-object v3, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mVersionlist:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Lcom/android/settings/device/DeviceCardInfo;

    invoke-interface {v3, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/android/settings/device/DeviceCardInfo;

    invoke-virtual {v2, v3}, Lcom/android/settings/device/DeviceInfoAdapter;->setDataList([Lcom/android/settings/device/DeviceCardInfo;)V

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getItemDecorationCount()I

    move-result v2

    if-lez v2, :cond_5

    invoke-virtual {v0, v6}, Landroidx/recyclerview/widget/RecyclerView;->getItemDecorationAt(I)Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    move-result-object v2

    if-nez v2, :cond_6

    :cond_5
    new-instance v2, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$VerisonSpaceItemDecoration;

    invoke-direct {v2, p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$VerisonSpaceItemDecoration;-><init>(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)V

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    :cond_6
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    return-void
.end method

.method private startUpdateInfoAsync()V
    .locals 3

    iget-boolean v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mIsNeedUpdateCpu:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$ReadCpuInfoTask;

    new-instance v1, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$UpdateCpuCallBack;

    invoke-direct {v1, p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$UpdateCpuCallBack;-><init>(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)V

    invoke-direct {v0, p0, v1}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$ReadCpuInfoTask;-><init>(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;Lcom/android/settings/device/controller/MiuiDeviceCpuInfoController$CallBack;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mCallback:Lcom/android/settings/device/MemoryInfoHelper$Callback;

    invoke-static {p0}, Lcom/android/settings/device/MemoryInfoHelper;->getAvailableMemorySize(Lcom/android/settings/device/MemoryInfoHelper$Callback;)V

    return-void
.end method


# virtual methods
.method protected createPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/settingslib/core/AbstractPreferenceController;",
            ">;"
        }
    .end annotation

    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lcom/android/settings/device/controller/MiuiPreInstallController;

    invoke-direct {v0, p1}, Lcom/android/settings/device/controller/MiuiPreInstallController;-><init>(Landroid/content/Context;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/settings/device/controller/MiuiSafetylegalController;

    invoke-direct {v0, p1}, Lcom/android/settings/device/controller/MiuiSafetylegalController;-><init>(Landroid/content/Context;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/settings/device/controller/MiuiInstructionController;

    invoke-direct {v0, p1}, Lcom/android/settings/device/controller/MiuiInstructionController;-><init>(Landroid/content/Context;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/settings/device/controller/MiuiLegalInfoController;

    invoke-direct {v0, p1}, Lcom/android/settings/device/controller/MiuiLegalInfoController;-><init>(Landroid/content/Context;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/settings/device/controller/MiuiDeviceStatusInfoController;

    invoke-direct {v0, p1}, Lcom/android/settings/device/controller/MiuiDeviceStatusInfoController;-><init>(Landroid/content/Context;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getInitHelper()Lcom/android/settings/device/DeviceParamsInitHelper;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mHelper:Lcom/android/settings/device/DeviceParamsInitHelper;

    return-object p0
.end method

.method protected getLogTag()Ljava/lang/String;
    .locals 0

    const-string p0, "MiuiMyDeviceDetail"

    return-object p0
.end method

.method public getName()Ljava/lang/String;
    .locals 0

    const-class p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method protected getPreferenceScreenResId()I
    .locals 0

    sget p0, Lcom/android/settings/R$xml;->my_device_detail_settings:I

    return p0
.end method

.method public initView()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->initGridView()V

    invoke-direct {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->initSoftWareVersion()V

    invoke-direct {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->initHardWareVersion()V

    invoke-direct {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->initMemoryInfo()V

    invoke-direct {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->disableRecyclerViewScrollDispatch()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/dashboard/DashboardFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->setHasOptionsMenu(Z)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/android/settings/device/DeviceDetailOnClickListener;

    invoke-direct {v0, p1}, Lcom/android/settings/device/DeviceDetailOnClickListener;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mClickListener:Landroid/view/View$OnClickListener;

    new-instance p1, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$UpdateMemoryCallBack;

    invoke-direct {p1, p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings$UpdateMemoryCallBack;-><init>(Lcom/android/settings/device/MiuiMyDeviceDetailSettings;)V

    iput-object p1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mCallback:Lcom/android/settings/device/MemoryInfoHelper$Callback;

    const-string/jumbo p0, "setting_About_phone_device"

    invoke-static {p0}, Lcom/android/settings/report/InternationalCompat;->trackReportEvent(Ljava/lang/String;)V

    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    new-instance p1, Lmiuix/appcompat/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p1, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v0, 0x104000a

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    invoke-virtual {p1}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p0

    sget v0, Lcom/android/settings/R$layout;->type_approved_content:I

    invoke-virtual {p0, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p0

    invoke-virtual {p1, p0}, Lmiuix/appcompat/app/AlertDialog;->setView(Landroid/view/View;)V

    return-object p1

    :cond_0
    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object p0

    return-object p0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    sget v0, Lcom/android/settings/R$layout;->miui_all_specs:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mRootView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->prefs_container:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/view/ViewGroup;

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/SettingsPreferenceFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->initView()V

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mRootView:Landroid/view/View;

    return-object p0
.end method

.method public onDestroy()V
    .locals 3

    invoke-super {p0}, Lcom/android/settingslib/core/lifecycle/ObservablePreferenceFragment;->onDestroy()V

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mRemoteService:Lcom/android/settings/aidl/IRemoteGetDeviceInfoService;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mDeviceInfoCallback:Lcom/android/settings/device/MiuiMyDeviceDetailSettings$UpdateInfoCallback;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/android/settings/aidl/IRemoteGetDeviceInfoService;->unregisteCallback(Lcom/android/settings/aidl/IRequestCallback;)V

    iput-object v2, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mDeviceInfoCallback:Lcom/android/settings/device/MiuiMyDeviceDetailSettings$UpdateInfoCallback;

    :cond_0
    iput-object v2, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mRemoteService:Lcom/android/settings/aidl/IRemoteGetDeviceInfoService;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    :goto_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mRemoteServiceConn:Lcom/android/settings/device/MiuiMyDeviceDetailSettings$RemoteServiceConn;

    if-eqz p0, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_2
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    invoke-super {p0}, Lcom/android/settingslib/miuisettings/preference/PreferenceFragment;->onDestroyView()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mRootView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mVersionlist:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object p0, p0, Lcom/android/settings/device/MiuiMyDeviceDetailSettings;->mHardwareList:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public onPreferenceTreeClick(Landroidx/preference/PreferenceScreen;Landroidx/preference/Preference;)Z
    .locals 2

    invoke-virtual {p2}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "wifi_type_approval"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/SettingsPreferenceFragment;->showDialog(I)V

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/settings/SettingsPreferenceFragment;->onPreferenceTreeClick(Landroidx/preference/PreferenceScreen;Landroidx/preference/Preference;)Z

    move-result p0

    return p0
.end method
