.class public Lcom/android/settings/device/DeviceBasicInfoPresenter$SpaceItemDecoration;
.super Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/device/DeviceBasicInfoPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SpaceItemDecoration"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/device/DeviceBasicInfoPresenter;


# direct methods
.method public constructor <init>(Lcom/android/settings/device/DeviceBasicInfoPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter$SpaceItemDecoration;->this$0:Lcom/android/settings/device/DeviceBasicInfoPresenter;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;-><init>()V

    return-void
.end method


# virtual methods
.method public getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$State;)V
    .locals 2

    iget-object p4, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter$SpaceItemDecoration;->this$0:Lcom/android/settings/device/DeviceBasicInfoPresenter;

    invoke-static {p4}, Lcom/android/settings/device/DeviceBasicInfoPresenter;->-$$Nest$fgetmContext(Lcom/android/settings/device/DeviceBasicInfoPresenter;)Landroid/content/Context;

    move-result-object p4

    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p4

    sget v0, Lcom/android/settings/R$dimen;->card_item_bottom:I

    invoke-virtual {p4, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p4

    iput p4, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p3, p2}, Landroidx/recyclerview/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result p2

    invoke-virtual {p3}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object p4

    check-cast p4, Landroidx/recyclerview/widget/GridLayoutManager;

    invoke-virtual {p4}, Landroidx/recyclerview/widget/GridLayoutManager;->getSpanCount()I

    move-result p4

    const/4 v0, 0x1

    if-eq p4, v0, :cond_3

    invoke-virtual {p3}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->getItemCount()I

    move-result v1

    sub-int/2addr v1, v0

    if-ne p2, v1, :cond_0

    goto :goto_1

    :cond_0
    add-int/2addr p2, v0

    rem-int/2addr p2, p4

    if-eqz p2, :cond_3

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getLayoutDirection()I

    move-result p2

    if-ne p2, v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    iget-object p0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter$SpaceItemDecoration;->this$0:Lcom/android/settings/device/DeviceBasicInfoPresenter;

    invoke-static {p0}, Lcom/android/settings/device/DeviceBasicInfoPresenter;->-$$Nest$fgetmContext(Lcom/android/settings/device/DeviceBasicInfoPresenter;)Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget p2, Lcom/android/settings/R$dimen;->my_device_card_margin_edge:I

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    iput p0, p1, Landroid/graphics/Rect;->left:I

    goto :goto_1

    :cond_2
    iget-object p0, p0, Lcom/android/settings/device/DeviceBasicInfoPresenter$SpaceItemDecoration;->this$0:Lcom/android/settings/device/DeviceBasicInfoPresenter;

    invoke-static {p0}, Lcom/android/settings/device/DeviceBasicInfoPresenter;->-$$Nest$fgetmContext(Lcom/android/settings/device/DeviceBasicInfoPresenter;)Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget p2, Lcom/android/settings/R$dimen;->my_device_card_margin_edge:I

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    iput p0, p1, Landroid/graphics/Rect;->right:I

    :cond_3
    :goto_1
    return-void
.end method
