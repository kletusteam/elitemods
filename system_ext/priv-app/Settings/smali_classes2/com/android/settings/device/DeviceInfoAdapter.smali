.class public Lcom/android/settings/device/DeviceInfoAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private cardInfos:[Lcom/android/settings/device/DeviceCardInfo;

.field private closeValueTextLineLimit:Z

.field private mCards:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/device/BorderedBaseDeviceCardItem;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mType:I


# direct methods
.method public static synthetic $r8$lambda$5KCjoA3trbZzPHWb-x8gGRexogI(Lcom/android/settings/device/DeviceInfoAdapter;Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/device/DeviceInfoAdapter;->lambda$initExternalRamIcon$0(Landroid/content/Context;Landroid/view/View;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mbuildAlertDialog(Lcom/android/settings/device/DeviceInfoAdapter;Landroid/content/Context;)Landroid/app/Dialog;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/device/DeviceInfoAdapter;->buildAlertDialog(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/device/DeviceInfoAdapter;->mCards:Ljava/util/List;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/device/DeviceInfoAdapter;->closeValueTextLineLimit:Z

    iput-object p1, p0, Lcom/android/settings/device/DeviceInfoAdapter;->mContext:Landroid/content/Context;

    return-void
.end method

.method private buildAlertDialog(Landroid/content/Context;)Landroid/app/Dialog;
    .locals 1

    new-instance p0, Lmiuix/appcompat/app/AlertDialog$Builder;

    sget v0, Lcom/android/settings/R$style;->AlertDialog_Theme_DayNight:I

    invoke-direct {p0, p1, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    sget v0, Lcom/android/settings/R$string;->external_ram_dialog_icon_title:I

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p0

    invoke-static {p1}, Lcom/android/settings/special/ExternalRamController;->getDialogInfo(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p0

    sget p1, Lcom/android/settings/R$string;->external_ram_dialog_icon_confirm:I

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object p0

    return-object p0
.end method

.method private initExternalRamIcon(Landroid/content/Context;Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;Ljava/lang/String;)V
    .locals 6

    if-nez p1, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo p3, "\u00a0"

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, " "

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget v1, Lcom/android/settings/R$drawable;->external_ram_notification:I

    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p3

    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {p3, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    new-instance v1, Landroid/text/SpannableString;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v0

    new-instance v2, Lcom/android/settings/device/DeviceInfoAdapter$3;

    invoke-direct {v2, p0, p1}, Lcom/android/settings/device/DeviceInfoAdapter$3;-><init>(Lcom/android/settings/device/DeviceInfoAdapter;Landroid/content/Context;)V

    new-instance v4, Lcom/android/settings/device/MiuiVersionCard$CustomImageSpan;

    const/4 v5, 0x2

    invoke-direct {v4, p3, v5}, Lcom/android/settings/device/MiuiVersionCard$CustomImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    add-int/lit8 p3, v0, -0x1

    const/16 v5, 0x11

    invoke-virtual {v1, v4, p3, v0, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v1, v2, p3, v0, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    iget-object p3, p2, Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;->card:Lcom/android/settings/device/BaseDeviceCardItem;

    invoke-virtual {p3, v1}, Lcom/android/settings/device/BaseDeviceCardItem;->setTitle(Ljava/lang/CharSequence;)V

    iget-object p3, p2, Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;->card:Lcom/android/settings/device/BaseDeviceCardItem;

    iget-object p3, p3, Lcom/android/settings/device/BaseDeviceCardItem;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {p3, v3}, Landroid/widget/TextView;->setHighlightColor(I)V

    iget-object p3, p2, Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;->card:Lcom/android/settings/device/BaseDeviceCardItem;

    iget-object p3, p3, Lcom/android/settings/device/BaseDeviceCardItem;->mTitleView:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    iget-object p2, p2, Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;->card:Lcom/android/settings/device/BaseDeviceCardItem;

    iget-object p2, p2, Lcom/android/settings/device/BaseDeviceCardItem;->mTitleView:Landroid/widget/TextView;

    new-instance p3, Lcom/android/settings/device/DeviceInfoAdapter$$ExternalSyntheticLambda0;

    invoke-direct {p3, p0, p1}, Lcom/android/settings/device/DeviceInfoAdapter$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/device/DeviceInfoAdapter;Landroid/content/Context;)V

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private synthetic lambda$initExternalRamIcon$0(Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/android/settings/stat/commonswitch/TalkbackSwitch;->isTalkbackEnable(Landroid/content/Context;)Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-direct {p0, p1}, Lcom/android/settings/device/DeviceInfoAdapter;->buildAlertDialog(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object p0

    invoke-virtual {p0}, Landroid/app/Dialog;->show()V

    :cond_0
    return-void
.end method

.method private updateCardHeight(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/device/BorderedBaseDeviceCardItem;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    goto :goto_1

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/device/BorderedBaseDeviceCardItem;

    invoke-virtual {v1}, Lcom/android/settings/device/BaseDeviceCardItem;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "miui_version"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lcom/android/settings/device/BorderedBaseDeviceCardItem;->getmBoardLayout()Landroid/widget/RelativeLayout;

    move-result-object v1

    if-nez v1, :cond_2

    return-void

    :cond_2
    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v3, Lcom/android/settings/device/DeviceInfoAdapter$4;

    invoke-direct {v3, p0, v1, p1}, Lcom/android/settings/device/DeviceInfoAdapter$4;-><init>(Lcom/android/settings/device/DeviceInfoAdapter;Landroid/widget/RelativeLayout;Ljava/util/List;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    :cond_3
    :goto_1
    return-void
.end method


# virtual methods
.method public closeValueTextLineLimit()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/device/DeviceInfoAdapter;->closeValueTextLineLimit:Z

    return-void
.end method

.method public getItemCount()I
    .locals 0

    iget-object p0, p0, Lcom/android/settings/device/DeviceInfoAdapter;->cardInfos:[Lcom/android/settings/device/DeviceCardInfo;

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_0
    array-length p0, p0

    :goto_0
    return p0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    check-cast p1, Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/device/DeviceInfoAdapter;->onBindViewHolder(Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;I)V
    .locals 7

    iget-object v0, p0, Lcom/android/settings/device/DeviceInfoAdapter;->cardInfos:[Lcom/android/settings/device/DeviceCardInfo;

    aget-object v0, v0, p2

    invoke-virtual {v0}, Lcom/android/settings/device/DeviceCardInfo;->getIndex()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget-object v0, p1, Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;->card:Lcom/android/settings/device/BaseDeviceCardItem;

    iget-object v1, p0, Lcom/android/settings/device/DeviceInfoAdapter;->cardInfos:[Lcom/android/settings/device/DeviceCardInfo;

    aget-object v1, v1, p2

    invoke-virtual {v1}, Lcom/android/settings/device/DeviceCardInfo;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/device/BaseDeviceCardItem;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/device/DeviceInfoAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/device/DeviceInfoAdapter;->cardInfos:[Lcom/android/settings/device/DeviceCardInfo;

    aget-object v1, v1, p2

    invoke-virtual {v1}, Lcom/android/settings/device/DeviceCardInfo;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, p1, v1}, Lcom/android/settings/device/DeviceInfoAdapter;->initExternalRamIcon(Landroid/content/Context;Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;Ljava/lang/String;)V

    :goto_0
    iget-boolean v0, p0, Lcom/android/settings/device/DeviceInfoAdapter;->closeValueTextLineLimit:Z

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;->card:Lcom/android/settings/device/BaseDeviceCardItem;

    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Lcom/android/settings/device/BaseDeviceCardItem;->setValueMaxLine(I)V

    :cond_1
    iget-object v0, p1, Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;->card:Lcom/android/settings/device/BaseDeviceCardItem;

    iget-object v1, p0, Lcom/android/settings/device/DeviceInfoAdapter;->cardInfos:[Lcom/android/settings/device/DeviceCardInfo;

    aget-object v1, v1, p2

    invoke-virtual {v1}, Lcom/android/settings/device/DeviceCardInfo;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/device/BaseDeviceCardItem;->setValue(Ljava/lang/String;)V

    iget v0, p0, Lcom/android/settings/device/DeviceInfoAdapter;->mType:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    iget-object v0, p1, Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;->card:Lcom/android/settings/device/BaseDeviceCardItem;

    iget-object v2, p0, Lcom/android/settings/device/DeviceInfoAdapter;->cardInfos:[Lcom/android/settings/device/DeviceCardInfo;

    aget-object v2, v2, p2

    invoke-virtual {v2}, Lcom/android/settings/device/DeviceCardInfo;->getIconResId()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/android/settings/device/BaseDeviceCardItem;->setIcon(I)V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/device/DeviceInfoAdapter;->cardInfos:[Lcom/android/settings/device/DeviceCardInfo;

    aget-object v0, v0, p2

    invoke-virtual {v0}, Lcom/android/settings/device/DeviceCardInfo;->getKey()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p1, Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;->card:Lcom/android/settings/device/BaseDeviceCardItem;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/settings/device/BaseDeviceCardItem;->setKey(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p1, Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;->card:Lcom/android/settings/device/BaseDeviceCardItem;

    invoke-virtual {v2, v0}, Lcom/android/settings/device/BaseDeviceCardItem;->setKey(Ljava/lang/String;)V

    const-string v2, "Android security patch"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string/jumbo v3, "miui_version"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_3
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/device/DeviceInfoAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    const-string v2, "bo"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;->card:Lcom/android/settings/device/BaseDeviceCardItem;

    iget-object v0, v0, Lcom/android/settings/device/BaseDeviceCardItem;->mTitleView:Landroid/widget/TextView;

    const/4 v2, 0x0

    const v3, 0x3f19999a    # 0.6f

    invoke-virtual {v0, v2, v3}, Landroid/widget/TextView;->setLineSpacing(FF)V

    :cond_4
    iget-object v0, p0, Lcom/android/settings/device/DeviceInfoAdapter;->mCards:Ljava/util/List;

    iget-object v2, p1, Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;->card:Lcom/android/settings/device/BaseDeviceCardItem;

    check-cast v2, Lcom/android/settings/device/BorderedBaseDeviceCardItem;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    iget-object v0, p0, Lcom/android/settings/device/DeviceInfoAdapter;->mCards:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_6

    iget-object v0, p0, Lcom/android/settings/device/DeviceInfoAdapter;->mCards:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/android/settings/device/DeviceInfoAdapter;->updateCardHeight(Ljava/util/List;)V

    :cond_6
    iget-object v0, p0, Lcom/android/settings/device/DeviceInfoAdapter;->cardInfos:[Lcom/android/settings/device/DeviceCardInfo;

    aget-object v0, v0, p2

    invoke-virtual {v0}, Lcom/android/settings/device/DeviceCardInfo;->getListener()Landroid/view/View$OnClickListener;

    move-result-object v0

    const/4 v3, 0x0

    if-eqz v0, :cond_d

    iget-object v4, p1, Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;->card:Lcom/android/settings/device/BaseDeviceCardItem;

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p1, Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;->card:Lcom/android/settings/device/BaseDeviceCardItem;

    iget-object v0, p0, Lcom/android/settings/device/DeviceInfoAdapter;->cardInfos:[Lcom/android/settings/device/DeviceCardInfo;

    aget-object p2, v0, p2

    invoke-virtual {p2}, Lcom/android/settings/device/DeviceCardInfo;->getType()I

    move-result p2

    if-eqz p2, :cond_b

    if-ne p2, v1, :cond_7

    iget v0, p0, Lcom/android/settings/device/DeviceInfoAdapter;->mType:I

    if-eq v0, v1, :cond_7

    goto :goto_1

    :cond_7
    new-array v0, v1, [Landroid/view/View;

    aput-object p1, v0, v3

    invoke-static {v0}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v0

    if-eq p2, v1, :cond_8

    if-ne p2, v2, :cond_a

    :cond_8
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    if-ne p2, v2, :cond_9

    iget-object v2, p0, Lcom/android/settings/device/DeviceInfoAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v4, Lcom/android/settings/R$dimen;->params_card_height:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    :cond_9
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getPaddingStart()I

    move-result v2

    iget-object v4, p0, Lcom/android/settings/device/DeviceInfoAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/android/settings/R$dimen;->board_layout_padding_top_bottom:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getPaddingStart()I

    move-result v6

    iget-object p0, p0, Lcom/android/settings/device/DeviceInfoAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    invoke-virtual {p1, v2, v4, v6, p0}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-interface {v0}, Lmiuix/animation/IFolme;->touch()Lmiuix/animation/ITouchStyle;

    move-result-object p0

    new-array v1, v3, [Lmiuix/animation/base/AnimConfig;

    invoke-interface {p0, p1, v1}, Lmiuix/animation/ITouchStyle;->handleTouchOf(Landroid/view/View;[Lmiuix/animation/base/AnimConfig;)V

    :cond_a
    const/4 p0, 0x3

    if-ne p2, p0, :cond_c

    sget p0, Lcom/android/settings/R$id;->board_layout:I

    invoke-virtual {p1, p0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p0

    sget p2, Lcom/android/settings/R$drawable;->card_list_background:I

    invoke-virtual {p0, p2}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-interface {v0}, Lmiuix/animation/IFolme;->touch()Lmiuix/animation/ITouchStyle;

    move-result-object p0

    const/high16 p2, 0x3f800000    # 1.0f

    new-array v0, v3, [Lmiuix/animation/ITouchStyle$TouchType;

    invoke-interface {p0, p2, v0}, Lmiuix/animation/ITouchStyle;->setScale(F[Lmiuix/animation/ITouchStyle$TouchType;)Lmiuix/animation/ITouchStyle;

    move-result-object p0

    new-array p2, v3, [Lmiuix/animation/base/AnimConfig;

    invoke-interface {p0, p1, p2}, Lmiuix/animation/ITouchStyle;->handleTouchOf(Landroid/view/View;[Lmiuix/animation/base/AnimConfig;)V

    goto :goto_2

    :cond_b
    :goto_1
    new-instance p2, Lcom/android/settings/device/DeviceInfoAdapter$2;

    invoke-direct {p2, p0}, Lcom/android/settings/device/DeviceInfoAdapter$2;-><init>(Lcom/android/settings/device/DeviceInfoAdapter;)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_c
    :goto_2
    return-void

    :cond_d
    iget-object p0, p1, Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;->card:Lcom/android/settings/device/BaseDeviceCardItem;

    invoke-virtual {p0, v3}, Landroid/widget/LinearLayout;->setClickable(Z)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/device/DeviceInfoAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;

    move-result-object p0

    return-object p0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;
    .locals 2

    iget p2, p0, Lcom/android/settings/device/DeviceInfoAdapter;->mType:I

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ne p2, v1, :cond_0

    iget-object p2, p0, Lcom/android/settings/device/DeviceInfoAdapter;->mContext:Landroid/content/Context;

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    sget v1, Lcom/android/settings/R$layout;->bordered_base_card_item_wrap:I

    invoke-virtual {p2, v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/android/settings/device/DeviceInfoAdapter;->mContext:Landroid/content/Context;

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    sget v1, Lcom/android/settings/R$layout;->base_card_item_wrap:I

    invoke-virtual {p2, v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    :goto_0
    new-instance p2, Lcom/android/settings/device/DeviceInfoAdapter$1;

    invoke-direct {p2, p0}, Lcom/android/settings/device/DeviceInfoAdapter$1;-><init>(Lcom/android/settings/device/DeviceInfoAdapter;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    new-instance p2, Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;

    invoke-direct {p2, p0, p1}, Lcom/android/settings/device/DeviceInfoAdapter$DeviceCardViewHolder;-><init>(Lcom/android/settings/device/DeviceInfoAdapter;Landroid/view/View;)V

    return-object p2
.end method

.method public setDataList([Lcom/android/settings/device/DeviceCardInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/device/DeviceInfoAdapter;->cardInfos:[Lcom/android/settings/device/DeviceCardInfo;

    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setType(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/device/DeviceInfoAdapter;->mType:I

    return-void
.end method
