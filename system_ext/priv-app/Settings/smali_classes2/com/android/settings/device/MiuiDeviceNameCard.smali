.class public Lcom/android/settings/device/MiuiDeviceNameCard;
.super Lcom/android/settings/widget/BaseSettingsCard;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mDeviceNameText:Landroid/widget/TextView;

.field private mFragment:Lcom/android/settings/dashboard/DashboardFragment;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/widget/BaseSettingsCard;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/android/settings/device/MiuiDeviceNameCard;->initView()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/widget/BaseSettingsCard;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/android/settings/device/MiuiDeviceNameCard;->initView()V

    return-void
.end method

.method private initView()V
    .locals 1

    sget v0, Lcom/android/settings/R$layout;->device_name_card:I

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/BaseSettingsCard;->addLayout(I)V

    sget v0, Lcom/android/settings/R$id;->my_device_name:I

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/device/MiuiDeviceNameCard;->mDeviceNameText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/device/MiuiDeviceNameCard;->refreshDeviceName()V

    invoke-virtual {p0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    const v0, 0x3e99999a    # 0.3f

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->setAlpha(F)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string p1, ":miui:starting_window_label"

    const-string v0, ""

    invoke-virtual {v4, p1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/device/MiuiDeviceNameCard;->mFragment:Lcom/android/settings/dashboard/DashboardFragment;

    const-class p0, Lcom/android/settings/MiuiDeviceNameEditFragment;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, v1

    invoke-virtual/range {v0 .. v5}, Lcom/android/settings/SettingsPreferenceFragment;->startFragment(Landroidx/fragment/app/Fragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z

    const-string/jumbo p0, "setting_About_phone_phonename"

    invoke-static {p0}, Lcom/android/settings/report/InternationalCompat;->trackReportEvent(Ljava/lang/String;)V

    return-void
.end method

.method public refreshDeviceName()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/widget/BaseSettingsCard;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/provider/MiuiSettings$System;->getDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object p0, p0, Lcom/android/settings/device/MiuiDeviceNameCard;->mDeviceNameText:Landroid/widget/TextView;

    if-eqz p0, :cond_0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setFragment(Lcom/android/settings/dashboard/DashboardFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/device/MiuiDeviceNameCard;->mFragment:Lcom/android/settings/dashboard/DashboardFragment;

    return-void
.end method
