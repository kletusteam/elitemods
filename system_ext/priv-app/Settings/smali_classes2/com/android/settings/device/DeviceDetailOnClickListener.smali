.class public Lcom/android/settings/device/DeviceDetailOnClickListener;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDebuggingFeaturesDisallowedAdmin:Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

.field private mDebuggingFeaturesDisallowedBySystem:Z

.field private mDevHitCountdown:I

.field private mFunDisallowedAdmin:Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

.field private mFunDisallowedBySystem:Z

.field private mHits:[J

.field private mKernelHitCountdown:I

.field private mLastHitKey:Ljava/lang/String;

.field private mLastKernelHitTime:J

.field private mLastMemoryHitTime:J

.field private mLastPrefHitTime:J

.field private mMemoryHitCountdown:I

.field private mPrefHitCountdown:I

.field private mUm:Landroid/os/UserManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    iput v0, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mPrefHitCountdown:I

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mLastPrefHitTime:J

    const/4 v3, 0x3

    new-array v3, v3, [J

    iput-object v3, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mHits:[J

    iput v0, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mKernelHitCountdown:I

    iput-wide v1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mLastKernelHitTime:J

    iput v0, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mMemoryHitCountdown:I

    iput-wide v1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mLastMemoryHitTime:J

    iput-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string/jumbo v0, "user"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/os/UserManager;

    iput-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mUm:Landroid/os/UserManager;

    iget-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    const-string/jumbo v1, "no_debugging_features"

    invoke-static {p1, v1, v0}, Lcom/android/settingslib/RestrictedLockUtilsInternal;->checkIfRestrictionEnforced(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mDebuggingFeaturesDisallowedAdmin:Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    iget-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-static {p1, v1, v0}, Lcom/android/settingslib/RestrictedLockUtilsInternal;->hasBaseUserRestriction(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mDebuggingFeaturesDisallowedBySystem:Z

    iget-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settingslib/development/DevelopmentSettingsEnabler;->isDevelopmentSettingsEnabled(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x7

    :goto_0
    iput p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mDevHitCountdown:I

    iget-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    const-string/jumbo v1, "no_fun"

    invoke-static {p1, v1, v0}, Lcom/android/settingslib/RestrictedLockUtilsInternal;->checkIfRestrictionEnforced(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mFunDisallowedAdmin:Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    iget-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-static {p1, v1, v0}, Lcom/android/settingslib/RestrictedLockUtilsInternal;->hasBaseUserRestriction(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mFunDisallowedBySystem:Z

    return-void
.end method

.method private dealCpuClick(Ljava/lang/String;)V
    .locals 7

    const-string v0, "cpu_item"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x4

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mLastHitKey:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mLastPrefHitTime:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0xbb8

    cmp-long p1, v2, v4

    if-lez p1, :cond_2

    iget p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mPrefHitCountdown:I

    const/4 v0, 0x1

    sub-int/2addr p1, v0

    iput p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mPrefHitCountdown:I

    if-lez p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/android/settings/R$plurals;->show_rep_countdown:I

    iget v4, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mPrefHitCountdown:I

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v0, v6

    invoke-virtual {v2, v3, v4, v0}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v6}, Lcom/android/settingslib/util/ToastUtil;->show(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    :cond_0
    iget p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mPrefHitCountdown:I

    if-gtz p1, :cond_2

    new-instance p1, Landroid/content/Intent;

    const-string v0, "android_secret_code://284"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v2, "android.provider.Telephony.SECRET_CODE"

    invoke-direct {p1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v0, 0x1000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    iput v1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mPrefHitCountdown:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mLastPrefHitTime:J

    goto :goto_0

    :cond_1
    iput v1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mPrefHitCountdown:I

    :cond_2
    :goto_0
    return-void
.end method

.method private dealFirmwareVersion(Ljava/lang/String;)V
    .locals 6

    const-string v0, "firmware_version"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mHits:[J

    array-length v0, p1

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    const/4 v2, 0x0

    invoke-static {p1, v1, p1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mHits:[J

    array-length v0, p1

    sub-int/2addr v0, v1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    aput-wide v3, p1, v0

    iget-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mHits:[J

    aget-wide v0, p1, v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x1f4

    sub-long/2addr v2, v4

    cmp-long p1, v0, v2

    if-ltz p1, :cond_3

    iget-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mUm:Landroid/os/UserManager;

    const-string/jumbo v0, "no_fun"

    invoke-virtual {p1, v0}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;)Z

    move-result p1

    const-string v0, "DeviceDetailOnClickListener"

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mFunDisallowedAdmin:Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    if-eqz p1, :cond_1

    iget-boolean v1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mFunDisallowedBySystem:Z

    if-nez v1, :cond_1

    iget-object p0, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    invoke-static {p0, p1}, Lcom/android/settingslib/RestrictedLockUtils;->sendShowAdminSupportDetailsIntent(Landroid/content/Context;Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)V

    :cond_1
    const-string p0, "Sorry, no fun for you!"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_2
    new-instance p1, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {p1, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/android/internal/app/PlatLogoActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :try_start_0
    iget-object p0, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unable to start activity "

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    :goto_0
    return-void
.end method

.method private dealKernelVersion(Ljava/lang/String;)V
    .locals 7

    const-string v0, "kernel_version"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/android/settings/device/MiuiAboutPhoneUtils;->supportCit()Z

    move-result v0

    const/4 v1, 0x4

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mLastHitKey:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mLastKernelHitTime:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0xbb8

    cmp-long p1, v2, v4

    const/4 v0, 0x0

    if-lez p1, :cond_2

    iget p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mKernelHitCountdown:I

    const/4 v2, 0x1

    sub-int/2addr p1, v2

    iput p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mKernelHitCountdown:I

    if-lez p1, :cond_1

    sget p1, Lcom/android/settings/R$plurals;->show_cit_countdown:I

    iget-object v3, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget v5, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mKernelHitCountdown:I

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v2, v0

    invoke-virtual {v4, p1, v5, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v3, p1, v0}, Lcom/android/settingslib/util/ToastUtil;->show(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    :cond_1
    iget p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mKernelHitCountdown:I

    if-gtz p1, :cond_4

    new-instance p1, Landroid/content/Intent;

    const-string v0, "android_secret_code://6484"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v2, "android.provider.Telephony.SECRET_CODE"

    invoke-direct {p1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v0, 0x1000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    iput v1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mKernelHitCountdown:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mLastKernelHitTime:J

    goto :goto_0

    :cond_2
    iget-object p0, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v1, Lcom/android/settings/R$string;->launching_cit:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1, v0}, Lcom/android/settingslib/util/ToastUtil;->show(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    goto :goto_0

    :cond_3
    iput v1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mKernelHitCountdown:I

    :cond_4
    :goto_0
    return-void
.end method

.method private dealMemoryClick(Ljava/lang/String;)V
    .locals 7

    const-string v0, "device_internal_memory"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/android/settings/device/MiuiAboutPhoneUtils;->supportCit()Z

    move-result v0

    const/4 v1, 0x4

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mLastHitKey:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mLastMemoryHitTime:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0xbb8

    cmp-long p1, v2, v4

    const/4 v0, 0x0

    if-lez p1, :cond_2

    iget p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mMemoryHitCountdown:I

    const/4 v2, 0x1

    sub-int/2addr p1, v2

    iput p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mMemoryHitCountdown:I

    if-lez p1, :cond_1

    iget-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/android/settings/R$plurals;->show_pho_countdown:I

    iget v5, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mMemoryHitCountdown:I

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v2, v0

    invoke-virtual {v3, v4, v5, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2, v0}, Lcom/android/settingslib/util/ToastUtil;->show(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    :cond_1
    iget p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mMemoryHitCountdown:I

    if-gtz p1, :cond_4

    new-instance p1, Landroid/content/Intent;

    const-string v0, "android_secret_code://4636"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v2, "android.telephony.action.SECRET_CODE"

    invoke-direct {p1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v0, 0x1000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    iput v1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mMemoryHitCountdown:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mLastMemoryHitTime:J

    goto :goto_0

    :cond_2
    iget-object p0, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v1, Lcom/android/settings/R$string;->launching_pho:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1, v0}, Lcom/android/settingslib/util/ToastUtil;->show(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    goto :goto_0

    :cond_3
    iput v1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mMemoryHitCountdown:I

    :cond_4
    :goto_0
    return-void
.end method

.method private dealMiuiVersionClick(Ljava/lang/String;)V
    .locals 5

    const-string/jumbo v0, "miui_version"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/android/settings/Utils;->isMonkeyRunning()Z

    move-result p1

    if-eqz p1, :cond_1

    return-void

    :cond_1
    iget-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/MiuiUtils;->isSecondSpace(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_8

    iget-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mUm:Landroid/os/UserManager;

    invoke-virtual {p1}, Landroid/os/UserManager;->isAdminUser()Z

    move-result p1

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mUm:Landroid/os/UserManager;

    invoke-virtual {p1}, Landroid/os/UserManager;->isDemoUser()Z

    move-result p1

    if-nez p1, :cond_2

    goto/16 :goto_0

    :cond_2
    iget-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mUm:Landroid/os/UserManager;

    const-string/jumbo v0, "no_debugging_features"

    invoke-virtual {p1, v0}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;)Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_5

    sget-boolean p1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez p1, :cond_3

    iget-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mUm:Landroid/os/UserManager;

    invoke-virtual {p1}, Landroid/os/UserManager;->isDemoUser()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/Utils;->getDeviceOwnerComponent(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object p1

    if-eqz p1, :cond_3

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    const-string v1, "com.android.settings.action.REQUEST_DEBUG_FEATURES"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    iget-object v1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object p0, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_3
    iget-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mDebuggingFeaturesDisallowedAdmin:Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    if-eqz p1, :cond_4

    iget-boolean v0, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mDebuggingFeaturesDisallowedBySystem:Z

    if-nez v0, :cond_4

    iget-object p0, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    invoke-static {p0, p1}, Lcom/android/settingslib/RestrictedLockUtils;->sendShowAdminSupportDetailsIntent(Landroid/content/Context;Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)V

    :cond_4
    return-void

    :cond_5
    iget p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mDevHitCountdown:I

    const/4 v1, 0x1

    if-lez p1, :cond_7

    sub-int/2addr p1, v1

    iput p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mDevHitCountdown:I

    if-nez p1, :cond_6

    iget-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    invoke-static {p1, v1}, Lcom/android/settingslib/development/DevelopmentSettingsEnabler;->setDevelopmentSettingsEnabled(Landroid/content/Context;Z)V

    iget-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    sget v0, Lcom/android/settings/R$string;->show_dev_on:I

    invoke-static {p1, v0, v1}, Lcom/android/settingslib/util/ToastUtil;->show(Landroid/content/Context;II)V

    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    const-string v0, "com.android.settings.action.DEV_OPEN"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v0, "show"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object p0, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    invoke-virtual {p0, p1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    :cond_6
    if-lez p1, :cond_8

    const/4 v2, 0x5

    if-ge p1, v2, :cond_8

    iget-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/android/settings/R$plurals;->show_dev_countdown:I

    iget p0, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mDevHitCountdown:I

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v0

    invoke-virtual {v2, v3, p0, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p1, p0, v0}, Lcom/android/settingslib/util/ToastUtil;->show(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    goto :goto_0

    :cond_7
    if-gez p1, :cond_8

    iget-object p0, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->show_dev_already:I

    invoke-static {p0, p1, v1}, Lcom/android/settingslib/util/ToastUtil;->show(Landroid/content/Context;II)V

    :cond_8
    :goto_0
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    instance-of v0, p1, Lcom/android/settings/device/BaseDeviceCardItem;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/android/settings/device/BaseDeviceCardItem;

    invoke-virtual {p1}, Lcom/android/settings/device/BaseDeviceCardItem;->getKey()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/android/settings/device/DeviceDetailOnClickListener;->dealCpuClick(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/android/settings/device/DeviceDetailOnClickListener;->dealMiuiVersionClick(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/android/settings/device/DeviceDetailOnClickListener;->dealFirmwareVersion(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/android/settings/device/DeviceDetailOnClickListener;->dealKernelVersion(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/android/settings/device/DeviceDetailOnClickListener;->dealMemoryClick(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/android/settings/device/DeviceDetailOnClickListener;->mLastHitKey:Ljava/lang/String;

    :cond_0
    return-void
.end method
