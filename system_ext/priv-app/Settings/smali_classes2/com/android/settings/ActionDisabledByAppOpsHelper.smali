.class final Lcom/android/settings/ActionDisabledByAppOpsHelper;
.super Ljava/lang/Object;


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mDialogView:Landroid/view/ViewGroup;


# direct methods
.method public static synthetic $r8$lambda$TY50dME7BiMSuaDJWZaX7EfiYAY(Lcom/android/settings/ActionDisabledByAppOpsHelper;Ljava/lang/String;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/ActionDisabledByAppOpsHelper;->lambda$prepareDialogBuilder$0(Ljava/lang/String;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method constructor <init>(Landroid/app/Activity;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/ActionDisabledByAppOpsHelper;->mActivity:Landroid/app/Activity;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    sget v0, Lcom/android/settings/R$layout;->support_details_dialog:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/android/settings/ActionDisabledByAppOpsHelper;->mDialogView:Landroid/view/ViewGroup;

    return-void
.end method

.method private initializeDialogViews(Landroid/view/View;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/settings/ActionDisabledByAppOpsHelper;->setSupportTitle(Landroid/view/View;)V

    invoke-virtual {p0, p1}, Lcom/android/settings/ActionDisabledByAppOpsHelper;->setSupportDetails(Landroid/view/View;)V

    return-void
.end method

.method private synthetic lambda$prepareDialogBuilder$0(Ljava/lang/String;Landroid/content/DialogInterface;I)V
    .locals 0

    iget-object p2, p0, Lcom/android/settings/ActionDisabledByAppOpsHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p1, p3}, Lcom/android/settingslib/HelpUtils;->getHelpIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p0, p0, Lcom/android/settings/ActionDisabledByAppOpsHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public prepareDialogBuilder()Landroidx/appcompat/app/AlertDialog$Builder;
    .locals 4

    iget-object v0, p0, Lcom/android/settings/ActionDisabledByAppOpsHelper;->mActivity:Landroid/app/Activity;

    sget v1, Lcom/android/settings/R$string;->help_url_action_disabled_by_restricted_settings:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroidx/appcompat/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/android/settings/ActionDisabledByAppOpsHelper;->mActivity:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/android/settings/R$string;->okay:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroidx/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/ActionDisabledByAppOpsHelper;->mDialogView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object v1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    sget v2, Lcom/android/settings/R$string;->learn_more:I

    new-instance v3, Lcom/android/settings/ActionDisabledByAppOpsHelper$$ExternalSyntheticLambda0;

    invoke-direct {v3, p0, v0}, Lcom/android/settings/ActionDisabledByAppOpsHelper$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/ActionDisabledByAppOpsHelper;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroidx/appcompat/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/ActionDisabledByAppOpsHelper;->mDialogView:Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/android/settings/ActionDisabledByAppOpsHelper;->initializeDialogViews(Landroid/view/View;)V

    return-object v1
.end method

.method setSupportDetails(Landroid/view/View;)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    sget p0, Lcom/android/settings/R$id;->admin_support_msg:I

    goto/32 :goto_4

    nop

    :goto_2
    check-cast p0, Landroid/widget/TextView;

    goto/32 :goto_5

    nop

    :goto_3
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(I)V

    goto/32 :goto_0

    nop

    :goto_4
    invoke-virtual {p1, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    goto/32 :goto_2

    nop

    :goto_5
    sget p1, Lcom/android/settings/R$string;->blocked_by_restricted_settings_content:I

    goto/32 :goto_3

    nop
.end method

.method setSupportTitle(Landroid/view/View;)V
    .locals 0

    goto/32 :goto_8

    nop

    :goto_0
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(I)V

    goto/32 :goto_5

    nop

    :goto_1
    if-eqz p0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_6

    nop

    :goto_2
    invoke-virtual {p1, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    goto/32 :goto_4

    nop

    :goto_3
    sget p1, Lcom/android/settings/R$string;->blocked_by_restricted_settings_title:I

    goto/32 :goto_0

    nop

    :goto_4
    check-cast p0, Landroid/widget/TextView;

    goto/32 :goto_1

    nop

    :goto_5
    return-void

    :goto_6
    return-void

    :goto_7
    goto/32 :goto_3

    nop

    :goto_8
    sget p0, Lcom/android/settings/R$id;->admin_support_dialog_title:I

    goto/32 :goto_2

    nop
.end method

.method public updateDialog()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/ActionDisabledByAppOpsHelper;->mDialogView:Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/android/settings/ActionDisabledByAppOpsHelper;->initializeDialogViews(Landroid/view/View;)V

    return-void
.end method
