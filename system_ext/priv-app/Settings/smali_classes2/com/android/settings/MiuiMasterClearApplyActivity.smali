.class public Lcom/android/settings/MiuiMasterClearApplyActivity;
.super Lmiuix/appcompat/app/AppCompatActivity;

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static STEP_RES:[I


# instance fields
.field private mAcceptButton:Landroid/widget/Button;

.field private mAutoNextStepTime:I

.field private mCurrentStep:I

.field private mHandler:Landroid/os/Handler;

.field private mNextStepTime:I

.field private mRejectButton:Landroid/widget/Button;

.field private mWarningInfoView:Landroid/widget/TextView;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAcceptButton(Lcom/android/settings/MiuiMasterClearApplyActivity;)Landroid/widget/Button;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mAcceptButton:Landroid/widget/Button;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmAutoNextStepTime(Lcom/android/settings/MiuiMasterClearApplyActivity;)I
    .locals 0

    iget p0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mAutoNextStepTime:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmCurrentStep(Lcom/android/settings/MiuiMasterClearApplyActivity;)I
    .locals 0

    iget p0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mCurrentStep:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/settings/MiuiMasterClearApplyActivity;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmAutoNextStepTime(Lcom/android/settings/MiuiMasterClearApplyActivity;I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mAutoNextStepTime:I

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [I

    sput-object v0, Lcom/android/settings/MiuiMasterClearApplyActivity;->STEP_RES:[I

    sget v1, Lcom/android/settings/R$string;->master_clear_apply_step_1:I

    const/4 v2, 0x1

    aput v1, v0, v2

    sget v1, Lcom/android/settings/R$string;->master_clear_apply_step_2:I

    const/4 v2, 0x2

    aput v1, v0, v2

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiuix/appcompat/app/AppCompatActivity;-><init>()V

    new-instance v0, Lcom/android/settings/MiuiMasterClearApplyActivity$1;

    invoke-direct {v0, p0}, Lcom/android/settings/MiuiMasterClearApplyActivity$1;-><init>(Lcom/android/settings/MiuiMasterClearApplyActivity;)V

    iput-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private getWarningInfo(I)Ljava/lang/CharSequence;
    .locals 1

    sget-object v0, Lcom/android/settings/MiuiMasterClearApplyActivity;->STEP_RES:[I

    aget p1, v0, p1

    invoke-virtual {p0, p1}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method private resetNextStepTime()V
    .locals 1

    iget v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mNextStepTime:I

    iput v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mAutoNextStepTime:I

    return-void
.end method


# virtual methods
.method public finish()V
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0}, Landroid/app/Activity;->overridePendingTransition(II)V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    sget v0, Lcom/android/settings/R$id;->cancel:I

    const/4 v1, 0x0

    const/16 v2, 0x64

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v2}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClearApplyActivity;->finish()V

    goto :goto_1

    :cond_0
    sget v0, Lcom/android/settings/R$id;->ok:I

    if-ne p1, v0, :cond_3

    iget p1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mCurrentStep:I

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v2}, Landroid/os/Handler;->removeMessages(I)V

    const/4 p1, -0x1

    invoke-virtual {p0, p1}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Lcom/android/settings/MiuiMasterClearApplyActivity;->finish()V

    goto :goto_1

    :cond_1
    const/4 v3, 0x1

    add-int/2addr p1, v3

    iput p1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mCurrentStep:I

    invoke-direct {p0}, Lcom/android/settings/MiuiMasterClearApplyActivity;->resetNextStepTime()V

    iget-object p1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mWarningInfoView:Landroid/widget/TextView;

    iget v4, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mCurrentStep:I

    invoke-direct {p0, v4}, Lcom/android/settings/MiuiMasterClearApplyActivity;->getWarningInfo(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget p1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mCurrentStep:I

    if-ne p1, v0, :cond_2

    iget-object p1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mAcceptButton:Landroid/widget/Button;

    sget v0, Lcom/android/settings/R$string;->button_text_ok_timer:I

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mAutoNextStepTime:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {p0, v0, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mAcceptButton:Landroid/widget/Button;

    sget v0, Lcom/android/settings/R$string;->button_text_next_step_timer:I

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mAutoNextStepTime:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {p0, v0, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object p1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mAcceptButton:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object p1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object p0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mHandler:Landroid/os/Handler;

    const-wide/16 v0, 0x3e8

    invoke-virtual {p0, v2, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_3
    :goto_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    const/high16 v0, 0x8000000

    invoke-virtual {p1, v0, v0}, Landroid/view/Window;->setFlags(II)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "format_internal_storage"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "1217"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :cond_0
    const/16 v0, 0xa

    :goto_0
    iput v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mNextStepTime:I

    if-eqz p1, :cond_1

    move v0, v1

    goto :goto_1

    :cond_1
    const/4 v0, 0x2

    :goto_1
    iput v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mCurrentStep:I

    sget v0, Lcom/android/settings/R$layout;->master_clear_apply:I

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/AppCompatActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/android/settings/MiuiMasterClearApplyActivity;->resetNextStepTime()V

    sget v0, Lcom/android/settings/R$id;->warning_info:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mWarningInfoView:Landroid/widget/TextView;

    sget v0, Lcom/android/settings/R$id;->cancel:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mRejectButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/android/settings/R$id;->ok:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mAcceptButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mWarningInfoView:Landroid/widget/TextView;

    iget v2, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mCurrentStep:I

    invoke-direct {p0, v2}, Lcom/android/settings/MiuiMasterClearApplyActivity;->getWarningInfo(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mAcceptButton:Landroid/widget/Button;

    if-eqz p1, :cond_2

    sget p1, Lcom/android/settings/R$string;->button_text_next_step_timer:I

    goto :goto_2

    :cond_2
    sget p1, Lcom/android/settings/R$string;->button_text_ok_timer:I

    :goto_2
    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mAutoNextStepTime:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p0, p1, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mAcceptButton:Landroid/widget/Button;

    invoke-virtual {p1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object p0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mHandler:Landroid/os/Handler;

    const/16 p1, 0x64

    const-wide/16 v0, 0x3e8

    invoke-virtual {p0, p1, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onDestroy()V

    iget-object p0, p0, Lcom/android/settings/MiuiMasterClearApplyActivity;->mHandler:Landroid/os/Handler;

    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method
