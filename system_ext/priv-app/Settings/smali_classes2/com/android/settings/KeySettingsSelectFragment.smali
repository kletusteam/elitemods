.class public Lcom/android/settings/KeySettingsSelectFragment;
.super Lcom/android/settings/SettingsPreferenceFragment;


# instance fields
.field private mActionChangeDialog:Lmiuix/appcompat/app/AlertDialog;

.field private mContentObserver:Landroid/database/ContentObserver;

.field private mContext:Landroid/content/Context;

.field private mFsgChangeDialog:Lmiuix/appcompat/app/AlertDialog;

.field private mHidedRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

.field private mKeyGestureFunctionCategory:Landroidx/preference/PreferenceCategory;

.field private mKeyGestureFunctionOptional:Lmiuix/preference/RadioButtonPreferenceCategory;

.field private mKeyGestureFunctionPreview:Lcom/android/settings/KeySettingsPreviewPreference;

.field private mPreferenceKey:Ljava/lang/String;

.field private mRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

.field private mTitle:Ljava/lang/String;

.field private resources:Landroid/content/res/Resources;


# direct methods
.method static bridge synthetic -$$Nest$fgetmActionChangeDialog(Lcom/android/settings/KeySettingsSelectFragment;)Lmiuix/appcompat/app/AlertDialog;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mActionChangeDialog:Lmiuix/appcompat/app/AlertDialog;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/settings/KeySettingsSelectFragment;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFsgChangeDialog(Lcom/android/settings/KeySettingsSelectFragment;)Lmiuix/appcompat/app/AlertDialog;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mFsgChangeDialog:Lmiuix/appcompat/app/AlertDialog;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmKeyGestureFunctionOptional(Lcom/android/settings/KeySettingsSelectFragment;)Lmiuix/preference/RadioButtonPreferenceCategory;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mKeyGestureFunctionOptional:Lmiuix/preference/RadioButtonPreferenceCategory;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmKeyGestureFunctionPreview(Lcom/android/settings/KeySettingsSelectFragment;)Lcom/android/settings/KeySettingsPreviewPreference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mKeyGestureFunctionPreview:Lcom/android/settings/KeySettingsPreviewPreference;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPreferenceKey(Lcom/android/settings/KeySettingsSelectFragment;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmRadioButtonPreference(Lcom/android/settings/KeySettingsSelectFragment;)Lmiuix/preference/RadioButtonPreference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmActionChangeDialog(Lcom/android/settings/KeySettingsSelectFragment;Lmiuix/appcompat/app/AlertDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mActionChangeDialog:Lmiuix/appcompat/app/AlertDialog;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFsgChangeDialog(Lcom/android/settings/KeySettingsSelectFragment;Lmiuix/appcompat/app/AlertDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mFsgChangeDialog:Lmiuix/appcompat/app/AlertDialog;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmRadioButtonPreference(Lcom/android/settings/KeySettingsSelectFragment;Lmiuix/preference/RadioButtonPreference;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    return-void
.end method

.method static bridge synthetic -$$Nest$mbringUpFsgChooseDlg(Lcom/android/settings/KeySettingsSelectFragment;Ljava/lang/String;Ljava/lang/String;Lmiuix/preference/RadioButtonPreference;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/KeySettingsSelectFragment;->bringUpFsgChooseDlg(Ljava/lang/String;Ljava/lang/String;Lmiuix/preference/RadioButtonPreference;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetLongPressPowerPreference(Lcom/android/settings/KeySettingsSelectFragment;Lmiuix/preference/RadioButtonPreferenceCategory;)Lmiuix/preference/RadioButtonPreference;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/KeySettingsSelectFragment;->getLongPressPowerPreference(Lmiuix/preference/RadioButtonPreferenceCategory;)Lmiuix/preference/RadioButtonPreference;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$misNeedFsgDlg(Lcom/android/settings/KeySettingsSelectFragment;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/KeySettingsSelectFragment;->isNeedFsgDlg(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mperformFsgChange(Lcom/android/settings/KeySettingsSelectFragment;Ljava/lang/String;Lmiuix/preference/RadioButtonPreference;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/KeySettingsSelectFragment;->performFsgChange(Ljava/lang/String;Lmiuix/preference/RadioButtonPreference;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mperformSettingsChange(Lcom/android/settings/KeySettingsSelectFragment;Ljava/lang/String;Lmiuix/preference/RadioButtonPreference;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/KeySettingsSelectFragment;->performSettingsChange(Ljava/lang/String;Lmiuix/preference/RadioButtonPreference;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/SettingsPreferenceFragment;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mFsgChangeDialog:Lmiuix/appcompat/app/AlertDialog;

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mActionChangeDialog:Lmiuix/appcompat/app/AlertDialog;

    return-void
.end method

.method private bringUpActionChooseDlg(Ljava/lang/String;Ljava/lang/String;Lmiuix/preference/RadioButtonPreference;)V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mActionChangeDialog:Lmiuix/appcompat/app/AlertDialog;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/android/settings/KeySettingsSelectFragment$3;

    invoke-direct {v0, p0, p1, p3}, Lcom/android/settings/KeySettingsSelectFragment$3;-><init>(Lcom/android/settings/KeySettingsSelectFragment;Ljava/lang/String;Lmiuix/preference/RadioButtonPreference;)V

    iget-object p3, p0, Lcom/android/settings/KeySettingsSelectFragment;->resources:Landroid/content/res/Resources;

    invoke-direct {p0, p1, p3}, Lcom/android/settings/KeySettingsSelectFragment;->getAction(Ljava/lang/String;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object p1

    iget-object p3, p0, Lcom/android/settings/KeySettingsSelectFragment;->resources:Landroid/content/res/Resources;

    invoke-direct {p0, p2, p3}, Lcom/android/settings/KeySettingsSelectFragment;->getFunction(Ljava/lang/String;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object p2

    new-instance p3, Lmiuix/appcompat/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContext:Landroid/content/Context;

    invoke-direct {p3, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {p3, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p3

    iget-object v1, p0, Lcom/android/settings/KeySettingsSelectFragment;->resources:Landroid/content/res/Resources;

    sget v2, Lcom/android/settings/R$string;->key_gesture_function_dialog_message:I

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 p1, 0x1

    aput-object p2, v3, p1

    const/4 p1, 0x2

    iget-object p2, p0, Lcom/android/settings/KeySettingsSelectFragment;->mTitle:Ljava/lang/String;

    aput-object p2, v3, p1

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    sget p2, Lcom/android/settings/R$string;->key_gesture_function_dialog_positive:I

    invoke-virtual {p1, p2, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    sget p2, Lcom/android/settings/R$string;->key_gesture_function_dialog_negative:I

    invoke-virtual {p1, p2, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    invoke-virtual {p1, v4}, Lmiuix/appcompat/app/AlertDialog$Builder;->setCancelable(Z)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mActionChangeDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private bringUpFsgChooseDlg(Ljava/lang/String;Ljava/lang/String;Lmiuix/preference/RadioButtonPreference;)V
    .locals 1

    iget-object p2, p0, Lcom/android/settings/KeySettingsSelectFragment;->mKeyGestureFunctionOptional:Lmiuix/preference/RadioButtonPreferenceCategory;

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {p2, v0}, Lmiuix/preference/RadioButtonPreferenceCategory;->setCheckedPreference(Landroidx/preference/Preference;)V

    iget-object p2, p0, Lcom/android/settings/KeySettingsSelectFragment;->mFsgChangeDialog:Lmiuix/appcompat/app/AlertDialog;

    if-eqz p2, :cond_0

    return-void

    :cond_0
    new-instance p2, Lcom/android/settings/KeySettingsSelectFragment$2;

    invoke-direct {p2, p0, p1, p3}, Lcom/android/settings/KeySettingsSelectFragment$2;-><init>(Lcom/android/settings/KeySettingsSelectFragment;Ljava/lang/String;Lmiuix/preference/RadioButtonPreference;)V

    new-instance p1, Lmiuix/appcompat/app/AlertDialog$Builder;

    iget-object p3, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContext:Landroid/content/Context;

    invoke-direct {p1, p3}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 p3, 0x0

    invoke-virtual {p1, p3}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    sget p3, Lcom/android/settings/R$string;->key_fsg_dialog_message:I

    invoke-virtual {p1, p3}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    sget p3, Lcom/android/settings/R$string;->key_fsg_dialog_positive:I

    invoke-virtual {p1, p3, p2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    sget p3, Lcom/android/settings/R$string;->key_fsg_dialog_negative:I

    invoke-virtual {p1, p3, p2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setCancelable(Z)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mFsgChangeDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private getAction(Ljava/lang/String;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    const-string p0, "double_click_power_key"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    sget p0, Lcom/android/settings/R$string;->double_click_power_key:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    const-string p0, "long_press_menu_key"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    sget p0, Lcom/android/settings/R$string;->long_press_menu_key:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    const-string p0, "long_press_home_key"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    sget p0, Lcom/android/settings/R$string;->long_press_home_key:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_2
    const-string p0, "long_press_back_key"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    sget p0, Lcom/android/settings/R$string;->long_press_back_key:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_3
    const-string p0, "key_combination_power_back"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    sget p0, Lcom/android/settings/R$string;->key_combination_power_back:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_4
    const-string p0, "key_combination_power_home"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    sget p0, Lcom/android/settings/R$string;->key_combination_power_home:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_5
    const-string p0, "key_combination_power_menu"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    sget p0, Lcom/android/settings/R$string;->key_combination_power_menu:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_6
    const-string p0, "long_press_menu_key_when_lock"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    sget p0, Lcom/android/settings/R$string;->long_press_menu_key_when_lock:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_7
    const-string/jumbo p0, "three_gesture_down"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    sget p0, Lcom/android/settings/R$string;->three_gesture_down:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_8
    const-string/jumbo p0, "three_gesture_long_press"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    sget p0, Lcom/android/settings/R$string;->three_gesture_long_press:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p2, 0x0

    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, p2

    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_9
    const-string p0, ""

    return-object p0
.end method

.method private getFunction(Ljava/lang/String;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 0

    const-string p0, "launch_camera"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    sget p0, Lcom/android/settings/R$string;->launch_camera:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    const-string/jumbo p0, "screen_shot"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    sget p0, Lcom/android/settings/R$string;->screen_shot:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    const-string/jumbo p0, "partial_screen_shot"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    sget p0, Lcom/android/settings/R$string;->regional_screen_shot:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_2
    const-string p0, "launch_voice_assistant"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    sget p0, Lcom/android/settings/R$string;->launch_voice_assistant:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_3
    const-string p0, "launch_google_search"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    sget p0, Lcom/android/settings/R$string;->launch_google_search:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_4
    const-string p0, "go_to_sleep"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    sget p0, Lcom/android/settings/R$string;->go_to_sleep:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_5
    const-string/jumbo p0, "turn_on_torch"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    sget p0, Lcom/android/settings/R$string;->turn_on_torch:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_6
    const-string p0, "close_app"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    sget p0, Lcom/android/settings/R$string;->close_app:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_7
    const-string/jumbo p0, "split_screen"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    sget p0, Lcom/android/settings/R$string;->split_screen:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_8
    const-string/jumbo p0, "mi_pay"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    sget p0, Lcom/android/settings/R$string;->mi_pay:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_9
    const-string p0, "dump_log"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    sget p0, Lcom/android/settings/R$string;->dump_log:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_a
    const-string/jumbo p0, "show_menu"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_b

    sget p0, Lcom/android/settings/R$string;->show_menu:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_b
    const-string p0, "launch_recents"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_c

    sget p0, Lcom/android/settings/R$string;->launch_recents:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_c
    const-string p0, "au_pay"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_d

    sget p0, Lcom/android/settings/R$string;->au_pay:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_d
    const-string p0, "google_pay"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_e

    sget p0, Lcom/android/settings/R$string;->google_pay:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_e
    const-string p0, "launch_smarthome"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_f

    sget p0, Lcom/android/settings/R$string;->launch_smarthome:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_f
    const-string p0, ""

    return-object p0
.end method

.method private getLongPressPowerPreference(Lmiuix/preference/RadioButtonPreferenceCategory;)Lmiuix/preference/RadioButtonPreference;
    .locals 3

    const/4 p0, 0x0

    :goto_0
    invoke-virtual {p1}, Landroidx/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    if-ge p0, v0, :cond_1

    invoke-virtual {p1, p0}, Landroidx/preference/PreferenceGroup;->getPreference(I)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {v0}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "long_press_power_key"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method private getResId(Ljava/lang/String;Landroid/content/res/Resources;)I
    .locals 1

    sget p0, Lcom/android/settings/R$string;->launch_camera:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return p0

    :cond_0
    sget p0, Lcom/android/settings/R$string;->screen_shot:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return p0

    :cond_1
    sget p0, Lcom/android/settings/R$string;->regional_screen_shot:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    return p0

    :cond_2
    sget p0, Lcom/android/settings/R$string;->launch_voice_assistant:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    return p0

    :cond_3
    sget p0, Lcom/android/settings/R$string;->launch_google_search:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    return p0

    :cond_4
    sget p0, Lcom/android/settings/R$string;->go_to_sleep:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    return p0

    :cond_5
    sget p0, Lcom/android/settings/R$string;->turn_on_torch:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    return p0

    :cond_6
    sget p0, Lcom/android/settings/R$string;->close_app:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    return p0

    :cond_7
    sget p0, Lcom/android/settings/R$string;->split_screen:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    return p0

    :cond_8
    sget p0, Lcom/android/settings/R$string;->mi_pay:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    return p0

    :cond_9
    sget p0, Lcom/android/settings/R$string;->dump_log:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    return p0

    :cond_a
    sget p0, Lcom/android/settings/R$string;->show_menu:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    return p0

    :cond_b
    sget p0, Lcom/android/settings/R$string;->launch_recents:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    return p0

    :cond_c
    sget p0, Lcom/android/settings/R$string;->au_pay:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    return p0

    :cond_d
    sget p0, Lcom/android/settings/R$string;->google_pay:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    return p0

    :cond_e
    sget p0, Lcom/android/settings/R$string;->launch_smarthome:I

    invoke-virtual {p2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_f

    return p0

    :cond_f
    const/4 p0, 0x0

    return p0
.end method

.method private init(Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mTitle:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/KeySettingsSelectFragment;->resources:Landroid/content/res/Resources;

    invoke-direct {p0, v0, v1}, Lcom/android/settings/KeySettingsSelectFragment;->getResId(Ljava/lang/String;Landroid/content/res/Resources;)I

    move-result v0

    sget v1, Lcom/android/settings/R$string;->launch_camera:I

    const-string v2, "KeySettings"

    const-string v3, "double_click_power_key"

    const/4 v4, 0x0

    if-ne v0, v1, :cond_0

    const-string v0, "launch_camera"

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    invoke-virtual {p1, v4, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto/16 :goto_1

    :cond_0
    sget v1, Lcom/android/settings/R$string;->screen_shot:I

    const-string/jumbo v5, "three_gesture_down"

    if-ne v0, v1, :cond_1

    const-string/jumbo v0, "screen_shot"

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    invoke-virtual {p1, v4, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto/16 :goto_1

    :cond_1
    sget v1, Lcom/android/settings/R$string;->regional_screen_shot:I

    if-ne v0, v1, :cond_2

    const-string/jumbo v0, "partial_screen_shot"

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    const-string/jumbo v0, "three_gesture_long_press"

    invoke-virtual {p1, v4, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto/16 :goto_1

    :cond_2
    sget v1, Lcom/android/settings/R$string;->launch_voice_assistant:I

    const-string v6, "long_press_power_key"

    if-ne v0, v1, :cond_3

    const-string v0, "launch_voice_assistant"

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/settings/R$bool;->config_has_aikey:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_10

    invoke-virtual {p1, v4, v6}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto/16 :goto_1

    :cond_3
    sget v1, Lcom/android/settings/R$string;->launch_google_search:I

    if-ne v0, v1, :cond_4

    const-string p1, "launch_google_search"

    iput-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    goto/16 :goto_1

    :cond_4
    sget v1, Lcom/android/settings/R$string;->go_to_sleep:I

    const-string v7, "key_combination_power_menu"

    const-string v8, "key_combination_power_home"

    const-string v9, "key_combination_power_back"

    if-ne v0, v1, :cond_5

    const-string v0, "go_to_sleep"

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p1, v8}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_5
    sget v1, Lcom/android/settings/R$string;->turn_on_torch:I

    if-ne v0, v1, :cond_6

    const-string/jumbo v0, "turn_on_torch"

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    invoke-virtual {p1, v4, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    const-string/jumbo v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v0

    :try_start_0
    invoke-interface {v0, v4}, Landroid/view/IWindowManager;->hasNavigationBar(I)Z

    move-result v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    move v0, v4

    :goto_0
    if-nez v0, :cond_10

    const-string v0, "long_press_menu_key_when_lock"

    invoke-virtual {p1, v4, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto/16 :goto_1

    :cond_6
    sget v1, Lcom/android/settings/R$string;->close_app:I

    if-ne v0, v1, :cond_7

    const-string p1, "close_app"

    iput-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    goto/16 :goto_1

    :cond_7
    sget v1, Lcom/android/settings/R$string;->split_screen:I

    if-ne v0, v1, :cond_8

    const-string/jumbo v0, "split_screen"

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p1, v8}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_8
    sget v1, Lcom/android/settings/R$string;->mi_pay:I

    if-ne v0, v1, :cond_9

    const-string/jumbo v0, "mi_pay"

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    const-string v0, "long_press_home_key"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const-string v0, "long_press_menu_key"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const-string v0, "long_press_back_key"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p1, v8}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p1, v4, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto/16 :goto_1

    :cond_9
    sget v1, Lcom/android/settings/R$string;->dump_log:I

    if-ne v0, v1, :cond_a

    const-string v0, "dump_log"

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    invoke-virtual {p1, v4, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_1

    :cond_a
    sget v1, Lcom/android/settings/R$string;->show_menu:I

    if-ne v0, v1, :cond_b

    const-string/jumbo p1, "show_menu"

    iput-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    goto :goto_1

    :cond_b
    sget v1, Lcom/android/settings/R$string;->launch_recents:I

    if-ne v0, v1, :cond_c

    const-string p1, "launch_recents"

    iput-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    goto :goto_1

    :cond_c
    sget v1, Lcom/android/settings/R$string;->au_pay:I

    const-string v4, "key_none"

    if-ne v0, v1, :cond_d

    const-string v0, "au_pay"

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_d
    sget v1, Lcom/android/settings/R$string;->google_pay:I

    if-ne v0, v1, :cond_e

    const-string v0, "google_pay"

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_e
    sget v1, Lcom/android/settings/R$string;->launch_smarthome:I

    if-ne v0, v1, :cond_f

    const-string v0, "launch_smarthome"

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_f
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "not found titleId"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    :cond_10
    :goto_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "mPreferenceKey = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v2, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private isNeedFsgDlg(Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "force_fsg_nav_bar"

    invoke-static {p0, v0}, Landroid/provider/MiuiSettings$Global;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    const-string p0, "double_click_power_key"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_0

    const-string p0, "long_press_power_key"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_0

    const-string/jumbo p0, "three_gesture_down"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_0

    const-string/jumbo p0, "three_gesture_long_press"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_0

    const-string p0, "key_none"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private performFsgChange(Ljava/lang/String;Lmiuix/preference/RadioButtonPreference;)V
    .locals 3

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "force_fsg_nav_bar"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/MiuiSettings$Global;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    invoke-direct {p0, p1, p2}, Lcom/android/settings/KeySettingsSelectFragment;->performSettingsChange(Ljava/lang/String;Lmiuix/preference/RadioButtonPreference;)V

    return-void
.end method

.method private performSettingsChange(Ljava/lang/String;Lmiuix/preference/RadioButtonPreference;)V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mKeyGestureFunctionOptional:Lmiuix/preference/RadioButtonPreferenceCategory;

    invoke-virtual {v0, p2}, Lmiuix/preference/RadioButtonPreferenceCategory;->setCheckedPreference(Landroidx/preference/Preference;)V

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mHidedRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    if-eqz v0, :cond_0

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mHidedRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {v0}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "key_none"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, -0x2

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/KeySettingsSelectFragment;->mRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {v3}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "none"

    invoke-static {v0, v3, v4, v2}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    const-string v3, "launch_voice_assistant"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {v0}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const-string v3, "long_press_power_key"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const-string v4, "long_press_power_launch_xiaoai"

    invoke-static {v0, v4, v3, v2}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    :cond_1
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    invoke-static {v0, p1, v1, v2}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    :cond_2
    iput-object p2, p0, Lcom/android/settings/KeySettingsSelectFragment;->mRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    iget-object p0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mKeyGestureFunctionPreview:Lcom/android/settings/KeySettingsPreviewPreference;

    invoke-virtual {p2}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/android/settings/KeySettingsPreviewPreference;->setCheckedAction(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 0

    const-class p0, Lcom/android/settings/KeySettingsSelectFragment;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 8

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    sget p1, Lcom/android/settings/R$xml;->key_settings_select_fragment:I

    invoke-virtual {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->resources:Landroid/content/res/Resources;

    sget v1, Lcom/android/settings/R$string;->launch_voice_assistant:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, ":settings:show_fragment_title"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mTitle:Ljava/lang/String;

    :cond_0
    iget-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->resources:Landroid/content/res/Resources;

    sget v0, Lcom/android/settings/R$array;->key_and_gesture_shortcut_action:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    array-length v1, p1

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_1

    aget-object v4, p1, v3

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0, v0}, Lcom/android/settings/KeySettingsSelectFragment;->init(Ljava/util/ArrayList;)V

    const-string p1, "key_gesture_function_preview"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/KeySettingsPreviewPreference;

    iput-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mKeyGestureFunctionPreview:Lcom/android/settings/KeySettingsPreviewPreference;

    iget-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    const-string v1, "dump_log"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    const/4 v1, 0x1

    if-eqz p1, :cond_2

    const-string p1, "key_gesture_function_category"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/PreferenceCategory;

    iput-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mKeyGestureFunctionCategory:Landroidx/preference/PreferenceCategory;

    iget-object v3, p0, Lcom/android/settings/KeySettingsSelectFragment;->mKeyGestureFunctionPreview:Lcom/android/settings/KeySettingsPreviewPreference;

    invoke-virtual {p1, v3}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    iget-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mKeyGestureFunctionCategory:Landroidx/preference/PreferenceCategory;

    sget v3, Lcom/android/settings/R$string;->dump_log_title:I

    invoke-virtual {p1, v3}, Landroidx/preference/Preference;->setTitle(I)V

    new-instance p1, Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    invoke-virtual {p0}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->getPrefContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {p1, v3}, Lcom/android/settingslib/miuisettings/preference/ValuePreference;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/android/settings/R$string;->dump_log_summary:I

    new-array v5, v1, [Ljava/lang/Object;

    const/16 v6, 0x11c

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v2}, Lcom/android/settingslib/miuisettings/preference/ValuePreference;->setShowRightArrow(Z)V

    invoke-virtual {p1, v2}, Landroidx/preference/Preference;->setSelectable(Z)V

    iget-object v3, p0, Lcom/android/settings/KeySettingsSelectFragment;->mKeyGestureFunctionCategory:Landroidx/preference/PreferenceCategory;

    invoke-virtual {v3, p1}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    :cond_2
    const-string p1, "key_gesture_function_optional"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lmiuix/preference/RadioButtonPreferenceCategory;

    iput-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mKeyGestureFunctionOptional:Lmiuix/preference/RadioButtonPreferenceCategory;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_3
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const-string v3, "key_none"

    const-string/jumbo v4, "three_gesture_long_press"

    const-string v5, "long_press_power_key"

    if-eqz v0, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v6, Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {p0}, Lcom/android/settings/core/InstrumentedPreferenceFragment;->getPrefContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Lmiuix/preference/RadioButtonPreference;-><init>(Landroid/content/Context;)V

    sget v7, Lcom/android/settings/R$layout;->miuix_preference_radiobutton_two_state_background:I

    invoke-virtual {v6, v7}, Landroidx/preference/Preference;->setLayoutResource(I)V

    invoke-virtual {v6, v0}, Landroidx/preference/Preference;->setKey(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v4, p0, Lcom/android/settings/KeySettingsSelectFragment;->resources:Landroid/content/res/Resources;

    sget v5, Lcom/android/settings/R$string;->long_press_power_key_tips:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v6, v4}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_4
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/android/settings/KeySettingsSelectFragment;->resources:Landroid/content/res/Resources;

    sget v5, Lcom/android/settings/R$string;->three_gesture_long_press:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v1, [Ljava/lang/Object;

    const/4 v7, 0x3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_5
    iget-object v4, p0, Lcom/android/settings/KeySettingsSelectFragment;->resources:Landroid/content/res/Resources;

    iget-object v5, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v7, "string"

    invoke-virtual {v4, v0, v7, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v6, v4}, Landroidx/preference/Preference;->setTitle(I)V

    :goto_2
    invoke-virtual {v6, v2}, Landroidx/preference/Preference;->setPersistent(Z)V

    iget-object v4, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContext:Landroid/content/Context;

    invoke-static {v4, v0}, Landroid/provider/MiuiSettings$Key;->getKeyAndGestureShortcutFunction(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/android/settings/KeySettingsSelectFragment;->mKeyGestureFunctionOptional:Lmiuix/preference/RadioButtonPreferenceCategory;

    invoke-virtual {v5, v6}, Lmiuix/preference/RadioButtonPreferenceCategory;->addPreference(Landroidx/preference/Preference;)Z

    iget-object v5, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    iput-object v6, p0, Lcom/android/settings/KeySettingsSelectFragment;->mRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    :cond_6
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    if-nez v0, :cond_3

    iput-object v6, p0, Lcom/android/settings/KeySettingsSelectFragment;->mRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    goto/16 :goto_1

    :cond_7
    iget-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string v0, "force_fsg_nav_bar"

    invoke-static {p1, v0}, Landroid/provider/MiuiSettings$Global;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_8

    iget-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    const-string v0, "double_click_power_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_8

    iget-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_8

    iget-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    const-string/jumbo v0, "three_gesture_down"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_8

    iget-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_8

    iget-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_8

    iget-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    iput-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mHidedRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    iget-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mKeyGestureFunctionOptional:Lmiuix/preference/RadioButtonPreferenceCategory;

    invoke-virtual {p1}, Landroidx/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    sub-int/2addr v0, v1

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->getPreference(I)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lmiuix/preference/RadioButtonPreference;

    iput-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    :cond_8
    iget-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mKeyGestureFunctionOptional:Lmiuix/preference/RadioButtonPreferenceCategory;

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {p1, v0}, Lmiuix/preference/RadioButtonPreferenceCategory;->setCheckedPreference(Landroidx/preference/Preference;)V

    iget-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mKeyGestureFunctionPreview:Lcom/android/settings/KeySettingsPreviewPreference;

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/android/settings/KeySettingsPreviewPreference;->setPreferenceKey(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mKeyGestureFunctionPreview:Lcom/android/settings/KeySettingsPreviewPreference;

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {v0}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/settings/KeySettingsPreviewPreference;->setCheckedAction(Ljava/lang/String;)V

    new-instance p1, Lcom/android/settings/KeySettingsSelectFragment$1;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p1, p0, v0}, Lcom/android/settings/KeySettingsSelectFragment$1;-><init>(Lcom/android/settings/KeySettingsSelectFragment;Landroid/os/Handler;)V

    iput-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContentObserver:Landroid/database/ContentObserver;

    iget-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string v0, "long_press_power_launch_xiaoai"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object p0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContentObserver:Landroid/database/ContentObserver;

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v2, p0, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onAttach(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->resources:Landroid/content/res/Resources;

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    iget-object p1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/MiuiShortcut$System;->isSupportNewVersionKeySettings(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    sget-boolean p1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const-string v0, ":settings:show_fragment_title"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.android.settings"

    const-string v3, "com.android.settings.SubSettings"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v1, ":settings:show_fragment"

    const-string v2, "com.android.settings.GestureShortcutSettingsSelectFragment"

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "voice_assist"

    invoke-static {v2, v1}, Lcom/android/settings/MiuiShortcut$Key;->getResourceForKey(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->finish()V

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mKeyGestureFunctionPreview:Lcom/android/settings/KeySettingsPreviewPreference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settings/KeySettingsPreviewPreference;->destroy()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mActionChangeDialog:Lmiuix/appcompat/app/AlertDialog;

    iput-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mFsgChangeDialog:Lmiuix/appcompat/app/AlertDialog;

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContentObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    :cond_1
    invoke-super {p0}, Lcom/android/settingslib/core/lifecycle/ObservablePreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onPreferenceTreeClick(Landroidx/preference/PreferenceScreen;Landroidx/preference/Preference;)Z
    .locals 5

    instance-of v0, p2, Lmiuix/preference/RadioButtonPreference;

    if-eqz v0, :cond_3

    move-object v0, p2

    check-cast v0, Lmiuix/preference/RadioButtonPreference;

    iget-object v1, p0, Lcom/android/settings/KeySettingsSelectFragment;->mRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    if-eq v1, v0, :cond_3

    invoke-virtual {v0}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/KeySettingsSelectFragment;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Landroid/provider/MiuiSettings$Key;->getKeyAndGestureShortcutFunction(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string/jumbo v3, "none"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lmiui/os/Build;->hasCameraFlash(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string/jumbo v3, "turn_on_torch"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/android/settings/KeySettingsSelectFragment;->mPreferenceKey:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/settings/KeySettingsSelectFragment;->mKeyGestureFunctionOptional:Lmiuix/preference/RadioButtonPreferenceCategory;

    iget-object v4, p0, Lcom/android/settings/KeySettingsSelectFragment;->mRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {v3, v4}, Lmiuix/preference/RadioButtonPreferenceCategory;->setCheckedPreference(Landroidx/preference/Preference;)V

    invoke-direct {p0, v1, v2, v0}, Lcom/android/settings/KeySettingsSelectFragment;->bringUpActionChooseDlg(Ljava/lang/String;Ljava/lang/String;Lmiuix/preference/RadioButtonPreference;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, v1}, Lcom/android/settings/KeySettingsSelectFragment;->isNeedFsgDlg(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v0, p0, Lcom/android/settings/KeySettingsSelectFragment;->mRadioButtonPreference:Lmiuix/preference/RadioButtonPreference;

    invoke-direct {p0, v1, v2, v0}, Lcom/android/settings/KeySettingsSelectFragment;->bringUpFsgChooseDlg(Ljava/lang/String;Ljava/lang/String;Lmiuix/preference/RadioButtonPreference;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v1, v0}, Lcom/android/settings/KeySettingsSelectFragment;->performSettingsChange(Ljava/lang/String;Lmiuix/preference/RadioButtonPreference;)V

    :cond_3
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/android/settings/SettingsPreferenceFragment;->onPreferenceTreeClick(Landroidx/preference/PreferenceScreen;Landroidx/preference/Preference;)Z

    move-result p0

    return p0
.end method
