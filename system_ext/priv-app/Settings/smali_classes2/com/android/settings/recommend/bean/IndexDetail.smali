.class public Lcom/android/settings/recommend/bean/IndexDetail;
.super Ljava/lang/Object;


# instance fields
.field private controller:Lcom/android/settingslib/core/AbstractPreferenceController;

.field private intent:Ljava/lang/String;

.field private rawIntent:Landroid/content/Intent;

.field private resId:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/settings/recommend/bean/IndexDetail;->resId:I

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/settings/recommend/bean/IndexDetail;->resId:I

    iput-object p2, p0, Lcom/android/settings/recommend/bean/IndexDetail;->intent:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Lcom/android/settingslib/core/AbstractPreferenceController;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/settings/recommend/bean/IndexDetail;->resId:I

    iput-object p2, p0, Lcom/android/settings/recommend/bean/IndexDetail;->intent:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/settings/recommend/bean/IndexDetail;->controller:Lcom/android/settingslib/core/AbstractPreferenceController;

    return-void
.end method


# virtual methods
.method public getController()Lcom/android/settingslib/core/AbstractPreferenceController;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/recommend/bean/IndexDetail;->controller:Lcom/android/settingslib/core/AbstractPreferenceController;

    return-object p0
.end method

.method public getIntent()Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/recommend/bean/IndexDetail;->intent:Ljava/lang/String;

    return-object p0
.end method

.method public getRawIntent()Landroid/content/Intent;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/recommend/bean/IndexDetail;->rawIntent:Landroid/content/Intent;

    return-object p0
.end method

.method public getResId()I
    .locals 0

    iget p0, p0, Lcom/android/settings/recommend/bean/IndexDetail;->resId:I

    return p0
.end method

.method public setController(Lcom/android/settingslib/core/AbstractPreferenceController;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/recommend/bean/IndexDetail;->controller:Lcom/android/settingslib/core/AbstractPreferenceController;

    return-void
.end method

.method public setIntent(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/recommend/bean/IndexDetail;->intent:Ljava/lang/String;

    return-void
.end method

.method public setRawIntent(Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/recommend/bean/IndexDetail;->rawIntent:Landroid/content/Intent;

    return-void
.end method

.method public setResId(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/recommend/bean/IndexDetail;->resId:I

    return-void
.end method
