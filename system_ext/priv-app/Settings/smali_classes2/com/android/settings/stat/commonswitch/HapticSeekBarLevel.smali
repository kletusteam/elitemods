.class public Lcom/android/settings/stat/commonswitch/HapticSeekBarLevel;
.super Lcom/android/settings/stat/commonswitch/SwitchStat;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/stat/commonswitch/SwitchStat;-><init>()V

    return-void
.end method

.method private getHapticLevel(Landroid/content/Context;)F
    .locals 1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string p1, "haptic_feedback_infinite_intensity"

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p0, p1, v0}, Landroid/provider/Settings$System;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result p0

    return p0
.end method


# virtual methods
.method getInfoList(Landroid/content/Context;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/settings/stat/commonswitch/SwitchStat$Info;",
            ">;"
        }
    .end annotation

    goto/32 :goto_22

    nop

    :goto_0
    if-ltz v3, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_15

    nop

    :goto_1
    const/4 v5, 0x0

    goto/32 :goto_0

    nop

    :goto_2
    if-gtz p1, :cond_1

    goto/32 :goto_19

    :cond_1
    goto/32 :goto_18

    nop

    :goto_3
    invoke-direct {v1, p0, v6, v2}, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;-><init>(Lcom/android/settings/stat/commonswitch/SwitchStat;Ljava/lang/String;Z)V

    goto/32 :goto_26

    nop

    :goto_4
    const v3, 0x3f95c28f    # 1.17f

    goto/32 :goto_21

    nop

    :goto_5
    const-string v6, "haptic_seek_bar_level_mid"

    goto/32 :goto_3

    nop

    :goto_6
    cmpg-float v3, p1, v2

    goto/32 :goto_1b

    nop

    :goto_7
    cmpg-float v2, p1, v3

    goto/32 :goto_11

    nop

    :goto_8
    invoke-direct {v1, p0, p1, v4}, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;-><init>(Lcom/android/settings/stat/commonswitch/SwitchStat;Ljava/lang/String;Z)V

    goto/32 :goto_13

    nop

    :goto_9
    move v2, v4

    goto/32 :goto_23

    nop

    :goto_a
    new-instance v1, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;

    goto/32 :goto_e

    nop

    :goto_b
    invoke-direct {v1, p0, v6, v3}, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;-><init>(Lcom/android/settings/stat/commonswitch/SwitchStat;Ljava/lang/String;Z)V

    goto/32 :goto_1c

    nop

    :goto_c
    goto :goto_1f

    :goto_d
    goto/32 :goto_1e

    nop

    :goto_e
    cmpl-float v2, p1, v2

    goto/32 :goto_4

    nop

    :goto_f
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_25

    nop

    :goto_10
    const-string v6, "haptic_seek_bar_level_low"

    goto/32 :goto_b

    nop

    :goto_11
    if-lez v2, :cond_2

    goto/32 :goto_24

    :cond_2
    goto/32 :goto_9

    nop

    :goto_12
    const-string p1, "haptic_seek_bar_level_high"

    goto/32 :goto_8

    nop

    :goto_13
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_20

    nop

    :goto_14
    const v2, 0x3f547ae1    # 0.83f

    goto/32 :goto_6

    nop

    :goto_15
    move v3, v4

    goto/32 :goto_c

    nop

    :goto_16
    move v4, v5

    :goto_17
    goto/32 :goto_12

    nop

    :goto_18
    goto :goto_17

    :goto_19
    goto/32 :goto_16

    nop

    :goto_1a
    new-instance v1, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;

    goto/32 :goto_14

    nop

    :goto_1b
    const/4 v4, 0x1

    goto/32 :goto_1

    nop

    :goto_1c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_a

    nop

    :goto_1d
    cmpl-float p1, p1, v3

    goto/32 :goto_2

    nop

    :goto_1e
    move v3, v5

    :goto_1f
    goto/32 :goto_10

    nop

    :goto_20
    return-object v0

    :goto_21
    if-gez v2, :cond_3

    goto/32 :goto_24

    :cond_3
    goto/32 :goto_7

    nop

    :goto_22
    invoke-direct {p0, p1}, Lcom/android/settings/stat/commonswitch/HapticSeekBarLevel;->getHapticLevel(Landroid/content/Context;)F

    move-result p1

    goto/32 :goto_f

    nop

    :goto_23
    goto :goto_29

    :goto_24
    goto/32 :goto_28

    nop

    :goto_25
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_1a

    nop

    :goto_26
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_27

    nop

    :goto_27
    new-instance v1, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;

    goto/32 :goto_1d

    nop

    :goto_28
    move v2, v5

    :goto_29
    goto/32 :goto_5

    nop
.end method
