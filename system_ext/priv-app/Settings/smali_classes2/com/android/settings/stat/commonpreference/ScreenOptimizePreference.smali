.class public Lcom/android/settings/stat/commonpreference/ScreenOptimizePreference;
.super Lcom/android/settings/stat/commonpreference/PreferenceStat;


# static fields
.field private static final DEFAULT_EXPERT_COLOR_GAMUT:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "expert_gamut_default"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/settings/stat/commonpreference/ScreenOptimizePreference;->DEFAULT_EXPERT_COLOR_GAMUT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/stat/commonpreference/PreferenceStat;-><init>()V

    return-void
.end method

.method public static getGamutFromDatabase(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "expert_data"

    invoke-static {p0, v0}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget p0, Lcom/android/settings/stat/commonpreference/ScreenOptimizePreference;->DEFAULT_EXPERT_COLOR_GAMUT:I

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p0, "color_gamut"

    invoke-virtual {v0, p0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result p0

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Lorg/json/JSONException;->printStackTrace()V

    const/4 p0, 0x0

    return-object p0
.end method

.method private static getScreenOptimizeMode(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "screen_optimize_mode"

    invoke-static {p0, v0}, Landroid/provider/MiuiSettings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method getInfoList(Landroid/content/Context;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/settings/stat/commonpreference/PreferenceStat$Info;",
            ">;"
        }
    .end annotation

    goto/32 :goto_3

    nop

    :goto_0
    new-instance v1, Lcom/android/settings/stat/commonpreference/PreferenceStat$Info;

    goto/32 :goto_5

    nop

    :goto_1
    invoke-direct {v1, p0, v2, p1}, Lcom/android/settings/stat/commonpreference/PreferenceStat$Info;-><init>(Lcom/android/settings/stat/commonpreference/PreferenceStat;Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_c

    nop

    :goto_2
    new-instance v2, Lcom/android/settings/stat/commonpreference/PreferenceStat$Info;

    goto/32 :goto_12

    nop

    :goto_3
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_9

    nop

    :goto_4
    invoke-static {p1}, Lcom/android/settings/stat/commonpreference/ScreenOptimizePreference;->getGamutFromDatabase(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_8

    nop

    :goto_5
    const-string v2, "color_gamut"

    goto/32 :goto_1

    nop

    :goto_6
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_7
    goto/32 :goto_4

    nop

    :goto_8
    const-string v2, "4"

    goto/32 :goto_13

    nop

    :goto_9
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_11

    nop

    :goto_a
    return-object v0

    :goto_b
    if-nez v1, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_14

    nop

    :goto_c
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_d
    goto/32 :goto_a

    nop

    :goto_e
    if-nez v1, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_2

    nop

    :goto_f
    invoke-direct {v2, p0, v4, v3}, Lcom/android/settings/stat/commonpreference/PreferenceStat$Info;-><init>(Lcom/android/settings/stat/commonpreference/PreferenceStat;Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_6

    nop

    :goto_10
    const-string/jumbo v4, "screen_optimize_mode"

    goto/32 :goto_f

    nop

    :goto_11
    invoke-static {p1}, Lcom/android/settings/stat/commonpreference/ScreenOptimizePreference;->getScreenOptimizeMode(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_e

    nop

    :goto_12
    invoke-static {p1}, Lcom/android/settings/stat/commonpreference/ScreenOptimizePreference;->getScreenOptimizeMode(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_10

    nop

    :goto_13
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto/32 :goto_b

    nop

    :goto_14
    if-nez p1, :cond_2

    goto/32 :goto_d

    :cond_2
    goto/32 :goto_0

    nop
.end method
