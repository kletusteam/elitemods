.class public abstract Lcom/android/settings/stat/commonswitch/SwitchStat;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/stat/commonswitch/SwitchStat$Info;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method getInfo(Landroid/content/Context;)Lcom/android/settings/stat/commonswitch/SwitchStat$Info;
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-object p0

    :goto_1
    const/4 p0, 0x0

    goto/32 :goto_0

    nop
.end method

.method getInfoList(Landroid/content/Context;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/settings/stat/commonswitch/SwitchStat$Info;",
            ">;"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    return-object p0

    :goto_1
    const/4 p0, 0x0

    goto/32 :goto_0

    nop
.end method

.method public track(Landroid/content/Context;)V
    .locals 4

    invoke-virtual {p0, p1}, Lcom/android/settings/stat/commonswitch/SwitchStat;->getInfoList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;

    iget-object v2, v1, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;->key:Ljava/lang/String;

    iget-boolean v3, v1, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;->value:Z

    invoke-static {v2, v3}, Lcom/android/settingslib/util/MiStatInterfaceUtils;->trackSwitchEvent(Ljava/lang/String;Z)V

    iget-object v2, v1, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;->key:Ljava/lang/String;

    iget-boolean v1, v1, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;->value:Z

    invoke-static {v2, v1}, Lcom/android/settingslib/util/OneTrackInterfaceUtils;->trackSwitchEvent(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/settings/stat/commonswitch/SwitchStat;->getInfo(Landroid/content/Context;)Lcom/android/settings/stat/commonswitch/SwitchStat$Info;

    move-result-object p0

    if-eqz p0, :cond_1

    iget-object p1, p0, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;->key:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;->value:Z

    invoke-static {p1, v0}, Lcom/android/settingslib/util/MiStatInterfaceUtils;->trackSwitchEvent(Ljava/lang/String;Z)V

    iget-object p1, p0, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;->key:Ljava/lang/String;

    iget-boolean p0, p0, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;->value:Z

    invoke-static {p1, p0}, Lcom/android/settingslib/util/OneTrackInterfaceUtils;->trackSwitchEvent(Ljava/lang/String;Z)V

    :cond_1
    return-void
.end method
