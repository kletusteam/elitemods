.class public Lcom/android/settings/stat/commonswitch/HapticSwitch;
.super Lcom/android/settings/stat/commonswitch/SwitchStat;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/stat/commonswitch/SwitchStat;-><init>()V

    return-void
.end method


# virtual methods
.method getInfo(Landroid/content/Context;)Lcom/android/settings/stat/commonswitch/SwitchStat$Info;
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    invoke-static {p1}, Lcom/android/settings/MiuiSoundSettings;->isSystemHapticEnable(Landroid/content/Context;)Z

    move-result p1

    goto/32 :goto_2

    nop

    :goto_1
    return-object v0

    :goto_2
    const-string v1, "haptic_switch"

    goto/32 :goto_4

    nop

    :goto_3
    new-instance v0, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;

    goto/32 :goto_0

    nop

    :goto_4
    invoke-direct {v0, p0, v1, p1}, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;-><init>(Lcom/android/settings/stat/commonswitch/SwitchStat;Ljava/lang/String;Z)V

    goto/32 :goto_1

    nop
.end method
