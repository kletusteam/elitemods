.class public Lcom/android/settings/stat/commonswitch/BrightnessModeSwitch;
.super Lcom/android/settings/stat/commonswitch/SwitchStat;


# instance fields
.field private mSunlightModeAvailable:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/stat/commonswitch/SwitchStat;-><init>()V

    return-void
.end method

.method private isAutomaticBrightnessModeEnable(Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo p1, "screen_brightness_mode"

    const/4 v0, 0x0

    const/4 v1, -0x2

    invoke-static {p0, p1, v0, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result p0

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private isSunlightModeSettingsEnable(Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo p1, "sunlight_mode"

    const/4 v0, 0x0

    const/4 v1, -0x2

    invoke-static {p0, p1, v0, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result p0

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private smoothAdjustLightAvailable()Z
    .locals 1

    const-string/jumbo p0, "support_backlight_bit_switch"

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result p0

    return p0
.end method

.method private sunlightModeAvailiable(Landroid/content/Context;)Z
    .locals 1

    const-string p1, "config_sunlight_mode_available"

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/settings/stat/commonswitch/BrightnessModeSwitch;->mSunlightModeAvailable:Z

    return p1
.end method


# virtual methods
.method getInfoList(Landroid/content/Context;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/settings/stat/commonswitch/SwitchStat$Info;",
            ">;"
        }
    .end annotation

    goto/32 :goto_1a

    nop

    :goto_0
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    goto/32 :goto_11

    nop

    :goto_2
    if-nez v3, :cond_0

    goto/32 :goto_15

    :cond_0
    goto/32 :goto_6

    nop

    :goto_3
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_4
    goto/32 :goto_1f

    nop

    :goto_5
    if-nez p1, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_1b

    nop

    :goto_6
    invoke-direct {p0, p1}, Lcom/android/settings/stat/commonswitch/BrightnessModeSwitch;->isAutomaticBrightnessModeEnable(Landroid/content/Context;)Z

    move-result p1

    goto/32 :goto_21

    nop

    :goto_7
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_d

    nop

    :goto_8
    const-string/jumbo v1, "persist.vendor.light.bit.switch"

    goto/32 :goto_10

    nop

    :goto_9
    const-string/jumbo v3, "screen_brightness_mode"

    goto/32 :goto_16

    nop

    :goto_a
    new-instance v1, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;

    goto/32 :goto_20

    nop

    :goto_b
    const-string/jumbo v2, "smooth_adjust_light_mode"

    goto/32 :goto_12

    nop

    :goto_c
    invoke-direct {p0, p1}, Lcom/android/settings/stat/commonswitch/BrightnessModeSwitch;->isSunlightModeSettingsEnable(Landroid/content/Context;)Z

    move-result v3

    goto/32 :goto_2

    nop

    :goto_d
    invoke-direct {p0, p1}, Lcom/android/settings/stat/commonswitch/BrightnessModeSwitch;->sunlightModeAvailiable(Landroid/content/Context;)Z

    move-result v1

    goto/32 :goto_f

    nop

    :goto_e
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_a

    nop

    :goto_f
    const/4 v2, 0x0

    goto/32 :goto_17

    nop

    :goto_10
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    goto/32 :goto_b

    nop

    :goto_11
    invoke-direct {p0}, Lcom/android/settings/stat/commonswitch/BrightnessModeSwitch;->smoothAdjustLightAvailable()Z

    move-result p1

    goto/32 :goto_5

    nop

    :goto_12
    invoke-direct {p1, p0, v2, v1}, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;-><init>(Lcom/android/settings/stat/commonswitch/SwitchStat;Ljava/lang/String;Z)V

    goto/32 :goto_3

    nop

    :goto_13
    const/4 p1, 0x1

    goto/32 :goto_14

    nop

    :goto_14
    goto :goto_19

    :goto_15
    goto/32 :goto_18

    nop

    :goto_16
    invoke-direct {v1, p0, v3, v2}, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;-><init>(Lcom/android/settings/stat/commonswitch/SwitchStat;Ljava/lang/String;Z)V

    goto/32 :goto_7

    nop

    :goto_17
    if-nez v1, :cond_2

    goto/32 :goto_1

    :cond_2
    goto/32 :goto_1d

    nop

    :goto_18
    move p1, v2

    :goto_19
    goto/32 :goto_1e

    nop

    :goto_1a
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_e

    nop

    :goto_1b
    new-instance p1, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;

    goto/32 :goto_8

    nop

    :goto_1c
    invoke-direct {v1, p0, v3, p1}, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;-><init>(Lcom/android/settings/stat/commonswitch/SwitchStat;Ljava/lang/String;Z)V

    goto/32 :goto_0

    nop

    :goto_1d
    new-instance v1, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;

    goto/32 :goto_c

    nop

    :goto_1e
    const-string/jumbo v3, "sunlight_mode"

    goto/32 :goto_1c

    nop

    :goto_1f
    return-object v0

    :goto_20
    invoke-direct {p0, p1}, Lcom/android/settings/stat/commonswitch/BrightnessModeSwitch;->isAutomaticBrightnessModeEnable(Landroid/content/Context;)Z

    move-result v2

    goto/32 :goto_9

    nop

    :goto_21
    if-nez p1, :cond_3

    goto/32 :goto_15

    :cond_3
    goto/32 :goto_13

    nop
.end method
