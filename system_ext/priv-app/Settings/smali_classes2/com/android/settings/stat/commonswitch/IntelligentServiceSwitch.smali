.class public Lcom/android/settings/stat/commonswitch/IntelligentServiceSwitch;
.super Lcom/android/settings/stat/commonswitch/SwitchStat;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/stat/commonswitch/SwitchStat;-><init>()V

    return-void
.end method

.method private static isIntelligentRingtoneEnable(Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "intelligent_recognition_service"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/provider/MiuiSettings$System;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result p0

    return p0
.end method

.method private static isIntelligentRingtoneSlot2Enable(Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "intelligent_recognition_service_slot2"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/provider/MiuiSettings$System;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result p0

    return p0
.end method


# virtual methods
.method getInfoList(Landroid/content/Context;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/settings/stat/commonswitch/SwitchStat$Info;",
            ">;"
        }
    .end annotation

    goto/32 :goto_7

    nop

    :goto_0
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_a

    nop

    :goto_1
    const-string v3, "intelligent_recognition_service"

    goto/32 :goto_8

    nop

    :goto_2
    new-instance v1, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;

    goto/32 :goto_b

    nop

    :goto_3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_c

    nop

    :goto_4
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_2

    nop

    :goto_5
    invoke-direct {v1, p0, v2, p1}, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;-><init>(Lcom/android/settings/stat/commonswitch/SwitchStat;Ljava/lang/String;Z)V

    goto/32 :goto_0

    nop

    :goto_6
    const-string v2, "intelligent_recognition_service_slot2"

    goto/32 :goto_5

    nop

    :goto_7
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_3

    nop

    :goto_8
    invoke-direct {v1, p0, v3, v2}, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;-><init>(Lcom/android/settings/stat/commonswitch/SwitchStat;Ljava/lang/String;Z)V

    goto/32 :goto_4

    nop

    :goto_9
    invoke-static {p1}, Lcom/android/settings/stat/commonswitch/IntelligentServiceSwitch;->isIntelligentRingtoneEnable(Landroid/content/Context;)Z

    move-result v2

    goto/32 :goto_1

    nop

    :goto_a
    return-object v0

    :goto_b
    invoke-static {p1}, Lcom/android/settings/stat/commonswitch/IntelligentServiceSwitch;->isIntelligentRingtoneSlot2Enable(Landroid/content/Context;)Z

    move-result p1

    goto/32 :goto_6

    nop

    :goto_c
    new-instance v1, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;

    goto/32 :goto_9

    nop
.end method
