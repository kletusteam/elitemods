.class public Lcom/android/settings/stat/commonswitch/ScreenEnhanceEngineSwitch;
.super Lcom/android/settings/stat/commonswitch/SwitchStat;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/stat/commonswitch/SwitchStat;-><init>()V

    return-void
.end method


# virtual methods
.method getInfoList(Landroid/content/Context;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/settings/stat/commonswitch/SwitchStat$Info;",
            ">;"
        }
    .end annotation

    goto/32 :goto_28

    nop

    :goto_0
    invoke-static {}, Lcom/android/settings/display/ScreenEnhanceEngineStatusCheck;->isSrForVideoSupport()Z

    move-result v1

    goto/32 :goto_a

    nop

    :goto_1
    new-instance v1, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;

    goto/32 :goto_6

    nop

    :goto_2
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_0

    nop

    :goto_3
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_4
    goto/32 :goto_22

    nop

    :goto_5
    invoke-direct {v1, p0, v3, v2}, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;-><init>(Lcom/android/settings/stat/commonswitch/SwitchStat;Ljava/lang/String;Z)V

    goto/32 :goto_3

    nop

    :goto_6
    invoke-static {p1}, Lcom/android/settings/display/ScreenEnhanceEngineStatusCheck;->getSrForImageStatus(Landroid/content/Context;)Z

    move-result v2

    goto/32 :goto_23

    nop

    :goto_7
    invoke-static {p1}, Lcom/android/settings/display/ScreenEnhanceEngineStatusCheck;->getMemcStatus(Landroid/content/Context;)Z

    move-result p1

    goto/32 :goto_20

    nop

    :goto_8
    invoke-static {p1}, Lcom/android/settings/display/ScreenEnhanceEngineStatusCheck;->getAiStatus(Landroid/content/Context;)Z

    move-result v2

    goto/32 :goto_25

    nop

    :goto_9
    invoke-static {p1}, Lcom/android/settings/display/ScreenEnhanceEngineStatusCheck;->getSrForVideoStatus(Landroid/content/Context;)Z

    move-result v2

    goto/32 :goto_1f

    nop

    :goto_a
    if-nez v1, :cond_0

    goto/32 :goto_11

    :cond_0
    goto/32 :goto_14

    nop

    :goto_b
    invoke-direct {v1, p0, v3, v2}, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;-><init>(Lcom/android/settings/stat/commonswitch/SwitchStat;Ljava/lang/String;Z)V

    goto/32 :goto_15

    nop

    :goto_c
    invoke-static {}, Lcom/android/settings/display/ScreenEnhanceEngineStatusCheck;->isS2hSupport()Z

    move-result v1

    goto/32 :goto_e

    nop

    :goto_d
    invoke-direct {v1, p0, v3, v2}, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;-><init>(Lcom/android/settings/stat/commonswitch/SwitchStat;Ljava/lang/String;Z)V

    goto/32 :goto_1a

    nop

    :goto_e
    if-nez v1, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_21

    nop

    :goto_f
    const-string/jumbo v3, "screen_enhance_engine_s2h_status"

    goto/32 :goto_5

    nop

    :goto_10
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_11
    goto/32 :goto_12

    nop

    :goto_12
    invoke-static {}, Lcom/android/settings/display/ScreenEnhanceEngineStatusCheck;->isSrForImageSupport()Z

    move-result v1

    goto/32 :goto_29

    nop

    :goto_13
    invoke-direct {v1, p0, v3, v2}, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;-><init>(Lcom/android/settings/stat/commonswitch/SwitchStat;Ljava/lang/String;Z)V

    goto/32 :goto_10

    nop

    :goto_14
    new-instance v1, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;

    goto/32 :goto_9

    nop

    :goto_15
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_16
    goto/32 :goto_c

    nop

    :goto_17
    if-nez v1, :cond_2

    goto/32 :goto_1e

    :cond_2
    goto/32 :goto_26

    nop

    :goto_18
    if-nez v1, :cond_3

    goto/32 :goto_16

    :cond_3
    goto/32 :goto_27

    nop

    :goto_19
    invoke-direct {v1, p0, v2, p1}, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;-><init>(Lcom/android/settings/stat/commonswitch/SwitchStat;Ljava/lang/String;Z)V

    goto/32 :goto_1d

    nop

    :goto_1a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1b
    goto/32 :goto_1c

    nop

    :goto_1c
    invoke-static {p1}, Lcom/android/settings/display/ScreenEnhanceEngineStatusCheck;->isAiSupport(Landroid/content/Context;)Z

    move-result v1

    goto/32 :goto_18

    nop

    :goto_1d
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1e
    goto/32 :goto_2a

    nop

    :goto_1f
    const-string/jumbo v3, "screen_enhance_engine_sr_for_video_status"

    goto/32 :goto_13

    nop

    :goto_20
    const-string/jumbo v2, "screen_enhance_engine_memc_status"

    goto/32 :goto_19

    nop

    :goto_21
    new-instance v1, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;

    goto/32 :goto_24

    nop

    :goto_22
    invoke-static {}, Lcom/android/settings/display/ScreenEnhanceEngineStatusCheck;->isMemcSupport()Z

    move-result v1

    goto/32 :goto_17

    nop

    :goto_23
    const-string/jumbo v3, "screen_enhance_engine_sr_for_image_status"

    goto/32 :goto_d

    nop

    :goto_24
    invoke-static {p1}, Lcom/android/settings/display/ScreenEnhanceEngineStatusCheck;->getS2hStatus(Landroid/content/Context;)Z

    move-result v2

    goto/32 :goto_f

    nop

    :goto_25
    const-string/jumbo v3, "screen_enhance_engine_ai_display_status"

    goto/32 :goto_b

    nop

    :goto_26
    new-instance v1, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;

    goto/32 :goto_7

    nop

    :goto_27
    new-instance v1, Lcom/android/settings/stat/commonswitch/SwitchStat$Info;

    goto/32 :goto_8

    nop

    :goto_28
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_2

    nop

    :goto_29
    if-nez v1, :cond_4

    goto/32 :goto_1b

    :cond_4
    goto/32 :goto_1

    nop

    :goto_2a
    return-object v0
.end method
