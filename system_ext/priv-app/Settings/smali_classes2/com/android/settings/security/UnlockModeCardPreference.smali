.class public Lcom/android/settings/security/UnlockModeCardPreference;
.super Lcom/android/settingslib/miuisettings/preference/Preference;

# interfaces
.implements Lmiuix/preference/FolmeAnimationController;


# instance fields
.field private mCardGridView:Lcom/android/settings/MiuiCardGridView;

.field private mCardList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/CardInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settingslib/miuisettings/preference/Preference;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settingslib/miuisettings/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public getData()Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/android/settings/CardInfo;",
            ">;"
        }
    .end annotation

    iget-object p0, p0, Lcom/android/settings/security/UnlockModeCardPreference;->mCardList:Ljava/util/List;

    return-object p0
.end method

.method public isTouchAnimationEnable()Z
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settingslib/miuisettings/preference/Preference;->onBindView(Landroid/view/View;)V

    sget v0, Lcom/android/settings/R$id;->miui_card_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/settings/MiuiCardGridView;

    iput-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreference;->mCardGridView:Lcom/android/settings/MiuiCardGridView;

    iget-object p0, p0, Lcom/android/settings/security/UnlockModeCardPreference;->mCardList:Ljava/util/List;

    if-eqz p0, :cond_0

    invoke-virtual {v0, p0}, Lcom/android/settings/MiuiCardGridView;->setData(Ljava/util/List;)V

    :cond_0
    invoke-static {}, Lcom/android/settings/MiuiUtils;->isMiuiSdkSupportFolme()Z

    move-result p0

    const/4 v0, 0x0

    if-eqz p0, :cond_1

    const/4 p0, 0x1

    new-array p0, p0, [Landroid/view/View;

    aput-object p1, p0, v0

    invoke-static {p0}, Lmiuix/animation/Folme;->clean([Ljava/lang/Object;)V

    :cond_1
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    return-void
.end method

.method protected onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    sget p1, Lcom/android/settings/R$layout;->unlock_card_view_layout:I

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setLayoutResource(I)V

    const/4 p0, 0x0

    return-object p0
.end method

.method public refresh()V
    .locals 0

    iget-object p0, p0, Lcom/android/settings/security/UnlockModeCardPreference;->mCardGridView:Lcom/android/settings/MiuiCardGridView;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/MiuiCardGridView;->notifyDataChanged()V

    :cond_0
    return-void
.end method

.method public setData(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/CardInfo;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/settings/security/UnlockModeCardPreference;->mCardList:Ljava/util/List;

    return-void
.end method
