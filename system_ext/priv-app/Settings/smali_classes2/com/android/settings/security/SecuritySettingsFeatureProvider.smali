.class public interface abstract Lcom/android/settings/security/SecuritySettingsFeatureProvider;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getAlternativeAdvancedSettingsCategoryKey()Ljava/lang/String;
.end method

.method public abstract getAlternativeSecuritySettingsFragmentClassname()Ljava/lang/String;
.end method

.method public abstract hasAlternativeSecuritySettingsFragment()Z
.end method
