.class public Lcom/android/settings/security/UnlockModeCardPreferenceController;
.super Lcom/android/settings/core/BasePreferenceController;


# static fields
.field private static final DISABLE_SECURITY_BY_MISHOW:Ljava/lang/String; = "disable_security_by_mishow"


# instance fields
.field private mBluetoothUnlockCard:Lcom/android/settings/CardInfo;

.field private mBluetoothUnlockController:Lcom/android/settings/BluetoothUnlockStateController;

.field private mCardList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/CardInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mControllerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/BaseCardViewController;",
            ">;"
        }
    .end annotation
.end field

.field private mFaceUnlockCard:Lcom/android/settings/CardInfo;

.field private mFaceUnlockController:Lcom/android/settings/FaceUnlockStateController;

.field private mFingerUnlockCard:Lcom/android/settings/CardInfo;

.field private mFingerprintUnlockController:Lcom/android/settings/FingerprintUnlockStateController;

.field private mFragment:Landroidx/fragment/app/Fragment;

.field private mPasswordUnlockCard:Lcom/android/settings/CardInfo;

.field private mPasswordUnlockController:Lcom/android/settings/PasswordUnlockStateController;

.field private mPerferenceKey:Ljava/lang/String;

.field private mPreferece:Lcom/android/settings/security/UnlockModeCardPreference;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroidx/fragment/app/Fragment;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/core/BasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mPasswordUnlockCard:Lcom/android/settings/CardInfo;

    iput-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFingerUnlockCard:Lcom/android/settings/CardInfo;

    iput-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFaceUnlockCard:Lcom/android/settings/CardInfo;

    iput-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mBluetoothUnlockCard:Lcom/android/settings/CardInfo;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mCardList:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mControllerList:Ljava/util/List;

    iput-object p1, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mPerferenceKey:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFragment:Landroidx/fragment/app/Fragment;

    return-void
.end method

.method private init()V
    .locals 4

    new-instance v0, Lcom/android/settings/CardInfo;

    sget v1, Lcom/android/settings/R$drawable;->ic_password_unlock:I

    sget v2, Lcom/android/settings/R$string;->password_unlock_title:I

    sget v3, Lcom/android/settings/R$string;->off:I

    invoke-direct {v0, v1, v2, v3}, Lcom/android/settings/CardInfo;-><init>(III)V

    iput-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mPasswordUnlockCard:Lcom/android/settings/CardInfo;

    new-instance v0, Lcom/android/settings/CardInfo;

    sget v1, Lcom/android/settings/R$drawable;->ic_finger_unlock:I

    sget v2, Lcom/android/settings/R$string;->privacy_password_use_finger_dialog_title:I

    invoke-direct {v0, v1, v2, v3}, Lcom/android/settings/CardInfo;-><init>(III)V

    iput-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFingerUnlockCard:Lcom/android/settings/CardInfo;

    new-instance v0, Lcom/android/settings/CardInfo;

    sget v1, Lcom/android/settings/R$drawable;->ic_face_unlock:I

    sget v2, Lcom/android/settings/R$string;->unlock_set_unlock_biometric_weak_title:I

    invoke-direct {v0, v1, v2, v3}, Lcom/android/settings/CardInfo;-><init>(III)V

    iput-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFaceUnlockCard:Lcom/android/settings/CardInfo;

    new-instance v0, Lcom/android/settings/CardInfo;

    sget v1, Lcom/android/settings/R$drawable;->ic_bluetooth_unlock:I

    sget v2, Lcom/android/settings/R$string;->bluetooth_unlock_title:I

    invoke-direct {v0, v1, v2, v3}, Lcom/android/settings/CardInfo;-><init>(III)V

    iput-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mBluetoothUnlockCard:Lcom/android/settings/CardInfo;

    new-instance v0, Lcom/android/settings/PasswordUnlockStateController;

    iget-object v1, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mPasswordUnlockCard:Lcom/android/settings/CardInfo;

    iget-object v3, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFragment:Landroidx/fragment/app/Fragment;

    invoke-direct {v0, v1, v2, v3}, Lcom/android/settings/PasswordUnlockStateController;-><init>(Landroid/content/Context;Lcom/android/settings/CardInfo;Landroidx/fragment/app/Fragment;)V

    iput-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mPasswordUnlockController:Lcom/android/settings/PasswordUnlockStateController;

    new-instance v0, Lcom/android/settings/FingerprintUnlockStateController;

    iget-object v1, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFingerUnlockCard:Lcom/android/settings/CardInfo;

    iget-object v3, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFragment:Landroidx/fragment/app/Fragment;

    invoke-direct {v0, v1, v2, v3}, Lcom/android/settings/FingerprintUnlockStateController;-><init>(Landroid/content/Context;Lcom/android/settings/CardInfo;Landroidx/fragment/app/Fragment;)V

    iput-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFingerprintUnlockController:Lcom/android/settings/FingerprintUnlockStateController;

    new-instance v0, Lcom/android/settings/FaceUnlockStateController;

    iget-object v1, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFaceUnlockCard:Lcom/android/settings/CardInfo;

    iget-object v3, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFragment:Landroidx/fragment/app/Fragment;

    invoke-direct {v0, v1, v2, v3}, Lcom/android/settings/FaceUnlockStateController;-><init>(Landroid/content/Context;Lcom/android/settings/CardInfo;Landroidx/fragment/app/Fragment;)V

    iput-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFaceUnlockController:Lcom/android/settings/FaceUnlockStateController;

    new-instance v0, Lcom/android/settings/BluetoothUnlockStateController;

    iget-object v1, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mBluetoothUnlockCard:Lcom/android/settings/CardInfo;

    iget-object v3, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFragment:Landroidx/fragment/app/Fragment;

    invoke-direct {v0, v1, v2, v3}, Lcom/android/settings/BluetoothUnlockStateController;-><init>(Landroid/content/Context;Lcom/android/settings/CardInfo;Landroidx/fragment/app/Fragment;)V

    iput-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mBluetoothUnlockController:Lcom/android/settings/BluetoothUnlockStateController;

    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mPasswordUnlockController:Lcom/android/settings/PasswordUnlockStateController;

    invoke-virtual {v0}, Lcom/android/settings/PasswordUnlockStateController;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mPasswordUnlockCard:Lcom/android/settings/CardInfo;

    sget v1, Lcom/android/settings/R$drawable;->ic_password_unlock_checked:I

    invoke-virtual {v0, v1}, Lcom/android/settings/CardInfo;->setCheckedIconResId(I)V

    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mPasswordUnlockCard:Lcom/android/settings/CardInfo;

    iget-object v1, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mPasswordUnlockController:Lcom/android/settings/PasswordUnlockStateController;

    invoke-virtual {v0, v1}, Lcom/android/settings/CardInfo;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mCardList:Ljava/util/List;

    iget-object v1, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mPasswordUnlockCard:Lcom/android/settings/CardInfo;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mControllerList:Ljava/util/List;

    iget-object v1, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mPasswordUnlockController:Lcom/android/settings/PasswordUnlockStateController;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFingerprintUnlockController:Lcom/android/settings/FingerprintUnlockStateController;

    invoke-virtual {v0}, Lcom/android/settings/FingerprintUnlockStateController;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFingerUnlockCard:Lcom/android/settings/CardInfo;

    sget v1, Lcom/android/settings/R$drawable;->ic_finger_unlock_checked:I

    invoke-virtual {v0, v1}, Lcom/android/settings/CardInfo;->setCheckedIconResId(I)V

    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFingerUnlockCard:Lcom/android/settings/CardInfo;

    iget-object v1, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFingerprintUnlockController:Lcom/android/settings/FingerprintUnlockStateController;

    invoke-virtual {v0, v1}, Lcom/android/settings/CardInfo;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mCardList:Ljava/util/List;

    iget-object v1, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFingerUnlockCard:Lcom/android/settings/CardInfo;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mControllerList:Ljava/util/List;

    iget-object v1, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFingerprintUnlockController:Lcom/android/settings/FingerprintUnlockStateController;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFaceUnlockController:Lcom/android/settings/FaceUnlockStateController;

    invoke-virtual {v0}, Lcom/android/settings/FaceUnlockStateController;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFaceUnlockCard:Lcom/android/settings/CardInfo;

    sget v1, Lcom/android/settings/R$drawable;->ic_face_unlock_checked:I

    invoke-virtual {v0, v1}, Lcom/android/settings/CardInfo;->setCheckedIconResId(I)V

    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFaceUnlockCard:Lcom/android/settings/CardInfo;

    iget-object v1, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFaceUnlockController:Lcom/android/settings/FaceUnlockStateController;

    invoke-virtual {v0, v1}, Lcom/android/settings/CardInfo;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mCardList:Ljava/util/List;

    iget-object v1, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFaceUnlockCard:Lcom/android/settings/CardInfo;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mControllerList:Ljava/util/List;

    iget-object v1, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFaceUnlockController:Lcom/android/settings/FaceUnlockStateController;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mBluetoothUnlockController:Lcom/android/settings/BluetoothUnlockStateController;

    invoke-virtual {v0}, Lcom/android/settings/BluetoothUnlockStateController;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mBluetoothUnlockCard:Lcom/android/settings/CardInfo;

    sget v1, Lcom/android/settings/R$drawable;->ic_bluetooth_unlock_checked:I

    invoke-virtual {v0, v1}, Lcom/android/settings/CardInfo;->setCheckedIconResId(I)V

    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mBluetoothUnlockCard:Lcom/android/settings/CardInfo;

    iget-object v1, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mBluetoothUnlockController:Lcom/android/settings/BluetoothUnlockStateController;

    invoke-virtual {v0, v1}, Lcom/android/settings/CardInfo;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mCardList:Ljava/util/List;

    iget-object v1, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mBluetoothUnlockCard:Lcom/android/settings/CardInfo;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mControllerList:Ljava/util/List;

    iget-object v1, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mBluetoothUnlockController:Lcom/android/settings/BluetoothUnlockStateController;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mControllerList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ge v0, v2, :cond_4

    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mPreferece:Lcom/android/settings/security/UnlockModeCardPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setVisible(Z)V

    :cond_4
    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "disable_security_by_mishow"

    invoke-static {v0, v3, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v2, :cond_5

    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mCardList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/CardInfo;

    invoke-virtual {v1, v2}, Lcom/android/settings/CardInfo;->setDisable(Z)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mPreferece:Lcom/android/settings/security/UnlockModeCardPreference;

    iget-object p0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mCardList:Ljava/util/List;

    invoke-virtual {v0, p0}, Lcom/android/settings/security/UnlockModeCardPreference;->setData(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public displayPreference(Landroidx/preference/PreferenceScreen;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/core/BasePreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mPerferenceKey:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/security/UnlockModeCardPreference;

    iput-object p1, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mPreferece:Lcom/android/settings/security/UnlockModeCardPreference;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/settings/security/UnlockModeCardPreference;->getData()Ljava/util/List;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/security/UnlockModeCardPreferenceController;->init()V

    :cond_0
    return-void
.end method

.method public getAvailabilityStatus()I
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method public bridge synthetic getBackgroundWorkerClass()Ljava/lang/Class;
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->getBackgroundWorkerClass()Ljava/lang/Class;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic getIntentFilter()Landroid/content/IntentFilter;
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->getIntentFilter()Landroid/content/IntentFilter;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic getSliceHighlightMenuRes()I
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->getSliceHighlightMenuRes()I

    move-result p0

    return p0
.end method

.method public handleActivityResult(IILandroid/content/Intent;)V
    .locals 1

    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFingerprintUnlockController:Lcom/android/settings/FingerprintUnlockStateController;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/settings/FingerprintUnlockStateController;->handleActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    :cond_0
    const/16 v0, 0x6b

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mPasswordUnlockController:Lcom/android/settings/PasswordUnlockStateController;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/settings/PasswordUnlockStateController;->handleActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    :cond_1
    const/16 v0, 0x3ea

    if-eq p1, v0, :cond_2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_3

    :cond_2
    iget-object p0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mFaceUnlockController:Lcom/android/settings/FaceUnlockStateController;

    if-eqz p0, :cond_3

    invoke-virtual {p0, p1, p2, p3}, Lcom/android/settings/FaceUnlockStateController;->handleActivityResult(IILandroid/content/Intent;)V

    :cond_3
    :goto_0
    return-void
.end method

.method public bridge synthetic hasAsyncUpdate()Z
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->hasAsyncUpdate()Z

    move-result p0

    return p0
.end method

.method public bridge synthetic isPublicSlice()Z
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->isPublicSlice()Z

    move-result p0

    return p0
.end method

.method public bridge synthetic isSliceable()Z
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->isSliceable()Z

    move-result p0

    return p0
.end method

.method public updateState(Landroidx/preference/Preference;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mPreferece:Lcom/android/settings/security/UnlockModeCardPreference;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-super {p0, p1}, Lcom/android/settingslib/core/AbstractPreferenceController;->updateState(Landroidx/preference/Preference;)V

    iget-object p1, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mControllerList:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/BaseCardViewController;

    invoke-virtual {v0}, Lcom/android/settings/BaseCardViewController;->onResume()V

    goto :goto_0

    :cond_1
    iget-object p0, p0, Lcom/android/settings/security/UnlockModeCardPreferenceController;->mPreferece:Lcom/android/settings/security/UnlockModeCardPreference;

    invoke-virtual {p0}, Lcom/android/settings/security/UnlockModeCardPreference;->refresh()V

    return-void
.end method

.method public bridge synthetic useDynamicSliceSummary()Z
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->useDynamicSliceSummary()Z

    move-result p0

    return p0
.end method
