.class public Lcom/android/settings/notification/zen/ZenModeBypassingAppsPreferenceController;
.super Lcom/android/settings/notification/zen/AbstractZenModePreferenceController;


# instance fields
.field private mAppSession:Lcom/android/settingslib/applications/ApplicationsState$Session;

.field private final mAppSessionCallbacks:Lcom/android/settingslib/applications/ApplicationsState$Callbacks;

.field private mNotificationBackend:Lcom/android/settings/notification/NotificationBackend;

.field protected mPreference:Landroidx/preference/Preference;
    .annotation build Lcom/android/internal/annotations/VisibleForTesting;
    .end annotation
.end field

.field private mSummary:Ljava/lang/String;


# direct methods
.method static bridge synthetic -$$Nest$mupdateAppsBypassingDndSummaryText(Lcom/android/settings/notification/zen/ZenModeBypassingAppsPreferenceController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/notification/zen/ZenModeBypassingAppsPreferenceController;->updateAppsBypassingDndSummaryText()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/app/Application;Lcom/android/settingslib/core/lifecycle/ObservablePreferenceFragment;Lcom/android/settingslib/core/lifecycle/Lifecycle;)V
    .locals 0

    if-nez p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    invoke-static {p2}, Lcom/android/settingslib/applications/ApplicationsState;->getInstance(Landroid/app/Application;)Lcom/android/settingslib/applications/ApplicationsState;

    move-result-object p2

    :goto_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settings/notification/zen/ZenModeBypassingAppsPreferenceController;-><init>(Landroid/content/Context;Lcom/android/settingslib/applications/ApplicationsState;Lcom/android/settingslib/core/lifecycle/ObservablePreferenceFragment;Lcom/android/settingslib/core/lifecycle/Lifecycle;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/android/settingslib/applications/ApplicationsState;Lcom/android/settingslib/core/lifecycle/ObservablePreferenceFragment;Lcom/android/settingslib/core/lifecycle/Lifecycle;)V
    .locals 1

    const-string/jumbo v0, "zen_mode_behavior_apps"

    invoke-direct {p0, p1, v0, p4}, Lcom/android/settings/notification/zen/AbstractZenModePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/android/settingslib/core/lifecycle/Lifecycle;)V

    new-instance p1, Lcom/android/settings/notification/NotificationBackend;

    invoke-direct {p1}, Lcom/android/settings/notification/NotificationBackend;-><init>()V

    iput-object p1, p0, Lcom/android/settings/notification/zen/ZenModeBypassingAppsPreferenceController;->mNotificationBackend:Lcom/android/settings/notification/NotificationBackend;

    new-instance p1, Lcom/android/settings/notification/zen/ZenModeBypassingAppsPreferenceController$1;

    invoke-direct {p1, p0}, Lcom/android/settings/notification/zen/ZenModeBypassingAppsPreferenceController$1;-><init>(Lcom/android/settings/notification/zen/ZenModeBypassingAppsPreferenceController;)V

    iput-object p1, p0, Lcom/android/settings/notification/zen/ZenModeBypassingAppsPreferenceController;->mAppSessionCallbacks:Lcom/android/settingslib/applications/ApplicationsState$Callbacks;

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/android/settingslib/core/lifecycle/ObservablePreferenceFragment;->getLifecycle()Lcom/android/settingslib/core/lifecycle/Lifecycle;

    move-result-object p3

    invoke-virtual {p2, p1, p3}, Lcom/android/settingslib/applications/ApplicationsState;->newSession(Lcom/android/settingslib/applications/ApplicationsState$Callbacks;Landroidx/lifecycle/Lifecycle;)Lcom/android/settingslib/applications/ApplicationsState$Session;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/notification/zen/ZenModeBypassingAppsPreferenceController;->mAppSession:Lcom/android/settingslib/applications/ApplicationsState$Session;

    :cond_0
    return-void
.end method

.method private updateAppsBypassingDndSummaryText()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/notification/zen/ZenModeBypassingAppsPreferenceController;->mAppSession:Lcom/android/settingslib/applications/ApplicationsState$Session;

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget-object v1, Lcom/android/settingslib/applications/ApplicationsState;->FILTER_ALL_ENABLED:Lcom/android/settingslib/applications/ApplicationsState$AppFilter;

    sget-object v2, Lcom/android/settingslib/applications/ApplicationsState;->ALPHA_COMPARATOR:Ljava/util/Comparator;

    invoke-virtual {v0, v1, v2}, Lcom/android/settingslib/applications/ApplicationsState$Session;->rebuild(Lcom/android/settingslib/applications/ApplicationsState$AppFilter;Ljava/util/Comparator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/notification/zen/ZenModeBypassingAppsPreferenceController;->updateAppsBypassingDndSummaryText(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public displayPreference(Landroidx/preference/PreferenceScreen;)V
    .locals 1

    const-string/jumbo v0, "zen_mode_behavior_apps"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/zen/ZenModeBypassingAppsPreferenceController;->mPreference:Landroidx/preference/Preference;

    invoke-direct {p0}, Lcom/android/settings/notification/zen/ZenModeBypassingAppsPreferenceController;->updateAppsBypassingDndSummaryText()V

    invoke-super {p0, p1}, Lcom/android/settings/notification/zen/AbstractZenModePreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    return-void
.end method

.method public getPreferenceKey()Ljava/lang/String;
    .locals 0

    const-string/jumbo p0, "zen_mode_behavior_apps"

    return-object p0
.end method

.method public bridge synthetic getSummary()Ljava/lang/CharSequence;
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/notification/zen/ZenModeBypassingAppsPreferenceController;->getSummary()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeBypassingAppsPreferenceController;->mSummary:Ljava/lang/String;

    return-object p0
.end method

.method public isAvailable()Z
    .locals 0

    const/4 p0, 0x1

    return p0
.end method

.method updateAppsBypassingDndSummaryText(Ljava/util/List;)V
    .locals 9
    .annotation build Lcom/android/internal/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settingslib/applications/ApplicationsState$AppEntry;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_16

    nop

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    goto/32 :goto_2c

    nop

    :goto_2
    invoke-direct {v5, v6, v7}, Landroid/icu/text/MessageFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto/32 :goto_54

    nop

    :goto_3
    if-ne v0, v2, :cond_0

    goto/32 :goto_28

    :cond_0
    goto/32 :goto_15

    nop

    :goto_4
    invoke-virtual {v8, v7, v6}, Lcom/android/settings/notification/NotificationBackend;->getNotificationChannelsBypassingDnd(Ljava/lang/String;I)Landroid/content/pm/ParceledListSlice;

    move-result-object v6

    goto/32 :goto_1f

    nop

    :goto_5
    iput-object p1, p0, Lcom/android/settings/notification/zen/ZenModeBypassingAppsPreferenceController;->mSummary:Ljava/lang/String;

    goto/32 :goto_52

    nop

    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    goto/32 :goto_3f

    nop

    :goto_7
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_30

    nop

    :goto_8
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_9
    goto/32 :goto_6

    nop

    :goto_a
    const/4 v1, 0x0

    goto/32 :goto_1d

    nop

    :goto_b
    invoke-virtual {v7}, Landroid/app/NotificationChannel;->getConversationId()Ljava/lang/String;

    move-result-object v8

    goto/32 :goto_29

    nop

    :goto_c
    const-string v0, "app_3"

    goto/32 :goto_4d

    nop

    :goto_d
    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    goto/32 :goto_0

    nop

    :goto_e
    new-instance v0, Landroid/util/ArraySet;

    goto/32 :goto_d

    nop

    :goto_f
    if-ge p1, v2, :cond_1

    goto/32 :goto_4e

    :cond_1
    goto/32 :goto_47

    nop

    :goto_10
    sget v0, Lcom/android/settings/R$string;->zen_mode_bypassing_apps_subtext_none:I

    goto/32 :goto_7

    nop

    :goto_11
    invoke-virtual {v5, v6}, Landroid/icu/text/MessageFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_5

    nop

    :goto_12
    if-eqz v8, :cond_2

    goto/32 :goto_44

    :cond_2
    goto/32 :goto_18

    nop

    :goto_13
    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto/32 :goto_21

    nop

    :goto_14
    iget-object p1, p0, Lcom/android/settings/notification/zen/ZenModeBypassingAppsPreferenceController;->mPreference:Landroidx/preference/Preference;

    goto/32 :goto_56

    nop

    :goto_15
    const/4 v3, 0x3

    goto/32 :goto_17

    nop

    :goto_16
    invoke-virtual {p0}, Lcom/android/settings/notification/zen/AbstractZenModePreferenceController;->getZenMode()I

    move-result v0

    goto/32 :goto_a

    nop

    :goto_17
    if-ne v0, v3, :cond_3

    goto/32 :goto_28

    :cond_3
    goto/32 :goto_1e

    nop

    :goto_18
    invoke-virtual {v7}, Landroid/app/NotificationChannel;->isDemoted()Z

    move-result v7

    goto/32 :goto_39

    nop

    :goto_19
    new-array v5, p1, [Ljava/lang/String;

    goto/32 :goto_33

    nop

    :goto_1a
    const/4 v4, 0x1

    goto/32 :goto_2d

    nop

    :goto_1b
    invoke-interface {v6, v7, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_f

    nop

    :goto_1c
    iget-object v6, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    goto/32 :goto_2a

    nop

    :goto_1d
    const/4 v2, 0x2

    goto/32 :goto_3

    nop

    :goto_1e
    iget-object v0, p0, Lcom/android/settings/notification/zen/ZenModeBypassingAppsPreferenceController;->mPreference:Landroidx/preference/Preference;

    goto/32 :goto_1a

    nop

    :goto_1f
    invoke-virtual {v6}, Landroid/content/pm/ParceledListSlice;->getList()Ljava/util/List;

    move-result-object v6

    goto/32 :goto_8

    nop

    :goto_20
    iget-object v8, v5, Lcom/android/settingslib/applications/ApplicationsState$AppEntry;->label:Ljava/lang/String;

    goto/32 :goto_32

    nop

    :goto_21
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    goto/32 :goto_2

    nop

    :goto_22
    invoke-interface {v6, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_34

    nop

    :goto_23
    check-cast v5, Lcom/android/settingslib/applications/ApplicationsState$AppEntry;

    goto/32 :goto_3a

    nop

    :goto_24
    invoke-interface {v0, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/32 :goto_41

    nop

    :goto_25
    invoke-interface {v6, v8, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_2e

    nop

    :goto_26
    check-cast v7, Landroid/app/NotificationChannel;

    goto/32 :goto_b

    nop

    :goto_27
    return-void

    :goto_28
    goto/32 :goto_14

    nop

    :goto_29
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    goto/32 :goto_12

    nop

    :goto_2a
    sget v7, Lcom/android/settings/R$string;->zen_mode_bypassing_apps_subtext:I

    goto/32 :goto_13

    nop

    :goto_2b
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    goto/32 :goto_26

    nop

    :goto_2c
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    goto/32 :goto_37

    nop

    :goto_2d
    invoke-virtual {v0, v4}, Landroidx/preference/Preference;->setEnabled(Z)V

    goto/32 :goto_4a

    nop

    :goto_2e
    if-ge p1, v4, :cond_4

    goto/32 :goto_4e

    :cond_4
    goto/32 :goto_53

    nop

    :goto_2f
    new-instance v5, Landroid/icu/text/MessageFormat;

    goto/32 :goto_1c

    nop

    :goto_30
    iput-object p1, p0, Lcom/android/settings/notification/zen/ZenModeBypassingAppsPreferenceController;->mSummary:Ljava/lang/String;

    goto/32 :goto_4c

    nop

    :goto_31
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    goto/32 :goto_10

    nop

    :goto_32
    invoke-virtual {v7, v8}, Landroidx/core/text/BidiFormatter;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto/32 :goto_24

    nop

    :goto_33
    invoke-interface {v0, v5}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_50

    nop

    :goto_34
    if-eq p1, v3, :cond_5

    goto/32 :goto_4e

    :cond_5
    goto/32 :goto_4b

    nop

    :goto_35
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result p1

    goto/32 :goto_19

    nop

    :goto_36
    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    goto/32 :goto_49

    nop

    :goto_37
    if-nez v5, :cond_6

    goto/32 :goto_42

    :cond_6
    goto/32 :goto_3d

    nop

    :goto_38
    const-string v4, "app_2"

    goto/32 :goto_22

    nop

    :goto_39
    if-eqz v7, :cond_7

    goto/32 :goto_44

    :cond_7
    goto/32 :goto_43

    nop

    :goto_3a
    iget-object v6, v5, Lcom/android/settingslib/applications/ApplicationsState$AppEntry;->info:Landroid/content/pm/ApplicationInfo;

    goto/32 :goto_46

    nop

    :goto_3b
    return-void

    :goto_3c
    goto/32 :goto_e

    nop

    :goto_3d
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    goto/32 :goto_23

    nop

    :goto_3e
    iget v6, v6, Landroid/content/pm/ApplicationInfo;->uid:I

    goto/32 :goto_4

    nop

    :goto_3f
    if-nez v7, :cond_8

    goto/32 :goto_1

    :cond_8
    goto/32 :goto_2b

    nop

    :goto_40
    const-string v8, "count"

    goto/32 :goto_25

    nop

    :goto_41
    goto/16 :goto_9

    :goto_42
    goto/32 :goto_35

    nop

    :goto_43
    goto/16 :goto_9

    :goto_44
    goto/32 :goto_48

    nop

    :goto_45
    iget-object v8, p0, Lcom/android/settings/notification/zen/ZenModeBypassingAppsPreferenceController;->mNotificationBackend:Lcom/android/settings/notification/NotificationBackend;

    goto/32 :goto_3e

    nop

    :goto_46
    iget-object v7, v6, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    goto/32 :goto_45

    nop

    :goto_47
    aget-object v1, v0, v4

    goto/32 :goto_38

    nop

    :goto_48
    invoke-static {}, Landroidx/core/text/BidiFormatter;->getInstance()Landroidx/core/text/BidiFormatter;

    move-result-object v7

    goto/32 :goto_20

    nop

    :goto_49
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    goto/32 :goto_40

    nop

    :goto_4a
    if-eqz p1, :cond_9

    goto/32 :goto_3c

    :cond_9
    goto/32 :goto_3b

    nop

    :goto_4b
    aget-object p1, v0, v2

    goto/32 :goto_c

    nop

    :goto_4c
    return-void

    :goto_4d
    invoke-interface {v6, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_4e
    goto/32 :goto_11

    nop

    :goto_4f
    iget-object p1, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    goto/32 :goto_31

    nop

    :goto_50
    check-cast v0, [Ljava/lang/String;

    goto/32 :goto_2f

    nop

    :goto_51
    const-string v7, "app_1"

    goto/32 :goto_1b

    nop

    :goto_52
    iget-object p1, p0, Lcom/android/settings/notification/zen/ZenModeBypassingAppsPreferenceController;->mPreference:Landroidx/preference/Preference;

    goto/32 :goto_55

    nop

    :goto_53
    aget-object v1, v0, v1

    goto/32 :goto_51

    nop

    :goto_54
    new-instance v6, Ljava/util/HashMap;

    goto/32 :goto_36

    nop

    :goto_55
    invoke-virtual {p0, p1}, Lcom/android/settingslib/core/AbstractPreferenceController;->refreshSummary(Landroidx/preference/Preference;)V

    goto/32 :goto_27

    nop

    :goto_56
    invoke-virtual {p1, v1}, Landroidx/preference/Preference;->setEnabled(Z)V

    goto/32 :goto_4f

    nop
.end method
