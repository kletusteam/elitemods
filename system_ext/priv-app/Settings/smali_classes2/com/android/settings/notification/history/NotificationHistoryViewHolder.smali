.class public Lcom/android/settings/notification/history/NotificationHistoryViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;


# instance fields
.field private final mSummary:Landroid/widget/TextView;

.field private final mTime:Landroid/widget/DateTimeView;

.field private final mTitle:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    sget v0, Lcom/android/settings/R$id;->timestamp:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/DateTimeView;

    iput-object v0, p0, Lcom/android/settings/notification/history/NotificationHistoryViewHolder;->mTime:Landroid/widget/DateTimeView;

    sget v0, Lcom/android/settings/R$id;->title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/notification/history/NotificationHistoryViewHolder;->mTitle:Landroid/widget/TextView;

    sget v0, Lcom/android/settings/R$id;->text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/android/settings/notification/history/NotificationHistoryViewHolder;->mSummary:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method setPostedTime(J)V
    .locals 0

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p0, p1, p2}, Landroid/widget/DateTimeView;->setTime(J)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    iget-object p0, p0, Lcom/android/settings/notification/history/NotificationHistoryViewHolder;->mTime:Landroid/widget/DateTimeView;

    goto/32 :goto_0

    nop
.end method

.method setSummary(Ljava/lang/CharSequence;)V
    .locals 1

    goto/32 :goto_6

    nop

    :goto_0
    const/16 p1, 0x8

    :goto_1
    goto/32 :goto_9

    nop

    :goto_2
    if-nez p1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_7

    nop

    :goto_3
    goto :goto_1

    :goto_4
    goto/32 :goto_0

    nop

    :goto_5
    return-void

    :goto_6
    iget-object v0, p0, Lcom/android/settings/notification/history/NotificationHistoryViewHolder;->mSummary:Landroid/widget/TextView;

    goto/32 :goto_a

    nop

    :goto_7
    const/4 p1, 0x0

    goto/32 :goto_3

    nop

    :goto_8
    iget-object p0, p0, Lcom/android/settings/notification/history/NotificationHistoryViewHolder;->mSummary:Landroid/widget/TextView;

    goto/32 :goto_2

    nop

    :goto_9
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/32 :goto_5

    nop

    :goto_a
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_8

    nop
.end method

.method setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    goto :goto_a

    :goto_1
    goto/32 :goto_9

    nop

    :goto_2
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_4

    nop

    :goto_3
    iget-object v0, p0, Lcom/android/settings/notification/history/NotificationHistoryViewHolder;->mTitle:Landroid/widget/TextView;

    goto/32 :goto_2

    nop

    :goto_4
    iget-object p0, p0, Lcom/android/settings/notification/history/NotificationHistoryViewHolder;->mTitle:Landroid/widget/TextView;

    goto/32 :goto_5

    nop

    :goto_5
    if-nez p1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_6

    nop

    :goto_6
    const/4 p1, 0x0

    goto/32 :goto_0

    nop

    :goto_7
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/32 :goto_8

    nop

    :goto_8
    return-void

    :goto_9
    const/4 p1, 0x4

    :goto_a
    goto/32 :goto_7

    nop
.end method
