.class Lcom/android/settings/notification/app/BubblePreference$ButtonViewHolder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/notification/app/BubblePreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ButtonViewHolder"
.end annotation


# instance fields
.field private mId:I

.field private mImageView:Landroid/widget/ImageView;

.field private mTextView:Landroid/widget/TextView;

.field private mView:Landroid/view/View;

.field final synthetic this$0:Lcom/android/settings/notification/app/BubblePreference;


# direct methods
.method constructor <init>(Lcom/android/settings/notification/app/BubblePreference;Landroid/view/View;Landroid/widget/ImageView;Landroid/widget/TextView;I)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/app/BubblePreference$ButtonViewHolder;->this$0:Lcom/android/settings/notification/app/BubblePreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/settings/notification/app/BubblePreference$ButtonViewHolder;->mView:Landroid/view/View;

    iput-object p3, p0, Lcom/android/settings/notification/app/BubblePreference$ButtonViewHolder;->mImageView:Landroid/widget/ImageView;

    iput-object p4, p0, Lcom/android/settings/notification/app/BubblePreference$ButtonViewHolder;->mTextView:Landroid/widget/TextView;

    iput p5, p0, Lcom/android/settings/notification/app/BubblePreference$ButtonViewHolder;->mId:I

    return-void
.end method


# virtual methods
.method setSelected(Landroid/content/Context;Z)V
    .locals 3

    goto/32 :goto_a

    nop

    :goto_0
    invoke-static {p1}, Lcom/android/settingslib/Utils;->getColorAccent(Landroid/content/Context;)Landroid/content/res/ColorStateList;

    move-result-object p1

    goto/32 :goto_14

    nop

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/32 :goto_c

    nop

    :goto_2
    iget-object p2, p0, Lcom/android/settings/notification/app/BubblePreference$ButtonViewHolder;->mImageView:Landroid/widget/ImageView;

    goto/32 :goto_8

    nop

    :goto_3
    invoke-static {p1, p2}, Lcom/android/settingslib/Utils;->getColorAttr(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object p1

    :goto_4
    goto/32 :goto_2

    nop

    :goto_5
    iget-object v1, p0, Lcom/android/settings/notification/app/BubblePreference$ButtonViewHolder;->this$0:Lcom/android/settings/notification/app/BubblePreference;

    goto/32 :goto_6

    nop

    :goto_6
    invoke-static {v1}, Lcom/android/settings/notification/app/BubblePreference;->-$$Nest$fgetmContext(Lcom/android/settings/notification/app/BubblePreference;)Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_11

    nop

    :goto_7
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto/32 :goto_e

    nop

    :goto_8
    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    goto/32 :goto_17

    nop

    :goto_9
    if-nez p2, :cond_0

    goto/32 :goto_15

    :cond_0
    goto/32 :goto_0

    nop

    :goto_a
    iget-object v0, p0, Lcom/android/settings/notification/app/BubblePreference$ButtonViewHolder;->mView:Landroid/view/View;

    goto/32 :goto_5

    nop

    :goto_b
    invoke-virtual {v1, v2}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_c
    iget-object v0, p0, Lcom/android/settings/notification/app/BubblePreference$ButtonViewHolder;->mView:Landroid/view/View;

    goto/32 :goto_d

    nop

    :goto_d
    invoke-virtual {v0, p2}, Landroid/view/View;->setSelected(Z)V

    goto/32 :goto_9

    nop

    :goto_e
    return-void

    :goto_f
    sget v2, Lcom/android/settingslib/R$drawable;->button_border_unselected:I

    :goto_10
    goto/32 :goto_b

    nop

    :goto_11
    if-nez p2, :cond_1

    goto/32 :goto_13

    :cond_1
    goto/32 :goto_16

    nop

    :goto_12
    goto :goto_10

    :goto_13
    goto/32 :goto_f

    nop

    :goto_14
    goto :goto_4

    :goto_15
    goto/32 :goto_18

    nop

    :goto_16
    sget v2, Lcom/android/settingslib/R$drawable;->button_border_selected:I

    goto/32 :goto_12

    nop

    :goto_17
    iget-object p0, p0, Lcom/android/settings/notification/app/BubblePreference$ButtonViewHolder;->mTextView:Landroid/widget/TextView;

    goto/32 :goto_7

    nop

    :goto_18
    const p2, 0x1010036

    goto/32 :goto_3

    nop
.end method
