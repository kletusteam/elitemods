.class public Lcom/android/settings/notification/app/PriorityConversationsPreferenceController;
.super Lcom/android/settings/notification/app/ConversationListPreferenceController;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/notification/NotificationBackend;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/notification/app/ConversationListPreferenceController;-><init>(Landroid/content/Context;Lcom/android/settings/notification/NotificationBackend;)V

    return-void
.end method


# virtual methods
.method public getPreferenceKey()Ljava/lang/String;
    .locals 0

    const-string p0, "important_conversations"

    return-object p0
.end method

.method getSummaryPreference()Landroidx/preference/Preference;
    .locals 1

    goto/32 :goto_9

    nop

    :goto_0
    invoke-direct {v0, p0}, Landroidx/preference/Preference;-><init>(Landroid/content/Context;)V

    goto/32 :goto_4

    nop

    :goto_1
    iget-object p0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    goto/32 :goto_0

    nop

    :goto_2
    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setOrder(I)V

    goto/32 :goto_5

    nop

    :goto_3
    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setSummary(I)V

    goto/32 :goto_6

    nop

    :goto_4
    const/4 p0, 0x1

    goto/32 :goto_2

    nop

    :goto_5
    sget p0, Lcom/android/settings/R$string;->important_conversations_summary_bubbles:I

    goto/32 :goto_3

    nop

    :goto_6
    const/4 p0, 0x0

    goto/32 :goto_8

    nop

    :goto_7
    return-object v0

    :goto_8
    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setSelectable(Z)V

    goto/32 :goto_7

    nop

    :goto_9
    new-instance v0, Landroidx/preference/Preference;

    goto/32 :goto_1

    nop
.end method

.method matchesFilter(Landroid/service/notification/ConversationChannelWrapper;)Z
    .locals 0

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p0}, Landroid/app/NotificationChannel;->isImportantConversation()Z

    move-result p0

    goto/32 :goto_1

    nop

    :goto_1
    return p0

    :goto_2
    invoke-virtual {p1}, Landroid/service/notification/ConversationChannelWrapper;->getNotificationChannel()Landroid/app/NotificationChannel;

    move-result-object p0

    goto/32 :goto_0

    nop
.end method
