.class Lcom/android/settings/notification/NotificationCardPreference$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/notification/NotificationCardPreference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/notification/NotificationCardPreference;

.field final synthetic val$floatNotification:Landroid/view/View;

.field final synthetic val$itemView:Landroid/view/View;

.field final synthetic val$lockScreenNotification:Landroid/view/View;

.field final synthetic val$showAppBadge:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/android/settings/notification/NotificationCardPreference;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/NotificationCardPreference$2;->this$0:Lcom/android/settings/notification/NotificationCardPreference;

    iput-object p2, p0, Lcom/android/settings/notification/NotificationCardPreference$2;->val$lockScreenNotification:Landroid/view/View;

    iput-object p3, p0, Lcom/android/settings/notification/NotificationCardPreference$2;->val$floatNotification:Landroid/view/View;

    iput-object p4, p0, Lcom/android/settings/notification/NotificationCardPreference$2;->val$showAppBadge:Landroid/view/View;

    iput-object p5, p0, Lcom/android/settings/notification/NotificationCardPreference$2;->val$itemView:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/notification/NotificationCardPreference$2;->val$lockScreenNotification:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/notification/NotificationCardPreference$2;->val$floatNotification:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Lcom/android/settings/notification/NotificationCardPreference$2;->val$showAppBadge:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    if-le v0, v1, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    if-le v0, v2, :cond_1

    move v2, v0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/notification/NotificationCardPreference$2;->val$lockScreenNotification:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v0, p0, Lcom/android/settings/notification/NotificationCardPreference$2;->val$floatNotification:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v0, p0, Lcom/android/settings/notification/NotificationCardPreference$2;->val$showAppBadge:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object p0, p0, Lcom/android/settings/notification/NotificationCardPreference$2;->val$itemView:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void
.end method
