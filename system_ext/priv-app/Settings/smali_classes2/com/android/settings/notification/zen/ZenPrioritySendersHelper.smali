.class public Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;
.super Ljava/lang/Object;


# static fields
.field private static final ALL_CONTACTS_INTENT:Landroid/content/Intent;

.field private static final FALLBACK_INTENT:Landroid/content/Intent;

.field private static final STARRED_CONTACTS_INTENT:Landroid/content/Intent;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mIsMessages:Z

.field private final mNotificationBackend:Lcom/android/settings/notification/NotificationBackend;

.field private mNumImportantConversations:I

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private mPreferenceCategory:Landroidx/preference/PreferenceCategory;

.field private final mSelectorClickListener:Lcom/android/settingslib/widget/SelectorWithWidgetPreference$OnClickListener;

.field private mSelectorPreferences:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settingslib/widget/SelectorWithWidgetPreference;",
            ">;"
        }
    .end annotation
.end field

.field private final mZenModeBackend:Lcom/android/settings/notification/zen/ZenModeBackend;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPackageManager(Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;)Landroid/content/pm/PackageManager;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mPackageManager:Landroid/content/pm/PackageManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$sfgetALL_CONTACTS_INTENT()Landroid/content/Intent;
    .locals 1

    sget-object v0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->ALL_CONTACTS_INTENT:Landroid/content/Intent;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetFALLBACK_INTENT()Landroid/content/Intent;
    .locals 1

    sget-object v0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->FALLBACK_INTENT:Landroid/content/Intent;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetSTARRED_CONTACTS_INTENT()Landroid/content/Intent;
    .locals 1

    sget-object v0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->STARRED_CONTACTS_INTENT:Landroid/content/Intent;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.contacts.action.LIST_DEFAULT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    sput-object v0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->ALL_CONTACTS_INTENT:Landroid/content/Intent;

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.android.contacts.action.LIST_STARRED"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    sput-object v0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->STARRED_CONTACTS_INTENT:Landroid/content/Intent;

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    sput-object v0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->FALLBACK_INTENT:Landroid/content/Intent;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZLcom/android/settings/notification/zen/ZenModeBackend;Lcom/android/settings/notification/NotificationBackend;Lcom/android/settingslib/widget/SelectorWithWidgetPreference$OnClickListener;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, -0xa

    iput v0, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mNumImportantConversations:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mSelectorPreferences:Ljava/util/List;

    iput-object p1, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mContext:Landroid/content/Context;

    iput-boolean p2, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mIsMessages:Z

    iput-object p3, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mZenModeBackend:Lcom/android/settings/notification/zen/ZenModeBackend;

    iput-object p4, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mNotificationBackend:Lcom/android/settings/notification/NotificationBackend;

    iput-object p5, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mSelectorClickListener:Lcom/android/settingslib/widget/SelectorWithWidgetPreference$OnClickListener;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mPackageManager:Landroid/content/pm/PackageManager;

    sget-object p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->FALLBACK_INTENT:Landroid/content/Intent;

    const-string p1, "android.intent.category.APP_CONTACTS"

    invoke-virtual {p0, p1}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    move-result p2

    if-nez p2, :cond_0

    invoke-virtual {p0, p1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    return-void
.end method

.method private getConversationSummary()Ljava/lang/String;
    .locals 3

    iget v0, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mNumImportantConversations:I

    const/16 v1, -0xa

    if-ne v0, v1, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    new-instance v1, Landroid/icu/text/MessageFormat;

    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mContext:Landroid/content/Context;

    sget v2, Lcom/android/settings/R$string;->zen_mode_conversations_count:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Landroid/icu/text/MessageFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    new-instance p0, Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v2, "count"

    invoke-interface {p0, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1, p0}, Landroid/icu/text/MessageFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private getSummary(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, 0x3

    const/4 v2, 0x2

    const/4 v3, 0x1

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string/jumbo v0, "senders_none"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x4

    goto :goto_1

    :sswitch_1
    const-string/jumbo v0, "senders_anyone"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    move p1, v1

    goto :goto_1

    :sswitch_2
    const-string v0, "conversations_important"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    move p1, v2

    goto :goto_1

    :sswitch_3
    const-string/jumbo v0, "senders_contacts"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    move p1, v3

    goto :goto_1

    :sswitch_4
    const-string/jumbo v0, "senders_starred_contacts"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_1

    :cond_0
    :goto_0
    const/4 p1, -0x1

    :goto_1
    if-eqz p1, :cond_5

    if-eq p1, v3, :cond_4

    if-eq p1, v2, :cond_3

    if-eq p1, v1, :cond_1

    const/4 p0, 0x0

    return-object p0

    :cond_1
    iget-object p1, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    iget-boolean p0, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mIsMessages:Z

    if-eqz p0, :cond_2

    sget p0, Lcom/android/settings/R$string;->zen_mode_all_messages_summary:I

    goto :goto_2

    :cond_2
    sget p0, Lcom/android/settings/R$string;->zen_mode_all_calls_summary:I

    :goto_2
    invoke-virtual {p1, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_3
    invoke-direct {p0}, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->getConversationSummary()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_4
    iget-object p1, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mZenModeBackend:Lcom/android/settings/notification/zen/ZenModeBackend;

    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mContext:Landroid/content/Context;

    invoke-virtual {p1, p0}, Lcom/android/settings/notification/zen/ZenModeBackend;->getContactsNumberSummary(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_5
    iget-object p1, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mZenModeBackend:Lcom/android/settings/notification/zen/ZenModeBackend;

    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mContext:Landroid/content/Context;

    invoke-virtual {p1, p0}, Lcom/android/settings/notification/zen/ZenModeBackend;->getStarredContactsSummary(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x444c2b2c -> :sswitch_4
        -0x7ef016c -> :sswitch_3
        0x2757b2f3 -> :sswitch_2
        0x66d5177b -> :sswitch_1
        0x695a95f9 -> :sswitch_0
    .end sparse-switch
.end method

.method private getWidgetClickListener(Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 4

    const-string/jumbo v0, "senders_contacts"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string/jumbo v2, "senders_starred_contacts"

    const/4 v3, 0x0

    if-nez v1, :cond_0

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "conversations_important"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    return-object v3

    :cond_0
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->isStarredIntentValid()Z

    move-result v1

    if-nez v1, :cond_1

    return-object v3

    :cond_1
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->isContactsIntentValid()Z

    move-result v0

    if-nez v0, :cond_2

    return-object v3

    :cond_2
    new-instance v0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper$1;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper$1;-><init>(Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;Ljava/lang/String;)V

    return-object v0
.end method

.method private isContactsIntentValid()Z
    .locals 2

    sget-object v0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->ALL_CONTACTS_INTENT:Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->FALLBACK_INTENT:Landroid/content/Intent;

    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, p0}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private isStarredIntentValid()Z
    .locals 2

    sget-object v0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->STARRED_CONTACTS_INTENT:Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->FALLBACK_INTENT:Landroid/content/Intent;

    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, p0}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private makeSelectorPreference(Ljava/lang/String;IZ)Lcom/android/settingslib/widget/SelectorWithWidgetPreference;
    .locals 2

    new-instance v0, Lcom/android/settingslib/widget/SelectorWithWidgetPreference;

    iget-object v1, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mPreferenceCategory:Landroidx/preference/PreferenceCategory;

    invoke-virtual {v1}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p3}, Lcom/android/settingslib/widget/SelectorWithWidgetPreference;-><init>(Landroid/content/Context;Z)V

    invoke-virtual {v0, p1}, Landroidx/preference/Preference;->setKey(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Landroidx/preference/Preference;->setTitle(I)V

    iget-object p2, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mSelectorClickListener:Lcom/android/settingslib/widget/SelectorWithWidgetPreference$OnClickListener;

    invoke-virtual {v0, p2}, Lcom/android/settingslib/widget/SelectorWithWidgetPreference;->setOnClickListener(Lcom/android/settingslib/widget/SelectorWithWidgetPreference$OnClickListener;)V

    invoke-direct {p0, p1}, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->getWidgetClickListener(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {v0, p1}, Lcom/android/settingslib/widget/SelectorWithWidgetPreference;->setExtraWidgetOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    iget-object p1, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mPreferenceCategory:Landroidx/preference/PreferenceCategory;

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mSelectorPreferences:Ljava/util/List;

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method


# virtual methods
.method displayPreference(Landroidx/preference/PreferenceCategory;)V
    .locals 2

    goto/32 :goto_18

    nop

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->updateChannelCounts()V

    :goto_1
    goto/32 :goto_c

    nop

    :goto_2
    sget p1, Lcom/android/settings/R$string;->zen_mode_from_contacts:I

    goto/32 :goto_a

    nop

    :goto_3
    invoke-direct {p0, v1, p1, v0}, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->makeSelectorPreference(Ljava/lang/String;IZ)Lcom/android/settingslib/widget/SelectorWithWidgetPreference;

    goto/32 :goto_f

    nop

    :goto_4
    const-string/jumbo v1, "senders_anyone"

    goto/32 :goto_1c

    nop

    :goto_5
    invoke-virtual {p1}, Landroidx/preference/PreferenceGroup;->getPreferenceCount()I

    move-result p1

    goto/32 :goto_1a

    nop

    :goto_6
    return-void

    :goto_7
    invoke-direct {p0, v1, p1, v0}, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->makeSelectorPreference(Ljava/lang/String;IZ)Lcom/android/settingslib/widget/SelectorWithWidgetPreference;

    goto/32 :goto_2

    nop

    :goto_8
    if-nez p1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_15

    nop

    :goto_9
    iget-boolean v0, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mIsMessages:Z

    goto/32 :goto_14

    nop

    :goto_a
    iget-boolean v0, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mIsMessages:Z

    goto/32 :goto_17

    nop

    :goto_b
    sget p1, Lcom/android/settings/R$string;->zen_mode_from_starred:I

    goto/32 :goto_19

    nop

    :goto_c
    sget p1, Lcom/android/settings/R$string;->zen_mode_from_anyone:I

    goto/32 :goto_1b

    nop

    :goto_d
    const/4 v0, 0x1

    goto/32 :goto_16

    nop

    :goto_e
    const-string/jumbo v1, "senders_starred_contacts"

    goto/32 :goto_7

    nop

    :goto_f
    invoke-virtual {p0}, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->updateSummaries()V

    :goto_10
    goto/32 :goto_6

    nop

    :goto_11
    invoke-direct {p0, v1, p1, v0}, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->makeSelectorPreference(Ljava/lang/String;IZ)Lcom/android/settingslib/widget/SelectorWithWidgetPreference;

    goto/32 :goto_0

    nop

    :goto_12
    invoke-direct {p0, v1, p1, v0}, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->makeSelectorPreference(Ljava/lang/String;IZ)Lcom/android/settingslib/widget/SelectorWithWidgetPreference;

    goto/32 :goto_1d

    nop

    :goto_13
    sget p1, Lcom/android/settings/R$string;->zen_mode_none_messages:I

    goto/32 :goto_9

    nop

    :goto_14
    const-string/jumbo v1, "senders_none"

    goto/32 :goto_3

    nop

    :goto_15
    sget p1, Lcom/android/settings/R$string;->zen_mode_from_important_conversations:I

    goto/32 :goto_d

    nop

    :goto_16
    const-string v1, "conversations_important"

    goto/32 :goto_11

    nop

    :goto_17
    const-string/jumbo v1, "senders_contacts"

    goto/32 :goto_12

    nop

    :goto_18
    iput-object p1, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mPreferenceCategory:Landroidx/preference/PreferenceCategory;

    goto/32 :goto_5

    nop

    :goto_19
    iget-boolean v0, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mIsMessages:Z

    goto/32 :goto_e

    nop

    :goto_1a
    if-eqz p1, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_b

    nop

    :goto_1b
    iget-boolean v0, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mIsMessages:Z

    goto/32 :goto_4

    nop

    :goto_1c
    invoke-direct {p0, v1, p1, v0}, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->makeSelectorPreference(Ljava/lang/String;IZ)Lcom/android/settingslib/widget/SelectorWithWidgetPreference;

    goto/32 :goto_13

    nop

    :goto_1d
    iget-boolean p1, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mIsMessages:Z

    goto/32 :goto_8

    nop
.end method

.method keyToSettingEndState(Ljava/lang/String;Z)[I
    .locals 11

    goto/32 :goto_34

    nop

    :goto_0
    if-eqz p2, :cond_0

    goto/32 :goto_19

    :cond_0
    goto/32 :goto_18

    nop

    :goto_1
    const/16 p2, -0xa

    goto/32 :goto_5d

    nop

    :goto_2
    const-string/jumbo v7, "senders_anyone"

    goto/32 :goto_68

    nop

    :goto_3
    iget-boolean p0, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mIsMessages:Z

    goto/32 :goto_f

    nop

    :goto_4
    if-eqz p2, :cond_1

    goto/32 :goto_12

    :cond_1
    goto/32 :goto_11

    nop

    :goto_5
    aput v9, v1, v10

    goto/32 :goto_50

    nop

    :goto_6
    aput v5, v1, v9

    goto/32 :goto_52

    nop

    :goto_7
    goto/16 :goto_76

    :goto_8
    goto/32 :goto_85

    nop

    :goto_9
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p0

    sparse-switch p0, :sswitch_data_3

    goto/32 :goto_90

    nop

    :goto_a
    move v0, v10

    :goto_b
    packed-switch v0, :pswitch_data_1

    goto/32 :goto_2e

    nop

    :goto_c
    goto/16 :goto_71

    :goto_d
    goto/32 :goto_26

    nop

    :goto_e
    goto/16 :goto_5c

    :pswitch_0
    goto/32 :goto_48

    nop

    :goto_f
    if-nez p0, :cond_2

    goto/32 :goto_4d

    :cond_2
    goto/32 :goto_9

    nop

    :goto_10
    if-eqz p2, :cond_3

    goto/32 :goto_43

    :cond_3
    goto/32 :goto_42

    nop

    :goto_11
    goto/16 :goto_76

    :goto_12
    goto/32 :goto_67

    nop

    :goto_13
    goto/16 :goto_4d

    :pswitch_1
    goto/32 :goto_91

    nop

    :goto_14
    if-eqz p2, :cond_4

    goto/32 :goto_d

    :cond_4
    goto/32 :goto_c

    nop

    :goto_15
    const/4 v9, 0x1

    goto/32 :goto_31

    nop

    :goto_16
    move p2, v0

    goto/32 :goto_8d

    nop

    :goto_17
    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    goto/32 :goto_8e

    nop

    :goto_18
    goto/16 :goto_76

    :goto_19
    goto/32 :goto_16

    nop

    :goto_1a
    aput v5, v1, v9

    goto/32 :goto_13

    nop

    :goto_1b
    const-string/jumbo v3, "senders_contacts"

    goto/32 :goto_4f

    nop

    :goto_1c
    goto/16 :goto_45

    :goto_1d
    goto/32 :goto_96

    nop

    :goto_1e
    if-nez p0, :cond_5

    goto/32 :goto_4d

    :cond_5
    goto/32 :goto_6c

    nop

    :goto_1f
    if-eqz p0, :cond_6

    goto/32 :goto_2c

    :cond_6
    goto/32 :goto_2b

    nop

    :goto_20
    goto/16 :goto_71

    :goto_21
    goto/32 :goto_82

    nop

    :goto_22
    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    goto/32 :goto_10

    nop

    :goto_23
    goto :goto_36

    :goto_24
    goto/32 :goto_35

    nop

    :goto_25
    new-instance p2, Ljava/lang/StringBuilder;

    goto/32 :goto_54

    nop

    :goto_26
    move p2, v10

    :goto_27
    packed-switch p2, :pswitch_data_0

    goto/32 :goto_33

    nop

    :goto_28
    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    goto/32 :goto_0

    nop

    :goto_29
    goto/16 :goto_71

    :goto_2a
    goto/32 :goto_89

    nop

    :goto_2b
    goto/16 :goto_6d

    :goto_2c
    goto/32 :goto_97

    nop

    :goto_2d
    goto/16 :goto_6d

    :sswitch_0
    goto/32 :goto_38

    nop

    :goto_2e
    goto/16 :goto_4d

    :pswitch_2
    goto/32 :goto_6

    nop

    :goto_2f
    goto/16 :goto_76

    :goto_30
    goto/32 :goto_6a

    nop

    :goto_31
    const/4 v10, 0x0

    goto/32 :goto_94

    nop

    :goto_32
    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_44

    nop

    :goto_33
    goto/16 :goto_79

    :pswitch_3
    goto/32 :goto_78

    nop

    :goto_34
    const/4 v0, 0x2

    goto/32 :goto_63

    nop

    :goto_35
    move v8, v10

    :goto_36
    packed-switch v8, :pswitch_data_3

    goto/32 :goto_62

    nop

    :goto_37
    move p2, v8

    goto/32 :goto_56

    nop

    :goto_38
    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    goto/32 :goto_1f

    nop

    :goto_39
    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    goto/32 :goto_4

    nop

    :goto_3a
    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    goto/32 :goto_95

    nop

    :goto_3b
    aget p0, v1, v9

    goto/32 :goto_69

    nop

    :goto_3c
    if-eqz p0, :cond_7

    goto/32 :goto_84

    :cond_7
    goto/32 :goto_83

    nop

    :goto_3d
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    goto/32 :goto_3c

    nop

    :goto_3e
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    goto/32 :goto_49

    nop

    :goto_3f
    goto/16 :goto_27

    :sswitch_1
    goto/32 :goto_55

    nop

    :goto_40
    goto/16 :goto_6b

    :sswitch_2
    goto/32 :goto_28

    nop

    :goto_41
    if-eqz p0, :cond_8

    goto/32 :goto_24

    :cond_8
    goto/32 :goto_23

    nop

    :goto_42
    goto/16 :goto_71

    :goto_43
    goto/32 :goto_80

    nop

    :goto_44
    throw p0

    :goto_45
    goto/32 :goto_5e

    nop

    :goto_46
    move p2, v8

    goto/32 :goto_66

    nop

    :goto_47
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    goto/32 :goto_70

    nop

    :goto_48
    aput v8, v1, v10

    goto/32 :goto_7c

    nop

    :goto_49
    if-eqz p2, :cond_9

    goto/32 :goto_21

    :cond_9
    goto/32 :goto_20

    nop

    :goto_4a
    const-string/jumbo v6, "senders_none"

    goto/32 :goto_2

    nop

    :goto_4b
    if-eqz p0, :cond_a

    goto/32 :goto_b

    :cond_a
    goto/32 :goto_2d

    nop

    :goto_4c
    aput v0, v1, v9

    :goto_4d
    goto/32 :goto_8b

    nop

    :goto_4e
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    goto/32 :goto_81

    nop

    :goto_4f
    const-string/jumbo v4, "senders_starred_contacts"

    goto/32 :goto_72

    nop

    :goto_50
    goto :goto_5c

    :pswitch_4
    goto/32 :goto_5b

    nop

    :goto_51
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_32

    nop

    :goto_52
    goto/16 :goto_4d

    :goto_53
    goto/32 :goto_7a

    nop

    :goto_54
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_6f

    nop

    :goto_55
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    goto/32 :goto_14

    nop

    :goto_56
    goto/16 :goto_6b

    :sswitch_3
    goto/32 :goto_39

    nop

    :goto_57
    move v8, v9

    goto/32 :goto_98

    nop

    :goto_58
    goto/16 :goto_36

    :goto_59
    goto/32 :goto_57

    nop

    :goto_5a
    aput v10, v1, v10

    goto/32 :goto_6e

    nop

    :goto_5b
    aput v0, v1, v10

    :goto_5c
    goto/32 :goto_3

    nop

    :goto_5d
    if-eq p0, p2, :cond_b

    goto/32 :goto_45

    :cond_b
    goto/32 :goto_3b

    nop

    :goto_5e
    return-object v1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x444c2b2c -> :sswitch_1
        -0x7ef016c -> :sswitch_6
        0x66d5177b -> :sswitch_c
        0x695a95f9 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        0x2757b2f3 -> :sswitch_9
        0x66d5177b -> :sswitch_0
        0x695a95f9 -> :sswitch_8
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :sswitch_data_2
    .sparse-switch
        -0x444c2b2c -> :sswitch_4
        -0x7ef016c -> :sswitch_a
        0x66d5177b -> :sswitch_2
        0x695a95f9 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_0
    .end packed-switch

    :sswitch_data_3
    .sparse-switch
        0x2757b2f3 -> :sswitch_d
        0x66d5177b -> :sswitch_7
        0x695a95f9 -> :sswitch_b
    .end sparse-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_8
        :pswitch_1
        :pswitch_5
    .end packed-switch

    :array_0
    .array-data 4
        -0xa
        -0xa
    .end array-data

    :goto_5f
    goto :goto_6b

    :sswitch_4
    goto/32 :goto_4e

    nop

    :goto_60
    goto/16 :goto_36

    :goto_61
    goto/32 :goto_99

    nop

    :goto_62
    goto/16 :goto_4d

    :pswitch_5
    goto/32 :goto_1a

    nop

    :goto_63
    new-array v1, v0, [I

    fill-array-data v1, :array_0

    goto/32 :goto_64

    nop

    :goto_64
    const-string v2, "conversations_important"

    goto/32 :goto_1b

    nop

    :goto_65
    if-eqz p0, :cond_c

    goto/32 :goto_61

    :cond_c
    goto/32 :goto_60

    nop

    :goto_66
    goto/16 :goto_27

    :sswitch_5
    goto/32 :goto_22

    nop

    :goto_67
    move p2, v5

    goto/32 :goto_40

    nop

    :goto_68
    const/4 v8, -0x1

    goto/32 :goto_15

    nop

    :goto_69
    if-ne p0, p2, :cond_d

    goto/32 :goto_1d

    :cond_d
    goto/32 :goto_1c

    nop

    :goto_6a
    move p2, v10

    :goto_6b
    packed-switch p2, :pswitch_data_2

    goto/32 :goto_e

    nop

    :goto_6c
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p0

    sparse-switch p0, :sswitch_data_1

    :goto_6d
    goto/32 :goto_7f

    nop

    :goto_6e
    goto/16 :goto_5c

    :pswitch_6
    goto/32 :goto_5

    nop

    :goto_6f
    const-string v0, "invalid key "

    goto/32 :goto_7d

    nop

    :goto_70
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p2

    sparse-switch p2, :sswitch_data_0

    :goto_71
    goto/32 :goto_46

    nop

    :goto_72
    const/4 v5, 0x3

    goto/32 :goto_4a

    nop

    :goto_73
    goto/16 :goto_27

    :sswitch_6
    goto/32 :goto_3e

    nop

    :goto_74
    goto/16 :goto_36

    :sswitch_7
    goto/32 :goto_3a

    nop

    :goto_75
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p2

    sparse-switch p2, :sswitch_data_2

    :goto_76
    goto/32 :goto_37

    nop

    :goto_77
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_51

    nop

    :goto_78
    aput v8, v1, v10

    :goto_79
    goto/32 :goto_92

    nop

    :goto_7a
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    goto/32 :goto_75

    nop

    :goto_7b
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    goto/32 :goto_41

    nop

    :goto_7c
    goto/16 :goto_5c

    :pswitch_7
    goto/32 :goto_5a

    nop

    :goto_7d
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_77

    nop

    :goto_7e
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    goto/32 :goto_8a

    nop

    :goto_7f
    move v0, v8

    goto/32 :goto_88

    nop

    :goto_80
    move p2, v5

    goto/32 :goto_93

    nop

    :goto_81
    if-eqz p2, :cond_e

    goto/32 :goto_30

    :cond_e
    goto/32 :goto_2f

    nop

    :goto_82
    move p2, v9

    goto/32 :goto_3f

    nop

    :goto_83
    goto :goto_6d

    :goto_84
    goto/32 :goto_a

    nop

    :goto_85
    move p2, v9

    goto/32 :goto_5f

    nop

    :goto_86
    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    goto/32 :goto_4b

    nop

    :goto_87
    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    goto/32 :goto_65

    nop

    :goto_88
    goto/16 :goto_b

    :sswitch_8
    goto/32 :goto_86

    nop

    :goto_89
    move p2, v0

    goto/32 :goto_73

    nop

    :goto_8a
    if-eqz p2, :cond_f

    goto/32 :goto_8

    :cond_f
    goto/32 :goto_7

    nop

    :goto_8b
    aget p0, v1, v10

    goto/32 :goto_1

    nop

    :goto_8c
    goto/16 :goto_b

    :sswitch_9
    goto/32 :goto_3d

    nop

    :goto_8d
    goto/16 :goto_6b

    :sswitch_a
    goto/32 :goto_7e

    nop

    :goto_8e
    if-eqz p2, :cond_10

    goto/32 :goto_2a

    :cond_10
    goto/32 :goto_29

    nop

    :goto_8f
    goto/16 :goto_4d

    :pswitch_8
    goto/32 :goto_4c

    nop

    :goto_90
    goto/16 :goto_36

    :sswitch_b
    goto/32 :goto_87

    nop

    :goto_91
    aput v9, v1, v9

    goto/32 :goto_8f

    nop

    :goto_92
    iget-boolean p0, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mIsMessages:Z

    goto/32 :goto_1e

    nop

    :goto_93
    goto/16 :goto_27

    :sswitch_c
    goto/32 :goto_17

    nop

    :goto_94
    if-eqz p2, :cond_11

    goto/32 :goto_53

    :cond_11
    goto/32 :goto_47

    nop

    :goto_95
    if-eqz p0, :cond_12

    goto/32 :goto_59

    :cond_12
    goto/32 :goto_58

    nop

    :goto_96
    new-instance p0, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_25

    nop

    :goto_97
    move v0, v9

    goto/32 :goto_8c

    nop

    :goto_98
    goto/16 :goto_36

    :sswitch_d
    goto/32 :goto_7b

    nop

    :goto_99
    move v8, v0

    goto/32 :goto_74

    nop
.end method

.method settingsToSaveOnClick(Lcom/android/settingslib/widget/SelectorWithWidgetPreference;II)[I
    .locals 6

    goto/32 :goto_c

    nop

    :goto_0
    goto :goto_3

    :goto_1
    goto/32 :goto_2

    nop

    :goto_2
    move v2, v4

    :goto_3
    goto/32 :goto_2d

    nop

    :goto_4
    aget v2, v1, v3

    goto/32 :goto_7

    nop

    :goto_5
    if-eq p0, p1, :cond_0

    goto/32 :goto_12

    :cond_0
    :goto_6
    goto/32 :goto_2f

    nop

    :goto_7
    aget v1, v1, v4

    goto/32 :goto_1a

    nop

    :goto_8
    const-string v1, "conversations_important"

    goto/32 :goto_28

    nop

    :goto_9
    aput v1, v0, v4

    :goto_a
    goto/32 :goto_19

    nop

    :goto_b
    if-ne v1, p3, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_9

    nop

    :goto_c
    const/4 v0, 0x2

    goto/32 :goto_27

    nop

    :goto_d
    iget-boolean p0, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mIsMessages:Z

    goto/32 :goto_24

    nop

    :goto_e
    if-nez v2, :cond_2

    goto/32 :goto_1

    :cond_2
    goto/32 :goto_20

    nop

    :goto_f
    const/4 v3, 0x0

    goto/32 :goto_16

    nop

    :goto_10
    if-ne p0, p2, :cond_3

    goto/32 :goto_6

    :cond_3
    goto/32 :goto_1d

    nop

    :goto_11
    aput p0, v0, v4

    :goto_12
    goto/32 :goto_2e

    nop

    :goto_13
    aput p0, v0, v3

    :goto_14
    goto/32 :goto_26

    nop

    :goto_15
    move v2, v3

    goto/32 :goto_0

    nop

    :goto_16
    const/4 v4, 0x1

    goto/32 :goto_e

    nop

    :goto_17
    if-ne v1, v5, :cond_4

    goto/32 :goto_a

    :cond_4
    goto/32 :goto_b

    nop

    :goto_18
    const/4 p0, 0x3

    goto/32 :goto_11

    nop

    :goto_19
    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_8

    nop

    :goto_1a
    const/16 v5, -0xa

    goto/32 :goto_1e

    nop

    :goto_1b
    const/4 p0, -0x1

    goto/32 :goto_13

    nop

    :goto_1c
    const-string/jumbo p2, "senders_starred_contacts"

    goto/32 :goto_10

    nop

    :goto_1d
    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_23

    nop

    :goto_1e
    if-ne v2, v5, :cond_5

    goto/32 :goto_2c

    :cond_5
    goto/32 :goto_1f

    nop

    :goto_1f
    if-ne v2, p2, :cond_6

    goto/32 :goto_2c

    :cond_6
    goto/32 :goto_2b

    nop

    :goto_20
    invoke-virtual {p1}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v2

    goto/32 :goto_25

    nop

    :goto_21
    goto/16 :goto_1

    :goto_22
    goto/32 :goto_15

    nop

    :goto_23
    const-string/jumbo p1, "senders_contacts"

    goto/32 :goto_5

    nop

    :goto_24
    if-nez p0, :cond_7

    goto/32 :goto_12

    :cond_7
    goto/32 :goto_17

    nop

    :goto_25
    if-eqz v2, :cond_8

    goto/32 :goto_22

    :cond_8
    goto/32 :goto_21

    nop

    :goto_26
    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_1c

    nop

    :goto_27
    new-array v0, v0, [I

    fill-array-data v0, :array_0

    goto/32 :goto_29

    nop

    :goto_28
    if-eq p0, v1, :cond_9

    goto/32 :goto_14

    :cond_9
    goto/32 :goto_30

    nop

    :goto_29
    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_2a

    nop

    :goto_2a
    invoke-virtual {p1}, Lcom/android/settingslib/widget/SelectorWithWidgetPreference;->isCheckBox()Z

    move-result v2

    goto/32 :goto_f

    nop

    :goto_2b
    aput v2, v0, v3

    :goto_2c
    goto/32 :goto_d

    nop

    :goto_2d
    invoke-virtual {p0, v1, v2}, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->keyToSettingEndState(Ljava/lang/String;Z)[I

    move-result-object v1

    goto/32 :goto_4

    nop

    :goto_2e
    return-object v0

    :array_0
    .array-data 4
        -0xa
        -0xa
    .end array-data

    :goto_2f
    if-eq p3, v4, :cond_a

    goto/32 :goto_12

    :cond_a
    goto/32 :goto_18

    nop

    :goto_30
    if-eqz p2, :cond_b

    goto/32 :goto_14

    :cond_b
    goto/32 :goto_1b

    nop
.end method

.method updateChannelCounts()V
    .locals 3

    goto/32 :goto_a

    nop

    :goto_0
    invoke-virtual {v0}, Landroid/content/pm/ParceledListSlice;->getList()Ljava/util/List;

    move-result-object v0

    goto/32 :goto_10

    nop

    :goto_1
    const/4 v1, 0x0

    goto/32 :goto_13

    nop

    :goto_2
    goto :goto_11

    :goto_3
    goto/32 :goto_5

    nop

    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_2

    nop

    :goto_5
    iput v1, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mNumImportantConversations:I

    goto/32 :goto_6

    nop

    :goto_6
    return-void

    :goto_7
    check-cast v2, Landroid/service/notification/ConversationChannelWrapper;

    goto/32 :goto_9

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    goto/32 :goto_b

    nop

    :goto_9
    invoke-virtual {v2}, Landroid/service/notification/ConversationChannelWrapper;->getNotificationChannel()Landroid/app/NotificationChannel;

    move-result-object v2

    goto/32 :goto_f

    nop

    :goto_a
    iget-object v0, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mNotificationBackend:Lcom/android/settings/notification/NotificationBackend;

    goto/32 :goto_e

    nop

    :goto_b
    if-nez v2, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_c

    nop

    :goto_c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_7

    nop

    :goto_d
    invoke-virtual {v0, v1}, Lcom/android/settings/notification/NotificationBackend;->getConversations(Z)Landroid/content/pm/ParceledListSlice;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_e
    const/4 v1, 0x1

    goto/32 :goto_d

    nop

    :goto_f
    invoke-virtual {v2}, Landroid/app/NotificationChannel;->isDemoted()Z

    move-result v2

    goto/32 :goto_12

    nop

    :goto_10
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_11
    goto/32 :goto_8

    nop

    :goto_12
    if-eqz v2, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_4

    nop

    :goto_13
    if-nez v0, :cond_2

    goto/32 :goto_3

    :cond_2
    goto/32 :goto_0

    nop
.end method

.method updateState(II)V
    .locals 8

    goto/32 :goto_11

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_22

    nop

    :goto_1
    move v6, v4

    :goto_2
    goto/32 :goto_21

    nop

    :goto_3
    check-cast v1, Lcom/android/settingslib/widget/SelectorWithWidgetPreference;

    goto/32 :goto_c

    nop

    :goto_4
    const/4 v4, 0x0

    goto/32 :goto_15

    nop

    :goto_5
    const/4 v3, 0x1

    goto/32 :goto_6

    nop

    :goto_6
    invoke-virtual {p0, v2, v3}, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->keyToSettingEndState(Ljava/lang/String;Z)[I

    move-result-object v2

    goto/32 :goto_4

    nop

    :goto_7
    if-eqz v6, :cond_0

    goto/32 :goto_1a

    :cond_0
    goto/32 :goto_19

    nop

    :goto_8
    move v6, v3

    goto/32 :goto_1c

    nop

    :goto_9
    if-eq v2, p2, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_a

    nop

    :goto_a
    goto :goto_18

    :goto_b
    goto/32 :goto_17

    nop

    :goto_c
    invoke-virtual {v1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_5

    nop

    :goto_d
    goto :goto_10

    :goto_e
    goto/32 :goto_1f

    nop

    :goto_f
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_10
    goto/32 :goto_0

    nop

    :goto_11
    iget-object v0, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mSelectorPreferences:Ljava/util/List;

    goto/32 :goto_f

    nop

    :goto_12
    if-eq v5, p1, :cond_2

    goto/32 :goto_1d

    :cond_2
    goto/32 :goto_8

    nop

    :goto_13
    move v6, v3

    :goto_14
    goto/32 :goto_23

    nop

    :goto_15
    aget v5, v2, v4

    goto/32 :goto_20

    nop

    :goto_16
    if-ne v2, v7, :cond_3

    goto/32 :goto_14

    :cond_3
    goto/32 :goto_7

    nop

    :goto_17
    move v3, v4

    :goto_18
    goto/32 :goto_13

    nop

    :goto_19
    if-eq v5, v7, :cond_4

    goto/32 :goto_b

    :cond_4
    :goto_1a
    goto/32 :goto_9

    nop

    :goto_1b
    if-nez v7, :cond_5

    goto/32 :goto_14

    :cond_5
    goto/32 :goto_1e

    nop

    :goto_1c
    goto/16 :goto_2

    :goto_1d
    goto/32 :goto_1

    nop

    :goto_1e
    const/16 v7, -0xa

    goto/32 :goto_16

    nop

    :goto_1f
    return-void

    :goto_20
    aget v2, v2, v3

    goto/32 :goto_12

    nop

    :goto_21
    iget-boolean v7, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mIsMessages:Z

    goto/32 :goto_1b

    nop

    :goto_22
    if-nez v1, :cond_6

    goto/32 :goto_e

    :cond_6
    goto/32 :goto_24

    nop

    :goto_23
    invoke-virtual {v1, v6}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    goto/32 :goto_d

    nop

    :goto_24
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_3

    nop
.end method

.method updateSummaries()V
    .locals 3

    goto/32 :goto_7

    nop

    :goto_0
    invoke-direct {p0, v2}, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->getSummary(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_9

    nop

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_a

    nop

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_b

    nop

    :goto_3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    goto/32 :goto_1

    nop

    :goto_5
    goto :goto_4

    :goto_6
    goto/32 :goto_8

    nop

    :goto_7
    iget-object v0, p0, Lcom/android/settings/notification/zen/ZenPrioritySendersHelper;->mSelectorPreferences:Ljava/util/List;

    goto/32 :goto_3

    nop

    :goto_8
    return-void

    :goto_9
    invoke-virtual {v1, v2}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/32 :goto_5

    nop

    :goto_a
    if-nez v1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_2

    nop

    :goto_b
    check-cast v1, Lcom/android/settingslib/widget/SelectorWithWidgetPreference;

    goto/32 :goto_c

    nop

    :goto_c
    invoke-virtual {v1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_0

    nop
.end method
