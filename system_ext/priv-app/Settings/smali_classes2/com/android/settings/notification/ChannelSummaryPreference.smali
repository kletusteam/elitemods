.class public Lcom/android/settings/notification/ChannelSummaryPreference;
.super Lcom/android/settingslib/TwoTargetPreference;


# instance fields
.field private mCheckBox:Landroid/widget/CheckBox;

.field private mChecked:Z

.field private mContext:Landroid/content/Context;

.field private mEnableCheckBox:Z

.field private mIntent:Landroid/content/Intent;

.field private mOnCheckBoxClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public static synthetic $r8$lambda$J72q0JP72DIMnKcuJ_SMgAdA5Z0(Lcom/android/settings/notification/ChannelSummaryPreference;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/notification/ChannelSummaryPreference;->lambda$onBindViewHolder$0(Landroid/view/View;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmCheckBox(Lcom/android/settings/notification/ChannelSummaryPreference;)Landroid/widget/CheckBox;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/notification/ChannelSummaryPreference;->mCheckBox:Landroid/widget/CheckBox;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmChecked(Lcom/android/settings/notification/ChannelSummaryPreference;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/notification/ChannelSummaryPreference;->mChecked:Z

    return p0
.end method

.method static synthetic access$000(Lcom/android/settings/notification/ChannelSummaryPreference;Z)Z
    .locals 0

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->persistBoolean(Z)Z

    move-result p0

    return p0
.end method

.method private synthetic lambda$onBindViewHolder$0(Landroid/view/View;)V
    .locals 0

    iget-object p1, p0, Lcom/android/settings/notification/ChannelSummaryPreference;->mContext:Landroid/content/Context;

    iget-object p0, p0, Lcom/android/settings/notification/ChannelSummaryPreference;->mIntent:Landroid/content/Intent;

    invoke-virtual {p1, p0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settingslib/TwoTargetPreference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V

    const v0, 0x1020018

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/android/settings/R$id;->two_target_divider:I

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/notification/ChannelSummaryPreference;->mIntent:Landroid/content/Intent;

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    new-instance v1, Lcom/android/settings/notification/ChannelSummaryPreference$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/ChannelSummaryPreference$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/notification/ChannelSummaryPreference;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_0
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    sget v0, Lcom/android/settings/R$id;->checkbox_container:I

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/android/settings/notification/ChannelSummaryPreference;->mOnCheckBoxClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    const v0, 0x1020001

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/CheckBox;

    iput-object p1, p0, Lcom/android/settings/notification/ChannelSummaryPreference;->mCheckBox:Landroid/widget/CheckBox;

    if-eqz p1, :cond_2

    iget-boolean v0, p0, Lcom/android/settings/notification/ChannelSummaryPreference;->mChecked:Z

    invoke-virtual {p1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object p1, p0, Lcom/android/settings/notification/ChannelSummaryPreference;->mCheckBox:Landroid/widget/CheckBox;

    iget-boolean p0, p0, Lcom/android/settings/notification/ChannelSummaryPreference;->mEnableCheckBox:Z

    invoke-virtual {p1, p0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    :cond_2
    return-void
.end method

.method public onClick()V
    .locals 1

    iget-object p0, p0, Lcom/android/settings/notification/ChannelSummaryPreference;->mOnCheckBoxClickListener:Landroid/view/View$OnClickListener;

    const/4 v0, 0x0

    invoke-interface {p0, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    return-void
.end method

.method public setChecked(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/notification/ChannelSummaryPreference;->mChecked:Z

    iget-object p0, p0, Lcom/android/settings/notification/ChannelSummaryPreference;->mCheckBox:Landroid/widget/CheckBox;

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/ChannelSummaryPreference;->mIntent:Landroid/content/Intent;

    return-void
.end method
