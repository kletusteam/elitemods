.class public Lcom/android/settings/notification/history/NotificationSbnViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;


# instance fields
.field private final mDivider:Landroid/view/View;

.field private final mIcon:Landroid/widget/ImageView;

.field private final mPkgName:Landroid/widget/TextView;

.field private final mProfileBadge:Landroid/widget/ImageView;

.field private final mSummary:Landroid/widget/TextView;

.field private final mTime:Landroid/widget/DateTimeView;

.field private final mTitle:Landroid/widget/TextView;


# direct methods
.method public static synthetic $r8$lambda$A-DCYeekPGpIhvTj6V-HIwW6F-g(Lcom/android/settings/notification/history/NotificationSbnViewHolder;Lcom/android/internal/logging/UiEventLogger;ZILjava/lang/String;Lcom/android/internal/logging/InstanceId;ILandroid/app/PendingIntent;ZLandroid/content/Intent;ILandroid/view/View;)V
    .locals 0

    invoke-direct/range {p0 .. p11}, Lcom/android/settings/notification/history/NotificationSbnViewHolder;->lambda$addOnClick$0(Lcom/android/internal/logging/UiEventLogger;ZILjava/lang/String;Lcom/android/internal/logging/InstanceId;ILandroid/app/PendingIntent;ZLandroid/content/Intent;ILandroid/view/View;)V

    return-void
.end method

.method constructor <init>(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    sget v0, Lcom/android/settings/R$id;->pkgname:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/notification/history/NotificationSbnViewHolder;->mPkgName:Landroid/widget/TextView;

    sget v0, Lcom/android/settings/R$id;->icon:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/notification/history/NotificationSbnViewHolder;->mIcon:Landroid/widget/ImageView;

    sget v0, Lcom/android/settings/R$id;->timestamp:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/DateTimeView;

    iput-object v0, p0, Lcom/android/settings/notification/history/NotificationSbnViewHolder;->mTime:Landroid/widget/DateTimeView;

    sget v0, Lcom/android/settings/R$id;->title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/notification/history/NotificationSbnViewHolder;->mTitle:Landroid/widget/TextView;

    sget v0, Lcom/android/settings/R$id;->text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/notification/history/NotificationSbnViewHolder;->mSummary:Landroid/widget/TextView;

    sget v0, Lcom/android/settings/R$id;->profile_badge:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/notification/history/NotificationSbnViewHolder;->mProfileBadge:Landroid/widget/ImageView;

    sget v0, Lcom/android/settings/R$id;->divider:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/notification/history/NotificationSbnViewHolder;->mDivider:Landroid/view/View;

    return-void
.end method

.method private synthetic lambda$addOnClick$0(Lcom/android/internal/logging/UiEventLogger;ZILjava/lang/String;Lcom/android/internal/logging/InstanceId;ILandroid/app/PendingIntent;ZLandroid/content/Intent;ILandroid/view/View;)V
    .locals 8

    move-object/from16 v0, p9

    if-eqz p2, :cond_0

    sget-object v1, Lcom/android/settings/notification/history/NotificationHistoryActivity$NotificationHistoryEvent;->NOTIFICATION_HISTORY_SNOOZED_ITEM_CLICK:Lcom/android/settings/notification/history/NotificationHistoryActivity$NotificationHistoryEvent;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/android/settings/notification/history/NotificationHistoryActivity$NotificationHistoryEvent;->NOTIFICATION_HISTORY_RECENT_ITEM_CLICK:Lcom/android/settings/notification/history/NotificationHistoryActivity$NotificationHistoryEvent;

    :goto_0
    move-object v3, v1

    move-object v2, p1

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    invoke-interface/range {v2 .. v7}, Lcom/android/internal/logging/UiEventLogger;->logWithInstanceIdAndPosition(Lcom/android/internal/logging/UiEventLogger$UiEventEnum;ILjava/lang/String;Lcom/android/internal/logging/InstanceId;I)V

    const-string v1, "SbnViewHolder"

    if-eqz p7, :cond_1

    if-eqz p8, :cond_1

    :try_start_0
    invoke-virtual {p7}, Landroid/app/PendingIntent;->send()V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v2, v0

    const-string v0, "Could not launch"

    invoke-static {v1, v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_1
    if-eqz v0, :cond_2

    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-object v2, p0

    :try_start_1
    iget-object v2, v2, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static/range {p10 .. p10}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    const-string/jumbo v2, "no launch activity"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    :goto_1
    return-void
.end method


# virtual methods
.method addOnClick(ILjava/lang/String;IILandroid/app/PendingIntent;Lcom/android/internal/logging/InstanceId;ZLcom/android/internal/logging/UiEventLogger;)V
    .locals 15

    goto/32 :goto_f

    nop

    :goto_0
    goto/16 :goto_20

    :goto_1
    goto/32 :goto_1f

    nop

    :goto_2
    move-object/from16 v5, p2

    goto/32 :goto_1e

    nop

    :goto_3
    iget-object v0, v12, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_7

    nop

    :goto_4
    invoke-virtual/range {p5 .. p5}, Landroid/app/PendingIntent;->isActivity()Z

    move-result v0

    goto/32 :goto_11

    nop

    :goto_5
    invoke-static {v0, v1}, Landroidx/core/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroidx/core/view/AccessibilityDelegateCompat;)V

    :goto_6
    goto/32 :goto_1d

    nop

    :goto_7
    new-instance v1, Lcom/android/settings/notification/history/NotificationSbnViewHolder$1;

    goto/32 :goto_24

    nop

    :goto_8
    move/from16 v3, p7

    goto/32 :goto_12

    nop

    :goto_9
    move-object/from16 v2, p8

    goto/32 :goto_8

    nop

    :goto_a
    if-nez v10, :cond_0

    goto/32 :goto_6

    :cond_0
    :goto_b
    goto/32 :goto_c

    nop

    :goto_c
    iget-object v13, v12, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_16

    nop

    :goto_d
    iget-object v0, v12, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_18

    nop

    :goto_e
    if-eqz v9, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_a

    nop

    :goto_f
    move-object v12, p0

    goto/32 :goto_d

    nop

    :goto_10
    move v9, v0

    goto/32 :goto_e

    nop

    :goto_11
    if-nez v0, :cond_2

    goto/32 :goto_1

    :cond_2
    goto/32 :goto_13

    nop

    :goto_12
    move/from16 v4, p3

    goto/32 :goto_23

    nop

    :goto_13
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_14
    move-object/from16 v8, p5

    goto/32 :goto_19

    nop

    :goto_15
    move-object v1, p0

    goto/32 :goto_9

    nop

    :goto_16
    new-instance v14, Lcom/android/settings/notification/history/NotificationSbnViewHolder$$ExternalSyntheticLambda0;

    goto/32 :goto_1a

    nop

    :goto_17
    if-nez p5, :cond_3

    goto/32 :goto_1

    :cond_3
    goto/32 :goto_4

    nop

    :goto_18
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    goto/32 :goto_21

    nop

    :goto_19
    move/from16 v11, p4

    goto/32 :goto_1c

    nop

    :goto_1a
    move-object v0, v14

    goto/32 :goto_15

    nop

    :goto_1b
    move-object/from16 v6, p6

    goto/32 :goto_22

    nop

    :goto_1c
    invoke-direct/range {v0 .. v11}, Lcom/android/settings/notification/history/NotificationSbnViewHolder$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/notification/history/NotificationSbnViewHolder;Lcom/android/internal/logging/UiEventLogger;ZILjava/lang/String;Lcom/android/internal/logging/InstanceId;ILandroid/app/PendingIntent;ZLandroid/content/Intent;I)V

    goto/32 :goto_25

    nop

    :goto_1d
    return-void

    :goto_1e
    invoke-virtual {v0, v5}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v10

    goto/32 :goto_17

    nop

    :goto_1f
    const/4 v0, 0x0

    :goto_20
    goto/32 :goto_10

    nop

    :goto_21
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_22
    move/from16 v7, p1

    goto/32 :goto_14

    nop

    :goto_23
    move-object/from16 v5, p2

    goto/32 :goto_1b

    nop

    :goto_24
    invoke-direct {v1, p0}, Lcom/android/settings/notification/history/NotificationSbnViewHolder$1;-><init>(Lcom/android/settings/notification/history/NotificationSbnViewHolder;)V

    goto/32 :goto_5

    nop

    :goto_25
    invoke-virtual {v13, v14}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/32 :goto_3

    nop
.end method

.method setDividerVisible(Z)V
    .locals 0

    goto/32 :goto_4

    nop

    :goto_0
    const/4 p1, 0x0

    goto/32 :goto_5

    nop

    :goto_1
    return-void

    :goto_2
    const/16 p1, 0x8

    :goto_3
    goto/32 :goto_7

    nop

    :goto_4
    iget-object p0, p0, Lcom/android/settings/notification/history/NotificationSbnViewHolder;->mDivider:Landroid/view/View;

    goto/32 :goto_8

    nop

    :goto_5
    goto :goto_3

    :goto_6
    goto/32 :goto_2

    nop

    :goto_7
    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    goto/32 :goto_1

    nop

    :goto_8
    if-nez p1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_0

    nop
.end method

.method setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    iget-object p0, p0, Lcom/android/settings/notification/history/NotificationSbnViewHolder;->mIcon:Landroid/widget/ImageView;

    goto/32 :goto_0

    nop
.end method

.method setIconBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iget-object p0, p0, Lcom/android/settings/notification/history/NotificationSbnViewHolder;->mIcon:Landroid/widget/ImageView;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/32 :goto_0

    nop
.end method

.method setPackageLabel(Ljava/lang/String;)V
    .locals 0

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_0

    nop

    :goto_2
    iget-object p0, p0, Lcom/android/settings/notification/history/NotificationSbnViewHolder;->mPkgName:Landroid/widget/TextView;

    goto/32 :goto_1

    nop
.end method

.method setPostedTime(J)V
    .locals 0

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p0, p1, p2}, Landroid/widget/DateTimeView;->setTime(J)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    iget-object p0, p0, Lcom/android/settings/notification/history/NotificationSbnViewHolder;->mTime:Landroid/widget/DateTimeView;

    goto/32 :goto_0

    nop
.end method

.method setProfileBadge(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    goto/32 :goto_a

    nop

    :goto_0
    goto :goto_9

    :goto_1
    goto/32 :goto_8

    nop

    :goto_2
    if-nez p1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_6

    nop

    :goto_3
    return-void

    :goto_4
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/32 :goto_5

    nop

    :goto_5
    iget-object p0, p0, Lcom/android/settings/notification/history/NotificationSbnViewHolder;->mProfileBadge:Landroid/widget/ImageView;

    goto/32 :goto_2

    nop

    :goto_6
    const/4 p1, 0x0

    goto/32 :goto_0

    nop

    :goto_7
    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/32 :goto_3

    nop

    :goto_8
    const/16 p1, 0x8

    :goto_9
    goto/32 :goto_7

    nop

    :goto_a
    iget-object v0, p0, Lcom/android/settings/notification/history/NotificationSbnViewHolder;->mProfileBadge:Landroid/widget/ImageView;

    goto/32 :goto_4

    nop
.end method

.method setSummary(Ljava/lang/CharSequence;)V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    if-nez v1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_b

    nop

    :goto_1
    iget-object v0, p0, Lcom/android/settings/notification/history/NotificationSbnViewHolder;->mSummary:Landroid/widget/TextView;

    goto/32 :goto_5

    nop

    :goto_2
    return-void

    :goto_3
    const/4 v1, 0x0

    :goto_4
    goto/32 :goto_8

    nop

    :goto_5
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    goto/32 :goto_0

    nop

    :goto_6
    goto :goto_4

    :goto_7
    goto/32 :goto_3

    nop

    :goto_8
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/32 :goto_a

    nop

    :goto_9
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_2

    nop

    :goto_a
    iget-object p0, p0, Lcom/android/settings/notification/history/NotificationSbnViewHolder;->mSummary:Landroid/widget/TextView;

    goto/32 :goto_9

    nop

    :goto_b
    const/16 v1, 0x8

    goto/32 :goto_6

    nop
.end method

.method setTitle(Ljava/lang/CharSequence;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iget-object p0, p0, Lcom/android/settings/notification/history/NotificationSbnViewHolder;->mTitle:Landroid/widget/TextView;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method
