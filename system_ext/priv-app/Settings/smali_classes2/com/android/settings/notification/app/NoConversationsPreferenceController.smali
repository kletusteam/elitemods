.class Lcom/android/settings/notification/app/NoConversationsPreferenceController;
.super Lcom/android/settingslib/core/AbstractPreferenceController;


# instance fields
.field private mIsAvailable:Z


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settingslib/core/AbstractPreferenceController;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/settings/notification/app/NoConversationsPreferenceController;->mIsAvailable:Z

    return-void
.end method


# virtual methods
.method public getPreferenceKey()Ljava/lang/String;
    .locals 0

    const-string/jumbo p0, "no_conversations"

    return-object p0
.end method

.method public isAvailable()Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/notification/app/NoConversationsPreferenceController;->mIsAvailable:Z

    return p0
.end method

.method setAvailable(Z)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iput-boolean p1, p0, Lcom/android/settings/notification/app/NoConversationsPreferenceController;->mIsAvailable:Z

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method
