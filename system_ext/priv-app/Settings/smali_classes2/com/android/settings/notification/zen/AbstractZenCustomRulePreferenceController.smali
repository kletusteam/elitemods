.class abstract Lcom/android/settings/notification/zen/AbstractZenCustomRulePreferenceController;
.super Lcom/android/settings/notification/zen/AbstractZenModePreferenceController;


# instance fields
.field mId:Ljava/lang/String;

.field mRule:Landroid/app/AutomaticZenRule;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/android/settingslib/core/lifecycle/Lifecycle;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/notification/zen/AbstractZenModePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/android/settingslib/core/lifecycle/Lifecycle;)V

    return-void
.end method


# virtual methods
.method createBundle()Landroid/os/Bundle;
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto/32 :goto_5

    nop

    :goto_1
    new-instance v0, Landroid/os/Bundle;

    goto/32 :goto_0

    nop

    :goto_2
    const-string v1, "RULE_ID"

    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_4

    nop

    :goto_4
    return-object v0

    :goto_5
    iget-object p0, p0, Lcom/android/settings/notification/zen/AbstractZenCustomRulePreferenceController;->mId:Ljava/lang/String;

    goto/32 :goto_2

    nop
.end method

.method public isAvailable()Z
    .locals 0

    iget-object p0, p0, Lcom/android/settings/notification/zen/AbstractZenCustomRulePreferenceController;->mRule:Landroid/app/AutomaticZenRule;

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public onResume()V
    .locals 0

    return-void
.end method

.method public setIdAndRule(Ljava/lang/String;Landroid/app/AutomaticZenRule;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/zen/AbstractZenCustomRulePreferenceController;->mId:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/settings/notification/zen/AbstractZenCustomRulePreferenceController;->mRule:Landroid/app/AutomaticZenRule;

    return-void
.end method
