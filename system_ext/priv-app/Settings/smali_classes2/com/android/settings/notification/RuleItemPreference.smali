.class public Lcom/android/settings/notification/RuleItemPreference;
.super Lcom/android/settingslib/miuisettings/preference/Preference;


# instance fields
.field public checked:Z

.field private mCheckListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mClickListener:Landroid/view/View$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private volatile mIsEditMode:Z

.field private mIsSelected:Z

.field private mOnLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mSelectedCb:Landroid/widget/CheckBox;

.field private mSlidingBtn:Lmiuix/slidingwidget/widget/SlidingButton;

.field private mSummary:Landroid/widget/TextView;

.field private mTitle:Landroid/widget/TextView;

.field private ruleConditionId:Landroid/net/Uri;

.field private ruleid:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public static synthetic $r8$lambda$5SC_vk-xpPsjiRwXti4oceuZeJY(Lcom/android/settings/notification/RuleItemPreference;Landroid/widget/CompoundButton;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/notification/RuleItemPreference;->lambda$onBindViewForEdit$2(Landroid/widget/CompoundButton;Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$DCcUQc9QtGOc4JhzHmuzl5bL14A(Lcom/android/settings/notification/RuleItemPreference;Landroid/view/View;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/notification/RuleItemPreference;->OnViewLongClickListener(Landroid/view/View;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$eM0TGYW5VxAxOOiGc-mL2ASRZkc(Lcom/android/settings/notification/RuleItemPreference;Landroid/widget/CompoundButton;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/notification/RuleItemPreference;->lambda$onBindView$0(Landroid/widget/CompoundButton;Z)V

    return-void
.end method

.method public static synthetic $r8$lambda$eNPTTOEU_105_EvW2MXl89vXJL4(Lcom/android/settings/notification/RuleItemPreference;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/notification/RuleItemPreference;->onViewClick(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$mFXK6mehquFcy3Eg7ItFefFb1Xc(Lcom/android/settings/notification/RuleItemPreference;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/notification/RuleItemPreference;->lambda$onBindViewForEdit$1(Landroid/view/View;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settingslib/miuisettings/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/android/settings/notification/RuleItemPreference;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settingslib/miuisettings/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object p1, p0, Lcom/android/settings/notification/RuleItemPreference;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settingslib/miuisettings/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object p1, p0, Lcom/android/settings/notification/RuleItemPreference;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;Landroid/net/Uri;Landroid/view/View$OnClickListener;Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/notification/RuleItemPreference;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settings/notification/RuleItemPreference;->title:Ljava/lang/String;

    iput-object p6, p0, Lcom/android/settings/notification/RuleItemPreference;->mClickListener:Landroid/view/View$OnClickListener;

    iput-object p7, p0, Lcom/android/settings/notification/RuleItemPreference;->mCheckListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    iput-boolean p3, p0, Lcom/android/settings/notification/RuleItemPreference;->checked:Z

    iput-object p4, p0, Lcom/android/settings/notification/RuleItemPreference;->ruleid:Ljava/lang/String;

    iput-object p5, p0, Lcom/android/settings/notification/RuleItemPreference;->ruleConditionId:Landroid/net/Uri;

    invoke-virtual {p0, p4}, Landroidx/preference/Preference;->setKey(Ljava/lang/String;)V

    return-void
.end method

.method private OnViewLongClickListener(Landroid/view/View;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/notification/RuleItemPreference;->mIsEditMode:Z

    if-nez v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/notification/RuleItemPreference;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-interface {p0, p1}, Landroid/view/View$OnLongClickListener;->onLongClick(Landroid/view/View;)Z

    :cond_0
    const/4 p0, 0x1

    return p0
.end method

.method private synthetic lambda$onBindView$0(Landroid/widget/CompoundButton;Z)V
    .locals 0

    iput-boolean p2, p0, Lcom/android/settings/notification/RuleItemPreference;->checked:Z

    iget-object p0, p0, Lcom/android/settings/notification/RuleItemPreference;->mCheckListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    if-eqz p0, :cond_0

    invoke-interface {p0, p1, p2}, Landroid/widget/CompoundButton$OnCheckedChangeListener;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    :cond_0
    return-void
.end method

.method private synthetic lambda$onBindViewForEdit$1(Landroid/view/View;)V
    .locals 0

    iget-object p1, p0, Lcom/android/settings/notification/RuleItemPreference;->mSelectedCb:Landroid/widget/CheckBox;

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Lcom/android/settings/notification/RuleItemPreference;->mIsSelected:Z

    iget-object p0, p0, Lcom/android/settings/notification/RuleItemPreference;->mSelectedCb:Landroid/widget/CheckBox;

    invoke-virtual {p0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    return-void
.end method

.method private synthetic lambda$onBindViewForEdit$2(Landroid/widget/CompoundButton;Z)V
    .locals 0

    iget-object p0, p0, Lcom/android/settings/notification/RuleItemPreference;->mCheckListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    if-eqz p0, :cond_0

    invoke-interface {p0, p1, p2}, Landroid/widget/CompoundButton$OnCheckedChangeListener;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    :cond_0
    return-void
.end method

.method private onBindViewForEdit(Landroid/view/View;)V
    .locals 3

    sget v0, Lcom/android/settings/R$id;->select_cb:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/android/settings/notification/RuleItemPreference;->mSelectedCb:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/android/settings/notification/RuleItemPreference;->ruleid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/android/settings/notification/RuleItemPreference;->mIsEditMode:Z

    const/4 v1, 0x0

    const/16 v2, 0x8

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/settings/notification/RuleItemPreference$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0}, Lcom/android/settings/notification/RuleItemPreference$$ExternalSyntheticLambda3;-><init>(Lcom/android/settings/notification/RuleItemPreference;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/android/settings/notification/RuleItemPreference;->mSelectedCb:Landroid/widget/CheckBox;

    invoke-virtual {p1, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object p1, p0, Lcom/android/settings/notification/RuleItemPreference;->mSlidingBtn:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {p1, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/android/settings/notification/RuleItemPreference;->mSelectedCb:Landroid/widget/CheckBox;

    invoke-virtual {p1, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object p1, p0, Lcom/android/settings/notification/RuleItemPreference;->mSlidingBtn:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {p1, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    :goto_0
    iget-object p1, p0, Lcom/android/settings/notification/RuleItemPreference;->mSelectedCb:Landroid/widget/CheckBox;

    iget-boolean v0, p0, Lcom/android/settings/notification/RuleItemPreference;->mIsSelected:Z

    invoke-virtual {p1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object p1, p0, Lcom/android/settings/notification/RuleItemPreference;->mSelectedCb:Landroid/widget/CheckBox;

    new-instance v0, Lcom/android/settings/notification/RuleItemPreference$$ExternalSyntheticLambda4;

    invoke-direct {v0, p0}, Lcom/android/settings/notification/RuleItemPreference$$ExternalSyntheticLambda4;-><init>(Lcom/android/settings/notification/RuleItemPreference;)V

    invoke-virtual {p1, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method private onViewClick(Landroid/view/View;)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/notification/RuleItemPreference;->mIsEditMode:Z

    if-nez v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/notification/RuleItemPreference;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {p0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public getRuleId()Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/notification/RuleItemPreference;->ruleid:Ljava/lang/String;

    return-object p0
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settingslib/miuisettings/preference/Preference;->onBindView(Landroid/view/View;)V

    sget v0, Lcom/android/settings/R$id;->enabled:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object v0, p0, Lcom/android/settings/notification/RuleItemPreference;->mSlidingBtn:Lmiuix/slidingwidget/widget/SlidingButton;

    sget v0, Lcom/android/settings/R$id;->ruletitle:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/notification/RuleItemPreference;->mTitle:Landroid/widget/TextView;

    sget v0, Lcom/android/settings/R$id;->rulesummary:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/notification/RuleItemPreference;->mSummary:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/notification/RuleItemPreference;->mTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/notification/RuleItemPreference;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/notification/RuleItemPreference;->mTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/notification/RuleItemPreference;->ruleid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/settings/notification/RuleItemPreference;->mSummary:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/notification/RuleItemPreference;->ruleid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/settings/notification/RuleItemPreference;->mSummary:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/settings/notification/RuleItemPreference;->zenRuleAnalysis()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/notification/RuleItemPreference;->mSlidingBtn:Lmiuix/slidingwidget/widget/SlidingButton;

    iget-object v1, p0, Lcom/android/settings/notification/RuleItemPreference;->ruleid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/settings/notification/RuleItemPreference;->mTitle:Landroid/widget/TextView;

    new-instance v1, Lcom/android/settings/notification/RuleItemPreference$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/RuleItemPreference$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/notification/RuleItemPreference;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/notification/RuleItemPreference;->mTitle:Landroid/widget/TextView;

    new-instance v1, Lcom/android/settings/notification/RuleItemPreference$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/RuleItemPreference$$ExternalSyntheticLambda1;-><init>(Lcom/android/settings/notification/RuleItemPreference;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v0, p0, Lcom/android/settings/notification/RuleItemPreference;->mSummary:Landroid/widget/TextView;

    new-instance v1, Lcom/android/settings/notification/RuleItemPreference$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/RuleItemPreference$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/notification/RuleItemPreference;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/notification/RuleItemPreference;->mSummary:Landroid/widget/TextView;

    new-instance v1, Lcom/android/settings/notification/RuleItemPreference$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/RuleItemPreference$$ExternalSyntheticLambda1;-><init>(Lcom/android/settings/notification/RuleItemPreference;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v0, p0, Lcom/android/settings/notification/RuleItemPreference;->mSlidingBtn:Lmiuix/slidingwidget/widget/SlidingButton;

    iget-boolean v1, p0, Lcom/android/settings/notification/RuleItemPreference;->checked:Z

    invoke-virtual {v0, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/notification/RuleItemPreference;->mSlidingBtn:Lmiuix/slidingwidget/widget/SlidingButton;

    new-instance v1, Lcom/android/settings/notification/RuleItemPreference$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0}, Lcom/android/settings/notification/RuleItemPreference$$ExternalSyntheticLambda2;-><init>(Lcom/android/settings/notification/RuleItemPreference;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/notification/RuleItemPreference;->ruleid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    new-instance v0, Lcom/android/settings/notification/RuleItemPreference$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/android/settings/notification/RuleItemPreference$$ExternalSyntheticLambda1;-><init>(Lcom/android/settings/notification/RuleItemPreference;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    invoke-direct {p0, p1}, Lcom/android/settings/notification/RuleItemPreference;->onBindViewForEdit(Landroid/view/View;)V

    return-void
.end method

.method protected onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    sget v0, Lcom/android/settings/R$layout;->rule_item:I

    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setLayoutResource(I)V

    invoke-super {p0, p1}, Lcom/android/settingslib/miuisettings/preference/Preference;->onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public setEditMode(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/notification/RuleItemPreference;->mIsEditMode:Z

    invoke-virtual {p0}, Landroidx/preference/Preference;->shouldDisableDependents()Z

    move-result p1

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->notifyDependencyChange(Z)V

    invoke-virtual {p0}, Landroidx/preference/Preference;->notifyChanged()V

    return-void
.end method

.method public setIsSelected(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/notification/RuleItemPreference;->mIsSelected:Z

    return-void
.end method

.method public setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/notification/RuleItemPreference;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    return-void
.end method

.method public setSelected(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/notification/RuleItemPreference;->mIsSelected:Z

    invoke-virtual {p0}, Landroidx/preference/Preference;->shouldDisableDependents()Z

    move-result p1

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->notifyDependencyChange(Z)V

    invoke-virtual {p0}, Landroidx/preference/Preference;->notifyChanged()V

    return-void
.end method

.method public zenRuleAnalysis()Ljava/lang/String;
    .locals 7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/settings/notification/RuleItemPreference;->ruleConditionId:Landroid/net/Uri;

    invoke-static {v1}, Landroid/service/notification/ZenModeConfig;->tryParseScheduleConditionId(Landroid/net/Uri;)Landroid/service/notification/ZenModeConfig$ScheduleInfo;

    move-result-object v1

    iget v2, v1, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->startHour:I

    mul-int/lit8 v2, v2, 0x3c

    iget v3, v1, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->startMinute:I

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x3c

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ":"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, v1, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->startHour:I

    mul-int/lit8 v3, v3, 0x3c

    iget v4, v1, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->startMinute:I

    add-int/2addr v3, v4

    rem-int/lit8 v3, v3, 0x3c

    const-string v4, "0"

    const/16 v5, 0xa

    if-ge v3, v5, :cond_0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "--"

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, v1, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->endHour:I

    mul-int/lit8 v3, v3, 0x3c

    iget v6, v1, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->endMinute:I

    add-int/2addr v3, v6

    div-int/lit8 v3, v3, 0x3c

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, v1, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->endHour:I

    mul-int/lit8 v2, v2, 0x3c

    iget v3, v1, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->endMinute:I

    add-int/2addr v2, v3

    rem-int/lit8 v2, v2, 0x3c

    if-ge v2, v5, :cond_1

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v2, Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

    iget-object v1, v1, Landroid/service/notification/ZenModeConfig$ScheduleInfo;->days:[I

    invoke-static {v1}, Lcom/android/settings/notification/SilentModeUtils;->parseDays([I)I

    move-result v1

    invoke-direct {v2, v1}, Lcom/android/settings/dndmode/Alarm$DaysOfWeek;-><init>(I)V

    iget-object p0, p0, Lcom/android/settings/notification/RuleItemPreference;->mContext:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-virtual {v2, p0, v1}, Lcom/android/settings/dndmode/Alarm$DaysOfWeek;->toString(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
