.class public Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/notification/zen/ZenModeSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SummaryBuilder"
.end annotation


# static fields
.field private static final ALL_PRIORITY_CATEGORIES:[I


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public static synthetic $r8$lambda$X2V0wrsnrfKvaIzsMaYV4b0kEm0(Ljava/lang/Integer;)Z
    .locals 0

    invoke-static {p0}, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->lambda$getCallsSettingSummary$1(Ljava/lang/Integer;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$cJHlRy7SFTBEdznxaTr_02yeBRU(Ljava/lang/Integer;)Z
    .locals 0

    invoke-static {p0}, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->lambda$getMessagesSettingSummary$2(Ljava/lang/Integer;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$f68SIop6NlXK3uLhACoU9A6u0bc(Ljava/lang/Integer;)Z
    .locals 0

    invoke-static {p0}, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->lambda$getOtherSoundCategoriesSummary$0(Ljava/lang/Integer;)Z

    move-result p0

    return p0
.end method

.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->ALL_PRIORITY_CATEGORIES:[I

    return-void

    :array_0
    .array-data 4
        0x20
        0x40
        0x80
        0x4
        0x100
        0x2
        0x1
        0x8
        0x10
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    return-void
.end method

.method private getCategory(ILandroid/app/NotificationManager$Policy;Z)Ljava/lang/String;
    .locals 3

    const/16 v0, 0x20

    if-ne p1, v0, :cond_1

    if-eqz p3, :cond_0

    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->zen_mode_alarms_list_first:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->zen_mode_alarms_list:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    const/16 v0, 0x40

    if-ne p1, v0, :cond_3

    if-eqz p3, :cond_2

    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->zen_mode_media_list_first:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_2
    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->zen_mode_media_list:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_3
    const/16 v0, 0x80

    if-ne p1, v0, :cond_5

    if-eqz p3, :cond_4

    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->zen_mode_system_list_first:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_4
    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->zen_mode_system_list:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_5
    const/4 v0, 0x4

    const/4 v1, 0x1

    if-ne p1, v0, :cond_8

    iget p1, p2, Landroid/app/NotificationManager$Policy;->priorityMessageSenders:I

    if-nez p1, :cond_6

    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->zen_mode_from_anyone:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_6
    if-ne p1, v1, :cond_7

    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->zen_mode_from_contacts:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_7
    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->zen_mode_from_starred:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_8
    const/16 v0, 0x100

    const/4 v2, 0x2

    if-ne p1, v0, :cond_a

    iget v0, p2, Landroid/app/NotificationManager$Policy;->priorityConversationSenders:I

    if-ne v0, v2, :cond_a

    if-eqz p3, :cond_9

    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->zen_mode_from_important_conversations:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_9
    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->zen_mode_from_important_conversations_second:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_a
    if-ne p1, v2, :cond_c

    if-eqz p3, :cond_b

    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->zen_mode_events_list_first:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_b
    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->zen_mode_events_list:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_c
    if-ne p1, v1, :cond_e

    if-eqz p3, :cond_d

    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->zen_mode_reminders_list_first:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_d
    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->zen_mode_reminders_list:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_e
    const/16 v0, 0x8

    if-ne p1, v0, :cond_14

    iget p1, p2, Landroid/app/NotificationManager$Policy;->priorityCallSenders:I

    if-nez p1, :cond_10

    if-eqz p3, :cond_f

    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->zen_mode_from_anyone:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_f
    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->zen_mode_all_callers:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_10
    if-ne p1, v1, :cond_12

    if-eqz p3, :cond_11

    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->zen_mode_from_contacts:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_11
    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->zen_mode_contacts_callers:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_12
    if-eqz p3, :cond_13

    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->zen_mode_from_starred:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_13
    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->zen_mode_starred_callers:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_14
    const/16 p2, 0x10

    if-ne p1, p2, :cond_16

    if-eqz p3, :cond_15

    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->zen_mode_repeat_callers:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_15
    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->zen_mode_repeat_callers_list:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_16
    const-string p0, ""

    return-object p0
.end method

.method private getEnabledCategories(Landroid/app/NotificationManager$Policy;Ljava/util/function/Predicate;Z)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/NotificationManager$Policy;",
            "Ljava/util/function/Predicate<",
            "Ljava/lang/Integer;",
            ">;Z)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->ALL_PRIORITY_CATEGORIES:[I

    array-length v2, v1

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v2, :cond_4

    aget v5, v1, v4

    if-eqz p3, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v6, 0x1

    goto :goto_1

    :cond_0
    move v6, v3

    :goto_1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {p2, v7}, Ljava/util/function/Predicate;->test(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-direct {p0, p1, v5}, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->isCategoryEnabled(Landroid/app/NotificationManager$Policy;I)Z

    move-result v7

    if-eqz v7, :cond_3

    const/16 v7, 0x10

    if-ne v5, v7, :cond_1

    const/16 v7, 0x8

    invoke-direct {p0, p1, v7}, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->isCategoryEnabled(Landroid/app/NotificationManager$Policy;I)Z

    move-result v7

    if-eqz v7, :cond_1

    iget v7, p1, Landroid/app/NotificationManager$Policy;->priorityCallSenders:I

    if-nez v7, :cond_1

    goto :goto_2

    :cond_1
    const/16 v7, 0x100

    if-ne v5, v7, :cond_2

    invoke-direct {p0, p1, v7}, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->isCategoryEnabled(Landroid/app/NotificationManager$Policy;I)Z

    move-result v7

    if-eqz v7, :cond_2

    iget v7, p1, Landroid/app/NotificationManager$Policy;->priorityConversationSenders:I

    const/4 v8, 0x2

    if-eq v7, v8, :cond_2

    goto :goto_2

    :cond_2
    invoke-direct {p0, v5, p1, v6}, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->getCategory(ILandroid/app/NotificationManager$Policy;Z)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_4
    return-object v0
.end method

.method private isCategoryEnabled(Landroid/app/NotificationManager$Policy;I)Z
    .locals 0

    iget p0, p1, Landroid/app/NotificationManager$Policy;->priorityCategories:I

    and-int/2addr p0, p2

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static synthetic lambda$getCallsSettingSummary$1(Ljava/lang/Integer;)Z
    .locals 2

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x8

    if-eq v1, v0, :cond_1

    const/16 v0, 0x10

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    if-ne v0, p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private static synthetic lambda$getMessagesSettingSummary$2(Ljava/lang/Integer;)Z
    .locals 2

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x4

    if-eq v1, v0, :cond_1

    const/16 v0, 0x100

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    if-ne v0, p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private static synthetic lambda$getOtherSoundCategoriesSummary$0(Ljava/lang/Integer;)Z
    .locals 3

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    const/16 v2, 0x20

    if-eq v2, v0, :cond_1

    const/16 v0, 0x40

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v0, v2, :cond_1

    const/16 v0, 0x80

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v0, v2, :cond_1

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v1, v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    if-ne v0, p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method


# virtual methods
.method getAutomaticRulesSummary()Ljava/lang/String;
    .locals 3

    goto/32 :goto_7

    nop

    :goto_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    goto/32 :goto_8

    nop

    :goto_1
    invoke-virtual {v0, v1}, Landroid/icu/text/MessageFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_4

    nop

    :goto_2
    sget v2, Lcom/android/settings/R$string;->zen_mode_settings_schedules_summary:I

    goto/32 :goto_a

    nop

    :goto_3
    invoke-direct {v0, v1, v2}, Landroid/icu/text/MessageFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto/32 :goto_5

    nop

    :goto_4
    return-object p0

    :goto_5
    new-instance v1, Ljava/util/HashMap;

    goto/32 :goto_c

    nop

    :goto_6
    iget-object v1, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    goto/32 :goto_2

    nop

    :goto_7
    new-instance v0, Landroid/icu/text/MessageFormat;

    goto/32 :goto_6

    nop

    :goto_8
    const-string v2, "count"

    goto/32 :goto_b

    nop

    :goto_9
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    goto/32 :goto_3

    nop

    :goto_a
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_b
    invoke-interface {v1, v2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_1

    nop

    :goto_c
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    goto/32 :goto_d

    nop

    :goto_d
    invoke-virtual {p0}, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->getEnabledAutomaticRulesCount()I

    move-result p0

    goto/32 :goto_0

    nop
.end method

.method getBlockedEffectsSummary(Landroid/app/NotificationManager$Policy;)Ljava/lang/String;
    .locals 0

    goto/32 :goto_10

    nop

    :goto_0
    return-object p0

    :goto_1
    goto/32 :goto_14

    nop

    :goto_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    goto/32 :goto_a

    nop

    :goto_3
    return-object p0

    :goto_4
    goto/32 :goto_9

    nop

    :goto_5
    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    goto/32 :goto_d

    nop

    :goto_6
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_7

    nop

    :goto_7
    return-object p0

    :goto_8
    sget p1, Lcom/android/settings/R$string;->zen_mode_restrict_notifications_summary_custom:I

    goto/32 :goto_6

    nop

    :goto_9
    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    goto/32 :goto_12

    nop

    :goto_a
    sget p1, Lcom/android/settings/R$string;->zen_mode_restrict_notifications_summary_hidden:I

    goto/32 :goto_13

    nop

    :goto_b
    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    goto/32 :goto_2

    nop

    :goto_c
    sget p1, Lcom/android/settings/R$string;->zen_mode_restrict_notifications_summary_muted:I

    goto/32 :goto_e

    nop

    :goto_d
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    goto/32 :goto_c

    nop

    :goto_e
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_0

    nop

    :goto_f
    if-nez p1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_b

    nop

    :goto_10
    iget p1, p1, Landroid/app/NotificationManager$Policy;->suppressedVisualEffects:I

    goto/32 :goto_11

    nop

    :goto_11
    if-eqz p1, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_5

    nop

    :goto_12
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    goto/32 :goto_8

    nop

    :goto_13
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_3

    nop

    :goto_14
    invoke-static {p1}, Landroid/app/NotificationManager$Policy;->areAllVisualEffectsSuppressed(I)Z

    move-result p1

    goto/32 :goto_f

    nop
.end method

.method getCallsSettingSummary(Landroid/app/NotificationManager$Policy;)Ljava/lang/String;
    .locals 5

    goto/32 :goto_16

    nop

    :goto_0
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_3

    nop

    :goto_1
    sget p1, Lcom/android/settings/R$string;->zen_mode_none_calls:I

    goto/32 :goto_12

    nop

    :goto_2
    if-eq v0, v1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_14

    nop

    :goto_3
    return-object p0

    :goto_4
    goto/32 :goto_e

    nop

    :goto_5
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    goto/32 :goto_1e

    nop

    :goto_6
    return-object p0

    :goto_7
    goto/32 :goto_18

    nop

    :goto_8
    const/4 v3, 0x2

    goto/32 :goto_a

    nop

    :goto_9
    new-array v1, v1, [Ljava/lang/Object;

    goto/32 :goto_f

    nop

    :goto_a
    new-array v3, v3, [Ljava/lang/Object;

    goto/32 :goto_1b

    nop

    :goto_b
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_15

    nop

    :goto_c
    invoke-virtual {p0, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_13

    nop

    :goto_d
    invoke-direct {p0, p1, v0, v1}, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->getEnabledCategories(Landroid/app/NotificationManager$Policy;Ljava/util/function/Predicate;Z)Ljava/util/List;

    move-result-object p1

    goto/32 :goto_5

    nop

    :goto_e
    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    goto/32 :goto_19

    nop

    :goto_f
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_10

    nop

    :goto_10
    aput-object p1, v1, v2

    goto/32 :goto_0

    nop

    :goto_11
    sget v0, Lcom/android/settings/R$string;->zen_mode_calls_summary_one:I

    goto/32 :goto_9

    nop

    :goto_12
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_6

    nop

    :goto_13
    return-object p0

    :goto_14
    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    goto/32 :goto_11

    nop

    :goto_15
    aput-object p1, v3, v1

    goto/32 :goto_c

    nop

    :goto_16
    new-instance v0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder$$ExternalSyntheticLambda1;

    goto/32 :goto_1a

    nop

    :goto_17
    aput-object v4, v3, v2

    goto/32 :goto_b

    nop

    :goto_18
    const/4 v2, 0x0

    goto/32 :goto_2

    nop

    :goto_19
    sget v0, Lcom/android/settings/R$string;->zen_mode_calls_summary_two:I

    goto/32 :goto_8

    nop

    :goto_1a
    invoke-direct {v0}, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder$$ExternalSyntheticLambda1;-><init>()V

    goto/32 :goto_1c

    nop

    :goto_1b
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_17

    nop

    :goto_1c
    const/4 v1, 0x1

    goto/32 :goto_d

    nop

    :goto_1d
    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    goto/32 :goto_1

    nop

    :goto_1e
    if-eqz v0, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_1d

    nop
.end method

.method getEnabledAutomaticRulesCount()I
    .locals 2

    goto/32 :goto_14

    nop

    :goto_0
    invoke-static {p0}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object p0

    goto/32 :goto_8

    nop

    :goto_1
    if-nez v1, :cond_0

    goto/32 :goto_11

    :cond_0
    goto/32 :goto_2

    nop

    :goto_2
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_13

    nop

    :goto_3
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_1

    nop

    :goto_4
    return v0

    :goto_5
    const/4 v0, 0x0

    goto/32 :goto_12

    nop

    :goto_6
    check-cast v1, Landroid/app/AutomaticZenRule;

    goto/32 :goto_a

    nop

    :goto_7
    invoke-virtual {v1}, Landroid/app/AutomaticZenRule;->isEnabled()Z

    move-result v1

    goto/32 :goto_e

    nop

    :goto_8
    invoke-virtual {p0}, Landroid/app/NotificationManager;->getAutomaticZenRules()Ljava/util/Map;

    move-result-object p0

    goto/32 :goto_5

    nop

    :goto_9
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_a
    if-nez v1, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_7

    nop

    :goto_b
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_c
    goto/32 :goto_3

    nop

    :goto_d
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_10

    nop

    :goto_e
    if-nez v1, :cond_2

    goto/32 :goto_c

    :cond_2
    goto/32 :goto_d

    nop

    :goto_f
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    goto/32 :goto_b

    nop

    :goto_10
    goto :goto_c

    :goto_11
    goto/32 :goto_4

    nop

    :goto_12
    if-nez p0, :cond_3

    goto/32 :goto_11

    :cond_3
    goto/32 :goto_f

    nop

    :goto_13
    check-cast v1, Ljava/util/Map$Entry;

    goto/32 :goto_9

    nop

    :goto_14
    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    goto/32 :goto_0

    nop
.end method

.method getMessagesSettingSummary(Landroid/app/NotificationManager$Policy;)Ljava/lang/String;
    .locals 5

    goto/32 :goto_19

    nop

    :goto_0
    const/4 v1, 0x1

    goto/32 :goto_6

    nop

    :goto_1
    sget v0, Lcom/android/settings/R$string;->zen_mode_calls_summary_two:I

    goto/32 :goto_d

    nop

    :goto_2
    aput-object v4, v3, v2

    goto/32 :goto_9

    nop

    :goto_3
    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    goto/32 :goto_1

    nop

    :goto_4
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_2

    nop

    :goto_5
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    goto/32 :goto_e

    nop

    :goto_6
    invoke-direct {p0, p1, v0, v1}, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->getEnabledCategories(Landroid/app/NotificationManager$Policy;Ljava/util/function/Predicate;Z)Ljava/util/List;

    move-result-object p1

    goto/32 :goto_5

    nop

    :goto_7
    aput-object p1, v3, v1

    goto/32 :goto_8

    nop

    :goto_8
    invoke-virtual {p0, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_15

    nop

    :goto_9
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_7

    nop

    :goto_a
    return-object p0

    :goto_b
    goto/32 :goto_3

    nop

    :goto_c
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    goto/32 :goto_13

    nop

    :goto_d
    const/4 v3, 0x2

    goto/32 :goto_11

    nop

    :goto_e
    if-eqz v0, :cond_0

    goto/32 :goto_17

    :cond_0
    goto/32 :goto_10

    nop

    :goto_f
    sget p1, Lcom/android/settings/R$string;->zen_mode_none_messages:I

    goto/32 :goto_14

    nop

    :goto_10
    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    goto/32 :goto_f

    nop

    :goto_11
    new-array v3, v3, [Ljava/lang/Object;

    goto/32 :goto_4

    nop

    :goto_12
    if-eq v0, v1, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_c

    nop

    :goto_13
    check-cast p0, Ljava/lang/String;

    goto/32 :goto_a

    nop

    :goto_14
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_16

    nop

    :goto_15
    return-object p0

    :goto_16
    return-object p0

    :goto_17
    goto/32 :goto_1a

    nop

    :goto_18
    invoke-direct {v0}, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder$$ExternalSyntheticLambda2;-><init>()V

    goto/32 :goto_0

    nop

    :goto_19
    new-instance v0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder$$ExternalSyntheticLambda2;

    goto/32 :goto_18

    nop

    :goto_1a
    const/4 v2, 0x0

    goto/32 :goto_12

    nop
.end method

.method getOtherSoundCategoriesSummary(Landroid/app/NotificationManager$Policy;)Ljava/lang/String;
    .locals 5

    goto/32 :goto_a

    nop

    :goto_0
    sget v3, Lcom/android/settings/R$string;->zen_mode_other_sounds_summary:I

    goto/32 :goto_21

    nop

    :goto_1
    invoke-interface {p0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_6

    nop

    :goto_2
    const/4 v3, 0x0

    goto/32 :goto_9

    nop

    :goto_3
    const-string/jumbo v4, "sound_category_2"

    goto/32 :goto_7

    nop

    :goto_4
    new-instance p0, Ljava/util/HashMap;

    goto/32 :goto_b

    nop

    :goto_5
    invoke-direct {v0}, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder$$ExternalSyntheticLambda0;-><init>()V

    goto/32 :goto_10

    nop

    :goto_6
    if-ge v0, v1, :cond_0

    goto/32 :goto_1d

    :cond_0
    goto/32 :goto_2

    nop

    :goto_7
    invoke-interface {p0, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_1a

    nop

    :goto_8
    const-string/jumbo v0, "sound_category_3"

    goto/32 :goto_1c

    nop

    :goto_9
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_d

    nop

    :goto_a
    new-instance v0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder$$ExternalSyntheticLambda0;

    goto/32 :goto_5

    nop

    :goto_b
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    goto/32 :goto_17

    nop

    :goto_c
    const-string v4, "count"

    goto/32 :goto_1

    nop

    :goto_d
    const-string/jumbo v4, "sound_category_1"

    goto/32 :goto_15

    nop

    :goto_e
    invoke-direct {v2, p0, v3}, Landroid/icu/text/MessageFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto/32 :goto_4

    nop

    :goto_f
    if-eq v0, v1, :cond_1

    goto/32 :goto_1d

    :cond_1
    goto/32 :goto_12

    nop

    :goto_10
    const/4 v1, 0x1

    goto/32 :goto_13

    nop

    :goto_11
    const/4 v3, 0x2

    goto/32 :goto_1b

    nop

    :goto_12
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_8

    nop

    :goto_13
    invoke-direct {p0, p1, v0, v1}, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->getEnabledCategories(Landroid/app/NotificationManager$Policy;Ljava/util/function/Predicate;Z)Ljava/util/List;

    move-result-object p1

    goto/32 :goto_14

    nop

    :goto_14
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    goto/32 :goto_1e

    nop

    :goto_15
    invoke-interface {p0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_11

    nop

    :goto_16
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_3

    nop

    :goto_17
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/32 :goto_c

    nop

    :goto_18
    return-object p0

    :goto_19
    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    goto/32 :goto_0

    nop

    :goto_1a
    const/4 v1, 0x3

    goto/32 :goto_f

    nop

    :goto_1b
    if-ge v0, v3, :cond_2

    goto/32 :goto_1d

    :cond_2
    goto/32 :goto_16

    nop

    :goto_1c
    invoke-interface {p0, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1d
    goto/32 :goto_20

    nop

    :goto_1e
    new-instance v2, Landroid/icu/text/MessageFormat;

    goto/32 :goto_19

    nop

    :goto_1f
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    goto/32 :goto_e

    nop

    :goto_20
    invoke-virtual {v2, p0}, Landroid/icu/text/MessageFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_18

    nop

    :goto_21
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_1f

    nop
.end method

.method getSoundSummary()Ljava/lang/String;
    .locals 4

    goto/32 :goto_14

    nop

    :goto_0
    invoke-static {v1, v2, v0, v3}, Landroid/service/notification/ZenModeConfig;->getDescription(Landroid/content/Context;ZLandroid/service/notification/ZenModeConfig;Z)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_1a

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_1d

    :cond_0
    goto/32 :goto_f

    nop

    :goto_2
    invoke-virtual {v0}, Landroid/app/NotificationManager;->getZenModeConfig()Landroid/service/notification/ZenModeConfig;

    move-result-object v0

    goto/32 :goto_10

    nop

    :goto_3
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    goto/32 :goto_22

    nop

    :goto_4
    return-object p0

    :goto_5
    goto/32 :goto_e

    nop

    :goto_6
    sget v1, Lcom/android/settings/R$string;->zen_mode_sound_summary_on_with_info:I

    goto/32 :goto_1f

    nop

    :goto_7
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_1e

    nop

    :goto_8
    invoke-static {v0}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_9
    return-object p0

    :goto_a
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_4

    nop

    :goto_b
    sget v2, Lcom/android/settings/R$string;->zen_mode_sound_summary_off:I

    goto/32 :goto_7

    nop

    :goto_c
    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_1c

    nop

    :goto_d
    invoke-interface {v1, v2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_11

    nop

    :goto_e
    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    goto/32 :goto_6

    nop

    :goto_f
    iget-object v0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    goto/32 :goto_8

    nop

    :goto_10
    iget-object v1, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    goto/32 :goto_1b

    nop

    :goto_11
    invoke-virtual {v0, v1}, Landroid/icu/text/MessageFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_9

    nop

    :goto_12
    sget v0, Lcom/android/settings/R$string;->zen_mode_sound_summary_on:I

    goto/32 :goto_a

    nop

    :goto_13
    invoke-direct {v0, v1, v2}, Landroid/icu/text/MessageFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto/32 :goto_18

    nop

    :goto_14
    iget-object v0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    goto/32 :goto_19

    nop

    :goto_15
    aput-object v0, v2, v3

    goto/32 :goto_c

    nop

    :goto_16
    const/4 v3, 0x0

    goto/32 :goto_0

    nop

    :goto_17
    invoke-virtual {v0}, Landroid/app/NotificationManager;->getZenMode()I

    move-result v0

    goto/32 :goto_1

    nop

    :goto_18
    new-instance v1, Ljava/util/HashMap;

    goto/32 :goto_3

    nop

    :goto_19
    invoke-static {v0}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object v0

    goto/32 :goto_17

    nop

    :goto_1a
    if-eqz v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_25

    nop

    :goto_1b
    const/4 v2, 0x1

    goto/32 :goto_16

    nop

    :goto_1c
    return-object p0

    :goto_1d
    goto/32 :goto_24

    nop

    :goto_1e
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    goto/32 :goto_13

    nop

    :goto_1f
    new-array v2, v2, [Ljava/lang/Object;

    goto/32 :goto_15

    nop

    :goto_20
    iget-object v1, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    goto/32 :goto_b

    nop

    :goto_21
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    goto/32 :goto_23

    nop

    :goto_22
    invoke-virtual {p0}, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->getEnabledAutomaticRulesCount()I

    move-result p0

    goto/32 :goto_21

    nop

    :goto_23
    const-string v2, "count"

    goto/32 :goto_d

    nop

    :goto_24
    new-instance v0, Landroid/icu/text/MessageFormat;

    goto/32 :goto_20

    nop

    :goto_25
    iget-object p0, p0, Lcom/android/settings/notification/zen/ZenModeSettings$SummaryBuilder;->mContext:Landroid/content/Context;

    goto/32 :goto_12

    nop
.end method
