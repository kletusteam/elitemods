.class public abstract Lcom/android/settings/notification/SilentModeRuleBaseSettings;
.super Lcom/android/settings/notification/SilentModeSettingsBase;

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceClickListener;
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/notification/SilentModeRuleBaseSettings$RuleInfo;
    }
.end annotation


# instance fields
.field protected mActivity:Landroid/app/Activity;

.field protected mBootDof:Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

.field protected mBootRepeatSummary:Ljava/lang/String;

.field protected mEditTitle:Lcom/android/settingslib/miuisettings/preference/EditTextPreference;

.field protected mEndTime:I

.field protected mEndTimePS:Lcom/android/settings/dndmode/LabelPreference;

.field protected mHint:Ljava/lang/String;

.field protected mIntentMode:I

.field protected mMode:I

.field private mOnTimeSetListener:Lmiuix/appcompat/app/TimePickerDialog$OnTimeSetListener;

.field protected mQuietWristband:Landroidx/preference/CheckBoxPreference;

.field protected mQuietWristbandCategor:Landroidx/preference/PreferenceCategory;

.field protected mRepeatTime:Lcom/android/settings/dndmode/RepeatPreference;

.field protected mRoot:Landroidx/preference/PreferenceScreen;

.field protected mRuleId:Ljava/lang/String;

.field protected mSilentMode:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

.field protected mStartTime:I

.field protected mStartTimePS:Lcom/android/settings/dndmode/LabelPreference;

.field protected mTimeFlag:Z


# direct methods
.method static bridge synthetic -$$Nest$mtimeTostring(Lcom/android/settings/notification/SilentModeRuleBaseSettings;I)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->timeTostring(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/notification/SilentModeSettingsBase;-><init>()V

    new-instance v0, Lcom/android/settings/notification/SilentModeRuleBaseSettings$1;

    invoke-direct {v0, p0}, Lcom/android/settings/notification/SilentModeRuleBaseSettings$1;-><init>(Lcom/android/settings/notification/SilentModeRuleBaseSettings;)V

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mOnTimeSetListener:Lmiuix/appcompat/app/TimePickerDialog$OnTimeSetListener;

    return-void
.end method

.method private getZenModeRules()Ljava/util/Set;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/util/Map$Entry<",
            "Ljava/lang/String;",
            "Landroid/app/AutomaticZenRule;",
            ">;>;"
        }
    .end annotation

    iget-object p0, p0, Lcom/android/settings/notification/SilentModeSettingsBase;->mContext:Landroid/content/Context;

    invoke-static {p0}, Landroid/app/NotificationManager;->from(Landroid/content/Context;)Landroid/app/NotificationManager;

    move-result-object p0

    invoke-virtual {p0}, Landroid/app/NotificationManager;->getAutomaticZenRules()Ljava/util/Map;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method

.method private minutes(I)Ljava/lang/String;
    .locals 1

    const/16 p0, 0xa

    if-ge p1, p0, :cond_0

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "0"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ""

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private restoreSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    const-string/jumbo v0, "start_time"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mStartTime:I

    const-string v0, "end_time"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mEndTime:I

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mBootDof:Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

    new-instance v1, Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

    const-string v2, "flag_bootdof"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-direct {v1, v2}, Lcom/android/settings/dndmode/Alarm$DaysOfWeek;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/android/settings/dndmode/Alarm$DaysOfWeek;->set(Lcom/android/settings/dndmode/Alarm$DaysOfWeek;)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mBootDof:Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mActivity:Landroid/app/Activity;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/settings/dndmode/Alarm$DaysOfWeek;->toString(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mBootRepeatSummary:Ljava/lang/String;

    const-string/jumbo v0, "silent_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mMode:I

    const-string v0, "key_edittitle"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mHint:Ljava/lang/String;

    return-void
.end method

.method private timeTostring(I)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    div-int/lit8 v1, p1, 0x3c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    rem-int/lit8 p1, p1, 0x3c

    invoke-direct {p0, p1}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->minutes(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method protected maybeRefreshRules(ZZ)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->getZenModeRules()Ljava/util/Set;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Refreshed mRules="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "ZenModeSettings"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->onZenModeConfigChanged()V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/android/settings/notification/SilentModeSettingsBase;->onCreate(Landroid/os/Bundle;)V

    sget v0, Lcom/android/settings/R$xml;->new_dndm_time_settings:I

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "mode"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mIntentMode:I

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "rule_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mRuleId:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string p1, "ZenModeSettings"

    const-string/jumbo v0, "rule id is null"

    invoke-static {p1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_0
    const-string/jumbo v0, "time_setting_root"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/PreferenceScreen;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mRoot:Landroidx/preference/PreferenceScreen;

    const-string/jumbo v0, "start_time"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dndmode/LabelPreference;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mStartTimePS:Lcom/android/settings/dndmode/LabelPreference;

    const-string v0, "end_time"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dndmode/LabelPreference;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mEndTimePS:Lcom/android/settings/dndmode/LabelPreference;

    const-string/jumbo v0, "repeat"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/dndmode/RepeatPreference;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mRepeatTime:Lcom/android/settings/dndmode/RepeatPreference;

    const-string/jumbo v0, "silent_mode"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mSilentMode:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    const-string v0, "key_edittitle"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/miuisettings/preference/EditTextPreference;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mEditTitle:Lcom/android/settingslib/miuisettings/preference/EditTextPreference;

    const-string v0, "key_quiet_wristband_category"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mQuietWristbandCategor:Landroidx/preference/PreferenceCategory;

    const-string v0, "key_quiet_wristband"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mQuietWristband:Landroidx/preference/CheckBoxPreference;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->setHasOptionsMenu(Z)V

    invoke-virtual {p0}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->onCreateInternal()V

    if-eqz p1, :cond_1

    invoke-direct {p0, p1}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->restoreSaveInstanceState(Landroid/os/Bundle;)V

    :cond_1
    iget-object p1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mEditTitle:Lcom/android/settingslib/miuisettings/preference/EditTextPreference;

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mHint:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroidx/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mEditTitle:Lcom/android/settingslib/miuisettings/preference/EditTextPreference;

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mHint:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mEditTitle:Lcom/android/settingslib/miuisettings/preference/EditTextPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-object p1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mStartTimePS:Lcom/android/settings/dndmode/LabelPreference;

    iget v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mStartTime:I

    invoke-direct {p0, v0}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->timeTostring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/settings/dndmode/LabelPreference;->setLabel(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mStartTimePS:Lcom/android/settings/dndmode/LabelPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    iget-object p1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mEndTimePS:Lcom/android/settings/dndmode/LabelPreference;

    iget v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mEndTime:I

    invoke-direct {p0, v0}, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->timeTostring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/settings/dndmode/LabelPreference;->setLabel(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mEndTimePS:Lcom/android/settings/dndmode/LabelPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    iget-object p1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mRepeatTime:Lcom/android/settings/dndmode/RepeatPreference;

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mBootRepeatSummary:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/android/settings/dndmode/RepeatPreference;->setLabel(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mRepeatTime:Lcom/android/settings/dndmode/RepeatPreference;

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mBootDof:Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

    invoke-virtual {p1, v0}, Lcom/android/settings/dndmode/RepeatPreference;->setDaysOfWeek(Lcom/android/settings/dndmode/Alarm$DaysOfWeek;)V

    iget-object p1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mSilentMode:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-object p1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mSilentMode:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmiuix/preference/DropDownPreference;->setValue(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mSilentMode:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    invoke-virtual {p1}, Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mRoot:Landroidx/preference/PreferenceScreen;

    iget-object p0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mQuietWristbandCategor:Landroidx/preference/PreferenceCategory;

    invoke-virtual {p1, p0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    return-void
.end method

.method protected abstract onCreateInternal()V
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mSilentMode:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, p2}, Lmiuix/preference/DropDownPreference;->setValue(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mSilentMode:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    invoke-virtual {p1}, Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mSilentMode:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    invoke-virtual {p1}, Landroidx/preference/Preference;->getOrder()I

    move-result p1

    iput p1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mMode:I

    return v1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mEditTitle:Lcom/android/settingslib/miuisettings/preference/EditTextPreference;

    if-ne p1, v0, :cond_1

    check-cast p2, Ljava/lang/String;

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mEditTitle:Lcom/android/settingslib/miuisettings/preference/EditTextPreference;

    invoke-virtual {p1, p2}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mEditTitle:Lcom/android/settingslib/miuisettings/preference/EditTextPreference;

    invoke-virtual {p1, p2}, Landroidx/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    iput-object p2, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mHint:Ljava/lang/String;

    return v1

    :cond_1
    const/4 p0, 0x0

    return p0
.end method

.method public onPreferenceClick(Landroidx/preference/Preference;)Z
    .locals 7

    new-instance v6, Lmiuix/appcompat/app/TimePickerDialog;

    iget-object v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mOnTimeSetListener:Lmiuix/appcompat/app/TimePickerDialog$OnTimeSetListener;

    iget v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mStartTime:I

    div-int/lit8 v3, v0, 0x3c

    rem-int/lit8 v4, v0, 0x3c

    const/4 v5, 0x1

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lmiuix/appcompat/app/TimePickerDialog;-><init>(Landroid/content/Context;Lmiuix/appcompat/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mStartTimePS:Lcom/android/settings/dndmode/LabelPreference;

    const/4 v1, 0x0

    if-ne p1, v0, :cond_0

    iput-boolean v1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mTimeFlag:Z

    iget p0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mStartTime:I

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mEndTimePS:Lcom/android/settings/dndmode/LabelPreference;

    if-ne p1, v0, :cond_1

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mTimeFlag:Z

    iget p0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mEndTime:I

    goto :goto_0

    :cond_1
    move p0, v1

    :goto_0
    if-lez p0, :cond_2

    goto :goto_1

    :cond_2
    move p0, v1

    :goto_1
    div-int/lit8 p1, p0, 0x3c

    rem-int/lit8 p0, p0, 0x3c

    invoke-virtual {v6, p1, p0}, Lmiuix/appcompat/app/TimePickerDialog;->updateTime(II)V

    invoke-virtual {v6}, Landroid/app/Dialog;->show()V

    return v1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    iget v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mStartTime:I

    const-string/jumbo v1, "start_time"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mEndTime:I

    const-string v1, "end_time"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mRepeatTime:Lcom/android/settings/dndmode/RepeatPreference;

    invoke-virtual {v0}, Lcom/android/settings/dndmode/RepeatPreference;->getDaysOfWeek()Lcom/android/settings/dndmode/Alarm$DaysOfWeek;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/dndmode/Alarm$DaysOfWeek;->getCoded()I

    move-result v0

    const-string v1, "flag_bootdof"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mMode:I

    const-string/jumbo v1, "silent_mode"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/android/settings/notification/SilentModeRuleBaseSettings;->mHint:Ljava/lang/String;

    const-string v1, "key_edittitle"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onZenModeChanged()V
    .locals 0

    return-void
.end method

.method protected onZenModeConfigChanged()V
    .locals 0

    return-void
.end method
