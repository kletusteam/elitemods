.class public Lcom/android/settings/faceunlock/MiuiFaceDataInput;
.super Landroid/app/Activity;


# instance fields
.field private mConfirmLockLaunched:Z

.field private mFaceEnrollToken:[B

.field private mIsKeyguardPasswordSecured:Z

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mNeedSkipConfirmPassword:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/faceunlock/MiuiFaceDataInput;->mConfirmLockLaunched:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/faceunlock/MiuiFaceDataInput;->mNeedSkipConfirmPassword:Z

    return-void
.end method

.method private isDeviceProvisioned(Landroid/content/Context;)Z
    .locals 1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string p1, "device_provisioned"

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    const/4 p1, 0x1

    if-ne p0, p1, :cond_0

    move v0, p1

    :cond_0
    return v0
.end method

.method private startFaceEnroll()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settings/faceunlock/KeyguardSettingsFaceUnlockUtils;->isSupportMultiFaceInput(Landroid/content/Context;)Z

    move-result v1

    const-string v2, "com.android.settings"

    if-eqz v1, :cond_0

    const-string v1, "com.android.settings.faceunlock.MiuiNormalCameraMultiFaceInput"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :cond_0
    const-string v1, "com.android.settings.faceunlock.MiuiNormalCameraFaceInput"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_0
    iget-object v1, p0, Lcom/android/settings/faceunlock/MiuiFaceDataInput;->mFaceEnrollToken:[B

    const-string v2, "for_face_enroll"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    const/4 v1, 0x1

    const-string v2, "for_face_enroll_from_normal"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {v0}, Lcom/android/settings/MiuiKeyguardSettingsUtils;->setCancelSettingsSplit(Landroid/content/Intent;)V

    const/16 v1, 0x65

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private startFaceOrConfirmPwd()V
    .locals 6

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Lcom/android/internal/widget/LockPatternUtils;

    invoke-direct {v1, p0}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/settings/faceunlock/MiuiFaceDataInput;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->isSecure(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/settings/faceunlock/MiuiFaceDataInput;->mIsKeyguardPasswordSecured:Z

    const-string v1, "input_facedata_need_skip_password"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/faceunlock/MiuiFaceDataInput;->mNeedSkipConfirmPassword:Z

    iget-boolean v1, p0, Lcom/android/settings/faceunlock/MiuiFaceDataInput;->mIsKeyguardPasswordSecured:Z

    const-string v3, "com.android.settings"

    const/4 v4, 0x1

    if-eqz v1, :cond_1

    iget-boolean v5, p0, Lcom/android/settings/faceunlock/MiuiFaceDataInput;->mConfirmLockLaunched:Z

    if-nez v5, :cond_1

    if-nez v0, :cond_0

    iput-boolean v4, p0, Lcom/android/settings/faceunlock/MiuiFaceDataInput;->mConfirmLockLaunched:Z

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-static {v0}, Lcom/android/settings/MiuiKeyguardSettingsUtils;->setCancelSettingsSplit(Landroid/content/Intent;)V

    const-string v1, "com.android.settings.MiuiConfirmCommonPassword"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget v1, Lcom/android/settings/R$string;->empty_title:I

    const-string v3, ":android:show_fragment_title"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v1, "support_tee_face_unlock"

    invoke-static {v1, v2}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    const-string v2, "has_challenge"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "for_face"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/faceunlock/MiuiFaceDataInput;->startFaceEnroll()V

    goto :goto_0

    :cond_1
    if-nez v1, :cond_2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-static {v0}, Lcom/android/settings/MiuiKeyguardSettingsUtils;->setCancelSettingsSplit(Landroid/content/Intent;)V

    const-class v1, Lcom/android/settings/faceunlock/MiuiFaceDataIntroduction;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0, v4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    iput-boolean v4, p0, Lcom/android/settings/faceunlock/MiuiFaceDataInput;->mConfirmLockLaunched:Z

    :cond_2
    :goto_0
    return-void
.end method

.method private startFacePrompt()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.android.settings"

    const-string v2, "com.android.settings.faceunlock.MiuiFaceDataPrompt"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v1, 0x3ea

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    const/16 v0, 0x3ee

    const/16 v1, 0x3ed

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, -0x1

    const/4 v5, 0x2

    if-eq p1, v5, :cond_7

    if-eq p1, v3, :cond_7

    if-ne p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/16 p3, 0x65

    if-ne p1, p3, :cond_5

    const/16 p3, 0x67

    if-ne p2, p3, :cond_1

    invoke-direct {p0}, Lcom/android/settings/faceunlock/MiuiFaceDataInput;->startFacePrompt()V

    goto/16 :goto_1

    :cond_1
    const/16 p3, 0x3eb

    if-ne p2, p3, :cond_3

    invoke-direct {p0, p0}, Lcom/android/settings/faceunlock/MiuiFaceDataInput;->isDeviceProvisioned(Landroid/content/Context;)Z

    move-result p3

    if-eqz p3, :cond_2

    new-instance p3, Landroid/content/Intent;

    invoke-direct {p3}, Landroid/content/Intent;-><init>()V

    invoke-static {p3}, Lcom/android/settings/MiuiKeyguardSettingsUtils;->setSettingsSplit(Landroid/content/Intent;)V

    const-string v1, "com.android.settings"

    const-string v2, "com.android.settings.faceunlock.MiuiFaceDataManage"

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "input_facedata_need_skip_password"

    invoke-virtual {p3, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, p3, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    :cond_2
    invoke-virtual {p0, v4}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_1

    :cond_3
    invoke-direct {p0, p0}, Lcom/android/settings/faceunlock/MiuiFaceDataInput;->isDeviceProvisioned(Landroid/content/Context;)Z

    move-result p3

    if-eqz p3, :cond_4

    move v4, p2

    :cond_4
    invoke-virtual {p0, v4}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_1

    :cond_5
    const/16 p3, 0x3ea

    if-ne p1, p3, :cond_c

    if-ne p2, v4, :cond_6

    iput-boolean v2, p0, Lcom/android/settings/faceunlock/MiuiFaceDataInput;->mConfirmLockLaunched:Z

    invoke-direct {p0}, Lcom/android/settings/faceunlock/MiuiFaceDataInput;->startFaceOrConfirmPwd()V

    goto :goto_1

    :cond_6
    invoke-virtual {p0, v2}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_1

    :cond_7
    :goto_0
    if-ne p2, v4, :cond_a

    invoke-static {p0}, Lcom/android/settings/MiuiKeyguardSettingsUtils;->isLargeScreen(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_9

    if-eqz p3, :cond_8

    const-string v1, "hw_auth_token"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object p3

    iput-object p3, p0, Lcom/android/settings/faceunlock/MiuiFaceDataInput;->mFaceEnrollToken:[B

    :cond_8
    invoke-direct {p0}, Lcom/android/settings/faceunlock/MiuiFaceDataInput;->startFaceEnroll()V

    goto :goto_1

    :cond_9
    invoke-direct {p0}, Lcom/android/settings/faceunlock/MiuiFaceDataInput;->startFacePrompt()V

    goto :goto_1

    :cond_a
    const/16 p3, 0x3ec

    if-ne p2, p3, :cond_b

    new-instance p3, Landroid/content/Intent;

    const-class v2, Lcom/android/settings/MiuiSecurityChooseUnlock$InternalActivity;

    invoke-direct {p3, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p3}, Lcom/android/settings/MiuiKeyguardSettingsUtils;->setCancelSettingsSplit(Landroid/content/Intent;)V

    const-string v2, "add_keyguard_password_then_add_face_recoginition"

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    sget v2, Lcom/android/settings/R$string;->empty_title:I

    const-string v3, ":android:show_fragment_title"

    invoke-virtual {p3, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, p3, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    :cond_b
    invoke-virtual {p0, v2}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_c
    :goto_1
    if-ne p1, v0, :cond_d

    invoke-virtual {p0, p2}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_d
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/android/settings/MiuiSecuritySettings;->isMiShowMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget p1, Lcom/android/settings/R$string;->mishow_disable_password_setting:I

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/android/settingslib/util/ToastUtil;->show(Landroid/content/Context;II)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_0
    invoke-static {p0}, Lcom/android/settings/MiuiKeyguardSettingsUtils;->isLargeScreen(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/faceunlock/MiuiFaceDataInput;->startFacePrompt()V

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_2

    const-string v0, "key_confirm_lock_launched"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/settings/faceunlock/MiuiFaceDataInput;->mConfirmLockLaunched:Z

    :cond_2
    invoke-direct {p0}, Lcom/android/settings/faceunlock/MiuiFaceDataInput;->startFaceOrConfirmPwd()V

    :goto_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-boolean p0, p0, Lcom/android/settings/faceunlock/MiuiFaceDataInput;->mConfirmLockLaunched:Z

    const-string v0, "key_confirm_lock_launched"

    invoke-virtual {p1, v0, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
