.class Lcom/android/settings/CutoutModeSettings$AppDialogAdapter;
.super Landroid/widget/BaseAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/CutoutModeSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AppDialogAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/CutoutModeSettings$AppDialogAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private mCheckedPosition:I

.field private mDatas:[Ljava/lang/String;

.field private mInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p2, p0, Lcom/android/settings/CutoutModeSettings$AppDialogAdapter;->mDatas:[Ljava/lang/String;

    iput p3, p0, Lcom/android/settings/CutoutModeSettings$AppDialogAdapter;->mCheckedPosition:I

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/CutoutModeSettings$AppDialogAdapter;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 0

    iget-object p0, p0, Lcom/android/settings/CutoutModeSettings$AppDialogAdapter;->mDatas:[Ljava/lang/String;

    array-length p0, p0

    return p0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/CutoutModeSettings$AppDialogAdapter;->mDatas:[Ljava/lang/String;

    aget-object p0, p0, p1

    return-object p0
.end method

.method public getItemId(I)J
    .locals 0

    int-to-long p0, p1

    return-wide p0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    if-nez p2, :cond_0

    iget-object p2, p0, Lcom/android/settings/CutoutModeSettings$AppDialogAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget p3, Lcom/android/settings/R$layout;->miuix_appcompat_select_dialog_singlechoice:I

    const/4 v0, 0x0

    invoke-virtual {p2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance p3, Lcom/android/settings/CutoutModeSettings$AppDialogAdapter$ViewHolder;

    invoke-direct {p3, p0}, Lcom/android/settings/CutoutModeSettings$AppDialogAdapter$ViewHolder;-><init>(Lcom/android/settings/CutoutModeSettings$AppDialogAdapter;)V

    const v0, 0x1020014

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p3, Lcom/android/settings/CutoutModeSettings$AppDialogAdapter$ViewHolder;->checkedView:Landroid/widget/CheckedTextView;

    invoke-virtual {p2, p3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/android/settings/CutoutModeSettings$AppDialogAdapter$ViewHolder;

    :goto_0
    iget-object v0, p0, Lcom/android/settings/CutoutModeSettings$AppDialogAdapter;->mDatas:[Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/settings/CutoutModeSettings$AppDialogAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p3, Lcom/android/settings/CutoutModeSettings$AppDialogAdapter$ViewHolder;->checkedView:Landroid/widget/CheckedTextView;

    iget-object v1, p0, Lcom/android/settings/CutoutModeSettings$AppDialogAdapter;->mDatas:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p3, p3, Lcom/android/settings/CutoutModeSettings$AppDialogAdapter$ViewHolder;->checkedView:Landroid/widget/CheckedTextView;

    iget p0, p0, Lcom/android/settings/CutoutModeSettings$AppDialogAdapter;->mCheckedPosition:I

    if-ne p1, p0, :cond_1

    const/4 p0, 0x1

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    :goto_1
    invoke-virtual {p3, p0}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    :cond_2
    return-object p2
.end method
