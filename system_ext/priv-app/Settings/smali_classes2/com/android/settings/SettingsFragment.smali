.class public Lcom/android/settings/SettingsFragment;
.super Lcom/android/settings/BasePreferenceFragment;

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/SettingsFragment$TipsListener;,
        Lcom/android/settings/SettingsFragment$SearchResultAdapter;,
        Lcom/android/settings/SettingsFragment$SearchItemViewHolder;,
        Lcom/android/settings/SettingsFragment$SearchHandler;,
        Lcom/android/settings/SettingsFragment$DeferredSetupHandler;
    }
.end annotation


# static fields
.field private static final CLOUD_SORT_WEIGHT:Ljava/lang/Double;

.field private static final PC_MODE_NOT_INTENT_RESOURCE:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final PC_MODE_NOT_SUPPORT_PKG:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final PC_MODE_NOT_SUPPORT_RESOURCE:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field isFirstEnter:Z

.field private mAnchorView:Landroid/view/View;

.field private mClickedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrSearchStatItem:Lcom/android/settings/analytics/SearchStatItem;

.field private mDeferredSetupHelper:Lcom/android/settings/DeferredSetupHelper;

.field private mGlobalSearch:Lcom/android/settings/search/SettingsGlobalSearcher;

.field private mHandler:Landroid/os/Handler;

.field private mHeaderAdapter:Lcom/android/settings/MiuiSettings$HeaderAdapter;

.field private mHintView:Landroid/view/View;

.field private volatile mIsActionModeDestroy:Z

.field private volatile mIsInActionMode:Z

.field private volatile mIsScrollEnableForListView:Z

.field private mIsSearchInited:Z

.field private mListView:Lmiuix/recyclerview/widget/RecyclerView;

.field private mMainHandler:Landroid/os/Handler;

.field private mMiuiCustSplitUtils:Lcom/android/settings/MiuiCustSplitUtils;

.field private mProxyAdapter:Lcom/android/settings/MiuiSettings$ProxyHeaderViewAdapter;

.field private mRemovableHintView:Landroid/view/View;

.field private mSearchAdapter:Lcom/android/settings/SettingsFragment$SearchResultAdapter;

.field private mSearchCallback:Lmiuix/view/SearchActionMode$Callback;

.field private mSearchExcludeMap:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSearchHandler:Lcom/android/settings/SettingsFragment$SearchHandler;

.field private mSearchHistoryFl:Lcom/android/settings/widget/FlowLayout;

.field private mSearchHistoryLists:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSearchHistoryText:Ljava/lang/String;

.field private mSearchInput:Landroid/widget/EditText;

.field private mSearchListLayout:Landroidx/core/widget/NestedScrollView;

.field private mSearchLoadingView:Landroid/view/View;

.field private volatile mSearchResult:Lcom/android/settings/search/SearchResult;

.field private mSearchResultItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settings/search/SearchResultItem;",
            ">;"
        }
    .end annotation
.end field

.field private mSearchResultLinearLayout:Landroid/widget/LinearLayout;

.field private mSearchResultListView:Lmiuix/recyclerview/widget/RecyclerView;

.field private mSearchText:Ljava/lang/String;

.field private mSearchThread:Landroid/os/HandlerThread;

.field private mSeparateAppSearchThread:Lcom/android/settings/search/appseparate/SeparateAppSearchThread;

.field private mStorageListSPUtils:Lcom/android/settings/utils/StorageListSPUtils;

.field private mTextWatcher:Landroid/text/TextWatcher;

.field private mTipsListener:Lcom/android/settings/SettingsFragment$TipsListener;

.field private volatile mTipsLocalModel:Lcom/android/settings/utils/TipsUtils$TipsLocalModel;

.field private mTrimMemoryUtils:Lcom/android/settings/TrimMemoryUtils;


# direct methods
.method public static synthetic $r8$lambda$6U7x1yv3jtjrs6WWXNpkEJHnxQA(Lcom/android/settings/SettingsFragment;Lcom/android/settings/notify/SettingsNotifyEasyModeBuilder$SettingsNotify;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/SettingsFragment;->lambda$loadRemovableHint$0(Lcom/android/settings/notify/SettingsNotifyEasyModeBuilder$SettingsNotify;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic $r8$lambda$A_iQAdqxt_lcCHKOLdcAjSb0Olg(Lcom/android/settings/SettingsFragment;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/SettingsFragment;->lambda$processSearchHistory$2(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$W30aR9RYy1ZJMrhBmmE0vf_WTYs(Lcom/android/settings/SettingsFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/SettingsFragment;->lambda$loadRemovableHint$1()V

    return-void
.end method

.method public static synthetic $r8$lambda$ZtPaUlyDl_mBRXwBHuWoQYMgPcc(Lcom/android/settings/SettingsFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/SettingsFragment;->initSearchHistoryView()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmAnchorView(Lcom/android/settings/SettingsFragment;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mAnchorView:Landroid/view/View;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmClickedList(Lcom/android/settings/SettingsFragment;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mClickedList:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCurrSearchStatItem(Lcom/android/settings/SettingsFragment;)Lcom/android/settings/analytics/SearchStatItem;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mCurrSearchStatItem:Lcom/android/settings/analytics/SearchStatItem;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmGlobalSearch(Lcom/android/settings/SettingsFragment;)Lcom/android/settings/search/SettingsGlobalSearcher;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mGlobalSearch:Lcom/android/settings/search/SettingsGlobalSearcher;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsActionModeDestroy(Lcom/android/settings/SettingsFragment;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/SettingsFragment;->mIsActionModeDestroy:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsInActionMode(Lcom/android/settings/SettingsFragment;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/SettingsFragment;->mIsInActionMode:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsScrollEnableForListView(Lcom/android/settings/SettingsFragment;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/SettingsFragment;->mIsScrollEnableForListView:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsSearchInited(Lcom/android/settings/SettingsFragment;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/SettingsFragment;->mIsSearchInited:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmListView(Lcom/android/settings/SettingsFragment;)Lmiuix/recyclerview/widget/RecyclerView;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mListView:Lmiuix/recyclerview/widget/RecyclerView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMainHandler(Lcom/android/settings/SettingsFragment;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mMainHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMiuiCustSplitUtils(Lcom/android/settings/SettingsFragment;)Lcom/android/settings/MiuiCustSplitUtils;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mMiuiCustSplitUtils:Lcom/android/settings/MiuiCustSplitUtils;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSearchAdapter(Lcom/android/settings/SettingsFragment;)Lcom/android/settings/SettingsFragment$SearchResultAdapter;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mSearchAdapter:Lcom/android/settings/SettingsFragment$SearchResultAdapter;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSearchCallback(Lcom/android/settings/SettingsFragment;)Lmiuix/view/SearchActionMode$Callback;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mSearchCallback:Lmiuix/view/SearchActionMode$Callback;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSearchExcludeMap(Lcom/android/settings/SettingsFragment;)Ljava/util/HashSet;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mSearchExcludeMap:Ljava/util/HashSet;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSearchHandler(Lcom/android/settings/SettingsFragment;)Lcom/android/settings/SettingsFragment$SearchHandler;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mSearchHandler:Lcom/android/settings/SettingsFragment$SearchHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSearchHistoryFl(Lcom/android/settings/SettingsFragment;)Lcom/android/settings/widget/FlowLayout;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryFl:Lcom/android/settings/widget/FlowLayout;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSearchHistoryLists(Lcom/android/settings/SettingsFragment;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryLists:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSearchHistoryText(Lcom/android/settings/SettingsFragment;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryText:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSearchInput(Lcom/android/settings/SettingsFragment;)Landroid/widget/EditText;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mSearchInput:Landroid/widget/EditText;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSearchListLayout(Lcom/android/settings/SettingsFragment;)Landroidx/core/widget/NestedScrollView;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mSearchListLayout:Landroidx/core/widget/NestedScrollView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSearchLoadingView(Lcom/android/settings/SettingsFragment;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mSearchLoadingView:Landroid/view/View;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSearchResult(Lcom/android/settings/SettingsFragment;)Lcom/android/settings/search/SearchResult;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mSearchResult:Lcom/android/settings/search/SearchResult;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSearchResultItems(Lcom/android/settings/SettingsFragment;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mSearchResultItems:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSearchResultLinearLayout(Lcom/android/settings/SettingsFragment;)Landroid/widget/LinearLayout;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mSearchResultLinearLayout:Landroid/widget/LinearLayout;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSearchResultListView(Lcom/android/settings/SettingsFragment;)Lmiuix/recyclerview/widget/RecyclerView;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mSearchResultListView:Lmiuix/recyclerview/widget/RecyclerView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSearchText(Lcom/android/settings/SettingsFragment;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mSearchText:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSeparateAppSearchThread(Lcom/android/settings/SettingsFragment;)Lcom/android/settings/search/appseparate/SeparateAppSearchThread;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mSeparateAppSearchThread:Lcom/android/settings/search/appseparate/SeparateAppSearchThread;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmStorageListSPUtils(Lcom/android/settings/SettingsFragment;)Lcom/android/settings/utils/StorageListSPUtils;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mStorageListSPUtils:Lcom/android/settings/utils/StorageListSPUtils;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTextWatcher(Lcom/android/settings/SettingsFragment;)Landroid/text/TextWatcher;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mTextWatcher:Landroid/text/TextWatcher;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmCurrSearchStatItem(Lcom/android/settings/SettingsFragment;Lcom/android/settings/analytics/SearchStatItem;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/SettingsFragment;->mCurrSearchStatItem:Lcom/android/settings/analytics/SearchStatItem;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsActionModeDestroy(Lcom/android/settings/SettingsFragment;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/SettingsFragment;->mIsActionModeDestroy:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsInActionMode(Lcom/android/settings/SettingsFragment;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/SettingsFragment;->mIsInActionMode:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsScrollEnableForListView(Lcom/android/settings/SettingsFragment;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/SettingsFragment;->mIsScrollEnableForListView:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsSearchInited(Lcom/android/settings/SettingsFragment;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/SettingsFragment;->mIsSearchInited:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmSearchHistoryText(Lcom/android/settings/SettingsFragment;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryText:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmSearchInput(Lcom/android/settings/SettingsFragment;Landroid/widget/EditText;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/SettingsFragment;->mSearchInput:Landroid/widget/EditText;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmSearchResultItems(Lcom/android/settings/SettingsFragment;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/SettingsFragment;->mSearchResultItems:Ljava/util/List;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmSearchText(Lcom/android/settings/SettingsFragment;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/SettingsFragment;->mSearchText:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$mensureSearchHandler(Lcom/android/settings/SettingsFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/SettingsFragment;->ensureSearchHandler()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetFinalResult(Lcom/android/settings/SettingsFragment;Ljava/util/List;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/SettingsFragment;->getFinalResult(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetNonEmptySearchResultCount(Lcom/android/settings/SettingsFragment;Ljava/util/List;)I
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/SettingsFragment;->getNonEmptySearchResultCount(Ljava/util/List;)I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mhideSoftKeyboard(Lcom/android/settings/SettingsFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/SettingsFragment;->hideSoftKeyboard()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mhighlight(Lcom/android/settings/SettingsFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settings/SettingsFragment;->highlight(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mprocessSearchHistory(Lcom/android/settings/SettingsFragment;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/SettingsFragment;->processSearchHistory(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mremoveHintView(Lcom/android/settings/SettingsFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/SettingsFragment;->removeHintView()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msearchItemClickStat(Lcom/android/settings/SettingsFragment;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/SettingsFragment;->searchItemClickStat(ILjava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetSearchHistoryVisiable(Lcom/android/settings/SettingsFragment;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/SettingsFragment;->setSearchHistoryVisiable(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetSearchMaskVisiable(Lcom/android/settings/SettingsFragment;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/SettingsFragment;->setSearchMaskVisiable(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetSearchResultItemViewJump(Lcom/android/settings/SettingsFragment;Landroid/view/View;Lcom/android/settings/search/SearchResultItem;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/SettingsFragment;->setSearchResultItemViewJump(Landroid/view/View;Lcom/android/settings/search/SearchResultItem;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msortSearchItemByCloudData(Lcom/android/settings/SettingsFragment;Ljava/util/List;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/SettingsFragment;->sortSearchItemByCloudData(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mstartSubIntentIfNeeded(Lcom/android/settings/SettingsFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/SettingsFragment;->startSubIntentIfNeeded()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateHintView(Lcom/android/settings/SettingsFragment;Landroid/service/settings/suggestions/Suggestion;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/SettingsFragment;->updateHintView(Landroid/service/settings/suggestions/Suggestion;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateSearch(Lcom/android/settings/SettingsFragment;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/SettingsFragment;->updateSearch(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateTips(Lcom/android/settings/SettingsFragment;ZLjava/lang/String;IIILandroid/view/View$OnClickListener;)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/android/settings/SettingsFragment;->updateTips(ZLjava/lang/String;IIILandroid/view/View$OnClickListener;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$smgetLanguage()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/android/settings/SettingsFragment;->getLanguage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 4

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/android/settings/SettingsFragment;->CLOUD_SORT_WEIGHT:Ljava/lang/Double;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/settings/SettingsFragment;->PC_MODE_NOT_SUPPORT_PKG:Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/android/settings/SettingsFragment;->PC_MODE_NOT_SUPPORT_RESOURCE:Ljava/util/List;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/android/settings/SettingsFragment;->PC_MODE_NOT_INTENT_RESOURCE:Ljava/util/List;

    const-string v3, "com.miui.securitycenter"

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v3, "com.miui.contentextension"

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v3, "com.miui.accessibility"

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v3, "com.miui.voiceassist"

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "accessibility_settings"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "com.android.settings/com.android.settings.accessibility.accessibilitymenu.AccessibilityMenuService"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "accessibility_setting_item_control_timeout_title"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string/jumbo v0, "usage_state_app_timer"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string/jumbo v0, "privacy_protection"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "accessibility_settings_tabs_visual"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "accessibility_settings_tabs_hearing"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "accessibility_settings_tabs_general"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "accessibility_settings_tabs_physical"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "accessibility_button_title"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "accessibility_button_description"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "com.google.android.marvin.talkback/com.google.android.marvin.talkback.TalkBackService"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string/jumbo v0, "voice_assist"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "game_booster_title"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string/jumbo v0, "video_tool_box_title"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "application_lock_name"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string/jumbo v0, "touch_assistant"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "freeform_guide_settings"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "home_title"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "launcher_wallpaper_title"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string/jumbo v0, "second_space"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string/jumbo v0, "personalize_title"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "launcher_icon_management"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string/jumbo v0, "screen_dark_mode_time_title"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "dark_mode_day_night_mode_title"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "dark_mode_auto_time_title"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "dark_mode_time_settings"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "light_color_mode"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "dark_color_mode"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string/jumbo v0, "more_dark_mode_settings"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string/jumbo v0, "miui.action.usagestas.MAIN"

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string/jumbo v0, "miui.intent.action.TOUCH_ASSISTANT_SETTINGS"

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string/jumbo v0, "miui.intent.action.PRIVACY_SETTINGS"

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/BasePreferenceFragment;-><init>()V

    new-instance v0, Lcom/android/settings/SettingsFragment$DeferredSetupHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/settings/SettingsFragment$DeferredSetupHandler;-><init>(Lcom/android/settings/SettingsFragment;Lcom/android/settings/SettingsFragment$DeferredSetupHandler-IA;)V

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->mHandler:Landroid/os/Handler;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->mClickedList:Ljava/util/List;

    iput-object v1, p0, Lcom/android/settings/SettingsFragment;->mSearchExcludeMap:Ljava/util/HashSet;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/SettingsFragment;->isFirstEnter:Z

    iput-boolean v0, p0, Lcom/android/settings/SettingsFragment;->mIsScrollEnableForListView:Z

    new-instance v0, Lcom/android/settings/SettingsFragment$8;

    invoke-direct {v0, p0}, Lcom/android/settings/SettingsFragment$8;-><init>(Lcom/android/settings/SettingsFragment;)V

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->mSearchCallback:Lmiuix/view/SearchActionMode$Callback;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->mMainHandler:Landroid/os/Handler;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/SettingsFragment;->mIsSearchInited:Z

    new-instance v0, Lcom/android/settings/SettingsFragment$9;

    invoke-direct {v0, p0}, Lcom/android/settings/SettingsFragment$9;-><init>(Lcom/android/settings/SettingsFragment;)V

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->mTextWatcher:Landroid/text/TextWatcher;

    return-void
.end method

.method private adapterAccessibility(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/utils/SettingsFeatures;->isSplitTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/SettingsFragment;->isFirstEnter:Z

    if-nez p1, :cond_1

    return-void

    :cond_1
    new-instance v0, Lcom/android/settings/SettingsFragment$7;

    invoke-direct {v0, p0}, Lcom/android/settings/SettingsFragment$7;-><init>(Lcom/android/settings/SettingsFragment;)V

    invoke-static {p1, v0}, Landroidx/core/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroidx/core/view/AccessibilityDelegateCompat;)V

    return-void
.end method

.method private addHintView()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mHintView:Landroid/view/View;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mListView:Lmiuix/recyclerview/widget/RecyclerView;

    const-string v1, "deferred_setup_hint"

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v2

    if-nez v2, :cond_1

    return-void

    :cond_1
    if-nez v0, :cond_2

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v2, Lcom/android/settings/R$layout;->deferred_setup_hint:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/android/settings/SettingsFragment;->mProxyAdapter:Lcom/android/settings/MiuiSettings$ProxyHeaderViewAdapter;

    if-eqz v1, :cond_2

    invoke-virtual {v1, v0}, Lcom/android/settings/MiuiSettings$ProxyHeaderViewAdapter;->addDeferedSetupView(Landroid/view/View;)V

    :cond_2
    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->mHintView:Landroid/view/View;

    sget p0, Lcom/android/settings/R$id;->deferred_setup_title:I

    invoke-virtual {v0, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/widget/TextView;

    sget v0, Lcom/android/settings/R$string;->deferred_setup_hintViewTitle:I

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method private ensureSearchHandler()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mSearchThread:Landroid/os/HandlerThread;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "SettingsFragment-Search"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->mSearchThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Lcom/android/settings/SettingsFragment$SearchHandler;

    iget-object v1, p0, Lcom/android/settings/SettingsFragment;->mSearchThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/settings/SettingsFragment$SearchHandler;-><init>(Lcom/android/settings/SettingsFragment;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->mSearchHandler:Lcom/android/settings/SettingsFragment$SearchHandler;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mSeparateAppSearchThread:Lcom/android/settings/search/appseparate/SeparateAppSearchThread;

    if-nez v0, :cond_1

    new-instance v0, Lcom/android/settings/search/appseparate/SeparateAppSearchThread;

    const-string v1, "SettingsFragment-SeparateAppSearch"

    invoke-direct {v0, v1, p0}, Lcom/android/settings/search/appseparate/SeparateAppSearchThread;-><init>(Ljava/lang/String;Lcom/android/settings/SettingsFragment;)V

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->mSeparateAppSearchThread:Lcom/android/settings/search/appseparate/SeparateAppSearchThread;

    :cond_1
    return-void
.end method

.method private getFinalResult(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/search/SearchResultItem;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/android/settings/search/SearchResultItem;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    invoke-static {p0}, Lcom/android/settings/utils/SettingsFeatures;->isOnPcMode(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_4

    const/4 p0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p0, v1, :cond_3

    invoke-interface {p1, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/search/SearchResultItem;

    iget-object v1, v1, Lcom/android/settings/search/SearchResultItem;->intent:Landroid/content/Intent;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    :cond_0
    invoke-interface {p1, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/search/SearchResultItem;

    iget-object v1, v1, Lcom/android/settings/search/SearchResultItem;->resource:Ljava/lang/String;

    sget-object v4, Lcom/android/settings/SettingsFragment;->PC_MODE_NOT_SUPPORT_PKG:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/android/settings/SettingsFragment;->PC_MODE_NOT_SUPPORT_RESOURCE:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/android/settings/SettingsFragment;->PC_MODE_NOT_INTENT_RESOURCE:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    invoke-interface {p1, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/search/SearchResultItem;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_1
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    :cond_3
    return-object v0

    :cond_4
    return-object p1
.end method

.method private static getLanguage()Ljava/lang/String;
    .locals 1

    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    :try_start_0
    invoke-interface {v0}, Landroid/app/IActivityManager;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private getMiuiCustSplitUtils()Lcom/android/settings/MiuiCustSplitUtils;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mMiuiCustSplitUtils:Lcom/android/settings/MiuiCustSplitUtils;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/MiuiCustSplitUtilsImpl;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/MiuiCustSplitUtilsImpl;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->mMiuiCustSplitUtils:Lcom/android/settings/MiuiCustSplitUtils;

    :cond_0
    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mMiuiCustSplitUtils:Lcom/android/settings/MiuiCustSplitUtils;

    return-object p0
.end method

.method private getNonEmptySearchResultCount(Ljava/util/List;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/search/SearchResultItem;",
            ">;)I"
        }
    .end annotation

    const/4 p0, 0x0

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p1, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/search/SearchResultItem;

    iget v0, v0, Lcom/android/settings/search/SearchResultItem;->type:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p0

    :cond_1
    :goto_0
    return p0
.end method

.method private hideSoftKeyboard()V
    .locals 2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mSearchResultListView:Lmiuix/recyclerview/widget/RecyclerView;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object p0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    return-void
.end method

.method private highlight(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_3

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_1

    :cond_0
    new-instance p0, Landroid/text/SpannableStringBuilder;

    invoke-direct {p0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const/16 p3, 0x12

    invoke-static {p2, p3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object p2

    invoke-virtual {p2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p1

    :goto_0
    invoke-virtual {p1}, Ljava/util/regex/Matcher;->find()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-virtual {p1}, Ljava/util/regex/Matcher;->end()I

    move-result p2

    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p3

    if-le p2, p3, :cond_1

    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p2

    :cond_1
    new-instance p3, Landroid/text/style/ForegroundColorSpan;

    const/high16 v0, -0x10000

    invoke-direct {p3, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Ljava/util/regex/Matcher;->start()I

    move-result v0

    const/16 v1, 0x21

    invoke-virtual {p0, p3, v0, p2, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_2
    return-object p0

    :cond_3
    :goto_1
    const/4 p0, 0x0

    return-object p0
.end method

.method private initGlobalSearchIfNeed()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mGlobalSearch:Lcom/android/settings/search/SettingsGlobalSearcher;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/settings/search/SettingsGlobalSearcher;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/search/SettingsGlobalSearcher;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->mGlobalSearch:Lcom/android/settings/search/SettingsGlobalSearcher;

    invoke-virtual {v0}, Lcom/android/settings/search/SettingsGlobalSearcher;->requestGlobalSearchUpdate()V

    :cond_0
    return-void
.end method

.method private initSearchHistoryView()V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryFl:Lcom/android/settings/widget/FlowLayout;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/SettingsFragment;->mStorageListSPUtils:Lcom/android/settings/utils/StorageListSPUtils;

    const-string/jumbo v2, "tagSearchHistory"

    invoke-virtual {v1, v2}, Lcom/android/settings/utils/StorageListSPUtils;->loadDataList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryLists:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryLists:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_0

    sget v2, Lcom/android/settings/R$layout;->search_history_tv:I

    iget-object v3, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryFl:Lcom/android/settings/widget/FlowLayout;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryLists:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v3, Lcom/android/settings/SettingsFragment$2;

    invoke-direct {v3, p0, v2}, Lcom/android/settings/SettingsFragment$2;-><init>(Lcom/android/settings/SettingsFragment;Landroid/widget/TextView;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryFl:Lcom/android/settings/widget/FlowLayout;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private isStartUpdaterResource(Lcom/android/settings/search/SearchResultItem;)Z
    .locals 1

    iget-object p0, p1, Lcom/android/settings/search/SearchResultItem;->resource:Ljava/lang/String;

    const-string/jumbo v0, "miui_updater"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1

    iget-object p0, p1, Lcom/android/settings/search/SearchResultItem;->resource:Ljava/lang/String;

    const-string p1, "device_miui_version"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private synthetic lambda$loadRemovableHint$0(Lcom/android/settings/notify/SettingsNotifyEasyModeBuilder$SettingsNotify;Landroid/view/View;)V
    .locals 0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {p1, p0}, Lcom/android/settings/notify/SettingsNotifyEasyModeBuilder$SettingsNotify;->goToTarget(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method

.method private synthetic lambda$loadRemovableHint$1()V
    .locals 1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/utils/TipsUtils;->query(Landroid/content/Context;)Lcom/android/settings/utils/TipsUtils$TipsLocalModel;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/SettingsFragment;->setTipsLocalModel(Lcom/android/settings/utils/TipsUtils$TipsLocalModel;)V

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mTipsListener:Lcom/android/settings/SettingsFragment$TipsListener;

    if-eqz p0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method private synthetic lambda$processSearchHistory$2(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mStorageListSPUtils:Lcom/android/settings/utils/StorageListSPUtils;

    const-string/jumbo v1, "tagSearchHistory"

    invoke-virtual {v0, v1}, Lcom/android/settings/utils/StorageListSPUtils;->loadDataList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryLists:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    iget-object v2, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryLists:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryLists:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryLists:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v3, 0xf

    if-lt v0, v3, :cond_1

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryLists:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryLists:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v0, v2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryLists:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    const/4 v0, -0x1

    :goto_0
    iget-object v3, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryLists:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_4

    iget-object v3, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryLists:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v0, v2

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryLists:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryLists:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v0, v2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :goto_1
    iget-object p1, p0, Lcom/android/settings/SettingsFragment;->mStorageListSPUtils:Lcom/android/settings/utils/StorageListSPUtils;

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryLists:Ljava/util/List;

    invoke-virtual {p1, v1, v0}, Lcom/android/settings/utils/StorageListSPUtils;->saveDataList(Ljava/lang/String;Ljava/util/List;)V

    iget-object p1, p0, Lcom/android/settings/SettingsFragment;->mMainHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/settings/SettingsFragment$$ExternalSyntheticLambda3;

    invoke-direct {v0, p0}, Lcom/android/settings/SettingsFragment$$ExternalSyntheticLambda3;-><init>(Lcom/android/settings/SettingsFragment;)V

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private logDetailSearchTime()V
    .locals 4

    sget-object p0, Lcom/android/settingslib/search/IndexTree;->SETTINGS_TREES_COST_TIME:Ljava/util/Map;

    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "className:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " takes "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "ms"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SettingsFragment"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    sget-object p0, Lcom/android/settingslib/search/IndexTree;->SETTINGS_TREES_COST_TIME:Ljava/util/Map;

    invoke-interface {p0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method private normalizeScore(Ljava/lang/Double;)Ljava/lang/Double;
    .locals 4

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpg-double p0, v0, v2

    if-gez p0, :cond_0

    const-wide/16 p0, 0x0

    invoke-static {p0, p1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide p0

    sub-double/2addr p0, v2

    const-wide/high16 v0, 0x4008000000000000L    # 3.0

    div-double/2addr p0, v0

    invoke-static {p0, p1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p0

    return-object p0
.end method

.method private processSearchHistory(Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/android/settings/SettingsFragment$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0, p1}, Lcom/android/settings/SettingsFragment$$ExternalSyntheticLambda2;-><init>(Lcom/android/settings/SettingsFragment;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/android/settingslib/utils/ThreadUtils;->postOnBackgroundThread(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method private releaseSettingsTree()V
    .locals 5

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/SettingsFragment;->mIsSearchInited:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, ""

    invoke-static {v1}, Lcom/android/settings/search/provider/SettingsProvider;->getSearchUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const-string/jumbo v4, "release"

    invoke-virtual {v0, v2, v4, v1, v3}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/SettingsFragment;->mIsSearchInited:Z

    :cond_0
    return-void
.end method

.method private removeHintView()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mListView:Lmiuix/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v1, "deferred_setup_hint"

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mProxyAdapter:Lcom/android/settings/MiuiSettings$ProxyHeaderViewAdapter;

    invoke-virtual {p0, v0}, Lcom/android/settings/MiuiSettings$ProxyHeaderViewAdapter;->removeDeferedSetupView(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method private searchItemClickStat(ILjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mCurrSearchStatItem:Lcom/android/settings/analytics/SearchStatItem;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/android/settings/analytics/SearchStatItem;->setClickedItemOrder(I)V

    iget-object p1, p0, Lcom/android/settings/SettingsFragment;->mCurrSearchStatItem:Lcom/android/settings/analytics/SearchStatItem;

    invoke-virtual {p1, p2}, Lcom/android/settings/analytics/SearchStatItem;->setClickedResource(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/settings/SettingsFragment;->mCurrSearchStatItem:Lcom/android/settings/analytics/SearchStatItem;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/android/settings/analytics/SearchStatItem;->traceSearchEvent(Z)V

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mCurrSearchStatItem:Lcom/android/settings/analytics/SearchStatItem;

    invoke-virtual {p0, p2}, Lcom/android/settings/analytics/SearchStatItem;->setIsAlreadyStat(Z)V

    :cond_0
    return-void
.end method

.method private setSearchHistoryVisiable(Z)V
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mSearchListLayout:Landroidx/core/widget/NestedScrollView;

    if-nez p0, :cond_0

    const-string p0, "SettingsFragment"

    const-string/jumbo p1, "setSearchHistoryVisiable: mSearchListLayout is null"

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    if-eqz p1, :cond_1

    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    return-void
.end method

.method private setSearchMaskVisiable(Z)V
    .locals 1

    :try_start_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p0

    sget v0, Lcom/android/settings/R$id;->search_mask:I

    invoke-virtual {p0, v0}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object p0

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    const-string p1, "SettingsFragment"

    const-string/jumbo v0, "setSearchMaskVisiable: "

    invoke-static {p1, v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_1
    return-void
.end method

.method private setSearchResultItemViewJump(Landroid/view/View;Lcom/android/settings/search/SearchResultItem;)V
    .locals 3

    iget-object p1, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryText:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/settings/SettingsFragment;->processSearchHistory(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/settings/SettingsFragment;->mCurrSearchStatItem:Lcom/android/settings/analytics/SearchStatItem;

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/settings/analytics/SearchStatItem;->getKeyWork()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mCurrSearchStatItem:Lcom/android/settings/analytics/SearchStatItem;

    invoke-virtual {v0}, Lcom/android/settings/analytics/SearchStatItem;->getKeyWork()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SearchKeyWord"

    invoke-virtual {p1, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v0, "setting_search_hotword"

    invoke-static {v0, p1}, Lcom/android/settings/report/InternationalCompat;->trackReportEvent(Ljava/lang/String;Ljava/util/Map;)V

    :cond_1
    const-string/jumbo p1, "setting_search_done"

    invoke-static {p1}, Lcom/android/settings/report/InternationalCompat;->trackReportEvent(Ljava/lang/String;)V

    iget-object p1, p2, Lcom/android/settings/search/SearchResultItem;->resource:Ljava/lang/String;

    if-eqz p1, :cond_2

    invoke-direct {p0, p2}, Lcom/android/settings/SettingsFragment;->isStartUpdaterResource(Lcom/android/settings/search/SearchResultItem;)Z

    move-result p1

    if-eqz p1, :cond_2

    iget p1, p2, Lcom/android/settings/search/SearchResultItem;->status:I

    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/android/settings/device/MiuiAboutPhoneUtils;->startUpdater(Landroid/app/Activity;)V

    goto :goto_0

    :cond_2
    iget-object p1, p2, Lcom/android/settings/search/SearchResultItem;->resource:Ljava/lang/String;

    const/4 v0, 0x4

    if-eqz p1, :cond_4

    const-string/jumbo v1, "virtual_keyboards_for_work_title"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object p1

    invoke-static {p1}, Lcom/android/settings/Utils;->getManagedProfile(Landroid/os/UserManager;)Landroid/os/UserHandle;

    move-result-object p1

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->isFoldDevice()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p2, Lcom/android/settings/search/SearchResultItem;->intent:Landroid/content/Intent;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addMiuiFlags(I)Landroid/content/Intent;

    :cond_3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object p2, p2, Lcom/android/settings/search/SearchResultItem;->intent:Landroid/content/Intent;

    invoke-virtual {v0, p2, p1}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto :goto_0

    :cond_4
    iget-object p1, p2, Lcom/android/settings/search/SearchResultItem;->intent:Landroid/content/Intent;

    if-eqz p1, :cond_6

    invoke-direct {p0, p2}, Lcom/android/settings/SettingsFragment;->startWifiP2pOrNot(Lcom/android/settings/search/SearchResultItem;)Z

    move-result p1

    if-eqz p1, :cond_6

    invoke-static {}, Lcom/android/settings/MiuiUtils;->getInstance()Lcom/android/settings/MiuiUtils;

    move-result-object p1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p2, Lcom/android/settings/search/SearchResultItem;->intent:Landroid/content/Intent;

    invoke-virtual {p1, v1, v2}, Lcom/android/settings/MiuiUtils;->canFindActivity(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result p1

    if-eqz p1, :cond_6

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->isFoldDevice()Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p2, Lcom/android/settings/search/SearchResultItem;->intent:Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addMiuiFlags(I)Landroid/content/Intent;

    :cond_5
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p2, Lcom/android/settings/search/SearchResultItem;->intent:Landroid/content/Intent;

    invoke-virtual {p1, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_6
    :goto_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    check-cast p0, Lcom/android/settings/MiuiSettings;

    if-eqz p0, :cond_7

    invoke-virtual {p0}, Lcom/android/settings/MiuiSettings;->disableSelectedPosition()V

    :cond_7
    return-void
.end method

.method private sortByGroup(Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/search/SearchResultItem;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/android/settings/search/SearchResultItem;",
            ">;"
        }
    .end annotation

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    const/4 v6, 0x1

    if-eqz v5, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/settings/search/SearchResultItem;

    iget-object v7, v5, Lcom/android/settings/search/SearchResultItem;->group:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v4, v5, Lcom/android/settings/search/SearchResultItem;->group:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/search/SearchResultGroupEntity;

    invoke-virtual {v4, v5}, Lcom/android/settings/search/SearchResultGroupEntity;->addResultItem(Lcom/android/settings/search/SearchResultItem;)Lcom/android/settings/search/SearchResultGroupEntity;

    move v4, v6

    goto :goto_0

    :cond_0
    iget-object v6, v5, Lcom/android/settings/search/SearchResultItem;->group:Ljava/lang/String;

    new-instance v7, Lcom/android/settings/search/SearchResultGroupEntity;

    invoke-direct {v7, v6}, Lcom/android/settings/search/SearchResultGroupEntity;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Lcom/android/settings/search/SearchResultGroupEntity;->addResultItem(Lcom/android/settings/search/SearchResultItem;)Lcom/android/settings/search/SearchResultGroupEntity;

    move-result-object v5

    invoke-virtual {v2, v6, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object p1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance p1, Lcom/android/settings/SettingsFragment$10;

    invoke-direct {p1, p0}, Lcom/android/settings/SettingsFragment$10;-><init>(Lcom/android/settings/SettingsFragment;)V

    invoke-static {v2, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    move p1, v3

    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge p1, v5, :cond_3

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/settings/search/SearchResultGroupEntity;

    invoke-virtual {v5}, Lcom/android/settings/search/SearchResultGroupEntity;->getGroupResultItems()Ljava/util/ArrayList;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/settings/search/SearchResultItem;

    iput-boolean v6, v7, Lcom/android/settings/search/SearchResultItem;->header:Z

    invoke-virtual {p0, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    if-eqz v4, :cond_2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    sub-int/2addr v5, v6

    if-eq p1, v5, :cond_2

    sget-object v5, Lcom/android/settings/search/SearchResultItem;->CATEGORY:Lcom/android/settings/search/SearchResultItem;

    invoke-virtual {p0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    :cond_3
    sget-object p1, Lcom/android/settings/search/SearchResultItem;->FOOTER:Lcom/android/settings/search/SearchResultItem;

    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "sortByGroup cost: "

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    invoke-virtual {p1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, " ms"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "SettingsFragment"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object p0
.end method

.method private sortSearchItemByCloudData(Ljava/util/List;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/search/SearchResultItem;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/android/settings/search/SearchResultItem;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/settings/search/cloud/SearchCloudSortUtils;->getInstance(Landroid/content/Context;)Lcom/android/settings/search/cloud/SearchCloudSortUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/search/cloud/SearchCloudSortUtils;->getCloudWeight()Ljava/lang/Double;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/search/SearchResultItem;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/android/settings/search/cloud/SearchCloudSortUtils;->getInstance(Landroid/content/Context;)Lcom/android/settings/search/cloud/SearchCloudSortUtils;

    move-result-object v3

    iget-object v4, v2, Lcom/android/settings/search/SearchResultItem;->resource:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/android/settings/search/cloud/SearchCloudSortUtils;->get(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v3

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    if-nez v3, :cond_0

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    :cond_0
    if-nez v0, :cond_1

    sget-object v0, Lcom/android/settings/SettingsFragment;->CLOUD_SORT_WEIGHT:Ljava/lang/Double;

    :cond_1
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-direct {p0, v3}, Lcom/android/settings/SettingsFragment;->normalizeScore(Ljava/lang/Double;)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    mul-double/2addr v6, v8

    iget-wide v8, v2, Lcom/android/settings/search/SearchResultItem;->score:D

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    sub-double/2addr v4, v10

    mul-double/2addr v8, v4

    add-double/2addr v6, v8

    iput-wide v6, v2, Lcom/android/settings/search/SearchResultItem;->score:D
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    const-string v5, "SettingsFragment"

    invoke-static {v5, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    :goto_1
    invoke-interface {p1, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/android/settings/SettingsFragment;->sortByGroup(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private startSubIntentIfNeeded()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mMiuiCustSplitUtils:Lcom/android/settings/MiuiCustSplitUtils;

    invoke-virtual {v0}, Lcom/android/settings/MiuiCustSplitUtils;->reachSplitSize()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mMiuiCustSplitUtils:Lcom/android/settings/MiuiCustSplitUtils;

    invoke-virtual {v0}, Lcom/android/settings/MiuiCustSplitUtils;->getCurrentSubIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->hasWindowFocus()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, ":settings:fragment_args_key"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method private startWifiP2pOrNot(Lcom/android/settings/search/SearchResultItem;)Z
    .locals 2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/settings/R$string;->wifi_menu_p2p:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_4

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p1, Lcom/android/settings/search/SearchResultItem;->title:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    return v1

    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    const/4 p1, 0x0

    if-nez p0, :cond_2

    return p1

    :cond_2
    const-string/jumbo v0, "wifi"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiApEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Lcom/android/settings/R$string;->wifi_direct_close_hotspot_hint:I

    invoke-static {p0, v0, v1}, Lcom/android/settingslib/util/ToastUtil;->show(Landroid/content/Context;II)V

    return p1

    :cond_3
    invoke-static {p0}, Lcom/android/settingslib/wifi/SlaveWifiUtils;->getInstance(Landroid/content/Context;)Lcom/android/settingslib/wifi/SlaveWifiUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settingslib/wifi/SlaveWifiUtils;->isSlaveWifiEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    sget v0, Lcom/android/settings/R$string;->wifi_direct_close_slave_wifi_hint:I

    invoke-static {p0, v0, v1}, Lcom/android/settingslib/util/ToastUtil;->show(Landroid/content/Context;II)V

    return p1

    :cond_4
    :goto_0
    return v1
.end method

.method private updateHintView(Landroid/service/settings/suggestions/Suggestion;)V
    .locals 6

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mHintView:Landroid/view/View;

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/SettingsFragment;->addHintView()V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mHintView:Landroid/view/View;

    if-nez v0, :cond_2

    const-string p0, "SettingsFragment"

    const-string/jumbo p1, "updateHintView: HintView is null"

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_2
    sget v1, Lcom/android/settings/R$id;->deferred_setup_title:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/service/settings/suggestions/Suggestion;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mHintView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->hint_layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    new-instance v1, Lcom/android/settings/SettingsFragment$3;

    invoke-direct {v1, p0, p1}, Lcom/android/settings/SettingsFragment$3;-><init>(Lcom/android/settings/SettingsFragment;Landroid/service/settings/suggestions/Suggestion;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 p1, 0x1

    new-array v1, p1, [Landroid/view/View;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-static {v1}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/IFolme;->touch()Lmiuix/animation/ITouchStyle;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    new-array p1, p1, [Lmiuix/animation/ITouchStyle$TouchType;

    sget-object v5, Lmiuix/animation/ITouchStyle$TouchType;->DOWN:Lmiuix/animation/ITouchStyle$TouchType;

    aput-object v5, p1, v2

    invoke-interface {v3, v4, p1}, Lmiuix/animation/ITouchStyle;->setScale(F[Lmiuix/animation/ITouchStyle$TouchType;)Lmiuix/animation/ITouchStyle;

    new-instance p1, Lcom/android/settings/SettingsFragment$4;

    invoke-direct {p1, p0, v1}, Lcom/android/settings/SettingsFragment$4;-><init>(Lcom/android/settings/SettingsFragment;Lmiuix/animation/IFolme;)V

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method private updateSearch(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mSearchText:Ljava/lang/String;

    invoke-static {v0, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/SettingsFragment;->ensureSearchHandler()V

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mSearchHandler:Lcom/android/settings/SettingsFragment$SearchHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mSearchHandler:Lcom/android/settings/SettingsFragment$SearchHandler;

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mSearchText:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mSearchLoadingView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iput-object p1, p0, Lcom/android/settings/SettingsFragment;->mSearchText:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method private updateTips(ZLjava/lang/String;IIILandroid/view/View$OnClickListener;)V
    .locals 3

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    const-string p0, "SettingsFragment"

    const-string/jumbo p1, "updateTips: Fragment SettingsFragment not attached to a context"

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mProxyAdapter:Lcom/android/settings/MiuiSettings$ProxyHeaderViewAdapter;

    invoke-virtual {v0}, Lcom/android/settings/MiuiSettings$ProxyHeaderViewAdapter;->getRemoveHintView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz p1, :cond_2

    if-nez v0, :cond_1

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object p1

    sget v0, Lcom/android/settings/R$layout;->deferred_setup_hint:I

    invoke-virtual {p1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const-string/jumbo p1, "removable_hint"

    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/android/settings/SettingsFragment;->mProxyAdapter:Lcom/android/settings/MiuiSettings$ProxyHeaderViewAdapter;

    invoke-virtual {p1, v0}, Lcom/android/settings/MiuiSettings$ProxyHeaderViewAdapter;->addRemovableHintView(Landroid/view/View;)V

    :cond_1
    invoke-virtual {p0, v0}, Lcom/android/settings/SettingsFragment;->setRemovableHintView(Landroid/view/View;)V

    sget p1, Lcom/android/settings/R$id;->deferred_setup_title:I

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    sget p1, Lcom/android/settings/R$id;->deferred_setup_arrow:I

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    invoke-virtual {p1, p5}, Landroid/widget/ImageView;->setImageResource(I)V

    sget p1, Lcom/android/settings/R$id;->hint_layout:I

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0, p4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p1, p6}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_3

    iget-object p1, p0, Lcom/android/settings/SettingsFragment;->mProxyAdapter:Lcom/android/settings/MiuiSettings$ProxyHeaderViewAdapter;

    invoke-virtual {p1, v0}, Lcom/android/settings/MiuiSettings$ProxyHeaderViewAdapter;->removeRemovableHintView(Landroid/view/View;)V

    invoke-virtual {p0, v2}, Lcom/android/settings/SettingsFragment;->setTipsLocalModel(Lcom/android/settings/utils/TipsUtils$TipsLocalModel;)V

    invoke-virtual {p0, v2}, Lcom/android/settings/SettingsFragment;->setRemovableHintView(Landroid/view/View;)V

    goto :goto_0

    :cond_3
    iget-object p1, p0, Lcom/android/settings/SettingsFragment;->mRemovableHintView:Landroid/view/View;

    if-eqz p1, :cond_4

    iget-object p2, p0, Lcom/android/settings/SettingsFragment;->mProxyAdapter:Lcom/android/settings/MiuiSettings$ProxyHeaderViewAdapter;

    invoke-virtual {p2, p1}, Lcom/android/settings/MiuiSettings$ProxyHeaderViewAdapter;->removeRemovableHintView(Landroid/view/View;)V

    invoke-virtual {p0, v2}, Lcom/android/settings/SettingsFragment;->setRemovableHintView(Landroid/view/View;)V

    :cond_4
    :goto_0
    return-void
.end method


# virtual methods
.method public buildAdapter()V
    .locals 8

    invoke-super {p0}, Lcom/android/settings/BasePreferenceFragment;->buildAdapter()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/android/settings/MiuiSettings;

    new-instance v7, Lcom/android/settings/MiuiSettings$HeaderAdapter;

    invoke-static {v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, p0, Lcom/android/settings/BasePreferenceFragment;->mHeaders:Ljava/util/List;

    invoke-virtual {v0}, Lcom/android/settings/MiuiSettings;->getAuthenticatorHelper()Lcom/android/settingslib/accounts/AuthenticatorHelper;

    move-result-object v5

    const/4 v6, 0x0

    move-object v1, v7

    move-object v2, v0

    move-object v3, v0

    invoke-direct/range {v1 .. v6}, Lcom/android/settings/MiuiSettings$HeaderAdapter;-><init>(Lcom/android/settings/MiuiSettings;Lmiuix/appcompat/app/AppCompatActivity;Ljava/util/List;Lcom/android/settingslib/accounts/AuthenticatorHelper;Z)V

    iput-object v7, p0, Lcom/android/settings/SettingsFragment;->mHeaderAdapter:Lcom/android/settings/MiuiSettings$HeaderAdapter;

    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->setHasStableIds(Z)V

    new-instance v2, Lcom/android/settings/MiuiSettings$ProxyHeaderViewAdapter;

    iget-object v3, p0, Lcom/android/settings/SettingsFragment;->mHeaderAdapter:Lcom/android/settings/MiuiSettings$HeaderAdapter;

    invoke-direct {v2, v0, v3}, Lcom/android/settings/MiuiSettings$ProxyHeaderViewAdapter;-><init>(Lcom/android/settings/MiuiSettings;Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    iput-object v2, p0, Lcom/android/settings/SettingsFragment;->mProxyAdapter:Lcom/android/settings/MiuiSettings$ProxyHeaderViewAdapter;

    new-instance v2, Lcom/android/settings/SettingsFragment$5;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/android/settings/SettingsFragment$5;-><init>(Lcom/android/settings/SettingsFragment;Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->setOrientation(I)V

    iget-object v3, p0, Lcom/android/settings/SettingsFragment;->mListView:Lmiuix/recyclerview/widget/RecyclerView;

    invoke-virtual {v3, v2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    iget-object v2, p0, Lcom/android/settings/SettingsFragment;->mListView:Lmiuix/recyclerview/widget/RecyclerView;

    iget-object v3, p0, Lcom/android/settings/SettingsFragment;->mProxyAdapter:Lcom/android/settings/MiuiSettings$ProxyHeaderViewAdapter;

    invoke-virtual {v2, v3}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    iget-object v2, p0, Lcom/android/settings/SettingsFragment;->mSearchResultListView:Lmiuix/recyclerview/widget/RecyclerView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    new-instance v2, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->setOrientation(I)V

    iget-object v1, p0, Lcom/android/settings/SettingsFragment;->mSearchResultListView:Lmiuix/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    iget-object v1, p0, Lcom/android/settings/SettingsFragment;->mSearchResultListView:Lmiuix/recyclerview/widget/RecyclerView;

    iget-object v2, p0, Lcom/android/settings/SettingsFragment;->mSearchAdapter:Lcom/android/settings/SettingsFragment$SearchResultAdapter;

    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    invoke-virtual {p0}, Lcom/android/settings/BasePreferenceFragment;->startSelectHeader()V

    invoke-static {v0}, Lcom/android/settings/utils/SettingsFeatures;->isSplitTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SettingsFragment;->loadRemovableHint()V

    :cond_0
    return-void
.end method

.method protected getHeadersResourceId()I
    .locals 0

    sget p0, Lcom/android/settings/R$xml;->settings_headers:I

    return p0
.end method

.method public getMergeSearchResults(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/search/SearchResultItem;",
            ">;",
            "Ljava/util/List<",
            "Lcom/android/settings/search/SearchResultItem;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/android/settings/search/SearchResultItem;",
            ">;"
        }
    .end annotation

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    goto :goto_0

    :cond_0
    move v3, v2

    :goto_0
    if-eqz p2, :cond_1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    goto :goto_1

    :cond_1
    move v4, v2

    :goto_1
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    invoke-direct {p0, p1}, Lcom/android/settings/SettingsFragment;->getNonEmptySearchResultCount(Ljava/util/List;)I

    move-result p0

    if-lez v4, :cond_2

    if-gtz p0, :cond_2

    return-object p2

    :cond_2
    if-gtz v4, :cond_3

    return-object p1

    :cond_3
    move p0, v2

    :goto_2
    if-ge v2, v3, :cond_5

    if-ge p0, v4, :cond_5

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/settings/search/SearchResultItem;

    iget-wide v6, v6, Lcom/android/settings/search/SearchResultItem;->score:D

    invoke-interface {p2, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/settings/search/SearchResultItem;

    iget-wide v8, v8, Lcom/android/settings/search/SearchResultItem;->score:D

    cmpl-double v6, v6, v8

    if-ltz v6, :cond_4

    add-int/lit8 v6, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/search/SearchResultItem;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v2, v6

    goto :goto_2

    :cond_4
    add-int/lit8 v6, p0, 0x1

    invoke-interface {p2, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/android/settings/search/SearchResultItem;

    invoke-interface {v5, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move p0, v6

    goto :goto_2

    :cond_5
    :goto_3
    if-ge v2, v3, :cond_6

    add-int/lit8 v6, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/search/SearchResultItem;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v2, v6

    goto :goto_3

    :cond_6
    :goto_4
    if-ge p0, v4, :cond_7

    add-int/lit8 p1, p0, 0x1

    invoke-interface {p2, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/android/settings/search/SearchResultItem;

    invoke-interface {v5, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move p0, p1

    goto :goto_4

    :cond_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p0

    long-to-double v0, v0

    long-to-double p0, p0

    const-string p2, "-"

    invoke-static {v0, v1, p0, p1, p2}, Lcom/android/settingslib/search/SearchUtils;->logCost(DDLjava/lang/Object;)V

    return-object v5
.end method

.method public getTipsLocalModel()Lcom/android/settings/utils/TipsUtils$TipsLocalModel;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mTipsLocalModel:Lcom/android/settings/utils/TipsUtils$TipsLocalModel;

    return-object p0
.end method

.method public loadRemovableHint()V
    .locals 8

    invoke-static {}, Lcom/android/settings/notify/SettingsNotifyEasyModeBuilder;->getInstance()Lcom/android/settings/notify/SettingsNotifyEasyModeBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/settings/notify/SettingsNotifyEasyModeBuilder;->build(Landroid/content/Context;)Lcom/android/settings/notify/SettingsNotifyEasyModeBuilder$SettingsNotify;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/android/settings/R$string;->easymode_hint:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/android/settings/R$color;->deferred_setup_text_color:I

    sget v5, Lcom/android/settings/R$drawable;->deferred_setup_corner:I

    sget v6, Lcom/android/settings/R$drawable;->ic_setup_hint:I

    new-instance v7, Lcom/android/settings/SettingsFragment$$ExternalSyntheticLambda0;

    invoke-direct {v7, p0, v0}, Lcom/android/settings/SettingsFragment$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/SettingsFragment;Lcom/android/settings/notify/SettingsNotifyEasyModeBuilder$SettingsNotify;)V

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/android/settings/SettingsFragment;->updateTips(ZLjava/lang/String;IIILandroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    new-instance v0, Lcom/android/settings/SettingsFragment$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/android/settings/SettingsFragment$$ExternalSyntheticLambda1;-><init>(Lcom/android/settings/SettingsFragment;)V

    invoke-static {v0}, Lcom/android/settingslib/utils/ThreadUtils;->postOnBackgroundThread(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method public mergeGlobalResults(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/android/settings/search/SearchResultItem;",
            ">;",
            "Ljava/util/List<",
            "Lcom/android/settings/search/SearchResultItem;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/android/settings/search/SearchResultItem;",
            ">;"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/android/settings/SettingsFragment;->getNonEmptySearchResultCount(Ljava/util/List;)I

    move-result v0

    if-gtz v0, :cond_0

    return-object p2

    :cond_0
    invoke-direct {p0, p2}, Lcom/android/settings/SettingsFragment;->getNonEmptySearchResultCount(Ljava/util/List;)I

    move-result v0

    if-gtz v0, :cond_1

    return-object p1

    :cond_1
    const/4 v0, 0x0

    invoke-interface {p1, v0, p2}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mGlobalSearch:Lcom/android/settings/search/SettingsGlobalSearcher;

    if-eqz p0, :cond_2

    invoke-virtual {p0, p1}, Lcom/android/settings/search/SettingsGlobalSearcher;->removeDuplicateSearchResult(Ljava/util/List;)V

    :cond_2
    return-object p1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/BasePreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryLists:Ljava/util/List;

    new-instance p1, Lcom/android/settings/utils/StorageListSPUtils;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    const-string/jumbo v1, "search_history"

    invoke-direct {p1, v0, v1}, Lcom/android/settings/utils/StorageListSPUtils;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/android/settings/SettingsFragment;->mStorageListSPUtils:Lcom/android/settings/utils/StorageListSPUtils;

    new-instance p1, Ljava/util/LinkedList;

    invoke-direct {p1}, Ljava/util/LinkedList;-><init>()V

    iput-object p1, p0, Lcom/android/settings/SettingsFragment;->mSearchResultItems:Ljava/util/List;

    sget-object v0, Lcom/android/settings/search/SearchResultItem;->EMPTY:Lcom/android/settings/search/SearchResultItem;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance p1, Lcom/android/settings/SettingsFragment$SearchResultAdapter;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/SettingsFragment;->mSearchResultItems:Ljava/util/List;

    invoke-direct {p1, p0, v0, v1}, Lcom/android/settings/SettingsFragment$SearchResultAdapter;-><init>(Lcom/android/settings/SettingsFragment;Landroid/content/Context;Ljava/util/List;)V

    iput-object p1, p0, Lcom/android/settings/SettingsFragment;->mSearchAdapter:Lcom/android/settings/SettingsFragment$SearchResultAdapter;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/android/settings/cloud/AccessibilityDisableList;->updateDisableSet(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/android/settings/SettingsFragment;->initGlobalSearchIfNeed()V

    iget-object p1, p0, Lcom/android/settings/SettingsFragment;->mSearchAdapter:Lcom/android/settings/SettingsFragment$SearchResultAdapter;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->setHasStableIds(Z)V

    new-instance p1, Lcom/android/settings/search/SearchResult;

    invoke-direct {p1}, Lcom/android/settings/search/SearchResult;-><init>()V

    iput-object p1, p0, Lcom/android/settings/SettingsFragment;->mSearchResult:Lcom/android/settings/search/SearchResult;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/android/settings/report/InternationalCompat;->reportSwitchStatus(Landroid/content/Context;)V

    const-string/jumbo p1, "settiing_homepage_show"

    invoke-static {p1}, Lcom/android/settings/report/InternationalCompat;->trackReportEvent(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/android/settings/utils/SettingsFeatures;->isSplitTablet(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/SettingsFragment;->getMiuiCustSplitUtils()Lcom/android/settings/MiuiCustSplitUtils;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/SettingsFragment;->mMiuiCustSplitUtils:Lcom/android/settings/MiuiCustSplitUtils;

    :cond_0
    iget-object p1, p0, Lcom/android/settings/SettingsFragment;->mTrimMemoryUtils:Lcom/android/settings/TrimMemoryUtils;

    if-nez p1, :cond_1

    new-instance p1, Lcom/android/settings/TrimMemoryUtils;

    invoke-direct {p1}, Lcom/android/settings/TrimMemoryUtils;-><init>()V

    iput-object p1, p0, Lcom/android/settings/SettingsFragment;->mTrimMemoryUtils:Lcom/android/settings/TrimMemoryUtils;

    invoke-virtual {p1}, Lcom/android/settings/TrimMemoryUtils;->addIdleHandler()V

    :cond_1
    invoke-static {}, Lcom/android/settings/search/SearchResult;->getSearchExcludeMap()Ljava/util/HashSet;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/SettingsFragment;->mSearchExcludeMap:Ljava/util/HashSet;

    iget-object p1, p0, Lcom/android/settings/SettingsFragment;->mTipsListener:Lcom/android/settings/SettingsFragment$TipsListener;

    if-nez p1, :cond_2

    new-instance p1, Lcom/android/settings/SettingsFragment$TipsListener;

    invoke-direct {p1, p0}, Lcom/android/settings/SettingsFragment$TipsListener;-><init>(Lcom/android/settings/SettingsFragment;)V

    iput-object p1, p0, Lcom/android/settings/SettingsFragment;->mTipsListener:Lcom/android/settings/SettingsFragment$TipsListener;

    :cond_2
    return-void
.end method

.method public onDestroy()V
    .locals 3

    invoke-super {p0}, Lcom/android/settings/BasePreferenceFragment;->onDestroy()V

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mTextWatcher:Landroid/text/TextWatcher;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2, v2, v2}, Landroid/text/TextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mSearchThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    iput-object v1, p0, Lcom/android/settings/SettingsFragment;->mSearchThread:Landroid/os/HandlerThread;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mSeparateAppSearchThread:Lcom/android/settings/search/appseparate/SeparateAppSearchThread;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    iput-object v1, p0, Lcom/android/settings/SettingsFragment;->mSeparateAppSearchThread:Lcom/android/settings/search/appseparate/SeparateAppSearchThread;

    :cond_1
    iput-object v1, p0, Lcom/android/settings/SettingsFragment;->mSearchResult:Lcom/android/settings/search/SearchResult;

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mGlobalSearch:Lcom/android/settings/search/SettingsGlobalSearcher;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/android/settings/search/SettingsGlobalSearcher;->unregisterSyncGlobalSearchCompleted()V

    :cond_2
    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mTrimMemoryUtils:Lcom/android/settings/TrimMemoryUtils;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/android/settings/TrimMemoryUtils;->removeIdleHandler()V

    iput-object v1, p0, Lcom/android/settings/SettingsFragment;->mTrimMemoryUtils:Lcom/android/settings/TrimMemoryUtils;

    :cond_3
    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    sget p3, Lcom/android/settings/R$layout;->settings_search_fragment:I

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    sget p2, Lcom/android/settings/R$id;->search_result_ll:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/LinearLayout;

    iput-object p2, p0, Lcom/android/settings/SettingsFragment;->mSearchResultLinearLayout:Landroid/widget/LinearLayout;

    sget p2, Lcom/android/settings/R$id;->scroll_headers:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lmiuix/recyclerview/widget/RecyclerView;

    iput-object p2, p0, Lcom/android/settings/SettingsFragment;->mListView:Lmiuix/recyclerview/widget/RecyclerView;

    const/4 p3, 0x1

    invoke-virtual {p2, p3}, Landroid/view/ViewGroup;->setFocusable(Z)V

    iget-object p2, p0, Lcom/android/settings/SettingsFragment;->mListView:Lmiuix/recyclerview/widget/RecyclerView;

    invoke-virtual {p2, p3}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    iget-object p2, p0, Lcom/android/settings/SettingsFragment;->mListView:Lmiuix/recyclerview/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    iget-object p2, p0, Lcom/android/settings/SettingsFragment;->mListView:Lmiuix/recyclerview/widget/RecyclerView;

    const/4 v2, -0x1

    invoke-virtual {p2, v2}, Landroidx/recyclerview/widget/RecyclerView;->setItemViewCacheSize(I)V

    sget p2, Lcom/android/settings/R$id;->search_result:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lmiuix/recyclerview/widget/RecyclerView;

    iput-object p2, p0, Lcom/android/settings/SettingsFragment;->mSearchResultListView:Lmiuix/recyclerview/widget/RecyclerView;

    invoke-virtual {p2, p3}, Landroid/view/ViewGroup;->setFocusable(Z)V

    iget-object p2, p0, Lcom/android/settings/SettingsFragment;->mSearchResultListView:Lmiuix/recyclerview/widget/RecyclerView;

    invoke-virtual {p2, p3}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    iget-object p2, p0, Lcom/android/settings/SettingsFragment;->mSearchResultListView:Lmiuix/recyclerview/widget/RecyclerView;

    invoke-virtual {p2, p0}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object p2, p0, Lcom/android/settings/SettingsFragment;->mSearchResultListView:Lmiuix/recyclerview/widget/RecyclerView;

    invoke-virtual {p2, v1}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    sget p2, Lcom/android/settings/R$id;->search_loading:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lcom/android/settings/SettingsFragment;->mSearchLoadingView:Landroid/view/View;

    sget p2, Lcom/android/settings/R$id;->search_history:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroidx/core/widget/NestedScrollView;

    iput-object p2, p0, Lcom/android/settings/SettingsFragment;->mSearchListLayout:Landroidx/core/widget/NestedScrollView;

    sget p2, Lcom/android/settings/R$id;->search_history_fl:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/android/settings/widget/FlowLayout;

    iput-object p2, p0, Lcom/android/settings/SettingsFragment;->mSearchHistoryFl:Lcom/android/settings/widget/FlowLayout;

    sget p2, Lcom/android/settings/R$id;->search_history_clear_tv:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    new-instance v3, Lcom/android/settings/SettingsFragment$1;

    invoke-direct {v3, p0}, Lcom/android/settings/SettingsFragment$1;-><init>(Lcom/android/settings/SettingsFragment;)V

    invoke-virtual {p2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/android/settings/SettingsFragment;->initSearchHistoryView()V

    :try_start_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    new-array p3, p3, [I

    const v3, 0x1010054

    aput v3, p3, v0

    invoke-virtual {p2, v1, p3}, Landroid/app/Activity;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p2

    iget-object p3, p0, Lcom/android/settings/SettingsFragment;->mSearchLoadingView:Landroid/view/View;

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p2

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mSearchLoadingView:Landroid/view/View;

    invoke-virtual {p0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    const-string p0, "SettingsFragment"

    const-string p3, "Fail to find windowBackground in current context"

    invoke-static {p0, p3, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-object p1
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/BasePreferenceFragment;->onResume()V

    invoke-virtual {p0}, Lcom/android/settings/SettingsFragment;->loadRemovableHint()V

    invoke-direct {p0}, Lcom/android/settings/SettingsFragment;->initGlobalSearchIfNeed()V

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mSeparateAppSearchThread:Lcom/android/settings/search/appseparate/SeparateAppSearchThread;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/SettingsFragment;->mIsInActionMode:Z

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mSeparateAppSearchThread:Lcom/android/settings/search/appseparate/SeparateAppSearchThread;

    invoke-virtual {p0}, Lcom/android/settings/search/appseparate/SeparateAppSearchThread;->sendInitMessage()V

    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 3

    invoke-super {p0}, Lcom/android/settings/BasePreferenceFragment;->onStart()V

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "DEFERRED_SETUP"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "isShow"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/SettingsFragment;->addHintView()V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mDeferredSetupHelper:Lcom/android/settings/DeferredSetupHelper;

    if-nez v0, :cond_2

    new-instance v0, Lcom/android/settings/DeferredSetupHelper;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/SettingsFragment;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1, v2}, Lcom/android/settings/DeferredSetupHelper;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->mDeferredSetupHelper:Lcom/android/settings/DeferredSetupHelper;

    :cond_2
    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mDeferredSetupHelper:Lcom/android/settings/DeferredSetupHelper;

    invoke-virtual {p0}, Lcom/android/settings/DeferredSetupHelper;->startLoad()V

    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/BasePreferenceFragment;->onStop()V

    invoke-direct {p0}, Lcom/android/settings/SettingsFragment;->logDetailSearchTime()V

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mDeferredSetupHelper:Lcom/android/settings/DeferredSetupHelper;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/settings/DeferredSetupHelper;->stop()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/SettingsFragment;->mDeferredSetupHelper:Lcom/android/settings/DeferredSetupHelper;

    :cond_1
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    iget-object p2, p0, Lcom/android/settings/SettingsFragment;->mSearchResultListView:Lmiuix/recyclerview/widget/RecyclerView;

    if-ne p1, p2, :cond_0

    invoke-direct {p0}, Lcom/android/settings/SettingsFragment;->hideSoftKeyboard()V

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public onTrimMemory(I)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settings/BasePreferenceFragment;->onTrimMemory(I)V

    const/16 v0, 0x50

    if-ne v0, p1, :cond_2

    const-string p1, "SettingsFragment"

    const-string/jumbo v0, "onTrimMemory TRIM_MEMORY_COMPLETE"

    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lcom/android/settings/SettingsFragment;->mGlobalSearch:Lcom/android/settings/search/SettingsGlobalSearcher;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/settings/search/SettingsGlobalSearcher;->unregisterSyncGlobalSearchCompleted()V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/android/settings/SettingsFragment;->mGlobalSearch:Lcom/android/settings/search/SettingsGlobalSearcher;

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/SettingsFragment;->releaseSettingsTree()V

    iget-object p1, p0, Lcom/android/settings/SettingsFragment;->mSeparateAppSearchThread:Lcom/android/settings/search/appseparate/SeparateAppSearchThread;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/android/settings/search/appseparate/SeparateAppSearchThread;->sendReleaseMessage()V

    :cond_1
    invoke-static {}, Lcom/android/settingslib/search/SearchUtils;->clearPackageExistedCache()V

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->isSplitTabletDevice()Z

    move-result p1

    if-nez p1, :cond_2

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->isFoldDevice()Z

    move-result p1

    if-nez p1, :cond_2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    const-wide/16 v0, 0x0

    invoke-static {p0, v0, v1}, Lmiui/settings/commonlib/MemoryOptimizationUtil;->sendMemoryOptimizationMsg(Landroid/content/Context;J)V

    :cond_2
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/android/settings/BasePreferenceFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    sget p2, Lcom/android/settings/R$id;->header_view:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/SettingsFragment;->mAnchorView:Landroid/view/View;

    new-instance p2, Lcom/android/settings/SettingsFragment$6;

    invoke-direct {p2, p0}, Lcom/android/settings/SettingsFragment$6;-><init>(Lcom/android/settings/SettingsFragment;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/android/settings/SettingsFragment;->mAnchorView:Landroid/view/View;

    const p2, 0x1020009

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    sget p2, Lcom/android/settings/R$string;->search_input_hint:I

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setHint(I)V

    iget-object p1, p0, Lcom/android/settings/SettingsFragment;->mAnchorView:Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/android/settings/SettingsFragment;->adapterAccessibility(Landroid/view/View;)V

    return-void
.end method

.method public setRemovableHintView(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/SettingsFragment;->mRemovableHintView:Landroid/view/View;

    return-void
.end method

.method public setTipsLocalModel(Lcom/android/settings/utils/TipsUtils$TipsLocalModel;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/SettingsFragment;->mTipsLocalModel:Lcom/android/settings/utils/TipsUtils$TipsLocalModel;

    return-void
.end method

.method public updateAdapter()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/SettingsFragment;->mListView:Lmiuix/recyclerview/widget/RecyclerView;

    if-eqz v0, :cond_1

    iget-object p0, p0, Lcom/android/settings/SettingsFragment;->mProxyAdapter:Lcom/android/settings/MiuiSettings$ProxyHeaderViewAdapter;

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    :cond_1
    :goto_0
    return-void
.end method
