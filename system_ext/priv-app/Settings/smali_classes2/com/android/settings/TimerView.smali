.class public Lcom/android/settings/TimerView;
.super Landroid/widget/FrameLayout;


# instance fields
.field private mBmpHour:Landroid/graphics/Bitmap;

.field private mBmpMinute:Landroid/graphics/Bitmap;

.field private mCalendar:Ljava/util/Calendar;

.field private mDate:Landroid/widget/TextView;

.field private mHalfHeight:I

.field private mHalfWidth:I

.field private mHeight:I

.field private mHourIV:Landroid/widget/ImageView;

.field private mMatrix:Landroid/graphics/Matrix;

.field private mMinuteIV:Landroid/widget/ImageView;

.field private mTime:Landroid/widget/TextView;

.field private mWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/android/settings/TimerView;->mMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, p1}, Lcom/android/settings/TimerView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance p2, Landroid/graphics/Matrix;

    invoke-direct {p2}, Landroid/graphics/Matrix;-><init>()V

    iput-object p2, p0, Lcom/android/settings/TimerView;->mMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, p1}, Lcom/android/settings/TimerView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance p2, Landroid/graphics/Matrix;

    invoke-direct {p2}, Landroid/graphics/Matrix;-><init>()V

    iput-object p2, p0, Lcom/android/settings/TimerView;->mMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, p1}, Lcom/android/settings/TimerView;->init(Landroid/content/Context;)V

    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 2

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    sget v1, Lcom/android/settings/R$layout;->usage_timer:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/settings/R$drawable;->clock_hour:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/TimerView;->mBmpHour:Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/android/settings/R$drawable;->clock_minutes:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    check-cast p1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/TimerView;->mBmpMinute:Landroid/graphics/Bitmap;

    iget-object p1, p0, Lcom/android/settings/TimerView;->mBmpHour:Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p1

    iput p1, p0, Lcom/android/settings/TimerView;->mWidth:I

    iget-object p1, p0, Lcom/android/settings/TimerView;->mBmpHour:Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    iput p1, p0, Lcom/android/settings/TimerView;->mHeight:I

    iget v0, p0, Lcom/android/settings/TimerView;->mWidth:I

    shr-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/settings/TimerView;->mHalfWidth:I

    shr-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/android/settings/TimerView;->mHalfHeight:I

    sget p1, Lcom/android/settings/R$id;->usage_time:I

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/android/settings/TimerView;->mTime:Landroid/widget/TextView;

    sget p1, Lcom/android/settings/R$id;->usage_date:I

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/android/settings/TimerView;->mDate:Landroid/widget/TextView;

    sget p1, Lcom/android/settings/R$id;->clock_hour:I

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/android/settings/TimerView;->mHourIV:Landroid/widget/ImageView;

    sget p1, Lcom/android/settings/R$id;->clock_minute:I

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/android/settings/TimerView;->mMinuteIV:Landroid/widget/ImageView;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/TimerView;->mCalendar:Ljava/util/Calendar;

    return-void
.end method


# virtual methods
.method public setTimer(Ljava/lang/Long;)V
    .locals 11

    iget-object v0, p0, Lcom/android/settings/TimerView;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object p1, p0, Lcom/android/settings/TimerView;->mCalendar:Ljava/util/Calendar;

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result p1

    iget-object v0, p0, Lcom/android/settings/TimerView;->mCalendar:Ljava/util/Calendar;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/TimerView;->mCalendar:Ljava/util/Calendar;

    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v5, 0x1

    aput-object p1, v3, v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x2

    aput-object p1, v3, v1

    const-string p1, "%d:%02d:%02d"

    invoke-static {p1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iget-object v3, p0, Lcom/android/settings/TimerView;->mTime:Landroid/widget/TextView;

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/android/settings/TimerView;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {p1, v5}, Ljava/util/Calendar;->get(I)I

    move-result p1

    iget-object v3, p0, Lcom/android/settings/TimerView;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v3, v1}, Ljava/util/Calendar;->get(I)I

    move-result v3

    add-int/2addr v3, v5

    iget-object v6, p0, Lcom/android/settings/TimerView;->mCalendar:Ljava/util/Calendar;

    const/4 v7, 0x5

    invoke-virtual {v6, v7}, Ljava/util/Calendar;->get(I)I

    move-result v6

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v2, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v2, v5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v2, v1

    const-string p1, "%d.%d.%d"

    invoke-static {p1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/android/settings/TimerView;->mDate:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/android/settings/TimerView;->mCalendar:Ljava/util/Calendar;

    const/16 v1, 0xa

    invoke-virtual {p1, v1}, Ljava/util/Calendar;->get(I)I

    move-result p1

    iget-object v1, p0, Lcom/android/settings/TimerView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    iget-object v1, p0, Lcom/android/settings/TimerView;->mMatrix:Landroid/graphics/Matrix;

    mul-int/lit8 p1, p1, 0x1e

    int-to-float p1, p1

    iget v2, p0, Lcom/android/settings/TimerView;->mHalfHeight:I

    int-to-float v2, v2

    iget v3, p0, Lcom/android/settings/TimerView;->mHalfWidth:I

    int-to-float v3, v3

    invoke-virtual {v1, p1, v2, v3}, Landroid/graphics/Matrix;->setRotate(FFF)V

    iget-object v4, p0, Lcom/android/settings/TimerView;->mBmpHour:Landroid/graphics/Bitmap;

    iget v7, p0, Lcom/android/settings/TimerView;->mWidth:I

    iget v8, p0, Lcom/android/settings/TimerView;->mHeight:I

    iget-object v9, p0, Lcom/android/settings/TimerView;->mMatrix:Landroid/graphics/Matrix;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v10, 0x1

    invoke-static/range {v4 .. v10}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p1

    iget-object v1, p0, Lcom/android/settings/TimerView;->mHourIV:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object p1, p0, Lcom/android/settings/TimerView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1}, Landroid/graphics/Matrix;->reset()V

    iget-object p1, p0, Lcom/android/settings/TimerView;->mMatrix:Landroid/graphics/Matrix;

    mul-int/lit8 v0, v0, 0x6

    int-to-float v0, v0

    iget v1, p0, Lcom/android/settings/TimerView;->mHalfHeight:I

    int-to-float v1, v1

    iget v2, p0, Lcom/android/settings/TimerView;->mHalfWidth:I

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    iget-object v3, p0, Lcom/android/settings/TimerView;->mBmpMinute:Landroid/graphics/Bitmap;

    iget v6, p0, Lcom/android/settings/TimerView;->mWidth:I

    iget v7, p0, Lcom/android/settings/TimerView;->mHeight:I

    iget-object v8, p0, Lcom/android/settings/TimerView;->mMatrix:Landroid/graphics/Matrix;

    const/4 v4, 0x0

    const/4 v9, 0x1

    invoke-static/range {v3 .. v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p1

    iget-object p0, p0, Lcom/android/settings/TimerView;->mMinuteIV:Landroid/widget/ImageView;

    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method
