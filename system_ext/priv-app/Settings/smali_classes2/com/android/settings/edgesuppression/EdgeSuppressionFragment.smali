.class public Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;
.super Lcom/android/settings/SettingsPreferenceFragment;

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceClickListener;


# static fields
.field private static final CURRENT_SUPPORT_EDGE_MODE_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContext:Landroid/content/Context;

.field private mEdgeModeSizePrefs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lmiuix/preference/RadioButtonPreference;",
            ">;"
        }
    .end annotation
.end field

.field private mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

.field private final mLayListener:Lcom/android/settings/edgesuppression/LaySensorWrapper$LaySensorChangeListener;

.field private mLaySensorState:I

.field private mLeftLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

.field private mLeftView:Landroid/view/View;

.field private mPrefCustomize:Lmiuix/preference/RadioButtonPreference;

.field private mPreferenceScreen:Landroidx/preference/PreferenceScreen;

.field private mRestrictedPreference:Landroidx/preference/Preference;

.field private mRestrictedSeekBar:Lcom/android/settings/widget/SeekBarPreference;

.field private mRestrictedType:Ljava/lang/String;

.field private mRestrictedValue:F

.field private mRightLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

.field private mRightView:Landroid/view/View;

.field private mSuppressionTipAreaView:Lcom/android/settings/edgesuppression/SuppressionTipAreaView;

.field private mTipAreaWidth:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmEdgeSuppressionManager(Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;)Lcom/android/settings/edgesuppression/EdgeSuppressionManager;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLaySensorState(Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;)I
    .locals 0

    iget p0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mLaySensorState:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmPrefCustomize(Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;)Lmiuix/preference/RadioButtonPreference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mPrefCustomize:Lmiuix/preference/RadioButtonPreference;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSuppressionTipAreaView(Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;)Lcom/android/settings/edgesuppression/SuppressionTipAreaView;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mSuppressionTipAreaView:Lcom/android/settings/edgesuppression/SuppressionTipAreaView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTipAreaWidth(Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;)I
    .locals 0

    iget p0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mTipAreaWidth:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmLaySensorState(Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mLaySensorState:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmTipAreaWidth(Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mTipAreaWidth:I

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetSeekBarValue(Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;I)F
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->getSeekBarValue(I)F

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$msetEdgeSuppression(Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;Lmiuix/preference/RadioButtonPreference;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->setEdgeSuppression(Lmiuix/preference/RadioButtonPreference;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetRestrictedViewWidth(Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->setRestrictedViewWidth(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateSuppreesionTipAreaView(Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;Lcom/android/settings/edgesuppression/SuppressionTipAreaView;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->updateSuppreesionTipAreaView(Lcom/android/settings/edgesuppression/SuppressionTipAreaView;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 4

    const-string v0, "default_suppression"

    const-string/jumbo v1, "strong_suppression"

    const-string/jumbo v2, "wake_suppression"

    const-string v3, "custom_suppression"

    filled-new-array {v0, v1, v2, v3}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->CURRENT_SUPPORT_EDGE_MODE_LIST:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/SettingsPreferenceFragment;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mLaySensorState:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeModeSizePrefs:Ljava/util/ArrayList;

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/16 v2, 0x33

    invoke-direct {v0, v1, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iput-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mLeftLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v2, 0x35

    invoke-direct {v0, v1, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iput-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRightLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    new-instance v0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment$1;

    invoke-direct {v0, p0}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment$1;-><init>(Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;)V

    iput-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mLayListener:Lcom/android/settings/edgesuppression/LaySensorWrapper$LaySensorChangeListener;

    return-void
.end method

.method private changeRestrictedType()V
    .locals 3

    sget-object v0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->CURRENT_SUPPORT_EDGE_MODE_LIST:Ljava/util/List;

    iget-object v1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedType:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "default_suppression"

    iput-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedType:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object p0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedType:Ljava/lang/String;

    const/4 v1, -0x2

    const-string v2, "edge_type"

    invoke-static {v0, v2, p0, v1}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    :cond_0
    return-void
.end method

.method private getSeekBarProgress()I
    .locals 3

    iget v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedValue:F

    iget-object v1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->getConditionSize(I)I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget-object p0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    invoke-virtual {p0}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->getAllowAdjustRange()I

    move-result p0

    int-to-float p0, p0

    div-float/2addr v0, p0

    const/high16 p0, 0x447a0000    # 1000.0f

    mul-float/2addr v0, p0

    float-to-int p0, v0

    return p0
.end method

.method private getSeekBarValue(I)F
    .locals 1

    int-to-float p1, p1

    const/high16 v0, 0x3f800000    # 1.0f

    mul-float/2addr p1, v0

    const/high16 v0, 0x447a0000    # 1000.0f

    div-float/2addr p1, v0

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    invoke-virtual {v0}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->getAllowAdjustRange()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr p1, v0

    iget-object p0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->getConditionSize(I)I

    move-result p0

    int-to-float p0, p0

    add-float/2addr p1, p0

    return p1
.end method

.method private initFragment()V
    .locals 5

    sget v0, Lcom/android/settings/R$xml;->edge_settings_select_fragment:I

    invoke-virtual {p0, v0}, Lcom/android/settings/SettingsPreferenceFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mPreferenceScreen:Landroidx/preference/PreferenceScreen;

    const-string v1, "default_suppression"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiuix/preference/RadioButtonPreference;

    iget-object v1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    invoke-virtual {v1}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->isSupportSensor()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/android/settings/R$string;->intelligence_suppression_title:I

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setTitle(I)V

    sget v1, Lcom/android/settings/R$string;->intelligence_suppression_summary:I

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setSummary(I)V

    :cond_0
    iget-object v1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mPreferenceScreen:Landroidx/preference/PreferenceScreen;

    const-string/jumbo v2, "strong_suppression"

    invoke-virtual {v1, v2}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v1

    check-cast v1, Lmiuix/preference/RadioButtonPreference;

    iget-object v2, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mPreferenceScreen:Landroidx/preference/PreferenceScreen;

    const-string/jumbo v3, "wake_suppression"

    invoke-virtual {v2, v3}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v2

    check-cast v2, Lmiuix/preference/RadioButtonPreference;

    iget-object v3, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mPreferenceScreen:Landroidx/preference/PreferenceScreen;

    const-string v4, "custom_suppression"

    invoke-virtual {v3, v4}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v3

    check-cast v3, Lmiuix/preference/RadioButtonPreference;

    iput-object v3, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mPrefCustomize:Lmiuix/preference/RadioButtonPreference;

    iget-object v3, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mPreferenceScreen:Landroidx/preference/PreferenceScreen;

    const-string/jumbo v4, "restricted_info"

    invoke-virtual {v3, v4}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v3

    check-cast v3, Lcom/android/settings/keys/RestrictedEdgeDescriptionPreference;

    iget-object v4, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeModeSizePrefs:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeModeSizePrefs:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeModeSizePrefs:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeModeSizePrefs:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mPrefCustomize:Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeModeSizePrefs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/preference/RadioButtonPreference;

    iget-object v2, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mPreferenceScreen:Landroidx/preference/PreferenceScreen;

    invoke-virtual {v2, v1}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    invoke-virtual {v1, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_0

    :cond_1
    iget-object p0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mPreferenceScreen:Landroidx/preference/PreferenceScreen;

    invoke-virtual {p0, v3}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    return-void
.end method

.method private initSeekBarFragment()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mPreferenceScreen:Landroidx/preference/PreferenceScreen;

    const-string v1, "edge_mode_adjust_level"

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedPreference:Landroidx/preference/Preference;

    invoke-virtual {p0, v1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settings/widget/SeekBarPreference;

    iput-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedSeekBar:Lcom/android/settings/widget/SeekBarPreference;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/SeekBarPreference;->setMax(I)V

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedSeekBar:Lcom/android/settings/widget/SeekBarPreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/SeekBarPreference;->setContinuousUpdates(Z)V

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedSeekBar:Lcom/android/settings/widget/SeekBarPreference;

    new-instance v1, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment$2;

    invoke-direct {v1, p0}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment$2;-><init>(Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;)V

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedSeekBar:Lcom/android/settings/widget/SeekBarPreference;

    new-instance v1, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment$3;

    invoke-direct {v1, p0}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment$3;-><init>(Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;)V

    invoke-virtual {v0, v1}, Lcom/android/settings/widget/SeekBarPreference;->setStopTrackingTouchListener(Lcom/android/settings/widget/SeekBarPreference$StopTrackingTouchListener;)V

    return-void
.end method

.method private initSuppressionTipAreaView()V
    .locals 6

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x8000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    new-instance v1, Lcom/android/settings/edgesuppression/SuppressionTipAreaView;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mTipAreaWidth:I

    iget-object v4, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    iget v5, v4, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->mScreenWidth:I

    iget v4, v4, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->mScreenHeight:I

    invoke-direct {v1, v2, v3, v5, v4}, Lcom/android/settings/edgesuppression/SuppressionTipAreaView;-><init>(Landroid/content/Context;III)V

    iput-object v1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mSuppressionTipAreaView:Lcom/android/settings/edgesuppression/SuppressionTipAreaView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method private initTipView(Landroid/content/Context;I)V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mLeftLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iput p2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iget-object v1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    iget v1, v1, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->mScreenHeight:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mLeftView:Landroid/view/View;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$color;->restricted_tip_area_color:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRightLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iput p2, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iget-object p2, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    iget p2, p2, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->mScreenHeight:I

    iput p2, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    new-instance p2, Landroid/view/View;

    invoke-direct {p2, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRightView:Landroid/view/View;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1, v2, v3}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result p1

    invoke-virtual {p2, p1}, Landroid/view/View;->setBackgroundColor(I)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    const/high16 p2, 0x8000000

    invoke-virtual {p1, p2}, Landroid/view/Window;->addFlags(I)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iget-object p2, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mLeftView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mLeftLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p1, p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p2, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRightView:Landroid/view/View;

    iget-object p0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRightLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p1, p2, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private resetSensorState()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->getConditionSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mTipAreaWidth:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mLaySensorState:I

    return-void
.end method

.method private setDefaultValue()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedType:Ljava/lang/String;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mPreferenceScreen:Landroidx/preference/PreferenceScreen;

    const-string v2, "default_suppression"

    invoke-virtual {v0, v2}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iput-object v2, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedType:Ljava/lang/String;

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedType:Ljava/lang/String;

    const-string v1, "custom_suppression"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->getSeekBarProgress()I

    move-result v0

    iget-object p0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedSeekBar:Lcom/android/settings/widget/SeekBarPreference;

    invoke-virtual {p0, v0}, Lcom/android/settings/widget/SeekBarPreference;->setProgress(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method private setEdgeSuppression(Lmiuix/preference/RadioButtonPreference;)V
    .locals 7

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeModeSizePrefs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/preference/RadioButtonPreference;

    if-ne v1, p1, :cond_0

    move v2, v3

    :cond_0
    invoke-virtual {v1, v2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v4

    const/4 v5, 0x3

    const/4 v6, 0x2

    sparse-switch v4, :sswitch_data_0

    :goto_1
    move v2, v1

    goto :goto_2

    :sswitch_0
    const-string v2, "custom_suppression"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_1

    :cond_2
    move v2, v5

    goto :goto_2

    :sswitch_1
    const-string/jumbo v2, "wake_suppression"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_1

    :cond_3
    move v2, v6

    goto :goto_2

    :sswitch_2
    const-string/jumbo v2, "strong_suppression"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    goto :goto_1

    :cond_4
    move v2, v3

    goto :goto_2

    :sswitch_3
    const-string v4, "default_suppression"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    goto :goto_1

    :cond_5
    :goto_2
    packed-switch v2, :pswitch_data_0

    goto :goto_3

    :pswitch_0
    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedSeekBar:Lcom/android/settings/widget/SeekBarPreference;

    invoke-virtual {v0}, Lcom/android/settings/widget/SeekBarPreference;->getProgress()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->getSeekBarValue(I)F

    move-result v0

    iput v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedValue:F

    goto :goto_3

    :pswitch_1
    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    invoke-virtual {v0, v3}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->getConditionSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedValue:F

    goto :goto_3

    :pswitch_2
    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    invoke-virtual {v0, v5}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->getConditionSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedValue:F

    goto :goto_3

    :pswitch_3
    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    invoke-virtual {v0, v6}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->getConditionSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedValue:F

    :goto_3
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    const-string v1, "edge_type"

    const/4 v2, -0x2

    invoke-static {v0, v1, p1, v2}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    iget-object p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    invoke-virtual {p1}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->isReflectionFailed()Z

    move-result p1

    if-eqz p1, :cond_6

    iget p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedValue:F

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->getConditionSize(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr p1, v0

    goto :goto_4

    :cond_6
    iget p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedValue:F

    :goto_4
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "edge_size"

    invoke-static {p0, v0, p1, v2}, Landroid/provider/Settings$System;->putFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;FI)Z

    return-void

    :sswitch_data_0
    .sparse-switch
        -0x1ca0abcb -> :sswitch_3
        0x3cb5238b -> :sswitch_2
        0x5d6e1318 -> :sswitch_1
        0x600bc165 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private setRestrictedViewWidth(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mLeftView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRightView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mLeftView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mLeftLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRightView:Landroid/view/View;

    iget-object p0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRightLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p1, p0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private setSeekBarEnable()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedSeekBar:Lcom/android/settings/widget/SeekBarPreference;

    iget-object v1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mPrefCustomize:Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {v1}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/RestrictedPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mPrefCustomize:Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {v0}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mPreferenceScreen:Landroidx/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedPreference:Landroidx/preference/Preference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedSeekBar:Lcom/android/settings/widget/SeekBarPreference;

    invoke-direct {p0}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->getSeekBarProgress()I

    move-result p0

    invoke-virtual {v0, p0}, Lcom/android/settings/widget/SeekBarPreference;->setProgress(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mPreferenceScreen:Landroidx/preference/PreferenceScreen;

    iget-object p0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedPreference:Landroidx/preference/Preference;

    invoke-virtual {v0, p0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :goto_0
    return-void
.end method

.method private updateSuppreesionTipAreaView(Lcom/android/settings/edgesuppression/SuppressionTipAreaView;)V
    .locals 2

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mTipAreaWidth:I

    iget-object p0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    iget v1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->mScreenWidth:I

    iget p0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->mScreenHeight:I

    invoke-virtual {p1, v0, v1, p0}, Lcom/android/settings/edgesuppression/SuppressionTipAreaView;->setTipWidth(III)V

    const/4 p0, 0x1

    invoke-virtual {p1, p0}, Landroid/view/View;->invalidate(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onAttach(Landroid/content/Context;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onAttach(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mContext:Landroid/content/Context;

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    sget v0, Lcom/android/settings/R$string;->edge_mode_state_title:I

    invoke-virtual {p1, v0}, Landroid/app/Activity;->setTitle(I)V

    iget-object p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->getInstance(Landroid/content/Context;)Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    invoke-virtual {p1}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->isReflectionFailed()Z

    move-result p1

    const/4 v0, 0x2

    const-string v1, "edge_size"

    const/4 v2, -0x2

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    iget-object v3, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    invoke-virtual {v3, v0}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->getOldConditionSize(I)F

    move-result v0

    invoke-static {p1, v1, v0, v2}, Landroid/provider/Settings$System;->getFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;FI)F

    move-result p1

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->getConditionSize(I)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr p1, v0

    iput p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedValue:F

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    iget-object v3, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    invoke-virtual {v3, v0}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->getConditionSize(I)I

    move-result v0

    int-to-float v0, v0

    invoke-static {p1, v1, v0, v2}, Landroid/provider/Settings$System;->getFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;FI)F

    move-result p1

    iput p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedValue:F

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string v0, "edge_type"

    invoke-static {p1, v0, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedType:Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->changeRestrictedType()V

    iget p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedValue:F

    float-to-int p1, p1

    iput p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mTipAreaWidth:I

    invoke-direct {p0}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->initFragment()V

    invoke-direct {p0}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->initSeekBarFragment()V

    invoke-direct {p0}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->setDefaultValue()V

    invoke-direct {p0}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->setSeekBarEnable()V

    iget-object p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    invoke-virtual {p1}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->setScreenSize()V

    iget-object p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    invoke-virtual {p1}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->isSupportSensor()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->initSuppressionTipAreaView()V

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mContext:Landroid/content/Context;

    iget v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mTipAreaWidth:I

    invoke-direct {p0, p1, v0}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->initTipView(Landroid/content/Context;I)V

    :goto_1
    return-void
.end method

.method public onPause()V
    .locals 4

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onPause()V

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    iget v2, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedValue:F

    iget-object v3, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedType:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->getSizeOfInputMethod(FLjava/lang/String;)I

    move-result v1

    const-string/jumbo v2, "vertical_edge_suppression_size"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    iget v2, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedValue:F

    iget-object v3, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedType:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->getSizeOfInputMethod(FLjava/lang/String;)I

    move-result v1

    const-string v2, "horizontal_edge_suppression_size"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    invoke-virtual {v0}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->isSupportSensor()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    invoke-virtual {v0}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->unRegisterLaySensor()V

    invoke-direct {p0}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->resetSensorState()V

    :cond_0
    return-void
.end method

.method public onPreferenceClick(Landroidx/preference/Preference;)Z
    .locals 2

    move-object v0, p1

    check-cast v0, Lmiuix/preference/RadioButtonPreference;

    invoke-direct {p0, v0}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->setEdgeSuppression(Lmiuix/preference/RadioButtonPreference;)V

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedType:Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->setSeekBarEnable()V

    iget-object p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    invoke-virtual {p1}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->isSupportSensor()Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->resetSensorState()V

    iget-object p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedType:Ljava/lang/String;

    const-string v0, "default_suppression"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    invoke-virtual {p1}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->unRegisterLaySensor()V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mLayListener:Lcom/android/settings/edgesuppression/LaySensorWrapper$LaySensorChangeListener;

    invoke-virtual {p1, v0}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->registerLaySensor(Lcom/android/settings/edgesuppression/LaySensorWrapper$LaySensorChangeListener;)V

    :cond_2
    :goto_0
    iget p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedValue:F

    float-to-int p1, p1

    iput p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mTipAreaWidth:I

    iget-object p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    invoke-virtual {p1}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->isSupportSensor()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mSuppressionTipAreaView:Lcom/android/settings/edgesuppression/SuppressionTipAreaView;

    invoke-direct {p0, p1}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->updateSuppreesionTipAreaView(Lcom/android/settings/edgesuppression/SuppressionTipAreaView;)V

    goto :goto_1

    :cond_3
    iget p1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mTipAreaWidth:I

    invoke-direct {p0, p1}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->setRestrictedViewWidth(I)V

    :goto_1
    return v1
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    invoke-virtual {v0}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->setScreenSize()V

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    invoke-virtual {v0}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->isSupportSensor()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mRestrictedType:Ljava/lang/String;

    const-string v1, "default_suppression"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mEdgeSuppressionManager:Lcom/android/settings/edgesuppression/EdgeSuppressionManager;

    iget-object v1, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mLayListener:Lcom/android/settings/edgesuppression/LaySensorWrapper$LaySensorChangeListener;

    invoke-virtual {v0, v1}, Lcom/android/settings/edgesuppression/EdgeSuppressionManager;->registerLaySensor(Lcom/android/settings/edgesuppression/LaySensorWrapper$LaySensorChangeListener;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mSuppressionTipAreaView:Lcom/android/settings/edgesuppression/SuppressionTipAreaView;

    invoke-direct {p0, v0}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->updateSuppreesionTipAreaView(Lcom/android/settings/edgesuppression/SuppressionTipAreaView;)V

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->mTipAreaWidth:I

    invoke-direct {p0, v0}, Lcom/android/settings/edgesuppression/EdgeSuppressionFragment;->setRestrictedViewWidth(I)V

    :goto_0
    return-void
.end method
