.class public Lcom/android/settings/CommonDialog;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/CommonDialog$DialogClickListener;
    }
.end annotation


# instance fields
.field protected mActivity:Landroid/app/Activity;

.field private mBaseDialogClickListener:Lcom/android/settings/CommonDialog$DialogClickListener;

.field private mClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private mDialog:Lmiuix/appcompat/app/AlertDialog;

.field private mMessage:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;


# direct methods
.method static bridge synthetic -$$Nest$monClick(Lcom/android/settings/CommonDialog;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/CommonDialog;->onClick(Landroid/content/DialogInterface;I)V

    return-void
.end method

.method protected constructor <init>(Landroid/app/Activity;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/CommonDialog;->mActivity:Landroid/app/Activity;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/CommonDialog;-><init>(Landroid/app/Activity;)V

    iput-object p2, p0, Lcom/android/settings/CommonDialog;->mClickListener:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method private onBuild(Lmiuix/appcompat/app/AlertDialog;)V
    .locals 0

    const/4 p0, 0x1

    invoke-virtual {p1, p0}, Lmiuix/appcompat/app/AlertDialog;->setCancelable(Z)V

    return-void
.end method

.method private onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    iget-object p0, p0, Lcom/android/settings/CommonDialog;->mClickListener:Landroid/content/DialogInterface$OnClickListener;

    if-eqz p0, :cond_0

    invoke-interface {p0, p1, p2}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected onPrepareBuild(Lmiuix/appcompat/app/AlertDialog$Builder;)V
    .locals 0

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/CommonDialog;->mTitle:Ljava/lang/String;

    return-void
.end method

.method public show()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/CommonDialog;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/CommonDialog$DialogClickListener;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/android/settings/CommonDialog$DialogClickListener;-><init>(Lcom/android/settings/CommonDialog;ZLcom/android/settings/CommonDialog$DialogClickListener-IA;)V

    iput-object v0, p0, Lcom/android/settings/CommonDialog;->mBaseDialogClickListener:Lcom/android/settings/CommonDialog$DialogClickListener;

    new-instance v0, Lmiuix/appcompat/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/android/settings/CommonDialog;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/android/settings/CommonDialog;->onPrepareBuild(Lmiuix/appcompat/app/AlertDialog$Builder;)V

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/CommonDialog;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-direct {p0, v0}, Lcom/android/settings/CommonDialog;->onBuild(Lmiuix/appcompat/app/AlertDialog;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/CommonDialog;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    iget-object v1, p0, Lcom/android/settings/CommonDialog;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/CommonDialog;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    iget-object v1, p0, Lcom/android/settings/CommonDialog;->mMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object p0, p0, Lcom/android/settings/CommonDialog;->mDialog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {p0}, Landroid/app/Dialog;->show()V

    return-void
.end method
