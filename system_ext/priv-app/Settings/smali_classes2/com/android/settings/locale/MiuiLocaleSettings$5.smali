.class Lcom/android/settings/locale/MiuiLocaleSettings$5;
.super Ljava/lang/Object;

# interfaces
.implements Lmiuix/appcompat/app/AlertDialog$OnDialogShowAnimListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/locale/MiuiLocaleSettings;->showInstallPreInstallAppDialog(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/locale/MiuiLocaleSettings;

.field final synthetic val$selectedLocale:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/settings/locale/MiuiLocaleSettings;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/locale/MiuiLocaleSettings$5;->this$0:Lcom/android/settings/locale/MiuiLocaleSettings;

    iput-object p2, p0, Lcom/android/settings/locale/MiuiLocaleSettings$5;->val$selectedLocale:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onShowAnimComplete()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/locale/MiuiLocaleSettings$5;->val$selectedLocale:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/os/MiuiInit;->initCustEnvironment(Ljava/lang/String;Lmiui/os/IMiuiInitObserver;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.TIMEZONE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x20000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string/jumbo v2, "time-zone"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settings/locale/MiuiLocaleSettings$5;->this$0:Lcom/android/settings/locale/MiuiLocaleSettings;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/locale/MiuiLocaleSettings$5;->this$0:Lcom/android/settings/locale/MiuiLocaleSettings;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    :cond_0
    new-instance v0, Lcom/android/settings/locale/MiuiLocaleSettings$InstallAppTask;

    iget-object p0, p0, Lcom/android/settings/locale/MiuiLocaleSettings$5;->this$0:Lcom/android/settings/locale/MiuiLocaleSettings;

    invoke-direct {v0, p0}, Lcom/android/settings/locale/MiuiLocaleSettings$InstallAppTask;-><init>(Lcom/android/settings/locale/MiuiLocaleSettings;)V

    invoke-static {v0}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_1
    const-string v0, "MiuiLocaleSettings"

    const-string v1, "Fail to call MiuiInit.initCustEnvironment, please retry."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p0, p0, Lcom/android/settings/locale/MiuiLocaleSettings$5;->this$0:Lcom/android/settings/locale/MiuiLocaleSettings;

    invoke-virtual {p0}, Lcom/android/settings/locale/MiuiLocaleSettings;->finish()V

    :goto_0
    return-void
.end method

.method public onShowAnimStart()V
    .locals 0

    return-void
.end method
