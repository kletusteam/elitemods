.class Lcom/android/settings/slices/SlicesIndexer;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHelper:Lcom/android/settings/slices/SlicesDatabaseHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/slices/SlicesIndexer;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/slices/SlicesDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/android/settings/slices/SlicesDatabaseHelper;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/slices/SlicesIndexer;->mHelper:Lcom/android/settings/slices/SlicesDatabaseHelper;

    return-void
.end method


# virtual methods
.method getSliceData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/android/settings/slices/SliceData;",
            ">;"
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {v0}, Lcom/android/settings/overlay/FeatureFactory;->getSlicesFeatureProvider()Lcom/android/settings/slices/SlicesFeatureProvider;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_1
    return-object p0

    :goto_2
    iget-object v0, p0, Lcom/android/settings/slices/SlicesIndexer;->mContext:Landroid/content/Context;

    goto/32 :goto_6

    nop

    :goto_3
    iget-object p0, p0, Lcom/android/settings/slices/SlicesIndexer;->mContext:Landroid/content/Context;

    goto/32 :goto_5

    nop

    :goto_4
    invoke-virtual {p0}, Lcom/android/settings/slices/SliceDataConverter;->getSliceData()Ljava/util/List;

    move-result-object p0

    goto/32 :goto_1

    nop

    :goto_5
    invoke-interface {v0, p0}, Lcom/android/settings/slices/SlicesFeatureProvider;->getSliceDataConverter(Landroid/content/Context;)Lcom/android/settings/slices/SliceDataConverter;

    move-result-object p0

    goto/32 :goto_4

    nop

    :goto_6
    invoke-static {v0}, Lcom/android/settings/overlay/FeatureFactory;->getFactory(Landroid/content/Context;)Lcom/android/settings/overlay/FeatureFactory;

    move-result-object v0

    goto/32 :goto_0

    nop
.end method

.method protected indexSliceData()V
    .locals 6

    iget-object v0, p0, Lcom/android/settings/slices/SlicesIndexer;->mHelper:Lcom/android/settings/slices/SlicesDatabaseHelper;

    invoke-virtual {v0}, Lcom/android/settings/slices/SlicesDatabaseHelper;->isSliceDataIndexed()Z

    move-result v0

    const-string v1, "SlicesIndexer"

    if-eqz v0, :cond_0

    const-string p0, "Slices already indexed - returning."

    invoke-static {v1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/slices/SlicesIndexer;->mHelper:Lcom/android/settings/slices/SlicesDatabaseHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    iget-object v4, p0, Lcom/android/settings/slices/SlicesIndexer;->mHelper:Lcom/android/settings/slices/SlicesDatabaseHelper;

    invoke-virtual {v4, v0}, Lcom/android/settings/slices/SlicesDatabaseHelper;->reconstruct(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-virtual {p0}, Lcom/android/settings/slices/SlicesIndexer;->getSliceData()Ljava/util/List;

    move-result-object v4

    invoke-virtual {p0, v0, v4}, Lcom/android/settings/slices/SlicesIndexer;->insertSliceData(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    iget-object p0, p0, Lcom/android/settings/slices/SlicesIndexer;->mHelper:Lcom/android/settings/slices/SlicesDatabaseHelper;

    invoke-virtual {p0}, Lcom/android/settings/slices/SlicesDatabaseHelper;->setIndexedState()V

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Indexing slices database took: "

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v2

    invoke-virtual {p0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception p0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw p0
.end method

.method insertSliceData(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List<",
            "Lcom/android/settings/slices/SliceData;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_1e

    nop

    :goto_0
    invoke-virtual {p2}, Lcom/android/settings/slices/SliceData;->getScreenTitle()Ljava/lang/CharSequence;

    move-result-object v1

    goto/32 :goto_29

    nop

    :goto_1
    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/32 :goto_31

    nop

    :goto_2
    const-string v1, "highlight_menu"

    goto/32 :goto_1

    nop

    :goto_3
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_36

    nop

    :goto_4
    return-void

    :goto_5
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_2f

    nop

    :goto_6
    invoke-virtual {p2}, Lcom/android/settings/slices/SliceData;->getSliceType()I

    move-result v1

    goto/32 :goto_30

    nop

    :goto_7
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_17

    nop

    :goto_8
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/32 :goto_1c

    nop

    :goto_9
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/32 :goto_37

    nop

    :goto_a
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_0

    nop

    :goto_b
    invoke-virtual {p2}, Lcom/android/settings/slices/SliceData;->getKeywords()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_32

    nop

    :goto_c
    const-string/jumbo v2, "summary"

    goto/32 :goto_a

    nop

    :goto_d
    goto/16 :goto_1f

    :goto_e
    goto/32 :goto_4

    nop

    :goto_f
    invoke-virtual {p2}, Lcom/android/settings/slices/SliceData;->isPublicSlice()Z

    move-result v1

    goto/32 :goto_8

    nop

    :goto_10
    const-string/jumbo v2, "slice_type"

    goto/32 :goto_1d

    nop

    :goto_11
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    goto/32 :goto_2a

    nop

    :goto_12
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_16

    nop

    :goto_13
    invoke-virtual {p2}, Lcom/android/settings/slices/SliceData;->getPreferenceController()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_2c

    nop

    :goto_14
    invoke-virtual {p2}, Lcom/android/settings/slices/SliceData;->getFragmentClassName()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_28

    nop

    :goto_15
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    goto/32 :goto_21

    nop

    :goto_16
    invoke-virtual {p2}, Lcom/android/settings/slices/SliceData;->getIconResource()I

    move-result v1

    goto/32 :goto_3b

    nop

    :goto_17
    invoke-virtual {p2}, Lcom/android/settings/slices/SliceData;->getUri()Landroid/net/Uri;

    move-result-object v1

    goto/32 :goto_24

    nop

    :goto_18
    const-string/jumbo v1, "slices_index"

    goto/32 :goto_3a

    nop

    :goto_19
    const-string v2, "icon"

    goto/32 :goto_34

    nop

    :goto_1a
    if-nez p2, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_11

    nop

    :goto_1b
    const-string v2, "key"

    goto/32 :goto_7

    nop

    :goto_1c
    const-string/jumbo v2, "public_slice"

    goto/32 :goto_9

    nop

    :goto_1d
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/32 :goto_22

    nop

    :goto_1e
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1f
    goto/32 :goto_25

    nop

    :goto_20
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    goto/32 :goto_2

    nop

    :goto_21
    invoke-virtual {p2}, Lcom/android/settings/slices/SliceData;->getKey()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_1b

    nop

    :goto_22
    invoke-virtual {p2}, Lcom/android/settings/slices/SliceData;->getUnavailableSliceSubtitle()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_2b

    nop

    :goto_23
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_f

    nop

    :goto_24
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_3c

    nop

    :goto_25
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    goto/32 :goto_1a

    nop

    :goto_26
    const-string/jumbo v2, "title"

    goto/32 :goto_3

    nop

    :goto_27
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_6

    nop

    :goto_28
    const-string v2, "fragment"

    goto/32 :goto_38

    nop

    :goto_29
    if-nez v1, :cond_1

    goto/32 :goto_2e

    :cond_1
    goto/32 :goto_33

    nop

    :goto_2a
    check-cast p2, Lcom/android/settings/slices/SliceData;

    goto/32 :goto_35

    nop

    :goto_2b
    const-string/jumbo v2, "unavailable_slice_subtitle"

    goto/32 :goto_23

    nop

    :goto_2c
    const-string v2, "controller"

    goto/32 :goto_27

    nop

    :goto_2d
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2e
    goto/32 :goto_b

    nop

    :goto_2f
    invoke-virtual {p2}, Lcom/android/settings/slices/SliceData;->getTitle()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_26

    nop

    :goto_30
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_10

    nop

    :goto_31
    const/4 p2, 0x0

    goto/32 :goto_18

    nop

    :goto_32
    const-string v2, "keywords"

    goto/32 :goto_12

    nop

    :goto_33
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_39

    nop

    :goto_34
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/32 :goto_14

    nop

    :goto_35
    new-instance v0, Landroid/content/ContentValues;

    goto/32 :goto_15

    nop

    :goto_36
    invoke-virtual {p2}, Lcom/android/settings/slices/SliceData;->getSummary()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_c

    nop

    :goto_37
    invoke-virtual {p2}, Lcom/android/settings/slices/SliceData;->getHighlightMenuRes()I

    move-result p2

    goto/32 :goto_20

    nop

    :goto_38
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_13

    nop

    :goto_39
    const-string/jumbo v2, "screentitle"

    goto/32 :goto_2d

    nop

    :goto_3a
    invoke-virtual {p1, v1, p2, v0}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto/32 :goto_d

    nop

    :goto_3b
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_19

    nop

    :goto_3c
    const-string/jumbo v2, "slice_uri"

    goto/32 :goto_5

    nop
.end method

.method public run()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/slices/SlicesIndexer;->indexSliceData()V

    return-void
.end method
