.class public Lcom/android/settings/EdgeModeGuideActivity;
.super Lmiuix/appcompat/app/AppCompatActivity;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/EdgeModeGuideActivity$EdgeModeGuideFragment;
    }
.end annotation


# static fields
.field private static final TITTLE_RES:[I


# instance fields
.field private mEdgeType:I

.field private mFragment:Lcom/android/settings/EdgeModeGuideActivity$EdgeModeGuideFragment;

.field private final mHandler:Landroid/os/Handler;

.field private mVideoView:Landroid/widget/VideoView;


# direct methods
.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/android/settings/EdgeModeGuideActivity;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/EdgeModeGuideActivity;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmVideoView(Lcom/android/settings/EdgeModeGuideActivity;)Landroid/widget/VideoView;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/EdgeModeGuideActivity;->mVideoView:Landroid/widget/VideoView;

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [I

    sget v1, Lcom/android/settings/R$string;->edge_mode_back:I

    const/4 v2, 0x0

    aput v1, v0, v2

    sget v1, Lcom/android/settings/R$string;->edge_mode_clean:I

    const/4 v2, 0x1

    aput v1, v0, v2

    sget v1, Lcom/android/settings/R$string;->edge_mode_photo:I

    const/4 v2, 0x2

    aput v1, v0, v2

    sput-object v0, Lcom/android/settings/EdgeModeGuideActivity;->TITTLE_RES:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiuix/appcompat/app/AppCompatActivity;-><init>()V

    new-instance v0, Lcom/android/settings/EdgeModeGuideActivity$1;

    invoke-direct {v0, p0}, Lcom/android/settings/EdgeModeGuideActivity$1;-><init>(Lcom/android/settings/EdgeModeGuideActivity;)V

    iput-object v0, p0, Lcom/android/settings/EdgeModeGuideActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private updateEdgeModeVideo()V
    .locals 5

    sget v0, Lcom/android/settings/R$id;->video:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/VideoView;

    iput-object v0, p0, Lcom/android/settings/EdgeModeGuideActivity;->mVideoView:Landroid/widget/VideoView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "android.resource://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v3, Lcom/android/settings/R$raw;->photo:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget v3, p0, Lcom/android/settings/EdgeModeGuideActivity;->mEdgeType:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v1, Lcom/android/settings/R$raw;->clean:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    if-nez v3, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v1, Lcom/android/settings/R$raw;->back:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/android/settings/EdgeModeGuideActivity;->mVideoView:Landroid/widget/VideoView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/android/settings/EdgeModeGuideActivity;->mVideoView:Landroid/widget/VideoView;

    new-instance v1, Lcom/android/settings/EdgeModeGuideActivity$3;

    invoke-direct {v1, p0}, Lcom/android/settings/EdgeModeGuideActivity$3;-><init>(Lcom/android/settings/EdgeModeGuideActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lcom/android/settings/EdgeModeGuideActivity;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0, v4}, Landroid/widget/VideoView;->setZOrderOnTop(Z)V

    iget-object p0, p0, Lcom/android/settings/EdgeModeGuideActivity;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {p0}, Landroid/widget/VideoView;->start()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    sget v0, Lcom/android/settings/R$layout;->edge_mode_guide:I

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/AppCompatActivity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "edge_mode_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/settings/EdgeModeGuideActivity;->mEdgeType:I

    :cond_0
    new-instance v0, Lcom/android/settings/EdgeModeGuideActivity$EdgeModeGuideFragment;

    invoke-direct {v0}, Lcom/android/settings/EdgeModeGuideActivity$EdgeModeGuideFragment;-><init>()V

    iput-object v0, p0, Lcom/android/settings/EdgeModeGuideActivity;->mFragment:Lcom/android/settings/EdgeModeGuideActivity$EdgeModeGuideFragment;

    iget v1, p0, Lcom/android/settings/EdgeModeGuideActivity;->mEdgeType:I

    invoke-virtual {v0, v1}, Lcom/android/settings/EdgeModeGuideActivity$EdgeModeGuideFragment;->setEdgeType(I)V

    if-nez p1, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    sget v0, Lcom/android/settings/R$id;->preference_container:I

    iget-object v1, p0, Lcom/android/settings/EdgeModeGuideActivity;->mFragment:Lcom/android/settings/EdgeModeGuideActivity$EdgeModeGuideFragment;

    invoke-virtual {p1, v0, v1}, Landroidx/fragment/app/FragmentTransaction;->add(ILandroidx/fragment/app/Fragment;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget-object v0, Lcom/android/settings/EdgeModeGuideActivity;->TITTLE_RES:[I

    iget v1, p0, Lcom/android/settings/EdgeModeGuideActivity;->mEdgeType:I

    aget v0, v0, v1

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    sget v0, Lcom/android/settings/R$id;->title:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v0, Lcom/android/settings/R$id;->action_bar_back:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    new-instance p1, Lcom/android/settings/EdgeModeGuideActivity$2;

    invoke-direct {p1, p0}, Lcom/android/settings/EdgeModeGuideActivity$2;-><init>(Lcom/android/settings/EdgeModeGuideActivity;)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/EdgeModeGuideActivity;->updateEdgeModeVideo()V

    return-void
.end method
