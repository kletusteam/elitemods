.class public interface abstract Lcom/android/settings/aware/AwareFeatureProvider;
.super Ljava/lang/Object;


# virtual methods
.method public abstract isEnabled(Landroid/content/Context;)Z
.end method

.method public abstract isSupported(Landroid/content/Context;)Z
.end method

.method public abstract showRestrictionDialog(Landroidx/fragment/app/Fragment;)V
.end method
