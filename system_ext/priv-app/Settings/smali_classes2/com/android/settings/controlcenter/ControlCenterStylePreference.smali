.class public Lcom/android/settings/controlcenter/ControlCenterStylePreference;
.super Lcom/android/settings/view/VisualCheckBoxPreference;


# static fields
.field private static final RES_LEGACY_BG:I

.field private static final RES_LEGACY_BG_INTER:I

.field private static final RES_MODERN:I

.field private static final RES_MODERN_BG:I

.field private static final RES_MODERN_BG_INTER:I

.field private static final RES_MODERN_INTER:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget v0, Lcom/android/settings/R$drawable;->bg_control_center_legacy:I

    sput v0, Lcom/android/settings/controlcenter/ControlCenterStylePreference;->RES_LEGACY_BG:I

    sget v0, Lcom/android/settings/R$drawable;->bg_control_center_modern:I

    sput v0, Lcom/android/settings/controlcenter/ControlCenterStylePreference;->RES_MODERN_BG:I

    sget v0, Lcom/android/settings/R$drawable;->bg_control_center_legacy_international:I

    sput v0, Lcom/android/settings/controlcenter/ControlCenterStylePreference;->RES_LEGACY_BG_INTER:I

    sget v0, Lcom/android/settings/R$drawable;->bg_control_center_modern_international:I

    sput v0, Lcom/android/settings/controlcenter/ControlCenterStylePreference;->RES_MODERN_BG_INTER:I

    sget v0, Lcom/android/settings/R$raw;->control_center_guide_modern:I

    sput v0, Lcom/android/settings/controlcenter/ControlCenterStylePreference;->RES_MODERN:I

    sget v0, Lcom/android/settings/R$raw;->control_center_guide_modern_international:I

    sput v0, Lcom/android/settings/controlcenter/ControlCenterStylePreference;->RES_MODERN_INTER:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/view/VisualCheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/view/VisualCheckBoxPreference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V

    iget-object p0, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const/4 p1, 0x2

    invoke-virtual {p0, p1}, Landroid/view/View;->setImportantForAccessibility(I)V

    return-void
.end method

.method protected onCreateVisualContent(Landroid/view/View;Landroid/view/View;)V
    .locals 2

    if-eqz p1, :cond_2

    sget v0, Lcom/android/settings/R$id;->video_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/android/settings/view/CornerVideoView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/VideoView;->setAudioFocusRequest(I)V

    sget-boolean v0, Lmiuix/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    sget v1, Lcom/android/settings/controlcenter/ControlCenterStylePreference;->RES_MODERN_INTER:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/android/settings/controlcenter/ControlCenterStylePreference;->RES_MODERN:I

    :goto_0
    if-eqz v0, :cond_1

    sget v0, Lcom/android/settings/controlcenter/ControlCenterStylePreference;->RES_MODERN_BG_INTER:I

    goto :goto_1

    :cond_1
    sget v0, Lcom/android/settings/controlcenter/ControlCenterStylePreference;->RES_LEGACY_BG:I

    :goto_1
    invoke-virtual {p1, v1, v0}, Lcom/android/settings/view/CornerVideoView;->play(II)V

    :cond_2
    if-eqz p2, :cond_4

    sget p1, Lcom/android/settings/R$id;->image_view:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object p0

    sget-boolean p2, Lmiuix/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz p2, :cond_3

    sget p2, Lcom/android/settings/controlcenter/ControlCenterStylePreference;->RES_LEGACY_BG_INTER:I

    goto :goto_2

    :cond_3
    sget p2, Lcom/android/settings/controlcenter/ControlCenterStylePreference;->RES_LEGACY_BG:I

    :goto_2
    invoke-virtual {p0, p2}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_4
    return-void
.end method
