.class public Lcom/android/settings/SettingsFragment$TipsListener;
.super Landroid/os/Handler;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/SettingsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TipsListener"
.end annotation


# instance fields
.field private final mRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/android/settings/SettingsFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/android/settings/SettingsFragment;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/SettingsFragment$TipsListener;->mRef:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget-object p1, p0, Lcom/android/settings/SettingsFragment$TipsListener;->mRef:Ljava/lang/ref/WeakReference;

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Lcom/android/settings/SettingsFragment;

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/android/settings/SettingsFragment;->getTipsLocalModel()Lcom/android/settings/utils/TipsUtils$TipsLocalModel;

    move-result-object p1

    if-nez p1, :cond_2

    return-void

    :cond_2
    invoke-virtual {p1}, Lcom/android/settings/utils/TipsUtils$TipsLocalModel;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "show"

    invoke-virtual {p1, v1, v2}, Lcom/android/settings/utils/TipsUtils$TipsLocalModel;->update(Landroid/content/Context;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1}, Lcom/android/settings/utils/TipsUtils$TipsLocalModel;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    iget-object v2, p1, Lcom/android/settings/utils/TipsUtils$TipsLocalModel;->TEXT:Ljava/lang/String;

    iget v3, p1, Lcom/android/settings/utils/TipsUtils$TipsLocalModel;->TEXT_COLOR:I

    iget v4, p1, Lcom/android/settings/utils/TipsUtils$TipsLocalModel;->TEXT_BACKGROUND:I

    iget v5, p1, Lcom/android/settings/utils/TipsUtils$TipsLocalModel;->ARROW_ICON:I

    move-object v6, p0

    invoke-static/range {v0 .. v6}, Lcom/android/settings/SettingsFragment;->-$$Nest$mupdateTips(Lcom/android/settings/SettingsFragment;ZLjava/lang/String;IIILandroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    iget-object p0, p0, Lcom/android/settings/SettingsFragment$TipsListener;->mRef:Ljava/lang/ref/WeakReference;

    if-nez p0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p0

    move-object v0, p0

    check-cast v0, Lcom/android/settings/SettingsFragment;

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/android/settings/SettingsFragment;->getTipsLocalModel()Lcom/android/settings/utils/TipsUtils$TipsLocalModel;

    move-result-object p0

    if-nez p0, :cond_2

    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/android/settings/utils/TipsUtils$TipsLocalModel;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/android/settings/BasePreferenceFragment;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/high16 v2, 0x10000

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->isFoldDevice()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->isSplitTabletDevice()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    const/16 v1, 0x10

    invoke-virtual {p1, v1}, Landroid/content/Intent;->addMiuiFlags(I)Landroid/content/Intent;

    :cond_4
    :try_start_0
    invoke-virtual {v0, p1}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "startActivity error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SettingsFragment"

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_5
    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/utils/TipsUtils$TipsLocalModel;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_6

    return-void

    :cond_6
    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v1, "click"

    invoke-virtual {p0, p1, v1}, Lcom/android/settings/utils/TipsUtils$TipsLocalModel;->update(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/settings/utils/TipsUtils$TipsLocalModel;->TEXT:Ljava/lang/String;

    iget v3, p0, Lcom/android/settings/utils/TipsUtils$TipsLocalModel;->TEXT_COLOR:I

    iget v4, p0, Lcom/android/settings/utils/TipsUtils$TipsLocalModel;->TEXT_BACKGROUND:I

    iget v5, p0, Lcom/android/settings/utils/TipsUtils$TipsLocalModel;->ARROW_ICON:I

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/android/settings/SettingsFragment;->-$$Nest$mupdateTips(Lcom/android/settings/SettingsFragment;ZLjava/lang/String;IIILandroid/view/View$OnClickListener;)V

    return-void
.end method
