.class public Lcom/android/settings/KeySettingsPreviewPreference;
.super Lcom/android/settingslib/miuisettings/preference/Preference;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;
    }
.end annotation


# instance fields
.field private final animations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/FramesSequenceAnimation;",
            ">;"
        }
    .end annotation
.end field

.field private mAction:Ljava/lang/String;

.field private mActionRelativeLayout:Landroid/widget/RelativeLayout;

.field private mBackGroundDrawable:Landroid/widget/ImageView;

.field private mBottomDrawable:Landroid/widget/ImageView;

.field private mContext:Landroid/content/Context;

.field private mDoublePowerDrawable:Landroid/widget/ImageView;

.field private mLongPressBottomDrawable:Landroid/widget/ImageView;

.field private mPowerDrawable:Landroid/widget/ImageView;

.field private mPreferenceKey:Ljava/lang/String;

.field private mThreeGestureDrawable:Landroid/widget/ImageView;

.field private mThreeLongPressDrawable:Landroid/widget/ImageView;


# direct methods
.method static bridge synthetic -$$Nest$mchangeBackground(Lcom/android/settings/KeySettingsPreviewPreference;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/KeySettingsPreviewPreference;->changeBackground()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mresetBackground(Lcom/android/settings/KeySettingsPreviewPreference;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/KeySettingsPreviewPreference;->resetBackground()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settingslib/miuisettings/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/android/settings/KeySettingsPreviewPreference;->animations:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$xml;->key_settings_preview:I

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setLayoutResource(I)V

    return-void
.end method

.method private addPreviewAnimationView(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/KeySettingsPreviewPreference;->addPreviewAnimationView(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;I)V

    return-void
.end method

.method private addPreviewAnimationView(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;I)V
    .locals 3

    invoke-static {p2}, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->-$$Nest$fgetmImgViewId(Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    new-instance v0, Lcom/android/settings/FramesSequenceAnimation;

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-static {p2}, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->-$$Nest$fgetmAnimArrayId(Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;)I

    move-result p2

    const/16 v2, 0xa

    invoke-direct {v0, v1, p1, p2, v2}, Lcom/android/settings/FramesSequenceAnimation;-><init>(Landroid/content/Context;Landroid/widget/ImageView;II)V

    if-eqz p3, :cond_0

    invoke-virtual {p1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p2

    check-cast p2, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p3

    float-to-int p3, p3

    iput p3, p2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    new-instance p1, Lcom/android/settings/KeySettingsPreviewPreference$1;

    invoke-direct {p1, p0}, Lcom/android/settings/KeySettingsPreviewPreference$1;-><init>(Lcom/android/settings/KeySettingsPreviewPreference;)V

    invoke-virtual {v0, p1}, Lcom/android/settings/FramesSequenceAnimation;->setAnimationListener(Lcom/android/settings/FramesSequenceAnimation$AnimationListener;)V

    iget-object p0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->animations:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private changeBackground()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mPreferenceKey:Ljava/lang/String;

    const-string v1, "launch_camera"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mActionRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object p0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v1, Lcom/android/settings/R$drawable;->keysettings_camera:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mPreferenceKey:Ljava/lang/String;

    const-string/jumbo v1, "screen_shot"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mActionRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object p0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v1, Lcom/android/settings/R$drawable;->keysettings_screen_shot:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mPreferenceKey:Ljava/lang/String;

    const-string/jumbo v1, "partial_screen_shot"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mActionRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object p0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v1, Lcom/android/settings/R$drawable;->keysettings_partial_screen_shot:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mPreferenceKey:Ljava/lang/String;

    const-string v1, "launch_voice_assistant"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mActionRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object p0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v1, Lcom/android/settings/R$drawable;->keysettings_voice_assistant:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mPreferenceKey:Ljava/lang/String;

    const-string v1, "launch_google_search"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mActionRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object p0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v1, Lcom/android/settings/R$drawable;->keysettings_voice_assistant:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mPreferenceKey:Ljava/lang/String;

    const-string v1, "go_to_sleep"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object p0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mActionRelativeLayout:Landroid/widget/RelativeLayout;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mPreferenceKey:Ljava/lang/String;

    const-string/jumbo v1, "turn_on_torch"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mActionRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object p0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v1, Lcom/android/settings/R$drawable;->keysettings_turn_on_torch:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mPreferenceKey:Ljava/lang/String;

    const-string v1, "close_app"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mActionRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object p0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v1, Lcom/android/settings/R$drawable;->keysettings_launcher:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mPreferenceKey:Ljava/lang/String;

    const-string/jumbo v1, "split_screen"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mActionRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object p0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v1, Lcom/android/settings/R$drawable;->keysettings_split_screen:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_8
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mPreferenceKey:Ljava/lang/String;

    const-string/jumbo v1, "mi_pay"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mActionRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object p0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v1, Lcom/android/settings/R$drawable;->keysettings_mipay:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_9
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mPreferenceKey:Ljava/lang/String;

    const-string/jumbo v1, "show_menu"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mActionRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object p0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v1, Lcom/android/settings/R$drawable;->keysettings_show_menu:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_a
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mPreferenceKey:Ljava/lang/String;

    const-string v1, "launch_recents"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mActionRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object p0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v1, Lcom/android/settings/R$drawable;->keysettings_launch_recents:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_b
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mActionRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object p0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v1, Lcom/android/settings/R$drawable;->keysettings_launcher:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void
.end method

.method private processPreviewAnimation(Landroid/view/View;)V
    .locals 4

    invoke-direct {p0}, Lcom/android/settings/KeySettingsPreviewPreference;->resetAnimationEnv()V

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mAction:Ljava/lang/String;

    const-string v1, "long_press_power_key"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/android/settings/KeySettingsPreviewPreference;->setBackgroundDrawable(Z)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->LONG_PRESS_POWER:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    invoke-direct {p0, p1, v0}, Lcom/android/settings/KeySettingsPreviewPreference;->addPreviewAnimationView(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;)V

    goto/16 :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mAction:Ljava/lang/String;

    const-string v2, "double_click_power_key"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/android/settings/KeySettingsPreviewPreference;->setBackgroundDrawable(Z)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->DOUBLE_CLICK_POWER:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    invoke-direct {p0, p1, v0}, Lcom/android/settings/KeySettingsPreviewPreference;->addPreviewAnimationView(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;)V

    goto/16 :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mAction:Ljava/lang/String;

    const-string v2, "long_press_menu_key"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    invoke-direct {p0, v2}, Lcom/android/settings/KeySettingsPreviewPreference;->setBackgroundDrawable(Z)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->LONG_PRESS:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    sget v1, Lcom/android/settings/R$dimen;->key_settings_click_menu_marginLeft:I

    invoke-direct {p0, p1, v0, v1}, Lcom/android/settings/KeySettingsPreviewPreference;->addPreviewAnimationView(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;I)V

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mAction:Ljava/lang/String;

    const-string v3, "long_press_menu_key_when_lock"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, v2}, Lcom/android/settings/KeySettingsPreviewPreference;->setBackgroundDrawable(Z)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->LONG_PRESS:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    sget v1, Lcom/android/settings/R$dimen;->key_settings_click_menu_marginLeft:I

    invoke-direct {p0, p1, v0, v1}, Lcom/android/settings/KeySettingsPreviewPreference;->addPreviewAnimationView(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;I)V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mAction:Ljava/lang/String;

    const-string v3, "long_press_home_key"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0, v2}, Lcom/android/settings/KeySettingsPreviewPreference;->setBackgroundDrawable(Z)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->LONG_PRESS:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    sget v1, Lcom/android/settings/R$dimen;->key_settings_click_home_marginLeft:I

    invoke-direct {p0, p1, v0, v1}, Lcom/android/settings/KeySettingsPreviewPreference;->addPreviewAnimationView(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;I)V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mAction:Ljava/lang/String;

    const-string v3, "long_press_back_key"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0, v2}, Lcom/android/settings/KeySettingsPreviewPreference;->setBackgroundDrawable(Z)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->LONG_PRESS:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    sget v1, Lcom/android/settings/R$dimen;->key_settings_click_back_marginLeft:I

    invoke-direct {p0, p1, v0, v1}, Lcom/android/settings/KeySettingsPreviewPreference;->addPreviewAnimationView(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;I)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mAction:Ljava/lang/String;

    const-string v3, "key_combination_power_back"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/android/settings/KeySettingsPreviewPreference;->setBackgroundDrawable(Z)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->POWER_CLICK:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    invoke-direct {p0, p1, v0}, Lcom/android/settings/KeySettingsPreviewPreference;->addPreviewAnimationView(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->CLICK_BOTTOM:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    sget v1, Lcom/android/settings/R$dimen;->key_settings_click_back_marginLeft:I

    invoke-direct {p0, p1, v0, v1}, Lcom/android/settings/KeySettingsPreviewPreference;->addPreviewAnimationView(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;I)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mAction:Ljava/lang/String;

    const-string v3, "key_combination_power_home"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/android/settings/KeySettingsPreviewPreference;->setBackgroundDrawable(Z)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->POWER_CLICK:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    invoke-direct {p0, p1, v0}, Lcom/android/settings/KeySettingsPreviewPreference;->addPreviewAnimationView(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->CLICK_BOTTOM:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    sget v1, Lcom/android/settings/R$dimen;->key_settings_click_home_marginLeft:I

    invoke-direct {p0, p1, v0, v1}, Lcom/android/settings/KeySettingsPreviewPreference;->addPreviewAnimationView(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;I)V

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mAction:Ljava/lang/String;

    const-string v3, "key_combination_power_menu"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-direct {p0, v1}, Lcom/android/settings/KeySettingsPreviewPreference;->setBackgroundDrawable(Z)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->POWER_CLICK:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    invoke-direct {p0, p1, v0}, Lcom/android/settings/KeySettingsPreviewPreference;->addPreviewAnimationView(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->CLICK_BOTTOM:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    sget v1, Lcom/android/settings/R$dimen;->key_settings_click_menu_marginLeft:I

    invoke-direct {p0, p1, v0, v1}, Lcom/android/settings/KeySettingsPreviewPreference;->addPreviewAnimationView(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;I)V

    goto :goto_0

    :cond_8
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mAction:Ljava/lang/String;

    const-string/jumbo v1, "three_gesture_down"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-direct {p0, v2}, Lcom/android/settings/KeySettingsPreviewPreference;->setBackgroundDrawable(Z)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->THREE_DROP:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    invoke-direct {p0, p1, v0}, Lcom/android/settings/KeySettingsPreviewPreference;->addPreviewAnimationView(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;)V

    goto :goto_0

    :cond_9
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mAction:Ljava/lang/String;

    const-string/jumbo v1, "three_gesture_long_press"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-direct {p0, v2}, Lcom/android/settings/KeySettingsPreviewPreference;->setBackgroundDrawable(Z)V

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->THREE_LONG_PRESS:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    invoke-direct {p0, p1, v0}, Lcom/android/settings/KeySettingsPreviewPreference;->addPreviewAnimationView(Landroid/view/View;Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;)V

    goto :goto_0

    :cond_a
    iget-object p1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mAction:Ljava/lang/String;

    const-string v0, "key_none"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_b

    invoke-direct {p0, v2}, Lcom/android/settings/KeySettingsPreviewPreference;->setBackgroundDrawable(Z)V

    iget-object p1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mActionRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/settings/R$drawable;->keysettings_launcher:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_b
    iget-object p1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mAction:Ljava/lang/String;

    const-string v0, "launch_recents"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_c

    invoke-direct {p0, v2}, Lcom/android/settings/KeySettingsPreviewPreference;->setBackgroundDrawable(Z)V

    iget-object p1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mActionRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/settings/R$drawable;->keysettings_launcher:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v0, "size = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->animations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "animationanimation"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->animations:Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_c

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/android/settings/FramesSequenceAnimation;

    invoke-virtual {p1}, Lcom/android/settings/FramesSequenceAnimation;->start()V

    goto :goto_1

    :cond_c
    return-void
.end method

.method private resetAnimationEnv()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mBackGroundDrawable:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mPowerDrawable:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mDoublePowerDrawable:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mBottomDrawable:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mLongPressBottomDrawable:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mThreeGestureDrawable:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mThreeLongPressDrawable:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->animations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->animations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/FramesSequenceAnimation;

    invoke-virtual {v1}, Lcom/android/settings/FramesSequenceAnimation;->stop()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->animations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/KeySettingsPreviewPreference;->resetBackground()V

    return-void
.end method

.method private resetBackground()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mPreferenceKey:Ljava/lang/String;

    const-string v1, "close_app"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mPreferenceKey:Ljava/lang/String;

    const-string/jumbo v1, "show_menu"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mPreferenceKey:Ljava/lang/String;

    const-string/jumbo v1, "split_screen"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mActionRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object p0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v1, Lcom/android/settings/R$drawable;->keysettings_show_menu:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mActionRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object p0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v1, Lcom/android/settings/R$drawable;->keysettings_launcher:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mActionRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object p0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v1, Lcom/android/settings/R$drawable;->keysettings_camera:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_1
    return-void
.end method

.method private setBackgroundDrawable(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mBackGroundDrawable:Landroid/widget/ImageView;

    iget-object p0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    if-eqz p1, :cond_0

    sget p1, Lcom/android/settings/R$drawable;->keysettings_common_power:I

    goto :goto_0

    :cond_0
    sget p1, Lcom/android/settings/R$drawable;->keysettings_common_no_power:I

    :goto_0
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mActionRelativeLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/KeySettingsPreviewPreference;->resetAnimationEnv()V

    :cond_0
    return-void
.end method

.method public onBindView(Landroid/view/View;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settingslib/miuisettings/preference/Preference;->onBindView(Landroid/view/View;)V

    sget v0, Lcom/android/settings/R$id;->key_settings_preview_action:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mActionRelativeLayout:Landroid/widget/RelativeLayout;

    sget v0, Lcom/android/settings/R$id;->key_settings_preview_background:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mBackGroundDrawable:Landroid/widget/ImageView;

    sget v0, Lcom/android/settings/R$id;->key_power_click:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mPowerDrawable:Landroid/widget/ImageView;

    sget v0, Lcom/android/settings/R$id;->key_power_double_click:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mDoublePowerDrawable:Landroid/widget/ImageView;

    sget v0, Lcom/android/settings/R$id;->key_click_bottom:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mBottomDrawable:Landroid/widget/ImageView;

    sget v0, Lcom/android/settings/R$id;->key_long_press_bottom:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mLongPressBottomDrawable:Landroid/widget/ImageView;

    sget v0, Lcom/android/settings/R$id;->key_three_gesture:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mThreeGestureDrawable:Landroid/widget/ImageView;

    sget v0, Lcom/android/settings/R$id;->key_three_long_press:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mThreeLongPressDrawable:Landroid/widget/ImageView;

    invoke-direct {p0, p1}, Lcom/android/settings/KeySettingsPreviewPreference;->processPreviewAnimation(Landroid/view/View;)V

    return-void
.end method

.method public setCheckedAction(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mAction:Ljava/lang/String;

    invoke-virtual {p0}, Landroidx/preference/Preference;->notifyChanged()V

    return-void
.end method

.method public setPreferenceKey(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/KeySettingsPreviewPreference;->mPreferenceKey:Ljava/lang/String;

    return-void
.end method
