.class public Lcom/android/settings/network/telephony/NetworkScanHelper;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/network/telephony/NetworkScanHelper$NetworkScanCallbackImpl;,
        Lcom/android/settings/network/telephony/NetworkScanHelper$NetworkScanCallback;
    }
.end annotation


# static fields
.field static final INCREMENTAL_RESULTS:Z = true

.field static final INCREMENTAL_RESULTS_PERIODICITY_SEC:I = 0x3

.field static final MAX_SEARCH_TIME_SEC:I = 0xfe

.field static final SEARCH_PERIODICITY_SEC:I = 0x5


# instance fields
.field private filter:Landroid/content/IntentFilter;

.field private mContext:Landroid/content/Context;

.field private final mExecutor:Ljava/util/concurrent/Executor;

.field private final mInternalNetworkScanCallback:Landroid/telephony/TelephonyScanManager$NetworkScanCallback;

.field private final mLegacyIncrScanReceiver:Lcom/android/settings/network/telephony/LegacyIncrementalScanBroadcastReceiver;

.field private final mNetworkScanCallback:Lcom/android/settings/network/telephony/NetworkScanHelper$NetworkScanCallback;

.field private mNetworkScanRequester:Landroid/telephony/NetworkScan;

.field private final mTelephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method public static synthetic $r8$lambda$eqrp2HdYHaktdJkJqYcgLcIGafc(I)Z
    .locals 0

    invoke-static {p0}, Lcom/android/settings/network/telephony/NetworkScanHelper;->lambda$hasNrSaCapability$0(I)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$monComplete(Lcom/android/settings/network/telephony/NetworkScanHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/network/telephony/NetworkScanHelper;->onComplete()V

    return-void
.end method

.method static bridge synthetic -$$Nest$monError(Lcom/android/settings/network/telephony/NetworkScanHelper;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/network/telephony/NetworkScanHelper;->onError(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$monResults(Lcom/android/settings/network/telephony/NetworkScanHelper;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/network/telephony/NetworkScanHelper;->onResults(Ljava/util/List;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/telephony/TelephonyManager;Lcom/android/settings/network/telephony/NetworkScanHelper$NetworkScanCallback;Ljava/util/concurrent/Executor;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "qualcomm.intent.action.ACTION_INCREMENTAL_NW_SCAN_IND"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->filter:Landroid/content/IntentFilter;

    iput-object p1, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iput-object p3, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->mNetworkScanCallback:Lcom/android/settings/network/telephony/NetworkScanHelper$NetworkScanCallback;

    new-instance p1, Lcom/android/settings/network/telephony/NetworkScanHelper$NetworkScanCallbackImpl;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lcom/android/settings/network/telephony/NetworkScanHelper$NetworkScanCallbackImpl;-><init>(Lcom/android/settings/network/telephony/NetworkScanHelper;Lcom/android/settings/network/telephony/NetworkScanHelper$NetworkScanCallbackImpl-IA;)V

    iput-object p1, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->mInternalNetworkScanCallback:Landroid/telephony/TelephonyScanManager$NetworkScanCallback;

    iput-object p4, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->mExecutor:Ljava/util/concurrent/Executor;

    new-instance p2, Lcom/android/settings/network/telephony/LegacyIncrementalScanBroadcastReceiver;

    iget-object p3, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->mContext:Landroid/content/Context;

    invoke-direct {p2, p3, p1}, Lcom/android/settings/network/telephony/LegacyIncrementalScanBroadcastReceiver;-><init>(Landroid/content/Context;Landroid/telephony/TelephonyScanManager$NetworkScanCallback;)V

    iput-object p2, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->mLegacyIncrScanReceiver:Lcom/android/settings/network/telephony/LegacyIncrementalScanBroadcastReceiver;

    return-void
.end method

.method private hasNrSaCapability()Z
    .locals 1

    iget-object p0, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getPhoneCapability()Landroid/telephony/PhoneCapability;

    move-result-object p0

    invoke-virtual {p0}, Landroid/telephony/PhoneCapability;->getDeviceNrCapabilities()[I

    move-result-object p0

    invoke-static {p0}, Ljava/util/Arrays;->stream([I)Ljava/util/stream/IntStream;

    move-result-object p0

    new-instance v0, Lcom/android/settings/network/telephony/NetworkScanHelper$$ExternalSyntheticLambda0;

    invoke-direct {v0}, Lcom/android/settings/network/telephony/NetworkScanHelper$$ExternalSyntheticLambda0;-><init>()V

    invoke-interface {p0, v0}, Ljava/util/stream/IntStream;->anyMatch(Ljava/util/function/IntPredicate;)Z

    move-result p0

    return p0
.end method

.method private static synthetic lambda$hasNrSaCapability$0(I)Z
    .locals 1

    const/4 v0, 0x2

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private onComplete()V
    .locals 0

    iget-object p0, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->mNetworkScanCallback:Lcom/android/settings/network/telephony/NetworkScanHelper$NetworkScanCallback;

    invoke-interface {p0}, Lcom/android/settings/network/telephony/NetworkScanHelper$NetworkScanCallback;->onComplete()V

    return-void
.end method

.method private onError(I)V
    .locals 0

    iget-object p0, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->mNetworkScanCallback:Lcom/android/settings/network/telephony/NetworkScanHelper$NetworkScanCallback;

    invoke-interface {p0, p1}, Lcom/android/settings/network/telephony/NetworkScanHelper$NetworkScanCallback;->onError(I)V

    return-void
.end method

.method private onResults(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/telephony/CellInfo;",
            ">;)V"
        }
    .end annotation

    iget-object p0, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->mNetworkScanCallback:Lcom/android/settings/network/telephony/NetworkScanHelper$NetworkScanCallback;

    invoke-interface {p0, p1}, Lcom/android/settings/network/telephony/NetworkScanHelper$NetworkScanCallback;->onResults(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method createNetworkScanForPreferredAccessNetworks()Landroid/telephony/NetworkScanRequest;
    .locals 11

    goto/32 :goto_3c

    nop

    :goto_0
    const/4 v8, 0x1

    goto/32 :goto_42

    nop

    :goto_1
    and-long/2addr v7, v0

    goto/32 :goto_d

    nop

    :goto_2
    const/16 v7, 0xfe

    goto/32 :goto_0

    nop

    :goto_3
    and-long/2addr v7, v0

    goto/32 :goto_48

    nop

    :goto_4
    return-object p0

    :goto_5
    invoke-direct {v7, v8, v6, v6}, Landroid/telephony/RadioAccessSpecifier;-><init>(I[I[I)V

    goto/32 :goto_34

    nop

    :goto_6
    invoke-direct {p0}, Lcom/android/settings/network/telephony/NetworkScanHelper;->hasNrSaCapability()Z

    move-result p0

    goto/32 :goto_28

    nop

    :goto_7
    and-long/2addr v7, v0

    goto/32 :goto_23

    nop

    :goto_8
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_a

    nop

    :goto_9
    const-wide/32 v7, 0x61000

    goto/32 :goto_1

    nop

    :goto_a
    const-wide/16 v3, 0x0

    goto/32 :goto_13

    nop

    :goto_b
    new-instance v7, Landroid/telephony/RadioAccessSpecifier;

    goto/32 :goto_e

    nop

    :goto_c
    new-instance p0, Landroid/telephony/RadioAccessSpecifier;

    goto/32 :goto_20

    nop

    :goto_d
    cmp-long v7, v7, v3

    goto/32 :goto_17

    nop

    :goto_e
    const/4 v8, 0x1

    goto/32 :goto_31

    nop

    :goto_f
    invoke-direct {p0, v0, v6, v6}, Landroid/telephony/RadioAccessSpecifier;-><init>(I[I[I)V

    goto/32 :goto_37

    nop

    :goto_10
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    goto/32 :goto_44

    nop

    :goto_11
    const-string/jumbo v0, "radioAccessSpecifiers add NGRAN."

    goto/32 :goto_24

    nop

    :goto_12
    const-string p0, "NetworkScanHelper"

    goto/32 :goto_11

    nop

    :goto_13
    cmp-long v5, v0, v3

    goto/32 :goto_3b

    nop

    :goto_14
    const-wide/32 v7, 0x80000

    goto/32 :goto_3a

    nop

    :goto_15
    move-object v3, p0

    goto/32 :goto_22

    nop

    :goto_16
    const/4 v10, 0x0

    goto/32 :goto_15

    nop

    :goto_17
    if-nez v7, :cond_0

    goto/32 :goto_39

    :cond_0
    :goto_18
    goto/32 :goto_33

    nop

    :goto_19
    new-instance v2, Ljava/util/ArrayList;

    goto/32 :goto_8

    nop

    :goto_1a
    const/4 v4, 0x0

    goto/32 :goto_10

    nop

    :goto_1b
    if-nez v7, :cond_1

    goto/32 :goto_35

    :cond_1
    :goto_1c
    goto/32 :goto_30

    nop

    :goto_1d
    if-nez v5, :cond_2

    goto/32 :goto_18

    :cond_2
    goto/32 :goto_9

    nop

    :goto_1e
    and-long/2addr v0, v2

    goto/32 :goto_19

    nop

    :goto_1f
    move-object v5, v0

    goto/32 :goto_43

    nop

    :goto_20
    const/4 v0, 0x6

    goto/32 :goto_f

    nop

    :goto_21
    if-nez v5, :cond_3

    goto/32 :goto_27

    :cond_3
    goto/32 :goto_6

    nop

    :goto_22
    invoke-direct/range {v3 .. v10}, Landroid/telephony/NetworkScanRequest;-><init>(I[Landroid/telephony/RadioAccessSpecifier;IIZILjava/util/ArrayList;)V

    goto/32 :goto_4

    nop

    :goto_23
    cmp-long v7, v7, v3

    goto/32 :goto_40

    nop

    :goto_24
    invoke-static {p0, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_25
    goto/32 :goto_45

    nop

    :goto_26
    if-nez p0, :cond_4

    goto/32 :goto_25

    :cond_4
    :goto_27
    goto/32 :goto_c

    nop

    :goto_28
    if-nez p0, :cond_5

    goto/32 :goto_25

    :cond_5
    goto/32 :goto_14

    nop

    :goto_29
    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2a
    goto/32 :goto_2e

    nop

    :goto_2b
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPreferredNetworkTypeBitmask()J

    move-result-wide v0

    goto/32 :goto_3e

    nop

    :goto_2c
    const/4 v6, 0x5

    goto/32 :goto_2

    nop

    :goto_2d
    const/4 v8, 0x3

    goto/32 :goto_46

    nop

    :goto_2e
    if-nez v5, :cond_6

    goto/32 :goto_1c

    :cond_6
    goto/32 :goto_47

    nop

    :goto_2f
    const-wide/32 v7, 0x804b

    goto/32 :goto_7

    nop

    :goto_30
    new-instance v7, Landroid/telephony/RadioAccessSpecifier;

    goto/32 :goto_3f

    nop

    :goto_31
    invoke-direct {v7, v8, v6, v6}, Landroid/telephony/RadioAccessSpecifier;-><init>(I[I[I)V

    goto/32 :goto_29

    nop

    :goto_32
    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_1f

    nop

    :goto_33
    new-instance v7, Landroid/telephony/RadioAccessSpecifier;

    goto/32 :goto_2d

    nop

    :goto_34
    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_35
    goto/32 :goto_1d

    nop

    :goto_36
    cmp-long p0, v0, v3

    goto/32 :goto_26

    nop

    :goto_37
    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_12

    nop

    :goto_38
    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_39
    goto/32 :goto_21

    nop

    :goto_3a
    and-long/2addr v0, v7

    goto/32 :goto_36

    nop

    :goto_3b
    const/4 v6, 0x0

    goto/32 :goto_3d

    nop

    :goto_3c
    iget-object v0, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    goto/32 :goto_2b

    nop

    :goto_3d
    if-nez v5, :cond_7

    goto/32 :goto_41

    :cond_7
    goto/32 :goto_2f

    nop

    :goto_3e
    const-wide/32 v2, 0xdd387

    goto/32 :goto_1e

    nop

    :goto_3f
    const/4 v8, 0x2

    goto/32 :goto_5

    nop

    :goto_40
    if-nez v7, :cond_8

    goto/32 :goto_2a

    :cond_8
    :goto_41
    goto/32 :goto_b

    nop

    :goto_42
    const/4 v9, 0x3

    goto/32 :goto_16

    nop

    :goto_43
    check-cast v5, [Landroid/telephony/RadioAccessSpecifier;

    goto/32 :goto_2c

    nop

    :goto_44
    new-array v0, v0, [Landroid/telephony/RadioAccessSpecifier;

    goto/32 :goto_32

    nop

    :goto_45
    new-instance p0, Landroid/telephony/NetworkScanRequest;

    goto/32 :goto_1a

    nop

    :goto_46
    invoke-direct {v7, v8, v6, v6}, Landroid/telephony/RadioAccessSpecifier;-><init>(I[I[I)V

    goto/32 :goto_38

    nop

    :goto_47
    const-wide/32 v7, 0x16bb4

    goto/32 :goto_3

    nop

    :goto_48
    cmp-long v7, v7, v3

    goto/32 :goto_1b

    nop
.end method

.method public startNetworkScan(I)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "startNetworkScan: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "NetworkScanHelper"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->mNetworkScanRequester:Landroid/telephony/NetworkScan;

    if-eqz p1, :cond_0

    return-void

    :cond_0
    iget-object p1, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {p0}, Lcom/android/settings/network/telephony/NetworkScanHelper;->createNetworkScanForPreferredAccessNetworks()Landroid/telephony/NetworkScanRequest;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->mExecutor:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->mInternalNetworkScanCallback:Landroid/telephony/TelephonyScanManager$NetworkScanCallback;

    invoke-virtual {p1, v0, v1, v2}, Landroid/telephony/TelephonyManager;->requestNetworkScan(Landroid/telephony/NetworkScanRequest;Ljava/util/concurrent/Executor;Landroid/telephony/TelephonyScanManager$NetworkScanCallback;)Landroid/telephony/NetworkScan;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->mNetworkScanRequester:Landroid/telephony/NetworkScan;

    if-nez p1, :cond_1

    const/16 p1, 0x2710

    invoke-direct {p0, p1}, Lcom/android/settings/network/telephony/NetworkScanHelper;->onError(I)V

    :cond_1
    return-void
.end method

.method public stopNetworkQuery()V
    .locals 3

    const-string v0, "NetworkScanHelper"

    iget-object v1, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->mNetworkScanRequester:Landroid/telephony/NetworkScan;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/telephony/NetworkScan;->stopScan()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->mNetworkScanRequester:Landroid/telephony/NetworkScan;

    goto :goto_1

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSlotIndex()I

    move-result v1

    if-ltz v1, :cond_1

    iget-object v2, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getActiveModemCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/android/settings/network/telephony/TelephonyUtils;->abortIncrementalScan(Landroid/content/Context;I)V

    goto :goto_0

    :cond_1
    const-string/jumbo v1, "slotIndex is invalid, skipping abort"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v1, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->mContext:Landroid/content/Context;

    iget-object p0, p0, Lcom/android/settings/network/telephony/NetworkScanHelper;->mLegacyIncrScanReceiver:Lcom/android/settings/network/telephony/LegacyIncrementalScanBroadcastReceiver;

    invoke-virtual {v1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    const-string p0, "IllegalArgumentException"

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_1
    move-exception p0

    const-string v1, "abortIncrementalScan Exception: "

    invoke-static {v0, v1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_1
    return-void
.end method
