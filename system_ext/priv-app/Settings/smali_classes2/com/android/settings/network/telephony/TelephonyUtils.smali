.class public final Lcom/android/settings/network/telephony/TelephonyUtils;
.super Ljava/lang/Object;


# static fields
.field public static DBG:Z

.field private static mIsServiceBound:Z

.field private static mIsSubsidyFeatureEnabled:Ljava/util/Optional;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Optional<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "TelephonyUtils"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/settings/network/telephony/TelephonyUtils;->DBG:Z

    invoke-static {}, Ljava/util/Optional;->empty()Ljava/util/Optional;

    move-result-object v0

    sput-object v0, Lcom/android/settings/network/telephony/TelephonyUtils;->mIsSubsidyFeatureEnabled:Ljava/util/Optional;

    return-void
.end method

.method public static abortIncrementalScan(Landroid/content/Context;I)V
    .locals 0

    sget-boolean p0, Lcom/android/settings/network/telephony/TelephonyUtils;->mIsServiceBound:Z

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const-string p0, "TelephonyUtils"

    const-string p1, "abortIncrementalScan: ExtTelephony Service not connected!"

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public static allowUsertoSetDDS(Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "allow_user_select_dds"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    move v1, v0

    :cond_0
    return v1
.end method

.method public static connectExtTelephonyService(Landroid/content/Context;)V
    .locals 1

    sget-boolean p0, Lcom/android/settings/network/telephony/TelephonyUtils;->mIsServiceBound:Z

    if-nez p0, :cond_0

    const-string p0, "TelephonyUtils"

    const-string v0, "Connect to ExtTelephonyService..."

    invoke-static {p0, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public static isAdvancedPlmnScanSupported(Landroid/content/Context;)Z
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

.method public static isServiceConnected()Z
    .locals 1

    sget-boolean v0, Lcom/android/settings/network/telephony/TelephonyUtils;->mIsServiceBound:Z

    return v0
.end method

.method public static isSubsidyFeatureEnabled(Landroid/content/Context;)Z
    .locals 2

    sget-object v0, Lcom/android/settings/network/telephony/TelephonyUtils;->mIsSubsidyFeatureEnabled:Ljava/util/Optional;

    invoke-virtual {v0}, Ljava/util/Optional;->isPresent()Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/android/settings/network/telephony/TelephonyUtils;->mIsServiceBound:Z

    const-string v1, "TelephonyUtils"

    if-nez v0, :cond_0

    const-string v0, "isSubsidyFeatureEnabled: ExtTelephony Service not connected!"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/android/settings/network/telephony/TelephonyUtils;->connectExtTelephonyService(Landroid/content/Context;)V

    :cond_0
    const/4 p0, 0x0

    :try_start_0
    sput-object p0, Lcom/android/settings/network/telephony/TelephonyUtils;->mIsSubsidyFeatureEnabled:Ljava/util/Optional;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    const-string v0, "isSubsidyFeatureEnabled: , Exception: "

    invoke-static {v1, v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    :goto_0
    sget-object p0, Lcom/android/settings/network/telephony/TelephonyUtils;->mIsSubsidyFeatureEnabled:Ljava/util/Optional;

    invoke-virtual {p0}, Ljava/util/Optional;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method public static isSubsidySimCard(Landroid/content/Context;I)Z
    .locals 1

    sget-boolean p1, Lcom/android/settings/network/telephony/TelephonyUtils;->mIsServiceBound:Z

    if-nez p1, :cond_0

    const-string p1, "TelephonyUtils"

    const-string v0, "isSubsidySimCard: ExtTelephony Service not connected!"

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/android/settings/network/telephony/TelephonyUtils;->connectExtTelephonyService(Landroid/content/Context;)V

    :cond_0
    const/4 p0, 0x0

    return p0
.end method
