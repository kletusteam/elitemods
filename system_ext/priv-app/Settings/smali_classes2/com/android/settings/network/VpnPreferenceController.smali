.class public Lcom/android/settings/network/VpnPreferenceController;
.super Lcom/android/settingslib/core/AbstractPreferenceController;

# interfaces
.implements Lcom/android/settings/core/PreferenceControllerMixin;
.implements Lcom/android/settingslib/core/lifecycle/LifecycleObserver;
.implements Lcom/android/settingslib/core/lifecycle/events/OnResume;
.implements Lcom/android/settingslib/core/lifecycle/events/OnPause;


# static fields
.field private static final REQUEST:Landroid/net/NetworkRequest;


# instance fields
.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private final mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

.field private mPreference:Landroidx/preference/Preference;


# direct methods
.method public static synthetic $r8$lambda$H8bpfEn3QzMTDBApRBbK-LKr1d8(Lcom/android/internal/net/VpnConfig;)Z
    .locals 0

    invoke-static {p0}, Lcom/android/settings/network/VpnPreferenceController;->lambda$getNumberOfNonLegacyVpn$2(Lcom/android/internal/net/VpnConfig;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$O5KbORzKEUDy0YXSZrngyCl-Fog(Lcom/android/settings/network/VpnPreferenceController;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/network/VpnPreferenceController;->lambda$updateSummary$0(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$VcFdqfcr6_38PLqOUdsFGFAiW9Q(Landroid/net/VpnManager;Landroid/content/pm/UserInfo;)Lcom/android/internal/net/VpnConfig;
    .locals 0

    invoke-static {p0, p1}, Lcom/android/settings/network/VpnPreferenceController;->lambda$getNumberOfNonLegacyVpn$1(Landroid/net/VpnManager;Landroid/content/pm/UserInfo;)Lcom/android/internal/net/VpnConfig;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic $r8$lambda$an7IOiMK9v4cO96psWAfgBRqAIQ(Lcom/android/internal/net/VpnProfile;)Z
    .locals 0

    invoke-static {p0}, Lcom/android/settings/network/VpnPreferenceController;->lambda$getInsecureVpnCount$4(Lcom/android/internal/net/VpnProfile;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$zpM1mGj6HpdENDBg4ZqUZgcaClo(Ljava/lang/String;)Lcom/android/internal/net/VpnProfile;
    .locals 0

    invoke-static {p0}, Lcom/android/settings/network/VpnPreferenceController;->lambda$getInsecureVpnCount$3(Ljava/lang/String;)Lcom/android/internal/net/VpnProfile;

    move-result-object p0

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v0}, Landroid/net/NetworkRequest$Builder;-><init>()V

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->removeCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v0

    sput-object v0, Lcom/android/settings/network/VpnPreferenceController;->REQUEST:Landroid/net/NetworkRequest;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settingslib/core/AbstractPreferenceController;-><init>(Landroid/content/Context;)V

    new-instance p1, Lcom/android/settings/network/VpnPreferenceController$1;

    invoke-direct {p1, p0}, Lcom/android/settings/network/VpnPreferenceController$1;-><init>(Lcom/android/settings/network/VpnPreferenceController;)V

    iput-object p1, p0, Lcom/android/settings/network/VpnPreferenceController;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    return-void
.end method

.method private static synthetic lambda$getInsecureVpnCount$3(Ljava/lang/String;)Lcom/android/internal/net/VpnProfile;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VPN_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/security/LegacyVpnProfileStore;->get(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/internal/net/VpnProfile;->decode(Ljava/lang/String;[B)Lcom/android/internal/net/VpnProfile;

    move-result-object p0

    return-object p0
.end method

.method private static synthetic lambda$getInsecureVpnCount$4(Lcom/android/internal/net/VpnProfile;)Z
    .locals 0

    iget p0, p0, Lcom/android/internal/net/VpnProfile;->type:I

    invoke-static {p0}, Lcom/android/internal/net/VpnProfile;->isLegacyType(I)Z

    move-result p0

    return p0
.end method

.method private static synthetic lambda$getNumberOfNonLegacyVpn$1(Landroid/net/VpnManager;Landroid/content/pm/UserInfo;)Lcom/android/internal/net/VpnConfig;
    .locals 0

    iget p1, p1, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {p0, p1}, Landroid/net/VpnManager;->getVpnConfig(I)Lcom/android/internal/net/VpnConfig;

    move-result-object p0

    return-object p0
.end method

.method private static synthetic lambda$getNumberOfNonLegacyVpn$2(Lcom/android/internal/net/VpnConfig;)Z
    .locals 0

    if-eqz p0, :cond_0

    iget-boolean p0, p0, Lcom/android/internal/net/VpnConfig;->legacy:Z

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private synthetic lambda$updateSummary$0(Ljava/lang/String;)V
    .locals 0

    iget-object p0, p0, Lcom/android/settings/network/VpnPreferenceController;->mPreference:Landroidx/preference/Preference;

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public displayPreference(Landroidx/preference/PreferenceScreen;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settingslib/core/AbstractPreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    invoke-virtual {p0, p1}, Lcom/android/settings/network/VpnPreferenceController;->getEffectivePreference(Landroidx/preference/PreferenceScreen;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/network/VpnPreferenceController;->mPreference:Landroidx/preference/Preference;

    return-void
.end method

.method protected getEffectivePreference(Landroidx/preference/PreferenceScreen;)Landroidx/preference/Preference;
    .locals 1

    const-string/jumbo v0, "vpn_settings"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    iget-object p0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "airplane_mode_toggleable_radios"

    invoke-static {p0, v0}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_1

    const-string/jumbo v0, "wifi"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_2

    :cond_1
    const-string p0, "airplane_mode"

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setDependency(Ljava/lang/String;)V

    :cond_2
    return-object p1
.end method

.method protected getInsecureVpnCount([Ljava/lang/String;)I
    .locals 0

    new-instance p0, Lcom/android/settings/network/VpnPreferenceController$$ExternalSyntheticLambda1;

    invoke-direct {p0}, Lcom/android/settings/network/VpnPreferenceController$$ExternalSyntheticLambda1;-><init>()V

    invoke-static {p1}, Ljava/util/Arrays;->stream([Ljava/lang/Object;)Ljava/util/stream/Stream;

    move-result-object p1

    invoke-interface {p1, p0}, Ljava/util/stream/Stream;->map(Ljava/util/function/Function;)Ljava/util/stream/Stream;

    move-result-object p0

    new-instance p1, Lcom/android/settings/network/VpnPreferenceController$$ExternalSyntheticLambda2;

    invoke-direct {p1}, Lcom/android/settings/network/VpnPreferenceController$$ExternalSyntheticLambda2;-><init>()V

    invoke-interface {p0, p1}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/stream/Stream;->count()J

    move-result-wide p0

    long-to-int p0, p0

    return p0
.end method

.method protected getInsecureVpnSummaryOverride(Landroid/os/UserManager;Landroid/net/VpnManager;)Ljava/lang/String;
    .locals 6

    iget-object v0, p0, Lcom/android/settings/network/VpnPreferenceController;->mPreference:Landroidx/preference/Preference;

    instance-of v0, v0, Lcom/android/settings/vpn2/VpnInfoPreference;

    if-eqz v0, :cond_3

    const-string v0, "VPN_"

    invoke-static {v0}, Landroid/security/LegacyVpnProfileStore;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/settings/network/VpnPreferenceController;->getInsecureVpnCount([Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-lez v1, :cond_0

    move v4, v3

    goto :goto_0

    :cond_0
    move v4, v2

    :goto_0
    iget-object v5, p0, Lcom/android/settings/network/VpnPreferenceController;->mPreference:Landroidx/preference/Preference;

    check-cast v5, Lcom/android/settings/vpn2/VpnInfoPreference;

    invoke-virtual {v5, v4}, Lcom/android/settings/vpn2/VpnInfoPreference;->setInsecureVpn(Z)V

    if-eqz v4, :cond_3

    array-length v0, v0

    if-gt v0, v3, :cond_1

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/network/VpnPreferenceController;->getNumberOfNonLegacyVpn(Landroid/os/UserManager;Landroid/net/VpnManager;)I

    move-result p1

    add-int/2addr v0, p1

    if-ne v0, v3, :cond_1

    iget-object p0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->vpn_settings_insecure_single:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    if-ne v1, v3, :cond_2

    iget-object p0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->vpn_settings_single_insecure_multiple_total:I

    new-array p2, v3, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p2, v2

    invoke-virtual {p0, p1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_2
    iget-object p0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    sget p1, Lcom/android/settings/R$string;->vpn_settings_multiple_insecure_multiple_total:I

    new-array p2, v3, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p2, v2

    invoke-virtual {p0, p1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_3
    const/4 p0, 0x0

    return-object p0
.end method

.method getNameForVpnConfig(Lcom/android/internal/net/VpnConfig;Landroid/os/UserHandle;)Ljava/lang/String;
    .locals 2

    goto/32 :goto_b

    nop

    :goto_0
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_12

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_a

    nop

    :goto_2
    const/4 p0, 0x0

    goto/32 :goto_e

    nop

    :goto_3
    return-object p0

    :goto_4
    goto/32 :goto_c

    nop

    :goto_5
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_f

    nop

    :goto_6
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_9

    nop

    :goto_7
    const-string p1, " is not present"

    goto/32 :goto_0

    nop

    :goto_8
    const-string p2, "VpnPreferenceController"

    goto/32 :goto_13

    nop

    :goto_9
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_7

    nop

    :goto_a
    iget-object p0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    goto/32 :goto_11

    nop

    :goto_b
    iget-boolean v0, p1, Lcom/android/internal/net/VpnConfig;->legacy:Z

    goto/32 :goto_1

    nop

    :goto_c
    iget-object p1, p1, Lcom/android/internal/net/VpnConfig;->user:Ljava/lang/String;

    :try_start_0
    iget-object p0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p2}, Landroid/content/Context;->createPackageContextAsUser(Ljava/lang/String;ILandroid/os/UserHandle;)Landroid/content/Context;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/android/internal/net/VpnConfig;->getVpnLabel(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_14

    nop

    :goto_d
    new-instance p2, Ljava/lang/StringBuilder;

    goto/32 :goto_5

    nop

    :goto_e
    return-object p0

    :goto_f
    const-string v0, "Package "

    goto/32 :goto_6

    nop

    :goto_10
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_3

    nop

    :goto_11
    sget p1, Lcom/android/settings/R$string;->wifi_display_status_connected:I

    goto/32 :goto_10

    nop

    :goto_12
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_8

    nop

    :goto_13
    invoke-static {p2, p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/32 :goto_2

    nop

    :goto_14
    return-object p0

    :catch_0
    move-exception p0

    goto/32 :goto_d

    nop
.end method

.method protected getNumberOfNonLegacyVpn(Landroid/os/UserManager;Landroid/net/VpnManager;)I
    .locals 0

    invoke-virtual {p1}, Landroid/os/UserManager;->getUsers()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object p0

    new-instance p1, Lcom/android/settings/network/VpnPreferenceController$$ExternalSyntheticLambda3;

    invoke-direct {p1, p2}, Lcom/android/settings/network/VpnPreferenceController$$ExternalSyntheticLambda3;-><init>(Landroid/net/VpnManager;)V

    invoke-interface {p0, p1}, Ljava/util/stream/Stream;->map(Ljava/util/function/Function;)Ljava/util/stream/Stream;

    move-result-object p0

    new-instance p1, Lcom/android/settings/network/VpnPreferenceController$$ExternalSyntheticLambda4;

    invoke-direct {p1}, Lcom/android/settings/network/VpnPreferenceController$$ExternalSyntheticLambda4;-><init>()V

    invoke-interface {p0, p1}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/stream/Stream;->count()J

    move-result-wide p0

    long-to-int p0, p0

    return p0
.end method

.method public getPreferenceKey()Ljava/lang/String;
    .locals 0

    const-string/jumbo p0, "vpn_settings"

    return-object p0
.end method

.method public isAvailable()Z
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    const-string v1, "disallow_vpn"

    invoke-static {v0, v1}, Lcom/miui/enterprise/RestrictionsHelper;->hasRestriction(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string p0, "Enterprise"

    const-string v0, "VPN is restricted"

    invoke-static {p0, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_0
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_1
    iget-object p0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    const-string/jumbo v1, "no_config_vpn"

    invoke-static {p0, v1, v0}, Lcom/android/settingslib/RestrictedLockUtilsInternal;->hasBaseUserRestriction(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    return p0
.end method

.method public onPause()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/network/VpnPreferenceController;->mConnectivityManager:Landroid/net/ConnectivityManager;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/network/VpnPreferenceController;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/network/VpnPreferenceController;->mConnectivityManager:Landroid/net/ConnectivityManager;

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/settings/network/VpnPreferenceController;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    const-class v1, Landroid/net/ConnectivityManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/android/settings/network/VpnPreferenceController;->mConnectivityManager:Landroid/net/ConnectivityManager;

    sget-object v1, Lcom/android/settings/network/VpnPreferenceController;->REQUEST:Landroid/net/NetworkRequest;

    iget-object p0, p0, Lcom/android/settings/network/VpnPreferenceController;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v0, v1, p0}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/network/VpnPreferenceController;->mConnectivityManager:Landroid/net/ConnectivityManager;

    :goto_0
    return-void
.end method

.method updateSummary()V
    .locals 4

    goto/32 :goto_11

    nop

    :goto_0
    return-void

    :goto_1
    goto/32 :goto_1f

    nop

    :goto_2
    if-eqz v2, :cond_0

    goto/32 :goto_2e

    :cond_0
    goto/32 :goto_1b

    nop

    :goto_3
    invoke-virtual {p0, v0, v1}, Lcom/android/settings/network/VpnPreferenceController;->getInsecureVpnSummaryOverride(Landroid/os/UserManager;Landroid/net/VpnManager;)Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_2

    nop

    :goto_4
    new-instance v0, Lcom/android/settings/network/VpnPreferenceController$$ExternalSyntheticLambda0;

    goto/32 :goto_b

    nop

    :goto_5
    sget v1, Lcom/android/settings/R$string;->vpn_disconnected_summary:I

    goto/32 :goto_17

    nop

    :goto_6
    if-eqz v0, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_0

    nop

    :goto_7
    check-cast v1, Landroid/net/VpnManager;

    goto/32 :goto_3

    nop

    :goto_8
    goto/16 :goto_2e

    :goto_9
    goto/32 :goto_28

    nop

    :goto_a
    const/4 v3, 0x3

    goto/32 :goto_15

    nop

    :goto_b
    invoke-direct {v0, p0, v2}, Lcom/android/settings/network/VpnPreferenceController$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/network/VpnPreferenceController;Ljava/lang/String;)V

    goto/32 :goto_f

    nop

    :goto_c
    const-class v2, Landroid/net/VpnManager;

    goto/32 :goto_2f

    nop

    :goto_d
    if-nez v2, :cond_2

    goto/32 :goto_2c

    :cond_2
    goto/32 :goto_27

    nop

    :goto_e
    check-cast v0, Landroid/os/UserManager;

    goto/32 :goto_22

    nop

    :goto_f
    invoke-static {v0}, Lcom/android/settingslib/utils/ThreadUtils;->postOnMainThread(Ljava/lang/Runnable;)V

    goto/32 :goto_1c

    nop

    :goto_10
    iget v0, v0, Landroid/content/pm/UserInfo;->restrictedProfileParentId:I

    goto/32 :goto_23

    nop

    :goto_11
    iget-object v0, p0, Lcom/android/settings/network/VpnPreferenceController;->mPreference:Landroidx/preference/Preference;

    goto/32 :goto_6

    nop

    :goto_12
    invoke-virtual {v1, v0}, Landroid/net/VpnManager;->getLegacyVpnInfo(I)Lcom/android/internal/net/LegacyVpnInfo;

    move-result-object v1

    goto/32 :goto_20

    nop

    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v0

    goto/32 :goto_26

    nop

    :goto_14
    iget v1, v1, Lcom/android/internal/net/LegacyVpnInfo;->state:I

    goto/32 :goto_a

    nop

    :goto_15
    if-ne v1, v3, :cond_3

    goto/32 :goto_2c

    :cond_3
    :goto_16
    goto/32 :goto_2b

    nop

    :goto_17
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_8

    nop

    :goto_18
    if-eqz v2, :cond_4

    goto/32 :goto_9

    :cond_4
    goto/32 :goto_25

    nop

    :goto_19
    if-nez v3, :cond_5

    goto/32 :goto_2c

    :cond_5
    goto/32 :goto_12

    nop

    :goto_1a
    const-class v1, Landroid/os/UserManager;

    goto/32 :goto_2a

    nop

    :goto_1b
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    goto/32 :goto_13

    nop

    :goto_1c
    return-void

    :goto_1d
    iget v0, v0, Landroid/content/pm/UserInfo;->id:I

    :goto_1e
    goto/32 :goto_21

    nop

    :goto_1f
    iget-object v0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    goto/32 :goto_1a

    nop

    :goto_20
    if-nez v1, :cond_6

    goto/32 :goto_16

    :cond_6
    goto/32 :goto_14

    nop

    :goto_21
    invoke-virtual {v1, v0}, Landroid/net/VpnManager;->getVpnConfig(I)Lcom/android/internal/net/VpnConfig;

    move-result-object v2

    goto/32 :goto_d

    nop

    :goto_22
    iget-object v1, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    goto/32 :goto_c

    nop

    :goto_23
    goto :goto_1e

    :goto_24
    goto/32 :goto_1d

    nop

    :goto_25
    iget-object v0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    goto/32 :goto_5

    nop

    :goto_26
    invoke-virtual {v0}, Landroid/content/pm/UserInfo;->isRestricted()Z

    move-result v2

    goto/32 :goto_29

    nop

    :goto_27
    iget-boolean v3, v2, Lcom/android/internal/net/VpnConfig;->legacy:Z

    goto/32 :goto_19

    nop

    :goto_28
    invoke-static {v0}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object v0

    goto/32 :goto_2d

    nop

    :goto_29
    if-nez v2, :cond_7

    goto/32 :goto_24

    :cond_7
    goto/32 :goto_10

    nop

    :goto_2a
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_e

    nop

    :goto_2b
    const/4 v2, 0x0

    :goto_2c
    goto/32 :goto_18

    nop

    :goto_2d
    invoke-virtual {p0, v2, v0}, Lcom/android/settings/network/VpnPreferenceController;->getNameForVpnConfig(Lcom/android/internal/net/VpnConfig;Landroid/os/UserHandle;)Ljava/lang/String;

    move-result-object v2

    :goto_2e
    goto/32 :goto_4

    nop

    :goto_2f
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_7

    nop
.end method
