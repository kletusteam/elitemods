.class Lcom/android/settings/network/telephony/UserPLMNListActivity$1;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/network/telephony/UserPLMNListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/network/telephony/UserPLMNListActivity;


# direct methods
.method constructor <init>(Lcom/android/settings/network/telephony/UserPLMNListActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/network/telephony/UserPLMNListActivity$1;->this$0:Lcom/android/settings/network/telephony/UserPLMNListActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string v0, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/android/settings/network/telephony/UserPLMNListActivity$1;->this$0:Lcom/android/settings/network/telephony/UserPLMNListActivity;

    const-string/jumbo v0, "state"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p2

    invoke-static {p1, p2}, Lcom/android/settings/network/telephony/UserPLMNListActivity;->-$$Nest$fputmAirplaneModeOn(Lcom/android/settings/network/telephony/UserPLMNListActivity;Z)V

    iget-object p0, p0, Lcom/android/settings/network/telephony/UserPLMNListActivity$1;->this$0:Lcom/android/settings/network/telephony/UserPLMNListActivity;

    invoke-static {p0}, Lcom/android/settings/network/telephony/UserPLMNListActivity;->-$$Nest$msetScreenEnabled(Lcom/android/settings/network/telephony/UserPLMNListActivity;)V

    goto :goto_1

    :cond_0
    const-string v0, "com.qualcomm.qti.intent.action.ACTION_READ_EF_RESULT"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v2, "exception"

    if-eqz v0, :cond_2

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "ACTION_READ_EF_BROADCAST with exception"

    invoke-static {p1}, Lcom/android/settings/network/telephony/UserPLMNListActivity;->-$$Nest$smlog(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/settings/network/telephony/UserPLMNListActivity$1;->this$0:Lcom/android/settings/network/telephony/UserPLMNListActivity;

    invoke-static {p1}, Lcom/android/settings/network/telephony/UserPLMNListActivity;->-$$Nest$fgetmHandler(Lcom/android/settings/network/telephony/UserPLMNListActivity;)Lcom/android/settings/network/telephony/UserPLMNListActivity$MyHandler;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object p1

    iget-object p2, p0, Lcom/android/settings/network/telephony/UserPLMNListActivity$1;->this$0:Lcom/android/settings/network/telephony/UserPLMNListActivity;

    invoke-static {p2}, Lcom/android/settings/network/telephony/UserPLMNListActivity;->-$$Nest$fgetmHandler(Lcom/android/settings/network/telephony/UserPLMNListActivity;)Lcom/android/settings/network/telephony/UserPLMNListActivity$MyHandler;

    iput v1, p1, Landroid/os/Message;->what:I

    new-instance p2, Landroid/os/AsyncResult;

    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    const/4 v1, 0x0

    invoke-direct {p2, v1, v1, v0}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    iput-object p2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object p0, p0, Lcom/android/settings/network/telephony/UserPLMNListActivity$1;->this$0:Lcom/android/settings/network/telephony/UserPLMNListActivity;

    invoke-static {p0}, Lcom/android/settings/network/telephony/UserPLMNListActivity;->-$$Nest$fgetmHandler(Lcom/android/settings/network/telephony/UserPLMNListActivity;)Lcom/android/settings/network/telephony/UserPLMNListActivity$MyHandler;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    :cond_1
    iget-object p0, p0, Lcom/android/settings/network/telephony/UserPLMNListActivity$1;->this$0:Lcom/android/settings/network/telephony/UserPLMNListActivity;

    const-string/jumbo p1, "payload"

    invoke-virtual {p2, p1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/android/settings/network/telephony/UserPLMNListActivity;->handleGetEFDone([B)V

    goto :goto_1

    :cond_2
    const-string v0, "com.qualcomm.qti.intent.action.ACTION_WRITE_EF_RESULT"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    if-eqz p1, :cond_3

    const-string p1, "ACTION_WRITE_EF_BROADCAST with exception"

    invoke-static {p1}, Lcom/android/settings/network/telephony/UserPLMNListActivity;->-$$Nest$smlog(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string p1, "handleSetEFDone: with OK result!"

    invoke-static {p1}, Lcom/android/settings/network/telephony/UserPLMNListActivity;->-$$Nest$smlog(Ljava/lang/String;)V

    :goto_0
    iget-object p0, p0, Lcom/android/settings/network/telephony/UserPLMNListActivity$1;->this$0:Lcom/android/settings/network/telephony/UserPLMNListActivity;

    invoke-static {p0}, Lcom/android/settings/network/telephony/UserPLMNListActivity;->-$$Nest$mgetUPLMNInfoFromEf(Lcom/android/settings/network/telephony/UserPLMNListActivity;)V

    :cond_4
    :goto_1
    return-void
.end method
