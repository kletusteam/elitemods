.class abstract Lcom/android/settings/network/ims/ImsQueryController;
.super Ljava/lang/Object;


# instance fields
.field private volatile mCapability:I

.field private volatile mTech:I

.field private volatile mTransportType:I


# direct methods
.method constructor <init>(III)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/settings/network/ims/ImsQueryController;->mCapability:I

    iput p2, p0, Lcom/android/settings/network/ims/ImsQueryController;->mTech:I

    iput p3, p0, Lcom/android/settings/network/ims/ImsQueryController;->mTransportType:I

    return-void
.end method


# virtual methods
.method isEnabledByPlatform(I)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Landroid/telephony/ims/ImsException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    goto/32 :goto_c

    nop

    :goto_0
    if-eqz v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_1

    nop

    :goto_1
    const/4 p0, 0x0

    goto/32 :goto_5

    nop

    :goto_2
    invoke-virtual {p1, v2, p0, v0, v1}, Landroid/telephony/ims/ImsMmTelManager;->isSupported(IILjava/util/concurrent/Executor;Ljava/util/function/Consumer;)V

    goto/32 :goto_7

    nop

    :goto_3
    new-instance v1, Lcom/android/settings/network/ims/BooleanConsumer;

    goto/32 :goto_4

    nop

    :goto_4
    invoke-direct {v1}, Lcom/android/settings/network/ims/BooleanConsumer;-><init>()V

    goto/32 :goto_8

    nop

    :goto_5
    return p0

    :goto_6
    goto/32 :goto_b

    nop

    :goto_7
    const-wide/16 p0, 0x7d0

    goto/32 :goto_d

    nop

    :goto_8
    iget v2, p0, Lcom/android/settings/network/ims/ImsQueryController;->mCapability:I

    goto/32 :goto_e

    nop

    :goto_9
    return p0

    :goto_a
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_b
    invoke-static {p1}, Landroid/telephony/ims/ImsMmTelManager;->createForSubscriptionId(I)Landroid/telephony/ims/ImsMmTelManager;

    move-result-object p1

    goto/32 :goto_a

    nop

    :goto_c
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->isValidSubscriptionId(I)Z

    move-result v0

    goto/32 :goto_0

    nop

    :goto_d
    invoke-virtual {v1, p0, p1}, Lcom/android/settings/network/ims/BooleanConsumer;->get(J)Z

    move-result p0

    goto/32 :goto_9

    nop

    :goto_e
    iget p0, p0, Lcom/android/settings/network/ims/ImsQueryController;->mTransportType:I

    goto/32 :goto_2

    nop
.end method

.method isProvisionedOnDevice(I)Z
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {v0}, Lcom/android/settings/network/ims/ImsQueryProvisioningStat;->query()Z

    move-result p0

    goto/32 :goto_a

    nop

    :goto_1
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->isValidSubscriptionId(I)Z

    move-result v0

    goto/32 :goto_2

    nop

    :goto_2
    if-eqz v0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_7

    nop

    :goto_3
    iget v1, p0, Lcom/android/settings/network/ims/ImsQueryController;->mCapability:I

    goto/32 :goto_5

    nop

    :goto_4
    new-instance v0, Lcom/android/settings/network/ims/ImsQueryProvisioningStat;

    goto/32 :goto_3

    nop

    :goto_5
    iget p0, p0, Lcom/android/settings/network/ims/ImsQueryController;->mTech:I

    goto/32 :goto_6

    nop

    :goto_6
    invoke-direct {v0, p1, v1, p0}, Lcom/android/settings/network/ims/ImsQueryProvisioningStat;-><init>(III)V

    goto/32 :goto_0

    nop

    :goto_7
    const/4 p0, 0x0

    goto/32 :goto_8

    nop

    :goto_8
    return p0

    :goto_9
    goto/32 :goto_4

    nop

    :goto_a
    return p0
.end method

.method isServiceStateReady(I)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Landroid/telephony/ims/ImsException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    goto/32 :goto_10

    nop

    :goto_0
    invoke-virtual {p0, p1, v1}, Landroid/telephony/ims/ImsMmTelManager;->getFeatureState(Ljava/util/concurrent/Executor;Ljava/util/function/Consumer;)V

    goto/32 :goto_b

    nop

    :goto_1
    const/4 p1, 0x2

    goto/32 :goto_9

    nop

    :goto_2
    if-eqz p0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_4

    nop

    :goto_3
    const/4 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_4
    return v0

    :goto_5
    goto/32 :goto_8

    nop

    :goto_6
    return v0

    :goto_7
    new-instance v1, Lcom/android/settings/network/ims/IntegerConsumer;

    goto/32 :goto_c

    nop

    :goto_8
    invoke-static {p1}, Landroid/telephony/ims/ImsMmTelManager;->createForSubscriptionId(I)Landroid/telephony/ims/ImsMmTelManager;

    move-result-object p0

    goto/32 :goto_d

    nop

    :goto_9
    if-eq p0, p1, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_e

    nop

    :goto_a
    invoke-virtual {v1, p0, p1}, Lcom/android/settings/network/ims/IntegerConsumer;->get(J)I

    move-result p0

    goto/32 :goto_1

    nop

    :goto_b
    const-wide/16 p0, 0x7d0

    goto/32 :goto_a

    nop

    :goto_c
    invoke-direct {v1}, Lcom/android/settings/network/ims/IntegerConsumer;-><init>()V

    goto/32 :goto_0

    nop

    :goto_d
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object p1

    goto/32 :goto_7

    nop

    :goto_e
    const/4 v0, 0x1

    :goto_f
    goto/32 :goto_6

    nop

    :goto_10
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->isValidSubscriptionId(I)Z

    move-result p0

    goto/32 :goto_3

    nop
.end method

.method isTtyOnVolteEnabled(I)Z
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/network/ims/ImsQueryTtyOnVolteStat;->query()Z

    move-result p0

    goto/32 :goto_3

    nop

    :goto_1
    new-instance p0, Lcom/android/settings/network/ims/ImsQueryTtyOnVolteStat;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-direct {p0, p1}, Lcom/android/settings/network/ims/ImsQueryTtyOnVolteStat;-><init>(I)V

    goto/32 :goto_0

    nop

    :goto_3
    return p0
.end method
