.class Lcom/android/settings/network/telephony/LegacyIncrementalScanBroadcastReceiver$QueryDetails;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/network/telephony/LegacyIncrementalScanBroadcastReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "QueryDetails"
.end annotation


# instance fields
.field storedScanInfo:[Ljava/lang/String;

.field final synthetic this$0:Lcom/android/settings/network/telephony/LegacyIncrementalScanBroadcastReceiver;


# direct methods
.method constructor <init>(Lcom/android/settings/network/telephony/LegacyIncrementalScanBroadcastReceiver;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/network/telephony/LegacyIncrementalScanBroadcastReceiver$QueryDetails;->this$0:Lcom/android/settings/network/telephony/LegacyIncrementalScanBroadcastReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/android/settings/network/telephony/LegacyIncrementalScanBroadcastReceiver$QueryDetails;->storedScanInfo:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method concatScanInfo([Ljava/lang/String;)V
    .locals 4

    goto/32 :goto_1

    nop

    :goto_0
    new-array v1, v1, [Ljava/lang/String;

    goto/32 :goto_3

    nop

    :goto_1
    iget-object v0, p0, Lcom/android/settings/network/telephony/LegacyIncrementalScanBroadcastReceiver$QueryDetails;->storedScanInfo:[Ljava/lang/String;

    goto/32 :goto_9

    nop

    :goto_2
    iput-object v1, p0, Lcom/android/settings/network/telephony/LegacyIncrementalScanBroadcastReceiver$QueryDetails;->storedScanInfo:[Ljava/lang/String;

    goto/32 :goto_b

    nop

    :goto_3
    array-length v2, v0

    goto/32 :goto_6

    nop

    :goto_4
    array-length v0, v0

    goto/32 :goto_7

    nop

    :goto_5
    iget-object v0, p0, Lcom/android/settings/network/telephony/LegacyIncrementalScanBroadcastReceiver$QueryDetails;->storedScanInfo:[Ljava/lang/String;

    goto/32 :goto_4

    nop

    :goto_6
    const/4 v3, 0x0

    goto/32 :goto_d

    nop

    :goto_7
    array-length v2, p1

    goto/32 :goto_c

    nop

    :goto_8
    add-int/2addr v1, v2

    goto/32 :goto_0

    nop

    :goto_9
    array-length v1, v0

    goto/32 :goto_a

    nop

    :goto_a
    array-length v2, p1

    goto/32 :goto_8

    nop

    :goto_b
    return-void

    :goto_c
    invoke-static {p1, v3, v1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto/32 :goto_2

    nop

    :goto_d
    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto/32 :goto_5

    nop
.end method

.method reset()V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    iput-object v0, p0, Lcom/android/settings/network/telephony/LegacyIncrementalScanBroadcastReceiver$QueryDetails;->storedScanInfo:[Ljava/lang/String;

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    const/4 v0, 0x0

    goto/32 :goto_0

    nop
.end method
