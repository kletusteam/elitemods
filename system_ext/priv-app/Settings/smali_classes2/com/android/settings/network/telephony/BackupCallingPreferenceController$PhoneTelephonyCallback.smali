.class Lcom/android/settings/network/telephony/BackupCallingPreferenceController$PhoneTelephonyCallback;
.super Landroid/telephony/TelephonyCallback;

# interfaces
.implements Landroid/telephony/TelephonyCallback$CallStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/network/telephony/BackupCallingPreferenceController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PhoneTelephonyCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/network/telephony/BackupCallingPreferenceController;

.field private tm:Landroid/telephony/TelephonyManager;


# direct methods
.method private constructor <init>(Lcom/android/settings/network/telephony/BackupCallingPreferenceController;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/network/telephony/BackupCallingPreferenceController$PhoneTelephonyCallback;->this$0:Lcom/android/settings/network/telephony/BackupCallingPreferenceController;

    invoke-direct {p0}, Landroid/telephony/TelephonyCallback;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settings/network/telephony/BackupCallingPreferenceController;Lcom/android/settings/network/telephony/BackupCallingPreferenceController$PhoneTelephonyCallback-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/network/telephony/BackupCallingPreferenceController$PhoneTelephonyCallback;-><init>(Lcom/android/settings/network/telephony/BackupCallingPreferenceController;)V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/network/telephony/BackupCallingPreferenceController$PhoneTelephonyCallback;->this$0:Lcom/android/settings/network/telephony/BackupCallingPreferenceController;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/android/settings/network/telephony/BackupCallingPreferenceController;->-$$Nest$fputmCallState(Lcom/android/settings/network/telephony/BackupCallingPreferenceController;Ljava/lang/Integer;)V

    iget-object p0, p0, Lcom/android/settings/network/telephony/BackupCallingPreferenceController$PhoneTelephonyCallback;->this$0:Lcom/android/settings/network/telephony/BackupCallingPreferenceController;

    invoke-static {p0}, Lcom/android/settings/network/telephony/BackupCallingPreferenceController;->-$$Nest$fgetmPreference(Lcom/android/settings/network/telephony/BackupCallingPreferenceController;)Landroidx/preference/Preference;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/android/settings/network/telephony/BackupCallingPreferenceController;->updateState(Landroidx/preference/Preference;)V

    return-void
.end method

.method public register(Landroid/content/Context;I)V
    .locals 2

    const-class v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/android/settings/network/telephony/BackupCallingPreferenceController$PhoneTelephonyCallback;->tm:Landroid/telephony/TelephonyManager;

    invoke-static {p2}, Landroid/telephony/SubscriptionManager;->isValidSubscriptionId(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/network/telephony/BackupCallingPreferenceController$PhoneTelephonyCallback;->tm:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0, p2}, Landroid/telephony/TelephonyManager;->createForSubscriptionId(I)Landroid/telephony/TelephonyManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/network/telephony/BackupCallingPreferenceController$PhoneTelephonyCallback;->tm:Landroid/telephony/TelephonyManager;

    :cond_0
    iget-object v0, p0, Lcom/android/settings/network/telephony/BackupCallingPreferenceController$PhoneTelephonyCallback;->this$0:Lcom/android/settings/network/telephony/BackupCallingPreferenceController;

    iget-object v1, p0, Lcom/android/settings/network/telephony/BackupCallingPreferenceController$PhoneTelephonyCallback;->tm:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1, p2}, Landroid/telephony/TelephonyManager;->getCallState(I)I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-static {v0, p2}, Lcom/android/settings/network/telephony/BackupCallingPreferenceController;->-$$Nest$fputmCallState(Lcom/android/settings/network/telephony/BackupCallingPreferenceController;Ljava/lang/Integer;)V

    iget-object p2, p0, Lcom/android/settings/network/telephony/BackupCallingPreferenceController$PhoneTelephonyCallback;->tm:Landroid/telephony/TelephonyManager;

    invoke-virtual {p1}, Landroid/content/Context;->getMainExecutor()Ljava/util/concurrent/Executor;

    move-result-object p1

    invoke-virtual {p2, p1, p0}, Landroid/telephony/TelephonyManager;->registerTelephonyCallback(Ljava/util/concurrent/Executor;Landroid/telephony/TelephonyCallback;)V

    return-void
.end method

.method public unregister()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/network/telephony/BackupCallingPreferenceController$PhoneTelephonyCallback;->this$0:Lcom/android/settings/network/telephony/BackupCallingPreferenceController;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/settings/network/telephony/BackupCallingPreferenceController;->-$$Nest$fputmCallState(Lcom/android/settings/network/telephony/BackupCallingPreferenceController;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/android/settings/network/telephony/BackupCallingPreferenceController$PhoneTelephonyCallback;->tm:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0, p0}, Landroid/telephony/TelephonyManager;->unregisterTelephonyCallback(Landroid/telephony/TelephonyCallback;)V

    return-void
.end method
