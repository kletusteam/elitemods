.class public Lcom/android/settings/network/InternetUpdater;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/settings/AirplaneModeEnabler$OnAirplaneModeChangedListener;
.implements Landroidx/lifecycle/LifecycleObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/network/InternetUpdater$InternetChangeListener;
    }
.end annotation


# static fields
.field private static sTransportMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field mAirplaneModeEnabler:Lcom/android/settings/AirplaneModeEnabler;

.field private final mConnectivityManager:Landroid/net/ConnectivityManager;

.field private final mContext:Landroid/content/Context;

.field mInternetAvailable:Z

.field private mInternetType:I

.field private mListener:Lcom/android/settings/network/InternetUpdater$InternetChangeListener;

.field private mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

.field mTransport:I

.field private final mWifiManager:Landroid/net/wifi/WifiManager;

.field private final mWifiStateFilter:Landroid/content/IntentFilter;

.field private final mWifiStateReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static bridge synthetic -$$Nest$fgetmListener(Lcom/android/settings/network/InternetUpdater;)Lcom/android/settings/network/InternetUpdater$InternetChangeListener;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/network/InternetUpdater;->mListener:Lcom/android/settings/network/InternetUpdater$InternetChangeListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmWifiManager(Lcom/android/settings/network/InternetUpdater;)Landroid/net/wifi/WifiManager;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/network/InternetUpdater;->mWifiManager:Landroid/net/wifi/WifiManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mfetchActiveNetwork(Lcom/android/settings/network/InternetUpdater;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/network/InternetUpdater;->fetchActiveNetwork()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/settings/network/InternetUpdater;->sTransportMap:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/settings/network/InternetUpdater;->sTransportMap:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/settings/network/InternetUpdater;->sTransportMap:Ljava/util/Map;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroidx/lifecycle/Lifecycle;Lcom/android/settings/network/InternetUpdater$InternetChangeListener;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/settings/network/InternetUpdater$1;

    invoke-direct {v0, p0}, Lcom/android/settings/network/InternetUpdater$1;-><init>(Lcom/android/settings/network/InternetUpdater;)V

    iput-object v0, p0, Lcom/android/settings/network/InternetUpdater;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    new-instance v0, Lcom/android/settings/network/InternetUpdater$2;

    invoke-direct {v0, p0}, Lcom/android/settings/network/InternetUpdater$2;-><init>(Lcom/android/settings/network/InternetUpdater;)V

    iput-object v0, p0, Lcom/android/settings/network/InternetUpdater;->mWifiStateReceiver:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/android/settings/network/InternetUpdater;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/android/settings/AirplaneModeEnabler;

    invoke-direct {v0, p1, p0}, Lcom/android/settings/AirplaneModeEnabler;-><init>(Landroid/content/Context;Lcom/android/settings/AirplaneModeEnabler$OnAirplaneModeChangedListener;)V

    iput-object v0, p0, Lcom/android/settings/network/InternetUpdater;->mAirplaneModeEnabler:Lcom/android/settings/AirplaneModeEnabler;

    const-class v0, Landroid/net/ConnectivityManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/android/settings/network/InternetUpdater;->mConnectivityManager:Landroid/net/ConnectivityManager;

    const-class v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/wifi/WifiManager;

    iput-object p1, p0, Lcom/android/settings/network/InternetUpdater;->mWifiManager:Landroid/net/wifi/WifiManager;

    new-instance p1, Landroid/content/IntentFilter;

    const-string v0, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-direct {p1, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/android/settings/network/InternetUpdater;->mWifiStateFilter:Landroid/content/IntentFilter;

    iput-object p3, p0, Lcom/android/settings/network/InternetUpdater;->mListener:Lcom/android/settings/network/InternetUpdater$InternetChangeListener;

    invoke-direct {p0}, Lcom/android/settings/network/InternetUpdater;->fetchActiveNetwork()V

    if-eqz p2, :cond_0

    invoke-virtual {p2, p0}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    :cond_0
    return-void
.end method

.method private fetchActiveNetwork()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/network/InternetUpdater;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetwork()Landroid/net/Network;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/android/settings/network/InternetUpdater;->mInternetAvailable:Z

    invoke-virtual {p0}, Lcom/android/settings/network/InternetUpdater;->updateInternetType()V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/settings/network/InternetUpdater;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v2, v0}, Landroid/net/ConnectivityManager;->getNetworkCapabilities(Landroid/net/Network;)Landroid/net/NetworkCapabilities;

    move-result-object v0

    if-nez v0, :cond_1

    iput-boolean v1, p0, Lcom/android/settings/network/InternetUpdater;->mInternetAvailable:Z

    invoke-virtual {p0}, Lcom/android/settings/network/InternetUpdater;->updateInternetType()V

    return-void

    :cond_1
    invoke-virtual {p0, v0}, Lcom/android/settings/network/InternetUpdater;->updateInternetAvailable(Landroid/net/NetworkCapabilities;)V

    return-void
.end method


# virtual methods
.method public getInternetType()I
    .locals 0

    iget p0, p0, Lcom/android/settings/network/InternetUpdater;->mInternetType:I

    return p0
.end method

.method protected isCarrierWifiActive()Z
    .locals 1

    iget-object p0, p0, Lcom/android/settings/network/InternetUpdater;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {p0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object p0

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Landroid/net/wifi/WifiInfo;->isCarrierMerged()Z

    move-result p0

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    const-string p0, "InternetUpdater"

    const-string v0, "Detect a merged carrier Wi-Fi connected."

    invoke-static {p0, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p0, 0x1

    return p0

    :cond_1
    :goto_0
    const/4 p0, 0x0

    return p0
.end method

.method public onAirplaneModeChanged(Z)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/network/InternetUpdater;->fetchActiveNetwork()V

    iget-object p0, p0, Lcom/android/settings/network/InternetUpdater;->mListener:Lcom/android/settings/network/InternetUpdater$InternetChangeListener;

    if-eqz p0, :cond_0

    invoke-interface {p0, p1}, Lcom/android/settings/network/InternetUpdater$InternetChangeListener;->onAirplaneModeChanged(Z)V

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 0
    .annotation runtime Landroidx/lifecycle/OnLifecycleEvent;
        value = .enum Landroidx/lifecycle/Lifecycle$Event;->ON_DESTROY:Landroidx/lifecycle/Lifecycle$Event;
    .end annotation

    iget-object p0, p0, Lcom/android/settings/network/InternetUpdater;->mAirplaneModeEnabler:Lcom/android/settings/AirplaneModeEnabler;

    invoke-virtual {p0}, Lcom/android/settings/network/GlobalSettingsChangeListener;->close()V

    return-void
.end method

.method public onPause()V
    .locals 2
    .annotation runtime Landroidx/lifecycle/OnLifecycleEvent;
        value = .enum Landroidx/lifecycle/Lifecycle$Event;->ON_PAUSE:Landroidx/lifecycle/Lifecycle$Event;
    .end annotation

    iget-object v0, p0, Lcom/android/settings/network/InternetUpdater;->mAirplaneModeEnabler:Lcom/android/settings/AirplaneModeEnabler;

    invoke-virtual {v0}, Lcom/android/settings/AirplaneModeEnabler;->stop()V

    iget-object v0, p0, Lcom/android/settings/network/InternetUpdater;->mConnectivityManager:Landroid/net/ConnectivityManager;

    iget-object v1, p0, Lcom/android/settings/network/InternetUpdater;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V

    iget-object v0, p0, Lcom/android/settings/network/InternetUpdater;->mContext:Landroid/content/Context;

    iget-object p0, p0, Lcom/android/settings/network/InternetUpdater;->mWifiStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onResume()V
    .locals 3
    .annotation runtime Landroidx/lifecycle/OnLifecycleEvent;
        value = .enum Landroidx/lifecycle/Lifecycle$Event;->ON_RESUME:Landroidx/lifecycle/Lifecycle$Event;
    .end annotation

    iget-object v0, p0, Lcom/android/settings/network/InternetUpdater;->mAirplaneModeEnabler:Lcom/android/settings/AirplaneModeEnabler;

    invoke-virtual {v0}, Lcom/android/settings/AirplaneModeEnabler;->start()V

    iget-object v0, p0, Lcom/android/settings/network/InternetUpdater;->mConnectivityManager:Landroid/net/ConnectivityManager;

    iget-object v1, p0, Lcom/android/settings/network/InternetUpdater;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->registerDefaultNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V

    iget-object v0, p0, Lcom/android/settings/network/InternetUpdater;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/network/InternetUpdater;->mWifiStateReceiver:Landroid/content/BroadcastReceiver;

    iget-object p0, p0, Lcom/android/settings/network/InternetUpdater;->mWifiStateFilter:Landroid/content/IntentFilter;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, p0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    return-void
.end method

.method updateInternetAvailable(Landroid/net/NetworkCapabilities;)V
    .locals 6

    goto/32 :goto_b

    nop

    :goto_0
    if-lt v2, v0, :cond_0

    goto/32 :goto_1c

    :cond_0
    goto/32 :goto_19

    nop

    :goto_1
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_2

    nop

    :goto_2
    const-string v0, "InternetUpdater"

    goto/32 :goto_a

    nop

    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_1b

    nop

    :goto_4
    invoke-virtual {p1}, Landroid/net/NetworkCapabilities;->getTransportTypes()[I

    move-result-object p1

    goto/32 :goto_6

    nop

    :goto_5
    iput v3, p0, Lcom/android/settings/network/InternetUpdater;->mTransport:I

    goto/32 :goto_11

    nop

    :goto_6
    array-length v0, p1

    goto/32 :goto_15

    nop

    :goto_7
    if-nez v0, :cond_1

    goto/32 :goto_1c

    :cond_1
    goto/32 :goto_4

    nop

    :goto_8
    const/4 v1, 0x1

    goto/32 :goto_22

    nop

    :goto_9
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop

    :goto_a
    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_8

    nop

    :goto_b
    const/16 v0, 0xc

    goto/32 :goto_1e

    nop

    :goto_c
    sget-object v4, Lcom/android/settings/network/InternetUpdater;->sTransportMap:Ljava/util/Map;

    goto/32 :goto_1d

    nop

    :goto_d
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1a

    nop

    :goto_e
    invoke-virtual {p0}, Lcom/android/settings/network/InternetUpdater;->updateInternetType()V

    goto/32 :goto_1f

    nop

    :goto_f
    if-nez v0, :cond_2

    goto/32 :goto_1c

    :cond_2
    goto/32 :goto_17

    nop

    :goto_10
    iget v0, p0, Lcom/android/settings/network/InternetUpdater;->mTransport:I

    goto/32 :goto_9

    nop

    :goto_11
    new-instance p1, Ljava/lang/StringBuilder;

    goto/32 :goto_d

    nop

    :goto_12
    if-nez v4, :cond_3

    goto/32 :goto_23

    :cond_3
    goto/32 :goto_5

    nop

    :goto_13
    invoke-virtual {p1, v0}, Landroid/net/NetworkCapabilities;->hasCapability(I)Z

    move-result v0

    goto/32 :goto_7

    nop

    :goto_14
    const/4 v1, 0x0

    goto/32 :goto_f

    nop

    :goto_15
    move v2, v1

    :goto_16
    goto/32 :goto_0

    nop

    :goto_17
    const/16 v0, 0x10

    goto/32 :goto_13

    nop

    :goto_18
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_10

    nop

    :goto_19
    aget v3, p1, v2

    goto/32 :goto_c

    nop

    :goto_1a
    const-string v0, "Detect an internet available network with transport type: "

    goto/32 :goto_18

    nop

    :goto_1b
    goto :goto_16

    :goto_1c
    goto/32 :goto_21

    nop

    :goto_1d
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto/32 :goto_20

    nop

    :goto_1e
    invoke-virtual {p1, v0}, Landroid/net/NetworkCapabilities;->hasCapability(I)Z

    move-result v0

    goto/32 :goto_14

    nop

    :goto_1f
    return-void

    :goto_20
    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    goto/32 :goto_12

    nop

    :goto_21
    iput-boolean v1, p0, Lcom/android/settings/network/InternetUpdater;->mInternetAvailable:Z

    goto/32 :goto_e

    nop

    :goto_22
    goto :goto_1c

    :goto_23
    goto/32 :goto_3

    nop
.end method

.method updateInternetType()V
    .locals 3

    goto/32 :goto_1c

    nop

    :goto_0
    if-nez v2, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_d

    nop

    :goto_1
    check-cast v0, Ljava/lang/Integer;

    goto/32 :goto_14

    nop

    :goto_2
    iget-object v0, p0, Lcom/android/settings/network/InternetUpdater;->mAirplaneModeEnabler:Lcom/android/settings/AirplaneModeEnabler;

    goto/32 :goto_f

    nop

    :goto_3
    if-nez p0, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_6

    nop

    :goto_4
    if-nez v0, :cond_2

    goto/32 :goto_17

    :cond_2
    goto/32 :goto_18

    nop

    :goto_5
    iget-object p0, p0, Lcom/android/settings/network/InternetUpdater;->mListener:Lcom/android/settings/network/InternetUpdater$InternetChangeListener;

    goto/32 :goto_3

    nop

    :goto_6
    invoke-interface {p0, v1}, Lcom/android/settings/network/InternetUpdater$InternetChangeListener;->onInternetTypeChanged(I)V

    :goto_7
    goto/32 :goto_1a

    nop

    :goto_8
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/32 :goto_21

    nop

    :goto_9
    goto :goto_1e

    :goto_a
    goto/32 :goto_2

    nop

    :goto_b
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v0

    goto/32 :goto_11

    nop

    :goto_c
    iget v2, p0, Lcom/android/settings/network/InternetUpdater;->mTransport:I

    goto/32 :goto_8

    nop

    :goto_d
    goto :goto_1e

    :goto_e
    goto/32 :goto_13

    nop

    :goto_f
    invoke-virtual {v0}, Lcom/android/settings/AirplaneModeEnabler;->isAirplaneModeOn()Z

    move-result v0

    goto/32 :goto_4

    nop

    :goto_10
    const/4 v1, 0x3

    goto/32 :goto_15

    nop

    :goto_11
    if-ne v0, v1, :cond_3

    goto/32 :goto_17

    :cond_3
    goto/32 :goto_19

    nop

    :goto_12
    sget-object v0, Lcom/android/settings/network/InternetUpdater;->sTransportMap:Ljava/util/Map;

    goto/32 :goto_c

    nop

    :goto_13
    move v1, v0

    goto/32 :goto_9

    nop

    :goto_14
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto/32 :goto_22

    nop

    :goto_15
    if-nez v0, :cond_4

    goto/32 :goto_a

    :cond_4
    goto/32 :goto_12

    nop

    :goto_16
    goto :goto_1e

    :goto_17
    goto/32 :goto_1d

    nop

    :goto_18
    iget-object v0, p0, Lcom/android/settings/network/InternetUpdater;->mWifiManager:Landroid/net/wifi/WifiManager;

    goto/32 :goto_b

    nop

    :goto_19
    const/4 v1, 0x0

    goto/32 :goto_16

    nop

    :goto_1a
    return-void

    :goto_1b
    iput v1, p0, Lcom/android/settings/network/InternetUpdater;->mInternetType:I

    goto/32 :goto_5

    nop

    :goto_1c
    iget-boolean v0, p0, Lcom/android/settings/network/InternetUpdater;->mInternetAvailable:Z

    goto/32 :goto_10

    nop

    :goto_1d
    const/4 v1, 0x1

    :goto_1e
    goto/32 :goto_1b

    nop

    :goto_1f
    invoke-virtual {p0}, Lcom/android/settings/network/InternetUpdater;->isCarrierWifiActive()Z

    move-result v2

    goto/32 :goto_0

    nop

    :goto_20
    if-eq v0, v2, :cond_5

    goto/32 :goto_e

    :cond_5
    goto/32 :goto_1f

    nop

    :goto_21
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_22
    const/4 v2, 0x2

    goto/32 :goto_20

    nop
.end method
