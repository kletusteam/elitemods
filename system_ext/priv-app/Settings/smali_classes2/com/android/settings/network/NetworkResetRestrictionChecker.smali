.class public Lcom/android/settings/network/NetworkResetRestrictionChecker;
.super Ljava/lang/Object;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mUserManager:Landroid/os/UserManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/network/NetworkResetRestrictionChecker;->mContext:Landroid/content/Context;

    const-string/jumbo v0, "user"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/os/UserManager;

    iput-object p1, p0, Lcom/android/settings/network/NetworkResetRestrictionChecker;->mUserManager:Landroid/os/UserManager;

    return-void
.end method


# virtual methods
.method hasUserBaseRestriction()Z
    .locals 2

    goto/32 :goto_4

    nop

    :goto_0
    invoke-static {p0, v1, v0}, Lcom/android/settingslib/RestrictedLockUtilsInternal;->hasBaseUserRestriction(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result p0

    goto/32 :goto_2

    nop

    :goto_1
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    goto/32 :goto_3

    nop

    :goto_2
    return p0

    :goto_3
    const-string/jumbo v1, "no_network_reset"

    goto/32 :goto_0

    nop

    :goto_4
    iget-object p0, p0, Lcom/android/settings/network/NetworkResetRestrictionChecker;->mContext:Landroid/content/Context;

    goto/32 :goto_1

    nop
.end method

.method hasUserRestriction()Z
    .locals 1

    goto/32 :goto_9

    nop

    :goto_0
    goto :goto_5

    :goto_1
    goto/32 :goto_8

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_7

    nop

    :goto_3
    return p0

    :goto_4
    goto :goto_c

    :goto_5
    goto/32 :goto_b

    nop

    :goto_6
    if-nez p0, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_0

    nop

    :goto_7
    invoke-virtual {p0}, Lcom/android/settings/network/NetworkResetRestrictionChecker;->hasUserBaseRestriction()Z

    move-result p0

    goto/32 :goto_6

    nop

    :goto_8
    const/4 p0, 0x0

    goto/32 :goto_4

    nop

    :goto_9
    iget-object v0, p0, Lcom/android/settings/network/NetworkResetRestrictionChecker;->mUserManager:Landroid/os/UserManager;

    goto/32 :goto_a

    nop

    :goto_a
    invoke-virtual {v0}, Landroid/os/UserManager;->isAdminUser()Z

    move-result v0

    goto/32 :goto_2

    nop

    :goto_b
    const/4 p0, 0x1

    :goto_c
    goto/32 :goto_3

    nop
.end method

.method isRestrictionEnforcedByAdmin()Z
    .locals 2

    goto/32 :goto_4

    nop

    :goto_0
    return p0

    :goto_1
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    goto/32 :goto_7

    nop

    :goto_2
    invoke-static {p0, v1, v0}, Lcom/android/settingslib/RestrictedLockUtilsInternal;->checkIfRestrictionEnforced(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    move-result-object p0

    goto/32 :goto_8

    nop

    :goto_3
    const/4 p0, 0x1

    goto/32 :goto_9

    nop

    :goto_4
    iget-object p0, p0, Lcom/android/settings/network/NetworkResetRestrictionChecker;->mContext:Landroid/content/Context;

    goto/32 :goto_1

    nop

    :goto_5
    const/4 p0, 0x0

    :goto_6
    goto/32 :goto_0

    nop

    :goto_7
    const-string/jumbo v1, "no_network_reset"

    goto/32 :goto_2

    nop

    :goto_8
    if-nez p0, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_3

    nop

    :goto_9
    goto :goto_6

    :goto_a
    goto/32 :goto_5

    nop
.end method
