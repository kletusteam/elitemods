.class public Lcom/android/settings/network/telephony/cdma/CdmaSubscriptionPreferenceController;
.super Lcom/android/settings/network/telephony/cdma/CdmaBasePreferenceController;

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final TYPE_NV:Ljava/lang/String; = "NV"

.field private static final TYPE_RUIM:Ljava/lang/String; = "RUIM"


# instance fields
.field mPreference:Landroidx/preference/ListPreference;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/network/telephony/cdma/CdmaBasePreferenceController;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method deviceSupportsNvAndRuim()Z
    .locals 8

    goto/32 :goto_17

    nop

    :goto_0
    const-string v7, "RUIM"

    goto/32 :goto_1b

    nop

    :goto_1
    goto/16 :goto_1e

    :goto_2
    goto/32 :goto_1d

    nop

    :goto_3
    move v3, v2

    goto/32 :goto_25

    nop

    :goto_4
    move v5, v1

    :goto_5
    goto/32 :goto_9

    nop

    :goto_6
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    goto/32 :goto_26

    nop

    :goto_7
    if-nez v7, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_21

    nop

    :goto_8
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    goto/32 :goto_13

    nop

    :goto_9
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_1f

    nop

    :goto_a
    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_1c

    nop

    :goto_b
    return v1

    :goto_c
    goto :goto_5

    :goto_d
    goto/32 :goto_0

    nop

    :goto_e
    move v5, v4

    :goto_f
    goto/32 :goto_12

    nop

    :goto_10
    if-nez v5, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_1

    nop

    :goto_11
    move v4, v2

    goto/32 :goto_e

    nop

    :goto_12
    if-nez v4, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_10

    nop

    :goto_13
    const/4 v1, 0x1

    goto/32 :goto_19

    nop

    :goto_14
    if-nez v6, :cond_3

    goto/32 :goto_5

    :cond_3
    goto/32 :goto_4

    nop

    :goto_15
    if-lt v3, v0, :cond_4

    goto/32 :goto_f

    :cond_4
    goto/32 :goto_1a

    nop

    :goto_16
    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    goto/32 :goto_7

    nop

    :goto_17
    invoke-virtual {p0}, Lcom/android/settings/network/telephony/cdma/CdmaSubscriptionPreferenceController;->getRilSubscriptionTypes()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_8

    nop

    :goto_18
    if-eqz v0, :cond_5

    goto/32 :goto_20

    :cond_5
    goto/32 :goto_22

    nop

    :goto_19
    const/4 v2, 0x0

    goto/32 :goto_18

    nop

    :goto_1a
    aget-object v6, p0, v3

    goto/32 :goto_6

    nop

    :goto_1b
    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    goto/32 :goto_14

    nop

    :goto_1c
    array-length v0, p0

    goto/32 :goto_3

    nop

    :goto_1d
    move v1, v2

    :goto_1e
    goto/32 :goto_b

    nop

    :goto_1f
    goto :goto_24

    :goto_20
    goto/32 :goto_11

    nop

    :goto_21
    move v4, v1

    goto/32 :goto_c

    nop

    :goto_22
    const-string v0, ","

    goto/32 :goto_a

    nop

    :goto_23
    move v5, v4

    :goto_24
    goto/32 :goto_15

    nop

    :goto_25
    move v4, v3

    goto/32 :goto_23

    nop

    :goto_26
    const-string v7, "NV"

    goto/32 :goto_16

    nop
.end method

.method public getAvailabilityStatus(I)I
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/android/settings/network/telephony/MobileNetworkUtils;->isCdmaOptions(Landroid/content/Context;I)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/network/telephony/cdma/CdmaSubscriptionPreferenceController;->deviceSupportsNvAndRuim()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_0
    const/4 p0, 0x2

    :goto_0
    return p0
.end method

.method public bridge synthetic getBackgroundWorkerClass()Ljava/lang/Class;
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->getBackgroundWorkerClass()Ljava/lang/Class;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic getIntentFilter()Landroid/content/IntentFilter;
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->getIntentFilter()Landroid/content/IntentFilter;

    move-result-object p0

    return-object p0
.end method

.method protected getRilSubscriptionTypes()Ljava/lang/String;
    .locals 0

    const-string/jumbo p0, "ril.subscription.types"

    invoke-static {p0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public bridge synthetic getSliceHighlightMenuRes()I
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->getSliceHighlightMenuRes()I

    move-result p0

    return p0
.end method

.method public bridge synthetic hasAsyncUpdate()Z
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->hasAsyncUpdate()Z

    move-result p0

    return p0
.end method

.method public bridge synthetic isPublicSlice()Z
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->isPublicSlice()Z

    move-result p0

    return p0
.end method

.method public bridge synthetic isSliceable()Z
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->isSliceable()Z

    move-result p0

    return p0
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 0

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    :try_start_0
    iget-object p2, p0, Lcom/android/settings/network/telephony/cdma/CdmaBasePreferenceController;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {p2, p1}, Landroid/telephony/TelephonyManager;->setCdmaSubscriptionMode(I)V

    iget-object p0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo p2, "subscription_mode"

    invoke-static {p0, p2, p1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p0, 0x1

    return p0

    :catch_0
    const/4 p0, 0x0

    return p0
.end method

.method public updateState(Landroidx/preference/Preference;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settingslib/core/AbstractPreferenceController;->updateState(Landroidx/preference/Preference;)V

    check-cast p1, Landroidx/preference/ListPreference;

    invoke-virtual {p0}, Lcom/android/settings/network/telephony/TelephonyBasePreferenceController;->getAvailabilityStatus()I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/android/settingslib/core/AbstractPreferenceController;->setVisible(Landroidx/preference/Preference;Z)V

    iget-object p0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string/jumbo v0, "subscription_mode"

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    const/4 v0, -0x1

    if-eq p0, v0, :cond_1

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroidx/preference/ListPreference;->setValue(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public bridge synthetic useDynamicSliceSummary()Z
    .locals 0

    invoke-super {p0}, Lcom/android/settings/slices/Sliceable;->useDynamicSliceSummary()Z

    move-result p0

    return p0
.end method
