.class public Lcom/android/settings/network/TetherPreferenceController;
.super Lcom/android/settingslib/core/AbstractPreferenceController;

# interfaces
.implements Lcom/android/settings/core/PreferenceControllerMixin;
.implements Lcom/android/settingslib/core/lifecycle/LifecycleObserver;
.implements Lcom/android/settingslib/core/lifecycle/events/OnCreate;
.implements Lcom/android/settingslib/core/lifecycle/events/OnResume;
.implements Lcom/android/settingslib/core/lifecycle/events/OnPause;
.implements Lcom/android/settingslib/core/lifecycle/events/OnDestroy;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/network/TetherPreferenceController$TetherBroadcastReceiver;,
        Lcom/android/settings/network/TetherPreferenceController$SettingObserver;
    }
.end annotation


# instance fields
.field private final mAdminDisallowedTetherConfig:Z

.field private mAirplaneModeObserver:Lcom/android/settings/network/TetherPreferenceController$SettingObserver;

.field private final mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private final mBluetoothPan:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Landroid/bluetooth/BluetoothPan;",
            ">;"
        }
    .end annotation
.end field

.field final mBtProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field private mPreference:Landroidx/preference/Preference;

.field private mTetherReceiver:Lcom/android/settings/network/TetherPreferenceController$TetherBroadcastReceiver;

.field private final mTetheringManager:Landroid/net/TetheringManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmBluetoothPan(Lcom/android/settings/network/TetherPreferenceController;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/network/TetherPreferenceController;->mBluetoothPan:Ljava/util/concurrent/atomic/AtomicReference;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mupdateSummaryToOff(Lcom/android/settings/network/TetherPreferenceController;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/network/TetherPreferenceController;->updateSummaryToOff()V

    return-void
.end method

.method constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settingslib/core/AbstractPreferenceController;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/android/settings/network/TetherPreferenceController$1;

    invoke-direct {v1, p0}, Lcom/android/settings/network/TetherPreferenceController$1;-><init>(Lcom/android/settings/network/TetherPreferenceController;)V

    iput-object v1, p0, Lcom/android/settings/network/TetherPreferenceController;->mBtProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/settings/network/TetherPreferenceController;->mAdminDisallowedTetherConfig:Z

    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v1, p0, Lcom/android/settings/network/TetherPreferenceController;->mBluetoothPan:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/network/TetherPreferenceController;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iput-object v0, p0, Lcom/android/settings/network/TetherPreferenceController;->mTetheringManager:Landroid/net/TetheringManager;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/settingslib/core/lifecycle/Lifecycle;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/settingslib/core/AbstractPreferenceController;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/android/settings/network/TetherPreferenceController$1;

    invoke-direct {v0, p0}, Lcom/android/settings/network/TetherPreferenceController$1;-><init>(Lcom/android/settings/network/TetherPreferenceController;)V

    iput-object v0, p0, Lcom/android/settings/network/TetherPreferenceController;->mBtProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/android/settings/network/TetherPreferenceController;->mBluetoothPan:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {p1}, Lcom/android/settings/network/TetherPreferenceController;->isTetherConfigDisallowed(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/network/TetherPreferenceController;->mAdminDisallowedTetherConfig:Z

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/network/TetherPreferenceController;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    const-class v0, Landroid/net/TetheringManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/TetheringManager;

    iput-object p1, p0, Lcom/android/settings/network/TetherPreferenceController;->mTetheringManager:Landroid/net/TetheringManager;

    if-eqz p2, :cond_0

    invoke-virtual {p2, p0}, Lcom/android/settingslib/core/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/network/TetherPreferenceController;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method public static isTetherConfigDisallowed(Landroid/content/Context;)Z
    .locals 2

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    const-string/jumbo v1, "no_config_tethering"

    invoke-static {p0, v1, v0}, Lcom/android/settingslib/RestrictedLockUtilsInternal;->checkIfRestrictionEnforced(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private updateSummaryToOff()V
    .locals 1

    iget-object p0, p0, Lcom/android/settings/network/TetherPreferenceController;->mPreference:Landroidx/preference/Preference;

    if-nez p0, :cond_0

    return-void

    :cond_0
    sget v0, Lcom/android/settings/R$string;->switch_off_text:I

    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setSummary(I)V

    return-void
.end method


# virtual methods
.method public displayPreference(Landroidx/preference/PreferenceScreen;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settingslib/core/AbstractPreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    const-string/jumbo v0, "tether_settings"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/network/TetherPreferenceController;->mPreference:Landroidx/preference/Preference;

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/network/TetherPreferenceController;->mAdminDisallowedTetherConfig:Z

    if-nez v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/network/TetherPreferenceController;->mTetheringManager:Landroid/net/TetheringManager;

    invoke-static {p0}, Lcom/android/settingslib/Utils;->getTetheringLabel(Landroid/net/TetheringManager;)I

    move-result p0

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setTitle(I)V

    :cond_0
    return-void
.end method

.method public getPreferenceKey()Ljava/lang/String;
    .locals 0

    const-string/jumbo p0, "tether_settings"

    return-object p0
.end method

.method public isAvailable()Z
    .locals 1

    iget-object v0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settingslib/TetherUtil;->isTetherAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    const-string/jumbo v0, "settings_tether_all_in_one"

    invoke-static {p0, v0}, Landroid/util/FeatureFlagUtils;->isEnabled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public onDestroy()V
    .locals 0

    return-void
.end method

.method public onPause()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/network/TetherPreferenceController;->mBluetoothPan:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothProfile;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/network/TetherPreferenceController;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v1, :cond_0

    const/4 v2, 0x5

    invoke-virtual {v1, v2, v0}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/network/TetherPreferenceController;->mAirplaneModeObserver:Lcom/android/settings/network/TetherPreferenceController$SettingObserver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/network/TetherPreferenceController;->mAirplaneModeObserver:Lcom/android/settings/network/TetherPreferenceController$SettingObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    :cond_1
    iget-object v0, p0, Lcom/android/settings/network/TetherPreferenceController;->mTetherReceiver:Lcom/android/settings/network/TetherPreferenceController$TetherBroadcastReceiver;

    if-eqz v0, :cond_2

    iget-object p0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_2
    return-void
.end method

.method public onResume()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/network/TetherPreferenceController;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/network/TetherPreferenceController;->mBluetoothPan:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothProfile;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/network/TetherPreferenceController;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/settings/network/TetherPreferenceController;->mBtProfileServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    :cond_0
    iget-object v0, p0, Lcom/android/settings/network/TetherPreferenceController;->mAirplaneModeObserver:Lcom/android/settings/network/TetherPreferenceController$SettingObserver;

    if-nez v0, :cond_1

    new-instance v0, Lcom/android/settings/network/TetherPreferenceController$SettingObserver;

    invoke-direct {v0, p0}, Lcom/android/settings/network/TetherPreferenceController$SettingObserver;-><init>(Lcom/android/settings/network/TetherPreferenceController;)V

    iput-object v0, p0, Lcom/android/settings/network/TetherPreferenceController;->mAirplaneModeObserver:Lcom/android/settings/network/TetherPreferenceController$SettingObserver;

    :cond_1
    iget-object v0, p0, Lcom/android/settings/network/TetherPreferenceController;->mTetherReceiver:Lcom/android/settings/network/TetherPreferenceController$TetherBroadcastReceiver;

    if-nez v0, :cond_2

    new-instance v0, Lcom/android/settings/network/TetherPreferenceController$TetherBroadcastReceiver;

    invoke-direct {v0, p0}, Lcom/android/settings/network/TetherPreferenceController$TetherBroadcastReceiver;-><init>(Lcom/android/settings/network/TetherPreferenceController;)V

    iput-object v0, p0, Lcom/android/settings/network/TetherPreferenceController;->mTetherReceiver:Lcom/android/settings/network/TetherPreferenceController$TetherBroadcastReceiver;

    :cond_2
    iget-object v0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/network/TetherPreferenceController;->mTetherReceiver:Lcom/android/settings/network/TetherPreferenceController$TetherBroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.net.conn.TETHER_STATE_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object p0, p0, Lcom/android/settings/network/TetherPreferenceController;->mAirplaneModeObserver:Lcom/android/settings/network/TetherPreferenceController$SettingObserver;

    iget-object v1, p0, Lcom/android/settings/network/TetherPreferenceController$SettingObserver;->uri:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method public updateState(Landroidx/preference/Preference;)V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/network/TetherPreferenceController;->updateSummary()V

    return-void
.end method

.method updateSummary()V
    .locals 12

    goto/32 :goto_5c

    nop

    :goto_0
    if-gtz v1, :cond_0

    goto/32 :goto_20

    :cond_0
    goto/32 :goto_23

    nop

    :goto_1
    iget-object p0, p0, Lcom/android/settings/network/TetherPreferenceController;->mPreference:Landroidx/preference/Preference;

    goto/32 :goto_42

    nop

    :goto_2
    if-nez v1, :cond_1

    goto/32 :goto_20

    :cond_1
    goto/32 :goto_18

    nop

    :goto_3
    array-length v0, v0

    goto/32 :goto_58

    nop

    :goto_4
    if-nez v0, :cond_2

    goto/32 :goto_39

    :cond_2
    goto/32 :goto_e

    nop

    :goto_5
    if-nez v7, :cond_3

    goto/32 :goto_37

    :cond_3
    goto/32 :goto_52

    nop

    :goto_6
    xor-int/lit8 v0, v7, 0x1

    goto/32 :goto_2d

    nop

    :goto_7
    if-nez v1, :cond_4

    goto/32 :goto_4b

    :cond_4
    goto/32 :goto_24

    nop

    :goto_8
    goto/16 :goto_3b

    :goto_9
    goto/32 :goto_3

    nop

    :goto_a
    invoke-virtual {v2}, Landroid/net/TetheringManager;->getTetherableBluetoothRegexs()[Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_5a

    nop

    :goto_b
    if-nez v0, :cond_5

    goto/32 :goto_39

    :cond_5
    goto/32 :goto_38

    nop

    :goto_c
    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setSummary(I)V

    goto/32 :goto_1d

    nop

    :goto_d
    iget-object v1, p0, Lcom/android/settings/network/TetherPreferenceController;->mTetheringManager:Landroid/net/TetheringManager;

    goto/32 :goto_26

    nop

    :goto_e
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothPan;->isTetheringOn()Z

    move-result v0

    goto/32 :goto_b

    nop

    :goto_f
    iget-object v2, p0, Lcom/android/settings/network/TetherPreferenceController;->mTetheringManager:Landroid/net/TetheringManager;

    goto/32 :goto_a

    nop

    :goto_10
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_56

    nop

    :goto_11
    iget-object v0, p0, Lcom/android/settings/network/TetherPreferenceController;->mBluetoothPan:Ljava/util/concurrent/atomic/AtomicReference;

    goto/32 :goto_10

    nop

    :goto_12
    return-void

    :goto_13
    move v7, v3

    :goto_14
    goto/32 :goto_28

    nop

    :goto_15
    array-length v9, v1

    goto/32 :goto_4c

    nop

    :goto_16
    if-gt v1, v4, :cond_6

    goto/32 :goto_9

    :cond_6
    goto/32 :goto_4e

    nop

    :goto_17
    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setSummary(I)V

    goto/32 :goto_36

    nop

    :goto_18
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v1

    goto/32 :goto_59

    nop

    :goto_19
    goto :goto_1c

    :goto_1a
    goto/32 :goto_5

    nop

    :goto_1b
    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setSummary(I)V

    :goto_1c
    goto/32 :goto_12

    nop

    :goto_1d
    goto :goto_1c

    :goto_1e
    goto/32 :goto_43

    nop

    :goto_1f
    move v0, v3

    :goto_20
    goto/32 :goto_48

    nop

    :goto_21
    invoke-virtual {p0, v0}, Landroidx/preference/Preference;->setSummary(I)V

    goto/32 :goto_19

    nop

    :goto_22
    move v6, v3

    goto/32 :goto_3e

    nop

    :goto_23
    iget-object v1, p0, Lcom/android/settings/network/TetherPreferenceController;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    goto/32 :goto_2

    nop

    :goto_24
    array-length v5, v0

    goto/32 :goto_22

    nop

    :goto_25
    if-nez v0, :cond_7

    goto/32 :goto_54

    :cond_7
    goto/32 :goto_7

    nop

    :goto_26
    invoke-virtual {v1}, Landroid/net/TetheringManager;->getTetherableWifiRegexs()[Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_f

    nop

    :goto_27
    if-eqz v0, :cond_8

    goto/32 :goto_61

    :cond_8
    goto/32 :goto_60

    nop

    :goto_28
    array-length v1, v0

    goto/32 :goto_16

    nop

    :goto_29
    aget-object v11, v1, v10

    goto/32 :goto_57

    nop

    :goto_2a
    invoke-virtual {v0}, Landroid/net/TetheringManager;->getTetheredIfaces()[Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_d

    nop

    :goto_2b
    goto/16 :goto_4d

    :goto_2c
    goto/32 :goto_5f

    nop

    :goto_2d
    goto :goto_3b

    :goto_2e
    goto/32 :goto_34

    nop

    :goto_2f
    add-int/lit8 v10, v10, 0x1

    goto/32 :goto_2b

    nop

    :goto_30
    if-lt v10, v9, :cond_9

    goto/32 :goto_2c

    :cond_9
    goto/32 :goto_29

    nop

    :goto_31
    iget-object p0, p0, Lcom/android/settings/network/TetherPreferenceController;->mPreference:Landroidx/preference/Preference;

    goto/32 :goto_5e

    nop

    :goto_32
    sget v0, Lcom/android/settings/R$string;->tether_settings_summary_hotspot_on_tether_on:I

    goto/32 :goto_17

    nop

    :goto_33
    if-nez v2, :cond_a

    goto/32 :goto_20

    :cond_a
    goto/32 :goto_51

    nop

    :goto_34
    move v0, v3

    goto/32 :goto_53

    nop

    :goto_35
    sget v0, Lcom/android/settings/R$string;->tether_settings_summary_hotspot_off_tether_on:I

    goto/32 :goto_1b

    nop

    :goto_36
    goto/16 :goto_1c

    :goto_37
    goto/32 :goto_44

    nop

    :goto_38
    move v3, v4

    :goto_39
    goto/32 :goto_1f

    nop

    :goto_3a
    move v7, v0

    :goto_3b
    goto/32 :goto_45

    nop

    :goto_3c
    if-eqz v0, :cond_b

    goto/32 :goto_1a

    :cond_b
    goto/32 :goto_1

    nop

    :goto_3d
    if-nez v11, :cond_c

    goto/32 :goto_50

    :cond_c
    goto/32 :goto_55

    nop

    :goto_3e
    move v7, v6

    :goto_3f
    goto/32 :goto_5d

    nop

    :goto_40
    iget-object v0, p0, Lcom/android/settings/network/TetherPreferenceController;->mTetheringManager:Landroid/net/TetheringManager;

    goto/32 :goto_2a

    nop

    :goto_41
    aget-object v8, v0, v6

    goto/32 :goto_15

    nop

    :goto_42
    sget v0, Lcom/android/settings/R$string;->switch_off_text:I

    goto/32 :goto_21

    nop

    :goto_43
    iget-object p0, p0, Lcom/android/settings/network/TetherPreferenceController;->mPreference:Landroidx/preference/Preference;

    goto/32 :goto_35

    nop

    :goto_44
    if-nez v7, :cond_d

    goto/32 :goto_1e

    :cond_d
    goto/32 :goto_31

    nop

    :goto_45
    if-eqz v0, :cond_e

    goto/32 :goto_20

    :cond_e
    goto/32 :goto_33

    nop

    :goto_46
    if-eq v1, v2, :cond_f

    goto/32 :goto_20

    :cond_f
    goto/32 :goto_11

    nop

    :goto_47
    const/4 v4, 0x1

    goto/32 :goto_25

    nop

    :goto_48
    if-eqz v7, :cond_10

    goto/32 :goto_1a

    :cond_10
    goto/32 :goto_3c

    nop

    :goto_49
    move v0, v3

    goto/32 :goto_3a

    nop

    :goto_4a
    goto :goto_3f

    :goto_4b
    goto/32 :goto_13

    nop

    :goto_4c
    move v10, v3

    :goto_4d
    goto/32 :goto_30

    nop

    :goto_4e
    move v0, v4

    goto/32 :goto_8

    nop

    :goto_4f
    goto/16 :goto_2c

    :goto_50
    goto/32 :goto_2f

    nop

    :goto_51
    array-length v1, v2

    goto/32 :goto_0

    nop

    :goto_52
    if-nez v0, :cond_11

    goto/32 :goto_37

    :cond_11
    goto/32 :goto_5b

    nop

    :goto_53
    goto/16 :goto_3b

    :goto_54
    goto/32 :goto_49

    nop

    :goto_55
    move v7, v4

    goto/32 :goto_4f

    nop

    :goto_56
    check-cast v0, Landroid/bluetooth/BluetoothPan;

    goto/32 :goto_4

    nop

    :goto_57
    invoke-virtual {v8, v11}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v11

    goto/32 :goto_3d

    nop

    :goto_58
    if-eq v0, v4, :cond_12

    goto/32 :goto_2e

    :cond_12
    goto/32 :goto_6

    nop

    :goto_59
    const/16 v2, 0xc

    goto/32 :goto_46

    nop

    :goto_5a
    const/4 v3, 0x0

    goto/32 :goto_47

    nop

    :goto_5b
    iget-object p0, p0, Lcom/android/settings/network/TetherPreferenceController;->mPreference:Landroidx/preference/Preference;

    goto/32 :goto_32

    nop

    :goto_5c
    iget-object v0, p0, Lcom/android/settings/network/TetherPreferenceController;->mPreference:Landroidx/preference/Preference;

    goto/32 :goto_27

    nop

    :goto_5d
    if-lt v6, v5, :cond_13

    goto/32 :goto_14

    :cond_13
    goto/32 :goto_41

    nop

    :goto_5e
    sget v0, Lcom/android/settings/R$string;->tether_settings_summary_hotspot_on_tether_off:I

    goto/32 :goto_c

    nop

    :goto_5f
    add-int/lit8 v6, v6, 0x1

    goto/32 :goto_4a

    nop

    :goto_60
    return-void

    :goto_61
    goto/32 :goto_40

    nop
.end method
