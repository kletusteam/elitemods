.class public Lcom/android/settings/CardInfo;
.super Ljava/lang/Object;


# instance fields
.field private checkedIconResId:I

.field private iconResId:I

.field private isChecked:Z

.field private isDisable:Z

.field private onClickListener:Landroid/view/View$OnClickListener;

.field private titleResId:I

.field private valueResId:I


# direct methods
.method public constructor <init>(III)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/settings/CardInfo;->iconResId:I

    iput p2, p0, Lcom/android/settings/CardInfo;->titleResId:I

    iput p3, p0, Lcom/android/settings/CardInfo;->valueResId:I

    return-void
.end method


# virtual methods
.method public getCheckedIconResId()I
    .locals 0

    iget p0, p0, Lcom/android/settings/CardInfo;->checkedIconResId:I

    return p0
.end method

.method public getIconResId()I
    .locals 0

    iget p0, p0, Lcom/android/settings/CardInfo;->iconResId:I

    return p0
.end method

.method public getOnClickListener()Landroid/view/View$OnClickListener;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/CardInfo;->onClickListener:Landroid/view/View$OnClickListener;

    return-object p0
.end method

.method public getTitleResId()I
    .locals 0

    iget p0, p0, Lcom/android/settings/CardInfo;->titleResId:I

    return p0
.end method

.method public getValueResId()I
    .locals 0

    iget p0, p0, Lcom/android/settings/CardInfo;->valueResId:I

    return p0
.end method

.method public isChecked()Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/CardInfo;->isChecked:Z

    return p0
.end method

.method public isDisable()Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/CardInfo;->isDisable:Z

    return p0
.end method

.method public setChecked(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/CardInfo;->isChecked:Z

    return-void
.end method

.method public setCheckedIconResId(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/CardInfo;->checkedIconResId:I

    return-void
.end method

.method public setDisable(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/CardInfo;->isDisable:Z

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/CardInfo;->onClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public setValueResId(I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/CardInfo;->valueResId:I

    return-void
.end method
