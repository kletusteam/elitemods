.class final enum Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/MiuiUserCredentialsSettings$Credential;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;

.field public static final enum CA_CERTIFICATE:Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;

.field public static final enum USER_CERTIFICATE:Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;

.field public static final enum USER_PRIVATE_KEY:Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;

.field public static final enum USER_SECRET_KEY:Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;


# instance fields
.field final prefix:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    new-instance v0, Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;

    const-string v1, "CA_CERTIFICATE"

    const/4 v2, 0x0

    const-string v3, "CACERT_"

    invoke-direct {v0, v1, v2, v3}, Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;->CA_CERTIFICATE:Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;

    new-instance v1, Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;

    const-string v3, "USER_CERTIFICATE"

    const/4 v4, 0x1

    const-string v5, "USRCERT_"

    invoke-direct {v1, v3, v4, v5}, Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;->USER_CERTIFICATE:Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;

    new-instance v3, Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;

    const-string v5, "USER_PRIVATE_KEY"

    const/4 v6, 0x2

    const-string v7, "USRPKEY_"

    invoke-direct {v3, v5, v6, v7}, Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;->USER_PRIVATE_KEY:Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;

    new-instance v5, Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;

    const-string v7, "USER_SECRET_KEY"

    const/4 v8, 0x3

    const-string v9, "USRSKEY_"

    invoke-direct {v5, v7, v8, v9}, Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;->USER_SECRET_KEY:Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;

    const/4 v7, 0x4

    new-array v7, v7, [Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;

    aput-object v0, v7, v2

    aput-object v1, v7, v4

    aput-object v3, v7, v6

    aput-object v5, v7, v8

    sput-object v7, Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;->$VALUES:[Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;->prefix:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;
    .locals 1

    const-class v0, Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;

    return-object p0
.end method

.method public static values()[Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;
    .locals 1

    sget-object v0, Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;->$VALUES:[Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;

    invoke-virtual {v0}, [Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/settings/MiuiUserCredentialsSettings$Credential$Type;

    return-object v0
.end method
