.class Lcom/android/settings/KeyDownloadDialogActivity$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/settings/KeyDownloadDialog$IOnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/KeyDownloadDialogActivity;->initView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/KeyDownloadDialogActivity;

.field final synthetic val$packageName:Ljava/lang/String;

.field final synthetic val$shortcut:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/settings/KeyDownloadDialogActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/KeyDownloadDialogActivity$1;->this$0:Lcom/android/settings/KeyDownloadDialogActivity;

    iput-object p2, p0, Lcom/android/settings/KeyDownloadDialogActivity$1;->val$shortcut:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/settings/KeyDownloadDialogActivity$1;->val$packageName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss()V
    .locals 0

    iget-object p0, p0, Lcom/android/settings/KeyDownloadDialogActivity$1;->this$0:Lcom/android/settings/KeyDownloadDialogActivity;

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onNegativeBtnClick()V
    .locals 0

    return-void
.end method

.method public onPositiveBtnClick()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/KeyDownloadDialogActivity$1;->val$shortcut:Ljava/lang/String;

    const-string v1, "knock_gesture_v"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/KeyDownloadDialogActivity$1;->this$0:Lcom/android/settings/KeyDownloadDialogActivity;

    invoke-virtual {v0}, Lcom/android/settings/KeyDownloadDialogActivity;->gotoKnockGestureVSetting()V

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/android/settings/KeyDownloadDialogActivity$1;->val$shortcut:Ljava/lang/String;

    const-string v1, "back_double_tap"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/KeyDownloadDialogActivity$1;->val$shortcut:Ljava/lang/String;

    const-string v1, "back_triple_tap"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/KeyDownloadDialogActivity$1;->val$shortcut:Ljava/lang/String;

    const-string v1, "fingerprint_double_tap"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/settings/KeyDownloadDialogActivity$1;->this$0:Lcom/android/settings/KeyDownloadDialogActivity;

    invoke-virtual {v0}, Lcom/android/settings/KeyDownloadDialogActivity;->gotoFingerPrintTapSettings()V

    goto :goto_1

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/android/settings/KeyDownloadDialogActivity$1;->this$0:Lcom/android/settings/KeyDownloadDialogActivity;

    invoke-virtual {v0}, Lcom/android/settings/KeyDownloadDialogActivity;->gotoBackTapGestureSettings()V

    :cond_3
    :goto_1
    iget-object p0, p0, Lcom/android/settings/KeyDownloadDialogActivity$1;->this$0:Lcom/android/settings/KeyDownloadDialogActivity;

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onReTipBtnClick()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/KeyDownloadDialogActivity$1;->this$0:Lcom/android/settings/KeyDownloadDialogActivity;

    iget-object v1, p0, Lcom/android/settings/KeyDownloadDialogActivity$1;->val$packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/settings/KeyDownloadDialogActivity;->openInMarket(Ljava/lang/String;)V

    iget-object p0, p0, Lcom/android/settings/KeyDownloadDialogActivity$1;->this$0:Lcom/android/settings/KeyDownloadDialogActivity;

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method
