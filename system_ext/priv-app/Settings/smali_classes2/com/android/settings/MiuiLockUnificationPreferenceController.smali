.class public Lcom/android/settings/MiuiLockUnificationPreferenceController;
.super Ljava/lang/Object;

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final MY_USER_ID:I


# instance fields
.field private mChooseLockSettingsHelper:Lcom/android/settings/MiuiChooseLockSettingsHelper;

.field private mContext:Landroid/content/Context;

.field private mCurrentDevicePassword:Ljava/lang/String;

.field private mCurrentProfilePassword:Ljava/lang/String;

.field private mFragment:Landroidx/fragment/app/Fragment;

.field private final mHost:Lcom/android/settings/security/MiuiSecurityAndPrivacySettings;

.field private final mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private final mProfileChallengeUserId:I

.field private final mUm:Landroid/os/UserManager;

.field private mUnifyProfile:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    sput v0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->MY_USER_ID:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v0}, Lcom/android/settings/MiuiLockUnificationPreferenceController;-><init>(Landroid/content/Context;Lcom/android/settings/security/MiuiSecurityAndPrivacySettings;Landroidx/fragment/app/Fragment;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/settings/security/MiuiSecurityAndPrivacySettings;Landroidx/fragment/app/Fragment;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mFragment:Landroidx/fragment/app/Fragment;

    iput-object p2, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mHost:Lcom/android/settings/security/MiuiSecurityAndPrivacySettings;

    const-string/jumbo p2, "user"

    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/os/UserManager;

    iput-object p2, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mUm:Landroid/os/UserManager;

    new-instance p3, Lcom/android/internal/widget/LockPatternUtils;

    invoke-direct {p3, p1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V

    iput-object p3, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    new-instance p3, Lcom/android/settings/MiuiChooseLockSettingsHelper;

    invoke-direct {p3, p1}, Lcom/android/settings/MiuiChooseLockSettingsHelper;-><init>(Landroid/content/Context;)V

    iput-object p3, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mChooseLockSettingsHelper:Lcom/android/settings/MiuiChooseLockSettingsHelper;

    sget p1, Lcom/android/settings/MiuiLockUnificationPreferenceController;->MY_USER_ID:I

    invoke-static {p2, p1}, Lcom/android/settings/Utils;->getManagedProfileId(Landroid/os/UserManager;I)I

    move-result p1

    iput p1, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mProfileChallengeUserId:I

    return-void
.end method

.method private launchConfirmProfileLockForUnification()V
    .locals 8

    iget-object v0, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mContext:Landroid/content/Context;

    sget v1, Lcom/android/settings/R$string;->unlock_set_unlock_launch_picker_title_profile:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v1, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mProfileChallengeUserId:I

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardStoredPasswordQuality(I)I

    move-result v5

    if-eqz v5, :cond_0

    iget-object v3, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mFragment:Landroidx/fragment/app/Fragment;

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mChooseLockSettingsHelper:Lcom/android/settings/MiuiChooseLockSettingsHelper;

    iget v4, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mProfileChallengeUserId:I

    const/16 v6, 0x81

    invoke-virtual/range {v2 .. v7}, Lcom/android/settings/MiuiChooseLockSettingsHelper;->launchConfirmationActivity(Landroidx/fragment/app/Fragment;IIILjava/lang/String;)Z

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/MiuiLockUnificationPreferenceController;->unifyLocks()V

    :goto_0
    return-void
.end method

.method private unifyLocks()V
    .locals 8

    iget-object v0, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v1, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mProfileChallengeUserId:I

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardStoredPasswordQuality(I)I

    move-result v6

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/high16 v2, 0x40000

    if-eq v2, v6, :cond_1

    const/high16 v2, 0x50000

    if-eq v2, v6, :cond_1

    const/high16 v2, 0x60000

    if-ne v2, v6, :cond_0

    goto :goto_0

    :cond_0
    move v2, v1

    goto :goto_1

    :cond_1
    :goto_0
    move v2, v0

    :goto_1
    const/high16 v3, 0x10000

    if-ne v6, v3, :cond_2

    iget-object v0, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget-object v2, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mCurrentProfilePassword:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/android/settings/compat/LockPatternUtilsCompat;->stringToPattern(Lcom/android/internal/widget/LockPatternUtils;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mCurrentDevicePassword:Ljava/lang/String;

    sget v4, Lcom/android/settings/MiuiLockUnificationPreferenceController;->MY_USER_ID:I

    invoke-static {v0, v2, v3, v4, v1}, Lcom/android/settings/compat/LockPatternUtilsCompat;->saveLockPattern(Lcom/android/internal/widget/LockPatternUtils;Ljava/util/List;Ljava/lang/String;IZ)V

    goto :goto_2

    :cond_2
    new-instance v3, Landroid/security/MiuiLockPatternUtils;

    iget-object v4, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/security/MiuiLockPatternUtils;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mCurrentProfilePassword:Ljava/lang/String;

    xor-int/2addr v0, v2

    iget-object v5, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mCurrentDevicePassword:Ljava/lang/String;

    sget v7, Lcom/android/settings/MiuiLockUnificationPreferenceController;->MY_USER_ID:I

    move-object v2, v3

    move-object v3, v4

    move v4, v0

    invoke-static/range {v2 .. v7}, Lcom/android/settings/compat/LockPatternUtilsCompat;->saveLockPassword(Lcom/android/internal/widget/LockPatternUtils;Ljava/lang/String;ZLjava/lang/String;II)V

    :goto_2
    iget-object v0, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v2, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mProfileChallengeUserId:I

    iget-object v3, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mCurrentProfilePassword:Ljava/lang/String;

    invoke-static {v0, v2, v1, v3}, Lcom/android/settings/compat/LockPatternUtilsCompat;->setSeparateProfileChallengeEnabled(Lcom/android/internal/widget/LockPatternUtils;IZLjava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v1, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mProfileChallengeUserId:I

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->isVisiblePatternEnabled(I)Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    sget v2, Lcom/android/settings/MiuiLockUnificationPreferenceController;->MY_USER_ID:I

    invoke-virtual {v1, v0, v2}, Lcom/android/internal/widget/LockPatternUtils;->setVisiblePatternEnabled(ZI)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mCurrentDevicePassword:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mCurrentProfilePassword:Ljava/lang/String;

    return-void
.end method

.method private ununifyLocks()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mFragment:Landroidx/fragment/app/Fragment;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget v1, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mProfileChallengeUserId:I

    const-string v2, "android.intent.extra.USER_ID"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object p0, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mFragment:Landroidx/fragment/app/Fragment;

    const-class v1, Lcom/android/settings/MiuiSecurityChooseUnlock$MiuiSecurityChooseUnlockFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget v3, Lcom/android/settings/R$string;->lock_settings_picker_update_profile_lock_title:I

    invoke-static {p0, v1, v2, v0, v3}, Lcom/android/settings/MiuiKeyguardSettingsUtils;->startFragment(Landroidx/fragment/app/Fragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public displayPreference(Landroidx/preference/PreferenceScreen;)V
    .locals 1

    const-string/jumbo v0, "unification"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iput-object p1, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mUnifyProfile:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method

.method public handleActivityResult(IILandroid/content/Intent;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, -0x1

    const/16 v2, 0x82

    if-ne p1, v2, :cond_0

    if-ne p2, v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/MiuiLockUnificationPreferenceController;->ununifyLocks()V

    return v0

    :cond_0
    const/16 v2, 0x80

    const-string/jumbo v3, "password"

    if-ne p1, v2, :cond_1

    if-ne p2, v1, :cond_1

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mCurrentDevicePassword:Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/settings/MiuiLockUnificationPreferenceController;->launchConfirmProfileLockForUnification()V

    return v0

    :cond_1
    const/16 v2, 0x81

    if-ne p1, v2, :cond_2

    if-ne p2, v1, :cond_2

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mCurrentProfilePassword:Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/settings/MiuiLockUnificationPreferenceController;->unifyLocks()V

    return v0

    :cond_2
    const/4 p0, 0x0

    return p0
.end method

.method public isAvailable()Z
    .locals 2

    iget v0, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mProfileChallengeUserId:I

    const/16 v1, -0x2710

    if-eq v0, v1, :cond_0

    iget-object p0, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mUm:Landroid/os/UserManager;

    invoke-virtual {p0, v0}, Landroid/os/UserManager;->isManagedProfile(I)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public launchConfirmDeviceLockForUnification()V
    .locals 8

    iget-object v0, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mContext:Landroid/content/Context;

    sget v1, Lcom/android/settings/R$string;->unlock_set_unlock_launch_picker_title:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    sget v4, Lcom/android/settings/MiuiLockUnificationPreferenceController;->MY_USER_ID:I

    invoke-virtual {v0, v4}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardStoredPasswordQuality(I)I

    move-result v5

    if-eqz v5, :cond_0

    iget-object v3, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mFragment:Landroidx/fragment/app/Fragment;

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mChooseLockSettingsHelper:Lcom/android/settings/MiuiChooseLockSettingsHelper;

    const/16 v6, 0x80

    invoke-virtual/range {v2 .. v7}, Lcom/android/settings/MiuiChooseLockSettingsHelper;->launchConfirmationActivity(Landroidx/fragment/app/Fragment;IIILjava/lang/String;)Z

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/MiuiLockUnificationPreferenceController;->launchConfirmProfileLockForUnification()V

    :goto_0
    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 7

    iget-object p1, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mUm:Landroid/os/UserManager;

    iget v1, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mProfileChallengeUserId:I

    invoke-static {p1, v0, v1}, Lcom/android/settings/Utils;->startQuietModeDialogIfNecessary(Landroid/content/Context;Landroid/os/UserManager;I)Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    return v0

    :cond_0
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    const/4 p2, 0x1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mHost:Lcom/android/settings/security/MiuiSecurityAndPrivacySettings;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v1, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mProfileChallengeUserId:I

    invoke-virtual {p1, v1}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardStoredPasswordQuality(I)I

    move-result p1

    const/high16 v1, 0x10000

    if-lt p1, v1, :cond_1

    iget-object p1, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mUm:Landroid/os/UserManager;

    iget v1, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mProfileChallengeUserId:I

    invoke-static {v1}, Landroid/os/UserHandle;->of(I)Landroid/os/UserHandle;

    move-result-object v1

    const-string/jumbo v2, "no_unified_password"

    invoke-virtual {p1, v2, v1}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;Landroid/os/UserHandle;)Z

    move-result p1

    if-nez p1, :cond_1

    move v0, p2

    :cond_1
    invoke-static {v0}, Lcom/android/settings/MiuiUnificationConfirmationDialog;->newInstance(Z)Lcom/android/settings/MiuiUnificationConfirmationDialog;

    move-result-object p1

    iget-object p0, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mHost:Lcom/android/settings/security/MiuiSecurityAndPrivacySettings;

    invoke-virtual {p1, p0}, Lcom/android/settings/MiuiUnificationConfirmationDialog;->show(Lcom/android/settings/security/MiuiSecurityAndPrivacySettings;)V

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mContext:Landroid/content/Context;

    sget v0, Lcom/android/settings/R$string;->unlock_set_unlock_launch_picker_title:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object p1, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    sget v3, Lcom/android/settings/MiuiLockUnificationPreferenceController;->MY_USER_ID:I

    invoke-virtual {p1, v3}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardStoredPasswordQuality(I)I

    move-result v4

    if-eqz v4, :cond_3

    iget-object v2, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mFragment:Landroidx/fragment/app/Fragment;

    if-eqz v2, :cond_3

    iget-object v1, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mChooseLockSettingsHelper:Lcom/android/settings/MiuiChooseLockSettingsHelper;

    const/16 v5, 0x82

    invoke-virtual/range {v1 .. v6}, Lcom/android/settings/MiuiChooseLockSettingsHelper;->launchConfirmationActivity(Landroidx/fragment/app/Fragment;IIILjava/lang/String;)Z

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/android/settings/MiuiLockUnificationPreferenceController;->ununifyLocks()V

    :goto_0
    return p2
.end method

.method public unifyUncompliantLocks()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v1, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mProfileChallengeUserId:I

    iget-object v2, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mCurrentProfilePassword:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v0, v1, v3, v2}, Lcom/android/settings/compat/LockPatternUtilsCompat;->setSeparateProfileChallengeEnabled(Lcom/android/internal/widget/LockPatternUtils;IZLjava/lang/String;)V

    iget-object p0, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mFragment:Landroidx/fragment/app/Fragment;

    if-eqz p0, :cond_0

    const-class v0, Lcom/android/settings/MiuiSecurityChooseUnlock$MiuiSecurityChooseUnlockFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    sget v2, Lcom/android/settings/R$string;->lock_settings_picker_title:I

    invoke-static {p0, v0, v3, v1, v2}, Lcom/android/settings/MiuiKeyguardSettingsUtils;->startFragment(Landroidx/fragment/app/Fragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z

    :cond_0
    return-void
.end method

.method public updateState()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mUnifyProfile:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    iget v1, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mProfileChallengeUserId:I

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternUtils;->isSeparateProfileChallengeEnabled(I)Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mUnifyProfile:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    xor-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mUnifyProfile:Lcom/android/settingslib/MiuiRestrictedSwitchPreference;

    iget-object v1, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mContext:Landroid/content/Context;

    iget p0, p0, Lcom/android/settings/MiuiLockUnificationPreferenceController;->mProfileChallengeUserId:I

    const-string/jumbo v2, "no_unified_password"

    invoke-static {v1, v2, p0}, Lcom/android/settings/compat/RestrictedLockUtilsCompat;->checkIfRestrictionEnforced(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/android/settingslib/RestrictedSwitchPreference;->setDisabledByAdmin(Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;)V

    :cond_0
    return-void
.end method
