.class public Lcom/android/settings/MiuiSettings$HeaderAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/MiuiSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "HeaderAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/android/settings/MiuiSettings$HeaderViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private isNightMode:Z

.field private mAuthHelper:Lcom/android/settingslib/accounts/AuthenticatorHelper;

.field private mContext:Landroid/content/Context;

.field private mHeaders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;",
            ">;"
        }
    .end annotation
.end field

.field private mInflater:Landroid/view/LayoutInflater;

.field private mIsFrequently:Z

.field private mIsMIUILite:Z

.field private mLocale:Ljava/util/Locale;

.field private mMiHomeManager:Lcom/android/settings/cust/MiHomeManager;

.field private mSettingsControllerMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Lcom/android/settings/BaseSettingsController;",
            ">;"
        }
    .end annotation
.end field

.field private mUiManager:Landroid/app/UiModeManager;

.field final synthetic this$0:Lcom/android/settings/MiuiSettings;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/settings/MiuiSettings$HeaderAdapter;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHeaders(Lcom/android/settings/MiuiSettings$HeaderAdapter;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mHeaders:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMiHomeManager(Lcom/android/settings/MiuiSettings$HeaderAdapter;)Lcom/android/settings/cust/MiHomeManager;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mMiHomeManager:Lcom/android/settings/cust/MiHomeManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mupdateAdminDisallowedConfig(Lcom/android/settings/MiuiSettings$HeaderAdapter;Lcom/android/settings/MiuiSettings$ProxyHeaderViewAdapter;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiSettings$HeaderAdapter;->updateAdminDisallowedConfig(Lcom/android/settings/MiuiSettings$ProxyHeaderViewAdapter;)V

    return-void
.end method

.method public constructor <init>(Lcom/android/settings/MiuiSettings;Lmiuix/appcompat/app/AppCompatActivity;Ljava/util/List;Lcom/android/settingslib/accounts/AuthenticatorHelper;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiuix/appcompat/app/AppCompatActivity;",
            "Ljava/util/List<",
            "Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;",
            ">;",
            "Lcom/android/settingslib/accounts/AuthenticatorHelper;",
            "Z)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    invoke-virtual {p2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mHeaders:Ljava/util/List;

    iput-object p4, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mAuthHelper:Lcom/android/settingslib/accounts/AuthenticatorHelper;

    const-string p1, "layout_inflater"

    invoke-virtual {p2, p1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/LayoutInflater;

    iput-object p1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mInflater:Landroid/view/LayoutInflater;

    iput-boolean p5, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mIsFrequently:Z

    iget-object p1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/cust/MiHomeManager;->getInstance(Landroid/content/Context;)Lcom/android/settings/cust/MiHomeManager;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mMiHomeManager:Lcom/android/settings/cust/MiHomeManager;

    invoke-virtual {p2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    iget-object p1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iput-object p1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mLocale:Ljava/util/Locale;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mSettingsControllerMap:Ljava/util/HashMap;

    sget p3, Lcom/android/settings/R$id;->wifi_settings:I

    int-to-long p3, p3

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    new-instance p4, Lcom/android/settings/wifi/WifiStatusController;

    const/4 p5, 0x0

    invoke-direct {p4, p2, p5}, Lcom/android/settings/wifi/WifiStatusController;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    invoke-virtual {p1, p3, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mSettingsControllerMap:Ljava/util/HashMap;

    sget p3, Lcom/android/settings/R$id;->bluetooth_settings:I

    int-to-long p3, p3

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    new-instance p4, Lcom/android/settings/bluetooth/BluetoothStatusController;

    invoke-direct {p4, p2, p5}, Lcom/android/settings/bluetooth/BluetoothStatusController;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    invoke-virtual {p1, p3, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mSettingsControllerMap:Ljava/util/HashMap;

    sget p3, Lcom/android/settings/R$id;->wifi_tether_settings:I

    int-to-long p3, p3

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    new-instance p4, Lcom/android/settings/restriction/TetherRestrictionController;

    invoke-direct {p4, p2, p5}, Lcom/android/settings/restriction/TetherRestrictionController;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    invoke-virtual {p1, p3, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-boolean p1, Lmiui/os/Build;->IS_ALPHA_BUILD:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mSettingsControllerMap:Ljava/util/HashMap;

    sget p3, Lcom/android/settings/R$id;->micloud_settings:I

    int-to-long p3, p3

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    new-instance p4, Lcom/android/settings/accounts/XiaomiAccountStatusController;

    invoke-direct {p4, p2, p5}, Lcom/android/settings/accounts/XiaomiAccountStatusController;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    invoke-virtual {p1, p3, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mSettingsControllerMap:Ljava/util/HashMap;

    sget p3, Lcom/android/settings/R$id;->mi_account_settings:I

    int-to-long p3, p3

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    new-instance p4, Lcom/android/settings/accounts/XiaomiAccountInfoController;

    invoke-direct {p4, p2, p5}, Lcom/android/settings/accounts/XiaomiAccountInfoController;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    invoke-virtual {p1, p3, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    iget-object p1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mSettingsControllerMap:Ljava/util/HashMap;

    sget p3, Lcom/android/settings/R$id;->font_settings:I

    int-to-long p3, p3

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    new-instance p4, Lcom/android/settings/display/FontStatusController;

    invoke-direct {p4, p2, p5}, Lcom/android/settings/display/FontStatusController;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    invoke-virtual {p1, p3, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mSettingsControllerMap:Ljava/util/HashMap;

    sget p3, Lcom/android/settings/R$id;->my_device:I

    int-to-long p3, p3

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    new-instance p4, Lcom/android/settings/device/DeviceStatusController;

    invoke-direct {p4, p2, p5}, Lcom/android/settings/device/DeviceStatusController;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    invoke-virtual {p1, p3, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mSettingsControllerMap:Ljava/util/HashMap;

    sget p3, Lcom/android/settings/R$id;->system_apps_updater:I

    int-to-long p3, p3

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    new-instance p4, Lcom/android/settings/applications/SystemAppUpdaterStatusController;

    iget-object v0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mLocale:Ljava/util/Locale;

    invoke-direct {p4, p2, p5, v0}, Lcom/android/settings/applications/SystemAppUpdaterStatusController;-><init>(Landroid/content/Context;Landroid/widget/TextView;Ljava/util/Locale;)V

    invoke-virtual {p1, p3, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mSettingsControllerMap:Ljava/util/HashMap;

    sget p3, Lcom/android/settings/R$id;->msim_settings:I

    int-to-long p3, p3

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    new-instance p4, Lcom/android/settings/restriction/SimManagementRestrictionController;

    invoke-direct {p4, p2, p5}, Lcom/android/settings/restriction/SimManagementRestrictionController;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    invoke-virtual {p1, p3, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mContext:Landroid/content/Context;

    const-string/jumbo p2, "uimode"

    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/UiModeManager;

    iput-object p1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mUiManager:Landroid/app/UiModeManager;

    iget-object p1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/Utils;->isNightMode(Landroid/content/Context;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->isNightMode:Z

    iget-object p1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settings/device/MiuiAboutPhoneUtils;->getInstance(Landroid/content/Context;)Lcom/android/settings/device/MiuiAboutPhoneUtils;

    move-result-object p1

    invoke-virtual {p1}, Lcom/android/settings/device/MiuiAboutPhoneUtils;->isMIUILite()Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mIsMIUILite:Z

    return-void
.end method

.method private getHeaderType(Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;)I
    .locals 4

    iget-object v0, p1, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    iget-wide v0, p1, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->id:J

    sget v2, Lcom/android/settings/R$id;->my_device:I

    int-to-long v2, v2

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    const/4 p0, 0x2

    return p0

    :cond_1
    invoke-virtual {p0, v0, v1}, Lcom/android/settings/MiuiSettings$HeaderAdapter;->isWirelessHeader(J)Z

    move-result p0

    if-eqz p0, :cond_2

    const/4 p0, 0x3

    return p0

    :cond_2
    iget-wide p0, p1, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->id:J

    sget v0, Lcom/android/settings/R$id;->system_apps_updater:I

    int-to-long v0, v0

    cmp-long p0, p0, v0

    if-nez p0, :cond_3

    const/4 p0, 0x5

    return p0

    :cond_3
    const/4 p0, 0x1

    return p0
.end method

.method private isAdapterVerticalSummary(Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;)Z
    .locals 3

    iget-object p0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mContext:Landroid/content/Context;

    invoke-static {p0}, Lcom/android/settings/MiuiSettings;->isDeviceAdapterVerticalSummary(Landroid/content/Context;)Z

    move-result p0

    const/4 v0, 0x0

    if-eqz p0, :cond_1

    iget-wide p0, p1, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->id:J

    sget v1, Lcom/android/settings/R$id;->my_device:I

    int-to-long v1, v1

    cmp-long v1, p0, v1

    if-eqz v1, :cond_0

    sget v1, Lcom/android/settings/R$id;->wifi_settings:I

    int-to-long v1, v1

    cmp-long v1, p0, v1

    if-eqz v1, :cond_0

    sget v1, Lcom/android/settings/R$id;->bluetooth_settings:I

    int-to-long v1, v1

    cmp-long p0, p0, v1

    if-nez p0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private setEnable(Lcom/android/settings/MiuiSettings$HeaderViewHolder;Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;)V
    .locals 6

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-wide v0, p2, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->id:J

    sget v2, Lcom/android/settings/R$id;->my_device:I

    int-to-long v2, v2

    cmp-long v2, v0, v2

    const/4 v3, 0x1

    if-eqz v2, :cond_6

    sget v2, Lcom/android/settings/R$id;->mi_account_settings:I

    int-to-long v4, v2

    cmp-long v2, v0, v4

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    sget p0, Lcom/android/settings/R$id;->msim_settings:I

    int-to-long v4, p0

    cmp-long p2, v0, v4

    if-nez p2, :cond_2

    goto :goto_2

    :cond_2
    int-to-long v4, p0

    cmp-long p0, v0, v4

    if-eqz p0, :cond_5

    sget p0, Lcom/android/settings/R$id;->mobile_network_settings:I

    int-to-long v4, p0

    cmp-long p0, v0, v4

    if-nez p0, :cond_3

    goto :goto_0

    :cond_3
    iget-object p0, p1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->title:Landroid/widget/TextView;

    if-eqz p0, :cond_4

    invoke-virtual {p0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_4
    iget-object p0, p1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->summary:Landroid/widget/TextView;

    if-eqz p0, :cond_7

    invoke-virtual {p0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_2

    :cond_5
    :goto_0
    iget-object p0, p1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object p0, p1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->summary:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_2

    :cond_6
    :goto_1
    iget-object v0, p1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->summary:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-direct {p0, p2}, Lcom/android/settings/MiuiSettings$HeaderAdapter;->isAdapterVerticalSummary(Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;)Z

    move-result p0

    if-eqz p0, :cond_7

    iget-object p0, p1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->summary:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_7
    :goto_2
    return-void
.end method

.method private setExtraPadding(Lcom/android/settings/MiuiSettings$HeaderViewHolder;Landroid/view/View;Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;)V
    .locals 8

    if-nez p2, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mContext:Landroid/content/Context;

    const/high16 v1, 0x41e00000    # 28.0f

    invoke-static {v0, v1}, Lcom/android/settings/MiuiUtils;->dp2px(Landroid/content/Context;F)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-static {v1}, Lcom/android/settings/MiuiSettings;->-$$Nest$fgetmAccountIconSize(Lcom/android/settings/MiuiSettings;)I

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-static {v1}, Lcom/android/settings/MiuiSettings;->-$$Nest$fgetmNormalIconSize(Lcom/android/settings/MiuiSettings;)I

    move-result v1

    if-nez v1, :cond_3

    :cond_1
    iget-object v1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mContext:Landroid/content/Context;

    sget v2, Lcom/android/settings/R$drawable;->ic_account_avatar:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-static {v2, v1}, Lcom/android/settings/MiuiSettings;->-$$Nest$fputmAccountIconSize(Lcom/android/settings/MiuiSettings;I)V

    :cond_2
    iget-object v1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    iget-object v2, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/android/settings/R$dimen;->header_icon_size:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/settings/MiuiSettings;->-$$Nest$fputmNormalIconSize(Lcom/android/settings/MiuiSettings;I)V

    :cond_3
    iget-object v1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-static {v1}, Lcom/android/settings/MiuiSettings;->-$$Nest$fgetmAccountIconSize(Lcom/android/settings/MiuiSettings;)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3f800000    # 1.0f

    mul-float/2addr v1, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v1, v3

    iget-object v4, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-static {v4}, Lcom/android/settings/MiuiSettings;->-$$Nest$fgetmNormalIconSize(Lcom/android/settings/MiuiSettings;)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v2

    div-float/2addr v4, v3

    float-to-int v2, v4

    add-int v3, v0, v2

    float-to-int v1, v1

    sub-int/2addr v3, v1

    iget-object p0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mContext:Landroid/content/Context;

    const/high16 v4, 0x41600000    # 14.0f

    invoke-static {p0, v4}, Lcom/android/settings/MiuiUtils;->dp2px(Landroid/content/Context;F)I

    move-result p0

    add-int/2addr v2, p0

    sub-int/2addr v2, v1

    iget-wide v4, p3, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->id:J

    sget p3, Lcom/android/settings/R$id;->mi_account_settings:I

    int-to-long v6, p3

    cmp-long p3, v4, v6

    const/4 v1, 0x0

    if-nez p3, :cond_4

    iget-object p0, p1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->icon:Landroid/widget/ImageView;

    invoke-virtual {p0, v3, v1, v2, v1}, Landroid/widget/ImageView;->setPaddingRelative(IIII)V

    invoke-virtual {p2}, Landroid/view/View;->getPaddingTop()I

    move-result p0

    invoke-virtual {p2}, Landroid/view/View;->getPaddingBottom()I

    move-result p1

    invoke-virtual {p2, v1, p0, v0, p1}, Landroid/view/View;->setPaddingRelative(IIII)V

    goto :goto_0

    :cond_4
    iget-object p1, p1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->icon:Landroid/widget/ImageView;

    invoke-virtual {p1, v1, v1, p0, v1}, Landroid/widget/ImageView;->setPaddingRelative(IIII)V

    invoke-virtual {p2}, Landroid/view/View;->getPaddingTop()I

    move-result p0

    invoke-virtual {p2}, Landroid/view/View;->getPaddingBottom()I

    move-result p1

    invoke-virtual {p2, v0, p0, v0, p1}, Landroid/view/View;->setPaddingRelative(IIII)V

    :goto_0
    return-void
.end method

.method private setRestrictionEnforced(Lcom/android/settings/MiuiSettings$HeaderViewHolder;Z)V
    .locals 4

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object p0, p1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->value:Landroid/widget/TextView;

    const/16 v0, 0x4d

    const/16 v1, 0xff

    if-eqz p0, :cond_2

    invoke-virtual {p0}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v2

    if-eqz p2, :cond_1

    move v3, v0

    goto :goto_0

    :cond_1
    move v3, v1

    :goto_0
    invoke-virtual {v2, v3}, Landroid/content/res/ColorStateList;->withAlpha(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    :cond_2
    iget-object p0, p1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->title:Landroid/widget/TextView;

    if-eqz p0, :cond_4

    invoke-virtual {p0}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v2

    if-eqz p2, :cond_3

    move v3, v0

    goto :goto_1

    :cond_3
    move v3, v1

    :goto_1
    invoke-virtual {v2, v3}, Landroid/content/res/ColorStateList;->withAlpha(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    :cond_4
    iget-object p0, p1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->icon:Landroid/widget/ImageView;

    if-eqz p0, :cond_6

    if-eqz p2, :cond_5

    goto :goto_2

    :cond_5
    move v0, v1

    :goto_2
    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageAlpha(I)V

    :cond_6
    return-void
.end method

.method private setSelectedHeaderView(Lcom/android/settings/MiuiSettings$HeaderViewHolder;I)V
    .locals 2

    if-eqz p1, :cond_2

    sget-boolean v0, Lcom/android/settings/utils/TabletUtils;->IS_TABLET:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-static {v0}, Lcom/android/settings/utils/SettingsFeatures;->isSplitTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-static {v0}, Lcom/android/settings/MiuiSettings;->-$$Nest$fgetmCurrentSelectedHeaderIndex(Lcom/android/settings/MiuiSettings;)I

    move-result v0

    const/4 v1, 0x0

    if-ne v0, p2, :cond_1

    invoke-direct {p0, v1}, Lcom/android/settings/MiuiSettings$HeaderAdapter;->setSelectorColor(I)V

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiSettings$HeaderAdapter;->setSelectedView(Landroid/view/View;)V

    invoke-static {}, Lcom/android/settings/MiuiSettings;->-$$Nest$sfgetSELECTOR_COLOR()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiSettings$HeaderAdapter;->setSelectorColor(I)V

    goto :goto_0

    :cond_1
    iget-object p0, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {p0, v1}, Landroid/view/View;->setSelected(Z)V

    :cond_2
    :goto_0
    return-void
.end method

.method private setSelectedView(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-static {v0}, Lcom/android/settings/MiuiSettings;->-$$Nest$fgetmSelectedView(Lcom/android/settings/MiuiSettings;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-static {v0}, Lcom/android/settings/MiuiSettings;->-$$Nest$fgetmSelectedView(Lcom/android/settings/MiuiSettings;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    :cond_0
    if-eqz p1, :cond_1

    iget-object p0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-static {p0, p1}, Lcom/android/settings/MiuiSettings;->-$$Nest$fputmSelectedView(Lcom/android/settings/MiuiSettings;Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method private setSelectorColor(I)V
    .locals 0

    iget-object p1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-static {p1}, Lcom/android/settings/MiuiSettings;->-$$Nest$fgetmSelectedView(Lcom/android/settings/MiuiSettings;)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-static {p0}, Lcom/android/settings/MiuiSettings;->-$$Nest$fgetmSelectedView(Lcom/android/settings/MiuiSettings;)Landroid/view/View;

    move-result-object p0

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Landroid/view/View;->setSelected(Z)V

    :cond_0
    return-void
.end method

.method private updateAdminDisallowItem(IZ)V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mHeaders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;

    iget-object v0, v0, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->extras:Landroid/os/Bundle;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    :cond_0
    iget-object v1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mHeaders:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;

    iput-object v0, v1, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->extras:Landroid/os/Bundle;

    iget-object p0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mHeaders:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;

    iget-object p0, p0, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->extras:Landroid/os/Bundle;

    const-string p1, "admin_disallow"

    invoke-virtual {p0, p1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method private updateAdminDisallowedConfig(Lcom/android/settings/MiuiSettings$ProxyHeaderViewAdapter;)V
    .locals 7

    iget-object v0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mContext:Landroid/content/Context;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    const-string/jumbo v2, "no_config_mobile_networks"

    invoke-static {v0, v2, v1}, Lcom/android/settingslib/RestrictedLockUtilsInternal;->checkIfRestrictionEnforced(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    iget-object v3, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mContext:Landroid/content/Context;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    const-string/jumbo v5, "no_config_tethering"

    invoke-static {v3, v5, v4}, Lcom/android/settingslib/RestrictedLockUtilsInternal;->checkIfRestrictionEnforced(Landroid/content/Context;Ljava/lang/String;I)Lcom/android/settingslib/RestrictedLockUtils$EnforcedAdmin;

    move-result-object v3

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    move v1, v2

    :goto_1
    iget-object v3, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mHeaders:Ljava/util/List;

    if-eqz v3, :cond_6

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    if-nez p1, :cond_2

    goto :goto_4

    :cond_2
    :goto_2
    iget-object v3, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mHeaders:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_6

    iget-object v3, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mHeaders:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;

    if-nez v3, :cond_3

    goto :goto_3

    :cond_3
    iget-wide v3, v3, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->id:J

    sget v5, Lcom/android/settings/R$id;->msim_settings:I

    int-to-long v5, v5

    cmp-long v5, v3, v5

    if-nez v5, :cond_4

    invoke-direct {p0, v2, v0}, Lcom/android/settings/MiuiSettings$HeaderAdapter;->updateAdminDisallowItem(IZ)V

    invoke-virtual {p1, v2}, Lcom/android/settings/MiuiSettings$ProxyHeaderViewAdapter;->updateItem(I)V

    goto :goto_3

    :cond_4
    sget v5, Lcom/android/settings/R$id;->wifi_tether_settings:I

    int-to-long v5, v5

    cmp-long v3, v3, v5

    if-nez v3, :cond_5

    invoke-direct {p0, v2, v1}, Lcom/android/settings/MiuiSettings$HeaderAdapter;->updateAdminDisallowItem(IZ)V

    invoke-virtual {p1, v2}, Lcom/android/settings/MiuiSettings$ProxyHeaderViewAdapter;->updateItem(I)V

    :cond_5
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_6
    :goto_4
    return-void
.end method


# virtual methods
.method public getItem(I)Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mHeaders:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;

    return-object p0
.end method

.method public getItemCount()I
    .locals 0

    iget-object p0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mHeaders:Ljava/util/List;

    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public getItemId(I)J
    .locals 0

    int-to-long p0, p1

    return-wide p0
.end method

.method public getItemViewType(I)I
    .locals 0

    invoke-virtual {p0, p1}, Lcom/android/settings/MiuiSettings$HeaderAdapter;->getItem(I)Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/android/settings/MiuiSettings$HeaderAdapter;->getHeaderType(Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;)I

    move-result p0

    return p0
.end method

.method public isWirelessHeader(J)Z
    .locals 0

    iget-object p0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mSettingsControllerMap:Ljava/util/HashMap;

    invoke-virtual {p0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object p0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {p0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    check-cast p1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/MiuiSettings$HeaderAdapter;->onBindViewHolder(Lcom/android/settings/MiuiSettings$HeaderViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/android/settings/MiuiSettings$HeaderViewHolder;I)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    if-ltz v2, :cond_18

    iget-object v3, v0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mHeaders:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v2, v3, :cond_0

    goto/16 :goto_9

    :cond_0
    invoke-virtual {v0, v2}, Lcom/android/settings/MiuiSettings$HeaderAdapter;->getItem(I)Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/android/settings/MiuiSettings$HeaderAdapter;->getHeaderType(Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;)I

    move-result v4

    iget-object v5, v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const/16 v6, 0x8

    const/4 v7, 0x1

    const/4 v8, 0x0

    if-eqz v4, :cond_f

    const/4 v9, -0x1

    const/4 v10, 0x2

    const/4 v11, 0x0

    if-eq v4, v7, :cond_3

    if-eq v4, v10, :cond_9

    const/4 v12, 0x3

    if-eq v4, v12, :cond_1

    const/4 v12, 0x5

    if-eq v4, v12, :cond_5

    goto/16 :goto_8

    :cond_1
    iget-object v12, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->value:Landroid/widget/TextView;

    invoke-virtual {v12, v11, v11, v11, v11}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object v12, v0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mSettingsControllerMap:Ljava/util/HashMap;

    iget-wide v13, v3, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->id:J

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/android/settings/BaseSettingsController;

    if-eqz v12, :cond_3

    invoke-direct {v0, v3}, Lcom/android/settings/MiuiSettings$HeaderAdapter;->isAdapterVerticalSummary(Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;)Z

    move-result v13

    if-eqz v13, :cond_2

    iget-object v13, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->summary:Landroid/widget/TextView;

    invoke-virtual {v12, v13}, Lcom/android/settings/BaseSettingsController;->setStatusView(Landroid/widget/TextView;)V

    iget-object v12, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->summary:Landroid/widget/TextView;

    sget v13, Lcom/android/settings/R$style;->Miuix_AppCompat_TextAppearance_PreferenceRight:I

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setTextAppearance(I)V

    goto :goto_0

    :cond_2
    iget-object v13, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->value:Landroid/widget/TextView;

    invoke-virtual {v12, v13}, Lcom/android/settings/BaseSettingsController;->setStatusView(Landroid/widget/TextView;)V

    iget-object v13, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->value:Landroid/widget/TextView;

    invoke-virtual {v13, v12}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    :cond_3
    :goto_0
    iget-object v12, v0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-virtual {v12}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v12

    iget-object v13, v0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-virtual {v13}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-virtual {v3, v13}, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->getTitle(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v13

    const-string/jumbo v14, "system_app"

    invoke-virtual {v14, v13}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_4

    iget-object v13, v3, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->intent:Landroid/content/Intent;

    invoke-virtual {v12, v13, v8}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v13

    iget-object v14, v13, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v14, v14, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v14, v14, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v15, "."

    const-string v10, "_"

    invoke-virtual {v14, v15, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    iget-object v13, v13, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v13, v12}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v12

    iput-object v12, v3, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    iget-object v12, v0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-virtual {v12}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    iget-object v13, v0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-virtual {v13}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v13

    const-string v14, "drawable"

    invoke-virtual {v12, v10, v14, v13}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v10

    iput v10, v3, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->iconRes:I

    :cond_4
    iget-wide v12, v3, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->id:J

    sget v10, Lcom/android/settings/R$id;->wifi_settings:I

    int-to-long v14, v10

    cmp-long v10, v12, v14

    if-nez v10, :cond_5

    iget-object v10, v0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-static {v10}, Lcom/android/settings/MiuiSettings;->-$$Nest$fgetmCurrentSelectedHeaderIndex(Lcom/android/settings/MiuiSettings;)I

    move-result v10

    if-ne v10, v9, :cond_5

    iget-object v10, v0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-static {v10, v2}, Lcom/android/settings/MiuiSettings;->-$$Nest$fputmCurrentSelectedHeaderIndex(Lcom/android/settings/MiuiSettings;I)V

    :cond_5
    iget-wide v12, v3, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->id:J

    sget v10, Lcom/android/settings/R$id;->system_apps_updater:I

    int-to-long v14, v10

    cmp-long v10, v12, v14

    if-nez v10, :cond_7

    iget-object v10, v0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mSettingsControllerMap:Ljava/util/HashMap;

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/settings/BaseSettingsController;

    iget-object v11, v0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-virtual {v11}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string/jumbo v12, "updatable_system_app_count"

    invoke-static {v11, v12, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v11

    iget-object v12, v0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-static {v12}, Lcom/android/settings/device/UpdateBroadcastManager;->getAppsAutoUpdateSuperscript(Landroid/content/Context;)I

    move-result v12

    add-int/2addr v11, v12

    iget-object v12, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->value:Landroid/widget/TextView;

    if-lez v11, :cond_6

    move v13, v8

    goto :goto_1

    :cond_6
    move v13, v6

    :goto_1
    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v12, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->value:Landroid/widget/TextView;

    iget-object v13, v0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mLocale:Ljava/util/Locale;

    invoke-static {v13}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v13

    int-to-long v14, v11

    invoke-virtual {v13, v14, v15}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v12, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v11, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->value:Landroid/widget/TextView;

    sget v12, Lcom/android/settings/R$drawable;->tv_shape_circle:I

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v11, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->value:Landroid/widget/TextView;

    const/16 v12, 0x11

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v11, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->value:Landroid/widget/TextView;

    invoke-virtual {v11, v9}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v9, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->value:Landroid/widget/TextView;

    const/high16 v11, 0x41400000    # 12.0f

    invoke-virtual {v9, v7, v11}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v9, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->value:Landroid/widget/TextView;

    const-string/jumbo v11, "miui-light"

    invoke-static {v11, v8}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v11

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v9, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->value:Landroid/widget/TextView;

    invoke-virtual {v10, v9}, Lcom/android/settings/BaseSettingsController;->setStatusView(Landroid/widget/TextView;)V

    goto :goto_2

    :cond_7
    iget-object v9, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->value:Landroid/widget/TextView;

    if-eqz v9, :cond_9

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v9, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->value:Landroid/widget/TextView;

    const v10, 0x800005

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v9, v0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mSettingsControllerMap:Ljava/util/HashMap;

    iget-wide v10, v3, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->id:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/settings/BaseSettingsController;

    if-eqz v9, :cond_9

    invoke-direct {v0, v3}, Lcom/android/settings/MiuiSettings$HeaderAdapter;->isAdapterVerticalSummary(Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;)Z

    move-result v10

    if-eqz v10, :cond_8

    iget-object v10, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->summary:Landroid/widget/TextView;

    invoke-virtual {v10, v8}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v10, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->summary:Landroid/widget/TextView;

    sget v11, Lcom/android/settings/R$style;->Miuix_AppCompat_TextAppearance_PreferenceRight:I

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setTextAppearance(I)V

    iget-object v10, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->summary:Landroid/widget/TextView;

    invoke-virtual {v9, v10}, Lcom/android/settings/BaseSettingsController;->setStatusView(Landroid/widget/TextView;)V

    goto :goto_2

    :cond_8
    iget-object v10, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->value:Landroid/widget/TextView;

    sget v11, Lcom/android/settings/R$style;->Miuix_AppCompat_TextAppearance_PreferenceRight:I

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setTextAppearance(I)V

    iget-object v10, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->value:Landroid/widget/TextView;

    invoke-virtual {v10, v8}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v10, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->value:Landroid/widget/TextView;

    invoke-virtual {v9, v10}, Lcom/android/settings/BaseSettingsController;->setStatusView(Landroid/widget/TextView;)V

    :cond_9
    :goto_2
    iget-object v9, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->title:Landroid/widget/TextView;

    iget-object v10, v0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-virtual {v10}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v3, v10}, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->getTitle(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v9, v0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-virtual {v9}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->getSummary(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_a

    iget-object v10, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->summary:Landroid/widget/TextView;

    invoke-virtual {v10, v8}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v10, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->summary:Landroid/widget/TextView;

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_a
    iget-object v9, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->summary:Landroid/widget/TextView;

    invoke-virtual {v9, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_3
    invoke-direct {v0, v3}, Lcom/android/settings/MiuiSettings$HeaderAdapter;->isAdapterVerticalSummary(Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;)Z

    move-result v9

    if-eqz v9, :cond_b

    iget-object v9, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->value:Landroid/widget/TextView;

    invoke-virtual {v9, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v6, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->summary:Landroid/widget/TextView;

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_b
    const/4 v6, 0x2

    if-ne v4, v6, :cond_d

    iget-object v4, v0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mSettingsControllerMap:Ljava/util/HashMap;

    iget-wide v9, v3, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->id:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/BaseSettingsController;

    check-cast v4, Lcom/android/settings/device/DeviceStatusController;

    invoke-direct {v0, v3}, Lcom/android/settings/MiuiSettings$HeaderAdapter;->isAdapterVerticalSummary(Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;)Z

    move-result v6

    if-eqz v6, :cond_c

    iget-object v6, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->summary:Landroid/widget/TextView;

    goto :goto_4

    :cond_c
    iget-object v6, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->value:Landroid/widget/TextView;

    :goto_4
    iget-object v9, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->arrowRight:Landroid/widget/ImageView;

    invoke-virtual {v4, v6, v9, v1}, Lcom/android/settings/device/DeviceStatusController;->setUpTextView(Landroid/widget/TextView;Landroid/widget/ImageView;Lcom/android/settings/MiuiSettings$HeaderViewHolder;)V

    :cond_d
    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    iget-wide v9, v3, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->id:J

    sget v4, Lcom/android/settings/R$id;->mi_account_settings:I

    int-to-long v11, v4

    cmp-long v4, v9, v11

    if-nez v4, :cond_e

    iget-object v4, v0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mSettingsControllerMap:Ljava/util/HashMap;

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/settings/BaseSettingsController;

    if-eqz v4, :cond_e

    check-cast v4, Lcom/android/settings/accounts/XiaomiAccountInfoController;

    iget-object v6, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->icon:Landroid/widget/ImageView;

    iget-object v9, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->title:Landroid/widget/TextView;

    iget-object v10, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->summary:Landroid/widget/TextView;

    iget-object v11, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->value:Landroid/widget/TextView;

    invoke-virtual {v4, v6, v9, v10, v11}, Lcom/android/settings/accounts/XiaomiAccountInfoController;->setUpTextView(Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;)V

    :cond_e
    invoke-direct {v0, v1, v5, v3}, Lcom/android/settings/MiuiSettings$HeaderAdapter;->setExtraPadding(Lcom/android/settings/MiuiSettings$HeaderViewHolder;Landroid/view/View;Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;)V

    goto :goto_8

    :cond_f
    iget-object v4, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->title:Landroid/widget/TextView;

    iget-object v9, v0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-virtual {v9}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->getTitle(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_11

    iget-object v4, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    if-eqz v5, :cond_15

    const/4 v4, 0x4

    invoke-virtual {v5, v4}, Landroid/view/View;->setImportantForAccessibility(I)V

    iget-boolean v4, v0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->isNightMode:Z

    if-eqz v4, :cond_10

    sget v4, Lcom/android/settings/R$drawable;->miuix_preference_category_bg_no_title_dark:I

    goto :goto_5

    :cond_10
    sget v4, Lcom/android/settings/R$drawable;->miuix_preference_category_bg_no_title_light:I

    :goto_5
    invoke-virtual {v5, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_8

    :cond_11
    iget-object v4, v1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    if-eqz v5, :cond_15

    iget-boolean v4, v0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->isNightMode:Z

    if-eqz v4, :cond_13

    if-nez v2, :cond_12

    sget v4, Lcom/android/settings/R$drawable;->miuix_preference_category_bg_first_dark:I

    goto :goto_6

    :cond_12
    sget v4, Lcom/android/settings/R$drawable;->miuix_preference_category_background_dark:I

    :goto_6
    invoke-virtual {v5, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_8

    :cond_13
    if-nez v2, :cond_14

    sget v4, Lcom/android/settings/R$drawable;->miuix_preference_category_bg_first_light:I

    goto :goto_7

    :cond_14
    sget v4, Lcom/android/settings/R$drawable;->miuix_preference_category_background_light:I

    :goto_7
    invoke-virtual {v5, v4}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_15
    :goto_8
    invoke-direct/range {p0 .. p2}, Lcom/android/settings/MiuiSettings$HeaderAdapter;->setSelectedHeaderView(Lcom/android/settings/MiuiSettings$HeaderViewHolder;I)V

    invoke-virtual {v0, v1, v3}, Lcom/android/settings/MiuiSettings$HeaderAdapter;->setIcon(Lcom/android/settings/MiuiSettings$HeaderViewHolder;Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;)V

    invoke-direct {v0, v1, v3}, Lcom/android/settings/MiuiSettings$HeaderAdapter;->setEnable(Lcom/android/settings/MiuiSettings$HeaderViewHolder;Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;)V

    iget-object v4, v3, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->extras:Landroid/os/Bundle;

    if-eqz v4, :cond_16

    const-string v5, "admin_disallow"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_16

    move v8, v7

    :cond_16
    if-eqz v8, :cond_17

    invoke-direct {v0, v1, v7}, Lcom/android/settings/MiuiSettings$HeaderAdapter;->setRestrictionEnforced(Lcom/android/settings/MiuiSettings$HeaderViewHolder;Z)V

    :cond_17
    invoke-virtual {v0, v1, v3, v2}, Lcom/android/settings/MiuiSettings$HeaderAdapter;->setClick(Lcom/android/settings/MiuiSettings$HeaderViewHolder;Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;I)V

    :cond_18
    :goto_9
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/MiuiSettings$HeaderAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/android/settings/MiuiSettings$HeaderViewHolder;

    move-result-object p0

    return-object p0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/android/settings/MiuiSettings$HeaderViewHolder;
    .locals 6

    const/4 v0, 0x0

    if-eqz p2, :cond_8

    const v1, 0x1020018

    const/4 v2, 0x1

    if-eq p2, v2, :cond_2

    const/4 v3, 0x2

    if-eq p2, v3, :cond_2

    const/4 v3, 0x3

    if-eq p2, v3, :cond_2

    const/4 v3, 0x5

    if-eq p2, v3, :cond_2

    iget-object v3, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-static {v3}, Lcom/android/settings/utils/SettingsFeatures;->isSplitTablet(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v4, Lcom/android/settings/R$layout;->miuix_preference_navigation_item:I

    invoke-virtual {v3, v4, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v4, Lcom/android/settings/R$layout;->miuix_preference_main_layout:I

    invoke-virtual {v3, v4, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_9

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iget-object v3, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-static {v3}, Lcom/android/settings/utils/SettingsFeatures;->isSplitTablet(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget v3, Lcom/android/settings/R$layout;->miuix_preference_widget_navigation_item_text:I

    invoke-virtual {p1, v3, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    goto/16 :goto_4

    :cond_1
    sget v3, Lcom/android/settings/R$layout;->miuix_preference_widget_text:I

    invoke-virtual {p1, v3, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    sget p1, Lcom/android/settings/R$id;->text_right:I

    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$dimen;->preference_text_right_max_width:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    goto/16 :goto_4

    :cond_2
    iget-object v3, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-static {v3}, Lcom/android/settings/utils/SettingsFeatures;->isSplitTablet(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v4, Lcom/android/settings/R$layout;->miuix_preference_navigation_item:I

    invoke-virtual {v3, v4, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v4, Lcom/android/settings/R$layout;->miuix_preference_main_layout:I

    invoke-virtual {v3, v4, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    :goto_1
    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_5

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iget-object v4, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-static {v4}, Lcom/android/settings/utils/SettingsFeatures;->isSplitTablet(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_4

    sget v4, Lcom/android/settings/R$layout;->miuix_preference_widget_navigation_item_text:I

    invoke-virtual {p1, v4, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    goto :goto_2

    :cond_4
    sget v4, Lcom/android/settings/R$layout;->miuix_preference_widget_text:I

    invoke-virtual {p1, v4, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    sget p1, Lcom/android/settings/R$id;->text_right:I

    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v4, Lcom/android/settings/R$dimen;->preference_text_right_max_width:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    :cond_5
    :goto_2
    sget p1, Lcom/android/settings/R$id;->arrow_right:I

    invoke-virtual {v3, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_6

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_6
    iget-object p1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-static {p1}, Lcom/android/settings/utils/SettingsFeatures;->isSplitTablet(Landroid/content/Context;)Z

    move-result p1

    const/high16 v1, 0x3f800000    # 1.0f

    if-nez p1, :cond_7

    new-array p1, v2, [Landroid/view/View;

    aput-object v3, p1, v0

    invoke-static {p1}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/IFolme;->touch()Lmiuix/animation/ITouchStyle;

    move-result-object p1

    new-array v4, v0, [Lmiuix/animation/ITouchStyle$TouchType;

    invoke-interface {p1, v1, v4}, Lmiuix/animation/ITouchStyle;->setScale(F[Lmiuix/animation/ITouchStyle$TouchType;)Lmiuix/animation/ITouchStyle;

    move-result-object p1

    iget-object v1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v4, Lcom/android/settings/R$color;->miuisettings_item_touch_color:I

    iget-object v5, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-virtual {v5}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v1

    invoke-interface {p1, v1}, Lmiuix/animation/ITouchStyle;->setBackgroundColor(I)Lmiuix/animation/ITouchStyle;

    move-result-object p1

    invoke-interface {p1, v2}, Lmiuix/animation/ITouchStyle;->setTintMode(I)Lmiuix/animation/ITouchStyle;

    move-result-object p1

    new-array v0, v0, [Lmiuix/animation/base/AnimConfig;

    invoke-interface {p1, v3, v0}, Lmiuix/animation/ITouchStyle;->handleTouchOf(Landroid/view/View;[Lmiuix/animation/base/AnimConfig;)V

    goto :goto_3

    :cond_7
    iget-object p1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    sget v4, Lcom/android/settings/R$attr;->navigationPreferenceItemBackground:I

    invoke-static {p1, v4}, Lmiuix/internal/util/AttributeResolver;->resolveDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    new-array p1, v2, [Landroid/view/View;

    aput-object v3, p1, v0

    invoke-static {p1}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/IFolme;->touch()Lmiuix/animation/ITouchStyle;

    move-result-object p1

    new-array v2, v0, [Lmiuix/animation/ITouchStyle$TouchType;

    invoke-interface {p1, v1, v2}, Lmiuix/animation/ITouchStyle;->setScale(F[Lmiuix/animation/ITouchStyle$TouchType;)Lmiuix/animation/ITouchStyle;

    move-result-object p1

    new-array v0, v0, [Lmiuix/animation/base/AnimConfig;

    invoke-interface {p1, v3, v0}, Lmiuix/animation/ITouchStyle;->handleTouchOf(Landroid/view/View;[Lmiuix/animation/base/AnimConfig;)V

    :goto_3
    move-object v0, v3

    goto :goto_4

    :cond_8
    iget-object v1, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v2, Lcom/android/settings/R$layout;->miuix_preference_category_layout:I

    invoke-virtual {v1, v2, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    :cond_9
    :goto_4
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    new-instance p1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;

    iget-object p0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->this$0:Lcom/android/settings/MiuiSettings;

    invoke-direct {p1, p0, v0}, Lcom/android/settings/MiuiSettings$HeaderViewHolder;-><init>(Lcom/android/settings/MiuiSettings;Landroid/view/View;)V

    return-object p1
.end method

.method public pause()V
    .locals 1

    iget-object p0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mSettingsControllerMap:Ljava/util/HashMap;

    invoke-virtual {p0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/BaseSettingsController;

    invoke-virtual {v0}, Lcom/android/settings/BaseSettingsController;->pause()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public resume()V
    .locals 1

    iget-object p0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mSettingsControllerMap:Ljava/util/HashMap;

    invoke-virtual {p0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/BaseSettingsController;

    invoke-virtual {v0}, Lcom/android/settings/BaseSettingsController;->resume()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setClick(Lcom/android/settings/MiuiSettings$HeaderViewHolder;Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;I)V
    .locals 2

    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    new-instance v1, Lcom/android/settings/MiuiSettings$HeaderAdapter$1;

    invoke-direct {v1, p0, p2, p3, p1}, Lcom/android/settings/MiuiSettings$HeaderAdapter$1;-><init>(Lcom/android/settings/MiuiSettings$HeaderAdapter;Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;ILcom/android/settings/MiuiSettings$HeaderViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setIcon(Lcom/android/settings/MiuiSettings$HeaderViewHolder;Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;)V
    .locals 4

    if-eqz p1, :cond_4

    iget-object p0, p1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->icon:Landroid/widget/ImageView;

    if-eqz p0, :cond_4

    invoke-virtual {p0}, Landroid/widget/ImageView;->getVisibility()I

    move-result p0

    const/16 v0, 0x8

    if-ne p0, v0, :cond_0

    goto :goto_1

    :cond_0
    iget-object p0, p2, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->fragmentArguments:Landroid/os/Bundle;

    if-eqz p0, :cond_1

    iget-wide v0, p2, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->id:J

    sget v2, Lcom/android/settings/R$id;->micloud_settings:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const-string v0, "account_type"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_1

    iget-object p0, p1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->icon:Landroid/widget/ImageView;

    sget v0, Lcom/android/settings/R$drawable;->xiaomi_account:I

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_1
    iget-wide v0, p2, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->id:J

    sget p0, Lcom/android/settings/R$id;->mi_account_settings:I

    int-to-long v2, p0

    cmp-long p0, v0, v2

    if-eqz p0, :cond_3

    iget p0, p2, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->iconRes:I

    if-eqz p0, :cond_2

    iget-object p0, p1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->icon:Landroid/widget/ImageView;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object p0, p1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->icon:Landroid/widget/ImageView;

    iget v0, p2, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->iconRes:I

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_2
    iget-object p0, p1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->icon:Landroid/widget/ImageView;

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_3
    :goto_0
    iget-wide v0, p2, Lcom/android/settingslib/miuisettings/preference/PreferenceActivity$Header;->id:J

    sget p0, Lcom/android/settings/R$id;->mi_account_settings:I

    int-to-long v2, p0

    cmp-long p0, v0, v2

    if-eqz p0, :cond_4

    iget-object p0, p1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->icon:Landroid/widget/ImageView;

    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p0

    instance-of p0, p0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz p0, :cond_4

    iget-object p0, p1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->icon:Landroid/widget/ImageView;

    invoke-virtual {p0}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget p2, Lcom/android/settings/R$dimen;->header_icon_size:I

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    iget-object p1, p1, Lcom/android/settings/MiuiSettings$HeaderViewHolder;->icon:Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p2

    invoke-static {p2, p0, p0}, Lcom/android/settings/Utils;->createBitmap(Landroid/graphics/drawable/Drawable;II)Landroid/graphics/Bitmap;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_4
    :goto_1
    return-void
.end method

.method public start()V
    .locals 1

    iget-object p0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mSettingsControllerMap:Ljava/util/HashMap;

    invoke-virtual {p0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/BaseSettingsController;

    invoke-virtual {v0}, Lcom/android/settings/BaseSettingsController;->start()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public stop()V
    .locals 1

    iget-object p0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mSettingsControllerMap:Ljava/util/HashMap;

    invoke-virtual {p0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/BaseSettingsController;

    invoke-virtual {v0}, Lcom/android/settings/BaseSettingsController;->stop()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public updateHeaderViewInfo()V
    .locals 2

    iget-object p0, p0, Lcom/android/settings/MiuiSettings$HeaderAdapter;->mSettingsControllerMap:Ljava/util/HashMap;

    if-eqz p0, :cond_0

    sget v0, Lcom/android/settings/R$id;->mi_account_settings:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/android/settings/BaseSettingsController;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/BaseSettingsController;->updateStatus()V

    :cond_0
    return-void
.end method
