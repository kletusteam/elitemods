.class public Lcom/android/settings/privacypassword/BussinessSpecificationInfo;
.super Ljava/lang/Object;


# instance fields
.field public final actionBarTitle:I

.field public final intentAction:Ljava/lang/String;

.field public final isGone:Z

.field public final specificImage:I

.field public final specificText:I

.field public final startPackage:Ljava/lang/String;


# direct methods
.method public constructor <init>(IIILjava/lang/String;ZLjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/settings/privacypassword/BussinessSpecificationInfo;->actionBarTitle:I

    iput p2, p0, Lcom/android/settings/privacypassword/BussinessSpecificationInfo;->specificText:I

    iput p3, p0, Lcom/android/settings/privacypassword/BussinessSpecificationInfo;->specificImage:I

    iput-object p4, p0, Lcom/android/settings/privacypassword/BussinessSpecificationInfo;->startPackage:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/android/settings/privacypassword/BussinessSpecificationInfo;->isGone:Z

    iput-object p6, p0, Lcom/android/settings/privacypassword/BussinessSpecificationInfo;->intentAction:Ljava/lang/String;

    return-void
.end method
