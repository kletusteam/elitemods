.class public Lcom/android/settings/privacypassword/AddAccountActivity;
.super Lmiuix/appcompat/app/AppCompatActivity;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/privacypassword/AddAccountActivity$AccountServiceConnection;
    }
.end annotation


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mAccountCallback:Landroid/accounts/AccountManagerCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/accounts/AccountManagerCallback<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private mAccountIcon:Landroid/widget/ImageView;

.field private mAccountInfo:Landroid/widget/TextView;

.field private mAccountName:Landroid/widget/TextView;

.field private mAccountTitleContent:Landroid/widget/TextView;

.field private mBackText:Landroid/widget/TextView;

.field private mBigAccountTitleContent:Landroid/widget/TextView;

.field private mCheckOnPcMode:Z

.field private mEnterWay:I

.field private mIsCancelLogin:Z

.field private mIsLoginAccount:Z

.field private mIsStartModify:Z

.field private mIsStartedLogin:Z

.field private mLeftButton:Landroid/widget/Button;

.field private mPasswordManager:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

.field private mResultIsOk:Z

.field private mRightButton:Landroid/widget/Button;

.field private mSplitMaskView:Landroid/widget/RelativeLayout;

.field private topView:Landroid/view/View;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAccount(Lcom/android/settings/privacypassword/AddAccountActivity;)Landroid/accounts/Account;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccount:Landroid/accounts/Account;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmAccountIcon(Lcom/android/settings/privacypassword/AddAccountActivity;)Landroid/widget/ImageView;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccountIcon:Landroid/widget/ImageView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsStartModify(Lcom/android/settings/privacypassword/AddAccountActivity;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mIsStartModify:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmPasswordManager(Lcom/android/settings/privacypassword/AddAccountActivity;)Lcom/android/settings/privacypassword/PrivacyPasswordManager;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mPasswordManager:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmIsCancelLogin(Lcom/android/settings/privacypassword/AddAccountActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mIsCancelLogin:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmResultIsOk(Lcom/android/settings/privacypassword/AddAccountActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mResultIsOk:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetAnalyticBindingResultKey(Lcom/android/settings/privacypassword/AddAccountActivity;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->getAnalyticBindingResultKey()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lmiuix/appcompat/app/AppCompatActivity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mResultIsOk:Z

    iput-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mIsStartModify:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mIsCancelLogin:Z

    iput-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mIsStartedLogin:Z

    iput-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mCheckOnPcMode:Z

    new-instance v0, Lcom/android/settings/privacypassword/AddAccountActivity$1;

    invoke-direct {v0, p0}, Lcom/android/settings/privacypassword/AddAccountActivity$1;-><init>(Lcom/android/settings/privacypassword/AddAccountActivity;)V

    iput-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccountCallback:Landroid/accounts/AccountManagerCallback;

    return-void
.end method

.method private adaptSmallWindow()V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->isRealInMultiWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mSplitMaskView:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->topView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/android/settings/R$id;->pvc_content:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p0

    invoke-virtual {p0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private addBackEvent()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mIsLoginAccount:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->getAnalyticBindingResultKey()Ljava/lang/String;

    move-result-object p0

    const-string v0, "logged_in_back"

    invoke-static {p0, v0}, Lcom/android/settings/privacypassword/analytics/AnalyticHelper;->statsForgetPageBindingResult(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mIsStartedLogin:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mIsCancelLogin:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->getAnalyticBindingResultKey()Ljava/lang/String;

    move-result-object p0

    const-string/jumbo v0, "not_logged_cancel_login_back"

    invoke-static {p0, v0}, Lcom/android/settings/privacypassword/analytics/AnalyticHelper;->statsForgetPageBindingResult(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->getAnalyticBindingResultKey()Ljava/lang/String;

    move-result-object p0

    const-string/jumbo v0, "not_logged_back"

    invoke-static {p0, v0}, Lcom/android/settings/privacypassword/analytics/AnalyticHelper;->statsForgetPageBindingResult(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private addSkipEvent()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mIsLoginAccount:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->getAnalyticBindingResultKey()Ljava/lang/String;

    move-result-object p0

    const-string v0, "logged_in_skip"

    invoke-static {p0, v0}, Lcom/android/settings/privacypassword/analytics/AnalyticHelper;->statsForgetPageBindingResult(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mIsStartedLogin:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mIsCancelLogin:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->getAnalyticBindingResultKey()Ljava/lang/String;

    move-result-object p0

    const-string/jumbo v0, "not_logged_cancel_login_skip"

    invoke-static {p0, v0}, Lcom/android/settings/privacypassword/analytics/AnalyticHelper;->statsForgetPageBindingResult(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->getAnalyticBindingResultKey()Ljava/lang/String;

    move-result-object p0

    const-string/jumbo v0, "not_logged_skip"

    invoke-static {p0, v0}, Lcom/android/settings/privacypassword/analytics/AnalyticHelper;->statsForgetPageBindingResult(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private getAnalyticBindingResultKey()Ljava/lang/String;
    .locals 1

    iget p0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mEnterWay:I

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    const-string p0, "binding_result"

    return-object p0

    :cond_0
    const-string p0, "app_binding_result"

    return-object p0
.end method

.method private handleExternalScreen()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccountInfo:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    sget v1, Lcom/android/settings/R$dimen;->px_160:I

    invoke-static {p0, v1}, Lcom/android/settings/privacypassword/PrivacyPasswordUtils;->getDimen(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    invoke-static {p0, v1}, Lcom/android/settings/privacypassword/PrivacyPasswordUtils;->getDimen(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    iget-object v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccountInfo:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccountInfo:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestLayout()V

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccountTitleContent:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mBigAccountTitleContent:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccountIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    sget v2, Lcom/android/settings/R$dimen;->account_icon_margin_top:I

    invoke-static {p0, v2}, Lcom/android/settings/privacypassword/PrivacyPasswordUtils;->getDimen(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v0, v1, v2, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccountIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->topView:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method private handleSplitModel()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mLeftButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    sget v1, Lcom/android/settings/R$dimen;->px_80:I

    invoke-static {p0, v1}, Lcom/android/settings/privacypassword/PrivacyPasswordUtils;->getDimen(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    iget-object v2, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mLeftButton:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mLeftButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestLayout()V

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mRightButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-static {p0, v1}, Lcom/android/settings/privacypassword/PrivacyPasswordUtils;->getDimen(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    sget v1, Lcom/android/settings/R$dimen;->px_40:I

    invoke-static {p0, v1}, Lcom/android/settings/privacypassword/PrivacyPasswordUtils;->getDimen(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    iget-object v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mRightButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mRightButton:Landroid/widget/Button;

    invoke-virtual {p0}, Landroid/widget/Button;->requestLayout()V

    return-void
.end method

.method private initViewData()V
    .locals 4

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "is_start_modify"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mIsStartModify:Z

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "enter_forgetpage_way"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mEnterWay:I

    invoke-static {p0}, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->getInstance(Landroid/content/Context;)Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mPasswordManager:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    sget v0, Lcom/android/settings/R$id;->pvc_icon:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccountIcon:Landroid/widget/ImageView;

    sget v0, Lcom/android/settings/R$id;->pvc_account_text:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccountName:Landroid/widget/TextView;

    sget v0, Lcom/android/settings/R$id;->pvc_add_account_info:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccountInfo:Landroid/widget/TextView;

    sget v0, Lcom/android/settings/R$id;->pvc_add_account_title_content:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccountTitleContent:Landroid/widget/TextView;

    sget v0, Lcom/android/settings/R$id;->big_title:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mBigAccountTitleContent:Landroid/widget/TextView;

    sget v0, Lcom/android/settings/R$id;->footerLeftButton:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mLeftButton:Landroid/widget/Button;

    sget v1, Lcom/android/settings/R$string;->privacy_password_not_add_account:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    sget v0, Lcom/android/settings/R$id;->footerRightButton:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mRightButton:Landroid/widget/Button;

    sget v0, Lcom/android/settings/R$id;->pvc_add_account_title:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mBackText:Landroid/widget/TextView;

    sget v0, Lcom/android/settings/R$id;->pvc_add_account_top_layout:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->topView:Landroid/view/View;

    invoke-static {p0}, Lcom/android/settings/privacypassword/XiaomiAccountUtils;->loginedXiaomiAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccount:Landroid/accounts/Account;

    sget v0, Lcom/android/settings/R$id;->split_screen_layout:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mSplitMaskView:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccount:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    move v2, v3

    :cond_0
    iput-boolean v2, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mIsLoginAccount:Z

    const-string v0, "account"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountManager;

    iget-boolean v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mIsLoginAccount:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccount:Landroid/accounts/Account;

    const-string v2, "acc_user_name"

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccountName:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccountName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccountName:Landroid/widget/TextView;

    sget v1, Lcom/android/settings/R$string;->privacy_password_not_login_account:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mRightButton:Landroid/widget/Button;

    sget v1, Lcom/android/settings/R$string;->privacy_password_add_account:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mLeftButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mRightButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mBackText:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mBackText:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$string;->setup_password_back:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->setUserAvatar()V

    iget-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mIsLoginAccount:Z

    if-eqz v0, :cond_3

    const-string v0, "logged_in"

    goto :goto_1

    :cond_3
    const-string/jumbo v0, "not_logged"

    :goto_1
    invoke-static {v0}, Lcom/android/settings/privacypassword/analytics/AnalyticHelper;->statsSet1ForgetPageAccount(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/settings/privacypassword/PrivacyPasswordUtils;->isNotch()Z

    move-result v0

    if-eqz v0, :cond_4

    sget v0, Lcom/android/settings/R$id;->top_actionBar:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/android/settings/R$dimen;->back_button_alight_top:I

    iget-object v2, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->topView:Landroid/view/View;

    sget v3, Lcom/android/settings/R$dimen;->top_account_actionBar:I

    invoke-static {p0, v0, v1, v2, v3}, Lcom/android/settings/privacypassword/PrivacyPasswordUtils;->adapteNotch(Landroid/content/Context;Landroid/view/View;ILandroid/view/View;I)V

    :cond_4
    return-void
.end method

.method private isRealInMultiWindow()Z
    .locals 5

    const-string v0, "AddAccountActivity"

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "isInMultiWindowMode"

    new-array v4, v1, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isRealInMultiWindow: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {v2, p0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {v2, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p0

    :catch_0
    move-exception p0

    const-string v2, "isRealInMultiWindow"

    invoke-static {v0, v2, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return v1
.end method

.method private loginXiaomiAccount(Landroid/app/Activity;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mIsStartedLogin:Z

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object p0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccountCallback:Landroid/accounts/AccountManagerCallback;

    invoke-static {p1, v0, p0}, Lcom/android/settings/privacypassword/XiaomiAccountUtils;->loginAccount(Landroid/app/Activity;Landroid/os/Bundle;Landroid/accounts/AccountManagerCallback;)V

    return-void
.end method

.method private setUserAvatar()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.xiaomi.account.action.BIND_XIAOMI_ACCOUNT_SERVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.xiaomi.account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Lcom/android/settings/privacypassword/AddAccountActivity$AccountServiceConnection;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/settings/privacypassword/AddAccountActivity$AccountServiceConnection;-><init>(Lcom/android/settings/privacypassword/AddAccountActivity;Lcom/android/settings/privacypassword/AddAccountActivity$AccountServiceConnection-IA;)V

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Landroid/app/Activity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method


# virtual methods
.method public finish()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mResultIsOk:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/app/Activity;->setResult(I)V

    invoke-super {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    invoke-super {p0}, Lmiuix/appcompat/app/AppCompatActivity;->onBackPressed()V

    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->addBackEvent()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    const-class v0, Lcom/android/settings/privacypassword/ModifyAndInstructionPrivacyPassword;

    new-instance v1, Landroid/security/ChooseLockSettingsHelper;

    const/4 v2, 0x3

    invoke-direct {v1, p0, v2}, Landroid/security/ChooseLockSettingsHelper;-><init>(Landroid/app/Activity;I)V

    iget-object v2, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mLeftButton:Landroid/widget/Button;

    const/4 v3, 0x1

    if-ne p1, v2, :cond_1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result p1

    invoke-virtual {v1, v3, p1}, Landroid/security/ChooseLockSettingsHelper;->setPrivacyPasswordEnabledAsUser(ZI)V

    iput-boolean v3, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mResultIsOk:Z

    iget-boolean p1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mIsStartModify:Z

    if-eqz p1, :cond_0

    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->addSkipEvent()V

    invoke-virtual {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->finish()V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mRightButton:Landroid/widget/Button;

    if-ne p1, v2, :cond_4

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result p1

    invoke-virtual {v1, v3, p1}, Landroid/security/ChooseLockSettingsHelper;->setPrivacyPasswordEnabledAsUser(ZI)V

    iget-boolean p1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mIsLoginAccount:Z

    if-eqz p1, :cond_3

    iput-boolean v3, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mResultIsOk:Z

    iget-object p1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mPasswordManager:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    iget-object v1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->bindXiaoMiAccount(Ljava/lang/String;)V

    iget-boolean p1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mIsStartModify:Z

    if-eqz p1, :cond_2

    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_2
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/android/settings/R$string;->bind_xiaomi_account_success:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->getAnalyticBindingResultKey()Ljava/lang/String;

    move-result-object p1

    const-string v0, "logged_in_binding"

    invoke-static {p1, v0}, Lcom/android/settings/privacypassword/analytics/AnalyticHelper;->statsForgetPageBindingResult(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->finish()V

    goto :goto_0

    :cond_3
    invoke-direct {p0, p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->loginXiaomiAccount(Landroid/app/Activity;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mBackText:Landroid/widget/TextView;

    if-ne p1, v0, :cond_5

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mResultIsOk:Z

    invoke-virtual {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->onBackPressed()V

    :cond_5
    :goto_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    iget p1, p1, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit16 p1, p1, 0x2000

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mCheckOnPcMode:Z

    if-nez p1, :cond_1

    sget p1, Lcom/android/settings/R$layout;->add_account_setting_cetus:I

    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->setContentView(I)V

    goto :goto_1

    :cond_1
    sget p1, Lcom/android/settings/R$layout;->add_account_setting:I

    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/AppCompatActivity;->setContentView(I)V

    :goto_1
    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->initViewData()V

    return-void
.end method

.method public onMultiWindowModeChanged(ZLandroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onMultiWindowModeChanged(ZLandroid/content/res/Configuration;)V

    invoke-static {p2}, Lcom/android/settings/privacypassword/PrivacyPasswordUtils;->getCurrentWindowMode(Landroid/content/res/Configuration;)I

    move-result p1

    const/4 p2, 0x1

    if-ne p1, p2, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->recreate()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onResume()V

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mPasswordManager:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    invoke-virtual {v0}, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->getBindXiaoMiAccount()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/android/settings/privacypassword/XiaomiAccountUtils;->isLoginXiaomiAccount(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mPasswordManager:Lcom/android/settings/privacypassword/PrivacyPasswordManager;

    invoke-virtual {v0}, Lcom/android/settings/privacypassword/PrivacyPasswordManager;->getBindXiaoMiAccount()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/android/settings/privacypassword/XiaomiAccountUtils;->getLoginedAccountMd5(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->finish()V

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->adaptSmallWindow()V

    invoke-static {p0}, Lcom/android/settings/privacypassword/PrivacyPasswordUtils;->isFoldInternalScreen(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getMiuiFlags()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mCheckOnPcMode:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->handleSplitModel()V

    goto :goto_0

    :cond_1
    invoke-static {p0}, Lcom/android/settings/privacypassword/PrivacyPasswordUtils;->isFoldInternalScreen(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/settings/privacypassword/AddAccountActivity;->mCheckOnPcMode:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/android/settings/privacypassword/AddAccountActivity;->handleExternalScreen()V

    :cond_2
    :goto_0
    return-void
.end method
