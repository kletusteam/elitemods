.class public Lcom/android/settings/privacypassword/PrivacyPasswordPreferenceTitle;
.super Lcom/android/settingslib/miuisettings/preference/Preference;


# instance fields
.field private mPreferenceTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/settings/privacypassword/PrivacyPasswordPreferenceTitle;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settingslib/miuisettings/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private getPreferenceTitleId()I
    .locals 0

    sget p0, Lcom/android/settings/R$string;->privacy_password_settings_summary:I

    return p0
.end method


# virtual methods
.method public onBindView(Landroid/view/View;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settingslib/miuisettings/preference/Preference;->onBindView(Landroid/view/View;)V

    sget v0, Lcom/android/settings/R$id;->preference_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/android/settings/privacypassword/PrivacyPasswordPreferenceTitle;->mPreferenceTitle:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/android/settings/privacypassword/PrivacyPasswordPreferenceTitle;->getPreferenceTitleId()I

    move-result p0

    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method protected onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    sget p1, Lcom/android/settings/R$layout;->privacy_password_preference_title:I

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setLayoutResource(I)V

    const/4 p0, 0x0

    return-object p0
.end method
