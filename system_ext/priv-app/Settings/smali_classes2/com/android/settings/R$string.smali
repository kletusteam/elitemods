.class public final Lcom/android/settings/R$string;
.super Ljava/lang/Object;


# static fields
.field public static final APN_NAME_CBNET:I = 0x7f120000

.field public static final APN_NAME_CMMMS:I = 0x7f120001

.field public static final APN_NAME_CMNET:I = 0x7f120002

.field public static final APN_NAME_CMWAP:I = 0x7f120003

.field public static final APN_NAME_CTLTE:I = 0x7f120004

.field public static final APN_NAME_CTMMS:I = 0x7f120005

.field public static final APN_NAME_CTNET:I = 0x7f120006

.field public static final APN_NAME_CTWAP:I = 0x7f120007

.field public static final APN_NAME_CUMMS:I = 0x7f120008

.field public static final APN_NAME_CUNET:I = 0x7f120009

.field public static final APN_NAME_CUSUPL:I = 0x7f12000a

.field public static final APN_NAME_CUWAP:I = 0x7f12000b

.field public static final AUTO_UPGRADE_FEATURE:I = 0x7f12000c

.field public static final Accounts_settings_title:I = 0x7f12000d

.field public static final BLE_MMA_FEATURE:I = 0x7f12000e

.field public static final CLOSE_BOX_SILENT_UPGRADE_EXIST:I = 0x7f12000f

.field public static final DETAIL_NOTIFICATION_FEATURE_V1:I = 0x7f120010

.field public static final EROS_MMA_FEATURE:I = 0x7f120011

.field public static final IS_SUPPORT_SPACE_SOUND:I = 0x7f120012

.field public static final Intelligentpickfree:I = 0x7f120013

.field public static final L71_AUTO_UPGRADE_FEATURE:I = 0x7f120014

.field public static final L71_MANUAL_AND_SILENT_UPGRADE:I = 0x7f120015

.field public static final MiSans_title:I = 0x7f120016

.field public static final SURPPORT_COMPATIBLE_AT_VERSION:I = 0x7f120017

.field public static final SURPPORT_EARBUDS_LOG_UPLOAD:I = 0x7f120018

.field public static final SURPPORT_HOT_WORD:I = 0x7f120019

.field public static final SURPPORT_QCOM_LE_AUDIO:I = 0x7f12001a

.field public static final Sound_Settings:I = 0x7f12001b

.field public static final VIRTUAL_SURROUND_SOUND_FEATURE:I = 0x7f12001c

.field public static final a11y_settings_label:I = 0x7f12001d

.field public static final abc_action_bar_home_description:I = 0x7f12001e

.field public static final abc_action_bar_up_description:I = 0x7f12001f

.field public static final abc_action_menu_overflow_description:I = 0x7f120020

.field public static final abc_action_mode_done:I = 0x7f120021

.field public static final abc_activity_chooser_view_see_all:I = 0x7f120022

.field public static final abc_activitychooserview_choose_application:I = 0x7f120023

.field public static final abc_capital_off:I = 0x7f120024

.field public static final abc_capital_on:I = 0x7f120025

.field public static final abc_menu_alt_shortcut_label:I = 0x7f120026

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f120027

.field public static final abc_menu_delete_shortcut_label:I = 0x7f120028

.field public static final abc_menu_enter_shortcut_label:I = 0x7f120029

.field public static final abc_menu_function_shortcut_label:I = 0x7f12002a

.field public static final abc_menu_meta_shortcut_label:I = 0x7f12002b

.field public static final abc_menu_shift_shortcut_label:I = 0x7f12002c

.field public static final abc_menu_space_shortcut_label:I = 0x7f12002d

.field public static final abc_menu_sym_shortcut_label:I = 0x7f12002e

.field public static final abc_prepend_shortcut_label:I = 0x7f12002f

.field public static final abc_search_hint:I = 0x7f120030

.field public static final abc_searchview_description_clear:I = 0x7f120031

.field public static final abc_searchview_description_query:I = 0x7f120032

.field public static final abc_searchview_description_search:I = 0x7f120033

.field public static final abc_searchview_description_submit:I = 0x7f120034

.field public static final abc_searchview_description_voice:I = 0x7f120035

.field public static final abc_shareactionprovider_share_with:I = 0x7f120036

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f120037

.field public static final abc_slice_error:I = 0x7f120038

.field public static final abc_slice_more:I = 0x7f120039

.field public static final abc_slice_more_content:I = 0x7f12003a

.field public static final abc_slice_permission_allow:I = 0x7f12003b

.field public static final abc_slice_permission_deny:I = 0x7f12003c

.field public static final abc_slice_permission_text_1:I = 0x7f12003d

.field public static final abc_slice_permission_text_2:I = 0x7f12003e

.field public static final abc_slice_permission_title:I = 0x7f12003f

.field public static final abc_slice_show_more:I = 0x7f120040

.field public static final abc_slice_updated:I = 0x7f120041

.field public static final abc_slices_permission_request:I = 0x7f120042

.field public static final abc_toolbar_collapse_description:I = 0x7f120043

.field public static final about_ad_service_instructions:I = 0x7f120044

.field public static final about_ad_service_instructions_for_global:I = 0x7f120045

.field public static final about_phone_device_name_warning:I = 0x7f120046

.field public static final about_phone_enter_xiaomi_net:I = 0x7f120047

.field public static final about_settings:I = 0x7f120048

.field public static final about_settings_summary:I = 0x7f120049

.field public static final ac_enable_summary:I = 0x7f12004a

.field public static final ac_enable_title:I = 0x7f12004b

.field public static final ac_privacy_mode_sum:I = 0x7f12004c

.field public static final ac_privacy_mode_title:I = 0x7f12004d

.field public static final ac_set_apps_title:I = 0x7f12004e

.field public static final accelerometer_summary_off:I = 0x7f12004f

.field public static final accelerometer_summary_on:I = 0x7f120050

.field public static final accelerometer_title:I = 0x7f120051

.field public static final accept_new_connection:I = 0x7f120052

.field public static final accept_new_connection_detail:I = 0x7f120053

.field public static final access_control_app_entry_multiple:I = 0x7f120054

.field public static final access_control_app_entry_single:I = 0x7f120055

.field public static final access_control_need_password:I = 0x7f120056

.field public static final access_control_need_to_unlock:I = 0x7f120057

.field public static final access_control_no_back_footer:I = 0x7f120058

.field public static final access_control_password_title:I = 0x7f120059

.field public static final access_control_recording_intro_header:I = 0x7f12005a

.field public static final access_control_reset_by_account:I = 0x7f12005b

.field public static final access_denied_info:I = 0x7f12005c

.field public static final access_point_names:I = 0x7f12005d

.field public static final accessibility_alarm_vibration_title:I = 0x7f12005e

.field public static final accessibility_audio_adjustment_title:I = 0x7f12005f

.field public static final accessibility_audio_description_summary:I = 0x7f120060

.field public static final accessibility_autoclick_about_title:I = 0x7f120061

.field public static final accessibility_autoclick_custom_title:I = 0x7f120062

.field public static final accessibility_autoclick_default_title:I = 0x7f120063

.field public static final accessibility_autoclick_description:I = 0x7f120064

.field public static final accessibility_autoclick_footer_learn_more_content_description:I = 0x7f120065

.field public static final accessibility_autoclick_intro_text:I = 0x7f120066

.field public static final accessibility_autoclick_long_summary:I = 0x7f120067

.field public static final accessibility_autoclick_long_title:I = 0x7f120068

.field public static final accessibility_autoclick_longer_desc:I = 0x7f120069

.field public static final accessibility_autoclick_medium_summary:I = 0x7f12006a

.field public static final accessibility_autoclick_medium_title:I = 0x7f12006b

.field public static final accessibility_autoclick_preference_title:I = 0x7f12006c

.field public static final accessibility_autoclick_seekbar_desc:I = 0x7f12006d

.field public static final accessibility_autoclick_short_summary:I = 0x7f12006e

.field public static final accessibility_autoclick_short_title:I = 0x7f12006f

.field public static final accessibility_autoclick_shorter_desc:I = 0x7f120070

.field public static final accessibility_back:I = 0x7f120071

.field public static final accessibility_banner_message_dismiss:I = 0x7f120072

.field public static final accessibility_button_about_title:I = 0x7f120073

.field public static final accessibility_button_description:I = 0x7f120074

.field public static final accessibility_button_fade_summary:I = 0x7f120075

.field public static final accessibility_button_fade_title:I = 0x7f120076

.field public static final accessibility_button_gesture_about_title:I = 0x7f120077

.field public static final accessibility_button_gesture_description:I = 0x7f120078

.field public static final accessibility_button_gesture_footer_learn_more_content_description:I = 0x7f120079

.field public static final accessibility_button_gesture_title:I = 0x7f12007a

.field public static final accessibility_button_high_label:I = 0x7f12007b

.field public static final accessibility_button_intro:I = 0x7f12007c

.field public static final accessibility_button_intro_text:I = 0x7f12007d

.field public static final accessibility_button_location_title:I = 0x7f12007e

.field public static final accessibility_button_low_label:I = 0x7f12007f

.field public static final accessibility_button_opacity_title:I = 0x7f120080

.field public static final accessibility_button_or_gesture_title:I = 0x7f120081

.field public static final accessibility_button_size_title:I = 0x7f120082

.field public static final accessibility_button_summary:I = 0x7f120083

.field public static final accessibility_button_title:I = 0x7f120084

.field public static final accessibility_call_vibration_category_title:I = 0x7f120085

.field public static final accessibility_caption_preference_intro:I = 0x7f120086

.field public static final accessibility_caption_preference_summary:I = 0x7f120087

.field public static final accessibility_caption_primary_switch_summary:I = 0x7f120088

.field public static final accessibility_caption_primary_switch_title:I = 0x7f120089

.field public static final accessibility_captioning_about_title:I = 0x7f12008a

.field public static final accessibility_captioning_footer_learn_more_content_description:I = 0x7f12008b

.field public static final accessibility_captioning_title:I = 0x7f12008c

.field public static final accessibility_category_personal:I = 0x7f12008d

.field public static final accessibility_category_work:I = 0x7f12008e

.field public static final accessibility_clear:I = 0x7f12008f

.field public static final accessibility_close_tab:I = 0x7f120090

.field public static final accessibility_color_and_motion_title:I = 0x7f120091

.field public static final accessibility_color_correction_auto_added_qs_tooltip_content:I = 0x7f120092

.field public static final accessibility_color_correction_qs_tooltip_content:I = 0x7f120093

.field public static final accessibility_color_inversion_about_title:I = 0x7f120094

.field public static final accessibility_color_inversion_auto_added_qs_tooltip_content:I = 0x7f120095

.field public static final accessibility_color_inversion_footer_learn_more_content_description:I = 0x7f120096

.field public static final accessibility_color_inversion_qs_tooltip_content:I = 0x7f120097

.field public static final accessibility_control_timeout_about_title:I = 0x7f120098

.field public static final accessibility_control_timeout_footer_learn_more_content_description:I = 0x7f120099

.field public static final accessibility_control_timeout_preference_intro_text:I = 0x7f12009a

.field public static final accessibility_control_timeout_preference_summary:I = 0x7f12009b

.field public static final accessibility_control_timeout_preference_title:I = 0x7f12009c

.field public static final accessibility_daltonizer_about_intro_text:I = 0x7f12009d

.field public static final accessibility_daltonizer_about_title:I = 0x7f12009e

.field public static final accessibility_daltonizer_footer_learn_more_content_description:I = 0x7f12009f

.field public static final accessibility_daltonizer_primary_switch_title:I = 0x7f1200a0

.field public static final accessibility_daltonizer_shortcut_title:I = 0x7f1200a1

.field public static final accessibility_data_one_bar:I = 0x7f1200a2

.field public static final accessibility_data_signal_full:I = 0x7f1200a3

.field public static final accessibility_data_three_bars:I = 0x7f1200a4

.field public static final accessibility_data_two_bars:I = 0x7f1200a5

.field public static final accessibility_description_state_stopped:I = 0x7f1200a6

.field public static final accessibility_dialog_button_allow:I = 0x7f1200a7

.field public static final accessibility_dialog_button_cancel:I = 0x7f1200a8

.field public static final accessibility_dialog_button_deny:I = 0x7f1200a9

.field public static final accessibility_dialog_button_stop:I = 0x7f1200aa

.field public static final accessibility_disable_animations:I = 0x7f1200ab

.field public static final accessibility_disable_animations_summary:I = 0x7f1200ac

.field public static final accessibility_display_daltonizer_preference_subtitle:I = 0x7f1200ad

.field public static final accessibility_display_daltonizer_preference_title:I = 0x7f1200ae

.field public static final accessibility_display_inversion_preference_intro_text:I = 0x7f1200af

.field public static final accessibility_display_inversion_preference_subtitle:I = 0x7f1200b0

.field public static final accessibility_display_inversion_preference_title:I = 0x7f1200b1

.field public static final accessibility_display_inversion_shortcut_title:I = 0x7f1200b2

.field public static final accessibility_display_inversion_switch_title:I = 0x7f1200b3

.field public static final accessibility_enable:I = 0x7f1200b4

.field public static final accessibility_ethernet_connected:I = 0x7f1200b5

.field public static final accessibility_ethernet_disconnected:I = 0x7f1200b6

.field public static final accessibility_feature_state_off:I = 0x7f1200b7

.field public static final accessibility_feature_state_on:I = 0x7f1200b8

.field public static final accessibility_global_gesture_preference_title:I = 0x7f1200b9

.field public static final accessibility_haptic:I = 0x7f1200ba

.field public static final accessibility_hearingaid_active_device_summary:I = 0x7f1200bb

.field public static final accessibility_hearingaid_adding_summary:I = 0x7f1200bc

.field public static final accessibility_hearingaid_instruction_continue_button:I = 0x7f1200bd

.field public static final accessibility_hearingaid_left_and_right_side_device_summary:I = 0x7f1200be

.field public static final accessibility_hearingaid_left_side_device_summary:I = 0x7f1200bf

.field public static final accessibility_hearingaid_more_device_summary:I = 0x7f1200c0

.field public static final accessibility_hearingaid_not_connected_summary:I = 0x7f1200c1

.field public static final accessibility_hearingaid_pair_instructions_message:I = 0x7f1200c2

.field public static final accessibility_hearingaid_pair_instructions_title:I = 0x7f1200c3

.field public static final accessibility_hearingaid_right_side_device_summary:I = 0x7f1200c4

.field public static final accessibility_hearingaid_title:I = 0x7f1200c5

.field public static final accessibility_interactive_haptics_category_title:I = 0x7f1200c6

.field public static final accessibility_introduction_title:I = 0x7f1200c7

.field public static final accessibility_lock_screen_progress:I = 0x7f1200c8

.field public static final accessibility_long_press_timeout_preference_title:I = 0x7f1200c9

.field public static final accessibility_magnification_area_settings_all_summary:I = 0x7f1200ca

.field public static final accessibility_magnification_area_settings_full_screen_summary:I = 0x7f1200cb

.field public static final accessibility_magnification_area_settings_message:I = 0x7f1200cc

.field public static final accessibility_magnification_area_settings_mode_switch_summary:I = 0x7f1200cd

.field public static final accessibility_magnification_area_settings_window_screen_summary:I = 0x7f1200ce

.field public static final accessibility_magnification_mode_dialog_option_full_screen:I = 0x7f1200cf

.field public static final accessibility_magnification_mode_dialog_option_switch:I = 0x7f1200d0

.field public static final accessibility_magnification_mode_dialog_option_window:I = 0x7f1200d1

.field public static final accessibility_magnification_mode_dialog_title:I = 0x7f1200d2

.field public static final accessibility_magnification_mode_title:I = 0x7f1200d3

.field public static final accessibility_magnification_service_settings_title:I = 0x7f1200d4

.field public static final accessibility_magnification_switch_shortcut_message:I = 0x7f1200d5

.field public static final accessibility_magnification_switch_shortcut_negative_button:I = 0x7f1200d6

.field public static final accessibility_magnification_switch_shortcut_positive_button:I = 0x7f1200d7

.field public static final accessibility_magnification_switch_shortcut_title:I = 0x7f1200d8

.field public static final accessibility_magnification_triple_tap_warning_message:I = 0x7f1200d9

.field public static final accessibility_magnification_triple_tap_warning_negative_button:I = 0x7f1200da

.field public static final accessibility_magnification_triple_tap_warning_positive_button:I = 0x7f1200db

.field public static final accessibility_magnification_triple_tap_warning_title:I = 0x7f1200dc

.field public static final accessibility_manual_zen_less_time:I = 0x7f1200dd

.field public static final accessibility_manual_zen_more_time:I = 0x7f1200de

.field public static final accessibility_media_vibration_title:I = 0x7f1200df

.field public static final accessibility_menu_description:I = 0x7f1200e0

.field public static final accessibility_menu_description_pad:I = 0x7f1200e1

.field public static final accessibility_menu_item_settings:I = 0x7f1200e2

.field public static final accessibility_menu_service_name:I = 0x7f1200e3

.field public static final accessibility_menu_summary:I = 0x7f1200e4

.field public static final accessibility_menu_summary_pad:I = 0x7f1200e5

.field public static final accessibility_more_settings:I = 0x7f1200e6

.field public static final accessibility_no_calling:I = 0x7f1200e7

.field public static final accessibility_no_data:I = 0x7f1200e8

.field public static final accessibility_no_phone:I = 0x7f1200e9

.field public static final accessibility_no_service_selected:I = 0x7f1200ea

.field public static final accessibility_no_services_installed:I = 0x7f1200eb

.field public static final accessibility_no_wifi:I = 0x7f1200ec

.field public static final accessibility_notification_alarm_vibration_category_title:I = 0x7f1200ed

.field public static final accessibility_notification_vibration_title:I = 0x7f1200ee

.field public static final accessibility_one_handed_mode_auto_added_qs_tooltip_content:I = 0x7f1200ef

.field public static final accessibility_one_handed_mode_qs_tooltip_content:I = 0x7f1200f0

.field public static final accessibility_personal_account_title:I = 0x7f1200f1

.field public static final accessibility_phone_one_bar:I = 0x7f1200f2

.field public static final accessibility_phone_signal_full:I = 0x7f1200f3

.field public static final accessibility_phone_three_bars:I = 0x7f1200f4

.field public static final accessibility_phone_two_bars:I = 0x7f1200f5

.field public static final accessibility_power_button_ends_call_prerefence_title:I = 0x7f1200f6

.field public static final accessibility_preference_magnification_summary:I = 0x7f1200f7

.field public static final accessibility_quick_settings_tooltip_dismiss:I = 0x7f1200f8

.field public static final accessibility_reduce_bright_colors_auto_added_qs_tooltip_content:I = 0x7f1200f9

.field public static final accessibility_reduce_bright_colors_qs_tooltip_content:I = 0x7f1200fa

.field public static final accessibility_ring_vibration_title:I = 0x7f1200fb

.field public static final accessibility_screen_magnification_about_title:I = 0x7f1200fc

.field public static final accessibility_screen_magnification_enable:I = 0x7f1200fd

.field public static final accessibility_screen_magnification_follow_typing_summary:I = 0x7f1200fe

.field public static final accessibility_screen_magnification_follow_typing_title:I = 0x7f1200ff

.field public static final accessibility_screen_magnification_footer_learn_more_content_description:I = 0x7f120100

.field public static final accessibility_screen_magnification_gesture_navigation_warning:I = 0x7f120101

.field public static final accessibility_screen_magnification_gestures_title:I = 0x7f120102

.field public static final accessibility_screen_magnification_intro_text:I = 0x7f120103

.field public static final accessibility_screen_magnification_navbar_configuration_warning:I = 0x7f120104

.field public static final accessibility_screen_magnification_navbar_short_summary:I = 0x7f120105

.field public static final accessibility_screen_magnification_navbar_summary:I = 0x7f120106

.field public static final accessibility_screen_magnification_navbar_title:I = 0x7f120107

.field public static final accessibility_screen_magnification_short_summary:I = 0x7f120108

.field public static final accessibility_screen_magnification_shortcut_title:I = 0x7f120109

.field public static final accessibility_screen_magnification_state_navbar_gesture:I = 0x7f12010a

.field public static final accessibility_screen_magnification_summary:I = 0x7f12010b

.field public static final accessibility_screen_magnification_title:I = 0x7f12010c

.field public static final accessibility_screen_option:I = 0x7f12010d

.field public static final accessibility_screen_reader_haptic_title:I = 0x7f12010e

.field public static final accessibility_service_action_perform_description:I = 0x7f12010f

.field public static final accessibility_service_action_perform_title:I = 0x7f120110

.field public static final accessibility_service_auto_added_qs_tooltip_content:I = 0x7f120111

.field public static final accessibility_service_default_description:I = 0x7f120112

.field public static final accessibility_service_primary_open_title:I = 0x7f120113

.field public static final accessibility_service_primary_switch_title:I = 0x7f120114

.field public static final accessibility_service_qs_tooltip_content:I = 0x7f120115

.field public static final accessibility_service_screen_control_description:I = 0x7f120116

.field public static final accessibility_service_screen_control_title:I = 0x7f120117

.field public static final accessibility_service_warning:I = 0x7f120118

.field public static final accessibility_service_warning_description:I = 0x7f120119

.field public static final accessibility_setting_item_control_timeout_title:I = 0x7f12011a

.field public static final accessibility_settings:I = 0x7f12011b

.field public static final accessibility_settings_summary:I = 0x7f12011c

.field public static final accessibility_settings_tabs_general:I = 0x7f12011d

.field public static final accessibility_settings_tabs_hearing:I = 0x7f12011e

.field public static final accessibility_settings_tabs_physical:I = 0x7f12011f

.field public static final accessibility_settings_tabs_visual:I = 0x7f120120

.field public static final accessibility_settings_title:I = 0x7f120121

.field public static final accessibility_shortcut_description:I = 0x7f120122

.field public static final accessibility_shortcut_edit_dialog_summary_hardware:I = 0x7f120123

.field public static final accessibility_shortcut_edit_dialog_summary_software:I = 0x7f120124

.field public static final accessibility_shortcut_edit_dialog_summary_software_floating:I = 0x7f120125

.field public static final accessibility_shortcut_edit_dialog_summary_software_gesture:I = 0x7f120126

.field public static final accessibility_shortcut_edit_dialog_summary_software_gesture_talkback:I = 0x7f120127

.field public static final accessibility_shortcut_edit_dialog_summary_triple_tap:I = 0x7f120128

.field public static final accessibility_shortcut_edit_dialog_title_advance:I = 0x7f120129

.field public static final accessibility_shortcut_edit_dialog_title_hardware:I = 0x7f12012a

.field public static final accessibility_shortcut_edit_dialog_title_software:I = 0x7f12012b

.field public static final accessibility_shortcut_edit_dialog_title_software_by_gesture:I = 0x7f12012c

.field public static final accessibility_shortcut_edit_dialog_title_software_gesture:I = 0x7f12012d

.field public static final accessibility_shortcut_edit_dialog_title_software_gesture_talkback:I = 0x7f12012e

.field public static final accessibility_shortcut_edit_dialog_title_triple_tap:I = 0x7f12012f

.field public static final accessibility_shortcut_edit_summary_software:I = 0x7f120130

.field public static final accessibility_shortcut_edit_summary_software_gesture:I = 0x7f120131

.field public static final accessibility_shortcut_hardware_keyword:I = 0x7f120132

.field public static final accessibility_shortcut_service_on_lock_screen_title:I = 0x7f120133

.field public static final accessibility_shortcut_service_title:I = 0x7f120134

.field public static final accessibility_shortcut_settings:I = 0x7f120135

.field public static final accessibility_shortcut_title:I = 0x7f120136

.field public static final accessibility_shortcut_triple_tap_keyword:I = 0x7f120137

.field public static final accessibility_shortcut_type_hardware:I = 0x7f120138

.field public static final accessibility_shortcut_type_software:I = 0x7f120139

.field public static final accessibility_shortcut_type_software_gesture:I = 0x7f12013a

.field public static final accessibility_shortcut_type_triple_tap:I = 0x7f12013b

.field public static final accessibility_shortcuts_settings_title:I = 0x7f12013c

.field public static final accessibility_summary_shortcut_disabled:I = 0x7f12013d

.field public static final accessibility_summary_shortcut_enabled:I = 0x7f12013e

.field public static final accessibility_summary_source:I = 0x7f12013f

.field public static final accessibility_summary_state_disabled:I = 0x7f120140

.field public static final accessibility_summary_state_enabled:I = 0x7f120141

.field public static final accessibility_summary_state_stopped:I = 0x7f120142

.field public static final accessibility_sync_disabled:I = 0x7f120143

.field public static final accessibility_sync_enabled:I = 0x7f120144

.field public static final accessibility_sync_error:I = 0x7f120145

.field public static final accessibility_sync_in_progress:I = 0x7f120146

.field public static final accessibility_system_controls_title:I = 0x7f120147

.field public static final accessibility_tap_assistance_title:I = 0x7f120148

.field public static final accessibility_text_reading_confirm_dialog_message:I = 0x7f120149

.field public static final accessibility_text_reading_confirm_dialog_reset_button:I = 0x7f12014a

.field public static final accessibility_text_reading_confirm_dialog_title:I = 0x7f12014b

.field public static final accessibility_text_reading_conversation_message_1:I = 0x7f12014c

.field public static final accessibility_text_reading_conversation_message_2:I = 0x7f12014d

.field public static final accessibility_text_reading_options_suggestion_title:I = 0x7f12014e

.field public static final accessibility_text_reading_options_title:I = 0x7f12014f

.field public static final accessibility_text_reading_preview_mail_content:I = 0x7f120150

.field public static final accessibility_text_reading_preview_mail_from:I = 0x7f120151

.field public static final accessibility_text_reading_preview_mail_subject:I = 0x7f120152

.field public static final accessibility_text_reading_reset_button_title:I = 0x7f120153

.field public static final accessibility_text_reading_reset_message:I = 0x7f120154

.field public static final accessibility_timeout_10secs:I = 0x7f120155

.field public static final accessibility_timeout_1min:I = 0x7f120156

.field public static final accessibility_timeout_2mins:I = 0x7f120157

.field public static final accessibility_timeout_30secs:I = 0x7f120158

.field public static final accessibility_timeout_default:I = 0x7f120159

.field public static final accessibility_toggle_audio_description_preference_title:I = 0x7f12015a

.field public static final accessibility_toggle_high_text_contrast_preference_summary:I = 0x7f12015b

.field public static final accessibility_toggle_high_text_contrast_preference_title:I = 0x7f12015c

.field public static final accessibility_toggle_large_pointer_icon_summary:I = 0x7f12015d

.field public static final accessibility_toggle_large_pointer_icon_title:I = 0x7f12015e

.field public static final accessibility_toggle_primary_balance_left_label:I = 0x7f12015f

.field public static final accessibility_toggle_primary_balance_right_label:I = 0x7f120160

.field public static final accessibility_toggle_primary_balance_title:I = 0x7f120161

.field public static final accessibility_toggle_primary_mono_summary:I = 0x7f120162

.field public static final accessibility_toggle_primary_mono_title:I = 0x7f120163

.field public static final accessibility_toggle_screen_magnification_auto_update_preference_summary:I = 0x7f120164

.field public static final accessibility_toggle_screen_magnification_auto_update_preference_title:I = 0x7f120165

.field public static final accessibility_touch_vibration_title:I = 0x7f120166

.field public static final accessibility_turn_screen_darker_title:I = 0x7f120167

.field public static final accessibility_tutorial_dialog_button:I = 0x7f120168

.field public static final accessibility_tutorial_dialog_message_button:I = 0x7f120169

.field public static final accessibility_tutorial_dialog_message_floating_button:I = 0x7f12016a

.field public static final accessibility_tutorial_dialog_message_gesture:I = 0x7f12016b

.field public static final accessibility_tutorial_dialog_message_gesture_settings:I = 0x7f12016c

.field public static final accessibility_tutorial_dialog_message_gesture_settings_talkback:I = 0x7f12016d

.field public static final accessibility_tutorial_dialog_message_gesture_talkback:I = 0x7f12016e

.field public static final accessibility_tutorial_dialog_message_triple:I = 0x7f12016f

.field public static final accessibility_tutorial_dialog_message_volume:I = 0x7f120170

.field public static final accessibility_tutorial_dialog_title_button:I = 0x7f120171

.field public static final accessibility_tutorial_dialog_title_gesture:I = 0x7f120172

.field public static final accessibility_tutorial_dialog_title_gesture_settings:I = 0x7f120173

.field public static final accessibility_tutorial_dialog_title_triple:I = 0x7f120174

.field public static final accessibility_tutorial_dialog_title_volume:I = 0x7f120175

.field public static final accessibility_tutorial_pager:I = 0x7f120176

.field public static final accessibility_usage_title:I = 0x7f120177

.field public static final accessibility_vibration_primary_switch_title:I = 0x7f120178

.field public static final accessibility_vibration_setting_disabled_for_silent_mode_summary:I = 0x7f120179

.field public static final accessibility_vibration_settings_state_off:I = 0x7f12017a

.field public static final accessibility_vibration_settings_state_on:I = 0x7f12017b

.field public static final accessibility_vibration_settings_summary:I = 0x7f12017c

.field public static final accessibility_vibration_settings_title:I = 0x7f12017d

.field public static final accessibility_voice_access_title:I = 0x7f12017e

.field public static final accessibility_vpn_settings_info:I = 0x7f12017f

.field public static final accessibility_wifi_off:I = 0x7f120180

.field public static final accessibility_wifi_one_bar:I = 0x7f120181

.field public static final accessibility_wifi_security_type_none:I = 0x7f120182

.field public static final accessibility_wifi_security_type_secured:I = 0x7f120183

.field public static final accessibility_wifi_signal_full:I = 0x7f120184

.field public static final accessibility_wifi_three_bars:I = 0x7f120185

.field public static final accessibility_wifi_two_bars:I = 0x7f120186

.field public static final accessibility_work_account_title:I = 0x7f120187

.field public static final accessibility_work_profile_app_description:I = 0x7f120188

.field public static final accessor_expires_text:I = 0x7f120189

.field public static final accessor_info_title:I = 0x7f12018a

.field public static final accessor_never_expires_text:I = 0x7f12018b

.field public static final accessor_no_description_text:I = 0x7f12018c

.field public static final account_add:I = 0x7f12018d

.field public static final account_autostar_label:I = 0x7f12018e

.field public static final account_confirmation_class:I = 0x7f12018f

.field public static final account_confirmation_package:I = 0x7f120190

.field public static final account_dashboard_default_summary:I = 0x7f120191

.field public static final account_dashboard_title:I = 0x7f120192

.field public static final account_for_section_header:I = 0x7f120193

.field public static final account_list:I = 0x7f120194

.field public static final account_other:I = 0x7f120195

.field public static final account_settings:I = 0x7f120196

.field public static final account_settings_menu_auto_sync:I = 0x7f120197

.field public static final account_settings_menu_auto_sync_personal:I = 0x7f120198

.field public static final account_settings_menu_auto_sync_work:I = 0x7f120199

.field public static final account_settings_title:I = 0x7f12019a

.field public static final account_sync_settings_title:I = 0x7f12019b

.field public static final account_sync_summary_all_off:I = 0x7f12019c

.field public static final account_sync_summary_all_on:I = 0x7f12019d

.field public static final account_sync_summary_some_on:I = 0x7f12019e

.field public static final account_sync_title:I = 0x7f12019f

.field public static final account_type:I = 0x7f1201a0

.field public static final acount_data:I = 0x7f1201a1

.field public static final action_drag_label_move_bottom:I = 0x7f1201a2

.field public static final action_drag_label_move_down:I = 0x7f1201a3

.field public static final action_drag_label_move_top:I = 0x7f1201a4

.field public static final action_drag_label_move_up:I = 0x7f1201a5

.field public static final action_drag_label_remove:I = 0x7f1201a6

.field public static final actionbar_button_up_description:I = 0x7f1201a7

.field public static final activating:I = 0x7f1201a8

.field public static final active:I = 0x7f1201a9

.field public static final active_device_admin_msg:I = 0x7f1201aa

.field public static final active_input_method_subtypes:I = 0x7f1201ab

.field public static final activity_check_context:I = 0x7f1201ac

.field public static final activity_check_title:I = 0x7f1201ad

.field public static final activity_not_found:I = 0x7f1201ae

.field public static final activity_picker_label:I = 0x7f1201af

.field public static final activity_title_power_manager:I = 0x7f1201b0

.field public static final ad_service:I = 0x7f1201b1

.field public static final adapt_embedded:I = 0x7f1201b2

.field public static final adaptive_battery_main_switch_title:I = 0x7f1201b3

.field public static final adaptive_brightness_main_switch_title:I = 0x7f1201b4

.field public static final adaptive_connectivity_summary:I = 0x7f1201b5

.field public static final adaptive_connectivity_switch_off:I = 0x7f1201b6

.field public static final adaptive_connectivity_switch_on:I = 0x7f1201b7

.field public static final adaptive_connectivity_title:I = 0x7f1201b8

.field public static final adaptive_sleep_camera_lock_summary:I = 0x7f1201b9

.field public static final adaptive_sleep_contextual_slice_summary:I = 0x7f1201ba

.field public static final adaptive_sleep_contextual_slice_title:I = 0x7f1201bb

.field public static final adaptive_sleep_description:I = 0x7f1201bc

.field public static final adaptive_sleep_manage_permission_button:I = 0x7f1201bd

.field public static final adaptive_sleep_privacy:I = 0x7f1201be

.field public static final adaptive_sleep_summary_no_permission:I = 0x7f1201bf

.field public static final adaptive_sleep_summary_off:I = 0x7f1201c0

.field public static final adaptive_sleep_summary_on:I = 0x7f1201c1

.field public static final adaptive_sleep_title:I = 0x7f1201c2

.field public static final adaptive_sleep_title_no_permission:I = 0x7f1201c3

.field public static final adb_authorization_timeout_summary:I = 0x7f1201c4

.field public static final adb_authorization_timeout_title:I = 0x7f1201c5

.field public static final adb_device_fingerprint_title_format:I = 0x7f1201c6

.field public static final adb_device_forget:I = 0x7f1201c7

.field public static final adb_keys_warning_message:I = 0x7f1201c8

.field public static final adb_pair_method_code_summary:I = 0x7f1201c9

.field public static final adb_pair_method_code_title:I = 0x7f1201ca

.field public static final adb_pair_method_qrcode_summary:I = 0x7f1201cb

.field public static final adb_pair_method_qrcode_title:I = 0x7f1201cc

.field public static final adb_paired_devices_title:I = 0x7f1201cd

.field public static final adb_pairing_device_dialog_failed_msg:I = 0x7f1201ce

.field public static final adb_pairing_device_dialog_failed_title:I = 0x7f1201cf

.field public static final adb_pairing_device_dialog_pairing_code_label:I = 0x7f1201d0

.field public static final adb_pairing_device_dialog_title:I = 0x7f1201d1

.field public static final adb_qrcode_pairing_device_failed_msg:I = 0x7f1201d2

.field public static final adb_warning_message:I = 0x7f1201d3

.field public static final adb_warning_title:I = 0x7f1201d4

.field public static final adb_wireless_connection_failed_message:I = 0x7f1201d5

.field public static final adb_wireless_connection_failed_title:I = 0x7f1201d6

.field public static final adb_wireless_device_connected_summary:I = 0x7f1201d7

.field public static final adb_wireless_device_details_title:I = 0x7f1201d8

.field public static final adb_wireless_error:I = 0x7f1201d9

.field public static final adb_wireless_ip_addr_preference_title:I = 0x7f1201da

.field public static final adb_wireless_list_empty_off:I = 0x7f1201db

.field public static final adb_wireless_no_network_msg:I = 0x7f1201dc

.field public static final adb_wireless_qrcode_pairing_description:I = 0x7f1201dd

.field public static final adb_wireless_qrcode_pairing_title:I = 0x7f1201de

.field public static final adb_wireless_qrcode_summary:I = 0x7f1201df

.field public static final adb_wireless_settings:I = 0x7f1201e0

.field public static final adb_wireless_verifying_qrcode_text:I = 0x7f1201e1

.field public static final adbwifi_warning_message:I = 0x7f1201e2

.field public static final adbwifi_warning_title:I = 0x7f1201e3

.field public static final add:I = 0x7f1201e4

.field public static final add_a_language:I = 0x7f1201e5

.field public static final add_account_label:I = 0x7f1201e6

.field public static final add_auto_startup_application:I = 0x7f1201e7

.field public static final add_back_fingerprint_instruction_msg:I = 0x7f1201e8

.field public static final add_broadcast_audio_source:I = 0x7f1201e9

.field public static final add_broadsize_fingerprint_instruction_msg:I = 0x7f1201ea

.field public static final add_device_admin:I = 0x7f1201eb

.field public static final add_device_admin_msg:I = 0x7f1201ec

.field public static final add_facerecoginition_text:I = 0x7f1201ed

.field public static final add_fingerprint_cancel_button_text:I = 0x7f1201ee

.field public static final add_fingerprint_dirty_fingerprint:I = 0x7f1201ef

.field public static final add_fingerprint_done_msg:I = 0x7f1201f0

.field public static final add_fingerprint_done_title:I = 0x7f1201f1

.field public static final add_fingerprint_edge_instruction_msg:I = 0x7f1201f2

.field public static final add_fingerprint_edge_instruction_title:I = 0x7f1201f3

.field public static final add_fingerprint_failed_retry_text:I = 0x7f1201f4

.field public static final add_fingerprint_finished_button_text:I = 0x7f1201f5

.field public static final add_fingerprint_instruction_msg:I = 0x7f1201f6

.field public static final add_fingerprint_instruction_title:I = 0x7f1201f7

.field public static final add_fingerprint_move_fast:I = 0x7f1201f8

.field public static final add_fingerprint_name_suggest_text:I = 0x7f1201f9

.field public static final add_fingerprint_put_finger_edge_msg:I = 0x7f1201fa

.field public static final add_fingerprint_put_finger_msg:I = 0x7f1201fb

.field public static final add_fingerprint_put_finger_title:I = 0x7f1201fc

.field public static final add_fingerprint_similar_fingerprint_input_error_msg:I = 0x7f1201fd

.field public static final add_fingerprint_success_msg:I = 0x7f1201fe

.field public static final add_fingerprint_text:I = 0x7f1201ff

.field public static final add_fingerprint_toast_text:I = 0x7f120200

.field public static final add_front_fingerprint_instruction_msg:I = 0x7f120201

.field public static final add_guest_failed:I = 0x7f120202

.field public static final add_new:I = 0x7f120203

.field public static final add_one_text_app_name_once:I = 0x7f120204

.field public static final add_rule:I = 0x7f120205

.field public static final add_source_button_text:I = 0x7f120206

.field public static final add_source_group:I = 0x7f120207

.field public static final add_text_app_name:I = 0x7f120208

.field public static final add_text_app_name_here:I = 0x7f120209

.field public static final add_text_app_package_name:I = 0x7f12020a

.field public static final add_user_failed:I = 0x7f12020b

.field public static final add_virtual_keyboard:I = 0x7f12020c

.field public static final added_sources_dashboard_title:I = 0x7f12020d

.field public static final added_sources_title:I = 0x7f12020e

.field public static final adding_profile_owner_warning:I = 0x7f12020f

.field public static final additional_battery_info:I = 0x7f120210

.field public static final additional_permissions:I = 0x7f120211

.field public static final additional_permissions_more:I = 0x7f120212

.field public static final additional_system_update:I = 0x7f120213

.field public static final additional_system_update_menu:I = 0x7f120214

.field public static final additional_system_update_settings_list_item_title:I = 0x7f120215

.field public static final adjust_3d_depth_title:I = 0x7f120216

.field public static final adjust_font_size_answer_cn:I = 0x7f120217

.field public static final adjust_font_size_answer_global:I = 0x7f120218

.field public static final adjust_ligth:I = 0x7f120219

.field public static final adjust_temperature_title:I = 0x7f12021a

.field public static final admin_device_owner_message:I = 0x7f12021b

.field public static final admin_disabled_other_options:I = 0x7f12021c

.field public static final admin_financed_message:I = 0x7f12021d

.field public static final admin_more_details:I = 0x7f12021e

.field public static final admin_profile_owner_message:I = 0x7f12021f

.field public static final admin_profile_owner_user_message:I = 0x7f120220

.field public static final admin_support_more_info:I = 0x7f120221

.field public static final advanced_apps:I = 0x7f120222

.field public static final advanced_battery_graph_subtext:I = 0x7f120223

.field public static final advanced_battery_preference_summary:I = 0x7f120224

.field public static final advanced_battery_preference_summary_with_hours:I = 0x7f120225

.field public static final advanced_battery_preference_title:I = 0x7f120226

.field public static final advanced_battery_title:I = 0x7f120227

.field public static final advanced_gesture:I = 0x7f120228

.field public static final advanced_screen_paper_mode_title:I = 0x7f120229

.field public static final advanced_section_header:I = 0x7f12022a

.field public static final advanced_security_title:I = 0x7f12022b

.field public static final advanced_settings:I = 0x7f12022c

.field public static final advanced_settings_summary:I = 0x7f12022d

.field public static final afternoon:I = 0x7f12022e

.field public static final aggregate_rule_group_disabled:I = 0x7f12022f

.field public static final aggregate_rule_group_enabled:I = 0x7f120230

.field public static final aggregate_title:I = 0x7f120231

.field public static final agps_roaming:I = 0x7f120232

.field public static final agps_roaming_dia:I = 0x7f120233

.field public static final agps_roaming_dia_title:I = 0x7f120234

.field public static final agps_roaming_summary:I = 0x7f120235

.field public static final ai_button_title:I = 0x7f120236

.field public static final ai_button_title_global:I = 0x7f120237

.field public static final ai_call_assistant:I = 0x7f120238

.field public static final ai_key_double_click:I = 0x7f120239

.field public static final ai_key_long_press:I = 0x7f12023a

.field public static final ai_key_single_click:I = 0x7f12023b

.field public static final ai_settings_click_title:I = 0x7f12023c

.field public static final ai_settings_double_click_category:I = 0x7f12023d

.field public static final ai_settings_double_click_title:I = 0x7f12023e

.field public static final ai_settings_long_click_category:I = 0x7f12023f

.field public static final ai_settings_long_click_title:I = 0x7f120240

.field public static final ai_settings_single_press_des:I = 0x7f120241

.field public static final airplane_mode:I = 0x7f120242

.field public static final airplane_mode_summary:I = 0x7f120243

.field public static final alarm_ringtone_title:I = 0x7f120244

.field public static final alarm_sound_dialog_title:I = 0x7f120245

.field public static final alarm_sound_title:I = 0x7f120246

.field public static final alarm_stop:I = 0x7f120247

.field public static final alarm_stop_info:I = 0x7f120248

.field public static final alarm_template:I = 0x7f120249

.field public static final alarm_template_far:I = 0x7f12024a

.field public static final alarm_volume_option_title:I = 0x7f12024b

.field public static final alarm_volume_title:I = 0x7f12024c

.field public static final alarms_and_reminders_footer_title:I = 0x7f12024d

.field public static final alarms_and_reminders_label:I = 0x7f12024e

.field public static final alarms_and_reminders_switch_title:I = 0x7f12024f

.field public static final alarms_and_reminders_title:I = 0x7f120250

.field public static final all_apps:I = 0x7f120251

.field public static final all_network_unavailable:I = 0x7f120252

.field public static final all_supported_app_locales_title:I = 0x7f120253

.field public static final all_volume_title:I = 0x7f120254

.field public static final allow:I = 0x7f120255

.field public static final allow_bind_app_widget_activity_allow_bind:I = 0x7f120256

.field public static final allow_bind_app_widget_activity_allow_bind_title:I = 0x7f120257

.field public static final allow_bind_app_widget_activity_always_allow_bind:I = 0x7f120258

.field public static final allow_data_usage_title:I = 0x7f120259

.field public static final allow_device_id_access:I = 0x7f12025a

.field public static final allow_float_summary:I = 0x7f12025b

.field public static final allow_float_title:I = 0x7f12025c

.field public static final allow_interruption:I = 0x7f12025d

.field public static final allow_interruption_summary:I = 0x7f12025e

.field public static final allow_keyguard_summary:I = 0x7f12025f

.field public static final allow_keyguard_title:I = 0x7f120260

.field public static final allow_lights_title:I = 0x7f120261

.field public static final allow_manage_external_storage_description:I = 0x7f120262

.field public static final allow_mock_location:I = 0x7f120263

.field public static final allow_mock_location_summary:I = 0x7f120264

.field public static final allow_mock_modem:I = 0x7f120265

.field public static final allow_mock_modem_summary:I = 0x7f120266

.field public static final allow_oaid_used:I = 0x7f120267

.field public static final allow_oaid_used_desc:I = 0x7f120268

.field public static final allow_ongoing_title:I = 0x7f120269

.field public static final allow_overlay_description:I = 0x7f12026a

.field public static final allow_sound_title:I = 0x7f12026b

.field public static final allow_turn_screen_on:I = 0x7f12026c

.field public static final allow_turn_screen_on_description:I = 0x7f12026d

.field public static final allow_vibrate_title:I = 0x7f12026e

.field public static final alpha_build:I = 0x7f12026f

.field public static final already_delete_system_app:I = 0x7f120270

.field public static final always:I = 0x7f120271

.field public static final always_allow_bind_appwidgets_text:I = 0x7f120272

.field public static final always_on:I = 0x7f120273

.field public static final always_running:I = 0x7f120274

.field public static final am:I = 0x7f120275

.field public static final amber_alerts_summary:I = 0x7f120276

.field public static final amber_alerts_title:I = 0x7f120277

.field public static final ambient_camera_battery_saver_off:I = 0x7f120278

.field public static final ambient_camera_summary_battery_saver_on:I = 0x7f120279

.field public static final ambient_display_category_triggers:I = 0x7f12027a

.field public static final ambient_display_pickup_summary:I = 0x7f12027b

.field public static final ambient_display_pickup_title:I = 0x7f12027c

.field public static final ambient_display_screen_summary_always_on:I = 0x7f12027d

.field public static final ambient_display_screen_summary_notifications:I = 0x7f12027e

.field public static final ambient_display_screen_title:I = 0x7f12027f

.field public static final ambient_display_summary:I = 0x7f120280

.field public static final ambient_display_tap_screen_summary:I = 0x7f120281

.field public static final ambient_display_tap_screen_title:I = 0x7f120282

.field public static final ambient_display_title:I = 0x7f120283

.field public static final ambient_display_wake_screen_title:I = 0x7f120284

.field public static final anc_auto:I = 0x7f120285

.field public static final anc_depth:I = 0x7f120286

.field public static final anc_mild:I = 0x7f120287

.field public static final android_beam_disabled_summary:I = 0x7f120288

.field public static final android_beam_explained:I = 0x7f120289

.field public static final android_beam_label:I = 0x7f12028a

.field public static final android_beam_off_summary:I = 0x7f12028b

.field public static final android_beam_on_summary:I = 0x7f12028c

.field public static final android_beam_settings_title:I = 0x7f12028d

.field public static final android_beam_tips:I = 0x7f12028e

.field public static final android_beam_witch_label:I = 0x7f12028f

.field public static final android_version_pending_update_summary:I = 0x7f120290

.field public static final android_version_summary:I = 0x7f120291

.field public static final androidx_startup:I = 0x7f120292

.field public static final angle_enabled_app:I = 0x7f120293

.field public static final angle_enabled_app_not_set:I = 0x7f120294

.field public static final angle_enabled_app_set:I = 0x7f120295

.field public static final animator_duration_scale_title:I = 0x7f120296

.field public static final another_migration_already_in_progress:I = 0x7f120297

.field public static final anti_spam:I = 0x7f120298

.field public static final aod_and_lock_screen_settings_title:I = 0x7f120299

.field public static final aod_close_color_inversion:I = 0x7f12029a

.field public static final aod_name_wanxiang:I = 0x7f12029b

.field public static final aod_notification_style:I = 0x7f12029c

.field public static final aod_notification_style_animation:I = 0x7f12029d

.field public static final aod_notification_style_none:I = 0x7f12029e

.field public static final aod_notification_style_screen_on:I = 0x7f12029f

.field public static final aod_settings_category_name:I = 0x7f1202a0

.field public static final aod_settings_title:I = 0x7f1202a1

.field public static final aod_show_mode_title:I = 0x7f1202a2

.field public static final aod_style_description:I = 0x7f1202a3

.field public static final aod_style_title:I = 0x7f1202a4

.field public static final aod_to_close:I = 0x7f1202a5

.field public static final ap_connect_failed:I = 0x7f1202a6

.field public static final ap_connected_battery_level:I = 0x7f1202a7

.field public static final apn_apn:I = 0x7f1202a8

.field public static final apn_auth_type:I = 0x7f1202a9

.field public static final apn_auth_type_chap:I = 0x7f1202aa

.field public static final apn_auth_type_none:I = 0x7f1202ab

.field public static final apn_auth_type_pap:I = 0x7f1202ac

.field public static final apn_auth_type_pap_chap:I = 0x7f1202ad

.field public static final apn_chinaunicom_3gnet_general:I = 0x7f1202ae

.field public static final apn_chinaunicom_3gwap_general:I = 0x7f1202af

.field public static final apn_edit:I = 0x7f1202b0

.field public static final apn_general:I = 0x7f1202b1

.field public static final apn_http_port:I = 0x7f1202b2

.field public static final apn_http_proxy:I = 0x7f1202b3

.field public static final apn_mcc:I = 0x7f1202b4

.field public static final apn_mms:I = 0x7f1202b5

.field public static final apn_mms_port:I = 0x7f1202b6

.field public static final apn_mms_proxy:I = 0x7f1202b7

.field public static final apn_mmsc:I = 0x7f1202b8

.field public static final apn_mnc:I = 0x7f1202b9

.field public static final apn_name:I = 0x7f1202ba

.field public static final apn_new:I = 0x7f1202bb

.field public static final apn_not_set:I = 0x7f1202bc

.field public static final apn_not_set_for_mvno:I = 0x7f1202bd

.field public static final apn_password:I = 0x7f1202be

.field public static final apn_ppp_number:I = 0x7f1202bf

.field public static final apn_protocol:I = 0x7f1202c0

.field public static final apn_roaming_protocol:I = 0x7f1202c1

.field public static final apn_save:I = 0x7f1202c2

.field public static final apn_save_msg:I = 0x7f1202c3

.field public static final apn_server:I = 0x7f1202c4

.field public static final apn_settings:I = 0x7f1202c5

.field public static final apn_settings_not_available:I = 0x7f1202c6

.field public static final apn_type:I = 0x7f1202c7

.field public static final apn_user:I = 0x7f1202c8

.field public static final app_and_notification_dashboard_summary:I = 0x7f1202c9

.field public static final app_and_notification_dashboard_title:I = 0x7f1202ca

.field public static final app_backup:I = 0x7f1202cb

.field public static final app_bluetooth_device_blacklist:I = 0x7f1202cc

.field public static final app_cellular_data_usage:I = 0x7f1202cd

.field public static final app_default_dashboard_title:I = 0x7f1202ce

.field public static final app_detail:I = 0x7f1202cf

.field public static final app_disable_dlg_positive:I = 0x7f1202d0

.field public static final app_disable_dlg_text:I = 0x7f1202d1

.field public static final app_disable_dlg_title:I = 0x7f1202d2

.field public static final app_disable_notifications_dlg_title:I = 0x7f1202d3

.field public static final app_enable_bluetooth_record:I = 0x7f1202d4

.field public static final app_factory_reset:I = 0x7f1202d5

.field public static final app_factory_reset_dlg_text:I = 0x7f1202d6

.field public static final app_factory_reset_dlg_title:I = 0x7f1202d7

.field public static final app_floating_window_dlg_disable:I = 0x7f1202d8

.field public static final app_floating_window_dlg_enable:I = 0x7f1202d9

.field public static final app_forward_locked:I = 0x7f1202da

.field public static final app_function:I = 0x7f1202db

.field public static final app_info_all_services_label:I = 0x7f1202dc

.field public static final app_info_storage_title:I = 0x7f1202dd

.field public static final app_install_details_group_title:I = 0x7f1202de

.field public static final app_install_details_summary:I = 0x7f1202df

.field public static final app_install_details_title:I = 0x7f1202e0

.field public static final app_install_location_summary:I = 0x7f1202e1

.field public static final app_install_location_title:I = 0x7f1202e2

.field public static final app_invisible:I = 0x7f1202e3

.field public static final app_launch_add_link:I = 0x7f1202e4

.field public static final app_launch_checking_links_title:I = 0x7f1202e5

.field public static final app_launch_dialog_cancel:I = 0x7f1202e6

.field public static final app_launch_dialog_ok:I = 0x7f1202e7

.field public static final app_launch_domain_links_title:I = 0x7f1202e8

.field public static final app_launch_footer:I = 0x7f1202e9

.field public static final app_launch_links_category:I = 0x7f1202ea

.field public static final app_launch_open_domain_urls_summary:I = 0x7f1202eb

.field public static final app_launch_open_domain_urls_title:I = 0x7f1202ec

.field public static final app_launch_other_defaults_title:I = 0x7f1202ed

.field public static final app_launch_supported_domain_urls_title:I = 0x7f1202ee

.field public static final app_launch_supported_links_add:I = 0x7f1202ef

.field public static final app_launch_supported_links_subtext:I = 0x7f1202f0

.field public static final app_launch_top_intro_message:I = 0x7f1202f1

.field public static final app_launch_verified_links_info_description:I = 0x7f1202f2

.field public static final app_link_open_always:I = 0x7f1202f3

.field public static final app_link_open_ask:I = 0x7f1202f4

.field public static final app_link_open_never:I = 0x7f1202f5

.field public static final app_link_title:I = 0x7f1202f6

.field public static final app_list_memory_use:I = 0x7f1202f7

.field public static final app_list_preference_none:I = 0x7f1202f8

.field public static final app_locale_picker_summary:I = 0x7f1202f9

.field public static final app_locale_picker_title:I = 0x7f1202fa

.field public static final app_locale_preference_title:I = 0x7f1202fb

.field public static final app_locales_picker_menu_title:I = 0x7f1202fc

.field public static final app_memory_use:I = 0x7f1202fd

.field public static final app_mobile:I = 0x7f1202fe

.field public static final app_name:I = 0x7f1202ff

.field public static final app_name_label:I = 0x7f120300

.field public static final app_name_na:I = 0x7f120301

.field public static final app_names_concatenation_template_2:I = 0x7f120302

.field public static final app_names_concatenation_template_3:I = 0x7f120303

.field public static final app_not_found_dlg_text:I = 0x7f120304

.field public static final app_not_found_dlg_title:I = 0x7f120305

.field public static final app_notification_block_summary:I = 0x7f120306

.field public static final app_notification_block_title:I = 0x7f120307

.field public static final app_notification_field:I = 0x7f120308

.field public static final app_notification_field_summary:I = 0x7f120309

.field public static final app_notification_importance_title:I = 0x7f12030a

.field public static final app_notification_listing_summary_zero:I = 0x7f12030b

.field public static final app_notification_override_dnd_summary:I = 0x7f12030c

.field public static final app_notification_override_dnd_title:I = 0x7f12030d

.field public static final app_notification_preferences:I = 0x7f12030e

.field public static final app_notification_row_banned:I = 0x7f12030f

.field public static final app_notification_row_priority:I = 0x7f120310

.field public static final app_notification_row_sensitive:I = 0x7f120311

.field public static final app_notification_visibility_override_title:I = 0x7f120312

.field public static final app_notifications_dialog_done:I = 0x7f120313

.field public static final app_notifications_off_desc:I = 0x7f120314

.field public static final app_notifications_switch_label:I = 0x7f120315

.field public static final app_notifications_title:I = 0x7f120316

.field public static final app_ops_never_used:I = 0x7f120317

.field public static final app_ops_running:I = 0x7f120318

.field public static final app_permission_summary_allowed:I = 0x7f120319

.field public static final app_permission_summary_not_allowed:I = 0x7f12031a

.field public static final app_permissions:I = 0x7f12031b

.field public static final app_permissions_summary:I = 0x7f12031c

.field public static final app_permissions_summary_more:I = 0x7f12031d

.field public static final app_pinning_intro:I = 0x7f12031e

.field public static final app_pinning_main_switch_title:I = 0x7f12031f

.field public static final app_process_limit_title:I = 0x7f120320

.field public static final app_restricted_settings_lockscreen_title:I = 0x7f120321

.field public static final app_restrictions_custom_label:I = 0x7f120322

.field public static final app_sees_restricted_accounts:I = 0x7f120323

.field public static final app_sees_restricted_accounts_and_controlled_by:I = 0x7f120324

.field public static final app_settings_link:I = 0x7f120325

.field public static final app_switch_key:I = 0x7f120326

.field public static final app_uninstall_sounds_enable_title:I = 0x7f120327

.field public static final app_usage_cycle:I = 0x7f120328

.field public static final app_usage_preference:I = 0x7f120329

.field public static final app_version:I = 0x7f12032a

.field public static final app_wifi:I = 0x7f12032b

.field public static final appbar_scrolling_view_behavior:I = 0x7f12032c

.field public static final application_data:I = 0x7f12032d

.field public static final application_forbid_auto_startup:I = 0x7f12032e

.field public static final application_info_label:I = 0x7f12032f

.field public static final application_item_already_permission:I = 0x7f120330

.field public static final application_item_disable:I = 0x7f120331

.field public static final application_item_permission:I = 0x7f120332

.field public static final application_lock_name:I = 0x7f120333

.field public static final application_manager_section_title:I = 0x7f120334

.field public static final application_mode_empty_text:I = 0x7f120335

.field public static final application_mode_name:I = 0x7f120336

.field public static final application_permission_manager:I = 0x7f120337

.field public static final application_permissions:I = 0x7f120338

.field public static final application_restrictions:I = 0x7f120339

.field public static final application_size_label:I = 0x7f12033a

.field public static final applications_settings:I = 0x7f12033b

.field public static final applications_settings_header:I = 0x7f12033c

.field public static final applications_settings_summary:I = 0x7f12033d

.field public static final applications_settings_title:I = 0x7f12033e

.field public static final apply:I = 0x7f12033f

.field public static final apply_fingerprint_to:I = 0x7f120340

.field public static final apply_fingerprint_to_category_title:I = 0x7f120341

.field public static final apply_now_button_text:I = 0x7f120342

.field public static final apply_password_to_category_title:I = 0x7f120343

.field public static final apply_update_fail:I = 0x7f120344

.field public static final approve_title:I = 0x7f120345

.field public static final apps_dashboard_title:I = 0x7f120346

.field public static final apps_storage:I = 0x7f120347

.field public static final apps_summary:I = 0x7f120348

.field public static final apps_summary_example:I = 0x7f120349

.field public static final apps_with_restrictions_header:I = 0x7f12034a

.field public static final apps_with_restrictions_settings_button:I = 0x7f12034b

.field public static final art_verifier_for_debuggable_summary:I = 0x7f12034c

.field public static final art_verifier_for_debuggable_title:I = 0x7f12034d

.field public static final ask_compatibility:I = 0x7f12034e

.field public static final ask_for_delete:I = 0x7f12034f

.field public static final assist_access_context_summary:I = 0x7f120350

.field public static final assist_access_context_title:I = 0x7f120351

.field public static final assist_access_screenshot_summary:I = 0x7f120352

.field public static final assist_access_screenshot_title:I = 0x7f120353

.field public static final assist_and_voice_input_title:I = 0x7f120354

.field public static final assist_flash_summary:I = 0x7f120355

.field public static final assist_flash_title:I = 0x7f120356

.field public static final assist_footer:I = 0x7f120357

.field public static final assist_gesture_title:I = 0x7f120358

.field public static final assistance_category_title:I = 0x7f120359

.field public static final assistant_corner_gesture_summary:I = 0x7f12035a

.field public static final assistant_corner_gesture_title:I = 0x7f12035b

.field public static final assistant_gesture_category_title:I = 0x7f12035c

.field public static final assistant_long_press_home_gesture_summary:I = 0x7f12035d

.field public static final assistant_long_press_home_gesture_title:I = 0x7f12035e

.field public static final assistant_security_warning:I = 0x7f12035f

.field public static final assistant_security_warning_agree:I = 0x7f120360

.field public static final assistant_security_warning_disagree:I = 0x7f120361

.field public static final assistant_security_warning_title:I = 0x7f120362

.field public static final assisted_gps:I = 0x7f120363

.field public static final assisted_gps_disabled:I = 0x7f120364

.field public static final assisted_gps_enabled:I = 0x7f120365

.field public static final asst_capabilities_actions_replies_summary:I = 0x7f120366

.field public static final asst_capabilities_actions_replies_title:I = 0x7f120367

.field public static final asst_capability_prioritizer_summary:I = 0x7f120368

.field public static final asst_capability_prioritizer_title:I = 0x7f120369

.field public static final asst_capability_ranking_summary:I = 0x7f12036a

.field public static final asst_capability_ranking_title:I = 0x7f12036b

.field public static final asst_feedback_indicator_summary:I = 0x7f12036c

.field public static final asst_feedback_indicator_title:I = 0x7f12036d

.field public static final asst_importance_reset_summary:I = 0x7f12036e

.field public static final asst_importance_reset_title:I = 0x7f12036f

.field public static final attempt_other_network:I = 0x7f120370

.field public static final au_pay:I = 0x7f120371

.field public static final audio_category_title:I = 0x7f120372

.field public static final audio_not_synced:I = 0x7f120373

.field public static final audio_profile_settings_title:I = 0x7f120374

.field public static final audio_record_proc_title:I = 0x7f120375

.field public static final audio_share_cancel:I = 0x7f120376

.field public static final audio_share_confirm:I = 0x7f120377

.field public static final audio_synced:I = 0x7f120378

.field public static final auto_adjst_effect_summary:I = 0x7f120379

.field public static final auto_adjust_effect_title:I = 0x7f12037a

.field public static final auto_anc_closed:I = 0x7f12037b

.field public static final auto_anc_fifteen_recover:I = 0x7f12037c

.field public static final auto_anc_five_recover:I = 0x7f12037d

.field public static final auto_anc_not_recover:I = 0x7f12037e

.field public static final auto_anc_ten_recover:I = 0x7f12037f

.field public static final auto_brightness_default_title:I = 0x7f120380

.field public static final auto_brightness_description:I = 0x7f120381

.field public static final auto_brightness_disclaimer:I = 0x7f120382

.field public static final auto_brightness_high_title:I = 0x7f120383

.field public static final auto_brightness_low_title:I = 0x7f120384

.field public static final auto_brightness_off_summary:I = 0x7f120385

.field public static final auto_brightness_off_title:I = 0x7f120386

.field public static final auto_brightness_subtitle:I = 0x7f120387

.field public static final auto_brightness_summary:I = 0x7f120388

.field public static final auto_brightness_summary_default:I = 0x7f120389

.field public static final auto_brightness_summary_high:I = 0x7f12038a

.field public static final auto_brightness_summary_low:I = 0x7f12038b

.field public static final auto_brightness_summary_off:I = 0x7f12038c

.field public static final auto_brightness_summary_on:I = 0x7f12038d

.field public static final auto_brightness_summary_very_high:I = 0x7f12038e

.field public static final auto_brightness_summary_very_low:I = 0x7f12038f

.field public static final auto_brightness_title:I = 0x7f120390

.field public static final auto_brightness_very_high_summary:I = 0x7f120391

.field public static final auto_brightness_very_high_title:I = 0x7f120392

.field public static final auto_brightness_very_low_title:I = 0x7f120393

.field public static final auto_caps:I = 0x7f120394

.field public static final auto_caps_summary:I = 0x7f120395

.field public static final auto_clean_description:I = 0x7f120396

.field public static final auto_clean_description_pad:I = 0x7f120397

.field public static final auto_clean_summary:I = 0x7f120398

.field public static final auto_clean_summary_new:I = 0x7f120399

.field public static final auto_clean_title:I = 0x7f12039a

.field public static final auto_disable_broadcast_audio:I = 0x7f12039b

.field public static final auto_disable_screenbuttons_ask:I = 0x7f12039c

.field public static final auto_disable_screenbuttons_auto:I = 0x7f12039d

.field public static final auto_disable_screenbuttons_cancel:I = 0x7f12039e

.field public static final auto_disable_screenbuttons_no:I = 0x7f12039f

.field public static final auto_disable_screenbuttons_rotation_summary:I = 0x7f1203a0

.field public static final auto_disable_screenbuttons_rotation_title:I = 0x7f1203a1

.field public static final auto_disable_screenbuttons_summary:I = 0x7f1203a2

.field public static final auto_disable_screenbuttons_title:I = 0x7f1203a3

.field public static final auto_dual_clock_aod_summary:I = 0x7f1203a4

.field public static final auto_dual_clock_summary:I = 0x7f1203a5

.field public static final auto_dual_clock_title:I = 0x7f1203a6

.field public static final auto_launch_disable_text:I = 0x7f1203a7

.field public static final auto_launch_enable_text:I = 0x7f1203a8

.field public static final auto_launch_label:I = 0x7f1203a9

.field public static final auto_launch_label_generic:I = 0x7f1203aa

.field public static final auto_punctuate:I = 0x7f1203ab

.field public static final auto_punctuate_summary:I = 0x7f1203ac

.field public static final auto_replace:I = 0x7f1203ad

.field public static final auto_replace_summary:I = 0x7f1203ae

.field public static final auto_restore_summary:I = 0x7f1203af

.field public static final auto_restore_title:I = 0x7f1203b0

.field public static final auto_rotate_camera_lock_summary:I = 0x7f1203b1

.field public static final auto_rotate_camera_lock_title:I = 0x7f1203b2

.field public static final auto_rotate_link_a11y:I = 0x7f1203b3

.field public static final auto_rotate_manage_permission_button:I = 0x7f1203b4

.field public static final auto_rotate_option_face_based:I = 0x7f1203b5

.field public static final auto_rotate_option_off:I = 0x7f1203b6

.field public static final auto_rotate_option_on:I = 0x7f1203b7

.field public static final auto_rotate_screen_summary:I = 0x7f1203b8

.field public static final auto_rotate_settings_primary_switch_title:I = 0x7f1203b9

.field public static final auto_rotate_summary_a11y:I = 0x7f1203ba

.field public static final auto_rotate_summary_no_permission:I = 0x7f1203bb

.field public static final auto_rotate_switch_face_based:I = 0x7f1203bc

.field public static final auto_rule:I = 0x7f1203bd

.field public static final auto_silent:I = 0x7f1203be

.field public static final auto_startup_application_manage:I = 0x7f1203bf

.field public static final auto_sync_account_summary:I = 0x7f1203c0

.field public static final auto_sync_account_title:I = 0x7f1203c1

.field public static final auto_sync_personal_account_title:I = 0x7f1203c2

.field public static final auto_sync_work_account_title:I = 0x7f1203c3

.field public static final auto_task_operation_close:I = 0x7f1203c4

.field public static final autofill_app:I = 0x7f1203c5

.field public static final autofill_confirmation_message:I = 0x7f1203c6

.field public static final autofill_keywords:I = 0x7f1203c7

.field public static final autofill_logging_level_title:I = 0x7f1203c8

.field public static final autofill_max_partitions:I = 0x7f1203c9

.field public static final autofill_max_visible_datasets:I = 0x7f1203ca

.field public static final autofill_passwords:I = 0x7f1203cb

.field public static final autofill_passwords_count_placeholder:I = 0x7f1203cc

.field public static final autofill_reset_developer_options:I = 0x7f1203cd

.field public static final autofill_reset_developer_options_complete:I = 0x7f1203ce

.field public static final automatic_brightness:I = 0x7f1203cf

.field public static final automatic_storage_manager_activation_warning:I = 0x7f1203d0

.field public static final automatic_storage_manager_days_title:I = 0x7f1203d1

.field public static final automatic_storage_manager_deactivation_warning:I = 0x7f1203d2

.field public static final automatic_storage_manager_freed_bytes:I = 0x7f1203d3

.field public static final automatic_storage_manager_preference_title:I = 0x7f1203d4

.field public static final automatic_storage_manager_primary_switch_title:I = 0x7f1203d5

.field public static final automatic_storage_manager_settings:I = 0x7f1203d6

.field public static final automatic_storage_manager_text:I = 0x7f1203d7

.field public static final automatic_system_heap_dump_summary:I = 0x7f1203d8

.field public static final automatic_system_heap_dump_title:I = 0x7f1203d9

.field public static final available_size:I = 0x7f1203da

.field public static final available_via_carrier:I = 0x7f1203db

.field public static final available_via_passpoint:I = 0x7f1203dc

.field public static final available_virtual_keyboard_category:I = 0x7f1203dd

.field public static final avatar_picker_title:I = 0x7f1203de

.field public static final average_memory_use:I = 0x7f1203df

.field public static final average_used:I = 0x7f1203e0

.field public static final avoid_ui:I = 0x7f1203e1

.field public static final awake:I = 0x7f1203e2

.field public static final aware_summary_when_bedtime_on:I = 0x7f1203e3

.field public static final ba_high_security_level:I = 0x7f1203e4

.field public static final ba_high_security_level_summary:I = 0x7f1203e5

.field public static final ba_low_security_level:I = 0x7f1203e6

.field public static final ba_low_security_level_summary:I = 0x7f1203e7

.field public static final ba_mid_security_level:I = 0x7f1203e8

.field public static final ba_mid_security_level_summary:I = 0x7f1203e9

.field public static final ba_security_settings:I = 0x7f1203ea

.field public static final ba_security_summary:I = 0x7f1203eb

.field public static final ba_security_summary_no_pin:I = 0x7f1203ec

.field public static final ba_security_summary_pin_unavailable:I = 0x7f1203ed

.field public static final back:I = 0x7f1203ee

.field public static final back_button:I = 0x7f1203ef

.field public static final back_double_tap:I = 0x7f1203f0

.field public static final back_key:I = 0x7f1203f1

.field public static final back_navigation_animation:I = 0x7f1203f2

.field public static final back_navigation_animation_dialog:I = 0x7f1203f3

.field public static final back_navigation_animation_summary:I = 0x7f1203f4

.field public static final back_sensitivity_dialog_message:I = 0x7f1203f5

.field public static final back_sensitivity_dialog_title:I = 0x7f1203f6

.field public static final back_tap:I = 0x7f1203f7

.field public static final back_triple_tap:I = 0x7f1203f8

.field public static final background_activity_disabled_dialog_text:I = 0x7f1203f9

.field public static final background_activity_summary:I = 0x7f1203fa

.field public static final background_activity_summary_allowlisted:I = 0x7f1203fb

.field public static final background_activity_summary_disabled:I = 0x7f1203fc

.field public static final background_activity_title:I = 0x7f1203fd

.field public static final background_activity_warning_dialog_text:I = 0x7f1203fe

.field public static final background_activity_warning_dialog_title:I = 0x7f1203ff

.field public static final background_check_pref:I = 0x7f120400

.field public static final background_check_title:I = 0x7f120401

.field public static final background_data:I = 0x7f120402

.field public static final background_data_dialog_message:I = 0x7f120403

.field public static final background_data_dialog_title:I = 0x7f120404

.field public static final background_data_summary:I = 0x7f120405

.field public static final background_process_stop_description:I = 0x7f120406

.field public static final background_restrict_app_dialog_message:I = 0x7f120407

.field public static final background_traffic:I = 0x7f120408

.field public static final backtouch_enable_title:I = 0x7f120409

.field public static final backup_at_once:I = 0x7f12040a

.field public static final backup_calling_setting_summary:I = 0x7f12040b

.field public static final backup_calling_settings_title:I = 0x7f12040c

.field public static final backup_configure_account_default_summary:I = 0x7f12040d

.field public static final backup_configure_account_title:I = 0x7f12040e

.field public static final backup_data:I = 0x7f12040f

.field public static final backup_data_management_title:I = 0x7f120410

.field public static final backup_data_summary:I = 0x7f120411

.field public static final backup_data_title:I = 0x7f120412

.field public static final backup_difference_content:I = 0x7f120413

.field public static final backup_difference_title:I = 0x7f120414

.field public static final backup_disabled:I = 0x7f120415

.field public static final backup_erase_dialog_message:I = 0x7f120416

.field public static final backup_erase_dialog_title:I = 0x7f120417

.field public static final backup_inactive_title:I = 0x7f120418

.field public static final backup_more_settings:I = 0x7f120419

.field public static final backup_pw_cancel_button_text:I = 0x7f12041a

.field public static final backup_pw_set_button_text:I = 0x7f12041b

.field public static final backup_restore_data_content:I = 0x7f12041c

.field public static final backup_restore_data_title:I = 0x7f12041d

.field public static final backup_section_title:I = 0x7f12041e

.field public static final backup_set_clear_success:I = 0x7f12041f

.field public static final backup_set_clear_title:I = 0x7f120420

.field public static final backup_set_confirm:I = 0x7f120421

.field public static final backup_set_confirm_clear:I = 0x7f120422

.field public static final backup_set_enter_new_pw:I = 0x7f120423

.field public static final backup_set_enter_new_pw_again:I = 0x7f120424

.field public static final backup_set_enter_old_pw:I = 0x7f120425

.field public static final backup_set_enter_pw_title:I = 0x7f120426

.field public static final backup_set_error_pw:I = 0x7f120427

.field public static final backup_set_modify_title:I = 0x7f120428

.field public static final backup_set_new_continue:I = 0x7f120429

.field public static final backup_set_new_exit:I = 0x7f12042a

.field public static final backup_set_new_pw_confirm_hint:I = 0x7f12042b

.field public static final backup_set_new_pw_confirm_summary:I = 0x7f12042c

.field public static final backup_set_new_pw_title:I = 0x7f12042d

.field public static final backup_set_null_pw:I = 0x7f12042e

.field public static final backup_set_pw_confirm_title:I = 0x7f12042f

.field public static final backup_summary_state_off:I = 0x7f120430

.field public static final backup_summary_state_on:I = 0x7f120431

.field public static final badge_title:I = 0x7f120432

.field public static final band_24G:I = 0x7f120433

.field public static final band_5G:I = 0x7f120434

.field public static final baseband_version:I = 0x7f120435

.field public static final batch_delete_saved_network:I = 0x7f120436

.field public static final batch_delete_saved_networks:I = 0x7f120437

.field public static final battery_action_app_details:I = 0x7f120438

.field public static final battery_action_app_settings:I = 0x7f120439

.field public static final battery_action_bluetooth:I = 0x7f12043a

.field public static final battery_action_display:I = 0x7f12043b

.field public static final battery_action_stop:I = 0x7f12043c

.field public static final battery_action_wifi:I = 0x7f12043d

.field public static final battery_active_for:I = 0x7f12043e

.field public static final battery_app_usage_for:I = 0x7f12043f

.field public static final battery_app_usage_for_past_24:I = 0x7f120440

.field public static final battery_auto_restriction_summary:I = 0x7f120441

.field public static final battery_auto_restriction_title:I = 0x7f120442

.field public static final battery_bg_usage:I = 0x7f120443

.field public static final battery_bg_usage_24hr:I = 0x7f120444

.field public static final battery_bg_usage_less_minute:I = 0x7f120445

.field public static final battery_bg_usage_less_minute_24hr:I = 0x7f120446

.field public static final battery_bg_usage_less_minute_with_period:I = 0x7f120447

.field public static final battery_bg_usage_with_period:I = 0x7f120448

.field public static final battery_desc_apps:I = 0x7f120449

.field public static final battery_desc_bluetooth:I = 0x7f12044a

.field public static final battery_desc_camera:I = 0x7f12044b

.field public static final battery_desc_display:I = 0x7f12044c

.field public static final battery_desc_flashlight:I = 0x7f12044d

.field public static final battery_desc_overcounted:I = 0x7f12044e

.field public static final battery_desc_radio:I = 0x7f12044f

.field public static final battery_desc_standby:I = 0x7f120450

.field public static final battery_desc_unaccounted:I = 0x7f120451

.field public static final battery_desc_users:I = 0x7f120452

.field public static final battery_desc_voice:I = 0x7f120453

.field public static final battery_desc_wifi:I = 0x7f120454

.field public static final battery_detail_background:I = 0x7f120455

.field public static final battery_detail_foreground:I = 0x7f120456

.field public static final battery_detail_info_title:I = 0x7f120457

.field public static final battery_detail_manage_title:I = 0x7f120458

.field public static final battery_detail_power_usage:I = 0x7f120459

.field public static final battery_detail_since_full_charge:I = 0x7f12045a

.field public static final battery_details_title:I = 0x7f12045b

.field public static final battery_footer_summary:I = 0x7f12045c

.field public static final battery_full_charge_last:I = 0x7f12045d

.field public static final battery_header_title_alternate:I = 0x7f12045e

.field public static final battery_indicator_style:I = 0x7f12045f

.field public static final battery_indicator_title:I = 0x7f120460

.field public static final battery_info_awake_battery:I = 0x7f120461

.field public static final battery_info_awake_plugged:I = 0x7f120462

.field public static final battery_info_health_cold:I = 0x7f120463

.field public static final battery_info_health_dead:I = 0x7f120464

.field public static final battery_info_health_good:I = 0x7f120465

.field public static final battery_info_health_label:I = 0x7f120466

.field public static final battery_info_health_over_voltage:I = 0x7f120467

.field public static final battery_info_health_overheat:I = 0x7f120468

.field public static final battery_info_health_unknown:I = 0x7f120469

.field public static final battery_info_health_unspecified_failure:I = 0x7f12046a

.field public static final battery_info_label:I = 0x7f12046b

.field public static final battery_info_level_label:I = 0x7f12046c

.field public static final battery_info_power_ac:I = 0x7f12046d

.field public static final battery_info_power_ac_usb:I = 0x7f12046e

.field public static final battery_info_power_label:I = 0x7f12046f

.field public static final battery_info_power_unknown:I = 0x7f120470

.field public static final battery_info_power_unplugged:I = 0x7f120471

.field public static final battery_info_power_usb:I = 0x7f120472

.field public static final battery_info_power_wireless:I = 0x7f120473

.field public static final battery_info_scale_label:I = 0x7f120474

.field public static final battery_info_screen_on:I = 0x7f120475

.field public static final battery_info_status_charging:I = 0x7f120476

.field public static final battery_info_status_charging_ac:I = 0x7f120477

.field public static final battery_info_status_charging_fast:I = 0x7f120478

.field public static final battery_info_status_charging_slow:I = 0x7f120479

.field public static final battery_info_status_charging_usb:I = 0x7f12047a

.field public static final battery_info_status_charging_wireless:I = 0x7f12047b

.field public static final battery_info_status_discharging:I = 0x7f12047c

.field public static final battery_info_status_full:I = 0x7f12047d

.field public static final battery_info_status_full_charged:I = 0x7f12047e

.field public static final battery_info_status_label:I = 0x7f12047f

.field public static final battery_info_status_not_charging:I = 0x7f120480

.field public static final battery_info_status_unknown:I = 0x7f120481

.field public static final battery_info_technology_label:I = 0x7f120482

.field public static final battery_info_temperature_label:I = 0x7f120483

.field public static final battery_info_temperature_units:I = 0x7f120484

.field public static final battery_info_uptime:I = 0x7f120485

.field public static final battery_info_voltage_label:I = 0x7f120486

.field public static final battery_info_voltage_units:I = 0x7f120487

.field public static final battery_last_full_charge:I = 0x7f120488

.field public static final battery_level_title:I = 0x7f120489

.field public static final battery_light_summary:I = 0x7f12048a

.field public static final battery_light_title:I = 0x7f12048b

.field public static final battery_manager_off:I = 0x7f12048c

.field public static final battery_manager_summary:I = 0x7f12048d

.field public static final battery_manager_summary_unsupported:I = 0x7f12048e

.field public static final battery_meter_very_low_overlay_symbol:I = 0x7f12048f

.field public static final battery_missing_link_a11y_message:I = 0x7f120490

.field public static final battery_missing_link_message:I = 0x7f120491

.field public static final battery_missing_message:I = 0x7f120492

.field public static final battery_msg_unaccounted:I = 0x7f120493

.field public static final battery_not_usage:I = 0x7f120494

.field public static final battery_not_usage_24hr:I = 0x7f120495

.field public static final battery_overall_usage:I = 0x7f120496

.field public static final battery_percentage:I = 0x7f120497

.field public static final battery_percentage_description:I = 0x7f120498

.field public static final battery_saver:I = 0x7f120499

.field public static final battery_saver_auto_no_schedule:I = 0x7f12049a

.field public static final battery_saver_auto_percentage:I = 0x7f12049b

.field public static final battery_saver_auto_percentage_summary:I = 0x7f12049c

.field public static final battery_saver_auto_routine:I = 0x7f12049d

.field public static final battery_saver_auto_routine_summary:I = 0x7f12049e

.field public static final battery_saver_auto_title:I = 0x7f12049f

.field public static final battery_saver_button_turn_off:I = 0x7f1204a0

.field public static final battery_saver_button_turn_on:I = 0x7f1204a1

.field public static final battery_saver_link_a11y:I = 0x7f1204a2

.field public static final battery_saver_main_switch_title:I = 0x7f1204a3

.field public static final battery_saver_master_switch_title:I = 0x7f1204a4

.field public static final battery_saver_off_scheduled_summary:I = 0x7f1204a5

.field public static final battery_saver_off_summary:I = 0x7f1204a6

.field public static final battery_saver_on_summary:I = 0x7f1204a7

.field public static final battery_saver_pref_auto_routine_summary:I = 0x7f1204a8

.field public static final battery_saver_schedule_settings_title:I = 0x7f1204a9

.field public static final battery_saver_seekbar_title:I = 0x7f1204aa

.field public static final battery_saver_seekbar_title_placeholder:I = 0x7f1204ab

.field public static final battery_saver_sticky_description_new:I = 0x7f1204ac

.field public static final battery_saver_sticky_title_new:I = 0x7f1204ad

.field public static final battery_saver_turn_on_automatically_never:I = 0x7f1204ae

.field public static final battery_saver_turn_on_automatically_pct:I = 0x7f1204af

.field public static final battery_saver_turn_on_automatically_title:I = 0x7f1204b0

.field public static final battery_saver_turn_on_summary:I = 0x7f1204b1

.field public static final battery_screen_usage:I = 0x7f1204b2

.field public static final battery_settings:I = 0x7f1204b3

.field public static final battery_settings_title:I = 0x7f1204b4

.field public static final battery_since_reset:I = 0x7f1204b5

.field public static final battery_since_unplugged:I = 0x7f1204b6

.field public static final battery_state_and_duration:I = 0x7f1204b7

.field public static final battery_stats_camera_on_label:I = 0x7f1204b8

.field public static final battery_stats_charging_label:I = 0x7f1204b9

.field public static final battery_stats_duration:I = 0x7f1204ba

.field public static final battery_stats_flashlight_on_label:I = 0x7f1204bb

.field public static final battery_stats_gps_on_label:I = 0x7f1204bc

.field public static final battery_stats_last_duration:I = 0x7f1204bd

.field public static final battery_stats_on_battery:I = 0x7f1204be

.field public static final battery_stats_phone_signal_label:I = 0x7f1204bf

.field public static final battery_stats_screen_on_label:I = 0x7f1204c0

.field public static final battery_stats_wake_lock_label:I = 0x7f1204c1

.field public static final battery_stats_wifi_running_label:I = 0x7f1204c2

.field public static final battery_status_title:I = 0x7f1204c3

.field public static final battery_sugg_apps_gps:I = 0x7f1204c4

.field public static final battery_sugg_apps_info:I = 0x7f1204c5

.field public static final battery_sugg_apps_settings:I = 0x7f1204c6

.field public static final battery_sugg_bluetooth_basic:I = 0x7f1204c7

.field public static final battery_sugg_bluetooth_headset:I = 0x7f1204c8

.field public static final battery_sugg_display:I = 0x7f1204c9

.field public static final battery_sugg_radio:I = 0x7f1204ca

.field public static final battery_sugg_wifi:I = 0x7f1204cb

.field public static final battery_suggestion_summary:I = 0x7f1204cc

.field public static final battery_suggestion_title:I = 0x7f1204cd

.field public static final battery_summary:I = 0x7f1204ce

.field public static final battery_summary_24hr:I = 0x7f1204cf

.field public static final battery_system_usage_for:I = 0x7f1204d0

.field public static final battery_system_usage_for_past_24:I = 0x7f1204d1

.field public static final battery_tip_dialog_message:I = 0x7f1204d2

.field public static final battery_tip_dialog_message_footer:I = 0x7f1204d3

.field public static final battery_tip_dialog_summary_message:I = 0x7f1204d4

.field public static final battery_tip_early_heads_up_done_summary:I = 0x7f1204d5

.field public static final battery_tip_early_heads_up_done_title:I = 0x7f1204d6

.field public static final battery_tip_early_heads_up_summary:I = 0x7f1204d7

.field public static final battery_tip_early_heads_up_title:I = 0x7f1204d8

.field public static final battery_tip_high_usage_summary:I = 0x7f1204d9

.field public static final battery_tip_high_usage_title:I = 0x7f1204da

.field public static final battery_tip_limited_temporarily_dialog_msg:I = 0x7f1204db

.field public static final battery_tip_limited_temporarily_dialog_resume_charge:I = 0x7f1204dc

.field public static final battery_tip_limited_temporarily_summary:I = 0x7f1204dd

.field public static final battery_tip_limited_temporarily_title:I = 0x7f1204de

.field public static final battery_tip_low_battery_summary:I = 0x7f1204df

.field public static final battery_tip_low_battery_title:I = 0x7f1204e0

.field public static final battery_tip_restrict_app_dialog_message:I = 0x7f1204e1

.field public static final battery_tip_restrict_app_dialog_ok:I = 0x7f1204e2

.field public static final battery_tip_restrict_apps_less_than_5_dialog_message:I = 0x7f1204e3

.field public static final battery_tip_restrict_apps_more_than_5_dialog_message:I = 0x7f1204e4

.field public static final battery_tip_smart_battery_summary:I = 0x7f1204e5

.field public static final battery_tip_smart_battery_title:I = 0x7f1204e6

.field public static final battery_tip_summary_summary:I = 0x7f1204e7

.field public static final battery_tip_summary_title:I = 0x7f1204e8

.field public static final battery_tip_unrestrict_app_dialog_cancel:I = 0x7f1204e9

.field public static final battery_tip_unrestrict_app_dialog_message:I = 0x7f1204ea

.field public static final battery_tip_unrestrict_app_dialog_ok:I = 0x7f1204eb

.field public static final battery_tip_unrestrict_app_dialog_title:I = 0x7f1204ec

.field public static final battery_total_and_bg_usage:I = 0x7f1204ed

.field public static final battery_total_and_bg_usage_24hr:I = 0x7f1204ee

.field public static final battery_total_and_bg_usage_with_period:I = 0x7f1204ef

.field public static final battery_total_usage:I = 0x7f1204f0

.field public static final battery_total_usage_24hr:I = 0x7f1204f1

.field public static final battery_total_usage_and_bg_less_minute_usage:I = 0x7f1204f2

.field public static final battery_total_usage_and_bg_less_minute_usage_24hr:I = 0x7f1204f3

.field public static final battery_total_usage_and_bg_less_minute_usage_with_period:I = 0x7f1204f4

.field public static final battery_total_usage_less_minute:I = 0x7f1204f5

.field public static final battery_total_usage_less_minute_24hr:I = 0x7f1204f6

.field public static final battery_total_usage_less_minute_with_period:I = 0x7f1204f7

.field public static final battery_total_usage_with_period:I = 0x7f1204f8

.field public static final battery_usage_background_less_than_one_minute:I = 0x7f1204f9

.field public static final battery_usage_chart:I = 0x7f1204fa

.field public static final battery_usage_chart_graph_hint:I = 0x7f1204fb

.field public static final battery_usage_for_background_time:I = 0x7f1204fc

.field public static final battery_usage_for_total_time:I = 0x7f1204fd

.field public static final battery_usage_screen_footer:I = 0x7f1204fe

.field public static final battery_usage_screen_footer_empty:I = 0x7f1204ff

.field public static final battery_usage_total_less_than_one_minute:I = 0x7f120500

.field public static final battery_usage_without_time:I = 0x7f120501

.field public static final battery_used_by:I = 0x7f120502

.field public static final battery_used_for:I = 0x7f120503

.field public static final bcast_audio_sync_state:I = 0x7f120504

.field public static final bcast_enable_audio_sync:I = 0x7f120505

.field public static final bcast_enable_metadata_sync:I = 0x7f120506

.field public static final bcast_info_update:I = 0x7f120507

.field public static final bcast_metadata_sync_state:I = 0x7f120508

.field public static final bcast_sink_volume_title:I = 0x7f120509

.field public static final bcast_source_address:I = 0x7f12050a

.field public static final bcast_source_enc_status:I = 0x7f12050b

.field public static final bcast_source_info_sid:I = 0x7f12050c

.field public static final bcast_source_metadata:I = 0x7f12050d

.field public static final bcast_update_code_summary:I = 0x7f12050e

.field public static final bcast_update_code_title:I = 0x7f12050f

.field public static final bearer:I = 0x7f120510

.field public static final beauty_camera:I = 0x7f120511

.field public static final beauty_camera_summary:I = 0x7f120512

.field public static final beauty_camera_switch:I = 0x7f120513

.field public static final beauty_fc_assistant:I = 0x7f120514

.field public static final billing_cycle:I = 0x7f120515

.field public static final billing_cycle_fragment_summary:I = 0x7f120516

.field public static final billing_cycle_less_than_one_day_left:I = 0x7f120517

.field public static final billing_cycle_none_left:I = 0x7f120518

.field public static final bind_xiaomi_account_confirm:I = 0x7f120519

.field public static final bind_xiaomi_account_dialog_summery:I = 0x7f12051a

.field public static final bind_xiaomi_account_success:I = 0x7f12051b

.field public static final biometric_settings_category_ways_to_unlock:I = 0x7f12051c

.field public static final biometric_settings_hand_back_to_guardian:I = 0x7f12051d

.field public static final biometric_settings_hand_back_to_guardian_ok:I = 0x7f12051e

.field public static final biometric_settings_intro:I = 0x7f12051f

.field public static final biometric_settings_use_biometric_for_apps:I = 0x7f120520

.field public static final biometric_settings_use_biometric_unlock_phone:I = 0x7f120521

.field public static final biometric_settings_use_face_or_fingerprint_preference_summary:I = 0x7f120522

.field public static final biometric_settings_use_face_preference_summary:I = 0x7f120523

.field public static final biometric_settings_use_fingerprint_preference_summary:I = 0x7f120524

.field public static final biometric_weak_improve_matching_title:I = 0x7f120525

.field public static final biometric_weak_liveliness_summary:I = 0x7f120526

.field public static final biometric_weak_liveliness_title:I = 0x7f120527

.field public static final biometrics_unlock_set_unlock_password:I = 0x7f120528

.field public static final biometrics_unlock_set_unlock_pattern:I = 0x7f120529

.field public static final biometrics_unlock_set_unlock_pin:I = 0x7f12052a

.field public static final biometrics_unlock_skip_biometrics:I = 0x7f12052b

.field public static final biometrics_unlock_title:I = 0x7f12052c

.field public static final blank_screen_no_device:I = 0x7f12052d

.field public static final blank_screen_no_record:I = 0x7f12052e

.field public static final ble_scan_notify_text:I = 0x7f12052f

.field public static final blob_expires_text:I = 0x7f120530

.field public static final blob_id_text:I = 0x7f120531

.field public static final blob_never_expires_text:I = 0x7f120532

.field public static final block_list_connected_devices:I = 0x7f120533

.field public static final block_list_diaglog_cancle:I = 0x7f120534

.field public static final block_list_diaglog_ok:I = 0x7f120535

.field public static final block_list_dialog_content:I = 0x7f120536

.field public static final block_list_dialog_title:I = 0x7f120537

.field public static final block_list_no_device:I = 0x7f120538

.field public static final block_list_remove_dialog_content:I = 0x7f120539

.field public static final block_list_remove_dialog_title:I = 0x7f12053a

.field public static final block_list_summary:I = 0x7f12053b

.field public static final block_list_title:I = 0x7f12053c

.field public static final block_title:I = 0x7f12053d

.field public static final blocked_by_restricted_settings_content:I = 0x7f12053e

.field public static final blocked_by_restricted_settings_title:I = 0x7f12053f

.field public static final bluetooth:I = 0x7f120540

.field public static final bluetooth_a2dp_codec_ldac_playback_quality_titles_1:I = 0x7f120541

.field public static final bluetooth_a2dp_codec_ldac_playback_quality_titles_2:I = 0x7f120542

.field public static final bluetooth_a2dp_codec_ldac_playback_quality_titles_3:I = 0x7f120543

.field public static final bluetooth_a2dp_codec_ldac_playback_quality_titles_4:I = 0x7f120544

.field public static final bluetooth_a2dp_profile_summary_connected:I = 0x7f120545

.field public static final bluetooth_a2dp_profile_summary_use_for:I = 0x7f120546

.field public static final bluetooth_active_battery_level:I = 0x7f120547

.field public static final bluetooth_active_battery_level_untethered:I = 0x7f120548

.field public static final bluetooth_active_no_battery_level:I = 0x7f120549

.field public static final bluetooth_advanced_titlebar:I = 0x7f12054a

.field public static final bluetooth_ask_disablement:I = 0x7f12054b

.field public static final bluetooth_ask_disablement_no_name:I = 0x7f12054c

.field public static final bluetooth_ask_discovery:I = 0x7f12054d

.field public static final bluetooth_ask_discovery_no_name:I = 0x7f12054e

.field public static final bluetooth_ask_enablement:I = 0x7f12054f

.field public static final bluetooth_ask_enablement_and_discovery:I = 0x7f120550

.field public static final bluetooth_ask_enablement_and_discovery_detected:I = 0x7f120551

.field public static final bluetooth_ask_enablement_and_discovery_no_name:I = 0x7f120552

.field public static final bluetooth_ask_enablement_and_lasting_discovery:I = 0x7f120553

.field public static final bluetooth_ask_enablement_and_lasting_discovery_detected:I = 0x7f120554

.field public static final bluetooth_ask_enablement_and_lasting_discovery_no_name:I = 0x7f120555

.field public static final bluetooth_ask_enablement_detected:I = 0x7f120556

.field public static final bluetooth_ask_enablement_no_name:I = 0x7f120557

.field public static final bluetooth_ask_lasting_discovery:I = 0x7f120558

.field public static final bluetooth_ask_lasting_discovery_no_name:I = 0x7f120559

.field public static final bluetooth_audio_share_feature_notice_summary:I = 0x7f12055a

.field public static final bluetooth_audio_share_feature_notice_title:I = 0x7f12055b

.field public static final bluetooth_battery_level:I = 0x7f12055c

.field public static final bluetooth_battery_level_untethered:I = 0x7f12055d

.field public static final bluetooth_bc_profile_summary_connected:I = 0x7f12055e

.field public static final bluetooth_bc_profile_summary_use_for:I = 0x7f12055f

.field public static final bluetooth_bcast_rcvr_device_name:I = 0x7f120560

.field public static final bluetooth_blacklist_cancel_remove_device:I = 0x7f120561

.field public static final bluetooth_blacklist_remove_device:I = 0x7f120562

.field public static final bluetooth_ble_audio_callback_colocated_src_unavailable:I = 0x7f120563

.field public static final bluetooth_ble_audio_callback_duplicate_addition:I = 0x7f120564

.field public static final bluetooth_ble_audio_callback_fail:I = 0x7f120565

.field public static final bluetooth_ble_audio_callback_fatal:I = 0x7f120566

.field public static final bluetooth_ble_audio_callback_invalid_group_op:I = 0x7f120567

.field public static final bluetooth_ble_audio_callback_invalid_source_id:I = 0x7f120568

.field public static final bluetooth_ble_audio_callback_invalid_source_select:I = 0x7f120569

.field public static final bluetooth_ble_audio_callback_no_empty_solt:I = 0x7f12056a

.field public static final bluetooth_ble_audio_callback_source_unavailable:I = 0x7f12056b

.field public static final bluetooth_ble_audio_callback_success:I = 0x7f12056c

.field public static final bluetooth_ble_audio_callback_txn_timeout:I = 0x7f12056d

.field public static final bluetooth_ble_audio_source_add_fail:I = 0x7f12056e

.field public static final bluetooth_ble_audio_source_adding:I = 0x7f12056f

.field public static final bluetooth_ble_audio_source_address:I = 0x7f120570

.field public static final bluetooth_ble_audio_source_audio_sync_in_sync:I = 0x7f120571

.field public static final bluetooth_ble_audio_source_audio_sync_invalid:I = 0x7f120572

.field public static final bluetooth_ble_audio_source_audio_sync_not_sync:I = 0x7f120573

.field public static final bluetooth_ble_audio_source_audio_sync_status:I = 0x7f120574

.field public static final bluetooth_ble_audio_source_audio_sync_unknown:I = 0x7f120575

.field public static final bluetooth_ble_audio_source_braodcast_code:I = 0x7f120576

.field public static final bluetooth_ble_audio_source_braodcast_encryption:I = 0x7f120577

.field public static final bluetooth_ble_audio_source_braodcast_encryption_unknown:I = 0x7f120578

.field public static final bluetooth_ble_audio_source_broadcast_code_correct:I = 0x7f120579

.field public static final bluetooth_ble_audio_source_broadcast_code_incorrect:I = 0x7f12057a

.field public static final bluetooth_ble_audio_source_broadcast_code_invalid:I = 0x7f12057b

.field public static final bluetooth_ble_audio_source_broadcast_code_need_add:I = 0x7f12057c

.field public static final bluetooth_ble_audio_source_broadcast_code_not_encrypted:I = 0x7f12057d

.field public static final bluetooth_ble_audio_source_broadcast_code_unkonwn:I = 0x7f12057e

.field public static final bluetooth_ble_audio_source_connect:I = 0x7f12057f

.field public static final bluetooth_ble_audio_source_default_fail:I = 0x7f120580

.field public static final bluetooth_ble_audio_source_default_fail_unknwon:I = 0x7f120581

.field public static final bluetooth_ble_audio_source_duplicate_add_error:I = 0x7f120582

.field public static final bluetooth_ble_audio_source_id:I = 0x7f120583

.field public static final bluetooth_ble_audio_source_invalid_error:I = 0x7f120584

.field public static final bluetooth_ble_audio_source_no_empty_slot_error:I = 0x7f120585

.field public static final bluetooth_ble_audio_source_not_synchronize:I = 0x7f120586

.field public static final bluetooth_ble_audio_source_pa_sync_fail:I = 0x7f120587

.field public static final bluetooth_ble_audio_source_pa_sync_idel:I = 0x7f120588

.field public static final bluetooth_ble_audio_source_pa_sync_in_sync:I = 0x7f120589

.field public static final bluetooth_ble_audio_source_pa_sync_invad:I = 0x7f12058a

.field public static final bluetooth_ble_audio_source_pa_sync_no_past:I = 0x7f12058b

.field public static final bluetooth_ble_audio_source_pa_sync_status:I = 0x7f12058c

.field public static final bluetooth_ble_audio_source_pa_sync_syncinfo_need:I = 0x7f12058d

.field public static final bluetooth_ble_audio_source_pa_sync_unknown:I = 0x7f12058e

.field public static final bluetooth_ble_audio_source_passoword:I = 0x7f12058f

.field public static final bluetooth_ble_audio_source_remove_confirm:I = 0x7f120590

.field public static final bluetooth_ble_audio_source_remove_confirm_content:I = 0x7f120591

.field public static final bluetooth_ble_audio_source_remove_fail:I = 0x7f120592

.field public static final bluetooth_ble_audio_source_select_error:I = 0x7f120593

.field public static final bluetooth_ble_audio_source_select_fail:I = 0x7f120594

.field public static final bluetooth_ble_audio_source_synchronized:I = 0x7f120595

.field public static final bluetooth_ble_audio_source_synchronizing:I = 0x7f120596

.field public static final bluetooth_ble_audio_source_unavailable_error:I = 0x7f120597

.field public static final bluetooth_ble_audio_source_update_fail:I = 0x7f120598

.field public static final bluetooth_ble_audio_source_update_pin_fail:I = 0x7f120599

.field public static final bluetooth_ble_audio_stop_synchronize_confirmed:I = 0x7f12059a

.field public static final bluetooth_ble_audio_stop_synchronize_message:I = 0x7f12059b

.field public static final bluetooth_ble_audio_stop_synchronize_title:I = 0x7f12059c

.field public static final bluetooth_broadcast:I = 0x7f12059d

.field public static final bluetooth_broadcast_audio:I = 0x7f12059e

.field public static final bluetooth_broadcast_audio_sharing:I = 0x7f12059f

.field public static final bluetooth_broadcast_audio_summary:I = 0x7f1205a0

.field public static final bluetooth_broadcast_audio_summary_disable:I = 0x7f1205a1

.field public static final bluetooth_broadcast_audio_switch_summary:I = 0x7f1205a2

.field public static final bluetooth_broadcast_audio_switch_title:I = 0x7f1205a3

.field public static final bluetooth_broadcast_dialog_broadcast_app:I = 0x7f1205a4

.field public static final bluetooth_broadcast_dialog_broadcast_message:I = 0x7f1205a5

.field public static final bluetooth_broadcast_dialog_find_message:I = 0x7f1205a6

.field public static final bluetooth_broadcast_dialog_title:I = 0x7f1205a7

.field public static final bluetooth_broadcast_pin_configure:I = 0x7f1205a8

.field public static final bluetooth_broadcast_pin_configure_dialog:I = 0x7f1205a9

.field public static final bluetooth_broadcasting:I = 0x7f1205aa

.field public static final bluetooth_col_grp_source_selection_options_detail:I = 0x7f1205ab

.field public static final bluetooth_col_source_selection_options_detail:I = 0x7f1205ac

.field public static final bluetooth_companion_app_body:I = 0x7f1205ad

.field public static final bluetooth_companion_app_remove_association_confirm_button:I = 0x7f1205ae

.field public static final bluetooth_companion_app_remove_association_dialog_title:I = 0x7f1205af

.field public static final bluetooth_confirm_passkey_msg:I = 0x7f1205b0

.field public static final bluetooth_connect_access_dialog_content:I = 0x7f1205b1

.field public static final bluetooth_connect_access_dialog_negative:I = 0x7f1205b2

.field public static final bluetooth_connect_access_dialog_positive:I = 0x7f1205b3

.field public static final bluetooth_connect_access_dialog_title:I = 0x7f1205b4

.field public static final bluetooth_connect_access_notification_content:I = 0x7f1205b5

.field public static final bluetooth_connect_access_notification_title:I = 0x7f1205b6

.field public static final bluetooth_connect_failed:I = 0x7f1205b7

.field public static final bluetooth_connect_settings:I = 0x7f1205b8

.field public static final bluetooth_connect_specific_profiles_title:I = 0x7f1205b9

.field public static final bluetooth_connected:I = 0x7f1205ba

.field public static final bluetooth_connected_battery_level:I = 0x7f1205bb

.field public static final bluetooth_connected_multiple_devices_summary:I = 0x7f1205bc

.field public static final bluetooth_connected_no_a2dp:I = 0x7f1205bd

.field public static final bluetooth_connected_no_a2dp_battery_level:I = 0x7f1205be

.field public static final bluetooth_connected_no_headset:I = 0x7f1205bf

.field public static final bluetooth_connected_no_headset_battery_level:I = 0x7f1205c0

.field public static final bluetooth_connected_no_headset_no_a2dp:I = 0x7f1205c1

.field public static final bluetooth_connected_no_headset_no_a2dp_battery_level:I = 0x7f1205c2

.field public static final bluetooth_connected_no_map:I = 0x7f1205c3

.field public static final bluetooth_connected_summary:I = 0x7f1205c4

.field public static final bluetooth_connecting:I = 0x7f1205c5

.field public static final bluetooth_connecting_error_message:I = 0x7f1205c6

.field public static final bluetooth_connection_dialog_text:I = 0x7f1205c7

.field public static final bluetooth_connection_notif_message:I = 0x7f1205c8

.field public static final bluetooth_connection_permission_request:I = 0x7f1205c9

.field public static final bluetooth_details_head_tracking_summary:I = 0x7f1205ca

.field public static final bluetooth_details_head_tracking_title:I = 0x7f1205cb

.field public static final bluetooth_details_spatial_audio_summary:I = 0x7f1205cc

.field public static final bluetooth_details_spatial_audio_title:I = 0x7f1205cd

.field public static final bluetooth_device:I = 0x7f1205ce

.field public static final bluetooth_device_address:I = 0x7f1205cf

.field public static final bluetooth_device_advanced_enable_opp_title:I = 0x7f1205d0

.field public static final bluetooth_device_advanced_online_mode_summary:I = 0x7f1205d1

.field public static final bluetooth_device_advanced_online_mode_title:I = 0x7f1205d2

.field public static final bluetooth_device_advanced_profile_header_title:I = 0x7f1205d3

.field public static final bluetooth_device_advanced_rename_device:I = 0x7f1205d4

.field public static final bluetooth_device_advanced_title:I = 0x7f1205d5

.field public static final bluetooth_device_blacklist_tital:I = 0x7f1205d6

.field public static final bluetooth_device_context_connect:I = 0x7f1205d7

.field public static final bluetooth_device_context_connect_advanced:I = 0x7f1205d8

.field public static final bluetooth_device_context_disconnect:I = 0x7f1205d9

.field public static final bluetooth_device_context_disconnect_unpair:I = 0x7f1205da

.field public static final bluetooth_device_context_pair_connect:I = 0x7f1205db

.field public static final bluetooth_device_context_unpair:I = 0x7f1205dc

.field public static final bluetooth_device_details:I = 0x7f1205dd

.field public static final bluetooth_device_mac_address:I = 0x7f1205de

.field public static final bluetooth_device_misc_title:I = 0x7f1205df

.field public static final bluetooth_device_name:I = 0x7f1205e0

.field public static final bluetooth_device_name_summary:I = 0x7f1205e1

.field public static final bluetooth_device_nearby_title:I = 0x7f1205e2

.field public static final bluetooth_device_searchable:I = 0x7f1205e3

.field public static final bluetooth_devices:I = 0x7f1205e4

.field public static final bluetooth_devices_card_off_summary:I = 0x7f1205e5

.field public static final bluetooth_devices_card_off_title:I = 0x7f1205e6

.field public static final bluetooth_disable_a2dp_hw_offload:I = 0x7f1205e7

.field public static final bluetooth_disable_absolute_volume:I = 0x7f1205e8

.field public static final bluetooth_disable_absolute_volume_summary:I = 0x7f1205e9

.field public static final bluetooth_disable_hw_offload_dialog_cancel:I = 0x7f1205ea

.field public static final bluetooth_disable_hw_offload_dialog_confirm:I = 0x7f1205eb

.field public static final bluetooth_disable_hw_offload_dialog_message:I = 0x7f1205ec

.field public static final bluetooth_disable_hw_offload_dialog_title:I = 0x7f1205ed

.field public static final bluetooth_disable_le_audio_hw_offload:I = 0x7f1205ee

.field public static final bluetooth_disable_profile_message:I = 0x7f1205ef

.field public static final bluetooth_disable_profile_title:I = 0x7f1205f0

.field public static final bluetooth_disabled:I = 0x7f1205f1

.field public static final bluetooth_disconnect_a2dp_profile:I = 0x7f1205f2

.field public static final bluetooth_disconnect_all_profiles:I = 0x7f1205f3

.field public static final bluetooth_disconnect_dialog_ok:I = 0x7f1205f4

.field public static final bluetooth_disconnect_headset_profile:I = 0x7f1205f5

.field public static final bluetooth_disconnect_hid_profile:I = 0x7f1205f6

.field public static final bluetooth_disconnect_pan_nap_profile:I = 0x7f1205f7

.field public static final bluetooth_disconnect_pan_user_profile:I = 0x7f1205f8

.field public static final bluetooth_disconnect_title:I = 0x7f1205f9

.field public static final bluetooth_disconnected:I = 0x7f1205fa

.field public static final bluetooth_disconnecting:I = 0x7f1205fb

.field public static final bluetooth_display_passkey_pin_msg:I = 0x7f1205fc

.field public static final bluetooth_display_settings:I = 0x7f1205fd

.field public static final bluetooth_dock_settings:I = 0x7f1205fe

.field public static final bluetooth_dock_settings_a2dp:I = 0x7f1205ff

.field public static final bluetooth_dock_settings_headset:I = 0x7f120600

.field public static final bluetooth_dock_settings_remember:I = 0x7f120601

.field public static final bluetooth_dock_settings_title:I = 0x7f120602

.field public static final bluetooth_dun_profile_summary_connected:I = 0x7f120603

.field public static final bluetooth_dun_profile_summary_use_for:I = 0x7f120604

.field public static final bluetooth_empty_list_bluetooth_off:I = 0x7f120605

.field public static final bluetooth_empty_list_user_restricted:I = 0x7f120606

.field public static final bluetooth_enable:I = 0x7f120607

.field public static final bluetooth_enable_alphanumeric_pin:I = 0x7f120608

.field public static final bluetooth_enable_gabeldorsche:I = 0x7f120609

.field public static final bluetooth_enable_gabeldorsche_summary:I = 0x7f12060a

.field public static final bluetooth_enable_opp_high_speed:I = 0x7f12060b

.field public static final bluetooth_enable_opp_high_speed_dialog_message:I = 0x7f12060c

.field public static final bluetooth_enable_opp_high_speed_dialog_negative:I = 0x7f12060d

.field public static final bluetooth_enable_opp_high_speed_dialog_positive:I = 0x7f12060e

.field public static final bluetooth_enable_opp_high_speed_dialog_title:I = 0x7f12060f

.field public static final bluetooth_enable_opp_high_speed_summary:I = 0x7f120610

.field public static final bluetooth_enable_page_scan:I = 0x7f120611

.field public static final bluetooth_enable_page_scan_summary:I = 0x7f120612

.field public static final bluetooth_enable_pts_test:I = 0x7f120613

.field public static final bluetooth_enable_pts_test_summary:I = 0x7f120614

.field public static final bluetooth_enter_passkey_msg:I = 0x7f120615

.field public static final bluetooth_enter_passkey_other_device:I = 0x7f120616

.field public static final bluetooth_enter_pin_other_device:I = 0x7f120617

.field public static final bluetooth_error_title:I = 0x7f120618

.field public static final bluetooth_fastConnect_key_device_id:I = 0x7f120619

.field public static final bluetooth_fastConnect_resource_load:I = 0x7f12061a

.field public static final bluetooth_find_broadcast:I = 0x7f12061b

.field public static final bluetooth_find_broadcast_button_leave:I = 0x7f12061c

.field public static final bluetooth_find_broadcast_button_scan:I = 0x7f12061d

.field public static final bluetooth_find_broadcast_summary:I = 0x7f12061e

.field public static final bluetooth_find_broadcast_title:I = 0x7f12061f

.field public static final bluetooth_footer_mac_message:I = 0x7f120620

.field public static final bluetooth_grp_source_selection_options_detail:I = 0x7f120621

.field public static final bluetooth_hd_music_play_summary:I = 0x7f120622

.field public static final bluetooth_hd_music_play_title:I = 0x7f120623

.field public static final bluetooth_headset_profile_summary_connected:I = 0x7f120624

.field public static final bluetooth_headset_profile_summary_use_for:I = 0x7f120625

.field public static final bluetooth_hearing_aid_left_active:I = 0x7f120626

.field public static final bluetooth_hearing_aid_left_and_right_active:I = 0x7f120627

.field public static final bluetooth_hearing_aid_profile_summary_connected:I = 0x7f120628

.field public static final bluetooth_hearing_aid_profile_summary_use_for:I = 0x7f120629

.field public static final bluetooth_hearing_aid_right_active:I = 0x7f12062a

.field public static final bluetooth_hearingaid_left_battery_level:I = 0x7f12062b

.field public static final bluetooth_hearingaid_left_pairing_message:I = 0x7f12062c

.field public static final bluetooth_hearingaid_right_battery_level:I = 0x7f12062d

.field public static final bluetooth_hearingaid_right_pairing_message:I = 0x7f12062e

.field public static final bluetooth_hid_profile_summary_connected:I = 0x7f12062f

.field public static final bluetooth_hid_profile_summary_use_for:I = 0x7f120630

.field public static final bluetooth_incoming_pairing_msg:I = 0x7f120631

.field public static final bluetooth_inform_messgae:I = 0x7f120632

.field public static final bluetooth_inform_messgae_title:I = 0x7f120633

.field public static final bluetooth_is_disconnect_question:I = 0x7f120634

.field public static final bluetooth_is_discoverable:I = 0x7f120635

.field public static final bluetooth_is_discoverable_always:I = 0x7f120636

.field public static final bluetooth_is_visible_message:I = 0x7f120637

.field public static final bluetooth_le_audio_profile_summary_connected:I = 0x7f120638

.field public static final bluetooth_le_audio_profile_summary_use_for:I = 0x7f120639

.field public static final bluetooth_left_name:I = 0x7f12063a

.field public static final bluetooth_lock_voice_dialing:I = 0x7f12063b

.field public static final bluetooth_lock_voice_dialing_summary:I = 0x7f12063c

.field public static final bluetooth_main_switch_title:I = 0x7f12063d

.field public static final bluetooth_map_acceptance_dialog_text:I = 0x7f12063e

.field public static final bluetooth_map_profile_summary_connected:I = 0x7f12063f

.field public static final bluetooth_map_profile_summary_use_for:I = 0x7f120640

.field public static final bluetooth_map_request:I = 0x7f120641

.field public static final bluetooth_max_connected_audio_devices_dialog_title:I = 0x7f120642

.field public static final bluetooth_max_connected_audio_devices_string:I = 0x7f120643

.field public static final bluetooth_menu_advanced:I = 0x7f120644

.field public static final bluetooth_message_access_dialog_content:I = 0x7f120645

.field public static final bluetooth_message_access_dialog_title:I = 0x7f120646

.field public static final bluetooth_message_access_notification_content:I = 0x7f120647

.field public static final bluetooth_message_move_out_of_blacklist:I = 0x7f120648

.field public static final bluetooth_mi_fast_connect:I = 0x7f120649

.field public static final bluetooth_mi_fast_connect_summary:I = 0x7f12064a

.field public static final bluetooth_middle_name:I = 0x7f12064b

.field public static final bluetooth_move_out_of_blacklist:I = 0x7f12064c

.field public static final bluetooth_multuple_devices_mac_address:I = 0x7f12064d

.field public static final bluetooth_name_not_set:I = 0x7f12064e

.field public static final bluetooth_no_devices_found:I = 0x7f12064f

.field public static final bluetooth_not_visible_to_other_devices:I = 0x7f120650

.field public static final bluetooth_notif_message:I = 0x7f120651

.field public static final bluetooth_notif_ticker:I = 0x7f120652

.field public static final bluetooth_notif_title:I = 0x7f120653

.field public static final bluetooth_off_footer:I = 0x7f120654

.field public static final bluetooth_on_time:I = 0x7f120655

.field public static final bluetooth_only_visible_to_paired_devices:I = 0x7f120656

.field public static final bluetooth_opp_profile_summary_connected:I = 0x7f120657

.field public static final bluetooth_opp_profile_summary_not_connected:I = 0x7f120658

.field public static final bluetooth_opp_profile_summary_use_for:I = 0x7f120659

.field public static final bluetooth_other_settings:I = 0x7f12065a

.field public static final bluetooth_pair_other_ear_dialog_left_ear_message:I = 0x7f12065b

.field public static final bluetooth_pair_other_ear_dialog_left_ear_positive_button:I = 0x7f12065c

.field public static final bluetooth_pair_other_ear_dialog_right_ear_message:I = 0x7f12065d

.field public static final bluetooth_pair_other_ear_dialog_right_ear_positive_button:I = 0x7f12065e

.field public static final bluetooth_pair_other_ear_dialog_title:I = 0x7f12065f

.field public static final bluetooth_paired:I = 0x7f120660

.field public static final bluetooth_paired_device_title:I = 0x7f120661

.field public static final bluetooth_pairing:I = 0x7f120662

.field public static final bluetooth_pairing_accept:I = 0x7f120663

.field public static final bluetooth_pairing_accept_all_caps:I = 0x7f120664

.field public static final bluetooth_pairing_add_to_blackfile:I = 0x7f120665

.field public static final bluetooth_pairing_decline:I = 0x7f120666

.field public static final bluetooth_pairing_device_down_error_message:I = 0x7f120667

.field public static final bluetooth_pairing_dialog_contants_request:I = 0x7f120668

.field public static final bluetooth_pairing_dialog_paring_request:I = 0x7f120669

.field public static final bluetooth_pairing_dialog_sharing_phonebook_title:I = 0x7f12066a

.field public static final bluetooth_pairing_dialog_title:I = 0x7f12066b

.field public static final bluetooth_pairing_error_message:I = 0x7f12066c

.field public static final bluetooth_pairing_key_msg:I = 0x7f12066d

.field public static final bluetooth_pairing_page_title:I = 0x7f12066e

.field public static final bluetooth_pairing_pin_error_message:I = 0x7f12066f

.field public static final bluetooth_pairing_pref_title:I = 0x7f120670

.field public static final bluetooth_pairing_rejected_error_message:I = 0x7f120671

.field public static final bluetooth_pairing_request:I = 0x7f120672

.field public static final bluetooth_pairing_shares_phonebook:I = 0x7f120673

.field public static final bluetooth_pairing_shares_phonebook_not_support_voice:I = 0x7f120674

.field public static final bluetooth_pairing_toast_add_to_blackfile:I = 0x7f120675

.field public static final bluetooth_pairing_will_share_phonebook:I = 0x7f120676

.field public static final bluetooth_pan_nap_profile_summary_connected:I = 0x7f120677

.field public static final bluetooth_pan_profile_summary_use_for:I = 0x7f120678

.field public static final bluetooth_pan_user_profile_summary_connected:I = 0x7f120679

.field public static final bluetooth_paring_group_msg:I = 0x7f12067a

.field public static final bluetooth_pb_acceptance_dialog_text:I = 0x7f12067b

.field public static final bluetooth_pb_remember_choice:I = 0x7f12067c

.field public static final bluetooth_phonebook_access_dialog_content:I = 0x7f12067d

.field public static final bluetooth_phonebook_access_dialog_title:I = 0x7f12067e

.field public static final bluetooth_phonebook_access_notification_content:I = 0x7f12067f

.field public static final bluetooth_phonebook_request:I = 0x7f120680

.field public static final bluetooth_pin_values_hint:I = 0x7f120681

.field public static final bluetooth_pin_values_hint_16_digits:I = 0x7f120682

.field public static final bluetooth_plugins_check_summary:I = 0x7f120683

.field public static final bluetooth_plugins_check_summary_new:I = 0x7f120684

.field public static final bluetooth_plugins_check_title:I = 0x7f120685

.field public static final bluetooth_pref_summary:I = 0x7f120686

.field public static final bluetooth_preference_connected_devices:I = 0x7f120687

.field public static final bluetooth_preference_device_settings:I = 0x7f120688

.field public static final bluetooth_preference_found_devices:I = 0x7f120689

.field public static final bluetooth_preference_found_media_devices:I = 0x7f12068a

.field public static final bluetooth_preference_no_found_devices:I = 0x7f12068b

.field public static final bluetooth_preference_paired_devices:I = 0x7f12068c

.field public static final bluetooth_preference_paired_dialog_contacts_option:I = 0x7f12068d

.field public static final bluetooth_preference_paired_dialog_internet_option:I = 0x7f12068e

.field public static final bluetooth_preference_paired_dialog_keyboard_option:I = 0x7f12068f

.field public static final bluetooth_preference_paired_dialog_name_label:I = 0x7f120690

.field public static final bluetooth_preference_paired_dialog_title:I = 0x7f120691

.field public static final bluetooth_preference_scan_title:I = 0x7f120692

.field public static final bluetooth_profile_a2dp:I = 0x7f120693

.field public static final bluetooth_profile_a2dp_high_quality:I = 0x7f120694

.field public static final bluetooth_profile_a2dp_high_quality_unknown_codec:I = 0x7f120695

.field public static final bluetooth_profile_bc:I = 0x7f120696

.field public static final bluetooth_profile_broadcast:I = 0x7f120697

.field public static final bluetooth_profile_details:I = 0x7f120698

.field public static final bluetooth_profile_dun:I = 0x7f120699

.field public static final bluetooth_profile_headset:I = 0x7f12069a

.field public static final bluetooth_profile_hearing_aid:I = 0x7f12069b

.field public static final bluetooth_profile_hid:I = 0x7f12069c

.field public static final bluetooth_profile_le_audio:I = 0x7f12069d

.field public static final bluetooth_profile_map:I = 0x7f12069e

.field public static final bluetooth_profile_opp:I = 0x7f12069f

.field public static final bluetooth_profile_pan:I = 0x7f1206a0

.field public static final bluetooth_profile_pan_nap:I = 0x7f1206a1

.field public static final bluetooth_profile_pbap:I = 0x7f1206a2

.field public static final bluetooth_profile_pbap_summary:I = 0x7f1206a3

.field public static final bluetooth_profile_sap:I = 0x7f1206a4

.field public static final bluetooth_profile_vcp:I = 0x7f1206a5

.field public static final bluetooth_profile_volume_control:I = 0x7f1206a6

.field public static final bluetooth_quick_toggle_summary:I = 0x7f1206a7

.field public static final bluetooth_quick_toggle_title:I = 0x7f1206a8

.field public static final bluetooth_remember_choice:I = 0x7f1206a9

.field public static final bluetooth_rename_button:I = 0x7f1206aa

.field public static final bluetooth_rename_device:I = 0x7f1206ab

.field public static final bluetooth_right_name:I = 0x7f1206ac

.field public static final bluetooth_sap_acceptance_dialog_text:I = 0x7f1206ad

.field public static final bluetooth_sap_profile_summary_connected:I = 0x7f1206ae

.field public static final bluetooth_sap_profile_summary_use_for:I = 0x7f1206af

.field public static final bluetooth_sap_request:I = 0x7f1206b0

.field public static final bluetooth_scan_for_devices:I = 0x7f1206b1

.field public static final bluetooth_scan_source_title:I = 0x7f1206b2

.field public static final bluetooth_scanning_on_info_message:I = 0x7f1206b3

.field public static final bluetooth_screen_related:I = 0x7f1206b4

.field public static final bluetooth_search_bcast_sources_pref_title:I = 0x7f1206b5

.field public static final bluetooth_search_broadcasters:I = 0x7f1206b6

.field public static final bluetooth_search_for_devices:I = 0x7f1206b7

.field public static final bluetooth_searching_for_devices:I = 0x7f1206b8

.field public static final bluetooth_select_a2dp_codec_aptxadaptive_mode:I = 0x7f1206b9

.field public static final bluetooth_select_a2dp_codec_aptxadaptive_mode_dialog_title:I = 0x7f1206ba

.field public static final bluetooth_select_a2dp_codec_bits_per_sample:I = 0x7f1206bb

.field public static final bluetooth_select_a2dp_codec_bits_per_sample_dialog_title:I = 0x7f1206bc

.field public static final bluetooth_select_a2dp_codec_channel_mode:I = 0x7f1206bd

.field public static final bluetooth_select_a2dp_codec_channel_mode_dialog_title:I = 0x7f1206be

.field public static final bluetooth_select_a2dp_codec_ldac_playback_quality:I = 0x7f1206bf

.field public static final bluetooth_select_a2dp_codec_ldac_playback_quality_dialog_title:I = 0x7f1206c0

.field public static final bluetooth_select_a2dp_codec_lhdc_latency:I = 0x7f1206c1

.field public static final bluetooth_select_a2dp_codec_lhdc_latency_dialog_title:I = 0x7f1206c2

.field public static final bluetooth_select_a2dp_codec_lhdc_playback_quality:I = 0x7f1206c3

.field public static final bluetooth_select_a2dp_codec_lhdc_playback_quality_dialog_title:I = 0x7f1206c4

.field public static final bluetooth_select_a2dp_codec_sample_rate:I = 0x7f1206c5

.field public static final bluetooth_select_a2dp_codec_sample_rate_dialog_title:I = 0x7f1206c6

.field public static final bluetooth_select_a2dp_codec_streaming_label:I = 0x7f1206c7

.field public static final bluetooth_select_a2dp_codec_type:I = 0x7f1206c8

.field public static final bluetooth_select_a2dp_codec_type_dialog_title:I = 0x7f1206c9

.field public static final bluetooth_select_a2dp_codec_type_help_info:I = 0x7f1206ca

.field public static final bluetooth_select_avrcp_version_dialog_title:I = 0x7f1206cb

.field public static final bluetooth_select_avrcp_version_string:I = 0x7f1206cc

.field public static final bluetooth_select_map_version_dialog_title:I = 0x7f1206cd

.field public static final bluetooth_select_map_version_string:I = 0x7f1206ce

.field public static final bluetooth_setting_off:I = 0x7f1206cf

.field public static final bluetooth_setting_on:I = 0x7f1206d0

.field public static final bluetooth_settings:I = 0x7f1206d1

.field public static final bluetooth_settings_summary:I = 0x7f1206d2

.field public static final bluetooth_settings_title:I = 0x7f1206d3

.field public static final bluetooth_share_audio:I = 0x7f1206d4

.field public static final bluetooth_share_broadcast:I = 0x7f1206d5

.field public static final bluetooth_share_broadcast_available_connect:I = 0x7f1206d6

.field public static final bluetooth_share_broadcast_delete:I = 0x7f1206d7

.field public static final bluetooth_share_broadcast_jion:I = 0x7f1206d8

.field public static final bluetooth_share_broadcast_listen:I = 0x7f1206d9

.field public static final bluetooth_share_broadcast_listen_summary:I = 0x7f1206da

.field public static final bluetooth_share_broadcast_nearby_available:I = 0x7f1206db

.field public static final bluetooth_share_broadcast_param:I = 0x7f1206dc

.field public static final bluetooth_share_broadcast_password_hint:I = 0x7f1206dd

.field public static final bluetooth_share_broadcast_summary:I = 0x7f1206de

.field public static final bluetooth_share_broadcast_update:I = 0x7f1206df

.field public static final bluetooth_show_devices_without_names:I = 0x7f1206e0

.field public static final bluetooth_show_devices_without_names_summary:I = 0x7f1206e1

.field public static final bluetooth_show_received_files:I = 0x7f1206e2

.field public static final bluetooth_sim_card_access_dialog_content:I = 0x7f1206e3

.field public static final bluetooth_sim_card_access_dialog_title:I = 0x7f1206e4

.field public static final bluetooth_sim_card_access_notification_content:I = 0x7f1206e5

.field public static final bluetooth_sim_card_access_notification_title:I = 0x7f1206e6

.field public static final bluetooth_source_added_message:I = 0x7f1206e7

.field public static final bluetooth_source_addition_error_message:I = 0x7f1206e8

.field public static final bluetooth_source_dup_addition_error_message:I = 0x7f1206e9

.field public static final bluetooth_source_no_empty_slot_error_message:I = 0x7f1206ea

.field public static final bluetooth_source_removal_error_message:I = 0x7f1206eb

.field public static final bluetooth_source_remove_invalid_group_op:I = 0x7f1206ec

.field public static final bluetooth_source_remove_invalid_src_id:I = 0x7f1206ed

.field public static final bluetooth_source_selection_error_message:I = 0x7f1206ee

.field public static final bluetooth_source_selection_error_src_unavail_message:I = 0x7f1206ef

.field public static final bluetooth_source_selection_options_detail:I = 0x7f1206f0

.field public static final bluetooth_source_selection_options_detail_title:I = 0x7f1206f1

.field public static final bluetooth_source_setpin_error_message:I = 0x7f1206f2

.field public static final bluetooth_source_update_error_message:I = 0x7f1206f3

.field public static final bluetooth_source_update_invalid_group_op:I = 0x7f1206f4

.field public static final bluetooth_source_update_invalid_src_id:I = 0x7f1206f5

.field public static final bluetooth_talkback_bluetooth:I = 0x7f1206f6

.field public static final bluetooth_talkback_computer:I = 0x7f1206f7

.field public static final bluetooth_talkback_group:I = 0x7f1206f8

.field public static final bluetooth_talkback_headphone:I = 0x7f1206f9

.field public static final bluetooth_talkback_headset:I = 0x7f1206fa

.field public static final bluetooth_talkback_imaging:I = 0x7f1206fb

.field public static final bluetooth_talkback_input_peripheral:I = 0x7f1206fc

.field public static final bluetooth_talkback_phone:I = 0x7f1206fd

.field public static final bluetooth_tether_checkbox_text:I = 0x7f1206fe

.field public static final bluetooth_tethering_available_subtext:I = 0x7f1206ff

.field public static final bluetooth_tethering_device_connected_subtext:I = 0x7f120700

.field public static final bluetooth_tethering_devices_connected_subtext:I = 0x7f120701

.field public static final bluetooth_tethering_errored_subtext:I = 0x7f120702

.field public static final bluetooth_tethering_off_subtext:I = 0x7f120703

.field public static final bluetooth_tethering_off_subtext_config:I = 0x7f120704

.field public static final bluetooth_tethering_overflow_error:I = 0x7f120705

.field public static final bluetooth_tethering_subtext:I = 0x7f120706

.field public static final bluetooth_turning_off:I = 0x7f120707

.field public static final bluetooth_turning_on:I = 0x7f120708

.field public static final bluetooth_unlock_add_device:I = 0x7f120709

.field public static final bluetooth_unlock_change_device_confirm_msg:I = 0x7f12070a

.field public static final bluetooth_unlock_change_matched_device:I = 0x7f12070b

.field public static final bluetooth_unlock_confirm_device_text:I = 0x7f12070c

.field public static final bluetooth_unlock_confirm_device_text_for_huami_watch:I = 0x7f12070d

.field public static final bluetooth_unlock_confirm_device_text_for_mi_wear:I = 0x7f12070e

.field public static final bluetooth_unlock_confirm_device_text_for_miband2:I = 0x7f12070f

.field public static final bluetooth_unlock_delete_device:I = 0x7f120710

.field public static final bluetooth_unlock_delete_device_confirm_msg:I = 0x7f120711

.field public static final bluetooth_unlock_device_matched_text:I = 0x7f120712

.field public static final bluetooth_unlock_no_matched_device_yet:I = 0x7f120713

.field public static final bluetooth_unlock_reject:I = 0x7f120714

.field public static final bluetooth_unlock_settings_devices_title:I = 0x7f120715

.field public static final bluetooth_unlock_settings_toggle_title:I = 0x7f120716

.field public static final bluetooth_unlock_settings_unlock_device:I = 0x7f120717

.field public static final bluetooth_unlock_settings_unlock_summary:I = 0x7f120718

.field public static final bluetooth_unlock_state_disconnected:I = 0x7f120719

.field public static final bluetooth_unlock_state_key_error:I = 0x7f12071a

.field public static final bluetooth_unlock_state_label:I = 0x7f12071b

.field public static final bluetooth_unlock_state_ok:I = 0x7f12071c

.field public static final bluetooth_unlock_state_too_far:I = 0x7f12071d

.field public static final bluetooth_unlock_title:I = 0x7f12071e

.field public static final bluetooth_unlock_turned_off:I = 0x7f12071f

.field public static final bluetooth_unlock_turned_on:I = 0x7f120720

.field public static final bluetooth_unlock_unsupport:I = 0x7f120721

.field public static final bluetooth_unpair_dialog_body:I = 0x7f120722

.field public static final bluetooth_unpair_dialog_forget_confirm_button:I = 0x7f120723

.field public static final bluetooth_unpair_dialog_title:I = 0x7f120724

.field public static final bluetooth_untether_blank:I = 0x7f120725

.field public static final bluetooth_version:I = 0x7f120726

.field public static final bluetooth_version_message:I = 0x7f120727

.field public static final bluetooth_visibility_timeout:I = 0x7f120728

.field public static final bluetooth_wake:I = 0x7f120729

.field public static final boot_sounds_title:I = 0x7f12072a

.field public static final bootloader:I = 0x7f12072b

.field public static final bootloader_apply_step_1:I = 0x7f12072c

.field public static final bootloader_apply_step_2:I = 0x7f12072d

.field public static final bootloader_apply_step_3:I = 0x7f12072e

.field public static final bootloader_apply_step_4:I = 0x7f12072f

.field public static final bootloader_apply_step_5:I = 0x7f120730

.field public static final bootloader_apply_title:I = 0x7f120731

.field public static final bootloader_device_bind_already:I = 0x7f120732

.field public static final bootloader_device_bind_exceed_sim_limit:I = 0x7f120733

.field public static final bootloader_device_bind_fail:I = 0x7f120734

.field public static final bootloader_device_bind_msg:I = 0x7f120735

.field public static final bootloader_device_bind_no_account:I = 0x7f120736

.field public static final bootloader_device_bind_no_data_network:I = 0x7f120737

.field public static final bootloader_device_bind_no_network:I = 0x7f120738

.field public static final bootloader_device_bind_no_sim:I = 0x7f120739

.field public static final bootloader_device_bind_server_code:I = 0x7f12073a

.field public static final bootloader_device_bind_server_token_expired_1:I = 0x7f12073b

.field public static final bootloader_device_bind_server_token_expired_2:I = 0x7f12073c

.field public static final bootloader_device_bind_sim_invalid:I = 0x7f12073d

.field public static final bootloader_locked_answer_1:I = 0x7f12073e

.field public static final bootloader_locked_answer_1_warning:I = 0x7f12073f

.field public static final bootloader_locked_answer_2:I = 0x7f120740

.field public static final bootloader_locked_answer_2_no_sim:I = 0x7f120741

.field public static final bootloader_locked_check_status:I = 0x7f120742

.field public static final bootloader_locked_question_1:I = 0x7f120743

.field public static final bootloader_locked_question_2:I = 0x7f120744

.field public static final bootloader_status:I = 0x7f120745

.field public static final bootloader_status_locked:I = 0x7f120746

.field public static final bootloader_status_locked_info:I = 0x7f120747

.field public static final bootloader_status_locked_summary:I = 0x7f120748

.field public static final bootloader_status_privacy_dialog_message:I = 0x7f120749

.field public static final bootloader_status_privacy_dialog_no:I = 0x7f12074a

.field public static final bootloader_status_privacy_dialog_title:I = 0x7f12074b

.field public static final bootloader_status_privacy_dialog_url_text:I = 0x7f12074c

.field public static final bootloader_status_privacy_dialog_yes:I = 0x7f12074d

.field public static final bootloader_status_summary:I = 0x7f12074e

.field public static final bootloader_status_unlocked:I = 0x7f12074f

.field public static final bootloader_status_unlocked_info:I = 0x7f120750

.field public static final bootloader_summary:I = 0x7f120751

.field public static final bootloader_tips:I = 0x7f120752

.field public static final bootloader_tips_ok:I = 0x7f120753

.field public static final bootloader_tips_warning:I = 0x7f120754

.field public static final bootloader_unlock_webhome:I = 0x7f120755

.field public static final bottom_function_settings:I = 0x7f120756

.field public static final bottom_sheet_behavior:I = 0x7f120757

.field public static final bottomsheet_action_expand_halfway:I = 0x7f120758

.field public static final breathing_light_color_menu_title:I = 0x7f120759

.field public static final breathing_light_color_summary:I = 0x7f12075a

.field public static final breathing_light_freq_summary:I = 0x7f12075b

.field public static final breathing_light_freq_title:I = 0x7f12075c

.field public static final brightness:I = 0x7f12075d

.field public static final brightness_auto_summary:I = 0x7f12075e

.field public static final brightness_summary:I = 0x7f12075f

.field public static final brightness_title:I = 0x7f120760

.field public static final bssid_label:I = 0x7f120761

.field public static final bt_aac_dialog_summary:I = 0x7f120762

.field public static final bt_aac_dialog_title:I = 0x7f120763

.field public static final bt_aac_open_dialog_summary:I = 0x7f120764

.field public static final bt_aac_open_dialog_title:I = 0x7f120765

.field public static final bt_aac_pre_title:I = 0x7f120766

.field public static final bt_absVolume_open_dialog_summary:I = 0x7f120767

.field public static final bt_absVolume_open_dialog_title:I = 0x7f120768

.field public static final bt_absVolume_pre_title:I = 0x7f120769

.field public static final bt_absVolume_summary:I = 0x7f12076a

.field public static final bt_always_show_icon:I = 0x7f12076b

.field public static final bt_aptx_adaptive_open_dialog_summary:I = 0x7f12076c

.field public static final bt_aptx_adaptive_open_dialog_title:I = 0x7f12076d

.field public static final bt_audio_repair_property:I = 0x7f12076e

.field public static final bt_audio_share_property:I = 0x7f12076f

.field public static final bt_audio_share_switch_summary:I = 0x7f120770

.field public static final bt_audio_share_switch_title:I = 0x7f120771

.field public static final bt_audiorepair_dialog_open_success:I = 0x7f120772

.field public static final bt_audiorepair_dialog_openok:I = 0x7f120773

.field public static final bt_audiorepair_open_dialog_summary:I = 0x7f120774

.field public static final bt_audiorepair_open_dialog_title:I = 0x7f120775

.field public static final bt_audiorepair_pre_title:I = 0x7f120776

.field public static final bt_audiorepair_summary:I = 0x7f120777

.field public static final bt_audiorepair_working:I = 0x7f120778

.field public static final bt_dialog_remember_choice:I = 0x7f120779

.field public static final bt_dialog_settings:I = 0x7f12077a

.field public static final bt_hci_snoop_log:I = 0x7f12077b

.field public static final bt_hci_snoop_log_summary:I = 0x7f12077c

.field public static final bt_lc3plus_open_dialog_summary:I = 0x7f12077d

.field public static final bt_lc3plus_open_dialog_title:I = 0x7f12077e

.field public static final bt_ldac_declaration:I = 0x7f12077f

.field public static final bt_ldac_dialog_summary:I = 0x7f120780

.field public static final bt_ldac_dialog_title:I = 0x7f120781

.field public static final bt_ldac_open_dialog_summary:I = 0x7f120782

.field public static final bt_ldac_open_dialog_title:I = 0x7f120783

.field public static final bt_ldac_pre_title:I = 0x7f120784

.field public static final bt_ldac_property:I = 0x7f120785

.field public static final bt_le_audio_broadcast_dialog_different_output:I = 0x7f120786

.field public static final bt_le_audio_broadcast_dialog_sub_title:I = 0x7f120787

.field public static final bt_le_audio_broadcast_dialog_switch_app:I = 0x7f120788

.field public static final bt_le_audio_broadcast_dialog_title:I = 0x7f120789

.field public static final bt_le_audio_qr_code_is_not_valid_format:I = 0x7f12078a

.field public static final bt_le_audio_scan_qr_code_scanner:I = 0x7f12078b

.field public static final bt_leaudio_cg_dialog_summary:I = 0x7f12078c

.field public static final bt_leaudio_open_dialog_summary:I = 0x7f12078d

.field public static final bt_leaudio_open_dialog_title:I = 0x7f12078e

.field public static final bt_leaudio_pre_title:I = 0x7f12078f

.field public static final bt_leaudio_summary:I = 0x7f120790

.field public static final bt_leaudio_support_multi_dialog_summary:I = 0x7f120791

.field public static final bt_lhdc_declaration:I = 0x7f120792

.field public static final bt_lhdc_dialog_summary:I = 0x7f120793

.field public static final bt_lhdc_dialog_title:I = 0x7f120794

.field public static final bt_lhdc_open_dialog_summary:I = 0x7f120795

.field public static final bt_lhdc_open_dialog_title:I = 0x7f120796

.field public static final bt_lhdc_pre_title:I = 0x7f120797

.field public static final bt_lhdc_property:I = 0x7f120798

.field public static final bt_plugin_current_version:I = 0x7f120799

.field public static final bt_plugin_new_version:I = 0x7f12079a

.field public static final bt_plugin_preference_button:I = 0x7f12079b

.field public static final bt_plugin_preference_msg:I = 0x7f12079c

.field public static final bt_plugin_preference_title:I = 0x7f12079d

.field public static final bt_plugin_set_net_dialog_cancle:I = 0x7f12079e

.field public static final bt_plugin_set_net_dialog_message:I = 0x7f12079f

.field public static final bt_plugin_set_net_dialog_retry:I = 0x7f1207a0

.field public static final bt_plugin_set_net_dialog_title:I = 0x7f1207a1

.field public static final bt_pre_summary:I = 0x7f1207a2

.field public static final bt_rarely_used_device_title:I = 0x7f1207a3

.field public static final bt_show_notification_summary:I = 0x7f1207a4

.field public static final bt_show_notification_title:I = 0x7f1207a5

.field public static final bt_trace_log:I = 0x7f1207a6

.field public static final bt_trace_log_summary:I = 0x7f1207a7

.field public static final btn_back:I = 0x7f1207a8

.field public static final bubble_app_setting_all:I = 0x7f1207a9

.field public static final bubble_app_setting_bubble_conversation:I = 0x7f1207aa

.field public static final bubble_app_setting_excluded_conversation_title:I = 0x7f1207ab

.field public static final bubble_app_setting_none:I = 0x7f1207ac

.field public static final bubble_app_setting_selected:I = 0x7f1207ad

.field public static final bubble_app_setting_selected_conversation_title:I = 0x7f1207ae

.field public static final bubble_app_setting_unbubble_conversation:I = 0x7f1207af

.field public static final bubbles_app_toggle_summary:I = 0x7f1207b0

.field public static final bubbles_app_toggle_title:I = 0x7f1207b1

.field public static final bubbles_conversation_app_link:I = 0x7f1207b2

.field public static final bubbles_conversation_toggle_summary:I = 0x7f1207b3

.field public static final bubbles_conversation_toggle_title:I = 0x7f1207b4

.field public static final bubbles_feature_disabled_button_approve:I = 0x7f1207b5

.field public static final bubbles_feature_disabled_button_cancel:I = 0x7f1207b6

.field public static final bubbles_feature_disabled_dialog_text:I = 0x7f1207b7

.field public static final bubbles_feature_disabled_dialog_title:I = 0x7f1207b8

.field public static final bubbles_feature_education:I = 0x7f1207b9

.field public static final bug_report_handler_picker_footer_text:I = 0x7f1207ba

.field public static final bug_report_handler_title:I = 0x7f1207bb

.field public static final bug_report_settings:I = 0x7f1207bc

.field public static final bugreport_in_power:I = 0x7f1207bd

.field public static final bugreport_in_power_summary:I = 0x7f1207be

.field public static final build_number:I = 0x7f1207bf

.field public static final builtin_keyboard_settings_summary:I = 0x7f1207c0

.field public static final builtin_keyboard_settings_title:I = 0x7f1207c1

.field public static final button_disconnect_network:I = 0x7f1207c2

.field public static final button_height_text:I = 0x7f1207c3

.field public static final button_light_timeout:I = 0x7f1207c4

.field public static final button_navigation_settings_activity_title:I = 0x7f1207c5

.field public static final button_normal_text:I = 0x7f1207c6

.field public static final button_text_accept:I = 0x7f1207c7

.field public static final button_text_accept_timer:I = 0x7f1207c8

.field public static final button_text_cancel:I = 0x7f1207c9

.field public static final button_text_login:I = 0x7f1207ca

.field public static final button_text_next_step:I = 0x7f1207cb

.field public static final button_text_next_step_timer:I = 0x7f1207cc

.field public static final button_text_ok:I = 0x7f1207cd

.field public static final button_text_ok_timer:I = 0x7f1207ce

.field public static final button_text_reboot_now:I = 0x7f1207cf

.field public static final button_text_reject:I = 0x7f1207d0

.field public static final byteShort:I = 0x7f1207d1

.field public static final ca_certificate:I = 0x7f1207d2

.field public static final ca_certificate_warning_description:I = 0x7f1207d3

.field public static final ca_certificate_warning_title:I = 0x7f1207d4

.field public static final cache_db_name:I = 0x7f1207d5

.field public static final cache_header_label:I = 0x7f1207d6

.field public static final cache_size_label:I = 0x7f1207d7

.field public static final cached:I = 0x7f1207d8

.field public static final cached_apps_freezer:I = 0x7f1207d9

.field public static final cached_apps_freezer_device_default:I = 0x7f1207da

.field public static final cached_apps_freezer_disabled:I = 0x7f1207db

.field public static final cached_apps_freezer_enabled:I = 0x7f1207dc

.field public static final cached_apps_freezer_reboot_dialog_text:I = 0x7f1207dd

.field public static final calendar_component_name:I = 0x7f1207de

.field public static final calendar_remind:I = 0x7f1207df

.field public static final calendar_sound_dialog_title:I = 0x7f1207e0

.field public static final calendar_sound_title:I = 0x7f1207e1

.field public static final call_category:I = 0x7f1207e2

.field public static final call_connected_tones_title:I = 0x7f1207e3

.field public static final call_example_name:I = 0x7f1207e4

.field public static final call_manager_enable_summary:I = 0x7f1207e5

.field public static final call_manager_enable_title:I = 0x7f1207e6

.field public static final call_manager_summary:I = 0x7f1207e7

.field public static final call_manager_title:I = 0x7f1207e8

.field public static final call_records_name_1:I = 0x7f1207e9

.field public static final call_records_name_2:I = 0x7f1207ea

.field public static final call_records_name_3:I = 0x7f1207eb

.field public static final call_records_summary_1:I = 0x7f1207ec

.field public static final call_records_summary_2:I = 0x7f1207ed

.field public static final call_records_summary_3:I = 0x7f1207ee

.field public static final call_settings_primary_user_only:I = 0x7f1207ef

.field public static final call_settings_summary:I = 0x7f1207f0

.field public static final call_settings_title:I = 0x7f1207f1

.field public static final call_volume_option_title:I = 0x7f1207f2

.field public static final calls_and_sms:I = 0x7f1207f3

.field public static final calls_and_sms_ask_every_time:I = 0x7f1207f4

.field public static final calls_and_sms_category:I = 0x7f1207f5

.field public static final calls_preference:I = 0x7f1207f6

.field public static final calls_preference_title:I = 0x7f1207f7

.field public static final calls_sms_calls_preferred:I = 0x7f1207f8

.field public static final calls_sms_footnote:I = 0x7f1207f9

.field public static final calls_sms_no_sim:I = 0x7f1207fa

.field public static final calls_sms_preferred:I = 0x7f1207fb

.field public static final calls_sms_sms_preferred:I = 0x7f1207fc

.field public static final calls_sms_temp_unavailable:I = 0x7f1207fd

.field public static final calls_sms_unavailable:I = 0x7f1207fe

.field public static final calls_sms_wfc_summary:I = 0x7f1207ff

.field public static final camera_double_tap_power_gesture_desc:I = 0x7f120800

.field public static final camera_double_tap_power_gesture_title:I = 0x7f120801

.field public static final camera_front:I = 0x7f120802

.field public static final camera_gesture_desc:I = 0x7f120803

.field public static final camera_gesture_title:I = 0x7f120804

.field public static final camera_key_action_app_title:I = 0x7f120805

.field public static final camera_key_action_category_title:I = 0x7f120806

.field public static final camera_key_action_none_title:I = 0x7f120807

.field public static final camera_key_action_setting_title:I = 0x7f120808

.field public static final camera_key_action_shortcut_call:I = 0x7f120809

.field public static final camera_key_action_shortcut_home:I = 0x7f12080a

.field public static final camera_key_action_shortcut_screenshot:I = 0x7f12080b

.field public static final camera_key_action_shortcut_search:I = 0x7f12080c

.field public static final camera_key_action_shortcut_title:I = 0x7f12080d

.field public static final camera_key_action_shortcut_wake:I = 0x7f12080e

.field public static final camera_key_action_title:I = 0x7f12080f

.field public static final camera_key_action_toggle_title:I = 0x7f120810

.field public static final camera_laser_sensor_switch:I = 0x7f120811

.field public static final camera_rear:I = 0x7f120812

.field public static final camera_toggle_title:I = 0x7f120813

.field public static final cancel:I = 0x7f120814

.field public static final cancel_action:I = 0x7f120815

.field public static final cancel_alert:I = 0x7f120816

.field public static final cancel_button:I = 0x7f120817

.field public static final cancel_download_buton_text:I = 0x7f120818

.field public static final cancel_lock_screen_dialog_button_label:I = 0x7f120819

.field public static final cancel_refresh_group:I = 0x7f12081a

.field public static final cannot_change_apn_toast:I = 0x7f12081b

.field public static final cant_sync_dialog_message:I = 0x7f12081c

.field public static final cant_sync_dialog_title:I = 0x7f12081d

.field public static final capabilities_list_title:I = 0x7f12081e

.field public static final captioning_background_color:I = 0x7f12081f

.field public static final captioning_background_opacity:I = 0x7f120820

.field public static final captioning_caption_appearance_summary:I = 0x7f120821

.field public static final captioning_caption_appearance_title:I = 0x7f120822

.field public static final captioning_custom_options_title:I = 0x7f120823

.field public static final captioning_edge_color:I = 0x7f120824

.field public static final captioning_edge_type:I = 0x7f120825

.field public static final captioning_foreground_color:I = 0x7f120826

.field public static final captioning_foreground_opacity:I = 0x7f120827

.field public static final captioning_locale:I = 0x7f120828

.field public static final captioning_more_options_title:I = 0x7f120829

.field public static final captioning_preset:I = 0x7f12082a

.field public static final captioning_preview_characters:I = 0x7f12082b

.field public static final captioning_preview_text:I = 0x7f12082c

.field public static final captioning_preview_title:I = 0x7f12082d

.field public static final captioning_standard_options_title:I = 0x7f12082e

.field public static final captioning_text_size:I = 0x7f12082f

.field public static final captioning_typeface:I = 0x7f120830

.field public static final captioning_window_color:I = 0x7f120831

.field public static final captioning_window_opacity:I = 0x7f120832

.field public static final captions_category_title:I = 0x7f120833

.field public static final capture_system_heap_dump_title:I = 0x7f120834

.field public static final capturing_system_heap_dump_message:I = 0x7f120835

.field public static final card_holder_header_title:I = 0x7f120836

.field public static final card_main_hbassistant_title:I = 0x7f120837

.field public static final cards_passes_lower:I = 0x7f120838

.field public static final cards_passes_sentence:I = 0x7f120839

.field public static final carrier:I = 0x7f12083a

.field public static final carrier_and_update_now_text:I = 0x7f12083b

.field public static final carrier_and_update_text:I = 0x7f12083c

.field public static final carrier_enabled:I = 0x7f12083d

.field public static final carrier_enabled_summaryOff:I = 0x7f12083e

.field public static final carrier_enabled_summaryOn:I = 0x7f12083f

.field public static final carrier_network_change_mode:I = 0x7f120840

.field public static final carrier_provisioning:I = 0x7f120841

.field public static final carrier_settings_euicc:I = 0x7f120842

.field public static final carrier_settings_title:I = 0x7f120843

.field public static final carrier_settings_version:I = 0x7f120844

.field public static final carrier_test:I = 0x7f120845

.field public static final carrier_wifi_network_title:I = 0x7f120846

.field public static final carrier_wifi_offload_summary:I = 0x7f120847

.field public static final carrier_wifi_offload_title:I = 0x7f120848

.field public static final category_name_appearance:I = 0x7f120849

.field public static final category_name_brightness:I = 0x7f12084a

.field public static final category_name_color:I = 0x7f12084b

.field public static final category_name_display_controls:I = 0x7f12084c

.field public static final category_name_general:I = 0x7f12084d

.field public static final category_name_lock_display:I = 0x7f12084e

.field public static final category_name_others:I = 0x7f12084f

.field public static final category_personal:I = 0x7f120850

.field public static final category_work:I = 0x7f120851

.field public static final cdma_lte_data_service:I = 0x7f120852

.field public static final cdma_security_settings_summary:I = 0x7f120853

.field public static final cdma_subscription_dialogtitle:I = 0x7f120854

.field public static final cdma_subscription_summary:I = 0x7f120855

.field public static final cdma_subscription_title:I = 0x7f120856

.field public static final cdma_system_select_dialogtitle:I = 0x7f120857

.field public static final cdma_system_select_summary:I = 0x7f120858

.field public static final cdma_system_select_title:I = 0x7f120859

.field public static final cell_broadcast_settings:I = 0x7f12085a

.field public static final cell_data_limit:I = 0x7f12085b

.field public static final cell_data_off_content_description:I = 0x7f12085c

.field public static final cell_data_template:I = 0x7f12085d

.field public static final cell_data_warning:I = 0x7f12085e

.field public static final cell_data_warning_and_limit:I = 0x7f12085f

.field public static final cellular_data_summary:I = 0x7f120860

.field public static final cellular_data_usage:I = 0x7f120861

.field public static final cert_not_installed:I = 0x7f120862

.field public static final certificate_management_app:I = 0x7f120863

.field public static final certificate_management_app_description:I = 0x7f120864

.field public static final certificate_warning_dont_install:I = 0x7f120865

.field public static final certificate_warning_install_anyway:I = 0x7f120866

.field public static final certinstaller_package:I = 0x7f120867

.field public static final change:I = 0x7f120868

.field public static final change_brightness:I = 0x7f120869

.field public static final change_lock_screen_password_title:I = 0x7f12086a

.field public static final change_password:I = 0x7f12086b

.field public static final change_security_lock:I = 0x7f12086c

.field public static final change_storage:I = 0x7f12086d

.field public static final change_theme_reboot:I = 0x7f12086e

.field public static final change_wifi_state_app_detail_summary:I = 0x7f12086f

.field public static final change_wifi_state_app_detail_switch:I = 0x7f120870

.field public static final change_wifi_state_title:I = 0x7f120871

.field public static final channel_group_notifications_off_desc:I = 0x7f120872

.field public static final channel_notifications_off_desc:I = 0x7f120873

.field public static final character_counter_content_description:I = 0x7f120874

.field public static final character_counter_overflowed_content_description:I = 0x7f120875

.field public static final character_counter_pattern:I = 0x7f120876

.field public static final charge_length_format:I = 0x7f120877

.field public static final charging_sounds_title:I = 0x7f120878

.field public static final check_find_device_status_failure_confirm:I = 0x7f120879

.field public static final check_update:I = 0x7f12087a

.field public static final checkbox_notification_same_as_incoming_call:I = 0x7f12087b

.field public static final checkbox_text:I = 0x7f12087c

.field public static final checking_find_device_status:I = 0x7f12087d

.field public static final checking_for_updates:I = 0x7f12087e

.field public static final chinese_day_1:I = 0x7f12087f

.field public static final chinese_day_10:I = 0x7f120880

.field public static final chinese_day_11:I = 0x7f120881

.field public static final chinese_day_12:I = 0x7f120882

.field public static final chinese_day_13:I = 0x7f120883

.field public static final chinese_day_14:I = 0x7f120884

.field public static final chinese_day_15:I = 0x7f120885

.field public static final chinese_day_16:I = 0x7f120886

.field public static final chinese_day_17:I = 0x7f120887

.field public static final chinese_day_18:I = 0x7f120888

.field public static final chinese_day_19:I = 0x7f120889

.field public static final chinese_day_2:I = 0x7f12088a

.field public static final chinese_day_20:I = 0x7f12088b

.field public static final chinese_day_21:I = 0x7f12088c

.field public static final chinese_day_22:I = 0x7f12088d

.field public static final chinese_day_23:I = 0x7f12088e

.field public static final chinese_day_24:I = 0x7f12088f

.field public static final chinese_day_25:I = 0x7f120890

.field public static final chinese_day_26:I = 0x7f120891

.field public static final chinese_day_27:I = 0x7f120892

.field public static final chinese_day_28:I = 0x7f120893

.field public static final chinese_day_29:I = 0x7f120894

.field public static final chinese_day_3:I = 0x7f120895

.field public static final chinese_day_30:I = 0x7f120896

.field public static final chinese_day_4:I = 0x7f120897

.field public static final chinese_day_5:I = 0x7f120898

.field public static final chinese_day_6:I = 0x7f120899

.field public static final chinese_day_7:I = 0x7f12089a

.field public static final chinese_day_8:I = 0x7f12089b

.field public static final chinese_day_9:I = 0x7f12089c

.field public static final chinese_day_elementary:I = 0x7f12089d

.field public static final chinese_digit_eight:I = 0x7f12089e

.field public static final chinese_digit_five:I = 0x7f12089f

.field public static final chinese_digit_four:I = 0x7f1208a0

.field public static final chinese_digit_nine:I = 0x7f1208a1

.field public static final chinese_digit_one:I = 0x7f1208a2

.field public static final chinese_digit_seven:I = 0x7f1208a3

.field public static final chinese_digit_six:I = 0x7f1208a4

.field public static final chinese_digit_ten:I = 0x7f1208a5

.field public static final chinese_digit_three:I = 0x7f1208a6

.field public static final chinese_digit_two:I = 0x7f1208a7

.field public static final chinese_digit_zero:I = 0x7f1208a8

.field public static final chinese_leap:I = 0x7f1208a9

.field public static final chinese_month:I = 0x7f1208aa

.field public static final chinese_month_april:I = 0x7f1208ab

.field public static final chinese_month_august:I = 0x7f1208ac

.field public static final chinese_month_december:I = 0x7f1208ad

.field public static final chinese_month_february:I = 0x7f1208ae

.field public static final chinese_month_january:I = 0x7f1208af

.field public static final chinese_month_july:I = 0x7f1208b0

.field public static final chinese_month_june:I = 0x7f1208b1

.field public static final chinese_month_march:I = 0x7f1208b2

.field public static final chinese_month_may:I = 0x7f1208b3

.field public static final chinese_month_november:I = 0x7f1208b4

.field public static final chinese_month_october:I = 0x7f1208b5

.field public static final chinese_month_september:I = 0x7f1208b6

.field public static final chinese_symbol_animals_chicken:I = 0x7f1208b7

.field public static final chinese_symbol_animals_cow:I = 0x7f1208b8

.field public static final chinese_symbol_animals_dog:I = 0x7f1208b9

.field public static final chinese_symbol_animals_dragon:I = 0x7f1208ba

.field public static final chinese_symbol_animals_horse:I = 0x7f1208bb

.field public static final chinese_symbol_animals_monkey:I = 0x7f1208bc

.field public static final chinese_symbol_animals_mouse:I = 0x7f1208bd

.field public static final chinese_symbol_animals_pig:I = 0x7f1208be

.field public static final chinese_symbol_animals_rabbit:I = 0x7f1208bf

.field public static final chinese_symbol_animals_sheep:I = 0x7f1208c0

.field public static final chinese_symbol_animals_snake:I = 0x7f1208c1

.field public static final chinese_symbol_animals_tiger:I = 0x7f1208c2

.field public static final chip_text:I = 0x7f1208c3

.field public static final choose_a_card_to_reset:I = 0x7f1208c4

.field public static final choose_keyguard_clock_style:I = 0x7f1208c5

.field public static final choose_keyguard_clock_summary:I = 0x7f1208c6

.field public static final choose_lock_pattern_header_text:I = 0x7f1208c7

.field public static final choose_network_title:I = 0x7f1208c8

.field public static final choose_profile:I = 0x7f1208c9

.field public static final choose_sim_activating:I = 0x7f1208ca

.field public static final choose_sim_could_not_activate:I = 0x7f1208cb

.field public static final choose_sim_item_summary_unknown:I = 0x7f1208cc

.field public static final choose_sim_text:I = 0x7f1208cd

.field public static final choose_sim_title:I = 0x7f1208ce

.field public static final choose_smartcover_type:I = 0x7f1208cf

.field public static final choose_spell_checker:I = 0x7f1208d0

.field public static final choose_timezone:I = 0x7f1208d1

.field public static final choose_unlock_face_msg:I = 0x7f1208d2

.field public static final choose_unlock_fingerprint_msg:I = 0x7f1208d3

.field public static final choose_voice_input_title:I = 0x7f1208d4

.field public static final classic_mode_summary:I = 0x7f1208d5

.field public static final classic_mode_title:I = 0x7f1208d6

.field public static final cleaner:I = 0x7f1208d7

.field public static final clear:I = 0x7f1208d8

.field public static final clear_activities:I = 0x7f1208d9

.field public static final clear_activities_summary:I = 0x7f1208da

.field public static final clear_adb_keys:I = 0x7f1208db

.field public static final clear_all_data:I = 0x7f1208dc

.field public static final clear_cache_btn_text:I = 0x7f1208dd

.field public static final clear_data_alert_info:I = 0x7f1208de

.field public static final clear_data_alert_info_pad:I = 0x7f1208df

.field public static final clear_data_dlg_text:I = 0x7f1208e0

.field public static final clear_data_dlg_title:I = 0x7f1208e1

.field public static final clear_failed_dlg_text:I = 0x7f1208e2

.field public static final clear_instant_app_confirmation:I = 0x7f1208e3

.field public static final clear_instant_app_data:I = 0x7f1208e4

.field public static final clear_phone_data_category:I = 0x7f1208e5

.field public static final clear_text_end_icon_content_description:I = 0x7f1208e6

.field public static final clear_uri_btn_text:I = 0x7f1208e7

.field public static final clear_user_data_text:I = 0x7f1208e8

.field public static final clearable_edittext_clear_description:I = 0x7f1208e9

.field public static final clock_component_name:I = 0x7f1208ea

.field public static final close:I = 0x7f1208eb

.field public static final close_aod:I = 0x7f1208ec

.field public static final close_app:I = 0x7f1208ed

.field public static final close_demo_mode:I = 0x7f1208ee

.field public static final close_fod:I = 0x7f1208ef

.field public static final close_fod_aod:I = 0x7f1208f0

.field public static final close_hotspot_hint:I = 0x7f1208f1

.field public static final close_lid_display_settings_title:I = 0x7f1208f2

.field public static final close_miui_optimization_title:I = 0x7f1208f3

.field public static final close_optimization_message:I = 0x7f1208f4

.field public static final close_optimization_option:I = 0x7f1208f5

.field public static final close_privacy_thumbnail_blur_title:I = 0x7f1208f6

.field public static final close_slave_wifi_hint:I = 0x7f1208f7

.field public static final closed_beta:I = 0x7f1208f8

.field public static final cloud_backup_section_title:I = 0x7f1208f9

.field public static final cloud_backup_settings_section_title:I = 0x7f1208fa

.field public static final cloud_category_text:I = 0x7f1208fb

.field public static final cloud_restore_section_summary:I = 0x7f1208fc

.field public static final cloud_restore_section_summary_new:I = 0x7f1208fd

.field public static final cloud_restore_section_title:I = 0x7f1208fe

.field public static final cloud_restore_section_title_new:I = 0x7f1208ff

.field public static final cmcc_hardware_version:I = 0x7f120900

.field public static final codec_low_latency_summary:I = 0x7f120901

.field public static final codec_low_latency_title:I = 0x7f120902

.field public static final codec_low_latency_video_summary:I = 0x7f120903

.field public static final codec_low_latency_zmi_summary:I = 0x7f120904

.field public static final codec_low_latency_zmi_title:I = 0x7f120905

.field public static final collect_diagnosis_log:I = 0x7f120906

.field public static final color_black:I = 0x7f120907

.field public static final color_blue:I = 0x7f120908

.field public static final color_calibration_none:I = 0x7f120909

.field public static final color_calibration_srgb:I = 0x7f12090a

.field public static final color_custom:I = 0x7f12090b

.field public static final color_cyan:I = 0x7f12090c

.field public static final color_game_led_applications_title:I = 0x7f12090d

.field public static final color_game_led_applications_title_summary:I = 0x7f12090e

.field public static final color_game_led_battery_title:I = 0x7f12090f

.field public static final color_game_led_charging_summary:I = 0x7f120910

.field public static final color_game_led_color_title:I = 0x7f120911

.field public static final color_game_led_end_time:I = 0x7f120912

.field public static final color_game_led_feature_off:I = 0x7f120913

.field public static final color_game_led_feature_on:I = 0x7f120914

.field public static final color_game_led_freq_title:I = 0x7f120915

.field public static final color_game_led_game_mode_summary:I = 0x7f120916

.field public static final color_game_led_game_mode_title:I = 0x7f120917

.field public static final color_game_led_incall_pulse_title:I = 0x7f120918

.field public static final color_game_led_notification_pulse_title:I = 0x7f120919

.field public static final color_game_led_remind_title:I = 0x7f12091a

.field public static final color_game_led_special_scene_title:I = 0x7f12091b

.field public static final color_game_led_start_time:I = 0x7f12091c

.field public static final color_game_led_summary:I = 0x7f12091d

.field public static final color_game_led_time_title:I = 0x7f12091e

.field public static final color_game_led_title:I = 0x7f12091f

.field public static final color_gray:I = 0x7f120920

.field public static final color_green:I = 0x7f120921

.field public static final color_lamp_applications_title:I = 0x7f120922

.field public static final color_lamp_applications_title_summary:I = 0x7f120923

.field public static final color_lamp_battery_title:I = 0x7f120924

.field public static final color_lamp_charging_summary:I = 0x7f120925

.field public static final color_lamp_incall_pulse_title:I = 0x7f120926

.field public static final color_lamp_notification_pulse_title:I = 0x7f120927

.field public static final color_lamp_title:I = 0x7f120928

.field public static final color_led_applications_title:I = 0x7f120929

.field public static final color_led_applications_title_summary:I = 0x7f12092a

.field public static final color_led_battery_title:I = 0x7f12092b

.field public static final color_led_charging_summary:I = 0x7f12092c

.field public static final color_led_end_time:I = 0x7f12092d

.field public static final color_led_game_mode_summary:I = 0x7f12092e

.field public static final color_led_game_mode_title:I = 0x7f12092f

.field public static final color_led_hint:I = 0x7f120930

.field public static final color_led_incall_pulse_title:I = 0x7f120931

.field public static final color_led_lucky_dialog_message:I = 0x7f120932

.field public static final color_led_lucky_dialog_title:I = 0x7f120933

.field public static final color_led_music_mode_summary:I = 0x7f120934

.field public static final color_led_music_mode_title:I = 0x7f120935

.field public static final color_led_notification_pulse_title:I = 0x7f120936

.field public static final color_led_red_more_feature:I = 0x7f120937

.field public static final color_led_red_package_title:I = 0x7f120938

.field public static final color_led_red_packets_summary:I = 0x7f120939

.field public static final color_led_special_scene_title:I = 0x7f12093a

.field public static final color_led_start_time:I = 0x7f12093b

.field public static final color_led_summary:I = 0x7f12093c

.field public static final color_led_title:I = 0x7f12093d

.field public static final color_magenta:I = 0x7f12093e

.field public static final color_mode_option_automatic:I = 0x7f12093f

.field public static final color_mode_option_boosted:I = 0x7f120940

.field public static final color_mode_option_natural:I = 0x7f120941

.field public static final color_mode_option_saturated:I = 0x7f120942

.field public static final color_mode_summary_automatic:I = 0x7f120943

.field public static final color_mode_summary_natural:I = 0x7f120944

.field public static final color_mode_title:I = 0x7f120945

.field public static final color_none:I = 0x7f120946

.field public static final color_orange:I = 0x7f120947

.field public static final color_purple:I = 0x7f120948

.field public static final color_red:I = 0x7f120949

.field public static final color_temperature:I = 0x7f12094a

.field public static final color_temperature_desc:I = 0x7f12094b

.field public static final color_temperature_toast:I = 0x7f12094c

.field public static final color_title:I = 0x7f12094d

.field public static final color_unspecified:I = 0x7f12094e

.field public static final color_white:I = 0x7f12094f

.field public static final color_yellow:I = 0x7f120950

.field public static final colors_viewpager_content_description:I = 0x7f120951

.field public static final common_issue:I = 0x7f120952

.field public static final common_settings:I = 0x7f120953

.field public static final complete_parameters:I = 0x7f120954

.field public static final complete_parameters_new:I = 0x7f120955

.field public static final complete_settings:I = 0x7f120956

.field public static final computer_backup_summary:I = 0x7f120957

.field public static final computer_backup_title:I = 0x7f120958

.field public static final computing_size:I = 0x7f120959

.field public static final condition_airplane_summary:I = 0x7f12095a

.field public static final condition_airplane_title:I = 0x7f12095b

.field public static final condition_battery_summary:I = 0x7f12095c

.field public static final condition_battery_title:I = 0x7f12095d

.field public static final condition_bg_data_summary:I = 0x7f12095e

.field public static final condition_bg_data_title:I = 0x7f12095f

.field public static final condition_cellular_summary:I = 0x7f120960

.field public static final condition_cellular_title:I = 0x7f120961

.field public static final condition_device_muted_action_turn_on_sound:I = 0x7f120962

.field public static final condition_device_muted_summary:I = 0x7f120963

.field public static final condition_device_muted_title:I = 0x7f120964

.field public static final condition_device_vibrate_summary:I = 0x7f120965

.field public static final condition_device_vibrate_title:I = 0x7f120966

.field public static final condition_expand_hide:I = 0x7f120967

.field public static final condition_expand_show:I = 0x7f120968

.field public static final condition_grayscale_summary:I = 0x7f120969

.field public static final condition_grayscale_title:I = 0x7f12096a

.field public static final condition_hotspot_title:I = 0x7f12096b

.field public static final condition_night_display_summary:I = 0x7f12096c

.field public static final condition_night_display_title:I = 0x7f12096d

.field public static final condition_summary:I = 0x7f12096e

.field public static final condition_turn_off:I = 0x7f12096f

.field public static final condition_turn_on:I = 0x7f120970

.field public static final condition_work_summary:I = 0x7f120971

.field public static final condition_work_title:I = 0x7f120972

.field public static final condition_zen_summary_phone_muted:I = 0x7f120973

.field public static final condition_zen_summary_with_exceptions:I = 0x7f120974

.field public static final condition_zen_title:I = 0x7f120975

.field public static final config_account_intent_uri:I = 0x7f120976

.field public static final config_aosp_emergency_intent_action:I = 0x7f120977

.field public static final config_aosp_emergency_package_name:I = 0x7f120978

.field public static final config_audio_storage_category_uri:I = 0x7f120979

.field public static final config_backup_settings_intent:I = 0x7f12097a

.field public static final config_backup_settings_label:I = 0x7f12097b

.field public static final config_battery_prediction_authority:I = 0x7f12097c

.field public static final config_cell_broadcast_receiver_package:I = 0x7f12097d

.field public static final config_charging_function_name:I = 0x7f12097e

.field public static final config_contextual_card_feedback_email:I = 0x7f12097f

.field public static final config_deletion_helper_class:I = 0x7f120980

.field public static final config_deletion_helper_package:I = 0x7f120981

.field public static final config_documents_and_other_storage_category_uri:I = 0x7f120982

.field public static final config_emergency_intent_action:I = 0x7f120983

.field public static final config_emergency_package_name:I = 0x7f120984

.field public static final config_face_enroll:I = 0x7f120985

.field public static final config_featureFactory:I = 0x7f120986

.field public static final config_grayscale_settings_intent:I = 0x7f120987

.field public static final config_images_storage_category_uri:I = 0x7f120988

.field public static final config_list_label:I = 0x7f120989

.field public static final config_mass_storage_function_name:I = 0x7f12098a

.field public static final config_mtp_function_name:I = 0x7f12098b

.field public static final config_nearby_devices_slice_uri:I = 0x7f12098c

.field public static final config_non_public_slice_query_uri:I = 0x7f12098d

.field public static final config_package_installer_package_name:I = 0x7f12098e

.field public static final config_ptp_function_name:I = 0x7f12098f

.field public static final config_rat_2g:I = 0x7f120990

.field public static final config_rat_3g:I = 0x7f120991

.field public static final config_rat_4g:I = 0x7f120992

.field public static final config_rat_unknown:I = 0x7f120993

.field public static final config_rtt_setting_intent_action:I = 0x7f120994

.field public static final config_rtt_setting_package_name:I = 0x7f120995

.field public static final config_schedules_provider_caller_package:I = 0x7f120996

.field public static final config_settingsintelligence_log_action:I = 0x7f120997

.field public static final config_settingsintelligence_package_name:I = 0x7f120998

.field public static final config_styles_and_wallpaper_picker_class:I = 0x7f120999

.field public static final config_videos_storage_category_uri:I = 0x7f12099a

.field public static final config_wallpaper_picker_class:I = 0x7f12099b

.field public static final config_wallpaper_picker_launch_extra:I = 0x7f12099c

.field public static final config_wallpaper_picker_package:I = 0x7f12099d

.field public static final configure:I = 0x7f12099e

.field public static final configure_apps:I = 0x7f12099f

.field public static final configure_input_method:I = 0x7f1209a0

.field public static final configure_notification_settings:I = 0x7f1209a1

.field public static final configure_section_header:I = 0x7f1209a2

.field public static final confirm_action:I = 0x7f1209a3

.field public static final confirm_bind_xiaomi_account_dialog_title:I = 0x7f1209a4

.field public static final confirm_device_credential_password:I = 0x7f1209a5

.field public static final confirm_device_credential_pattern:I = 0x7f1209a6

.field public static final confirm_device_credential_pin:I = 0x7f1209a7

.field public static final confirm_download_cancelation_dialog_message:I = 0x7f1209a8

.field public static final confirm_enable_oem_unlock_text:I = 0x7f1209a9

.field public static final confirm_enable_oem_unlock_title:I = 0x7f1209aa

.field public static final confirm_erase:I = 0x7f1209ab

.field public static final confirm_fingerprint:I = 0x7f1209ac

.field public static final confirm_fingerprint_msg:I = 0x7f1209ad

.field public static final confirm_frp_credential_message:I = 0x7f1209ae

.field public static final confirm_frp_credential_skip_txt:I = 0x7f1209af

.field public static final confirm_frp_credential_title:I = 0x7f1209b0

.field public static final confirm_lock_pattern_header_text:I = 0x7f1209b1

.field public static final confirm_new_backup_pw_prompt:I = 0x7f1209b2

.field public static final confirm_password_with_fingerprint:I = 0x7f1209b3

.field public static final confirm_password_with_password:I = 0x7f1209b4

.field public static final confirm_privacy_password:I = 0x7f1209b5

.field public static final confirm_privacy_password_fingerprint:I = 0x7f1209b6

.field public static final confirm_reset_aaid:I = 0x7f1209b7

.field public static final confirm_sim_deletion_description:I = 0x7f1209b8

.field public static final confirm_sim_deletion_title:I = 0x7f1209b9

.field public static final confirmation_turn_on:I = 0x7f1209ba

.field public static final connect_group:I = 0x7f1209bb

.field public static final connect_help:I = 0x7f1209bc

.field public static final connect_later:I = 0x7f1209bd

.field public static final connect_mode:I = 0x7f1209be

.field public static final connect_to_internet:I = 0x7f1209bf

.field public static final connected_device_add_device_summary:I = 0x7f1209c0

.field public static final connected_device_bluetooth_turned_on_toast:I = 0x7f1209c1

.field public static final connected_device_call_device_title:I = 0x7f1209c2

.field public static final connected_device_connections_title:I = 0x7f1209c3

.field public static final connected_device_media_device_title:I = 0x7f1209c4

.field public static final connected_device_other_device_title:I = 0x7f1209c5

.field public static final connected_device_previously_connected_screen_title:I = 0x7f1209c6

.field public static final connected_device_previously_connected_title:I = 0x7f1209c7

.field public static final connected_device_saved_title:I = 0x7f1209c8

.field public static final connected_device_see_all_summary:I = 0x7f1209c9

.field public static final connected_devices_dashboard_android_auto_no_driving_mode_summary:I = 0x7f1209ca

.field public static final connected_devices_dashboard_android_auto_no_nfc_no_driving_mode:I = 0x7f1209cb

.field public static final connected_devices_dashboard_android_auto_no_nfc_summary:I = 0x7f1209cc

.field public static final connected_devices_dashboard_android_auto_summary:I = 0x7f1209cd

.field public static final connected_devices_dashboard_default_summary:I = 0x7f1209ce

.field public static final connected_devices_dashboard_no_driving_mode_no_nfc_summary:I = 0x7f1209cf

.field public static final connected_devices_dashboard_no_driving_mode_summary:I = 0x7f1209d0

.field public static final connected_devices_dashboard_no_nfc_summary:I = 0x7f1209d1

.field public static final connected_devices_dashboard_summary:I = 0x7f1209d2

.field public static final connected_devices_dashboard_title:I = 0x7f1209d3

.field public static final connected_to_metered_access_point:I = 0x7f1209d4

.field public static final connected_tws_device_saved_title:I = 0x7f1209d5

.field public static final connected_via_app:I = 0x7f1209d6

.field public static final connected_via_carrier:I = 0x7f1209d7

.field public static final connected_via_network_scorer:I = 0x7f1209d8

.field public static final connected_via_network_scorer_default:I = 0x7f1209d9

.field public static final connected_via_passpoint:I = 0x7f1209da

.field public static final connected_wifi:I = 0x7f1209db

.field public static final connecting_to_vpn:I = 0x7f1209dc

.field public static final connection_and_sharing:I = 0x7f1209dd

.field public static final connection_control:I = 0x7f1209de

.field public static final contact_data:I = 0x7f1209df

.field public static final contact_discovery_opt_in_dialog_message:I = 0x7f1209e0

.field public static final contact_discovery_opt_in_dialog_message_no_carrier_defined:I = 0x7f1209e1

.field public static final contact_discovery_opt_in_dialog_title:I = 0x7f1209e2

.field public static final contact_discovery_opt_in_dialog_title_no_carrier_defined:I = 0x7f1209e3

.field public static final contact_discovery_opt_in_summary:I = 0x7f1209e4

.field public static final contact_discovery_opt_in_title:I = 0x7f1209e5

.field public static final content_capture:I = 0x7f1209e6

.field public static final content_capture_summary:I = 0x7f1209e7

.field public static final content_description_menu_button:I = 0x7f1209e8

.field public static final content_miui_music_mute_by_user:I = 0x7f1209e9

.field public static final contextual_card_dismiss_confirm_message:I = 0x7f1209ea

.field public static final contextual_card_dismiss_keep:I = 0x7f1209eb

.field public static final contextual_card_dismiss_remove:I = 0x7f1209ec

.field public static final contextual_card_feedback_confirm_message:I = 0x7f1209ed

.field public static final contextual_card_feedback_send:I = 0x7f1209ee

.field public static final contextual_card_removed_message:I = 0x7f1209ef

.field public static final contextual_card_undo_dismissal_text:I = 0x7f1209f0

.field public static final continue_reset_factory:I = 0x7f1209f1

.field public static final contributors_title:I = 0x7f1209f2

.field public static final control_center:I = 0x7f1209f3

.field public static final control_center_force_use_panel_guide_text:I = 0x7f1209f4

.field public static final control_center_style:I = 0x7f1209f5

.field public static final control_center_style_summary:I = 0x7f1209f6

.field public static final controls_label:I = 0x7f1209f7

.field public static final controls_subtitle:I = 0x7f1209f8

.field public static final convenience_key_category_title:I = 0x7f1209f9

.field public static final conversation_category_title:I = 0x7f1209fa

.field public static final conversation_notifs_category:I = 0x7f1209fb

.field public static final conversation_onboarding_summary:I = 0x7f1209fc

.field public static final conversation_onboarding_title:I = 0x7f1209fd

.field public static final conversation_section_switch_summary:I = 0x7f1209fe

.field public static final conversation_section_switch_title:I = 0x7f1209ff

.field public static final conversation_settings_clear_recents:I = 0x7f120a00

.field public static final conversations_category_title:I = 0x7f120a01

.field public static final convert_to_file_encryption_done:I = 0x7f120a02

.field public static final convo_not_supported_summary:I = 0x7f120a03

.field public static final cool_color:I = 0x7f120a04

.field public static final copy:I = 0x7f120a05

.field public static final copyable_slice_toast:I = 0x7f120a06

.field public static final copyright:I = 0x7f120a07

.field public static final copyright_title:I = 0x7f120a08

.field public static final country_name:I = 0x7f120a09

.field public static final create:I = 0x7f120a0a

.field public static final creating_new_guest_dialog_message:I = 0x7f120a0b

.field public static final creating_new_user_dialog_message:I = 0x7f120a0c

.field public static final credential_contains:I = 0x7f120a0d

.field public static final credential_for_vpn_and_apps:I = 0x7f120a0e

.field public static final credential_for_wifi:I = 0x7f120a0f

.field public static final credentials_configure_lock_screen_hint:I = 0x7f120a10

.field public static final credentials_erased:I = 0x7f120a11

.field public static final credentials_install:I = 0x7f120a12

.field public static final credentials_install_summary:I = 0x7f120a13

.field public static final credentials_not_erased:I = 0x7f120a14

.field public static final credentials_password_too_short:I = 0x7f120a15

.field public static final credentials_reset:I = 0x7f120a16

.field public static final credentials_reset_hint:I = 0x7f120a17

.field public static final credentials_reset_summary:I = 0x7f120a18

.field public static final credentials_settings_not_available:I = 0x7f120a19

.field public static final credentials_title:I = 0x7f120a1a

.field public static final credentials_title_verification:I = 0x7f120a1b

.field public static final crisp_text:I = 0x7f120a1c

.field public static final critical_feedback:I = 0x7f120a1d

.field public static final cross_profile_calendar_summary:I = 0x7f120a1e

.field public static final cross_profile_calendar_title:I = 0x7f120a1f

.field public static final crypt_keeper_button_text:I = 0x7f120a20

.field public static final crypt_keeper_conditions:I = 0x7f120a21

.field public static final crypt_keeper_conditions_title:I = 0x7f120a22

.field public static final crypt_keeper_content:I = 0x7f120a23

.field public static final crypt_keeper_content_title:I = 0x7f120a24

.field public static final crypt_keeper_decrypt_methods:I = 0x7f120a25

.field public static final crypt_keeper_decrypt_methods_cannot_decrypt:I = 0x7f120a26

.field public static final crypt_keeper_decrypt_methods_title:I = 0x7f120a27

.field public static final crypt_keeper_dialog_need_password_message:I = 0x7f120a28

.field public static final crypt_keeper_dialog_need_password_title:I = 0x7f120a29

.field public static final crypt_keeper_encrypt_title:I = 0x7f120a2a

.field public static final crypt_keeper_encrypted_summary:I = 0x7f120a2b

.field public static final crypt_keeper_final_desc:I = 0x7f120a2c

.field public static final crypt_keeper_low_charge_text:I = 0x7f120a2d

.field public static final crypt_keeper_settings_title:I = 0x7f120a2e

.field public static final crypt_keeper_switch_input_method:I = 0x7f120a2f

.field public static final crypt_keeper_switch_to_pattern:I = 0x7f120a30

.field public static final crypt_keeper_switch_to_pin:I = 0x7f120a31

.field public static final crypt_keeper_unplugged_text:I = 0x7f120a32

.field public static final current_backup_pw_prompt:I = 0x7f120a33

.field public static final current_input_method:I = 0x7f120a34

.field public static final current_password:I = 0x7f120a35

.field public static final current_screen_lock:I = 0x7f120a36

.field public static final current_use_default_wallet:I = 0x7f120a37

.field public static final current_use_ese:I = 0x7f120a38

.field public static final current_use_ese_wallet:I = 0x7f120a39

.field public static final current_use_hce_wallet:I = 0x7f120a3a

.field public static final current_use_uicc_se:I = 0x7f120a3b

.field public static final current_use_uicc_wallet:I = 0x7f120a3c

.field public static final current_zone:I = 0x7f120a3d

.field public static final custom_carrier_title:I = 0x7f120a3e

.field public static final custom_color:I = 0x7f120a3f

.field public static final custom_gesture_title:I = 0x7f120a40

.field public static final custom_wifi_name_title:I = 0x7f120a41

.field public static final customize_button_title:I = 0x7f120a42

.field public static final customize_knock_gesture:I = 0x7f120a43

.field public static final cutout_mode_always:I = 0x7f120a44

.field public static final cutout_mode_default:I = 0x7f120a45

.field public static final cutout_mode_settings_title:I = 0x7f120a46

.field public static final cutout_mode_title:I = 0x7f120a47

.field public static final cutout_type_desc:I = 0x7f120a48

.field public static final cutout_type_title:I = 0x7f120a49

.field public static final daltonizer_mode_deuteranomaly:I = 0x7f120a4a

.field public static final daltonizer_mode_deuteranomaly_summary:I = 0x7f120a4b

.field public static final daltonizer_mode_deuteranomaly_title:I = 0x7f120a4c

.field public static final daltonizer_mode_disabled:I = 0x7f120a4d

.field public static final daltonizer_mode_grayscale_title:I = 0x7f120a4e

.field public static final daltonizer_mode_monochromacy:I = 0x7f120a4f

.field public static final daltonizer_mode_protanomaly:I = 0x7f120a50

.field public static final daltonizer_mode_protanomaly_summary:I = 0x7f120a51

.field public static final daltonizer_mode_protanomaly_title:I = 0x7f120a52

.field public static final daltonizer_mode_tritanomaly:I = 0x7f120a53

.field public static final daltonizer_mode_tritanomaly_summary:I = 0x7f120a54

.field public static final daltonizer_mode_tritanomaly_title:I = 0x7f120a55

.field public static final daltonizer_type_overridden:I = 0x7f120a56

.field public static final dangerous_option:I = 0x7f120a57

.field public static final dangerous_option_hint:I = 0x7f120a58

.field public static final dangerous_option_hint_title:I = 0x7f120a59

.field public static final dangerous_option_switch_title:I = 0x7f120a5a

.field public static final dark_color_mode:I = 0x7f120a5b

.field public static final dark_color_summary_lcd:I = 0x7f120a5c

.field public static final dark_color_summary_oled:I = 0x7f120a5d

.field public static final dark_mode_alert_dialog_cancel:I = 0x7f120a5e

.field public static final dark_mode_alert_dialog_checkbox:I = 0x7f120a5f

.field public static final dark_mode_alert_dialog_confirm:I = 0x7f120a60

.field public static final dark_mode_alert_dialog_message:I = 0x7f120a61

.field public static final dark_mode_alert_dialog_title:I = 0x7f120a62

.field public static final dark_mode_apps_header:I = 0x7f120a63

.field public static final dark_mode_auto_time_summary:I = 0x7f120a64

.field public static final dark_mode_auto_time_title:I = 0x7f120a65

.field public static final dark_mode_contrast:I = 0x7f120a66

.field public static final dark_mode_contrast_hint:I = 0x7f120a67

.field public static final dark_mode_day_night_mode_summary:I = 0x7f120a68

.field public static final dark_mode_day_night_mode_title:I = 0x7f120a69

.field public static final dark_mode_font_background_title:I = 0x7f120a6a

.field public static final dark_mode_time_settings:I = 0x7f120a6b

.field public static final dark_theme_main_switch_title:I = 0x7f120a6c

.field public static final dark_theme_slice_subtitle:I = 0x7f120a6d

.field public static final dark_theme_slice_title:I = 0x7f120a6e

.field public static final dark_ui_activation_off_auto:I = 0x7f120a6f

.field public static final dark_ui_activation_off_custom:I = 0x7f120a70

.field public static final dark_ui_activation_off_manual:I = 0x7f120a71

.field public static final dark_ui_activation_on_auto:I = 0x7f120a72

.field public static final dark_ui_activation_on_custom:I = 0x7f120a73

.field public static final dark_ui_activation_on_manual:I = 0x7f120a74

.field public static final dark_ui_auto_mode_auto:I = 0x7f120a75

.field public static final dark_ui_auto_mode_custom:I = 0x7f120a76

.field public static final dark_ui_auto_mode_custom_bedtime:I = 0x7f120a77

.field public static final dark_ui_auto_mode_never:I = 0x7f120a78

.field public static final dark_ui_auto_mode_title:I = 0x7f120a79

.field public static final dark_ui_bedtime_footer_action:I = 0x7f120a7a

.field public static final dark_ui_bedtime_footer_summary:I = 0x7f120a7b

.field public static final dark_ui_mode:I = 0x7f120a7c

.field public static final dark_ui_mode_disabled_summary_dark_theme_off:I = 0x7f120a7d

.field public static final dark_ui_mode_disabled_summary_dark_theme_on:I = 0x7f120a7e

.field public static final dark_ui_mode_title:I = 0x7f120a7f

.field public static final dark_ui_settings_light_summary:I = 0x7f120a80

.field public static final dark_ui_status_title:I = 0x7f120a81

.field public static final dark_ui_summary_off_auto_mode_auto:I = 0x7f120a82

.field public static final dark_ui_summary_off_auto_mode_custom:I = 0x7f120a83

.field public static final dark_ui_summary_off_auto_mode_custom_bedtime:I = 0x7f120a84

.field public static final dark_ui_summary_off_auto_mode_never:I = 0x7f120a85

.field public static final dark_ui_summary_on_auto_mode_auto:I = 0x7f120a86

.field public static final dark_ui_summary_on_auto_mode_custom:I = 0x7f120a87

.field public static final dark_ui_summary_on_auto_mode_custom_bedtime:I = 0x7f120a88

.field public static final dark_ui_summary_on_auto_mode_never:I = 0x7f120a89

.field public static final dark_ui_text:I = 0x7f120a8a

.field public static final dark_ui_title:I = 0x7f120a8b

.field public static final dark_wallpaper_mode:I = 0x7f120a8c

.field public static final dashboard_title:I = 0x7f120a8d

.field public static final data_acceleration_summery:I = 0x7f120a8e

.field public static final data_connection_3_5g:I = 0x7f120a8f

.field public static final data_connection_3_5g_plus:I = 0x7f120a90

.field public static final data_connection_3g:I = 0x7f120a91

.field public static final data_connection_4g:I = 0x7f120a92

.field public static final data_connection_4g_lte:I = 0x7f120a93

.field public static final data_connection_4g_lte_plus:I = 0x7f120a94

.field public static final data_connection_4g_plus:I = 0x7f120a95

.field public static final data_connection_5g:I = 0x7f120a96

.field public static final data_connection_5g_basic:I = 0x7f120a97

.field public static final data_connection_5g_plus:I = 0x7f120a98

.field public static final data_connection_5g_sa:I = 0x7f120a99

.field public static final data_connection_5g_uwb:I = 0x7f120a9a

.field public static final data_connection_5ge_html:I = 0x7f120a9b

.field public static final data_connection_carrier_wifi:I = 0x7f120a9c

.field public static final data_connection_cdma:I = 0x7f120a9d

.field public static final data_connection_edge:I = 0x7f120a9e

.field public static final data_connection_gprs:I = 0x7f120a9f

.field public static final data_connection_lte:I = 0x7f120aa0

.field public static final data_connection_lte_plus:I = 0x7f120aa1

.field public static final data_during_calls_summary:I = 0x7f120aa2

.field public static final data_during_calls_title:I = 0x7f120aa3

.field public static final data_limit:I = 0x7f120aa4

.field public static final data_overusage:I = 0x7f120aa5

.field public static final data_preference:I = 0x7f120aa6

.field public static final data_remaining:I = 0x7f120aa7

.field public static final data_roaming_enable_mobile:I = 0x7f120aa8

.field public static final data_saver_off:I = 0x7f120aa9

.field public static final data_saver_on:I = 0x7f120aaa

.field public static final data_saver_switch_title:I = 0x7f120aab

.field public static final data_saver_title:I = 0x7f120aac

.field public static final data_size_label:I = 0x7f120aad

.field public static final data_summary_format:I = 0x7f120aae

.field public static final data_switch_started:I = 0x7f120aaf

.field public static final data_usage_accounting:I = 0x7f120ab0

.field public static final data_usage_app:I = 0x7f120ab1

.field public static final data_usage_app_info_label:I = 0x7f120ab2

.field public static final data_usage_app_items_header_text:I = 0x7f120ab3

.field public static final data_usage_app_restrict_background:I = 0x7f120ab4

.field public static final data_usage_app_restrict_background_summary:I = 0x7f120ab5

.field public static final data_usage_app_restrict_background_summary_disabled:I = 0x7f120ab6

.field public static final data_usage_app_restrict_dialog:I = 0x7f120ab7

.field public static final data_usage_app_restrict_dialog_title:I = 0x7f120ab8

.field public static final data_usage_app_restricted:I = 0x7f120ab9

.field public static final data_usage_app_settings:I = 0x7f120aba

.field public static final data_usage_app_summary_title:I = 0x7f120abb

.field public static final data_usage_auto_sync_off_dialog:I = 0x7f120abc

.field public static final data_usage_auto_sync_off_dialog_title:I = 0x7f120abd

.field public static final data_usage_auto_sync_on_dialog:I = 0x7f120abe

.field public static final data_usage_auto_sync_on_dialog_title:I = 0x7f120abf

.field public static final data_usage_background_label:I = 0x7f120ac0

.field public static final data_usage_cellular_data:I = 0x7f120ac1

.field public static final data_usage_cellular_data_summary:I = 0x7f120ac2

.field public static final data_usage_change_cycle:I = 0x7f120ac3

.field public static final data_usage_chart_brief_content_description:I = 0x7f120ac4

.field public static final data_usage_chart_no_data_content_description:I = 0x7f120ac5

.field public static final data_usage_cycle:I = 0x7f120ac6

.field public static final data_usage_cycle_editor_positive:I = 0x7f120ac7

.field public static final data_usage_cycle_editor_subtitle:I = 0x7f120ac8

.field public static final data_usage_cycle_editor_title:I = 0x7f120ac9

.field public static final data_usage_data_limit:I = 0x7f120aca

.field public static final data_usage_disable_3g_limit:I = 0x7f120acb

.field public static final data_usage_disable_4g_limit:I = 0x7f120acc

.field public static final data_usage_disable_mobile:I = 0x7f120acd

.field public static final data_usage_disable_mobile_limit:I = 0x7f120ace

.field public static final data_usage_disable_wifi_limit:I = 0x7f120acf

.field public static final data_usage_disclaimer:I = 0x7f120ad0

.field public static final data_usage_empty:I = 0x7f120ad1

.field public static final data_usage_enable_3g:I = 0x7f120ad2

.field public static final data_usage_enable_4g:I = 0x7f120ad3

.field public static final data_usage_enable_mobile:I = 0x7f120ad4

.field public static final data_usage_forground_label:I = 0x7f120ad5

.field public static final data_usage_label_background:I = 0x7f120ad6

.field public static final data_usage_label_foreground:I = 0x7f120ad7

.field public static final data_usage_limit_dialog_mobile:I = 0x7f120ad8

.field public static final data_usage_limit_dialog_title:I = 0x7f120ad9

.field public static final data_usage_limit_editor_title:I = 0x7f120ada

.field public static final data_usage_list_mobile:I = 0x7f120adb

.field public static final data_usage_list_none:I = 0x7f120adc

.field public static final data_usage_menu_allow_background:I = 0x7f120add

.field public static final data_usage_menu_auto_sync:I = 0x7f120ade

.field public static final data_usage_menu_hide_ethernet:I = 0x7f120adf

.field public static final data_usage_menu_hide_wifi:I = 0x7f120ae0

.field public static final data_usage_menu_metered:I = 0x7f120ae1

.field public static final data_usage_menu_restrict_background:I = 0x7f120ae2

.field public static final data_usage_menu_roaming:I = 0x7f120ae3

.field public static final data_usage_menu_show_ethernet:I = 0x7f120ae4

.field public static final data_usage_menu_show_wifi:I = 0x7f120ae5

.field public static final data_usage_menu_sim_cards:I = 0x7f120ae6

.field public static final data_usage_menu_split_4g:I = 0x7f120ae7

.field public static final data_usage_metered_auto:I = 0x7f120ae8

.field public static final data_usage_metered_body:I = 0x7f120ae9

.field public static final data_usage_metered_mobile:I = 0x7f120aea

.field public static final data_usage_metered_no:I = 0x7f120aeb

.field public static final data_usage_metered_title:I = 0x7f120aec

.field public static final data_usage_metered_wifi:I = 0x7f120aed

.field public static final data_usage_metered_wifi_disabled:I = 0x7f120aee

.field public static final data_usage_metered_yes:I = 0x7f120aef

.field public static final data_usage_ota:I = 0x7f120af0

.field public static final data_usage_other_apps:I = 0x7f120af1

.field public static final data_usage_pick_cycle_day:I = 0x7f120af2

.field public static final data_usage_received_sent:I = 0x7f120af3

.field public static final data_usage_restrict_background:I = 0x7f120af4

.field public static final data_usage_restrict_background_multiuser:I = 0x7f120af5

.field public static final data_usage_restrict_background_title:I = 0x7f120af6

.field public static final data_usage_restrict_denied_dialog:I = 0x7f120af7

.field public static final data_usage_summary_format:I = 0x7f120af8

.field public static final data_usage_summary_title:I = 0x7f120af9

.field public static final data_usage_sweep_limit:I = 0x7f120afa

.field public static final data_usage_sweep_warning:I = 0x7f120afb

.field public static final data_usage_tab_3g:I = 0x7f120afc

.field public static final data_usage_tab_4g:I = 0x7f120afd

.field public static final data_usage_tab_ethernet:I = 0x7f120afe

.field public static final data_usage_tab_mobile:I = 0x7f120aff

.field public static final data_usage_tab_wifi:I = 0x7f120b00

.field public static final data_usage_template:I = 0x7f120b01

.field public static final data_usage_title:I = 0x7f120b02

.field public static final data_usage_total_during_range:I = 0x7f120b03

.field public static final data_usage_total_during_range_mobile:I = 0x7f120b04

.field public static final data_usage_uninstalled_apps:I = 0x7f120b05

.field public static final data_usage_uninstalled_apps_users:I = 0x7f120b06

.field public static final data_usage_warning_editor_title:I = 0x7f120b07

.field public static final data_usage_wifi_format:I = 0x7f120b08

.field public static final data_usage_wifi_title:I = 0x7f120b09

.field public static final data_used:I = 0x7f120b0a

.field public static final data_used_formatted:I = 0x7f120b0b

.field public static final data_used_template:I = 0x7f120b0c

.field public static final data_warning:I = 0x7f120b0d

.field public static final data_warning_footnote:I = 0x7f120b0e

.field public static final datasize_decrease_content:I = 0x7f120b0f

.field public static final datasize_decrease_title:I = 0x7f120b10

.field public static final date_and_time:I = 0x7f120b11

.field public static final date_and_time_settings_summary:I = 0x7f120b12

.field public static final date_and_time_settings_title:I = 0x7f120b13

.field public static final date_and_time_settings_title_setup_wizard:I = 0x7f120b14

.field public static final date_picker_label_day:I = 0x7f120b15

.field public static final date_picker_label_month:I = 0x7f120b16

.field public static final date_picker_label_year:I = 0x7f120b17

.field public static final date_picker_lunar:I = 0x7f120b18

.field public static final date_picker_title:I = 0x7f120b19

.field public static final date_time_24hour:I = 0x7f120b1a

.field public static final date_time_24hour_auto:I = 0x7f120b1b

.field public static final date_time_24hour_title:I = 0x7f120b1c

.field public static final date_time_auto:I = 0x7f120b1d

.field public static final date_time_picker_dialog_title:I = 0x7f120b1e

.field public static final date_time_search_region:I = 0x7f120b1f

.field public static final date_time_select_fixed_offset_time_zones:I = 0x7f120b20

.field public static final date_time_select_region:I = 0x7f120b21

.field public static final date_time_set_date_title:I = 0x7f120b22

.field public static final date_time_set_time_title:I = 0x7f120b23

.field public static final date_time_set_timezone:I = 0x7f120b24

.field public static final date_time_set_timezone_title:I = 0x7f120b25

.field public static final date_time_settings:I = 0x7f120b26

.field public static final day_of_week:I = 0x7f120b27

.field public static final dc_light_dialog_summary:I = 0x7f120b28

.field public static final dc_light_summary:I = 0x7f120b29

.field public static final dc_light_summary_new:I = 0x7f120b2a

.field public static final dc_light_title:I = 0x7f120b2b

.field public static final dclight_unavailable_prompt:I = 0x7f120b2c

.field public static final dds_preference_smart_dds_switch_is_on:I = 0x7f120b2d

.field public static final debug_app:I = 0x7f120b2e

.field public static final debug_app_not_set:I = 0x7f120b2f

.field public static final debug_app_set:I = 0x7f120b30

.field public static final debug_applications_category:I = 0x7f120b31

.field public static final debug_autofill_category:I = 0x7f120b32

.field public static final debug_debugging_category:I = 0x7f120b33

.field public static final debug_drawing_category:I = 0x7f120b34

.field public static final debug_hw_drawing_category:I = 0x7f120b35

.field public static final debug_hw_overdraw:I = 0x7f120b36

.field public static final debug_input_category:I = 0x7f120b37

.field public static final debug_intent_sender_label:I = 0x7f120b38

.field public static final debug_layout:I = 0x7f120b39

.field public static final debug_layout_summary:I = 0x7f120b3a

.field public static final debug_monitoring_category:I = 0x7f120b3b

.field public static final debug_networking_category:I = 0x7f120b3c

.field public static final debug_view_attributes:I = 0x7f120b3d

.field public static final decline_remote_bugreport_action:I = 0x7f120b3e

.field public static final defalut_launcher_title:I = 0x7f120b3f

.field public static final default_active_sim_calls:I = 0x7f120b40

.field public static final default_active_sim_mobile_data:I = 0x7f120b41

.field public static final default_active_sim_sms:I = 0x7f120b42

.field public static final default_admin_support_msg:I = 0x7f120b43

.field public static final default_app:I = 0x7f120b44

.field public static final default_apps_title:I = 0x7f120b45

.field public static final default_assist_title:I = 0x7f120b46

.field public static final default_browser_title:I = 0x7f120b47

.field public static final default_browser_title_none:I = 0x7f120b48

.field public static final default_calendar_app_title:I = 0x7f120b49

.field public static final default_contacts_app_title:I = 0x7f120b4a

.field public static final default_digital_assistant_title:I = 0x7f120b4b

.field public static final default_emergency_app:I = 0x7f120b4c

.field public static final default_for_calls:I = 0x7f120b4d

.field public static final default_for_calls_and_sms:I = 0x7f120b4e

.field public static final default_for_mobile_data:I = 0x7f120b4f

.field public static final default_for_sms:I = 0x7f120b50

.field public static final default_for_work:I = 0x7f120b51

.field public static final default_input_method:I = 0x7f120b52

.field public static final default_keyboard:I = 0x7f120b53

.field public static final default_keyboard_layout:I = 0x7f120b54

.field public static final default_launcher_set_jeejen_message:I = 0x7f120b55

.field public static final default_launcher_set_system_message:I = 0x7f120b56

.field public static final default_launcher_title:I = 0x7f120b57

.field public static final default_map_app_title:I = 0x7f120b58

.field public static final default_notification_assistant:I = 0x7f120b59

.field public static final default_phone_title:I = 0x7f120b5a

.field public static final default_print_service_main_switch_title:I = 0x7f120b5b

.field public static final default_rule_name:I = 0x7f120b5c

.field public static final default_see_all_apps_title:I = 0x7f120b5d

.field public static final default_sound:I = 0x7f120b5e

.field public static final default_spell_checker:I = 0x7f120b5f

.field public static final default_suppression_title:I = 0x7f120b60

.field public static final default_theme:I = 0x7f120b61

.field public static final default_usb_mode:I = 0x7f120b62

.field public static final default_user_icon_description:I = 0x7f120b63

.field public static final deferred_setup_hintViewTitle:I = 0x7f120b64

.field public static final delete:I = 0x7f120b65

.field public static final delete_blob_confirmation_text:I = 0x7f120b66

.field public static final delete_blob_text:I = 0x7f120b67

.field public static final delete_fingerprint_confirm_msg:I = 0x7f120b68

.field public static final delete_fingerprint_confirm_title:I = 0x7f120b69

.field public static final delete_fingerprint_text:I = 0x7f120b6a

.field public static final delete_legacy_fingerprint:I = 0x7f120b6b

.field public static final delete_or_keep_legacy_passwords_confirm_msg:I = 0x7f120b6c

.field public static final delete_rule:I = 0x7f120b6d

.field public static final delete_saved_network:I = 0x7f120b6e

.field public static final delete_sound_effect:I = 0x7f120b6f

.field public static final deleted_apps:I = 0x7f120b70

.field public static final deletion_helper_automatic_title:I = 0x7f120b71

.field public static final deletion_helper_manual_title:I = 0x7f120b72

.field public static final deletion_helper_preference_title:I = 0x7f120b73

.field public static final demo_mode:I = 0x7f120b74

.field public static final demo_mode_summary:I = 0x7f120b75

.field public static final demote_conversation_summary:I = 0x7f120b76

.field public static final demote_conversation_title:I = 0x7f120b77

.field public static final density_pixel_summary:I = 0x7f120b78

.field public static final deny:I = 0x7f120b79

.field public static final desc_app_locale_disclaimer:I = 0x7f120b7a

.field public static final desc_app_locale_selection_supported:I = 0x7f120b7b

.field public static final desc_no_available_supported_locale:I = 0x7f120b7c

.field public static final detail_app_button:I = 0x7f120b7d

.field public static final detail_question:I = 0x7f120b7e

.field public static final detailed_wifi_logging_disabled:I = 0x7f120b7f

.field public static final detailed_wifi_logging_enabled:I = 0x7f120b80

.field public static final details_subtitle:I = 0x7f120b81

.field public static final dev_logpersist_clear_warning_message:I = 0x7f120b82

.field public static final dev_logpersist_clear_warning_title:I = 0x7f120b83

.field public static final dev_settings_disabled_warning:I = 0x7f120b84

.field public static final dev_settings_warning_message:I = 0x7f120b85

.field public static final dev_settings_warning_title:I = 0x7f120b86

.field public static final developer_build:I = 0x7f120b87

.field public static final developer_options_main_switch_title:I = 0x7f120b88

.field public static final developer_smallest_width:I = 0x7f120b89

.field public static final development_enable:I = 0x7f120b8a

.field public static final development_settings_enable:I = 0x7f120b8b

.field public static final development_settings_not_available:I = 0x7f120b8c

.field public static final development_settings_summary:I = 0x7f120b8d

.field public static final development_settings_title:I = 0x7f120b8e

.field public static final device_actionbar_title:I = 0x7f120b8f

.field public static final device_activation_info:I = 0x7f120b90

.field public static final device_admin_add_title:I = 0x7f120b91

.field public static final device_admin_settings_title:I = 0x7f120b92

.field public static final device_admin_status:I = 0x7f120b93

.field public static final device_admin_warning:I = 0x7f120b94

.field public static final device_admin_warning_simplified:I = 0x7f120b95

.field public static final device_available_memory:I = 0x7f120b96

.field public static final device_battery:I = 0x7f120b97

.field public static final device_build_id:I = 0x7f120b98

.field public static final device_camera:I = 0x7f120b99

.field public static final device_complete_parameters:I = 0x7f120b9a

.field public static final device_cpu:I = 0x7f120b9b

.field public static final device_cpu10_info:I = 0x7f120b9c

.field public static final device_cpu2_info:I = 0x7f120b9d

.field public static final device_cpu4_info:I = 0x7f120b9e

.field public static final device_cpu6_info:I = 0x7f120b9f

.field public static final device_cpu8_info:I = 0x7f120ba0

.field public static final device_details_title:I = 0x7f120ba1

.field public static final device_display_layout_title:I = 0x7f120ba2

.field public static final device_edit_title:I = 0x7f120ba3

.field public static final device_feedback:I = 0x7f120ba4

.field public static final device_id:I = 0x7f120ba5

.field public static final device_ime_keyboard_fold:I = 0x7f120ba6

.field public static final device_ime_keyboard_pad:I = 0x7f120ba7

.field public static final device_info_default:I = 0x7f120ba8

.field public static final device_info_not_available:I = 0x7f120ba9

.field public static final device_internal_memory:I = 0x7f120baa

.field public static final device_mall_card_title:I = 0x7f120bab

.field public static final device_memory:I = 0x7f120bac

.field public static final device_miui_more:I = 0x7f120bad

.field public static final device_miui_version:I = 0x7f120bae

.field public static final device_miui_version_for_POCO:I = 0x7f120baf

.field public static final device_miui_version_parameters:I = 0x7f120bb0

.field public static final device_miui_version_update:I = 0x7f120bb1

.field public static final device_name:I = 0x7f120bb2

.field public static final device_name_input_error:I = 0x7f120bb3

.field public static final device_name_input_hint:I = 0x7f120bb4

.field public static final device_name_input_overlength:I = 0x7f120bb5

.field public static final device_name_usage_point:I = 0x7f120bb6

.field public static final device_oaid:I = 0x7f120bb7

.field public static final device_oaid_desc:I = 0x7f120bb8

.field public static final device_oaid_title:I = 0x7f120bb9

.field public static final device_of_someone:I = 0x7f120bba

.field public static final device_opcust_version:I = 0x7f120bbb

.field public static final device_picker:I = 0x7f120bbc

.field public static final device_rear_camera:I = 0x7f120bbd

.field public static final device_screen_resolution:I = 0x7f120bbe

.field public static final device_screen_size:I = 0x7f120bbf

.field public static final device_screen_usage:I = 0x7f120bc0

.field public static final device_sd_memory:I = 0x7f120bc1

.field public static final device_share_loading:I = 0x7f120bc2

.field public static final device_status:I = 0x7f120bc3

.field public static final device_status_activity_title:I = 0x7f120bc4

.field public static final device_status_summary:I = 0x7f120bc5

.field public static final device_theme:I = 0x7f120bc6

.field public static final device_total_memory:I = 0x7f120bc7

.field public static final device_usage_list_summary:I = 0x7f120bc8

.field public static final devices_title:I = 0x7f120bc9

.field public static final diagnosis_log_send_failed:I = 0x7f120bca

.field public static final diagnosis_log_sent_format:I = 0x7f120bcb

.field public static final dial_application_title:I = 0x7f120bcc

.field public static final dial_pad_tones_title:I = 0x7f120bcd

.field public static final dialog_background_check_message:I = 0x7f120bce

.field public static final dialog_background_check_ok:I = 0x7f120bcf

.field public static final dialog_background_check_title:I = 0x7f120bd0

.field public static final dialog_backup_title:I = 0x7f120bd1

.field public static final dialog_check_button_negative:I = 0x7f120bd2

.field public static final dialog_check_button_positive:I = 0x7f120bd3

.field public static final dialog_check_context:I = 0x7f120bd4

.field public static final dialog_check_title:I = 0x7f120bd5

.field public static final dialog_chose_item:I = 0x7f120bd6

.field public static final dialog_content_login:I = 0x7f120bd7

.field public static final dialog_esim_button_negative:I = 0x7f120bd8

.field public static final dialog_esim_button_positive:I = 0x7f120bd9

.field public static final dialog_location_message:I = 0x7f120bda

.field public static final dialog_location_ok:I = 0x7f120bdb

.field public static final dialog_location_title:I = 0x7f120bdc

.field public static final dialog_network_priority_title:I = 0x7f120bdd

.field public static final dialog_no:I = 0x7f120bde

.field public static final dialog_restrict_all_mobile_summary:I = 0x7f120bdf

.field public static final dialog_restrict_all_mobile_title:I = 0x7f120be0

.field public static final dialog_restrict_all_wlan_summary:I = 0x7f120be1

.field public static final dialog_restrict_all_wlan_title:I = 0x7f120be2

.field public static final dialog_restrict_negative:I = 0x7f120be3

.field public static final dialog_restrict_positive:I = 0x7f120be4

.field public static final dialog_softbank_esim_wipe_cancel:I = 0x7f120be5

.field public static final dialog_softbank_esim_wipe_info:I = 0x7f120be6

.field public static final dialog_softbank_esim_wipe_ok:I = 0x7f120be7

.field public static final dialog_softbank_esim_wipe_title:I = 0x7f120be8

.field public static final dialog_softbank_esime_context:I = 0x7f120be9

.field public static final dialog_softbank_esime_title:I = 0x7f120bea

.field public static final dialog_speaker_auto_clean_button:I = 0x7f120beb

.field public static final dialog_speaker_auto_clean_content:I = 0x7f120bec

.field public static final dialog_speaker_auto_clean_content_new:I = 0x7f120bed

.field public static final dialog_speaker_auto_clean_title:I = 0x7f120bee

.field public static final dialog_speaker_auto_clean_title_new:I = 0x7f120bef

.field public static final dialog_stop_message:I = 0x7f120bf0

.field public static final dialog_stop_message_wakeup_alarm:I = 0x7f120bf1

.field public static final dialog_stop_ok:I = 0x7f120bf2

.field public static final dialog_stop_title:I = 0x7f120bf3

.field public static final dialog_title_color_calibration:I = 0x7f120bf4

.field public static final dialog_title_hdmi_color_calibration:I = 0x7f120bf5

.field public static final dialog_title_login:I = 0x7f120bf6

.field public static final dialog_wifi_button_negative:I = 0x7f120bf7

.field public static final dialog_wifi_button_positive:I = 0x7f120bf8

.field public static final dialog_wifi_context:I = 0x7f120bf9

.field public static final dialog_wifi_title:I = 0x7f120bfa

.field public static final dialog_yes:I = 0x7f120bfb

.field public static final digital_clock_dot:I = 0x7f120bfc

.field public static final direct_boot_unaware_dialog_message:I = 0x7f120bfd

.field public static final disableNetwork:I = 0x7f120bfe

.field public static final disable_overlays:I = 0x7f120bff

.field public static final disable_overlays_summary:I = 0x7f120c00

.field public static final disable_power_key_long_press_under_keyguard:I = 0x7f120c01

.field public static final disable_service_message:I = 0x7f120c02

.field public static final disable_service_title:I = 0x7f120c03

.field public static final disable_text:I = 0x7f120c04

.field public static final disable_wifi_hotspot_title:I = 0x7f120c05

.field public static final disable_wifi_hotspot_when_bluetooth_and_ethernet_on:I = 0x7f120c06

.field public static final disable_wifi_hotspot_when_bluetooth_on:I = 0x7f120c07

.field public static final disable_wifi_hotspot_when_ethernet_on:I = 0x7f120c08

.field public static final disable_wifi_hotspot_when_usb_and_bluetooth_and_ethernet_on:I = 0x7f120c09

.field public static final disable_wifi_hotspot_when_usb_and_bluetooth_on:I = 0x7f120c0a

.field public static final disable_wifi_hotspot_when_usb_and_ethernet_on:I = 0x7f120c0b

.field public static final disable_wifi_hotspot_when_usb_on:I = 0x7f120c0c

.field public static final disabled:I = 0x7f120c0d

.field public static final disabled_authorised_apps_title:I = 0x7f120c0e

.field public static final disabled_because_no_backup_security:I = 0x7f120c0f

.field public static final disabled_by_admin:I = 0x7f120c10

.field public static final disabled_by_admin_summary_text:I = 0x7f120c11

.field public static final disabled_by_administrator_summary:I = 0x7f120c12

.field public static final disabled_by_app_ops_text:I = 0x7f120c13

.field public static final disabled_by_policy_content_biometric_parental_consent:I = 0x7f120c14

.field public static final disabled_by_policy_title:I = 0x7f120c15

.field public static final disabled_by_policy_title_adjust_volume:I = 0x7f120c16

.field public static final disabled_by_policy_title_biometric_parental_consent:I = 0x7f120c17

.field public static final disabled_by_policy_title_camera:I = 0x7f120c18

.field public static final disabled_by_policy_title_financed_device:I = 0x7f120c19

.field public static final disabled_by_policy_title_outgoing_calls:I = 0x7f120c1a

.field public static final disabled_by_policy_title_screen_capture:I = 0x7f120c1b

.field public static final disabled_by_policy_title_sms:I = 0x7f120c1c

.field public static final disabled_by_policy_title_suspend_packages:I = 0x7f120c1d

.field public static final disabled_dependent_setting_summary:I = 0x7f120c1e

.field public static final disabled_feature:I = 0x7f120c1f

.field public static final disabled_feature_reason_slow_down_phone:I = 0x7f120c20

.field public static final disabled_for_user_setting_summary:I = 0x7f120c21

.field public static final disabled_low_ram_device:I = 0x7f120c22

.field public static final disallow_lock_screen_summary:I = 0x7f120c23

.field public static final disconnect:I = 0x7f120c24

.field public static final disconnect_group:I = 0x7f120c25

.field public static final disconnected:I = 0x7f120c26

.field public static final display_advanced_mode_summary:I = 0x7f120c27

.field public static final display_advanced_mode_title:I = 0x7f120c28

.field public static final display_animate_category:I = 0x7f120c29

.field public static final display_animate_content:I = 0x7f120c2a

.field public static final display_animate_title:I = 0x7f120c2b

.field public static final display_auto_rotate_rotate:I = 0x7f120c2c

.field public static final display_auto_rotate_stay_in_current:I = 0x7f120c2d

.field public static final display_auto_rotate_stay_in_landscape:I = 0x7f120c2e

.field public static final display_auto_rotate_stay_in_portrait:I = 0x7f120c2f

.field public static final display_auto_rotate_title:I = 0x7f120c30

.field public static final display_category_title:I = 0x7f120c31

.field public static final display_cutout_emulation:I = 0x7f120c32

.field public static final display_cutout_emulation_keywords:I = 0x7f120c33

.field public static final display_dashboard_summary:I = 0x7f120c34

.field public static final display_label:I = 0x7f120c35

.field public static final display_order_text:I = 0x7f120c36

.field public static final display_prompt_confirm:I = 0x7f120c37

.field public static final display_settings:I = 0x7f120c38

.field public static final display_settings_title:I = 0x7f120c39

.field public static final display_summary:I = 0x7f120c3a

.field public static final display_summary_example:I = 0x7f120c3b

.field public static final display_vr_pref_low_persistence:I = 0x7f120c3c

.field public static final display_vr_pref_off:I = 0x7f120c3d

.field public static final display_vr_pref_title:I = 0x7f120c3e

.field public static final display_white_balance_summary:I = 0x7f120c3f

.field public static final display_white_balance_title:I = 0x7f120c40

.field public static final diy_suppression_summary:I = 0x7f120c41

.field public static final diy_suppression_title:I = 0x7f120c42

.field public static final dlg_cancel:I = 0x7f120c43

.field public static final dlg_confirm_unmount_text:I = 0x7f120c44

.field public static final dlg_confirm_unmount_title:I = 0x7f120c45

.field public static final dlg_delete:I = 0x7f120c46

.field public static final dlg_error_unmount_text:I = 0x7f120c47

.field public static final dlg_error_unmount_title:I = 0x7f120c48

.field public static final dlg_ok:I = 0x7f120c49

.field public static final dlg_remove_locales_error_message:I = 0x7f120c4a

.field public static final dlg_remove_locales_error_title:I = 0x7f120c4b

.field public static final dlg_remove_locales_message:I = 0x7f120c4c

.field public static final dndm_alarm_content:I = 0x7f120c4d

.field public static final dndm_alarm_content_all:I = 0x7f120c4e

.field public static final dndm_alarm_content_call:I = 0x7f120c4f

.field public static final dndm_alarm_content_call_sms:I = 0x7f120c50

.field public static final dndm_alarm_content_event:I = 0x7f120c51

.field public static final dndm_alarm_content_event_and_call:I = 0x7f120c52

.field public static final dndm_alarm_content_event_and_sms:I = 0x7f120c53

.field public static final dndm_alarm_content_not_all:I = 0x7f120c54

.field public static final dndm_alarm_content_sms:I = 0x7f120c55

.field public static final dndm_auto_button_title:I = 0x7f120c56

.field public static final dndm_auto_setting_group:I = 0x7f120c57

.field public static final dndm_auto_time_setting:I = 0x7f120c58

.field public static final dndm_auto_time_setting_summary:I = 0x7f120c59

.field public static final dndm_auto_time_turn_off:I = 0x7f120c5a

.field public static final dndm_auto_time_turn_off_title:I = 0x7f120c5b

.field public static final dndm_auto_time_turn_on:I = 0x7f120c5c

.field public static final dndm_auto_time_turn_on_title:I = 0x7f120c5d

.field public static final dndm_dlg_remove_ok:I = 0x7f120c5e

.field public static final dndm_dlg_remove_vip:I = 0x7f120c5f

.field public static final dndm_menu_add_vip_custom:I = 0x7f120c60

.field public static final dndm_mute_all:I = 0x7f120c61

.field public static final dndm_notification:I = 0x7f120c62

.field public static final dndm_open:I = 0x7f120c63

.field public static final dndm_repeated_call_setting_title:I = 0x7f120c64

.field public static final dndm_repeated_call_summary_3min:I = 0x7f120c65

.field public static final dndm_repeated_call_summary_none:I = 0x7f120c66

.field public static final dndm_special_call_setting_group:I = 0x7f120c67

.field public static final dndm_summary:I = 0x7f120c68

.field public static final dndm_summary_for_pad:I = 0x7f120c69

.field public static final dndm_twelve_hour_time_format:I = 0x7f120c6a

.field public static final dndm_twenty_four_hour_time_format:I = 0x7f120c6b

.field public static final dndm_vip_call_button_title:I = 0x7f120c6c

.field public static final dndm_vip_call_setting_detal:I = 0x7f120c6d

.field public static final dndm_vip_call_setting_title:I = 0x7f120c6e

.field public static final dndm_vip_call_summary_contact:I = 0x7f120c6f

.field public static final dndm_vip_call_summary_custom:I = 0x7f120c70

.field public static final dndm_vip_call_summary_none:I = 0x7f120c71

.field public static final dndm_vip_list_all:I = 0x7f120c72

.field public static final dndm_vip_list_contact:I = 0x7f120c73

.field public static final dndm_vip_list_custom:I = 0x7f120c74

.field public static final dndm_vip_list_custom_group:I = 0x7f120c75

.field public static final dndm_vip_list_group:I = 0x7f120c76

.field public static final dndm_vip_list_starred_contact:I = 0x7f120c77

.field public static final dndm_wristband:I = 0x7f120c78

.field public static final dndm_wristband_detailed:I = 0x7f120c79

.field public static final dns_select_dialog_title:I = 0x7f120c7a

.field public static final do_disclosure_generic:I = 0x7f120c7b

.field public static final do_disclosure_learn_more_separator:I = 0x7f120c7c

.field public static final do_disclosure_with_name:I = 0x7f120c7d

.field public static final do_not_disturb_main_switch_title_off:I = 0x7f120c7e

.field public static final do_not_disturb_main_switch_title_on:I = 0x7f120c7f

.field public static final do_not_disturb_mode:I = 0x7f120c80

.field public static final dock_audio_media_disabled:I = 0x7f120c81

.field public static final dock_audio_media_enabled:I = 0x7f120c82

.field public static final dock_audio_media_title:I = 0x7f120c83

.field public static final dock_audio_settings_title:I = 0x7f120c84

.field public static final dock_audio_summary_car:I = 0x7f120c85

.field public static final dock_audio_summary_desk:I = 0x7f120c86

.field public static final dock_audio_summary_none:I = 0x7f120c87

.field public static final dock_audio_summary_unknown:I = 0x7f120c88

.field public static final dock_not_found_text:I = 0x7f120c89

.field public static final dock_not_found_title:I = 0x7f120c8a

.field public static final dock_settings:I = 0x7f120c8b

.field public static final dock_settings_summary:I = 0x7f120c8c

.field public static final dock_settings_title:I = 0x7f120c8d

.field public static final dock_sounds_enable_summary_off:I = 0x7f120c8e

.field public static final dock_sounds_enable_summary_on:I = 0x7f120c8f

.field public static final dock_sounds_enable_title:I = 0x7f120c90

.field public static final docking_sounds_title:I = 0x7f120c91

.field public static final does_not_exist:I = 0x7f120c92

.field public static final domain_url_section_title:I = 0x7f120c93

.field public static final domain_urls_apps_summary_off:I = 0x7f120c94

.field public static final domain_urls_summary_none:I = 0x7f120c95

.field public static final domain_urls_summary_one:I = 0x7f120c96

.field public static final domain_urls_summary_some:I = 0x7f120c97

.field public static final domain_urls_title:I = 0x7f120c98

.field public static final done:I = 0x7f120c99

.field public static final done_button:I = 0x7f120c9a

.field public static final double_click_power_key:I = 0x7f120c9b

.field public static final double_click_volume_down_when_lock:I = 0x7f120c9c

.field public static final double_connect:I = 0x7f120c9d

.field public static final double_connect_summary:I = 0x7f120c9e

.field public static final double_g_v:I = 0x7f120c9f

.field public static final double_g_v_sumary:I = 0x7f120ca0

.field public static final double_knock:I = 0x7f120ca1

.field public static final double_tap_power_for_camera_suggestion_title:I = 0x7f120ca2

.field public static final double_tap_power_for_camera_summary:I = 0x7f120ca3

.field public static final double_tap_power_for_camera_title:I = 0x7f120ca4

.field public static final double_twist_for_camera_mode_summary:I = 0x7f120ca5

.field public static final double_twist_for_camera_mode_title:I = 0x7f120ca6

.field public static final double_twist_for_camera_suggestion_title:I = 0x7f120ca7

.field public static final download_current_rom:I = 0x7f120ca8

.field public static final downloaded_sim_category_title:I = 0x7f120ca9

.field public static final downloaded_sims_category_title:I = 0x7f120caa

.field public static final downloading_description:I = 0x7f120cab

.field public static final doze_always_on_summary:I = 0x7f120cac

.field public static final doze_always_on_title:I = 0x7f120cad

.field public static final doze_summary:I = 0x7f120cae

.field public static final doze_title:I = 0x7f120caf

.field public static final dpp_before_share_summary:I = 0x7f120cb0

.field public static final dpp_cannot_connect_target:I = 0x7f120cb1

.field public static final dpp_connect_success:I = 0x7f120cb2

.field public static final dpp_not_support:I = 0x7f120cb3

.field public static final dpp_qr_code_summary:I = 0x7f120cb4

.field public static final dpp_qrcode_generator_summary:I = 0x7f120cb5

.field public static final dpp_scanner_summary:I = 0x7f120cb6

.field public static final dpp_shared_fail:I = 0x7f120cb7

.field public static final dpp_shared_success:I = 0x7f120cb8

.field public static final dpp_sharing_wifi_summary:I = 0x7f120cb9

.field public static final dpp_theme_title:I = 0x7f120cba

.field public static final draw_overlay:I = 0x7f120cbb

.field public static final dream_complication_title_aqi:I = 0x7f120cbc

.field public static final dream_complication_title_cast_info:I = 0x7f120cbd

.field public static final dream_complication_title_date:I = 0x7f120cbe

.field public static final dream_complication_title_time:I = 0x7f120cbf

.field public static final dream_complication_title_weather:I = 0x7f120cc0

.field public static final dream_complications_toggle_summary:I = 0x7f120cc1

.field public static final dream_complications_toggle_title:I = 0x7f120cc2

.field public static final dream_more_settings_category:I = 0x7f120cc3

.field public static final dream_picker_category:I = 0x7f120cc4

.field public static final dream_preview_button_title:I = 0x7f120cc5

.field public static final dream_setup_description:I = 0x7f120cc6

.field public static final dream_setup_title:I = 0x7f120cc7

.field public static final dsda_switch_summary:I = 0x7f120cc8

.field public static final dsda_switch_title:I = 0x7f120cc9

.field public static final dsds_activation_failure_body_msg1:I = 0x7f120cca

.field public static final dsds_activation_failure_body_msg2:I = 0x7f120ccb

.field public static final dsds_activation_failure_title:I = 0x7f120ccc

.field public static final dsds_notification_after_suw_text:I = 0x7f120ccd

.field public static final dsds_notification_after_suw_title:I = 0x7f120cce

.field public static final dsu_is_running:I = 0x7f120ccf

.field public static final dsu_loader_description:I = 0x7f120cd0

.field public static final dsu_loader_loading:I = 0x7f120cd1

.field public static final dsu_loader_title:I = 0x7f120cd2

.field public static final dtmf_tone_enable_title:I = 0x7f120cd3

.field public static final dual_cdma_sim_warning_notification_channel_title:I = 0x7f120cd4

.field public static final dual_cdma_sim_warning_notification_summary:I = 0x7f120cd5

.field public static final dual_clock:I = 0x7f120cd6

.field public static final dual_clock_aod_open_message:I = 0x7f120cd7

.field public static final dual_clock_hint:I = 0x7f120cd8

.field public static final dual_clock_ok:I = 0x7f120cd9

.field public static final dual_clock_open_message:I = 0x7f120cda

.field public static final dual_setting_simcard1:I = 0x7f120cdb

.field public static final dual_setting_simcard2:I = 0x7f120cdc

.field public static final dual_wifi:I = 0x7f120cdd

.field public static final dual_wifi_acceleration:I = 0x7f120cde

.field public static final dual_wifi_auto_disable:I = 0x7f120cdf

.field public static final dual_wifi_auto_disable_summary:I = 0x7f120ce0

.field public static final dual_wifi_avaliable_slave_wifi:I = 0x7f120ce1

.field public static final dual_wifi_avaliable_slave_wifi_24G:I = 0x7f120ce2

.field public static final dual_wifi_avaliable_slave_wifi_5G:I = 0x7f120ce3

.field public static final dual_wifi_enabler_summary:I = 0x7f120ce4

.field public static final dual_wifi_off:I = 0x7f120ce5

.field public static final dual_wifi_on:I = 0x7f120ce6

.field public static final dual_wifi_primary_wifi_connected:I = 0x7f120ce7

.field public static final dual_wifi_slave_wifi_connected:I = 0x7f120ce8

.field public static final dual_wifi_summary:I = 0x7f120ce9

.field public static final dual_wifi_switching_not_remind:I = 0x7f120cea

.field public static final dual_wifi_switching_prompt:I = 0x7f120ceb

.field public static final dual_wifi_switching_summary:I = 0x7f120cec

.field public static final dump_log:I = 0x7f120ced

.field public static final dump_log_summary:I = 0x7f120cee

.field public static final dump_log_title:I = 0x7f120cef

.field public static final early_morning:I = 0x7f120cf0

.field public static final earthly_branches_chen:I = 0x7f120cf1

.field public static final earthly_branches_chou:I = 0x7f120cf2

.field public static final earthly_branches_hai:I = 0x7f120cf3

.field public static final earthly_branches_mao:I = 0x7f120cf4

.field public static final earthly_branches_shen:I = 0x7f120cf5

.field public static final earthly_branches_si:I = 0x7f120cf6

.field public static final earthly_branches_wei:I = 0x7f120cf7

.field public static final earthly_branches_wu:I = 0x7f120cf8

.field public static final earthly_branches_xu:I = 0x7f120cf9

.field public static final earthly_branches_yin:I = 0x7f120cfa

.field public static final earthly_branches_you:I = 0x7f120cfb

.field public static final earthly_branches_zi:I = 0x7f120cfc

.field public static final easymode_hint:I = 0x7f120cfd

.field public static final edge_mode_back:I = 0x7f120cfe

.field public static final edge_mode_back_summary:I = 0x7f120cff

.field public static final edge_mode_clean:I = 0x7f120d00

.field public static final edge_mode_clean_summary:I = 0x7f120d01

.field public static final edge_mode_guide_title:I = 0x7f120d02

.field public static final edge_mode_notification_summary:I = 0x7f120d03

.field public static final edge_mode_notification_title:I = 0x7f120d04

.field public static final edge_mode_photo:I = 0x7f120d05

.field public static final edge_mode_photo_summary:I = 0x7f120d06

.field public static final edge_mode_screenshot:I = 0x7f120d07

.field public static final edge_mode_screenshot_summary:I = 0x7f120d08

.field public static final edge_mode_state_summary:I = 0x7f120d09

.field public static final edge_mode_state_title:I = 0x7f120d0a

.field public static final edge_to_edge_navigation_summary:I = 0x7f120d0b

.field public static final edge_to_edge_navigation_title:I = 0x7f120d0c

.field public static final edit_rule:I = 0x7f120d0d

.field public static final edit_rules:I = 0x7f120d0e

.field public static final effect_preview:I = 0x7f120d0f

.field public static final eighthours:I = 0x7f120d10

.field public static final elastic_text:I = 0x7f120d11

.field public static final emergency_address_summary:I = 0x7f120d12

.field public static final emergency_address_title:I = 0x7f120d13

.field public static final emergency_dashboard_summary:I = 0x7f120d14

.field public static final emergency_gesture_call_for_help_dialog_title:I = 0x7f120d15

.field public static final emergency_gesture_call_for_help_summary:I = 0x7f120d16

.field public static final emergency_gesture_call_for_help_title:I = 0x7f120d17

.field public static final emergency_gesture_category_call_for_help_title:I = 0x7f120d18

.field public static final emergency_gesture_entrypoint_summary:I = 0x7f120d19

.field public static final emergency_gesture_number_override_notes:I = 0x7f120d1a

.field public static final emergency_gesture_screen_summary:I = 0x7f120d1b

.field public static final emergency_gesture_screen_title:I = 0x7f120d1c

.field public static final emergency_gesture_settings_package:I = 0x7f120d1d

.field public static final emergency_gesture_sound_setting_summary:I = 0x7f120d1e

.field public static final emergency_gesture_sound_setting_title:I = 0x7f120d1f

.field public static final emergency_gesture_switchbar_title:I = 0x7f120d20

.field public static final emergency_info_summary:I = 0x7f120d21

.field public static final emergency_info_title:I = 0x7f120d22

.field public static final emergency_settings_preference_title:I = 0x7f120d23

.field public static final emergency_sos_title:I = 0x7f120d24

.field public static final emergency_tone_alert:I = 0x7f120d25

.field public static final emergency_tone_silent:I = 0x7f120d26

.field public static final emergency_tone_summary:I = 0x7f120d27

.field public static final emergency_tone_title:I = 0x7f120d28

.field public static final emergency_tone_vibrate:I = 0x7f120d29

.field public static final empty:I = 0x7f120d2a

.field public static final empty_networks_list:I = 0x7f120d2b

.field public static final empty_title:I = 0x7f120d2c

.field public static final emulate_sd_data:I = 0x7f120d2d

.field public static final enableNetwork:I = 0x7f120d2e

.field public static final enable_2g_summary:I = 0x7f120d2f

.field public static final enable_2g_summary_disabled_carrier:I = 0x7f120d30

.field public static final enable_2g_title:I = 0x7f120d31

.field public static final enable_adb:I = 0x7f120d32

.field public static final enable_adb_summary:I = 0x7f120d33

.field public static final enable_adb_wireless:I = 0x7f120d34

.field public static final enable_adb_wireless_summary:I = 0x7f120d35

.field public static final enable_compatibility:I = 0x7f120d36

.field public static final enable_freeform_support:I = 0x7f120d37

.field public static final enable_freeform_support_summary:I = 0x7f120d38

.field public static final enable_gnss_raw_meas_full_tracking:I = 0x7f120d39

.field public static final enable_gnss_raw_meas_full_tracking_summary:I = 0x7f120d3a

.field public static final enable_gpu_debug_layers:I = 0x7f120d3b

.field public static final enable_gpu_debug_layers_summary:I = 0x7f120d3c

.field public static final enable_local_backup:I = 0x7f120d3d

.field public static final enable_local_backup_cancel:I = 0x7f120d3e

.field public static final enable_local_backup_message:I = 0x7f120d3f

.field public static final enable_local_backup_message_intledition:I = 0x7f120d40

.field public static final enable_local_backup_ok:I = 0x7f120d41

.field public static final enable_local_backup_title:I = 0x7f120d42

.field public static final enable_mms_notification_channel_title:I = 0x7f120d43

.field public static final enable_mms_notification_summary:I = 0x7f120d44

.field public static final enable_mobile_data_when_opening_hotspot:I = 0x7f120d45

.field public static final enable_non_resizable_multi_window:I = 0x7f120d46

.field public static final enable_non_resizable_multi_window_summary:I = 0x7f120d47

.field public static final enable_opengl_traces_title:I = 0x7f120d48

.field public static final enable_quick_setting:I = 0x7f120d49

.field public static final enable_receiving_mms_notification_title:I = 0x7f120d4a

.field public static final enable_screen_projection_summary:I = 0x7f120d4b

.field public static final enable_sending_mms_notification_title:I = 0x7f120d4c

.field public static final enable_service_title:I = 0x7f120d4d

.field public static final enable_sizecompat_freeform:I = 0x7f120d4e

.field public static final enable_sizecompat_freeform_summary:I = 0x7f120d4f

.field public static final enable_snapshot_screenlock:I = 0x7f120d50

.field public static final enable_snpn:I = 0x7f120d51

.field public static final enable_terminal_summary:I = 0x7f120d52

.field public static final enable_terminal_title:I = 0x7f120d53

.field public static final enable_text:I = 0x7f120d54

.field public static final enable_verbose_vendor_logging:I = 0x7f120d55

.field public static final enable_verbose_vendor_logging_summary:I = 0x7f120d56

.field public static final enable_wifi_display:I = 0x7f120d57

.field public static final enabled:I = 0x7f120d58

.field public static final enabled_5g_mode_title:I = 0x7f120d59

.field public static final enabled_authorised_apps_summary:I = 0x7f120d5a

.field public static final enabled_authorised_apps_title:I = 0x7f120d5b

.field public static final enabled_by_admin:I = 0x7f120d5c

.field public static final enalbe_identify_iPhone:I = 0x7f120d5d

.field public static final encrypt_talkback_dialog_message_password:I = 0x7f120d5e

.field public static final encrypt_talkback_dialog_message_pattern:I = 0x7f120d5f

.field public static final encrypt_talkback_dialog_message_pin:I = 0x7f120d60

.field public static final encrypt_talkback_dialog_require_password:I = 0x7f120d61

.field public static final encrypt_talkback_dialog_require_pattern:I = 0x7f120d62

.field public static final encrypt_talkback_dialog_require_pin:I = 0x7f120d63

.field public static final encrypt_title:I = 0x7f120d64

.field public static final encrypted_summary:I = 0x7f120d65

.field public static final encryption_and_credential_settings_summary:I = 0x7f120d66

.field public static final encryption_and_credential_settings_title:I = 0x7f120d67

.field public static final encryption_continue_button:I = 0x7f120d68

.field public static final encryption_interstitial_header:I = 0x7f120d69

.field public static final encryption_interstitial_message_password:I = 0x7f120d6a

.field public static final encryption_interstitial_message_password_for_biometrics:I = 0x7f120d6b

.field public static final encryption_interstitial_message_password_for_face:I = 0x7f120d6c

.field public static final encryption_interstitial_message_password_for_fingerprint:I = 0x7f120d6d

.field public static final encryption_interstitial_message_pattern:I = 0x7f120d6e

.field public static final encryption_interstitial_message_pattern_for_biometrics:I = 0x7f120d6f

.field public static final encryption_interstitial_message_pattern_for_face:I = 0x7f120d70

.field public static final encryption_interstitial_message_pattern_for_fingerprint:I = 0x7f120d71

.field public static final encryption_interstitial_message_pin:I = 0x7f120d72

.field public static final encryption_interstitial_message_pin_for_biometrics:I = 0x7f120d73

.field public static final encryption_interstitial_message_pin_for_face:I = 0x7f120d74

.field public static final encryption_interstitial_message_pin_for_fingerprint:I = 0x7f120d75

.field public static final encryption_interstitial_no:I = 0x7f120d76

.field public static final encryption_interstitial_yes:I = 0x7f120d77

.field public static final encryption_progress_summary:I = 0x7f120d78

.field public static final encryption_settings_title:I = 0x7f120d79

.field public static final enhanced_4g_lte_mode_summary:I = 0x7f120d7a

.field public static final enhanced_4g_lte_mode_summary_4g_calling:I = 0x7f120d7b

.field public static final enhanced_4g_lte_mode_title:I = 0x7f120d7c

.field public static final enhanced_4g_lte_mode_title_4g_calling:I = 0x7f120d7d

.field public static final enhanced_4g_lte_mode_title_advanced_calling:I = 0x7f120d7e

.field public static final enhanced_connectivity_summary:I = 0x7f120d7f

.field public static final enrol_fingerprint_data:I = 0x7f120d80

.field public static final enter_password:I = 0x7f120d81

.field public static final enterprise_mode:I = 0x7f120d82

.field public static final enterprise_privacy_always_on_vpn_device:I = 0x7f120d83

.field public static final enterprise_privacy_always_on_vpn_personal:I = 0x7f120d84

.field public static final enterprise_privacy_always_on_vpn_work:I = 0x7f120d85

.field public static final enterprise_privacy_apps_count_estimation_info:I = 0x7f120d86

.field public static final enterprise_privacy_bug_reports:I = 0x7f120d87

.field public static final enterprise_privacy_ca_certs_device:I = 0x7f120d88

.field public static final enterprise_privacy_ca_certs_personal:I = 0x7f120d89

.field public static final enterprise_privacy_ca_certs_work:I = 0x7f120d8a

.field public static final enterprise_privacy_camera_access:I = 0x7f120d8b

.field public static final enterprise_privacy_device_access_category:I = 0x7f120d8c

.field public static final enterprise_privacy_enterprise_data:I = 0x7f120d8d

.field public static final enterprise_privacy_enterprise_installed_packages:I = 0x7f120d8e

.field public static final enterprise_privacy_enterprise_set_default_apps:I = 0x7f120d8f

.field public static final enterprise_privacy_exposure_category:I = 0x7f120d90

.field public static final enterprise_privacy_exposure_changes_category:I = 0x7f120d91

.field public static final enterprise_privacy_failed_password_wipe_device:I = 0x7f120d92

.field public static final enterprise_privacy_failed_password_wipe_work:I = 0x7f120d93

.field public static final enterprise_privacy_global_http_proxy:I = 0x7f120d94

.field public static final enterprise_privacy_header:I = 0x7f120d95

.field public static final enterprise_privacy_input_method:I = 0x7f120d96

.field public static final enterprise_privacy_input_method_name:I = 0x7f120d97

.field public static final enterprise_privacy_installed_packages:I = 0x7f120d98

.field public static final enterprise_privacy_location_access:I = 0x7f120d99

.field public static final enterprise_privacy_lock_device:I = 0x7f120d9a

.field public static final enterprise_privacy_microphone_access:I = 0x7f120d9b

.field public static final enterprise_privacy_network_logs:I = 0x7f120d9c

.field public static final enterprise_privacy_none:I = 0x7f120d9d

.field public static final enterprise_privacy_security_logs:I = 0x7f120d9e

.field public static final enterprise_privacy_settings:I = 0x7f120d9f

.field public static final enterprise_privacy_settings_summary_generic:I = 0x7f120da0

.field public static final enterprise_privacy_settings_summary_with_name:I = 0x7f120da1

.field public static final enterprise_privacy_usage_stats:I = 0x7f120da2

.field public static final enterprise_privacy_wipe_device:I = 0x7f120da3

.field public static final environmental_speech_recognition:I = 0x7f120da4

.field public static final eras_ad:I = 0x7f120da5

.field public static final eras_bc:I = 0x7f120da6

.field public static final erase_application:I = 0x7f120da7

.field public static final erase_application_description:I = 0x7f120da8

.field public static final erase_data:I = 0x7f120da9

.field public static final erase_euicc_data_button:I = 0x7f120daa

.field public static final erase_external_storage:I = 0x7f120dab

.field public static final erase_external_storage_description:I = 0x7f120dac

.field public static final erase_internal_storage:I = 0x7f120dad

.field public static final erase_internal_storage_description:I = 0x7f120dae

.field public static final erase_optional:I = 0x7f120daf

.field public static final erase_personal_data:I = 0x7f120db0

.field public static final erase_personal_data_description:I = 0x7f120db1

.field public static final erase_sd_data:I = 0x7f120db2

.field public static final erase_sd_data_description:I = 0x7f120db3

.field public static final erase_sim_confirm_button:I = 0x7f120db4

.field public static final erase_sim_dialog_text:I = 0x7f120db5

.field public static final erase_sim_dialog_title:I = 0x7f120db6

.field public static final erase_sim_fail_text:I = 0x7f120db7

.field public static final erase_sim_fail_title:I = 0x7f120db8

.field public static final erase_sure:I = 0x7f120db9

.field public static final erasing_sim:I = 0x7f120dba

.field public static final error_adding_apn_type:I = 0x7f120dbb

.field public static final error_apn_empty:I = 0x7f120dbc

.field public static final error_apn_not_allowed:I = 0x7f120dbd

.field public static final error_apn_restrict_string:I = 0x7f120dbe

.field public static final error_capturing_system_heap_dump_message:I = 0x7f120dbf

.field public static final error_icon_content_description:I = 0x7f120dc0

.field public static final error_mcc_not3:I = 0x7f120dc1

.field public static final error_mnc_not23:I = 0x7f120dc2

.field public static final error_name_empty:I = 0x7f120dc3

.field public static final error_title:I = 0x7f120dc4

.field public static final error_xcap_not_add:I = 0x7f120dc5

.field public static final estimated_charging_time_left:I = 0x7f120dc6

.field public static final estimated_time_description:I = 0x7f120dc7

.field public static final estimated_time_left:I = 0x7f120dc8

.field public static final ethernet:I = 0x7f120dc9

.field public static final ethernet_data_template:I = 0x7f120dca

.field public static final ethernet_data_usage:I = 0x7f120dcb

.field public static final ethernet_tether_checkbox_text:I = 0x7f120dcc

.field public static final ethernet_tethering_subtext:I = 0x7f120dcd

.field public static final evening:I = 0x7f120dce

.field public static final every_day:I = 0x7f120dcf

.field public static final exceptional_case:I = 0x7f120dd0

.field public static final expand_button_title:I = 0x7f120dd1

.field public static final expand_card_action:I = 0x7f120dd2

.field public static final expandable_under_keyguard_title:I = 0x7f120dd3

.field public static final experimental_category_title:I = 0x7f120dd4

.field public static final experimental_preference:I = 0x7f120dd5

.field public static final expert_detail_content_description:I = 0x7f120dd6

.field public static final exposed_dropdown_menu_content_description:I = 0x7f120dd7

.field public static final exterior:I = 0x7f120dd8

.field public static final external_auto_rotate_title:I = 0x7f120dd9

.field public static final external_code_size_label:I = 0x7f120dda

.field public static final external_data_size_label:I = 0x7f120ddb

.field public static final external_ram_dialog_btn_cancel:I = 0x7f120ddc

.field public static final external_ram_dialog_btn_ok:I = 0x7f120ddd

.field public static final external_ram_dialog_icon_confirm:I = 0x7f120dde

.field public static final external_ram_dialog_icon_info:I = 0x7f120ddf

.field public static final external_ram_dialog_icon_title:I = 0x7f120de0

.field public static final external_ram_dialog_message:I = 0x7f120de1

.field public static final external_ram_dialog_title:I = 0x7f120de2

.field public static final external_ram_intro:I = 0x7f120de3

.field public static final external_ram_remind_toast:I = 0x7f120de4

.field public static final external_ram_summary:I = 0x7f120de5

.field public static final external_ram_title:I = 0x7f120de6

.field public static final external_ram_toast:I = 0x7f120de7

.field public static final external_source_switch_title:I = 0x7f120de8

.field public static final external_source_trusted:I = 0x7f120de9

.field public static final external_source_untrusted:I = 0x7f120dea

.field public static final extreme_threats_summary:I = 0x7f120deb

.field public static final extreme_threats_title:I = 0x7f120dec

.field public static final eye_gaze_summary:I = 0x7f120ded

.field public static final eye_gaze_title:I = 0x7f120dee

.field public static final fab_transformation_scrim_behavior:I = 0x7f120def

.field public static final fab_transformation_sheet_behavior:I = 0x7f120df0

.field public static final face_add_max:I = 0x7f120df1

.field public static final face_data_apply_for_category_title:I = 0x7f120df2

.field public static final face_data_apply_for_keyguard_title:I = 0x7f120df3

.field public static final face_data_input_camera_fail:I = 0x7f120df4

.field public static final face_data_input_camera_ok:I = 0x7f120df5

.field public static final face_data_input_cancel_msg:I = 0x7f120df6

.field public static final face_data_input_ok:I = 0x7f120df7

.field public static final face_data_input_ok_title:I = 0x7f120df8

.field public static final face_data_input_structure_title:I = 0x7f120df9

.field public static final face_data_input_title:I = 0x7f120dfa

.field public static final face_data_introduction_add:I = 0x7f120dfb

.field public static final face_data_introduction_msg:I = 0x7f120dfc

.field public static final face_data_introduction_next:I = 0x7f120dfd

.field public static final face_data_introduction_title:I = 0x7f120dfe

.field public static final face_data_manage_delete:I = 0x7f120dff

.field public static final face_data_manage_delete_msg:I = 0x7f120e00

.field public static final face_data_manage_delete_sure:I = 0x7f120e01

.field public static final face_data_manage_unlock_liftcamera_msg:I = 0x7f120e02

.field public static final face_data_manage_unlock_msg:I = 0x7f120e03

.field public static final face_data_manage_unlock_slide_msg:I = 0x7f120e04

.field public static final face_data_manage_unlock_title:I = 0x7f120e05

.field public static final face_data_siggesstion_next:I = 0x7f120e06

.field public static final face_data_siggesstion_next_cn:I = 0x7f120e07

.field public static final face_data_siggesstion_next_time:I = 0x7f120e08

.field public static final face_data_siggesstion_next_time_cn:I = 0x7f120e09

.field public static final face_data_suggesstion_first:I = 0x7f120e0a

.field public static final face_data_suggesstion_first_cn:I = 0x7f120e0b

.field public static final face_data_suggesstion_first_cn_highlight:I = 0x7f120e0c

.field public static final face_data_suggesstion_second:I = 0x7f120e0d

.field public static final face_intro_error_max:I = 0x7f120e0e

.field public static final face_intro_error_unknown:I = 0x7f120e0f

.field public static final face_password_unlock:I = 0x7f120e10

.field public static final face_unlock:I = 0x7f120e11

.field public static final face_unlock_be_on_the_screen:I = 0x7f120e12

.field public static final face_unlock_by_notification_screen_on_msg:I = 0x7f120e13

.field public static final face_unlock_by_notification_screen_on_title:I = 0x7f120e14

.field public static final face_unlock_category_title:I = 0x7f120e15

.field public static final face_unlock_check_failed:I = 0x7f120e16

.field public static final face_unlock_close_screen:I = 0x7f120e17

.field public static final face_unlock_disable:I = 0x7f120e18

.field public static final face_unlock_down:I = 0x7f120e19

.field public static final face_unlock_enable:I = 0x7f120e1a

.field public static final face_unlock_not_found:I = 0x7f120e1b

.field public static final face_unlock_offset_bottom:I = 0x7f120e1c

.field public static final face_unlock_offset_left:I = 0x7f120e1d

.field public static final face_unlock_offset_right:I = 0x7f120e1e

.field public static final face_unlock_offset_top:I = 0x7f120e1f

.field public static final face_unlock_open_eye:I = 0x7f120e20

.field public static final face_unlock_prompt:I = 0x7f120e21

.field public static final face_unlock_quality:I = 0x7f120e22

.field public static final face_unlock_reveal_eye:I = 0x7f120e23

.field public static final face_unlock_reveal_mouth:I = 0x7f120e24

.field public static final face_unlock_rise:I = 0x7f120e25

.field public static final face_unlock_rotated_left:I = 0x7f120e26

.field public static final face_unlock_rotated_right:I = 0x7f120e27

.field public static final face_unlock_sc_status:I = 0x7f120e28

.field public static final face_unlock_set_unlock_password:I = 0x7f120e29

.field public static final face_unlock_set_unlock_pattern:I = 0x7f120e2a

.field public static final face_unlock_set_unlock_pin:I = 0x7f120e2b

.field public static final face_unlock_skip_face:I = 0x7f120e2c

.field public static final face_unlock_stay_away_screen:I = 0x7f120e2d

.field public static final face_unlock_success_show_message_dialog_msg:I = 0x7f120e2e

.field public static final face_unlock_success_show_message_dialog_negative_btn:I = 0x7f120e2f

.field public static final face_unlock_success_show_message_dialog_positive_btn:I = 0x7f120e30

.field public static final face_unlock_success_show_message_dialog_title:I = 0x7f120e31

.field public static final face_unlock_success_show_message_msg:I = 0x7f120e32

.field public static final face_unlock_success_show_message_title:I = 0x7f120e33

.field public static final face_unlock_success_show_message_toast:I = 0x7f120e34

.field public static final face_unlock_success_stay_screen_msg:I = 0x7f120e35

.field public static final face_unlock_success_stay_screen_title:I = 0x7f120e36

.field public static final face_unlock_title:I = 0x7f120e37

.field public static final factory_reset_summary:I = 0x7f120e38

.field public static final failed_attempts_now_wiping_device:I = 0x7f120e39

.field public static final failed_attempts_now_wiping_dialog_dismiss:I = 0x7f120e3a

.field public static final failed_attempts_now_wiping_profile:I = 0x7f120e3b

.field public static final failed_attempts_now_wiping_user:I = 0x7f120e3c

.field public static final failed_to_check_find_device_status_content:I = 0x7f120e3d

.field public static final failed_to_check_find_device_status_title:I = 0x7f120e3e

.field public static final failed_to_erase_text:I = 0x7f120e3f

.field public static final failed_to_erase_title:I = 0x7f120e40

.field public static final failed_to_open_app_settings_toast:I = 0x7f120e41

.field public static final failed_to_shut_down_find_device_content:I = 0x7f120e42

.field public static final failed_to_shut_down_find_device_title:I = 0x7f120e43

.field public static final fallback_home_title:I = 0x7f120e44

.field public static final fcc_equipment_id:I = 0x7f120e45

.field public static final feature_flags_dashboard_title:I = 0x7f120e46

.field public static final feedback:I = 0x7f120e47

.field public static final feedback_btn_text:I = 0x7f120e48

.field public static final feedback_settings:I = 0x7f120e49

.field public static final feedback_settings_new:I = 0x7f120e4a

.field public static final felica_wallet:I = 0x7f120e4b

.field public static final felica_wallet_desc:I = 0x7f120e4c

.field public static final file_privacy_password_role_instruction:I = 0x7f120e4d

.field public static final filter:I = 0x7f120e4e

.field public static final filter_all_apps:I = 0x7f120e4f

.field public static final filter_apps_all:I = 0x7f120e50

.field public static final filter_apps_cached:I = 0x7f120e51

.field public static final filter_apps_disabled:I = 0x7f120e52

.field public static final filter_apps_onsdcard:I = 0x7f120e53

.field public static final filter_apps_running:I = 0x7f120e54

.field public static final filter_apps_third_party:I = 0x7f120e55

.field public static final filter_dlg_title:I = 0x7f120e56

.field public static final filter_enabled_apps:I = 0x7f120e57

.field public static final filter_install_sources_apps:I = 0x7f120e58

.field public static final filter_instant_apps:I = 0x7f120e59

.field public static final filter_manage_external_storage:I = 0x7f120e5a

.field public static final filter_notif_all_apps:I = 0x7f120e5b

.field public static final filter_notif_blocked_apps:I = 0x7f120e5c

.field public static final filter_notif_blocked_channels:I = 0x7f120e5d

.field public static final filter_notif_dnd_channels:I = 0x7f120e5e

.field public static final filter_notif_low_channels:I = 0x7f120e5f

.field public static final filter_notif_urgent_channels:I = 0x7f120e60

.field public static final filter_overlay_apps:I = 0x7f120e61

.field public static final filter_write_settings_apps:I = 0x7f120e62

.field public static final financed_privacy_IMEI:I = 0x7f120e63

.field public static final financed_privacy_allowlisted_apps:I = 0x7f120e64

.field public static final financed_privacy_config_date_time:I = 0x7f120e65

.field public static final financed_privacy_credit_provider_capabilities_category:I = 0x7f120e66

.field public static final financed_privacy_developer_options:I = 0x7f120e67

.field public static final financed_privacy_emergency_calls:I = 0x7f120e68

.field public static final financed_privacy_factory_reset:I = 0x7f120e69

.field public static final financed_privacy_fully_paid_category:I = 0x7f120e6a

.field public static final financed_privacy_install_apps:I = 0x7f120e6b

.field public static final financed_privacy_intro:I = 0x7f120e6c

.field public static final financed_privacy_locked_mode_category:I = 0x7f120e6d

.field public static final financed_privacy_multi_users:I = 0x7f120e6e

.field public static final financed_privacy_notifications:I = 0x7f120e6f

.field public static final financed_privacy_restrictions_category:I = 0x7f120e70

.field public static final financed_privacy_restrictions_removed:I = 0x7f120e71

.field public static final financed_privacy_safe_mode:I = 0x7f120e72

.field public static final financed_privacy_settings:I = 0x7f120e73

.field public static final financed_privacy_system_info:I = 0x7f120e74

.field public static final financed_privacy_turn_on_off_device:I = 0x7f120e75

.field public static final financed_privacy_uninstall_creditor_app:I = 0x7f120e76

.field public static final find_broadcast_password_dialog_connection_error:I = 0x7f120e77

.field public static final find_broadcast_password_dialog_password_error:I = 0x7f120e78

.field public static final find_broadcast_password_dialog_title:I = 0x7f120e79

.field public static final find_my_device:I = 0x7f120e7a

.field public static final find_my_device_disable:I = 0x7f120e7b

.field public static final find_my_device_enable:I = 0x7f120e7c

.field public static final find_my_device_summary:I = 0x7f120e7d

.field public static final finger_add_user_info_dialog_cancel:I = 0x7f120e7e

.field public static final finger_add_user_info_dialog_message:I = 0x7f120e7f

.field public static final finger_add_user_info_dialog_next:I = 0x7f120e80

.field public static final finger_add_user_info_dialog_title:I = 0x7f120e81

.field public static final finger_detail_info_text:I = 0x7f120e82

.field public static final fingerprint_add_max:I = 0x7f120e83

.field public static final fingerprint_add_title:I = 0x7f120e84

.field public static final fingerprint_and_password_entrance_title:I = 0x7f120e85

.field public static final fingerprint_apply_to_privacy_password:I = 0x7f120e86

.field public static final fingerprint_apply_to_privacy_password_scenarios:I = 0x7f120e87

.field public static final fingerprint_base_title:I = 0x7f120e88

.field public static final fingerprint_delete_message:I = 0x7f120e89

.field public static final fingerprint_delete_title:I = 0x7f120e8a

.field public static final fingerprint_double_tap:I = 0x7f120e8b

.field public static final fingerprint_enable_keyguard_toggle_title:I = 0x7f120e8c

.field public static final fingerprint_enroll_button_add:I = 0x7f120e8d

.field public static final fingerprint_enroll_button_next:I = 0x7f120e8e

.field public static final fingerprint_face_password_unlock:I = 0x7f120e8f

.field public static final fingerprint_first_open_screen_password_face_tittle:I = 0x7f120e90

.field public static final fingerprint_first_open_screen_password_tittle:I = 0x7f120e91

.field public static final fingerprint_for_keyguard_disabled:I = 0x7f120e92

.field public static final fingerprint_gesture_screen_title:I = 0x7f120e93

.field public static final fingerprint_gxzw_add_fingerprint_finish:I = 0x7f120e94

.field public static final fingerprint_identify_input_password_msg:I = 0x7f120e95

.field public static final fingerprint_identify_msg:I = 0x7f120e96

.field public static final fingerprint_identify_try_again_msg:I = 0x7f120e97

.field public static final fingerprint_intro_error_max:I = 0x7f120e98

.field public static final fingerprint_intro_error_unknown:I = 0x7f120e99

.field public static final fingerprint_last_delete_confirm:I = 0x7f120e9a

.field public static final fingerprint_last_delete_message:I = 0x7f120e9b

.field public static final fingerprint_last_delete_message_profile_challenge:I = 0x7f120e9c

.field public static final fingerprint_last_delete_title:I = 0x7f120e9d

.field public static final fingerprint_list_title:I = 0x7f120e9e

.field public static final fingerprint_lock_first:I = 0x7f120e9f

.field public static final fingerprint_lock_second:I = 0x7f120ea0

.field public static final fingerprint_manage:I = 0x7f120ea1

.field public static final fingerprint_manage_category_title:I = 0x7f120ea2

.field public static final fingerprint_manage_tittle:I = 0x7f120ea3

.field public static final fingerprint_not_identified_msg:I = 0x7f120ea4

.field public static final fingerprint_not_recognized:I = 0x7f120ea5

.field public static final fingerprint_others_title:I = 0x7f120ea6

.field public static final fingerprint_password_unlock:I = 0x7f120ea7

.field public static final fingerprint_payment:I = 0x7f120ea8

.field public static final fingerprint_removal_failed:I = 0x7f120ea9

.field public static final fingerprint_swipe_for_notifications_suggestion_title:I = 0x7f120eaa

.field public static final fingerprint_swipe_for_notifications_summary:I = 0x7f120eab

.field public static final fingerprint_swipe_for_notifications_title:I = 0x7f120eac

.field public static final fingerprint_unlock_press_type:I = 0x7f120ead

.field public static final fingerprint_unlock_set_unlock_password:I = 0x7f120eae

.field public static final fingerprint_unlock_set_unlock_pattern:I = 0x7f120eaf

.field public static final fingerprint_unlock_set_unlock_pin:I = 0x7f120eb0

.field public static final fingerprint_unlock_skip_fingerprint:I = 0x7f120eb1

.field public static final fingerprint_unlock_title:I = 0x7f120eb2

.field public static final fingerprint_unlock_touch_type:I = 0x7f120eb3

.field public static final fingerprint_unlock_type:I = 0x7f120eb4

.field public static final fingerprint_usage_category_title:I = 0x7f120eb5

.field public static final fingerprint_v2_delete_message:I = 0x7f120eb6

.field public static final finish_application:I = 0x7f120eb7

.field public static final finish_button:I = 0x7f120eb8

.field public static final firewall_allow_all:I = 0x7f120eb9

.field public static final firewall_allow_data:I = 0x7f120eba

.field public static final firewall_allow_network:I = 0x7f120ebb

.field public static final firewall_allow_wlan:I = 0x7f120ebc

.field public static final firewall_applist:I = 0x7f120ebd

.field public static final firewall_mobile:I = 0x7f120ebe

.field public static final firewall_network_allow:I = 0x7f120ebf

.field public static final firewall_network_restrict_notify_message:I = 0x7f120ec0

.field public static final firewall_network_restrict_notify_mobile:I = 0x7f120ec1

.field public static final firewall_partial:I = 0x7f120ec2

.field public static final firewall_permission:I = 0x7f120ec3

.field public static final firewall_restrict_all:I = 0x7f120ec4

.field public static final firewall_restrict_android_dialog_content:I = 0x7f120ec5

.field public static final firewall_restrict_android_dialog_title:I = 0x7f120ec6

.field public static final firewall_restrict_data:I = 0x7f120ec7

.field public static final firewall_restrict_notify_title:I = 0x7f120ec8

.field public static final firewall_restrict_wlan:I = 0x7f120ec9

.field public static final firewall_set_bg_firewall_default_dialog_summary:I = 0x7f120eca

.field public static final firewall_set_bg_firewall_default_dialog_title:I = 0x7f120ecb

.field public static final firewall_temp_rule_reason:I = 0x7f120ecc

.field public static final firewall_wifi:I = 0x7f120ecd

.field public static final firewall_wifi_dialog_message:I = 0x7f120ece

.field public static final firmware_version:I = 0x7f120ecf

.field public static final fiveg_nrca_switch_summary:I = 0x7f120ed0

.field public static final fiveg_nrca_switch_title:I = 0x7f120ed1

.field public static final fiveg_sa_switch_summary:I = 0x7f120ed2

.field public static final fiveg_sa_vice_switch_title:I = 0x7f120ed3

.field public static final fiveg_vonr_switch_summary:I = 0x7f120ed4

.field public static final fiveg_vonr_switch_title:I = 0x7f120ed5

.field public static final fix_connectivity:I = 0x7f120ed6

.field public static final flash_drive_backup_disable:I = 0x7f120ed7

.field public static final flash_drive_backup_enable:I = 0x7f120ed8

.field public static final flash_drive_backup_restore:I = 0x7f120ed9

.field public static final flash_drive_backup_restore_summary:I = 0x7f120eda

.field public static final flashback_background_start_switch_summary:I = 0x7f120edb

.field public static final flashback_background_start_switch_title:I = 0x7f120edc

.field public static final flashback_baidu_map_summary:I = 0x7f120edd

.field public static final flashback_didi_summary:I = 0x7f120ede

.field public static final flashback_meituan_summary:I = 0x7f120edf

.field public static final flashback_meituan_waimai_summary:I = 0x7f120ee0

.field public static final flashback_open_switch_summary:I = 0x7f120ee1

.field public static final flashback_open_switch_title:I = 0x7f120ee2

.field public static final flashback_shansong_summary:I = 0x7f120ee3

.field public static final flashback_status_close:I = 0x7f120ee4

.field public static final flashback_status_open:I = 0x7f120ee5

.field public static final flashback_summary:I = 0x7f120ee6

.field public static final flashback_support_apps_title:I = 0x7f120ee7

.field public static final flashback_title:I = 0x7f120ee8

.field public static final flashback_wangzhe_summary:I = 0x7f120ee9

.field public static final floating_window_switch_label:I = 0x7f120eea

.field public static final floating_window_switch_label_disable:I = 0x7f120eeb

.field public static final fluency_mode:I = 0x7f120eec

.field public static final fluency_mode_confirm_and_reboot:I = 0x7f120eed

.field public static final fmt_chinese_date:I = 0x7f120eee

.field public static final fmt_date:I = 0x7f120eef

.field public static final fmt_date_day:I = 0x7f120ef0

.field public static final fmt_date_long_month:I = 0x7f120ef1

.field public static final fmt_date_long_month_day:I = 0x7f120ef2

.field public static final fmt_date_long_year_month:I = 0x7f120ef3

.field public static final fmt_date_long_year_month_day:I = 0x7f120ef4

.field public static final fmt_date_numeric_day:I = 0x7f120ef5

.field public static final fmt_date_numeric_month:I = 0x7f120ef6

.field public static final fmt_date_numeric_month_day:I = 0x7f120ef7

.field public static final fmt_date_numeric_year:I = 0x7f120ef8

.field public static final fmt_date_numeric_year_month:I = 0x7f120ef9

.field public static final fmt_date_numeric_year_month_day:I = 0x7f120efa

.field public static final fmt_date_short_month:I = 0x7f120efb

.field public static final fmt_date_short_month_day:I = 0x7f120efc

.field public static final fmt_date_short_year_month:I = 0x7f120efd

.field public static final fmt_date_short_year_month_day:I = 0x7f120efe

.field public static final fmt_date_time:I = 0x7f120eff

.field public static final fmt_date_time_timezone:I = 0x7f120f00

.field public static final fmt_date_timezone:I = 0x7f120f01

.field public static final fmt_date_year:I = 0x7f120f02

.field public static final fmt_time:I = 0x7f120f03

.field public static final fmt_time_12hour:I = 0x7f120f04

.field public static final fmt_time_12hour_minute:I = 0x7f120f05

.field public static final fmt_time_12hour_minute_pm:I = 0x7f120f06

.field public static final fmt_time_12hour_minute_second:I = 0x7f120f07

.field public static final fmt_time_12hour_minute_second_millis:I = 0x7f120f08

.field public static final fmt_time_12hour_minute_second_millis_pm:I = 0x7f120f09

.field public static final fmt_time_12hour_minute_second_pm:I = 0x7f120f0a

.field public static final fmt_time_12hour_pm:I = 0x7f120f0b

.field public static final fmt_time_24hour:I = 0x7f120f0c

.field public static final fmt_time_24hour_minute:I = 0x7f120f0d

.field public static final fmt_time_24hour_minute_second:I = 0x7f120f0e

.field public static final fmt_time_24hour_minute_second_millis:I = 0x7f120f0f

.field public static final fmt_time_millis:I = 0x7f120f10

.field public static final fmt_time_minute:I = 0x7f120f11

.field public static final fmt_time_minute_second:I = 0x7f120f12

.field public static final fmt_time_minute_second_millis:I = 0x7f120f13

.field public static final fmt_time_second:I = 0x7f120f14

.field public static final fmt_time_second_millis:I = 0x7f120f15

.field public static final fmt_time_timezone:I = 0x7f120f16

.field public static final fmt_timezone:I = 0x7f120f17

.field public static final fmt_weekday:I = 0x7f120f18

.field public static final fmt_weekday_date:I = 0x7f120f19

.field public static final fmt_weekday_date_time:I = 0x7f120f1a

.field public static final fmt_weekday_date_time_timezone:I = 0x7f120f1b

.field public static final fmt_weekday_date_timezone:I = 0x7f120f1c

.field public static final fmt_weekday_long:I = 0x7f120f1d

.field public static final fmt_weekday_short:I = 0x7f120f1e

.field public static final fmt_weekday_time:I = 0x7f120f1f

.field public static final fmt_weekday_time_timezone:I = 0x7f120f20

.field public static final fmt_weekday_timezone:I = 0x7f120f21

.field public static final fold_application_mode_name:I = 0x7f120f22

.field public static final fold_game_adaptation:I = 0x7f120f23

.field public static final fold_rule_default:I = 0x7f120f24

.field public static final fold_rule_important:I = 0x7f120f25

.field public static final fold_rule_unimportant:I = 0x7f120f26

.field public static final fold_screen_application_adaptation_title:I = 0x7f120f27

.field public static final fold_screen_settings_title:I = 0x7f120f28

.field public static final fold_screen_special_function_title:I = 0x7f120f29

.field public static final fold_title:I = 0x7f120f2a

.field public static final font_cta_alert_btn_negative:I = 0x7f120f2b

.field public static final font_cta_alert_btn_positive:I = 0x7f120f2c

.field public static final font_cta_alert_message:I = 0x7f120f2d

.field public static final font_cta_alert_title:I = 0x7f120f2e

.field public static final font_setting_weight_toast:I = 0x7f120f2f

.field public static final font_settings_jump:I = 0x7f120f30

.field public static final font_settings_recommend_title:I = 0x7f120f31

.field public static final font_settings_summary_cn:I = 0x7f120f32

.field public static final font_size_exhuge:I = 0x7f120f33

.field public static final font_size_huge:I = 0x7f120f34

.field public static final font_size_kddi:I = 0x7f120f35

.field public static final font_size_large:I = 0x7f120f36

.field public static final font_size_make_larger_desc:I = 0x7f120f37

.field public static final font_size_make_smaller_desc:I = 0x7f120f38

.field public static final font_size_medium:I = 0x7f120f39

.field public static final font_size_normal:I = 0x7f120f3a

.field public static final font_size_preview:I = 0x7f120f3b

.field public static final font_size_preview_text_body:I = 0x7f120f3c

.field public static final font_size_preview_text_headline:I = 0x7f120f3d

.field public static final font_size_preview_text_subtitle:I = 0x7f120f3e

.field public static final font_size_preview_text_title:I = 0x7f120f3f

.field public static final font_size_save:I = 0x7f120f40

.field public static final font_size_small:I = 0x7f120f41

.field public static final font_size_summary:I = 0x7f120f42

.field public static final font_size_summary1:I = 0x7f120f43

.field public static final font_size_summary2:I = 0x7f120f44

.field public static final footer_learn_more_content_description:I = 0x7f120f45

.field public static final for_diagnosis_usage_only:I = 0x7f120f46

.field public static final forbidden_network:I = 0x7f120f47

.field public static final forbidden_set_unlock_password_msg:I = 0x7f120f48

.field public static final forbidden_switch_apn_string:I = 0x7f120f49

.field public static final force_allow_on_external:I = 0x7f120f4a

.field public static final force_allow_on_external_summary:I = 0x7f120f4b

.field public static final force_bold_text:I = 0x7f120f4c

.field public static final force_desktop_mode:I = 0x7f120f4d

.field public static final force_desktop_mode_summary:I = 0x7f120f4e

.field public static final force_high_refresh_rate_desc:I = 0x7f120f4f

.field public static final force_high_refresh_rate_toggle:I = 0x7f120f50

.field public static final force_immersive_compatibility_dont_hide:I = 0x7f120f51

.field public static final force_immersive_compatibility_hide:I = 0x7f120f52

.field public static final force_immersive_compatibility_hint_message:I = 0x7f120f53

.field public static final force_immersive_compatibility_hint_title:I = 0x7f120f54

.field public static final force_msaa:I = 0x7f120f55

.field public static final force_msaa_new:I = 0x7f120f56

.field public static final force_msaa_prefrence_summary_new:I = 0x7f120f57

.field public static final force_msaa_summary:I = 0x7f120f58

.field public static final force_resizable_activities:I = 0x7f120f59

.field public static final force_resizable_activities_summary:I = 0x7f120f5a

.field public static final force_rtl_layout_all_locales:I = 0x7f120f5b

.field public static final force_rtl_layout_all_locales_summary:I = 0x7f120f5c

.field public static final force_stop:I = 0x7f120f5d

.field public static final force_stop_dlg_text:I = 0x7f120f5e

.field public static final force_stop_dlg_title:I = 0x7f120f5f

.field public static final foreground_traffic:I = 0x7f120f60

.field public static final forget:I = 0x7f120f61

.field public static final forget_group:I = 0x7f120f62

.field public static final forget_passpoint_dialog_message:I = 0x7f120f63

.field public static final forget_passpoint_wifi_message:I = 0x7f120f64

.field public static final forget_password_button_text:I = 0x7f120f65

.field public static final forget_password_dialog_button_text:I = 0x7f120f66

.field public static final forget_password_dialog_content_four:I = 0x7f120f67

.field public static final forget_password_dialog_content_one:I = 0x7f120f68

.field public static final forget_password_dialog_content_three:I = 0x7f120f69

.field public static final forget_password_dialog_content_two:I = 0x7f120f6a

.field public static final forget_password_dialog_title:I = 0x7f120f6b

.field public static final forget_pattern:I = 0x7f120f6c

.field public static final forget_privacy_password_setting:I = 0x7f120f6d

.field public static final forget_privacy_password_summary:I = 0x7f120f6e

.field public static final forget_wifi_message:I = 0x7f120f6f

.field public static final forgot_password_text:I = 0x7f120f70

.field public static final forgot_password_title:I = 0x7f120f71

.field public static final found_new_version:I = 0x7f120f72

.field public static final fourhours:I = 0x7f120f73

.field public static final fp_nav_center_back_to_home_title:I = 0x7f120f74

.field public static final free_memory:I = 0x7f120f75

.field public static final free_wifi_agreement_remind_never:I = 0x7f120f76

.field public static final free_wifi_confirm:I = 0x7f120f77

.field public static final free_wifi_connect:I = 0x7f120f78

.field public static final free_wifi_connect_dialog_summary:I = 0x7f120f79

.field public static final free_wifi_connect_dialog_title:I = 0x7f120f7a

.field public static final free_wifi_connect_fail:I = 0x7f120f7b

.field public static final free_wifi_connect_fail2:I = 0x7f120f7c

.field public static final free_wifi_connect_summary:I = 0x7f120f7d

.field public static final free_wifi_connect_title:I = 0x7f120f7e

.field public static final free_wifi_connected:I = 0x7f120f7f

.field public static final free_wifi_connected_time:I = 0x7f120f80

.field public static final free_wifi_connecting:I = 0x7f120f81

.field public static final free_wifi_login_again:I = 0x7f120f82

.field public static final free_wifi_manage_summary:I = 0x7f120f83

.field public static final free_wifi_speed_title:I = 0x7f120f84

.field public static final free_wifi_summary:I = 0x7f120f85

.field public static final free_wifi_user_agreement:I = 0x7f120f86

.field public static final free_wifi_user_agreement_allow:I = 0x7f120f87

.field public static final free_wifi_user_agreement_title:I = 0x7f120f88

.field public static final free_wifi_wiwide_provider:I = 0x7f120f89

.field public static final free_xiaomi_wifi_connect_summary:I = 0x7f120f8a

.field public static final freeform_guide_drop_down_to_fullscreen_summary:I = 0x7f120f8b

.field public static final freeform_guide_drop_down_to_fullscreen_title:I = 0x7f120f8c

.field public static final freeform_guide_move_summary:I = 0x7f120f8d

.field public static final freeform_guide_move_title:I = 0x7f120f8e

.field public static final freeform_guide_move_to_small_freeform_window_summary:I = 0x7f120f8f

.field public static final freeform_guide_move_to_small_freeform_window_title:I = 0x7f120f90

.field public static final freeform_guide_multi_window:I = 0x7f120f91

.field public static final freeform_guide_notification_drop_down_summary:I = 0x7f120f92

.field public static final freeform_guide_notification_drop_down_title:I = 0x7f120f93

.field public static final freeform_guide_settings:I = 0x7f120f94

.field public static final freeform_guide_slide_to_small_freeform_summary:I = 0x7f120f95

.field public static final freeform_guide_slide_to_small_freeform_summary_pad:I = 0x7f120f96

.field public static final freeform_guide_slide_to_small_freeform_summary_pad_global:I = 0x7f120f97

.field public static final freeform_guide_slide_to_small_freeform_summary_squarehot:I = 0x7f120f98

.field public static final freeform_guide_slide_to_small_freeform_title:I = 0x7f120f99

.field public static final freeform_guide_slide_up_to_close_summary:I = 0x7f120f9a

.field public static final freeform_guide_slide_up_to_close_title:I = 0x7f120f9b

.field public static final freeform_guide_to_sidehide_summary:I = 0x7f120f9c

.field public static final freeform_guide_to_sidehide_title:I = 0x7f120f9d

.field public static final freeform_guide_vedio_description:I = 0x7f120f9e

.field public static final freeform_vedio_description:I = 0x7f120f9f

.field public static final freeform_vedio_description_open_freeform_and_splitscreen:I = 0x7f120fa0

.field public static final freeform_vedio_description_open_freeform_and_splitscreen_pad:I = 0x7f120fa1

.field public static final freeform_vedio_description_pad:I = 0x7f120fa2

.field public static final frequently_fragment_delete:I = 0x7f120fa3

.field public static final frequently_fragment_tempty:I = 0x7f120fa4

.field public static final friday:I = 0x7f120fa5

.field public static final friday_short:I = 0x7f120fa6

.field public static final friday_shortest:I = 0x7f120fa7

.field public static final fsg_mistake_touch_summary:I = 0x7f120fa8

.field public static final fsg_mistake_touch_title:I = 0x7f120fa9

.field public static final full_screen_display:I = 0x7f120faa

.field public static final full_screen_keyboard_optimization:I = 0x7f120fab

.field public static final full_screen_keyboard_settings:I = 0x7f120fac

.field public static final full_transparent_smarrcover_title:I = 0x7f120fad

.field public static final fullbackup_data_summary:I = 0x7f120fae

.field public static final fullbackup_erase_dialog_message:I = 0x7f120faf

.field public static final gadget_picker_title:I = 0x7f120fb0

.field public static final gadget_toggle_wifi:I = 0x7f120fb1

.field public static final gadget_wifi:I = 0x7f120fb2

.field public static final gallery_privacy_password_role_instruction:I = 0x7f120fb3

.field public static final game_booster:I = 0x7f120fb4

.field public static final game_booster_title:I = 0x7f120fb5

.field public static final game_controller_settings_category:I = 0x7f120fb6

.field public static final game_storage_settings:I = 0x7f120fb7

.field public static final gd_setting_title:I = 0x7f120fb8

.field public static final general_category_title:I = 0x7f120fb9

.field public static final general_notification_header:I = 0x7f120fba

.field public static final general_text:I = 0x7f120fbb

.field public static final general_time:I = 0x7f120fbc

.field public static final gentle_notifications_also_display:I = 0x7f120fbd

.field public static final gentle_notifications_display_lock:I = 0x7f120fbe

.field public static final gentle_notifications_display_status:I = 0x7f120fbf

.field public static final gentle_notifications_display_summary_shade:I = 0x7f120fc0

.field public static final gentle_notifications_display_summary_shade_lock:I = 0x7f120fc1

.field public static final gentle_notifications_display_summary_shade_status:I = 0x7f120fc2

.field public static final gentle_notifications_display_summary_shade_status_lock:I = 0x7f120fc3

.field public static final gentle_notifications_education:I = 0x7f120fc4

.field public static final gentle_notifications_title:I = 0x7f120fc5

.field public static final gesture_double_twist_sensor_type:I = 0x7f120fc6

.field public static final gesture_double_twist_sensor_vendor:I = 0x7f120fc7

.field public static final gesture_function_dialog_message:I = 0x7f120fc8

.field public static final gesture_not_supported_dialog_message:I = 0x7f120fc9

.field public static final gesture_not_supported_positive_button:I = 0x7f120fca

.field public static final gesture_preference_summary:I = 0x7f120fcb

.field public static final gesture_preference_title:I = 0x7f120fcc

.field public static final gesture_prevent_ringing_screen_title:I = 0x7f120fcd

.field public static final gesture_prevent_ringing_sound_title:I = 0x7f120fce

.field public static final gesture_prevent_ringing_title:I = 0x7f120fcf

.field public static final gesture_setting_off:I = 0x7f120fd0

.field public static final gesture_setting_on:I = 0x7f120fd1

.field public static final gesture_settings_activity_title:I = 0x7f120fd2

.field public static final gesture_settings_title:I = 0x7f120fd3

.field public static final gesture_wakeup_summary:I = 0x7f120fd4

.field public static final gesture_wakeup_title:I = 0x7f120fd5

.field public static final getConfiguredNetworks:I = 0x7f120fd6

.field public static final getConnectionInfo:I = 0x7f120fd7

.field public static final giga_hertz:I = 0x7f120fd8

.field public static final gigabyteShort:I = 0x7f120fd9

.field public static final global_change_warning:I = 0x7f120fda

.field public static final global_locale_change_title:I = 0x7f120fdb

.field public static final global_stylus_summary:I = 0x7f120fdc

.field public static final global_stylus_title:I = 0x7f120fdd

.field public static final gmscore_settings_title:I = 0x7f120fde

.field public static final go_back_button_label:I = 0x7f120fdf

.field public static final go_to_sleep:I = 0x7f120fe0

.field public static final google_account_title:I = 0x7f120fe1

.field public static final google_assistant_guide_cancel:I = 0x7f120fe2

.field public static final google_assistant_guide_message:I = 0x7f120fe3

.field public static final google_assistant_guide_ok:I = 0x7f120fe4

.field public static final google_assistant_guide_tip:I = 0x7f120fe5

.field public static final google_assistant_guide_title:I = 0x7f120fe6

.field public static final google_assistant_label:I = 0x7f120fe7

.field public static final google_backup_enable:I = 0x7f120fe8

.field public static final google_backup_section_title:I = 0x7f120fe9

.field public static final google_pay:I = 0x7f120fea

.field public static final google_service_title:I = 0x7f120feb

.field public static final google_title:I = 0x7f120fec

.field public static final got_it:I = 0x7f120fed

.field public static final gps_description:I = 0x7f120fee

.field public static final gps_description_global:I = 0x7f120fef

.field public static final graphics_driver_all_apps_preference_title:I = 0x7f120ff0

.field public static final graphics_driver_app_preference_default:I = 0x7f120ff1

.field public static final graphics_driver_app_preference_prerelease_driver:I = 0x7f120ff2

.field public static final graphics_driver_app_preference_production_driver:I = 0x7f120ff3

.field public static final graphics_driver_app_preference_system:I = 0x7f120ff4

.field public static final graphics_driver_app_preference_title:I = 0x7f120ff5

.field public static final graphics_driver_dashboard_summary:I = 0x7f120ff6

.field public static final graphics_driver_dashboard_title:I = 0x7f120ff7

.field public static final graphics_driver_footer_text:I = 0x7f120ff8

.field public static final graphics_driver_main_switch_title:I = 0x7f120ff9

.field public static final group_active_devices:I = 0x7f120ffa

.field public static final group_apply_changes_dialog_title:I = 0x7f120ffb

.field public static final group_bonded_devices:I = 0x7f120ffc

.field public static final group_confirm_dialog_apply_button:I = 0x7f120ffd

.field public static final group_confirm_dialog_body:I = 0x7f120ffe

.field public static final group_connected_devices:I = 0x7f120fff

.field public static final group_help_url_all_connected_devices:I = 0x7f121000

.field public static final group_help_url_previously_connected_devices:I = 0x7f121001

.field public static final group_id:I = 0x7f121002

.field public static final group_options:I = 0x7f121003

.field public static final group_previously_connected_screen_title:I = 0x7f121004

.field public static final group_remove_source_message:I = 0x7f121005

.field public static final group_remove_source_title:I = 0x7f121006

.field public static final group_settings:I = 0x7f121007

.field public static final group_update_source_message:I = 0x7f121008

.field public static final group_update_source_title:I = 0x7f121009

.field public static final groupaudio_unpair_dialog_body:I = 0x7f12100a

.field public static final groupaudio_unpair_dialog_forget_confirm_button:I = 0x7f12100b

.field public static final groupaudio_unpair_dialog_title:I = 0x7f12100c

.field public static final gsm_to_wifi_connect_type_title:I = 0x7f12100d

.field public static final gsm_umts_network_preferences_dialogtitle:I = 0x7f12100e

.field public static final gsm_umts_network_preferences_title:I = 0x7f12100f

.field public static final guest_exit_guest:I = 0x7f121010

.field public static final guest_new_guest:I = 0x7f121011

.field public static final guest_remove_guest_confirm_button:I = 0x7f121012

.field public static final guest_remove_guest_dialog_title:I = 0x7f121013

.field public static final guest_reset_guest:I = 0x7f121014

.field public static final guest_reset_guest_confirm_button:I = 0x7f121015

.field public static final guest_reset_guest_dialog_title:I = 0x7f121016

.field public static final guest_resetting:I = 0x7f121017

.field public static final guide_button_text:I = 0x7f121018

.field public static final guide_text_center:I = 0x7f121019

.field public static final guide_text_left:I = 0x7f12101a

.field public static final guide_text_right:I = 0x7f12101b

.field public static final guide_title_center:I = 0x7f12101c

.field public static final guide_title_left:I = 0x7f12101d

.field public static final guide_title_right:I = 0x7f12101e

.field public static final gxzw_add_fingerprint_area:I = 0x7f12101f

.field public static final gxzw_add_fingerprint_continue:I = 0x7f121020

.field public static final gxzw_add_fingerprint_edge_message:I = 0x7f121021

.field public static final gxzw_add_fingerprint_edge_title:I = 0x7f121022

.field public static final gxzw_add_fingerprint_finish:I = 0x7f121023

.field public static final gxzw_add_fingerprint_indicate:I = 0x7f121024

.field public static final gxzw_add_fingerprint_know:I = 0x7f121025

.field public static final gxzw_add_fingerprint_message:I = 0x7f121026

.field public static final gxzw_add_fingerprint_move_indicate:I = 0x7f121027

.field public static final gxzw_add_fingerprint_move_message:I = 0x7f121028

.field public static final gxzw_add_fingerprint_move_title:I = 0x7f121029

.field public static final gxzw_add_fingerprint_put_finger_message:I = 0x7f12102a

.field public static final gxzw_add_fingerprint_put_finger_title:I = 0x7f12102b

.field public static final gxzw_add_fingerprint_title:I = 0x7f12102c

.field public static final gxzw_anim_aurora:I = 0x7f12102d

.field public static final gxzw_anim_dialog_cancel_enroll:I = 0x7f12102e

.field public static final gxzw_anim_dialog_enroll:I = 0x7f12102f

.field public static final gxzw_anim_dialog_message:I = 0x7f121030

.field public static final gxzw_anim_dialog_title:I = 0x7f121031

.field public static final gxzw_anim_light:I = 0x7f121032

.field public static final gxzw_anim_pulse:I = 0x7f121033

.field public static final gxzw_anim_rhythm:I = 0x7f121034

.field public static final gxzw_anim_star:I = 0x7f121035

.field public static final gxzw_anim_title:I = 0x7f121036

.field public static final gxzw_aod_show_summary:I = 0x7f121037

.field public static final gxzw_aod_show_tittle:I = 0x7f121038

.field public static final gxzw_dialog_message:I = 0x7f121039

.field public static final gxzw_dialog_not_show_again:I = 0x7f12103a

.field public static final gxzw_dialog_ok:I = 0x7f12103b

.field public static final gxzw_dialog_title:I = 0x7f12103c

.field public static final gxzw_enroll_again:I = 0x7f12103d

.field public static final gxzw_lowlight_open_summary:I = 0x7f12103e

.field public static final gxzw_lowlight_open_title:I = 0x7f12103f

.field public static final gxzw_quick_open_enable_summary:I = 0x7f121040

.field public static final gxzw_quick_open_enable_title:I = 0x7f121041

.field public static final gxzw_quick_open_fast_summary:I = 0x7f121042

.field public static final gxzw_quick_open_fast_title:I = 0x7f121043

.field public static final gxzw_quick_open_slow_summary:I = 0x7f121044

.field public static final gxzw_quick_open_slow_title:I = 0x7f121045

.field public static final gxzw_quick_open_title:I = 0x7f121046

.field public static final gxzw_vibrate_summary:I = 0x7f121047

.field public static final gxzw_vibrate_title:I = 0x7f121048

.field public static final handy_mode:I = 0x7f121049

.field public static final handy_mode_enable:I = 0x7f12104a

.field public static final handy_mode_hint:I = 0x7f12104b

.field public static final handy_mode_size:I = 0x7f12104c

.field public static final handy_mode_tips:I = 0x7f12104d

.field public static final handy_mode_tips_full_screen:I = 0x7f12104e

.field public static final haptic_agent_completion_01_content_description:I = 0x7f12104f

.field public static final haptic_cursor_movement_07_content_description:I = 0x7f121050

.field public static final haptic_descripthion_for_max_level:I = 0x7f121051

.field public static final haptic_descripthion_for_middle_level:I = 0x7f121052

.field public static final haptic_descripthion_for_min_level:I = 0x7f121053

.field public static final haptic_experience:I = 0x7f121054

.field public static final haptic_experience_details:I = 0x7f121055

.field public static final haptic_feedback_enable_title:I = 0x7f121056

.field public static final haptic_feedback_level:I = 0x7f121057

.field public static final haptic_feedback_progress:I = 0x7f121058

.field public static final haptic_feedback_summary:I = 0x7f121059

.field public static final haptic_feedback_title:I = 0x7f12105a

.field public static final haptic_fingerprint_unlock_16_content_description:I = 0x7f12105b

.field public static final haptic_generate_folder_08_content_description:I = 0x7f12105c

.field public static final haptic_generate_small_window_gestures_09_content_description:I = 0x7f12105d

.field public static final haptic_icon_sorting_12_content_description:I = 0x7f12105e

.field public static final haptic_index_bar_10_content_description:I = 0x7f12105f

.field public static final haptic_lock_screen_11_content_description:I = 0x7f121060

.field public static final haptic_multitasking_card_long_press_03_content_description:I = 0x7f121061

.field public static final haptic_multitasking_gestures_04_content_description:I = 0x7f121062

.field public static final haptic_preview:I = 0x7f121063

.field public static final haptic_return_gesture_05_content_description:I = 0x7f121064

.field public static final haptic_shutdown_menu_06_content_description:I = 0x7f121065

.field public static final haptic_single_selection_02_content_description:I = 0x7f121066

.field public static final haptic_switch_summary:I = 0x7f121067

.field public static final haptic_switch_title:I = 0x7f121068

.field public static final haptic_text_edit_popup_13_content_description:I = 0x7f121069

.field public static final haptic_upper_right_menu_15_content_description:I = 0x7f12106a

.field public static final haptic_video_content_description:I = 0x7f12106b

.field public static final haptic_volume_adjustment_14_content_description:I = 0x7f12106c

.field public static final hardkeyboard_category:I = 0x7f12106d

.field public static final hardware_revision:I = 0x7f12106e

.field public static final hardware_version:I = 0x7f12106f

.field public static final has_screenshot_sound:I = 0x7f121070

.field public static final hdcp_checking_dialog_title:I = 0x7f121071

.field public static final hdcp_checking_title:I = 0x7f121072

.field public static final hdmi_3d_display_enable_summary:I = 0x7f121073

.field public static final hdmi_3d_display_enable_title:I = 0x7f121074

.field public static final hdmi_3d_help_title:I = 0x7f121075

.field public static final hdmi_3d_stereo_separation_label:I = 0x7f121076

.field public static final hdmi_3dplay_control_disable_subtext:I = 0x7f121077

.field public static final hdmi_3dplay_control_enable_subtext:I = 0x7f121078

.field public static final hdmi_3dplay_control_text:I = 0x7f121079

.field public static final hdmi_3dplay_settings_title:I = 0x7f12107a

.field public static final hdmi_color_calibration_none:I = 0x7f12107b

.field public static final hdmi_color_calibration_srgb:I = 0x7f12107c

.field public static final header_account_settings:I = 0x7f12107d

.field public static final header_add_an_account:I = 0x7f12107e

.field public static final header_application_sync_settings:I = 0x7f12107f

.field public static final header_category_accounts:I = 0x7f121080

.field public static final header_category_system:I = 0x7f121081

.field public static final header_category_wireless_networks:I = 0x7f121082

.field public static final header_data_and_synchronization:I = 0x7f121083

.field public static final headphone_assisted_summary:I = 0x7f121084

.field public static final headphone_assisted_title:I = 0x7f121085

.field public static final headset_aac:I = 0x7f121086

.field public static final headset_ambient_sound:I = 0x7f121087

.field public static final headset_aptx_adapter:I = 0x7f121088

.field public static final headset_connectiong:I = 0x7f121089

.field public static final headset_find_both:I = 0x7f12108a

.field public static final headset_find_cancel:I = 0x7f12108b

.field public static final headset_find_connected:I = 0x7f12108c

.field public static final headset_find_disconnect:I = 0x7f12108d

.field public static final headset_find_handle_tip:I = 0x7f12108e

.field public static final headset_find_left:I = 0x7f12108f

.field public static final headset_find_notify_connected:I = 0x7f121090

.field public static final headset_find_notify_disconnect:I = 0x7f121091

.field public static final headset_find_play_warning:I = 0x7f121092

.field public static final headset_find_playing:I = 0x7f121093

.field public static final headset_find_query_play:I = 0x7f121094

.field public static final headset_find_right:I = 0x7f121095

.field public static final headset_find_tip:I = 0x7f121096

.field public static final headset_find_tip_title:I = 0x7f121097

.field public static final headset_find_title:I = 0x7f121098

.field public static final headset_find_voice_warning:I = 0x7f121099

.field public static final headset_head_tracking_desc:I = 0x7f12109a

.field public static final headset_head_tracking_title:I = 0x7f12109b

.field public static final headset_notification_switch_summary:I = 0x7f12109c

.field public static final headset_notification_switch_title:I = 0x7f12109d

.field public static final headset_remove_message:I = 0x7f12109e

.field public static final headset_remove_message_indicate:I = 0x7f12109f

.field public static final headset_sbc:I = 0x7f1210a0

.field public static final headset_settings_title:I = 0x7f1210a1

.field public static final headset_silent_upgrade_switch_summary:I = 0x7f1210a2

.field public static final headset_silent_upgrade_switch_title:I = 0x7f1210a3

.field public static final headset_spatial_audio:I = 0x7f1210a4

.field public static final headset_toast_tip_disconnect_others:I = 0x7f1210a5

.field public static final headset_unsaved_devices:I = 0x7f1210a6

.field public static final hearing_aids_category:I = 0x7f1210a7

.field public static final heavenly_stems_bing:I = 0x7f1210a8

.field public static final heavenly_stems_ding:I = 0x7f1210a9

.field public static final heavenly_stems_geng:I = 0x7f1210aa

.field public static final heavenly_stems_gui:I = 0x7f1210ab

.field public static final heavenly_stems_ji:I = 0x7f1210ac

.field public static final heavenly_stems_jia:I = 0x7f1210ad

.field public static final heavenly_stems_ren:I = 0x7f1210ae

.field public static final heavenly_stems_wu:I = 0x7f1210af

.field public static final heavenly_stems_xin:I = 0x7f1210b0

.field public static final heavenly_stems_yi:I = 0x7f1210b1

.field public static final heavy_weight_stop_description:I = 0x7f1210b2

.field public static final height_light_list_text:I = 0x7f1210b3

.field public static final help_and_feedback_title:I = 0x7f1210b4

.field public static final help_feedback_label:I = 0x7f1210b5

.field public static final help_label:I = 0x7f1210b6

.field public static final help_uri_5g_dsds:I = 0x7f1210b7

.field public static final help_uri_about:I = 0x7f1210b8

.field public static final help_uri_about_phone_v2:I = 0x7f1210b9

.field public static final help_uri_accessibility:I = 0x7f1210ba

.field public static final help_uri_accessibility_vibration:I = 0x7f1210bb

.field public static final help_uri_alarms_and_reminders:I = 0x7f1210bc

.field public static final help_uri_apps:I = 0x7f1210bd

.field public static final help_uri_apps_games:I = 0x7f1210be

.field public static final help_uri_apps_high_power:I = 0x7f1210bf

.field public static final help_uri_apps_manage_sources:I = 0x7f1210c0

.field public static final help_uri_apps_overlay:I = 0x7f1210c1

.field public static final help_uri_apps_storage:I = 0x7f1210c2

.field public static final help_uri_apps_wifi_access:I = 0x7f1210c3

.field public static final help_uri_apps_write_settings:I = 0x7f1210c4

.field public static final help_uri_beam:I = 0x7f1210c5

.field public static final help_uri_bluetooth_screen:I = 0x7f1210c6

.field public static final help_uri_default:I = 0x7f1210c7

.field public static final help_uri_display:I = 0x7f1210c8

.field public static final help_uri_interruptions:I = 0x7f1210c9

.field public static final help_uri_manage_external_storage:I = 0x7f1210ca

.field public static final help_uri_media_management_apps:I = 0x7f1210cb

.field public static final help_uri_nfc_and_payment_settings:I = 0x7f1210cc

.field public static final help_uri_notifications:I = 0x7f1210cd

.field public static final help_uri_other_sounds:I = 0x7f1210ce

.field public static final help_uri_prevent_ringing_gesture:I = 0x7f1210cf

.field public static final help_uri_printing:I = 0x7f1210d0

.field public static final help_uri_private_dns:I = 0x7f1210d1

.field public static final help_uri_process_stats_apps:I = 0x7f1210d2

.field public static final help_uri_process_stats_summary:I = 0x7f1210d3

.field public static final help_uri_restricted_apps:I = 0x7f1210d4

.field public static final help_uri_sim_combination_warning:I = 0x7f1210d5

.field public static final help_uri_smart_battery_settings:I = 0x7f1210d6

.field public static final help_uri_storage:I = 0x7f1210d7

.field public static final help_uri_wallpaper:I = 0x7f1210d8

.field public static final help_uri_wifi_calling:I = 0x7f1210d9

.field public static final help_uri_wifi_scanning_required:I = 0x7f1210da

.field public static final help_url_accessibility_shortcut:I = 0x7f1210db

.field public static final help_url_account_detail:I = 0x7f1210dc

.field public static final help_url_accounts:I = 0x7f1210dd

.field public static final help_url_action_disabled_by_it_admin:I = 0x7f1210de

.field public static final help_url_action_disabled_by_restricted_settings:I = 0x7f1210df

.field public static final help_url_adaptive_sleep:I = 0x7f1210e0

.field public static final help_url_app_usage_settings:I = 0x7f1210e1

.field public static final help_url_apps_and_notifications:I = 0x7f1210e2

.field public static final help_url_audio_accessory_not_supported:I = 0x7f1210e3

.field public static final help_url_auto_brightness:I = 0x7f1210e4

.field public static final help_url_auto_rotate_settings:I = 0x7f1210e5

.field public static final help_url_autoclick:I = 0x7f1210e6

.field public static final help_url_backup_reset:I = 0x7f1210e7

.field public static final help_url_battery:I = 0x7f1210e8

.field public static final help_url_battery_defender:I = 0x7f1210e9

.field public static final help_url_battery_missing:I = 0x7f1210ea

.field public static final help_url_battery_saver_settings:I = 0x7f1210eb

.field public static final help_url_bluetooth:I = 0x7f1210ec

.field public static final help_url_caption:I = 0x7f1210ed

.field public static final help_url_choose_lockscreen:I = 0x7f1210ee

.field public static final help_url_color_correction:I = 0x7f1210ef

.field public static final help_url_color_inversion:I = 0x7f1210f0

.field public static final help_url_connected_devices:I = 0x7f1210f1

.field public static final help_url_dark_theme:I = 0x7f1210f2

.field public static final help_url_data_saver:I = 0x7f1210f3

.field public static final help_url_data_usage:I = 0x7f1210f4

.field public static final help_url_display_size:I = 0x7f1210f5

.field public static final help_url_double_tap_screen:I = 0x7f1210f6

.field public static final help_url_dreams:I = 0x7f1210f7

.field public static final help_url_encryption:I = 0x7f1210f8

.field public static final help_url_face:I = 0x7f1210f9

.field public static final help_url_fingerprint:I = 0x7f1210fa

.field public static final help_url_font_size:I = 0x7f1210fb

.field public static final help_url_gestures:I = 0x7f1210fc

.field public static final help_url_icc_lock:I = 0x7f1210fd

.field public static final help_url_insecure_vpn:I = 0x7f1210fe

.field public static final help_url_install_certificate:I = 0x7f1210ff

.field public static final help_url_location_access:I = 0x7f121100

.field public static final help_url_lockscreen:I = 0x7f121101

.field public static final help_url_magnification:I = 0x7f121102

.field public static final help_url_manage_storage:I = 0x7f121103

.field public static final help_url_more_networks:I = 0x7f121104

.field public static final help_url_network_dashboard:I = 0x7f121105

.field public static final help_url_nfc_payment:I = 0x7f121106

.field public static final help_url_night_display:I = 0x7f121107

.field public static final help_url_pickup_gesture:I = 0x7f121108

.field public static final help_url_previously_connected_devices:I = 0x7f121109

.field public static final help_url_previously_connected_group_devices:I = 0x7f12110a

.field public static final help_url_privacy_dashboard:I = 0x7f12110b

.field public static final help_url_remote_display:I = 0x7f12110c

.field public static final help_url_screen_pinning:I = 0x7f12110d

.field public static final help_url_screen_saver:I = 0x7f12110e

.field public static final help_url_security:I = 0x7f12110f

.field public static final help_url_sound:I = 0x7f121110

.field public static final help_url_storage_dashboard:I = 0x7f121111

.field public static final help_url_system_dashboard:I = 0x7f121112

.field public static final help_url_tether:I = 0x7f121113

.field public static final help_url_timeout:I = 0x7f121114

.field public static final help_url_trust_agent:I = 0x7f121115

.field public static final help_url_unrestricted_data_access:I = 0x7f121116

.field public static final help_url_upgrading:I = 0x7f121117

.field public static final help_url_usage_access:I = 0x7f121118

.field public static final help_url_usb_contaminant_detected:I = 0x7f121119

.field public static final help_url_user_and_account_dashboard:I = 0x7f12111a

.field public static final help_url_users:I = 0x7f12111b

.field public static final help_url_vpn:I = 0x7f12111c

.field public static final help_url_wifi:I = 0x7f12111d

.field public static final help_url_wifi_p2p:I = 0x7f12111e

.field public static final hidden_ssid_label:I = 0x7f12111f

.field public static final hide_bottom_view_on_scroll_behavior:I = 0x7f121120

.field public static final hide_extra_apps:I = 0x7f121121

.field public static final hide_silent_icons_summary:I = 0x7f121122

.field public static final hide_silent_icons_title:I = 0x7f121123

.field public static final high_brightness_hint_summary:I = 0x7f121124

.field public static final high_label:I = 0x7f121125

.field public static final high_performance_notif_summary:I = 0x7f121126

.field public static final high_performance_notif_title:I = 0x7f121127

.field public static final high_performance_title:I = 0x7f121128

.field public static final high_power_apps:I = 0x7f121129

.field public static final high_power_desc:I = 0x7f12112a

.field public static final high_power_filter_on:I = 0x7f12112b

.field public static final high_power_off:I = 0x7f12112c

.field public static final high_power_on:I = 0x7f12112d

.field public static final high_power_prompt_body:I = 0x7f12112e

.field public static final high_power_prompt_title:I = 0x7f12112f

.field public static final high_power_system:I = 0x7f121130

.field public static final hint_unadjustable_text:I = 0x7f121131

.field public static final history_details_title:I = 0x7f121132

.field public static final history_toggled_on_summary:I = 0x7f121133

.field public static final history_toggled_on_title:I = 0x7f121134

.field public static final home:I = 0x7f121135

.field public static final home_app:I = 0x7f121136

.field public static final home_key:I = 0x7f121137

.field public static final home_title:I = 0x7f121138

.field public static final home_work_profile_not_supported:I = 0x7f121139

.field public static final homepage_all_settings:I = 0x7f12113a

.field public static final homepage_condition_footer_content_description:I = 0x7f12113b

.field public static final homepage_personal_settings:I = 0x7f12113c

.field public static final how_adjust_font_size:I = 0x7f12113d

.field public static final how_backup_content:I = 0x7f12113e

.field public static final how_backup_title:I = 0x7f12113f

.field public static final how_encrypt_content:I = 0x7f121140

.field public static final how_encrypt_title:I = 0x7f121141

.field public static final how_restore_backup_content:I = 0x7f121142

.field public static final how_restore_backup_title:I = 0x7f121143

.field public static final http_invoke_app:I = 0x7f121144

.field public static final http_invoke_app_summary:I = 0x7f121145

.field public static final huanji_history_title:I = 0x7f121146

.field public static final hwui_force_dark_summary:I = 0x7f121147

.field public static final hwui_force_dark_title:I = 0x7f121148

.field public static final icon_content_description:I = 0x7f121149

.field public static final ignore_optimizations_off:I = 0x7f12114a

.field public static final ignore_optimizations_off_desc:I = 0x7f12114b

.field public static final ignore_optimizations_on:I = 0x7f12114c

.field public static final ignore_optimizations_on_desc:I = 0x7f12114d

.field public static final ignore_optimizations_title:I = 0x7f12114e

.field public static final ime_security_warning:I = 0x7f12114f

.field public static final ime_security_warning_global:I = 0x7f121150

.field public static final ime_security_warning_title_global:I = 0x7f121151

.field public static final imei_information_summary:I = 0x7f121152

.field public static final imei_information_title:I = 0x7f121153

.field public static final imei_multi_sim:I = 0x7f121154

.field public static final imitate_real_keyboard:I = 0x7f121155

.field public static final imitate_real_keyboard_summary:I = 0x7f121156

.field public static final immediately_destroy_activities:I = 0x7f121157

.field public static final immediately_destroy_activities_summary:I = 0x7f121158

.field public static final immersive_audio_sources:I = 0x7f121159

.field public static final importance_default:I = 0x7f12115a

.field public static final importance_high:I = 0x7f12115b

.field public static final importance_low:I = 0x7f12115c

.field public static final importance_min:I = 0x7f12115d

.field public static final importance_title:I = 0x7f12115e

.field public static final important_bubble:I = 0x7f12115f

.field public static final important_conversation_behavior_summary:I = 0x7f121160

.field public static final important_conversations:I = 0x7f121161

.field public static final important_conversations_summary:I = 0x7f121162

.field public static final important_conversations_summary_bubbles:I = 0x7f121163

.field public static final ims_reg_status_not_registered:I = 0x7f121164

.field public static final ims_reg_status_registered:I = 0x7f121165

.field public static final ims_reg_title:I = 0x7f121166

.field public static final inactive_app_active_summary:I = 0x7f121167

.field public static final inactive_app_inactive_summary:I = 0x7f121168

.field public static final inactive_apps_title:I = 0x7f121169

.field public static final incall_show:I = 0x7f12116a

.field public static final include_app_data_title:I = 0x7f12116b

.field public static final incoming_call_breathing_light_color_title:I = 0x7f12116c

.field public static final incoming_call_breathing_light_freq_title:I = 0x7f12116d

.field public static final incoming_call_volume_title:I = 0x7f12116e

.field public static final infinity_display_settings:I = 0x7f12116f

.field public static final infinity_display_slogan:I = 0x7f121170

.field public static final infinity_display_title:I = 0x7f121171

.field public static final information_dialog_button_text:I = 0x7f121172

.field public static final information_label:I = 0x7f121173

.field public static final ingress_rate_limit_dialog_title:I = 0x7f121174

.field public static final ingress_rate_limit_no_limit_entry:I = 0x7f121175

.field public static final ingress_rate_limit_summary:I = 0x7f121176

.field public static final ingress_rate_limit_title:I = 0x7f121177

.field public static final input_assistance:I = 0x7f121178

.field public static final input_device_category:I = 0x7f121179

.field public static final input_method:I = 0x7f12117a

.field public static final input_method_bottom_close:I = 0x7f12117b

.field public static final input_method_bottom_open:I = 0x7f12117c

.field public static final input_method_category:I = 0x7f12117d

.field public static final input_method_clipboard_settings:I = 0x7f12117e

.field public static final input_method_cloud_data_title:I = 0x7f12117f

.field public static final input_method_cloud_paste_mode_bubble_des:I = 0x7f121180

.field public static final input_method_cloud_paste_mode_bubble_title:I = 0x7f121181

.field public static final input_method_cloud_paste_mode_no_des:I = 0x7f121182

.field public static final input_method_cloud_paste_mode_no_title:I = 0x7f121183

.field public static final input_method_cloud_paste_mode_red_point_des:I = 0x7f121184

.field public static final input_method_cloud_paste_mode_red_point_title:I = 0x7f121185

.field public static final input_method_function_clipboard:I = 0x7f121186

.field public static final input_method_function_clipboard_des:I = 0x7f121187

.field public static final input_method_function_no_function:I = 0x7f121188

.field public static final input_method_function_no_function_des:I = 0x7f121189

.field public static final input_method_function_switch:I = 0x7f12118a

.field public static final input_method_function_switch_des:I = 0x7f12118b

.field public static final input_method_function_switch_keyboard_language:I = 0x7f12118c

.field public static final input_method_function_switch_keyboard_language_des:I = 0x7f12118d

.field public static final input_method_function_switch_keyboard_type:I = 0x7f12118e

.field public static final input_method_function_switch_keyboard_type_des:I = 0x7f12118f

.field public static final input_method_function_voice:I = 0x7f121190

.field public static final input_method_function_voice_des:I = 0x7f121191

.field public static final input_method_function_xiaoai:I = 0x7f121192

.field public static final input_method_function_xiaoai_des:I = 0x7f121193

.field public static final input_method_manage:I = 0x7f121194

.field public static final input_method_middle_function_move_cursor:I = 0x7f121195

.field public static final input_method_middle_function_move_cursor_des:I = 0x7f121196

.field public static final input_method_middle_function_no_function_des:I = 0x7f121197

.field public static final input_method_quick_access:I = 0x7f121198

.field public static final input_method_quick_cloud_mode_title:I = 0x7f121199

.field public static final input_method_quick_cloud_summary:I = 0x7f12119a

.field public static final input_method_quick_cloud_tips:I = 0x7f12119b

.field public static final input_method_quick_cloud_title:I = 0x7f12119c

.field public static final input_method_quick_paste_summary:I = 0x7f12119d

.field public static final input_method_quick_paste_title:I = 0x7f12119e

.field public static final input_method_quick_taobao_summary:I = 0x7f12119f

.field public static final input_method_quick_taobao_title:I = 0x7f1211a0

.field public static final input_method_quick_url_summary:I = 0x7f1211a1

.field public static final input_method_quick_url_title:I = 0x7f1211a2

.field public static final input_method_selector:I = 0x7f1211a3

.field public static final input_method_selector_always_hide_title:I = 0x7f1211a4

.field public static final input_method_selector_always_hide_value:I = 0x7f1211a5

.field public static final input_method_selector_always_show_title:I = 0x7f1211a6

.field public static final input_method_selector_always_show_value:I = 0x7f1211a7

.field public static final input_method_selector_show_automatically_title:I = 0x7f1211a8

.field public static final input_method_selector_show_automatically_value:I = 0x7f1211a9

.field public static final input_method_selector_visibility_default_value:I = 0x7f1211aa

.field public static final input_method_settings:I = 0x7f1211ab

.field public static final input_method_settings_button:I = 0x7f1211ac

.field public static final input_method_settings_text:I = 0x7f1211ad

.field public static final input_methods_and_subtype_enabler_title:I = 0x7f1211ae

.field public static final input_methods_settings_label_format:I = 0x7f1211af

.field public static final input_methods_settings_title:I = 0x7f1211b0

.field public static final input_pattern_hint_text:I = 0x7f1211b1

.field public static final input_settings:I = 0x7f1211b2

.field public static final input_text:I = 0x7f1211b3

.field public static final inputdebuglog_select_level:I = 0x7f1211b4

.field public static final inputdebuglog_select_level_show:I = 0x7f1211b5

.field public static final inputdebuglog_select_level_summary:I = 0x7f1211b6

.field public static final install_all_warning:I = 0x7f1211b7

.field public static final install_applications:I = 0x7f1211b8

.field public static final install_applications_title:I = 0x7f1211b9

.field public static final install_no_market_app_summary:I = 0x7f1211ba

.field public static final install_no_market_app_title:I = 0x7f1211bb

.field public static final install_other_apps:I = 0x7f1211bc

.field public static final install_preinstall_app:I = 0x7f1211bd

.field public static final install_text:I = 0x7f1211be

.field public static final install_type_instant:I = 0x7f1211bf

.field public static final install_whole_rom:I = 0x7f1211c0

.field public static final installed:I = 0x7f1211c1

.field public static final instant_app_details_summary:I = 0x7f1211c2

.field public static final instant_apps_settings:I = 0x7f1211c3

.field public static final instruction_privacy_password:I = 0x7f1211c4

.field public static final instruction_title:I = 0x7f1211c5

.field public static final insufficient_storage:I = 0x7f1211c6

.field public static final intelligence_suppression_summary:I = 0x7f1211c7

.field public static final intelligence_suppression_title:I = 0x7f1211c8

.field public static final intelligent_keep_screen_on:I = 0x7f1211c9

.field public static final intelligent_keep_screen_on_summary:I = 0x7f1211ca

.field public static final intent_sender_account_label:I = 0x7f1211cb

.field public static final intent_sender_action_label:I = 0x7f1211cc

.field public static final intent_sender_data_label:I = 0x7f1211cd

.field public static final intent_sender_resource_label:I = 0x7f1211ce

.field public static final intent_sender_sendbroadcast_text:I = 0x7f1211cf

.field public static final intent_sender_startactivity_text:I = 0x7f1211d0

.field public static final interact_across_profiles_app_detail_title:I = 0x7f1211d1

.field public static final interact_across_profiles_consent_dialog_app_data_summary:I = 0x7f1211d2

.field public static final interact_across_profiles_consent_dialog_app_data_title:I = 0x7f1211d3

.field public static final interact_across_profiles_consent_dialog_permissions_summary:I = 0x7f1211d4

.field public static final interact_across_profiles_consent_dialog_permissions_title:I = 0x7f1211d5

.field public static final interact_across_profiles_consent_dialog_summary:I = 0x7f1211d6

.field public static final interact_across_profiles_consent_dialog_title:I = 0x7f1211d7

.field public static final interact_across_profiles_empty_text:I = 0x7f1211d8

.field public static final interact_across_profiles_install_app_summary:I = 0x7f1211d9

.field public static final interact_across_profiles_install_personal_app_title:I = 0x7f1211da

.field public static final interact_across_profiles_install_work_app_title:I = 0x7f1211db

.field public static final interact_across_profiles_keywords:I = 0x7f1211dc

.field public static final interact_across_profiles_number_of_connected_apps_none:I = 0x7f1211dd

.field public static final interact_across_profiles_summary_1:I = 0x7f1211de

.field public static final interact_across_profiles_summary_2:I = 0x7f1211df

.field public static final interact_across_profiles_summary_3:I = 0x7f1211e0

.field public static final interact_across_profiles_summary_allowed:I = 0x7f1211e1

.field public static final interact_across_profiles_summary_not_allowed:I = 0x7f1211e2

.field public static final interact_across_profiles_switch_disabled:I = 0x7f1211e3

.field public static final interact_across_profiles_switch_enabled:I = 0x7f1211e4

.field public static final interact_across_profiles_title:I = 0x7f1211e5

.field public static final interaction_control_category_title:I = 0x7f1211e6

.field public static final interconnection:I = 0x7f1211e7

.field public static final interesting_text:I = 0x7f1211e8

.field public static final interesting_title:I = 0x7f1211e9

.field public static final internal_auto_rotate_title:I = 0x7f1211ea

.field public static final internal_memory:I = 0x7f1211eb

.field public static final internal_storage:I = 0x7f1211ec

.field public static final internet_connectivity_panel_title:I = 0x7f1211ed

.field public static final invalid_location:I = 0x7f1211ee

.field public static final invalid_size_value:I = 0x7f1211ef

.field public static final ipaddr_label:I = 0x7f1211f0

.field public static final item_system_app:I = 0x7f1211f1

.field public static final item_view_role_description:I = 0x7f1211f2

.field public static final join_two_items:I = 0x7f1211f3

.field public static final join_two_unrelated_items:I = 0x7f1211f4

.field public static final kddi_nfc_category_title:I = 0x7f1211f5

.field public static final kddi_nfc_disabled_summary:I = 0x7f1211f6

.field public static final kddi_nfc_quick_toggle_summary:I = 0x7f1211f7

.field public static final kddi_nfc_quick_toggle_title:I = 0x7f1211f8

.field public static final kddi_nfc_secure_settings_title:I = 0x7f1211f9

.field public static final kddi_nfc_secure_toggle_summary:I = 0x7f1211fa

.field public static final keep_legacy_fingerprint:I = 0x7f1211fb

.field public static final keep_screen_on:I = 0x7f1211fc

.field public static final keep_screen_on_after_folding_off_summary:I = 0x7f1211fd

.field public static final keep_screen_on_after_folding_on_summary:I = 0x7f1211fe

.field public static final keep_screen_on_after_folding_text:I = 0x7f1211ff

.field public static final keep_screen_on_after_folding_title:I = 0x7f121200

.field public static final keep_screen_on_summary:I = 0x7f121201

.field public static final kernel_version:I = 0x7f121202

.field public static final key_combination_power_back:I = 0x7f121203

.field public static final key_combination_power_home:I = 0x7f121204

.field public static final key_combination_power_menu:I = 0x7f121205

.field public static final key_combination_power_volume_down:I = 0x7f121206

.field public static final key_fsg_dialog_message:I = 0x7f121207

.field public static final key_fsg_dialog_negative:I = 0x7f121208

.field public static final key_fsg_dialog_positive:I = 0x7f121209

.field public static final key_gesture_function_dialog_message:I = 0x7f12120a

.field public static final key_gesture_function_dialog_negative:I = 0x7f12120b

.field public static final key_gesture_function_dialog_positive:I = 0x7f12120c

.field public static final key_gesture_function_optional:I = 0x7f12120d

.field public static final key_gesture_function_preview:I = 0x7f12120e

.field public static final key_gesture_function_shortcut:I = 0x7f12120f

.field public static final key_handwriting_input_summary:I = 0x7f121210

.field public static final key_handwriting_input_title:I = 0x7f121211

.field public static final key_none:I = 0x7f121212

.field public static final key_operation_diagram:I = 0x7f121213

.field public static final key_position_cat:I = 0x7f121214

.field public static final key_settings:I = 0x7f121215

.field public static final key_settings_title:I = 0x7f121216

.field public static final key_shortcut_settings_title:I = 0x7f121217

.field public static final key_system_control:I = 0x7f121218

.field public static final keyboard_and_input_methods_category:I = 0x7f121219

.field public static final keyboard_assistance_category:I = 0x7f12121a

.field public static final keyboard_disconnected:I = 0x7f12121b

.field public static final keyboard_funtion_key:I = 0x7f12121c

.field public static final keyboard_layout_default_label:I = 0x7f12121d

.field public static final keyboard_layout_dialog_setup_button:I = 0x7f12121e

.field public static final keyboard_layout_dialog_switch_hint:I = 0x7f12121f

.field public static final keyboard_layout_dialog_title:I = 0x7f121220

.field public static final keyboard_layout_picker_title:I = 0x7f121221

.field public static final keyboard_mouse_touch:I = 0x7f121222

.field public static final keyboard_select:I = 0x7f121223

.field public static final keyboard_select_summary:I = 0x7f121224

.field public static final keyboard_settings_category:I = 0x7f121225

.field public static final keyboard_settings_title:I = 0x7f121226

.field public static final keyboard_shortcuts_helper:I = 0x7f121227

.field public static final keyboard_shortcuts_helper_summary:I = 0x7f121228

.field public static final keyboard_skin_follow_system_enable:I = 0x7f121229

.field public static final keyguard_advance_setting_title:I = 0x7f12122a

.field public static final keyguard_security_setting_others:I = 0x7f12122b

.field public static final keyguard_security_setting_privacy_policy:I = 0x7f12122c

.field public static final keyguard_setting_privacy_policy_url:I = 0x7f12122d

.field public static final keywords_accessibility:I = 0x7f12122e

.field public static final keywords_accessibility_menu:I = 0x7f12122f

.field public static final keywords_accessibility_vibration_primary_switch:I = 0x7f121230

.field public static final keywords_accounts:I = 0x7f121231

.field public static final keywords_adb_wireless:I = 0x7f121232

.field public static final keywords_add_bt_device:I = 0x7f121233

.field public static final keywords_add_language:I = 0x7f121234

.field public static final keywords_airplane:I = 0x7f121235

.field public static final keywords_airplane_safe_networks:I = 0x7f121236

.field public static final keywords_alarm_vibration:I = 0x7f121237

.field public static final keywords_alarms_and_reminders:I = 0x7f121238

.field public static final keywords_all_apps:I = 0x7f121239

.field public static final keywords_always_show_time_info:I = 0x7f12123a

.field public static final keywords_ambient_display:I = 0x7f12123b

.field public static final keywords_ambient_display_screen:I = 0x7f12123c

.field public static final keywords_android_version:I = 0x7f12123d

.field public static final keywords_app:I = 0x7f12123e

.field public static final keywords_app_default:I = 0x7f12123f

.field public static final keywords_app_permissions:I = 0x7f121240

.field public static final keywords_app_pinning:I = 0x7f121241

.field public static final keywords_applications_settings:I = 0x7f121242

.field public static final keywords_assist_gesture_launch:I = 0x7f121243

.field public static final keywords_assist_input:I = 0x7f121244

.field public static final keywords_audio_description:I = 0x7f121245

.field public static final keywords_auto_click:I = 0x7f121246

.field public static final keywords_auto_rotate:I = 0x7f121247

.field public static final keywords_backup:I = 0x7f121248

.field public static final keywords_backup_calling:I = 0x7f121249

.field public static final keywords_battery:I = 0x7f12124a

.field public static final keywords_battery_adaptive_preferences:I = 0x7f12124b

.field public static final keywords_battery_saver:I = 0x7f12124c

.field public static final keywords_battery_saver_schedule:I = 0x7f12124d

.field public static final keywords_battery_saver_sticky:I = 0x7f12124e

.field public static final keywords_battery_usage:I = 0x7f12124f

.field public static final keywords_biometric_settings:I = 0x7f121250

.field public static final keywords_biometric_unlock:I = 0x7f121251

.field public static final keywords_bluetooth:I = 0x7f121252

.field public static final keywords_bluetooth_settings:I = 0x7f121253

.field public static final keywords_bold_text:I = 0x7f121254

.field public static final keywords_button_navigation_settings:I = 0x7f121255

.field public static final keywords_change_wifi_state:I = 0x7f121256

.field public static final keywords_color_correction:I = 0x7f121257

.field public static final keywords_color_inversion:I = 0x7f121258

.field public static final keywords_color_mode:I = 0x7f121259

.field public static final keywords_color_temperature:I = 0x7f12125a

.field public static final keywords_content_capture:I = 0x7f12125b

.field public static final keywords_dark_ui_mode:I = 0x7f12125c

.field public static final keywords_data:I = 0x7f12125d

.field public static final keywords_date_and_time:I = 0x7f12125e

.field public static final keywords_default_apps:I = 0x7f12125f

.field public static final keywords_default_browser:I = 0x7f121260

.field public static final keywords_default_links:I = 0x7f121261

.field public static final keywords_default_payment_app:I = 0x7f121262

.field public static final keywords_default_phone_app:I = 0x7f121263

.field public static final keywords_device_feedback:I = 0x7f121264

.field public static final keywords_display:I = 0x7f121265

.field public static final keywords_display_adaptive_sleep:I = 0x7f121266

.field public static final keywords_display_auto_brightness:I = 0x7f121267

.field public static final keywords_display_brightness_level:I = 0x7f121268

.field public static final keywords_display_cast_screen:I = 0x7f121269

.field public static final keywords_display_font_size:I = 0x7f12126a

.field public static final keywords_display_night_display:I = 0x7f12126b

.field public static final keywords_display_size:I = 0x7f12126c

.field public static final keywords_display_wallpaper:I = 0x7f12126d

.field public static final keywords_do_not_disturb:I = 0x7f12126e

.field public static final keywords_draw_overlay:I = 0x7f12126f

.field public static final keywords_emergency_app:I = 0x7f121270

.field public static final keywords_enhance_4g_lte:I = 0x7f121271

.field public static final keywords_face_settings:I = 0x7f121272

.field public static final keywords_face_unlock:I = 0x7f121273

.field public static final keywords_factory_data_reset:I = 0x7f121274

.field public static final keywords_fingerprint_settings:I = 0x7f121275

.field public static final keywords_flashlight:I = 0x7f121276

.field public static final keywords_font_size:I = 0x7f121277

.field public static final keywords_gesture:I = 0x7f121278

.field public static final keywords_gesture_navigation_settings:I = 0x7f121279

.field public static final keywords_gps:I = 0x7f12127a

.field public static final keywords_hearing_aids:I = 0x7f12127b

.field public static final keywords_home:I = 0x7f12127c

.field public static final keywords_hotspot:I = 0x7f12127d

.field public static final keywords_hotspot_tethering:I = 0x7f12127e

.field public static final keywords_ignore_optimizations:I = 0x7f12127f

.field public static final keywords_imei_info:I = 0x7f121280

.field public static final keywords_install_other_apps:I = 0x7f121281

.field public static final keywords_internet:I = 0x7f121282

.field public static final keywords_keyboard_and_ime:I = 0x7f121283

.field public static final keywords_live_caption:I = 0x7f121284

.field public static final keywords_live_transcribe:I = 0x7f121285

.field public static final keywords_location:I = 0x7f121286

.field public static final keywords_lock_screen_notif:I = 0x7f121287

.field public static final keywords_lockscreen:I = 0x7f121288

.field public static final keywords_lockscreen_bypass:I = 0x7f121289

.field public static final keywords_magnification:I = 0x7f12128a

.field public static final keywords_media_controls:I = 0x7f12128b

.field public static final keywords_media_management_apps:I = 0x7f12128c

.field public static final keywords_media_vibration:I = 0x7f12128d

.field public static final keywords_model_and_hardware:I = 0x7f12128e

.field public static final keywords_more_default_sms_app:I = 0x7f12128f

.field public static final keywords_more_mobile_networks:I = 0x7f121290

.field public static final keywords_network_reset:I = 0x7f121291

.field public static final keywords_notification_vibration:I = 0x7f121292

.field public static final keywords_nr_advanced_calling:I = 0x7f121293

.field public static final keywords_one_handed:I = 0x7f121294

.field public static final keywords_paper_mode:I = 0x7f121295

.field public static final keywords_payment_settings:I = 0x7f121296

.field public static final keywords_printing:I = 0x7f121297

.field public static final keywords_profile_challenge:I = 0x7f121298

.field public static final keywords_quick_ball:I = 0x7f121299

.field public static final keywords_ramping_ringer_vibration:I = 0x7f12129a

.field public static final keywords_reduce_bright_colors:I = 0x7f12129b

.field public static final keywords_reset_apps:I = 0x7f12129c

.field public static final keywords_ring_vibration:I = 0x7f12129d

.field public static final keywords_rtt:I = 0x7f12129e

.field public static final keywords_screen_resolution:I = 0x7f12129f

.field public static final keywords_screen_timeout:I = 0x7f1212a0

.field public static final keywords_select_to_speak:I = 0x7f1212a1

.field public static final keywords_silent:I = 0x7f1212a2

.field public static final keywords_sim_status:I = 0x7f1212a3

.field public static final keywords_sound_amplifier:I = 0x7f1212a4

.field public static final keywords_sound_notifications:I = 0x7f1212a5

.field public static final keywords_sounds:I = 0x7f1212a6

.field public static final keywords_sounds_and_notifications_interruptions:I = 0x7f1212a7

.field public static final keywords_spell_checker:I = 0x7f1212a8

.field public static final keywords_storage:I = 0x7f1212a9

.field public static final keywords_storage_files:I = 0x7f1212aa

.field public static final keywords_storage_menu_free:I = 0x7f1212ab

.field public static final keywords_storage_settings:I = 0x7f1212ac

.field public static final keywords_styles:I = 0x7f1212ad

.field public static final keywords_switch_access:I = 0x7f1212ae

.field public static final keywords_sync:I = 0x7f1212af

.field public static final keywords_system_navigation:I = 0x7f1212b0

.field public static final keywords_system_update_settings:I = 0x7f1212b1

.field public static final keywords_systemui_theme:I = 0x7f1212b2

.field public static final keywords_talkback:I = 0x7f1212b3

.field public static final keywords_text_to_speech_output:I = 0x7f1212b4

.field public static final keywords_time_format:I = 0x7f1212b5

.field public static final keywords_time_zone:I = 0x7f1212b6

.field public static final keywords_torch:I = 0x7f1212b7

.field public static final keywords_touch_vibration:I = 0x7f1212b8

.field public static final keywords_unification:I = 0x7f1212b9

.field public static final keywords_users:I = 0x7f1212ba

.field public static final keywords_vibrate:I = 0x7f1212bb

.field public static final keywords_vibrate_for_calls:I = 0x7f1212bc

.field public static final keywords_vibration:I = 0x7f1212bd

.field public static final keywords_virtual_keyboard:I = 0x7f1212be

.field public static final keywords_voice_access:I = 0x7f1212bf

.field public static final keywords_voice_input:I = 0x7f1212c0

.field public static final keywords_vr_listener:I = 0x7f1212c1

.field public static final keywords_wallet:I = 0x7f1212c2

.field public static final keywords_wallpaper:I = 0x7f1212c3

.field public static final keywords_wifi:I = 0x7f1212c4

.field public static final keywords_wifi_calling:I = 0x7f1212c5

.field public static final keywords_wifi_data_usage:I = 0x7f1212c6

.field public static final keywords_wifi_display_settings:I = 0x7f1212c7

.field public static final keywords_wifi_notify_open_networks:I = 0x7f1212c8

.field public static final keywords_write_settings:I = 0x7f1212c9

.field public static final keywords_zen_mode_settings:I = 0x7f1212ca

.field public static final kid_space_forbidden_set_unlock_password_msg:I = 0x7f1212cb

.field public static final kid_space_settings:I = 0x7f1212cc

.field public static final kilobyteShort:I = 0x7f1212cd

.field public static final kilobyte_per_second:I = 0x7f1212ce

.field public static final knock_edge_area_invalid:I = 0x7f1212cf

.field public static final knock_guide_dialog_title:I = 0x7f1212d0

.field public static final knock_guide_drive_desc:I = 0x7f1212d1

.field public static final knock_guide_launch_desc:I = 0x7f1212d2

.field public static final knock_guide_set_screenshot:I = 0x7f1212d3

.field public static final knock_guide_set_voiceassist:I = 0x7f1212d4

.field public static final knock_guide_sub_title:I = 0x7f1212d5

.field public static final knock_guide_title:I = 0x7f1212d6

.field public static final knock_long_press_horizontal_slid:I = 0x7f1212d7

.field public static final knock_slide_shape:I = 0x7f1212d8

.field public static final knock_slide_v:I = 0x7f1212d9

.field public static final label_available:I = 0x7f1212da

.field public static final label_mi_stats:I = 0x7f1212db

.field public static final language_and_input_for_work_category_title:I = 0x7f1212dc

.field public static final language_empty_list_user_restricted:I = 0x7f1212dd

.field public static final language_input_gesture_summary_off:I = 0x7f1212de

.field public static final language_input_gesture_summary_on_non_assist:I = 0x7f1212df

.field public static final language_input_gesture_summary_on_with_assist:I = 0x7f1212e0

.field public static final language_input_gesture_title:I = 0x7f1212e1

.field public static final language_keyboard_settings_title:I = 0x7f1212e2

.field public static final language_picker_title:I = 0x7f1212e3

.field public static final language_settings:I = 0x7f1212e4

.field public static final last_synced:I = 0x7f1212e5

.field public static final last_time_used_label:I = 0x7f1212e6

.field public static final lattice_smartcover_title:I = 0x7f1212e7

.field public static final launch_ai_shortcut:I = 0x7f1212e8

.field public static final launch_alipay_health_code:I = 0x7f1212e9

.field public static final launch_alipay_health_code_auto_close:I = 0x7f1212ea

.field public static final launch_alipay_payment_code:I = 0x7f1212eb

.field public static final launch_alipay_scanner:I = 0x7f1212ec

.field public static final launch_by_default:I = 0x7f1212ed

.field public static final launch_calculator:I = 0x7f1212ee

.field public static final launch_camera:I = 0x7f1212ef

.field public static final launch_control_center:I = 0x7f1212f0

.field public static final launch_defaults_none:I = 0x7f1212f1

.field public static final launch_defaults_some:I = 0x7f1212f2

.field public static final launch_google_search:I = 0x7f1212f3

.field public static final launch_instant_app:I = 0x7f1212f4

.field public static final launch_mdp_app_text:I = 0x7f1212f5

.field public static final launch_noapp_dialog_cancel:I = 0x7f1212f6

.field public static final launch_noapp_dialog_install:I = 0x7f1212f7

.field public static final launch_noapp_dialog_replace:I = 0x7f1212f8

.field public static final launch_notification_center:I = 0x7f1212f9

.field public static final launch_recents:I = 0x7f1212fa

.field public static final launch_smarthome:I = 0x7f1212fb

.field public static final launch_voice_assistant:I = 0x7f1212fc

.field public static final launch_wechat_health_code:I = 0x7f1212fd

.field public static final launch_wechat_health_code_auto_close:I = 0x7f1212fe

.field public static final launch_wechat_payment_code:I = 0x7f1212ff

.field public static final launch_wechat_scanner:I = 0x7f121300

.field public static final launch_wifi_text:I = 0x7f121301

.field public static final launcher_icon:I = 0x7f121302

.field public static final launcher_icon_management:I = 0x7f121303

.field public static final launcher_title:I = 0x7f121304

.field public static final launching_cit:I = 0x7f121305

.field public static final launching_pho:I = 0x7f121306

.field public static final launching_rep:I = 0x7f121307

.field public static final layout_exhuge_font_take_effect_tips:I = 0x7f121308

.field public static final layout_exhuge_font_tips:I = 0x7f121309

.field public static final layout_exhuge_font_tips_reg:I = 0x7f12130a

.field public static final layout_size_exhuge:I = 0x7f12130b

.field public static final layout_size_exhuge_text:I = 0x7f12130c

.field public static final layout_size_extral_small:I = 0x7f12130d

.field public static final layout_size_huge:I = 0x7f12130e

.field public static final layout_size_huge_text:I = 0x7f12130f

.field public static final layout_size_large:I = 0x7f121310

.field public static final layout_size_large_text:I = 0x7f121311

.field public static final layout_size_medium:I = 0x7f121312

.field public static final layout_size_medium_text:I = 0x7f121313

.field public static final layout_size_normal:I = 0x7f121314

.field public static final layout_size_normal_text:I = 0x7f121315

.field public static final layout_size_small:I = 0x7f121316

.field public static final layout_size_small_text:I = 0x7f121317

.field public static final le_audio_title:I = 0x7f121318

.field public static final learn_more:I = 0x7f121319

.field public static final led_colors_summary:I = 0x7f12131a

.field public static final led_frequence_summary:I = 0x7f12131b

.field public static final led_pulse_summary:I = 0x7f12131c

.field public static final led_settings:I = 0x7f12131d

.field public static final left_edge:I = 0x7f12131e

.field public static final left_function_key:I = 0x7f12131f

.field public static final left_right_hand_switch:I = 0x7f121320

.field public static final left_right_hand_switch_summary:I = 0x7f121321

.field public static final left_shoulder_key:I = 0x7f121322

.field public static final legacy_control_center_desc:I = 0x7f121323

.field public static final legacy_control_center_title:I = 0x7f121324

.field public static final legacy_navigation_summary:I = 0x7f121325

.field public static final legacy_navigation_title:I = 0x7f121326

.field public static final legal_information:I = 0x7f121327

.field public static final legal_workday:I = 0x7f121328

.field public static final legal_workday_invalidate:I = 0x7f121329

.field public static final legal_workday_invalidate_message:I = 0x7f12132a

.field public static final legal_workday_message:I = 0x7f12132b

.field public static final legal_written_offer:I = 0x7f12132c

.field public static final license_title:I = 0x7f12132d

.field public static final lift_to_wake_title:I = 0x7f12132e

.field public static final light_color_mode:I = 0x7f12132f

.field public static final lights_title:I = 0x7f121330

.field public static final line_breaking_summary:I = 0x7f121331

.field public static final line_breaking_title:I = 0x7f121332

.field public static final link_speed:I = 0x7f121333

.field public static final link_turbo_prompt:I = 0x7f121334

.field public static final link_turbo_settings_all_app_display_loading:I = 0x7f121335

.field public static final link_turbo_settings_all_app_reach_max:I = 0x7f121336

.field public static final link_turbo_usage_state_month:I = 0x7f121337

.field public static final link_turbo_usage_state_month_total:I = 0x7f121338

.field public static final link_turbo_usage_state_today:I = 0x7f121339

.field public static final link_turbo_usage_state_today_total:I = 0x7f12133a

.field public static final list_bg_header_text:I = 0x7f12133b

.field public static final list_empty_text:I = 0x7f12133c

.field public static final lite_font_weight_title:I = 0x7f12133d

.field public static final live_caption_summary:I = 0x7f12133e

.field public static final live_caption_title:I = 0x7f12133f

.field public static final load_networks_progress:I = 0x7f121340

.field public static final loading:I = 0x7f121341

.field public static final loading_injected_setting_summary:I = 0x7f121342

.field public static final loading_notification_apps:I = 0x7f121343

.field public static final local_auto_backup_off:I = 0x7f121344

.field public static final local_auto_backup_on:I = 0x7f121345

.field public static final local_auto_backup_section_title:I = 0x7f121346

.field public static final local_backup_password_summary_change:I = 0x7f121347

.field public static final local_backup_password_summary_none:I = 0x7f121348

.field public static final local_backup_password_title:I = 0x7f121349

.field public static final local_backup_password_toast_confirmation_mismatch:I = 0x7f12134a

.field public static final local_backup_password_toast_success:I = 0x7f12134b

.field public static final local_backup_password_toast_validation_failure:I = 0x7f12134c

.field public static final local_backup_section_summary:I = 0x7f12134d

.field public static final local_backup_section_summary_new:I = 0x7f12134e

.field public static final local_backup_section_title:I = 0x7f12134f

.field public static final local_backup_section_title_new:I = 0x7f121350

.field public static final local_backup_usestatus_title:I = 0x7f121351

.field public static final locale_default:I = 0x7f121352

.field public static final locale_not_translated:I = 0x7f121353

.field public static final locale_picker_category_title:I = 0x7f121354

.field public static final locale_remove_menu:I = 0x7f121355

.field public static final locale_settings:I = 0x7f121356

.field public static final location_access_summary:I = 0x7f121357

.field public static final location_access_title:I = 0x7f121358

.field public static final location_agps_apn_choose_dialog_title:I = 0x7f121359

.field public static final location_agps_apn_summary:I = 0x7f12135a

.field public static final location_agps_apn_type:I = 0x7f12135b

.field public static final location_agps_def_location_mode:I = 0x7f12135c

.field public static final location_agps_def_network_mode:I = 0x7f12135d

.field public static final location_agps_def_reset_type:I = 0x7f12135e

.field public static final location_agps_def_supl_host:I = 0x7f12135f

.field public static final location_agps_def_supl_port:I = 0x7f121360

.field public static final location_agps_params_settings_title:I = 0x7f121361

.field public static final location_agps_pref_apn_title:I = 0x7f121362

.field public static final location_agps_pref_network_title:I = 0x7f121363

.field public static final location_agps_pref_reset_type_title:I = 0x7f121364

.field public static final location_agps_pref_si_title:I = 0x7f121365

.field public static final location_agps_register_network_type:I = 0x7f121366

.field public static final location_agps_register_network_type_dialog_title:I = 0x7f121367

.field public static final location_agps_register_network_type_summary:I = 0x7f121368

.field public static final location_agps_reset_type:I = 0x7f121369

.field public static final location_agps_reset_type_choose_dialog_title:I = 0x7f12136a

.field public static final location_agps_reset_type_summary:I = 0x7f12136b

.field public static final location_agps_roaming_help:I = 0x7f12136c

.field public static final location_agps_si_mode:I = 0x7f12136d

.field public static final location_agps_si_mode_dialog_title:I = 0x7f12136e

.field public static final location_agps_si_mode_summary:I = 0x7f12136f

.field public static final location_agps_supl_server_addr:I = 0x7f121370

.field public static final location_agps_supl_server_port:I = 0x7f121371

.field public static final location_agps_supl_settings:I = 0x7f121372

.field public static final location_app_level_permissions:I = 0x7f121373

.field public static final location_app_permission_summary_location_off:I = 0x7f121374

.field public static final location_category:I = 0x7f121375

.field public static final location_category_location_services:I = 0x7f121376

.field public static final location_category_recent_location_access:I = 0x7f121377

.field public static final location_category_recent_location_requests:I = 0x7f121378

.field public static final location_description_no_gps:I = 0x7f121379

.field public static final location_gps:I = 0x7f12137a

.field public static final location_high_battery_use:I = 0x7f12137b

.field public static final location_indicator_settings_description:I = 0x7f12137c

.field public static final location_indicator_settings_title:I = 0x7f12137d

.field public static final location_low_battery_use:I = 0x7f12137e

.field public static final location_mode_battery_saving_description:I = 0x7f12137f

.field public static final location_mode_battery_saving_title:I = 0x7f121380

.field public static final location_mode_high_accuracy_description:I = 0x7f121381

.field public static final location_mode_high_accuracy_title:I = 0x7f121382

.field public static final location_mode_location_off_title:I = 0x7f121383

.field public static final location_mode_screen_title:I = 0x7f121384

.field public static final location_mode_sensors_only_description:I = 0x7f121385

.field public static final location_mode_sensors_only_title:I = 0x7f121386

.field public static final location_modem_summary:I = 0x7f121387

.field public static final location_modem_title:I = 0x7f121388

.field public static final location_neighborhood_level:I = 0x7f121389

.field public static final location_neighborhood_level_wifi:I = 0x7f12138a

.field public static final location_network_based:I = 0x7f12138b

.field public static final location_no_recent_accesses:I = 0x7f12138c

.field public static final location_no_recent_apps:I = 0x7f12138d

.field public static final location_recent_location_access_see_all:I = 0x7f12138e

.field public static final location_recent_location_access_view_details:I = 0x7f12138f

.field public static final location_recent_location_requests_see_all:I = 0x7f121390

.field public static final location_scanning_bluetooth_always_scanning_description:I = 0x7f121391

.field public static final location_scanning_bluetooth_always_scanning_title:I = 0x7f121392

.field public static final location_scanning_screen_title:I = 0x7f121393

.field public static final location_scanning_wifi_always_scanning_description:I = 0x7f121394

.field public static final location_scanning_wifi_always_scanning_title:I = 0x7f121395

.field public static final location_services_preference_title:I = 0x7f121396

.field public static final location_services_screen_title:I = 0x7f121397

.field public static final location_settings_footer_general:I = 0x7f121398

.field public static final location_settings_footer_learn_more_content_description:I = 0x7f121399

.field public static final location_settings_footer_learn_more_link:I = 0x7f12139a

.field public static final location_settings_footer_location_off:I = 0x7f12139b

.field public static final location_settings_loading_app_permission_stats:I = 0x7f12139c

.field public static final location_settings_primary_switch_title:I = 0x7f12139d

.field public static final location_settings_summary_location_off:I = 0x7f12139e

.field public static final location_settings_title:I = 0x7f12139f

.field public static final location_sources_heading:I = 0x7f1213a0

.field public static final location_street_level:I = 0x7f1213a1

.field public static final location_time_zone_detection_auto_is_off:I = 0x7f1213a2

.field public static final location_time_zone_detection_location_is_off_dialog_cancel_button:I = 0x7f1213a3

.field public static final location_time_zone_detection_location_is_off_dialog_message:I = 0x7f1213a4

.field public static final location_time_zone_detection_location_is_off_dialog_ok_button:I = 0x7f1213a5

.field public static final location_time_zone_detection_location_is_off_dialog_title:I = 0x7f1213a6

.field public static final location_time_zone_detection_not_allowed:I = 0x7f1213a7

.field public static final location_time_zone_detection_not_applicable:I = 0x7f1213a8

.field public static final location_time_zone_detection_not_supported:I = 0x7f1213a9

.field public static final location_time_zone_detection_toggle_title:I = 0x7f1213aa

.field public static final location_title:I = 0x7f1213ab

.field public static final location_toggle_title:I = 0x7f1213ac

.field public static final lock_after_timeout:I = 0x7f1213ad

.field public static final lock_after_timeout_summary:I = 0x7f1213ae

.field public static final lock_after_timeout_summary_with_exception:I = 0x7f1213af

.field public static final lock_failed_attempts_before_wipe:I = 0x7f1213b0

.field public static final lock_immediately_summary_with_exception:I = 0x7f1213b1

.field public static final lock_intro_message:I = 0x7f1213b2

.field public static final lock_last_attempt_before_wipe_warning_title:I = 0x7f1213b3

.field public static final lock_last_password_attempt_before_wipe_device:I = 0x7f1213b4

.field public static final lock_last_password_attempt_before_wipe_profile:I = 0x7f1213b5

.field public static final lock_last_password_attempt_before_wipe_user:I = 0x7f1213b6

.field public static final lock_last_pattern_attempt_before_wipe_device:I = 0x7f1213b7

.field public static final lock_last_pattern_attempt_before_wipe_profile:I = 0x7f1213b8

.field public static final lock_last_pattern_attempt_before_wipe_user:I = 0x7f1213b9

.field public static final lock_last_pin_attempt_before_wipe_device:I = 0x7f1213ba

.field public static final lock_last_pin_attempt_before_wipe_profile:I = 0x7f1213bb

.field public static final lock_last_pin_attempt_before_wipe_user:I = 0x7f1213bc

.field public static final lock_profile_wipe_warning_content_password:I = 0x7f1213bd

.field public static final lock_profile_wipe_warning_content_pattern:I = 0x7f1213be

.field public static final lock_profile_wipe_warning_content_pin:I = 0x7f1213bf

.field public static final lock_screen_action:I = 0x7f1213c0

.field public static final lock_screen_after_fold_screen_summary:I = 0x7f1213c1

.field public static final lock_screen_after_fold_screen_title:I = 0x7f1213c2

.field public static final lock_screen_display_category_name:I = 0x7f1213c3

.field public static final lock_screen_intro_skip_dialog_text:I = 0x7f1213c4

.field public static final lock_screen_intro_skip_dialog_text_frp:I = 0x7f1213c5

.field public static final lock_screen_intro_skip_title:I = 0x7f1213c6

.field public static final lock_screen_notifications_interstitial_message:I = 0x7f1213c7

.field public static final lock_screen_notifications_interstitial_message_profile:I = 0x7f1213c8

.field public static final lock_screen_notifications_interstitial_title:I = 0x7f1213c9

.field public static final lock_screen_notifications_interstitial_title_profile:I = 0x7f1213ca

.field public static final lock_screen_notifications_summary_disable:I = 0x7f1213cb

.field public static final lock_screen_notifications_summary_hide:I = 0x7f1213cc

.field public static final lock_screen_notifications_summary_hide_profile:I = 0x7f1213cd

.field public static final lock_screen_notifications_summary_show:I = 0x7f1213ce

.field public static final lock_screen_notifications_summary_show_profile:I = 0x7f1213cf

.field public static final lock_screen_notifications_title:I = 0x7f1213d0

.field public static final lock_screen_notifs_redact:I = 0x7f1213d1

.field public static final lock_screen_notifs_redact_summary:I = 0x7f1213d2

.field public static final lock_screen_notifs_redact_work:I = 0x7f1213d3

.field public static final lock_screen_notifs_redact_work_summary:I = 0x7f1213d4

.field public static final lock_screen_notifs_show_alerting:I = 0x7f1213d5

.field public static final lock_screen_notifs_show_all:I = 0x7f1213d6

.field public static final lock_screen_notifs_show_all_summary:I = 0x7f1213d7

.field public static final lock_screen_notifs_show_none:I = 0x7f1213d8

.field public static final lock_screen_notifs_title:I = 0x7f1213d9

.field public static final lock_screen_now:I = 0x7f1213da

.field public static final lock_screen_password_skip_biometrics_message:I = 0x7f1213db

.field public static final lock_screen_password_skip_biometrics_title:I = 0x7f1213dc

.field public static final lock_screen_password_skip_face_message:I = 0x7f1213dd

.field public static final lock_screen_password_skip_face_title:I = 0x7f1213de

.field public static final lock_screen_password_skip_fingerprint_message:I = 0x7f1213df

.field public static final lock_screen_password_skip_fingerprint_title:I = 0x7f1213e0

.field public static final lock_screen_password_skip_message:I = 0x7f1213e1

.field public static final lock_screen_password_skip_title:I = 0x7f1213e2

.field public static final lock_screen_pattern_skip_biometrics_message:I = 0x7f1213e3

.field public static final lock_screen_pattern_skip_biometrics_title:I = 0x7f1213e4

.field public static final lock_screen_pattern_skip_face_message:I = 0x7f1213e5

.field public static final lock_screen_pattern_skip_face_title:I = 0x7f1213e6

.field public static final lock_screen_pattern_skip_fingerprint_message:I = 0x7f1213e7

.field public static final lock_screen_pattern_skip_fingerprint_title:I = 0x7f1213e8

.field public static final lock_screen_pattern_skip_message:I = 0x7f1213e9

.field public static final lock_screen_pattern_skip_title:I = 0x7f1213ea

.field public static final lock_screen_pin_skip_biometrics_message:I = 0x7f1213eb

.field public static final lock_screen_pin_skip_biometrics_title:I = 0x7f1213ec

.field public static final lock_screen_pin_skip_face_message:I = 0x7f1213ed

.field public static final lock_screen_pin_skip_face_title:I = 0x7f1213ee

.field public static final lock_screen_pin_skip_fingerprint_message:I = 0x7f1213ef

.field public static final lock_screen_pin_skip_fingerprint_title:I = 0x7f1213f0

.field public static final lock_screen_pin_skip_message:I = 0x7f1213f1

.field public static final lock_screen_pin_skip_title:I = 0x7f1213f2

.field public static final lock_screen_settings_title:I = 0x7f1213f3

.field public static final lock_screen_signature_summary:I = 0x7f1213f4

.field public static final lock_screen_signature_title:I = 0x7f1213f5

.field public static final lock_screen_unlock_show:I = 0x7f1213f6

.field public static final lock_screen_unlock_show_summary:I = 0x7f1213f7

.field public static final lock_secure_after_timeout_click_me_to_change_text:I = 0x7f1213f8

.field public static final lock_settings:I = 0x7f1213f9

.field public static final lock_settings_common_title:I = 0x7f1213fa

.field public static final lock_settings_picker_admin_restricted_personal_message:I = 0x7f1213fb

.field public static final lock_settings_picker_admin_restricted_personal_message_action:I = 0x7f1213fc

.field public static final lock_settings_picker_biometric_message:I = 0x7f1213fd

.field public static final lock_settings_picker_biometrics_added_security_message:I = 0x7f1213fe

.field public static final lock_settings_picker_new_lock_title:I = 0x7f1213ff

.field public static final lock_settings_picker_new_profile_lock_title:I = 0x7f121400

.field public static final lock_settings_picker_profile_message:I = 0x7f121401

.field public static final lock_settings_picker_settings:I = 0x7f121402

.field public static final lock_settings_picker_title:I = 0x7f121403

.field public static final lock_settings_picker_update_lock_title:I = 0x7f121404

.field public static final lock_settings_picker_update_profile_lock_title:I = 0x7f121405

.field public static final lock_settings_profile_screen_lock_title:I = 0x7f121406

.field public static final lock_settings_profile_title:I = 0x7f121407

.field public static final lock_settings_profile_unification_dialog_body:I = 0x7f121408

.field public static final lock_settings_profile_unification_dialog_confirm:I = 0x7f121409

.field public static final lock_settings_profile_unification_dialog_title:I = 0x7f12140a

.field public static final lock_settings_profile_unification_dialog_uncompliant_body:I = 0x7f12140b

.field public static final lock_settings_profile_unification_dialog_uncompliant_confirm:I = 0x7f12140c

.field public static final lock_settings_profile_unification_summary:I = 0x7f12140d

.field public static final lock_settings_profile_unification_title:I = 0x7f12140e

.field public static final lock_settings_profile_unified_summary:I = 0x7f12140f

.field public static final lock_settings_title:I = 0x7f121410

.field public static final lock_settings_with_fingerprint:I = 0x7f121411

.field public static final lock_setup:I = 0x7f121412

.field public static final lock_sound_effect_enable_title:I = 0x7f121413

.field public static final lock_sounds_enable_title:I = 0x7f121414

.field public static final locked_work_profile_notification_title:I = 0x7f121415

.field public static final lockpassword_cancel_label:I = 0x7f121416

.field public static final lockpassword_choose_for_second_user:I = 0x7f121417

.field public static final lockpassword_choose_lock_generic_header:I = 0x7f121418

.field public static final lockpassword_choose_your_password_header:I = 0x7f121419

.field public static final lockpassword_choose_your_password_header_for_biometrics:I = 0x7f12141a

.field public static final lockpassword_choose_your_password_header_for_face:I = 0x7f12141b

.field public static final lockpassword_choose_your_password_header_for_fingerprint:I = 0x7f12141c

.field public static final lockpassword_choose_your_password_second_space:I = 0x7f12141d

.field public static final lockpassword_choose_your_pattern_header:I = 0x7f12141e

.field public static final lockpassword_choose_your_pattern_header_for_biometrics:I = 0x7f12141f

.field public static final lockpassword_choose_your_pattern_header_for_face:I = 0x7f121420

.field public static final lockpassword_choose_your_pattern_header_for_fingerprint:I = 0x7f121421

.field public static final lockpassword_choose_your_pin_header:I = 0x7f121422

.field public static final lockpassword_choose_your_pin_header_for_biometrics:I = 0x7f121423

.field public static final lockpassword_choose_your_pin_header_for_face:I = 0x7f121424

.field public static final lockpassword_choose_your_pin_header_for_fingerprint:I = 0x7f121425

.field public static final lockpassword_choose_your_pin_header_second_space:I = 0x7f121426

.field public static final lockpassword_choose_your_pin_message:I = 0x7f121427

.field public static final lockpassword_choose_your_pin_title:I = 0x7f121428

.field public static final lockpassword_choose_your_profile_password_header:I = 0x7f121429

.field public static final lockpassword_choose_your_profile_pattern_header:I = 0x7f12142a

.field public static final lockpassword_choose_your_profile_pin_header:I = 0x7f12142b

.field public static final lockpassword_clear_label:I = 0x7f12142c

.field public static final lockpassword_confirm_for_second_user:I = 0x7f12142d

.field public static final lockpassword_confirm_label:I = 0x7f12142e

.field public static final lockpassword_confirm_passwords_dont_match:I = 0x7f12142f

.field public static final lockpassword_confirm_pins_dont_match:I = 0x7f121430

.field public static final lockpassword_confirm_workspace_password:I = 0x7f121431

.field public static final lockpassword_confirm_your_lock_password_header:I = 0x7f121432

.field public static final lockpassword_confirm_your_lock_pattern_header:I = 0x7f121433

.field public static final lockpassword_confirm_your_lock_pin_header:I = 0x7f121434

.field public static final lockpassword_confirm_your_password_details_frp:I = 0x7f121435

.field public static final lockpassword_confirm_your_password_generic:I = 0x7f121436

.field public static final lockpassword_confirm_your_password_generic_profile:I = 0x7f121437

.field public static final lockpassword_confirm_your_password_header:I = 0x7f121438

.field public static final lockpassword_confirm_your_password_header_frp:I = 0x7f121439

.field public static final lockpassword_confirm_your_pattern_details_frp:I = 0x7f12143a

.field public static final lockpassword_confirm_your_pattern_generic:I = 0x7f12143b

.field public static final lockpassword_confirm_your_pattern_generic_profile:I = 0x7f12143c

.field public static final lockpassword_confirm_your_pattern_header:I = 0x7f12143d

.field public static final lockpassword_confirm_your_pattern_header_frp:I = 0x7f12143e

.field public static final lockpassword_confirm_your_pin_details_frp:I = 0x7f12143f

.field public static final lockpassword_confirm_your_pin_generic:I = 0x7f121440

.field public static final lockpassword_confirm_your_pin_generic_profile:I = 0x7f121441

.field public static final lockpassword_confirm_your_pin_header:I = 0x7f121442

.field public static final lockpassword_confirm_your_pin_header_frp:I = 0x7f121443

.field public static final lockpassword_confirm_your_private_password_header:I = 0x7f121444

.field public static final lockpassword_confirm_your_private_pattern_header:I = 0x7f121445

.field public static final lockpassword_confirm_your_work_password_header:I = 0x7f121446

.field public static final lockpassword_confirm_your_work_pattern_header:I = 0x7f121447

.field public static final lockpassword_confirm_your_work_pin_header:I = 0x7f121448

.field public static final lockpassword_continue_label:I = 0x7f121449

.field public static final lockpassword_credential_changed:I = 0x7f12144a

.field public static final lockpassword_draw_your_pattern_again_header:I = 0x7f12144b

.field public static final lockpassword_forgot_password:I = 0x7f12144c

.field public static final lockpassword_forgot_pattern:I = 0x7f12144d

.field public static final lockpassword_forgot_pin:I = 0x7f12144e

.field public static final lockpassword_illegal_character:I = 0x7f12144f

.field public static final lockpassword_invalid_password:I = 0x7f121450

.field public static final lockpassword_invalid_pin:I = 0x7f121451

.field public static final lockpassword_ok_label:I = 0x7f121452

.field public static final lockpassword_password_recently_used:I = 0x7f121453

.field public static final lockpassword_password_requires_alpha:I = 0x7f121454

.field public static final lockpassword_password_requires_digit:I = 0x7f121455

.field public static final lockpassword_password_requires_symbol:I = 0x7f121456

.field public static final lockpassword_password_set_toast:I = 0x7f121457

.field public static final lockpassword_password_too_short_all_numeric:I = 0x7f121458

.field public static final lockpassword_pattern_set_toast:I = 0x7f121459

.field public static final lockpassword_pin_contains_non_digits:I = 0x7f12145a

.field public static final lockpassword_pin_no_sequential_digits:I = 0x7f12145b

.field public static final lockpassword_pin_recently_used:I = 0x7f12145c

.field public static final lockpassword_pin_set_toast:I = 0x7f12145d

.field public static final lockpassword_pin_too_long:I = 0x7f12145e

.field public static final lockpassword_pin_too_short:I = 0x7f12145f

.field public static final lockpassword_reenter_your_profile_password_header:I = 0x7f121460

.field public static final lockpassword_reenter_your_profile_pin_header:I = 0x7f121461

.field public static final lockpassword_strong_auth_required_device_password:I = 0x7f121462

.field public static final lockpassword_strong_auth_required_device_pattern:I = 0x7f121463

.field public static final lockpassword_strong_auth_required_device_pin:I = 0x7f121464

.field public static final lockpassword_strong_auth_required_work_password:I = 0x7f121465

.field public static final lockpassword_strong_auth_required_work_pattern:I = 0x7f121466

.field public static final lockpassword_strong_auth_required_work_pin:I = 0x7f121467

.field public static final lockpattern_change_lock_pattern_label:I = 0x7f121468

.field public static final lockpattern_change_lock_pin_label:I = 0x7f121469

.field public static final lockpattern_confirm_button_text:I = 0x7f12146a

.field public static final lockpattern_continue_button_text:I = 0x7f12146b

.field public static final lockpattern_need_to_confirm:I = 0x7f12146c

.field public static final lockpattern_need_to_unlock_wrong:I = 0x7f12146d

.field public static final lockpattern_pattern_confirmed_header:I = 0x7f12146e

.field public static final lockpattern_pattern_entered_header:I = 0x7f12146f

.field public static final lockpattern_pattern_same_with_others:I = 0x7f121470

.field public static final lockpattern_pattern_same_with_owner:I = 0x7f121471

.field public static final lockpattern_pattern_same_with_security_space:I = 0x7f121472

.field public static final lockpattern_recording_incorrect_too_short:I = 0x7f121473

.field public static final lockpattern_recording_inprogress:I = 0x7f121474

.field public static final lockpattern_recording_intro_footer:I = 0x7f121475

.field public static final lockpattern_recording_intro_header:I = 0x7f121476

.field public static final lockpattern_recording_intro_header_second_space:I = 0x7f121477

.field public static final lockpattern_restart_button_text:I = 0x7f121478

.field public static final lockpattern_retry_button_text:I = 0x7f121479

.field public static final lockpattern_settings_change_lock_pattern:I = 0x7f12147a

.field public static final lockpattern_settings_choose_lock_pattern:I = 0x7f12147b

.field public static final lockpattern_settings_enable_power_button_instantly_locks:I = 0x7f12147c

.field public static final lockpattern_settings_enable_summary:I = 0x7f12147d

.field public static final lockpattern_settings_enable_tactile_feedback_title:I = 0x7f12147e

.field public static final lockpattern_settings_enable_title:I = 0x7f12147f

.field public static final lockpattern_settings_enable_visible_pattern_title:I = 0x7f121480

.field public static final lockpattern_settings_enable_visible_pattern_title_profile:I = 0x7f121481

.field public static final lockpattern_settings_help_how_to_record:I = 0x7f121482

.field public static final lockpattern_settings_power_button_instantly_locks_summary:I = 0x7f121483

.field public static final lockpattern_settings_require_cred_before_startup:I = 0x7f121484

.field public static final lockpattern_settings_require_password_before_startup_summary:I = 0x7f121485

.field public static final lockpattern_settings_require_pattern_before_startup_summary:I = 0x7f121486

.field public static final lockpattern_settings_require_pin_before_startup_summary:I = 0x7f121487

.field public static final lockpattern_settings_title:I = 0x7f121488

.field public static final lockpattern_too_many_failed_confirmation_attempts:I = 0x7f121489

.field public static final lockpattern_too_many_failed_confirmation_attempts_header:I = 0x7f12148a

.field public static final lockpattern_tutorial_cancel_label:I = 0x7f12148b

.field public static final lockpattern_tutorial_continue_label:I = 0x7f12148c

.field public static final lockscreen_bypass_summary:I = 0x7f12148d

.field public static final lockscreen_bypass_title:I = 0x7f12148e

.field public static final lockscreen_double_line_clock_setting_toggle:I = 0x7f12148f

.field public static final lockscreen_double_line_clock_summary:I = 0x7f121490

.field public static final lockscreen_glogin_checking_password:I = 0x7f121491

.field public static final lockscreen_glogin_password_hint:I = 0x7f121492

.field public static final lockscreen_glogin_submit_button:I = 0x7f121493

.field public static final lockscreen_label:I = 0x7f121494

.field public static final lockscreen_magazine:I = 0x7f121495

.field public static final lockscreen_magazine_india:I = 0x7f121496

.field public static final lockscreen_magazine_summary:I = 0x7f121497

.field public static final lockscreen_magazine_title:I = 0x7f121498

.field public static final lockscreen_privacy_controls_setting_toggle:I = 0x7f121499

.field public static final lockscreen_privacy_controls_summary:I = 0x7f12149a

.field public static final lockscreen_privacy_not_secure:I = 0x7f12149b

.field public static final lockscreen_privacy_qr_code_scanner_setting_toggle:I = 0x7f12149c

.field public static final lockscreen_privacy_qr_code_scanner_summary:I = 0x7f12149d

.field public static final lockscreen_privacy_wallet_setting_toggle:I = 0x7f12149e

.field public static final lockscreen_privacy_wallet_summary:I = 0x7f12149f

.field public static final lockscreen_remote_input:I = 0x7f1214a0

.field public static final lockscreen_settings_title:I = 0x7f1214a1

.field public static final lockscreen_settings_what_to_show_category:I = 0x7f1214a2

.field public static final lockscreen_trivial_controls_setting_toggle:I = 0x7f1214a3

.field public static final lockscreen_trivial_controls_summary:I = 0x7f1214a4

.field public static final lockscreen_trivial_disabled_controls_summary:I = 0x7f1214a5

.field public static final login_account_dialog_message:I = 0x7f1214a6

.field public static final login_account_dialog_title:I = 0x7f1214a7

.field public static final login_account_summary:I = 0x7f1214a8

.field public static final login_account_summary_temp:I = 0x7f1214a9

.field public static final login_action:I = 0x7f1214aa

.field public static final long_press_back_key:I = 0x7f1214ab

.field public static final long_press_buttom_key:I = 0x7f1214ac

.field public static final long_press_home_key:I = 0x7f1214ad

.field public static final long_press_menu_key:I = 0x7f1214ae

.field public static final long_press_menu_key_when_lock:I = 0x7f1214af

.field public static final long_press_power_key:I = 0x7f1214b0

.field public static final long_press_power_key_half_of_second:I = 0x7f1214b1

.field public static final long_press_power_key_half_second:I = 0x7f1214b2

.field public static final long_press_power_key_tips:I = 0x7f1214b3

.field public static final long_press_volume_down:I = 0x7f1214b4

.field public static final long_time:I = 0x7f1214b5

.field public static final lost_internet_access_cancel:I = 0x7f1214b6

.field public static final lost_internet_access_persist:I = 0x7f1214b7

.field public static final lost_internet_access_switch:I = 0x7f1214b8

.field public static final lost_internet_access_text:I = 0x7f1214b9

.field public static final lost_internet_access_title:I = 0x7f1214ba

.field public static final low_battery_summary:I = 0x7f1214bb

.field public static final low_dc_light_title:I = 0x7f1214bc

.field public static final low_label:I = 0x7f1214bd

.field public static final low_storage_summary:I = 0x7f1214be

.field public static final m3_ref_typeface_brand_display_regular:I = 0x7f1214bf

.field public static final m3_ref_typeface_brand_medium:I = 0x7f1214c0

.field public static final m3_ref_typeface_brand_regular:I = 0x7f1214c1

.field public static final m3_ref_typeface_plain_medium:I = 0x7f1214c2

.field public static final m3_ref_typeface_plain_regular:I = 0x7f1214c3

.field public static final m3_sys_motion_easing_accelerated:I = 0x7f1214c4

.field public static final m3_sys_motion_easing_decelerated:I = 0x7f1214c5

.field public static final m3_sys_motion_easing_emphasized:I = 0x7f1214c6

.field public static final m3_sys_motion_easing_emphasized_accelerate:I = 0x7f1214c7

.field public static final m3_sys_motion_easing_emphasized_decelerate:I = 0x7f1214c8

.field public static final m3_sys_motion_easing_legacy:I = 0x7f1214c9

.field public static final m3_sys_motion_easing_legacy_accelerate:I = 0x7f1214ca

.field public static final m3_sys_motion_easing_legacy_decelerate:I = 0x7f1214cb

.field public static final m3_sys_motion_easing_linear:I = 0x7f1214cc

.field public static final m3_sys_motion_easing_standard:I = 0x7f1214cd

.field public static final m3_sys_motion_easing_standard_accelerate:I = 0x7f1214ce

.field public static final m3_sys_motion_easing_standard_decelerate:I = 0x7f1214cf

.field public static final macaddr_label:I = 0x7f1214d0

.field public static final magic_list_empty_text_pad:I = 0x7f1214d1

.field public static final magic_window_name_pad:I = 0x7f1214d2

.field public static final mah:I = 0x7f1214d3

.field public static final main_clear_accounts:I = 0x7f1214d4

.field public static final main_clear_button_text:I = 0x7f1214d5

.field public static final main_clear_confirm_title:I = 0x7f1214d6

.field public static final main_clear_desc:I = 0x7f1214d7

.field public static final main_clear_desc_also_erases_esim:I = 0x7f1214d8

.field public static final main_clear_desc_also_erases_external:I = 0x7f1214d9

.field public static final main_clear_desc_erase_external_storage:I = 0x7f1214da

.field public static final main_clear_desc_no_cancel_mobile_plan:I = 0x7f1214db

.field public static final main_clear_final_desc:I = 0x7f1214dc

.field public static final main_clear_final_desc_esim:I = 0x7f1214dd

.field public static final main_clear_not_available:I = 0x7f1214de

.field public static final main_clear_other_users_present:I = 0x7f1214df

.field public static final main_clear_progress_text:I = 0x7f1214e0

.field public static final main_clear_progress_title:I = 0x7f1214e1

.field public static final main_clear_short_title:I = 0x7f1214e2

.field public static final main_clear_title:I = 0x7f1214e3

.field public static final main_netcheck_summary:I = 0x7f1214e4

.field public static final main_running_process_description:I = 0x7f1214e5

.field public static final main_toolbar_statistic:I = 0x7f1214e6

.field public static final maintenance_title:I = 0x7f1214e7

.field public static final make_sure_cancel_update:I = 0x7f1214e8

.field public static final man_add_application:I = 0x7f1214e9

.field public static final manage_conversations:I = 0x7f1214ea

.field public static final manage_device_admin:I = 0x7f1214eb

.field public static final manage_external_storage_title:I = 0x7f1214ec

.field public static final manage_facerecoginition_text:I = 0x7f1214ed

.field public static final manage_fakecell_settings:I = 0x7f1214ee

.field public static final manage_fingerprint_text:I = 0x7f1214ef

.field public static final manage_mobile_plan_title:I = 0x7f1214f0

.field public static final manage_notification_access_summary:I = 0x7f1214f1

.field public static final manage_notification_access_summary_zero:I = 0x7f1214f2

.field public static final manage_notification_access_title:I = 0x7f1214f3

.field public static final manage_notification_title:I = 0x7f1214f4

.field public static final manage_password:I = 0x7f1214f5

.field public static final manage_password_summary:I = 0x7f1214f6

.field public static final manage_space_text:I = 0x7f1214f7

.field public static final manage_trust_agents:I = 0x7f1214f8

.field public static final manage_trust_agents_summary:I = 0x7f1214f9

.field public static final manage_xiaomi_router:I = 0x7f1214fa

.field public static final manage_zen_access_title:I = 0x7f1214fb

.field public static final manageapplications_settings_summary:I = 0x7f1214fc

.field public static final manageapplications_settings_title:I = 0x7f1214fd

.field public static final managed_profile_contact_search_summary:I = 0x7f1214fe

.field public static final managed_profile_contact_search_title:I = 0x7f1214ff

.field public static final managed_profile_location_category:I = 0x7f121500

.field public static final managed_profile_location_services:I = 0x7f121501

.field public static final managed_profile_location_switch_lockdown:I = 0x7f121502

.field public static final managed_profile_location_switch_title:I = 0x7f121503

.field public static final managed_profile_not_available_label:I = 0x7f121504

.field public static final managed_profile_settings_title:I = 0x7f121505

.field public static final managed_user_title:I = 0x7f121506

.field public static final manager_battery_usage_footer:I = 0x7f121507

.field public static final manager_battery_usage_footer_limited:I = 0x7f121508

.field public static final manager_battery_usage_link_a11y:I = 0x7f121509

.field public static final manager_battery_usage_optimized_only:I = 0x7f12150a

.field public static final manager_battery_usage_optimized_summary:I = 0x7f12150b

.field public static final manager_battery_usage_optimized_title:I = 0x7f12150c

.field public static final manager_battery_usage_restricted_summary:I = 0x7f12150d

.field public static final manager_battery_usage_restricted_title:I = 0x7f12150e

.field public static final manager_battery_usage_unrestricted_only:I = 0x7f12150f

.field public static final manager_battery_usage_unrestricted_summary:I = 0x7f121510

.field public static final manager_battery_usage_unrestricted_title:I = 0x7f121511

.field public static final managing_admin:I = 0x7f121512

.field public static final manual:I = 0x7f121513

.field public static final manual_mode_disallowed_summary:I = 0x7f121514

.field public static final manually_add_network:I = 0x7f121515

.field public static final mashup_sound:I = 0x7f121516

.field public static final mashup_sound_africa:I = 0x7f121517

.field public static final mashup_sound_amazon:I = 0x7f121518

.field public static final mashup_sound_arctic:I = 0x7f121519

.field public static final mashup_sound_australia:I = 0x7f12151a

.field public static final master_clear_all:I = 0x7f12151b

.field public static final master_clear_apply_step_1:I = 0x7f12151c

.field public static final master_clear_apply_step_2:I = 0x7f12151d

.field public static final master_clear_apply_title:I = 0x7f12151e

.field public static final master_clear_button_text:I = 0x7f12151f

.field public static final master_clear_erase_application_progress:I = 0x7f121520

.field public static final master_clear_personal_data:I = 0x7f121521

.field public static final master_clear_personal_data_application:I = 0x7f121522

.field public static final master_clear_personal_data_sdcard:I = 0x7f121523

.field public static final master_clear_progress_text:I = 0x7f121524

.field public static final master_clear_progress_title:I = 0x7f121525

.field public static final master_clear_title:I = 0x7f121526

.field public static final master_clear_title_new:I = 0x7f121527

.field public static final material_clock_display_divider:I = 0x7f121528

.field public static final material_clock_toggle_content_description:I = 0x7f121529

.field public static final material_hour_selection:I = 0x7f12152a

.field public static final material_hour_suffix:I = 0x7f12152b

.field public static final material_minute_selection:I = 0x7f12152c

.field public static final material_minute_suffix:I = 0x7f12152d

.field public static final material_motion_easing_accelerated:I = 0x7f12152e

.field public static final material_motion_easing_decelerated:I = 0x7f12152f

.field public static final material_motion_easing_emphasized:I = 0x7f121530

.field public static final material_motion_easing_linear:I = 0x7f121531

.field public static final material_motion_easing_standard:I = 0x7f121532

.field public static final material_slider_range_end:I = 0x7f121533

.field public static final material_slider_range_start:I = 0x7f121534

.field public static final material_timepicker_am:I = 0x7f121535

.field public static final material_timepicker_clock_mode_description:I = 0x7f121536

.field public static final material_timepicker_hour:I = 0x7f121537

.field public static final material_timepicker_minute:I = 0x7f121538

.field public static final material_timepicker_pm:I = 0x7f121539

.field public static final material_timepicker_select_time:I = 0x7f12153a

.field public static final material_timepicker_text_input_mode_description:I = 0x7f12153b

.field public static final max_aspect_settings_all_app_display_loading:I = 0x7f12153c

.field public static final max_cpu10_info:I = 0x7f12153d

.field public static final max_cpu2_info:I = 0x7f12153e

.field public static final max_cpu4_info:I = 0x7f12153f

.field public static final max_cpu6_info:I = 0x7f121540

.field public static final max_cpu8_info:I = 0x7f121541

.field public static final max_fingerprint_number_reached:I = 0x7f121542

.field public static final max_fingerprint_reached_toast:I = 0x7f121543

.field public static final maximum_memory_use:I = 0x7f121544

.field public static final mechanical_ime:I = 0x7f121545

.field public static final mechanical_ime_hint:I = 0x7f121546

.field public static final media_button:I = 0x7f121547

.field public static final media_category:I = 0x7f121548

.field public static final media_controls_hide_player:I = 0x7f121549

.field public static final media_controls_recommendations_description:I = 0x7f12154a

.field public static final media_controls_recommendations_title:I = 0x7f12154b

.field public static final media_controls_resume_description:I = 0x7f12154c

.field public static final media_controls_resume_title:I = 0x7f12154d

.field public static final media_controls_show_player:I = 0x7f12154e

.field public static final media_controls_title:I = 0x7f12154f

.field public static final media_feedback_enable_title:I = 0x7f121550

.field public static final media_management_apps_description:I = 0x7f121551

.field public static final media_management_apps_title:I = 0x7f121552

.field public static final media_management_apps_toggle_label:I = 0x7f121553

.field public static final media_out_summary_ongoing_call_state:I = 0x7f121554

.field public static final media_output_default_summary:I = 0x7f121555

.field public static final media_output_disconnected_status:I = 0x7f121556

.field public static final media_output_group:I = 0x7f121557

.field public static final media_output_group_panel_multiple_devices_summary:I = 0x7f121558

.field public static final media_output_group_panel_single_device_summary:I = 0x7f121559

.field public static final media_output_group_panel_title:I = 0x7f12155a

.field public static final media_output_label_title:I = 0x7f12155b

.field public static final media_output_panel_stop_casting_button:I = 0x7f12155c

.field public static final media_output_panel_summary_of_playing_device:I = 0x7f12155d

.field public static final media_output_panel_title:I = 0x7f12155e

.field public static final media_output_summary:I = 0x7f12155f

.field public static final media_output_summary_unavailable:I = 0x7f121560

.field public static final media_output_switch_error_text:I = 0x7f121561

.field public static final media_output_switching:I = 0x7f121562

.field public static final media_output_title:I = 0x7f121563

.field public static final media_transfer_this_device_name:I = 0x7f121564

.field public static final media_transfer_this_phone:I = 0x7f121565

.field public static final media_transfer_wired_device_name:I = 0x7f121566

.field public static final media_transfer_wired_usb_device_name:I = 0x7f121567

.field public static final media_volume_option_title:I = 0x7f121568

.field public static final media_volume_summary:I = 0x7f121569

.field public static final media_volume_title:I = 0x7f12156a

.field public static final megabyteShort:I = 0x7f12156b

.field public static final megabyte_per_second:I = 0x7f12156c

.field public static final meid_multi_sim:I = 0x7f12156d

.field public static final mem_details_title:I = 0x7f12156e

.field public static final memory:I = 0x7f12156f

.field public static final memory_apps_usage:I = 0x7f121570

.field public static final memory_available:I = 0x7f121571

.field public static final memory_available_read_only:I = 0x7f121572

.field public static final memory_avg_desc:I = 0x7f121573

.field public static final memory_avg_use:I = 0x7f121574

.field public static final memory_calculating_size:I = 0x7f121575

.field public static final memory_dcim_usage:I = 0x7f121576

.field public static final memory_details:I = 0x7f121577

.field public static final memory_details_title:I = 0x7f121578

.field public static final memory_downloads_usage:I = 0x7f121579

.field public static final memory_max_desc:I = 0x7f12157a

.field public static final memory_max_use:I = 0x7f12157b

.field public static final memory_maximum_usage:I = 0x7f12157c

.field public static final memory_media_cache_usage:I = 0x7f12157d

.field public static final memory_media_misc_usage:I = 0x7f12157e

.field public static final memory_media_usage:I = 0x7f12157f

.field public static final memory_music_usage:I = 0x7f121580

.field public static final memory_performance:I = 0x7f121581

.field public static final memory_rightvalue:I = 0x7f121582

.field public static final memory_settings_title:I = 0x7f121583

.field public static final memory_size:I = 0x7f121584

.field public static final memory_summary:I = 0x7f121585

.field public static final memory_system_rom:I = 0x7f121586

.field public static final memory_system_space:I = 0x7f121587

.field public static final memory_title:I = 0x7f121588

.field public static final memory_usage:I = 0x7f121589

.field public static final memory_usage_apps:I = 0x7f12158a

.field public static final memory_use_running_format:I = 0x7f12158b

.field public static final memory_use_summary:I = 0x7f12158c

.field public static final memorycard_calculating:I = 0x7f12158d

.field public static final menu_cancel:I = 0x7f12158e

.field public static final menu_check:I = 0x7f12158f

.field public static final menu_delete:I = 0x7f121590

.field public static final menu_duration_12h:I = 0x7f121591

.field public static final menu_duration_1d:I = 0x7f121592

.field public static final menu_duration_3h:I = 0x7f121593

.field public static final menu_duration_6h:I = 0x7f121594

.field public static final menu_hide_system:I = 0x7f121595

.field public static final menu_item_notification_power_text:I = 0x7f121596

.field public static final menu_key:I = 0x7f121597

.field public static final menu_key_about_device:I = 0x7f121598

.field public static final menu_key_accessibility:I = 0x7f121599

.field public static final menu_key_accounts:I = 0x7f12159a

.field public static final menu_key_apps:I = 0x7f12159b

.field public static final menu_key_battery:I = 0x7f12159c

.field public static final menu_key_connected_devices:I = 0x7f12159d

.field public static final menu_key_display:I = 0x7f12159e

.field public static final menu_key_emergency:I = 0x7f12159f

.field public static final menu_key_location:I = 0x7f1215a0

.field public static final menu_key_network:I = 0x7f1215a1

.field public static final menu_key_notifications:I = 0x7f1215a2

.field public static final menu_key_privacy:I = 0x7f1215a3

.field public static final menu_key_safety_center:I = 0x7f1215a4

.field public static final menu_key_security:I = 0x7f1215a5

.field public static final menu_key_sound:I = 0x7f1215a6

.field public static final menu_key_storage:I = 0x7f1215a7

.field public static final menu_key_support:I = 0x7f1215a8

.field public static final menu_key_system:I = 0x7f1215a9

.field public static final menu_key_wallpaper:I = 0x7f1215aa

.field public static final menu_new:I = 0x7f1215ab

.field public static final menu_new_display_open:I = 0x7f1215ac

.field public static final menu_pc_open:I = 0x7f1215ad

.field public static final menu_proc_stats_duration:I = 0x7f1215ae

.field public static final menu_proc_stats_type:I = 0x7f1215af

.field public static final menu_proc_stats_type_background:I = 0x7f1215b0

.field public static final menu_proc_stats_type_cached:I = 0x7f1215b1

.field public static final menu_proc_stats_type_foreground:I = 0x7f1215b2

.field public static final menu_restore:I = 0x7f1215b3

.field public static final menu_save:I = 0x7f1215b4

.field public static final menu_share:I = 0x7f1215b5

.field public static final menu_show_percentage:I = 0x7f1215b6

.field public static final menu_show_system:I = 0x7f1215b7

.field public static final menu_stats_last_unplugged:I = 0x7f1215b8

.field public static final menu_stats_refresh:I = 0x7f1215b9

.field public static final menu_stats_total:I = 0x7f1215ba

.field public static final menu_stats_unplugged:I = 0x7f1215bb

.field public static final menu_use_uss:I = 0x7f1215bc

.field public static final metadata_not_synced:I = 0x7f1215bd

.field public static final metadata_synced:I = 0x7f1215be

.field public static final mi_headset_healthy:I = 0x7f1215bf

.field public static final mi_pay:I = 0x7f1215c0

.field public static final mi_pay_indonesia:I = 0x7f1215c1

.field public static final mi_pay_summary:I = 0x7f1215c2

.field public static final mi_pay_summary_without_nfc:I = 0x7f1215c3

.field public static final mi_service:I = 0x7f1215c4

.field public static final mi_service_app_detail:I = 0x7f1215c5

.field public static final mi_shopping_mall:I = 0x7f1215c6

.field public static final mi_smart_hub_entry_title:I = 0x7f1215c7

.field public static final mi_smart_play_entry_title:I = 0x7f1215c8

.field public static final mi_transfer:I = 0x7f1215c9

.field public static final mi_upi_settings:I = 0x7f1215ca

.field public static final mi_wallet:I = 0x7f1215cb

.field public static final mi_wallet_desc:I = 0x7f1215cc

.field public static final mic_toggle_description:I = 0x7f1215cd

.field public static final mic_toggle_title:I = 0x7f1215ce

.field public static final micloud_service_summary:I = 0x7f1215cf

.field public static final micloud_service_title:I = 0x7f1215d0

.field public static final middle_function_key:I = 0x7f1215d1

.field public static final midnight:I = 0x7f1215d2

.field public static final miheadet_up_music:I = 0x7f1215d3

.field public static final miheadset_Box:I = 0x7f1215d4

.field public static final miheadset_Intelligentpickfree_summary:I = 0x7f1215d5

.field public static final miheadset_LEAudio_summary:I = 0x7f1215d6

.field public static final miheadset_LEAudio_title:I = 0x7f1215d7

.field public static final miheadset_LE_on_toast:I = 0x7f1215d8

.field public static final miheadset_LeftEar:I = 0x7f1215d9

.field public static final miheadset_RightEar:I = 0x7f1215da

.field public static final miheadset_active:I = 0x7f1215db

.field public static final miheadset_adaptive:I = 0x7f1215dc

.field public static final miheadset_ancClosed:I = 0x7f1215dd

.field public static final miheadset_ancDepth:I = 0x7f1215de

.field public static final miheadset_ancEquilibrium:I = 0x7f1215df

.field public static final miheadset_ancHigh:I = 0x7f1215e0

.field public static final miheadset_ancLow:I = 0x7f1215e1

.field public static final miheadset_ancMedium:I = 0x7f1215e2

.field public static final miheadset_ancMild:I = 0x7f1215e3

.field public static final miheadset_anc_indicate:I = 0x7f1215e4

.field public static final miheadset_anc_manual:I = 0x7f1215e5

.field public static final miheadset_audio_mode:I = 0x7f1215e6

.field public static final miheadset_audio_mode_high:I = 0x7f1215e7

.field public static final miheadset_audio_mode_listen:I = 0x7f1215e8

.field public static final miheadset_audio_mode_normal:I = 0x7f1215e9

.field public static final miheadset_audio_quality:I = 0x7f1215ea

.field public static final miheadset_audio_quality_summary:I = 0x7f1215eb

.field public static final miheadset_audio_sound_quality:I = 0x7f1215ec

.field public static final miheadset_audio_sound_quality_summary:I = 0x7f1215ed

.field public static final miheadset_auto_ack_mode:I = 0x7f1215ee

.field public static final miheadset_auto_ack_mode_summary:I = 0x7f1215ef

.field public static final miheadset_auto_answer_call:I = 0x7f1215f0

.field public static final miheadset_bluetooth_audio_title:I = 0x7f1215f1

.field public static final miheadset_bright:I = 0x7f1215f2

.field public static final miheadset_call_assist:I = 0x7f1215f3

.field public static final miheadset_cancel_device:I = 0x7f1215f4

.field public static final miheadset_checkUpdate:I = 0x7f1215f5

.field public static final miheadset_closeAnc:I = 0x7f1215f6

.field public static final miheadset_codec_sample_rate:I = 0x7f1215f7

.field public static final miheadset_common_problem:I = 0x7f1215f8

.field public static final miheadset_connectHeadsetForFw:I = 0x7f1215f9

.field public static final miheadset_connect_play_music:I = 0x7f1215fa

.field public static final miheadset_connect_toast_tips:I = 0x7f1215fb

.field public static final miheadset_custom_sound:I = 0x7f1215fc

.field public static final miheadset_custom_sound_summary:I = 0x7f1215fd

.field public static final miheadset_define_self:I = 0x7f1215fe

.field public static final miheadset_disconnect:I = 0x7f1215ff

.field public static final miheadset_disconnect_device:I = 0x7f121600

.field public static final miheadset_double_click_key_mode:I = 0x7f121601

.field public static final miheadset_double_click_left:I = 0x7f121602

.field public static final miheadset_double_click_right:I = 0x7f121603

.field public static final miheadset_double_connection:I = 0x7f121604

.field public static final miheadset_double_connection_summary:I = 0x7f121605

.field public static final miheadset_double_press_left:I = 0x7f121606

.field public static final miheadset_double_press_right:I = 0x7f121607

.field public static final miheadset_dual_connect_mode:I = 0x7f121608

.field public static final miheadset_dual_connect_mode_summary:I = 0x7f121609

.field public static final miheadset_enhanceVoice:I = 0x7f12160a

.field public static final miheadset_enhence_high:I = 0x7f12160b

.field public static final miheadset_enhence_low:I = 0x7f12160c

.field public static final miheadset_equalizer:I = 0x7f12160d

.field public static final miheadset_feedback:I = 0x7f12160e

.field public static final miheadset_feedback_summary:I = 0x7f12160f

.field public static final miheadset_feedback_title:I = 0x7f121610

.field public static final miheadset_fitness_check:I = 0x7f121611

.field public static final miheadset_fitness_check_checking:I = 0x7f121612

.field public static final miheadset_fitness_check_continue:I = 0x7f121613

.field public static final miheadset_fitness_check_done:I = 0x7f121614

.field public static final miheadset_fitness_check_restart:I = 0x7f121615

.field public static final miheadset_fitness_check_result_dialog1:I = 0x7f121616

.field public static final miheadset_fitness_check_result_dialog2:I = 0x7f121617

.field public static final miheadset_fitness_check_result_dialog_title:I = 0x7f121618

.field public static final miheadset_fitness_check_result_l_not_ok:I = 0x7f121619

.field public static final miheadset_fitness_check_result_not_ok:I = 0x7f12161a

.field public static final miheadset_fitness_check_result_ok:I = 0x7f12161b

.field public static final miheadset_fitness_check_result_r_not_ok:I = 0x7f12161c

.field public static final miheadset_fitness_check_result_summary1:I = 0x7f12161d

.field public static final miheadset_fitness_check_result_summary2:I = 0x7f12161e

.field public static final miheadset_fitness_check_start:I = 0x7f12161f

.field public static final miheadset_fitness_check_summary1:I = 0x7f121620

.field public static final miheadset_fitness_check_summary2:I = 0x7f121621

.field public static final miheadset_fitness_check_summary3:I = 0x7f121622

.field public static final miheadset_formal:I = 0x7f121623

.field public static final miheadset_function_settings:I = 0x7f121624

.field public static final miheadset_game_mode:I = 0x7f121625

.field public static final miheadset_game_mode_summary:I = 0x7f121626

.field public static final miheadset_gesture_operation:I = 0x7f121627

.field public static final miheadset_hd_audio_summary:I = 0x7f121628

.field public static final miheadset_hd_audio_title:I = 0x7f121629

.field public static final miheadset_head_tracking:I = 0x7f12162a

.field public static final miheadset_head_tracking_summary:I = 0x7f12162b

.field public static final miheadset_high_quality:I = 0x7f12162c

.field public static final miheadset_ignore:I = 0x7f12162d

.field public static final miheadset_in_ear_check:I = 0x7f12162e

.field public static final miheadset_in_ear_check_summary:I = 0x7f12162f

.field public static final miheadset_key_config_ANC_list_title:I = 0x7f121630

.field public static final miheadset_key_config_call_ai:I = 0x7f121631

.field public static final miheadset_key_config_close:I = 0x7f121632

.field public static final miheadset_key_config_close_summary:I = 0x7f121633

.field public static final miheadset_key_config_gesture_control:I = 0x7f121634

.field public static final miheadset_key_config_next_music:I = 0x7f121635

.field public static final miheadset_key_config_noise_control:I = 0x7f121636

.field public static final miheadset_key_config_noise_control_summary:I = 0x7f121637

.field public static final miheadset_key_config_noise_summary:I = 0x7f121638

.field public static final miheadset_key_config_press_left:I = 0x7f121639

.field public static final miheadset_key_config_press_right:I = 0x7f12163a

.field public static final miheadset_key_config_trans_summary:I = 0x7f12163b

.field public static final miheadset_key_config_triple_left:I = 0x7f12163c

.field public static final miheadset_key_config_triple_right:I = 0x7f12163d

.field public static final miheadset_key_config_volume_down:I = 0x7f12163e

.field public static final miheadset_key_config_volume_up:I = 0x7f12163f

.field public static final miheadset_last_version:I = 0x7f121640

.field public static final miheadset_local_ota:I = 0x7f121641

.field public static final miheadset_low_latency:I = 0x7f121642

.field public static final miheadset_manual:I = 0x7f121643

.field public static final miheadset_minus:I = 0x7f121644

.field public static final miheadset_moreSettingsInAi:I = 0x7f121645

.field public static final miheadset_multi_connect_mode:I = 0x7f121646

.field public static final miheadset_multi_connect_mode_summary:I = 0x7f121647

.field public static final miheadset_network_not_available:I = 0x7f121648

.field public static final miheadset_next:I = 0x7f121649

.field public static final miheadset_notification_summary:I = 0x7f12164a

.field public static final miheadset_notification_title:I = 0x7f12164b

.field public static final miheadset_ok_device:I = 0x7f12164c

.field public static final miheadset_openAnc:I = 0x7f12164d

.field public static final miheadset_play_pause:I = 0x7f12164e

.field public static final miheadset_plus:I = 0x7f12164f

.field public static final miheadset_preferences:I = 0x7f121650

.field public static final miheadset_rename:I = 0x7f121651

.field public static final miheadset_rename_device:I = 0x7f121652

.field public static final miheadset_same_account_incall:I = 0x7f121653

.field public static final miheadset_same_account_incall_summary:I = 0x7f121654

.field public static final miheadset_shared_contacts:I = 0x7f121655

.field public static final miheadset_shared_contacts_summary:I = 0x7f121656

.field public static final miheadset_single_ear_indicate:I = 0x7f121657

.field public static final miheadset_singleear_toast:I = 0x7f121658

.field public static final miheadset_soft:I = 0x7f121659

.field public static final miheadset_sound_settings:I = 0x7f12165a

.field public static final miheadset_spatial_audio_off:I = 0x7f12165b

.field public static final miheadset_spatial_audio_on:I = 0x7f12165c

.field public static final miheadset_spatial_audio_summary:I = 0x7f12165d

.field public static final miheadset_support:I = 0x7f12165e

.field public static final miheadset_transparent:I = 0x7f12165f

.field public static final miheadset_transparentMode:I = 0x7f121660

.field public static final miheadset_triple_click_key_mode:I = 0x7f121661

.field public static final miheadset_triple_press_left:I = 0x7f121662

.field public static final miheadset_triple_press_right:I = 0x7f121663

.field public static final miheadset_unpair_device_text:I = 0x7f121664

.field public static final miheadset_virtual_surround:I = 0x7f121665

.field public static final miheadset_virtual_surround_summary:I = 0x7f121666

.field public static final miheadset_voice_control_summary:I = 0x7f121667

.field public static final miheadset_windnoise:I = 0x7f121668

.field public static final mipush_fake_channel_group_name:I = 0x7f121669

.field public static final misc_files:I = 0x7f12166a

.field public static final misc_files_selected_count:I = 0x7f12166b

.field public static final misc_files_selected_count_bytes:I = 0x7f12166c

.field public static final mishow_disable_password_setting:I = 0x7f12166d

.field public static final miui_accessibility_asr_summary:I = 0x7f12166e

.field public static final miui_accessibility_asr_title:I = 0x7f12166f

.field public static final miui_allow_alarm:I = 0x7f121670

.field public static final miui_allow_light:I = 0x7f121671

.field public static final miui_allow_music:I = 0x7f121672

.field public static final miui_allow_vibrate:I = 0x7f121673

.field public static final miui_bubbles_notification:I = 0x7f121674

.field public static final miui_bubbles_notification_status_opened:I = 0x7f121675

.field public static final miui_data_usage_menu_metered:I = 0x7f121676

.field public static final miui_experience_optimization:I = 0x7f121677

.field public static final miui_face_enroll_risk_warning_btn_cancel:I = 0x7f121678

.field public static final miui_face_enroll_risk_warning_title_text:I = 0x7f121679

.field public static final miui_face_enroll_risk_warning_title_text_cn:I = 0x7f12167a

.field public static final miui_face_enroll_success:I = 0x7f12167b

.field public static final miui_input_method_attention:I = 0x7f12167c

.field public static final miui_input_method_settings:I = 0x7f12167d

.field public static final miui_keyboard_assistance_category:I = 0x7f12167e

.field public static final miui_keyboard_back_light:I = 0x7f12167f

.field public static final miui_keyboard_layout_default_label:I = 0x7f121680

.field public static final miui_keyboard_mouse_touch:I = 0x7f121681

.field public static final miui_keyboard_shortcuts_helper:I = 0x7f121682

.field public static final miui_keyboard_shortcuts_helper_summary:I = 0x7f121683

.field public static final miui_lab_ai_cloud_closed:I = 0x7f121684

.field public static final miui_lab_ai_preload_desc:I = 0x7f121685

.field public static final miui_lab_ai_preload_title:I = 0x7f121686

.field public static final miui_lab_aiasst_call_screen_desc:I = 0x7f121687

.field public static final miui_lab_aiasst_call_screen_title:I = 0x7f121688

.field public static final miui_lab_drive_mode_desc:I = 0x7f121689

.field public static final miui_lab_drive_mode_desc_new:I = 0x7f12168a

.field public static final miui_lab_drive_mode_title:I = 0x7f12168b

.field public static final miui_lab_feature_applied:I = 0x7f12168c

.field public static final miui_lab_feature_off:I = 0x7f12168d

.field public static final miui_lab_feature_on:I = 0x7f12168e

.field public static final miui_lab_features:I = 0x7f12168f

.field public static final miui_lab_gallery_search_desc:I = 0x7f121690

.field public static final miui_lab_gallery_search_title:I = 0x7f121691

.field public static final miui_lab_gesture_desc:I = 0x7f121692

.field public static final miui_lab_gesture_title:I = 0x7f121693

.field public static final miui_lab_install_drive_mode_error:I = 0x7f121694

.field public static final miui_lab_install_drive_mode_summary:I = 0x7f121695

.field public static final miui_lab_install_drive_mode_title:I = 0x7f121696

.field public static final miui_lab_settings:I = 0x7f121697

.field public static final miui_lab_slogan:I = 0x7f121698

.field public static final miui_master_clear_prompt_battery_low:I = 0x7f121699

.field public static final miui_master_clear_summary:I = 0x7f12169a

.field public static final miui_more_special_feature:I = 0x7f12169b

.field public static final miui_nfc_dialog_message:I = 0x7f12169c

.field public static final miui_nfc_dialog_negative:I = 0x7f12169d

.field public static final miui_nfc_dialog_positive:I = 0x7f12169e

.field public static final miui_nfc_dialog_title:I = 0x7f12169f

.field public static final miui_nfc_mi_pay_summary:I = 0x7f1216a0

.field public static final miui_optimization_disabled:I = 0x7f1216a1

.field public static final miui_pointer_settings_category:I = 0x7f1216a2

.field public static final miui_pointer_speed:I = 0x7f1216a3

.field public static final miui_sos_around_photo_summary:I = 0x7f1216a4

.field public static final miui_sos_around_photo_title:I = 0x7f1216a5

.field public static final miui_sos_around_voice_summary:I = 0x7f1216a6

.field public static final miui_sos_around_voice_title:I = 0x7f1216a7

.field public static final miui_sos_audio_summary:I = 0x7f1216a8

.field public static final miui_sos_audio_title:I = 0x7f1216a9

.field public static final miui_sos_call_fire:I = 0x7f1216aa

.field public static final miui_sos_call_hospital:I = 0x7f1216ab

.field public static final miui_sos_call_log_duration:I = 0x7f1216ac

.field public static final miui_sos_call_log_none:I = 0x7f1216ad

.field public static final miui_sos_call_log_stranger:I = 0x7f1216ae

.field public static final miui_sos_call_log_summary:I = 0x7f1216af

.field public static final miui_sos_call_log_time_unit:I = 0x7f1216b0

.field public static final miui_sos_call_log_title:I = 0x7f1216b1

.field public static final miui_sos_call_police:I = 0x7f1216b2

.field public static final miui_sos_call_warning_sim_unable:I = 0x7f1216b3

.field public static final miui_sos_calling_summary:I = 0x7f1216b4

.field public static final miui_sos_calling_title:I = 0x7f1216b5

.field public static final miui_sos_contacts_add:I = 0x7f1216b6

.field public static final miui_sos_contacts_category:I = 0x7f1216b7

.field public static final miui_sos_emergency_contacts:I = 0x7f1216b8

.field public static final miui_sos_emergency_delete:I = 0x7f1216b9

.field public static final miui_sos_emergency_delivered_sms:I = 0x7f1216ba

.field public static final miui_sos_emergency_delivered_sms_noname:I = 0x7f1216bb

.field public static final miui_sos_emergency_failed_sms:I = 0x7f1216bc

.field public static final miui_sos_emergency_failed_sms_noname:I = 0x7f1216bd

.field public static final miui_sos_emergency_title:I = 0x7f1216be

.field public static final miui_sos_enable_switch_summary:I = 0x7f1216bf

.field public static final miui_sos_enable_switch_title:I = 0x7f1216c0

.field public static final miui_sos_exit_dialog_cancel:I = 0x7f1216c1

.field public static final miui_sos_exit_dialog_message:I = 0x7f1216c2

.field public static final miui_sos_exit_dialog_ok:I = 0x7f1216c3

.field public static final miui_sos_exit_dialog_title:I = 0x7f1216c4

.field public static final miui_sos_exit_emergency_title:I = 0x7f1216c5

.field public static final miui_sos_exit_send_location_title:I = 0x7f1216c6

.field public static final miui_sos_green_guard_summary:I = 0x7f1216c7

.field public static final miui_sos_green_guard_title:I = 0x7f1216c8

.field public static final miui_sos_hospital_card:I = 0x7f1216c9

.field public static final miui_sos_hospital_card_important_info:I = 0x7f1216ca

.field public static final miui_sos_launch_error:I = 0x7f1216cb

.field public static final miui_sos_launch_error_confirm:I = 0x7f1216cc

.field public static final miui_sos_launch_error_message:I = 0x7f1216cd

.field public static final miui_sos_launching_summary:I = 0x7f1216ce

.field public static final miui_sos_launching_title:I = 0x7f1216cf

.field public static final miui_sos_menu_add:I = 0x7f1216d0

.field public static final miui_sos_menu_edit:I = 0x7f1216d1

.field public static final miui_sos_msg_call_log_prefix:I = 0x7f1216d2

.field public static final miui_sos_pa_title:I = 0x7f1216d3

.field public static final miui_sos_pa_turn_off:I = 0x7f1216d4

.field public static final miui_sos_pa_turn_on:I = 0x7f1216d5

.field public static final miui_sos_play_voice:I = 0x7f1216d6

.field public static final miui_sos_remind_add:I = 0x7f1216d7

.field public static final miui_sos_remind_close:I = 0x7f1216d8

.field public static final miui_sos_remind_close_confirm:I = 0x7f1216d9

.field public static final miui_sos_remind_infocontent:I = 0x7f1216da

.field public static final miui_sos_remind_open:I = 0x7f1216db

.field public static final miui_sos_remind_sendinfo:I = 0x7f1216dc

.field public static final miui_sos_remind_sendnow:I = 0x7f1216dd

.field public static final miui_sos_remind_sendnow_donot_send:I = 0x7f1216de

.field public static final miui_sos_remind_title:I = 0x7f1216df

.field public static final miui_sos_settings_alert_contacts_repeat:I = 0x7f1216e0

.field public static final miui_sos_settings_alert_contacts_too_many:I = 0x7f1216e1

.field public static final miui_sos_settings_category:I = 0x7f1216e2

.field public static final miui_sos_statusbar_summary:I = 0x7f1216e3

.field public static final miui_sos_statusbar_title:I = 0x7f1216e4

.field public static final miui_sos_unknow_contract:I = 0x7f1216e5

.field public static final miui_special_feature:I = 0x7f1216e6

.field public static final miui_touch_filtered_touchassistant_warning:I = 0x7f1216e7

.field public static final miui_touch_filtered_warning:I = 0x7f1216e8

.field public static final miui_type_style_summary:I = 0x7f1216e9

.field public static final miui_type_style_title:I = 0x7f1216ea

.field public static final miui_updater:I = 0x7f1216eb

.field public static final miui_userguide:I = 0x7f1216ec

.field public static final miuix_access_state_desc:I = 0x7f1216ed

.field public static final miuix_alphabet_indexer_name:I = 0x7f1216ee

.field public static final miuix_appcompat_action_mode_deselect_all:I = 0x7f1216ef

.field public static final miuix_appcompat_action_mode_select_all:I = 0x7f1216f0

.field public static final miuix_appcompat_action_mode_title_empty:I = 0x7f1216f1

.field public static final miuix_appcompat_actionbar_immersion_button_more_description:I = 0x7f1216f2

.field public static final miuix_appcompat_cancel_description:I = 0x7f1216f3

.field public static final miuix_appcompat_confirm_description:I = 0x7f1216f4

.field public static final miuix_appcompat_delete_description:I = 0x7f1216f5

.field public static final miuix_appcompat_deselect_all:I = 0x7f1216f6

.field public static final miuix_appcompat_deselect_all_description:I = 0x7f1216f7

.field public static final miuix_appcompat_search_action_mode_cancel:I = 0x7f1216f8

.field public static final miuix_appcompat_search_input_description:I = 0x7f1216f9

.field public static final miuix_appcompat_select_all:I = 0x7f1216fa

.field public static final miuix_appcompat_select_all_description:I = 0x7f1216fb

.field public static final miuix_appcompat_select_item:I = 0x7f1216fc

.field public static final miuix_indexer_collect:I = 0x7f1216fd

.field public static final miuix_indexer_selected:I = 0x7f1216fe

.field public static final miuix_sbl_tracking_progress_labe_pull_to_refresh:I = 0x7f1216ff

.field public static final miuix_sbl_tracking_progress_labe_refreshed:I = 0x7f121700

.field public static final miuix_sbl_tracking_progress_labe_refreshing:I = 0x7f121701

.field public static final miuix_sbl_tracking_progress_labe_release_to_refresh:I = 0x7f121702

.field public static final miuix_sbl_tracking_progress_labe_up_nodata:I = 0x7f121703

.field public static final miuix_sbl_tracking_progress_labe_up_none:I = 0x7f121704

.field public static final miuix_sbl_tracking_progress_labe_up_refresh:I = 0x7f121705

.field public static final miuix_sbl_tracking_progress_labe_up_refresh_fail:I = 0x7f121706

.field public static final mms_breathing_light_color_title:I = 0x7f121707

.field public static final mms_breathing_light_freq_title:I = 0x7f121708

.field public static final mms_message_summary:I = 0x7f121709

.field public static final mms_message_title:I = 0x7f12170a

.field public static final mms_privacy_password_role_instruction:I = 0x7f12170b

.field public static final mobile_connect_to_internet:I = 0x7f12170c

.field public static final mobile_data_active:I = 0x7f12170d

.field public static final mobile_data_always_on:I = 0x7f12170e

.field public static final mobile_data_always_on_summary:I = 0x7f12170f

.field public static final mobile_data_ap_mode_disabled:I = 0x7f121710

.field public static final mobile_data_connection_active:I = 0x7f121711

.field public static final mobile_data_disable_message:I = 0x7f121712

.field public static final mobile_data_disable_message_default_carrier:I = 0x7f121713

.field public static final mobile_data_disable_title:I = 0x7f121714

.field public static final mobile_data_no_connection:I = 0x7f121715

.field public static final mobile_data_off:I = 0x7f121716

.field public static final mobile_data_off_summary:I = 0x7f121717

.field public static final mobile_data_settings_summary:I = 0x7f121718

.field public static final mobile_data_settings_summary_auto_switch:I = 0x7f121719

.field public static final mobile_data_settings_summary_dds_roaming_unavailable:I = 0x7f12171a

.field public static final mobile_data_settings_summary_default_data_unavailable:I = 0x7f12171b

.field public static final mobile_data_settings_summary_unavailable:I = 0x7f12171c

.field public static final mobile_data_settings_title:I = 0x7f12171d

.field public static final mobile_data_usage_title:I = 0x7f12171e

.field public static final mobile_insert_sim_card:I = 0x7f12171f

.field public static final mobile_network_active_esim:I = 0x7f121720

.field public static final mobile_network_active_sim:I = 0x7f121721

.field public static final mobile_network_apn_title:I = 0x7f121722

.field public static final mobile_network_disable_sim_explanation:I = 0x7f121723

.field public static final mobile_network_erase_sim:I = 0x7f121724

.field public static final mobile_network_erase_sim_error_dialog_body:I = 0x7f121725

.field public static final mobile_network_erase_sim_error_dialog_title:I = 0x7f121726

.field public static final mobile_network_esim_swap_confirm_body:I = 0x7f121727

.field public static final mobile_network_esim_swap_confirm_ok:I = 0x7f121728

.field public static final mobile_network_esim_swap_confirm_title:I = 0x7f121729

.field public static final mobile_network_in_range:I = 0x7f12172a

.field public static final mobile_network_inactive_esim:I = 0x7f12172b

.field public static final mobile_network_inactive_sim:I = 0x7f12172c

.field public static final mobile_network_list_add_more:I = 0x7f12172d

.field public static final mobile_network_mode_error:I = 0x7f12172e

.field public static final mobile_network_not_in_range:I = 0x7f12172f

.field public static final mobile_network_sim_color_label:I = 0x7f121730

.field public static final mobile_network_sim_name:I = 0x7f121731

.field public static final mobile_network_sim_name_label:I = 0x7f121732

.field public static final mobile_network_sim_name_rename:I = 0x7f121733

.field public static final mobile_network_summary_add_a_network:I = 0x7f121734

.field public static final mobile_network_tap_to_activate:I = 0x7f121735

.field public static final mobile_network_use_sim_off:I = 0x7f121736

.field public static final mobile_network_use_sim_on:I = 0x7f121737

.field public static final mobile_no_provisioning_url:I = 0x7f121738

.field public static final mobile_unknown_sim_operator:I = 0x7f121739

.field public static final mock_location_app:I = 0x7f12173a

.field public static final mock_location_app_not_set:I = 0x7f12173b

.field public static final mock_location_app_set:I = 0x7f12173c

.field public static final mode_disable:I = 0x7f12173d

.field public static final mode_enable:I = 0x7f12173e

.field public static final model_hardware_summary:I = 0x7f12173f

.field public static final model_info:I = 0x7f121740

.field public static final model_name:I = 0x7f121741

.field public static final model_number:I = 0x7f121742

.field public static final model_number_pad:I = 0x7f121743

.field public static final modern_control_center_desc:I = 0x7f121744

.field public static final modern_control_center_title:I = 0x7f121745

.field public static final modify_default_browser_message:I = 0x7f121746

.field public static final modify_instruction_password_settings:I = 0x7f121747

.field public static final modify_privacy_password:I = 0x7f121748

.field public static final module_license_title:I = 0x7f121749

.field public static final module_version:I = 0x7f12174a

.field public static final monday:I = 0x7f12174b

.field public static final monday_short:I = 0x7f12174c

.field public static final monday_shortest:I = 0x7f12174d

.field public static final month_april:I = 0x7f12174e

.field public static final month_april_short:I = 0x7f12174f

.field public static final month_april_shortest:I = 0x7f121750

.field public static final month_august:I = 0x7f121751

.field public static final month_august_short:I = 0x7f121752

.field public static final month_august_shortest:I = 0x7f121753

.field public static final month_december:I = 0x7f121754

.field public static final month_december_short:I = 0x7f121755

.field public static final month_december_shortest:I = 0x7f121756

.field public static final month_february:I = 0x7f121757

.field public static final month_february_short:I = 0x7f121758

.field public static final month_february_shortest:I = 0x7f121759

.field public static final month_january:I = 0x7f12175a

.field public static final month_january_short:I = 0x7f12175b

.field public static final month_january_shortest:I = 0x7f12175c

.field public static final month_july:I = 0x7f12175d

.field public static final month_july_short:I = 0x7f12175e

.field public static final month_july_shortest:I = 0x7f12175f

.field public static final month_june:I = 0x7f121760

.field public static final month_june_short:I = 0x7f121761

.field public static final month_june_shortest:I = 0x7f121762

.field public static final month_march:I = 0x7f121763

.field public static final month_march_short:I = 0x7f121764

.field public static final month_march_shortest:I = 0x7f121765

.field public static final month_may:I = 0x7f121766

.field public static final month_may_short:I = 0x7f121767

.field public static final month_may_shortest:I = 0x7f121768

.field public static final month_november:I = 0x7f121769

.field public static final month_november_short:I = 0x7f12176a

.field public static final month_november_shortest:I = 0x7f12176b

.field public static final month_october:I = 0x7f12176c

.field public static final month_october_short:I = 0x7f12176d

.field public static final month_october_shortest:I = 0x7f12176e

.field public static final month_september:I = 0x7f12176f

.field public static final month_september_short:I = 0x7f121770

.field public static final month_september_shortest:I = 0x7f121771

.field public static final more:I = 0x7f121772

.field public static final more_account_settings_title:I = 0x7f121773

.field public static final more_dangerous_option_hint:I = 0x7f121774

.field public static final more_dark_mode_settings:I = 0x7f121775

.field public static final more_font_pref_title:I = 0x7f121776

.field public static final more_installed_services_title:I = 0x7f121777

.field public static final more_settings_button:I = 0x7f121778

.field public static final morning:I = 0x7f121779

.field public static final move_app:I = 0x7f12177a

.field public static final move_app_failed_dlg_text:I = 0x7f12177b

.field public static final move_app_failed_dlg_title:I = 0x7f12177c

.field public static final move_app_to_internal:I = 0x7f12177d

.field public static final move_app_to_sdcard:I = 0x7f12177e

.field public static final move_error_device_admin:I = 0x7f12177f

.field public static final mqs_privacy_automatically_upload:I = 0x7f121780

.field public static final mqs_privacy_dialog_cancel:I = 0x7f121781

.field public static final mqs_privacy_dialog_confirm:I = 0x7f121782

.field public static final mqs_privacy_dialog_content:I = 0x7f121783

.field public static final mqs_privacy_dialog_title:I = 0x7f121784

.field public static final mr_button_content_description:I = 0x7f121785

.field public static final mr_cast_button_connected:I = 0x7f121786

.field public static final mr_cast_button_connecting:I = 0x7f121787

.field public static final mr_cast_button_disconnected:I = 0x7f121788

.field public static final mr_cast_dialog_title_view_placeholder:I = 0x7f121789

.field public static final mr_chooser_searching:I = 0x7f12178a

.field public static final mr_chooser_title:I = 0x7f12178b

.field public static final mr_controller_album_art:I = 0x7f12178c

.field public static final mr_controller_casting_screen:I = 0x7f12178d

.field public static final mr_controller_close_description:I = 0x7f12178e

.field public static final mr_controller_collapse_group:I = 0x7f12178f

.field public static final mr_controller_disconnect:I = 0x7f121790

.field public static final mr_controller_expand_group:I = 0x7f121791

.field public static final mr_controller_no_info_available:I = 0x7f121792

.field public static final mr_controller_no_media_selected:I = 0x7f121793

.field public static final mr_controller_pause:I = 0x7f121794

.field public static final mr_controller_play:I = 0x7f121795

.field public static final mr_controller_stop:I = 0x7f121796

.field public static final mr_controller_stop_casting:I = 0x7f121797

.field public static final mr_controller_volume_slider:I = 0x7f121798

.field public static final mr_dialog_default_group_name:I = 0x7f121799

.field public static final mr_dialog_groupable_header:I = 0x7f12179a

.field public static final mr_dialog_transferable_header:I = 0x7f12179b

.field public static final mr_system_route_name:I = 0x7f12179c

.field public static final mr_user_route_category_name:I = 0x7f12179d

.field public static final msd_driver_download:I = 0x7f12179e

.field public static final msd_driver_download_mac:I = 0x7f12179f

.field public static final msd_driver_download_mac_url:I = 0x7f1217a0

.field public static final msd_driver_download_xp:I = 0x7f1217a1

.field public static final msd_driver_download_xp_url:I = 0x7f1217a2

.field public static final msd_help_summary:I = 0x7f1217a3

.field public static final msd_help_title:I = 0x7f1217a4

.field public static final msd_install:I = 0x7f1217a5

.field public static final msd_install_download_zhushou:I = 0x7f1217a6

.field public static final msd_install_finish:I = 0x7f1217a7

.field public static final msd_install_next:I = 0x7f1217a8

.field public static final msd_install_retry:I = 0x7f1217a9

.field public static final msd_install_retry_connect:I = 0x7f1217aa

.field public static final msd_install_retry_reinstall:I = 0x7f1217ab

.field public static final msd_install_retry_reinstall_mac:I = 0x7f1217ac

.field public static final msd_install_status_done:I = 0x7f1217ad

.field public static final msd_install_status_undo:I = 0x7f1217ae

.field public static final msd_install_succeed:I = 0x7f1217af

.field public static final msd_pc_system_install_finish_summary_vista:I = 0x7f1217b0

.field public static final msd_pc_system_install_finish_summary_xp:I = 0x7f1217b1

.field public static final msd_pc_system_install_summary_xp:I = 0x7f1217b2

.field public static final msg_wlan_signal_found:I = 0x7f1217b3

.field public static final msim_sim_radio_off:I = 0x7f1217b4

.field public static final mtp_connection_failed:I = 0x7f1217b5

.field public static final mtp_connection_failed_title:I = 0x7f1217b6

.field public static final mtp_connection_not_remind:I = 0x7f1217b7

.field public static final mtp_ptp_mode_summary:I = 0x7f1217b8

.field public static final mtp_solution_button_cancel:I = 0x7f1217b9

.field public static final mtp_solution_button_ok:I = 0x7f1217ba

.field public static final mtrl_badge_numberless_content_description:I = 0x7f1217bb

.field public static final mtrl_chip_close_icon_content_description:I = 0x7f1217bc

.field public static final mtrl_exceed_max_badge_number_content_description:I = 0x7f1217bd

.field public static final mtrl_exceed_max_badge_number_suffix:I = 0x7f1217be

.field public static final mtrl_picker_a11y_next_month:I = 0x7f1217bf

.field public static final mtrl_picker_a11y_prev_month:I = 0x7f1217c0

.field public static final mtrl_picker_announce_current_selection:I = 0x7f1217c1

.field public static final mtrl_picker_cancel:I = 0x7f1217c2

.field public static final mtrl_picker_confirm:I = 0x7f1217c3

.field public static final mtrl_picker_date_header_selected:I = 0x7f1217c4

.field public static final mtrl_picker_date_header_title:I = 0x7f1217c5

.field public static final mtrl_picker_date_header_unselected:I = 0x7f1217c6

.field public static final mtrl_picker_day_of_week_column_header:I = 0x7f1217c7

.field public static final mtrl_picker_invalid_format:I = 0x7f1217c8

.field public static final mtrl_picker_invalid_format_example:I = 0x7f1217c9

.field public static final mtrl_picker_invalid_format_use:I = 0x7f1217ca

.field public static final mtrl_picker_invalid_range:I = 0x7f1217cb

.field public static final mtrl_picker_navigate_to_year_description:I = 0x7f1217cc

.field public static final mtrl_picker_out_of_range:I = 0x7f1217cd

.field public static final mtrl_picker_range_header_only_end_selected:I = 0x7f1217ce

.field public static final mtrl_picker_range_header_only_start_selected:I = 0x7f1217cf

.field public static final mtrl_picker_range_header_selected:I = 0x7f1217d0

.field public static final mtrl_picker_range_header_title:I = 0x7f1217d1

.field public static final mtrl_picker_range_header_unselected:I = 0x7f1217d2

.field public static final mtrl_picker_save:I = 0x7f1217d3

.field public static final mtrl_picker_text_input_date_hint:I = 0x7f1217d4

.field public static final mtrl_picker_text_input_date_range_end_hint:I = 0x7f1217d5

.field public static final mtrl_picker_text_input_date_range_start_hint:I = 0x7f1217d6

.field public static final mtrl_picker_text_input_day_abbr:I = 0x7f1217d7

.field public static final mtrl_picker_text_input_month_abbr:I = 0x7f1217d8

.field public static final mtrl_picker_text_input_year_abbr:I = 0x7f1217d9

.field public static final mtrl_picker_toggle_to_calendar_input_mode:I = 0x7f1217da

.field public static final mtrl_picker_toggle_to_day_selection:I = 0x7f1217db

.field public static final mtrl_picker_toggle_to_text_input_mode:I = 0x7f1217dc

.field public static final mtrl_picker_toggle_to_year_selection:I = 0x7f1217dd

.field public static final mtrl_timepicker_confirm:I = 0x7f1217de

.field public static final muffled_text:I = 0x7f1217df

.field public static final multi_face_delete_message:I = 0x7f1217e0

.field public static final multi_face_delete_show_message:I = 0x7f1217e1

.field public static final multi_face_input:I = 0x7f1217e2

.field public static final multi_face_input_reach_upper_limit:I = 0x7f1217e3

.field public static final multi_face_input_success_name_title:I = 0x7f1217e4

.field public static final multi_face_list:I = 0x7f1217e5

.field public static final multi_face_name_prefix:I = 0x7f1217e6

.field public static final multi_face_number_reach_limit:I = 0x7f1217e7

.field public static final multi_network_acceleration_app_settings:I = 0x7f1217e8

.field public static final multi_network_acceleration_summery:I = 0x7f1217e9

.field public static final multi_touch_points_category_title:I = 0x7f1217ea

.field public static final multi_touch_points_preference:I = 0x7f1217eb

.field public static final multi_window_cvw_summary:I = 0x7f1217ec

.field public static final multi_window_cvw_summary_global:I = 0x7f1217ed

.field public static final multi_window_cvw_title:I = 0x7f1217ee

.field public static final multifunction_keyboard:I = 0x7f1217ef

.field public static final multiple_users_main_switch_title:I = 0x7f1217f0

.field public static final music_equalizer:I = 0x7f1217f1

.field public static final music_playback_control_cross_device_flow:I = 0x7f1217f2

.field public static final music_video_flow_across_device:I = 0x7f1217f3

.field public static final musicfx_title:I = 0x7f1217f4

.field public static final mute:I = 0x7f1217f5

.field public static final mute_media_sound_summary:I = 0x7f1217f6

.field public static final mute_media_sound_title:I = 0x7f1217f7

.field public static final mute_music_stream:I = 0x7f1217f8

.field public static final mute_voiceassist_stream:I = 0x7f1217f9

.field public static final mvno_match_data:I = 0x7f1217fa

.field public static final mvno_type:I = 0x7f1217fb

.field public static final my_device:I = 0x7f1217fc

.field public static final my_device_disclaimer:I = 0x7f1217fd

.field public static final my_device_info_account_preference_title:I = 0x7f1217fe

.field public static final my_device_info_basic_info_category_title:I = 0x7f1217ff

.field public static final my_device_info_device_details_category_title:I = 0x7f121800

.field public static final my_device_info_device_identifiers_category_title:I = 0x7f121801

.field public static final my_device_info_device_name_preference_title:I = 0x7f121802

.field public static final my_device_info_legal_category_title:I = 0x7f121803

.field public static final n_cacrts:I = 0x7f121804

.field public static final natural_haptic:I = 0x7f121805

.field public static final natural_haptic_summary:I = 0x7f121806

.field public static final natural_scroll:I = 0x7f121807

.field public static final nature_color:I = 0x7f121808

.field public static final navigation_guide_app_quick_switch_hide_line_summary:I = 0x7f121809

.field public static final navigation_guide_app_quick_switch_summary:I = 0x7f12180a

.field public static final navigation_guide_app_quick_switch_title:I = 0x7f12180b

.field public static final navigation_guide_app_summary:I = 0x7f12180c

.field public static final navigation_guide_app_title:I = 0x7f12180d

.field public static final navigation_guide_appswitch_anim_summary:I = 0x7f12180e

.field public static final navigation_guide_appswitch_anim_title:I = 0x7f12180f

.field public static final navigation_guide_appswitch_click:I = 0x7f121810

.field public static final navigation_guide_appswitch_summary:I = 0x7f121811

.field public static final navigation_guide_appswitch_title:I = 0x7f121812

.field public static final navigation_guide_back_summary:I = 0x7f121813

.field public static final navigation_guide_back_title:I = 0x7f121814

.field public static final navigation_guide_dialog_dont_show_again:I = 0x7f121815

.field public static final navigation_guide_dialog_ok:I = 0x7f121816

.field public static final navigation_guide_dialog_skip:I = 0x7f121817

.field public static final navigation_guide_dialog_summary:I = 0x7f121818

.field public static final navigation_guide_dialog_title:I = 0x7f121819

.field public static final navigation_guide_gesture_line_dialog_summary:I = 0x7f12181a

.field public static final navigation_guide_gesture_line_dialog_title:I = 0x7f12181b

.field public static final navigation_guide_hide_gesture_line_summary:I = 0x7f12181c

.field public static final navigation_guide_hide_gesture_line_title:I = 0x7f12181d

.field public static final navigation_guide_home_summary:I = 0x7f12181e

.field public static final navigation_guide_home_title:I = 0x7f12181f

.field public static final navigation_guide_recent_summary:I = 0x7f121820

.field public static final navigation_guide_recent_title:I = 0x7f121821

.field public static final navigation_guide_settings:I = 0x7f121822

.field public static final navigation_type_radio_text_full_screen:I = 0x7f121823

.field public static final navigation_type_radio_text_virtual_key:I = 0x7f121824

.field public static final navigation_type_settings:I = 0x7f121825

.field public static final nearby_wifi:I = 0x7f121826

.field public static final need_reboot_now:I = 0x7f121827

.field public static final netcheck_title:I = 0x7f121828

.field public static final network_1x:I = 0x7f121829

.field public static final network_2G:I = 0x7f12182a

.field public static final network_2G_3G_preferred:I = 0x7f12182b

.field public static final network_2G_only:I = 0x7f12182c

.field public static final network_3G:I = 0x7f12182d

.field public static final network_3G_only:I = 0x7f12182e

.field public static final network_4G:I = 0x7f12182f

.field public static final network_4G_pure:I = 0x7f121830

.field public static final network_5G_recommended:I = 0x7f121831

.field public static final network_acceleration:I = 0x7f121832

.field public static final network_alarm:I = 0x7f121833

.field public static final network_alarm_summary:I = 0x7f121834

.field public static final network_alarm_summary_reg:I = 0x7f121835

.field public static final network_alarm_support_apps:I = 0x7f121836

.field public static final network_and_internet_preferences_summary:I = 0x7f121837

.field public static final network_and_internet_preferences_title:I = 0x7f121838

.field public static final network_assistant:I = 0x7f121839

.field public static final network_auto_switch:I = 0x7f12183a

.field public static final network_auto_switch_summery:I = 0x7f12183b

.field public static final network_changed_noti_summary:I = 0x7f12183c

.field public static final network_changed_noti_title:I = 0x7f12183d

.field public static final network_changed_notification_text:I = 0x7f12183e

.field public static final network_connected:I = 0x7f12183f

.field public static final network_connecting:I = 0x7f121840

.field public static final network_connection_connect_failure:I = 0x7f121841

.field public static final network_connection_connect_successful:I = 0x7f121842

.field public static final network_connection_connecting_message:I = 0x7f121843

.field public static final network_connection_errorstate_dialog_message:I = 0x7f121844

.field public static final network_connection_request_dialog_showall:I = 0x7f121845

.field public static final network_connection_request_dialog_summary:I = 0x7f121846

.field public static final network_connection_request_dialog_title:I = 0x7f121847

.field public static final network_connection_searching_message:I = 0x7f121848

.field public static final network_connection_timeout_dialog_message:I = 0x7f121849

.field public static final network_connection_timeout_dialog_ok:I = 0x7f12184a

.field public static final network_control:I = 0x7f12184b

.field public static final network_could_not_connect:I = 0x7f12184c

.field public static final network_dashboard_summary_mobile:I = 0x7f12184d

.field public static final network_dashboard_summary_no_mobile:I = 0x7f12184e

.field public static final network_dashboard_title:I = 0x7f12184f

.field public static final network_detail:I = 0x7f121850

.field public static final network_disconnected:I = 0x7f121851

.field public static final network_error:I = 0x7f121852

.field public static final network_error_info:I = 0x7f121853

.field public static final network_global:I = 0x7f121854

.field public static final network_id:I = 0x7f121855

.field public static final network_lte:I = 0x7f121856

.field public static final network_lte_pure:I = 0x7f121857

.field public static final network_name_input_error:I = 0x7f121858

.field public static final network_operator_category:I = 0x7f121859

.field public static final network_operators_settings:I = 0x7f12185a

.field public static final network_query_error:I = 0x7f12185b

.field public static final network_reset_not_available:I = 0x7f12185c

.field public static final network_restrictions:I = 0x7f12185d

.field public static final network_scorer_change_active_dialog_text:I = 0x7f12185e

.field public static final network_scorer_change_active_dialog_title:I = 0x7f12185f

.field public static final network_scorer_change_active_no_previous_dialog_text:I = 0x7f121860

.field public static final network_scorer_picker_none_preference:I = 0x7f121861

.field public static final network_scorer_picker_title:I = 0x7f121862

.field public static final network_select_title:I = 0x7f121863

.field public static final network_settings_title:I = 0x7f121864

.field public static final network_speed_for_apps_close:I = 0x7f121865

.field public static final network_speed_for_apps_close_app_dlg_msg:I = 0x7f121866

.field public static final network_speed_for_apps_close_system_app_dlg_msg:I = 0x7f121867

.field public static final network_speed_for_apps_others:I = 0x7f121868

.field public static final network_speed_for_apps_title:I = 0x7f121869

.field public static final network_speed_for_apps_total:I = 0x7f12186a

.field public static final network_state_label:I = 0x7f12186b

.field public static final network_timeout_info:I = 0x7f12186c

.field public static final network_world_mode_cdma_lte:I = 0x7f12186d

.field public static final network_world_mode_gsm_lte:I = 0x7f12186e

.field public static final networkid_label:I = 0x7f12186f

.field public static final networks:I = 0x7f121870

.field public static final networks_available:I = 0x7f121871

.field public static final networks_title:I = 0x7f121872

.field public static final never:I = 0x7f121873

.field public static final new_backup_pw_prompt:I = 0x7f121874

.field public static final new_fingerprint_to_new_face_dialog_msg:I = 0x7f121875

.field public static final new_fingerprint_to_new_face_dialog_tittle:I = 0x7f121876

.field public static final new_password_to_new_fingerprint_dialog_msg:I = 0x7f121877

.field public static final new_password_to_new_fingerprint_dialog_negative_msg:I = 0x7f121878

.field public static final new_password_to_new_fingerprint_dialog_positive_msg:I = 0x7f121879

.field public static final new_password_to_new_fingerprint_dialog_tittle:I = 0x7f12187a

.field public static final next_button_label:I = 0x7f12187b

.field public static final next_label:I = 0x7f12187c

.field public static final next_page_content_description:I = 0x7f12187d

.field public static final nfc_and_payment_settings_no_payment_installed_summary:I = 0x7f12187e

.field public static final nfc_and_payment_settings_payment_off_nfc_off_summary:I = 0x7f12187f

.field public static final nfc_category_title:I = 0x7f121880

.field public static final nfc_default_payment_footer:I = 0x7f121881

.field public static final nfc_default_payment_settings_title:I = 0x7f121882

.field public static final nfc_default_payment_workapp_confirmation_message_1:I = 0x7f121883

.field public static final nfc_default_payment_workapp_confirmation_message_2:I = 0x7f121884

.field public static final nfc_default_payment_workapp_confirmation_message_title:I = 0x7f121885

.field public static final nfc_default_payment_workapp_confirmation_title:I = 0x7f121886

.field public static final nfc_disabled_summary:I = 0x7f121887

.field public static final nfc_disclaimer_content:I = 0x7f121888

.field public static final nfc_disclaimer_title:I = 0x7f121889

.field public static final nfc_dnd_mode_summary:I = 0x7f12188a

.field public static final nfc_dnd_mode_title:I = 0x7f12188b

.field public static final nfc_how_it_works_content:I = 0x7f12188c

.field public static final nfc_how_it_works_got_it:I = 0x7f12188d

.field public static final nfc_how_it_works_title:I = 0x7f12188e

.field public static final nfc_image_preference_text_click:I = 0x7f12188f

.field public static final nfc_image_preference_text_noclick:I = 0x7f121890

.field public static final nfc_main_switch_title:I = 0x7f121891

.field public static final nfc_more_details:I = 0x7f121892

.field public static final nfc_more_title:I = 0x7f121893

.field public static final nfc_payment_app_and_desc:I = 0x7f121894

.field public static final nfc_payment_btn_text_set_deault:I = 0x7f121895

.field public static final nfc_payment_btn_text_update:I = 0x7f121896

.field public static final nfc_payment_default:I = 0x7f121897

.field public static final nfc_payment_default_not_set:I = 0x7f121898

.field public static final nfc_payment_favor_default:I = 0x7f121899

.field public static final nfc_payment_favor_open:I = 0x7f12189a

.field public static final nfc_payment_how_it_works:I = 0x7f12189b

.field public static final nfc_payment_no_apps:I = 0x7f12189c

.field public static final nfc_payment_pay_with:I = 0x7f12189d

.field public static final nfc_payment_set_default:I = 0x7f12189e

.field public static final nfc_payment_set_default_instead_of:I = 0x7f12189f

.field public static final nfc_payment_set_default_label:I = 0x7f1218a0

.field public static final nfc_payment_settings_title:I = 0x7f1218a1

.field public static final nfc_payment_update_default_label:I = 0x7f1218a2

.field public static final nfc_payment_use_default:I = 0x7f1218a3

.field public static final nfc_payment_use_default_dialog:I = 0x7f1218a4

.field public static final nfc_quick_toggle_summary:I = 0x7f1218a5

.field public static final nfc_quick_toggle_title:I = 0x7f1218a6

.field public static final nfc_repair_action_btn:I = 0x7f1218a7

.field public static final nfc_repair_action_desc:I = 0x7f1218a8

.field public static final nfc_repair_btn_no:I = 0x7f1218a9

.field public static final nfc_repair_btn_reboot:I = 0x7f1218aa

.field public static final nfc_repair_btn_reboot_later:I = 0x7f1218ab

.field public static final nfc_repair_btn_retry:I = 0x7f1218ac

.field public static final nfc_repair_btn_yes:I = 0x7f1218ad

.field public static final nfc_repair_confirm_desc:I = 0x7f1218ae

.field public static final nfc_repair_fail_desc:I = 0x7f1218af

.field public static final nfc_repair_fail_title:I = 0x7f1218b0

.field public static final nfc_repair_ongoing:I = 0x7f1218b1

.field public static final nfc_repair_pass_desc:I = 0x7f1218b2

.field public static final nfc_repair_pass_title:I = 0x7f1218b3

.field public static final nfc_repair_summary:I = 0x7f1218b4

.field public static final nfc_repair_title:I = 0x7f1218b5

.field public static final nfc_se_route_title:I = 0x7f1218b6

.field public static final nfc_se_wallet_title:I = 0x7f1218b7

.field public static final nfc_secure_settings_title:I = 0x7f1218b8

.field public static final nfc_secure_toggle_summary:I = 0x7f1218b9

.field public static final nfc_setting_off:I = 0x7f1218ba

.field public static final nfc_setting_on:I = 0x7f1218bb

.field public static final nfc_stack_debuglog_summary:I = 0x7f1218bc

.field public static final nfc_stack_debuglog_title:I = 0x7f1218bd

.field public static final nfc_work_text:I = 0x7f1218be

.field public static final night:I = 0x7f1218bf

.field public static final night_display_activation_off_custom:I = 0x7f1218c0

.field public static final night_display_activation_off_manual:I = 0x7f1218c1

.field public static final night_display_activation_off_twilight:I = 0x7f1218c2

.field public static final night_display_activation_on_custom:I = 0x7f1218c3

.field public static final night_display_activation_on_manual:I = 0x7f1218c4

.field public static final night_display_activation_on_twilight:I = 0x7f1218c5

.field public static final night_display_auto_mode_custom:I = 0x7f1218c6

.field public static final night_display_auto_mode_never:I = 0x7f1218c7

.field public static final night_display_auto_mode_title:I = 0x7f1218c8

.field public static final night_display_auto_mode_twilight:I = 0x7f1218c9

.field public static final night_display_end_time_title:I = 0x7f1218ca

.field public static final night_display_not_currently_on:I = 0x7f1218cb

.field public static final night_display_start_time_title:I = 0x7f1218cc

.field public static final night_display_status_title:I = 0x7f1218cd

.field public static final night_display_suggestion_summary:I = 0x7f1218ce

.field public static final night_display_suggestion_title:I = 0x7f1218cf

.field public static final night_display_summary_off_auto_mode_custom:I = 0x7f1218d0

.field public static final night_display_summary_off_auto_mode_never:I = 0x7f1218d1

.field public static final night_display_summary_off_auto_mode_twilight:I = 0x7f1218d2

.field public static final night_display_summary_on_auto_mode_custom:I = 0x7f1218d3

.field public static final night_display_summary_on_auto_mode_never:I = 0x7f1218d4

.field public static final night_display_summary_on_auto_mode_twilight:I = 0x7f1218d5

.field public static final night_display_temperature_title:I = 0x7f1218d6

.field public static final night_display_text:I = 0x7f1218d7

.field public static final night_display_title:I = 0x7f1218d8

.field public static final night_light_main_switch_title:I = 0x7f1218d9

.field public static final nls_feature_read_summary:I = 0x7f1218da

.field public static final nls_feature_read_title:I = 0x7f1218db

.field public static final nls_feature_reply_summary:I = 0x7f1218dc

.field public static final nls_feature_reply_title:I = 0x7f1218dd

.field public static final nls_feature_settings_summary:I = 0x7f1218de

.field public static final nls_feature_settings_title:I = 0x7f1218df

.field public static final nls_warning_prompt:I = 0x7f1218e0

.field public static final no:I = 0x7f1218e1

.field public static final no_5g_in_dsds_text:I = 0x7f1218e2

.field public static final no_application:I = 0x7f1218e3

.field public static final no_applications:I = 0x7f1218e4

.field public static final no_background_processes:I = 0x7f1218e5

.field public static final no_battery_summary:I = 0x7f1218e6

.field public static final no_battery_summary_24hr:I = 0x7f1218e7

.field public static final no_carrier_update_now_text:I = 0x7f1218e8

.field public static final no_carrier_update_text:I = 0x7f1218e9

.field public static final no_certificate_management_app:I = 0x7f1218ea

.field public static final no_channels:I = 0x7f1218eb

.field public static final no_connected_devices:I = 0x7f1218ec

.field public static final no_data_usage:I = 0x7f1218ed

.field public static final no_default_apps:I = 0x7f1218ee

.field public static final no_default_home:I = 0x7f1218ef

.field public static final no_device_admins:I = 0x7f1218f0

.field public static final no_internet_access_remember:I = 0x7f1218f1

.field public static final no_internet_access_text:I = 0x7f1218f2

.field public static final no_media_button_receiver:I = 0x7f1218f3

.field public static final no_memory_use_summary:I = 0x7f1218f4

.field public static final no_nearby_wifi:I = 0x7f1218f5

.field public static final no_notification_assistant:I = 0x7f1218f6

.field public static final no_notification_listeners:I = 0x7f1218f7

.field public static final no_running_services:I = 0x7f1218f8

.field public static final no_screen_lock_issue_action_label:I = 0x7f1218f9

.field public static final no_screen_lock_issue_summary:I = 0x7f1218fa

.field public static final no_screen_lock_issue_title:I = 0x7f1218fb

.field public static final no_services:I = 0x7f1218fc

.field public static final no_sim_card:I = 0x7f1218fd

.field public static final no_trust_agents:I = 0x7f1218fe

.field public static final no_update_found:I = 0x7f1218ff

.field public static final no_updates_found:I = 0x7f121900

.field public static final no_updates_warning:I = 0x7f121901

.field public static final no_updates_warning2:I = 0x7f121902

.field public static final no_vr_listeners:I = 0x7f121903

.field public static final noise_suppression:I = 0x7f121904

.field public static final non_carrier_data_usage:I = 0x7f121905

.field public static final non_carrier_data_usage_warning:I = 0x7f121906

.field public static final non_carrier_network_unavailable:I = 0x7f121907

.field public static final none:I = 0x7f121908

.field public static final noon:I = 0x7f121909

.field public static final normal:I = 0x7f12190a

.field public static final normal_info:I = 0x7f12190b

.field public static final normal_list_text:I = 0x7f12190c

.field public static final normal_smartcover_title:I = 0x7f12190d

.field public static final not_allowed:I = 0x7f12190e

.field public static final not_allowed_by_ent:I = 0x7f12190f

.field public static final not_battery_optimizing:I = 0x7f121910

.field public static final not_default_data_content_description:I = 0x7f121911

.field public static final not_enabled:I = 0x7f121912

.field public static final not_encrypted_summary:I = 0x7f121913

.field public static final not_installed:I = 0x7f121914

.field public static final not_set:I = 0x7f121915

.field public static final notch_and_status_bar_settings:I = 0x7f121916

.field public static final notch_force_black_desc:I = 0x7f121917

.field public static final notch_force_black_title:I = 0x7f121918

.field public static final notch_settings:I = 0x7f121919

.field public static final notch_style_mod_description:I = 0x7f12191a

.field public static final notch_style_mod_title:I = 0x7f12191b

.field public static final notch_style_show:I = 0x7f12191c

.field public static final notch_style_status_bar_inside:I = 0x7f12191d

.field public static final notch_style_status_bar_outside:I = 0x7f12191e

.field public static final notes_privacy_password_role_instruction:I = 0x7f12191f

.field public static final notice_header:I = 0x7f121920

.field public static final notif_listener_excluded_app_screen_title:I = 0x7f121921

.field public static final notif_listener_excluded_app_summary:I = 0x7f121922

.field public static final notif_listener_excluded_app_title:I = 0x7f121923

.field public static final notif_listener_not_migrated:I = 0x7f121924

.field public static final notif_type_alerting:I = 0x7f121925

.field public static final notif_type_alerting_summary:I = 0x7f121926

.field public static final notif_type_conversation:I = 0x7f121927

.field public static final notif_type_conversation_summary:I = 0x7f121928

.field public static final notif_type_ongoing:I = 0x7f121929

.field public static final notif_type_ongoing_summary:I = 0x7f12192a

.field public static final notif_type_silent:I = 0x7f12192b

.field public static final notif_type_silent_summary:I = 0x7f12192c

.field public static final notifi_channels:I = 0x7f12192d

.field public static final notifi_channels_other:I = 0x7f12192e

.field public static final notifi_importance_default:I = 0x7f12192f

.field public static final notifi_importance_high:I = 0x7f121930

.field public static final notifi_importance_low:I = 0x7f121931

.field public static final notifi_importance_min:I = 0x7f121932

.field public static final notifi_importance_none:I = 0x7f121933

.field public static final notifi_importance_unspecified:I = 0x7f121934

.field public static final notifi_no_channels:I = 0x7f121935

.field public static final notification_access_detail_switch:I = 0x7f121936

.field public static final notification_alert_title:I = 0x7f121937

.field public static final notification_animation_style:I = 0x7f121938

.field public static final notification_app_settings_button:I = 0x7f121939

.field public static final notification_app_switch_label:I = 0x7f12193a

.field public static final notification_assistant_security_warning_summary:I = 0x7f12193b

.field public static final notification_assistant_security_warning_title:I = 0x7f12193c

.field public static final notification_assistant_summary:I = 0x7f12193d

.field public static final notification_assistant_title:I = 0x7f12193e

.field public static final notification_badge_title:I = 0x7f12193f

.field public static final notification_badging_title:I = 0x7f121940

.field public static final notification_block_title:I = 0x7f121941

.field public static final notification_breathing_light_color_title:I = 0x7f121942

.field public static final notification_breathing_light_freq_title:I = 0x7f121943

.field public static final notification_bubbles_title:I = 0x7f121944

.field public static final notification_center:I = 0x7f121945

.field public static final notification_channel_badge_title:I = 0x7f121946

.field public static final notification_channel_sound_title:I = 0x7f121947

.field public static final notification_channel_summary_default:I = 0x7f121948

.field public static final notification_channel_summary_high:I = 0x7f121949

.field public static final notification_channel_summary_low:I = 0x7f12194a

.field public static final notification_channel_summary_min:I = 0x7f12194b

.field public static final notification_channel_summary_priority:I = 0x7f12194c

.field public static final notification_channel_title:I = 0x7f12194d

.field public static final notification_channels:I = 0x7f12194e

.field public static final notification_channels_other:I = 0x7f12194f

.field public static final notification_content_block_summary:I = 0x7f121950

.field public static final notification_content_block_title:I = 0x7f121951

.field public static final notification_control_center:I = 0x7f121952

.field public static final notification_conversation_add_to_home:I = 0x7f121953

.field public static final notification_conversation_important:I = 0x7f121954

.field public static final notification_conversation_summary:I = 0x7f121955

.field public static final notification_conversation_summary_low:I = 0x7f121956

.field public static final notification_dashboard_summary:I = 0x7f121957

.field public static final notification_display_settings:I = 0x7f121958

.field public static final notification_fold_summary:I = 0x7f121959

.field public static final notification_fold_title:I = 0x7f12195a

.field public static final notification_group_title:I = 0x7f12195b

.field public static final notification_header_divider_symbol_with_spaces:I = 0x7f12195c

.field public static final notification_history:I = 0x7f12195d

.field public static final notification_history_dismiss:I = 0x7f12195e

.field public static final notification_history_off_summary:I = 0x7f12195f

.field public static final notification_history_off_title_extended:I = 0x7f121960

.field public static final notification_history_open_notification:I = 0x7f121961

.field public static final notification_history_snooze:I = 0x7f121962

.field public static final notification_history_summary:I = 0x7f121963

.field public static final notification_history_title:I = 0x7f121964

.field public static final notification_history_today:I = 0x7f121965

.field public static final notification_history_toggle:I = 0x7f121966

.field public static final notification_history_view_settings:I = 0x7f121967

.field public static final notification_importance_blocked:I = 0x7f121968

.field public static final notification_importance_default:I = 0x7f121969

.field public static final notification_importance_default_title:I = 0x7f12196a

.field public static final notification_importance_divider:I = 0x7f12196b

.field public static final notification_importance_high:I = 0x7f12196c

.field public static final notification_importance_high_silent:I = 0x7f12196d

.field public static final notification_importance_high_title:I = 0x7f12196e

.field public static final notification_importance_low:I = 0x7f12196f

.field public static final notification_importance_low_title:I = 0x7f121970

.field public static final notification_importance_min:I = 0x7f121971

.field public static final notification_importance_min_title:I = 0x7f121972

.field public static final notification_importance_title:I = 0x7f121973

.field public static final notification_importance_unspecified:I = 0x7f121974

.field public static final notification_listener_allowed:I = 0x7f121975

.field public static final notification_listener_disable_warning_cancel:I = 0x7f121976

.field public static final notification_listener_disable_warning_confirm:I = 0x7f121977

.field public static final notification_listener_disable_warning_summary:I = 0x7f121978

.field public static final notification_listener_not_allowed:I = 0x7f121979

.field public static final notification_listener_security_warning_summary:I = 0x7f12197a

.field public static final notification_listener_security_warning_title:I = 0x7f12197b

.field public static final notification_listener_type_title:I = 0x7f12197c

.field public static final notification_log_channel:I = 0x7f12197d

.field public static final notification_log_details_actions:I = 0x7f12197e

.field public static final notification_log_details_alerted:I = 0x7f12197f

.field public static final notification_log_details_ashmem:I = 0x7f121980

.field public static final notification_log_details_badge:I = 0x7f121981

.field public static final notification_log_details_content_intent:I = 0x7f121982

.field public static final notification_log_details_content_view:I = 0x7f121983

.field public static final notification_log_details_delete_intent:I = 0x7f121984

.field public static final notification_log_details_delimiter:I = 0x7f121985

.field public static final notification_log_details_explanation:I = 0x7f121986

.field public static final notification_log_details_extras:I = 0x7f121987

.field public static final notification_log_details_full_screen_intent:I = 0x7f121988

.field public static final notification_log_details_group:I = 0x7f121989

.field public static final notification_log_details_group_summary:I = 0x7f12198a

.field public static final notification_log_details_icon:I = 0x7f12198b

.field public static final notification_log_details_importance:I = 0x7f12198c

.field public static final notification_log_details_key:I = 0x7f12198d

.field public static final notification_log_details_none:I = 0x7f12198e

.field public static final notification_log_details_package:I = 0x7f12198f

.field public static final notification_log_details_parcel:I = 0x7f121990

.field public static final notification_log_details_priority:I = 0x7f121991

.field public static final notification_log_details_public_version:I = 0x7f121992

.field public static final notification_log_details_ranking_none:I = 0x7f121993

.field public static final notification_log_details_ranking_null:I = 0x7f121994

.field public static final notification_log_details_remoteinput:I = 0x7f121995

.field public static final notification_log_details_title:I = 0x7f121996

.field public static final notification_log_details_visibility:I = 0x7f121997

.field public static final notification_log_no_title:I = 0x7f121998

.field public static final notification_log_title:I = 0x7f121999

.field public static final notification_people_strip_title:I = 0x7f12199a

.field public static final notification_priority_title:I = 0x7f12199b

.field public static final notification_pulse_title:I = 0x7f12199c

.field public static final notification_reboot_btn:I = 0x7f12199d

.field public static final notification_reboot_summary_hm:I = 0x7f12199e

.field public static final notification_reboot_title:I = 0x7f12199f

.field public static final notification_reboot_title_hm:I = 0x7f1219a0

.field public static final notification_remind:I = 0x7f1219a1

.field public static final notification_ringtone_title:I = 0x7f1219a2

.field public static final notification_screen_summary:I = 0x7f1219a3

.field public static final notification_settings:I = 0x7f1219a4

.field public static final notification_settings_work_profile:I = 0x7f1219a5

.field public static final notification_shade_shortcut_title:I = 0x7f1219a6

.field public static final notification_show_lights_title:I = 0x7f1219a7

.field public static final notification_silence_title:I = 0x7f1219a8

.field public static final notification_sound_default:I = 0x7f1219a9

.field public static final notification_sound_dialog_title:I = 0x7f1219aa

.field public static final notification_sound_title:I = 0x7f1219ab

.field public static final notification_style_title:I = 0x7f1219ac

.field public static final notification_suggestion_summary:I = 0x7f1219ad

.field public static final notification_suggestion_title:I = 0x7f1219ae

.field public static final notification_summary_channel:I = 0x7f1219af

.field public static final notification_summary_level:I = 0x7f1219b0

.field public static final notification_summary_none:I = 0x7f1219b1

.field public static final notification_switch_label:I = 0x7f1219b2

.field public static final notification_toggle_off:I = 0x7f1219b3

.field public static final notification_toggle_on:I = 0x7f1219b4

.field public static final notification_unknown_sound_title:I = 0x7f1219b5

.field public static final notification_update_new_version:I = 0x7f1219b6

.field public static final notification_update_now:I = 0x7f1219b7

.field public static final notification_vibrate_title:I = 0x7f1219b8

.field public static final notification_volume_option_title:I = 0x7f1219b9

.field public static final notification_volume_title:I = 0x7f1219ba

.field public static final notifications_bubble_setting_description:I = 0x7f1219bb

.field public static final notifications_bubble_setting_on_summary:I = 0x7f1219bc

.field public static final notifications_bubble_setting_title:I = 0x7f1219bd

.field public static final notifications_disabled:I = 0x7f1219be

.field public static final notifications_enabled:I = 0x7f1219bf

.field public static final notifications_enabled_with_info:I = 0x7f1219c0

.field public static final notifications_hidden:I = 0x7f1219c1

.field public static final notifications_label:I = 0x7f1219c2

.field public static final notifications_partly_blocked:I = 0x7f1219c3

.field public static final notifications_priority:I = 0x7f1219c4

.field public static final notifications_redacted:I = 0x7f1219c5

.field public static final notifications_sent_never:I = 0x7f1219c6

.field public static final notifications_silenced:I = 0x7f1219c7

.field public static final notifications_summary_divider:I = 0x7f1219c8

.field public static final notifications_title:I = 0x7f1219c9

.field public static final now:I = 0x7f1219ca

.field public static final nr_advanced_calling_summary:I = 0x7f1219cb

.field public static final nr_advanced_calling_title:I = 0x7f1219cc

.field public static final nrca_need_reset:I = 0x7f1219cd

.field public static final nrca_switch_title:I = 0x7f1219ce

.field public static final number_of_device_admins_none:I = 0x7f1219cf

.field public static final oaid_apps_summary:I = 0x7f1219d0

.field public static final oaid_apps_title:I = 0x7f1219d1

.field public static final oaid_string:I = 0x7f1219d2

.field public static final oem_lock_info_message:I = 0x7f1219d3

.field public static final oem_preferred_feedback_reporter:I = 0x7f1219d4

.field public static final oem_unlock_enable:I = 0x7f1219d5

.field public static final oem_unlock_enable_disabled_summary_bootloader_unlocked:I = 0x7f1219d6

.field public static final oem_unlock_enable_disabled_summary_connectivity:I = 0x7f1219d7

.field public static final oem_unlock_enable_disabled_summary_connectivity_or_locked:I = 0x7f1219d8

.field public static final oem_unlock_enable_disabled_summary_sim_locked_device:I = 0x7f1219d9

.field public static final oem_unlock_enable_summary:I = 0x7f1219da

.field public static final off:I = 0x7f1219db

.field public static final ok:I = 0x7f1219dc

.field public static final ok_rules:I = 0x7f1219dd

.field public static final okay:I = 0x7f1219de

.field public static final oldman_app_label:I = 0x7f1219df

.field public static final oldman_installed_current_btn:I = 0x7f1219e0

.field public static final oldman_installed_current_des:I = 0x7f1219e1

.field public static final oldman_installed_not_current_btn:I = 0x7f1219e2

.field public static final oldman_installed_not_current_des:I = 0x7f1219e3

.field public static final oldman_mode_entry_name:I = 0x7f1219e4

.field public static final oldman_mode_settings:I = 0x7f1219e5

.field public static final oldman_uninstalled_btn:I = 0x7f1219e6

.field public static final oldman_uninstalled_des:I = 0x7f1219e7

.field public static final on:I = 0x7f1219e8

.field public static final one_cacrt:I = 0x7f1219e9

.field public static final one_handed_action_pull_down_screen_summary:I = 0x7f1219ea

.field public static final one_handed_action_pull_down_screen_title:I = 0x7f1219eb

.field public static final one_handed_action_show_notification_summary:I = 0x7f1219ec

.field public static final one_handed_action_show_notification_title:I = 0x7f1219ed

.field public static final one_handed_mode_enabled:I = 0x7f1219ee

.field public static final one_handed_mode_footer_text:I = 0x7f1219ef

.field public static final one_handed_mode_intro_text:I = 0x7f1219f0

.field public static final one_handed_mode_shortcut_title:I = 0x7f1219f1

.field public static final one_handed_mode_swipe_down_category:I = 0x7f1219f2

.field public static final one_handed_mode_use_shortcut_category:I = 0x7f1219f3

.field public static final one_handed_title:I = 0x7f1219f4

.field public static final one_key_migrate_record:I = 0x7f1219f5

.field public static final one_usercrt:I = 0x7f1219f6

.field public static final one_userkey:I = 0x7f1219f7

.field public static final onehour:I = 0x7f1219f8

.field public static final onekey_migrate:I = 0x7f1219f9

.field public static final onscreen_keyboard_settings_summary:I = 0x7f1219fa

.field public static final open_app_button:I = 0x7f1219fb

.field public static final open_demo_mode:I = 0x7f1219fc

.field public static final open_demo_mode_warning_message:I = 0x7f1219fd

.field public static final open_game_accelerate:I = 0x7f1219fe

.field public static final open_haptic_feedback:I = 0x7f1219ff

.field public static final open_miui_optimization_title:I = 0x7f121a00

.field public static final open_optimization_message:I = 0x7f121a01

.field public static final open_optimization_option:I = 0x7f121a02

.field public static final open_privacy_thumbnail_blur_title:I = 0x7f121a03

.field public static final open_supported_links_footer:I = 0x7f121a04

.field public static final open_wifi_connect_summary:I = 0x7f121a05

.field public static final open_wifi_connect_title:I = 0x7f121a06

.field public static final opening_paragraph_delete_profile_unknown_company:I = 0x7f121a07

.field public static final operation_guide_title:I = 0x7f121a08

.field public static final operator_warning:I = 0x7f121a09

.field public static final optimization_cancel:I = 0x7f121a0a

.field public static final osu_completing_sign_up:I = 0x7f121a0b

.field public static final osu_connect_failed:I = 0x7f121a0c

.field public static final osu_opening_provider:I = 0x7f121a0d

.field public static final osu_sign_up_complete:I = 0x7f121a0e

.field public static final osu_sign_up_failed:I = 0x7f121a0f

.field public static final ota_disable_automatic_update:I = 0x7f121a10

.field public static final ota_disable_automatic_update_summary:I = 0x7f121a11

.field public static final otg_button:I = 0x7f121a12

.field public static final otg_closed:I = 0x7f121a13

.field public static final otg_opened:I = 0x7f121a14

.field public static final otg_settings:I = 0x7f121a15

.field public static final otg_tip:I = 0x7f121a16

.field public static final other:I = 0x7f121a17

.field public static final other_advanced_settings:I = 0x7f121a18

.field public static final other_conversations:I = 0x7f121a19

.field public static final other_conversations_summary:I = 0x7f121a1a

.field public static final other_data:I = 0x7f121a1b

.field public static final other_device_info:I = 0x7f121a1c

.field public static final other_input_method:I = 0x7f121a1d

.field public static final other_input_settings:I = 0x7f121a1e

.field public static final other_password:I = 0x7f121a1f

.field public static final other_personal_settings:I = 0x7f121a20

.field public static final other_relative:I = 0x7f121a21

.field public static final other_settings_sync_settings:I = 0x7f121a22

.field public static final other_sound_category_preference_title:I = 0x7f121a23

.field public static final other_sound_settings:I = 0x7f121a24

.field public static final other_sound_settings_title:I = 0x7f121a25

.field public static final overlay_display_devices_title:I = 0x7f121a26

.field public static final overlay_option_device_default:I = 0x7f121a27

.field public static final overlay_settings:I = 0x7f121a28

.field public static final overlay_settings_summary:I = 0x7f121a29

.field public static final overlay_settings_title:I = 0x7f121a2a

.field public static final overlay_toast_failed_to_apply:I = 0x7f121a2b

.field public static final owner_info_settings_edit_text_hint:I = 0x7f121a2c

.field public static final owner_info_settings_status:I = 0x7f121a2d

.field public static final owner_info_settings_summary:I = 0x7f121a2e

.field public static final owner_info_settings_title:I = 0x7f121a2f

.field public static final owner_space:I = 0x7f121a30

.field public static final packages_subtitle:I = 0x7f121a31

.field public static final pad_add_fingerprint_finish_message:I = 0x7f121a32

.field public static final pad_add_fingerprint_message:I = 0x7f121a33

.field public static final page_layout_1:I = 0x7f121a34

.field public static final page_layout_10:I = 0x7f121a35

.field public static final page_layout_11:I = 0x7f121a36

.field public static final page_layout_12:I = 0x7f121a37

.field public static final page_layout_13:I = 0x7f121a38

.field public static final page_layout_18:I = 0x7f121a39

.field public static final page_layout_19:I = 0x7f121a3a

.field public static final page_layout_2:I = 0x7f121a3b

.field public static final page_layout_20:I = 0x7f121a3c

.field public static final page_layout_21:I = 0x7f121a3d

.field public static final page_layout_22:I = 0x7f121a3e

.field public static final page_layout_23:I = 0x7f121a3f

.field public static final page_layout_24:I = 0x7f121a40

.field public static final page_layout_25:I = 0x7f121a41

.field public static final page_layout_26:I = 0x7f121a42

.field public static final page_layout_27:I = 0x7f121a43

.field public static final page_layout_28:I = 0x7f121a44

.field public static final page_layout_29:I = 0x7f121a45

.field public static final page_layout_3:I = 0x7f121a46

.field public static final page_layout_30:I = 0x7f121a47

.field public static final page_layout_31:I = 0x7f121a48

.field public static final page_layout_32:I = 0x7f121a49

.field public static final page_layout_33:I = 0x7f121a4a

.field public static final page_layout_34:I = 0x7f121a4b

.field public static final page_layout_35:I = 0x7f121a4c

.field public static final page_layout_4:I = 0x7f121a4d

.field public static final page_layout_5:I = 0x7f121a4e

.field public static final page_layout_6:I = 0x7f121a4f

.field public static final page_layout_7:I = 0x7f121a50

.field public static final page_layout_8:I = 0x7f121a51

.field public static final page_layout_9:I = 0x7f121a52

.field public static final page_layout_confirm_message:I = 0x7f121a53

.field public static final page_layout_confirm_message_enlarge:I = 0x7f121a54

.field public static final page_layout_confirm_message_narrow:I = 0x7f121a55

.field public static final page_layout_confirm_title:I = 0x7f121a56

.field public static final page_layout_confirm_title_enlarge:I = 0x7f121a57

.field public static final page_layout_confirm_title_narrow:I = 0x7f121a58

.field public static final page_layout_godzilla_title:I = 0x7f121a59

.field public static final page_layout_huge_title:I = 0x7f121a5a

.field public static final page_layout_large_title:I = 0x7f121a5b

.field public static final page_layout_medium_title:I = 0x7f121a5c

.field public static final page_layout_normal_title:I = 0x7f121a5d

.field public static final page_layout_page2_godzilla_summary:I = 0x7f121a5e

.field public static final page_layout_page2_godzilla_title:I = 0x7f121a5f

.field public static final page_layout_page2_huge_summary:I = 0x7f121a60

.field public static final page_layout_page2_huge_title:I = 0x7f121a61

.field public static final page_layout_page2_large_summary:I = 0x7f121a62

.field public static final page_layout_page2_large_title:I = 0x7f121a63

.field public static final page_layout_page2_medium_summary:I = 0x7f121a64

.field public static final page_layout_page2_medium_title:I = 0x7f121a65

.field public static final page_layout_page2_normal_summary:I = 0x7f121a66

.field public static final page_layout_page2_normal_title:I = 0x7f121a67

.field public static final page_layout_page2_small_summary:I = 0x7f121a68

.field public static final page_layout_page2_small_title:I = 0x7f121a69

.field public static final page_layout_page2_title:I = 0x7f121a6a

.field public static final page_layout_small_title:I = 0x7f121a6b

.field public static final page_tab_title_summary:I = 0x7f121a6c

.field public static final page_tab_title_support:I = 0x7f121a6d

.field public static final paper_color_title:I = 0x7f121a6e

.field public static final paper_mode_auto_twilight_summary:I = 0x7f121a6f

.field public static final paper_mode_auto_twilight_title:I = 0x7f121a70

.field public static final paper_mode_customize_time_summary:I = 0x7f121a71

.field public static final paper_mode_customize_time_title:I = 0x7f121a72

.field public static final paper_mode_end_time_title:I = 0x7f121a73

.field public static final paper_mode_get_location_dlg_msg:I = 0x7f121a74

.field public static final paper_mode_get_location_dlg_negative_btn_text:I = 0x7f121a75

.field public static final paper_mode_get_location_dlg_positive_btn_text:I = 0x7f121a76

.field public static final paper_mode_get_location_dlg_title:I = 0x7f121a77

.field public static final paper_mode_reset:I = 0x7f121a78

.field public static final paper_mode_side_effect_hint:I = 0x7f121a79

.field public static final paper_mode_start_time_title:I = 0x7f121a7a

.field public static final paper_mode_summary:I = 0x7f121a7b

.field public static final paper_mode_title:I = 0x7f121a7c

.field public static final papger_effect_category:I = 0x7f121a7d

.field public static final params_interpretation:I = 0x7f121a7e

.field public static final partial_connectivity_text:I = 0x7f121a7f

.field public static final passpoint_content:I = 0x7f121a80

.field public static final passpoint_label:I = 0x7f121a81

.field public static final passpoint_r1_privacy_dialog_message:I = 0x7f121a82

.field public static final passpoint_r1_privacy_dialog_no:I = 0x7f121a83

.field public static final passpoint_r1_privacy_dialog_title:I = 0x7f121a84

.field public static final passpoint_r1_privacy_dialog_url_text:I = 0x7f121a85

.field public static final passpoint_r1_privacy_dialog_yes:I = 0x7f121a86

.field public static final passwordIncorrect:I = 0x7f121a87

.field public static final password_and_security:I = 0x7f121a88

.field public static final password_entrance_title:I = 0x7f121a89

.field public static final password_guard:I = 0x7f121a8a

.field public static final password_security_settings_title:I = 0x7f121a8b

.field public static final password_toggle_content_description:I = 0x7f121a8c

.field public static final password_turned_off_text:I = 0x7f121a8d

.field public static final password_turned_on_text:I = 0x7f121a8e

.field public static final password_unlock:I = 0x7f121a8f

.field public static final password_unlock_title:I = 0x7f121a90

.field public static final path_password_eye:I = 0x7f121a91

.field public static final path_password_eye_mask_strike_through:I = 0x7f121a92

.field public static final path_password_eye_mask_visible:I = 0x7f121a93

.field public static final path_password_strike_through:I = 0x7f121a94

.field public static final payment_summary:I = 0x7f121a95

.field public static final peak_refresh_rate_summary:I = 0x7f121a96

.field public static final peak_refresh_rate_title:I = 0x7f121a97

.field public static final peek_notification_icon_title:I = 0x7f121a98

.field public static final performance_category:I = 0x7f121a99

.field public static final perm_toggle_description:I = 0x7f121a9a

.field public static final permission_action_prompt:I = 0x7f121a9b

.field public static final permission_bar_chart_details:I = 0x7f121a9c

.field public static final permission_bar_chart_empty_text:I = 0x7f121a9d

.field public static final permission_bar_chart_title:I = 0x7f121a9e

.field public static final permission_detail_info:I = 0x7f121a9f

.field public static final permission_detail_info_summary:I = 0x7f121aa0

.field public static final permission_manage:I = 0x7f121aa1

.field public static final permission_manage_global_build_title:I = 0x7f121aa2

.field public static final permission_manage_summary:I = 0x7f121aa3

.field public static final permission_manage_title:I = 0x7f121aa4

.field public static final permission_manager:I = 0x7f121aa5

.field public static final permission_mgr:I = 0x7f121aa6

.field public static final permission_records:I = 0x7f121aa7

.field public static final permission_records_summary:I = 0x7f121aa8

.field public static final permission_settings_summary:I = 0x7f121aa9

.field public static final permission_settings_title:I = 0x7f121aaa

.field public static final permissions_label:I = 0x7f121aab

.field public static final permissions_usage_summary:I = 0x7f121aac

.field public static final permissions_usage_title:I = 0x7f121aad

.field public static final permit_draw_overlay:I = 0x7f121aae

.field public static final permit_manage_external_storage:I = 0x7f121aaf

.field public static final permit_running_network:I = 0x7f121ab0

.field public static final permit_usage_access:I = 0x7f121ab1

.field public static final permit_write_settings:I = 0x7f121ab2

.field public static final person_hotpot:I = 0x7f121ab3

.field public static final personal_data_section_title:I = 0x7f121ab4

.field public static final personal_profile_app_subtext:I = 0x7f121ab5

.field public static final personal_section_title:I = 0x7f121ab6

.field public static final personalize_title:I = 0x7f121ab7

.field public static final phone_backup_summary:I = 0x7f121ab8

.field public static final phone_backup_title:I = 0x7f121ab9

.field public static final phone_call_noise_suppression:I = 0x7f121aba

.field public static final phone_language:I = 0x7f121abb

.field public static final phone_language_summary:I = 0x7f121abc

.field public static final photo_data:I = 0x7f121abd

.field public static final physical_keyboard_title:I = 0x7f121abe

.field public static final pick_up_gesture_wakeup_title:I = 0x7f121abf

.field public static final picker_text:I = 0x7f121ac0

.field public static final picture_color_mode:I = 0x7f121ac1

.field public static final picture_color_mode_desc:I = 0x7f121ac2

.field public static final picture_in_picture_app_detail_summary:I = 0x7f121ac3

.field public static final picture_in_picture_app_detail_switch:I = 0x7f121ac4

.field public static final picture_in_picture_app_detail_title:I = 0x7f121ac5

.field public static final picture_in_picture_empty_text:I = 0x7f121ac6

.field public static final picture_in_picture_keywords:I = 0x7f121ac7

.field public static final picture_in_picture_title:I = 0x7f121ac8

.field public static final pin_failed:I = 0x7f121ac9

.field public static final ping_test_label:I = 0x7f121aca

.field public static final placeholder_activity:I = 0x7f121acb

.field public static final platform_compat_dashboard_summary:I = 0x7f121acc

.field public static final platform_compat_dashboard_title:I = 0x7f121acd

.field public static final platform_compat_default_disabled_title:I = 0x7f121ace

.field public static final platform_compat_default_enabled_title:I = 0x7f121acf

.field public static final platform_compat_dialog_text_no_apps:I = 0x7f121ad0

.field public static final platform_compat_dialog_title_no_apps:I = 0x7f121ad1

.field public static final platform_compat_selected_app_summary:I = 0x7f121ad2

.field public static final platform_compat_target_sdk_title:I = 0x7f121ad3

.field public static final please_select_phase2:I = 0x7f121ad4

.field public static final please_type_passphrase:I = 0x7f121ad5

.field public static final plmn_failure_radio_off:I = 0x7f121ad6

.field public static final pm:I = 0x7f121ad7

.field public static final poco_launcher_privacy_policy:I = 0x7f121ad8

.field public static final poco_launcher_user_agreement:I = 0x7f121ad9

.field public static final pointer_location:I = 0x7f121ada

.field public static final pointer_location_summary:I = 0x7f121adb

.field public static final pointer_settings_category:I = 0x7f121adc

.field public static final pointer_speed:I = 0x7f121add

.field public static final popup_gesture_summary:I = 0x7f121ade

.field public static final popup_gesture_title:I = 0x7f121adf

.field public static final popup_led_summary:I = 0x7f121ae0

.field public static final popup_led_title:I = 0x7f121ae1

.field public static final popup_led_try_later:I = 0x7f121ae2

.field public static final popup_title:I = 0x7f121ae3

.field public static final popup_title_cangmen:I = 0x7f121ae4

.field public static final popup_title_chilun:I = 0x7f121ae5

.field public static final popup_title_jijia:I = 0x7f121ae6

.field public static final popup_title_mofa:I = 0x7f121ae7

.field public static final popup_title_muqin:I = 0x7f121ae8

.field public static final popup_title_yingyan:I = 0x7f121ae9

.field public static final popup_voice_summary:I = 0x7f121aea

.field public static final popup_voice_title:I = 0x7f121aeb

.field public static final popup_window:I = 0x7f121aec

.field public static final post_dsds_reboot_notification_text:I = 0x7f121aed

.field public static final post_dsds_reboot_notification_title_with_carrier:I = 0x7f121aee

.field public static final power_autorun_apps_empty:I = 0x7f121aef

.field public static final power_autorun_empty:I = 0x7f121af0

.field public static final power_bluetooth:I = 0x7f121af1

.field public static final power_camera:I = 0x7f121af2

.field public static final power_cell:I = 0x7f121af3

.field public static final power_center:I = 0x7f121af4

.field public static final power_charge_remaining:I = 0x7f121af5

.field public static final power_charging:I = 0x7f121af6

.field public static final power_charging_duration:I = 0x7f121af7

.field public static final power_charging_limited:I = 0x7f121af8

.field public static final power_checker:I = 0x7f121af9

.field public static final power_discharge_by:I = 0x7f121afa

.field public static final power_discharge_by_enhanced:I = 0x7f121afb

.field public static final power_discharge_by_only:I = 0x7f121afc

.field public static final power_discharge_by_only_enhanced:I = 0x7f121afd

.field public static final power_discharge_by_only_short:I = 0x7f121afe

.field public static final power_discharge_remaining:I = 0x7f121aff

.field public static final power_discharging_duration:I = 0x7f121b00

.field public static final power_discharging_duration_enhanced:I = 0x7f121b01

.field public static final power_flashlight:I = 0x7f121b02

.field public static final power_hide_mode_summary:I = 0x7f121b03

.field public static final power_hide_mode_title:I = 0x7f121b04

.field public static final power_idle:I = 0x7f121b05

.field public static final power_key_how_power_force_off:I = 0x7f121b06

.field public static final power_key_how_power_force_off_detail:I = 0x7f121b07

.field public static final power_key_how_power_off:I = 0x7f121b08

.field public static final power_key_how_power_off_detail:I = 0x7f121b09

.field public static final power_key_how_start:I = 0x7f121b0a

.field public static final power_key_how_start_detail:I = 0x7f121b0b

.field public static final power_key_start_enjoy:I = 0x7f121b0c

.field public static final power_key_start_guide:I = 0x7f121b0d

.field public static final power_label:I = 0x7f121b0e

.field public static final power_menu_long_press_for_assist:I = 0x7f121b0f

.field public static final power_menu_long_press_for_assist_sensitivity_high_label:I = 0x7f121b10

.field public static final power_menu_long_press_for_assist_sensitivity_low_label:I = 0x7f121b11

.field public static final power_menu_long_press_for_assist_sensitivity_summary:I = 0x7f121b12

.field public static final power_menu_long_press_for_assist_sensitivity_title:I = 0x7f121b13

.field public static final power_menu_long_press_for_assist_summary:I = 0x7f121b14

.field public static final power_menu_power_prevent_ringing_hint:I = 0x7f121b15

.field public static final power_menu_power_volume_up_hint:I = 0x7f121b16

.field public static final power_menu_setting_name:I = 0x7f121b17

.field public static final power_menu_summary_long_press_for_assist_disabled_no_action:I = 0x7f121b18

.field public static final power_menu_summary_long_press_for_assist_disabled_with_power_menu:I = 0x7f121b19

.field public static final power_menu_summary_long_press_for_assist_enabled:I = 0x7f121b1a

.field public static final power_mgr:I = 0x7f121b1b

.field public static final power_mode:I = 0x7f121b1c

.field public static final power_mode_balanced_title:I = 0x7f121b1d

.field public static final power_monitor_summary:I = 0x7f121b1e

.field public static final power_monitor_title:I = 0x7f121b1f

.field public static final power_optimize:I = 0x7f121b20

.field public static final power_overcounted:I = 0x7f121b21

.field public static final power_phone:I = 0x7f121b22

.field public static final power_remaining_charging_duration_only:I = 0x7f121b23

.field public static final power_remaining_duration_only:I = 0x7f121b24

.field public static final power_remaining_duration_only_enhanced:I = 0x7f121b25

.field public static final power_remaining_duration_only_short:I = 0x7f121b26

.field public static final power_remaining_duration_only_shutdown_imminent:I = 0x7f121b27

.field public static final power_remaining_duration_shutdown_imminent:I = 0x7f121b28

.field public static final power_remaining_less_than_duration:I = 0x7f121b29

.field public static final power_remaining_less_than_duration_only:I = 0x7f121b2a

.field public static final power_remaining_more_than_subtext:I = 0x7f121b2b

.field public static final power_remaining_only_more_than_subtext:I = 0x7f121b2c

.field public static final power_remaining_settings_home_page:I = 0x7f121b2d

.field public static final power_screen:I = 0x7f121b2e

.field public static final power_settings_title:I = 0x7f121b2f

.field public static final power_suggestion_battery_run_out:I = 0x7f121b30

.field public static final power_unaccounted:I = 0x7f121b31

.field public static final power_usage_enhanced_debug:I = 0x7f121b32

.field public static final power_usage_history:I = 0x7f121b33

.field public static final power_usage_level_and_status:I = 0x7f121b34

.field public static final power_usage_list_summary:I = 0x7f121b35

.field public static final power_usage_not_available:I = 0x7f121b36

.field public static final power_usage_old_debug:I = 0x7f121b37

.field public static final power_usage_summary:I = 0x7f121b38

.field public static final power_usage_summary_title:I = 0x7f121b39

.field public static final power_usage_summary_title_new:I = 0x7f121b3a

.field public static final power_wifi:I = 0x7f121b3b

.field public static final pre_install_app_browser:I = 0x7f121b3c

.field public static final pre_install_app_camera:I = 0x7f121b3d

.field public static final pre_install_app_gallery:I = 0x7f121b3e

.field public static final pre_install_app_other:I = 0x7f121b3f

.field public static final pre_install_app_phone:I = 0x7f121b40

.field public static final pre_install_app_sms:I = 0x7f121b41

.field public static final pre_install_app_weather:I = 0x7f121b42

.field public static final pre_install_application:I = 0x7f121b43

.field public static final pre_install_category_appname:I = 0x7f121b44

.field public static final pre_install_category_question:I = 0x7f121b45

.field public static final pre_install_network_unavailable:I = 0x7f121b46

.field public static final pre_install_question_disable_summary:I = 0x7f121b47

.field public static final pre_install_question_disable_title:I = 0x7f121b48

.field public static final pre_install_question_query_summary:I = 0x7f121b49

.field public static final pre_install_question_query_title:I = 0x7f121b4a

.field public static final pre_install_question_uninstall_summary:I = 0x7f121b4b

.field public static final pre_install_question_uninstall_title:I = 0x7f121b4c

.field public static final pre_installed_application:I = 0x7f121b4d

.field public static final pref_bg_network_title:I = 0x7f121b4e

.field public static final pref_button_light:I = 0x7f121b4f

.field public static final pref_button_light_summary:I = 0x7f121b50

.field public static final pref_button_light_summary_sub:I = 0x7f121b51

.field public static final pref_edge_handgrip:I = 0x7f121b52

.field public static final pref_key_tethering_setting:I = 0x7f121b53

.field public static final pref_key_tethering_title:I = 0x7f121b54

.field public static final pref_manage_bg_network:I = 0x7f121b55

.field public static final pref_new_installed_preconfig_title:I = 0x7f121b56

.field public static final pref_operational_key:I = 0x7f121b57

.field public static final pref_show_lunar_calendar_title:I = 0x7f121b58

.field public static final pref_title_network_details:I = 0x7f121b59

.field public static final pref_title_update_notification:I = 0x7f121b5a

.field public static final pref_volume_launch_camera_summary:I = 0x7f121b5b

.field public static final pref_volume_launch_camera_title:I = 0x7f121b5c

.field public static final pref_volume_wake_summary:I = 0x7f121b5d

.field public static final pref_volume_wake_title:I = 0x7f121b5e

.field public static final prefer_vonr_mode_summary:I = 0x7f121b5f

.field public static final prefer_vonr_mode_title:I = 0x7f121b60

.field public static final preference_change_password_title:I = 0x7f121b61

.field public static final preference_copied:I = 0x7f121b62

.field public static final preference_of_system_locale_summary:I = 0x7f121b63

.field public static final preference_of_system_locale_title:I = 0x7f121b64

.field public static final preference_summary_default_combination:I = 0x7f121b65

.field public static final preference_toggle_nfc_dnd_mode_summary:I = 0x7f121b66

.field public static final preferred_app_settings:I = 0x7f121b67

.field public static final preferred_app_settings_default:I = 0x7f121b68

.field public static final preferred_app_settings_not_selected:I = 0x7f121b69

.field public static final preferred_app_settings_reset:I = 0x7f121b6a

.field public static final preferred_app_settings_reset_button:I = 0x7f121b6b

.field public static final preferred_app_settings_reset_message:I = 0x7f121b6c

.field public static final preferred_application_title:I = 0x7f121b6d

.field public static final preferred_application_warning_message:I = 0x7f121b6e

.field public static final preferred_application_warning_replace:I = 0x7f121b6f

.field public static final preferred_application_warning_title:I = 0x7f121b70

.field public static final preferred_application_warning_use_system:I = 0x7f121b71

.field public static final preferred_media_app_summary:I = 0x7f121b72

.field public static final preferred_network_mode_cdma_evdo_gsm_wcdma_summary:I = 0x7f121b73

.field public static final preferred_network_mode_cdma_evdo_summary:I = 0x7f121b74

.field public static final preferred_network_mode_cdma_only_summary:I = 0x7f121b75

.field public static final preferred_network_mode_cdma_summary:I = 0x7f121b76

.field public static final preferred_network_mode_dialogtitle:I = 0x7f121b77

.field public static final preferred_network_mode_evdo_only_summary:I = 0x7f121b78

.field public static final preferred_network_mode_global_summary:I = 0x7f121b79

.field public static final preferred_network_mode_gsm_only_summary:I = 0x7f121b7a

.field public static final preferred_network_mode_gsm_wcdma_summary:I = 0x7f121b7b

.field public static final preferred_network_mode_lte_cdma_evdo_gsm_wcdma_summary:I = 0x7f121b7c

.field public static final preferred_network_mode_lte_cdma_evdo_summary:I = 0x7f121b7d

.field public static final preferred_network_mode_lte_cdma_summary:I = 0x7f121b7e

.field public static final preferred_network_mode_lte_gsm_umts_summary:I = 0x7f121b7f

.field public static final preferred_network_mode_lte_gsm_wcdma_summary:I = 0x7f121b80

.field public static final preferred_network_mode_lte_summary:I = 0x7f121b81

.field public static final preferred_network_mode_lte_tdscdma_cdma_evdo_gsm_wcdma_summary:I = 0x7f121b82

.field public static final preferred_network_mode_lte_tdscdma_gsm_summary:I = 0x7f121b83

.field public static final preferred_network_mode_lte_tdscdma_gsm_wcdma_summary:I = 0x7f121b84

.field public static final preferred_network_mode_lte_tdscdma_summary:I = 0x7f121b85

.field public static final preferred_network_mode_lte_tdscdma_wcdma_summary:I = 0x7f121b86

.field public static final preferred_network_mode_lte_wcdma_summary:I = 0x7f121b87

.field public static final preferred_network_mode_nr_lte_cdma_evdo_gsm_wcdma_summary:I = 0x7f121b88

.field public static final preferred_network_mode_nr_lte_cdma_evdo_summary:I = 0x7f121b89

.field public static final preferred_network_mode_nr_lte_gsm_wcdma_summary:I = 0x7f121b8a

.field public static final preferred_network_mode_nr_lte_summary:I = 0x7f121b8b

.field public static final preferred_network_mode_nr_lte_tdscdma_cdma_evdo_gsm_wcdma_summary:I = 0x7f121b8c

.field public static final preferred_network_mode_nr_lte_tdscdma_gsm_summary:I = 0x7f121b8d

.field public static final preferred_network_mode_nr_lte_tdscdma_gsm_wcdma_summary:I = 0x7f121b8e

.field public static final preferred_network_mode_nr_lte_tdscdma_summary:I = 0x7f121b8f

.field public static final preferred_network_mode_nr_lte_tdscdma_wcdma_summary:I = 0x7f121b90

.field public static final preferred_network_mode_nr_lte_wcdma_summary:I = 0x7f121b91

.field public static final preferred_network_mode_nr_only_summary:I = 0x7f121b92

.field public static final preferred_network_mode_summary:I = 0x7f121b93

.field public static final preferred_network_mode_tdscdma_cdma_evdo_gsm_wcdma_summary:I = 0x7f121b94

.field public static final preferred_network_mode_tdscdma_gsm_summary:I = 0x7f121b95

.field public static final preferred_network_mode_tdscdma_gsm_wcdma_summary:I = 0x7f121b96

.field public static final preferred_network_mode_tdscdma_summary:I = 0x7f121b97

.field public static final preferred_network_mode_tdscdma_wcdma_summary:I = 0x7f121b98

.field public static final preferred_network_mode_title:I = 0x7f121b99

.field public static final preferred_network_mode_wcdma_only_summary:I = 0x7f121b9a

.field public static final preferred_network_mode_wcdma_perf_summary:I = 0x7f121b9b

.field public static final preferred_network_offload_footer:I = 0x7f121b9c

.field public static final preferred_network_offload_header:I = 0x7f121b9d

.field public static final preferred_network_offload_popup:I = 0x7f121b9e

.field public static final preferred_network_offload_title:I = 0x7f121b9f

.field public static final preferred_network_type_summary:I = 0x7f121ba0

.field public static final preferred_network_type_title:I = 0x7f121ba1

.field public static final preferred_sms_settings:I = 0x7f121ba2

.field public static final preferred_sms_title:I = 0x7f121ba3

.field public static final preferred_sms_warning_message:I = 0x7f121ba4

.field public static final preferred_sms_warning_use_system:I = 0x7f121ba5

.field public static final premium_sms_access:I = 0x7f121ba6

.field public static final premium_sms_none:I = 0x7f121ba7

.field public static final premium_sms_warning:I = 0x7f121ba8

.field public static final prevent_ringing_main_switch_title:I = 0x7f121ba9

.field public static final prevent_ringing_option_mute:I = 0x7f121baa

.field public static final prevent_ringing_option_mute_summary:I = 0x7f121bab

.field public static final prevent_ringing_option_none:I = 0x7f121bac

.field public static final prevent_ringing_option_unavailable_lpp_summary:I = 0x7f121bad

.field public static final prevent_ringing_option_vibrate:I = 0x7f121bae

.field public static final prevent_ringing_option_vibrate_summary:I = 0x7f121baf

.field public static final preview_page_indicator_content_description:I = 0x7f121bb0

.field public static final preview_pager_content_description:I = 0x7f121bb1

.field public static final previous_connected_see_all:I = 0x7f121bb2

.field public static final previous_connected_see_all_groups:I = 0x7f121bb3

.field public static final previous_page_content_description:I = 0x7f121bb4

.field public static final previously_connected_group_screen_title:I = 0x7f121bb5

.field public static final print_blocked_state_title_template:I = 0x7f121bb6

.field public static final print_cancel:I = 0x7f121bb7

.field public static final print_cancelling_state_title_template:I = 0x7f121bb8

.field public static final print_configuring_state_title_template:I = 0x7f121bb9

.field public static final print_failed_state_title_template:I = 0x7f121bba

.field public static final print_feature_state_off:I = 0x7f121bbb

.field public static final print_feature_state_on:I = 0x7f121bbc

.field public static final print_job_summary:I = 0x7f121bbd

.field public static final print_menu_item_add_printer:I = 0x7f121bbe

.field public static final print_menu_item_add_printers:I = 0x7f121bbf

.field public static final print_menu_item_add_service:I = 0x7f121bc0

.field public static final print_menu_item_search:I = 0x7f121bc1

.field public static final print_menu_item_settings:I = 0x7f121bc2

.field public static final print_no_printers_found:I = 0x7f121bc3

.field public static final print_no_services_installed:I = 0x7f121bc4

.field public static final print_print_job:I = 0x7f121bc5

.field public static final print_print_jobs:I = 0x7f121bc6

.field public static final print_printing_state_title_template:I = 0x7f121bc7

.field public static final print_restart:I = 0x7f121bc8

.field public static final print_search_box_hidden_utterance:I = 0x7f121bc9

.field public static final print_search_box_shown_utterance:I = 0x7f121bca

.field public static final print_searching_for_printers:I = 0x7f121bcb

.field public static final print_service_disabled:I = 0x7f121bcc

.field public static final print_settings:I = 0x7f121bcd

.field public static final print_settings_summary_no_service:I = 0x7f121bce

.field public static final print_settings_title:I = 0x7f121bcf

.field public static final printer_info_desc:I = 0x7f121bd0

.field public static final priority_conversation_count_zero:I = 0x7f121bd1

.field public static final priority_storage_app_settings:I = 0x7f121bd2

.field public static final priority_storage_settings_summary:I = 0x7f121bd3

.field public static final priority_storage_summary:I = 0x7f121bd4

.field public static final priority_storage_title:I = 0x7f121bd5

.field public static final priority_summary:I = 0x7f121bd6

.field public static final priority_title:I = 0x7f121bd7

.field public static final privacy_advanced_settings:I = 0x7f121bd8

.field public static final privacy_advanced_settings_summary:I = 0x7f121bd9

.field public static final privacy_authorize_cloud_msg:I = 0x7f121bda

.field public static final privacy_authorize_dialog_agree:I = 0x7f121bdb

.field public static final privacy_authorize_dialog_exit:I = 0x7f121bdc

.field public static final privacy_authorize_dialog_message:I = 0x7f121bdd

.field public static final privacy_authorize_network_error:I = 0x7f121bde

.field public static final privacy_authorize_privacy_policy:I = 0x7f121bdf

.field public static final privacy_authorize_revoke:I = 0x7f121be0

.field public static final privacy_authorize_revoke_dialog_msg:I = 0x7f121be1

.field public static final privacy_authorize_revoke_dialog_title:I = 0x7f121be2

.field public static final privacy_authorize_revoke_failed:I = 0x7f121be3

.field public static final privacy_authorize_revoke_success:I = 0x7f121be4

.field public static final privacy_authorize_revoke_time:I = 0x7f121be5

.field public static final privacy_authorize_revoke_title:I = 0x7f121be6

.field public static final privacy_authorize_revoking:I = 0x7f121be7

.field public static final privacy_authorize_success:I = 0x7f121be8

.field public static final privacy_authorize_title:I = 0x7f121be9

.field public static final privacy_close_dlg_button_ok:I = 0x7f121bea

.field public static final privacy_close_dlg_msg:I = 0x7f121beb

.field public static final privacy_close_dlg_title:I = 0x7f121bec

.field public static final privacy_controls_title:I = 0x7f121bed

.field public static final privacy_dashboard_summary:I = 0x7f121bee

.field public static final privacy_dashboard_title:I = 0x7f121bef

.field public static final privacy_delete_space_dlg_message:I = 0x7f121bf0

.field public static final privacy_dlg_button_cancel:I = 0x7f121bf1

.field public static final privacy_factory_reset_dlg_button_text:I = 0x7f121bf2

.field public static final privacy_factory_reset_dlg_message:I = 0x7f121bf3

.field public static final privacy_factory_reset_dlg_title:I = 0x7f121bf4

.field public static final privacy_failed_need_to_unlock_nofingerprint:I = 0x7f121bf5

.field public static final privacy_file:I = 0x7f121bf6

.field public static final privacy_gallery:I = 0x7f121bf7

.field public static final privacy_header_text_less_than_half:I = 0x7f121bf8

.field public static final privacy_lab_settings:I = 0x7f121bf9

.field public static final privacy_mms:I = 0x7f121bfa

.field public static final privacy_mode_dialog_message:I = 0x7f121bfb

.field public static final privacy_mode_dialog_title:I = 0x7f121bfc

.field public static final privacy_mode_enable_sum:I = 0x7f121bfd

.field public static final privacy_mode_enable_title:I = 0x7f121bfe

.field public static final privacy_notes:I = 0x7f121bff

.field public static final privacy_password:I = 0x7f121c00

.field public static final privacy_password_add_account:I = 0x7f121c01

.field public static final privacy_password_add_account_info:I = 0x7f121c02

.field public static final privacy_password_add_account_setting:I = 0x7f121c03

.field public static final privacy_password_bind_account:I = 0x7f121c04

.field public static final privacy_password_bind_account_immediate:I = 0x7f121c05

.field public static final privacy_password_confirm_SecondSpace_Password:I = 0x7f121c06

.field public static final privacy_password_confirm_use_finger:I = 0x7f121c07

.field public static final privacy_password_enter_privacy:I = 0x7f121c08

.field public static final privacy_password_exit_account_info:I = 0x7f121c09

.field public static final privacy_password_fingerprint_confirm_failed:I = 0x7f121c0a

.field public static final privacy_password_fingerprint_confirm_succes:I = 0x7f121c0b

.field public static final privacy_password_fingerprint_dialog_error_title:I = 0x7f121c0c

.field public static final privacy_password_fingerprint_dialog_message:I = 0x7f121c0d

.field public static final privacy_password_fingerprint_dialog_title:I = 0x7f121c0e

.field public static final privacy_password_login_and_add_account:I = 0x7f121c0f

.field public static final privacy_password_never_remind:I = 0x7f121c10

.field public static final privacy_password_not_add_account:I = 0x7f121c11

.field public static final privacy_password_not_login_account:I = 0x7f121c12

.field public static final privacy_password_remind_bind_notlogin_account:I = 0x7f121c13

.field public static final privacy_password_remind_bindaccount_title:I = 0x7f121c14

.field public static final privacy_password_settings:I = 0x7f121c15

.field public static final privacy_password_settings_next:I = 0x7f121c16

.field public static final privacy_password_settings_summary:I = 0x7f121c17

.field public static final privacy_password_settings_titles:I = 0x7f121c18

.field public static final privacy_password_settings_titles_elder_mode:I = 0x7f121c19

.field public static final privacy_password_settings_titles_second_space:I = 0x7f121c1a

.field public static final privacy_password_title:I = 0x7f121c1b

.field public static final privacy_password_toggle_summary:I = 0x7f121c1c

.field public static final privacy_password_transparent_tiltle:I = 0x7f121c1d

.field public static final privacy_password_use_finger_dialog_mess:I = 0x7f121c1e

.field public static final privacy_password_use_finger_dialog_open:I = 0x7f121c1f

.field public static final privacy_password_use_finger_dialog_title:I = 0x7f121c20

.field public static final privacy_password_use_finger_success:I = 0x7f121c21

.field public static final privacy_password_visible_pattern:I = 0x7f121c22

.field public static final privacy_policy:I = 0x7f121c23

.field public static final privacy_protect_title:I = 0x7f121c24

.field public static final privacy_protection:I = 0x7f121c25

.field public static final privacy_protection_title:I = 0x7f121c26

.field public static final privacy_relative:I = 0x7f121c27

.field public static final privacy_settings:I = 0x7f121c28

.field public static final privacy_settings_new:I = 0x7f121c29

.field public static final privacy_settings_title:I = 0x7f121c2a

.field public static final privacy_thumbnail_blur_progress_message:I = 0x7f121c2b

.field public static final privacy_thumbnail_blur_title:I = 0x7f121c2c

.field public static final private_dns_broken:I = 0x7f121c2d

.field public static final private_dns_help_message:I = 0x7f121c2e

.field public static final private_dns_mode_off:I = 0x7f121c2f

.field public static final private_dns_mode_on:I = 0x7f121c30

.field public static final private_dns_mode_opportunistic:I = 0x7f121c31

.field public static final private_dns_mode_provider:I = 0x7f121c32

.field public static final private_dns_mode_provider_failure:I = 0x7f121c33

.field public static final private_dns_mode_provider_hostname_hint:I = 0x7f121c34

.field public static final private_gallery_need_to_unlock:I = 0x7f121c35

.field public static final private_gallery_recording_intro_header:I = 0x7f121c36

.field public static final private_gallery_reset_by_account:I = 0x7f121c37

.field public static final private_sms_need_to_unlock:I = 0x7f121c38

.field public static final private_sms_recording_intro_header:I = 0x7f121c39

.field public static final private_sms_reset_by_account:I = 0x7f121c3a

.field public static final privileged_action_disable_fail_text:I = 0x7f121c3b

.field public static final privileged_action_disable_fail_title:I = 0x7f121c3c

.field public static final privileged_action_disable_sub_dialog_progress:I = 0x7f121c3d

.field public static final privileged_action_disable_sub_dialog_title:I = 0x7f121c3e

.field public static final privileged_action_disable_sub_dialog_title_without_carrier:I = 0x7f121c3f

.field public static final process_dex2oat_label:I = 0x7f121c40

.field public static final process_format:I = 0x7f121c41

.field public static final process_kernel_label:I = 0x7f121c42

.field public static final process_mediaserver_label:I = 0x7f121c43

.field public static final process_network_tethering:I = 0x7f121c44

.field public static final process_provider_in_use_description:I = 0x7f121c45

.field public static final process_removed_apps:I = 0x7f121c46

.field public static final process_service_in_use_description:I = 0x7f121c47

.field public static final process_stats_bg_ram_use:I = 0x7f121c48

.field public static final process_stats_os_cache:I = 0x7f121c49

.field public static final process_stats_os_kernel:I = 0x7f121c4a

.field public static final process_stats_os_label:I = 0x7f121c4b

.field public static final process_stats_os_native:I = 0x7f121c4c

.field public static final process_stats_os_zram:I = 0x7f121c4d

.field public static final process_stats_ram_use:I = 0x7f121c4e

.field public static final process_stats_run_time:I = 0x7f121c4f

.field public static final process_stats_summary:I = 0x7f121c50

.field public static final process_stats_summary_title:I = 0x7f121c51

.field public static final process_stats_total_duration:I = 0x7f121c52

.field public static final process_stats_total_duration_percentage:I = 0x7f121c53

.field public static final process_stats_type_background:I = 0x7f121c54

.field public static final process_stats_type_cached:I = 0x7f121c55

.field public static final process_stats_type_foreground:I = 0x7f121c56

.field public static final processes_subtitle:I = 0x7f121c57

.field public static final profile_connect_timeout_subtext:I = 0x7f121c58

.field public static final profile_info_settings_title:I = 0x7f121c59

.field public static final profile_owner_add_title:I = 0x7f121c5a

.field public static final profile_owner_add_title_simplified:I = 0x7f121c5b

.field public static final profile_section_header:I = 0x7f121c5c

.field public static final profile_section_header_for_advanced_privacy:I = 0x7f121c5d

.field public static final progress_dialog_speaker_auto_clean_button:I = 0x7f121c5e

.field public static final progress_dialog_speaker_auto_clean_title:I = 0x7f121c5f

.field public static final progress_scanning:I = 0x7f121c60

.field public static final progressbar_title:I = 0x7f121c61

.field public static final promote_conversation_summary:I = 0x7f121c62

.field public static final promote_conversation_title:I = 0x7f121c63

.field public static final provider_internet_settings:I = 0x7f121c64

.field public static final provider_network_settings_title:I = 0x7f121c65

.field public static final provision_activation:I = 0x7f121c66

.field public static final provision_back:I = 0x7f121c67

.field public static final provision_next:I = 0x7f121c68

.field public static final provision_page_layout_confirm_message:I = 0x7f121c69

.field public static final provision_skip:I = 0x7f121c6a

.field public static final provision_skip_underline:I = 0x7f121c6b

.field public static final proxy_action_text:I = 0x7f121c6c

.field public static final proxy_clear_text:I = 0x7f121c6d

.field public static final proxy_defaultView_text:I = 0x7f121c6e

.field public static final proxy_error:I = 0x7f121c6f

.field public static final proxy_error_dismiss:I = 0x7f121c70

.field public static final proxy_error_empty_host_set_port:I = 0x7f121c71

.field public static final proxy_error_empty_port:I = 0x7f121c72

.field public static final proxy_error_invalid_exclusion_list:I = 0x7f121c73

.field public static final proxy_error_invalid_host:I = 0x7f121c74

.field public static final proxy_error_invalid_port:I = 0x7f121c75

.field public static final proxy_exclusionlist_hint:I = 0x7f121c76

.field public static final proxy_exclusionlist_label:I = 0x7f121c77

.field public static final proxy_hostname_hint:I = 0x7f121c78

.field public static final proxy_hostname_label:I = 0x7f121c79

.field public static final proxy_port_hint:I = 0x7f121c7a

.field public static final proxy_port_label:I = 0x7f121c7b

.field public static final proxy_settings_label:I = 0x7f121c7c

.field public static final proxy_settings_title:I = 0x7f121c7d

.field public static final proxy_url_hint:I = 0x7f121c7e

.field public static final proxy_url_title:I = 0x7f121c7f

.field public static final proxy_warning_limited_support:I = 0x7f121c80

.field public static final qtifeedback_activity:I = 0x7f121c81

.field public static final qtifeedback_intent_action:I = 0x7f121c82

.field public static final qtifeedback_package:I = 0x7f121c83

.field public static final qtifeedback_settings_subtitle:I = 0x7f121c84

.field public static final qtifeedback_settings_title:I = 0x7f121c85

.field public static final quick_controls_lower:I = 0x7f121c86

.field public static final quick_reply_title:I = 0x7f121c87

.field public static final quick_settings_developer_tiles:I = 0x7f121c88

.field public static final quick_settings_label:I = 0x7f121c89

.field public static final quiet:I = 0x7f121c8a

.field public static final quiet_group:I = 0x7f121c8b

.field public static final quiet_important_interruptions_mode:I = 0x7f121c8c

.field public static final quiet_no_interruptions_mode:I = 0x7f121c8d

.field public static final quiet_off_mode:I = 0x7f121c8e

.field public static final radioInfo_data_connected:I = 0x7f121c8f

.field public static final radioInfo_data_connecting:I = 0x7f121c90

.field public static final radioInfo_data_disconnected:I = 0x7f121c91

.field public static final radioInfo_data_suspended:I = 0x7f121c92

.field public static final radioInfo_menu_viewADN:I = 0x7f121c93

.field public static final radioInfo_roaming_in:I = 0x7f121c94

.field public static final radioInfo_roaming_not:I = 0x7f121c95

.field public static final radioInfo_service_emergency:I = 0x7f121c96

.field public static final radioInfo_service_in:I = 0x7f121c97

.field public static final radioInfo_service_off:I = 0x7f121c98

.field public static final radioInfo_service_out:I = 0x7f121c99

.field public static final radioInfo_unknown:I = 0x7f121c9a

.field public static final radio_controls_summary:I = 0x7f121c9b

.field public static final radio_controls_title:I = 0x7f121c9c

.field public static final radio_info_asdiv_switch_label:I = 0x7f121c9d

.field public static final radio_info_http_client_test:I = 0x7f121c9e

.field public static final radio_info_ping_hostname_v4:I = 0x7f121c9f

.field public static final radio_info_ping_hostname_v6:I = 0x7f121ca0

.field public static final ram_total_size:I = 0x7f121ca1

.field public static final rank_title:I = 0x7f121ca2

.field public static final rarely_running:I = 0x7f121ca3

.field public static final reading_data:I = 0x7f121ca4

.field public static final reading_settings:I = 0x7f121ca5

.field public static final really_remove_account_message:I = 0x7f121ca6

.field public static final really_remove_account_title:I = 0x7f121ca7

.field public static final reboot_dialog_enable_freeform_support:I = 0x7f121ca8

.field public static final reboot_dialog_force_desktop_mode:I = 0x7f121ca9

.field public static final reboot_dialog_reboot_later:I = 0x7f121caa

.field public static final reboot_dialog_reboot_now:I = 0x7f121cab

.field public static final reboot_with_mte_message:I = 0x7f121cac

.field public static final reboot_with_mte_title:I = 0x7f121cad

.field public static final rebootiing_for_updates:I = 0x7f121cae

.field public static final receiver_clean:I = 0x7f121caf

.field public static final recent_app_category_title:I = 0x7f121cb0

.field public static final recent_apps_label:I = 0x7f121cb1

.field public static final recent_conversations:I = 0x7f121cb2

.field public static final recent_convo_removed:I = 0x7f121cb3

.field public static final recent_convos_removed:I = 0x7f121cb4

.field public static final recent_location_requests_summary:I = 0x7f121cb5

.field public static final recent_location_requests_title:I = 0x7f121cb6

.field public static final recent_notifications:I = 0x7f121cb7

.field public static final recent_notifications_see_all_title:I = 0x7f121cb8

.field public static final recommend_item_1:I = 0x7f121cb9

.field public static final recommend_item_2:I = 0x7f121cba

.field public static final recommend_item_3:I = 0x7f121cbb

.field public static final recommend_item_4:I = 0x7f121cbc

.field public static final recommend_item_title:I = 0x7f121cbd

.field public static final recommend_text_wakeup_xiaoai:I = 0x7f121cbe

.field public static final recommend_tip:I = 0x7f121cbf

.field public static final recommend_tip_wakeup_xiaoai:I = 0x7f121cc0

.field public static final recompute_size:I = 0x7f121cc1

.field public static final reduce_bright_colors_about_title:I = 0x7f121cc2

.field public static final reduce_bright_colors_intensity_end_label:I = 0x7f121cc3

.field public static final reduce_bright_colors_intensity_preference_title:I = 0x7f121cc4

.field public static final reduce_bright_colors_intensity_start_label:I = 0x7f121cc5

.field public static final reduce_bright_colors_persist_preference_title:I = 0x7f121cc6

.field public static final reduce_bright_colors_preference_intro_text:I = 0x7f121cc7

.field public static final reduce_bright_colors_preference_subtitle:I = 0x7f121cc8

.field public static final reduce_bright_colors_preference_summary:I = 0x7f121cc9

.field public static final reduce_bright_colors_preference_title:I = 0x7f121cca

.field public static final reduce_bright_colors_shortcut_title:I = 0x7f121ccb

.field public static final reduce_bright_colors_switch_title:I = 0x7f121ccc

.field public static final refresh_group:I = 0x7f121ccd

.field public static final regional_screen_shot:I = 0x7f121cce

.field public static final register_automatically:I = 0x7f121ccf

.field public static final register_on_network:I = 0x7f121cd0

.field public static final registration_done:I = 0x7f121cd1

.field public static final regulatory_info_text:I = 0x7f121cd2

.field public static final regulatory_labels:I = 0x7f121cd3

.field public static final remaining_length_format:I = 0x7f121cd4

.field public static final remote_media_volume_option_title:I = 0x7f121cd5

.field public static final remove_account_failed:I = 0x7f121cd6

.field public static final remove_account_label:I = 0x7f121cd7

.field public static final remove_and_uninstall_device_admin:I = 0x7f121cd8

.field public static final remove_association_button:I = 0x7f121cd9

.field public static final remove_continue:I = 0x7f121cda

.field public static final remove_credential_management_app:I = 0x7f121cdb

.field public static final remove_credential_management_app_dialog_message:I = 0x7f121cdc

.field public static final remove_credential_management_app_dialog_title:I = 0x7f121cdd

.field public static final remove_device_admin:I = 0x7f121cde

.field public static final remove_from_frequently_fragment:I = 0x7f121cdf

.field public static final remove_managed_profile_label:I = 0x7f121ce0

.field public static final remove_sourceinfo_btn_txt:I = 0x7f121ce1

.field public static final rename_words_reached_limit:I = 0x7f121ce2

.field public static final repeat:I = 0x7f121ce3

.field public static final repeat_call:I = 0x7f121ce4

.field public static final repeat_call_info:I = 0x7f121ce5

.field public static final repeat_days_in_week:I = 0x7f121ce6

.field public static final repeat_never:I = 0x7f121ce7

.field public static final repeat_title:I = 0x7f121ce8

.field public static final request_manage_bluetooth_permission_dont_allow:I = 0x7f121ce9

.field public static final request_manage_credentials_allow:I = 0x7f121cea

.field public static final request_manage_credentials_description:I = 0x7f121ceb

.field public static final request_manage_credentials_dont_allow:I = 0x7f121cec

.field public static final request_manage_credentials_more:I = 0x7f121ced

.field public static final request_manage_credentials_title:I = 0x7f121cee

.field public static final requires_modern_control_center:I = 0x7f121cef

.field public static final reset_aaid:I = 0x7f121cf0

.field public static final reset_app_preferences:I = 0x7f121cf1

.field public static final reset_app_preferences_button:I = 0x7f121cf2

.field public static final reset_app_preferences_desc:I = 0x7f121cf3

.field public static final reset_app_preferences_title:I = 0x7f121cf4

.field public static final reset_confirm_directory:I = 0x7f121cf5

.field public static final reset_confirm_directory_pad:I = 0x7f121cf6

.field public static final reset_dashboard_summary:I = 0x7f121cf7

.field public static final reset_dashboard_summary_onlyApps:I = 0x7f121cf8

.field public static final reset_dashboard_title:I = 0x7f121cf9

.field public static final reset_default_apn_failed:I = 0x7f121cfa

.field public static final reset_esim_desc:I = 0x7f121cfb

.field public static final reset_esim_error_msg:I = 0x7f121cfc

.field public static final reset_esim_error_title:I = 0x7f121cfd

.field public static final reset_esim_title:I = 0x7f121cfe

.field public static final reset_importance_completed:I = 0x7f121cff

.field public static final reset_internet_text:I = 0x7f121d00

.field public static final reset_legacy_password_settings_preface_text:I = 0x7f121d01

.field public static final reset_network_bluetooth:I = 0x7f121d02

.field public static final reset_network_button_text:I = 0x7f121d03

.field public static final reset_network_category:I = 0x7f121d04

.field public static final reset_network_complete_toast:I = 0x7f121d05

.field public static final reset_network_confirm_title:I = 0x7f121d06

.field public static final reset_network_data:I = 0x7f121d07

.field public static final reset_network_desc:I = 0x7f121d08

.field public static final reset_network_final_button_text:I = 0x7f121d09

.field public static final reset_network_final_desc:I = 0x7f121d0a

.field public static final reset_network_final_desc_esim:I = 0x7f121d0b

.field public static final reset_network_title:I = 0x7f121d0c

.field public static final reset_network_title_noSim:I = 0x7f121d0d

.field public static final reset_network_wifi:I = 0x7f121d0e

.field public static final reset_remove_data_hint:I = 0x7f121d0f

.field public static final reset_second_space_passwd:I = 0x7f121d10

.field public static final reset_second_space_passwd_cancel:I = 0x7f121d11

.field public static final reset_second_space_passwd_confirm:I = 0x7f121d12

.field public static final reset_second_space_passwd_detail:I = 0x7f121d13

.field public static final reset_shortcut_manager_throttling:I = 0x7f121d14

.field public static final reset_shortcut_manager_throttling_complete:I = 0x7f121d15

.field public static final reset_title:I = 0x7f121d16

.field public static final reset_your_internet_title:I = 0x7f121d17

.field public static final resetting_internet_text:I = 0x7f121d18

.field public static final resetting_second_space_passwd:I = 0x7f121d19

.field public static final resident_timezone_summary:I = 0x7f121d1a

.field public static final resolution_fhd:I = 0x7f121d1b

.field public static final resolution_fhd_summary:I = 0x7f121d1c

.field public static final resolution_qhd:I = 0x7f121d1d

.field public static final resolution_qhd_summary:I = 0x7f121d1e

.field public static final resolution_sub_title:I = 0x7f121d1f

.field public static final resolver_settings_label:I = 0x7f121d20

.field public static final restore_default_apn:I = 0x7f121d21

.field public static final restore_default_apn_completed:I = 0x7f121d22

.field public static final restore_device_id_content:I = 0x7f121d23

.field public static final restore_device_id_title:I = 0x7f121d24

.field public static final restore_dialog_content:I = 0x7f121d25

.field public static final restore_dialog_positive:I = 0x7f121d26

.field public static final restore_dialog_title:I = 0x7f121d27

.field public static final restore_oaid_content:I = 0x7f121d28

.field public static final restore_oaid_dialog_content:I = 0x7f121d29

.field public static final restore_oaid_positive:I = 0x7f121d2a

.field public static final restore_oaid_positive_step:I = 0x7f121d2b

.field public static final restore_oaid_title:I = 0x7f121d2c

.field public static final restr_pin_enter_admin_pin:I = 0x7f121d2d

.field public static final restrict_background_blocklisted:I = 0x7f121d2e

.field public static final restrict_bg_app:I = 0x7f121d2f

.field public static final restrict_max_aspect_ratio_title:I = 0x7f121d30

.field public static final restricted_app_detail_footer:I = 0x7f121d31

.field public static final restricted_app_time_summary:I = 0x7f121d32

.field public static final restricted_app_title:I = 0x7f121d33

.field public static final restricted_false_label:I = 0x7f121d34

.field public static final restricted_true_label:I = 0x7f121d35

.field public static final restriction_bluetooth_config_summary:I = 0x7f121d36

.field public static final restriction_bluetooth_config_title:I = 0x7f121d37

.field public static final restriction_location_enable_summary:I = 0x7f121d38

.field public static final restriction_location_enable_title:I = 0x7f121d39

.field public static final restriction_menu_change_pin:I = 0x7f121d3a

.field public static final restriction_menu_reset:I = 0x7f121d3b

.field public static final restriction_nfc_enable_summary:I = 0x7f121d3c

.field public static final restriction_nfc_enable_summary_config:I = 0x7f121d3d

.field public static final restriction_nfc_enable_title:I = 0x7f121d3e

.field public static final restriction_settings_title:I = 0x7f121d3f

.field public static final restriction_wifi_config_summary:I = 0x7f121d40

.field public static final restriction_wifi_config_title:I = 0x7f121d41

.field public static final retail_demo_reset_message:I = 0x7f121d42

.field public static final retail_demo_reset_next:I = 0x7f121d43

.field public static final retail_demo_reset_title:I = 0x7f121d44

.field public static final retry:I = 0x7f121d45

.field public static final rgb_face_data_manage_delete_msg:I = 0x7f121d46

.field public static final rgb_manage_facerecoginition_text:I = 0x7f121d47

.field public static final right_edge:I = 0x7f121d48

.field public static final right_function_key:I = 0x7f121d49

.field public static final right_shoulder_key:I = 0x7f121d4a

.field public static final ring_to_you:I = 0x7f121d4b

.field public static final ring_to_you_address:I = 0x7f121d4c

.field public static final ring_to_you_info:I = 0x7f121d4d

.field public static final ring_volume_option_title:I = 0x7f121d4e

.field public static final ring_volume_title:I = 0x7f121d4f

.field public static final ringer_mode_setting_summary:I = 0x7f121d50

.field public static final ringer_mode_setting_title:I = 0x7f121d51

.field public static final ringtone_slot_setting_title:I = 0x7f121d52

.field public static final ringtone_title:I = 0x7f121d53

.field public static final ringtone_uniform_setting_title:I = 0x7f121d54

.field public static final ringtones_category_preference_title:I = 0x7f121d55

.field public static final ringtones_install_custom_sound_content:I = 0x7f121d56

.field public static final ringtones_install_custom_sound_title:I = 0x7f121d57

.field public static final risk_tip:I = 0x7f121d58

.field public static final roaming:I = 0x7f121d59

.field public static final roaming_alert_title:I = 0x7f121d5a

.field public static final roaming_check_price_warning:I = 0x7f121d5b

.field public static final roaming_disable:I = 0x7f121d5c

.field public static final roaming_enable:I = 0x7f121d5d

.field public static final roaming_reenable_message:I = 0x7f121d5e

.field public static final roaming_reenable_title:I = 0x7f121d5f

.field public static final roaming_turn_it_on_button:I = 0x7f121d60

.field public static final roaming_warning:I = 0x7f121d61

.field public static final roaming_warning_multiuser:I = 0x7f121d62

.field public static final rom_total_size:I = 0x7f121d63

.field public static final root:I = 0x7f121d64

.field public static final rssi_label:I = 0x7f121d65

.field public static final rtt_settings_always_visible:I = 0x7f121d66

.field public static final rtt_settings_no_visible:I = 0x7f121d67

.field public static final rtt_settings_title:I = 0x7f121d68

.field public static final rtt_settings_visible_during_call:I = 0x7f121d69

.field public static final rule_name:I = 0x7f121d6a

.field public static final running_frequency:I = 0x7f121d6b

.field public static final running_process_item_removed_user_label:I = 0x7f121d6c

.field public static final running_process_item_user_label:I = 0x7f121d6d

.field public static final running_processes_header_apps_prefix:I = 0x7f121d6e

.field public static final running_processes_header_cached_prefix:I = 0x7f121d6f

.field public static final running_processes_header_footer:I = 0x7f121d70

.field public static final running_processes_header_free_prefix:I = 0x7f121d71

.field public static final running_processes_header_ram:I = 0x7f121d72

.field public static final running_processes_header_system_prefix:I = 0x7f121d73

.field public static final running_processes_header_title:I = 0x7f121d74

.field public static final running_processes_header_used_prefix:I = 0x7f121d75

.field public static final running_processes_item_description_p_p:I = 0x7f121d76

.field public static final running_processes_item_description_p_s:I = 0x7f121d77

.field public static final running_processes_item_description_s_p:I = 0x7f121d78

.field public static final running_processes_item_description_s_s:I = 0x7f121d79

.field public static final runningservicedetails_processes_title:I = 0x7f121d7a

.field public static final runningservicedetails_services_title:I = 0x7f121d7b

.field public static final runningservicedetails_settings_title:I = 0x7f121d7c

.field public static final runningservicedetails_stop_dlg_text:I = 0x7f121d7d

.field public static final runningservicedetails_stop_dlg_title:I = 0x7f121d7e

.field public static final runningservices_settings_summary:I = 0x7f121d7f

.field public static final runningservices_settings_title:I = 0x7f121d80

.field public static final runtime_permissions_summary_control_app_access:I = 0x7f121d81

.field public static final runtime_permissions_summary_no_permissions_granted:I = 0x7f121d82

.field public static final runtime_permissions_summary_no_permissions_requested:I = 0x7f121d83

.field public static final rx_link_speed:I = 0x7f121d84

.field public static final rx_link_speed_label:I = 0x7f121d85

.field public static final rx_wifi_speed:I = 0x7f121d86

.field public static final safe_install_mode_settings:I = 0x7f121d87

.field public static final safety_and_regulatory_info:I = 0x7f121d88

.field public static final safety_center_summary:I = 0x7f121d89

.field public static final safety_center_title:I = 0x7f121d8a

.field public static final sar:I = 0x7f121d8b

.field public static final saturday:I = 0x7f121d8c

.field public static final saturday_short:I = 0x7f121d8d

.field public static final saturday_shortest:I = 0x7f121d8e

.field public static final save:I = 0x7f121d8f

.field public static final save_battery_mode:I = 0x7f121d90

.field public static final save_battery_mode_summary:I = 0x7f121d91

.field public static final saved_network:I = 0x7f121d92

.field public static final saved_wifi:I = 0x7f121d93

.field public static final saved_wifi_delete:I = 0x7f121d94

.field public static final scan_list_label:I = 0x7f121d95

.field public static final scanning_status_text_off:I = 0x7f121d96

.field public static final scanning_status_text_on:I = 0x7f121d97

.field public static final scanning_status_text_wifi_off_ble_off:I = 0x7f121d98

.field public static final scanning_status_text_wifi_off_ble_on:I = 0x7f121d99

.field public static final scanning_status_text_wifi_on_ble_off:I = 0x7f121d9a

.field public static final scanning_status_text_wifi_on_ble_on:I = 0x7f121d9b

.field public static final screen:I = 0x7f121d9c

.field public static final screen_brightness:I = 0x7f121d9d

.field public static final screen_cancel:I = 0x7f121d9e

.field public static final screen_color_and_optimize:I = 0x7f121d9f

.field public static final screen_color_and_optimize_disabled:I = 0x7f121da0

.field public static final screen_color_temperature:I = 0x7f121da1

.field public static final screen_color_temperature_and_saturation:I = 0x7f121da2

.field public static final screen_compatibility_label:I = 0x7f121da3

.field public static final screen_compatibility_text:I = 0x7f121da4

.field public static final screen_confirm:I = 0x7f121da5

.field public static final screen_dark_mode_select_time_error_summary:I = 0x7f121da6

.field public static final screen_dark_mode_smart_dark_apps_setting_summary:I = 0x7f121da7

.field public static final screen_dark_mode_smart_dark_title:I = 0x7f121da8

.field public static final screen_dark_mode_time_title:I = 0x7f121da9

.field public static final screen_effect:I = 0x7f121daa

.field public static final screen_enhance_engine_ai_note:I = 0x7f121dab

.field public static final screen_enhance_engine_ai_note_2:I = 0x7f121dac

.field public static final screen_enhance_engine_ai_note_3:I = 0x7f121dad

.field public static final screen_enhance_engine_ai_open:I = 0x7f121dae

.field public static final screen_enhance_engine_ai_pic_summary:I = 0x7f121daf

.field public static final screen_enhance_engine_ai_title:I = 0x7f121db0

.field public static final screen_enhance_engine_memc_note:I = 0x7f121db1

.field public static final screen_enhance_engine_memc_note_1:I = 0x7f121db2

.field public static final screen_enhance_engine_memc_note_2:I = 0x7f121db3

.field public static final screen_enhance_engine_memc_open:I = 0x7f121db4

.field public static final screen_enhance_engine_memc_pic_summary:I = 0x7f121db5

.field public static final screen_enhance_engine_memc_title:I = 0x7f121db6

.field public static final screen_enhance_engine_s2h_open:I = 0x7f121db7

.field public static final screen_enhance_engine_s2h_pic_summary:I = 0x7f121db8

.field public static final screen_enhance_engine_s2h_title:I = 0x7f121db9

.field public static final screen_enhance_engine_sr_for_pic:I = 0x7f121dba

.field public static final screen_enhance_engine_sr_for_video:I = 0x7f121dbb

.field public static final screen_enhance_engine_sr_for_video_note:I = 0x7f121dbc

.field public static final screen_enhance_engine_sr_note:I = 0x7f121dbd

.field public static final screen_enhance_engine_sr_pic_summary:I = 0x7f121dbe

.field public static final screen_enhance_engine_sr_pic_summary_video_only:I = 0x7f121dbf

.field public static final screen_enhance_engine_sr_title:I = 0x7f121dc0

.field public static final screen_enhance_engine_title:I = 0x7f121dc1

.field public static final screen_enhance_status_disable:I = 0x7f121dc2

.field public static final screen_enhance_status_enable:I = 0x7f121dc3

.field public static final screen_fps_summary_power_save:I = 0x7f121dc4

.field public static final screen_fps_summary_smooth:I = 0x7f121dc5

.field public static final screen_fps_title:I = 0x7f121dc6

.field public static final screen_fps_unit:I = 0x7f121dc7

.field public static final screen_fps_unit_power_save:I = 0x7f121dc8

.field public static final screen_fps_unit_smooth:I = 0x7f121dc9

.field public static final screen_key_long_press:I = 0x7f121dca

.field public static final screen_key_long_press_action:I = 0x7f121dcb

.field public static final screen_key_long_press_timeout:I = 0x7f121dcc

.field public static final screen_key_position:I = 0x7f121dcd

.field public static final screen_key_press:I = 0x7f121dce

.field public static final screen_key_press_action:I = 0x7f121dcf

.field public static final screen_locking_sounds_title:I = 0x7f121dd0

.field public static final screen_max_aspect_ratio_desc:I = 0x7f121dd1

.field public static final screen_max_aspect_ratio_title:I = 0x7f121dd2

.field public static final screen_monochrome_mode_apps_loading:I = 0x7f121dd3

.field public static final screen_monochrome_mode_apps_title:I = 0x7f121dd4

.field public static final screen_monochrome_mode_global_summary:I = 0x7f121dd5

.field public static final screen_monochrome_mode_global_title:I = 0x7f121dd6

.field public static final screen_monochrome_mode_local_summary:I = 0x7f121dd7

.field public static final screen_monochrome_mode_local_title:I = 0x7f121dd8

.field public static final screen_monochrome_mode_off_apps_title:I = 0x7f121dd9

.field public static final screen_monochrome_mode_range_title:I = 0x7f121dda

.field public static final screen_monochrome_mode_set_apps_title:I = 0x7f121ddb

.field public static final screen_monochrome_mode_summary:I = 0x7f121ddc

.field public static final screen_monochrome_mode_title:I = 0x7f121ddd

.field public static final screen_never_timeout_title:I = 0x7f121dde

.field public static final screen_on_proximity_sensor_summary:I = 0x7f121ddf

.field public static final screen_on_proximity_sensor_title:I = 0x7f121de0

.field public static final screen_optimize:I = 0x7f121de1

.field public static final screen_optimize_disabled:I = 0x7f121de2

.field public static final screen_paper_mode_adjust_level:I = 0x7f121de3

.field public static final screen_paper_mode_apps_loading:I = 0x7f121de4

.field public static final screen_paper_mode_apps_title:I = 0x7f121de5

.field public static final screen_paper_mode_description:I = 0x7f121de6

.field public static final screen_paper_mode_global_summary:I = 0x7f121de7

.field public static final screen_paper_mode_global_title:I = 0x7f121de8

.field public static final screen_paper_mode_hdr_toast:I = 0x7f121de9

.field public static final screen_paper_mode_hdr_toast_new:I = 0x7f121dea

.field public static final screen_paper_mode_local_summary:I = 0x7f121deb

.field public static final screen_paper_mode_local_title:I = 0x7f121dec

.field public static final screen_paper_mode_off_apps_title:I = 0x7f121ded

.field public static final screen_paper_mode_range_title:I = 0x7f121dee

.field public static final screen_paper_mode_set_apps_title:I = 0x7f121def

.field public static final screen_paper_mode_summary:I = 0x7f121df0

.field public static final screen_paper_mode_time_summary:I = 0x7f121df1

.field public static final screen_paper_mode_time_title:I = 0x7f121df2

.field public static final screen_paper_mode_title:I = 0x7f121df3

.field public static final screen_paper_mode_turn_off:I = 0x7f121df4

.field public static final screen_paper_mode_turn_on:I = 0x7f121df5

.field public static final screen_paper_mode_update_white_pkg_list_privacy_again:I = 0x7f121df6

.field public static final screen_paper_mode_update_white_pkg_list_privacy_agree:I = 0x7f121df7

.field public static final screen_paper_mode_update_white_pkg_list_privacy_summary:I = 0x7f121df8

.field public static final screen_paper_mode_update_white_pkg_list_privacy_title:I = 0x7f121df9

.field public static final screen_paper_mode_update_white_pkg_list_summary:I = 0x7f121dfa

.field public static final screen_paper_mode_update_white_pkg_list_title:I = 0x7f121dfb

.field public static final screen_paper_texture:I = 0x7f121dfc

.field public static final screen_pinning_description:I = 0x7f121dfd

.field public static final screen_pinning_dialog_message:I = 0x7f121dfe

.field public static final screen_pinning_guest_user_description:I = 0x7f121dff

.field public static final screen_pinning_switch_off_text:I = 0x7f121e00

.field public static final screen_pinning_switch_on_text:I = 0x7f121e01

.field public static final screen_pinning_title:I = 0x7f121e02

.field public static final screen_pinning_unlock_none:I = 0x7f121e03

.field public static final screen_pinning_unlock_password:I = 0x7f121e04

.field public static final screen_pinning_unlock_pattern:I = 0x7f121e05

.field public static final screen_pinning_unlock_pin:I = 0x7f121e06

.field public static final screen_projection:I = 0x7f121e07

.field public static final screen_projection_characteristics:I = 0x7f121e08

.field public static final screen_projection_characteristics_off_screen_summary:I = 0x7f121e09

.field public static final screen_projection_characteristics_off_screen_title:I = 0x7f121e0a

.field public static final screen_projection_characteristics_privacy_protect_summary:I = 0x7f121e0b

.field public static final screen_projection_characteristics_privacy_protect_title:I = 0x7f121e0c

.field public static final screen_projection_characteristics_small_window_summary:I = 0x7f121e0d

.field public static final screen_projection_characteristics_small_window_title:I = 0x7f121e0e

.field public static final screen_projection_example_document_summary:I = 0x7f121e0f

.field public static final screen_projection_example_document_title:I = 0x7f121e10

.field public static final screen_projection_example_gallery_summary:I = 0x7f121e11

.field public static final screen_projection_example_gallery_title:I = 0x7f121e12

.field public static final screen_projection_example_game_summary:I = 0x7f121e13

.field public static final screen_projection_example_game_title:I = 0x7f121e14

.field public static final screen_projection_help:I = 0x7f121e15

.field public static final screen_projection_sample_summary:I = 0x7f121e16

.field public static final screen_projection_sample_title:I = 0x7f121e17

.field public static final screen_projection_title:I = 0x7f121e18

.field public static final screen_reader_category_title:I = 0x7f121e19

.field public static final screen_recorder_title:I = 0x7f121e1a

.field public static final screen_refresh_unavailable_prompt:I = 0x7f121e1b

.field public static final screen_resolution_dialog_title:I = 0x7f121e1c

.field public static final screen_resolution_footer:I = 0x7f121e1d

.field public static final screen_resolution_high_format:I = 0x7f121e1e

.field public static final screen_resolution_normal_format:I = 0x7f121e1f

.field public static final screen_resolution_option_high:I = 0x7f121e20

.field public static final screen_resolution_option_highest:I = 0x7f121e21

.field public static final screen_resolution_summary:I = 0x7f121e22

.field public static final screen_resolution_summary_high:I = 0x7f121e23

.field public static final screen_resolution_summary_highest:I = 0x7f121e24

.field public static final screen_resolution_switch:I = 0x7f121e25

.field public static final screen_resolution_title:I = 0x7f121e26

.field public static final screen_resolution_ultrahigh_format:I = 0x7f121e27

.field public static final screen_saturation:I = 0x7f121e28

.field public static final screen_shot:I = 0x7f121e29

.field public static final screen_timeout:I = 0x7f121e2a

.field public static final screen_timeout_subtitle:I = 0x7f121e2b

.field public static final screen_timeout_summary:I = 0x7f121e2c

.field public static final screen_timeout_title:I = 0x7f121e2d

.field public static final screen_usage_summary:I = 0x7f121e2e

.field public static final screen_zoom_big:I = 0x7f121e2f

.field public static final screen_zoom_conversation_icon_alex:I = 0x7f121e30

.field public static final screen_zoom_conversation_icon_pete:I = 0x7f121e31

.field public static final screen_zoom_conversation_message_1:I = 0x7f121e32

.field public static final screen_zoom_conversation_message_2:I = 0x7f121e33

.field public static final screen_zoom_conversation_message_3:I = 0x7f121e34

.field public static final screen_zoom_conversation_message_4:I = 0x7f121e35

.field public static final screen_zoom_conversation_timestamp_1:I = 0x7f121e36

.field public static final screen_zoom_conversation_timestamp_2:I = 0x7f121e37

.field public static final screen_zoom_conversation_timestamp_3:I = 0x7f121e38

.field public static final screen_zoom_conversation_timestamp_4:I = 0x7f121e39

.field public static final screen_zoom_keywords:I = 0x7f121e3a

.field public static final screen_zoom_make_larger_desc:I = 0x7f121e3b

.field public static final screen_zoom_make_smaller_desc:I = 0x7f121e3c

.field public static final screen_zoom_normal:I = 0x7f121e3d

.field public static final screen_zoom_preview_title:I = 0x7f121e3e

.field public static final screen_zoom_short_summary:I = 0x7f121e3f

.field public static final screen_zoom_small:I = 0x7f121e40

.field public static final screen_zoom_summary:I = 0x7f121e41

.field public static final screen_zoom_summary_custom:I = 0x7f121e42

.field public static final screen_zoom_summary_default:I = 0x7f121e43

.field public static final screen_zoom_summary_extremely_large:I = 0x7f121e44

.field public static final screen_zoom_summary_large:I = 0x7f121e45

.field public static final screen_zoom_summary_small:I = 0x7f121e46

.field public static final screen_zoom_summary_very_large:I = 0x7f121e47

.field public static final screen_zoom_title:I = 0x7f121e48

.field public static final screensaver_settings_button:I = 0x7f121e49

.field public static final screensaver_settings_current:I = 0x7f121e4a

.field public static final screensaver_settings_disabled_prompt:I = 0x7f121e4b

.field public static final screensaver_settings_summary_dock:I = 0x7f121e4c

.field public static final screensaver_settings_summary_either_long:I = 0x7f121e4d

.field public static final screensaver_settings_summary_either_short:I = 0x7f121e4e

.field public static final screensaver_settings_summary_never:I = 0x7f121e4f

.field public static final screensaver_settings_summary_off:I = 0x7f121e50

.field public static final screensaver_settings_summary_sleep:I = 0x7f121e51

.field public static final screensaver_settings_title:I = 0x7f121e52

.field public static final screensaver_settings_toggle_title:I = 0x7f121e53

.field public static final screensaver_settings_when_to_dream:I = 0x7f121e54

.field public static final screenshot:I = 0x7f121e55

.field public static final screenshot_label:I = 0x7f121e56

.field public static final screenshot_three_gesture:I = 0x7f121e57

.field public static final screenshot_three_gesture_summary:I = 0x7f121e58

.field public static final sd_card_settings_label:I = 0x7f121e59

.field public static final sd_data:I = 0x7f121e5a

.field public static final sd_eject:I = 0x7f121e5b

.field public static final sd_eject_summary:I = 0x7f121e5c

.field public static final sd_ejecting_summary:I = 0x7f121e5d

.field public static final sd_ejecting_title:I = 0x7f121e5e

.field public static final sd_format:I = 0x7f121e5f

.field public static final sd_format_summary:I = 0x7f121e60

.field public static final sd_insert_summary:I = 0x7f121e61

.field public static final sd_memory:I = 0x7f121e62

.field public static final sd_mount:I = 0x7f121e63

.field public static final sd_mount_summary:I = 0x7f121e64

.field public static final sdcard_changes_instructions:I = 0x7f121e65

.field public static final sdcard_format:I = 0x7f121e66

.field public static final sdcard_setting:I = 0x7f121e67

.field public static final sdcard_settings_available_bytes_label:I = 0x7f121e68

.field public static final sdcard_settings_bad_removal_status:I = 0x7f121e69

.field public static final sdcard_settings_mass_storage_status:I = 0x7f121e6a

.field public static final sdcard_settings_not_present_status:I = 0x7f121e6b

.field public static final sdcard_settings_read_only_status:I = 0x7f121e6c

.field public static final sdcard_settings_scanning_status:I = 0x7f121e6d

.field public static final sdcard_settings_screen_mass_storage_text:I = 0x7f121e6e

.field public static final sdcard_settings_total_bytes_label:I = 0x7f121e6f

.field public static final sdcard_settings_unmounted_status:I = 0x7f121e70

.field public static final sdcard_settings_used_bytes_label:I = 0x7f121e71

.field public static final sdcard_unmount:I = 0x7f121e72

.field public static final se_in_ese:I = 0x7f121e73

.field public static final se_in_ese_desc:I = 0x7f121e74

.field public static final se_route_default:I = 0x7f121e75

.field public static final se_route_ese:I = 0x7f121e76

.field public static final se_route_uicc:I = 0x7f121e77

.field public static final se_wallet_ese:I = 0x7f121e78

.field public static final se_wallet_hce:I = 0x7f121e79

.field public static final se_wallet_uicc:I = 0x7f121e7a

.field public static final se_wallet_uicc_jp:I = 0x7f121e7b

.field public static final search_about_settings:I = 0x7f121e7c

.field public static final search_accelerometer_title:I = 0x7f121e7d

.field public static final search_access_control:I = 0x7f121e7e

.field public static final search_accessibility_captioning_title:I = 0x7f121e7f

.field public static final search_accessibility_display_daltonizer_preference_title:I = 0x7f121e80

.field public static final search_accessibility_global_gesture_preference_title:I = 0x7f121e81

.field public static final search_accessibility_long_press_timeout_preference_title:I = 0x7f121e82

.field public static final search_accessibility_screen_magnification_title:I = 0x7f121e83

.field public static final search_accessibility_settings:I = 0x7f121e84

.field public static final search_account_list:I = 0x7f121e85

.field public static final search_add_facerecoginition_text:I = 0x7f121e86

.field public static final search_advanced_settings:I = 0x7f121e87

.field public static final search_airplane_mode:I = 0x7f121e88

.field public static final search_alarm_sound_title:I = 0x7f121e89

.field public static final search_ambient_display_screen_title:I = 0x7f121e8a

.field public static final search_android_auto:I = 0x7f121e8b

.field public static final search_android_beam_settings_title:I = 0x7f121e8c

.field public static final search_apn_settings:I = 0x7f121e8d

.field public static final search_app_invisible:I = 0x7f121e8e

.field public static final search_app_switch_key:I = 0x7f121e8f

.field public static final search_application_count:I = 0x7f121e90

.field public static final search_application_lock_name:I = 0x7f121e91

.field public static final search_applications_settings:I = 0x7f121e92

.field public static final search_back_key:I = 0x7f121e93

.field public static final search_bar_account_avatar_content_description:I = 0x7f121e94

.field public static final search_battery_indicator:I = 0x7f121e95

.field public static final search_bluetooth_device_searchable:I = 0x7f121e96

.field public static final search_bluetooth_enable:I = 0x7f121e97

.field public static final search_bluetooth_settings_title:I = 0x7f121e98

.field public static final search_bluetooth_tether_checkbox_text:I = 0x7f121e99

.field public static final search_breadcrumb_connector:I = 0x7f121e9a

.field public static final search_brightness:I = 0x7f121e9b

.field public static final search_button_light_timeout:I = 0x7f121e9c

.field public static final search_calendar_sound_title:I = 0x7f121e9d

.field public static final search_captioning_locale:I = 0x7f121e9e

.field public static final search_change_password:I = 0x7f121e9f

.field public static final search_children_mode_title:I = 0x7f121ea0

.field public static final search_choose_timezone:I = 0x7f121ea1

.field public static final search_cloud_backup_settings_section_title:I = 0x7f121ea2

.field public static final search_cloud_restore_section_title:I = 0x7f121ea3

.field public static final search_connection_and_sharing:I = 0x7f121ea4

.field public static final search_control_center_style:I = 0x7f121ea5

.field public static final search_copyright_title:I = 0x7f121ea6

.field public static final search_credentials_install:I = 0x7f121ea7

.field public static final search_date_and_time_settings_title:I = 0x7f121ea8

.field public static final search_description:I = 0x7f121ea9

.field public static final search_development_settings_title:I = 0x7f121eaa

.field public static final search_device_name:I = 0x7f121eab

.field public static final search_device_status:I = 0x7f121eac

.field public static final search_display_settings:I = 0x7f121ead

.field public static final search_dndm_alarm_content:I = 0x7f121eae

.field public static final search_dndm_auto_button_title:I = 0x7f121eaf

.field public static final search_dndm_auto_time_setting:I = 0x7f121eb0

.field public static final search_dndm_open:I = 0x7f121eb1

.field public static final search_dndm_repeated_call_setting_title:I = 0x7f121eb2

.field public static final search_dndm_vip_call_setting_title:I = 0x7f121eb3

.field public static final search_dndm_wristband:I = 0x7f121eb4

.field public static final search_do_not_disturb_mode:I = 0x7f121eb5

.field public static final search_doze_always_on_title:I = 0x7f121eb6

.field public static final search_dtmf_tone_enable_title:I = 0x7f121eb7

.field public static final search_empty:I = 0x7f121eb8

.field public static final search_enable_adb:I = 0x7f121eb9

.field public static final search_fluency_mode:I = 0x7f121eba

.field public static final search_font:I = 0x7f121ebb

.field public static final search_force_touch:I = 0x7f121ebc

.field public static final search_gesture_wakeup_title:I = 0x7f121ebd

.field public static final search_gmscore_settings_title:I = 0x7f121ebe

.field public static final search_gps:I = 0x7f121ebf

.field public static final search_handy_mode:I = 0x7f121ec0

.field public static final search_hint:I = 0x7f121ec1

.field public static final search_history_clear_description:I = 0x7f121ec2

.field public static final search_history_description:I = 0x7f121ec3

.field public static final search_home_key:I = 0x7f121ec4

.field public static final search_input_hint:I = 0x7f121ec5

.field public static final search_input_txt_na:I = 0x7f121ec6

.field public static final search_key_settings_title:I = 0x7f121ec7

.field public static final search_lab_functions_title:I = 0x7f121ec8

.field public static final search_label:I = 0x7f121ec9

.field public static final search_language_settings:I = 0x7f121eca

.field public static final search_led_settings:I = 0x7f121ecb

.field public static final search_legal_information:I = 0x7f121ecc

.field public static final search_local_auto_backup_section_title:I = 0x7f121ecd

.field public static final search_local_backup_section_title:I = 0x7f121ece

.field public static final search_location_settings_title:I = 0x7f121ecf

.field public static final search_lock_settings:I = 0x7f121ed0

.field public static final search_lock_sounds_enable_title:I = 0x7f121ed1

.field public static final search_manage_device_admin:I = 0x7f121ed2

.field public static final search_manage_facerecoginition_text:I = 0x7f121ed3

.field public static final search_manage_notification_access_title:I = 0x7f121ed4

.field public static final search_master_clear_title:I = 0x7f121ed5

.field public static final search_menu:I = 0x7f121ed6

.field public static final search_menu_title:I = 0x7f121ed7

.field public static final search_micloud:I = 0x7f121ed8

.field public static final search_music_audio_effect_optimize:I = 0x7f121ed9

.field public static final search_music_equalizer:I = 0x7f121eda

.field public static final search_music_headset_calibrate:I = 0x7f121edb

.field public static final search_music_headset_mode_settings:I = 0x7f121edc

.field public static final search_music_headset_settings:I = 0x7f121edd

.field public static final search_music_mi_effect_title:I = 0x7f121ede

.field public static final search_my_device:I = 0x7f121edf

.field public static final search_networks_title:I = 0x7f121ee0

.field public static final search_nfc_quick_toggle_title:I = 0x7f121ee1

.field public static final search_nfc_se_route_title:I = 0x7f121ee2

.field public static final search_notification_sound_title:I = 0x7f121ee3

.field public static final search_ntts_settings_title:I = 0x7f121ee4

.field public static final search_oldman_mode_entry_name:I = 0x7f121ee5

.field public static final search_onekey_migrate:I = 0x7f121ee6

.field public static final search_other_advanced_settings:I = 0x7f121ee7

.field public static final search_other_settings_sync_settings:I = 0x7f121ee8

.field public static final search_owner_info_settings_title:I = 0x7f121ee9

.field public static final search_page_layout_settings:I = 0x7f121eea

.field public static final search_permission_manager:I = 0x7f121eeb

.field public static final search_permission_settings_title:I = 0x7f121eec

.field public static final search_pick_up_gesture_wakeup_title:I = 0x7f121eed

.field public static final search_pointer_speed:I = 0x7f121eee

.field public static final search_power_checker:I = 0x7f121eef

.field public static final search_power_hide_mode_title:I = 0x7f121ef0

.field public static final search_power_mode:I = 0x7f121ef1

.field public static final search_power_optimize:I = 0x7f121ef2

.field public static final search_power_usage_summary_title:I = 0x7f121ef3

.field public static final search_pref_edge_handgrip:I = 0x7f121ef4

.field public static final search_pref_volume_wake_title:I = 0x7f121ef5

.field public static final search_preferred_network_type_title:I = 0x7f121ef6

.field public static final search_privacy_policy:I = 0x7f121ef7

.field public static final search_privacy_settings:I = 0x7f121ef8

.field public static final search_result_feedback_text:I = 0x7f121ef9

.field public static final search_results_title:I = 0x7f121efa

.field public static final search_ring_volume_option_title:I = 0x7f121efb

.field public static final search_ringtone_title:I = 0x7f121efc

.field public static final search_screen_effect:I = 0x7f121efd

.field public static final search_screen_key_long_press_timeout:I = 0x7f121efe

.field public static final search_screen_on_proximity_sensor_title:I = 0x7f121eff

.field public static final search_screen_paper_mode_title:I = 0x7f121f00

.field public static final search_screen_recorder_title:I = 0x7f121f01

.field public static final search_screen_saver_settings:I = 0x7f121f02

.field public static final search_screen_shot:I = 0x7f121f03

.field public static final search_screen_timeout:I = 0x7f121f04

.field public static final search_second_space:I = 0x7f121f05

.field public static final search_security_encryption_title:I = 0x7f121f06

.field public static final search_security_passwords_title:I = 0x7f121f07

.field public static final search_security_privacy_settings_title:I = 0x7f121f08

.field public static final search_security_settings:I = 0x7f121f09

.field public static final search_settings:I = 0x7f121f0a

.field public static final search_silent_mode_title:I = 0x7f121f0b

.field public static final search_sim_management_title:I = 0x7f121f0c

.field public static final search_sim_status_title:I = 0x7f121f0d

.field public static final search_slot_status_title:I = 0x7f121f0e

.field public static final search_sms_delivered_sound_title:I = 0x7f121f0f

.field public static final search_sms_received_sound_title:I = 0x7f121f10

.field public static final search_sound_effects_enable_title:I = 0x7f121f11

.field public static final search_sound_settings:I = 0x7f121f12

.field public static final search_status_bar_settings:I = 0x7f121f13

.field public static final search_status_bar_settings_custom_carrier:I = 0x7f121f14

.field public static final search_status_bar_settings_expandable_under_keyguard:I = 0x7f121f15

.field public static final search_status_bar_settings_manage_notification:I = 0x7f121f16

.field public static final search_status_bar_settings_show_carrier:I = 0x7f121f17

.field public static final search_status_bar_settings_show_network_speed:I = 0x7f121f18

.field public static final search_status_bar_settings_show_notification_icon:I = 0x7f121f19

.field public static final search_status_bar_settings_show_screenshot_notification:I = 0x7f121f1a

.field public static final search_status_bar_settings_style:I = 0x7f121f1b

.field public static final search_status_bar_settings_toggle_collapse_after_clicked:I = 0x7f121f1c

.field public static final search_status_bar_settings_toggle_position:I = 0x7f121f1d

.field public static final search_storage_settings:I = 0x7f121f1e

.field public static final search_sync_settings:I = 0x7f121f1f

.field public static final search_system_app_settings:I = 0x7f121f20

.field public static final search_theme_settings_title:I = 0x7f121f21

.field public static final search_title_font_settings:I = 0x7f121f22

.field public static final search_touch_assistant:I = 0x7f121f23

.field public static final search_touch_sensitive_title:I = 0x7f121f24

.field public static final search_trusted_credentials:I = 0x7f121f25

.field public static final search_tts_settings_title:I = 0x7f121f26

.field public static final search_turn_on_security_lock:I = 0x7f121f27

.field public static final search_unlogin_account_title:I = 0x7f121f28

.field public static final search_usb_tethering_button_text:I = 0x7f121f29

.field public static final search_user_agreement:I = 0x7f121f2a

.field public static final search_user_dict_settings_title:I = 0x7f121f2b

.field public static final search_vibrate_in_silent_title:I = 0x7f121f2c

.field public static final search_vibrate_on_touch_title:I = 0x7f121f2d

.field public static final search_vibrate_when_ringing_title:I = 0x7f121f2e

.field public static final search_virtual_sim:I = 0x7f121f2f

.field public static final search_vpn_enable:I = 0x7f121f30

.field public static final search_vpn_settings_password:I = 0x7f121f31

.field public static final search_vpn_settings_title:I = 0x7f121f32

.field public static final search_wakeup_for_keyguard_notification_title:I = 0x7f121f33

.field public static final search_wallpaper_settings_title:I = 0x7f121f34

.field public static final search_wfd_settings_title:I = 0x7f121f35

.field public static final search_wifi_advanced_ip_address_title:I = 0x7f121f36

.field public static final search_wifi_advanced_mac_address_title:I = 0x7f121f37

.field public static final search_wifi_enable:I = 0x7f121f38

.field public static final search_wifi_install_credentials:I = 0x7f121f39

.field public static final search_wifi_menu_p2p:I = 0x7f121f3a

.field public static final search_wifi_menu_wps_pin:I = 0x7f121f3b

.field public static final search_wifi_scan_always_available:I = 0x7f121f3c

.field public static final search_wifi_setting_frequency_band_title:I = 0x7f121f3d

.field public static final search_wifi_setting_sleep_policy_title:I = 0x7f121f3e

.field public static final search_wifi_settings_title:I = 0x7f121f3f

.field public static final search_wifi_tether_checkbox:I = 0x7f121f40

.field public static final search_wifi_tether_configure_ap_text:I = 0x7f121f41

.field public static final search_wifi_tether_settings_title:I = 0x7f121f42

.field public static final search_wifi_wapi_cert_install:I = 0x7f121f43

.field public static final search_wifi_wapi_cert_uninstall:I = 0x7f121f44

.field public static final search_wps_connect_title:I = 0x7f121f45

.field public static final search_xiaomi_account:I = 0x7f121f46

.field public static final search_xspace:I = 0x7f121f47

.field public static final second_space:I = 0x7f121f48

.field public static final secondspace_category:I = 0x7f121f49

.field public static final secondspace_delete_space_title:I = 0x7f121f4a

.field public static final secspace_fod_finger_tips:I = 0x7f121f4b

.field public static final secspace_fod_finger_title:I = 0x7f121f4c

.field public static final secspace_fod_use_password:I = 0x7f121f4d

.field public static final secure_keyguard_business_title:I = 0x7f121f4e

.field public static final security_advanced_settings:I = 0x7f121f4f

.field public static final security_advanced_settings_keywords:I = 0x7f121f50

.field public static final security_advanced_settings_no_work_profile_settings_summary:I = 0x7f121f51

.field public static final security_advanced_settings_work_profile_settings_summary:I = 0x7f121f52

.field public static final security_center_title:I = 0x7f121f53

.field public static final security_check_progress_dialog_message:I = 0x7f121f54

.field public static final security_dashboard_summary:I = 0x7f121f55

.field public static final security_enable_widgets_disabled_summary:I = 0x7f121f56

.field public static final security_enable_widgets_title:I = 0x7f121f57

.field public static final security_encryption_alert_dialog_info_first:I = 0x7f121f58

.field public static final security_encryption_alert_dialog_title_first:I = 0x7f121f59

.field public static final security_encryption_attention_1:I = 0x7f121f5a

.field public static final security_encryption_attention_2:I = 0x7f121f5b

.field public static final security_encryption_attention_3:I = 0x7f121f5c

.field public static final security_encryption_attention_4:I = 0x7f121f5d

.field public static final security_encryption_attention_info:I = 0x7f121f5e

.field public static final security_encryption_attention_symbol:I = 0x7f121f5f

.field public static final security_encryption_attention_title:I = 0x7f121f60

.field public static final security_encryption_close_dialog_info:I = 0x7f121f61

.field public static final security_encryption_close_dialog_title:I = 0x7f121f62

.field public static final security_encryption_diaglog_continue:I = 0x7f121f63

.field public static final security_encryption_is_used:I = 0x7f121f64

.field public static final security_encryption_progress_dialog_close:I = 0x7f121f65

.field public static final security_encryption_progress_dialog_open:I = 0x7f121f66

.field public static final security_encryption_rejected_via_home_manager:I = 0x7f121f67

.field public static final security_encryption_title:I = 0x7f121f68

.field public static final security_fingerprint_disclaimer_lockscreen_disabled_1:I = 0x7f121f69

.field public static final security_fingerprint_disclaimer_lockscreen_disabled_2:I = 0x7f121f6a

.field public static final security_input:I = 0x7f121f6b

.field public static final security_input_enable:I = 0x7f121f6c

.field public static final security_input_hint:I = 0x7f121f6d

.field public static final security_input_num_order:I = 0x7f121f6e

.field public static final security_input_num_random_order:I = 0x7f121f6f

.field public static final security_input_num_setting_title:I = 0x7f121f70

.field public static final security_keyguard_category_name:I = 0x7f121f71

.field public static final security_password_category_name:I = 0x7f121f72

.field public static final security_passwords_title:I = 0x7f121f73

.field public static final security_patch:I = 0x7f121f74

.field public static final security_privacy_settings_title:I = 0x7f121f75

.field public static final security_relative:I = 0x7f121f76

.field public static final security_settings_biometric_preference_summary_both_fp_multiple:I = 0x7f121f77

.field public static final security_settings_biometric_preference_summary_both_fp_single:I = 0x7f121f78

.field public static final security_settings_biometric_preference_summary_none_enrolled:I = 0x7f121f79

.field public static final security_settings_biometric_preference_title:I = 0x7f121f7a

.field public static final security_settings_face_enroll_consent_introduction_title:I = 0x7f121f7b

.field public static final security_settings_face_enroll_dialog_ok:I = 0x7f121f7c

.field public static final security_settings_face_enroll_done:I = 0x7f121f7d

.field public static final security_settings_face_enroll_education_accessibility_dialog_message:I = 0x7f121f7e

.field public static final security_settings_face_enroll_education_accessibility_dialog_negative:I = 0x7f121f7f

.field public static final security_settings_face_enroll_education_accessibility_dialog_positive:I = 0x7f121f80

.field public static final security_settings_face_enroll_education_message:I = 0x7f121f81

.field public static final security_settings_face_enroll_education_message_accessibility:I = 0x7f121f82

.field public static final security_settings_face_enroll_education_start:I = 0x7f121f83

.field public static final security_settings_face_enroll_education_title:I = 0x7f121f84

.field public static final security_settings_face_enroll_education_title_accessibility:I = 0x7f121f85

.field public static final security_settings_face_enroll_education_title_unlock_disabled:I = 0x7f121f86

.field public static final security_settings_face_enroll_enrolling_skip:I = 0x7f121f87

.field public static final security_settings_face_enroll_error_dialog_title:I = 0x7f121f88

.field public static final security_settings_face_enroll_error_generic_dialog_message:I = 0x7f121f89

.field public static final security_settings_face_enroll_error_timeout_dialog_message:I = 0x7f121f8a

.field public static final security_settings_face_enroll_finish_title:I = 0x7f121f8b

.field public static final security_settings_face_enroll_improve_face_alert_body:I = 0x7f121f8c

.field public static final security_settings_face_enroll_improve_face_alert_body_fingerprint:I = 0x7f121f8d

.field public static final security_settings_face_enroll_improve_face_alert_title:I = 0x7f121f8e

.field public static final security_settings_face_enroll_introduction_accessibility:I = 0x7f121f8f

.field public static final security_settings_face_enroll_introduction_accessibility_diversity:I = 0x7f121f90

.field public static final security_settings_face_enroll_introduction_accessibility_expanded:I = 0x7f121f91

.field public static final security_settings_face_enroll_introduction_accessibility_vision:I = 0x7f121f92

.field public static final security_settings_face_enroll_introduction_agree:I = 0x7f121f93

.field public static final security_settings_face_enroll_introduction_cancel:I = 0x7f121f94

.field public static final security_settings_face_enroll_introduction_consent_message:I = 0x7f121f95

.field public static final security_settings_face_enroll_introduction_control_consent_message:I = 0x7f121f96

.field public static final security_settings_face_enroll_introduction_control_consent_title:I = 0x7f121f97

.field public static final security_settings_face_enroll_introduction_control_message:I = 0x7f121f98

.field public static final security_settings_face_enroll_introduction_control_title:I = 0x7f121f99

.field public static final security_settings_face_enroll_introduction_how_consent_message:I = 0x7f121f9a

.field public static final security_settings_face_enroll_introduction_how_message:I = 0x7f121f9b

.field public static final security_settings_face_enroll_introduction_how_title:I = 0x7f121f9c

.field public static final security_settings_face_enroll_introduction_info_consent_gaze:I = 0x7f121f9d

.field public static final security_settings_face_enroll_introduction_info_consent_glasses:I = 0x7f121f9e

.field public static final security_settings_face_enroll_introduction_info_consent_looking:I = 0x7f121f9f

.field public static final security_settings_face_enroll_introduction_info_gaze:I = 0x7f121fa0

.field public static final security_settings_face_enroll_introduction_info_glasses:I = 0x7f121fa1

.field public static final security_settings_face_enroll_introduction_info_less_secure:I = 0x7f121fa2

.field public static final security_settings_face_enroll_introduction_info_looking:I = 0x7f121fa3

.field public static final security_settings_face_enroll_introduction_info_title:I = 0x7f121fa4

.field public static final security_settings_face_enroll_introduction_message:I = 0x7f121fa5

.field public static final security_settings_face_enroll_introduction_message_setup:I = 0x7f121fa6

.field public static final security_settings_face_enroll_introduction_message_unlock_disabled:I = 0x7f121fa7

.field public static final security_settings_face_enroll_introduction_more:I = 0x7f121fa8

.field public static final security_settings_face_enroll_introduction_no_thanks:I = 0x7f121fa9

.field public static final security_settings_face_enroll_introduction_title:I = 0x7f121faa

.field public static final security_settings_face_enroll_introduction_title_unlock_disabled:I = 0x7f121fab

.field public static final security_settings_face_enroll_must_re_enroll_subtitle:I = 0x7f121fac

.field public static final security_settings_face_enroll_must_re_enroll_title:I = 0x7f121fad

.field public static final security_settings_face_enroll_repeat_title:I = 0x7f121fae

.field public static final security_settings_face_enroll_should_re_enroll_subtitle:I = 0x7f121faf

.field public static final security_settings_face_enroll_should_re_enroll_title:I = 0x7f121fb0

.field public static final security_settings_face_preference_summary:I = 0x7f121fb1

.field public static final security_settings_face_preference_summary_none:I = 0x7f121fb2

.field public static final security_settings_face_preference_title:I = 0x7f121fb3

.field public static final security_settings_face_profile_preference_title:I = 0x7f121fb4

.field public static final security_settings_face_settings_context_subtitle:I = 0x7f121fb5

.field public static final security_settings_face_settings_enroll:I = 0x7f121fb6

.field public static final security_settings_face_settings_footer:I = 0x7f121fb7

.field public static final security_settings_face_settings_footer_attention_not_supported:I = 0x7f121fb8

.field public static final security_settings_face_settings_preferences_category:I = 0x7f121fb9

.field public static final security_settings_face_settings_remove_dialog_details:I = 0x7f121fba

.field public static final security_settings_face_settings_remove_dialog_details_convenience:I = 0x7f121fbb

.field public static final security_settings_face_settings_remove_dialog_title:I = 0x7f121fbc

.field public static final security_settings_face_settings_remove_face_model:I = 0x7f121fbd

.field public static final security_settings_face_settings_require_attention:I = 0x7f121fbe

.field public static final security_settings_face_settings_require_attention_details:I = 0x7f121fbf

.field public static final security_settings_face_settings_require_confirmation:I = 0x7f121fc0

.field public static final security_settings_face_settings_require_confirmation_details:I = 0x7f121fc1

.field public static final security_settings_face_settings_use_face_category:I = 0x7f121fc2

.field public static final security_settings_fingerprint_bad_calibration:I = 0x7f121fc3

.field public static final security_settings_fingerprint_enroll_consent_introduction_title:I = 0x7f121fc4

.field public static final security_settings_fingerprint_enroll_dialog_delete:I = 0x7f121fc5

.field public static final security_settings_fingerprint_enroll_dialog_name_label:I = 0x7f121fc6

.field public static final security_settings_fingerprint_enroll_dialog_ok:I = 0x7f121fc7

.field public static final security_settings_fingerprint_enroll_disclaimer:I = 0x7f121fc8

.field public static final security_settings_fingerprint_enroll_done:I = 0x7f121fc9

.field public static final security_settings_fingerprint_enroll_enrolling_skip:I = 0x7f121fca

.field public static final security_settings_fingerprint_enroll_error_dialog_title:I = 0x7f121fcb

.field public static final security_settings_fingerprint_enroll_error_generic_dialog_message:I = 0x7f121fcc

.field public static final security_settings_fingerprint_enroll_error_timeout_dialog_message:I = 0x7f121fcd

.field public static final security_settings_fingerprint_enroll_find_sensor_content_description:I = 0x7f121fce

.field public static final security_settings_fingerprint_enroll_find_sensor_message:I = 0x7f121fcf

.field public static final security_settings_fingerprint_enroll_find_sensor_title:I = 0x7f121fd0

.field public static final security_settings_fingerprint_enroll_finish_title:I = 0x7f121fd1

.field public static final security_settings_fingerprint_enroll_finish_v2_message:I = 0x7f121fd2

.field public static final security_settings_fingerprint_enroll_introduction_agree:I = 0x7f121fd3

.field public static final security_settings_fingerprint_enroll_introduction_cancel:I = 0x7f121fd4

.field public static final security_settings_fingerprint_enroll_introduction_consent_message:I = 0x7f121fd5

.field public static final security_settings_fingerprint_enroll_introduction_footer_message_1:I = 0x7f121fd6

.field public static final security_settings_fingerprint_enroll_introduction_footer_message_2:I = 0x7f121fd7

.field public static final security_settings_fingerprint_enroll_introduction_footer_message_3:I = 0x7f121fd8

.field public static final security_settings_fingerprint_enroll_introduction_footer_title_1:I = 0x7f121fd9

.field public static final security_settings_fingerprint_enroll_introduction_footer_title_2:I = 0x7f121fda

.field public static final security_settings_fingerprint_enroll_introduction_footer_title_consent_1:I = 0x7f121fdb

.field public static final security_settings_fingerprint_enroll_introduction_message_unlock_disabled:I = 0x7f121fdc

.field public static final security_settings_fingerprint_enroll_introduction_no_thanks:I = 0x7f121fdd

.field public static final security_settings_fingerprint_enroll_introduction_title:I = 0x7f121fde

.field public static final security_settings_fingerprint_enroll_introduction_title_unlock_disabled:I = 0x7f121fdf

.field public static final security_settings_fingerprint_enroll_introduction_v2_message:I = 0x7f121fe0

.field public static final security_settings_fingerprint_enroll_lift_touch_again:I = 0x7f121fe1

.field public static final security_settings_fingerprint_enroll_repeat_message:I = 0x7f121fe2

.field public static final security_settings_fingerprint_enroll_repeat_title:I = 0x7f121fe3

.field public static final security_settings_fingerprint_enroll_setup_screen_lock:I = 0x7f121fe4

.field public static final security_settings_fingerprint_enroll_start_message:I = 0x7f121fe5

.field public static final security_settings_fingerprint_enroll_start_title:I = 0x7f121fe6

.field public static final security_settings_fingerprint_enroll_touch_dialog_message:I = 0x7f121fe7

.field public static final security_settings_fingerprint_enroll_touch_dialog_title:I = 0x7f121fe8

.field public static final security_settings_fingerprint_enroll_udfps_title:I = 0x7f121fe9

.field public static final security_settings_fingerprint_preference_summary_none:I = 0x7f121fea

.field public static final security_settings_fingerprint_preference_title:I = 0x7f121feb

.field public static final security_settings_fingerprint_v2_enroll_acquire_already_enrolled:I = 0x7f121fec

.field public static final security_settings_fingerprint_v2_enroll_acquire_clean_sensor:I = 0x7f121fed

.field public static final security_settings_fingerprint_v2_enroll_acquire_partially_detected:I = 0x7f121fee

.field public static final security_settings_fingerprint_v2_enroll_acquire_too_bright:I = 0x7f121fef

.field public static final security_settings_fingerprint_v2_enroll_acquire_too_fast:I = 0x7f121ff0

.field public static final security_settings_fingerprint_v2_enroll_acquire_try_adjusting:I = 0x7f121ff1

.field public static final security_settings_fingerprint_v2_enroll_error_max_attempts:I = 0x7f121ff2

.field public static final security_settings_fingerprint_v2_enroll_introduction_footer_message_1:I = 0x7f121ff3

.field public static final security_settings_fingerprint_v2_enroll_introduction_footer_message_2:I = 0x7f121ff4

.field public static final security_settings_fingerprint_v2_enroll_introduction_footer_message_3:I = 0x7f121ff5

.field public static final security_settings_fingerprint_v2_enroll_introduction_footer_message_4:I = 0x7f121ff6

.field public static final security_settings_fingerprint_v2_enroll_introduction_footer_message_5:I = 0x7f121ff7

.field public static final security_settings_fingerprint_v2_enroll_introduction_footer_message_6:I = 0x7f121ff8

.field public static final security_settings_fingerprint_v2_enroll_introduction_footer_message_consent_2:I = 0x7f121ff9

.field public static final security_settings_fingerprint_v2_enroll_introduction_footer_message_consent_3:I = 0x7f121ffa

.field public static final security_settings_fingerprint_v2_enroll_introduction_footer_message_consent_4:I = 0x7f121ffb

.field public static final security_settings_fingerprint_v2_enroll_introduction_footer_message_consent_5:I = 0x7f121ffc

.field public static final security_settings_fingerprint_v2_enroll_introduction_footer_message_consent_6:I = 0x7f121ffd

.field public static final security_settings_fingerprint_v2_enroll_introduction_footer_title_1:I = 0x7f121ffe

.field public static final security_settings_fingerprint_v2_enroll_introduction_footer_title_2:I = 0x7f121fff

.field public static final security_settings_fingerprint_v2_enroll_introduction_message_learn_more:I = 0x7f122000

.field public static final security_settings_fingerprint_v2_enroll_introduction_message_setup:I = 0x7f122001

.field public static final security_settings_fingerprint_v2_home_screen_text:I = 0x7f122002

.field public static final security_settings_summary:I = 0x7f122003

.field public static final security_settings_title:I = 0x7f122004

.field public static final security_settings_udfps_enroll_a11y:I = 0x7f122005

.field public static final security_settings_udfps_enroll_edge_message:I = 0x7f122006

.field public static final security_settings_udfps_enroll_find_sensor_message:I = 0x7f122007

.field public static final security_settings_udfps_enroll_find_sensor_start_button:I = 0x7f122008

.field public static final security_settings_udfps_enroll_find_sensor_title:I = 0x7f122009

.field public static final security_settings_udfps_enroll_fingertip_title:I = 0x7f12200a

.field public static final security_settings_udfps_enroll_left_edge_title:I = 0x7f12200b

.field public static final security_settings_udfps_enroll_progress_a11y_message:I = 0x7f12200c

.field public static final security_settings_udfps_enroll_repeat_a11y_message:I = 0x7f12200d

.field public static final security_settings_udfps_enroll_repeat_message:I = 0x7f12200e

.field public static final security_settings_udfps_enroll_repeat_title_touch_icon:I = 0x7f12200f

.field public static final security_settings_udfps_enroll_right_edge_title:I = 0x7f122010

.field public static final security_settings_udfps_enroll_start_message:I = 0x7f122011

.field public static final security_settings_udfps_enroll_title_one_more_time:I = 0x7f122012

.field public static final security_settings_udfps_side_fingerprint_help:I = 0x7f122013

.field public static final security_settings_udfps_tip_fingerprint_help:I = 0x7f122014

.field public static final security_settings_work_fingerprint_preference_title:I = 0x7f122015

.field public static final security_status:I = 0x7f122016

.field public static final security_status_title:I = 0x7f122017

.field public static final security_update:I = 0x7f122018

.field public static final see_less:I = 0x7f122019

.field public static final see_more:I = 0x7f12201a

.field public static final seek_bar_info:I = 0x7f12201b

.field public static final seekbar_text:I = 0x7f12201c

.field public static final select_all:I = 0x7f12201d

.field public static final select_application:I = 0x7f12201e

.field public static final select_automatically:I = 0x7f12201f

.field public static final select_device_admin_msg:I = 0x7f122020

.field public static final select_invalid_bug_report_handler_toast_text:I = 0x7f122021

.field public static final select_logd_level:I = 0x7f122022

.field public static final select_logd_size_dialog_title:I = 0x7f122023

.field public static final select_logd_size_title:I = 0x7f122024

.field public static final select_logpersist_dialog_title:I = 0x7f122025

.field public static final select_logpersist_title:I = 0x7f122026

.field public static final select_private_dns_configuration_dialog_title:I = 0x7f122027

.field public static final select_private_dns_configuration_title:I = 0x7f122028

.field public static final select_sim_card:I = 0x7f122029

.field public static final select_sim_for_calls:I = 0x7f12202a

.field public static final select_sim_for_data:I = 0x7f12202b

.field public static final select_sim_for_sms:I = 0x7f12202c

.field public static final select_specific_sim_for_data_button:I = 0x7f12202d

.field public static final select_specific_sim_for_data_msg:I = 0x7f12202e

.field public static final select_specific_sim_for_data_title:I = 0x7f12202f

.field public static final select_ssid_type_title:I = 0x7f122030

.field public static final select_time_format_category_title:I = 0x7f122031

.field public static final select_time_format_title:I = 0x7f122032

.field public static final select_to_speak_summary:I = 0x7f122033

.field public static final select_usb_configuration_dialog_title:I = 0x7f122034

.field public static final select_usb_configuration_title:I = 0x7f122035

.field public static final select_webview_provider_dialog_title:I = 0x7f122036

.field public static final select_webview_provider_title:I = 0x7f122037

.field public static final select_webview_provider_toast_text:I = 0x7f122038

.field public static final send:I = 0x7f122039

.field public static final send_diagnosis_log:I = 0x7f12203a

.field public static final sending:I = 0x7f12203b

.field public static final sensor_toggle_description:I = 0x7f12203c

.field public static final sensors_off_quick_settings_title:I = 0x7f12203d

.field public static final separate_keyboard:I = 0x7f12203e

.field public static final separate_keyboard_summary_fold:I = 0x7f12203f

.field public static final separate_keyboard_summary_pad:I = 0x7f122040

.field public static final service_background_processes:I = 0x7f122041

.field public static final service_client_name:I = 0x7f122042

.field public static final service_foreground_processes:I = 0x7f122043

.field public static final service_manage:I = 0x7f122044

.field public static final service_manage_description:I = 0x7f122045

.field public static final service_process_name:I = 0x7f122046

.field public static final service_restarting:I = 0x7f122047

.field public static final service_started_by_app:I = 0x7f122048

.field public static final service_stop:I = 0x7f122049

.field public static final service_stop_description:I = 0x7f12204a

.field public static final services_subtitle:I = 0x7f12204b

.field public static final set_data_limit:I = 0x7f12204c

.field public static final set_data_warning:I = 0x7f12204d

.field public static final set_jeejen_prompt_message:I = 0x7f12204e

.field public static final set_jeejen_prompt_title:I = 0x7f12204f

.field public static final set_screen_paper_mode:I = 0x7f122050

.field public static final set_shoulder_key_shortcut_checkbox_message:I = 0x7f122051

.field public static final set_shoulder_key_shortcut_message:I = 0x7f122052

.field public static final set_shoulder_key_shortcut_negative_button:I = 0x7f122053

.field public static final set_shoulder_key_shortcut_positive_button:I = 0x7f122054

.field public static final set_shoulder_key_shortcut_title:I = 0x7f122055

.field public static final setting_privacy_password:I = 0x7f122056

.field public static final settings_ad_loading:I = 0x7f122057

.field public static final settings_ad_loading_unavailable:I = 0x7f122058

.field public static final settings_backup:I = 0x7f122059

.field public static final settings_backup_summary:I = 0x7f12205a

.field public static final settings_button:I = 0x7f12205b

.field public static final settings_charging_at_fast_speed:I = 0x7f12205c

.field public static final settings_charging_at_normal_speed:I = 0x7f12205d

.field public static final settings_charging_at_top_speed:I = 0x7f12205e

.field public static final settings_charging_fully_charged:I = 0x7f12205f

.field public static final settings_charging_not_charging:I = 0x7f122060

.field public static final settings_forbidden_message:I = 0x7f122061

.field public static final settings_keyboard_title:I = 0x7f122062

.field public static final settings_keyboard_warning:I = 0x7f122063

.field public static final settings_label:I = 0x7f122064

.field public static final settings_label_launcher:I = 0x7f122065

.field public static final settings_license_activity_loading:I = 0x7f122066

.field public static final settings_license_activity_title:I = 0x7f122067

.field public static final settings_license_activity_unavailable:I = 0x7f122068

.field public static final settings_manual_activity_title:I = 0x7f122069

.field public static final settings_manual_activity_unavailable:I = 0x7f12206a

.field public static final settings_new_version_btn:I = 0x7f12206b

.field public static final settings_notify_need_backup:I = 0x7f12206c

.field public static final settings_notify_new_version:I = 0x7f12206d

.field public static final settings_notify_phone_recycled:I = 0x7f12206e

.field public static final settings_package:I = 0x7f12206f

.field public static final settings_panel_title:I = 0x7f122070

.field public static final settings_safetylegal_activity_loading:I = 0x7f122071

.field public static final settings_safetylegal_activity_title:I = 0x7f122072

.field public static final settings_safetylegal_activity_unreachable:I = 0x7f122073

.field public static final settings_safetylegal_title:I = 0x7f122074

.field public static final settings_shortcut:I = 0x7f122075

.field public static final settingslib_config_headlineFontFamily:I = 0x7f122076

.field public static final settingslib_learn_more_text:I = 0x7f122077

.field public static final setup_choose_unlock_mixed_password:I = 0x7f122078

.field public static final setup_choose_unlock_mixpassword_title:I = 0x7f122079

.field public static final setup_choose_unlock_password:I = 0x7f12207a

.field public static final setup_choose_unlock_password_msg:I = 0x7f12207b

.field public static final setup_choose_unlock_password_title:I = 0x7f12207c

.field public static final setup_choose_unlock_pattern:I = 0x7f12207d

.field public static final setup_choose_unlock_pattern_title:I = 0x7f12207e

.field public static final setup_choose_unlock_type_title:I = 0x7f12207f

.field public static final setup_fingerprint_enroll_enrolling_skip_message:I = 0x7f122080

.field public static final setup_fingerprint_enroll_enrolling_skip_title:I = 0x7f122081

.field public static final setup_fingerprint_enroll_skip_after_adding_lock_text:I = 0x7f122082

.field public static final setup_fingerprint_enroll_skip_title:I = 0x7f122083

.field public static final setup_lock_settings_options_button_label:I = 0x7f122084

.field public static final setup_lock_settings_options_dialog_title:I = 0x7f122085

.field public static final setup_lock_settings_picker_message:I = 0x7f122086

.field public static final setup_lock_settings_picker_title:I = 0x7f122087

.field public static final setup_password_back:I = 0x7f122088

.field public static final setup_password_confirm:I = 0x7f122089

.field public static final setup_password_skip:I = 0x7f12208a

.field public static final severe_threats_summary:I = 0x7f12208b

.field public static final severe_threats_title:I = 0x7f12208c

.field public static final share:I = 0x7f12208d

.field public static final share_remote_bugreport_action:I = 0x7f12208e

.field public static final share_remote_bugreport_dialog_message:I = 0x7f12208f

.field public static final share_remote_bugreport_dialog_message_finished:I = 0x7f122090

.field public static final share_remote_bugreport_dialog_title:I = 0x7f122091

.field public static final share_this_wifi:I = 0x7f122092

.field public static final shared_data_delete_failure_text:I = 0x7f122093

.field public static final shared_data_no_accessors_dialog_text:I = 0x7f122094

.field public static final shared_data_no_blobs_text:I = 0x7f122095

.field public static final shared_data_query_failure_text:I = 0x7f122096

.field public static final shared_data_summary:I = 0x7f122097

.field public static final shared_data_title:I = 0x7f122098

.field public static final sharing_remote_bugreport_dialog_message:I = 0x7f122099

.field public static final short_summary_font_size:I = 0x7f12209a

.field public static final short_time:I = 0x7f12209b

.field public static final shoulder_key_diagram:I = 0x7f12209c

.field public static final shoulder_key_game_light_settings:I = 0x7f12209d

.field public static final shoulder_key_settings:I = 0x7f12209e

.field public static final shoulder_key_settings_title:I = 0x7f12209f

.field public static final shoulder_key_shortcut_double_click:I = 0x7f1220a0

.field public static final shoulder_key_shortcut_launch_camera:I = 0x7f1220a1

.field public static final shoulder_key_shortcut_launch_screen_recorder:I = 0x7f1220a2

.field public static final shoulder_key_shortcut_launch_sound_recorder:I = 0x7f1220a3

.field public static final shoulder_key_shortcut_launch_video:I = 0x7f1220a4

.field public static final shoulder_key_shortcut_long_press:I = 0x7f1220a5

.field public static final shoulder_key_shortcut_mute:I = 0x7f1220a6

.field public static final shoulder_key_shortcut_none:I = 0x7f1220a7

.field public static final shoulder_key_shortcut_settings:I = 0x7f1220a8

.field public static final shoulder_key_shortcut_settings_summary:I = 0x7f1220a9

.field public static final shoulder_key_shortcut_single_click:I = 0x7f1220aa

.field public static final shoulder_key_shortcut_turn_on_torch:I = 0x7f1220ab

.field public static final shoulder_key_shortcut_vibrate:I = 0x7f1220ac

.field public static final shoulder_key_sound_settings:I = 0x7f1220ad

.field public static final shoulder_key_sound_switch_title:I = 0x7f1220ae

.field public static final shoulder_key_sound_text_bullet:I = 0x7f1220af

.field public static final shoulder_key_sound_text_current:I = 0x7f1220b0

.field public static final shoulder_key_sound_text_sword:I = 0x7f1220b1

.field public static final shoulder_key_sound_text_wind:I = 0x7f1220b2

.field public static final shoulder_key_switch_status_close:I = 0x7f1220b3

.field public static final shoulder_key_switch_status_open:I = 0x7f1220b4

.field public static final show_all_anrs:I = 0x7f1220b5

.field public static final show_all_anrs_summary:I = 0x7f1220b6

.field public static final show_all_apps:I = 0x7f1220b7

.field public static final show_background_processes:I = 0x7f1220b8

.field public static final show_badge_notifications_short:I = 0x7f1220b9

.field public static final show_carrier_under_keyguard_title:I = 0x7f1220ba

.field public static final show_charging_in_non_lockscreen_animation:I = 0x7f1220bb

.field public static final show_charging_in_non_lockscreen_animation_summary:I = 0x7f1220bc

.field public static final show_clip_access_notification:I = 0x7f1220bd

.field public static final show_clip_access_notification_summary:I = 0x7f1220be

.field public static final show_connected_devices_summary:I = 0x7f1220bf

.field public static final show_connected_devices_title:I = 0x7f1220c0

.field public static final show_dev_already:I = 0x7f1220c1

.field public static final show_dev_on:I = 0x7f1220c2

.field public static final show_fc:I = 0x7f1220c3

.field public static final show_fc_summary:I = 0x7f1220c4

.field public static final show_first_crash_dialog:I = 0x7f1220c5

.field public static final show_first_crash_dialog_summary:I = 0x7f1220c6

.field public static final show_hw_layers_updates:I = 0x7f1220c7

.field public static final show_hw_layers_updates_summary:I = 0x7f1220c8

.field public static final show_hw_screen_updates:I = 0x7f1220c9

.field public static final show_hw_screen_updates_summary:I = 0x7f1220ca

.field public static final show_ime:I = 0x7f1220cb

.field public static final show_ime_summary:I = 0x7f1220cc

.field public static final show_magnifier_when_input:I = 0x7f1220cd

.field public static final show_menu:I = 0x7f1220ce

.field public static final show_network_speed_title:I = 0x7f1220cf

.field public static final show_non_rect_clip:I = 0x7f1220d0

.field public static final show_notification_channel_warnings:I = 0x7f1220d1

.field public static final show_notification_channel_warnings_summary:I = 0x7f1220d2

.field public static final show_notification_icon_title:I = 0x7f1220d3

.field public static final show_operator_name_summary:I = 0x7f1220d4

.field public static final show_operator_name_title:I = 0x7f1220d5

.field public static final show_owner_info_on_lockscreen_label:I = 0x7f1220d6

.field public static final show_password:I = 0x7f1220d7

.field public static final show_password_summary:I = 0x7f1220d8

.field public static final show_profile_info_on_lockscreen_label:I = 0x7f1220d9

.field public static final show_refresh_rate:I = 0x7f1220da

.field public static final show_refresh_rate_summary:I = 0x7f1220db

.field public static final show_rounded_corners:I = 0x7f1220dc

.field public static final show_running_services:I = 0x7f1220dd

.field public static final show_screen_updates:I = 0x7f1220de

.field public static final show_screen_updates_summary:I = 0x7f1220df

.field public static final show_touches:I = 0x7f1220e0

.field public static final show_touches_summary:I = 0x7f1220e1

.field public static final show_user_info_on_lockscreen_label:I = 0x7f1220e2

.field public static final show_wifi_name_title:I = 0x7f1220e3

.field public static final shut_down_find_device_failure_confirm:I = 0x7f1220e4

.field public static final shut_down_find_device_network_failure_content:I = 0x7f1220e5

.field public static final shut_down_find_device_network_failure_title:I = 0x7f1220e6

.field public static final shuting_down_find_device:I = 0x7f1220e7

.field public static final sidebar_setting:I = 0x7f1220e8

.field public static final silent_duration:I = 0x7f1220e9

.field public static final silent_mode:I = 0x7f1220ea

.field public static final silent_mode_disable_media:I = 0x7f1220eb

.field public static final silent_mode_enable_media:I = 0x7f1220ec

.field public static final silent_mode_summary:I = 0x7f1220ed

.field public static final silent_mode_title:I = 0x7f1220ee

.field public static final silent_mode_toggle_title:I = 0x7f1220ef

.field public static final silent_notifications_status_bar:I = 0x7f1220f0

.field public static final silent_off:I = 0x7f1220f1

.field public static final silent_settings:I = 0x7f1220f2

.field public static final silent_standard:I = 0x7f1220f3

.field public static final silent_time:I = 0x7f1220f4

.field public static final silent_total:I = 0x7f1220f5

.field public static final sim_action_cancel:I = 0x7f1220f6

.field public static final sim_action_continue:I = 0x7f1220f7

.field public static final sim_action_enable_dsds_text:I = 0x7f1220f8

.field public static final sim_action_enable_dsds_title:I = 0x7f1220f9

.field public static final sim_action_enable_sim_fail_text:I = 0x7f1220fa

.field public static final sim_action_enable_sim_fail_title:I = 0x7f1220fb

.field public static final sim_action_enable_sub_dialog_title:I = 0x7f1220fc

.field public static final sim_action_enable_sub_dialog_title_without_carrier_name:I = 0x7f1220fd

.field public static final sim_action_enabling_sim_without_carrier_name:I = 0x7f1220fe

.field public static final sim_action_no_thanks:I = 0x7f1220ff

.field public static final sim_action_reboot:I = 0x7f122100

.field public static final sim_action_restart_text:I = 0x7f122101

.field public static final sim_action_restart_title:I = 0x7f122102

.field public static final sim_action_switch_psim_dialog_title:I = 0x7f122103

.field public static final sim_action_switch_sub_dialog_carrier_list_item_for_turning_off:I = 0x7f122104

.field public static final sim_action_switch_sub_dialog_confirm:I = 0x7f122105

.field public static final sim_action_switch_sub_dialog_info_outline_for_turning_off:I = 0x7f122106

.field public static final sim_action_switch_sub_dialog_mep_text:I = 0x7f122107

.field public static final sim_action_switch_sub_dialog_mep_title:I = 0x7f122108

.field public static final sim_action_switch_sub_dialog_progress:I = 0x7f122109

.field public static final sim_action_switch_sub_dialog_text:I = 0x7f12210a

.field public static final sim_action_switch_sub_dialog_text_downloaded:I = 0x7f12210b

.field public static final sim_action_switch_sub_dialog_text_single_sim:I = 0x7f12210c

.field public static final sim_action_switch_sub_dialog_title:I = 0x7f12210d

.field public static final sim_action_yes:I = 0x7f12210e

.field public static final sim_bad_pin:I = 0x7f12210f

.field public static final sim_call_back_title:I = 0x7f122110

.field public static final sim_calls_always_use:I = 0x7f122111

.field public static final sim_calls_ask_first_prefs_title:I = 0x7f122112

.field public static final sim_card:I = 0x7f122113

.field public static final sim_card_label:I = 0x7f122114

.field public static final sim_card_lock:I = 0x7f122115

.field public static final sim_card_number_title:I = 0x7f122116

.field public static final sim_card_select_title:I = 0x7f122117

.field public static final sim_cards_changed_message:I = 0x7f122118

.field public static final sim_cards_changed_message_summary:I = 0x7f122119

.field public static final sim_category_active_sim:I = 0x7f12211a

.field public static final sim_category_default_active_sim:I = 0x7f12211b

.field public static final sim_category_inactive_sim:I = 0x7f12211c

.field public static final sim_category_title:I = 0x7f12211d

.field public static final sim_cellular_data_unavailable:I = 0x7f12211e

.field public static final sim_cellular_data_unavailable_summary:I = 0x7f12211f

.field public static final sim_change_data_message:I = 0x7f122120

.field public static final sim_change_data_ok:I = 0x7f122121

.field public static final sim_change_data_title:I = 0x7f122122

.field public static final sim_change_failed:I = 0x7f122123

.field public static final sim_change_failed_enable_sim_lock:I = 0x7f122124

.field public static final sim_change_pin:I = 0x7f122125

.field public static final sim_change_succeeded:I = 0x7f122126

.field public static final sim_combination_warning_notification_title:I = 0x7f122127

.field public static final sim_disable_sim_lock:I = 0x7f122128

.field public static final sim_editor_carrier:I = 0x7f122129

.field public static final sim_editor_color:I = 0x7f12212a

.field public static final sim_editor_name:I = 0x7f12212b

.field public static final sim_editor_number:I = 0x7f12212c

.field public static final sim_editor_title:I = 0x7f12212d

.field public static final sim_enable_sim_lock:I = 0x7f12212e

.field public static final sim_enter_cancel:I = 0x7f12212f

.field public static final sim_enter_new:I = 0x7f122130

.field public static final sim_enter_ok:I = 0x7f122131

.field public static final sim_enter_old:I = 0x7f122132

.field public static final sim_enter_pin:I = 0x7f122133

.field public static final sim_lock_change_not_supported:I = 0x7f122134

.field public static final sim_lock_failed:I = 0x7f122135

.field public static final sim_lock_off:I = 0x7f122136

.field public static final sim_lock_on:I = 0x7f122137

.field public static final sim_lock_settings:I = 0x7f122138

.field public static final sim_lock_settings_category:I = 0x7f122139

.field public static final sim_lock_settings_summary_off:I = 0x7f12213a

.field public static final sim_lock_settings_summary_on:I = 0x7f12213b

.field public static final sim_lock_settings_title:I = 0x7f12213c

.field public static final sim_management_title:I = 0x7f12213d

.field public static final sim_management_title_singlesim:I = 0x7f12213e

.field public static final sim_multi_sims_summary:I = 0x7f12213f

.field public static final sim_multi_sims_title:I = 0x7f122140

.field public static final sim_name_hint:I = 0x7f122141

.field public static final sim_no_inserted_msg:I = 0x7f122142

.field public static final sim_no_service:I = 0x7f122143

.field public static final sim_notification_summary:I = 0x7f122144

.field public static final sim_notification_title:I = 0x7f122145

.field public static final sim_nrca_title:I = 0x7f122146

.field public static final sim_other_call_settings:I = 0x7f122147

.field public static final sim_outgoing_call_title:I = 0x7f122148

.field public static final sim_pin_change:I = 0x7f122149

.field public static final sim_pin_disable_failed:I = 0x7f12214a

.field public static final sim_pin_enable_failed:I = 0x7f12214b

.field public static final sim_pin_toggle:I = 0x7f12214c

.field public static final sim_pins_dont_match:I = 0x7f12214d

.field public static final sim_preferred_message:I = 0x7f12214e

.field public static final sim_preferred_title:I = 0x7f12214f

.field public static final sim_radio_off:I = 0x7f122150

.field public static final sim_reenter_new:I = 0x7f122151

.field public static final sim_select_card:I = 0x7f122152

.field public static final sim_selection_channel_title:I = 0x7f122153

.field public static final sim_selection_required_pref:I = 0x7f122154

.field public static final sim_settings_summary:I = 0x7f122155

.field public static final sim_settings_title:I = 0x7f122156

.field public static final sim_setup_channel_id:I = 0x7f122157

.field public static final sim_setup_wizard_title:I = 0x7f122158

.field public static final sim_signal_strength:I = 0x7f122159

.field public static final sim_status_title:I = 0x7f12215a

.field public static final sim_status_title_sim_slot:I = 0x7f12215b

.field public static final sim_switch_button:I = 0x7f12215c

.field public static final sim_switch_channel_id:I = 0x7f12215d

.field public static final sim_vonr_title:I = 0x7f12215e

.field public static final sim_wallet:I = 0x7f12215f

.field public static final sim_wallet_desc:I = 0x7f122160

.field public static final simulate_color_space:I = 0x7f122161

.field public static final single_key_use:I = 0x7f122162

.field public static final single_key_use_summary:I = 0x7f122163

.field public static final single_press_AI_button:I = 0x7f122164

.field public static final size_byte:I = 0x7f122165

.field public static final size_compat1:I = 0x7f122166

.field public static final size_compat2:I = 0x7f122167

.field public static final size_embedded:I = 0x7f122168

.field public static final size_full:I = 0x7f122169

.field public static final size_giga_byte:I = 0x7f12216a

.field public static final size_kilo_byte:I = 0x7f12216b

.field public static final size_mega_byte:I = 0x7f12216c

.field public static final size_peta_byte:I = 0x7f12216d

.field public static final size_suffix:I = 0x7f12216e

.field public static final size_tera_byte:I = 0x7f12216f

.field public static final skip_anyway_button_label:I = 0x7f122170

.field public static final skip_label:I = 0x7f122171

.field public static final skip_lock_screen_confirm_msg:I = 0x7f122172

.field public static final skip_lock_screen_dialog_button_label:I = 0x7f122173

.field public static final skip_step:I = 0x7f122174

.field public static final slider_app_open:I = 0x7f122175

.field public static final slider_app_open_name:I = 0x7f122176

.field public static final slider_app_open_none:I = 0x7f122177

.field public static final slider_button_title:I = 0x7f122178

.field public static final slider_choose_app:I = 0x7f122179

.field public static final slider_game_page_code:I = 0x7f12217a

.field public static final slider_game_tool_summary:I = 0x7f12217b

.field public static final slider_game_tool_tilte:I = 0x7f12217c

.field public static final slider_global_summary_app:I = 0x7f12217d

.field public static final slider_global_summary_drawer:I = 0x7f12217e

.field public static final slider_global_summary_selfie:I = 0x7f12217f

.field public static final slider_global_title:I = 0x7f122180

.field public static final slider_high_grade_summary:I = 0x7f122181

.field public static final slider_high_grade_title:I = 0x7f122182

.field public static final slider_selfie:I = 0x7f122183

.field public static final slider_title:I = 0x7f122184

.field public static final slider_title_jianghu:I = 0x7f122185

.field public static final slider_title_jixie:I = 0x7f122186

.field public static final slider_title_keji:I = 0x7f122187

.field public static final slider_title_lingdong:I = 0x7f122188

.field public static final slider_title_zippo:I = 0x7f122189

.field public static final slider_tool_drawer:I = 0x7f12218a

.field public static final slider_tool_none:I = 0x7f12218b

.field public static final slider_tool_none_summary:I = 0x7f12218c

.field public static final slider_video_tool_summary:I = 0x7f12218d

.field public static final slider_video_tool_title:I = 0x7f12218e

.field public static final slider_vocie_click_too_often:I = 0x7f12218f

.field public static final slider_vocie_online_close:I = 0x7f122190

.field public static final slider_vocie_online_default:I = 0x7f122191

.field public static final slider_vocie_online_open:I = 0x7f122192

.field public static final slider_vocie_online_title:I = 0x7f122193

.field public static final slider_voice_online_theme:I = 0x7f122194

.field public static final slider_voice_summary:I = 0x7f122195

.field public static final slider_voice_title:I = 0x7f122196

.field public static final slight_pause:I = 0x7f122197

.field public static final slot_number:I = 0x7f122198

.field public static final slot_status_title:I = 0x7f122199

.field public static final smallwindow_smartcover_title:I = 0x7f12219a

.field public static final smart_battery_footer:I = 0x7f12219b

.field public static final smart_battery_manager_title:I = 0x7f12219c

.field public static final smart_battery_summary:I = 0x7f12219d

.field public static final smart_battery_title:I = 0x7f12219e

.field public static final smart_dual_sim_title:I = 0x7f12219f

.field public static final smart_forwarding_failed_not_activated_text:I = 0x7f1221a0

.field public static final smart_forwarding_failed_text:I = 0x7f1221a1

.field public static final smart_forwarding_failed_title:I = 0x7f1221a2

.field public static final smart_forwarding_input_mdn_dialog_title:I = 0x7f1221a3

.field public static final smart_forwarding_input_mdn_title:I = 0x7f1221a4

.field public static final smart_forwarding_missing_alert_dialog_text:I = 0x7f1221a5

.field public static final smart_forwarding_missing_mdn_text:I = 0x7f1221a6

.field public static final smart_forwarding_ongoing_text:I = 0x7f1221a7

.field public static final smart_forwarding_ongoing_title:I = 0x7f1221a8

.field public static final smart_forwarding_summary_disabled:I = 0x7f1221a9

.field public static final smart_forwarding_summary_enabled:I = 0x7f1221aa

.field public static final smart_forwarding_title:I = 0x7f1221ab

.field public static final smart_home_select_smart_device:I = 0x7f1221ac

.field public static final smart_home_title:I = 0x7f1221ad

.field public static final smart_home_value_none:I = 0x7f1221ae

.field public static final smart_home_value_unknown:I = 0x7f1221af

.field public static final smart_notifications_title:I = 0x7f1221b0

.field public static final smart_rotate_text_headline:I = 0x7f1221b1

.field public static final smartcover_lock_or_unlock_screen_tittle:I = 0x7f1221b2

.field public static final smartcover_sensitive_summary:I = 0x7f1221b3

.field public static final smartcover_sensitive_title:I = 0x7f1221b4

.field public static final smooth_adjust_ligth:I = 0x7f1221b5

.field public static final smooth_adjust_ligth_summary:I = 0x7f1221b6

.field public static final sms_application_title:I = 0x7f1221b7

.field public static final sms_change_default_dialog_text:I = 0x7f1221b8

.field public static final sms_change_default_dialog_title:I = 0x7f1221b9

.field public static final sms_change_default_no_previous_dialog_text:I = 0x7f1221ba

.field public static final sms_delivered_sound_dialog_title:I = 0x7f1221bb

.field public static final sms_delivered_sound_title:I = 0x7f1221bc

.field public static final sms_preference:I = 0x7f1221bd

.field public static final sms_preference_title:I = 0x7f1221be

.field public static final sms_received_sound_dialog_title:I = 0x7f1221bf

.field public static final sms_received_sound_title:I = 0x7f1221c0

.field public static final snooze_options_title:I = 0x7f1221c1

.field public static final softbank_apn_lock_toast:I = 0x7f1221c2

.field public static final software_version:I = 0x7f1221c3

.field public static final solar_term_autumn_begins:I = 0x7f1221c4

.field public static final solar_term_autumn_equinox:I = 0x7f1221c5

.field public static final solar_term_clear_and_bright:I = 0x7f1221c6

.field public static final solar_term_cold_dews:I = 0x7f1221c7

.field public static final solar_term_grain_buds:I = 0x7f1221c8

.field public static final solar_term_grain_in_ear:I = 0x7f1221c9

.field public static final solar_term_grain_rain:I = 0x7f1221ca

.field public static final solar_term_great_cold:I = 0x7f1221cb

.field public static final solar_term_great_heat:I = 0x7f1221cc

.field public static final solar_term_heavy_snow:I = 0x7f1221cd

.field public static final solar_term_hoar_frost_falls:I = 0x7f1221ce

.field public static final solar_term_insects_awaken:I = 0x7f1221cf

.field public static final solar_term_light_snow:I = 0x7f1221d0

.field public static final solar_term_slight_cold:I = 0x7f1221d1

.field public static final solar_term_slight_heat:I = 0x7f1221d2

.field public static final solar_term_spring_begins:I = 0x7f1221d3

.field public static final solar_term_stopping_the_heat:I = 0x7f1221d4

.field public static final solar_term_summer_begins:I = 0x7f1221d5

.field public static final solar_term_summer_solstice:I = 0x7f1221d6

.field public static final solar_term_the_rains:I = 0x7f1221d7

.field public static final solar_term_vernal_equinox:I = 0x7f1221d8

.field public static final solar_term_white_dews:I = 0x7f1221d9

.field public static final solar_term_winter_begins:I = 0x7f1221da

.field public static final solar_term_winter_solstice:I = 0x7f1221db

.field public static final solid_text:I = 0x7f1221dc

.field public static final sometimes_running:I = 0x7f1221dd

.field public static final sort_avg_use:I = 0x7f1221de

.field public static final sort_max_use:I = 0x7f1221df

.field public static final sort_order_alpha:I = 0x7f1221e0

.field public static final sort_order_frequent_notification:I = 0x7f1221e1

.field public static final sort_order_recent_notification:I = 0x7f1221e2

.field public static final sort_order_size:I = 0x7f1221e3

.field public static final sos_phone_call_privacy_dialog_message:I = 0x7f1221e4

.field public static final sos_phone_call_privacy_dialog_message_confirm:I = 0x7f1221e5

.field public static final sos_phone_log_privacy_dialog_message:I = 0x7f1221e6

.field public static final sos_privacy_and_content_policy:I = 0x7f1221e7

.field public static final sos_privacy_dialog_button_cancel:I = 0x7f1221e8

.field public static final sos_privacy_dialog_button_ok:I = 0x7f1221e9

.field public static final sos_privacy_dialog_message:I = 0x7f1221ea

.field public static final sos_privacy_dialog_message_new_version:I = 0x7f1221eb

.field public static final sos_privacy_dialog_message_old_version:I = 0x7f1221ec

.field public static final sos_privacy_dialog_title:I = 0x7f1221ed

.field public static final sos_privacy_policy:I = 0x7f1221ee

.field public static final sos_privacy_policy_change_endtitle:I = 0x7f1221ef

.field public static final sos_privacy_policy_change_message_reject:I = 0x7f1221f0

.field public static final sos_privacy_policy_change_subtitle:I = 0x7f1221f1

.field public static final sos_privacy_policy_change_title:I = 0x7f1221f2

.field public static final sos_privacy_policy_change_title_reject:I = 0x7f1221f3

.field public static final sos_privacy_policy_no_net_button:I = 0x7f1221f4

.field public static final sos_privacy_policy_no_net_message:I = 0x7f1221f5

.field public static final sos_privacy_policy_no_net_title:I = 0x7f1221f6

.field public static final sos_privacy_policy_revoke:I = 0x7f1221f7

.field public static final sos_privacy_policy_revoke_message_reject:I = 0x7f1221f8

.field public static final sos_privacy_policy_revoke_title_reject:I = 0x7f1221f9

.field public static final sos_privacy_revoke_failed_dialog_content:I = 0x7f1221fa

.field public static final sos_privacy_revoke_failed_dialog_title:I = 0x7f1221fb

.field public static final sound_assist_summary:I = 0x7f1221fc

.field public static final sound_assist_title:I = 0x7f1221fd

.field public static final sound_category_call_ringtone_vibrate_title:I = 0x7f1221fe

.field public static final sound_category_system_title:I = 0x7f1221ff

.field public static final sound_dashboard_summary:I = 0x7f122200

.field public static final sound_effects_enable_title:I = 0x7f122201

.field public static final sound_haptic_settings:I = 0x7f122202

.field public static final sound_mode:I = 0x7f122203

.field public static final sound_settings:I = 0x7f122204

.field public static final sound_settings_example_summary:I = 0x7f122205

.field public static final sound_settings_summary:I = 0x7f122206

.field public static final sound_settings_summary_silent:I = 0x7f122207

.field public static final sound_settings_summary_vibrate:I = 0x7f122208

.field public static final sound_settings_tab_haptic:I = 0x7f122209

.field public static final sound_settings_tab_sound:I = 0x7f12220a

.field public static final sound_stereo_mode_summary:I = 0x7f12220b

.field public static final sound_stereo_mode_title:I = 0x7f12220c

.field public static final sound_title:I = 0x7f12220d

.field public static final sound_vibrate_settings:I = 0x7f12220e

.field public static final sound_work_settings:I = 0x7f12220f

.field public static final source_info_details_title:I = 0x7f122210

.field public static final spatial_audio_footer_learn_more_text:I = 0x7f122211

.field public static final spatial_audio_footer_title:I = 0x7f122212

.field public static final spatial_audio_speaker:I = 0x7f122213

.field public static final spatial_audio_text:I = 0x7f122214

.field public static final spatial_audio_title:I = 0x7f122215

.field public static final spatial_audio_wired_headphones:I = 0x7f122216

.field public static final spatial_summary_off:I = 0x7f122217

.field public static final spatial_summary_on_one:I = 0x7f122218

.field public static final spatial_summary_on_two:I = 0x7f122219

.field public static final speaker:I = 0x7f12221a

.field public static final speaker_clean:I = 0x7f12221b

.field public static final special_access:I = 0x7f12221c

.field public static final special_access_more:I = 0x7f12221d

.field public static final special_preview:I = 0x7f12221e

.field public static final speech_category_title:I = 0x7f12221f

.field public static final speed_label_fast:I = 0x7f122220

.field public static final speed_label_medium:I = 0x7f122221

.field public static final speed_label_okay:I = 0x7f122222

.field public static final speed_label_slow:I = 0x7f122223

.field public static final speed_label_very_fast:I = 0x7f122224

.field public static final speed_label_very_slow:I = 0x7f122225

.field public static final speed_mode:I = 0x7f122226

.field public static final speed_mode_noti_summary:I = 0x7f122227

.field public static final speed_mode_noti_title:I = 0x7f122228

.field public static final speed_mode_summary:I = 0x7f122229

.field public static final spell_checker_not_selected:I = 0x7f12222a

.field public static final spell_checker_primary_switch_title:I = 0x7f12222b

.field public static final spellchecker_language:I = 0x7f12222c

.field public static final spellchecker_quick_settings:I = 0x7f12222d

.field public static final spellchecker_security_warning:I = 0x7f12222e

.field public static final spellchecker_switch_title:I = 0x7f12222f

.field public static final spellcheckers_settings_for_work_title:I = 0x7f122230

.field public static final spellcheckers_settings_title:I = 0x7f122231

.field public static final split_screen:I = 0x7f122232

.field public static final ssid_label:I = 0x7f122233

.field public static final ssl_ca_cert_info_message:I = 0x7f122234

.field public static final ssl_ca_cert_info_message_device_owner:I = 0x7f122235

.field public static final ssl_ca_cert_warning:I = 0x7f122236

.field public static final ssl_ca_cert_warning_message:I = 0x7f122237

.field public static final stable_build:I = 0x7f122238

.field public static final standard:I = 0x7f122239

.field public static final standard_info:I = 0x7f12223a

.field public static final standard_limit:I = 0x7f12223b

.field public static final standby_bucket_summary:I = 0x7f12223c

.field public static final start_silent_mode:I = 0x7f12223d

.field public static final status_awake_time:I = 0x7f12223e

.field public static final status_bar_app_des_separator:I = 0x7f12223f

.field public static final status_bar_app_disable_notification:I = 0x7f122240

.field public static final status_bar_app_enable_floating:I = 0x7f122241

.field public static final status_bar_app_enable_keyguard:I = 0x7f122242

.field public static final status_bar_app_enable_message:I = 0x7f122243

.field public static final status_bar_carrier_settings_no_sim_card:I = 0x7f122244

.field public static final status_bar_notification_info_overflow:I = 0x7f122245

.field public static final status_bar_settings:I = 0x7f122246

.field public static final status_bar_settings_all_app_display_loading:I = 0x7f122247

.field public static final status_bar_settings_carrier_sim:I = 0x7f122248

.field public static final status_bar_settings_manage_notification:I = 0x7f122249

.field public static final status_bar_settings_notification_bar:I = 0x7f12224a

.field public static final status_bar_settings_status_bar:I = 0x7f12224b

.field public static final status_bar_title:I = 0x7f12224c

.field public static final status_bt_address:I = 0x7f12224d

.field public static final status_data_network_type:I = 0x7f12224e

.field public static final status_data_state:I = 0x7f12224f

.field public static final status_device_wifi_mac_address:I = 0x7f122250

.field public static final status_esim_id:I = 0x7f122251

.field public static final status_icc_id:I = 0x7f122252

.field public static final status_iccid:I = 0x7f122253

.field public static final status_imei:I = 0x7f122254

.field public static final status_imei_sv:I = 0x7f122255

.field public static final status_latest_area_info:I = 0x7f122256

.field public static final status_meid_number:I = 0x7f122257

.field public static final status_min_number:I = 0x7f122258

.field public static final status_msid_number:I = 0x7f122259

.field public static final status_number:I = 0x7f12225a

.field public static final status_number_sim_slot:I = 0x7f12225b

.field public static final status_number_sim_status:I = 0x7f12225c

.field public static final status_operator:I = 0x7f12225d

.field public static final status_prl_version:I = 0x7f12225e

.field public static final status_roaming:I = 0x7f12225f

.field public static final status_serial_number:I = 0x7f122260

.field public static final status_serialno:I = 0x7f122261

.field public static final status_service_state:I = 0x7f122262

.field public static final status_signal_strength:I = 0x7f122263

.field public static final status_sim:I = 0x7f122264

.field public static final status_sim_1:I = 0x7f122265

.field public static final status_sim_2:I = 0x7f122266

.field public static final status_summary:I = 0x7f122267

.field public static final status_unavailable:I = 0x7f122268

.field public static final status_up_time:I = 0x7f122269

.field public static final status_voice_network_type:I = 0x7f12226a

.field public static final status_wifi_mac_address:I = 0x7f12226b

.field public static final storage_apps:I = 0x7f12226c

.field public static final storage_audio:I = 0x7f12226d

.field public static final storage_category:I = 0x7f12226e

.field public static final storage_default_internal_storage:I = 0x7f12226f

.field public static final storage_detail_dialog_system:I = 0x7f122270

.field public static final storage_dialog_unmountable:I = 0x7f122271

.field public static final storage_dialog_unmounted:I = 0x7f122272

.field public static final storage_documents_and_other:I = 0x7f122273

.field public static final storage_external_title:I = 0x7f122274

.field public static final storage_files:I = 0x7f122275

.field public static final storage_format:I = 0x7f122276

.field public static final storage_format_failure:I = 0x7f122277

.field public static final storage_format_success:I = 0x7f122278

.field public static final storage_free_up_space_summary:I = 0x7f122279

.field public static final storage_free_up_space_title:I = 0x7f12227a

.field public static final storage_games:I = 0x7f12227b

.field public static final storage_images:I = 0x7f12227c

.field public static final storage_internal_forget_confirm:I = 0x7f12227d

.field public static final storage_internal_forget_confirm_title:I = 0x7f12227e

.field public static final storage_internal_forget_details:I = 0x7f12227f

.field public static final storage_internal_format_details:I = 0x7f122280

.field public static final storage_internal_title:I = 0x7f122281

.field public static final storage_internal_unmount_details:I = 0x7f122282

.field public static final storage_label:I = 0x7f122283

.field public static final storage_low_summary:I = 0x7f122284

.field public static final storage_low_title:I = 0x7f122285

.field public static final storage_manager_indicator:I = 0x7f122286

.field public static final storage_manager_indicator_off:I = 0x7f122287

.field public static final storage_manager_indicator_on:I = 0x7f122288

.field public static final storage_menu_forget:I = 0x7f122289

.field public static final storage_menu_format:I = 0x7f12228a

.field public static final storage_menu_format_private:I = 0x7f12228b

.field public static final storage_menu_format_public:I = 0x7f12228c

.field public static final storage_menu_free:I = 0x7f12228d

.field public static final storage_menu_manage:I = 0x7f12228e

.field public static final storage_menu_migrate:I = 0x7f12228f

.field public static final storage_menu_mount:I = 0x7f122290

.field public static final storage_menu_rename:I = 0x7f122291

.field public static final storage_menu_set_up:I = 0x7f122292

.field public static final storage_menu_unmount:I = 0x7f122293

.field public static final storage_mount_failure:I = 0x7f122294

.field public static final storage_mount_success:I = 0x7f122295

.field public static final storage_movies_tv:I = 0x7f122296

.field public static final storage_music_audio:I = 0x7f122297

.field public static final storage_other_apps:I = 0x7f122298

.field public static final storage_other_users:I = 0x7f122299

.field public static final storage_percent_full:I = 0x7f12229a

.field public static final storage_photos_videos:I = 0x7f12229b

.field public static final storage_rename_title:I = 0x7f12229c

.field public static final storage_settings:I = 0x7f12229d

.field public static final storage_settings_for_app:I = 0x7f12229e

.field public static final storage_settings_summary:I = 0x7f12229f

.field public static final storage_settings_title:I = 0x7f1222a0

.field public static final storage_size_large:I = 0x7f1222a1

.field public static final storage_size_large_alternate:I = 0x7f1222a2

.field public static final storage_summary:I = 0x7f1222a3

.field public static final storage_summary_format:I = 0x7f1222a4

.field public static final storage_summary_with_sdcard:I = 0x7f1222a5

.field public static final storage_system:I = 0x7f1222a6

.field public static final storage_title_usb:I = 0x7f1222a7

.field public static final storage_total_summary:I = 0x7f1222a8

.field public static final storage_trash:I = 0x7f1222a9

.field public static final storage_trash_dialog_ask_message:I = 0x7f1222aa

.field public static final storage_trash_dialog_confirm:I = 0x7f1222ab

.field public static final storage_trash_dialog_empty_message:I = 0x7f1222ac

.field public static final storage_trash_dialog_title:I = 0x7f1222ad

.field public static final storage_type_external:I = 0x7f1222ae

.field public static final storage_type_internal:I = 0x7f1222af

.field public static final storage_unmount_failure:I = 0x7f1222b0

.field public static final storage_unmount_success:I = 0x7f1222b1

.field public static final storage_usage_summary:I = 0x7f1222b2

.field public static final storage_usb:I = 0x7f1222b3

.field public static final storage_usb_settings:I = 0x7f1222b4

.field public static final storage_use_title:I = 0x7f1222b5

.field public static final storage_used:I = 0x7f1222b6

.field public static final storage_videos:I = 0x7f1222b7

.field public static final storage_volume_total:I = 0x7f1222b8

.field public static final storage_wizard_format_confirm_body:I = 0x7f1222b9

.field public static final storage_wizard_format_confirm_next:I = 0x7f1222ba

.field public static final storage_wizard_format_confirm_public_body:I = 0x7f1222bb

.field public static final storage_wizard_format_confirm_public_title:I = 0x7f1222bc

.field public static final storage_wizard_format_confirm_third_body:I = 0x7f1222bd

.field public static final storage_wizard_format_confirm_title:I = 0x7f1222be

.field public static final storage_wizard_format_confirm_v2_action:I = 0x7f1222bf

.field public static final storage_wizard_format_confirm_v2_body:I = 0x7f1222c0

.field public static final storage_wizard_format_confirm_v2_title:I = 0x7f1222c1

.field public static final storage_wizard_format_progress_body:I = 0x7f1222c2

.field public static final storage_wizard_format_progress_title:I = 0x7f1222c3

.field public static final storage_wizard_init_external_summary:I = 0x7f1222c4

.field public static final storage_wizard_init_external_title:I = 0x7f1222c5

.field public static final storage_wizard_init_internal_summary:I = 0x7f1222c6

.field public static final storage_wizard_init_internal_title:I = 0x7f1222c7

.field public static final storage_wizard_init_title:I = 0x7f1222c8

.field public static final storage_wizard_init_v2_external_action:I = 0x7f1222c9

.field public static final storage_wizard_init_v2_external_summary:I = 0x7f1222ca

.field public static final storage_wizard_init_v2_external_title:I = 0x7f1222cb

.field public static final storage_wizard_init_v2_internal_action:I = 0x7f1222cc

.field public static final storage_wizard_init_v2_internal_summary:I = 0x7f1222cd

.field public static final storage_wizard_init_v2_internal_title:I = 0x7f1222ce

.field public static final storage_wizard_init_v2_later:I = 0x7f1222cf

.field public static final storage_wizard_init_v2_or:I = 0x7f1222d0

.field public static final storage_wizard_init_v2_title:I = 0x7f1222d1

.field public static final storage_wizard_migrate_body:I = 0x7f1222d2

.field public static final storage_wizard_migrate_confirm_body:I = 0x7f1222d3

.field public static final storage_wizard_migrate_confirm_next:I = 0x7f1222d4

.field public static final storage_wizard_migrate_confirm_title:I = 0x7f1222d5

.field public static final storage_wizard_migrate_details:I = 0x7f1222d6

.field public static final storage_wizard_migrate_later:I = 0x7f1222d7

.field public static final storage_wizard_migrate_now:I = 0x7f1222d8

.field public static final storage_wizard_migrate_progress_title:I = 0x7f1222d9

.field public static final storage_wizard_migrate_progress_v2_title:I = 0x7f1222da

.field public static final storage_wizard_migrate_title:I = 0x7f1222db

.field public static final storage_wizard_migrate_v2_body:I = 0x7f1222dc

.field public static final storage_wizard_migrate_v2_checklist:I = 0x7f1222dd

.field public static final storage_wizard_migrate_v2_checklist_apps:I = 0x7f1222de

.field public static final storage_wizard_migrate_v2_checklist_battery:I = 0x7f1222df

.field public static final storage_wizard_migrate_v2_checklist_media:I = 0x7f1222e0

.field public static final storage_wizard_migrate_v2_later:I = 0x7f1222e1

.field public static final storage_wizard_migrate_v2_now:I = 0x7f1222e2

.field public static final storage_wizard_migrate_v2_title:I = 0x7f1222e3

.field public static final storage_wizard_move_confirm_body:I = 0x7f1222e4

.field public static final storage_wizard_move_confirm_title:I = 0x7f1222e5

.field public static final storage_wizard_move_progress_body:I = 0x7f1222e6

.field public static final storage_wizard_move_progress_cancel:I = 0x7f1222e7

.field public static final storage_wizard_move_progress_title:I = 0x7f1222e8

.field public static final storage_wizard_move_unlock:I = 0x7f1222e9

.field public static final storage_wizard_ready_external_body:I = 0x7f1222ea

.field public static final storage_wizard_ready_internal_body:I = 0x7f1222eb

.field public static final storage_wizard_ready_title:I = 0x7f1222ec

.field public static final storage_wizard_ready_v2_external_body:I = 0x7f1222ed

.field public static final storage_wizard_ready_v2_internal_body:I = 0x7f1222ee

.field public static final storage_wizard_ready_v2_internal_moved_body:I = 0x7f1222ef

.field public static final storage_wizard_slow_body:I = 0x7f1222f0

.field public static final storage_wizard_slow_v2_body:I = 0x7f1222f1

.field public static final storage_wizard_slow_v2_continue:I = 0x7f1222f2

.field public static final storage_wizard_slow_v2_start_over:I = 0x7f1222f3

.field public static final storage_wizard_slow_v2_title:I = 0x7f1222f4

.field public static final storageuse_settings_summary:I = 0x7f1222f5

.field public static final storageuse_settings_title:I = 0x7f1222f6

.field public static final strict_mode:I = 0x7f1222f7

.field public static final strict_mode_summary:I = 0x7f1222f8

.field public static final string_KB:I = 0x7f1222f9

.field public static final string_KB_per_log_buffer:I = 0x7f1222fa

.field public static final string_MB:I = 0x7f1222fb

.field public static final string_MB_per_log_buffer:I = 0x7f1222fc

.field public static final string_after_int_seconds:I = 0x7f1222fd

.field public static final string_always:I = 0x7f1222fe

.field public static final string_bluetooth_a2dp_codec_lhdc_latency_titles_1:I = 0x7f1222ff

.field public static final string_bluetooth_a2dp_codec_lhdc_latency_titles_2:I = 0x7f122300

.field public static final string_bluetooth_a2dp_codec_lhdc_latency_titles_3:I = 0x7f122301

.field public static final string_bluetooth_a2dp_codec_lhdc_playback_quality_titles_1:I = 0x7f122302

.field public static final string_bluetooth_a2dp_codec_lhdc_playback_quality_titles_2:I = 0x7f122303

.field public static final string_bluetooth_a2dp_codec_lhdc_playback_quality_titles_3:I = 0x7f122304

.field public static final string_bluetooth_a2dp_codec_lhdc_playback_quality_titles_4:I = 0x7f122305

.field public static final string_bluetooth_a2dp_codec_lhdc_playback_quality_titles_5:I = 0x7f122306

.field public static final string_float_seconds:I = 0x7f122307

.field public static final string_immediately:I = 0x7f122308

.field public static final string_kHz_1:I = 0x7f122309

.field public static final string_kHz_2:I = 0x7f12230a

.field public static final string_kHz_3:I = 0x7f12230b

.field public static final string_kHz_4:I = 0x7f12230c

.field public static final string_never:I = 0x7f12230d

.field public static final string_off:I = 0x7f12230e

.field public static final string_offed:I = 0x7f12230f

.field public static final string_unlimited:I = 0x7f122310

.field public static final strong_suppression_summary:I = 0x7f122311

.field public static final strong_suppression_title:I = 0x7f122312

.field public static final structure_add_facerecoginition_text:I = 0x7f122313

.field public static final structure_face_data_delete_fail:I = 0x7f122314

.field public static final structure_face_data_input_down:I = 0x7f122315

.field public static final structure_face_data_input_error_away_screen:I = 0x7f122316

.field public static final structure_face_data_input_error_expose_features:I = 0x7f122317

.field public static final structure_face_data_input_error_keep_inface:I = 0x7f122318

.field public static final structure_face_data_input_error_overly_light:I = 0x7f122319

.field public static final structure_face_data_input_left:I = 0x7f12231a

.field public static final structure_face_data_input_right:I = 0x7f12231b

.field public static final structure_face_data_input_timeout_msg:I = 0x7f12231c

.field public static final structure_face_data_input_timeout_title:I = 0x7f12231d

.field public static final structure_face_data_input_up:I = 0x7f12231e

.field public static final structure_face_data_introduction_back:I = 0x7f12231f

.field public static final structure_face_data_introduction_msg:I = 0x7f122320

.field public static final structure_face_data_introduction_title:I = 0x7f122321

.field public static final structure_face_data_ir_input_face:I = 0x7f122322

.field public static final structure_face_data_manage_delete_msg:I = 0x7f122323

.field public static final structure_face_data_suggestion_text:I = 0x7f122324

.field public static final structure_face_data_suggestion_warning_text:I = 0x7f122325

.field public static final structure_face_delete_data_dialog_msg:I = 0x7f122326

.field public static final structure_face_delete_data_dialog_msg_retry:I = 0x7f122327

.field public static final structure_face_delete_data_dialog_title:I = 0x7f122328

.field public static final structure_face_enroll_not_able_low_electric_itle:I = 0x7f122329

.field public static final structure_face_enroll_not_able_low_electric_msg:I = 0x7f12232a

.field public static final structure_face_hardware_abnormal_btn:I = 0x7f12232b

.field public static final structure_face_hardware_abnormal_msg:I = 0x7f12232c

.field public static final structure_face_hardware_abnormal_title:I = 0x7f12232d

.field public static final structure_face_modules_abnormal_btn:I = 0x7f12232e

.field public static final structure_face_modules_abnormal_msg:I = 0x7f12232f

.field public static final structure_face_modules_abnormal_title:I = 0x7f122330

.field public static final structure_face_unlock_model2d_dialog_msg:I = 0x7f122331

.field public static final structure_face_unlock_model2d_dialog_title:I = 0x7f122332

.field public static final structure_face_unlock_model3d_dialog_msg:I = 0x7f122333

.field public static final structure_face_unlock_model3d_dialog_title:I = 0x7f122334

.field public static final structure_face_unlock_model_dialog_enable:I = 0x7f122335

.field public static final structure_face_unlock_model_dialog_notenable:I = 0x7f122336

.field public static final structure_face_unlock_model_other:I = 0x7f122337

.field public static final structure_face_unlock_model_other_msg_2d:I = 0x7f122338

.field public static final structure_face_unlock_model_other_msg_3d:I = 0x7f122339

.field public static final structure_face_unlock_model_other_suggestion_2d:I = 0x7f12233a

.field public static final structure_face_unlock_model_other_suggestion_3d:I = 0x7f12233b

.field public static final structure_face_unlock_model_other_title:I = 0x7f12233c

.field public static final structure_face_unlock_not_same_person:I = 0x7f12233d

.field public static final structure_face_unlock_tof_covered:I = 0x7f12233e

.field public static final structure_manage_facerecoginition_text:I = 0x7f12233f

.field public static final style_and_wallpaper_settings_title:I = 0x7f122340

.field public static final style_suggestion_summary:I = 0x7f122341

.field public static final style_suggestion_title:I = 0x7f122342

.field public static final stylus_btn_continue:I = 0x7f122343

.field public static final stylus_btn_learn:I = 0x7f122344

.field public static final stylus_btn_quick_write:I = 0x7f122345

.field public static final stylus_btn_screen_shot:I = 0x7f122346

.field public static final stylus_btn_skip:I = 0x7f122347

.field public static final stylus_connect:I = 0x7f122348

.field public static final stylus_connect_fail:I = 0x7f122349

.field public static final stylus_connect_fail_no_battery:I = 0x7f12234a

.field public static final stylus_connect_fail_no_reason:I = 0x7f12234b

.field public static final stylus_connect_fail_not_open_gps:I = 0x7f12234c

.field public static final stylus_connecting:I = 0x7f12234d

.field public static final stylus_default_percent:I = 0x7f12234e

.field public static final stylus_function_and_action:I = 0x7f12234f

.field public static final stylus_handwriting:I = 0x7f122350

.field public static final stylus_handwriting_summary:I = 0x7f122351

.field public static final stylus_keyboard_settings_title:I = 0x7f122352

.field public static final stylus_not_connect_message:I = 0x7f122353

.field public static final stylus_ota_check_failed_battery:I = 0x7f122354

.field public static final stylus_ota_check_failed_info:I = 0x7f122355

.field public static final stylus_ota_check_update:I = 0x7f122356

.field public static final stylus_ota_completed:I = 0x7f122357

.field public static final stylus_ota_completed_tip:I = 0x7f122358

.field public static final stylus_ota_current_firmware_version:I = 0x7f122359

.field public static final stylus_ota_demands:I = 0x7f12235a

.field public static final stylus_ota_download_failed:I = 0x7f12235b

.field public static final stylus_ota_downloading_firmware:I = 0x7f12235c

.field public static final stylus_ota_firmware_info:I = 0x7f12235d

.field public static final stylus_ota_firmware_update:I = 0x7f12235e

.field public static final stylus_ota_hardware_version:I = 0x7f12235f

.field public static final stylus_ota_hava_new_version:I = 0x7f122360

.field public static final stylus_ota_latest:I = 0x7f122361

.field public static final stylus_ota_model:I = 0x7f122362

.field public static final stylus_ota_net_error:I = 0x7f122363

.field public static final stylus_ota_new_version:I = 0x7f122364

.field public static final stylus_ota_serial_number:I = 0x7f122365

.field public static final stylus_ota_timeout_failed:I = 0x7f122366

.field public static final stylus_ota_update:I = 0x7f122367

.field public static final stylus_ota_update_failed:I = 0x7f122368

.field public static final stylus_ota_updating:I = 0x7f122369

.field public static final stylus_primary_key:I = 0x7f12236a

.field public static final stylus_quick_write:I = 0x7f12236b

.field public static final stylus_quick_write_summary:I = 0x7f12236c

.field public static final stylus_schematic_diagram:I = 0x7f12236d

.field public static final stylus_screen_shot:I = 0x7f12236e

.field public static final stylus_screen_shot_summary:I = 0x7f12236f

.field public static final stylus_settings_title:I = 0x7f122370

.field public static final stylus_shortcut_actions_summary:I = 0x7f122371

.field public static final stylus_shortcut_actions_title:I = 0x7f122372

.field public static final stylus_shortcut_title_note:I = 0x7f122373

.field public static final stylus_shortcut_title_screenshot:I = 0x7f122374

.field public static final stylus_success:I = 0x7f122375

.field public static final stylus_vice_key:I = 0x7f122376

.field public static final stylus_welcome:I = 0x7f122377

.field public static final subscreen_title:I = 0x7f122378

.field public static final subscription_available:I = 0x7f122379

.field public static final sudFontSecondary:I = 0x7f12237a

.field public static final sudFontSecondaryMedium:I = 0x7f12237b

.field public static final sudFontSecondaryMediumMaterialYou:I = 0x7f12237c

.field public static final sudFontSecondaryText:I = 0x7f12237d

.field public static final sud_back_button_label:I = 0x7f12237e

.field public static final sud_more_button_label:I = 0x7f12237f

.field public static final sud_next_button_label:I = 0x7f122380

.field public static final suggest_unrestrict_max_aspect_ratio_title:I = 0x7f122381

.field public static final suggested_app_locales_title:I = 0x7f122382

.field public static final suggested_fingerprint_lock_settings_summary:I = 0x7f122383

.field public static final suggested_fingerprint_lock_settings_title:I = 0x7f122384

.field public static final suggested_lock_settings_summary:I = 0x7f122385

.field public static final suggested_lock_settings_title:I = 0x7f122386

.field public static final suggestion_additional_fingerprints:I = 0x7f122387

.field public static final suggestion_additional_fingerprints_summary:I = 0x7f122388

.field public static final suggestion_button_close:I = 0x7f122389

.field public static final suggestion_button_text:I = 0x7f12238a

.field public static final suggestion_for_phone_recycled:I = 0x7f12238b

.field public static final suggestion_remove:I = 0x7f12238c

.field public static final suggestions_more_title:I = 0x7f12238d

.field public static final suggestions_summary:I = 0x7f12238e

.field public static final suggestions_title:I = 0x7f12238f

.field public static final suggestions_title_v2:I = 0x7f122390

.field public static final sum_carrier_select:I = 0x7f122391

.field public static final summary_collapsed_preference_list:I = 0x7f122392

.field public static final summary_divider_text:I = 0x7f122393

.field public static final summary_empty:I = 0x7f122394

.field public static final summary_font_size:I = 0x7f122395

.field public static final summary_placeholder:I = 0x7f122396

.field public static final summary_range_symbol_combination:I = 0x7f122397

.field public static final summary_range_verbal_combination:I = 0x7f122398

.field public static final summary_two_lines_placeholder:I = 0x7f122399

.field public static final sunday:I = 0x7f12239a

.field public static final sunday_short:I = 0x7f12239b

.field public static final sunday_shortest:I = 0x7f12239c

.field public static final sunlight_low_power_notification:I = 0x7f12239d

.field public static final sunlight_mode:I = 0x7f12239e

.field public static final sunlight_mode_summary:I = 0x7f12239f

.field public static final supl_not_set:I = 0x7f1223a0

.field public static final supplicant_state_label:I = 0x7f1223a1

.field public static final support_summary:I = 0x7f1223a2

.field public static final sure_sync_now:I = 0x7f1223a3

.field public static final surround_sound_all_desc:I = 0x7f1223a4

.field public static final surround_sound_only_headset_desc:I = 0x7f1223a5

.field public static final surround_sound_only_speaker_desc:I = 0x7f1223a6

.field public static final surround_sound_title:I = 0x7f1223a7

.field public static final swipe_direction_ltr:I = 0x7f1223a8

.field public static final swipe_direction_rtl:I = 0x7f1223a9

.field public static final swipe_direction_title:I = 0x7f1223aa

.field public static final swipe_up_to_continue_using:I = 0x7f1223ab

.field public static final swipe_up_to_continue_using_summary:I = 0x7f1223ac

.field public static final swipe_up_to_switch_apps_suggestion_summary:I = 0x7f1223ad

.field public static final swipe_up_to_switch_apps_suggestion_title:I = 0x7f1223ae

.field public static final swipe_up_to_switch_apps_summary:I = 0x7f1223af

.field public static final swipe_up_to_switch_apps_title:I = 0x7f1223b0

.field public static final switchNetworkToast:I = 0x7f1223b1

.field public static final switch_access_service_on:I = 0x7f1223b2

.field public static final switch_bootup_sound:I = 0x7f1223b3

.field public static final switch_headset_anti_lost:I = 0x7f1223b4

.field public static final switch_headset_anti_lost_title:I = 0x7f1223b5

.field public static final switch_off_text:I = 0x7f1223b6

.field public static final switch_on_text:I = 0x7f1223b7

.field public static final switch_power_sound:I = 0x7f1223b8

.field public static final switch_screen_button_order:I = 0x7f1223b9

.field public static final switch_screen_button_order_desc:I = 0x7f1223ba

.field public static final switch_select_to_speak_on:I = 0x7f1223bb

.field public static final switch_sim_dialog_no_switch_text:I = 0x7f1223bc

.field public static final switch_sim_dialog_no_switch_title:I = 0x7f1223bd

.field public static final switch_sim_dialog_text:I = 0x7f1223be

.field public static final switch_sim_dialog_title:I = 0x7f1223bf

.field public static final switch_to_removable_notification:I = 0x7f1223c0

.field public static final switch_to_removable_notification_no_carrier_name:I = 0x7f1223c1

.field public static final switch_to_user_zero_when_docked:I = 0x7f1223c2

.field public static final symbol_fast_input:I = 0x7f1223c3

.field public static final symbol_fast_input_summary:I = 0x7f1223c4

.field public static final symbols_not_allowed_for_password:I = 0x7f1223c5

.field public static final sync_active:I = 0x7f1223c6

.field public static final sync_automatically:I = 0x7f1223c7

.field public static final sync_calendar:I = 0x7f1223c8

.field public static final sync_contacts:I = 0x7f1223c9

.field public static final sync_disabled:I = 0x7f1223ca

.field public static final sync_enable:I = 0x7f1223cb

.field public static final sync_enabled:I = 0x7f1223cc

.field public static final sync_error:I = 0x7f1223cd

.field public static final sync_failed:I = 0x7f1223ce

.field public static final sync_gmail:I = 0x7f1223cf

.field public static final sync_in_progress:I = 0x7f1223d0

.field public static final sync_is_failing:I = 0x7f1223d1

.field public static final sync_menu_sync_cancel:I = 0x7f1223d2

.field public static final sync_menu_sync_now:I = 0x7f1223d3

.field public static final sync_now:I = 0x7f1223d4

.field public static final sync_one_time_sync:I = 0x7f1223d5

.field public static final sync_plug:I = 0x7f1223d6

.field public static final sync_settings:I = 0x7f1223d7

.field public static final sync_success:I = 0x7f1223d8

.field public static final sync_wifi_label:I = 0x7f1223d9

.field public static final sync_wifi_only:I = 0x7f1223da

.field public static final system:I = 0x7f1223db

.field public static final system_alert_window_access_title:I = 0x7f1223dc

.field public static final system_alert_window_apps_title:I = 0x7f1223dd

.field public static final system_alert_window_settings:I = 0x7f1223de

.field public static final system_alert_window_summary:I = 0x7f1223df

.field public static final system_and_device_section_title:I = 0x7f1223e0

.field public static final system_app:I = 0x7f1223e1

.field public static final system_app_settings:I = 0x7f1223e2

.field public static final system_apps_updater:I = 0x7f1223e3

.field public static final system_backup_section_title:I = 0x7f1223e4

.field public static final system_backup_section_title_new:I = 0x7f1223e5

.field public static final system_dashboard_summary:I = 0x7f1223e6

.field public static final system_default_app:I = 0x7f1223e7

.field public static final system_default_app_subtext:I = 0x7f1223e8

.field public static final system_error_info:I = 0x7f1223e9

.field public static final system_haptic_feedback:I = 0x7f1223ea

.field public static final system_navigation_title:I = 0x7f1223eb

.field public static final system_package:I = 0x7f1223ec

.field public static final system_print_service_help:I = 0x7f1223ed

.field public static final system_shortcut_wake:I = 0x7f1223ee

.field public static final system_storage:I = 0x7f1223ef

.field public static final system_touch:I = 0x7f1223f0

.field public static final system_ui_settings:I = 0x7f1223f1

.field public static final system_update_settings_list_item_summary:I = 0x7f1223f2

.field public static final system_update_settings_list_item_title:I = 0x7f1223f3

.field public static final tab_text:I = 0x7f1223f4

.field public static final tablet_screen_application_adaptation_title:I = 0x7f1223f5

.field public static final tablet_screen_settings_title:I = 0x7f1223f6

.field public static final tablet_screen_special_function_title:I = 0x7f1223f7

.field public static final take_call_on_title:I = 0x7f1223f8

.field public static final talkback_summary:I = 0x7f1223f9

.field public static final talkback_title:I = 0x7f1223fa

.field public static final talkback_watermark_enable:I = 0x7f1223fb

.field public static final tap_a_network_to_connect:I = 0x7f1223fc

.field public static final tap_to_sign_up:I = 0x7f1223fd

.field public static final tap_to_wake:I = 0x7f1223fe

.field public static final tap_to_wake_summary:I = 0x7f1223ff

.field public static final taplus_title:I = 0x7f122400

.field public static final tare_actions_base_price:I = 0x7f122401

.field public static final tare_actions_ctp:I = 0x7f122402

.field public static final tare_alarm_clock:I = 0x7f122403

.field public static final tare_alarmmanager:I = 0x7f122404

.field public static final tare_balances:I = 0x7f122405

.field public static final tare_consumption_limits:I = 0x7f122406

.field public static final tare_dialog_confirm_button_title:I = 0x7f122407

.field public static final tare_hard_consumption_limit:I = 0x7f122408

.field public static final tare_initial_consumption_limit:I = 0x7f122409

.field public static final tare_job_default_running:I = 0x7f12240a

.field public static final tare_job_default_start:I = 0x7f12240b

.field public static final tare_job_high_running:I = 0x7f12240c

.field public static final tare_job_high_start:I = 0x7f12240d

.field public static final tare_job_low_running:I = 0x7f12240e

.field public static final tare_job_low_start:I = 0x7f12240f

.field public static final tare_job_max_running:I = 0x7f122410

.field public static final tare_job_max_start:I = 0x7f122411

.field public static final tare_job_min_running:I = 0x7f122412

.field public static final tare_job_min_start:I = 0x7f122413

.field public static final tare_job_timeout_penalty:I = 0x7f122414

.field public static final tare_jobscheduler:I = 0x7f122415

.field public static final tare_max_satiated_balance:I = 0x7f122416

.field public static final tare_min_balance_exempted:I = 0x7f122417

.field public static final tare_min_balance_headless_app:I = 0x7f122418

.field public static final tare_min_balance_other_app:I = 0x7f122419

.field public static final tare_modifiers:I = 0x7f12241a

.field public static final tare_nonwakeup_exact:I = 0x7f12241b

.field public static final tare_nonwakeup_exact_idle:I = 0x7f12241c

.field public static final tare_nonwakeup_inexact:I = 0x7f12241d

.field public static final tare_nonwakeup_inexact_idle:I = 0x7f12241e

.field public static final tare_notification_interaction:I = 0x7f12241f

.field public static final tare_notification_seen:I = 0x7f122420

.field public static final tare_notification_seen_15_min:I = 0x7f122421

.field public static final tare_off:I = 0x7f122422

.field public static final tare_on:I = 0x7f122423

.field public static final tare_other_interaction:I = 0x7f122424

.field public static final tare_revert:I = 0x7f122425

.field public static final tare_rewards_instantaneous:I = 0x7f122426

.field public static final tare_rewards_max:I = 0x7f122427

.field public static final tare_rewards_ongoing:I = 0x7f122428

.field public static final tare_settings:I = 0x7f122429

.field public static final tare_settings_reverted_toast:I = 0x7f12242a

.field public static final tare_title:I = 0x7f12242b

.field public static final tare_top_activity:I = 0x7f12242c

.field public static final tare_wakeup_exact:I = 0x7f12242d

.field public static final tare_wakeup_exact_idle:I = 0x7f12242e

.field public static final tare_wakeup_inexact:I = 0x7f12242f

.field public static final tare_wakeup_inexact_idle:I = 0x7f122430

.field public static final tare_widget_interaction:I = 0x7f122431

.field public static final temporary_not_available:I = 0x7f122432

.field public static final terms_title:I = 0x7f122433

.field public static final testing:I = 0x7f122434

.field public static final testing_phone_info:I = 0x7f122435

.field public static final testing_sim_toolkit:I = 0x7f122436

.field public static final testing_usage_stats:I = 0x7f122437

.field public static final testing_wifi_info:I = 0x7f122438

.field public static final tether_and_hotspot_title:I = 0x7f122439

.field public static final tether_auto_disable_summary:I = 0x7f12243a

.field public static final tether_auto_disable_title:I = 0x7f12243b

.field public static final tether_custom_dialog_title:I = 0x7f12243c

.field public static final tether_data_usage_limit:I = 0x7f12243d

.field public static final tether_data_usage_limit_summary:I = 0x7f12243e

.field public static final tether_devices_management_title:I = 0x7f12243f

.field public static final tether_devices_max_number:I = 0x7f122440

.field public static final tether_limit_dialog_title:I = 0x7f122441

.field public static final tether_limit_value_custom:I = 0x7f122442

.field public static final tether_over_limit_dialog_cancle_btn:I = 0x7f122443

.field public static final tether_over_limit_dialog_message:I = 0x7f122444

.field public static final tether_over_limit_dialog_title:I = 0x7f122445

.field public static final tether_over_limit_warning_notify_message:I = 0x7f122446

.field public static final tether_over_limit_warning_notify_title:I = 0x7f122447

.field public static final tether_password_illegal_character:I = 0x7f122448

.field public static final tether_settings_disabled:I = 0x7f122449

.field public static final tether_settings_disabled_on_data_saver:I = 0x7f12244a

.field public static final tether_settings_summary_all:I = 0x7f12244b

.field public static final tether_settings_summary_bluetooth_and_ethernet:I = 0x7f12244c

.field public static final tether_settings_summary_bluetooth_tethering_only:I = 0x7f12244d

.field public static final tether_settings_summary_ethernet_tethering_only:I = 0x7f12244e

.field public static final tether_settings_summary_hotspot_and_bluetooth:I = 0x7f12244f

.field public static final tether_settings_summary_hotspot_and_bluetooth_and_ethernet:I = 0x7f122450

.field public static final tether_settings_summary_hotspot_and_ethernet:I = 0x7f122451

.field public static final tether_settings_summary_hotspot_and_usb:I = 0x7f122452

.field public static final tether_settings_summary_hotspot_and_usb_and_bluetooth:I = 0x7f122453

.field public static final tether_settings_summary_hotspot_and_usb_and_ethernet:I = 0x7f122454

.field public static final tether_settings_summary_hotspot_off_tether_on:I = 0x7f122455

.field public static final tether_settings_summary_hotspot_on_tether_off:I = 0x7f122456

.field public static final tether_settings_summary_hotspot_on_tether_on:I = 0x7f122457

.field public static final tether_settings_summary_hotspot_only:I = 0x7f122458

.field public static final tether_settings_summary_off:I = 0x7f122459

.field public static final tether_settings_summary_usb_and_bluetooth:I = 0x7f12245a

.field public static final tether_settings_summary_usb_and_bluetooth_and_ethernet:I = 0x7f12245b

.field public static final tether_settings_summary_usb_and_ethernet:I = 0x7f12245c

.field public static final tether_settings_summary_usb_tethering_only:I = 0x7f12245d

.field public static final tether_settings_title_all:I = 0x7f12245e

.field public static final tether_settings_title_bluetooth:I = 0x7f12245f

.field public static final tether_settings_title_usb:I = 0x7f122460

.field public static final tether_settings_title_usb_bluetooth:I = 0x7f122461

.field public static final tether_settings_title_wifi:I = 0x7f122462

.field public static final tether_share_qrcode_title:I = 0x7f122463

.field public static final tether_use_wifi6_summary:I = 0x7f122464

.field public static final tether_use_wifi6_title:I = 0x7f122465

.field public static final tethering_footer_info:I = 0x7f122466

.field public static final tethering_footer_info_sta_ap_concurrency:I = 0x7f122467

.field public static final tethering_hardware_offload:I = 0x7f122468

.field public static final tethering_hardware_offload_summary:I = 0x7f122469

.field public static final tethering_help_button_text:I = 0x7f12246a

.field public static final tethering_interface_options:I = 0x7f12246b

.field public static final tethering_settings_not_available:I = 0x7f12246c

.field public static final texture_adjust_temperature_title:I = 0x7f12246d

.field public static final the_anniversary_of_lifting_martial_law:I = 0x7f12246e

.field public static final the_anti_aggression_day:I = 0x7f12246f

.field public static final the_arbor_day:I = 0x7f122470

.field public static final the_armed_forces_day:I = 0x7f122471

.field public static final the_armys_day:I = 0x7f122472

.field public static final the_childrens_day:I = 0x7f122473

.field public static final the_chinese_youth_day:I = 0x7f122474

.field public static final the_christmas_day:I = 0x7f122475

.field public static final the_double_ninth_festival:I = 0x7f122476

.field public static final the_dragon_boat_festival:I = 0x7f122477

.field public static final the_easter_day:I = 0x7f122478

.field public static final the_eve_of_the_spring_festival:I = 0x7f122479

.field public static final the_fifth_day:I = 0x7f12247a

.field public static final the_fools_day:I = 0x7f12247b

.field public static final the_forth_day:I = 0x7f12247c

.field public static final the_hksar_establishment_day:I = 0x7f12247d

.field public static final the_international_womens_day:I = 0x7f12247e

.field public static final the_laba_festival:I = 0x7f12247f

.field public static final the_labour_day:I = 0x7f122480

.field public static final the_lantern_festival:I = 0x7f122481

.field public static final the_mid_autumn_festival:I = 0x7f122482

.field public static final the_national_day:I = 0x7f122483

.field public static final the_national_father_day:I = 0x7f122484

.field public static final the_new_years_day:I = 0x7f122485

.field public static final the_night_of_sevens:I = 0x7f122486

.field public static final the_partys_day:I = 0x7f122487

.field public static final the_peace_day:I = 0x7f122488

.field public static final the_retrocession_day:I = 0x7f122489

.field public static final the_second_day:I = 0x7f12248a

.field public static final the_seventh_day:I = 0x7f12248b

.field public static final the_sixth_day:I = 0x7f12248c

.field public static final the_spirit_festival:I = 0x7f12248d

.field public static final the_spring_festival:I = 0x7f12248e

.field public static final the_teachers_day:I = 0x7f12248f

.field public static final the_third_day:I = 0x7f122490

.field public static final the_tw_childrens_day:I = 0x7f122491

.field public static final the_tw_youth_day:I = 0x7f122492

.field public static final the_united_nations_day:I = 0x7f122493

.field public static final the_valentines_day:I = 0x7f122494

.field public static final the_water_lantern_festival:I = 0x7f122495

.field public static final the_way_of_wakeup_voice_assistant_summary:I = 0x7f122496

.field public static final theme_customization_device_default:I = 0x7f122497

.field public static final theme_settings_title:I = 0x7f122498

.field public static final thememanager_not_found:I = 0x7f122499

.field public static final three_gesture_down:I = 0x7f12249a

.field public static final three_gesture_horizontal:I = 0x7f12249b

.field public static final three_gesture_long_press:I = 0x7f12249c

.field public static final three_gesture_pc_mode:I = 0x7f12249d

.field public static final thursday:I = 0x7f12249e

.field public static final thursday_short:I = 0x7f12249f

.field public static final thursday_shortest:I = 0x7f1224a0

.field public static final time_floating_window:I = 0x7f1224a1

.field public static final time_floating_window_summary:I = 0x7f1224a2

.field public static final time_format_category_title:I = 0x7f1224a3

.field public static final time_format_entries_1:I = 0x7f1224a4

.field public static final time_format_entries_2:I = 0x7f1224a5

.field public static final time_format_entries_3:I = 0x7f1224a6

.field public static final time_picker_dialog_title:I = 0x7f1224a7

.field public static final time_picker_label_hour:I = 0x7f1224a8

.field public static final time_picker_label_minute:I = 0x7f1224a9

.field public static final time_picker_title:I = 0x7f1224aa

.field public static final time_spent_in_app_pref_title:I = 0x7f1224ab

.field public static final time_unit_just_now:I = 0x7f1224ac

.field public static final time_zen_mode_turn_off:I = 0x7f1224ad

.field public static final time_zen_mode_turn_on:I = 0x7f1224ae

.field public static final time_zone_auto_stub:I = 0x7f1224af

.field public static final timed_titlei:I = 0x7f1224b0

.field public static final timezone_settings:I = 0x7f1224b1

.field public static final tip_remove_data:I = 0x7f1224b2

.field public static final title_color_calibration:I = 0x7f1224b3

.field public static final title_font:I = 0x7f1224b4

.field public static final title_font2:I = 0x7f1224b5

.field public static final title_font_current:I = 0x7f1224b6

.field public static final title_font_current2:I = 0x7f1224b7

.field public static final title_font_settings:I = 0x7f1224b8

.field public static final title_font_size:I = 0x7f1224b9

.field public static final title_hdmi_color_calibration:I = 0x7f1224ba

.field public static final title_layout_current:I = 0x7f1224bb

.field public static final title_layout_current2:I = 0x7f1224bc

.field public static final title_layout_current2_weight:I = 0x7f1224bd

.field public static final title_miui_music_mute_by_user:I = 0x7f1224be

.field public static final title_of_auto_launch_manage:I = 0x7f1224bf

.field public static final title_special_features_health_global:I = 0x7f1224c0

.field public static final to_close:I = 0x7f1224c1

.field public static final to_switch_networks_disconnect_ethernet:I = 0x7f1224c2

.field public static final toast_allows_restricted_settings_successfully:I = 0x7f1224c3

.field public static final toast_apply_font_fail:I = 0x7f1224c4

.field public static final toast_disconnect_earphone:I = 0x7f1224c5

.field public static final toast_lockscreen_glogin_empty_password:I = 0x7f1224c6

.field public static final toast_need_login:I = 0x7f1224c7

.field public static final toast_sta_ap_works_wifi_display_disable:I = 0x7f1224c8

.field public static final today:I = 0x7f1224c9

.field public static final toggle_auto_speaker_preference_summary:I = 0x7f1224ca

.field public static final toggle_auto_speaker_preference_title:I = 0x7f1224cb

.field public static final toggle_collapse_after_clicked_title:I = 0x7f1224cc

.field public static final tomorrow:I = 0x7f1224cd

.field public static final total:I = 0x7f1224ce

.field public static final total_info:I = 0x7f1224cf

.field public static final total_info_no_vibrator:I = 0x7f1224d0

.field public static final total_memory:I = 0x7f1224d1

.field public static final total_size:I = 0x7f1224d2

.field public static final total_size_label:I = 0x7f1224d3

.field public static final touch_assistant:I = 0x7f1224d4

.field public static final touch_filtered_warning:I = 0x7f1224d5

.field public static final touch_sensitive_summary:I = 0x7f1224d6

.field public static final touch_sensitive_title:I = 0x7f1224d7

.field public static final touch_sensitive_turn_off_cancel:I = 0x7f1224d8

.field public static final touch_sensitive_turn_off_confirm:I = 0x7f1224d9

.field public static final touch_sensitive_turn_off_summary:I = 0x7f1224da

.field public static final touch_sensitive_turn_off_title:I = 0x7f1224db

.field public static final touch_sounds_title:I = 0x7f1224dc

.field public static final touch_title:I = 0x7f1224dd

.field public static final track_frame_time:I = 0x7f1224de

.field public static final track_frame_time_keywords:I = 0x7f1224df

.field public static final trackball_settings:I = 0x7f1224e0

.field public static final trackball_wake_title:I = 0x7f1224e1

.field public static final traffic_none:I = 0x7f1224e2

.field public static final transcode_default:I = 0x7f1224e3

.field public static final transcode_disable_cache:I = 0x7f1224e4

.field public static final transcode_enable_all:I = 0x7f1224e5

.field public static final transcode_notification:I = 0x7f1224e6

.field public static final transcode_settings_title:I = 0x7f1224e7

.field public static final transcode_user_control:I = 0x7f1224e8

.field public static final transition_animation_scale_title:I = 0x7f1224e9

.field public static final transparent_human_voice:I = 0x7f1224ea

.field public static final transparent_normal:I = 0x7f1224eb

.field public static final transparent_surroundings:I = 0x7f1224ec

.field public static final trigger_carrier_provisioning:I = 0x7f1224ed

.field public static final truetone_summary:I = 0x7f1224ee

.field public static final truetone_title:I = 0x7f1224ef

.field public static final trust_lost_locks_screen_summary:I = 0x7f1224f0

.field public static final trust_lost_locks_screen_title:I = 0x7f1224f1

.field public static final trusted_credentials:I = 0x7f1224f2

.field public static final trusted_credentials_disable_confirmation:I = 0x7f1224f3

.field public static final trusted_credentials_disable_label:I = 0x7f1224f4

.field public static final trusted_credentials_enable_confirmation:I = 0x7f1224f5

.field public static final trusted_credentials_enable_label:I = 0x7f1224f6

.field public static final trusted_credentials_remove_confirmation:I = 0x7f1224f7

.field public static final trusted_credentials_remove_label:I = 0x7f1224f8

.field public static final trusted_credentials_summary:I = 0x7f1224f9

.field public static final trusted_credentials_system_tab:I = 0x7f1224fa

.field public static final trusted_credentials_trust_label:I = 0x7f1224fb

.field public static final trusted_credentials_user_tab:I = 0x7f1224fc

.field public static final tts_default_lang_summary:I = 0x7f1224fd

.field public static final tts_default_lang_title:I = 0x7f1224fe

.field public static final tts_default_pitch_summary:I = 0x7f1224ff

.field public static final tts_default_pitch_title:I = 0x7f122500

.field public static final tts_default_rate_summary:I = 0x7f122501

.field public static final tts_default_rate_title:I = 0x7f122502

.field public static final tts_default_sample_string:I = 0x7f122503

.field public static final tts_engine_network_required:I = 0x7f122504

.field public static final tts_engine_preference_section_title:I = 0x7f122505

.field public static final tts_engine_preference_title:I = 0x7f122506

.field public static final tts_engine_section_title:I = 0x7f122507

.field public static final tts_engine_security_warning:I = 0x7f122508

.field public static final tts_engine_settings_button:I = 0x7f122509

.field public static final tts_engine_settings_title:I = 0x7f12250a

.field public static final tts_general_section_title:I = 0x7f12250b

.field public static final tts_install_data_summary:I = 0x7f12250c

.field public static final tts_install_data_title:I = 0x7f12250d

.field public static final tts_install_voice_title:I = 0x7f12250e

.field public static final tts_install_voices_cancel:I = 0x7f12250f

.field public static final tts_install_voices_open:I = 0x7f122510

.field public static final tts_install_voices_text:I = 0x7f122511

.field public static final tts_install_voices_title:I = 0x7f122512

.field public static final tts_lang_not_selected:I = 0x7f122513

.field public static final tts_lang_use_system:I = 0x7f122514

.field public static final tts_play:I = 0x7f122515

.field public static final tts_play_example_summary:I = 0x7f122516

.field public static final tts_play_example_title:I = 0x7f122517

.field public static final tts_reset:I = 0x7f122518

.field public static final tts_reset_speech_pitch_summary:I = 0x7f122519

.field public static final tts_reset_speech_pitch_title:I = 0x7f12251a

.field public static final tts_reset_speech_rate_summary:I = 0x7f12251b

.field public static final tts_reset_speech_rate_title:I = 0x7f12251c

.field public static final tts_settings:I = 0x7f12251d

.field public static final tts_settings_title:I = 0x7f12251e

.field public static final tts_sliders_title:I = 0x7f12251f

.field public static final tts_spoken_language:I = 0x7f122520

.field public static final tts_status_checking:I = 0x7f122521

.field public static final tts_status_not_supported:I = 0x7f122522

.field public static final tts_status_ok:I = 0x7f122523

.field public static final tts_status_requires_network:I = 0x7f122524

.field public static final tts_status_title:I = 0x7f122525

.field public static final tuesday:I = 0x7f122526

.field public static final tuesday_short:I = 0x7f122527

.field public static final tuesday_shortest:I = 0x7f122528

.field public static final turn_off_common_password_confirm_msg:I = 0x7f122529

.field public static final turn_off_keyguard_password_alert_title:I = 0x7f12252a

.field public static final turn_off_keyguard_password_confirm_msg:I = 0x7f12252b

.field public static final turn_off_keyguard_password_title:I = 0x7f12252c

.field public static final turn_off_keyguard_password_wait_dialog:I = 0x7f12252d

.field public static final turn_off_keyguard_password_with_face_confirm_msg:I = 0x7f12252e

.field public static final turn_off_keyguard_password_with_fingerprint_confirm_msg:I = 0x7f12252f

.field public static final turn_off_keyguard_password_with_fingerprint_face_confirm_msg:I = 0x7f122530

.field public static final turn_off_security_lock:I = 0x7f122531

.field public static final turn_off_wifi:I = 0x7f122532

.field public static final turn_on_keyguard_password:I = 0x7f122533

.field public static final turn_on_keyguard_password_alert:I = 0x7f122534

.field public static final turn_on_keyguard_password_alert_choose:I = 0x7f122535

.field public static final turn_on_keyguard_password_alert_foget_cacel:I = 0x7f122536

.field public static final turn_on_keyguard_password_alert_foget_ok:I = 0x7f122537

.field public static final turn_on_keyguard_password_alert_foget_ok_time:I = 0x7f122538

.field public static final turn_on_keyguard_password_alert_forget_content:I = 0x7f122539

.field public static final turn_on_keyguard_password_alert_forget_title:I = 0x7f12253a

.field public static final turn_on_security_lock:I = 0x7f12253b

.field public static final turn_on_torch:I = 0x7f12253c

.field public static final turn_on_wifi:I = 0x7f12253d

.field public static final turn_screen_on_title:I = 0x7f12253e

.field public static final turn_update_keyguard_password_wait_dialog:I = 0x7f12253f

.field public static final twilight_mode_launch_location:I = 0x7f122540

.field public static final twilight_mode_location_off_dialog_message:I = 0x7f122541

.field public static final twohours:I = 0x7f122542

.field public static final tx_link_speed:I = 0x7f122543

.field public static final tx_link_speed_label:I = 0x7f122544

.field public static final tx_wifi_speed:I = 0x7f122545

.field public static final ucar_screen_projection_title:I = 0x7f122546

.field public static final unadapt_embedded:I = 0x7f122547

.field public static final under_keyguard:I = 0x7f122548

.field public static final unimportant:I = 0x7f122549

.field public static final uninstall_all_users_text:I = 0x7f12254a

.field public static final uninstall_certs_credential_management_app:I = 0x7f12254b

.field public static final uninstall_device_admin:I = 0x7f12254c

.field public static final uninstall_dlg_msg:I = 0x7f12254d

.field public static final uninstall_dlg_title:I = 0x7f12254e

.field public static final uninstall_protected_dlg_msg:I = 0x7f12254f

.field public static final uninstall_protected_dlg_title:I = 0x7f122550

.field public static final uninstall_text:I = 0x7f122551

.field public static final uninstall_with_xspace_app_dlg_msg:I = 0x7f122552

.field public static final uninstall_xspace_app_dlg_msg:I = 0x7f122553

.field public static final uninstall_xspace_app_dlg_title:I = 0x7f122554

.field public static final unknown:I = 0x7f122555

.field public static final unknown_app:I = 0x7f122556

.field public static final unknown_error_info:I = 0x7f122557

.field public static final unknown_unavailability_setting_summary:I = 0x7f122558

.field public static final unlock_change_lock_password_title:I = 0x7f122559

.field public static final unlock_change_lock_pattern_title:I = 0x7f12255a

.field public static final unlock_change_lock_pin_title:I = 0x7f12255b

.field public static final unlock_disable_frp_warning_content_password:I = 0x7f12255c

.field public static final unlock_disable_frp_warning_content_password_face:I = 0x7f12255d

.field public static final unlock_disable_frp_warning_content_password_face_fingerprint:I = 0x7f12255e

.field public static final unlock_disable_frp_warning_content_password_fingerprint:I = 0x7f12255f

.field public static final unlock_disable_frp_warning_content_pattern:I = 0x7f122560

.field public static final unlock_disable_frp_warning_content_pattern_face:I = 0x7f122561

.field public static final unlock_disable_frp_warning_content_pattern_face_fingerprint:I = 0x7f122562

.field public static final unlock_disable_frp_warning_content_pattern_fingerprint:I = 0x7f122563

.field public static final unlock_disable_frp_warning_content_pin:I = 0x7f122564

.field public static final unlock_disable_frp_warning_content_pin_face:I = 0x7f122565

.field public static final unlock_disable_frp_warning_content_pin_face_fingerprint:I = 0x7f122566

.field public static final unlock_disable_frp_warning_content_pin_fingerprint:I = 0x7f122567

.field public static final unlock_disable_frp_warning_content_profile:I = 0x7f122568

.field public static final unlock_disable_frp_warning_content_unknown:I = 0x7f122569

.field public static final unlock_disable_frp_warning_content_unknown_face:I = 0x7f12256a

.field public static final unlock_disable_frp_warning_content_unknown_face_fingerprint:I = 0x7f12256b

.field public static final unlock_disable_frp_warning_content_unknown_fingerprint:I = 0x7f12256c

.field public static final unlock_disable_frp_warning_ok:I = 0x7f12256d

.field public static final unlock_disable_frp_warning_title:I = 0x7f12256e

.field public static final unlock_disable_frp_warning_title_profile:I = 0x7f12256f

.field public static final unlock_disable_lock_pin_summary:I = 0x7f122570

.field public static final unlock_disable_lock_title:I = 0x7f122571

.field public static final unlock_failed:I = 0x7f122572

.field public static final unlock_footer_high_complexity_requested:I = 0x7f122573

.field public static final unlock_footer_low_complexity_requested:I = 0x7f122574

.field public static final unlock_footer_medium_complexity_requested:I = 0x7f122575

.field public static final unlock_footer_none_complexity_requested:I = 0x7f122576

.field public static final unlock_mode_title:I = 0x7f122577

.field public static final unlock_set_do_later_title:I = 0x7f122578

.field public static final unlock_set_enter_system:I = 0x7f122579

.field public static final unlock_set_enter_system_summary:I = 0x7f12257a

.field public static final unlock_set_unlock_biometric_weak_summary:I = 0x7f12257b

.field public static final unlock_set_unlock_biometric_weak_title:I = 0x7f12257c

.field public static final unlock_set_unlock_disabled_summary:I = 0x7f12257d

.field public static final unlock_set_unlock_launch_picker_change_summary:I = 0x7f12257e

.field public static final unlock_set_unlock_launch_picker_change_title:I = 0x7f12257f

.field public static final unlock_set_unlock_launch_picker_enable_summary:I = 0x7f122580

.field public static final unlock_set_unlock_launch_picker_summary_lock_after_timeout:I = 0x7f122581

.field public static final unlock_set_unlock_launch_picker_summary_lock_immediately:I = 0x7f122582

.field public static final unlock_set_unlock_launch_picker_title:I = 0x7f122583

.field public static final unlock_set_unlock_launch_picker_title_profile:I = 0x7f122584

.field public static final unlock_set_unlock_mode_none:I = 0x7f122585

.field public static final unlock_set_unlock_mode_off:I = 0x7f122586

.field public static final unlock_set_unlock_mode_password:I = 0x7f122587

.field public static final unlock_set_unlock_mode_pattern:I = 0x7f122588

.field public static final unlock_set_unlock_mode_pin:I = 0x7f122589

.field public static final unlock_set_unlock_none_summary:I = 0x7f12258a

.field public static final unlock_set_unlock_none_title:I = 0x7f12258b

.field public static final unlock_set_unlock_off_summary:I = 0x7f12258c

.field public static final unlock_set_unlock_off_title:I = 0x7f12258d

.field public static final unlock_set_unlock_password_none:I = 0x7f12258e

.field public static final unlock_set_unlock_password_summary:I = 0x7f12258f

.field public static final unlock_set_unlock_password_title:I = 0x7f122590

.field public static final unlock_set_unlock_pattern_summary:I = 0x7f122591

.field public static final unlock_set_unlock_pattern_title:I = 0x7f122592

.field public static final unlock_set_unlock_pin_summary:I = 0x7f122593

.field public static final unlock_set_unlock_pin_title:I = 0x7f122594

.field public static final unlock_set_unlock_summary:I = 0x7f122595

.field public static final unlock_setup_wizard_fingerprint_details:I = 0x7f122596

.field public static final unlock_sound_effect_enable_title:I = 0x7f122597

.field public static final unlock_type_category_title:I = 0x7f122598

.field public static final unlogin_account_summary:I = 0x7f122599

.field public static final unlogin_account_title:I = 0x7f12259a

.field public static final unmount_inform_text:I = 0x7f12259b

.field public static final unrestrict_max_aspect_ratio_title:I = 0x7f12259c

.field public static final unrestricted_app_summary:I = 0x7f12259d

.field public static final unrestricted_app_title:I = 0x7f12259e

.field public static final unrestricted_data_saver:I = 0x7f12259f

.field public static final unsupported_setting_summary:I = 0x7f1225a0

.field public static final unused_apps:I = 0x7f1225a1

.field public static final unused_apps_category:I = 0x7f1225a2

.field public static final unused_apps_switch:I = 0x7f1225a3

.field public static final unused_apps_switch_summary:I = 0x7f1225a4

.field public static final update_new_version:I = 0x7f1225a5

.field public static final update_progress_for_updates:I = 0x7f1225a6

.field public static final update_sourceinfo_btn_txt:I = 0x7f1225a7

.field public static final updating_settings:I = 0x7f1225a8

.field public static final uplmn_list_setting_add_plmn:I = 0x7f1225a9

.field public static final uplmn_settings_summary:I = 0x7f1225aa

.field public static final uplmn_settings_title:I = 0x7f1225ab

.field public static final upload_debug_log_detail:I = 0x7f1225ac

.field public static final upload_debug_log_summary:I = 0x7f1225ad

.field public static final upload_debug_log_title:I = 0x7f1225ae

.field public static final usage:I = 0x7f1225af

.field public static final usage_access:I = 0x7f1225b0

.field public static final usage_access_description:I = 0x7f1225b1

.field public static final usage_access_title:I = 0x7f1225b2

.field public static final usage_app_get_it:I = 0x7f1225b3

.field public static final usage_app_limit_alter_summary:I = 0x7f1225b4

.field public static final usage_app_limit_alter_title:I = 0x7f1225b5

.field public static final usage_app_limit_prelong:I = 0x7f1225b6

.field public static final usage_app_limit_reach_summay:I = 0x7f1225b7

.field public static final usage_app_limit_reach_title:I = 0x7f1225b8

.field public static final usage_app_limit_summary:I = 0x7f1225b9

.field public static final usage_app_limit_title:I = 0x7f1225ba

.field public static final usage_app_other_settings:I = 0x7f1225bb

.field public static final usage_app_over_content:I = 0x7f1225bc

.field public static final usage_app_over_summary:I = 0x7f1225bd

.field public static final usage_app_set_limit_time:I = 0x7f1225be

.field public static final usage_app_system_app_summary:I = 0x7f1225bf

.field public static final usage_app_weekday_limit:I = 0x7f1225c0

.field public static final usage_app_weekend_limit:I = 0x7f1225c1

.field public static final usage_log:I = 0x7f1225c2

.field public static final usage_state_app_timer:I = 0x7f1225c3

.field public static final usage_state_app_usage_detail_title:I = 0x7f1225c4

.field public static final usage_state_app_usage_list_title:I = 0x7f1225c5

.field public static final usage_state_app_usage_list_week_title:I = 0x7f1225c6

.field public static final usage_state_app_usage_tip_title:I = 0x7f1225c7

.field public static final usage_state_app_use_time:I = 0x7f1225c8

.field public static final usage_state_channel_mouth:I = 0x7f1225c9

.field public static final usage_state_channel_week:I = 0x7f1225ca

.field public static final usage_state_date:I = 0x7f1225cb

.field public static final usage_state_device_noti_today_tip_title:I = 0x7f1225cc

.field public static final usage_state_device_unlock_today_tip_title:I = 0x7f1225cd

.field public static final usage_state_friday:I = 0x7f1225ce

.field public static final usage_state_go_more:I = 0x7f1225cf

.field public static final usage_state_hour_minute:I = 0x7f1225d0

.field public static final usage_state_less_one_minute:I = 0x7f1225d1

.field public static final usage_state_monday:I = 0x7f1225d2

.field public static final usage_state_mourth_day:I = 0x7f1225d3

.field public static final usage_state_noti_mourth_day:I = 0x7f1225d4

.field public static final usage_state_notification:I = 0x7f1225d5

.field public static final usage_state_notification_manager:I = 0x7f1225d6

.field public static final usage_state_remind_summary:I = 0x7f1225d7

.field public static final usage_state_remind_title:I = 0x7f1225d8

.field public static final usage_state_rest_day:I = 0x7f1225d9

.field public static final usage_state_saturday:I = 0x7f1225da

.field public static final usage_state_set_invalid_time_toast:I = 0x7f1225db

.field public static final usage_state_sunday:I = 0x7f1225dc

.field public static final usage_state_thursday:I = 0x7f1225dd

.field public static final usage_state_today:I = 0x7f1225de

.field public static final usage_state_today_total_text:I = 0x7f1225df

.field public static final usage_state_tuesday:I = 0x7f1225e0

.field public static final usage_state_unlock_mourth_day:I = 0x7f1225e1

.field public static final usage_state_usage_time:I = 0x7f1225e2

.field public static final usage_state_usage_time_remind:I = 0x7f1225e3

.field public static final usage_state_use_time_remind_title:I = 0x7f1225e4

.field public static final usage_state_wednesday:I = 0x7f1225e5

.field public static final usage_state_week_day_hour_minute:I = 0x7f1225e6

.field public static final usage_state_week_total_text:I = 0x7f1225e7

.field public static final usage_state_which_mouth:I = 0x7f1225e8

.field public static final usage_state_work_day:I = 0x7f1225e9

.field public static final usage_state_work_day_hour_minute:I = 0x7f1225ea

.field public static final usage_stats_label:I = 0x7f1225eb

.field public static final usage_stats_normal_day:I = 0x7f1225ec

.field public static final usage_stats_week_day:I = 0x7f1225ed

.field public static final usage_time_label:I = 0x7f1225ee

.field public static final usage_type_actual_power:I = 0x7f1225ef

.field public static final usage_type_audio:I = 0x7f1225f0

.field public static final usage_type_camera:I = 0x7f1225f1

.field public static final usage_type_computed_power:I = 0x7f1225f2

.field public static final usage_type_cpu:I = 0x7f1225f3

.field public static final usage_type_cpu_foreground:I = 0x7f1225f4

.field public static final usage_type_data_recv:I = 0x7f1225f5

.field public static final usage_type_data_send:I = 0x7f1225f6

.field public static final usage_type_data_wifi_recv:I = 0x7f1225f7

.field public static final usage_type_data_wifi_send:I = 0x7f1225f8

.field public static final usage_type_flashlight:I = 0x7f1225f9

.field public static final usage_type_gps:I = 0x7f1225fa

.field public static final usage_type_no_coverage:I = 0x7f1225fb

.field public static final usage_type_on_time:I = 0x7f1225fc

.field public static final usage_type_phone:I = 0x7f1225fd

.field public static final usage_type_radio_active:I = 0x7f1225fe

.field public static final usage_type_total_battery_capacity:I = 0x7f1225ff

.field public static final usage_type_video:I = 0x7f122600

.field public static final usage_type_wake_lock:I = 0x7f122601

.field public static final usage_type_wifi_running:I = 0x7f122602

.field public static final usagestats_app_category_miui_financial:I = 0x7f122603

.field public static final usagestats_app_category_miui_game:I = 0x7f122604

.field public static final usagestats_app_category_miui_life:I = 0x7f122605

.field public static final usagestats_app_category_miui_productivity:I = 0x7f122606

.field public static final usagestats_app_category_miui_reading:I = 0x7f122607

.field public static final usagestats_app_category_miui_shopping:I = 0x7f122608

.field public static final usagestats_app_category_miui_social:I = 0x7f122609

.field public static final usagestats_app_category_miui_system:I = 0x7f12260a

.field public static final usagestats_app_category_miui_tools:I = 0x7f12260b

.field public static final usagestats_app_category_miui_undefined:I = 0x7f12260c

.field public static final usagestats_app_category_miui_video_etc:I = 0x7f12260d

.field public static final usagestats_device_notification_des:I = 0x7f12260e

.field public static final usagestats_device_notification_des_over:I = 0x7f12260f

.field public static final usagestats_device_notification_des_reset:I = 0x7f122610

.field public static final usagestats_device_notification_title:I = 0x7f122611

.field public static final usagestats_device_timeout_des:I = 0x7f122612

.field public static final usagestats_device_timeout_negative:I = 0x7f122613

.field public static final usagestats_device_timeout_positive:I = 0x7f122614

.field public static final usagestats_device_timeout_title:I = 0x7f122615

.field public static final usb_adb_input:I = 0x7f122616

.field public static final usb_adb_input_desc:I = 0x7f122617

.field public static final usb_audio_disable_routing:I = 0x7f122618

.field public static final usb_audio_disable_routing_summary:I = 0x7f122619

.field public static final usb_charging_summary:I = 0x7f12261a

.field public static final usb_charging_title:I = 0x7f12261b

.field public static final usb_connection_category:I = 0x7f12261c

.field public static final usb_control_device:I = 0x7f12261d

.field public static final usb_control_host:I = 0x7f12261e

.field public static final usb_control_title:I = 0x7f12261f

.field public static final usb_default_info:I = 0x7f122620

.field public static final usb_default_label:I = 0x7f122621

.field public static final usb_eject:I = 0x7f122622

.field public static final usb_eject_summary:I = 0x7f122623

.field public static final usb_file_transfer_title:I = 0x7f122624

.field public static final usb_format:I = 0x7f122625

.field public static final usb_format_summary:I = 0x7f122626

.field public static final usb_headset_not_support_cancel:I = 0x7f122627

.field public static final usb_headset_not_support_message:I = 0x7f122628

.field public static final usb_headset_not_support_tile:I = 0x7f122629

.field public static final usb_host_mode_subtext:I = 0x7f12262a

.field public static final usb_host_mode_text:I = 0x7f12262b

.field public static final usb_insert_summary:I = 0x7f12262c

.field public static final usb_install_app:I = 0x7f12262d

.field public static final usb_install_app_desc:I = 0x7f12262e

.field public static final usb_midi_summary:I = 0x7f12262f

.field public static final usb_midi_title:I = 0x7f122630

.field public static final usb_mount:I = 0x7f122631

.field public static final usb_mount_summary:I = 0x7f122632

.field public static final usb_msd_driver_installer_summary:I = 0x7f122633

.field public static final usb_msd_driver_installer_title:I = 0x7f122634

.field public static final usb_msd_summary:I = 0x7f122635

.field public static final usb_msd_title:I = 0x7f122636

.field public static final usb_mtp_summary:I = 0x7f122637

.field public static final usb_mtp_title:I = 0x7f122638

.field public static final usb_power_title:I = 0x7f122639

.field public static final usb_pref:I = 0x7f12263a

.field public static final usb_preference:I = 0x7f12263b

.field public static final usb_ptp_summary:I = 0x7f12263c

.field public static final usb_ptp_title:I = 0x7f12263d

.field public static final usb_reverse_charge:I = 0x7f12263e

.field public static final usb_summary_MIDI:I = 0x7f12263f

.field public static final usb_summary_MIDI_power:I = 0x7f122640

.field public static final usb_summary_charging_only:I = 0x7f122641

.field public static final usb_summary_file_transfers:I = 0x7f122642

.field public static final usb_summary_file_transfers_power:I = 0x7f122643

.field public static final usb_summary_photo_transfers:I = 0x7f122644

.field public static final usb_summary_photo_transfers_power:I = 0x7f122645

.field public static final usb_summary_power_only:I = 0x7f122646

.field public static final usb_summary_tether:I = 0x7f122647

.field public static final usb_summary_tether_power:I = 0x7f122648

.field public static final usb_switching:I = 0x7f122649

.field public static final usb_switching_failed:I = 0x7f12264a

.field public static final usb_tethering_active_subtext:I = 0x7f12264b

.field public static final usb_tethering_available_subtext:I = 0x7f12264c

.field public static final usb_tethering_button_text:I = 0x7f12264d

.field public static final usb_tethering_errored_subtext:I = 0x7f12264e

.field public static final usb_tethering_storage_active_subtext:I = 0x7f12264f

.field public static final usb_tethering_subtext:I = 0x7f122650

.field public static final usb_tethering_unavailable_subtext:I = 0x7f122651

.field public static final usb_title:I = 0x7f122652

.field public static final usb_transcode_files:I = 0x7f122653

.field public static final usb_transcode_files_summary:I = 0x7f122654

.field public static final usb_use:I = 0x7f122655

.field public static final usb_use_MIDI:I = 0x7f122656

.field public static final usb_use_MIDI_desc:I = 0x7f122657

.field public static final usb_use_MIDI_miui12_5:I = 0x7f122658

.field public static final usb_use_charging_only:I = 0x7f122659

.field public static final usb_use_charging_only_desc:I = 0x7f12265a

.field public static final usb_use_file_transfers:I = 0x7f12265b

.field public static final usb_use_file_transfers_desc:I = 0x7f12265c

.field public static final usb_use_file_transfers_miui12_5:I = 0x7f12265d

.field public static final usb_use_miui12_5:I = 0x7f12265e

.field public static final usb_use_photo_transfers:I = 0x7f12265f

.field public static final usb_use_photo_transfers_desc:I = 0x7f122660

.field public static final usb_use_photo_transfers_miui12_5:I = 0x7f122661

.field public static final usb_use_power_only:I = 0x7f122662

.field public static final usb_use_tethering:I = 0x7f122663

.field public static final use_control_panel:I = 0x7f122664

.field public static final use_location_summary:I = 0x7f122665

.field public static final use_location_title:I = 0x7f122666

.field public static final use_open_wifi_automatically_summary:I = 0x7f122667

.field public static final use_open_wifi_automatically_summary_scorer_unsupported_disabled:I = 0x7f122668

.field public static final use_open_wifi_automatically_summary_scoring_disabled:I = 0x7f122669

.field public static final use_open_wifi_automatically_title:I = 0x7f12266a

.field public static final use_otg_charge_only:I = 0x7f12266b

.field public static final use_otg_charge_only_summary:I = 0x7f12266c

.field public static final use_otg_file_transfers:I = 0x7f12266d

.field public static final use_otg_file_transfers_summary:I = 0x7f12266e

.field public static final use_otg_title:I = 0x7f12266f

.field public static final use_personalized_ad_service:I = 0x7f122670

.field public static final use_personalized_ad_service_summary:I = 0x7f122671

.field public static final use_personalized_ad_service_summary_for_global:I = 0x7f122672

.field public static final use_privacy_password_immediate:I = 0x7f122673

.field public static final use_system_language_to_select_input_method_subtypes:I = 0x7f122674

.field public static final use_system_preference:I = 0x7f122675

.field public static final use_this_wlan:I = 0x7f122676

.field public static final use_wifi_hotsopt_main_switch_title:I = 0x7f122677

.field public static final user_account_title:I = 0x7f122678

.field public static final user_add_max_count:I = 0x7f122679

.field public static final user_add_on_lockscreen_menu:I = 0x7f12267a

.field public static final user_add_profile_item_summary:I = 0x7f12267b

.field public static final user_add_profile_item_title:I = 0x7f12267c

.field public static final user_add_user:I = 0x7f12267d

.field public static final user_add_user_item_summary:I = 0x7f12267e

.field public static final user_add_user_item_title:I = 0x7f12267f

.field public static final user_add_user_message_long:I = 0x7f122680

.field public static final user_add_user_message_short:I = 0x7f122681

.field public static final user_add_user_or_profile_menu:I = 0x7f122682

.field public static final user_add_user_title:I = 0x7f122683

.field public static final user_add_user_type_title:I = 0x7f122684

.field public static final user_adding_new_user:I = 0x7f122685

.field public static final user_admin:I = 0x7f122686

.field public static final user_agreement:I = 0x7f122687

.field public static final user_cannot_add_accounts_message:I = 0x7f122688

.field public static final user_cannot_manage_message:I = 0x7f122689

.field public static final user_certificate:I = 0x7f12268a

.field public static final user_choose_copy_apps_to_another_user:I = 0x7f12268b

.field public static final user_confirm_remove_message:I = 0x7f12268c

.field public static final user_confirm_remove_self_message:I = 0x7f12268d

.field public static final user_confirm_remove_self_title:I = 0x7f12268e

.field public static final user_confirm_remove_title:I = 0x7f12268f

.field public static final user_copy_apps_menu_title:I = 0x7f122690

.field public static final user_credential_none_installed:I = 0x7f122691

.field public static final user_credential_removed:I = 0x7f122692

.field public static final user_credential_title:I = 0x7f122693

.field public static final user_credentials:I = 0x7f122694

.field public static final user_credentials_summary:I = 0x7f122695

.field public static final user_delete_button:I = 0x7f122696

.field public static final user_delete_user_description:I = 0x7f122697

.field public static final user_dict_settings_add_dialog_confirm:I = 0x7f122698

.field public static final user_dict_settings_add_dialog_less_options:I = 0x7f122699

.field public static final user_dict_settings_add_dialog_more_options:I = 0x7f12269a

.field public static final user_dict_settings_add_dialog_title:I = 0x7f12269b

.field public static final user_dict_settings_add_locale_option_name:I = 0x7f12269c

.field public static final user_dict_settings_add_menu_title:I = 0x7f12269d

.field public static final user_dict_settings_add_screen_title:I = 0x7f12269e

.field public static final user_dict_settings_add_shortcut_hint:I = 0x7f12269f

.field public static final user_dict_settings_add_shortcut_option_name:I = 0x7f1226a0

.field public static final user_dict_settings_add_word_hint:I = 0x7f1226a1

.field public static final user_dict_settings_add_word_option_name:I = 0x7f1226a2

.field public static final user_dict_settings_all_languages:I = 0x7f1226a3

.field public static final user_dict_settings_context_menu_delete_title:I = 0x7f1226a4

.field public static final user_dict_settings_context_menu_edit_title:I = 0x7f1226a5

.field public static final user_dict_settings_edit_dialog_title:I = 0x7f1226a6

.field public static final user_dict_settings_empty_text:I = 0x7f1226a7

.field public static final user_dict_settings_for_work_title:I = 0x7f1226a8

.field public static final user_dict_settings_more_languages:I = 0x7f1226a9

.field public static final user_dict_settings_summary:I = 0x7f1226aa

.field public static final user_dict_settings_title:I = 0x7f1226ab

.field public static final user_enable_calling:I = 0x7f1226ac

.field public static final user_enable_calling_and_sms_confirm_message:I = 0x7f1226ad

.field public static final user_enable_calling_and_sms_confirm_title:I = 0x7f1226ae

.field public static final user_enable_calling_confirm_message:I = 0x7f1226af

.field public static final user_enable_calling_confirm_title:I = 0x7f1226b0

.field public static final user_enable_calling_sms:I = 0x7f1226b1

.field public static final user_exit_guest_confirm_message:I = 0x7f1226b2

.field public static final user_exit_guest_dialog_remove:I = 0x7f1226b3

.field public static final user_experience_open_url:I = 0x7f1226b4

.field public static final user_experience_program:I = 0x7f1226b5

.field public static final user_experience_program_summary:I = 0x7f1226b6

.field public static final user_experience_program_title:I = 0x7f1226b7

.field public static final user_image_choose_photo:I = 0x7f1226b8

.field public static final user_image_photo_selector:I = 0x7f1226b9

.field public static final user_image_take_photo:I = 0x7f1226ba

.field public static final user_info_settings_title:I = 0x7f1226bb

.field public static final user_installed_services_category_title:I = 0x7f1226bc

.field public static final user_list_title:I = 0x7f1226bd

.field public static final user_lockscreen_settings:I = 0x7f1226be

.field public static final user_need_lock_message:I = 0x7f1226bf

.field public static final user_new_profile_name:I = 0x7f1226c0

.field public static final user_new_user_name:I = 0x7f1226c1

.field public static final user_nickname:I = 0x7f1226c2

.field public static final user_picture_title:I = 0x7f1226c3

.field public static final user_profile_confirm_remove_message:I = 0x7f1226c4

.field public static final user_profile_confirm_remove_title:I = 0x7f1226c5

.field public static final user_remove_user:I = 0x7f1226c6

.field public static final user_remove_user_menu:I = 0x7f1226c7

.field public static final user_rename:I = 0x7f1226c8

.field public static final user_restrictions_controlled_by:I = 0x7f1226c9

.field public static final user_restrictions_title:I = 0x7f1226ca

.field public static final user_set_lock_button:I = 0x7f1226cb

.field public static final user_settings_footer_text:I = 0x7f1226cc

.field public static final user_settings_forbidden:I = 0x7f1226cd

.field public static final user_settings_title:I = 0x7f1226ce

.field public static final user_setup_button_setup_later:I = 0x7f1226cf

.field public static final user_setup_button_setup_now:I = 0x7f1226d0

.field public static final user_setup_dialog_message:I = 0x7f1226d1

.field public static final user_setup_dialog_title:I = 0x7f1226d2

.field public static final user_setup_profile_dialog_message:I = 0x7f1226d3

.field public static final user_summary_managed_profile_not_set_up:I = 0x7f1226d4

.field public static final user_summary_not_set_up:I = 0x7f1226d5

.field public static final user_summary_restricted_not_set_up:I = 0x7f1226d6

.field public static final user_summary_restricted_profile:I = 0x7f1226d7

.field public static final user_switch_to_user:I = 0x7f1226d8

.field public static final user_you:I = 0x7f1226d9

.field public static final users_summary:I = 0x7f1226da

.field public static final uwb_settings_summary:I = 0x7f1226db

.field public static final uwb_settings_summary_airplane_mode:I = 0x7f1226dc

.field public static final uwb_settings_title:I = 0x7f1226dd

.field public static final v7_preference_off:I = 0x7f1226de

.field public static final v7_preference_on:I = 0x7f1226df

.field public static final verify_apps_over_usb_summary:I = 0x7f1226e0

.field public static final verify_apps_over_usb_title:I = 0x7f1226e1

.field public static final version_text:I = 0x7f1226e2

.field public static final vibrate_category_title:I = 0x7f1226e3

.field public static final vibrate_icon_title:I = 0x7f1226e4

.field public static final vibrate_in_normal_title:I = 0x7f1226e5

.field public static final vibrate_in_silent_title:I = 0x7f1226e6

.field public static final vibrate_input_devices:I = 0x7f1226e7

.field public static final vibrate_input_devices_summary:I = 0x7f1226e8

.field public static final vibrate_title:I = 0x7f1226e9

.field public static final vibrate_when_ringing_option_always_vibrate:I = 0x7f1226ea

.field public static final vibrate_when_ringing_option_never_vibrate:I = 0x7f1226eb

.field public static final vibrate_when_ringing_option_ramping_ringer:I = 0x7f1226ec

.field public static final vibrate_when_ringing_title:I = 0x7f1226ed

.field public static final video_calling_settings_title:I = 0x7f1226ee

.field public static final video_tool_box_title:I = 0x7f1226ef

.field public static final view_saved_network:I = 0x7f1226f0

.field public static final vip_list:I = 0x7f1226f1

.field public static final vip_service_settings:I = 0x7f1226f2

.field public static final virtual_keyboard_category:I = 0x7f1226f3

.field public static final virtual_keyboards_for_work_title:I = 0x7f1226f4

.field public static final virtual_sim_category_title:I = 0x7f1226f5

.field public static final virtual_sim_description:I = 0x7f1226f6

.field public static final virtual_sim_global_category_title:I = 0x7f1226f7

.field public static final virtual_sim_global_description:I = 0x7f1226f8

.field public static final virtual_sim_global_title:I = 0x7f1226f9

.field public static final virtual_sim_title:I = 0x7f1226fa

.field public static final virtual_surround_sound_switch_summary:I = 0x7f1226fb

.field public static final virtual_surround_sound_switch_title:I = 0x7f1226fc

.field public static final virus_scan:I = 0x7f1226fd

.field public static final virus_scan_app_continue:I = 0x7f1226fe

.field public static final virus_scan_app_finish:I = 0x7f1226ff

.field public static final virus_scan_app_force_stop:I = 0x7f122700

.field public static final virus_scan_app_is_stop_scan:I = 0x7f122701

.field public static final virus_scan_app_multi_engines:I = 0x7f122702

.field public static final virus_scan_app_no_risk:I = 0x7f122703

.field public static final virus_scan_app_passed:I = 0x7f122704

.field public static final virus_scan_app_risk:I = 0x7f122705

.field public static final virus_scan_app_safe_scanning:I = 0x7f122706

.field public static final virus_scan_app_scan_finish:I = 0x7f122707

.field public static final virus_scan_app_scanning:I = 0x7f122708

.field public static final virus_scan_app_stop_scan:I = 0x7f122709

.field public static final virus_scan_app_uninstall:I = 0x7f12270a

.field public static final virus_scan_app_waiting:I = 0x7f12270b

.field public static final virus_scanner_auto_update:I = 0x7f12270c

.field public static final virus_scanner_date_formater:I = 0x7f12270d

.field public static final virus_scanner_lib_already_latest:I = 0x7f12270e

.field public static final virus_scanner_lib_update_failed:I = 0x7f12270f

.field public static final virus_scanner_lib_updating:I = 0x7f122710

.field public static final virus_scanner_manual_update:I = 0x7f122711

.field public static final virus_scanner_provider:I = 0x7f122712

.field public static final virus_scanner_scan:I = 0x7f122713

.field public static final virus_scanner_scanning:I = 0x7f122714

.field public static final virus_scanner_select_Cancel:I = 0x7f122715

.field public static final virus_scanner_select_OK:I = 0x7f122716

.field public static final virus_scanner_select_lib:I = 0x7f122717

.field public static final virus_scanner_select_multi:I = 0x7f122718

.field public static final virus_scanner_select_single:I = 0x7f122719

.field public static final virus_scanner_select_trust_lib:I = 0x7f12271a

.field public static final virus_scanner_title:I = 0x7f12271b

.field public static final virus_scanner_update_lib:I = 0x7f12271c

.field public static final visibility_override_title:I = 0x7f12271d

.field public static final vision_settings_description:I = 0x7f12271e

.field public static final vision_settings_suggestion_title:I = 0x7f12271f

.field public static final vision_settings_title:I = 0x7f122720

.field public static final voice_access:I = 0x7f122721

.field public static final voice_assist:I = 0x7f122722

.field public static final voice_assist_volume_option_title:I = 0x7f122723

.field public static final voice_category:I = 0x7f122724

.field public static final voice_helper_title:I = 0x7f122725

.field public static final voice_input_output_settings:I = 0x7f122726

.field public static final voice_input_output_settings_title:I = 0x7f122727

.field public static final voice_input_settings:I = 0x7f122728

.field public static final voice_input_settings_title:I = 0x7f122729

.field public static final voice_interaction_security_warning:I = 0x7f12272a

.field public static final voice_interactor_preference_summary:I = 0x7f12272b

.field public static final voice_recognizer_preference_summary:I = 0x7f12272c

.field public static final voice_search_settings_title:I = 0x7f12272d

.field public static final voice_service_preference_section_title:I = 0x7f12272e

.field public static final voicemail_number_not_set:I = 0x7f12272f

.field public static final voip_assistant_settings:I = 0x7f122730

.field public static final voip_assistant_settings_off:I = 0x7f122731

.field public static final voip_assistant_settings_on:I = 0x7f122732

.field public static final voip_assistant_settings_summary:I = 0x7f122733

.field public static final volte_5G_limited_text:I = 0x7f122734

.field public static final volte_5G_limited_title:I = 0x7f122735

.field public static final volume_adjustment:I = 0x7f122736

.field public static final volume_alarm_description:I = 0x7f122737

.field public static final volume_alarm_mute:I = 0x7f122738

.field public static final volume_bluetooth_description:I = 0x7f122739

.field public static final volume_category_title:I = 0x7f12273a

.field public static final volume_connectivity_panel_title:I = 0x7f12273b

.field public static final volume_hifi_description:I = 0x7f12273c

.field public static final volume_label:I = 0x7f12273d

.field public static final volume_launch_camera_summary:I = 0x7f12273e

.field public static final volume_media_description:I = 0x7f12273f

.field public static final volume_media_mute:I = 0x7f122740

.field public static final volume_notification_description:I = 0x7f122741

.field public static final volume_notification_mute:I = 0x7f122742

.field public static final volume_restore:I = 0x7f122743

.field public static final volume_restore_alert:I = 0x7f122744

.field public static final volume_ring_description:I = 0x7f122745

.field public static final volume_ring_mute:I = 0x7f122746

.field public static final volume_system_description:I = 0x7f122747

.field public static final volume_voice_description:I = 0x7f122748

.field public static final vonr_switch_title:I = 0x7f122749

.field public static final vpn_always_on_invalid_reason_dns:I = 0x7f12274a

.field public static final vpn_always_on_invalid_reason_no_dns:I = 0x7f12274b

.field public static final vpn_always_on_invalid_reason_other:I = 0x7f12274c

.field public static final vpn_always_on_invalid_reason_server:I = 0x7f12274d

.field public static final vpn_always_on_invalid_reason_type:I = 0x7f12274e

.field public static final vpn_always_on_summary:I = 0x7f12274f

.field public static final vpn_always_on_summary_active:I = 0x7f122750

.field public static final vpn_always_on_summary_not_supported:I = 0x7f122751

.field public static final vpn_cancel:I = 0x7f122752

.field public static final vpn_cant_connect_message:I = 0x7f122753

.field public static final vpn_cant_connect_title:I = 0x7f122754

.field public static final vpn_configure:I = 0x7f122755

.field public static final vpn_connect:I = 0x7f122756

.field public static final vpn_connect_to:I = 0x7f122757

.field public static final vpn_create:I = 0x7f122758

.field public static final vpn_disconnect:I = 0x7f122759

.field public static final vpn_disconnect_confirm:I = 0x7f12275a

.field public static final vpn_disconnected:I = 0x7f12275b

.field public static final vpn_disconnected_summary:I = 0x7f12275c

.field public static final vpn_dns_servers:I = 0x7f12275d

.field public static final vpn_dns_servers_hint:I = 0x7f12275e

.field public static final vpn_done:I = 0x7f12275f

.field public static final vpn_edit:I = 0x7f122760

.field public static final vpn_enable:I = 0x7f122761

.field public static final vpn_first_always_on_vpn_message:I = 0x7f122762

.field public static final vpn_forget:I = 0x7f122763

.field public static final vpn_forget_long:I = 0x7f122764

.field public static final vpn_insecure_dialog_subtitle:I = 0x7f122765

.field public static final vpn_insecure_summary:I = 0x7f122766

.field public static final vpn_ipsec_ca_cert:I = 0x7f122767

.field public static final vpn_ipsec_identifier:I = 0x7f122768

.field public static final vpn_ipsec_secret:I = 0x7f122769

.field public static final vpn_ipsec_server_cert:I = 0x7f12276a

.field public static final vpn_ipsec_user_cert:I = 0x7f12276b

.field public static final vpn_l2tp_secret:I = 0x7f12276c

.field public static final vpn_lockdown_config_error:I = 0x7f12276d

.field public static final vpn_lockdown_none:I = 0x7f12276e

.field public static final vpn_lockdown_summary:I = 0x7f12276f

.field public static final vpn_menu_delete:I = 0x7f122770

.field public static final vpn_menu_edit:I = 0x7f122771

.field public static final vpn_menu_lockdown:I = 0x7f122772

.field public static final vpn_missing_cert:I = 0x7f122773

.field public static final vpn_mppe:I = 0x7f122774

.field public static final vpn_name:I = 0x7f122775

.field public static final vpn_no_ca_cert:I = 0x7f122776

.field public static final vpn_no_network:I = 0x7f122777

.field public static final vpn_no_selected:I = 0x7f122778

.field public static final vpn_no_server_cert:I = 0x7f122779

.field public static final vpn_no_vpns_added:I = 0x7f12277a

.field public static final vpn_not_used:I = 0x7f12277b

.field public static final vpn_off:I = 0x7f12277c

.field public static final vpn_on:I = 0x7f12277d

.field public static final vpn_password:I = 0x7f12277e

.field public static final vpn_replace:I = 0x7f12277f

.field public static final vpn_replace_always_on_vpn_disable_message:I = 0x7f122780

.field public static final vpn_replace_always_on_vpn_enable_message:I = 0x7f122781

.field public static final vpn_replace_vpn_message:I = 0x7f122782

.field public static final vpn_replace_vpn_title:I = 0x7f122783

.field public static final vpn_require_connection:I = 0x7f122784

.field public static final vpn_require_connection_title:I = 0x7f122785

.field public static final vpn_routes:I = 0x7f122786

.field public static final vpn_routes_hint:I = 0x7f122787

.field public static final vpn_safe:I = 0x7f122788

.field public static final vpn_save:I = 0x7f122789

.field public static final vpn_save_login:I = 0x7f12278a

.field public static final vpn_search_domains:I = 0x7f12278b

.field public static final vpn_server:I = 0x7f12278c

.field public static final vpn_set_screen_lock_content:I = 0x7f12278d

.field public static final vpn_set_screen_lock_title:I = 0x7f12278e

.field public static final vpn_set_vpn_title:I = 0x7f12278f

.field public static final vpn_settings_insecure_single:I = 0x7f122790

.field public static final vpn_settings_multiple_insecure_multiple_total:I = 0x7f122791

.field public static final vpn_settings_not_available:I = 0x7f122792

.field public static final vpn_settings_password:I = 0x7f122793

.field public static final vpn_settings_single_insecure_multiple_total:I = 0x7f122794

.field public static final vpn_settings_summary:I = 0x7f122795

.field public static final vpn_settings_title:I = 0x7f122796

.field public static final vpn_show_options:I = 0x7f122797

.field public static final vpn_title:I = 0x7f122798

.field public static final vpn_turn_on:I = 0x7f122799

.field public static final vpn_type:I = 0x7f12279a

.field public static final vpn_username:I = 0x7f12279b

.field public static final vpn_version:I = 0x7f12279c

.field public static final vr_listener_security_warning_summary:I = 0x7f12279d

.field public static final vr_listener_security_warning_title:I = 0x7f12279e

.field public static final vr_listeners_title:I = 0x7f12279f

.field public static final wait_for_debugger:I = 0x7f1227a0

.field public static final wait_for_debugger_summary:I = 0x7f1227a1

.field public static final wake_suppression_summary:I = 0x7f1227a2

.field public static final wake_suppression_title:I = 0x7f1227a3

.field public static final wakeup_and_sleep_settings_category_name:I = 0x7f1227a4

.field public static final wakeup_for_keyguard_notification_title:I = 0x7f1227a5

.field public static final wallet_header_title:I = 0x7f1227a6

.field public static final wallet_reset:I = 0x7f1227a7

.field public static final wallet_reset_cancel_btn:I = 0x7f1227a8

.field public static final wallet_reset_ok_btn:I = 0x7f1227a9

.field public static final wallet_reset_title:I = 0x7f1227aa

.field public static final wallpaper_attributions:I = 0x7f1227ab

.field public static final wallpaper_attributions_values:I = 0x7f1227ac

.field public static final wallpaper_dashboard_summary:I = 0x7f1227ad

.field public static final wallpaper_desktop:I = 0x7f1227ae

.field public static final wallpaper_live:I = 0x7f1227af

.field public static final wallpaper_lockscreen:I = 0x7f1227b0

.field public static final wallpaper_setting_title:I = 0x7f1227b1

.field public static final wallpaper_settings_fragment_title:I = 0x7f1227b2

.field public static final wallpaper_settings_summary_custom:I = 0x7f1227b3

.field public static final wallpaper_settings_summary_default:I = 0x7f1227b4

.field public static final wallpaper_settings_title:I = 0x7f1227b5

.field public static final wallpaper_suggestion_summary:I = 0x7f1227b6

.field public static final wallpaper_suggestion_title:I = 0x7f1227b7

.field public static final wapi_all_sel_mode:I = 0x7f1227b8

.field public static final wapi_auto_sel_cert:I = 0x7f1227b9

.field public static final wapi_auto_sel_cert_fail:I = 0x7f1227ba

.field public static final wapi_cert_is_not_exist:I = 0x7f1227bb

.field public static final wapi_cert_is_not_selected:I = 0x7f1227bc

.field public static final wapi_cert_manage_error:I = 0x7f1227bd

.field public static final wapi_cert_manage_summary:I = 0x7f1227be

.field public static final wapi_cert_manage_title:I = 0x7f1227bf

.field public static final wapi_cert_roaming:I = 0x7f1227c0

.field public static final wapi_cert_select:I = 0x7f1227c1

.field public static final wapi_no:I = 0x7f1227c2

.field public static final wapi_no_vaild_cert:I = 0x7f1227c3

.field public static final wapi_password_empty:I = 0x7f1227c4

.field public static final wapi_password_hex_format_add:I = 0x7f1227c5

.field public static final wapi_password_hex_format_invalid:I = 0x7f1227c6

.field public static final wapi_password_hex_length_invalid:I = 0x7f1227c7

.field public static final wapi_password_is_empty:I = 0x7f1227c8

.field public static final wapi_password_is_not_hex_format:I = 0x7f1227c9

.field public static final wapi_password_length_invalid:I = 0x7f1227ca

.field public static final wapi_password_length_is_max:I = 0x7f1227cb

.field public static final wapi_password_length_is_min:I = 0x7f1227cc

.field public static final wapi_password_length_is_min_to_max:I = 0x7f1227cd

.field public static final wapi_psk_type:I = 0x7f1227ce

.field public static final wapi_psk_type_ascii:I = 0x7f1227cf

.field public static final wapi_psk_type_hex:I = 0x7f1227d0

.field public static final wapi_yes:I = 0x7f1227d1

.field public static final warm_color:I = 0x7f1227d2

.field public static final warning_button_text:I = 0x7f1227d3

.field public static final web_action_enable_summary:I = 0x7f1227d4

.field public static final web_action_enable_title:I = 0x7f1227d5

.field public static final web_action_section_title:I = 0x7f1227d6

.field public static final webview_disabled_for_user:I = 0x7f1227d7

.field public static final webview_license_title:I = 0x7f1227d8

.field public static final webview_uninstalled_for_user:I = 0x7f1227d9

.field public static final wechat:I = 0x7f1227da

.field public static final wednesday:I = 0x7f1227db

.field public static final wednesday_short:I = 0x7f1227dc

.field public static final wednesday_shortest:I = 0x7f1227dd

.field public static final weight_heavy:I = 0x7f1227de

.field public static final weight_light:I = 0x7f1227df

.field public static final weight_setting_toast:I = 0x7f1227e0

.field public static final welcome:I = 0x7f1227e1

.field public static final wellbing_title:I = 0x7f1227e2

.field public static final wfc_disclaimer_agree_button_text:I = 0x7f1227e3

.field public static final wfc_disclaimer_disagree_text:I = 0x7f1227e4

.field public static final wfc_disclaimer_emergency_limitation_desc_text:I = 0x7f1227e5

.field public static final wfc_disclaimer_emergency_limitation_title_text:I = 0x7f1227e6

.field public static final wfc_disclaimer_location_desc_text:I = 0x7f1227e7

.field public static final wfc_disclaimer_location_title_text:I = 0x7f1227e8

.field public static final wfc_disclaimer_title_text:I = 0x7f1227e9

.field public static final wfd_available_devices:I = 0x7f1227ea

.field public static final wfd_disconnect_diag_msg:I = 0x7f1227eb

.field public static final wfd_disconnect_diag_title:I = 0x7f1227ec

.field public static final wfd_disconnect_multiple_diag_message:I = 0x7f1227ed

.field public static final wfd_enable_wifi_diag_title:I = 0x7f1227ee

.field public static final wfd_enabler_title:I = 0x7f1227ef

.field public static final wfd_menu_search:I = 0x7f1227f0

.field public static final wfd_menu_searching:I = 0x7f1227f1

.field public static final wfd_menu_teardown:I = 0x7f1227f2

.field public static final wfd_settings_description:I = 0x7f1227f3

.field public static final wfd_settings_title:I = 0x7f1227f4

.field public static final wfd_status_dialog_disconnect:I = 0x7f1227f5

.field public static final wfd_status_dialog_message:I = 0x7f1227f6

.field public static final wfd_status_dialog_title:I = 0x7f1227f7

.field public static final wfd_wps_keypad:I = 0x7f1227f8

.field public static final wfd_wps_pbc:I = 0x7f1227f9

.field public static final whetstone_level_reboot:I = 0x7f1227fa

.field public static final whetstone_level_reboot_OK:I = 0x7f1227fb

.field public static final whetstone_level_title:I = 0x7f1227fc

.field public static final widget_picker_title:I = 0x7f1227fd

.field public static final wifi:I = 0x7f1227fe

.field public static final wifi_access_points:I = 0x7f1227ff

.field public static final wifi_add_app_network_save_failed_summary:I = 0x7f122800

.field public static final wifi_add_app_networks_saved_summary:I = 0x7f122801

.field public static final wifi_add_app_networks_saving_summary:I = 0x7f122802

.field public static final wifi_add_app_networks_summary:I = 0x7f122803

.field public static final wifi_add_app_networks_title:I = 0x7f122804

.field public static final wifi_add_app_single_network_saved_summary:I = 0x7f122805

.field public static final wifi_add_app_single_network_saving_summary:I = 0x7f122806

.field public static final wifi_add_app_single_network_summary:I = 0x7f122807

.field public static final wifi_add_app_single_network_title:I = 0x7f122808

.field public static final wifi_add_network:I = 0x7f122809

.field public static final wifi_advanced_device_mac_address_title:I = 0x7f12280a

.field public static final wifi_advanced_ip_address_title:I = 0x7f12280b

.field public static final wifi_advanced_not_available:I = 0x7f12280c

.field public static final wifi_advanced_randomized_mac_address_disconnected_title:I = 0x7f12280d

.field public static final wifi_advanced_randomized_mac_address_title:I = 0x7f12280e

.field public static final wifi_advanced_settings_label:I = 0x7f12280f

.field public static final wifi_advanced_ssid_title:I = 0x7f122810

.field public static final wifi_advanced_titlebar:I = 0x7f122811

.field public static final wifi_advanced_toggle_description:I = 0x7f122812

.field public static final wifi_advanced_toggle_description_collapsed:I = 0x7f122813

.field public static final wifi_ap_2G:I = 0x7f122814

.field public static final wifi_ap_5G:I = 0x7f122815

.field public static final wifi_ap_band_config:I = 0x7f122816

.field public static final wifi_ap_band_select_one:I = 0x7f122817

.field public static final wifi_ap_choose:I = 0x7f122818

.field public static final wifi_ap_choose_2G:I = 0x7f122819

.field public static final wifi_ap_choose_5G:I = 0x7f12281a

.field public static final wifi_ap_choose_6G:I = 0x7f12281b

.field public static final wifi_ap_choose_auto:I = 0x7f12281c

.field public static final wifi_ap_choose_dual:I = 0x7f12281d

.field public static final wifi_ap_choose_vendor_dual_band:I = 0x7f12281e

.field public static final wifi_ap_hidden_ssid:I = 0x7f12281f

.field public static final wifi_ap_prefer_5G:I = 0x7f122820

.field public static final wifi_ap_prefer_6G:I = 0x7f122821

.field public static final wifi_ap_ssid_off:I = 0x7f122822

.field public static final wifi_ap_ssid_on:I = 0x7f122823

.field public static final wifi_ap_unable_to_handle_new_sta:I = 0x7f122824

.field public static final wifi_api_test:I = 0x7f122825

.field public static final wifi_ask_disable:I = 0x7f122826

.field public static final wifi_ask_enable:I = 0x7f122827

.field public static final wifi_assist:I = 0x7f122828

.field public static final wifi_assistant:I = 0x7f122829

.field public static final wifi_assistant_auto_confirm_data_help:I = 0x7f12282a

.field public static final wifi_assistant_auto_confirm_data_message:I = 0x7f12282b

.field public static final wifi_assistant_confirm_always:I = 0x7f12282c

.field public static final wifi_assistant_confirm_connection_title:I = 0x7f12282d

.field public static final wifi_assistant_data_prompt:I = 0x7f12282e

.field public static final wifi_assistant_data_prompt_summary:I = 0x7f12282f

.field public static final wifi_assistant_description:I = 0x7f122830

.field public static final wifi_assistant_disconnect:I = 0x7f122831

.field public static final wifi_assistant_explicitly_confirm_data_message:I = 0x7f122832

.field public static final wifi_assistant_explicitly_confirm_disconnect:I = 0x7f122833

.field public static final wifi_assistant_explicitly_confirm_wifi_message:I = 0x7f122834

.field public static final wifi_assistant_keep_connection:I = 0x7f122835

.field public static final wifi_assistant_keep_wifi_connection:I = 0x7f122836

.field public static final wifi_assistant_off:I = 0x7f122837

.field public static final wifi_assistant_ok:I = 0x7f122838

.field public static final wifi_assistant_on:I = 0x7f122839

.field public static final wifi_assistant_prompt:I = 0x7f12283a

.field public static final wifi_assistant_prompt_button:I = 0x7f12283b

.field public static final wifi_assistant_prompt_checkbox:I = 0x7f12283c

.field public static final wifi_assistant_prompt_summary:I = 0x7f12283d

.field public static final wifi_assistant_setup_wifi_assistant:I = 0x7f12283e

.field public static final wifi_assistant_summary:I = 0x7f12283f

.field public static final wifi_assistant_switch_data:I = 0x7f122840

.field public static final wifi_assistant_switch_wifi:I = 0x7f122841

.field public static final wifi_auto_connect:I = 0x7f122842

.field public static final wifi_auto_connect_summary:I = 0x7f122843

.field public static final wifi_auto_connect_title:I = 0x7f122844

.field public static final wifi_auto_sel_wapi_cert:I = 0x7f122845

.field public static final wifi_available_title:I = 0x7f122846

.field public static final wifi_avoid_poor_network_detection_summary:I = 0x7f122847

.field public static final wifi_badging_thresholds_default:I = 0x7f122848

.field public static final wifi_band_24ghz:I = 0x7f122849

.field public static final wifi_band_5ghz:I = 0x7f12284a

.field public static final wifi_band_60ghz:I = 0x7f12284b

.field public static final wifi_band_6ghz:I = 0x7f12284c

.field public static final wifi_calling_main_switch_title:I = 0x7f12284d

.field public static final wifi_calling_mode_cellular_preferred_summary:I = 0x7f12284e

.field public static final wifi_calling_mode_dialog_title:I = 0x7f12284f

.field public static final wifi_calling_mode_ims_preferred_summary:I = 0x7f122850

.field public static final wifi_calling_mode_title:I = 0x7f122851

.field public static final wifi_calling_mode_wifi_only_summary:I = 0x7f122852

.field public static final wifi_calling_mode_wifi_preferred_summary:I = 0x7f122853

.field public static final wifi_calling_not_supported:I = 0x7f122854

.field public static final wifi_calling_off_explanation:I = 0x7f122855

.field public static final wifi_calling_off_explanation_2:I = 0x7f122856

.field public static final wifi_calling_pref_managed_by_carrier:I = 0x7f122857

.field public static final wifi_calling_roaming_mode_dialog_title:I = 0x7f122858

.field public static final wifi_calling_roaming_mode_summary:I = 0x7f122859

.field public static final wifi_calling_roaming_mode_title:I = 0x7f12285a

.field public static final wifi_calling_settings_activation_instructions:I = 0x7f12285b

.field public static final wifi_calling_settings_title:I = 0x7f12285c

.field public static final wifi_calling_suggestion_summary:I = 0x7f12285d

.field public static final wifi_calling_suggestion_title:I = 0x7f12285e

.field public static final wifi_calling_summary:I = 0x7f12285f

.field public static final wifi_calling_turn_on:I = 0x7f122860

.field public static final wifi_cancel:I = 0x7f122861

.field public static final wifi_cant_connect:I = 0x7f122862

.field public static final wifi_cant_connect_to_ap:I = 0x7f122863

.field public static final wifi_carrier_connect:I = 0x7f122864

.field public static final wifi_carrier_content:I = 0x7f122865

.field public static final wifi_cellular_data_fallback_summary:I = 0x7f122866

.field public static final wifi_cellular_data_fallback_title:I = 0x7f122867

.field public static final wifi_certificate:I = 0x7f122868

.field public static final wifi_check_password_try_again:I = 0x7f122869

.field public static final wifi_click_login_wlan:I = 0x7f12286a

.field public static final wifi_click_share_wlan:I = 0x7f12286b

.field public static final wifi_config_info:I = 0x7f12286c

.field public static final wifi_configure_settings_preference_summary_wakeup_off:I = 0x7f12286d

.field public static final wifi_configure_settings_preference_summary_wakeup_on:I = 0x7f12286e

.field public static final wifi_configure_settings_preference_title:I = 0x7f12286f

.field public static final wifi_configured_ap_list:I = 0x7f122870

.field public static final wifi_connect:I = 0x7f122871

.field public static final wifi_connect_channel:I = 0x7f122872

.field public static final wifi_connect_cmcc_dialog_cancel:I = 0x7f122873

.field public static final wifi_connect_cmcc_dialog_confirm:I = 0x7f122874

.field public static final wifi_connect_cmcc_dialog_content:I = 0x7f122875

.field public static final wifi_connect_cmcc_dialog_not_remind:I = 0x7f122876

.field public static final wifi_connect_cmcc_dialog_title:I = 0x7f122877

.field public static final wifi_connect_failed_message:I = 0x7f122878

.field public static final wifi_connect_frequency:I = 0x7f122879

.field public static final wifi_connected_low_quality:I = 0x7f12287a

.field public static final wifi_connected_no_internet:I = 0x7f12287b

.field public static final wifi_connected_to_message:I = 0x7f12287c

.field public static final wifi_connecting:I = 0x7f12287d

.field public static final wifi_connecting_to_message:I = 0x7f12287e

.field public static final wifi_connection_management:I = 0x7f12287f

.field public static final wifi_context_menu_change_password:I = 0x7f122880

.field public static final wifi_context_menu_change_wapi_certificate:I = 0x7f122881

.field public static final wifi_context_menu_connect:I = 0x7f122882

.field public static final wifi_context_menu_forget:I = 0x7f122883

.field public static final wifi_coverage_extend:I = 0x7f122884

.field public static final wifi_coverage_extend_summary:I = 0x7f122885

.field public static final wifi_data_template:I = 0x7f122886

.field public static final wifi_data_usage:I = 0x7f122887

.field public static final wifi_datanetwork_switch_not_remind:I = 0x7f122888

.field public static final wifi_details_dns:I = 0x7f122889

.field public static final wifi_details_ipv6_address_header:I = 0x7f12288a

.field public static final wifi_details_subnet_mask:I = 0x7f12288b

.field public static final wifi_details_title:I = 0x7f12288c

.field public static final wifi_dialog_remind_type_title:I = 0x7f12288d

.field public static final wifi_direct_close_hotspot_hint:I = 0x7f12288e

.field public static final wifi_direct_close_slave_wifi_hint:I = 0x7f12288f

.field public static final wifi_disabled_by_recommendation_provider:I = 0x7f122890

.field public static final wifi_disabled_generic:I = 0x7f122891

.field public static final wifi_disabled_help:I = 0x7f122892

.field public static final wifi_disabled_network_failure:I = 0x7f122893

.field public static final wifi_disabled_password_failure:I = 0x7f122894

.field public static final wifi_disabled_wifi_failure:I = 0x7f122895

.field public static final wifi_disconnect_button_text:I = 0x7f122896

.field public static final wifi_disconnected:I = 0x7f122897

.field public static final wifi_disconnected_from:I = 0x7f122898

.field public static final wifi_disconnected_notification_summary:I = 0x7f122899

.field public static final wifi_disconnected_notification_title:I = 0x7f12289a

.field public static final wifi_display_autonomous_go:I = 0x7f12289b

.field public static final wifi_display_certification:I = 0x7f12289c

.field public static final wifi_display_certification_heading:I = 0x7f12289d

.field public static final wifi_display_certification_summary:I = 0x7f12289e

.field public static final wifi_display_details:I = 0x7f12289f

.field public static final wifi_display_enable_menu_item:I = 0x7f1228a0

.field public static final wifi_display_listen_channel:I = 0x7f1228a1

.field public static final wifi_display_listen_mode:I = 0x7f1228a2

.field public static final wifi_display_no_devices_found:I = 0x7f1228a3

.field public static final wifi_display_operating_channel:I = 0x7f1228a4

.field public static final wifi_display_options_done:I = 0x7f1228a5

.field public static final wifi_display_options_forget:I = 0x7f1228a6

.field public static final wifi_display_options_name:I = 0x7f1228a7

.field public static final wifi_display_options_title:I = 0x7f1228a8

.field public static final wifi_display_pause:I = 0x7f1228a9

.field public static final wifi_display_resume:I = 0x7f1228aa

.field public static final wifi_display_session_info:I = 0x7f1228ab

.field public static final wifi_display_settings_empty_list_wifi_display_disabled:I = 0x7f1228ac

.field public static final wifi_display_settings_title:I = 0x7f1228ad

.field public static final wifi_display_status_connected:I = 0x7f1228ae

.field public static final wifi_display_status_connecting:I = 0x7f1228af

.field public static final wifi_display_status_in_use:I = 0x7f1228b0

.field public static final wifi_display_status_not_available:I = 0x7f1228b1

.field public static final wifi_display_wps_config:I = 0x7f1228b2

.field public static final wifi_dns1:I = 0x7f1228b3

.field public static final wifi_dns1_hint:I = 0x7f1228b4

.field public static final wifi_dns2:I = 0x7f1228b5

.field public static final wifi_dns2_hint:I = 0x7f1228b6

.field public static final wifi_do_not_provide_eap_user_cert:I = 0x7f1228b7

.field public static final wifi_do_not_validate_eap_server:I = 0x7f1228b8

.field public static final wifi_do_not_validate_eap_server_warning:I = 0x7f1228b9

.field public static final wifi_dpp_add_another_device:I = 0x7f1228ba

.field public static final wifi_dpp_add_device:I = 0x7f1228bb

.field public static final wifi_dpp_add_device_to_network:I = 0x7f1228bc

.field public static final wifi_dpp_add_device_to_wifi:I = 0x7f1228bd

.field public static final wifi_dpp_center_qr_code:I = 0x7f1228be

.field public static final wifi_dpp_check_connection_try_again:I = 0x7f1228bf

.field public static final wifi_dpp_choose_different_network:I = 0x7f1228c0

.field public static final wifi_dpp_choose_network:I = 0x7f1228c1

.field public static final wifi_dpp_choose_network_to_connect_device:I = 0x7f1228c2

.field public static final wifi_dpp_connect_network_using_qr_code:I = 0x7f1228c3

.field public static final wifi_dpp_connecting:I = 0x7f1228c4

.field public static final wifi_dpp_could_not_add_device:I = 0x7f1228c5

.field public static final wifi_dpp_device_found:I = 0x7f1228c6

.field public static final wifi_dpp_failure_authentication_or_configuration:I = 0x7f1228c7

.field public static final wifi_dpp_failure_cannot_find_network:I = 0x7f1228c8

.field public static final wifi_dpp_failure_enrollee_authentication:I = 0x7f1228c9

.field public static final wifi_dpp_failure_enrollee_rejected_configuration:I = 0x7f1228ca

.field public static final wifi_dpp_failure_generic:I = 0x7f1228cb

.field public static final wifi_dpp_failure_not_compatible:I = 0x7f1228cc

.field public static final wifi_dpp_failure_not_supported:I = 0x7f1228cd

.field public static final wifi_dpp_failure_timeout:I = 0x7f1228ce

.field public static final wifi_dpp_hotspot_password:I = 0x7f1228cf

.field public static final wifi_dpp_lockscreen_title:I = 0x7f1228d0

.field public static final wifi_dpp_qr_code_is_not_valid_format:I = 0x7f1228d1

.field public static final wifi_dpp_scan_open_network_qr_code_with_another_device:I = 0x7f1228d2

.field public static final wifi_dpp_scan_qr_code:I = 0x7f1228d3

.field public static final wifi_dpp_scan_qr_code_join_network:I = 0x7f1228d4

.field public static final wifi_dpp_scan_qr_code_join_unknown_network:I = 0x7f1228d5

.field public static final wifi_dpp_scan_qr_code_with_another_device:I = 0x7f1228d6

.field public static final wifi_dpp_share_hotspot:I = 0x7f1228d7

.field public static final wifi_dpp_share_wifi:I = 0x7f1228d8

.field public static final wifi_dpp_sharing_wifi_with_this_device:I = 0x7f1228d9

.field public static final wifi_dpp_wifi_password:I = 0x7f1228da

.field public static final wifi_dpp_wifi_shared_with_device:I = 0x7f1228db

.field public static final wifi_eap_anonymous:I = 0x7f1228dc

.field public static final wifi_eap_ca_cert:I = 0x7f1228dd

.field public static final wifi_eap_domain:I = 0x7f1228de

.field public static final wifi_eap_identity:I = 0x7f1228df

.field public static final wifi_eap_method:I = 0x7f1228e0

.field public static final wifi_eap_ocsp:I = 0x7f1228e1

.field public static final wifi_eap_options_advanced:I = 0x7f1228e2

.field public static final wifi_eap_options_simple:I = 0x7f1228e3

.field public static final wifi_eap_sim_simcard:I = 0x7f1228e4

.field public static final wifi_eap_user_cert:I = 0x7f1228e5

.field public static final wifi_empty_list_user_restricted:I = 0x7f1228e6

.field public static final wifi_empty_list_wifi_off:I = 0x7f1228e7

.field public static final wifi_empty_list_wifi_on:I = 0x7f1228e8

.field public static final wifi_enable:I = 0x7f1228e9

.field public static final wifi_enable_watchdog_service_summary:I = 0x7f1228ea

.field public static final wifi_enhanced_handover:I = 0x7f1228eb

.field public static final wifi_enhanced_handover_summary:I = 0x7f1228ec

.field public static final wifi_error:I = 0x7f1228ed

.field public static final wifi_expiry_time:I = 0x7f1228ee

.field public static final wifi_fail_to_scan:I = 0x7f1228ef

.field public static final wifi_failed_connect_message:I = 0x7f1228f0

.field public static final wifi_failed_forget_message:I = 0x7f1228f1

.field public static final wifi_failed_save_message:I = 0x7f1228f2

.field public static final wifi_forget:I = 0x7f1228f3

.field public static final wifi_forget_dialog_message:I = 0x7f1228f4

.field public static final wifi_forget_dialog_title:I = 0x7f1228f5

.field public static final wifi_frequency:I = 0x7f1228f6

.field public static final wifi_gateway:I = 0x7f1228f7

.field public static final wifi_gateway_hint:I = 0x7f1228f8

.field public static final wifi_generation:I = 0x7f1228f9

.field public static final wifi_generation_icon_display:I = 0x7f1228fa

.field public static final wifi_generation_status:I = 0x7f1228fb

.field public static final wifi_generation_summary:I = 0x7f1228fc

.field public static final wifi_generation_title:I = 0x7f1228fd

.field public static final wifi_hand_sel_wapi_cert:I = 0x7f1228fe

.field public static final wifi_hidden_gbk:I = 0x7f1228ff

.field public static final wifi_hidden_network:I = 0x7f122900

.field public static final wifi_hidden_network_warning:I = 0x7f122901

.field public static final wifi_hide_password:I = 0x7f122902

.field public static final wifi_hotspot_ap_band_title:I = 0x7f122903

.field public static final wifi_hotspot_auto_off_summary:I = 0x7f122904

.field public static final wifi_hotspot_auto_off_title:I = 0x7f122905

.field public static final wifi_hotspot_checkbox_text:I = 0x7f122906

.field public static final wifi_hotspot_configure_ap_text:I = 0x7f122907

.field public static final wifi_hotspot_configure_ap_text_summary:I = 0x7f122908

.field public static final wifi_hotspot_connect:I = 0x7f122909

.field public static final wifi_hotspot_footer_info_local_only:I = 0x7f12290a

.field public static final wifi_hotspot_footer_info_regular:I = 0x7f12290b

.field public static final wifi_hotspot_message:I = 0x7f12290c

.field public static final wifi_hotspot_name_summary_connected:I = 0x7f12290d

.field public static final wifi_hotspot_name_summary_connecting:I = 0x7f12290e

.field public static final wifi_hotspot_name_title:I = 0x7f12290f

.field public static final wifi_hotspot_no_password_subtext:I = 0x7f122910

.field public static final wifi_hotspot_off_subtext:I = 0x7f122911

.field public static final wifi_hotspot_on_local_only_subtext:I = 0x7f122912

.field public static final wifi_hotspot_password_title:I = 0x7f122913

.field public static final wifi_hotspot_switch_off_text:I = 0x7f122914

.field public static final wifi_hotspot_switch_on_text:I = 0x7f122915

.field public static final wifi_hotspot_tethering_on_subtext:I = 0x7f122916

.field public static final wifi_hotspot_title:I = 0x7f122917

.field public static final wifi_in_airplane_mode:I = 0x7f122918

.field public static final wifi_info_reconnect:I = 0x7f122919

.field public static final wifi_install_credentials:I = 0x7f12291a

.field public static final wifi_ip_address:I = 0x7f12291b

.field public static final wifi_ip_address_hint:I = 0x7f12291c

.field public static final wifi_ip_settings:I = 0x7f12291d

.field public static final wifi_ip_settings_invalid_dns:I = 0x7f12291e

.field public static final wifi_ip_settings_invalid_gateway:I = 0x7f12291f

.field public static final wifi_ip_settings_invalid_ip_address:I = 0x7f122920

.field public static final wifi_ip_settings_invalid_network_prefix_length:I = 0x7f122921

.field public static final wifi_ip_settings_menu_cancel:I = 0x7f122922

.field public static final wifi_ip_settings_menu_save:I = 0x7f122923

.field public static final wifi_is_off:I = 0x7f122924

.field public static final wifi_limit_optimizations_summary:I = 0x7f122925

.field public static final wifi_limited_connection:I = 0x7f122926

.field public static final wifi_link_turbo:I = 0x7f122927

.field public static final wifi_link_turbo_app_settings_summary:I = 0x7f122928

.field public static final wifi_link_turbo_app_settings_title:I = 0x7f122929

.field public static final wifi_link_turbo_customize:I = 0x7f12292a

.field public static final wifi_link_turbo_customize_summary:I = 0x7f12292b

.field public static final wifi_link_turbo_list:I = 0x7f12292c

.field public static final wifi_link_turbo_mode:I = 0x7f12292d

.field public static final wifi_link_turbo_no_app:I = 0x7f12292e

.field public static final wifi_link_turbo_off:I = 0x7f12292f

.field public static final wifi_link_turbo_on:I = 0x7f122930

.field public static final wifi_link_turbo_recommendation:I = 0x7f122931

.field public static final wifi_link_turbo_recommendation_summary:I = 0x7f122932

.field public static final wifi_link_turbo_summary:I = 0x7f122933

.field public static final wifi_menu_advanced:I = 0x7f122934

.field public static final wifi_menu_configure:I = 0x7f122935

.field public static final wifi_menu_connect:I = 0x7f122936

.field public static final wifi_menu_forget:I = 0x7f122937

.field public static final wifi_menu_modify:I = 0x7f122938

.field public static final wifi_menu_more_options:I = 0x7f122939

.field public static final wifi_menu_p2p:I = 0x7f12293a

.field public static final wifi_menu_remember:I = 0x7f12293b

.field public static final wifi_menu_scan:I = 0x7f12293c

.field public static final wifi_menu_wfd:I = 0x7f12293d

.field public static final wifi_menu_wps_pin:I = 0x7f12293e

.field public static final wifi_metered_label:I = 0x7f12293f

.field public static final wifi_metered_title:I = 0x7f122940

.field public static final wifi_modify:I = 0x7f122941

.field public static final wifi_more:I = 0x7f122942

.field public static final wifi_multi_network_acceleration:I = 0x7f122943

.field public static final wifi_multi_network_acceleration_title:I = 0x7f122944

.field public static final wifi_multiple_cert_added:I = 0x7f122945

.field public static final wifi_network_optimization:I = 0x7f122946

.field public static final wifi_network_prefix_length:I = 0x7f122947

.field public static final wifi_network_prefix_length_hint:I = 0x7f122948

.field public static final wifi_no_domain_warning:I = 0x7f122949

.field public static final wifi_no_internet:I = 0x7f12294a

.field public static final wifi_no_internet_no_reconnect:I = 0x7f12294b

.field public static final wifi_no_related_sim_card:I = 0x7f12294c

.field public static final wifi_no_sim_card:I = 0x7f12294d

.field public static final wifi_no_user_cert_warning:I = 0x7f12294e

.field public static final wifi_non_persistent_mac_randomization:I = 0x7f12294f

.field public static final wifi_non_persistent_mac_randomization_summary:I = 0x7f122950

.field public static final wifi_not_in_range:I = 0x7f122951

.field public static final wifi_not_in_range_message:I = 0x7f122952

.field public static final wifi_notify_cmcc_connected_summary:I = 0x7f122953

.field public static final wifi_notify_cmcc_connected_title:I = 0x7f122954

.field public static final wifi_notify_open_networks:I = 0x7f122955

.field public static final wifi_notify_open_networks_summary:I = 0x7f122956

.field public static final wifi_off_airplane_on_cancel:I = 0x7f122957

.field public static final wifi_off_airplane_on_confirm:I = 0x7f122958

.field public static final wifi_off_airplane_on_content:I = 0x7f122959

.field public static final wifi_off_airplane_on_not_remind:I = 0x7f12295a

.field public static final wifi_off_airplane_on_title:I = 0x7f12295b

.field public static final wifi_on_time:I = 0x7f12295c

.field public static final wifi_other_connection_mode:I = 0x7f12295d

.field public static final wifi_p2p_cancel_connect_message:I = 0x7f12295e

.field public static final wifi_p2p_cancel_connect_title:I = 0x7f12295f

.field public static final wifi_p2p_delete_group_message:I = 0x7f122960

.field public static final wifi_p2p_device_info:I = 0x7f122961

.field public static final wifi_p2p_disconnect_message:I = 0x7f122962

.field public static final wifi_p2p_disconnect_multiple_message:I = 0x7f122963

.field public static final wifi_p2p_disconnect_title:I = 0x7f122964

.field public static final wifi_p2p_failed_connect_message:I = 0x7f122965

.field public static final wifi_p2p_failed_rename_message:I = 0x7f122966

.field public static final wifi_p2p_menu_rename:I = 0x7f122967

.field public static final wifi_p2p_menu_search:I = 0x7f122968

.field public static final wifi_p2p_menu_searching:I = 0x7f122969

.field public static final wifi_p2p_peer_devices:I = 0x7f12296a

.field public static final wifi_p2p_persist_network:I = 0x7f12296b

.field public static final wifi_p2p_remembered_groups:I = 0x7f12296c

.field public static final wifi_p2p_settings_title:I = 0x7f12296d

.field public static final wifi_p2p_wps_setup:I = 0x7f12296e

.field public static final wifi_passpoint_expired:I = 0x7f12296f

.field public static final wifi_password:I = 0x7f122970

.field public static final wifi_poor_network_detection:I = 0x7f122971

.field public static final wifi_poor_network_detection_summary:I = 0x7f122972

.field public static final wifi_primary:I = 0x7f122973

.field public static final wifi_priority_changed:I = 0x7f122974

.field public static final wifi_priority_label:I = 0x7f122975

.field public static final wifi_priority_settings_label:I = 0x7f122976

.field public static final wifi_priority_settings_summary:I = 0x7f122977

.field public static final wifi_priority_settings_title:I = 0x7f122978

.field public static final wifi_priority_type_summary:I = 0x7f122979

.field public static final wifi_priority_type_title:I = 0x7f12297a

.field public static final wifi_privacy_settings:I = 0x7f12297b

.field public static final wifi_privacy_settings_ephemeral_summary:I = 0x7f12297c

.field public static final wifi_property:I = 0x7f12297d

.field public static final wifi_protection_on:I = 0x7f12297e

.field public static final wifi_protection_settings_title:I = 0x7f12297f

.field public static final wifi_protection_tip:I = 0x7f122980

.field public static final wifi_quick_toggle_summary:I = 0x7f122981

.field public static final wifi_quick_toggle_title:I = 0x7f122982

.field public static final wifi_remembered:I = 0x7f122983

.field public static final wifi_remembered_disabled_auto_connect:I = 0x7f122984

.field public static final wifi_require_sim_card_to_connect:I = 0x7f122985

.field public static final wifi_require_specific_sim_card_to_connect:I = 0x7f122986

.field public static final wifi_required_info_text:I = 0x7f122987

.field public static final wifi_sap_no_channel_error:I = 0x7f122988

.field public static final wifi_save:I = 0x7f122989

.field public static final wifi_saved_access_point:I = 0x7f12298a

.field public static final wifi_saved_access_points_label:I = 0x7f12298b

.field public static final wifi_saved_other_networks_tab:I = 0x7f12298c

.field public static final wifi_scan_always_available:I = 0x7f12298d

.field public static final wifi_scan_always_available_summary:I = 0x7f12298e

.field public static final wifi_scan_always_confirm_allow:I = 0x7f12298f

.field public static final wifi_scan_always_confirm_deny:I = 0x7f122990

.field public static final wifi_scan_always_turn_on_message_unknown:I = 0x7f122991

.field public static final wifi_scan_always_turnoff_message:I = 0x7f122992

.field public static final wifi_scan_always_turnon_message:I = 0x7f122993

.field public static final wifi_scan_change:I = 0x7f122994

.field public static final wifi_scan_notify_message:I = 0x7f122995

.field public static final wifi_scan_notify_remember_choice:I = 0x7f122996

.field public static final wifi_scan_notify_text:I = 0x7f122997

.field public static final wifi_scan_notify_text_scanning_off:I = 0x7f122998

.field public static final wifi_scan_throttling:I = 0x7f122999

.field public static final wifi_scan_throttling_summary:I = 0x7f12299a

.field public static final wifi_security:I = 0x7f12299b

.field public static final wifi_security_WAPI_CERT:I = 0x7f12299c

.field public static final wifi_security_WAPI_PSK:I = 0x7f12299d

.field public static final wifi_security_dpp:I = 0x7f12299e

.field public static final wifi_security_eap:I = 0x7f12299f

.field public static final wifi_security_eap_suiteb:I = 0x7f1229a0

.field public static final wifi_security_eap_wpa:I = 0x7f1229a1

.field public static final wifi_security_eap_wpa2_wpa3:I = 0x7f1229a2

.field public static final wifi_security_eap_wpa3:I = 0x7f1229a3

.field public static final wifi_security_eap_wpa_wpa2:I = 0x7f1229a4

.field public static final wifi_security_none:I = 0x7f1229a5

.field public static final wifi_security_none_owe:I = 0x7f1229a6

.field public static final wifi_security_owe:I = 0x7f1229a7

.field public static final wifi_security_passpoint:I = 0x7f1229a8

.field public static final wifi_security_psk_generic:I = 0x7f1229a9

.field public static final wifi_security_psk_sae:I = 0x7f1229aa

.field public static final wifi_security_sae:I = 0x7f1229ab

.field public static final wifi_security_short_WAPI_CERT:I = 0x7f1229ac

.field public static final wifi_security_short_WAPI_PSK:I = 0x7f1229ad

.field public static final wifi_security_short_dpp:I = 0x7f1229ae

.field public static final wifi_security_short_eap:I = 0x7f1229af

.field public static final wifi_security_short_eap_suiteb:I = 0x7f1229b0

.field public static final wifi_security_short_eap_wpa:I = 0x7f1229b1

.field public static final wifi_security_short_eap_wpa2_wpa3:I = 0x7f1229b2

.field public static final wifi_security_short_none_owe:I = 0x7f1229b3

.field public static final wifi_security_short_owe:I = 0x7f1229b4

.field public static final wifi_security_short_psk_generic:I = 0x7f1229b5

.field public static final wifi_security_short_psk_sae:I = 0x7f1229b6

.field public static final wifi_security_short_sae:I = 0x7f1229b7

.field public static final wifi_security_short_wep:I = 0x7f1229b8

.field public static final wifi_security_short_wpa:I = 0x7f1229b9

.field public static final wifi_security_short_wpa2:I = 0x7f1229ba

.field public static final wifi_security_short_wpa_wpa2:I = 0x7f1229bb

.field public static final wifi_security_wapi_cert:I = 0x7f1229bc

.field public static final wifi_security_wapi_psk:I = 0x7f1229bd

.field public static final wifi_security_wep:I = 0x7f1229be

.field public static final wifi_security_wpa:I = 0x7f1229bf

.field public static final wifi_security_wpa2:I = 0x7f1229c0

.field public static final wifi_security_wpa_wpa2:I = 0x7f1229c1

.field public static final wifi_select_network:I = 0x7f1229c2

.field public static final wifi_setting_category:I = 0x7f1229c3

.field public static final wifi_setting_frequency_band_error:I = 0x7f1229c4

.field public static final wifi_setting_frequency_band_summary:I = 0x7f1229c5

.field public static final wifi_setting_frequency_band_title:I = 0x7f1229c6

.field public static final wifi_setting_on_during_sleep_title:I = 0x7f1229c7

.field public static final wifi_setting_sleep_policy_error:I = 0x7f1229c8

.field public static final wifi_setting_sleep_policy_title:I = 0x7f1229c9

.field public static final wifi_settings:I = 0x7f1229ca

.field public static final wifi_settings_category:I = 0x7f1229cb

.field public static final wifi_settings_primary_switch_title:I = 0x7f1229cc

.field public static final wifi_settings_scanning_required_enabled:I = 0x7f1229cd

.field public static final wifi_settings_scanning_required_info:I = 0x7f1229ce

.field public static final wifi_settings_scanning_required_summary:I = 0x7f1229cf

.field public static final wifi_settings_scanning_required_title:I = 0x7f1229d0

.field public static final wifi_settings_scanning_required_turn_on:I = 0x7f1229d1

.field public static final wifi_settings_summary:I = 0x7f1229d2

.field public static final wifi_settings_title:I = 0x7f1229d3

.field public static final wifi_setup_cancel:I = 0x7f1229d4

.field public static final wifi_setup_description_connected:I = 0x7f1229d5

.field public static final wifi_setup_detail:I = 0x7f1229d6

.field public static final wifi_setup_eap_not_supported:I = 0x7f1229d7

.field public static final wifi_setup_title:I = 0x7f1229d8

.field public static final wifi_setup_title_connected_network:I = 0x7f1229d9

.field public static final wifi_setup_title_connecting_network:I = 0x7f1229da

.field public static final wifi_setup_title_editing_network:I = 0x7f1229db

.field public static final wifi_setup_wizard_title:I = 0x7f1229dc

.field public static final wifi_setup_wps:I = 0x7f1229dd

.field public static final wifi_share:I = 0x7f1229de

.field public static final wifi_share_action_turn_off:I = 0x7f1229df

.field public static final wifi_share_action_turn_on:I = 0x7f1229e0

.field public static final wifi_share_action_turn_on_summary:I = 0x7f1229e1

.field public static final wifi_share_add:I = 0x7f1229e2

.field public static final wifi_share_add_summary:I = 0x7f1229e3

.field public static final wifi_share_connect:I = 0x7f1229e4

.field public static final wifi_share_connected_summary:I = 0x7f1229e5

.field public static final wifi_share_connected_title:I = 0x7f1229e6

.field public static final wifi_share_current_network:I = 0x7f1229e7

.field public static final wifi_share_know:I = 0x7f1229e8

.field public static final wifi_share_list_empty:I = 0x7f1229e9

.field public static final wifi_share_login:I = 0x7f1229ea

.field public static final wifi_share_login_summary:I = 0x7f1229eb

.field public static final wifi_share_notification_summary:I = 0x7f1229ec

.field public static final wifi_share_other_network:I = 0x7f1229ed

.field public static final wifi_share_qrcode_finish:I = 0x7f1229ee

.field public static final wifi_share_qrcode_summary:I = 0x7f1229ef

.field public static final wifi_share_qrcode_title:I = 0x7f1229f0

.field public static final wifi_share_show_network:I = 0x7f1229f1

.field public static final wifi_share_summary1:I = 0x7f1229f2

.field public static final wifi_share_summary2:I = 0x7f1229f3

.field public static final wifi_share_summary3:I = 0x7f1229f4

.field public static final wifi_share_turn_off:I = 0x7f1229f5

.field public static final wifi_share_turn_on:I = 0x7f1229f6

.field public static final wifi_share_upload:I = 0x7f1229f7

.field public static final wifi_share_upload_finish:I = 0x7f1229f8

.field public static final wifi_share_uploading:I = 0x7f1229f9

.field public static final wifi_shared:I = 0x7f1229fa

.field public static final wifi_show_advanced:I = 0x7f1229fb

.field public static final wifi_show_password:I = 0x7f1229fc

.field public static final wifi_sign_in_button_text:I = 0x7f1229fd

.field public static final wifi_signal:I = 0x7f1229fe

.field public static final wifi_signal_found_msg:I = 0x7f1229ff

.field public static final wifi_speed:I = 0x7f122a00

.field public static final wifi_ssid:I = 0x7f122a01

.field public static final wifi_ssid_hint:I = 0x7f122a02

.field public static final wifi_ssid_is_empty:I = 0x7f122a03

.field public static final wifi_ssid_length_is_max:I = 0x7f122a04

.field public static final wifi_ssid_too_long:I = 0x7f122a05

.field public static final wifi_starting:I = 0x7f122a06

.field public static final wifi_state_disabled:I = 0x7f122a07

.field public static final wifi_state_disabling:I = 0x7f122a08

.field public static final wifi_state_enabled:I = 0x7f122a09

.field public static final wifi_state_enabling:I = 0x7f122a0a

.field public static final wifi_state_label:I = 0x7f122a0b

.field public static final wifi_state_unknown:I = 0x7f122a0c

.field public static final wifi_status:I = 0x7f122a0d

.field public static final wifi_status_mac_randomized:I = 0x7f122a0e

.field public static final wifi_status_no_internet:I = 0x7f122a0f

.field public static final wifi_status_sign_in_required:I = 0x7f122a10

.field public static final wifi_status_test:I = 0x7f122a11

.field public static final wifi_stopping:I = 0x7f122a12

.field public static final wifi_subnet_mask:I = 0x7f122a13

.field public static final wifi_subscribed_access_points_tab:I = 0x7f122a14

.field public static final wifi_subscription:I = 0x7f122a15

.field public static final wifi_subscription_summary:I = 0x7f122a16

.field public static final wifi_suspend_efficiency_title:I = 0x7f122a17

.field public static final wifi_suspend_optimizations:I = 0x7f122a18

.field public static final wifi_suspend_optimizations_summary:I = 0x7f122a19

.field public static final wifi_switch_away_when_unvalidated:I = 0x7f122a1a

.field public static final wifi_switch_to_gsm_message:I = 0x7f122a1b

.field public static final wifi_switch_to_gsm_title:I = 0x7f122a1c

.field public static final wifi_tap_to_sign_in:I = 0x7f122a1d

.field public static final wifi_tether_carrier_unsupport_dialog_content:I = 0x7f122a1e

.field public static final wifi_tether_carrier_unsupport_dialog_title:I = 0x7f122a1f

.field public static final wifi_tether_checkbox_kddi_text:I = 0x7f122a20

.field public static final wifi_tether_checkbox_text:I = 0x7f122a21

.field public static final wifi_tether_configure_ap_kddi_text:I = 0x7f122a22

.field public static final wifi_tether_configure_ap_text:I = 0x7f122a23

.field public static final wifi_tether_configure_ssid_default:I = 0x7f122a24

.field public static final wifi_tether_configure_subtext:I = 0x7f122a25

.field public static final wifi_tether_enabled_nosecurity_summary:I = 0x7f122a26

.field public static final wifi_tether_enabled_subtext:I = 0x7f122a27

.field public static final wifi_tether_enabled_summary:I = 0x7f122a28

.field public static final wifi_tether_failed_subtext:I = 0x7f122a29

.field public static final wifi_tether_setting_title:I = 0x7f122a2a

.field public static final wifi_tether_settings_title:I = 0x7f122a2b

.field public static final wifi_tether_starting:I = 0x7f122a2c

.field public static final wifi_tether_stopping:I = 0x7f122a2d

.field public static final wifi_time_remaining:I = 0x7f122a2e

.field public static final wifi_traffic_priority:I = 0x7f122a2f

.field public static final wifi_traffic_priority_extreme:I = 0x7f122a30

.field public static final wifi_traffic_priority_extreme_summary:I = 0x7f122a31

.field public static final wifi_traffic_priority_off:I = 0x7f122a32

.field public static final wifi_traffic_priority_on:I = 0x7f122a33

.field public static final wifi_traffic_priority_regular:I = 0x7f122a34

.field public static final wifi_traffic_priority_regular_summary:I = 0x7f122a35

.field public static final wifi_traffic_priority_summary:I = 0x7f122a36

.field public static final wifi_trust_on_first_use:I = 0x7f122a37

.field public static final wifi_turned_on_message:I = 0x7f122a38

.field public static final wifi_type_approval:I = 0x7f122a39

.field public static final wifi_type_approval_dialog_title:I = 0x7f122a3a

.field public static final wifi_type_approval_title:I = 0x7f122a3b

.field public static final wifi_type_title:I = 0x7f122a3c

.field public static final wifi_unchanged:I = 0x7f122a3d

.field public static final wifi_unmetered_label:I = 0x7f122a3e

.field public static final wifi_unspecified:I = 0x7f122a3f

.field public static final wifi_update:I = 0x7f122a40

.field public static final wifi_use_system_certs:I = 0x7f122a41

.field public static final wifi_venue_website_button_text:I = 0x7f122a42

.field public static final wifi_verbose_logging:I = 0x7f122a43

.field public static final wifi_verbose_logging_summary:I = 0x7f122a44

.field public static final wifi_wakeup:I = 0x7f122a45

.field public static final wifi_wakeup_location_summary:I = 0x7f122a46

.field public static final wifi_wakeup_negative_button:I = 0x7f122a47

.field public static final wifi_wakeup_positive_button:I = 0x7f122a48

.field public static final wifi_wakeup_scan_summary:I = 0x7f122a49

.field public static final wifi_wakeup_summary:I = 0x7f122a4a

.field public static final wifi_wakeup_summary_no_location:I = 0x7f122a4b

.field public static final wifi_wakeup_summary_scanning_disabled:I = 0x7f122a4c

.field public static final wifi_wakeup_summary_scoring_disabled:I = 0x7f122a4d

.field public static final wifi_wakeup_title:I = 0x7f122a4e

.field public static final wifi_wapi_as_cert:I = 0x7f122a4f

.field public static final wifi_wapi_as_certificate:I = 0x7f122a50

.field public static final wifi_wapi_cert_cancel_button:I = 0x7f122a51

.field public static final wifi_wapi_cert_create_subdir:I = 0x7f122a52

.field public static final wifi_wapi_cert_delet_subdir:I = 0x7f122a53

.field public static final wifi_wapi_cert_install:I = 0x7f122a54

.field public static final wifi_wapi_cert_install_button:I = 0x7f122a55

.field public static final wifi_wapi_cert_mgmt_as_dont_exist:I = 0x7f122a56

.field public static final wifi_wapi_cert_mgmt_as_format_is_wrong:I = 0x7f122a57

.field public static final wifi_wapi_cert_mgmt_as_name_is_empty:I = 0x7f122a58

.field public static final wifi_wapi_cert_mgmt_subdir_create_fail:I = 0x7f122a59

.field public static final wifi_wapi_cert_mgmt_subdir_exist:I = 0x7f122a5a

.field public static final wifi_wapi_cert_mgmt_subdir_name_is_empty:I = 0x7f122a5b

.field public static final wifi_wapi_cert_mgmt_titlebar:I = 0x7f122a5c

.field public static final wifi_wapi_cert_mgmt_user_dont_exist:I = 0x7f122a5d

.field public static final wifi_wapi_cert_mgmt_user_format_is_wrong:I = 0x7f122a5e

.field public static final wifi_wapi_cert_mgmt_user_name_is_empty:I = 0x7f122a5f

.field public static final wifi_wapi_cert_mode_hand_text:I = 0x7f122a60

.field public static final wifi_wapi_cert_uninstall:I = 0x7f122a61

.field public static final wifi_wapi_cert_uninstall_button:I = 0x7f122a62

.field public static final wifi_wapi_certification_dont_exist:I = 0x7f122a63

.field public static final wifi_wapi_fail_to_auth:I = 0x7f122a64

.field public static final wifi_wapi_fail_to_init:I = 0x7f122a65

.field public static final wifi_wapi_psk_type:I = 0x7f122a66

.field public static final wifi_wapi_user_cert:I = 0x7f122a67

.field public static final wifi_wapi_user_certificate:I = 0x7f122a68

.field public static final wifi_wps_available_first_item:I = 0x7f122a69

.field public static final wifi_wps_available_second_item:I = 0x7f122a6a

.field public static final wifi_wps_connected:I = 0x7f122a6b

.field public static final wifi_wps_onstart_pbc:I = 0x7f122a6c

.field public static final wifi_wps_onstart_pin:I = 0x7f122a6d

.field public static final wifi_wps_overlap_error:I = 0x7f122a6e

.field public static final wifi_wps_setup_title:I = 0x7f122a6f

.field public static final wifitrackerlib_admin_restricted_network:I = 0x7f122a70

.field public static final wifitrackerlib_auto_connect_disable:I = 0x7f122a71

.field public static final wifitrackerlib_available_via_app:I = 0x7f122a72

.field public static final wifitrackerlib_certinstaller_package:I = 0x7f122a73

.field public static final wifitrackerlib_connected_via_app:I = 0x7f122a74

.field public static final wifitrackerlib_help_url_imsi_protection:I = 0x7f122a75

.field public static final wifitrackerlib_imsi_protection_warning:I = 0x7f122a76

.field public static final wifitrackerlib_no_attribution_annotation_packages:I = 0x7f122a77

.field public static final wifitrackerlib_osu_completing_sign_up:I = 0x7f122a78

.field public static final wifitrackerlib_osu_connect_failed:I = 0x7f122a79

.field public static final wifitrackerlib_osu_opening_provider:I = 0x7f122a7a

.field public static final wifitrackerlib_osu_sign_up_complete:I = 0x7f122a7b

.field public static final wifitrackerlib_osu_sign_up_failed:I = 0x7f122a7c

.field public static final wifitrackerlib_private_dns_broken:I = 0x7f122a7d

.field public static final wifitrackerlib_saved_network:I = 0x7f122a7e

.field public static final wifitrackerlib_settings_package:I = 0x7f122a7f

.field public static final wifitrackerlib_summary_separator:I = 0x7f122a80

.field public static final wifitrackerlib_tap_to_renew_subscription_and_connect:I = 0x7f122a81

.field public static final wifitrackerlib_tap_to_sign_up:I = 0x7f122a82

.field public static final wifitrackerlib_wifi_ap_unable_to_handle_new_sta:I = 0x7f122a83

.field public static final wifitrackerlib_wifi_check_password_try_again:I = 0x7f122a84

.field public static final wifitrackerlib_wifi_connected_cannot_provide_internet:I = 0x7f122a85

.field public static final wifitrackerlib_wifi_disabled_generic:I = 0x7f122a86

.field public static final wifitrackerlib_wifi_disabled_network_failure:I = 0x7f122a87

.field public static final wifitrackerlib_wifi_disabled_password_failure:I = 0x7f122a88

.field public static final wifitrackerlib_wifi_disconnected:I = 0x7f122a89

.field public static final wifitrackerlib_wifi_limited_connection:I = 0x7f122a8a

.field public static final wifitrackerlib_wifi_mbo_assoc_disallowed_cannot_connect:I = 0x7f122a8b

.field public static final wifitrackerlib_wifi_mbo_assoc_disallowed_max_num_sta_associated:I = 0x7f122a8c

.field public static final wifitrackerlib_wifi_mbo_oce_assoc_disallowed_insufficient_rssi:I = 0x7f122a8d

.field public static final wifitrackerlib_wifi_metered_label:I = 0x7f122a8e

.field public static final wifitrackerlib_wifi_network_not_found:I = 0x7f122a8f

.field public static final wifitrackerlib_wifi_no_internet:I = 0x7f122a90

.field public static final wifitrackerlib_wifi_no_internet_no_reconnect:I = 0x7f122a91

.field public static final wifitrackerlib_wifi_passpoint_expired:I = 0x7f122a92

.field public static final wifitrackerlib_wifi_poor_channel_conditions:I = 0x7f122a93

.field public static final wifitrackerlib_wifi_remembered:I = 0x7f122a94

.field public static final wifitrackerlib_wifi_security_eap_suiteb:I = 0x7f122a95

.field public static final wifitrackerlib_wifi_security_eap_wpa3:I = 0x7f122a96

.field public static final wifitrackerlib_wifi_security_eap_wpa_wpa2:I = 0x7f122a97

.field public static final wifitrackerlib_wifi_security_eap_wpa_wpa2_wpa3:I = 0x7f122a98

.field public static final wifitrackerlib_wifi_security_none:I = 0x7f122a99

.field public static final wifitrackerlib_wifi_security_owe:I = 0x7f122a9a

.field public static final wifitrackerlib_wifi_security_passpoint:I = 0x7f122a9b

.field public static final wifitrackerlib_wifi_security_sae:I = 0x7f122a9c

.field public static final wifitrackerlib_wifi_security_short_eap_suiteb:I = 0x7f122a9d

.field public static final wifitrackerlib_wifi_security_short_eap_wpa3:I = 0x7f122a9e

.field public static final wifitrackerlib_wifi_security_short_eap_wpa_wpa2:I = 0x7f122a9f

.field public static final wifitrackerlib_wifi_security_short_eap_wpa_wpa2_wpa3:I = 0x7f122aa0

.field public static final wifitrackerlib_wifi_security_short_owe:I = 0x7f122aa1

.field public static final wifitrackerlib_wifi_security_short_sae:I = 0x7f122aa2

.field public static final wifitrackerlib_wifi_security_short_wpa_wpa2:I = 0x7f122aa3

.field public static final wifitrackerlib_wifi_security_short_wpa_wpa2_wpa3:I = 0x7f122aa4

.field public static final wifitrackerlib_wifi_security_wep:I = 0x7f122aa5

.field public static final wifitrackerlib_wifi_security_wpa_wpa2:I = 0x7f122aa6

.field public static final wifitrackerlib_wifi_security_wpa_wpa2_wpa3:I = 0x7f122aa7

.field public static final wifitrackerlib_wifi_standard_11ac:I = 0x7f122aa8

.field public static final wifitrackerlib_wifi_standard_11ad:I = 0x7f122aa9

.field public static final wifitrackerlib_wifi_standard_11ax:I = 0x7f122aaa

.field public static final wifitrackerlib_wifi_standard_11be:I = 0x7f122aab

.field public static final wifitrackerlib_wifi_standard_11n:I = 0x7f122aac

.field public static final wifitrackerlib_wifi_standard_legacy:I = 0x7f122aad

.field public static final wifitrackerlib_wifi_standard_unknown:I = 0x7f122aae

.field public static final wifitrackerlib_wifi_unmetered_label:I = 0x7f122aaf

.field public static final wifitrackerlib_wifi_wont_autoconnect_for_now:I = 0x7f122ab0

.field public static final window_animation_scale_title:I = 0x7f122ab1

.field public static final window_blurs:I = 0x7f122ab2

.field public static final winscope_trace_quick_settings_title:I = 0x7f122ab3

.field public static final wireless_connected:I = 0x7f122ab4

.field public static final wireless_debugging_main_switch_title:I = 0x7f122ab5

.field public static final wireless_networks_settings_title:I = 0x7f122ab6

.field public static final wireless_off:I = 0x7f122ab7

.field public static final wireless_on:I = 0x7f122ab8

.field public static final wizard_back:I = 0x7f122ab9

.field public static final wizard_finish:I = 0x7f122aba

.field public static final wizard_next:I = 0x7f122abb

.field public static final wlan_scanner_unresizeble:I = 0x7f122abc

.field public static final wlan_switch_tips:I = 0x7f122abd

.field public static final wlan_switch_tips_message:I = 0x7f122abe

.field public static final words_title:I = 0x7f122abf

.field public static final work_alarm_ringtone_title:I = 0x7f122ac0

.field public static final work_mode_label:I = 0x7f122ac1

.field public static final work_mode_off_summary:I = 0x7f122ac2

.field public static final work_mode_on_summary:I = 0x7f122ac3

.field public static final work_notification_ringtone_title:I = 0x7f122ac4

.field public static final work_policy_privacy_settings:I = 0x7f122ac5

.field public static final work_policy_privacy_settings_summary:I = 0x7f122ac6

.field public static final work_profile_app_subtext:I = 0x7f122ac7

.field public static final work_profile_confirm_remove_message:I = 0x7f122ac8

.field public static final work_profile_confirm_remove_title:I = 0x7f122ac9

.field public static final work_profile_notification_access_blocked_summary:I = 0x7f122aca

.field public static final work_profile_usage_access_warning:I = 0x7f122acb

.field public static final work_ringtone_title:I = 0x7f122acc

.field public static final work_sim_title:I = 0x7f122acd

.field public static final work_sound_permission_dialog_button_text_known:I = 0x7f122ace

.field public static final work_sound_permission_dialog_message:I = 0x7f122acf

.field public static final work_sound_permission_dialog_title:I = 0x7f122ad0

.field public static final work_sound_same_as_personal:I = 0x7f122ad1

.field public static final work_space_delete:I = 0x7f122ad2

.field public static final work_space_delete_confirm:I = 0x7f122ad3

.field public static final work_space_delete_description:I = 0x7f122ad4

.field public static final work_sync_dialog_message:I = 0x7f122ad5

.field public static final work_sync_dialog_title:I = 0x7f122ad6

.field public static final work_sync_dialog_yes:I = 0x7f122ad7

.field public static final work_use_personal_sounds_summary:I = 0x7f122ad8

.field public static final work_use_personal_sounds_title:I = 0x7f122ad9

.field public static final wps_category:I = 0x7f122ada

.field public static final wps_connect_summary:I = 0x7f122adb

.field public static final wps_connect_title:I = 0x7f122adc

.field public static final write_settings:I = 0x7f122add

.field public static final write_settings_description:I = 0x7f122ade

.field public static final write_settings_off:I = 0x7f122adf

.field public static final write_settings_on:I = 0x7f122ae0

.field public static final write_settings_summary:I = 0x7f122ae1

.field public static final write_settings_title:I = 0x7f122ae2

.field public static final write_system_settings:I = 0x7f122ae3

.field public static final wrong_pin_code_one:I = 0x7f122ae4

.field public static final wrong_pin_code_pukked:I = 0x7f122ae5

.field public static final xiaoai_global_shortcut:I = 0x7f122ae6

.field public static final xiaoai_key_shortcut:I = 0x7f122ae7

.field public static final xiaoai_other_shortcut:I = 0x7f122ae8

.field public static final xiaoai_shortcut_settings_title:I = 0x7f122ae9

.field public static final xiaomi_account:I = 0x7f122aea

.field public static final xiaomi_cloud_find_device:I = 0x7f122aeb

.field public static final xiaomi_cloud_service:I = 0x7f122aec

.field public static final xiaomi_cloud_service_description:I = 0x7f122aed

.field public static final xiaomi_cloud_service_description_without_contacts:I = 0x7f122aee

.field public static final xiaomi_cloud_service_disabled:I = 0x7f122aef

.field public static final xiaomi_cloud_service_unknown:I = 0x7f122af0

.field public static final xiaomi_custom_input_method:I = 0x7f122af1

.field public static final xiaomi_hp_location:I = 0x7f122af2

.field public static final xiaomi_hp_location_description:I = 0x7f122af3

.field public static final xiaomi_lanting_title:I = 0x7f122af4

.field public static final xiaomi_money_service:I = 0x7f122af5

.field public static final xiaomi_router_available_notif_summary:I = 0x7f122af6

.field public static final xiaomi_router_available_notif_title:I = 0x7f122af7

.field public static final xiaomi_router_notif_summary:I = 0x7f122af8

.field public static final xiaomi_router_notif_title:I = 0x7f122af9

.field public static final xiaomi_transfer:I = 0x7f122afa

.field public static final xspace:I = 0x7f122afb

.field public static final yes:I = 0x7f122afc

.field public static final yesterday:I = 0x7f122afd

.field public static final zen_access_detail_switch:I = 0x7f122afe

.field public static final zen_access_disabled_package_warning:I = 0x7f122aff

.field public static final zen_access_empty_text:I = 0x7f122b00

.field public static final zen_access_revoke_warning_dialog_summary:I = 0x7f122b01

.field public static final zen_access_revoke_warning_dialog_title:I = 0x7f122b02

.field public static final zen_access_warning_dialog_summary:I = 0x7f122b03

.field public static final zen_access_warning_dialog_title:I = 0x7f122b04

.field public static final zen_alarm_warning:I = 0x7f122b05

.field public static final zen_alarm_warning_indef:I = 0x7f122b06

.field public static final zen_calls_summary_contacts_repeat:I = 0x7f122b07

.field public static final zen_calls_summary_repeat_only:I = 0x7f122b08

.field public static final zen_calls_summary_starred_repeat:I = 0x7f122b09

.field public static final zen_category_apps:I = 0x7f122b0a

.field public static final zen_category_behavior:I = 0x7f122b0b

.field public static final zen_category_duration:I = 0x7f122b0c

.field public static final zen_category_exceptions:I = 0x7f122b0d

.field public static final zen_category_people:I = 0x7f122b0e

.field public static final zen_category_schedule:I = 0x7f122b0f

.field public static final zen_custom_settings_dialog_ok:I = 0x7f122b10

.field public static final zen_custom_settings_dialog_review_schedule:I = 0x7f122b11

.field public static final zen_custom_settings_dialog_title:I = 0x7f122b12

.field public static final zen_custom_settings_duration_header:I = 0x7f122b13

.field public static final zen_custom_settings_notifications_header:I = 0x7f122b14

.field public static final zen_event_rule_enabled_toast:I = 0x7f122b15

.field public static final zen_event_rule_type_name:I = 0x7f122b16

.field public static final zen_interruption_level_priority:I = 0x7f122b17

.field public static final zen_mode_add:I = 0x7f122b18

.field public static final zen_mode_add_event_rule:I = 0x7f122b19

.field public static final zen_mode_add_rule:I = 0x7f122b1a

.field public static final zen_mode_add_time_rule:I = 0x7f122b1b

.field public static final zen_mode_alarm_use:I = 0x7f122b1c

.field public static final zen_mode_alarms:I = 0x7f122b1d

.field public static final zen_mode_alarms_list:I = 0x7f122b1e

.field public static final zen_mode_alarms_list_first:I = 0x7f122b1f

.field public static final zen_mode_alarms_summary:I = 0x7f122b20

.field public static final zen_mode_all_callers:I = 0x7f122b21

.field public static final zen_mode_all_calls_summary:I = 0x7f122b22

.field public static final zen_mode_all_messages_summary:I = 0x7f122b23

.field public static final zen_mode_always:I = 0x7f122b24

.field public static final zen_mode_and_condition:I = 0x7f122b25

.field public static final zen_mode_app_set_behavior:I = 0x7f122b26

.field public static final zen_mode_auto_rule:I = 0x7f122b27

.field public static final zen_mode_automatic_rule_settings_page_title:I = 0x7f122b28

.field public static final zen_mode_automation_settings_page_title:I = 0x7f122b29

.field public static final zen_mode_automation_settings_title:I = 0x7f122b2a

.field public static final zen_mode_automation_suggestion_summary:I = 0x7f122b2b

.field public static final zen_mode_automation_suggestion_title:I = 0x7f122b2c

.field public static final zen_mode_behavior_alarms_only:I = 0x7f122b2d

.field public static final zen_mode_behavior_summary_custom:I = 0x7f122b2e

.field public static final zen_mode_block_effect_ambient:I = 0x7f122b2f

.field public static final zen_mode_block_effect_badge:I = 0x7f122b30

.field public static final zen_mode_block_effect_intent:I = 0x7f122b31

.field public static final zen_mode_block_effect_light:I = 0x7f122b32

.field public static final zen_mode_block_effect_list:I = 0x7f122b33

.field public static final zen_mode_block_effect_peek:I = 0x7f122b34

.field public static final zen_mode_block_effect_sound:I = 0x7f122b35

.field public static final zen_mode_block_effect_status:I = 0x7f122b36

.field public static final zen_mode_block_effect_summary_all:I = 0x7f122b37

.field public static final zen_mode_block_effect_summary_none:I = 0x7f122b38

.field public static final zen_mode_block_effect_summary_screen_off:I = 0x7f122b39

.field public static final zen_mode_block_effect_summary_screen_on:I = 0x7f122b3a

.field public static final zen_mode_block_effect_summary_some:I = 0x7f122b3b

.field public static final zen_mode_block_effect_summary_sound:I = 0x7f122b3c

.field public static final zen_mode_block_effects_screen_off:I = 0x7f122b3d

.field public static final zen_mode_block_effects_screen_on:I = 0x7f122b3e

.field public static final zen_mode_blocked_effects_footer:I = 0x7f122b3f

.field public static final zen_mode_button_turn_off:I = 0x7f122b40

.field public static final zen_mode_button_turn_on:I = 0x7f122b41

.field public static final zen_mode_bypassing_app_channels_header:I = 0x7f122b42

.field public static final zen_mode_bypassing_app_channels_toggle_all:I = 0x7f122b43

.field public static final zen_mode_bypassing_apps:I = 0x7f122b44

.field public static final zen_mode_bypassing_apps_add:I = 0x7f122b45

.field public static final zen_mode_bypassing_apps_add_header:I = 0x7f122b46

.field public static final zen_mode_bypassing_apps_all_summary:I = 0x7f122b47

.field public static final zen_mode_bypassing_apps_footer:I = 0x7f122b48

.field public static final zen_mode_bypassing_apps_header:I = 0x7f122b49

.field public static final zen_mode_bypassing_apps_none:I = 0x7f122b4a

.field public static final zen_mode_bypassing_apps_some_summary:I = 0x7f122b4b

.field public static final zen_mode_bypassing_apps_subtext:I = 0x7f122b4c

.field public static final zen_mode_bypassing_apps_subtext_none:I = 0x7f122b4d

.field public static final zen_mode_bypassing_apps_summary_all:I = 0x7f122b4e

.field public static final zen_mode_bypassing_apps_summary_some:I = 0x7f122b4f

.field public static final zen_mode_bypassing_apps_title:I = 0x7f122b50

.field public static final zen_mode_calls:I = 0x7f122b51

.field public static final zen_mode_calls_footer:I = 0x7f122b52

.field public static final zen_mode_calls_header:I = 0x7f122b53

.field public static final zen_mode_calls_list:I = 0x7f122b54

.field public static final zen_mode_calls_summary_one:I = 0x7f122b55

.field public static final zen_mode_calls_summary_two:I = 0x7f122b56

.field public static final zen_mode_calls_title:I = 0x7f122b57

.field public static final zen_mode_choose_rule_type:I = 0x7f122b58

.field public static final zen_mode_contacts_callers:I = 0x7f122b59

.field public static final zen_mode_contacts_count:I = 0x7f122b5a

.field public static final zen_mode_conversations_count:I = 0x7f122b5b

.field public static final zen_mode_conversations_section_title:I = 0x7f122b5c

.field public static final zen_mode_conversations_title:I = 0x7f122b5d

.field public static final zen_mode_custom_behavior_category_title:I = 0x7f122b5e

.field public static final zen_mode_custom_behavior_summary:I = 0x7f122b5f

.field public static final zen_mode_custom_behavior_summary_default:I = 0x7f122b60

.field public static final zen_mode_custom_behavior_title:I = 0x7f122b61

.field public static final zen_mode_custom_calls_footer:I = 0x7f122b62

.field public static final zen_mode_custom_messages_footer:I = 0x7f122b63

.field public static final zen_mode_delete_automatic_rules:I = 0x7f122b64

.field public static final zen_mode_delete_rule:I = 0x7f122b65

.field public static final zen_mode_delete_rule_button:I = 0x7f122b66

.field public static final zen_mode_delete_rule_confirmation:I = 0x7f122b67

.field public static final zen_mode_duration_always_prompt_title:I = 0x7f122b68

.field public static final zen_mode_duration_settings_title:I = 0x7f122b69

.field public static final zen_mode_duration_summary_always_prompt:I = 0x7f122b6a

.field public static final zen_mode_duration_summary_forever:I = 0x7f122b6b

.field public static final zen_mode_duration_summary_time_hours:I = 0x7f122b6c

.field public static final zen_mode_duration_summary_time_minutes:I = 0x7f122b6d

.field public static final zen_mode_effected_always:I = 0x7f122b6e

.field public static final zen_mode_enable_dialog_turn_on:I = 0x7f122b6f

.field public static final zen_mode_end_time:I = 0x7f122b70

.field public static final zen_mode_end_time_next_day_summary_format:I = 0x7f122b71

.field public static final zen_mode_event_rule_calendar:I = 0x7f122b72

.field public static final zen_mode_event_rule_calendar_any:I = 0x7f122b73

.field public static final zen_mode_event_rule_reply:I = 0x7f122b74

.field public static final zen_mode_event_rule_reply_any_except_no:I = 0x7f122b75

.field public static final zen_mode_event_rule_reply_yes:I = 0x7f122b76

.field public static final zen_mode_event_rule_reply_yes_or_maybe:I = 0x7f122b77

.field public static final zen_mode_event_rule_summary_any_calendar:I = 0x7f122b78

.field public static final zen_mode_event_rule_summary_calendar_template:I = 0x7f122b79

.field public static final zen_mode_event_rule_summary_reply_template:I = 0x7f122b7a

.field public static final zen_mode_events:I = 0x7f122b7b

.field public static final zen_mode_events_list:I = 0x7f122b7c

.field public static final zen_mode_events_list_first:I = 0x7f122b7d

.field public static final zen_mode_events_summary:I = 0x7f122b7e

.field public static final zen_mode_forever:I = 0x7f122b7f

.field public static final zen_mode_from_all_conversations:I = 0x7f122b80

.field public static final zen_mode_from_anyone:I = 0x7f122b81

.field public static final zen_mode_from_call:I = 0x7f122b82

.field public static final zen_mode_from_contacts:I = 0x7f122b83

.field public static final zen_mode_from_important_conversations:I = 0x7f122b84

.field public static final zen_mode_from_important_conversations_second:I = 0x7f122b85

.field public static final zen_mode_from_no_conversations:I = 0x7f122b86

.field public static final zen_mode_from_sms:I = 0x7f122b87

.field public static final zen_mode_from_some:I = 0x7f122b88

.field public static final zen_mode_from_starred:I = 0x7f122b89

.field public static final zen_mode_important_category:I = 0x7f122b8a

.field public static final zen_mode_lockscreen:I = 0x7f122b8b

.field public static final zen_mode_media:I = 0x7f122b8c

.field public static final zen_mode_media_list:I = 0x7f122b8d

.field public static final zen_mode_media_list_first:I = 0x7f122b8e

.field public static final zen_mode_media_summary:I = 0x7f122b8f

.field public static final zen_mode_messages:I = 0x7f122b90

.field public static final zen_mode_messages_footer:I = 0x7f122b91

.field public static final zen_mode_messages_header:I = 0x7f122b92

.field public static final zen_mode_messages_list:I = 0x7f122b93

.field public static final zen_mode_messages_title:I = 0x7f122b94

.field public static final zen_mode_miui_silent_category:I = 0x7f122b95

.field public static final zen_mode_more_actions:I = 0x7f122b96

.field public static final zen_mode_no_exceptions:I = 0x7f122b97

.field public static final zen_mode_none_calls:I = 0x7f122b98

.field public static final zen_mode_none_messages:I = 0x7f122b99

.field public static final zen_mode_option_miui_silent:I = 0x7f122b9a

.field public static final zen_mode_option_miui_vibrate:I = 0x7f122b9b

.field public static final zen_mode_other_options:I = 0x7f122b9c

.field public static final zen_mode_other_sounds_summary:I = 0x7f122b9d

.field public static final zen_mode_people_all:I = 0x7f122b9e

.field public static final zen_mode_people_calls_messages_section_title:I = 0x7f122b9f

.field public static final zen_mode_people_footer:I = 0x7f122ba0

.field public static final zen_mode_people_none:I = 0x7f122ba1

.field public static final zen_mode_people_some:I = 0x7f122ba2

.field public static final zen_mode_phone_calls:I = 0x7f122ba3

.field public static final zen_mode_qs_set_behavior:I = 0x7f122ba4

.field public static final zen_mode_reminders:I = 0x7f122ba5

.field public static final zen_mode_reminders_list:I = 0x7f122ba6

.field public static final zen_mode_reminders_list_first:I = 0x7f122ba7

.field public static final zen_mode_reminders_summary:I = 0x7f122ba8

.field public static final zen_mode_repeat_callers:I = 0x7f122ba9

.field public static final zen_mode_repeat_callers_list:I = 0x7f122baa

.field public static final zen_mode_repeat_callers_summary:I = 0x7f122bab

.field public static final zen_mode_repeat_callers_title:I = 0x7f122bac

.field public static final zen_mode_restrict_notifications_category:I = 0x7f122bad

.field public static final zen_mode_restrict_notifications_custom:I = 0x7f122bae

.field public static final zen_mode_restrict_notifications_disable_custom:I = 0x7f122baf

.field public static final zen_mode_restrict_notifications_enable_custom:I = 0x7f122bb0

.field public static final zen_mode_restrict_notifications_hide:I = 0x7f122bb1

.field public static final zen_mode_restrict_notifications_hide_footer:I = 0x7f122bb2

.field public static final zen_mode_restrict_notifications_hide_summary:I = 0x7f122bb3

.field public static final zen_mode_restrict_notifications_mute:I = 0x7f122bb4

.field public static final zen_mode_restrict_notifications_mute_footer:I = 0x7f122bb5

.field public static final zen_mode_restrict_notifications_mute_summary:I = 0x7f122bb6

.field public static final zen_mode_restrict_notifications_summary_custom:I = 0x7f122bb7

.field public static final zen_mode_restrict_notifications_summary_hidden:I = 0x7f122bb8

.field public static final zen_mode_restrict_notifications_summary_muted:I = 0x7f122bb9

.field public static final zen_mode_restrict_notifications_title:I = 0x7f122bba

.field public static final zen_mode_rule_delete_button:I = 0x7f122bbb

.field public static final zen_mode_rule_name:I = 0x7f122bbc

.field public static final zen_mode_rule_name_edit:I = 0x7f122bbd

.field public static final zen_mode_rule_name_hint:I = 0x7f122bbe

.field public static final zen_mode_rule_name_warning:I = 0x7f122bbf

.field public static final zen_mode_rule_not_found_text:I = 0x7f122bc0

.field public static final zen_mode_rule_rename_button:I = 0x7f122bc1

.field public static final zen_mode_rule_summary_enabled_combination:I = 0x7f122bc2

.field public static final zen_mode_rule_summary_provider_combination:I = 0x7f122bc3

.field public static final zen_mode_rule_type_unknown:I = 0x7f122bc4

.field public static final zen_mode_schedule_alarm_summary:I = 0x7f122bc5

.field public static final zen_mode_schedule_alarm_title:I = 0x7f122bc6

.field public static final zen_mode_schedule_category_title:I = 0x7f122bc7

.field public static final zen_mode_schedule_delete:I = 0x7f122bc8

.field public static final zen_mode_schedule_rule_days:I = 0x7f122bc9

.field public static final zen_mode_schedule_rule_days_all:I = 0x7f122bca

.field public static final zen_mode_schedule_rule_days_none:I = 0x7f122bcb

.field public static final zen_mode_schedule_title:I = 0x7f122bcc

.field public static final zen_mode_screen_locked_only:I = 0x7f122bcd

.field public static final zen_mode_screen_off:I = 0x7f122bce

.field public static final zen_mode_screen_off_summary:I = 0x7f122bcf

.field public static final zen_mode_screen_off_summary_no_led:I = 0x7f122bd0

.field public static final zen_mode_screen_on:I = 0x7f122bd1

.field public static final zen_mode_screen_on_summary:I = 0x7f122bd2

.field public static final zen_mode_settings_category:I = 0x7f122bd3

.field public static final zen_mode_settings_dnd_automatic_rule:I = 0x7f122bd4

.field public static final zen_mode_settings_dnd_automatic_rule_app:I = 0x7f122bd5

.field public static final zen_mode_settings_dnd_custom_settings_footer:I = 0x7f122bd6

.field public static final zen_mode_settings_dnd_custom_settings_footer_link:I = 0x7f122bd7

.field public static final zen_mode_settings_dnd_manual_end_time:I = 0x7f122bd8

.field public static final zen_mode_settings_dnd_manual_indefinite:I = 0x7f122bd9

.field public static final zen_mode_settings_schedules_summary:I = 0x7f122bda

.field public static final zen_mode_settings_summary:I = 0x7f122bdb

.field public static final zen_mode_settings_summary_off:I = 0x7f122bdc

.field public static final zen_mode_settings_title:I = 0x7f122bdd

.field public static final zen_mode_settings_turn_on_dialog_title:I = 0x7f122bde

.field public static final zen_mode_slice_subtitle:I = 0x7f122bdf

.field public static final zen_mode_sound_summary_off:I = 0x7f122be0

.field public static final zen_mode_sound_summary_on:I = 0x7f122be1

.field public static final zen_mode_sound_summary_on_with_info:I = 0x7f122be2

.field public static final zen_mode_sounds_none:I = 0x7f122be3

.field public static final zen_mode_starred_callers:I = 0x7f122be4

.field public static final zen_mode_starred_contacts_empty_name:I = 0x7f122be5

.field public static final zen_mode_starred_contacts_summary_contacts:I = 0x7f122be6

.field public static final zen_mode_starred_contacts_title:I = 0x7f122be7

.field public static final zen_mode_start_time:I = 0x7f122be8

.field public static final zen_mode_summary:I = 0x7f122be9

.field public static final zen_mode_summary2:I = 0x7f122bea

.field public static final zen_mode_summary_alarms_only_by_time:I = 0x7f122beb

.field public static final zen_mode_summary_alarms_only_indefinite:I = 0x7f122bec

.field public static final zen_mode_summary_always:I = 0x7f122bed

.field public static final zen_mode_summary_combination:I = 0x7f122bee

.field public static final zen_mode_system:I = 0x7f122bef

.field public static final zen_mode_system_list:I = 0x7f122bf0

.field public static final zen_mode_system_list_first:I = 0x7f122bf1

.field public static final zen_mode_system_summary:I = 0x7f122bf2

.field public static final zen_mode_title:I = 0x7f122bf3

.field public static final zen_mode_turned_off:I = 0x7f122bf4

.field public static final zen_mode_turned_on:I = 0x7f122bf5

.field public static final zen_mode_unknown_app_set_behavior:I = 0x7f122bf6

.field public static final zen_mode_use_automatic_rule:I = 0x7f122bf7

.field public static final zen_mode_visual_interruptions_settings_title:I = 0x7f122bf8

.field public static final zen_mode_visual_signals_settings_subtitle:I = 0x7f122bf9

.field public static final zen_mode_what_to_block_title:I = 0x7f122bfa

.field public static final zen_mode_when:I = 0x7f122bfb

.field public static final zen_mode_when_every_night:I = 0x7f122bfc

.field public static final zen_mode_when_never:I = 0x7f122bfd

.field public static final zen_mode_when_weeknights:I = 0x7f122bfe

.field public static final zen_msg_event_reminder_footer:I = 0x7f122bff

.field public static final zen_msg_event_reminder_title:I = 0x7f122c00

.field public static final zen_onboarding_current_setting_summary:I = 0x7f122c01

.field public static final zen_onboarding_current_setting_title:I = 0x7f122c02

.field public static final zen_onboarding_dnd_visual_disturbances_description:I = 0x7f122c03

.field public static final zen_onboarding_dnd_visual_disturbances_header:I = 0x7f122c04

.field public static final zen_onboarding_more_options:I = 0x7f122c05

.field public static final zen_onboarding_new_setting_summary:I = 0x7f122c06

.field public static final zen_onboarding_new_setting_title:I = 0x7f122c07

.field public static final zen_onboarding_ok:I = 0x7f122c08

.field public static final zen_onboarding_screen_off_summary:I = 0x7f122c09

.field public static final zen_onboarding_screen_off_title:I = 0x7f122c0a

.field public static final zen_onboarding_screen_on_summary:I = 0x7f122c0b

.field public static final zen_onboarding_screen_on_title:I = 0x7f122c0c

.field public static final zen_onboarding_settings:I = 0x7f122c0d

.field public static final zen_rule_name_hint:I = 0x7f122c0e

.field public static final zen_schedule_rule_enabled_toast:I = 0x7f122c0f

.field public static final zen_schedule_rule_type_name:I = 0x7f122c10

.field public static final zen_settings_general:I = 0x7f122c11

.field public static final zen_sound_footer:I = 0x7f122c12

.field public static final zen_suggestion_summary:I = 0x7f122c13

.field public static final zen_suggestion_title:I = 0x7f122c14

.field public static final zone_auto_summaryOff:I = 0x7f122c15

.field public static final zone_auto_summaryOn:I = 0x7f122c16

.field public static final zone_auto_title:I = 0x7f122c17

.field public static final zone_change_to_from_dst:I = 0x7f122c18

.field public static final zone_info_exemplar_location_and_offset:I = 0x7f122c19

.field public static final zone_info_footer:I = 0x7f122c1a

.field public static final zone_info_footer_no_dst:I = 0x7f122c1b

.field public static final zone_info_offset_and_name:I = 0x7f122c1c

.field public static final zone_list_menu_sort_alphabetically:I = 0x7f122c1d

.field public static final zone_list_menu_sort_by_timezone:I = 0x7f122c1e

.field public static final zone_menu_by_offset:I = 0x7f122c1f

.field public static final zone_menu_by_region:I = 0x7f122c20

.field public static final zone_time_type_dst:I = 0x7f122c21

.field public static final zone_time_type_standard:I = 0x7f122c22


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
