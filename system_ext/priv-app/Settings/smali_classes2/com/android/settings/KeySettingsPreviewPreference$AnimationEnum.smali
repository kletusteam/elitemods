.class final enum Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/KeySettingsPreviewPreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "AnimationEnum"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

.field public static final enum CLICK_BOTTOM:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

.field public static final enum DOUBLE_CLICK_POWER:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

.field public static final enum LONG_PRESS:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

.field public static final enum LONG_PRESS_POWER:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

.field public static final enum POWER_CLICK:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

.field public static final enum THREE_DROP:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

.field public static final enum THREE_LONG_PRESS:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;


# instance fields
.field private mAnimArrayId:I

.field private mImgViewId:I


# direct methods
.method static bridge synthetic -$$Nest$fgetmAnimArrayId(Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;)I
    .locals 0

    iget p0, p0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->mAnimArrayId:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmImgViewId(Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;)I
    .locals 0

    iget p0, p0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->mImgViewId:I

    return p0
.end method

.method static constructor <clinit>()V
    .locals 16

    new-instance v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    sget v1, Lcom/android/settings/R$id;->key_power_click:I

    sget v2, Lcom/android/settings/R$array;->power_click:I

    const-string v3, "POWER_CLICK"

    const/4 v4, 0x0

    invoke-direct {v0, v3, v4, v1, v2}, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->POWER_CLICK:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    new-instance v1, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    sget v2, Lcom/android/settings/R$id;->key_long_press_bottom:I

    sget v3, Lcom/android/settings/R$array;->long_press:I

    const-string v5, "LONG_PRESS"

    const/4 v6, 0x1

    invoke-direct {v1, v5, v6, v2, v3}, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;-><init>(Ljava/lang/String;III)V

    sput-object v1, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->LONG_PRESS:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    new-instance v2, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    sget v3, Lcom/android/settings/R$id;->key_click_bottom:I

    sget v5, Lcom/android/settings/R$array;->click_bottom:I

    const-string v7, "CLICK_BOTTOM"

    const/4 v8, 0x2

    invoke-direct {v2, v7, v8, v3, v5}, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;-><init>(Ljava/lang/String;III)V

    sput-object v2, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->CLICK_BOTTOM:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    new-instance v3, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    sget v5, Lcom/android/settings/R$id;->key_power_double_click:I

    sget v7, Lcom/android/settings/R$array;->power_double_click:I

    const-string v9, "DOUBLE_CLICK_POWER"

    const/4 v10, 0x3

    invoke-direct {v3, v9, v10, v5, v7}, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;-><init>(Ljava/lang/String;III)V

    sput-object v3, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->DOUBLE_CLICK_POWER:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    new-instance v7, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    sget v9, Lcom/android/settings/R$id;->key_three_gesture:I

    sget v11, Lcom/android/settings/R$array;->three_drop:I

    const-string v12, "THREE_DROP"

    const/4 v13, 0x4

    invoke-direct {v7, v12, v13, v9, v11}, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;-><init>(Ljava/lang/String;III)V

    sput-object v7, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->THREE_DROP:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    new-instance v9, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    sget v11, Lcom/android/settings/R$id;->key_three_long_press:I

    sget v12, Lcom/android/settings/R$array;->three_long_press:I

    const-string v14, "THREE_LONG_PRESS"

    const/4 v15, 0x5

    invoke-direct {v9, v14, v15, v11, v12}, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;-><init>(Ljava/lang/String;III)V

    sput-object v9, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->THREE_LONG_PRESS:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    new-instance v11, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    sget v12, Lcom/android/settings/R$array;->power_long_press:I

    const-string v14, "LONG_PRESS_POWER"

    const/4 v15, 0x6

    invoke-direct {v11, v14, v15, v5, v12}, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;-><init>(Ljava/lang/String;III)V

    sput-object v11, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->LONG_PRESS_POWER:Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    const/4 v5, 0x7

    new-array v5, v5, [Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    aput-object v0, v5, v4

    aput-object v1, v5, v6

    aput-object v2, v5, v8

    aput-object v3, v5, v10

    aput-object v7, v5, v13

    const/4 v0, 0x5

    aput-object v9, v5, v0

    aput-object v11, v5, v15

    sput-object v5, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->$VALUES:[Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->mImgViewId:I

    iput p4, p0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->mAnimArrayId:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;
    .locals 1

    const-class v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    return-object p0
.end method

.method public static values()[Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;
    .locals 1

    sget-object v0, Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->$VALUES:[Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    invoke-virtual {v0}, [Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/settings/KeySettingsPreviewPreference$AnimationEnum;

    return-object v0
.end method
