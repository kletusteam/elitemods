.class public Lcom/android/settings/SettingsApplication;
.super Lmiuix/autodensity/MiuixApplication;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/SettingsApplication$InitTask;
    }
.end annotation


# static fields
.field private static ENABLEQIGSAW:Z

.field private static HEADSETPLUGIN:Ljava/lang/String;

.field private static HEADSETPLUGIN_ENABLE:I

.field private static HEADSETPLUGIN_INITED:I

.field private static HEADSETPLUGIN_INITED_NOTIFY:Ljava/lang/String;

.field private static HEADSETPLUGIN_NOTSET:I

.field public static final PROC_USER_ID:I

.field private static SP_QIGSAW_ENABLE:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field public ENABLEQIGSAWINITED:Z

.field private final TOPIC:Ljava/lang/String;

.field private mHomeActivity:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/android/settings/homepage/SettingsHomepageActivity;",
            ">;"
        }
    .end annotation
.end field

.field public mMainProcess:Z

.field public mQigsawStarted:I


# direct methods
.method static bridge synthetic -$$Nest$mdeleteV5Shortcuts(Lcom/android/settings/SettingsApplication;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/SettingsApplication;->deleteV5Shortcuts(Landroid/content/Context;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mensureOpenSmMonitor(Lcom/android/settings/SettingsApplication;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/SettingsApplication;->ensureOpenSmMonitor()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCloud(Lcom/android/settings/SettingsApplication;ZLandroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/SettingsApplication;->updateCloud(ZLandroid/content/Context;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetHEADSETPLUGIN()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/settings/SettingsApplication;->HEADSETPLUGIN:Ljava/lang/String;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetHEADSETPLUGIN_ENABLE()I
    .locals 1

    sget v0, Lcom/android/settings/SettingsApplication;->HEADSETPLUGIN_ENABLE:I

    return v0
.end method

.method static bridge synthetic -$$Nest$sfgetHEADSETPLUGIN_NOTSET()I
    .locals 1

    sget v0, Lcom/android/settings/SettingsApplication;->HEADSETPLUGIN_NOTSET:I

    return v0
.end method

.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/android/settings/SettingsApplication;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/SettingsApplication;->TAG:Ljava/lang/String;

    const-string v0, "HeadsetPluginDefault"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/android/settings/SettingsApplication;->ENABLEQIGSAW:Z

    const-string v0, "BLUETOOTHHEADSETPLUGIN"

    sput-object v0, Lcom/android/settings/SettingsApplication;->HEADSETPLUGIN:Ljava/lang/String;

    const-string v0, "BLUETOOTHHEADSETPLUGIN_INITED"

    sput-object v0, Lcom/android/settings/SettingsApplication;->HEADSETPLUGIN_INITED_NOTIFY:Ljava/lang/String;

    const/4 v0, 0x1

    sput v0, Lcom/android/settings/SettingsApplication;->HEADSETPLUGIN_ENABLE:I

    const/4 v1, -0x1

    sput v1, Lcom/android/settings/SettingsApplication;->HEADSETPLUGIN_NOTSET:I

    sput v0, Lcom/android/settings/SettingsApplication;->HEADSETPLUGIN_INITED:I

    const-string/jumbo v0, "sp_qigsaw_enable"

    sput-object v0, Lcom/android/settings/SettingsApplication;->SP_QIGSAW_ENABLE:Ljava/lang/String;

    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/UserHandle;->hashCode()I

    move-result v0

    sput v0, Lcom/android/settings/SettingsApplication;->PROC_USER_ID:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lmiuix/autodensity/MiuixApplication;-><init>()V

    const-string v0, "SECURITY_TOPIC"

    iput-object v0, p0, Lcom/android/settings/SettingsApplication;->TOPIC:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/SettingsApplication;->ENABLEQIGSAWINITED:Z

    iput-boolean v0, p0, Lcom/android/settings/SettingsApplication;->mMainProcess:Z

    sget v0, Lcom/android/settings/SettingsApplication;->HEADSETPLUGIN_NOTSET:I

    iput v0, p0, Lcom/android/settings/SettingsApplication;->mQigsawStarted:I

    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/SettingsApplication;->mHomeActivity:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method private checkEnableQigsaw(Landroid/content/Context;)Z
    .locals 1

    sget-object p0, Lcom/android/settings/SettingsApplication;->SP_QIGSAW_ENABLE:Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p0

    if-eqz p0, :cond_0

    const-string p1, "enableQigsaw"

    invoke-interface {p0, p1, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p0

    return p0

    :cond_0
    return v0
.end method

.method private deleteV5Shortcuts(Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, Landroidx/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p0

    const-string v0, "key_delete_v5_shortcuts"

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/android/settings/ShortcutHelper;->getInstance(Landroid/content/Context;)Lcom/android/settings/ShortcutHelper;

    move-result-object p1

    sget-object v1, Lcom/android/settings/ShortcutHelper$Shortcut;->OPTIMIZE_CENTER:Lcom/android/settings/ShortcutHelper$Shortcut;

    invoke-virtual {p1, v1}, Lcom/android/settings/ShortcutHelper;->removeShortcut(Lcom/android/settings/ShortcutHelper$Shortcut;)V

    sget-object v1, Lcom/android/settings/ShortcutHelper$Shortcut;->POWER_CENTER:Lcom/android/settings/ShortcutHelper$Shortcut;

    invoke-virtual {p1, v1}, Lcom/android/settings/ShortcutHelper;->removeShortcut(Lcom/android/settings/ShortcutHelper$Shortcut;)V

    sget-object v1, Lcom/android/settings/ShortcutHelper$Shortcut;->VIRUS_CENTER:Lcom/android/settings/ShortcutHelper$Shortcut;

    invoke-virtual {p1, v1}, Lcom/android/settings/ShortcutHelper;->removeShortcut(Lcom/android/settings/ShortcutHelper$Shortcut;)V

    sget-object v1, Lcom/android/settings/ShortcutHelper$Shortcut;->PERM_CENTER:Lcom/android/settings/ShortcutHelper$Shortcut;

    invoke-virtual {p1, v1}, Lcom/android/settings/ShortcutHelper;->removeShortcut(Lcom/android/settings/ShortcutHelper$Shortcut;)V

    sget-object v1, Lcom/android/settings/ShortcutHelper$Shortcut;->NETWORK_ASSISTANT:Lcom/android/settings/ShortcutHelper$Shortcut;

    invoke-virtual {p1, v1}, Lcom/android/settings/ShortcutHelper;->removeShortcut(Lcom/android/settings/ShortcutHelper$Shortcut;)V

    sget-object v1, Lcom/android/settings/ShortcutHelper$Shortcut;->ANTISPAM:Lcom/android/settings/ShortcutHelper$Shortcut;

    invoke-virtual {p1, v1}, Lcom/android/settings/ShortcutHelper;->removeShortcut(Lcom/android/settings/ShortcutHelper$Shortcut;)V

    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    const/4 p1, 0x1

    invoke-interface {p0, v0, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    return-void
.end method

.method private ensureOpenSmMonitor()V
    .locals 5

    invoke-virtual {p0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "misettings_st_enable_sm"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    const/4 v4, 0x1

    if-ne v1, v4, :cond_0

    move v3, v4

    :cond_0
    if-nez v3, :cond_1

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_1
    invoke-virtual {p0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0}, Lcom/android/settings/report/InternationalCompat;->init(Landroid/content/Context;)V

    return-void
.end method

.method private updateCloud(ZLandroid/content/Context;)V
    .locals 5

    const-string v0, "bt_plugin_settings_qigsaw"

    :try_start_0
    sget-object v1, Lcom/android/settings/SettingsApplication;->SP_QIGSAW_ENABLE:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "enableQigsaw"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    const-string v1, "bt_plugin_settings_miuix"

    const-string v2, "#"

    if-eqz p1, :cond_2

    :try_start_1
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    sget v0, Lcom/android/settings/SettingsApplication;->PROC_USER_ID:I

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const-string/jumbo v3, "settings_miuix_version_2"

    if-nez p1, :cond_1

    :try_start_2
    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_5

    :cond_1
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1, v3}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_0

    :cond_2
    iget-boolean p1, p0, Lcom/android/settings/SettingsApplication;->mMainProcess:Z

    if-eqz p1, :cond_3

    sget-boolean p1, Lcom/android/settings/SettingsApplication;->ENABLEQIGSAW:Z

    if-nez p1, :cond_5

    iget-boolean p0, p0, Lcom/android/settings/SettingsApplication;->ENABLEQIGSAWINITED:Z

    if-nez p0, :cond_5

    :cond_3
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    sget v3, Lcom/android/settings/SettingsApplication;->PROC_USER_ID:I

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    const-string p1, ""

    if-nez p0, :cond_4

    :try_start_3
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, p1}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    :cond_4
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_5

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p0, p2, p1}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_5
    :goto_0
    return-void
.end method


# virtual methods
.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 5

    invoke-super {p0, p1}, Landroid/app/Application;->attachBaseContext(Landroid/content/Context;)V

    :try_start_0
    const-string v0, "com.android.settings"

    invoke-static {}, Landroid/app/Application;->getProcessName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/settings/SettingsApplication;->mMainProcess:Z

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/android/settings/SettingsApplication;->checkEnableQigsaw(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/android/settings/SettingsApplication;->ENABLEQIGSAW:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v2

    :goto_1
    sput-boolean v0, Lcom/android/settings/SettingsApplication;->ENABLEQIGSAW:Z

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/iqiyi/android/qigsaw/core/SplitConfiguration;->newBuilder()Lcom/iqiyi/android/qigsaw/core/SplitConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/iqiyi/android/qigsaw/core/SplitConfiguration$Builder;->splitLoadMode(I)Lcom/iqiyi/android/qigsaw/core/SplitConfiguration$Builder;

    move-result-object v0

    new-instance v3, Lcom/android/settings/bluetooth/plugin/reporter/SampleLogger;

    invoke-direct {v3}, Lcom/android/settings/bluetooth/plugin/reporter/SampleLogger;-><init>()V

    invoke-virtual {v0, v3}, Lcom/iqiyi/android/qigsaw/core/SplitConfiguration$Builder;->logger(Lcom/iqiyi/android/qigsaw/core/common/SplitLog$Logger;)Lcom/iqiyi/android/qigsaw/core/SplitConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/iqiyi/android/qigsaw/core/SplitConfiguration$Builder;->verifySignature(Z)Lcom/iqiyi/android/qigsaw/core/SplitConfiguration$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/bluetooth/plugin/reporter/SampleSplitLoadReporter;

    invoke-direct {v1, p0}, Lcom/android/settings/bluetooth/plugin/reporter/SampleSplitLoadReporter;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/iqiyi/android/qigsaw/core/SplitConfiguration$Builder;->loadReporter(Lcom/iqiyi/android/qigsaw/core/splitreport/SplitLoadReporter;)Lcom/iqiyi/android/qigsaw/core/SplitConfiguration$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/bluetooth/plugin/reporter/SampleSplitInstallReporter;

    invoke-direct {v1, p0}, Lcom/android/settings/bluetooth/plugin/reporter/SampleSplitInstallReporter;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/iqiyi/android/qigsaw/core/SplitConfiguration$Builder;->installReporter(Lcom/iqiyi/android/qigsaw/core/splitreport/SplitInstallReporter;)Lcom/iqiyi/android/qigsaw/core/SplitConfiguration$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/bluetooth/plugin/reporter/SampleSplitUninstallReporter;

    invoke-direct {v1, p0}, Lcom/android/settings/bluetooth/plugin/reporter/SampleSplitUninstallReporter;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/iqiyi/android/qigsaw/core/SplitConfiguration$Builder;->uninstallReporter(Lcom/iqiyi/android/qigsaw/core/splitreport/SplitUninstallReporter;)Lcom/iqiyi/android/qigsaw/core/SplitConfiguration$Builder;

    move-result-object v0

    new-instance v1, Lcom/android/settings/bluetooth/plugin/reporter/SampleSplitUpdateReporter;

    invoke-direct {v1, p0}, Lcom/android/settings/bluetooth/plugin/reporter/SampleSplitUpdateReporter;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/iqiyi/android/qigsaw/core/SplitConfiguration$Builder;->updateReporter(Lcom/iqiyi/android/qigsaw/core/splitreport/SplitUpdateReporter;)Lcom/iqiyi/android/qigsaw/core/SplitConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/iqiyi/android/qigsaw/core/SplitConfiguration$Builder;->build()Lcom/iqiyi/android/qigsaw/core/SplitConfiguration;

    move-result-object v0

    new-instance v1, Lcom/android/settings/bluetooth/plugin/downloader/SampleDownloader;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object p1

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "bluetooth"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo p1, "plugins"

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/android/settings/bluetooth/plugin/downloader/SampleDownloader;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v1, v0}, Lcom/iqiyi/android/qigsaw/core/Qigsaw;->install(Landroid/content/Context;Lcom/iqiyi/android/qigsaw/core/splitdownload/Downloader;Lcom/iqiyi/android/qigsaw/core/SplitConfiguration;)V

    iput-boolean v2, p0, Lcom/android/settings/SettingsApplication;->ENABLEQIGSAWINITED:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_2
    :goto_2
    return-void
.end method

.method public getHomeActivity()Lcom/android/settings/homepage/SettingsHomepageActivity;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/SettingsApplication;->mHomeActivity:Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/android/settings/homepage/SettingsHomepageActivity;

    return-object p0
.end method

.method public getProcessName(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    const-string p0, "activity"

    invoke-virtual {p1, p0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/app/ActivityManager;

    invoke-virtual {p0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object p0

    const/4 p1, 0x0

    if-nez p0, :cond_0

    return-object p1

    :cond_0
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v2, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v2, v0, :cond_1

    iget-object p0, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    return-object p0

    :cond_2
    return-object p1
.end method

.method public getResources()Landroid/content/res/Resources;
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/SettingsApplication;->mMainProcess:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/android/settings/SettingsApplication;->ENABLEQIGSAW:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/settings/SettingsApplication;->ENABLEQIGSAWINITED:Z

    if-eqz v0, :cond_1

    :cond_0
    invoke-super {p0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/iqiyi/android/qigsaw/core/Qigsaw;->onApplicationGetResources(Landroid/content/res/Resources;)V

    :cond_1
    invoke-super {p0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    return-object p0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/app/Application;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget p0, p1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 p0, p0, 0xf

    const/4 p1, 0x3

    if-ne p0, p1, :cond_0

    const/4 p0, 0x1

    invoke-static {p0}, Lcom/android/settings/utils/TabletUtils;->changeDeviceForm(I)V

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    invoke-static {p0}, Lcom/android/settings/utils/TabletUtils;->changeDeviceForm(I)V

    :goto_0
    return-void
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Lmiuix/autodensity/MiuixApplication;->onCreate()V

    new-instance v0, Lcom/android/settings/activityembedding/ActivityEmbeddingRulesController;

    invoke-direct {v0, p0}, Lcom/android/settings/activityembedding/ActivityEmbeddingRulesController;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/android/settings/activityembedding/ActivityEmbeddingRulesController;->initRules()V

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/android/settings/SettingsApplication$InitTask;

    invoke-virtual {p0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/settings/SettingsApplication$InitTask;-><init>(Lcom/android/settings/SettingsApplication;Landroid/content/Context;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :try_start_0
    iget-boolean v0, p0, Lcom/android/settings/SettingsApplication;->mMainProcess:Z

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/android/settings/SettingsApplication;->ENABLEQIGSAW:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/settings/SettingsApplication;->ENABLEQIGSAWINITED:Z

    if-eqz v0, :cond_2

    :cond_1
    invoke-static {}, Lcom/iqiyi/android/qigsaw/core/Qigsaw;->onApplicationCreated()V

    invoke-virtual {p0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v2, Lcom/android/settings/SettingsApplication;->PROC_USER_ID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/android/settings/SettingsApplication;->HEADSETPLUGIN_INITED_NOTIFY:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/android/settings/SettingsApplication;->HEADSETPLUGIN_INITED:I

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    sget v0, Lcom/android/settings/SettingsApplication;->HEADSETPLUGIN_INITED:I

    iput v0, p0, Lcom/android/settings/SettingsApplication;->mQigsawStarted:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_2
    :goto_0
    return-void
.end method

.method public onLowMemory()V
    .locals 0

    invoke-super {p0}, Landroid/app/Application;->onLowMemory()V

    invoke-static {}, Lcom/android/settingslib/applications/AppIconCacheManager;->getInstance()Lcom/android/settingslib/applications/AppIconCacheManager;

    invoke-static {}, Lcom/android/settingslib/applications/AppIconCacheManager;->release()V

    return-void
.end method

.method public setHomeActivity(Lcom/android/settings/homepage/SettingsHomepageActivity;)V
    .locals 1

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/settings/SettingsApplication;->mHomeActivity:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public shouldAdaptAutoDensity()Z
    .locals 0

    const/4 p0, 0x1

    return p0
.end method
