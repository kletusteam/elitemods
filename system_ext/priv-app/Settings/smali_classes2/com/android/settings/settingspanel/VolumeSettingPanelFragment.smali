.class public Lcom/android/settings/settingspanel/VolumeSettingPanelFragment;
.super Lcom/android/settings/SettingsPreferenceFragment;


# instance fields
.field private mVolumePrefs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/android/settings/sound/VolumeSeekBarPreference;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/SettingsPreferenceFragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/settingspanel/VolumeSettingPanelFragment;->mVolumePrefs:Ljava/util/ArrayList;

    return-void
.end method

.method private initVolume()V
    .locals 3

    sget v0, Lcom/android/settings/R$drawable;->ring_volume_icon:I

    const-string/jumbo v1, "ring_volume"

    const/4 v2, 0x2

    invoke-direct {p0, v1, v2, v0}, Lcom/android/settings/settingspanel/VolumeSettingPanelFragment;->initVolumePreference(Ljava/lang/String;II)V

    sget v0, Lcom/android/settings/R$drawable;->alarm_volume_icon:I

    const-string v1, "alarm_volume"

    const/4 v2, 0x4

    invoke-direct {p0, v1, v2, v0}, Lcom/android/settings/settingspanel/VolumeSettingPanelFragment;->initVolumePreference(Ljava/lang/String;II)V

    sget v0, Lcom/android/settings/R$drawable;->media_volume_icon:I

    const-string/jumbo v1, "media_volume"

    const/4 v2, 0x3

    invoke-direct {p0, v1, v2, v0}, Lcom/android/settings/settingspanel/VolumeSettingPanelFragment;->initVolumePreference(Ljava/lang/String;II)V

    return-void
.end method

.method private initVolumePreference(Ljava/lang/String;II)V
    .locals 0

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settings/sound/VolumeSeekBarPreference;

    invoke-virtual {p1, p2}, Lcom/android/settings/sound/VolumeSeekBarPreference;->setStream(I)V

    invoke-virtual {p1, p3}, Landroidx/preference/Preference;->setIcon(I)V

    new-instance p2, Lcom/android/settings/sound/SeekBarVolumizer;

    invoke-direct {p2, p1}, Lcom/android/settings/sound/SeekBarVolumizer;-><init>(Lcom/android/settings/sound/VolumeSeekBarPreference;)V

    invoke-virtual {p1, p2}, Lcom/android/settings/sound/VolumeSeekBarPreference;->setSeekBarVolumizer(Lcom/android/settings/sound/SeekBarVolumizer;)V

    iget-object p0, p0, Lcom/android/settings/settingspanel/VolumeSettingPanelFragment;->mVolumePrefs:Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    sget p1, Lcom/android/settings/R$style;->Theme_Provision_Notitle_WifiSettings:I

    invoke-virtual {p0, p1}, Lcom/android/settingslib/miuisettings/preference/PreferenceFragment;->setThemeRes(I)V

    sget p1, Lcom/android/settings/R$xml;->miui_volume_settings_panel:I

    invoke-virtual {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->addPreferencesFromResource(I)V

    invoke-direct {p0}, Lcom/android/settings/settingspanel/VolumeSettingPanelFragment;->initVolume()V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onPause()V

    iget-object p0, p0, Lcom/android/settings/settingspanel/VolumeSettingPanelFragment;->mVolumePrefs:Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/sound/VolumeSeekBarPreference;

    invoke-virtual {v0}, Lcom/android/settings/sound/VolumeSeekBarPreference;->getSeekBarVolumizer()Lcom/android/settings/sound/SeekBarVolumizer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/sound/SeekBarVolumizer;->pause()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onResume()V

    iget-object p0, p0, Lcom/android/settings/settingspanel/VolumeSettingPanelFragment;->mVolumePrefs:Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/sound/VolumeSeekBarPreference;

    invoke-virtual {v0}, Lcom/android/settings/sound/VolumeSeekBarPreference;->getSeekBarVolumizer()Lcom/android/settings/sound/SeekBarVolumizer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settings/sound/SeekBarVolumizer;->resume()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/android/settings/SettingsPreferenceFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getListView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object p0

    check-cast p0, Landroid/view/View;

    instance-of p1, p0, Lmiuix/springback/view/SpringBackLayout;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    return-void
.end method
