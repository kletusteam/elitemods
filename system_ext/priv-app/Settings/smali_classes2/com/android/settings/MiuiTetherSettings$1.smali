.class Lcom/android/settings/MiuiTetherSettings$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/net/wifi/WifiManager$SoftApCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/MiuiTetherSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/MiuiTetherSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/MiuiTetherSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/MiuiTetherSettings$1;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnectedClientsChanged(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/net/wifi/WifiClient;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/settings/MiuiTetherSettings$1;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-static {v0, p1}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fputmNumClients(Lcom/android/settings/MiuiTetherSettings;I)V

    iget-object p0, p0, Lcom/android/settings/MiuiTetherSettings$1;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p0}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$mmanageShowConnectedDevices(Lcom/android/settings/MiuiTetherSettings;)V

    return-void
.end method

.method public onStateChanged(II)V
    .locals 6

    const/16 p2, 0xd

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/16 v2, 0xb

    if-ne p1, v2, :cond_3

    iget-object v2, p0, Lcom/android/settings/MiuiTetherSettings$1;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {v2}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fgetmRestartWifiApAfterConfigChange(Lcom/android/settings/MiuiTetherSettings;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/MiuiTetherSettings$1;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {v2, v0}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fputmRestartWifiApAfterConfigChange(Lcom/android/settings/MiuiTetherSettings;Z)V

    const-string v2, "MiuiTetherSettings"

    const-string v3, "Restarting WifiAp due to prior config change."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/settings/MiuiTetherSettings$1;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-virtual {v3}, Landroidx/fragment/app/Fragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v2, p0, Lcom/android/settings/MiuiTetherSettings$1;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {v2}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fgetmDelayStartTetherRunnable(Lcom/android/settings/MiuiTetherSettings;)Ljava/lang/Runnable;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/MiuiTetherSettings$1;->this$0:Lcom/android/settings/MiuiTetherSettings;

    new-instance v3, Lcom/android/settings/MiuiTetherSettings$1$1;

    invoke-direct {v3, p0}, Lcom/android/settings/MiuiTetherSettings$1$1;-><init>(Lcom/android/settings/MiuiTetherSettings$1;)V

    invoke-static {v2, v3}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fputmDelayStartTetherRunnable(Lcom/android/settings/MiuiTetherSettings;Ljava/lang/Runnable;)V

    :cond_0
    iget-object v2, p0, Lcom/android/settings/MiuiTetherSettings$1;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getMainThreadHandler()Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/MiuiTetherSettings$1;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {v3}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fgetmDelayStartTetherRunnable(Lcom/android/settings/MiuiTetherSettings;)Ljava/lang/Runnable;

    move-result-object v3

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_1
    const-string v3, "The fragment is not added, skip restart."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/android/settings/MiuiTetherSettings$1;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {v2, v0}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fputmSoftApEnabled(Lcom/android/settings/MiuiTetherSettings;Z)V

    iget-object v2, p0, Lcom/android/settings/MiuiTetherSettings$1;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {v2}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$mmanageShowConnectedDevices(Lcom/android/settings/MiuiTetherSettings;)V

    goto :goto_1

    :cond_3
    if-ne p1, p2, :cond_4

    iget-object v2, p0, Lcom/android/settings/MiuiTetherSettings$1;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {v2, v1}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fputmSoftApEnabled(Lcom/android/settings/MiuiTetherSettings;Z)V

    iget-object v2, p0, Lcom/android/settings/MiuiTetherSettings$1;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {v2}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$mmanageShowConnectedDevices(Lcom/android/settings/MiuiTetherSettings;)V

    :cond_4
    :goto_1
    iget-object v2, p0, Lcom/android/settings/MiuiTetherSettings$1;->this$0:Lcom/android/settings/MiuiTetherSettings;

    if-ne p1, p2, :cond_5

    move v0, v1

    :cond_5
    invoke-static {v2, v0}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$mshowOrHideShareQrcode(Lcom/android/settings/MiuiTetherSettings;Z)V

    iget-object p1, p0, Lcom/android/settings/MiuiTetherSettings$1;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-static {p1, v1}, Lcom/android/settings/MiuiTetherSettings;->-$$Nest$fputmPageRefreshComplete(Lcom/android/settings/MiuiTetherSettings;Z)V

    iget-object p0, p0, Lcom/android/settings/MiuiTetherSettings$1;->this$0:Lcom/android/settings/MiuiTetherSettings;

    invoke-virtual {p0}, Lcom/android/settings/MiuiTetherSettings;->highlightPreferenceIfNeeded()V

    return-void
.end method
