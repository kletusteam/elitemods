.class final enum Lcom/android/settings/TrustedCredentialsSettings$Tab;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/TrustedCredentialsSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Tab"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/android/settings/TrustedCredentialsSettings$Tab;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/settings/TrustedCredentialsSettings$Tab;

.field public static final enum SYSTEM:Lcom/android/settings/TrustedCredentialsSettings$Tab;

.field public static final enum USER:Lcom/android/settings/TrustedCredentialsSettings$Tab;


# instance fields
.field private final mLabel:I

.field final mSwitch:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetmLabel(Lcom/android/settings/TrustedCredentialsSettings$Tab;)I
    .locals 0

    iget p0, p0, Lcom/android/settings/TrustedCredentialsSettings$Tab;->mLabel:I

    return p0
.end method

.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Lcom/android/settings/TrustedCredentialsSettings$Tab;

    sget v1, Lcom/android/settings/R$string;->trusted_credentials_system_tab:I

    const-string v2, "SYSTEM"

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-direct {v0, v2, v3, v1, v4}, Lcom/android/settings/TrustedCredentialsSettings$Tab;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/android/settings/TrustedCredentialsSettings$Tab;->SYSTEM:Lcom/android/settings/TrustedCredentialsSettings$Tab;

    new-instance v1, Lcom/android/settings/TrustedCredentialsSettings$Tab;

    sget v2, Lcom/android/settings/R$string;->trusted_credentials_user_tab:I

    const-string v5, "USER"

    invoke-direct {v1, v5, v4, v2, v3}, Lcom/android/settings/TrustedCredentialsSettings$Tab;-><init>(Ljava/lang/String;IIZ)V

    sput-object v1, Lcom/android/settings/TrustedCredentialsSettings$Tab;->USER:Lcom/android/settings/TrustedCredentialsSettings$Tab;

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/android/settings/TrustedCredentialsSettings$Tab;

    aput-object v0, v2, v3

    aput-object v1, v2, v4

    sput-object v2, Lcom/android/settings/TrustedCredentialsSettings$Tab;->$VALUES:[Lcom/android/settings/TrustedCredentialsSettings$Tab;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/android/settings/TrustedCredentialsSettings$Tab;->mLabel:I

    iput-boolean p4, p0, Lcom/android/settings/TrustedCredentialsSettings$Tab;->mSwitch:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/settings/TrustedCredentialsSettings$Tab;
    .locals 1

    const-class v0, Lcom/android/settings/TrustedCredentialsSettings$Tab;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/android/settings/TrustedCredentialsSettings$Tab;

    return-object p0
.end method

.method public static values()[Lcom/android/settings/TrustedCredentialsSettings$Tab;
    .locals 1

    sget-object v0, Lcom/android/settings/TrustedCredentialsSettings$Tab;->$VALUES:[Lcom/android/settings/TrustedCredentialsSettings$Tab;

    invoke-virtual {v0}, [Lcom/android/settings/TrustedCredentialsSettings$Tab;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/settings/TrustedCredentialsSettings$Tab;

    return-object v0
.end method


# virtual methods
.method deleted(Landroid/security/IKeyChainService;Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    goto/32 :goto_9

    nop

    :goto_0
    return p0

    :goto_1
    goto/32 :goto_6

    nop

    :goto_2
    invoke-direct {p0}, Ljava/lang/AssertionError;-><init>()V

    goto/32 :goto_b

    nop

    :goto_3
    const/4 p1, 0x2

    goto/32 :goto_10

    nop

    :goto_4
    const/4 v0, 0x1

    goto/32 :goto_a

    nop

    :goto_5
    xor-int/2addr p0, v0

    goto/32 :goto_e

    nop

    :goto_6
    new-instance p0, Ljava/lang/AssertionError;

    goto/32 :goto_2

    nop

    :goto_7
    const/4 p0, 0x0

    goto/32 :goto_0

    nop

    :goto_8
    invoke-interface {p1, p2}, Landroid/security/IKeyChainService;->containsCaAlias(Ljava/lang/String;)Z

    move-result p0

    goto/32 :goto_5

    nop

    :goto_9
    sget-object v0, Lcom/android/settings/TrustedCredentialsSettings$1;->$SwitchMap$com$android$settings$TrustedCredentialsSettings$Tab:[I

    goto/32 :goto_f

    nop

    :goto_a
    if-ne p0, v0, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_3

    nop

    :goto_b
    throw p0

    :goto_c
    goto/32 :goto_8

    nop

    :goto_d
    aget p0, v0, p0

    goto/32 :goto_4

    nop

    :goto_e
    return p0

    :goto_f
    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result p0

    goto/32 :goto_d

    nop

    :goto_10
    if-eq p0, p1, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_7

    nop
.end method

.method getAliases(Landroid/security/IKeyChainService;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/security/IKeyChainService;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    goto/32 :goto_5

    nop

    :goto_0
    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result p0

    goto/32 :goto_3

    nop

    :goto_1
    return-object p0

    :goto_2
    new-instance p0, Ljava/lang/AssertionError;

    goto/32 :goto_11

    nop

    :goto_3
    aget p0, v0, p0

    goto/32 :goto_d

    nop

    :goto_4
    invoke-virtual {p0}, Landroid/content/pm/StringParceledListSlice;->getList()Ljava/util/List;

    move-result-object p0

    goto/32 :goto_1

    nop

    :goto_5
    sget-object v0, Lcom/android/settings/TrustedCredentialsSettings$1;->$SwitchMap$com$android$settings$TrustedCredentialsSettings$Tab:[I

    goto/32 :goto_0

    nop

    :goto_6
    throw p0

    :goto_7
    goto/32 :goto_e

    nop

    :goto_8
    if-ne p0, v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_9

    nop

    :goto_9
    const/4 v0, 0x2

    goto/32 :goto_a

    nop

    :goto_a
    if-eq p0, v0, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_10

    nop

    :goto_b
    return-object p0

    :goto_c
    goto/32 :goto_2

    nop

    :goto_d
    const/4 v0, 0x1

    goto/32 :goto_8

    nop

    :goto_e
    invoke-interface {p1}, Landroid/security/IKeyChainService;->getSystemCaAliases()Landroid/content/pm/StringParceledListSlice;

    move-result-object p0

    goto/32 :goto_4

    nop

    :goto_f
    invoke-virtual {p0}, Landroid/content/pm/StringParceledListSlice;->getList()Ljava/util/List;

    move-result-object p0

    goto/32 :goto_b

    nop

    :goto_10
    invoke-interface {p1}, Landroid/security/IKeyChainService;->getUserCaAliases()Landroid/content/pm/StringParceledListSlice;

    move-result-object p0

    goto/32 :goto_f

    nop

    :goto_11
    invoke-direct {p0}, Ljava/lang/AssertionError;-><init>()V

    goto/32 :goto_6

    nop
.end method
