.class public final enum Lcom/android/settings/ShortcutHelper$Shortcut;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/ShortcutHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Shortcut"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/android/settings/ShortcutHelper$Shortcut;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/settings/ShortcutHelper$Shortcut;

.field public static final enum ANTISPAM:Lcom/android/settings/ShortcutHelper$Shortcut;

.field public static final enum NETWORK_ASSISTANT:Lcom/android/settings/ShortcutHelper$Shortcut;

.field public static final enum OPTIMIZE_CENTER:Lcom/android/settings/ShortcutHelper$Shortcut;

.field public static final enum PERM_CENTER:Lcom/android/settings/ShortcutHelper$Shortcut;

.field public static final enum POWER_CENTER:Lcom/android/settings/ShortcutHelper$Shortcut;

.field public static final enum VIRUS_CENTER:Lcom/android/settings/ShortcutHelper$Shortcut;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    new-instance v0, Lcom/android/settings/ShortcutHelper$Shortcut;

    const-string v1, "OPTIMIZE_CENTER"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/android/settings/ShortcutHelper$Shortcut;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/settings/ShortcutHelper$Shortcut;->OPTIMIZE_CENTER:Lcom/android/settings/ShortcutHelper$Shortcut;

    new-instance v1, Lcom/android/settings/ShortcutHelper$Shortcut;

    const-string v3, "POWER_CENTER"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/android/settings/ShortcutHelper$Shortcut;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/android/settings/ShortcutHelper$Shortcut;->POWER_CENTER:Lcom/android/settings/ShortcutHelper$Shortcut;

    new-instance v3, Lcom/android/settings/ShortcutHelper$Shortcut;

    const-string v5, "VIRUS_CENTER"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/android/settings/ShortcutHelper$Shortcut;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/android/settings/ShortcutHelper$Shortcut;->VIRUS_CENTER:Lcom/android/settings/ShortcutHelper$Shortcut;

    new-instance v5, Lcom/android/settings/ShortcutHelper$Shortcut;

    const-string v7, "PERM_CENTER"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/android/settings/ShortcutHelper$Shortcut;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/android/settings/ShortcutHelper$Shortcut;->PERM_CENTER:Lcom/android/settings/ShortcutHelper$Shortcut;

    new-instance v7, Lcom/android/settings/ShortcutHelper$Shortcut;

    const-string v9, "NETWORK_ASSISTANT"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/android/settings/ShortcutHelper$Shortcut;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/android/settings/ShortcutHelper$Shortcut;->NETWORK_ASSISTANT:Lcom/android/settings/ShortcutHelper$Shortcut;

    new-instance v9, Lcom/android/settings/ShortcutHelper$Shortcut;

    const-string v11, "ANTISPAM"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/android/settings/ShortcutHelper$Shortcut;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/android/settings/ShortcutHelper$Shortcut;->ANTISPAM:Lcom/android/settings/ShortcutHelper$Shortcut;

    const/4 v11, 0x6

    new-array v11, v11, [Lcom/android/settings/ShortcutHelper$Shortcut;

    aput-object v0, v11, v2

    aput-object v1, v11, v4

    aput-object v3, v11, v6

    aput-object v5, v11, v8

    aput-object v7, v11, v10

    aput-object v9, v11, v12

    sput-object v11, Lcom/android/settings/ShortcutHelper$Shortcut;->$VALUES:[Lcom/android/settings/ShortcutHelper$Shortcut;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/settings/ShortcutHelper$Shortcut;
    .locals 1

    const-class v0, Lcom/android/settings/ShortcutHelper$Shortcut;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/android/settings/ShortcutHelper$Shortcut;

    return-object p0
.end method

.method public static values()[Lcom/android/settings/ShortcutHelper$Shortcut;
    .locals 1

    sget-object v0, Lcom/android/settings/ShortcutHelper$Shortcut;->$VALUES:[Lcom/android/settings/ShortcutHelper$Shortcut;

    invoke-virtual {v0}, [Lcom/android/settings/ShortcutHelper$Shortcut;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/settings/ShortcutHelper$Shortcut;

    return-object v0
.end method
