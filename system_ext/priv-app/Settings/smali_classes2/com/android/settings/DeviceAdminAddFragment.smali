.class public Lcom/android/settings/DeviceAdminAddFragment;
.super Lcom/android/settings/BaseFragment;


# instance fields
.field mActionButton:Landroid/widget/Button;

.field final mActivePolicies:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field mAddMsg:Landroid/widget/TextView;

.field mAddMsgEllipsized:Z

.field mAddMsgExpander:Landroid/widget/ImageView;

.field mAddMsgText:Ljava/lang/CharSequence;

.field mAdding:Z

.field final mAddingPolicies:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field mAdminDescription:Landroid/widget/TextView;

.field mAdminIcon:Landroid/widget/ImageView;

.field mAdminName:Landroid/widget/TextView;

.field mAdminPolicies:Landroid/view/ViewGroup;

.field mAdminWarning:Landroid/widget/TextView;

.field mCancelButton:Landroid/widget/Button;

.field mDPM:Landroid/app/admin/DevicePolicyManager;

.field mDeviceAdmin:Landroid/app/admin/DeviceAdminInfo;

.field private mExtraDisableWarningMsg:Ljava/lang/CharSequence;

.field mHandler:Landroid/os/Handler;

.field mRefreshing:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetmExtraDisableWarningMsg(Lcom/android/settings/DeviceAdminAddFragment;)Ljava/lang/CharSequence;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mExtraDisableWarningMsg:Ljava/lang/CharSequence;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmExtraDisableWarningMsg(Lcom/android/settings/DeviceAdminAddFragment;Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/DeviceAdminAddFragment;->mExtraDisableWarningMsg:Ljava/lang/CharSequence;

    return-void
.end method

.method static bridge synthetic -$$Nest$misManagedProfile(Lcom/android/settings/DeviceAdminAddFragment;Landroid/app/admin/DeviceAdminInfo;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/DeviceAdminAddFragment;->isManagedProfile(Landroid/app/admin/DeviceAdminInfo;)Z

    move-result p0

    return p0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/BaseFragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAddMsgEllipsized:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAddingPolicies:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mActivePolicies:Ljava/util/ArrayList;

    return-void
.end method

.method private getPermissionItemView(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/view/View;
    .locals 5

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    sget v1, Lcom/android/settings/R$layout;->app_permission_item_old:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/android/settings/R$id;->permission_group:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v2, Lcom/android/settings/R$id;->permission_list:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sget v3, Lcom/android/settings/R$id;->perm_icon:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v4, Lcom/android/settings/R$drawable;->ic_text_dot:I

    invoke-virtual {p0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v3, p0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    if-eqz p1, :cond_0

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 p0, 0x8

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-object v0
.end method

.method private isManagedProfile(Landroid/app/admin/DeviceAdminInfo;)Z
    .locals 0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    invoke-static {p0}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object p0

    invoke-virtual {p1}, Landroid/app/admin/DeviceAdminInfo;->getActivityInfo()Landroid/content/pm/ActivityInfo;

    move-result-object p1

    iget-object p1, p1, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget p1, p1, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {p1}, Landroid/os/UserHandle;->getUserId(I)I

    move-result p1

    invoke-virtual {p0, p1}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/pm/UserInfo;->isManagedProfile()Z

    move-result p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static setViewVisibility(Ljava/util/ArrayList;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;I)V"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2, p1}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public doInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 0

    sget p0, Lcom/android/settings/R$layout;->miui_device_admin_add:I

    const/4 p3, 0x0

    invoke-virtual {p1, p0, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method getEllipsizedLines()I
    .locals 1

    goto/32 :goto_8

    nop

    :goto_0
    invoke-interface {p0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p0

    goto/32 :goto_c

    nop

    :goto_1
    return p0

    :goto_2
    const/4 p0, 0x2

    :goto_3
    goto/32 :goto_1

    nop

    :goto_4
    check-cast p0, Landroid/view/WindowManager;

    goto/32 :goto_0

    nop

    :goto_5
    invoke-virtual {p0, v0}, Lcom/android/settings/BaseFragment;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    goto/32 :goto_4

    nop

    :goto_6
    if-gt v0, p0, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_9

    nop

    :goto_7
    invoke-virtual {p0}, Landroid/view/Display;->getWidth()I

    move-result p0

    goto/32 :goto_6

    nop

    :goto_8
    const-string/jumbo v0, "window"

    goto/32 :goto_5

    nop

    :goto_9
    const/4 p0, 0x5

    goto/32 :goto_a

    nop

    :goto_a
    goto :goto_3

    :goto_b
    goto/32 :goto_2

    nop

    :goto_c
    invoke-virtual {p0}, Landroid/view/Display;->getHeight()I

    move-result v0

    goto/32 :goto_7

    nop
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12

    const-string v0, "Bad "

    const-string v1, "Unable to retrieve device policy "

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    new-instance v2, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-direct {v2, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/android/settings/DeviceAdminAddFragment;->mHandler:Landroid/os/Handler;

    const-string p1, "device_policy"

    invoke-virtual {p0, p1}, Lcom/android/settings/BaseFragment;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/admin/DevicePolicyManager;

    iput-object p1, p0, Lcom/android/settings/DeviceAdminAddFragment;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    const-string v2, "android.app.extra.DEVICE_ADMIN"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Landroid/content/ComponentName;

    const-string v2, "DeviceAdminAdd"

    if-nez p1, :cond_0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "No component specified in "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/android/settings/BaseFragment;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/BaseFragment;->finish()V

    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/android/settings/BaseFragment;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/16 v4, 0x80

    invoke-virtual {v3, p1, v4}, Landroid/content/pm/PackageManager;->getReceiverInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v3
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_4

    iget-object v4, p0, Lcom/android/settings/DeviceAdminAddFragment;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v4, p1}, Landroid/app/admin/DevicePolicyManager;->isAdminActive(Landroid/content/ComponentName;)Z

    move-result v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-nez v4, :cond_4

    invoke-virtual {p0}, Lcom/android/settings/BaseFragment;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    new-instance v7, Landroid/content/Intent;

    const-string v8, "android.app.action.DEVICE_ADMIN_ENABLED"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const v8, 0x8000

    invoke-virtual {v4, v7, v8}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_1

    move v7, v6

    goto :goto_0

    :cond_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    :goto_0
    move v8, v6

    :goto_1
    if-ge v8, v7, :cond_3

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/pm/ResolveInfo;

    iget-object v10, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v11, v9, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    iget-object v10, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    iget-object v11, v9, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    :try_start_1
    iput-object v3, v9, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    new-instance v4, Landroid/app/admin/DeviceAdminInfo;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v7

    invoke-direct {v4, v7, v9}, Landroid/app/admin/DeviceAdminInfo;-><init>(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)V
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move v0, v5

    goto :goto_3

    :catch_0
    move-exception v4

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v9, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :catch_1
    move-exception v4

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v9, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_3
    :goto_2
    move v0, v6

    :goto_3
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Request to add invalid device admin: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/BaseFragment;->finish()V

    return-void

    :cond_4
    new-instance v0, Landroid/content/pm/ResolveInfo;

    invoke-direct {v0}, Landroid/content/pm/ResolveInfo;-><init>()V

    iput-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    :try_start_2
    new-instance v3, Landroid/app/admin/DeviceAdminInfo;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Landroid/app/admin/DeviceAdminInfo;-><init>(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)V

    iput-object v3, p0, Lcom/android/settings/DeviceAdminAddFragment;->mDeviceAdmin:Landroid/app/admin/DeviceAdminInfo;
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    invoke-virtual {p0}, Lcom/android/settings/BaseFragment;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.app.action.ADD_DEVICE_ADMIN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iput-boolean v6, p0, Lcom/android/settings/DeviceAdminAddFragment;->mRefreshing:Z

    iget-object v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, p1}, Landroid/app/admin/DevicePolicyManager;->isAdminActive(Landroid/content/ComponentName;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mDeviceAdmin:Landroid/app/admin/DeviceAdminInfo;

    invoke-virtual {v0}, Landroid/app/admin/DeviceAdminInfo;->getUsedPolicies()Ljava/util/ArrayList;

    move-result-object v0

    :goto_4
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v6, v1, :cond_6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;

    iget-object v2, p0, Lcom/android/settings/DeviceAdminAddFragment;->mDPM:Landroid/app/admin/DevicePolicyManager;

    iget v1, v1, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;->ident:I

    invoke-virtual {v2, p1, v1}, Landroid/app/admin/DevicePolicyManager;->hasGrantedPolicy(Landroid/content/ComponentName;I)Z

    move-result v1

    if-nez v1, :cond_5

    iput-boolean v5, p0, Lcom/android/settings/DeviceAdminAddFragment;->mRefreshing:Z

    goto :goto_5

    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_6
    :goto_5
    iget-boolean p1, p0, Lcom/android/settings/DeviceAdminAddFragment;->mRefreshing:Z

    if-nez p1, :cond_7

    const/4 p1, -0x1

    invoke-virtual {p0, p1}, Lcom/android/settings/BaseFragment;->setResult(I)V

    invoke-virtual {p0}, Lcom/android/settings/BaseFragment;->finish()V

    return-void

    :cond_7
    invoke-virtual {p0}, Lcom/android/settings/BaseFragment;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "android.app.extra.ADD_EXPLANATION"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAddMsgText:Ljava/lang/CharSequence;

    return-void

    :catch_2
    move-exception v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p0}, Lcom/android/settings/BaseFragment;->finish()V

    return-void

    :catch_3
    move-exception v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p0}, Lcom/android/settings/BaseFragment;->finish()V

    return-void

    :catch_4
    move-exception v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p0}, Lcom/android/settings/BaseFragment;->finish()V

    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 2

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object p0

    return-object p0

    :cond_0
    new-instance p1, Lmiuix/appcompat/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p1, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mExtraDisableWarningMsg:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    sget v0, Lcom/android/settings/R$string;->dlg_ok:I

    new-instance v1, Lcom/android/settings/DeviceAdminAddFragment$4;

    invoke-direct {v1, p0}, Lcom/android/settings/DeviceAdminAddFragment$4;-><init>(Lcom/android/settings/DeviceAdminAddFragment;)V

    invoke-virtual {p1, v0, v1}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    sget p0, Lcom/android/settings/R$string;->dlg_cancel:I

    const/4 v0, 0x0

    invoke-virtual {p1, p0, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    invoke-virtual {p1}, Lmiuix/appcompat/app/AlertDialog$Builder;->create()Lmiuix/appcompat/app/AlertDialog;

    move-result-object p0

    return-object p0
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onResume()V

    invoke-virtual {p0}, Lcom/android/settings/DeviceAdminAddFragment;->updateInterface()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    sget v0, Lcom/android/settings/R$id;->admin_icon:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAdminIcon:Landroid/widget/ImageView;

    sget v0, Lcom/android/settings/R$id;->admin_name:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAdminName:Landroid/widget/TextView;

    sget v0, Lcom/android/settings/R$id;->admin_description:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAdminDescription:Landroid/widget/TextView;

    sget v0, Lcom/android/settings/R$id;->add_msg:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAddMsg:Landroid/widget/TextView;

    sget v0, Lcom/android/settings/R$id;->add_msg_expander:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAddMsgExpander:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAddMsg:Landroid/widget/TextView;

    new-instance v1, Lcom/android/settings/DeviceAdminAddFragment$1;

    invoke-direct {v1, p0}, Lcom/android/settings/DeviceAdminAddFragment$1;-><init>(Lcom/android/settings/DeviceAdminAddFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAddMsg:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/android/settings/DeviceAdminAddFragment;->toggleMessageEllipsis(Landroid/view/View;)V

    sget v0, Lcom/android/settings/R$id;->admin_warning:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAdminWarning:Landroid/widget/TextView;

    sget v0, Lcom/android/settings/R$id;->admin_policies:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAdminPolicies:Landroid/view/ViewGroup;

    sget v0, Lcom/android/settings/R$id;->cancel_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mCancelButton:Landroid/widget/Button;

    new-instance v1, Lcom/android/settings/DeviceAdminAddFragment$2;

    invoke-direct {v1, p0}, Lcom/android/settings/DeviceAdminAddFragment$2;-><init>(Lcom/android/settings/DeviceAdminAddFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/android/settings/R$id;->action_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mActionButton:Landroid/widget/Button;

    new-instance v1, Lcom/android/settings/DeviceAdminAddFragment$3;

    invoke-direct {v1, p0}, Lcom/android/settings/DeviceAdminAddFragment$3;-><init>(Lcom/android/settings/DeviceAdminAddFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-super {p0, p1, p2}, Lcom/android/settings/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    return-void
.end method

.method toggleMessageEllipsis(Landroid/view/View;)V
    .locals 1

    goto/32 :goto_d

    nop

    :goto_0
    const p0, 0x1108001b

    goto/32 :goto_14

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_f

    :cond_0
    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {p0}, Lcom/android/settings/DeviceAdminAddFragment;->getEllipsizedLines()I

    move-result v0

    goto/32 :goto_e

    nop

    :goto_3
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    goto/32 :goto_c

    nop

    :goto_4
    if-nez p0, :cond_1

    goto/32 :goto_15

    :cond_1
    goto/32 :goto_0

    nop

    :goto_5
    const/4 v0, 0x0

    :goto_6
    goto/32 :goto_13

    nop

    :goto_7
    return-void

    :goto_8
    goto :goto_6

    :goto_9
    goto/32 :goto_5

    nop

    :goto_a
    const p0, 0x1108001a

    :goto_b
    goto/32 :goto_16

    nop

    :goto_c
    iget-object p1, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAddMsgExpander:Landroid/widget/ImageView;

    goto/32 :goto_10

    nop

    :goto_d
    check-cast p1, Landroid/widget/TextView;

    goto/32 :goto_12

    nop

    :goto_e
    goto :goto_1a

    :goto_f
    goto/32 :goto_19

    nop

    :goto_10
    iget-boolean p0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAddMsgEllipsized:Z

    goto/32 :goto_4

    nop

    :goto_11
    iget-boolean v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAddMsgEllipsized:Z

    goto/32 :goto_1

    nop

    :goto_12
    iget-boolean v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAddMsgEllipsized:Z

    goto/32 :goto_18

    nop

    :goto_13
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto/32 :goto_11

    nop

    :goto_14
    goto :goto_b

    :goto_15
    goto/32 :goto_a

    nop

    :goto_16
    invoke-virtual {p1, p0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/32 :goto_7

    nop

    :goto_17
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    goto/32 :goto_8

    nop

    :goto_18
    xor-int/lit8 v0, v0, 0x1

    goto/32 :goto_1b

    nop

    :goto_19
    const/16 v0, 0xf

    :goto_1a
    goto/32 :goto_3

    nop

    :goto_1b
    iput-boolean v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAddMsgEllipsized:Z

    goto/32 :goto_1c

    nop

    :goto_1c
    if-nez v0, :cond_2

    goto/32 :goto_9

    :cond_2
    goto/32 :goto_17

    nop
.end method

.method updateInterface()V
    .locals 7

    goto/32 :goto_32

    nop

    :goto_0
    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAdminDescription:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/settings/DeviceAdminAddFragment;->mDeviceAdmin:Landroid/app/admin/DeviceAdminInfo;

    invoke-virtual {p0}, Lcom/android/settings/BaseFragment;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/admin/DeviceAdminInfo;->loadDescription(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAdminDescription:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_22

    nop

    :goto_1
    sget v0, Lcom/android/settings/R$string;->remove_managed_profile_label:I

    goto/32 :goto_79

    nop

    :goto_2
    iget-object v2, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAddingPolicies:Ljava/util/ArrayList;

    goto/32 :goto_78

    nop

    :goto_3
    iget-object v2, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAddingPolicies:Ljava/util/ArrayList;

    goto/32 :goto_5e

    nop

    :goto_4
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_84

    nop

    :goto_5
    iget-object v2, p0, Lcom/android/settings/DeviceAdminAddFragment;->mDeviceAdmin:Landroid/app/admin/DeviceAdminInfo;

    goto/32 :goto_1b

    nop

    :goto_6
    iget-object v2, p0, Lcom/android/settings/DeviceAdminAddFragment;->mActivePolicies:Ljava/util/ArrayList;

    goto/32 :goto_10

    nop

    :goto_7
    iget v5, v5, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;->label:I

    goto/32 :goto_2c

    nop

    :goto_8
    iget-object v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mActionButton:Landroid/widget/Button;

    goto/32 :goto_23

    nop

    :goto_9
    iget-object v6, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAdminPolicies:Landroid/view/ViewGroup;

    goto/32 :goto_14

    nop

    :goto_a
    invoke-virtual {p0, v2, v3}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_72

    nop

    :goto_b
    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    goto/32 :goto_44

    nop

    :goto_c
    move v4, v1

    :goto_d
    goto/32 :goto_85

    nop

    :goto_e
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/32 :goto_49

    nop

    :goto_f
    iput-boolean v1, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAdding:Z

    goto/32 :goto_13

    nop

    :goto_10
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    goto/32 :goto_45

    nop

    :goto_11
    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    goto/32 :goto_37

    nop

    :goto_12
    check-cast v5, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;

    goto/32 :goto_3a

    nop

    :goto_13
    iget-object v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mDeviceAdmin:Landroid/app/admin/DeviceAdminInfo;

    goto/32 :goto_82

    nop

    :goto_14
    invoke-virtual {v6, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto/32 :goto_54

    nop

    :goto_15
    iget-object v1, p0, Lcom/android/settings/DeviceAdminAddFragment;->mDPM:Landroid/app/admin/DevicePolicyManager;

    goto/32 :goto_70

    nop

    :goto_16
    iput-boolean v3, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAdding:Z

    :goto_17
    goto/32 :goto_6a

    nop

    :goto_18
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_88

    nop

    :goto_19
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1a
    goto/32 :goto_80

    nop

    :goto_1b
    invoke-virtual {v2}, Landroid/app/admin/DeviceAdminInfo;->getUsedPolicies()Ljava/util/ArrayList;

    move-result-object v2

    goto/32 :goto_c

    nop

    :goto_1c
    if-lt v4, v5, :cond_0

    goto/32 :goto_59

    :cond_0
    goto/32 :goto_39

    nop

    :goto_1d
    iget-object v1, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAdminName:Landroid/widget/TextView;

    goto/32 :goto_4

    nop

    :goto_1e
    iget-object v1, p0, Lcom/android/settings/DeviceAdminAddFragment;->mDeviceAdmin:Landroid/app/admin/DeviceAdminInfo;

    goto/32 :goto_52

    nop

    :goto_1f
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_16

    nop

    :goto_20
    if-nez v1, :cond_1

    goto/32 :goto_17

    :cond_1
    goto/32 :goto_7e

    nop

    :goto_21
    iget-object v2, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAddingPolicies:Ljava/util/ArrayList;

    goto/32 :goto_61

    nop

    :goto_22
    goto :goto_1a

    :catch_0
    goto/32 :goto_8a

    nop

    :goto_23
    sget v1, Lcom/android/settings/R$string;->add_device_admin:I

    goto/32 :goto_5a

    nop

    :goto_24
    if-eqz v2, :cond_2

    goto/32 :goto_42

    :cond_2
    goto/32 :goto_57

    nop

    :goto_25
    iget-object v6, p0, Lcom/android/settings/DeviceAdminAddFragment;->mActivePolicies:Ljava/util/ArrayList;

    goto/32 :goto_8f

    nop

    :goto_26
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_75

    nop

    :goto_27
    invoke-direct {p0, v6, v5}, Lcom/android/settings/DeviceAdminAddFragment;->getPermissionItemView(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/view/View;

    move-result-object v5

    goto/32 :goto_7f

    nop

    :goto_28
    iget-object v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mActionButton:Landroid/widget/Button;

    goto/32 :goto_8e

    nop

    :goto_29
    invoke-virtual {v4}, Landroid/app/admin/DeviceAdminInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    goto/32 :goto_74

    nop

    :goto_2a
    iget-object v2, p0, Lcom/android/settings/DeviceAdminAddFragment;->mActivePolicies:Ljava/util/ArrayList;

    goto/32 :goto_76

    nop

    :goto_2b
    invoke-virtual {p0}, Lcom/android/settings/BaseFragment;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    goto/32 :goto_36

    nop

    :goto_2c
    invoke-virtual {p0, v5}, Landroidx/fragment/app/Fragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    goto/32 :goto_30

    nop

    :goto_2d
    invoke-virtual {v1}, Lmiuix/appcompat/app/AppCompatActivity;->getAppCompatActionBar()Lmiuix/appcompat/app/ActionBar;

    move-result-object v1

    goto/32 :goto_8d

    nop

    :goto_2e
    if-nez v0, :cond_3

    goto/32 :goto_17

    :cond_3
    goto/32 :goto_20

    nop

    :goto_2f
    const/16 v0, 0x8

    goto/32 :goto_0

    nop

    :goto_30
    const-string v6, ""

    goto/32 :goto_5b

    nop

    :goto_31
    check-cast v5, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;

    goto/32 :goto_7

    nop

    :goto_32
    iget-object v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAdminIcon:Landroid/widget/ImageView;

    goto/32 :goto_40

    nop

    :goto_33
    invoke-virtual {p0}, Lcom/android/settings/BaseFragment;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    goto/32 :goto_48

    nop

    :goto_34
    iget v5, v5, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;->description:I

    goto/32 :goto_46

    nop

    :goto_35
    iget-object v5, p0, Lcom/android/settings/DeviceAdminAddFragment;->mDeviceAdmin:Landroid/app/admin/DeviceAdminInfo;

    goto/32 :goto_56

    nop

    :goto_36
    invoke-virtual {v1, v2}, Landroid/app/admin/DeviceAdminInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto/32 :goto_7b

    nop

    :goto_37
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_f

    nop

    :goto_38
    sget v2, Lcom/android/settings/R$string;->device_admin_status:I

    goto/32 :goto_68

    nop

    :goto_39
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    goto/32 :goto_31

    nop

    :goto_3a
    iget v6, v5, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;->label:I

    goto/32 :goto_3b

    nop

    :goto_3b
    invoke-virtual {p0, v6}, Landroidx/fragment/app/Fragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    goto/32 :goto_34

    nop

    :goto_3c
    move v4, v1

    :goto_3d
    goto/32 :goto_63

    nop

    :goto_3e
    iget-object v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAdminWarning:Landroid/widget/TextView;

    goto/32 :goto_4c

    nop

    :goto_3f
    iget-object v2, p0, Lcom/android/settings/DeviceAdminAddFragment;->mDPM:Landroid/app/admin/DevicePolicyManager;

    goto/32 :goto_77

    nop

    :goto_40
    iget-object v1, p0, Lcom/android/settings/DeviceAdminAddFragment;->mDeviceAdmin:Landroid/app/admin/DeviceAdminInfo;

    goto/32 :goto_2b

    nop

    :goto_41
    goto :goto_3d

    :goto_42
    goto/32 :goto_21

    nop

    :goto_43
    iget-object p0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mActionButton:Landroid/widget/Button;

    goto/32 :goto_1

    nop

    :goto_44
    invoke-virtual {p0}, Lcom/android/settings/BaseFragment;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    goto/32 :goto_6c

    nop

    :goto_45
    if-eqz v2, :cond_4

    goto/32 :goto_59

    :cond_4
    goto/32 :goto_5

    nop

    :goto_46
    invoke-virtual {p0, v5}, Landroidx/fragment/app/Fragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    goto/32 :goto_27

    nop

    :goto_47
    iget-boolean v2, p0, Lcom/android/settings/DeviceAdminAddFragment;->mRefreshing:Z

    goto/32 :goto_7d

    nop

    :goto_48
    invoke-virtual {v0, v1}, Landroid/app/admin/DeviceAdminInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    goto/32 :goto_1d

    nop

    :goto_49
    goto/16 :goto_6f

    :goto_4a
    goto/32 :goto_8b

    nop

    :goto_4b
    invoke-virtual {v4}, Landroid/app/admin/DeviceAdminInfo;->getActivityInfo()Landroid/content/pm/ActivityInfo;

    move-result-object v4

    goto/32 :goto_6d

    nop

    :goto_4c
    sget v2, Lcom/android/settings/R$string;->device_admin_warning:I

    goto/32 :goto_5d

    nop

    :goto_4d
    if-nez v2, :cond_5

    goto/32 :goto_65

    :cond_5
    goto/32 :goto_6

    nop

    :goto_4e
    iget-object v3, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAddMsg:Landroid/widget/TextView;

    goto/32 :goto_26

    nop

    :goto_4f
    iget-object v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mDeviceAdmin:Landroid/app/admin/DeviceAdminInfo;

    goto/32 :goto_33

    nop

    :goto_50
    aput-object v5, v4, v1

    goto/32 :goto_8c

    nop

    :goto_51
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_8

    nop

    :goto_52
    invoke-direct {p0, v1}, Lcom/android/settings/DeviceAdminAddFragment;->isManagedProfile(Landroid/app/admin/DeviceAdminInfo;)Z

    move-result v1

    goto/32 :goto_2e

    nop

    :goto_53
    iget-object v4, p0, Lcom/android/settings/DeviceAdminAddFragment;->mDeviceAdmin:Landroid/app/admin/DeviceAdminInfo;

    goto/32 :goto_4b

    nop

    :goto_54
    add-int/lit8 v4, v4, 0x1

    goto/32 :goto_58

    nop

    :goto_55
    invoke-virtual {v0, v1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_1e

    nop

    :goto_56
    invoke-virtual {v5}, Landroid/app/admin/DeviceAdminInfo;->getActivityInfo()Landroid/content/pm/ActivityInfo;

    move-result-object v5

    goto/32 :goto_b

    nop

    :goto_57
    iget-object v2, p0, Lcom/android/settings/DeviceAdminAddFragment;->mDeviceAdmin:Landroid/app/admin/DeviceAdminInfo;

    goto/32 :goto_62

    nop

    :goto_58
    goto/16 :goto_d

    :goto_59
    goto/32 :goto_2a

    nop

    :goto_5a
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    goto/32 :goto_1f

    nop

    :goto_5b
    invoke-direct {p0, v5, v6}, Lcom/android/settings/DeviceAdminAddFragment;->getPermissionItemView(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/view/View;

    move-result-object v5

    goto/32 :goto_25

    nop

    :goto_5c
    if-nez v2, :cond_6

    goto/32 :goto_4a

    :cond_6
    goto/32 :goto_4e

    nop

    :goto_5d
    new-array v4, v3, [Ljava/lang/Object;

    goto/32 :goto_35

    nop

    :goto_5e
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    goto/32 :goto_24

    nop

    :goto_5f
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    goto/32 :goto_12

    nop

    :goto_60
    invoke-virtual {v4, v5}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    goto/32 :goto_86

    nop

    :goto_61
    invoke-static {v2, v1}, Lcom/android/settings/DeviceAdminAddFragment;->setViewVisibility(Ljava/util/ArrayList;I)V

    goto/32 :goto_67

    nop

    :goto_62
    invoke-virtual {v2}, Landroid/app/admin/DeviceAdminInfo;->getUsedPolicies()Ljava/util/ArrayList;

    move-result-object v2

    goto/32 :goto_3c

    nop

    :goto_63
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    goto/32 :goto_66

    nop

    :goto_64
    goto/16 :goto_17

    :goto_65
    goto/32 :goto_3

    nop

    :goto_66
    if-lt v4, v5, :cond_7

    goto/32 :goto_42

    :cond_7
    goto/32 :goto_5f

    nop

    :goto_67
    iget-object v2, p0, Lcom/android/settings/DeviceAdminAddFragment;->mActivePolicies:Ljava/util/ArrayList;

    goto/32 :goto_7c

    nop

    :goto_68
    new-array v3, v3, [Ljava/lang/Object;

    goto/32 :goto_53

    nop

    :goto_69
    invoke-virtual {p0}, Lcom/android/settings/BaseFragment;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    goto/32 :goto_60

    nop

    :goto_6a
    return-void

    :goto_6b
    add-int/lit8 v4, v4, 0x1

    goto/32 :goto_41

    nop

    :goto_6c
    invoke-virtual {v5, v6}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    goto/32 :goto_50

    nop

    :goto_6d
    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    goto/32 :goto_69

    nop

    :goto_6e
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_6f
    goto/32 :goto_47

    nop

    :goto_70
    invoke-virtual {v1}, Landroid/app/admin/DevicePolicyManager;->getProfileOwner()Landroid/content/ComponentName;

    move-result-object v1

    goto/32 :goto_55

    nop

    :goto_71
    sget v1, Lcom/android/settings/R$string;->admin_profile_owner_message:I

    goto/32 :goto_87

    nop

    :goto_72
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_28

    nop

    :goto_73
    iget-object v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAdminWarning:Landroid/widget/TextView;

    goto/32 :goto_38

    nop

    :goto_74
    invoke-virtual {v2, v4}, Landroid/app/admin/DevicePolicyManager;->isAdminActive(Landroid/content/ComponentName;)Z

    move-result v2

    goto/32 :goto_4d

    nop

    :goto_75
    iget-object v2, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAddMsg:Landroid/widget/TextView;

    goto/32 :goto_e

    nop

    :goto_76
    invoke-static {v2, v1}, Lcom/android/settings/DeviceAdminAddFragment;->setViewVisibility(Ljava/util/ArrayList;I)V

    goto/32 :goto_2

    nop

    :goto_77
    iget-object v4, p0, Lcom/android/settings/DeviceAdminAddFragment;->mDeviceAdmin:Landroid/app/admin/DeviceAdminInfo;

    goto/32 :goto_29

    nop

    :goto_78
    invoke-static {v2, v0}, Lcom/android/settings/DeviceAdminAddFragment;->setViewVisibility(Ljava/util/ArrayList;I)V

    goto/32 :goto_73

    nop

    :goto_79
    invoke-virtual {p0, v0}, Landroid/widget/Button;->setText(I)V

    goto/32 :goto_64

    nop

    :goto_7a
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto/32 :goto_81

    nop

    :goto_7b
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/32 :goto_4f

    nop

    :goto_7c
    invoke-static {v2, v0}, Lcom/android/settings/DeviceAdminAddFragment;->setViewVisibility(Ljava/util/ArrayList;I)V

    goto/32 :goto_3e

    nop

    :goto_7d
    const/4 v3, 0x1

    goto/32 :goto_89

    nop

    :goto_7e
    iget-object v0, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAdminWarning:Landroid/widget/TextView;

    goto/32 :goto_71

    nop

    :goto_7f
    iget-object v6, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAddingPolicies:Ljava/util/ArrayList;

    goto/32 :goto_18

    nop

    :goto_80
    iget-object v2, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAddMsgText:Ljava/lang/CharSequence;

    goto/32 :goto_5c

    nop

    :goto_81
    iget-object v2, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAddMsgExpander:Landroid/widget/ImageView;

    goto/32 :goto_6e

    nop

    :goto_82
    invoke-virtual {v0}, Landroid/app/admin/DeviceAdminInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    goto/32 :goto_15

    nop

    :goto_83
    invoke-virtual {v6, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto/32 :goto_6b

    nop

    :goto_84
    invoke-virtual {p0}, Lmiuix/appcompat/app/Fragment;->getAppCompatActivity()Lmiuix/appcompat/app/AppCompatActivity;

    move-result-object v1

    goto/32 :goto_2d

    nop

    :goto_85
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    goto/32 :goto_1c

    nop

    :goto_86
    aput-object v4, v3, v1

    goto/32 :goto_a

    nop

    :goto_87
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/32 :goto_43

    nop

    :goto_88
    iget-object v6, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAdminPolicies:Landroid/view/ViewGroup;

    goto/32 :goto_83

    nop

    :goto_89
    if-eqz v2, :cond_8

    goto/32 :goto_65

    :cond_8
    goto/32 :goto_3f

    nop

    :goto_8a
    iget-object v2, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAdminDescription:Landroid/widget/TextView;

    goto/32 :goto_19

    nop

    :goto_8b
    iget-object v2, p0, Lcom/android/settings/DeviceAdminAddFragment;->mAddMsg:Landroid/widget/TextView;

    goto/32 :goto_7a

    nop

    :goto_8c
    invoke-virtual {p0, v2, v4}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_51

    nop

    :goto_8d
    invoke-virtual {v1, v0}, Landroidx/appcompat/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto/32 :goto_2f

    nop

    :goto_8e
    sget v2, Lcom/android/settings/R$string;->remove_device_admin:I

    goto/32 :goto_11

    nop

    :goto_8f
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_9

    nop
.end method
