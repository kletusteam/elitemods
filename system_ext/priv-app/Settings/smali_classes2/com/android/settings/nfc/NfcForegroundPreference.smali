.class public Lcom/android/settings/nfc/NfcForegroundPreference;
.super Lcom/android/settingslib/miuisettings/preference/ListPreference;

# interfaces
.implements Lcom/android/settings/nfc/PaymentBackend$Callback;
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private final mPaymentBackend:Lcom/android/settings/nfc/PaymentBackend;


# virtual methods
.method public onPaymentAppsChanged()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/nfc/NfcForegroundPreference;->refresh()V

    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 0

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p2}, Landroidx/preference/ListPreference;->setValue(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settings/nfc/NfcForegroundPreference;->refresh()V

    const/4 p0, 0x1

    return p0
.end method

.method protected persistString(Ljava/lang/String;)Z
    .locals 1

    iget-object p0, p0, Lcom/android/settings/nfc/NfcForegroundPreference;->mPaymentBackend:Lcom/android/settings/nfc/PaymentBackend;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    move p1, v0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/settings/nfc/PaymentBackend;->setForegroundMode(Z)V

    return v0
.end method

.method refresh()V
    .locals 1

    goto/32 :goto_b

    nop

    :goto_0
    invoke-virtual {p0, v0}, Landroidx/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/32 :goto_c

    nop

    :goto_1
    invoke-virtual {p0, v0}, Landroidx/preference/ListPreference;->setValue(Ljava/lang/String;)V

    :goto_2
    goto/32 :goto_6

    nop

    :goto_3
    invoke-virtual {p0, v0}, Landroidx/preference/ListPreference;->setValue(Ljava/lang/String;)V

    goto/32 :goto_9

    nop

    :goto_4
    const-string v0, "0"

    goto/32 :goto_1

    nop

    :goto_5
    invoke-virtual {v0}, Lcom/android/settings/nfc/PaymentBackend;->isForegroundMode()Z

    move-result v0

    goto/32 :goto_7

    nop

    :goto_6
    invoke-virtual {p0}, Landroidx/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_7
    if-nez v0, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_8

    nop

    :goto_8
    const-string v0, "1"

    goto/32 :goto_3

    nop

    :goto_9
    goto :goto_2

    :goto_a
    goto/32 :goto_4

    nop

    :goto_b
    iget-object v0, p0, Lcom/android/settings/nfc/NfcForegroundPreference;->mPaymentBackend:Lcom/android/settings/nfc/PaymentBackend;

    goto/32 :goto_5

    nop

    :goto_c
    return-void
.end method
