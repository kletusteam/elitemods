.class public Lcom/android/settings/nfc/MiuiAndroidBeam;
.super Lcom/android/settings/BaseFragment;


# instance fields
.field private mAndroidBeamSwitch:Lmiuix/slidingwidget/widget/SlidingButton;

.field private mNfcAdapter:Landroid/nfc/NfcAdapter;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAndroidBeamSwitch(Lcom/android/settings/nfc/MiuiAndroidBeam;)Lmiuix/slidingwidget/widget/SlidingButton;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/nfc/MiuiAndroidBeam;->mAndroidBeamSwitch:Lmiuix/slidingwidget/widget/SlidingButton;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmNfcAdapter(Lcom/android/settings/nfc/MiuiAndroidBeam;)Landroid/nfc/NfcAdapter;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/nfc/MiuiAndroidBeam;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    return-object p0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/BaseFragment;-><init>()V

    return-void
.end method

.method private initView(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/nfc/MiuiAndroidBeam;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget v0, Lcom/android/settings/R$id;->android_beam_switch:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object p1, p0, Lcom/android/settings/nfc/MiuiAndroidBeam;->mAndroidBeamSwitch:Lmiuix/slidingwidget/widget/SlidingButton;

    iget-object v0, p0, Lcom/android/settings/nfc/MiuiAndroidBeam;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->isNdefPushEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    iget-object p1, p0, Lcom/android/settings/nfc/MiuiAndroidBeam;->mAndroidBeamSwitch:Lmiuix/slidingwidget/widget/SlidingButton;

    new-instance v0, Lcom/android/settings/nfc/MiuiAndroidBeam$1;

    invoke-direct {v0, p0}, Lcom/android/settings/nfc/MiuiAndroidBeam$1;-><init>(Lcom/android/settings/nfc/MiuiAndroidBeam;)V

    invoke-virtual {p1, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method


# virtual methods
.method public doInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    sget p3, Lcom/android/settings/R$layout;->android_beam:I

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/android/settings/nfc/MiuiAndroidBeam;->initView(Landroid/view/View;)V

    return-object p1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/settings/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-static {p1}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/nfc/MiuiAndroidBeam;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    if-nez p1, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p0

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method
