.class public Lcom/android/settings/nfc/NfcAirplaneModeObserver;
.super Landroid/database/ContentObserver;


# static fields
.field static final AIRPLANE_MODE_URI:Landroid/net/Uri;


# instance fields
.field private mAirplaneMode:I

.field private final mContext:Landroid/content/Context;

.field private final mNfcAdapter:Landroid/nfc/NfcAdapter;

.field private final mPreference:Landroidx/preference/Preference;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "airplane_mode_on"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/settings/nfc/NfcAirplaneModeObserver;->AIRPLANE_MODE_URI:Landroid/net/Uri;

    return-void
.end method

.method private updateNfcPreference()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/nfc/NfcAirplaneModeObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget v1, p0, Lcom/android/settings/nfc/NfcAirplaneModeObserver;->mAirplaneMode:I

    const-string v2, "airplane_mode_on"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iget v1, p0, Lcom/android/settings/nfc/NfcAirplaneModeObserver;->mAirplaneMode:I

    if-ne v0, v1, :cond_0

    return-void

    :cond_0
    iput v0, p0, Lcom/android/settings/nfc/NfcAirplaneModeObserver;->mAirplaneMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/settings/nfc/NfcAirplaneModeObserver;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->disable()Z

    iget-object v0, p0, Lcom/android/settings/nfc/NfcAirplaneModeObserver;->mPreference:Landroidx/preference/Preference;

    iget-object p0, p0, Lcom/android/settings/nfc/NfcAirplaneModeObserver;->mContext:Landroid/content/Context;

    invoke-static {p0}, Lcom/android/settings/nfc/NfcPreferenceController;->isToggleableInAirplaneMode(Landroid/content/Context;)Z

    move-result p0

    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setEnabled(Z)V

    goto :goto_0

    :cond_1
    iget-object p0, p0, Lcom/android/settings/nfc/NfcAirplaneModeObserver;->mPreference:Landroidx/preference/Preference;

    invoke-virtual {p0, v1}, Landroidx/preference/Preference;->setEnabled(Z)V

    :goto_0
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 0

    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    invoke-direct {p0}, Lcom/android/settings/nfc/NfcAirplaneModeObserver;->updateNfcPreference()V

    return-void
.end method
