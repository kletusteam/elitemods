.class Lcom/android/settings/nfc/MiuiAndroidBeam$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/nfc/MiuiAndroidBeam;->initView(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/nfc/MiuiAndroidBeam;


# direct methods
.method constructor <init>(Lcom/android/settings/nfc/MiuiAndroidBeam;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/nfc/MiuiAndroidBeam$1;->this$0:Lcom/android/settings/nfc/MiuiAndroidBeam;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    iget-object p1, p0, Lcom/android/settings/nfc/MiuiAndroidBeam$1;->this$0:Lcom/android/settings/nfc/MiuiAndroidBeam;

    invoke-static {p1}, Lcom/android/settings/nfc/MiuiAndroidBeam;->-$$Nest$fgetmAndroidBeamSwitch(Lcom/android/settings/nfc/MiuiAndroidBeam;)Lmiuix/slidingwidget/widget/SlidingButton;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/android/settings/nfc/MiuiAndroidBeam$1;->this$0:Lcom/android/settings/nfc/MiuiAndroidBeam;

    invoke-static {p1}, Lcom/android/settings/nfc/MiuiAndroidBeam;->-$$Nest$fgetmNfcAdapter(Lcom/android/settings/nfc/MiuiAndroidBeam;)Landroid/nfc/NfcAdapter;

    move-result-object p1

    invoke-virtual {p1}, Landroid/nfc/NfcAdapter;->enableNdefPush()Z

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/android/settings/nfc/MiuiAndroidBeam$1;->this$0:Lcom/android/settings/nfc/MiuiAndroidBeam;

    invoke-static {p1}, Lcom/android/settings/nfc/MiuiAndroidBeam;->-$$Nest$fgetmNfcAdapter(Lcom/android/settings/nfc/MiuiAndroidBeam;)Landroid/nfc/NfcAdapter;

    move-result-object p1

    invoke-virtual {p1}, Landroid/nfc/NfcAdapter;->disableNdefPush()Z

    :goto_0
    iget-object p0, p0, Lcom/android/settings/nfc/MiuiAndroidBeam$1;->this$0:Lcom/android/settings/nfc/MiuiAndroidBeam;

    invoke-static {p0}, Lcom/android/settings/nfc/MiuiAndroidBeam;->-$$Nest$fgetmAndroidBeamSwitch(Lcom/android/settings/nfc/MiuiAndroidBeam;)Lmiuix/slidingwidget/widget/SlidingButton;

    move-result-object p0

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    return-void
.end method
