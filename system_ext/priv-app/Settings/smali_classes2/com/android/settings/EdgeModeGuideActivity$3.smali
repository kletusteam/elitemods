.class Lcom/android/settings/EdgeModeGuideActivity$3;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/EdgeModeGuideActivity;->updateEdgeModeVideo()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/EdgeModeGuideActivity;


# direct methods
.method constructor <init>(Lcom/android/settings/EdgeModeGuideActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/EdgeModeGuideActivity$3;->this$0:Lcom/android/settings/EdgeModeGuideActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2

    iget-object p1, p0, Lcom/android/settings/EdgeModeGuideActivity$3;->this$0:Lcom/android/settings/EdgeModeGuideActivity;

    invoke-static {p1}, Lcom/android/settings/EdgeModeGuideActivity;->-$$Nest$fgetmHandler(Lcom/android/settings/EdgeModeGuideActivity;)Landroid/os/Handler;

    move-result-object p1

    iget-object p0, p0, Lcom/android/settings/EdgeModeGuideActivity$3;->this$0:Lcom/android/settings/EdgeModeGuideActivity;

    invoke-static {p0}, Lcom/android/settings/EdgeModeGuideActivity;->-$$Nest$fgetmHandler(Lcom/android/settings/EdgeModeGuideActivity;)Landroid/os/Handler;

    move-result-object p0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object p0

    const-wide/16 v0, 0x1f4

    invoke-virtual {p1, p0, v0, v1}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method
