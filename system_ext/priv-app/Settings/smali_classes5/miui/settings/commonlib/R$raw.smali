.class public final Lmiui/settings/commonlib/R$raw;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/settings/commonlib/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "raw"
.end annotation


# static fields
.field public static final aab_brightness:I = 0x7f110000

.field public static final accessibility_color_inversion_banner:I = 0x7f110001

.field public static final accessibility_magnification_banner:I = 0x7f110002

.field public static final accessibility_screen_magnification:I = 0x7f110003

.field public static final accessibility_shortcut_type_triple_tap:I = 0x7f110004

.field public static final accessibility_timeout:I = 0x7f110005

.field public static final accessibility_timeout_banner:I = 0x7f110006

.field public static final adaptive_sleep:I = 0x7f110007

.field public static final aigesture:I = 0x7f110008

.field public static final auto_awesome_battery:I = 0x7f110009

.field public static final auto_awesome_battery_lottie:I = 0x7f11000a

.field public static final back:I = 0x7f11000b

.field public static final backtap_guide:I = 0x7f11000c

.field public static final clean:I = 0x7f11000d

.field public static final control_center_guide_modern:I = 0x7f11000e

.field public static final control_center_guide_modern_international:I = 0x7f11000f

.field public static final core_scan_output_01:I = 0x7f110010

.field public static final core_scan_output_02:I = 0x7f110011

.field public static final core_scan_output_03:I = 0x7f110012

.field public static final core_scan_output_04:I = 0x7f110013

.field public static final core_scan_output_05:I = 0x7f110014

.field public static final core_scan_output_06:I = 0x7f110015

.field public static final core_scan_output_07:I = 0x7f110016

.field public static final core_scan_output_08:I = 0x7f110017

.field public static final core_scan_output_09:I = 0x7f110018

.field public static final core_scan_output_10:I = 0x7f110019

.field public static final core_scan_output_11:I = 0x7f11001a

.field public static final core_scan_output_12:I = 0x7f11001b

.field public static final double_click_power_key:I = 0x7f11001c

.field public static final double_click_volume_down_when_lock:I = 0x7f11001d

.field public static final double_knock:I = 0x7f11001e

.field public static final emergency_contact:I = 0x7f11001f

.field public static final extra_dim_banner:I = 0x7f110020

.field public static final face_education:I = 0x7f110021

.field public static final face_education_lottie:I = 0x7f110022

.field public static final face_enroll_introduction_animation:I = 0x7f110023

.field public static final face_settings:I = 0x7f110024

.field public static final finger_enroll_motion_01:I = 0x7f110025

.field public static final finger_enroll_motion_02:I = 0x7f110026

.field public static final finger_enroll_motion_03:I = 0x7f110027

.field public static final finger_enroll_motion_04:I = 0x7f110028

.field public static final finger_enroll_motion_05:I = 0x7f110029

.field public static final finger_enroll_motion_06:I = 0x7f11002a

.field public static final finger_enroll_motion_07:I = 0x7f11002b

.field public static final finger_enroll_motion_08:I = 0x7f11002c

.field public static final finger_enroll_motion_09:I = 0x7f11002d

.field public static final finger_enroll_motion_10:I = 0x7f11002e

.field public static final finger_enroll_motion_11:I = 0x7f11002f

.field public static final finger_enroll_motion_12:I = 0x7f110030

.field public static final finger_enroll_motion_13:I = 0x7f110031

.field public static final finger_enroll_motion_14:I = 0x7f110032

.field public static final finger_enroll_motion_15:I = 0x7f110033

.field public static final finger_enroll_motion_16:I = 0x7f110034

.field public static final finger_enroll_motion_17:I = 0x7f110035

.field public static final finger_enroll_motion_18:I = 0x7f110036

.field public static final finger_enroll_motion_19:I = 0x7f110037

.field public static final finger_enroll_motion_20:I = 0x7f110038

.field public static final finger_enroll_motion_21:I = 0x7f110039

.field public static final finger_enroll_motion_22:I = 0x7f11003a

.field public static final finger_enroll_motion_23:I = 0x7f11003b

.field public static final finger_enroll_motion_24:I = 0x7f11003c

.field public static final finger_enroll_motion_25:I = 0x7f11003d

.field public static final finger_enroll_success_motion:I = 0x7f11003e

.field public static final fingerprint_edu_lottie:I = 0x7f11003f

.field public static final fingerprint_edu_lottie_portrait:I = 0x7f110040

.field public static final fingerprint_location_animation:I = 0x7f110041

.field public static final freeform_guide:I = 0x7f110042

.field public static final freeform_guide_cvw:I = 0x7f110043

.field public static final freeform_guide_pin_fold_large:I = 0x7f110044

.field public static final freeform_guide_pin_fold_small:I = 0x7f110045

.field public static final freeform_guide_pin_pad:I = 0x7f110046

.field public static final gesture_ambient_lift:I = 0x7f110047

.field public static final gesture_ambient_tap:I = 0x7f110048

.field public static final gesture_ambient_tap_screen:I = 0x7f110049

.field public static final gesture_ambient_wake_lock_screen:I = 0x7f11004a

.field public static final gesture_ambient_wake_screen:I = 0x7f11004b

.field public static final gesture_assist:I = 0x7f11004c

.field public static final gesture_double_tap:I = 0x7f11004d

.field public static final gesture_fingerprint_swipe:I = 0x7f11004e

.field public static final gesture_global_actions_panel:I = 0x7f11004f

.field public static final gesture_prevent_ringing:I = 0x7f110050

.field public static final gesture_twist:I = 0x7f110051

.field public static final gxzw_anim_aurora:I = 0x7f110052

.field public static final gxzw_anim_light:I = 0x7f110053

.field public static final gxzw_anim_pulse:I = 0x7f110054

.field public static final gxzw_anim_star:I = 0x7f110055

.field public static final gxzw_quick_open_navi_fast:I = 0x7f110056

.field public static final gxzw_quick_open_navi_slow:I = 0x7f110057

.field public static final handymode_fullscreen_guide:I = 0x7f110058

.field public static final handymode_navigationbar_guide:I = 0x7f110059

.field public static final haptic01:I = 0x7f11005a

.field public static final haptic02:I = 0x7f11005b

.field public static final haptic03:I = 0x7f11005c

.field public static final haptic04:I = 0x7f11005d

.field public static final haptic05:I = 0x7f11005e

.field public static final haptic06:I = 0x7f11005f

.field public static final haptic07:I = 0x7f110060

.field public static final haptic08:I = 0x7f110061

.field public static final haptic09:I = 0x7f110062

.field public static final haptic10:I = 0x7f110063

.field public static final haptic11:I = 0x7f110064

.field public static final haptic12:I = 0x7f110065

.field public static final haptic13:I = 0x7f110066

.field public static final haptic14:I = 0x7f110067

.field public static final haptic15:I = 0x7f110068

.field public static final haptic16:I = 0x7f110069

.field public static final headset_fitness_detect:I = 0x7f11006a

.field public static final illustration_accessibility_gesture_three_finger:I = 0x7f11006b

.field public static final illustration_accessibility_gesture_two_finger:I = 0x7f11006c

.field public static final keep_screen_on:I = 0x7f11006d

.field public static final key_combination_left_power_volume_down:I = 0x7f11006e

.field public static final key_combination_power_volume_down:I = 0x7f11006f

.field public static final keys_car_open_l:I = 0x7f110070

.field public static final keys_kanata_open_l:I = 0x7f110071

.field public static final keys_mechanicals_open_l:I = 0x7f110072

.field public static final keys_scifi_open_l:I = 0x7f110073

.field public static final knock_gesture_v:I = 0x7f110074

.field public static final knock_long_press_horizontal_slid:I = 0x7f110075

.field public static final knock_slide_shape:I = 0x7f110076

.field public static final lock_screen_now:I = 0x7f110077

.field public static final long_press_power_key:I = 0x7f110078

.field public static final lottie_adaptive_battery:I = 0x7f110079

.field public static final lottie_adaptive_brightness:I = 0x7f11007a

.field public static final lottie_bubbles:I = 0x7f11007b

.field public static final lottie_button_nav_menu:I = 0x7f11007c

.field public static final lottie_lift_to_check_phone:I = 0x7f11007d

.field public static final lottie_one_hand_mode:I = 0x7f11007e

.field public static final lottie_power_menu:I = 0x7f11007f

.field public static final lottie_prevent_ringing:I = 0x7f110080

.field public static final lottie_swipe_fingerprint:I = 0x7f110081

.field public static final lottie_swipe_for_notifications:I = 0x7f110082

.field public static final lottie_system_nav_2_button:I = 0x7f110083

.field public static final lottie_system_nav_3_button:I = 0x7f110084

.field public static final lottie_system_nav_fully_gestural:I = 0x7f110085

.field public static final lottie_tap_to_check_phone:I = 0x7f110086

.field public static final m2_haptic04:I = 0x7f110087

.field public static final m2_haptic05:I = 0x7f110088

.field public static final m2_haptic07:I = 0x7f110089

.field public static final m2_haptic08:I = 0x7f11008a

.field public static final m2_haptic09:I = 0x7f11008b

.field public static final m2_haptic10:I = 0x7f11008c

.field public static final m2_haptic12:I = 0x7f11008d

.field public static final m2_haptic14:I = 0x7f11008e

.field public static final main_haptic_video:I = 0x7f11008f

.field public static final media_volume:I = 0x7f110090

.field public static final miui_face_input_suggestion_video:I = 0x7f110091

.field public static final miui_face_prompt:I = 0x7f110092

.field public static final miui_facea_input_success:I = 0x7f110093

.field public static final navigation_picker_full_screen:I = 0x7f110094

.field public static final notification_interruption_model:I = 0x7f110095

.field public static final number_picker_value_change:I = 0x7f110096

.field public static final photo:I = 0x7f110097

.field public static final screen_enhance_engine_memc_video:I = 0x7f110098

.field public static final screen_enhance_engine_s2h_video:I = 0x7f110099

.field public static final screen_enhance_engine_sr_video:I = 0x7f11009a

.field public static final sos:I = 0x7f11009b

.field public static final sos_player_voice:I = 0x7f11009c

.field public static final speaker_clean_sound:I = 0x7f11009d

.field public static final stylus_quick_notes:I = 0x7f11009e

.field public static final stylus_quick_screenshot:I = 0x7f11009f

.field public static final stylus_rotation:I = 0x7f1100a0

.field public static final swipe_up_to_continue_using:I = 0x7f1100a1

.field public static final system_nav_2_button:I = 0x7f1100a2

.field public static final system_nav_3_button:I = 0x7f1100a3

.field public static final system_nav_fully_gestural:I = 0x7f1100a4

.field public static final three_gesture_down:I = 0x7f1100a5

.field public static final three_gesture_horizontal:I = 0x7f1100a6

.field public static final three_gesture_long_press:I = 0x7f1100a7

.field public static final udfps_edu_a11y_lottie:I = 0x7f1100a8

.field public static final udfps_edu_lottie:I = 0x7f1100a9

.field public static final udfps_left_edge_hint_lottie:I = 0x7f1100aa

.field public static final udfps_right_edge_hint_lottie:I = 0x7f1100ab

.field public static final udfps_tip_hint_lottie:I = 0x7f1100ac

.field public static final v01:I = 0x7f1100ad

.field public static final v02:I = 0x7f1100ae

.field public static final v03:I = 0x7f1100af

.field public static final v04:I = 0x7f1100b0

.field public static final v05:I = 0x7f1100b1

.field public static final v06:I = 0x7f1100b2

.field public static final v07:I = 0x7f1100b3

.field public static final v08:I = 0x7f1100b4

.field public static final v09:I = 0x7f1100b5

.field public static final v10:I = 0x7f1100b6

.field public static final v11:I = 0x7f1100b7

.field public static final v12:I = 0x7f1100b8

.field public static final v13:I = 0x7f1100b9

.field public static final v14:I = 0x7f1100ba

.field public static final v15:I = 0x7f1100bb

.field public static final v16:I = 0x7f1100bc

.field public static final xiaoai_mitang_volume:I = 0x7f1100bd

.field public static final xiaomi_static_config:I = 0x7f1100be


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
