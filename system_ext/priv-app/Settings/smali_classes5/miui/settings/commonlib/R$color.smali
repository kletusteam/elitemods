.class public final Lmiui/settings/commonlib/R$color;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/settings/commonlib/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final SIM_color_blue:I = 0x7f060000

.field public static final SIM_color_blue800:I = 0x7f060001

.field public static final SIM_color_cyan:I = 0x7f060002

.field public static final SIM_color_green800:I = 0x7f060003

.field public static final SIM_color_indigo:I = 0x7f060004

.field public static final SIM_color_orange:I = 0x7f060005

.field public static final SIM_color_pink:I = 0x7f060006

.field public static final SIM_color_pink800:I = 0x7f060007

.field public static final SIM_color_purple:I = 0x7f060008

.field public static final SIM_color_purple800:I = 0x7f060009

.field public static final SIM_color_red:I = 0x7f06000a

.field public static final SIM_color_teal:I = 0x7f06000b

.field public static final SIM_dark_mode_color_blue:I = 0x7f06000c

.field public static final SIM_dark_mode_color_cyan:I = 0x7f06000d

.field public static final SIM_dark_mode_color_green:I = 0x7f06000e

.field public static final SIM_dark_mode_color_orange:I = 0x7f06000f

.field public static final SIM_dark_mode_color_pink:I = 0x7f060010

.field public static final SIM_dark_mode_color_purple:I = 0x7f060011

.field public static final a11ymenu_bg_color:I = 0x7f060012

.field public static final a11ymenu_grid_item_text_color:I = 0x7f060013

.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f060014

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f060015

.field public static final abc_btn_colored_borderless_text_material:I = 0x7f060016

.field public static final abc_btn_colored_text_material:I = 0x7f060017

.field public static final abc_color_highlight_material:I = 0x7f060018

.field public static final abc_decor_view_status_guard:I = 0x7f060019

.field public static final abc_decor_view_status_guard_light:I = 0x7f06001a

.field public static final abc_hint_foreground_material_dark:I = 0x7f06001b

.field public static final abc_hint_foreground_material_light:I = 0x7f06001c

.field public static final abc_input_method_navigation_guard:I = 0x7f06001d

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f06001e

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f06001f

.field public static final abc_primary_text_material_dark:I = 0x7f060020

.field public static final abc_primary_text_material_light:I = 0x7f060021

.field public static final abc_search_url_text:I = 0x7f060022

.field public static final abc_search_url_text_normal:I = 0x7f060023

.field public static final abc_search_url_text_pressed:I = 0x7f060024

.field public static final abc_search_url_text_selected:I = 0x7f060025

.field public static final abc_secondary_text_material_dark:I = 0x7f060026

.field public static final abc_secondary_text_material_light:I = 0x7f060027

.field public static final abc_tint_btn_checkable:I = 0x7f060028

.field public static final abc_tint_default:I = 0x7f060029

.field public static final abc_tint_edittext:I = 0x7f06002a

.field public static final abc_tint_seek_thumb:I = 0x7f06002b

.field public static final abc_tint_spinner:I = 0x7f06002c

.field public static final abc_tint_switch_track:I = 0x7f06002d

.field public static final accent_material_dark:I = 0x7f06002e

.field public static final accent_material_light:I = 0x7f06002f

.field public static final accent_select_background:I = 0x7f060030

.field public static final accent_select_primary_text:I = 0x7f060031

.field public static final accent_select_secondary_text:I = 0x7f060032

.field public static final accessibility_color_inversion_background:I = 0x7f060033

.field public static final accessibility_daltonizer_background:I = 0x7f060034

.field public static final accessibility_feature_background:I = 0x7f060035

.field public static final accessibility_magnification_background:I = 0x7f060036

.field public static final accessibility_settings_action_bar_active_color:I = 0x7f060037

.field public static final accessibility_settings_action_bar_inactive_color:I = 0x7f060038

.field public static final account_and_sync_item_bg:I = 0x7f060039

.field public static final account_exit_info_text_color:I = 0x7f06003a

.field public static final account_info_summary:I = 0x7f06003b

.field public static final account_info_text_color:I = 0x7f06003c

.field public static final action_bar_subtitle:I = 0x7f06003d

.field public static final action_button_edit_color:I = 0x7f06003e

.field public static final action_mode_title_default_button_text_color_disable_light:I = 0x7f06003f

.field public static final action_mode_title_default_button_text_color_normal_light:I = 0x7f060040

.field public static final action_mode_title_default_button_text_color_pressed_light:I = 0x7f060041

.field public static final action_mode_title_default_button_text_dark:I = 0x7f060042

.field public static final action_title_color:I = 0x7f060043

.field public static final actionbar_haptic_crisp_normal:I = 0x7f060044

.field public static final actionbar_haptic_crisp_select:I = 0x7f060045

.field public static final actionbar_text_selector_tablet:I = 0x7f060046

.field public static final ad_service_instructions_color:I = 0x7f060047

.field public static final add_account_text_color:I = 0x7f060048

.field public static final add_fingerprint_finish_title_text_color:I = 0x7f060049

.field public static final add_fingerprint_msg_text_color:I = 0x7f06004a

.field public static final add_fingerprint_title_text_color:I = 0x7f06004b

.field public static final advanced_icon_color:I = 0x7f06004c

.field public static final advanced_outline_color:I = 0x7f06004d

.field public static final ai_color_animation_black:I = 0x7f06004e

.field public static final ai_color_item_bg_normal:I = 0x7f06004f

.field public static final ai_color_item_bg_pressed:I = 0x7f060050

.field public static final ai_color_none_item_bg_normal:I = 0x7f060051

.field public static final ai_color_none_item_bg_pressed:I = 0x7f060052

.field public static final ai_settings_item_bg_color:I = 0x7f060053

.field public static final ai_settings_none_item_bg_color:I = 0x7f060054

.field public static final ai_settings_summary_color:I = 0x7f060055

.field public static final ai_settings_title_color:I = 0x7f060056

.field public static final ai_sub_background:I = 0x7f060057

.field public static final alpha_0:I = 0x7f060058

.field public static final anc_bar_background:I = 0x7f060059

.field public static final anc_text_color:I = 0x7f06005a

.field public static final anc_text_unselect_color:I = 0x7f06005b

.field public static final androidx_core_ripple_material_light:I = 0x7f06005c

.field public static final androidx_core_secondary_text_default_material_light:I = 0x7f06005d

.field public static final ap_battery_color:I = 0x7f06005e

.field public static final ap_battery_color_connected:I = 0x7f06005f

.field public static final application_mode_aplit:I = 0x7f060060

.field public static final auto_text_allowed_color:I = 0x7f060061

.field public static final back_gesture_indicator:I = 0x7f060062

.field public static final back_image_exit_color:I = 0x7f060063

.field public static final background_floating_material_dark:I = 0x7f060064

.field public static final background_floating_material_light:I = 0x7f060065

.field public static final background_green:I = 0x7f060066

.field public static final background_material_dark:I = 0x7f060067

.field public static final background_material_light:I = 0x7f060068

.field public static final backup_issue_title_color:I = 0x7f060069

.field public static final backup_settings_color:I = 0x7f06006a

.field public static final balance_seekbar_center_marker_color:I = 0x7f06006b

.field public static final banner_accent_attention_high:I = 0x7f06006c

.field public static final banner_accent_attention_low:I = 0x7f06006d

.field public static final banner_accent_attention_medium:I = 0x7f06006e

.field public static final banner_background_attention_high:I = 0x7f06006f

.field public static final banner_background_attention_low:I = 0x7f060070

.field public static final banner_background_attention_medium:I = 0x7f060071

.field public static final basic_info_item_divider_color:I = 0x7f060072

.field public static final basic_info_item_summary_color:I = 0x7f060073

.field public static final basic_info_item_title_color:I = 0x7f060074

.field public static final battery_bad_color_dark:I = 0x7f060075

.field public static final battery_bad_color_light:I = 0x7f060076

.field public static final battery_first_color:I = 0x7f060077

.field public static final battery_good_color_dark:I = 0x7f060078

.field public static final battery_good_color_light:I = 0x7f060079

.field public static final battery_history_title_color:I = 0x7f06007a

.field public static final battery_icon_color_error:I = 0x7f06007b

.field public static final battery_info_error_color_black:I = 0x7f06007c

.field public static final battery_info_error_color_red:I = 0x7f06007d

.field public static final battery_maybe_color_dark:I = 0x7f06007e

.field public static final battery_maybe_color_light:I = 0x7f06007f

.field public static final battery_second_color:I = 0x7f060080

.field public static final battery_third_color:I = 0x7f060081

.field public static final batterymeter_bolt_color:I = 0x7f060082

.field public static final batterymeter_plus_color:I = 0x7f060083

.field public static final bg_btn_normal_color:I = 0x7f060084

.field public static final bg_btn_suggest_color:I = 0x7f060085

.field public static final bg_spinner_parent:I = 0x7f060086

.field public static final bg_translate_default:I = 0x7f060087

.field public static final big_add_account_title_text_color:I = 0x7f060088

.field public static final biometric_enroll_intro_color_bar:I = 0x7f060089

.field public static final biometric_enroll_intro_color_icon:I = 0x7f06008a

.field public static final biometric_enroll_intro_color_outline:I = 0x7f06008b

.field public static final black:I = 0x7f06008c

.field public static final black_translucent:I = 0x7f06008d

.field public static final blackalpha100:I = 0x7f06008e

.field public static final blackalpha40:I = 0x7f06008f

.field public static final blue:I = 0x7f060090

.field public static final bluetooth_dialog_default_text_color:I = 0x7f060091

.field public static final bluetooth_dialog_text_color:I = 0x7f060092

.field public static final bluetooth_dialog_text_color_2:I = 0x7f060093

.field public static final bootloader_apply_accept:I = 0x7f060094

.field public static final bootloader_apply_background:I = 0x7f060095

.field public static final bootloader_apply_reject:I = 0x7f060096

.field public static final bootloader_apply_title:I = 0x7f060097

.field public static final bootloader_apply_warning_info:I = 0x7f060098

.field public static final bootloader_status_default_color:I = 0x7f060099

.field public static final bootloader_status_line_color:I = 0x7f06009a

.field public static final bootloader_status_question_color:I = 0x7f06009b

.field public static final bootloader_status_url_color:I = 0x7f06009c

.field public static final bootloader_url:I = 0x7f06009d

.field public static final bootloader_warning:I = 0x7f06009e

.field public static final bottom_navigation_colors:I = 0x7f06009f

.field public static final bright_foreground_disabled_material_dark:I = 0x7f0600a0

.field public static final bright_foreground_disabled_material_light:I = 0x7f0600a1

.field public static final bright_foreground_inverse_material_dark:I = 0x7f0600a2

.field public static final bright_foreground_inverse_material_light:I = 0x7f0600a3

.field public static final bright_foreground_material_dark:I = 0x7f0600a4

.field public static final bright_foreground_material_light:I = 0x7f0600a5

.field public static final bt_color_bg_1:I = 0x7f0600a6

.field public static final bt_color_bg_2:I = 0x7f0600a7

.field public static final bt_color_bg_3:I = 0x7f0600a8

.field public static final bt_color_bg_4:I = 0x7f0600a9

.field public static final bt_color_bg_5:I = 0x7f0600aa

.field public static final bt_color_bg_6:I = 0x7f0600ab

.field public static final bt_color_bg_7:I = 0x7f0600ac

.field public static final bt_color_icon_1:I = 0x7f0600ad

.field public static final bt_color_icon_2:I = 0x7f0600ae

.field public static final bt_color_icon_3:I = 0x7f0600af

.field public static final bt_color_icon_4:I = 0x7f0600b0

.field public static final bt_color_icon_5:I = 0x7f0600b1

.field public static final bt_color_icon_6:I = 0x7f0600b2

.field public static final bt_color_icon_7:I = 0x7f0600b3

.field public static final bt_outline_color:I = 0x7f0600b4

.field public static final bt_refresh_bg_color:I = 0x7f0600b5

.field public static final bt_refresh_color:I = 0x7f0600b6

.field public static final btn_bg:I = 0x7f0600b7

.field public static final btn_bg_normal_pressed_color:I = 0x7f0600b8

.field public static final btn_bg_suggest_pressed_color:I = 0x7f0600b9

.field public static final btn_icon_night:I = 0x7f0600ba

.field public static final bubble_left_bg_color:I = 0x7f0600bb

.field public static final bubble_right_bg_color:I = 0x7f0600bc

.field public static final button_background_color:I = 0x7f0600bd

.field public static final button_bg_color_warn_disabled_dark:I = 0x7f0600be

.field public static final button_bg_color_warn_normal_dark:I = 0x7f0600bf

.field public static final button_bg_color_warn_pressed_dark:I = 0x7f0600c0

.field public static final button_height_light:I = 0x7f0600c1

.field public static final button_material_dark:I = 0x7f0600c2

.field public static final button_material_light:I = 0x7f0600c3

.field public static final button_normal:I = 0x7f0600c4

.field public static final button_text_color_dialog_disabled_light:I = 0x7f0600c5

.field public static final button_text_color_dialog_light:I = 0x7f0600c6

.field public static final button_text_dark:I = 0x7f0600c7

.field public static final button_text_disable_color:I = 0x7f0600c8

.field public static final button_text_high_light_color:I = 0x7f0600c9

.field public static final button_text_high_light_color_pressed:I = 0x7f0600ca

.field public static final button_text_low_light_color:I = 0x7f0600cb

.field public static final button_text_low_light_color_pressed:I = 0x7f0600cc

.field public static final button_text_normal_color:I = 0x7f0600cd

.field public static final button_text_normal_color_pressed:I = 0x7f0600ce

.field public static final card_background_stroke_color:I = 0x7f0600cf

.field public static final card_style_summary_normal_color:I = 0x7f0600d0

.field public static final card_style_summary_selected_color:I = 0x7f0600d1

.field public static final card_style_title_normal_color:I = 0x7f0600d2

.field public static final card_style_title_selected_color:I = 0x7f0600d3

.field public static final card_view_background_color:I = 0x7f0600d4

.field public static final card_view_checked_color:I = 0x7f0600d5

.field public static final card_view_title_checked_color:I = 0x7f0600d6

.field public static final card_view_title_color:I = 0x7f0600d7

.field public static final card_view_value_checked_color:I = 0x7f0600d8

.field public static final card_view_value_color:I = 0x7f0600d9

.field public static final cardview_dark_background:I = 0x7f0600da

.field public static final cardview_light_background:I = 0x7f0600db

.field public static final cardview_shadow_end_color:I = 0x7f0600dc

.field public static final cardview_shadow_start_color:I = 0x7f0600dd

.field public static final category_text_color:I = 0x7f0600de

.field public static final checkbox_bg:I = 0x7f0600df

.field public static final checkbox_themeable_attribute_color:I = 0x7f0600e0

.field public static final choose_lock_button_enable_text_color:I = 0x7f0600e1

.field public static final choose_lock_button_text_color:I = 0x7f0600e2

.field public static final choose_password_btn_normal_color:I = 0x7f0600e3

.field public static final choose_password_btn_pressed_color:I = 0x7f0600e4

.field public static final choose_password_btn_stroke_color:I = 0x7f0600e5

.field public static final choose_password_forget_text_color_light:I = 0x7f0600e6

.field public static final choose_unlock_item_bg_normal:I = 0x7f0600e7

.field public static final choose_unlock_item_bg_pressed:I = 0x7f0600e8

.field public static final choose_unlock_item_bg_selected:I = 0x7f0600e9

.field public static final choose_unlock_item_bg_selected_pressed:I = 0x7f0600ea

.field public static final circle_outline_color:I = 0x7f0600eb

.field public static final clear_all_data_text_color:I = 0x7f0600ec

.field public static final clear_all_data_text_disable_color:I = 0x7f0600ed

.field public static final clear_all_data_text_pressed_color:I = 0x7f0600ee

.field public static final color_button_text_disable:I = 0x7f0600ef

.field public static final color_button_text_high_light:I = 0x7f0600f0

.field public static final color_button_text_high_light_pressed:I = 0x7f0600f1

.field public static final color_button_text_normal:I = 0x7f0600f2

.field public static final color_button_text_normal_pressed:I = 0x7f0600f3

.field public static final color_checkable_btn:I = 0x7f0600f4

.field public static final color_explain_text:I = 0x7f0600f5

.field public static final color_not_selected:I = 0x7f0600f6

.field public static final color_radio_button_text:I = 0x7f0600f7

.field public static final color_sub_title:I = 0x7f0600f8

.field public static final color_surface_header:I = 0x7f0600f9

.field public static final color_title:I = 0x7f0600fa

.field public static final column_split_divider_color:I = 0x7f0600fb

.field public static final config_defaultNotificationColor_London:I = 0x7f0600fc

.field public static final confirm_device_credential_transparent_black:I = 0x7f0600fd

.field public static final confirm_password_forget_edittext_bg:I = 0x7f0600fe

.field public static final confirm_password_header_message_color:I = 0x7f0600ff

.field public static final confirm_password_header_text_color:I = 0x7f060100

.field public static final confirm_password_use_fingerprint_btn_bg_solid:I = 0x7f060101

.field public static final confirm_password_use_fingerprint_btn_bg_stroke:I = 0x7f060102

.field public static final contextual_card_background:I = 0x7f060103

.field public static final contextual_card_dismissal_background:I = 0x7f060104

.field public static final control_center_style_delete_hint_guide:I = 0x7f060105

.field public static final credentials_bg:I = 0x7f060106

.field public static final crypt_button_enabled_text_color:I = 0x7f060107

.field public static final crypt_keeper_password_background:I = 0x7f060108

.field public static final custom_checkbox_selected_text_color:I = 0x7f060109

.field public static final custom_checkbox_text_color:I = 0x7f06010a

.field public static final dark_color_mode:I = 0x7f06010b

.field public static final dark_color_mode_inner:I = 0x7f06010c

.field public static final dark_color_mode_outer:I = 0x7f06010d

.field public static final dark_light_text_color:I = 0x7f06010e

.field public static final dark_mode_apps_header:I = 0x7f06010f

.field public static final dark_mode_icon_color_dual_tone_background:I = 0x7f060110

.field public static final dark_mode_icon_color_dual_tone_fill:I = 0x7f060111

.field public static final dark_mode_icon_color_single_tone:I = 0x7f060112

.field public static final dark_mode_notify_title:I = 0x7f060113

.field public static final day_night_white:I = 0x7f060114

.field public static final day_white_night_black:I = 0x7f060115

.field public static final default_text_color:I = 0x7f060116

.field public static final deferred_setup_hint_background:I = 0x7f060117

.field public static final deferred_setup_text_color:I = 0x7f060118

.field public static final demo_bg:I = 0x7f060119

.field public static final design_bottom_navigation_shadow_color:I = 0x7f06011a

.field public static final design_box_stroke_color:I = 0x7f06011b

.field public static final design_dark_default_color_background:I = 0x7f06011c

.field public static final design_dark_default_color_error:I = 0x7f06011d

.field public static final design_dark_default_color_on_background:I = 0x7f06011e

.field public static final design_dark_default_color_on_error:I = 0x7f06011f

.field public static final design_dark_default_color_on_primary:I = 0x7f060120

.field public static final design_dark_default_color_on_secondary:I = 0x7f060121

.field public static final design_dark_default_color_on_surface:I = 0x7f060122

.field public static final design_dark_default_color_primary:I = 0x7f060123

.field public static final design_dark_default_color_primary_dark:I = 0x7f060124

.field public static final design_dark_default_color_primary_variant:I = 0x7f060125

.field public static final design_dark_default_color_secondary:I = 0x7f060126

.field public static final design_dark_default_color_secondary_variant:I = 0x7f060127

.field public static final design_dark_default_color_surface:I = 0x7f060128

.field public static final design_default_color_background:I = 0x7f060129

.field public static final design_default_color_error:I = 0x7f06012a

.field public static final design_default_color_on_background:I = 0x7f06012b

.field public static final design_default_color_on_error:I = 0x7f06012c

.field public static final design_default_color_on_primary:I = 0x7f06012d

.field public static final design_default_color_on_secondary:I = 0x7f06012e

.field public static final design_default_color_on_surface:I = 0x7f06012f

.field public static final design_default_color_primary:I = 0x7f060130

.field public static final design_default_color_primary_dark:I = 0x7f060131

.field public static final design_default_color_primary_variant:I = 0x7f060132

.field public static final design_default_color_secondary:I = 0x7f060133

.field public static final design_default_color_secondary_variant:I = 0x7f060134

.field public static final design_default_color_surface:I = 0x7f060135

.field public static final design_error:I = 0x7f060136

.field public static final design_fab_shadow_end_color:I = 0x7f060137

.field public static final design_fab_shadow_mid_color:I = 0x7f060138

.field public static final design_fab_shadow_start_color:I = 0x7f060139

.field public static final design_fab_stroke_end_inner_color:I = 0x7f06013a

.field public static final design_fab_stroke_end_outer_color:I = 0x7f06013b

.field public static final design_fab_stroke_top_inner_color:I = 0x7f06013c

.field public static final design_fab_stroke_top_outer_color:I = 0x7f06013d

.field public static final design_icon_tint:I = 0x7f06013e

.field public static final design_snackbar_background_color:I = 0x7f06013f

.field public static final detal_description_color:I = 0x7f060140

.field public static final device_card_background:I = 0x7f060141

.field public static final device_icon_fill_color:I = 0x7f060142

.field public static final device_icon_fill_color_new:I = 0x7f060143

.field public static final dialog_item_color:I = 0x7f060144

.field public static final dialog_message:I = 0x7f060145

.field public static final dialog_message_text_color_light:I = 0x7f060146

.field public static final dim_foreground_disabled_material_dark:I = 0x7f060147

.field public static final dim_foreground_disabled_material_light:I = 0x7f060148

.field public static final dim_foreground_material_dark:I = 0x7f060149

.field public static final dim_foreground_material_light:I = 0x7f06014a

.field public static final disabled_text_color:I = 0x7f06014b

.field public static final divider_bg:I = 0x7f06014c

.field public static final divider_color:I = 0x7f06014d

.field public static final divider_fill_color:I = 0x7f06014e

.field public static final divider_line_color:I = 0x7f06014f

.field public static final divider_line_dark:I = 0x7f060150

.field public static final dream_card_color_state_list:I = 0x7f060151

.field public static final dream_card_icon_color_state_list:I = 0x7f060152

.field public static final dream_card_text_color_state_list:I = 0x7f060153

.field public static final dropdown_popup_backgroud_color:I = 0x7f060154

.field public static final edge_mode_guide_background_color:I = 0x7f060155

.field public static final eighty_percent_black_color:I = 0x7f060156

.field public static final emergency_contact_item_bg:I = 0x7f060157

.field public static final emergency_contact_name_color:I = 0x7f060158

.field public static final emergency_contact_number_color:I = 0x7f060159

.field public static final enable_local_backup_bg:I = 0x7f06015a

.field public static final enable_local_backup_text:I = 0x7f06015b

.field public static final equalizer_curve:I = 0x7f06015c

.field public static final equalizer_curve_shadow:I = 0x7f06015d

.field public static final error_color_material_dark:I = 0x7f06015e

.field public static final error_color_material_light:I = 0x7f06015f

.field public static final external_ram_icon_fill_color:I = 0x7f060160

.field public static final face_anim_particle_color_1:I = 0x7f060161

.field public static final face_anim_particle_color_2:I = 0x7f060162

.field public static final face_anim_particle_color_3:I = 0x7f060163

.field public static final face_anim_particle_color_4:I = 0x7f060164

.field public static final face_anim_particle_error:I = 0x7f060165

.field public static final face_detail_name_text_color:I = 0x7f060166

.field public static final face_enroll_icon_color:I = 0x7f060167

.field public static final fallback_color_device_default_dark:I = 0x7f060168

.field public static final fallback_color_device_default_light:I = 0x7f060169

.field public static final fallback_tintColor:I = 0x7f06016a

.field public static final finger_detail_info_text_color:I = 0x7f06016b

.field public static final fingerprint_enrollment_finish_color_outline:I = 0x7f06016c

.field public static final fingerprint_indicator_background_activated:I = 0x7f06016d

.field public static final fingerprint_message_color:I = 0x7f06016e

.field public static final fingerprint_title_area_bg:I = 0x7f06016f

.field public static final fingerprint_title_color:I = 0x7f060170

.field public static final fingerprint_top_text_color:I = 0x7f060171

.field public static final fingerprint_top_title_color:I = 0x7f060172

.field public static final first_text_color:I = 0x7f060173

.field public static final fod_authentication_dialog_title:I = 0x7f060174

.field public static final fod_dialog_window_background:I = 0x7f060175

.field public static final fold_description_text_color:I = 0x7f060176

.field public static final fold_diagram_bg_color:I = 0x7f060177

.field public static final fold_feedback_link_color:I = 0x7f060178

.field public static final fold_support_text_color:I = 0x7f060179

.field public static final font_bubble_left_font_color:I = 0x7f06017a

.field public static final font_color:I = 0x7f06017b

.field public static final font_settings_bg_color:I = 0x7f06017c

.field public static final font_settings_icon_text_color:I = 0x7f06017d

.field public static final font_settings_view_big_color:I = 0x7f06017e

.field public static final font_settings_visual_box_border_color:I = 0x7f06017f

.field public static final font_size_seekbar_big_pointer_blue:I = 0x7f060180

.field public static final font_size_settings_color:I = 0x7f060181

.field public static final font_size_settings_seekbar_color:I = 0x7f060182

.field public static final font_size_settings_seekbar_tick_color:I = 0x7f060183

.field public static final font_size_title_background:I = 0x7f060184

.field public static final font_size_view_background:I = 0x7f060185

.field public static final font_size_view_big_center_color:I = 0x7f060186

.field public static final font_size_view_big_color:I = 0x7f060187

.field public static final font_size_view_small_color:I = 0x7f060188

.field public static final font_view_call_bg_color:I = 0x7f060189

.field public static final font_weight_view_small_color:I = 0x7f06018a

.field public static final foreground_material_dark:I = 0x7f06018b

.field public static final foreground_material_light:I = 0x7f06018c

.field public static final forget_password_dialog_background:I = 0x7f06018d

.field public static final forget_password_dialog_content_text_color:I = 0x7f06018e

.field public static final forget_password_dialog_title_text_color:I = 0x7f06018f

.field public static final forget_password_text_color:I = 0x7f060190

.field public static final free_wifi_login_bg_color:I = 0x7f060191

.field public static final free_wifi_login_fail_bg_color:I = 0x7f060192

.field public static final frp_skip_password_text_color:I = 0x7f060193

.field public static final gary_bg:I = 0x7f060194

.field public static final gestures_setting_background_color:I = 0x7f060195

.field public static final gps_support_color:I = 0x7f060196

.field public static final gray:I = 0x7f060197

.field public static final guide_color:I = 0x7f060198

.field public static final guide_text_color:I = 0x7f060199

.field public static final gxzw_anim_background:I = 0x7f06019a

.field public static final gxzw_anim_text_normal:I = 0x7f06019b

.field public static final gxzw_anim_text_select:I = 0x7f06019c

.field public static final gxzw_quick_open_summary:I = 0x7f06019d

.field public static final gxzw_quick_open_title_select:I = 0x7f06019e

.field public static final gxzw_quick_open_title_unselect:I = 0x7f06019f

.field public static final half_transparent_75:I = 0x7f0601a0

.field public static final handy_mode_guide_scale_screen_color:I = 0x7f0601a1

.field public static final handy_mode_guide_touch_circle_color:I = 0x7f0601a2

.field public static final haptic_crisps:I = 0x7f0601a3

.field public static final haptic_crisps_bottom:I = 0x7f0601a4

.field public static final haptic_demo_background_background:I = 0x7f0601a5

.field public static final haptic_interesting:I = 0x7f0601a6

.field public static final haptic_interesting_bottom:I = 0x7f0601a7

.field public static final haptic_low:I = 0x7f0601a8

.field public static final haptic_low_bottom:I = 0x7f0601a9

.field public static final haptic_radio_preference_bg_selected_color:I = 0x7f0601aa

.field public static final haptic_radio_preference_bg_unselected_color:I = 0x7f0601ab

.field public static final haptic_radio_preference_summary_selected_color:I = 0x7f0601ac

.field public static final haptic_radio_preference_summary_unselected_color:I = 0x7f0601ad

.field public static final haptic_radio_preference_title_selected_color:I = 0x7f0601ae

.field public static final haptic_radio_preference_title_unselected_color:I = 0x7f0601af

.field public static final haptic_soft:I = 0x7f0601b0

.field public static final haptic_soft_bottom:I = 0x7f0601b1

.field public static final headset_battery_back_grey:I = 0x7f0601b2

.field public static final headset_find_device_10:I = 0x7f0601b3

.field public static final headset_find_device_100:I = 0x7f0601b4

.field public static final headset_find_device_30:I = 0x7f0601b5

.field public static final headset_find_device_60:I = 0x7f0601b6

.field public static final headset_find_device_80:I = 0x7f0601b7

.field public static final headset_find_device_blue:I = 0x7f0601b8

.field public static final headset_find_device_gray:I = 0x7f0601b9

.field public static final headset_find_device_white:I = 0x7f0601ba

.field public static final headset_fitness_result_not_ok:I = 0x7f0601bb

.field public static final headset_fitness_result_ok:I = 0x7f0601bc

.field public static final height_light_list_bg:I = 0x7f0601bd

.field public static final highlight_list_bg:I = 0x7f0601be

.field public static final highlighted_text_material_dark:I = 0x7f0601bf

.field public static final highlighted_text_material_light:I = 0x7f0601c0

.field public static final homepage_about_background:I = 0x7f0601c1

.field public static final homepage_accessibility_background:I = 0x7f0601c2

.field public static final homepage_accounts_background:I = 0x7f0601c3

.field public static final homepage_app_and_notification_background:I = 0x7f0601c4

.field public static final homepage_battery_background:I = 0x7f0601c5

.field public static final homepage_card_dismissal_background:I = 0x7f0601c6

.field public static final homepage_connected_device_background:I = 0x7f0601c7

.field public static final homepage_display_background:I = 0x7f0601c8

.field public static final homepage_emergency_background:I = 0x7f0601c9

.field public static final homepage_generic_icon_background:I = 0x7f0601ca

.field public static final homepage_location_background:I = 0x7f0601cb

.field public static final homepage_network_background:I = 0x7f0601cc

.field public static final homepage_notification_background:I = 0x7f0601cd

.field public static final homepage_privacy_background:I = 0x7f0601ce

.field public static final homepage_security_background:I = 0x7f0601cf

.field public static final homepage_sound_background:I = 0x7f0601d0

.field public static final homepage_storage_background:I = 0x7f0601d1

.field public static final homepage_support_background:I = 0x7f0601d2

.field public static final homepage_system_background:I = 0x7f0601d3

.field public static final homepage_wallpaper_background:I = 0x7f0601d4

.field public static final icon_accent:I = 0x7f0601d5

.field public static final icon_launcher_setting_color:I = 0x7f0601d6

.field public static final important_conversation:I = 0x7f0601d7

.field public static final input_method_cloud_paste_tips_text_color:I = 0x7f0601d8

.field public static final input_method_function_des_unselected:I = 0x7f0601d9

.field public static final input_method_function_icon_color_selected:I = 0x7f0601da

.field public static final input_method_function_icon_color_unselected:I = 0x7f0601db

.field public static final input_method_function_select_background:I = 0x7f0601dc

.field public static final input_method_function_selected:I = 0x7f0601dd

.field public static final input_method_item_selected:I = 0x7f0601de

.field public static final input_method_keyboard_bg_color:I = 0x7f0601df

.field public static final input_method_keyboard_des_text_color_selected:I = 0x7f0601e0

.field public static final input_method_keyboard_des_text_color_unselected:I = 0x7f0601e1

.field public static final input_method_manage_item_enable_normal_bg_color:I = 0x7f0601e2

.field public static final input_method_manage_item_not_enable_normal_bg_color:I = 0x7f0601e3

.field public static final input_method_manage_line_color:I = 0x7f0601e4

.field public static final input_method_manage_summary_color:I = 0x7f0601e5

.field public static final input_method_manage_text_enable_color:I = 0x7f0601e6

.field public static final input_method_manage_text_un_enable_color:I = 0x7f0601e7

.field public static final input_method_manage_title_color:I = 0x7f0601e8

.field public static final input_view_bg_color:I = 0x7f0601e9

.field public static final input_view_cursor_color:I = 0x7f0601ea

.field public static final instructions_text_color:I = 0x7f0601eb

.field public static final instructions_text_color_append:I = 0x7f0601ec

.field public static final intent_highlight_text:I = 0x7f0601ed

.field public static final item_setting_pressed:I = 0x7f0601ee

.field public static final keyboard_layout_dialog_list_item_background:I = 0x7f0601ef

.field public static final keyboard_shortcut_background:I = 0x7f0601f0

.field public static final keyboard_shortcut_color_action_text:I = 0x7f0601f1

.field public static final keyboard_shortcut_color_edit_background:I = 0x7f0601f2

.field public static final keyboard_shortcut_color_text:I = 0x7f0601f3

.field public static final launch_dialog_redownload:I = 0x7f0601f4

.field public static final legacy_icon_background:I = 0x7f0601f5

.field public static final light_color_mode:I = 0x7f0601f6

.field public static final light_mode_icon_color_dual_tone_background:I = 0x7f0601f7

.field public static final light_mode_icon_color_dual_tone_fill:I = 0x7f0601f8

.field public static final light_mode_icon_color_single_tone:I = 0x7f0601f9

.field public static final line_color_gray:I = 0x7f0601fa

.field public static final list_card_background:I = 0x7f0601fb

.field public static final list_divider_color:I = 0x7f0601fc

.field public static final list_item_bg_color:I = 0x7f0601fd

.field public static final list_item_bg_color_pressed:I = 0x7f0601fe

.field public static final list_item_normal:I = 0x7f0601ff

.field public static final list_item_pressed:I = 0x7f060200

.field public static final list_item_text_color:I = 0x7f060201

.field public static final list_secondary_text_color_normal_light:I = 0x7f060202

.field public static final lock_pattern_background:I = 0x7f060203

.field public static final lock_pattern_view_success_color:I = 0x7f060204

.field public static final login_account_text_color:I = 0x7f060205

.field public static final m3_appbar_overlay_color:I = 0x7f060206

.field public static final m3_assist_chip_icon_tint_color:I = 0x7f060207

.field public static final m3_assist_chip_stroke_color:I = 0x7f060208

.field public static final m3_button_background_color_selector:I = 0x7f060209

.field public static final m3_button_foreground_color_selector:I = 0x7f06020a

.field public static final m3_button_outline_color_selector:I = 0x7f06020b

.field public static final m3_button_ripple_color:I = 0x7f06020c

.field public static final m3_button_ripple_color_selector:I = 0x7f06020d

.field public static final m3_calendar_item_disabled_text:I = 0x7f06020e

.field public static final m3_calendar_item_stroke_color:I = 0x7f06020f

.field public static final m3_card_foreground_color:I = 0x7f060210

.field public static final m3_card_ripple_color:I = 0x7f060211

.field public static final m3_card_stroke_color:I = 0x7f060212

.field public static final m3_chip_assist_text_color:I = 0x7f060213

.field public static final m3_chip_background_color:I = 0x7f060214

.field public static final m3_chip_ripple_color:I = 0x7f060215

.field public static final m3_chip_stroke_color:I = 0x7f060216

.field public static final m3_chip_text_color:I = 0x7f060217

.field public static final m3_dark_default_color_primary_text:I = 0x7f060218

.field public static final m3_dark_default_color_secondary_text:I = 0x7f060219

.field public static final m3_dark_highlighted_text:I = 0x7f06021a

.field public static final m3_dark_hint_foreground:I = 0x7f06021b

.field public static final m3_dark_primary_text_disable_only:I = 0x7f06021c

.field public static final m3_default_color_primary_text:I = 0x7f06021d

.field public static final m3_default_color_secondary_text:I = 0x7f06021e

.field public static final m3_dynamic_dark_default_color_primary_text:I = 0x7f06021f

.field public static final m3_dynamic_dark_default_color_secondary_text:I = 0x7f060220

.field public static final m3_dynamic_dark_highlighted_text:I = 0x7f060221

.field public static final m3_dynamic_dark_hint_foreground:I = 0x7f060222

.field public static final m3_dynamic_dark_primary_text_disable_only:I = 0x7f060223

.field public static final m3_dynamic_default_color_primary_text:I = 0x7f060224

.field public static final m3_dynamic_default_color_secondary_text:I = 0x7f060225

.field public static final m3_dynamic_highlighted_text:I = 0x7f060226

.field public static final m3_dynamic_hint_foreground:I = 0x7f060227

.field public static final m3_dynamic_primary_text_disable_only:I = 0x7f060228

.field public static final m3_elevated_chip_background_color:I = 0x7f060229

.field public static final m3_highlighted_text:I = 0x7f06022a

.field public static final m3_hint_foreground:I = 0x7f06022b

.field public static final m3_navigation_bar_item_with_indicator_icon_tint:I = 0x7f06022c

.field public static final m3_navigation_bar_item_with_indicator_label_tint:I = 0x7f06022d

.field public static final m3_navigation_bar_ripple_color_selector:I = 0x7f06022e

.field public static final m3_navigation_item_background_color:I = 0x7f06022f

.field public static final m3_navigation_item_icon_tint:I = 0x7f060230

.field public static final m3_navigation_item_text_color:I = 0x7f060231

.field public static final m3_popupmenu_overlay_color:I = 0x7f060232

.field public static final m3_primary_text_disable_only:I = 0x7f060233

.field public static final m3_radiobutton_ripple_tint:I = 0x7f060234

.field public static final m3_ref_palette_dynamic_neutral0:I = 0x7f060235

.field public static final m3_ref_palette_dynamic_neutral10:I = 0x7f060236

.field public static final m3_ref_palette_dynamic_neutral100:I = 0x7f060237

.field public static final m3_ref_palette_dynamic_neutral20:I = 0x7f060238

.field public static final m3_ref_palette_dynamic_neutral30:I = 0x7f060239

.field public static final m3_ref_palette_dynamic_neutral40:I = 0x7f06023a

.field public static final m3_ref_palette_dynamic_neutral50:I = 0x7f06023b

.field public static final m3_ref_palette_dynamic_neutral60:I = 0x7f06023c

.field public static final m3_ref_palette_dynamic_neutral70:I = 0x7f06023d

.field public static final m3_ref_palette_dynamic_neutral80:I = 0x7f06023e

.field public static final m3_ref_palette_dynamic_neutral90:I = 0x7f06023f

.field public static final m3_ref_palette_dynamic_neutral95:I = 0x7f060240

.field public static final m3_ref_palette_dynamic_neutral99:I = 0x7f060241

.field public static final m3_ref_palette_dynamic_neutral_variant0:I = 0x7f060242

.field public static final m3_ref_palette_dynamic_neutral_variant10:I = 0x7f060243

.field public static final m3_ref_palette_dynamic_neutral_variant100:I = 0x7f060244

.field public static final m3_ref_palette_dynamic_neutral_variant20:I = 0x7f060245

.field public static final m3_ref_palette_dynamic_neutral_variant30:I = 0x7f060246

.field public static final m3_ref_palette_dynamic_neutral_variant40:I = 0x7f060247

.field public static final m3_ref_palette_dynamic_neutral_variant50:I = 0x7f060248

.field public static final m3_ref_palette_dynamic_neutral_variant60:I = 0x7f060249

.field public static final m3_ref_palette_dynamic_neutral_variant70:I = 0x7f06024a

.field public static final m3_ref_palette_dynamic_neutral_variant80:I = 0x7f06024b

.field public static final m3_ref_palette_dynamic_neutral_variant90:I = 0x7f06024c

.field public static final m3_ref_palette_dynamic_neutral_variant95:I = 0x7f06024d

.field public static final m3_ref_palette_dynamic_neutral_variant99:I = 0x7f06024e

.field public static final m3_ref_palette_dynamic_primary0:I = 0x7f06024f

.field public static final m3_ref_palette_dynamic_primary10:I = 0x7f060250

.field public static final m3_ref_palette_dynamic_primary100:I = 0x7f060251

.field public static final m3_ref_palette_dynamic_primary20:I = 0x7f060252

.field public static final m3_ref_palette_dynamic_primary30:I = 0x7f060253

.field public static final m3_ref_palette_dynamic_primary40:I = 0x7f060254

.field public static final m3_ref_palette_dynamic_primary50:I = 0x7f060255

.field public static final m3_ref_palette_dynamic_primary60:I = 0x7f060256

.field public static final m3_ref_palette_dynamic_primary70:I = 0x7f060257

.field public static final m3_ref_palette_dynamic_primary80:I = 0x7f060258

.field public static final m3_ref_palette_dynamic_primary90:I = 0x7f060259

.field public static final m3_ref_palette_dynamic_primary95:I = 0x7f06025a

.field public static final m3_ref_palette_dynamic_primary99:I = 0x7f06025b

.field public static final m3_ref_palette_dynamic_secondary0:I = 0x7f06025c

.field public static final m3_ref_palette_dynamic_secondary10:I = 0x7f06025d

.field public static final m3_ref_palette_dynamic_secondary100:I = 0x7f06025e

.field public static final m3_ref_palette_dynamic_secondary20:I = 0x7f06025f

.field public static final m3_ref_palette_dynamic_secondary30:I = 0x7f060260

.field public static final m3_ref_palette_dynamic_secondary40:I = 0x7f060261

.field public static final m3_ref_palette_dynamic_secondary50:I = 0x7f060262

.field public static final m3_ref_palette_dynamic_secondary60:I = 0x7f060263

.field public static final m3_ref_palette_dynamic_secondary70:I = 0x7f060264

.field public static final m3_ref_palette_dynamic_secondary80:I = 0x7f060265

.field public static final m3_ref_palette_dynamic_secondary90:I = 0x7f060266

.field public static final m3_ref_palette_dynamic_secondary95:I = 0x7f060267

.field public static final m3_ref_palette_dynamic_secondary99:I = 0x7f060268

.field public static final m3_ref_palette_dynamic_tertiary0:I = 0x7f060269

.field public static final m3_ref_palette_dynamic_tertiary10:I = 0x7f06026a

.field public static final m3_ref_palette_dynamic_tertiary100:I = 0x7f06026b

.field public static final m3_ref_palette_dynamic_tertiary20:I = 0x7f06026c

.field public static final m3_ref_palette_dynamic_tertiary30:I = 0x7f06026d

.field public static final m3_ref_palette_dynamic_tertiary40:I = 0x7f06026e

.field public static final m3_ref_palette_dynamic_tertiary50:I = 0x7f06026f

.field public static final m3_ref_palette_dynamic_tertiary60:I = 0x7f060270

.field public static final m3_ref_palette_dynamic_tertiary70:I = 0x7f060271

.field public static final m3_ref_palette_dynamic_tertiary80:I = 0x7f060272

.field public static final m3_ref_palette_dynamic_tertiary90:I = 0x7f060273

.field public static final m3_ref_palette_dynamic_tertiary95:I = 0x7f060274

.field public static final m3_ref_palette_dynamic_tertiary99:I = 0x7f060275

.field public static final m3_ref_palette_error0:I = 0x7f060276

.field public static final m3_ref_palette_error10:I = 0x7f060277

.field public static final m3_ref_palette_error100:I = 0x7f060278

.field public static final m3_ref_palette_error20:I = 0x7f060279

.field public static final m3_ref_palette_error30:I = 0x7f06027a

.field public static final m3_ref_palette_error40:I = 0x7f06027b

.field public static final m3_ref_palette_error50:I = 0x7f06027c

.field public static final m3_ref_palette_error60:I = 0x7f06027d

.field public static final m3_ref_palette_error70:I = 0x7f06027e

.field public static final m3_ref_palette_error80:I = 0x7f06027f

.field public static final m3_ref_palette_error90:I = 0x7f060280

.field public static final m3_ref_palette_error95:I = 0x7f060281

.field public static final m3_ref_palette_error99:I = 0x7f060282

.field public static final m3_ref_palette_neutral0:I = 0x7f060283

.field public static final m3_ref_palette_neutral10:I = 0x7f060284

.field public static final m3_ref_palette_neutral100:I = 0x7f060285

.field public static final m3_ref_palette_neutral20:I = 0x7f060286

.field public static final m3_ref_palette_neutral30:I = 0x7f060287

.field public static final m3_ref_palette_neutral40:I = 0x7f060288

.field public static final m3_ref_palette_neutral50:I = 0x7f060289

.field public static final m3_ref_palette_neutral60:I = 0x7f06028a

.field public static final m3_ref_palette_neutral70:I = 0x7f06028b

.field public static final m3_ref_palette_neutral80:I = 0x7f06028c

.field public static final m3_ref_palette_neutral90:I = 0x7f06028d

.field public static final m3_ref_palette_neutral95:I = 0x7f06028e

.field public static final m3_ref_palette_neutral99:I = 0x7f06028f

.field public static final m3_ref_palette_neutral_variant0:I = 0x7f060290

.field public static final m3_ref_palette_neutral_variant10:I = 0x7f060291

.field public static final m3_ref_palette_neutral_variant100:I = 0x7f060292

.field public static final m3_ref_palette_neutral_variant20:I = 0x7f060293

.field public static final m3_ref_palette_neutral_variant30:I = 0x7f060294

.field public static final m3_ref_palette_neutral_variant40:I = 0x7f060295

.field public static final m3_ref_palette_neutral_variant50:I = 0x7f060296

.field public static final m3_ref_palette_neutral_variant60:I = 0x7f060297

.field public static final m3_ref_palette_neutral_variant70:I = 0x7f060298

.field public static final m3_ref_palette_neutral_variant80:I = 0x7f060299

.field public static final m3_ref_palette_neutral_variant90:I = 0x7f06029a

.field public static final m3_ref_palette_neutral_variant95:I = 0x7f06029b

.field public static final m3_ref_palette_neutral_variant99:I = 0x7f06029c

.field public static final m3_ref_palette_primary0:I = 0x7f06029d

.field public static final m3_ref_palette_primary10:I = 0x7f06029e

.field public static final m3_ref_palette_primary100:I = 0x7f06029f

.field public static final m3_ref_palette_primary20:I = 0x7f0602a0

.field public static final m3_ref_palette_primary30:I = 0x7f0602a1

.field public static final m3_ref_palette_primary40:I = 0x7f0602a2

.field public static final m3_ref_palette_primary50:I = 0x7f0602a3

.field public static final m3_ref_palette_primary60:I = 0x7f0602a4

.field public static final m3_ref_palette_primary70:I = 0x7f0602a5

.field public static final m3_ref_palette_primary80:I = 0x7f0602a6

.field public static final m3_ref_palette_primary90:I = 0x7f0602a7

.field public static final m3_ref_palette_primary95:I = 0x7f0602a8

.field public static final m3_ref_palette_primary99:I = 0x7f0602a9

.field public static final m3_ref_palette_secondary0:I = 0x7f0602aa

.field public static final m3_ref_palette_secondary10:I = 0x7f0602ab

.field public static final m3_ref_palette_secondary100:I = 0x7f0602ac

.field public static final m3_ref_palette_secondary20:I = 0x7f0602ad

.field public static final m3_ref_palette_secondary30:I = 0x7f0602ae

.field public static final m3_ref_palette_secondary40:I = 0x7f0602af

.field public static final m3_ref_palette_secondary50:I = 0x7f0602b0

.field public static final m3_ref_palette_secondary60:I = 0x7f0602b1

.field public static final m3_ref_palette_secondary70:I = 0x7f0602b2

.field public static final m3_ref_palette_secondary80:I = 0x7f0602b3

.field public static final m3_ref_palette_secondary90:I = 0x7f0602b4

.field public static final m3_ref_palette_secondary95:I = 0x7f0602b5

.field public static final m3_ref_palette_secondary99:I = 0x7f0602b6

.field public static final m3_ref_palette_tertiary0:I = 0x7f0602b7

.field public static final m3_ref_palette_tertiary10:I = 0x7f0602b8

.field public static final m3_ref_palette_tertiary100:I = 0x7f0602b9

.field public static final m3_ref_palette_tertiary20:I = 0x7f0602ba

.field public static final m3_ref_palette_tertiary30:I = 0x7f0602bb

.field public static final m3_ref_palette_tertiary40:I = 0x7f0602bc

.field public static final m3_ref_palette_tertiary50:I = 0x7f0602bd

.field public static final m3_ref_palette_tertiary60:I = 0x7f0602be

.field public static final m3_ref_palette_tertiary70:I = 0x7f0602bf

.field public static final m3_ref_palette_tertiary80:I = 0x7f0602c0

.field public static final m3_ref_palette_tertiary90:I = 0x7f0602c1

.field public static final m3_ref_palette_tertiary95:I = 0x7f0602c2

.field public static final m3_ref_palette_tertiary99:I = 0x7f0602c3

.field public static final m3_selection_control_button_tint:I = 0x7f0602c4

.field public static final m3_selection_control_ripple_color_selector:I = 0x7f0602c5

.field public static final m3_slider_active_track_color:I = 0x7f0602c6

.field public static final m3_slider_halo_color:I = 0x7f0602c7

.field public static final m3_slider_inactive_track_color:I = 0x7f0602c8

.field public static final m3_slider_thumb_color:I = 0x7f0602c9

.field public static final m3_switch_thumb_tint:I = 0x7f0602ca

.field public static final m3_switch_track_tint:I = 0x7f0602cb

.field public static final m3_sys_color_dark_background:I = 0x7f0602cc

.field public static final m3_sys_color_dark_error:I = 0x7f0602cd

.field public static final m3_sys_color_dark_error_container:I = 0x7f0602ce

.field public static final m3_sys_color_dark_inverse_on_surface:I = 0x7f0602cf

.field public static final m3_sys_color_dark_inverse_primary:I = 0x7f0602d0

.field public static final m3_sys_color_dark_inverse_surface:I = 0x7f0602d1

.field public static final m3_sys_color_dark_on_background:I = 0x7f0602d2

.field public static final m3_sys_color_dark_on_error:I = 0x7f0602d3

.field public static final m3_sys_color_dark_on_error_container:I = 0x7f0602d4

.field public static final m3_sys_color_dark_on_primary:I = 0x7f0602d5

.field public static final m3_sys_color_dark_on_primary_container:I = 0x7f0602d6

.field public static final m3_sys_color_dark_on_secondary:I = 0x7f0602d7

.field public static final m3_sys_color_dark_on_secondary_container:I = 0x7f0602d8

.field public static final m3_sys_color_dark_on_surface:I = 0x7f0602d9

.field public static final m3_sys_color_dark_on_surface_variant:I = 0x7f0602da

.field public static final m3_sys_color_dark_on_tertiary:I = 0x7f0602db

.field public static final m3_sys_color_dark_on_tertiary_container:I = 0x7f0602dc

.field public static final m3_sys_color_dark_outline:I = 0x7f0602dd

.field public static final m3_sys_color_dark_primary:I = 0x7f0602de

.field public static final m3_sys_color_dark_primary_container:I = 0x7f0602df

.field public static final m3_sys_color_dark_secondary:I = 0x7f0602e0

.field public static final m3_sys_color_dark_secondary_container:I = 0x7f0602e1

.field public static final m3_sys_color_dark_surface:I = 0x7f0602e2

.field public static final m3_sys_color_dark_surface_variant:I = 0x7f0602e3

.field public static final m3_sys_color_dark_tertiary:I = 0x7f0602e4

.field public static final m3_sys_color_dark_tertiary_container:I = 0x7f0602e5

.field public static final m3_sys_color_dynamic_dark_background:I = 0x7f0602e6

.field public static final m3_sys_color_dynamic_dark_inverse_on_surface:I = 0x7f0602e7

.field public static final m3_sys_color_dynamic_dark_inverse_primary:I = 0x7f0602e8

.field public static final m3_sys_color_dynamic_dark_inverse_surface:I = 0x7f0602e9

.field public static final m3_sys_color_dynamic_dark_on_background:I = 0x7f0602ea

.field public static final m3_sys_color_dynamic_dark_on_primary:I = 0x7f0602eb

.field public static final m3_sys_color_dynamic_dark_on_primary_container:I = 0x7f0602ec

.field public static final m3_sys_color_dynamic_dark_on_secondary:I = 0x7f0602ed

.field public static final m3_sys_color_dynamic_dark_on_secondary_container:I = 0x7f0602ee

.field public static final m3_sys_color_dynamic_dark_on_surface:I = 0x7f0602ef

.field public static final m3_sys_color_dynamic_dark_on_surface_variant:I = 0x7f0602f0

.field public static final m3_sys_color_dynamic_dark_on_tertiary:I = 0x7f0602f1

.field public static final m3_sys_color_dynamic_dark_on_tertiary_container:I = 0x7f0602f2

.field public static final m3_sys_color_dynamic_dark_outline:I = 0x7f0602f3

.field public static final m3_sys_color_dynamic_dark_primary:I = 0x7f0602f4

.field public static final m3_sys_color_dynamic_dark_primary_container:I = 0x7f0602f5

.field public static final m3_sys_color_dynamic_dark_secondary:I = 0x7f0602f6

.field public static final m3_sys_color_dynamic_dark_secondary_container:I = 0x7f0602f7

.field public static final m3_sys_color_dynamic_dark_surface:I = 0x7f0602f8

.field public static final m3_sys_color_dynamic_dark_surface_variant:I = 0x7f0602f9

.field public static final m3_sys_color_dynamic_dark_tertiary:I = 0x7f0602fa

.field public static final m3_sys_color_dynamic_dark_tertiary_container:I = 0x7f0602fb

.field public static final m3_sys_color_dynamic_light_background:I = 0x7f0602fc

.field public static final m3_sys_color_dynamic_light_inverse_on_surface:I = 0x7f0602fd

.field public static final m3_sys_color_dynamic_light_inverse_primary:I = 0x7f0602fe

.field public static final m3_sys_color_dynamic_light_inverse_surface:I = 0x7f0602ff

.field public static final m3_sys_color_dynamic_light_on_background:I = 0x7f060300

.field public static final m3_sys_color_dynamic_light_on_primary:I = 0x7f060301

.field public static final m3_sys_color_dynamic_light_on_primary_container:I = 0x7f060302

.field public static final m3_sys_color_dynamic_light_on_secondary:I = 0x7f060303

.field public static final m3_sys_color_dynamic_light_on_secondary_container:I = 0x7f060304

.field public static final m3_sys_color_dynamic_light_on_surface:I = 0x7f060305

.field public static final m3_sys_color_dynamic_light_on_surface_variant:I = 0x7f060306

.field public static final m3_sys_color_dynamic_light_on_tertiary:I = 0x7f060307

.field public static final m3_sys_color_dynamic_light_on_tertiary_container:I = 0x7f060308

.field public static final m3_sys_color_dynamic_light_outline:I = 0x7f060309

.field public static final m3_sys_color_dynamic_light_primary:I = 0x7f06030a

.field public static final m3_sys_color_dynamic_light_primary_container:I = 0x7f06030b

.field public static final m3_sys_color_dynamic_light_secondary:I = 0x7f06030c

.field public static final m3_sys_color_dynamic_light_secondary_container:I = 0x7f06030d

.field public static final m3_sys_color_dynamic_light_surface:I = 0x7f06030e

.field public static final m3_sys_color_dynamic_light_surface_variant:I = 0x7f06030f

.field public static final m3_sys_color_dynamic_light_tertiary:I = 0x7f060310

.field public static final m3_sys_color_dynamic_light_tertiary_container:I = 0x7f060311

.field public static final m3_sys_color_light_background:I = 0x7f060312

.field public static final m3_sys_color_light_error:I = 0x7f060313

.field public static final m3_sys_color_light_error_container:I = 0x7f060314

.field public static final m3_sys_color_light_inverse_on_surface:I = 0x7f060315

.field public static final m3_sys_color_light_inverse_primary:I = 0x7f060316

.field public static final m3_sys_color_light_inverse_surface:I = 0x7f060317

.field public static final m3_sys_color_light_on_background:I = 0x7f060318

.field public static final m3_sys_color_light_on_error:I = 0x7f060319

.field public static final m3_sys_color_light_on_error_container:I = 0x7f06031a

.field public static final m3_sys_color_light_on_primary:I = 0x7f06031b

.field public static final m3_sys_color_light_on_primary_container:I = 0x7f06031c

.field public static final m3_sys_color_light_on_secondary:I = 0x7f06031d

.field public static final m3_sys_color_light_on_secondary_container:I = 0x7f06031e

.field public static final m3_sys_color_light_on_surface:I = 0x7f06031f

.field public static final m3_sys_color_light_on_surface_variant:I = 0x7f060320

.field public static final m3_sys_color_light_on_tertiary:I = 0x7f060321

.field public static final m3_sys_color_light_on_tertiary_container:I = 0x7f060322

.field public static final m3_sys_color_light_outline:I = 0x7f060323

.field public static final m3_sys_color_light_primary:I = 0x7f060324

.field public static final m3_sys_color_light_primary_container:I = 0x7f060325

.field public static final m3_sys_color_light_secondary:I = 0x7f060326

.field public static final m3_sys_color_light_secondary_container:I = 0x7f060327

.field public static final m3_sys_color_light_surface:I = 0x7f060328

.field public static final m3_sys_color_light_surface_variant:I = 0x7f060329

.field public static final m3_sys_color_light_tertiary:I = 0x7f06032a

.field public static final m3_sys_color_light_tertiary_container:I = 0x7f06032b

.field public static final m3_tabs_icon_color:I = 0x7f06032c

.field public static final m3_tabs_ripple_color:I = 0x7f06032d

.field public static final m3_text_button_background_color_selector:I = 0x7f06032e

.field public static final m3_text_button_foreground_color_selector:I = 0x7f06032f

.field public static final m3_text_button_ripple_color_selector:I = 0x7f060330

.field public static final m3_textfield_filled_background_color:I = 0x7f060331

.field public static final m3_textfield_indicator_text_color:I = 0x7f060332

.field public static final m3_textfield_input_text_color:I = 0x7f060333

.field public static final m3_textfield_label_color:I = 0x7f060334

.field public static final m3_textfield_stroke_color:I = 0x7f060335

.field public static final m3_timepicker_button_background_color:I = 0x7f060336

.field public static final m3_timepicker_button_ripple_color:I = 0x7f060337

.field public static final m3_timepicker_button_text_color:I = 0x7f060338

.field public static final m3_timepicker_clock_text_color:I = 0x7f060339

.field public static final m3_timepicker_display_background_color:I = 0x7f06033a

.field public static final m3_timepicker_display_ripple_color:I = 0x7f06033b

.field public static final m3_timepicker_display_stroke_color:I = 0x7f06033c

.field public static final m3_timepicker_display_text_color:I = 0x7f06033d

.field public static final m3_timepicker_secondary_text_button_ripple_color:I = 0x7f06033e

.field public static final m3_timepicker_secondary_text_button_text_color:I = 0x7f06033f

.field public static final m3_tonal_button_ripple_color_selector:I = 0x7f060340

.field public static final main_page_background:I = 0x7f060341

.field public static final mall_card_arrowRight:I = 0x7f060342

.field public static final material_blue_500:I = 0x7f060343

.field public static final material_blue_700:I = 0x7f060344

.field public static final material_blue_grey_800:I = 0x7f060345

.field public static final material_blue_grey_900:I = 0x7f060346

.field public static final material_blue_grey_950:I = 0x7f060347

.field public static final material_cursor_color:I = 0x7f060348

.field public static final material_deep_teal_200:I = 0x7f060349

.field public static final material_deep_teal_500:I = 0x7f06034a

.field public static final material_divider_color:I = 0x7f06034b

.field public static final material_dynamic_neutral0:I = 0x7f06034c

.field public static final material_dynamic_neutral10:I = 0x7f06034d

.field public static final material_dynamic_neutral100:I = 0x7f06034e

.field public static final material_dynamic_neutral20:I = 0x7f06034f

.field public static final material_dynamic_neutral30:I = 0x7f060350

.field public static final material_dynamic_neutral40:I = 0x7f060351

.field public static final material_dynamic_neutral50:I = 0x7f060352

.field public static final material_dynamic_neutral60:I = 0x7f060353

.field public static final material_dynamic_neutral70:I = 0x7f060354

.field public static final material_dynamic_neutral80:I = 0x7f060355

.field public static final material_dynamic_neutral90:I = 0x7f060356

.field public static final material_dynamic_neutral95:I = 0x7f060357

.field public static final material_dynamic_neutral99:I = 0x7f060358

.field public static final material_dynamic_neutral_variant0:I = 0x7f060359

.field public static final material_dynamic_neutral_variant10:I = 0x7f06035a

.field public static final material_dynamic_neutral_variant100:I = 0x7f06035b

.field public static final material_dynamic_neutral_variant20:I = 0x7f06035c

.field public static final material_dynamic_neutral_variant30:I = 0x7f06035d

.field public static final material_dynamic_neutral_variant40:I = 0x7f06035e

.field public static final material_dynamic_neutral_variant50:I = 0x7f06035f

.field public static final material_dynamic_neutral_variant60:I = 0x7f060360

.field public static final material_dynamic_neutral_variant70:I = 0x7f060361

.field public static final material_dynamic_neutral_variant80:I = 0x7f060362

.field public static final material_dynamic_neutral_variant90:I = 0x7f060363

.field public static final material_dynamic_neutral_variant95:I = 0x7f060364

.field public static final material_dynamic_neutral_variant99:I = 0x7f060365

.field public static final material_dynamic_primary0:I = 0x7f060366

.field public static final material_dynamic_primary10:I = 0x7f060367

.field public static final material_dynamic_primary100:I = 0x7f060368

.field public static final material_dynamic_primary20:I = 0x7f060369

.field public static final material_dynamic_primary30:I = 0x7f06036a

.field public static final material_dynamic_primary40:I = 0x7f06036b

.field public static final material_dynamic_primary50:I = 0x7f06036c

.field public static final material_dynamic_primary60:I = 0x7f06036d

.field public static final material_dynamic_primary70:I = 0x7f06036e

.field public static final material_dynamic_primary80:I = 0x7f06036f

.field public static final material_dynamic_primary90:I = 0x7f060370

.field public static final material_dynamic_primary95:I = 0x7f060371

.field public static final material_dynamic_primary99:I = 0x7f060372

.field public static final material_dynamic_secondary0:I = 0x7f060373

.field public static final material_dynamic_secondary10:I = 0x7f060374

.field public static final material_dynamic_secondary100:I = 0x7f060375

.field public static final material_dynamic_secondary20:I = 0x7f060376

.field public static final material_dynamic_secondary30:I = 0x7f060377

.field public static final material_dynamic_secondary40:I = 0x7f060378

.field public static final material_dynamic_secondary50:I = 0x7f060379

.field public static final material_dynamic_secondary60:I = 0x7f06037a

.field public static final material_dynamic_secondary70:I = 0x7f06037b

.field public static final material_dynamic_secondary80:I = 0x7f06037c

.field public static final material_dynamic_secondary90:I = 0x7f06037d

.field public static final material_dynamic_secondary95:I = 0x7f06037e

.field public static final material_dynamic_secondary99:I = 0x7f06037f

.field public static final material_dynamic_tertiary0:I = 0x7f060380

.field public static final material_dynamic_tertiary10:I = 0x7f060381

.field public static final material_dynamic_tertiary100:I = 0x7f060382

.field public static final material_dynamic_tertiary20:I = 0x7f060383

.field public static final material_dynamic_tertiary30:I = 0x7f060384

.field public static final material_dynamic_tertiary40:I = 0x7f060385

.field public static final material_dynamic_tertiary50:I = 0x7f060386

.field public static final material_dynamic_tertiary60:I = 0x7f060387

.field public static final material_dynamic_tertiary70:I = 0x7f060388

.field public static final material_dynamic_tertiary80:I = 0x7f060389

.field public static final material_dynamic_tertiary90:I = 0x7f06038a

.field public static final material_dynamic_tertiary95:I = 0x7f06038b

.field public static final material_dynamic_tertiary99:I = 0x7f06038c

.field public static final material_empty_color_light:I = 0x7f06038d

.field public static final material_grey_100:I = 0x7f06038e

.field public static final material_grey_200:I = 0x7f06038f

.field public static final material_grey_300:I = 0x7f060390

.field public static final material_grey_50:I = 0x7f060391

.field public static final material_grey_600:I = 0x7f060392

.field public static final material_grey_800:I = 0x7f060393

.field public static final material_grey_850:I = 0x7f060394

.field public static final material_grey_900:I = 0x7f060395

.field public static final material_harmonized_color_error:I = 0x7f060396

.field public static final material_harmonized_color_error_container:I = 0x7f060397

.field public static final material_harmonized_color_on_error:I = 0x7f060398

.field public static final material_harmonized_color_on_error_container:I = 0x7f060399

.field public static final material_on_background_disabled:I = 0x7f06039a

.field public static final material_on_background_emphasis_high_type:I = 0x7f06039b

.field public static final material_on_background_emphasis_medium:I = 0x7f06039c

.field public static final material_on_primary_disabled:I = 0x7f06039d

.field public static final material_on_primary_emphasis_high_type:I = 0x7f06039e

.field public static final material_on_primary_emphasis_medium:I = 0x7f06039f

.field public static final material_on_surface_disabled:I = 0x7f0603a0

.field public static final material_on_surface_emphasis_high_type:I = 0x7f0603a1

.field public static final material_on_surface_emphasis_medium:I = 0x7f0603a2

.field public static final material_on_surface_stroke:I = 0x7f0603a3

.field public static final material_slider_active_tick_marks_color:I = 0x7f0603a4

.field public static final material_slider_active_track_color:I = 0x7f0603a5

.field public static final material_slider_halo_color:I = 0x7f0603a6

.field public static final material_slider_inactive_tick_marks_color:I = 0x7f0603a7

.field public static final material_slider_inactive_track_color:I = 0x7f0603a8

.field public static final material_slider_thumb_color:I = 0x7f0603a9

.field public static final material_timepicker_button_background:I = 0x7f0603aa

.field public static final material_timepicker_button_stroke:I = 0x7f0603ab

.field public static final material_timepicker_clock_text_color:I = 0x7f0603ac

.field public static final material_timepicker_clockface:I = 0x7f0603ad

.field public static final material_timepicker_modebutton_tint:I = 0x7f0603ae

.field public static final message_bubble_incoming:I = 0x7f0603af

.field public static final message_bubble_outgoing:I = 0x7f0603b0

.field public static final message_icon_background_incoming:I = 0x7f0603b1

.field public static final message_icon_background_outgoing:I = 0x7f0603b2

.field public static final message_icon_color:I = 0x7f0603b3

.field public static final message_icon_text_incoming:I = 0x7f0603b4

.field public static final message_icon_text_outgoing:I = 0x7f0603b5

.field public static final message_text_incoming:I = 0x7f0603b6

.field public static final message_text_outgoing:I = 0x7f0603b7

.field public static final message_view_guide_bg_color_light:I = 0x7f0603b8

.field public static final message_view_text_color_light:I = 0x7f0603b9

.field public static final meter_background_color:I = 0x7f0603ba

.field public static final meter_consumed_color:I = 0x7f0603bb

.field public static final miheadset_key_config_init_color:I = 0x7f0603bc

.field public static final miheadset_key_config_list_title:I = 0x7f0603bd

.field public static final miui_face_data_prompt_text_color:I = 0x7f0603be

.field public static final miui_face_detail_edittext_text_color:I = 0x7f0603bf

.field public static final miui_face_detail_edittext_text_hint_color:I = 0x7f0603c0

.field public static final miui_face_input_button_text_disable:I = 0x7f0603c1

.field public static final miui_face_input_button_text_enable:I = 0x7f0603c2

.field public static final miui_face_input_cameraview_cover_color:I = 0x7f0603c3

.field public static final miui_face_input_first_suggestion_color:I = 0x7f0603c4

.field public static final miui_face_input_name_edittext_hint_text_color:I = 0x7f0603c5

.field public static final miui_face_input_normal_text_color:I = 0x7f0603c6

.field public static final miui_face_input_second_suggestion_color:I = 0x7f0603c7

.field public static final miui_face_input_warning_dialog_highlight_color:I = 0x7f0603c8

.field public static final miui_keyboard_bg:I = 0x7f0603c9

.field public static final miui_lab_divider:I = 0x7f0603ca

.field public static final miui_seekbar_title_color:I = 0x7f0603cb

.field public static final miuisettings_item_touch_color:I = 0x7f0603cc

.field public static final miuix_appcompat_accent_color_dark:I = 0x7f0603cd

.field public static final miuix_appcompat_accent_color_light:I = 0x7f0603ce

.field public static final miuix_appcompat_action_bar_overlay_mask_color_oled_dark:I = 0x7f0603cf

.field public static final miuix_appcompat_action_bar_overlay_mask_color_oled_light:I = 0x7f0603d0

.field public static final miuix_appcompat_action_bar_search_mask_color_dark:I = 0x7f0603d1

.field public static final miuix_appcompat_action_bar_search_mask_color_light:I = 0x7f0603d2

.field public static final miuix_appcompat_action_bar_subtitle_text_dark:I = 0x7f0603d3

.field public static final miuix_appcompat_action_bar_subtitle_text_light:I = 0x7f0603d4

.field public static final miuix_appcompat_action_bar_tab_collapse_text_color_disable_dark:I = 0x7f0603d5

.field public static final miuix_appcompat_action_bar_tab_collapse_text_color_disable_light:I = 0x7f0603d6

.field public static final miuix_appcompat_action_bar_tab_collapse_text_color_normal_dark:I = 0x7f0603d7

.field public static final miuix_appcompat_action_bar_tab_collapse_text_color_normal_light:I = 0x7f0603d8

.field public static final miuix_appcompat_action_bar_tab_collapse_text_color_pressed_dark:I = 0x7f0603d9

.field public static final miuix_appcompat_action_bar_tab_collapse_text_color_pressed_light:I = 0x7f0603da

.field public static final miuix_appcompat_action_bar_tab_collapse_text_color_selected_dark:I = 0x7f0603db

.field public static final miuix_appcompat_action_bar_tab_collapse_text_color_selected_light:I = 0x7f0603dc

.field public static final miuix_appcompat_action_bar_tab_collapse_text_dark:I = 0x7f0603dd

.field public static final miuix_appcompat_action_bar_tab_collapse_text_light:I = 0x7f0603de

.field public static final miuix_appcompat_action_bar_tab_text_color_disable_dark:I = 0x7f0603df

.field public static final miuix_appcompat_action_bar_tab_text_color_disable_light:I = 0x7f0603e0

.field public static final miuix_appcompat_action_bar_tab_text_color_normal_dark:I = 0x7f0603e1

.field public static final miuix_appcompat_action_bar_tab_text_color_normal_light:I = 0x7f0603e2

.field public static final miuix_appcompat_action_bar_tab_text_color_pressed_dark:I = 0x7f0603e3

.field public static final miuix_appcompat_action_bar_tab_text_color_pressed_light:I = 0x7f0603e4

.field public static final miuix_appcompat_action_bar_tab_text_color_selected_dark:I = 0x7f0603e5

.field public static final miuix_appcompat_action_bar_tab_text_color_selected_light:I = 0x7f0603e6

.field public static final miuix_appcompat_action_bar_tab_text_dark:I = 0x7f0603e7

.field public static final miuix_appcompat_action_bar_tab_text_light:I = 0x7f0603e8

.field public static final miuix_appcompat_action_bar_title_settings_text_color_light:I = 0x7f0603e9

.field public static final miuix_appcompat_action_bar_title_text_color_dark:I = 0x7f0603ea

.field public static final miuix_appcompat_action_bar_title_text_color_disabled_dark:I = 0x7f0603eb

.field public static final miuix_appcompat_action_bar_title_text_color_disabled_light:I = 0x7f0603ec

.field public static final miuix_appcompat_action_bar_title_text_color_light:I = 0x7f0603ed

.field public static final miuix_appcompat_action_bar_title_text_dark:I = 0x7f0603ee

.field public static final miuix_appcompat_action_bar_title_text_light:I = 0x7f0603ef

.field public static final miuix_appcompat_action_button_color_normal_dark:I = 0x7f0603f0

.field public static final miuix_appcompat_action_button_main_color:I = 0x7f0603f1

.field public static final miuix_appcompat_action_button_text_color_disable_dark:I = 0x7f0603f2

.field public static final miuix_appcompat_action_button_text_color_disable_light:I = 0x7f0603f3

.field public static final miuix_appcompat_action_button_text_color_normal_dark:I = 0x7f0603f4

.field public static final miuix_appcompat_action_button_text_color_normal_light:I = 0x7f0603f5

.field public static final miuix_appcompat_action_button_text_color_pressed_dark:I = 0x7f0603f6

.field public static final miuix_appcompat_action_button_text_color_pressed_light:I = 0x7f0603f7

.field public static final miuix_appcompat_action_button_text_color_selected_dark:I = 0x7f0603f8

.field public static final miuix_appcompat_action_button_text_color_selected_light:I = 0x7f0603f9

.field public static final miuix_appcompat_action_button_text_dark:I = 0x7f0603fa

.field public static final miuix_appcompat_action_button_text_light:I = 0x7f0603fb

.field public static final miuix_appcompat_action_mode_button_text_color_disable_dark:I = 0x7f0603fc

.field public static final miuix_appcompat_action_mode_button_text_color_disable_light:I = 0x7f0603fd

.field public static final miuix_appcompat_action_mode_button_text_color_normal_dark:I = 0x7f0603fe

.field public static final miuix_appcompat_action_mode_button_text_color_normal_light:I = 0x7f0603ff

.field public static final miuix_appcompat_action_mode_button_text_color_pressed_dark:I = 0x7f060400

.field public static final miuix_appcompat_action_mode_button_text_color_pressed_light:I = 0x7f060401

.field public static final miuix_appcompat_action_mode_button_text_color_selected_dark:I = 0x7f060402

.field public static final miuix_appcompat_action_mode_button_text_color_selected_light:I = 0x7f060403

.field public static final miuix_appcompat_action_mode_button_text_dark:I = 0x7f060404

.field public static final miuix_appcompat_action_mode_button_text_light:I = 0x7f060405

.field public static final miuix_appcompat_action_mode_title_button_text_dark:I = 0x7f060406

.field public static final miuix_appcompat_action_mode_title_button_text_light:I = 0x7f060407

.field public static final miuix_appcompat_action_mode_title_default_button_text_color_disable_dark:I = 0x7f060408

.field public static final miuix_appcompat_action_mode_title_default_button_text_color_disable_light:I = 0x7f060409

.field public static final miuix_appcompat_action_mode_title_default_button_text_color_normal_dark:I = 0x7f06040a

.field public static final miuix_appcompat_action_mode_title_default_button_text_color_normal_light:I = 0x7f06040b

.field public static final miuix_appcompat_action_mode_title_default_button_text_color_pressed_dark:I = 0x7f06040c

.field public static final miuix_appcompat_action_mode_title_default_button_text_color_pressed_light:I = 0x7f06040d

.field public static final miuix_appcompat_action_mode_title_default_button_text_dark:I = 0x7f06040e

.field public static final miuix_appcompat_action_mode_title_default_button_text_light:I = 0x7f06040f

.field public static final miuix_appcompat_action_mode_title_text_color_dark:I = 0x7f060410

.field public static final miuix_appcompat_action_mode_title_text_color_light:I = 0x7f060411

.field public static final miuix_appcompat_action_title_color_dark:I = 0x7f060412

.field public static final miuix_appcompat_action_title_color_light:I = 0x7f060413

.field public static final miuix_appcompat_activated_text_dark:I = 0x7f060414

.field public static final miuix_appcompat_activated_text_light:I = 0x7f060415

.field public static final miuix_appcompat_alphabet_indexer_activated_text_color:I = 0x7f060416

.field public static final miuix_appcompat_alphabet_indexer_activated_text_color_dark:I = 0x7f060417

.field public static final miuix_appcompat_alphabet_indexer_bg_color_dark:I = 0x7f060418

.field public static final miuix_appcompat_alphabet_indexer_bg_color_light:I = 0x7f060419

.field public static final miuix_appcompat_alphabet_indexer_highlight_text_color:I = 0x7f06041a

.field public static final miuix_appcompat_alphabet_indexer_highlight_text_color_dark:I = 0x7f06041b

.field public static final miuix_appcompat_alphabet_indexer_overlay_bg_color_dark:I = 0x7f06041c

.field public static final miuix_appcompat_alphabet_indexer_overlay_bg_color_light:I = 0x7f06041d

.field public static final miuix_appcompat_alphabet_indexer_overlay_dark:I = 0x7f06041e

.field public static final miuix_appcompat_alphabet_indexer_overlay_light:I = 0x7f06041f

.field public static final miuix_appcompat_alphabet_indexer_overlay_text_color:I = 0x7f060420

.field public static final miuix_appcompat_alphabet_indexer_overlay_text_color_dark:I = 0x7f060421

.field public static final miuix_appcompat_alphabet_indexer_text_color:I = 0x7f060422

.field public static final miuix_appcompat_alphabet_indexer_text_color_dark:I = 0x7f060423

.field public static final miuix_appcompat_alphabet_indexer_text_dark:I = 0x7f060424

.field public static final miuix_appcompat_alphabet_indexer_text_light:I = 0x7f060425

.field public static final miuix_appcompat_arrow_popup_triangle_color_dark:I = 0x7f060426

.field public static final miuix_appcompat_arrow_popup_triangle_color_light:I = 0x7f060427

.field public static final miuix_appcompat_arrow_popup_window_dim_color_dark:I = 0x7f060428

.field public static final miuix_appcompat_arrow_popup_window_dim_color_light:I = 0x7f060429

.field public static final miuix_appcompat_arrow_right_disable_fill_color_dark:I = 0x7f06042a

.field public static final miuix_appcompat_arrow_right_disable_fill_color_light:I = 0x7f06042b

.field public static final miuix_appcompat_arrow_right_fill_color_dark:I = 0x7f06042c

.field public static final miuix_appcompat_arrow_right_fill_color_light:I = 0x7f06042d

.field public static final miuix_appcompat_arrow_right_normal_fill_color_dark:I = 0x7f06042e

.field public static final miuix_appcompat_arrow_right_normal_fill_color_light:I = 0x7f06042f

.field public static final miuix_appcompat_arrow_right_pressed_fill_color_dark:I = 0x7f060430

.field public static final miuix_appcompat_arrow_right_pressed_fill_color_light:I = 0x7f060431

.field public static final miuix_appcompat_background_dark:I = 0x7f060432

.field public static final miuix_appcompat_background_light:I = 0x7f060433

.field public static final miuix_appcompat_black:I = 0x7f060434

.field public static final miuix_appcompat_blank_page_button_text_color_dark:I = 0x7f060435

.field public static final miuix_appcompat_blank_page_button_text_color_light:I = 0x7f060436

.field public static final miuix_appcompat_blank_page_text_color_dark:I = 0x7f060437

.field public static final miuix_appcompat_blank_page_text_color_light:I = 0x7f060438

.field public static final miuix_appcompat_blur_bg_mix_color_dark:I = 0x7f060439

.field public static final miuix_appcompat_blur_bg_mix_color_light:I = 0x7f06043a

.field public static final miuix_appcompat_bright_foreground_dark:I = 0x7f06043b

.field public static final miuix_appcompat_bright_foreground_dark_disabled:I = 0x7f06043c

.field public static final miuix_appcompat_bright_foreground_light:I = 0x7f06043d

.field public static final miuix_appcompat_bright_foreground_light_disabled:I = 0x7f06043e

.field public static final miuix_appcompat_btn_bg_color_dark:I = 0x7f06043f

.field public static final miuix_appcompat_btn_bg_color_light:I = 0x7f060440

.field public static final miuix_appcompat_btn_bg_primary_color_dark:I = 0x7f060441

.field public static final miuix_appcompat_btn_bg_primary_color_light:I = 0x7f060442

.field public static final miuix_appcompat_button_bg_color_normal_dark:I = 0x7f060443

.field public static final miuix_appcompat_button_bg_color_normal_light:I = 0x7f060444

.field public static final miuix_appcompat_button_bg_color_pressed_dark:I = 0x7f060445

.field public static final miuix_appcompat_button_bg_color_pressed_light:I = 0x7f060446

.field public static final miuix_appcompat_button_bg_color_warn_disabled_dark:I = 0x7f060447

.field public static final miuix_appcompat_button_bg_color_warn_disabled_light:I = 0x7f060448

.field public static final miuix_appcompat_button_bg_color_warn_normal_dark:I = 0x7f060449

.field public static final miuix_appcompat_button_bg_color_warn_normal_light:I = 0x7f06044a

.field public static final miuix_appcompat_button_bg_color_warn_pressed_dark:I = 0x7f06044b

.field public static final miuix_appcompat_button_bg_color_warn_pressed_light:I = 0x7f06044c

.field public static final miuix_appcompat_button_bg_primary_color_disabled_dark:I = 0x7f06044d

.field public static final miuix_appcompat_button_bg_primary_color_disabled_light:I = 0x7f06044e

.field public static final miuix_appcompat_button_bg_primary_color_normal_dark:I = 0x7f06044f

.field public static final miuix_appcompat_button_bg_primary_color_normal_light:I = 0x7f060450

.field public static final miuix_appcompat_button_border_color_dark:I = 0x7f060451

.field public static final miuix_appcompat_button_text_color_guide_dark:I = 0x7f060452

.field public static final miuix_appcompat_button_text_color_guide_disabled_dark:I = 0x7f060453

.field public static final miuix_appcompat_button_text_color_guide_disabled_light:I = 0x7f060454

.field public static final miuix_appcompat_button_text_color_guide_light:I = 0x7f060455

.field public static final miuix_appcompat_button_text_color_guide_normal_dark:I = 0x7f060456

.field public static final miuix_appcompat_button_text_color_guide_normal_light:I = 0x7f060457

.field public static final miuix_appcompat_button_text_color_negative_dark:I = 0x7f060458

.field public static final miuix_appcompat_button_text_color_negative_light:I = 0x7f060459

.field public static final miuix_appcompat_button_text_color_positive_disabled_dark:I = 0x7f06045a

.field public static final miuix_appcompat_button_text_color_positive_disabled_light:I = 0x7f06045b

.field public static final miuix_appcompat_button_text_color_positive_light:I = 0x7f06045c

.field public static final miuix_appcompat_button_text_color_positive_normal_dark:I = 0x7f06045d

.field public static final miuix_appcompat_button_text_color_positive_normal_light:I = 0x7f06045e

.field public static final miuix_appcompat_button_text_color_warning_disabled_dark:I = 0x7f06045f

.field public static final miuix_appcompat_button_text_color_warning_disabled_light:I = 0x7f060460

.field public static final miuix_appcompat_button_text_color_warning_normal_dark:I = 0x7f060461

.field public static final miuix_appcompat_button_text_color_warning_normal_light:I = 0x7f060462

.field public static final miuix_appcompat_button_text_dark:I = 0x7f060463

.field public static final miuix_appcompat_button_text_light:I = 0x7f060464

.field public static final miuix_appcompat_checkable_btn_text_color_stable_dark:I = 0x7f060465

.field public static final miuix_appcompat_checkable_btn_text_color_stable_light:I = 0x7f060466

.field public static final miuix_appcompat_checkable_btn_text_dark:I = 0x7f060467

.field public static final miuix_appcompat_checkable_btn_text_light:I = 0x7f060468

.field public static final miuix_appcompat_checkbox_btn_disable_background_color_dark:I = 0x7f060469

.field public static final miuix_appcompat_checkbox_btn_disable_background_color_light:I = 0x7f06046a

.field public static final miuix_appcompat_checkbox_btn_stroke_color_dark:I = 0x7f06046b

.field public static final miuix_appcompat_checkbox_btn_stroke_color_light:I = 0x7f06046c

.field public static final miuix_appcompat_checked_disabled_text_light:I = 0x7f06046d

.field public static final miuix_appcompat_checked_text_dark:I = 0x7f06046e

.field public static final miuix_appcompat_checked_text_light:I = 0x7f06046f

.field public static final miuix_appcompat_context_menu_item_bg_color_normal_dark:I = 0x7f060470

.field public static final miuix_appcompat_context_menu_item_bg_color_normal_light:I = 0x7f060471

.field public static final miuix_appcompat_context_menu_item_bg_color_pressed_dark:I = 0x7f060472

.field public static final miuix_appcompat_context_menu_item_bg_color_pressed_light:I = 0x7f060473

.field public static final miuix_appcompat_context_menu_separate_item_bg_color_dark:I = 0x7f060474

.field public static final miuix_appcompat_context_menu_separate_item_bg_color_light:I = 0x7f060475

.field public static final miuix_appcompat_date_picker_lunar_text_color:I = 0x7f060476

.field public static final miuix_appcompat_default_ic_visible_fillcolor_dark:I = 0x7f060477

.field public static final miuix_appcompat_default_ic_visible_fillcolor_light:I = 0x7f060478

.field public static final miuix_appcompat_default_number_picker_highlight_color:I = 0x7f060479

.field public static final miuix_appcompat_default_number_picker_highlight_color_dark:I = 0x7f06047a

.field public static final miuix_appcompat_default_number_picker_hint_color:I = 0x7f06047b

.field public static final miuix_appcompat_default_number_picker_hint_color_dark:I = 0x7f06047c

.field public static final miuix_appcompat_dialog_bg_color_dark:I = 0x7f06047d

.field public static final miuix_appcompat_dialog_bg_color_light:I = 0x7f06047e

.field public static final miuix_appcompat_dialog_button_divider_line_light:I = 0x7f06047f

.field public static final miuix_appcompat_dialog_default_bg_color_dark:I = 0x7f060480

.field public static final miuix_appcompat_dialog_default_bg_color_light:I = 0x7f060481

.field public static final miuix_appcompat_dialog_default_button_bg_color_normal_dark:I = 0x7f060482

.field public static final miuix_appcompat_dialog_default_button_bg_color_normal_light:I = 0x7f060483

.field public static final miuix_appcompat_dialog_default_button_bg_primary_color_disabled_dark:I = 0x7f060484

.field public static final miuix_appcompat_dialog_default_button_bg_primary_color_disabled_light:I = 0x7f060485

.field public static final miuix_appcompat_dialog_default_button_bg_primary_color_normal_dark:I = 0x7f060486

.field public static final miuix_appcompat_dialog_default_button_bg_primary_color_normal_light:I = 0x7f060487

.field public static final miuix_appcompat_dialog_default_button_disabled_text_dark:I = 0x7f060488

.field public static final miuix_appcompat_dialog_default_button_disabled_text_light:I = 0x7f060489

.field public static final miuix_appcompat_dialog_default_button_text_color_dark:I = 0x7f06048a

.field public static final miuix_appcompat_dialog_default_button_text_color_guide_disabled_dark:I = 0x7f06048b

.field public static final miuix_appcompat_dialog_default_button_text_color_guide_disabled_light:I = 0x7f06048c

.field public static final miuix_appcompat_dialog_default_button_text_color_guide_normal_dark:I = 0x7f06048d

.field public static final miuix_appcompat_dialog_default_button_text_color_guide_normal_light:I = 0x7f06048e

.field public static final miuix_appcompat_dialog_default_button_text_color_light:I = 0x7f06048f

.field public static final miuix_appcompat_dialog_default_checkbox_btn_on_background_color_dark:I = 0x7f060490

.field public static final miuix_appcompat_dialog_default_checkbox_btn_on_background_color_light:I = 0x7f060491

.field public static final miuix_appcompat_dialog_default_checkbox_btn_on_foreground_color_dark:I = 0x7f060492

.field public static final miuix_appcompat_dialog_default_checkbox_btn_on_foreground_color_light:I = 0x7f060493

.field public static final miuix_appcompat_dialog_default_checkbox_text_color_dark:I = 0x7f060494

.field public static final miuix_appcompat_dialog_default_checkbox_text_color_light:I = 0x7f060495

.field public static final miuix_appcompat_dialog_default_comment_text_color_dark:I = 0x7f060496

.field public static final miuix_appcompat_dialog_default_comment_text_color_light:I = 0x7f060497

.field public static final miuix_appcompat_dialog_default_edit_text_bg_solid_dark:I = 0x7f060498

.field public static final miuix_appcompat_dialog_default_edit_text_bg_solid_light:I = 0x7f060499

.field public static final miuix_appcompat_dialog_default_edit_text_dialog_color_normal_dark:I = 0x7f06049a

.field public static final miuix_appcompat_dialog_default_edit_text_dialog_color_normal_light:I = 0x7f06049b

.field public static final miuix_appcompat_dialog_default_hint_edit_text_color_dark:I = 0x7f06049c

.field public static final miuix_appcompat_dialog_default_hint_edit_text_color_light:I = 0x7f06049d

.field public static final miuix_appcompat_dialog_default_list_item_bg_dialog_color_checked_dark:I = 0x7f06049e

.field public static final miuix_appcompat_dialog_default_list_item_bg_dialog_color_checked_light:I = 0x7f06049f

.field public static final miuix_appcompat_dialog_default_list_text_color_disabled_dark:I = 0x7f0604a0

.field public static final miuix_appcompat_dialog_default_list_text_color_disabled_light:I = 0x7f0604a1

.field public static final miuix_appcompat_dialog_default_list_text_color_normal_dark:I = 0x7f0604a2

.field public static final miuix_appcompat_dialog_default_list_text_color_normal_light:I = 0x7f0604a3

.field public static final miuix_appcompat_dialog_default_list_text_color_pressed_dark:I = 0x7f0604a4

.field public static final miuix_appcompat_dialog_default_list_text_color_pressed_light:I = 0x7f0604a5

.field public static final miuix_appcompat_dialog_default_message_text_color_dark:I = 0x7f0604a6

.field public static final miuix_appcompat_dialog_default_message_text_color_light:I = 0x7f0604a7

.field public static final miuix_appcompat_dialog_default_number_picker_highlight_region_bg_color_dark:I = 0x7f0604a8

.field public static final miuix_appcompat_dialog_default_number_picker_highlight_region_bg_color_light:I = 0x7f0604a9

.field public static final miuix_appcompat_dialog_default_progress_percent_color:I = 0x7f0604aa

.field public static final miuix_appcompat_dialog_default_singleChoice_checkbox_btn_on_background_color_dark:I = 0x7f0604ab

.field public static final miuix_appcompat_dialog_default_singleChoice_checkbox_btn_on_background_color_light:I = 0x7f0604ac

.field public static final miuix_appcompat_dialog_default_singleChoice_checked_text_dark:I = 0x7f0604ad

.field public static final miuix_appcompat_dialog_default_singleChoice_checked_text_light:I = 0x7f0604ae

.field public static final miuix_appcompat_dialog_default_title_text_color_dark:I = 0x7f0604af

.field public static final miuix_appcompat_dialog_default_title_text_color_light:I = 0x7f0604b0

.field public static final miuix_appcompat_dialog_list_text_dark:I = 0x7f0604b1

.field public static final miuix_appcompat_dialog_list_text_dark_single_choice:I = 0x7f0604b2

.field public static final miuix_appcompat_dialog_list_text_light:I = 0x7f0604b3

.field public static final miuix_appcompat_dialog_list_text_light_single_choice:I = 0x7f0604b4

.field public static final miuix_appcompat_dialog_simple_list_title_color_dark:I = 0x7f0604b5

.field public static final miuix_appcompat_dialog_simple_list_title_color_light:I = 0x7f0604b6

.field public static final miuix_appcompat_dim_foreground_dark:I = 0x7f0604b7

.field public static final miuix_appcompat_dim_foreground_dark_disabled:I = 0x7f0604b8

.field public static final miuix_appcompat_dim_foreground_light:I = 0x7f0604b9

.field public static final miuix_appcompat_dim_foreground_light_disabled:I = 0x7f0604ba

.field public static final miuix_appcompat_disabled_text_dark:I = 0x7f0604bb

.field public static final miuix_appcompat_disabled_text_light:I = 0x7f0604bc

.field public static final miuix_appcompat_divider_line_dark:I = 0x7f0604bd

.field public static final miuix_appcompat_divider_line_light:I = 0x7f0604be

.field public static final miuix_appcompat_dropdown_popup_backgroud_color_dark:I = 0x7f0604bf

.field public static final miuix_appcompat_dropdown_popup_backgroud_color_light:I = 0x7f0604c0

.field public static final miuix_appcompat_dropdown_popup_list_item_bg_checked_dark:I = 0x7f0604c1

.field public static final miuix_appcompat_dropdown_popup_list_item_bg_checked_light:I = 0x7f0604c2

.field public static final miuix_appcompat_dropdown_popup_list_item_bg_dark:I = 0x7f0604c3

.field public static final miuix_appcompat_dropdown_popup_list_item_bg_light:I = 0x7f0604c4

.field public static final miuix_appcompat_dropdown_popup_list_item_bg_normal_dark:I = 0x7f0604c5

.field public static final miuix_appcompat_dropdown_popup_list_item_bg_normal_light:I = 0x7f0604c6

.field public static final miuix_appcompat_dropdown_popup_list_item_bg_pressed_dark:I = 0x7f0604c7

.field public static final miuix_appcompat_dropdown_popup_list_item_bg_pressed_light:I = 0x7f0604c8

.field public static final miuix_appcompat_dropdown_popup_list_text_color_checked_dark:I = 0x7f0604c9

.field public static final miuix_appcompat_dropdown_popup_list_text_color_checked_light:I = 0x7f0604ca

.field public static final miuix_appcompat_dropdown_popup_list_text_color_disabled_dark:I = 0x7f0604cb

.field public static final miuix_appcompat_dropdown_popup_list_text_color_disabled_light:I = 0x7f0604cc

.field public static final miuix_appcompat_dropdown_popup_list_text_color_normal_dark:I = 0x7f0604cd

.field public static final miuix_appcompat_dropdown_popup_list_text_color_normal_light:I = 0x7f0604ce

.field public static final miuix_appcompat_dropdown_popup_list_text_color_pressed_dark:I = 0x7f0604cf

.field public static final miuix_appcompat_dropdown_popup_list_text_color_pressed_light:I = 0x7f0604d0

.field public static final miuix_appcompat_dropdown_popup_list_text_dark:I = 0x7f0604d1

.field public static final miuix_appcompat_dropdown_popup_list_text_light:I = 0x7f0604d2

.field public static final miuix_appcompat_edit_text_bg_color_dark:I = 0x7f0604d3

.field public static final miuix_appcompat_edit_text_bg_color_light:I = 0x7f0604d4

.field public static final miuix_appcompat_edit_text_border_color:I = 0x7f0604d5

.field public static final miuix_appcompat_edit_text_border_color_error:I = 0x7f0604d6

.field public static final miuix_appcompat_edit_text_dialog_bg_stroke_dark:I = 0x7f0604d7

.field public static final miuix_appcompat_edit_text_dialog_bg_stroke_light:I = 0x7f0604d8

.field public static final miuix_appcompat_edit_text_dialog_color_disabled_dark:I = 0x7f0604d9

.field public static final miuix_appcompat_edit_text_dialog_color_disabled_light:I = 0x7f0604da

.field public static final miuix_appcompat_edit_text_dialog_dark:I = 0x7f0604db

.field public static final miuix_appcompat_edit_text_dialog_light:I = 0x7f0604dc

.field public static final miuix_appcompat_edit_text_search_bg_color_dark:I = 0x7f0604dd

.field public static final miuix_appcompat_edit_text_search_bg_color_light:I = 0x7f0604de

.field public static final miuix_appcompat_edit_text_search_bg_solid_dark:I = 0x7f0604df

.field public static final miuix_appcompat_edit_text_search_bg_solid_light:I = 0x7f0604e0

.field public static final miuix_appcompat_edit_text_search_color_dark:I = 0x7f0604e1

.field public static final miuix_appcompat_edit_text_search_color_light:I = 0x7f0604e2

.field public static final miuix_appcompat_edit_text_search_hint_color_dark:I = 0x7f0604e3

.field public static final miuix_appcompat_edit_text_search_hint_color_light:I = 0x7f0604e4

.field public static final miuix_appcompat_fab_color_dark:I = 0x7f0604e5

.field public static final miuix_appcompat_fab_color_light:I = 0x7f0604e6

.field public static final miuix_appcompat_filter_sort_arrow_view_color_dark:I = 0x7f0604e7

.field public static final miuix_appcompat_filter_sort_arrow_view_color_light:I = 0x7f0604e8

.field public static final miuix_appcompat_filter_sort_hover_bg_color:I = 0x7f0604e9

.field public static final miuix_appcompat_filter_sort_tab_view_background_normal_dark:I = 0x7f0604ea

.field public static final miuix_appcompat_filter_sort_tab_view_background_normal_light:I = 0x7f0604eb

.field public static final miuix_appcompat_filter_sort_tab_view_background_pressed_dark:I = 0x7f0604ec

.field public static final miuix_appcompat_filter_sort_tab_view_background_pressed_light:I = 0x7f0604ed

.field public static final miuix_appcompat_filter_sort_tab_view_bg_color_dark:I = 0x7f0604ee

.field public static final miuix_appcompat_filter_sort_tab_view_bg_color_light:I = 0x7f0604ef

.field public static final miuix_appcompat_filter_sort_tab_view_bg_dark:I = 0x7f0604f0

.field public static final miuix_appcompat_filter_sort_tab_view_bg_light:I = 0x7f0604f1

.field public static final miuix_appcompat_filter_sort_tab_view_text_dark:I = 0x7f0604f2

.field public static final miuix_appcompat_filter_sort_tab_view_text_disable_dark:I = 0x7f0604f3

.field public static final miuix_appcompat_filter_sort_tab_view_text_disable_light:I = 0x7f0604f4

.field public static final miuix_appcompat_filter_sort_tab_view_text_light:I = 0x7f0604f5

.field public static final miuix_appcompat_filter_sort_tab_view_text_normal:I = 0x7f0604f6

.field public static final miuix_appcompat_filter_sort_tab_view_text_normal_dark:I = 0x7f0604f7

.field public static final miuix_appcompat_filter_sort_tab_view_text_normal_light:I = 0x7f0604f8

.field public static final miuix_appcompat_filter_sort_tab_view_text_pressed_dark:I = 0x7f0604f9

.field public static final miuix_appcompat_filter_sort_tab_view_text_pressed_light:I = 0x7f0604fa

.field public static final miuix_appcompat_filter_sort_tab_view_text_selected:I = 0x7f0604fb

.field public static final miuix_appcompat_filter_sort_view_bg_color_dark:I = 0x7f0604fc

.field public static final miuix_appcompat_filter_sort_view_bg_color_light:I = 0x7f0604fd

.field public static final miuix_appcompat_floating_action_bar_search_mask_color_dark:I = 0x7f0604fe

.field public static final miuix_appcompat_floating_action_bar_search_mask_color_light:I = 0x7f0604ff

.field public static final miuix_appcompat_floating_action_bar_split_bg_color_dark:I = 0x7f060500

.field public static final miuix_appcompat_floating_action_bar_split_bg_color_light:I = 0x7f060501

.field public static final miuix_appcompat_floating_action_bar_split_bg_expanded_color_dark:I = 0x7f060502

.field public static final miuix_appcompat_floating_action_bar_split_bg_expanded_color_light:I = 0x7f060503

.field public static final miuix_appcompat_floating_action_mode_bg_color_dark:I = 0x7f060504

.field public static final miuix_appcompat_floating_action_mode_bg_color_light:I = 0x7f060505

.field public static final miuix_appcompat_floating_list_menu_bg_color_dark:I = 0x7f060506

.field public static final miuix_appcompat_floating_list_menu_bg_color_light:I = 0x7f060507

.field public static final miuix_appcompat_floating_primary_color_dark:I = 0x7f060508

.field public static final miuix_appcompat_floating_primary_color_light:I = 0x7f060509

.field public static final miuix_appcompat_floating_window_bg_color_dark:I = 0x7f06050a

.field public static final miuix_appcompat_floating_window_bg_color_light:I = 0x7f06050b

.field public static final miuix_appcompat_floating_window_border_color_dark:I = 0x7f06050c

.field public static final miuix_appcompat_floating_window_border_color_light:I = 0x7f06050d

.field public static final miuix_appcompat_guide_popup_background_dark:I = 0x7f06050e

.field public static final miuix_appcompat_guide_popup_background_light:I = 0x7f06050f

.field public static final miuix_appcompat_guide_popup_item_text_dark:I = 0x7f060510

.field public static final miuix_appcompat_guide_popup_item_text_light:I = 0x7f060511

.field public static final miuix_appcompat_guide_popup_paint_dark:I = 0x7f060512

.field public static final miuix_appcompat_guide_popup_paint_light:I = 0x7f060513

.field public static final miuix_appcompat_guide_popup_text_dark:I = 0x7f060514

.field public static final miuix_appcompat_guide_popup_text_light:I = 0x7f060515

.field public static final miuix_appcompat_handle_and_cursor_color:I = 0x7f060516

.field public static final miuix_appcompat_highlight_disabled_light:I = 0x7f060517

.field public static final miuix_appcompat_highlight_normal_dark:I = 0x7f060518

.field public static final miuix_appcompat_highlight_normal_light:I = 0x7f060519

.field public static final miuix_appcompat_highlight_pressed_light:I = 0x7f06051a

.field public static final miuix_appcompat_highlighted_text_dark:I = 0x7f06051b

.field public static final miuix_appcompat_highlighted_text_light:I = 0x7f06051c

.field public static final miuix_appcompat_hint_edit_text_color_dark:I = 0x7f06051d

.field public static final miuix_appcompat_hint_edit_text_color_light:I = 0x7f06051e

.field public static final miuix_appcompat_icon_foreground_color_dark:I = 0x7f06051f

.field public static final miuix_appcompat_icon_foreground_color_light:I = 0x7f060520

.field public static final miuix_appcompat_immersion_text_color_disable_dark:I = 0x7f060521

.field public static final miuix_appcompat_immersion_text_color_disable_light:I = 0x7f060522

.field public static final miuix_appcompat_immersion_text_color_normal_dark:I = 0x7f060523

.field public static final miuix_appcompat_immersion_text_color_normal_light:I = 0x7f060524

.field public static final miuix_appcompat_immersion_text_color_pressed_dark:I = 0x7f060525

.field public static final miuix_appcompat_immersion_text_color_pressed_light:I = 0x7f060526

.field public static final miuix_appcompat_immersion_text_dark:I = 0x7f060527

.field public static final miuix_appcompat_immersion_text_light:I = 0x7f060528

.field public static final miuix_appcompat_integrated_spinner_text_color_dark:I = 0x7f060529

.field public static final miuix_appcompat_integrated_spinner_text_color_disable_dark:I = 0x7f06052a

.field public static final miuix_appcompat_integrated_spinner_text_color_disable_light:I = 0x7f06052b

.field public static final miuix_appcompat_integrated_spinner_text_color_light:I = 0x7f06052c

.field public static final miuix_appcompat_integrated_spinner_text_color_normal_dark:I = 0x7f06052d

.field public static final miuix_appcompat_integrated_spinner_text_color_normal_light:I = 0x7f06052e

.field public static final miuix_appcompat_link_text_dark:I = 0x7f06052f

.field public static final miuix_appcompat_link_text_light:I = 0x7f060530

.field public static final miuix_appcompat_list_item_bg_color_checked_dark:I = 0x7f060531

.field public static final miuix_appcompat_list_item_bg_color_checked_light:I = 0x7f060532

.field public static final miuix_appcompat_list_item_bg_color_dark:I = 0x7f060533

.field public static final miuix_appcompat_list_item_bg_color_light:I = 0x7f060534

.field public static final miuix_appcompat_list_item_bg_color_pressed_dark:I = 0x7f060535

.field public static final miuix_appcompat_list_item_bg_color_pressed_light:I = 0x7f060536

.field public static final miuix_appcompat_list_item_bg_dialog_color_checked_dark:I = 0x7f060537

.field public static final miuix_appcompat_list_item_bg_dialog_color_checked_light:I = 0x7f060538

.field public static final miuix_appcompat_list_item_bg_dialog_color_dark:I = 0x7f060539

.field public static final miuix_appcompat_list_item_bg_dialog_color_light:I = 0x7f06053a

.field public static final miuix_appcompat_list_item_bg_dialog_color_normal_dark:I = 0x7f06053b

.field public static final miuix_appcompat_list_item_bg_dialog_color_normal_light:I = 0x7f06053c

.field public static final miuix_appcompat_list_item_bg_multichoice_color_checked_dark:I = 0x7f06053d

.field public static final miuix_appcompat_list_item_bg_multichoice_color_checked_light:I = 0x7f06053e

.field public static final miuix_appcompat_list_item_bg_multichoice_color_dark:I = 0x7f06053f

.field public static final miuix_appcompat_list_item_bg_multichoice_color_light:I = 0x7f060540

.field public static final miuix_appcompat_list_item_bg_multichoice_color_normal_dark:I = 0x7f060541

.field public static final miuix_appcompat_list_item_bg_multichoice_color_normal_light:I = 0x7f060542

.field public static final miuix_appcompat_list_item_floating_bg_color_dark:I = 0x7f060543

.field public static final miuix_appcompat_list_item_floating_bg_color_light:I = 0x7f060544

.field public static final miuix_appcompat_list_item_mask_floating_bg_color_dark:I = 0x7f060545

.field public static final miuix_appcompat_list_item_mask_floating_bg_color_light:I = 0x7f060546

.field public static final miuix_appcompat_list_menu_bg_color_dark:I = 0x7f060547

.field public static final miuix_appcompat_list_menu_bg_color_light:I = 0x7f060548

.field public static final miuix_appcompat_list_secondary_text_color_checked_dark:I = 0x7f060549

.field public static final miuix_appcompat_list_secondary_text_color_checked_light:I = 0x7f06054a

.field public static final miuix_appcompat_list_secondary_text_color_disable_dark:I = 0x7f06054b

.field public static final miuix_appcompat_list_secondary_text_color_disable_light:I = 0x7f06054c

.field public static final miuix_appcompat_list_secondary_text_color_normal_dark:I = 0x7f06054d

.field public static final miuix_appcompat_list_secondary_text_color_normal_light:I = 0x7f06054e

.field public static final miuix_appcompat_list_secondary_text_color_pressed_dark:I = 0x7f06054f

.field public static final miuix_appcompat_list_secondary_text_color_pressed_light:I = 0x7f060550

.field public static final miuix_appcompat_list_secondary_text_dark:I = 0x7f060551

.field public static final miuix_appcompat_list_secondary_text_light:I = 0x7f060552

.field public static final miuix_appcompat_list_text_color_checked_dark:I = 0x7f060553

.field public static final miuix_appcompat_list_text_color_checked_light:I = 0x7f060554

.field public static final miuix_appcompat_list_text_color_disable_dark:I = 0x7f060555

.field public static final miuix_appcompat_list_text_color_disable_light:I = 0x7f060556

.field public static final miuix_appcompat_list_text_color_normal_dark:I = 0x7f060557

.field public static final miuix_appcompat_list_text_color_normal_light:I = 0x7f060558

.field public static final miuix_appcompat_list_text_color_pressed_dark:I = 0x7f060559

.field public static final miuix_appcompat_list_text_color_pressed_light:I = 0x7f06055a

.field public static final miuix_appcompat_list_text_dark:I = 0x7f06055b

.field public static final miuix_appcompat_list_text_dark_dialog_single_choice:I = 0x7f06055c

.field public static final miuix_appcompat_list_text_light:I = 0x7f06055d

.field public static final miuix_appcompat_list_text_light_dialog_single_choice:I = 0x7f06055e

.field public static final miuix_appcompat_list_view_item_group_header_text_dark:I = 0x7f06055f

.field public static final miuix_appcompat_list_view_item_group_header_text_light:I = 0x7f060560

.field public static final miuix_appcompat_menu_list_text_color_disabled_dark:I = 0x7f060561

.field public static final miuix_appcompat_menu_list_text_color_disabled_light:I = 0x7f060562

.field public static final miuix_appcompat_menu_list_text_color_normal_dark:I = 0x7f060563

.field public static final miuix_appcompat_menu_list_text_color_normal_light:I = 0x7f060564

.field public static final miuix_appcompat_menu_list_text_color_pressed_dark:I = 0x7f060565

.field public static final miuix_appcompat_menu_list_text_color_pressed_light:I = 0x7f060566

.field public static final miuix_appcompat_menu_list_text_dark:I = 0x7f060567

.field public static final miuix_appcompat_menu_list_text_light:I = 0x7f060568

.field public static final miuix_appcompat_message_view_error_bg_color_dark:I = 0x7f060569

.field public static final miuix_appcompat_message_view_error_bg_color_light:I = 0x7f06056a

.field public static final miuix_appcompat_message_view_guide_bg_color_dark:I = 0x7f06056b

.field public static final miuix_appcompat_message_view_guide_bg_color_light:I = 0x7f06056c

.field public static final miuix_appcompat_message_view_text_color_error_light:I = 0x7f06056d

.field public static final miuix_appcompat_message_view_text_color_light:I = 0x7f06056e

.field public static final miuix_appcompat_message_view_text_color_warning_light:I = 0x7f06056f

.field public static final miuix_appcompat_message_view_warning_bg_color_dark:I = 0x7f060570

.field public static final miuix_appcompat_message_view_warning_bg_color_light:I = 0x7f060571

.field public static final miuix_appcompat_normal_text_dark:I = 0x7f060572

.field public static final miuix_appcompat_normal_text_light:I = 0x7f060573

.field public static final miuix_appcompat_number_picker_highlight_region_bg_color:I = 0x7f060574

.field public static final miuix_appcompat_number_picker_highlight_region_bg_color_dark:I = 0x7f060575

.field public static final miuix_appcompat_number_picker_label_color:I = 0x7f060576

.field public static final miuix_appcompat_number_picker_label_color_dark:I = 0x7f060577

.field public static final miuix_appcompat_pressed_text_dark:I = 0x7f060578

.field public static final miuix_appcompat_pressed_text_light:I = 0x7f060579

.field public static final miuix_appcompat_primary_color_dark:I = 0x7f06057a

.field public static final miuix_appcompat_primary_color_light:I = 0x7f06057b

.field public static final miuix_appcompat_progressBackground_dark:I = 0x7f06057c

.field public static final miuix_appcompat_progressBackground_light:I = 0x7f06057d

.field public static final miuix_appcompat_progress_background_dark:I = 0x7f06057e

.field public static final miuix_appcompat_progress_background_icon_dark:I = 0x7f06057f

.field public static final miuix_appcompat_progress_background_icon_light:I = 0x7f060580

.field public static final miuix_appcompat_progress_background_light:I = 0x7f060581

.field public static final miuix_appcompat_progress_disable_color_dark:I = 0x7f060582

.field public static final miuix_appcompat_progress_disable_color_light:I = 0x7f060583

.field public static final miuix_appcompat_progress_primary_color_dark:I = 0x7f060584

.field public static final miuix_appcompat_progress_primary_color_light:I = 0x7f060585

.field public static final miuix_appcompat_progress_primary_colors_dark:I = 0x7f060586

.field public static final miuix_appcompat_progress_primary_colors_light:I = 0x7f060587

.field public static final miuix_appcompat_progressbar_circle_color_dark:I = 0x7f060588

.field public static final miuix_appcompat_progressbar_circle_color_light:I = 0x7f060589

.field public static final miuix_appcompat_progressbar_color_dark:I = 0x7f06058a

.field public static final miuix_appcompat_progressbar_color_light:I = 0x7f06058b

.field public static final miuix_appcompat_progressbar_text_color_dark:I = 0x7f06058c

.field public static final miuix_appcompat_progressbar_text_color_light:I = 0x7f06058d

.field public static final miuix_appcompat_scrollbar_thumb_vertical_bg_dark:I = 0x7f06058e

.field public static final miuix_appcompat_scrollbar_thumb_vertical_bg_light:I = 0x7f06058f

.field public static final miuix_appcompat_search_action_mode_cancel_text_color:I = 0x7f060590

.field public static final miuix_appcompat_search_action_mode_cancel_text_color_normal:I = 0x7f060591

.field public static final miuix_appcompat_search_action_mode_cancel_text_color_pressed:I = 0x7f060592

.field public static final miuix_appcompat_search_blurview_fg_dark:I = 0x7f060593

.field public static final miuix_appcompat_search_blurview_fg_light:I = 0x7f060594

.field public static final miuix_appcompat_secondary_text_dark:I = 0x7f060595

.field public static final miuix_appcompat_secondary_text_light:I = 0x7f060596

.field public static final miuix_appcompat_seekbar_progress_color:I = 0x7f060597

.field public static final miuix_appcompat_selected_text_dark:I = 0x7f060598

.field public static final miuix_appcompat_selected_text_light:I = 0x7f060599

.field public static final miuix_appcompat_sliding_btn_slider_off_normal_color_light:I = 0x7f06059a

.field public static final miuix_appcompat_sliding_btn_slider_off_pressed_color_light:I = 0x7f06059b

.field public static final miuix_appcompat_sliding_btn_slider_on_normal_color_light:I = 0x7f06059c

.field public static final miuix_appcompat_sliding_btn_slider_on_pressed_color_light:I = 0x7f06059d

.field public static final miuix_appcompat_sliding_button_bar_off_dark:I = 0x7f06059e

.field public static final miuix_appcompat_sliding_button_bar_off_light:I = 0x7f06059f

.field public static final miuix_appcompat_sliding_button_bar_on_dark:I = 0x7f0605a0

.field public static final miuix_appcompat_sliding_button_bar_on_light:I = 0x7f0605a1

.field public static final miuix_appcompat_sliding_button_bg_border_color:I = 0x7f0605a2

.field public static final miuix_appcompat_spinner_arrow_color_dark:I = 0x7f0605a3

.field public static final miuix_appcompat_spinner_arrow_color_light:I = 0x7f0605a4

.field public static final miuix_appcompat_spinner_dropdown_selector_text_color:I = 0x7f0605a5

.field public static final miuix_appcompat_spinner_dropdown_selector_text_color_dark:I = 0x7f0605a6

.field public static final miuix_appcompat_spinner_edit_item_label:I = 0x7f0605a7

.field public static final miuix_appcompat_spinner_edit_item_label_normal:I = 0x7f0605a8

.field public static final miuix_appcompat_spinner_edit_item_label_pressed:I = 0x7f0605a9

.field public static final miuix_appcompat_spinner_item_checked_bg_color_dark:I = 0x7f0605aa

.field public static final miuix_appcompat_spinner_item_checked_bg_color_light:I = 0x7f0605ab

.field public static final miuix_appcompat_spinner_item_normal_bg_color_dark:I = 0x7f0605ac

.field public static final miuix_appcompat_spinner_item_normal_bg_color_light:I = 0x7f0605ad

.field public static final miuix_appcompat_spinner_popup_checkable_item_bg_color_dark:I = 0x7f0605ae

.field public static final miuix_appcompat_spinner_popup_checkable_item_bg_color_light:I = 0x7f0605af

.field public static final miuix_appcompat_spinner_text_color_dark:I = 0x7f0605b0

.field public static final miuix_appcompat_spinner_text_color_light:I = 0x7f0605b1

.field public static final miuix_appcompat_svg_icon_color_blue_disable_dark:I = 0x7f0605b2

.field public static final miuix_appcompat_svg_icon_color_blue_disable_light:I = 0x7f0605b3

.field public static final miuix_appcompat_svg_icon_color_blue_normal_dark:I = 0x7f0605b4

.field public static final miuix_appcompat_svg_icon_color_blue_normal_light:I = 0x7f0605b5

.field public static final miuix_appcompat_svg_icon_color_blue_selected_dark:I = 0x7f0605b6

.field public static final miuix_appcompat_svg_icon_color_blue_selected_light:I = 0x7f0605b7

.field public static final miuix_appcompat_svg_icon_color_disable_dark:I = 0x7f0605b8

.field public static final miuix_appcompat_svg_icon_color_disable_light:I = 0x7f0605b9

.field public static final miuix_appcompat_svg_icon_color_normal_dark:I = 0x7f0605ba

.field public static final miuix_appcompat_svg_icon_color_normal_light:I = 0x7f0605bb

.field public static final miuix_appcompat_svg_icon_color_pressed_dark:I = 0x7f0605bc

.field public static final miuix_appcompat_svg_icon_color_pressed_light:I = 0x7f0605bd

.field public static final miuix_appcompat_svg_icon_color_red_disable_dark:I = 0x7f0605be

.field public static final miuix_appcompat_svg_icon_color_red_disable_light:I = 0x7f0605bf

.field public static final miuix_appcompat_svg_icon_color_red_normal_dark:I = 0x7f0605c0

.field public static final miuix_appcompat_svg_icon_color_red_normal_light:I = 0x7f0605c1

.field public static final miuix_appcompat_text_color_primary_dark:I = 0x7f0605c2

.field public static final miuix_appcompat_text_color_primary_light:I = 0x7f0605c3

.field public static final miuix_appcompat_transparent:I = 0x7f0605c4

.field public static final miuix_appcompat_water_box_bg_color:I = 0x7f0605c5

.field public static final miuix_appcompat_water_box_water_color:I = 0x7f0605c6

.field public static final miuix_appcompat_white:I = 0x7f0605c7

.field public static final miuix_blurdrawable_view_fg_dark:I = 0x7f0605c8

.field public static final miuix_blurdrawable_view_fg_light:I = 0x7f0605c9

.field public static final miuix_color_black_40:I = 0x7f0605ca

.field public static final miuix_color_black_40_no_alpha:I = 0x7f0605cb

.field public static final miuix_color_white_40:I = 0x7f0605cc

.field public static final miuix_color_white_40_no_alpha:I = 0x7f0605cd

.field public static final miuix_color_white_50:I = 0x7f0605ce

.field public static final miuix_color_white_50_no_alpha:I = 0x7f0605cf

.field public static final miuix_folme_color_blink_tint:I = 0x7f0605d0

.field public static final miuix_folme_color_touch_tint:I = 0x7f0605d1

.field public static final miuix_folme_color_touch_tint_dark:I = 0x7f0605d2

.field public static final miuix_folme_color_touch_tint_dark_p1:I = 0x7f0605d3

.field public static final miuix_folme_color_touch_tint_dark_p2:I = 0x7f0605d4

.field public static final miuix_folme_color_touch_tint_dark_p3:I = 0x7f0605d5

.field public static final miuix_folme_color_touch_tint_light:I = 0x7f0605d6

.field public static final miuix_folme_color_touch_tint_light_p1:I = 0x7f0605d7

.field public static final miuix_folme_color_touch_tint_light_p2:I = 0x7f0605d8

.field public static final miuix_folme_color_touch_tint_light_p3:I = 0x7f0605d9

.field public static final miuix_picker_datetime_text_color:I = 0x7f0605da

.field public static final miuix_picker_view_color:I = 0x7f0605db

.field public static final miuix_preference_arrow_right_disable_stroke_color:I = 0x7f0605dc

.field public static final miuix_preference_arrow_right_normal_stroke_color:I = 0x7f0605dd

.field public static final miuix_preference_arrow_right_pressed_stroke_color:I = 0x7f0605de

.field public static final miuix_preference_arrow_right_stroke_color:I = 0x7f0605df

.field public static final miuix_preference_btn_radio_arrow_color:I = 0x7f0605e0

.field public static final miuix_preference_category_text_color_dark:I = 0x7f0605e1

.field public static final miuix_preference_category_text_color_light:I = 0x7f0605e2

.field public static final miuix_preference_checkable_item_fill_color_checked_dark:I = 0x7f0605e3

.field public static final miuix_preference_checkable_item_fill_color_checked_light:I = 0x7f0605e4

.field public static final miuix_preference_checkable_item_fill_color_normal_dark:I = 0x7f0605e5

.field public static final miuix_preference_checkable_item_fill_color_normal_light:I = 0x7f0605e6

.field public static final miuix_preference_checkable_item_two_state_color_pressed_checked_dark:I = 0x7f0605e7

.field public static final miuix_preference_checkable_item_two_state_color_pressed_checked_light:I = 0x7f0605e8

.field public static final miuix_preference_checkable_item_two_state_color_pressed_dark:I = 0x7f0605e9

.field public static final miuix_preference_checkable_item_two_state_color_pressed_light:I = 0x7f0605ea

.field public static final miuix_preference_connect_bg_connected_color:I = 0x7f0605eb

.field public static final miuix_preference_connect_bg_connected_color_dark:I = 0x7f0605ec

.field public static final miuix_preference_connect_bg_connected_color_light:I = 0x7f0605ed

.field public static final miuix_preference_connect_bg_disconnected_color:I = 0x7f0605ee

.field public static final miuix_preference_connect_bg_disconnected_color_dark:I = 0x7f0605ef

.field public static final miuix_preference_connect_bg_disconnected_color_light:I = 0x7f0605f0

.field public static final miuix_preference_connect_icon_color:I = 0x7f0605f1

.field public static final miuix_preference_connect_icon_connected_color:I = 0x7f0605f2

.field public static final miuix_preference_connect_icon_disconnected_color:I = 0x7f0605f3

.field public static final miuix_preference_connect_icon_disconnected_color_dark:I = 0x7f0605f4

.field public static final miuix_preference_connect_icon_disconnected_color_light:I = 0x7f0605f5

.field public static final miuix_preference_connect_summary_color:I = 0x7f0605f6

.field public static final miuix_preference_connect_summary_connected_color:I = 0x7f0605f7

.field public static final miuix_preference_connect_summary_disconnected_color:I = 0x7f0605f8

.field public static final miuix_preference_connect_summary_disconnected_color_dark:I = 0x7f0605f9

.field public static final miuix_preference_connect_summary_disconnected_color_light:I = 0x7f0605fa

.field public static final miuix_preference_connect_title_color:I = 0x7f0605fb

.field public static final miuix_preference_connect_title_connected_color:I = 0x7f0605fc

.field public static final miuix_preference_connect_title_disconnected_color:I = 0x7f0605fd

.field public static final miuix_preference_connect_title_disconnected_color_dark:I = 0x7f0605fe

.field public static final miuix_preference_connect_title_disconnected_color_light:I = 0x7f0605ff

.field public static final miuix_preference_first_last_divider_line_dark:I = 0x7f060600

.field public static final miuix_preference_first_last_divider_line_light:I = 0x7f060601

.field public static final miuix_preference_list_item_bg_color_dark:I = 0x7f060602

.field public static final miuix_preference_list_item_bg_color_light:I = 0x7f060603

.field public static final miuix_preference_list_item_bg_color_selected:I = 0x7f060604

.field public static final miuix_preference_list_item_two_state_bg_color:I = 0x7f060605

.field public static final miuix_preference_list_item_two_state_bg_color_pressed:I = 0x7f060606

.field public static final miuix_preference_primary_text_color_dark:I = 0x7f060607

.field public static final miuix_preference_primary_text_color_disable_dark:I = 0x7f060608

.field public static final miuix_preference_primary_text_color_disable_light:I = 0x7f060609

.field public static final miuix_preference_primary_text_color_light:I = 0x7f06060a

.field public static final miuix_preference_primary_text_dark:I = 0x7f06060b

.field public static final miuix_preference_primary_text_light:I = 0x7f06060c

.field public static final miuix_preference_right_text_color_dark:I = 0x7f06060d

.field public static final miuix_preference_right_text_color_disable_dark:I = 0x7f06060e

.field public static final miuix_preference_right_text_color_disable_light:I = 0x7f06060f

.field public static final miuix_preference_right_text_color_light:I = 0x7f060610

.field public static final miuix_preference_right_text_color_normal_dark:I = 0x7f060611

.field public static final miuix_preference_right_text_color_normal_light:I = 0x7f060612

.field public static final miuix_preference_secondary_text_color_checked_dark:I = 0x7f060613

.field public static final miuix_preference_secondary_text_color_checked_light:I = 0x7f060614

.field public static final miuix_preference_secondary_text_color_dark:I = 0x7f060615

.field public static final miuix_preference_secondary_text_color_disable_dark:I = 0x7f060616

.field public static final miuix_preference_secondary_text_color_disable_light:I = 0x7f060617

.field public static final miuix_preference_secondary_text_color_light:I = 0x7f060618

.field public static final miuix_preference_secondary_text_dark:I = 0x7f060619

.field public static final miuix_preference_secondary_text_light:I = 0x7f06061a

.field public static final miuix_preference_selectable_item_fill_color_selected_dark:I = 0x7f06061b

.field public static final miuix_preference_selectable_item_fill_color_selected_light:I = 0x7f06061c

.field public static final miuix_sbl_black:I = 0x7f06061d

.field public static final miuix_sbl_loading_light:I = 0x7f06061e

.field public static final miuix_sbl_locked_blue:I = 0x7f06061f

.field public static final miuix_sbl_locked_gray:I = 0x7f060620

.field public static final miuix_sbl_locked_text_blue:I = 0x7f060621

.field public static final miuix_sbl_locked_text_gray:I = 0x7f060622

.field public static final miuix_sbl_transparent:I = 0x7f060623

.field public static final miuix_sbl_white:I = 0x7f060624

.field public static final miuix_state_Image_color_dark:I = 0x7f060625

.field public static final miuix_state_Image_color_light:I = 0x7f060626

.field public static final miuix_usb_dialog_bg_color:I = 0x7f060627

.field public static final miuix_usb_dialog_text_color:I = 0x7f060628

.field public static final mr_cast_meta_black_scrim:I = 0x7f060629

.field public static final mr_cast_meta_default_background:I = 0x7f06062a

.field public static final mr_cast_meta_default_text_color:I = 0x7f06062b

.field public static final mr_cast_progressbar_background_dark:I = 0x7f06062c

.field public static final mr_cast_progressbar_background_light:I = 0x7f06062d

.field public static final mr_cast_progressbar_progress_and_thumb_dark:I = 0x7f06062e

.field public static final mr_cast_progressbar_progress_and_thumb_light:I = 0x7f06062f

.field public static final mr_cast_route_divider_dark:I = 0x7f060630

.field public static final mr_cast_route_divider_light:I = 0x7f060631

.field public static final mr_dynamic_dialog_background_dark:I = 0x7f060632

.field public static final mr_dynamic_dialog_background_light:I = 0x7f060633

.field public static final mr_dynamic_dialog_header_text_color_dark:I = 0x7f060634

.field public static final mr_dynamic_dialog_header_text_color_light:I = 0x7f060635

.field public static final mr_dynamic_dialog_icon_dark:I = 0x7f060636

.field public static final mr_dynamic_dialog_icon_light:I = 0x7f060637

.field public static final mr_dynamic_dialog_route_text_color_dark:I = 0x7f060638

.field public static final mr_dynamic_dialog_route_text_color_light:I = 0x7f060639

.field public static final mtrl_btn_bg_color_selector:I = 0x7f06063a

.field public static final mtrl_btn_ripple_color:I = 0x7f06063b

.field public static final mtrl_btn_stroke_color_selector:I = 0x7f06063c

.field public static final mtrl_btn_text_btn_bg_color_selector:I = 0x7f06063d

.field public static final mtrl_btn_text_btn_ripple_color:I = 0x7f06063e

.field public static final mtrl_btn_text_color_disabled:I = 0x7f06063f

.field public static final mtrl_btn_text_color_selector:I = 0x7f060640

.field public static final mtrl_btn_transparent_bg_color:I = 0x7f060641

.field public static final mtrl_calendar_item_stroke_color:I = 0x7f060642

.field public static final mtrl_calendar_selected_range:I = 0x7f060643

.field public static final mtrl_card_view_foreground:I = 0x7f060644

.field public static final mtrl_card_view_ripple:I = 0x7f060645

.field public static final mtrl_chip_background_color:I = 0x7f060646

.field public static final mtrl_chip_close_icon_tint:I = 0x7f060647

.field public static final mtrl_chip_surface_color:I = 0x7f060648

.field public static final mtrl_chip_text_color:I = 0x7f060649

.field public static final mtrl_choice_chip_background_color:I = 0x7f06064a

.field public static final mtrl_choice_chip_ripple_color:I = 0x7f06064b

.field public static final mtrl_choice_chip_text_color:I = 0x7f06064c

.field public static final mtrl_error:I = 0x7f06064d

.field public static final mtrl_fab_bg_color_selector:I = 0x7f06064e

.field public static final mtrl_fab_icon_text_color_selector:I = 0x7f06064f

.field public static final mtrl_fab_ripple_color:I = 0x7f060650

.field public static final mtrl_filled_background_color:I = 0x7f060651

.field public static final mtrl_filled_icon_tint:I = 0x7f060652

.field public static final mtrl_filled_stroke_color:I = 0x7f060653

.field public static final mtrl_indicator_text_color:I = 0x7f060654

.field public static final mtrl_navigation_bar_colored_item_tint:I = 0x7f060655

.field public static final mtrl_navigation_bar_colored_ripple_color:I = 0x7f060656

.field public static final mtrl_navigation_bar_item_tint:I = 0x7f060657

.field public static final mtrl_navigation_bar_ripple_color:I = 0x7f060658

.field public static final mtrl_navigation_item_background_color:I = 0x7f060659

.field public static final mtrl_navigation_item_icon_tint:I = 0x7f06065a

.field public static final mtrl_navigation_item_text_color:I = 0x7f06065b

.field public static final mtrl_on_primary_text_btn_text_color_selector:I = 0x7f06065c

.field public static final mtrl_on_surface_ripple_color:I = 0x7f06065d

.field public static final mtrl_outlined_icon_tint:I = 0x7f06065e

.field public static final mtrl_outlined_stroke_color:I = 0x7f06065f

.field public static final mtrl_popupmenu_overlay_color:I = 0x7f060660

.field public static final mtrl_scrim_color:I = 0x7f060661

.field public static final mtrl_tabs_colored_ripple_color:I = 0x7f060662

.field public static final mtrl_tabs_icon_color_selector:I = 0x7f060663

.field public static final mtrl_tabs_icon_color_selector_colored:I = 0x7f060664

.field public static final mtrl_tabs_legacy_text_color_selector:I = 0x7f060665

.field public static final mtrl_tabs_ripple_color:I = 0x7f060666

.field public static final mtrl_text_btn_text_color_selector:I = 0x7f060667

.field public static final mtrl_textinput_default_box_stroke_color:I = 0x7f060668

.field public static final mtrl_textinput_disabled_color:I = 0x7f060669

.field public static final mtrl_textinput_filled_box_default_background_color:I = 0x7f06066a

.field public static final mtrl_textinput_focused_box_stroke_color:I = 0x7f06066b

.field public static final mtrl_textinput_hovered_box_stroke_color:I = 0x7f06066c

.field public static final my_device_bold_color:I = 0x7f06066d

.field public static final need_update_text_color:I = 0x7f06066e

.field public static final new_version_button_color:I = 0x7f06066f

.field public static final new_version_text_color:I = 0x7f060670

.field public static final no_action_bg:I = 0x7f060671

.field public static final no_devices:I = 0x7f060672

.field public static final normal_list_bg:I = 0x7f060673

.field public static final normal_summary_color:I = 0x7f060674

.field public static final notch_radio_button_text:I = 0x7f060675

.field public static final notch_radio_button_text_checked:I = 0x7f060676

.field public static final notification_action_color_filter:I = 0x7f060677

.field public static final notification_alert_color:I = 0x7f060678

.field public static final notification_block_color:I = 0x7f060679

.field public static final notification_history_background:I = 0x7f06067a

.field public static final notification_icon_bg_color:I = 0x7f06067b

.field public static final notification_importance_button_selected:I = 0x7f06067c

.field public static final notification_importance_button_unselected:I = 0x7f06067d

.field public static final notification_importance_selection_bg:I = 0x7f06067e

.field public static final notification_material_background_media_default_color:I = 0x7f06067f

.field public static final notification_silence_color:I = 0x7f060680

.field public static final oaid_anonymous_icon_color:I = 0x7f060681

.field public static final orange:I = 0x7f060682

.field public static final pad_internal_line_color:I = 0x7f060683

.field public static final palette_list_color_blue:I = 0x7f060684

.field public static final palette_list_color_cyan:I = 0x7f060685

.field public static final palette_list_color_gray:I = 0x7f060686

.field public static final palette_list_color_green:I = 0x7f060687

.field public static final palette_list_color_orange:I = 0x7f060688

.field public static final palette_list_color_purple:I = 0x7f060689

.field public static final palette_list_color_red:I = 0x7f06068a

.field public static final palette_list_color_yellow:I = 0x7f06068b

.field public static final palette_list_dark_mode_color_blue:I = 0x7f06068c

.field public static final palette_list_dark_mode_color_cyan:I = 0x7f06068d

.field public static final palette_list_dark_mode_color_gray:I = 0x7f06068e

.field public static final palette_list_dark_mode_color_green:I = 0x7f06068f

.field public static final palette_list_dark_mode_color_orange:I = 0x7f060690

.field public static final palette_list_dark_mode_color_purple:I = 0x7f060691

.field public static final palette_list_dark_mode_color_red:I = 0x7f060692

.field public static final palette_list_dark_mode_color_yellow:I = 0x7f060693

.field public static final palette_list_gradient_background:I = 0x7f060694

.field public static final paper_mode_disable_tv_color:I = 0x7f060695

.field public static final paper_mode_hint_color:I = 0x7f060696

.field public static final passport_verify_dialog_def_bg_color:I = 0x7f060697

.field public static final password_confirm_bg_color:I = 0x7f060698

.field public static final password_confirm_fail_bg_color:I = 0x7f060699

.field public static final password_entry_divider_color:I = 0x7f06069a

.field public static final password_entry_solid_color:I = 0x7f06069b

.field public static final password_entry_stroke_color:I = 0x7f06069c

.field public static final permission_text_color:I = 0x7f06069d

.field public static final permission_trust:I = 0x7f06069e

.field public static final picker_bg:I = 0x7f06069f

.field public static final pinner_list_header:I = 0x7f0606a0

.field public static final preference_divide:I = 0x7f0606a1

.field public static final preference_fallback_accent_color:I = 0x7f0606a2

.field public static final preference_highlight_color:I = 0x7f0606a3

.field public static final preference_primary_text_color:I = 0x7f0606a4

.field public static final preference_primary_text_color_checked:I = 0x7f0606a5

.field public static final primary_dark_material_dark:I = 0x7f0606a6

.field public static final primary_dark_material_light:I = 0x7f0606a7

.field public static final primary_material_dark:I = 0x7f0606a8

.field public static final primary_material_light:I = 0x7f0606a9

.field public static final primary_text_default_material_dark:I = 0x7f0606aa

.field public static final primary_text_default_material_light:I = 0x7f0606ab

.field public static final primary_text_disabled_material_dark:I = 0x7f0606ac

.field public static final primary_text_disabled_material_light:I = 0x7f0606ad

.field public static final privacy_dialog_positive_button_disabled:I = 0x7f0606ae

.field public static final privacy_dialog_positive_button_enabled:I = 0x7f0606af

.field public static final privacy_hightlight_color_blue:I = 0x7f0606b0

.field public static final privacy_list_header_line:I = 0x7f0606b1

.field public static final privacy_list_header_summary:I = 0x7f0606b2

.field public static final privacy_list_header_title:I = 0x7f0606b3

.field public static final progress_card_view_background_color:I = 0x7f0606b4

.field public static final progress_paint_color:I = 0x7f0606b5

.field public static final prompt_text_color:I = 0x7f0606b6

.field public static final provision_btn_next:I = 0x7f0606b7

.field public static final provision_button_next_low_light:I = 0x7f0606b8

.field public static final provision_button_next_normal:I = 0x7f0606b9

.field public static final provision_button_next_pressed:I = 0x7f0606ba

.field public static final provision_button_text_disable:I = 0x7f0606bb

.field public static final provision_button_text_low_light:I = 0x7f0606bc

.field public static final provision_button_text_normal:I = 0x7f0606bd

.field public static final provision_button_text_pressed:I = 0x7f0606be

.field public static final provision_color_list_item_bg:I = 0x7f0606bf

.field public static final provision_input_title:I = 0x7f0606c0

.field public static final provision_line_background_color:I = 0x7f0606c1

.field public static final provision_list_item_text_light:I = 0x7f0606c2

.field public static final provision_other_ap_color_list_item_bg:I = 0x7f0606c3

.field public static final provision_spinner_text:I = 0x7f0606c4

.field public static final pvc_exit_account_info_text_color:I = 0x7f0606c5

.field public static final qr_background_color:I = 0x7f0606c6

.field public static final qr_code_background_color:I = 0x7f0606c7

.field public static final qr_code_corner_line_color:I = 0x7f0606c8

.field public static final qr_code_focused_corner_line_color:I = 0x7f0606c9

.field public static final qr_corner_line_color:I = 0x7f0606ca

.field public static final qr_focused_corner_line_color:I = 0x7f0606cb

.field public static final qr_scanner_border:I = 0x7f0606cc

.field public static final qr_scanner_corner:I = 0x7f0606cd

.field public static final qr_scanner_guide_line:I = 0x7f0606ce

.field public static final qrcode_other_color:I = 0x7f0606cf

.field public static final qrcode_pixel_color:I = 0x7f0606d0

.field public static final radiobutton_themeable_attribute_color:I = 0x7f0606d1

.field public static final raise_bottom_height_of_keyboard_sub_text_color:I = 0x7f0606d2

.field public static final raise_bottom_height_of_keyboard_text_color:I = 0x7f0606d3

.field public static final rc_decoration:I = 0x7f0606d4

.field public static final recommend_bg_color:I = 0x7f0606d5

.field public static final recommend_item_link:I = 0x7f0606d6

.field public static final recommend_item_link_pressed:I = 0x7f0606d7

.field public static final recommend_region_color:I = 0x7f0606d8

.field public static final recommend_title_color:I = 0x7f0606d9

.field public static final red:I = 0x7f0606da

.field public static final relative_medium_aquamarin:I = 0x7f0606db

.field public static final rename_back:I = 0x7f0606dc

.field public static final reset_hint_content:I = 0x7f0606dd

.field public static final reset_paper_color:I = 0x7f0606de

.field public static final restricted_lock_item_select_bg:I = 0x7f0606df

.field public static final restricted_screen_timeout_subtitle_textcolor:I = 0x7f0606e0

.field public static final restricted_tip_area_color:I = 0x7f0606e1

.field public static final ringtone_item_region_color:I = 0x7f0606e2

.field public static final ringtone_item_title_color:I = 0x7f0606e3

.field public static final ringtone_item_value_color:I = 0x7f0606e4

.field public static final ripple_material_dark:I = 0x7f0606e5

.field public static final ripple_material_inverse:I = 0x7f0606e6

.field public static final ripple_material_light:I = 0x7f0606e7

.field public static final running_processes_free_ram:I = 0x7f0606e8

.field public static final running_processes_system_ram:I = 0x7f0606e9

.field public static final screen_effect_btn_checked:I = 0x7f0606ea

.field public static final screen_effect_btn_stroke_checked:I = 0x7f0606eb

.field public static final screen_effect_btn_stroke_unchecked:I = 0x7f0606ec

.field public static final screen_effect_btn_unchecked:I = 0x7f0606ed

.field public static final screen_effect_darkmode_btn_checked:I = 0x7f0606ee

.field public static final screen_effect_darkmode_btn_unchecked:I = 0x7f0606ef

.field public static final search_bar_background:I = 0x7f0606f0

.field public static final search_locale_highlight_text:I = 0x7f0606f1

.field public static final second_space_setting_bg:I = 0x7f0606f2

.field public static final second_text_color:I = 0x7f0606f3

.field public static final secondary_text_default_material_dark:I = 0x7f0606f4

.field public static final secondary_text_default_material_light:I = 0x7f0606f5

.field public static final secondary_text_disabled_material_dark:I = 0x7f0606f6

.field public static final secondary_text_disabled_material_light:I = 0x7f0606f7

.field public static final secspace_fod_finger_bg_color:I = 0x7f0606f8

.field public static final secspace_fod_finger_color:I = 0x7f0606f9

.field public static final security_lock_pattern_head_text:I = 0x7f0606fa

.field public static final security_lock_pattern_paint:I = 0x7f0606fb

.field public static final security_lock_pattern_wrong:I = 0x7f0606fc

.field public static final seekbar_color_bottom:I = 0x7f0606fd

.field public static final seekbar_color_top:I = 0x7f0606fe

.field public static final set_second_space_background:I = 0x7f0606ff

.field public static final settings_bar_view_1_color:I = 0x7f060700

.field public static final settings_bar_view_2_color:I = 0x7f060701

.field public static final settings_bar_view_3_color:I = 0x7f060702

.field public static final settings_bar_view_4_color:I = 0x7f060703

.field public static final settings_default_bg_color:I = 0x7f060704

.field public static final settings_dialog_colorError:I = 0x7f060705

.field public static final settings_notify_fg:I = 0x7f060706

.field public static final settings_two_pane_background_color:I = 0x7f060707

.field public static final settingslib_accent_device_default_dark:I = 0x7f060708

.field public static final settingslib_accent_device_default_light:I = 0x7f060709

.field public static final settingslib_accent_primary_device_default:I = 0x7f06070a

.field public static final settingslib_accent_primary_variant:I = 0x7f06070b

.field public static final settingslib_accent_secondary_device_default:I = 0x7f06070c

.field public static final settingslib_background_device_default_dark:I = 0x7f06070d

.field public static final settingslib_background_device_default_light:I = 0x7f06070e

.field public static final settingslib_btn_colored_background_material:I = 0x7f06070f

.field public static final settingslib_btn_colored_text_material:I = 0x7f060710

.field public static final settingslib_button_ripple:I = 0x7f060711

.field public static final settingslib_colorAccentPrimary:I = 0x7f060712

.field public static final settingslib_colorAccentSecondary:I = 0x7f060713

.field public static final settingslib_colorSurface:I = 0x7f060714

.field public static final settingslib_colorSurfaceHeader:I = 0x7f060715

.field public static final settingslib_colorSurfaceVariant:I = 0x7f060716

.field public static final settingslib_color_blue100:I = 0x7f060717

.field public static final settingslib_color_blue300:I = 0x7f060718

.field public static final settingslib_color_blue400:I = 0x7f060719

.field public static final settingslib_color_blue50:I = 0x7f06071a

.field public static final settingslib_color_blue600:I = 0x7f06071b

.field public static final settingslib_color_cyan100:I = 0x7f06071c

.field public static final settingslib_color_cyan300:I = 0x7f06071d

.field public static final settingslib_color_cyan400:I = 0x7f06071e

.field public static final settingslib_color_cyan600:I = 0x7f06071f

.field public static final settingslib_color_green100:I = 0x7f060720

.field public static final settingslib_color_green400:I = 0x7f060721

.field public static final settingslib_color_green50:I = 0x7f060722

.field public static final settingslib_color_green600:I = 0x7f060723

.field public static final settingslib_color_grey200:I = 0x7f060724

.field public static final settingslib_color_grey300:I = 0x7f060725

.field public static final settingslib_color_grey400:I = 0x7f060726

.field public static final settingslib_color_grey600:I = 0x7f060727

.field public static final settingslib_color_grey700:I = 0x7f060728

.field public static final settingslib_color_grey800:I = 0x7f060729

.field public static final settingslib_color_grey900:I = 0x7f06072a

.field public static final settingslib_color_orange100:I = 0x7f06072b

.field public static final settingslib_color_orange300:I = 0x7f06072c

.field public static final settingslib_color_orange400:I = 0x7f06072d

.field public static final settingslib_color_orange600:I = 0x7f06072e

.field public static final settingslib_color_pink100:I = 0x7f06072f

.field public static final settingslib_color_pink300:I = 0x7f060730

.field public static final settingslib_color_pink400:I = 0x7f060731

.field public static final settingslib_color_pink600:I = 0x7f060732

.field public static final settingslib_color_purple100:I = 0x7f060733

.field public static final settingslib_color_purple300:I = 0x7f060734

.field public static final settingslib_color_purple400:I = 0x7f060735

.field public static final settingslib_color_purple600:I = 0x7f060736

.field public static final settingslib_color_red100:I = 0x7f060737

.field public static final settingslib_color_red400:I = 0x7f060738

.field public static final settingslib_color_red50:I = 0x7f060739

.field public static final settingslib_color_red600:I = 0x7f06073a

.field public static final settingslib_color_yellow100:I = 0x7f06073b

.field public static final settingslib_color_yellow400:I = 0x7f06073c

.field public static final settingslib_color_yellow50:I = 0x7f06073d

.field public static final settingslib_color_yellow600:I = 0x7f06073e

.field public static final settingslib_dialog_accent:I = 0x7f06073f

.field public static final settingslib_dialog_background:I = 0x7f060740

.field public static final settingslib_dialog_colorError:I = 0x7f060741

.field public static final settingslib_material_grey_900:I = 0x7f060742

.field public static final settingslib_preference_list_item_bg_color:I = 0x7f060743

.field public static final settingslib_primary_dark_device_default_settings:I = 0x7f060744

.field public static final settingslib_primary_device_default_settings_light:I = 0x7f060745

.field public static final settingslib_protection_color:I = 0x7f060746

.field public static final settingslib_ripple_color:I = 0x7f060747

.field public static final settingslib_ripple_material_dark:I = 0x7f060748

.field public static final settingslib_ripple_material_light:I = 0x7f060749

.field public static final settingslib_spinner_dropdown_color:I = 0x7f06074a

.field public static final settingslib_spinner_title_color:I = 0x7f06074b

.field public static final settingslib_state_off_color:I = 0x7f06074c

.field public static final settingslib_state_on_color:I = 0x7f06074d

.field public static final settingslib_surface_dark:I = 0x7f06074e

.field public static final settingslib_surface_light:I = 0x7f06074f

.field public static final settingslib_switch_thumb_color:I = 0x7f060750

.field public static final settingslib_switch_track_color:I = 0x7f060751

.field public static final settingslib_switch_track_off:I = 0x7f060752

.field public static final settingslib_switch_track_on:I = 0x7f060753

.field public static final settingslib_switchbar_switch_thumb_tint:I = 0x7f060754

.field public static final settingslib_switchbar_switch_track_tint:I = 0x7f060755

.field public static final settingslib_tabs_indicator_color:I = 0x7f060756

.field public static final settingslib_tabs_text_color:I = 0x7f060757

.field public static final settingslib_text_color_preference_category_title:I = 0x7f060758

.field public static final settingslib_text_color_primary:I = 0x7f060759

.field public static final settingslib_text_color_primary_device_default:I = 0x7f06075a

.field public static final settingslib_text_color_secondary:I = 0x7f06075b

.field public static final settingslib_text_color_secondary_device_default:I = 0x7f06075c

.field public static final settingslib_thumb_disabled_color:I = 0x7f06075d

.field public static final settingslib_thumb_off_color:I = 0x7f06075e

.field public static final settingslib_track_off_color:I = 0x7f06075f

.field public static final settingslib_track_on_color:I = 0x7f060760

.field public static final setup_choose_password_msg_color:I = 0x7f060761

.field public static final setup_choose_password_title_color:I = 0x7f060762

.field public static final setup_choose_password_view_color:I = 0x7f060763

.field public static final setup_list_item_bg:I = 0x7f060764

.field public static final setup_wizard_wifi_color_dark:I = 0x7f060765

.field public static final setup_wizard_wifi_color_light:I = 0x7f060766

.field public static final shadow:I = 0x7f060767

.field public static final shortcut_background:I = 0x7f060768

.field public static final silent_zen_mode_bg_color:I = 0x7f060769

.field public static final silent_zen_mode_icon_color:I = 0x7f06076a

.field public static final silent_zen_mode_selected_bg_color:I = 0x7f06076b

.field public static final sim_noitification:I = 0x7f06076c

.field public static final sim_red_dot:I = 0x7f06076d

.field public static final slider_color_focus:I = 0x7f06076e

.field public static final slider_color_green:I = 0x7f06076f

.field public static final slider_color_header:I = 0x7f060770

.field public static final slider_color_white:I = 0x7f060771

.field public static final sliding_tab_title_text_color:I = 0x7f060772

.field public static final sos_common_bg_color:I = 0x7f060773

.field public static final sos_dialog_window_background:I = 0x7f060774

.field public static final sos_el_bg_color:I = 0x7f060775

.field public static final sos_player_bg:I = 0x7f060776

.field public static final sos_player_linear_bg:I = 0x7f060777

.field public static final sos_player_linear_pressed_bg:I = 0x7f060778

.field public static final sos_solid_bg:I = 0x7f060779

.field public static final sos_status_bar_bg_adapt_r:I = 0x7f06077a

.field public static final sos_stroke_bg:I = 0x7f06077b

.field public static final sos_text_color:I = 0x7f06077c

.field public static final sos_text_color_white:I = 0x7f06077d

.field public static final sound_speaker_desc_color:I = 0x7f06077e

.field public static final split_screen_layout_color:I = 0x7f06077f

.field public static final storage_green:I = 0x7f060780

.field public static final storage_red:I = 0x7f060781

.field public static final storage_size_bg:I = 0x7f060782

.field public static final storage_wizard_button:I = 0x7f060783

.field public static final storage_wizard_button_red:I = 0x7f060784

.field public static final structure_keyguard_face_circle_init:I = 0x7f060785

.field public static final structure_keyguard_face_introduction_goon:I = 0x7f060786

.field public static final stylus_color_bg:I = 0x7f060787

.field public static final stylus_color_black:I = 0x7f060788

.field public static final stylus_color_fg:I = 0x7f060789

.field public static final stylus_edit_show_bg_color:I = 0x7f06078a

.field public static final stylus_menu_summary_color:I = 0x7f06078b

.field public static final stylus_pen_key:I = 0x7f06078c

.field public static final stylus_setting_background:I = 0x7f06078d

.field public static final stylus_text_color:I = 0x7f06078e

.field public static final suc_customization_button_highlight_default:I = 0x7f06078f

.field public static final suc_customization_button_highlight_ripple:I = 0x7f060790

.field public static final success_color_device_default_dark:I = 0x7f060791

.field public static final success_color_device_default_light:I = 0x7f060792

.field public static final sud_autofilled_highlight_bg_color:I = 0x7f060793

.field public static final sud_color_accent_dark:I = 0x7f060794

.field public static final sud_color_accent_glif_dark:I = 0x7f060795

.field public static final sud_color_accent_glif_light:I = 0x7f060796

.field public static final sud_color_accent_glif_v3_dark:I = 0x7f060797

.field public static final sud_color_accent_glif_v3_light:I = 0x7f060798

.field public static final sud_color_accent_light:I = 0x7f060799

.field public static final sud_color_background_dark:I = 0x7f06079a

.field public static final sud_color_background_light:I = 0x7f06079b

.field public static final sud_color_error_text_dark:I = 0x7f06079c

.field public static final sud_color_error_text_light:I = 0x7f06079d

.field public static final sud_dynamic_color_accent_glif_v3:I = 0x7f06079e

.field public static final sud_dynamic_color_accent_glif_v3_dark:I = 0x7f06079f

.field public static final sud_dynamic_color_accent_glif_v3_light:I = 0x7f0607a0

.field public static final sud_dynamic_switch_thumb_off_dark:I = 0x7f0607a1

.field public static final sud_dynamic_switch_thumb_off_light:I = 0x7f0607a2

.field public static final sud_dynamic_switch_thumb_on_dark:I = 0x7f0607a3

.field public static final sud_dynamic_switch_thumb_on_light:I = 0x7f0607a4

.field public static final sud_dynamic_switch_track_off_dark:I = 0x7f0607a5

.field public static final sud_dynamic_switch_track_off_light:I = 0x7f0607a6

.field public static final sud_dynamic_switch_track_on_dark:I = 0x7f0607a7

.field public static final sud_dynamic_switch_track_on_light:I = 0x7f0607a8

.field public static final sud_error_warning_default_dark:I = 0x7f0607a9

.field public static final sud_error_warning_default_light:I = 0x7f0607aa

.field public static final sud_flat_button_highlight:I = 0x7f0607ab

.field public static final sud_glif_background_color_dark:I = 0x7f0607ac

.field public static final sud_glif_background_color_light:I = 0x7f0607ad

.field public static final sud_glif_edit_text_bg_dark_color:I = 0x7f0607ae

.field public static final sud_glif_edit_text_bg_light_color:I = 0x7f0607af

.field public static final sud_glif_v3_dialog_background_color_dark:I = 0x7f0607b0

.field public static final sud_glif_v3_dialog_background_color_light:I = 0x7f0607b1

.field public static final sud_glif_v3_nav_bar_color_dark:I = 0x7f0607b2

.field public static final sud_glif_v3_nav_bar_color_light:I = 0x7f0607b3

.field public static final sud_glif_v3_nav_bar_divider_color_dark:I = 0x7f0607b4

.field public static final sud_glif_v3_nav_bar_divider_color_light:I = 0x7f0607b5

.field public static final sud_glif_v3_text_color_dark:I = 0x7f0607b6

.field public static final sud_glif_v3_text_color_light:I = 0x7f0607b7

.field public static final sud_glif_window_bg_dark_color:I = 0x7f0607b8

.field public static final sud_glif_window_bg_light_color:I = 0x7f0607b9

.field public static final sud_inactive_default_dark:I = 0x7f0607ba

.field public static final sud_inactive_default_light:I = 0x7f0607bb

.field public static final sud_link_color_dark:I = 0x7f0607bc

.field public static final sud_link_color_light:I = 0x7f0607bd

.field public static final sud_list_item_icon_color_dark:I = 0x7f0607be

.field public static final sud_list_item_icon_color_light:I = 0x7f0607bf

.field public static final sud_navbar_bg_dark:I = 0x7f0607c0

.field public static final sud_navbar_bg_light:I = 0x7f0607c1

.field public static final sud_portal_pending_progress:I = 0x7f0607c2

.field public static final sud_portal_pending_progress_dark:I = 0x7f0607c3

.field public static final sud_portal_pending_progress_light:I = 0x7f0607c4

.field public static final sud_primary_default_text_dark:I = 0x7f0607c5

.field public static final sud_primary_default_text_light:I = 0x7f0607c6

.field public static final sud_progress_bar_color_dark:I = 0x7f0607c7

.field public static final sud_progress_bar_color_light:I = 0x7f0607c8

.field public static final sud_secondary_default_text_dark:I = 0x7f0607c9

.field public static final sud_secondary_default_text_light:I = 0x7f0607ca

.field public static final sud_switch_thumb_off:I = 0x7f0607cb

.field public static final sud_switch_thumb_off_dark:I = 0x7f0607cc

.field public static final sud_switch_thumb_off_light:I = 0x7f0607cd

.field public static final sud_switch_thumb_on:I = 0x7f0607ce

.field public static final sud_switch_thumb_on_dark:I = 0x7f0607cf

.field public static final sud_switch_thumb_on_light:I = 0x7f0607d0

.field public static final sud_switch_track_off:I = 0x7f0607d1

.field public static final sud_switch_track_off_dark:I = 0x7f0607d2

.field public static final sud_switch_track_off_light:I = 0x7f0607d3

.field public static final sud_switch_track_on:I = 0x7f0607d4

.field public static final sud_switch_track_on_dark:I = 0x7f0607d5

.field public static final sud_switch_track_on_light:I = 0x7f0607d6

.field public static final sud_system_accent1_100:I = 0x7f0607d7

.field public static final sud_system_accent1_200:I = 0x7f0607d8

.field public static final sud_system_accent1_300:I = 0x7f0607d9

.field public static final sud_system_accent1_600:I = 0x7f0607da

.field public static final sud_system_accent2_100:I = 0x7f0607db

.field public static final sud_system_accent_icon_text_button:I = 0x7f0607dc

.field public static final sud_system_background_surface:I = 0x7f0607dd

.field public static final sud_system_button_surface:I = 0x7f0607de

.field public static final sud_system_button_text:I = 0x7f0607df

.field public static final sud_system_dividing_line:I = 0x7f0607e0

.field public static final sud_system_error_warning:I = 0x7f0607e1

.field public static final sud_system_fallback_accent:I = 0x7f0607e2

.field public static final sud_system_hyperlink_text:I = 0x7f0607e3

.field public static final sud_system_neutral1_0:I = 0x7f0607e4

.field public static final sud_system_neutral1_10:I = 0x7f0607e5

.field public static final sud_system_neutral1_1000:I = 0x7f0607e6

.field public static final sud_system_neutral1_200:I = 0x7f0607e7

.field public static final sud_system_neutral1_300:I = 0x7f0607e8

.field public static final sud_system_neutral1_400:I = 0x7f0607e9

.field public static final sud_system_neutral1_50:I = 0x7f0607ea

.field public static final sud_system_neutral1_500:I = 0x7f0607eb

.field public static final sud_system_neutral1_600:I = 0x7f0607ec

.field public static final sud_system_neutral1_700:I = 0x7f0607ed

.field public static final sud_system_neutral1_800:I = 0x7f0607ee

.field public static final sud_system_neutral1_900:I = 0x7f0607ef

.field public static final sud_system_neutral2_100:I = 0x7f0607f0

.field public static final sud_system_neutral2_200:I = 0x7f0607f1

.field public static final sud_system_neutral2_300:I = 0x7f0607f2

.field public static final sud_system_neutral2_400:I = 0x7f0607f3

.field public static final sud_system_neutral2_50:I = 0x7f0607f4

.field public static final sud_system_neutral2_500:I = 0x7f0607f5

.field public static final sud_system_neutral2_700:I = 0x7f0607f6

.field public static final sud_system_primary_text:I = 0x7f0607f7

.field public static final sud_system_secondary_text:I = 0x7f0607f8

.field public static final sud_system_success_done:I = 0x7f0607f9

.field public static final sud_system_surface:I = 0x7f0607fa

.field public static final sud_system_tertiary_text_inactive:I = 0x7f0607fb

.field public static final sud_uniformity_backdrop_color:I = 0x7f0607fc

.field public static final suggest_color_text:I = 0x7f0607fd

.field public static final suw_color_accent_light:I = 0x7f0607fe

.field public static final svg_ic_union_color:I = 0x7f0607ff

.field public static final svg_ic_union_color_disable:I = 0x7f060800

.field public static final svg_icon_color_blue:I = 0x7f060801

.field public static final svg_icon_color_disable_light:I = 0x7f060802

.field public static final svg_icon_color_normal_light:I = 0x7f060803

.field public static final svg_icon_color_pressed_light:I = 0x7f060804

.field public static final svg_stroke_color:I = 0x7f060805

.field public static final switch_thumb_disabled_material_dark:I = 0x7f060806

.field public static final switch_thumb_disabled_material_light:I = 0x7f060807

.field public static final switch_thumb_material_dark:I = 0x7f060808

.field public static final switch_thumb_material_light:I = 0x7f060809

.field public static final switch_thumb_normal_material_dark:I = 0x7f06080a

.field public static final switch_thumb_normal_material_light:I = 0x7f06080b

.field public static final switchbar_background_color:I = 0x7f06080c

.field public static final switchbar_switch_thumb_tint:I = 0x7f06080d

.field public static final switchbar_switch_track_tint:I = 0x7f06080e

.field public static final sync_icon_color:I = 0x7f06080f

.field public static final sys_apps_updater_color:I = 0x7f060810

.field public static final tab_color:I = 0x7f060811

.field public static final test_color:I = 0x7f060812

.field public static final test_mtrl_calendar_day:I = 0x7f060813

.field public static final test_mtrl_calendar_day_selected:I = 0x7f060814

.field public static final text_black:I = 0x7f060815

.field public static final text_blue:I = 0x7f060816

.field public static final text_default_speech_robot:I = 0x7f060817

.field public static final text_gray:I = 0x7f060818

.field public static final text_green:I = 0x7f060819

.field public static final text_light_red:I = 0x7f06081a

.field public static final text_orange:I = 0x7f06081b

.field public static final text_press_gray:I = 0x7f06081c

.field public static final text_purple:I = 0x7f06081d

.field public static final text_red:I = 0x7f06081e

.field public static final text_register:I = 0x7f06081f

.field public static final text_transparent:I = 0x7f060820

.field public static final text_white:I = 0x7f060821

.field public static final thumbnail_border_color:I = 0x7f060822

.field public static final thumbnail_count_color:I = 0x7f060823

.field public static final time_not_selected:I = 0x7f060824

.field public static final time_selected:I = 0x7f060825

.field public static final time_zen_mode_bg:I = 0x7f060826

.field public static final timestamp_text_incoming:I = 0x7f060827

.field public static final timestamp_text_outgoing:I = 0x7f060828

.field public static final title_big_text_color:I = 0x7f060829

.field public static final title_color:I = 0x7f06082a

.field public static final title_small_text_color:I = 0x7f06082b

.field public static final tooltip_background_dark:I = 0x7f06082c

.field public static final tooltip_background_light:I = 0x7f06082d

.field public static final transparent:I = 0x7f06082e

.field public static final transparent_30_balck:I = 0x7f06082f

.field public static final transparent_40_balck:I = 0x7f060830

.field public static final transparent_40_white:I = 0x7f060831

.field public static final transparent_45_balck:I = 0x7f060832

.field public static final transparent_50_balck:I = 0x7f060833

.field public static final transparent_60_balck:I = 0x7f060834

.field public static final transparent_70_balck:I = 0x7f060835

.field public static final transparent_70_white:I = 0x7f060836

.field public static final transparent_80_balck:I = 0x7f060837

.field public static final turn_off_password_color:I = 0x7f060838

.field public static final tv_color_selector:I = 0x7f060839

.field public static final type_approved_dialog_title_divider:I = 0x7f06083a

.field public static final unlock_pattern_view_regular_color:I = 0x7f06083b

.field public static final unlock_pattern_view_success_color:I = 0x7f06083c

.field public static final unlock_text_dark:I = 0x7f06083d

.field public static final unlock_text_light:I = 0x7f06083e

.field public static final unlock_text_summary:I = 0x7f06083f

.field public static final unlock_text_tips:I = 0x7f060840

.field public static final unlock_text_title:I = 0x7f060841

.field public static final usage_graph_dots:I = 0x7f060842

.field public static final usage_new_home_action_bar_active_color:I = 0x7f060843

.field public static final usage_new_home_action_bar_unactive_color:I = 0x7f060844

.field public static final usage_state_black:I = 0x7f060845

.field public static final usage_state_main_bg:I = 0x7f060846

.field public static final usage_stats_app_limit_bg1:I = 0x7f060847

.field public static final usage_stats_app_limit_solid1:I = 0x7f060848

.field public static final usage_stats_app_usage_bar_normal_day:I = 0x7f060849

.field public static final usage_stats_app_usage_bar_today:I = 0x7f06084a

.field public static final usage_stats_app_usage_bar_unlock:I = 0x7f06084b

.field public static final usage_stats_app_usage_bar_week_day:I = 0x7f06084c

.field public static final usage_stats_app_usage_barr_notification:I = 0x7f06084d

.field public static final usage_stats_app_usage_text_select:I = 0x7f06084e

.field public static final usage_stats_app_usage_text_select_bg:I = 0x7f06084f

.field public static final usage_stats_bar_divide_line:I = 0x7f060850

.field public static final usage_stats_black10:I = 0x7f060851

.field public static final usage_stats_black15:I = 0x7f060852

.field public static final usage_stats_black30:I = 0x7f060853

.field public static final usage_stats_black35:I = 0x7f060854

.field public static final usage_stats_black40:I = 0x7f060855

.field public static final usage_stats_black50:I = 0x7f060856

.field public static final usage_stats_black60:I = 0x7f060857

.field public static final usage_stats_black70:I = 0x7f060858

.field public static final usage_stats_black8:I = 0x7f060859

.field public static final usage_stats_black90:I = 0x7f06085a

.field public static final usage_stats_dash_line_color:I = 0x7f06085b

.field public static final usage_stats_dash_line_text_color:I = 0x7f06085c

.field public static final usage_stats_item_level_bar:I = 0x7f06085d

.field public static final usage_stats_rect_white:I = 0x7f06085e

.field public static final usage_stats_remind_bg:I = 0x7f06085f

.field public static final usage_stats_remind_time_over_bg:I = 0x7f060860

.field public static final usage_stats_show_tips_bg:I = 0x7f060861

.field public static final usage_stats_show_tips_title_text_color:I = 0x7f060862

.field public static final usage_stats_show_tips_value_text_color:I = 0x7f060863

.field public static final usage_stats_tab_select:I = 0x7f060864

.field public static final usage_stats_tab_unselect:I = 0x7f060865

.field public static final usage_stats_time_over_text:I = 0x7f060866

.field public static final usage_stats_transparent:I = 0x7f060867

.field public static final usage_stats_white:I = 0x7f060868

.field public static final usb_dialog_selected_bg_color:I = 0x7f060869

.field public static final user_agreement_content_color:I = 0x7f06086a

.field public static final user_avatar_color_bg:I = 0x7f06086b

.field public static final view_line:I = 0x7f06086c

.field public static final visual_check_borderlayout_bg_stroke_color:I = 0x7f06086d

.field public static final visual_check_text_color:I = 0x7f06086e

.field public static final visual_check_text_color_red:I = 0x7f06086f

.field public static final visual_check_textview_checked_text_color:I = 0x7f060870

.field public static final visual_check_textview_checked_text_color_red:I = 0x7f060871

.field public static final visual_check_textview_unchecked_text_color:I = 0x7f060872

.field public static final voice_access_radio_preference_bg_selected_color:I = 0x7f060873

.field public static final voice_access_radio_preference_bg_unselected_color:I = 0x7f060874

.field public static final voice_access_radio_preference_summary_selected_color:I = 0x7f060875

.field public static final voice_access_radio_preference_summary_unselected_color:I = 0x7f060876

.field public static final voice_access_radio_preference_title_selected_color:I = 0x7f060877

.field public static final voice_access_radio_preference_title_unselected_color:I = 0x7f060878

.field public static final volume_icon_color:I = 0x7f060879

.field public static final volume_icon_color_obvious:I = 0x7f06087a

.field public static final volume_seekbar_bg_color:I = 0x7f06087b

.field public static final volume_seekbar_disable_progress_color:I = 0x7f06087c

.field public static final volume_seekbar_progress_color:I = 0x7f06087d

.field public static final volume_seekbar_title_color:I = 0x7f06087e

.field public static final warning_text_color:I = 0x7f06087f

.field public static final whiltealpha100:I = 0x7f060880

.field public static final whiltealpha80:I = 0x7f060881

.field public static final white:I = 0x7f060882

.field public static final white_translucent:I = 0x7f060883

.field public static final wifi_add_network_title_color:I = 0x7f060884

.field public static final wifi_detail_delete_color:I = 0x7f060885

.field public static final wifi_detail_disconnect_color:I = 0x7f060886

.field public static final wifi_detail_item_icon_color:I = 0x7f060887

.field public static final wifi_detail_modify_color:I = 0x7f060888

.field public static final wifi_dialog_text_color:I = 0x7f060889

.field public static final wifi_finddevice_dialog_color:I = 0x7f06088a

.field public static final wifi_preference_divider:I = 0x7f06088b

.field public static final wifi_signal_icon_color:I = 0x7f06088c

.field public static final wifi_signal_icon_connected_color:I = 0x7f06088d

.field public static final window_background_color:I = 0x7f06088e

.field public static final word_photo_color:I = 0x7f06088f

.field public static final word_photo_color_dark:I = 0x7f060890


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
