.class public Landroidx/preference/TileDropDownPreference;
.super Lmiuix/preference/DropDownPreference;
.source "TileDropDownPreference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/preference/TileDropDownPreference$OnChangeListener;
    }
.end annotation


# instance fields
.field private listener:Landroidx/preference/TileDropDownPreference$OnChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmiuix/preference/DropDownPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private getPrefKey()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/TileDropDownPreference;->getKey()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public callChangeListener(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Landroidx/preference/TileDropDownPreference;->listener:Landroidx/preference/TileDropDownPreference$OnChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroidx/preference/TileDropDownPreference;->listener:Landroidx/preference/TileDropDownPreference$OnChangeListener;

    invoke-interface {v0, p0, p1}, Landroidx/preference/TileDropDownPreference$OnChangeListener;->onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)V

    :cond_0
    invoke-super {p0, p1}, Lmiuix/preference/DropDownPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public onDependencyChanged(Landroidx/preference/Preference;Z)V
    .locals 2

    const-string v0, "big_tile_column"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;I)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0}, Landroidx/preference/TileDropDownPreference;->getPrefKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    if-ge v0, v1, :cond_0

    const/4 p2, 0x1

    :goto_0
    invoke-super {p0, p1, p2}, Lmiuix/preference/DropDownPreference;->onDependencyChanged(Landroidx/preference/Preference;Z)V

    return-void

    :cond_0
    const/4 p2, 0x0

    goto :goto_0
.end method

.method public setOnChangeListener(Landroidx/preference/TileDropDownPreference$OnChangeListener;)V
    .locals 0

    iput-object p1, p0, Landroidx/preference/TileDropDownPreference;->listener:Landroidx/preference/TileDropDownPreference$OnChangeListener;

    return-void
.end method
