.class Landroidx/preference/FontStylePreferenceDialogFragmentCompat$LoadFile;
.super Landroid/os/AsyncTask;
.source "FontStylePreferenceDialogFragmentCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/preference/FontStylePreferenceDialogFragmentCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LoadFile"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Landroidx/preference/FontStylePreferenceDialogFragmentCompat;",
        "Ljava/util/List",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final mFragment:Landroidx/preference/FontStylePreferenceDialogFragmentCompat;


# direct methods
.method private constructor <init>(Landroidx/preference/FontStylePreferenceDialogFragmentCompat;)V
    .locals 0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat$LoadFile;->mFragment:Landroidx/preference/FontStylePreferenceDialogFragmentCompat;

    return-void
.end method

.method synthetic constructor <init>(Landroidx/preference/FontStylePreferenceDialogFragmentCompat;Landroidx/preference/FontStylePreferenceDialogFragmentCompat$1;)V
    .locals 0

    invoke-direct {p0, p1}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat$LoadFile;-><init>(Landroidx/preference/FontStylePreferenceDialogFragmentCompat;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat$LoadFile;->doInBackground([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v1, 0x0

    new-instance v0, Ljava/io/File;

    const/4 v5, 0x0

    aget-object v5, p1, v5

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    if-eqz v4, :cond_0

    array-length v5, v4

    if-lez v5, :cond_0

    array-length v2, v4

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v5, v4, v1

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    return-object v3
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat$LoadFile;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    const/4 v0, 0x0

    const-string v1, "Default"

    invoke-interface {p1, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v0, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat$LoadFile;->mFragment:Landroidx/preference/FontStylePreferenceDialogFragmentCompat;

    new-instance v1, Landroidx/preference/FontStylePreferenceDialogFragmentCompat$FileListAdapter;

    iget-object v2, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat$LoadFile;->mFragment:Landroidx/preference/FontStylePreferenceDialogFragmentCompat;

    const/4 v3, 0x0

    invoke-direct {v1, v2, p1, v3}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat$FileListAdapter;-><init>(Landroidx/preference/FontStylePreferenceDialogFragmentCompat;Ljava/util/List;Landroidx/preference/FontStylePreferenceDialogFragmentCompat$1;)V

    invoke-static {v0, v1}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->access$102(Landroidx/preference/FontStylePreferenceDialogFragmentCompat;Landroidx/preference/FontStylePreferenceDialogFragmentCompat$FileListAdapter;)Landroidx/preference/FontStylePreferenceDialogFragmentCompat$FileListAdapter;

    iget-object v0, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat$LoadFile;->mFragment:Landroidx/preference/FontStylePreferenceDialogFragmentCompat;

    invoke-static {v0}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->access$300(Landroidx/preference/FontStylePreferenceDialogFragmentCompat;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat$LoadFile;->mFragment:Landroidx/preference/FontStylePreferenceDialogFragmentCompat;

    invoke-static {v0}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->access$400(Landroidx/preference/FontStylePreferenceDialogFragmentCompat;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat$LoadFile;->mFragment:Landroidx/preference/FontStylePreferenceDialogFragmentCompat;

    invoke-static {v1}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->access$100(Landroidx/preference/FontStylePreferenceDialogFragmentCompat;)Landroidx/preference/FontStylePreferenceDialogFragmentCompat$FileListAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method
