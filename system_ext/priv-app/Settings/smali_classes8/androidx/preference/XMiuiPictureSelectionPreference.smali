.class public Landroidx/preference/XMiuiPictureSelectionPreference;
.super Landroidx/preference/Preference;
.source "XMiuiPictureSelectionPreference.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Landroidx/preference/MiuiPictureSelection/fakeFragment$OnImageCreatedListener;


# instance fields
.field private Helper:Landroidx/preference/XMiuiPreferenceHelper;

.field private fragment:Landroidx/fragment/app/Fragment;

.field private isNeedCopyImage:Z

.field private mAspectX:I

.field private mAspectY:I

.field private final mContext:Landroid/content/Context;

.field private final mDensity:F

.field private mDrawableIcon:Landroid/graphics/drawable/Drawable;

.field private mHeight:I

.field private mName:Ljava/lang/String;

.field private final mOk:Z

.field private mThumb:Z

.field private mType:Ljava/lang/String;

.field private mView:Landroid/view/View;

.field private mWidth:I

.field private pictureSelectionHelper:Landroidx/preference/MiuiPictureSelection/PictureSelectionHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroidx/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string/jumbo v0, "none"

    iput-object v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mType:Ljava/lang/String;

    iput-object p1, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mContext:Landroid/content/Context;

    invoke-direct {p0, p2}, Landroidx/preference/XMiuiPictureSelectionPreference;->initialize(Landroid/util/AttributeSet;)Z

    move-result v0

    iput-boolean v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mOk:Z

    invoke-virtual {p0}, Landroidx/preference/XMiuiPictureSelectionPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mDensity:F

    return-void
.end method

.method static synthetic access$000(Landroidx/preference/XMiuiPictureSelectionPreference;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Landroidx/preference/XMiuiPictureSelectionPreference;)Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mDrawableIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private copyToTheme()V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    const-string/jumbo v4, "mount -o rw,remount /system"

    invoke-static {v4, v7, v7}, Landroid/Utils/Shell;->execCommand(Ljava/lang/String;ZZ)Landroid/Utils/Shell$CommandResult;

    move-result-object v1

    iget v4, v1, Landroid/Utils/Shell$CommandResult;->result:I

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "copyToTheme: res = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Landroid/Utils/Shell$CommandResult;->result:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " error = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Landroid/Utils/Shell$CommandResult;->errorMsg:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "   succes = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Landroid/Utils/Shell$CommandResult;->successMsg:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v4, "mount -o rw,remount /system-root"

    invoke-static {v4, v7, v7}, Landroid/Utils/Shell;->execCommand(Ljava/lang/String;ZZ)Landroid/Utils/Shell$CommandResult;

    move-result-object v1

    iget v4, v1, Landroid/Utils/Shell$CommandResult;->result:I

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "copyToTheme: res = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Landroid/Utils/Shell$CommandResult;->result:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " error = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Landroid/Utils/Shell$CommandResult;->errorMsg:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "   succes = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Landroid/Utils/Shell$CommandResult;->successMsg:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v4, "mount -o rw,remount /"

    invoke-static {v4, v7, v7}, Landroid/Utils/Shell;->execCommand(Ljava/lang/String;ZZ)Landroid/Utils/Shell$CommandResult;

    move-result-object v1

    :cond_0
    iget v4, v1, Landroid/Utils/Shell$CommandResult;->result:I

    if-nez v4, :cond_1

    const/4 v4, 0x2

    new-array v2, v4, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cp -p -f "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Landroidx/preference/MiuiPictureSelection/PictureSelectionHelper;->PATH:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".png /system/media/theme/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".png"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v8

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "chmod 0666 /system/media/theme/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".png"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v7

    invoke-static {v2, v7, v7}, Landroid/Utils/Shell;->execCommand([Ljava/lang/String;ZZ)Landroid/Utils/Shell$CommandResult;

    move-result-object v1

    iget v4, v1, Landroid/Utils/Shell$CommandResult;->result:I

    if-nez v4, :cond_1

    const-string/jumbo v0, "Copy successful"

    iget-object v4, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mContext:Landroid/content/Context;

    invoke-static {v4, v0, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "copyToTheme: res = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Landroid/Utils/Shell$CommandResult;->result:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " error = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Landroid/Utils/Shell$CommandResult;->errorMsg:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "   succes = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Landroid/Utils/Shell$CommandResult;->successMsg:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getDrawableFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 1

    invoke-static {p0}, Landroidx/preference/MiuiPictureSelection/PictureSelectionHelper;->getDrawableFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private initialize(Landroid/util/AttributeSet;)Z
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    new-instance v0, Landroidx/preference/XMiuiPreferenceHelper;

    iget-object v3, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3, p1}, Landroidx/preference/XMiuiPreferenceHelper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->Helper:Landroidx/preference/XMiuiPreferenceHelper;

    iget-object v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->Helper:Landroidx/preference/XMiuiPreferenceHelper;

    const-string/jumbo v3, "name"

    invoke-virtual {v0, v3}, Landroidx/preference/XMiuiPreferenceHelper;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mName:Ljava/lang/String;

    if-nez v0, :cond_0

    :goto_0
    return v1

    :cond_0
    iget-object v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->Helper:Landroidx/preference/XMiuiPreferenceHelper;

    const-string v3, "copytotheme"

    invoke-virtual {v0, v3, v1}, Landroidx/preference/XMiuiPreferenceHelper;->getAttributeBool(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->isNeedCopyImage:Z

    iget-object v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->Helper:Landroidx/preference/XMiuiPreferenceHelper;

    const-string/jumbo v3, "thumbnail"

    invoke-virtual {v0, v3, v2}, Landroidx/preference/XMiuiPreferenceHelper;->getAttributeBool(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mThumb:Z

    iget-object v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->Helper:Landroidx/preference/XMiuiPreferenceHelper;

    const-string/jumbo v3, "type"

    invoke-virtual {v0, v3}, Landroidx/preference/XMiuiPreferenceHelper;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mType:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mType:Ljava/lang/String;

    iget-object v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mType:Ljava/lang/String;

    const-string/jumbo v3, "portrait"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mType:Ljava/lang/String;

    const-string v3, "landscape"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mType:Ljava/lang/String;

    const-string/jumbo v3, "square"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mType:Ljava/lang/String;

    const-string/jumbo v3, "not_defined"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mType:Ljava/lang/String;

    goto :goto_1

    :cond_3
    iget-object v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->Helper:Landroidx/preference/XMiuiPreferenceHelper;

    const-string v3, "aspectx"

    invoke-virtual {v0, v3, v1}, Landroidx/preference/XMiuiPreferenceHelper;->getAttributeInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mAspectX:I

    iget-object v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->Helper:Landroidx/preference/XMiuiPreferenceHelper;

    const-string v3, "aspecty"

    invoke-virtual {v0, v3, v1}, Landroidx/preference/XMiuiPreferenceHelper;->getAttributeInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mAspectY:I

    iget-object v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->Helper:Landroidx/preference/XMiuiPreferenceHelper;

    const-string/jumbo v3, "width"

    invoke-virtual {v0, v3, v1}, Landroidx/preference/XMiuiPreferenceHelper;->getAttributeInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mWidth:I

    iget-object v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->Helper:Landroidx/preference/XMiuiPreferenceHelper;

    const-string v3, "height"

    invoke-virtual {v0, v3, v1}, Landroidx/preference/XMiuiPreferenceHelper;->getAttributeInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mHeight:I

    iget v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mAspectY:I

    if-eqz v0, :cond_4

    iget v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mAspectX:I

    if-nez v0, :cond_5

    :cond_4
    iget v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mHeight:I

    if-eqz v0, :cond_6

    iget v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mWidth:I

    if-eqz v0, :cond_6

    :cond_5
    move v0, v2

    :goto_2
    move v1, v0

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method private static sendToast(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private setPreviewPicture()V
    .locals 9

    const/4 v8, 0x0

    const/high16 v7, 0x42200000    # 40.0f

    iget-object v5, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mView:Landroid/view/View;

    if-nez v5, :cond_1

    iget-boolean v5, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mThumb:Z

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Landroidx/preference/XMiuiPictureSelectionPreference;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v1, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iget-object v5, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mView:Landroid/view/View;

    const v6, 0x1020018

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget v5, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mDensity:F

    mul-float/2addr v5, v7

    float-to-int v5, v5

    iget v6, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mDensity:F

    mul-float/2addr v6, v7

    float-to-int v6, v6

    invoke-direct {v3, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v5, 0x11

    iput v5, v3, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    iget v5, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mDensity:F

    const/high16 v6, 0x40a00000    # 5.0f

    mul-float/2addr v5, v6

    float-to-int v2, v5

    int-to-float v5, v2

    const/high16 v6, 0x3fc00000    # 1.5f

    mul-float/2addr v5, v6

    float-to-int v5, v5

    invoke-virtual {v3, v2, v2, v5, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    if-eqz v4, :cond_0

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {v4, v8, v0}, Landroid/widget/LinearLayout;->removeViews(II)V

    :cond_2
    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v5, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mName:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mName:Ljava/lang/String;

    invoke-static {v5}, Landroidx/preference/MiuiPictureSelection/PictureSelectionHelper;->getDrawableFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mDrawableIcon:Landroid/graphics/drawable/Drawable;

    iget-object v5, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mDrawableIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v5, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mDrawableIcon:Landroid/graphics/drawable/Drawable;

    if-eqz v5, :cond_0

    new-instance v5, Landroidx/preference/XMiuiPictureSelectionPreference$1;

    invoke-direct {v5, p0}, Landroidx/preference/XMiuiPictureSelectionPreference$1;-><init>(Landroidx/preference/XMiuiPictureSelectionPreference;)V

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method public onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/preference/Preference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V

    invoke-virtual {p0, p1}, Landroidx/preference/XMiuiPictureSelectionPreference;->onMyBindViewHolder(Ljava/lang/Object;)V

    return-void
.end method

.method protected onClick()V
    .locals 4

    iget-boolean v1, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mOk:Z

    if-eqz v1, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "name"

    iget-object v2, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "type"

    iget-object v2, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "aspectX"

    iget v2, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mAspectX:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "aspectY"

    iget v2, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mAspectY:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "width"

    iget v2, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mWidth:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "height"

    iget v2, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mHeight:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "listener"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    new-instance v1, Landroidx/preference/MiuiPictureSelection/fakeFragment;

    invoke-direct {v1}, Landroidx/preference/MiuiPictureSelection/fakeFragment;-><init>()V

    iput-object v1, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->fragment:Landroidx/fragment/app/Fragment;

    iget-object v1, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->fragment:Landroidx/fragment/app/Fragment;

    invoke-virtual {v1, v0}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    sget-object v1, Lmiuix/appcompat/app/AppCompatActivity;->sFragmentManager:Landroidx/fragment/app/FragmentManager;

    invoke-virtual {v1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x1020002

    iget-object v3, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->fragment:Landroidx/fragment/app/Fragment;

    invoke-virtual {v1, v2, v3}, Landroidx/fragment/app/FragmentTransaction;->add(ILandroidx/fragment/app/Fragment;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    :goto_0
    invoke-super {p0}, Landroidx/preference/Preference;->onClick()V

    return-void

    :cond_0
    iget-object v1, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mContext:Landroid/content/Context;

    const-string v2, "Whoops - Attribute not correct!"

    invoke-static {v1, v2}, Landroidx/preference/XMiuiPictureSelectionPreference;->sendToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onImageCreated(Z)V
    .locals 2

    sget-object v0, Lmiuix/appcompat/app/AppCompatActivity;->sFragmentManager:Landroidx/fragment/app/FragmentManager;

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->fragment:Landroidx/fragment/app/Fragment;

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentTransaction;->remove(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    if-eqz p1, :cond_0

    iget-object v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->Helper:Landroidx/preference/XMiuiPreferenceHelper;

    invoke-virtual {v0}, Landroidx/preference/XMiuiPreferenceHelper;->sendIntent()V

    iget-boolean v0, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->isNeedCopyImage:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Landroidx/preference/XMiuiPictureSelectionPreference;->copyToTheme()V

    :cond_0
    invoke-direct {p0}, Landroidx/preference/XMiuiPictureSelectionPreference;->setPreviewPicture()V

    return-void
.end method

.method public onMyBindViewHolder(Ljava/lang/Object;)V
    .locals 5

    if-eqz p1, :cond_0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "itemView"

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Landroid/view/View;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    iput-object v2, p0, Landroidx/preference/XMiuiPictureSelectionPreference;->mView:Landroid/view/View;

    invoke-direct {p0}, Landroidx/preference/XMiuiPictureSelectionPreference;->setPreviewPicture()V

    :cond_0
    return-void

    :catch_0
    move-exception v3

    goto :goto_0

    :catch_1
    move-exception v3

    goto :goto_0
.end method
