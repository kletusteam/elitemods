.class public Landroidx/preference/SlidersStylePreference;
.super Landroidx/preference/XMiuiDropDownPreference;
.source "SlidersStylePreference.java"


# instance fields
.field private mLoadingIcon:Lmiuix/appcompat/app/ProgressDialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroidx/preference/XMiuiDropDownPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method static synthetic access$000(Landroidx/preference/SlidersStylePreference;)V
    .locals 0

    invoke-direct {p0}, Landroidx/preference/SlidersStylePreference;->hideLoadingIcon()V

    return-void
.end method

.method private hideLoadingIcon()V
    .locals 2

    iget-object v0, p0, Landroidx/preference/SlidersStylePreference;->mLoadingIcon:Lmiuix/appcompat/app/ProgressDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/app/ProgressDialog;->cancel()V

    const/4 v1, 0x0

    iput-object v1, p0, Landroidx/preference/SlidersStylePreference;->mLoadingIcon:Lmiuix/appcompat/app/ProgressDialog;

    :cond_0
    return-void
.end method

.method private showLoadingIcon()V
    .locals 3

    iget-object v0, p0, Landroidx/preference/SlidersStylePreference;->mLoadingIcon:Lmiuix/appcompat/app/ProgressDialog;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroidx/preference/SlidersStylePreference;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/CharSequence;

    const-string v2, "EliteDevelopment.com.pk"

    invoke-static {v1, v0, v2}, Lmiuix/appcompat/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lmiuix/appcompat/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Landroidx/preference/SlidersStylePreference;->mLoadingIcon:Lmiuix/appcompat/app/ProgressDialog;

    :cond_0
    return-void
.end method

.method private updateCenter()V
    .locals 4

    invoke-direct {p0}, Landroidx/preference/SlidersStylePreference;->showLoadingIcon()V

    const-string v0, "use_control_panel"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/preference/SettingsEliteHelper;->putBoolinSettings(Ljava/lang/String;Z)V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Landroidx/preference/SlidersStylePreference$1;

    invoke-direct {v1, p0}, Landroidx/preference/SlidersStylePreference$1;-><init>(Landroidx/preference/SlidersStylePreference;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Landroidx/preference/SlidersStylePreference$2;

    invoke-direct {v1, p0}, Landroidx/preference/SlidersStylePreference$2;-><init>(Landroidx/preference/SlidersStylePreference;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method


# virtual methods
.method public callChangeListener(Ljava/lang/Object;)Z
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/SlidersStylePreference;->getValue()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Landroidx/preference/SlidersStylePreference;->updateCenter()V

    :cond_0
    invoke-super {p0, p1}, Landroidx/preference/XMiuiDropDownPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method
