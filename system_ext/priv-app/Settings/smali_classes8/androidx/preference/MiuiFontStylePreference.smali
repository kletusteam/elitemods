.class public Landroidx/preference/MiuiFontStylePreference;
.super Landroidx/preference/DialogPreference;
.source "MiuiFontStylePreference.java"


# instance fields
.field private Helper:Landroidx/preference/XMiuiPreferenceHelper;

.field private mPath:Ljava/lang/String;

.field private mSummary:Landroid/widget/TextView;

.field private mValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Landroidx/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string v0, ""

    iput-object v0, p0, Landroidx/preference/MiuiFontStylePreference;->mValue:Ljava/lang/String;

    const-string v0, "/system/myfonts/"

    iput-object v0, p0, Landroidx/preference/MiuiFontStylePreference;->mPath:Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroidx/preference/MiuiFontStylePreference;->setPersistent(Z)V

    new-instance v0, Landroidx/preference/XMiuiPreferenceHelper;

    invoke-direct {v0, p1, p2}, Landroidx/preference/XMiuiPreferenceHelper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Landroidx/preference/MiuiFontStylePreference;->Helper:Landroidx/preference/XMiuiPreferenceHelper;

    const-string/jumbo v1, "path"

    invoke-virtual {v0, v1}, Landroidx/preference/XMiuiPreferenceHelper;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iput-object v0, p0, Landroidx/preference/MiuiFontStylePreference;->mPath:Ljava/lang/String;

    :cond_0
    iget-object v1, p0, Landroidx/preference/MiuiFontStylePreference;->Helper:Landroidx/preference/XMiuiPreferenceHelper;

    invoke-virtual {v1}, Landroidx/preference/XMiuiPreferenceHelper;->isValidateKey()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroidx/preference/MiuiFontStylePreference;->Helper:Landroidx/preference/XMiuiPreferenceHelper;

    invoke-virtual {v1}, Landroidx/preference/XMiuiPreferenceHelper;->getStr()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroidx/preference/MiuiFontStylePreference;->mValue:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string v1, "Default"

    invoke-virtual {p0, v1}, Landroidx/preference/MiuiFontStylePreference;->onItemClick(Ljava/lang/String;)V

    :goto_0
    const v1, 0x1090014

    invoke-virtual {p0, v1}, Landroidx/preference/MiuiFontStylePreference;->setDialogLayoutResource(I)V

    invoke-virtual {p0}, Landroidx/preference/MiuiFontStylePreference;->setSummary()V

    return-void
.end method


# virtual methods
.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroidx/preference/MiuiFontStylePreference;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method public getPreferenceIntent()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroidx/preference/MiuiFontStylePreference;->Helper:Landroidx/preference/XMiuiPreferenceHelper;

    iget-object v0, v0, Landroidx/preference/XMiuiPreferenceHelper;->mIntent:Ljava/lang/String;

    return-object v0
.end method

.method getTypeFase(Ljava/lang/String;)Landroid/graphics/Typeface;
    .locals 3

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Landroidx/preference/MiuiFontStylePreference;->mPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Landroidx/preference/MiuiPreferenceHelper;->getTAG(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "getTypeFase : fonts not found"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    return-object v1
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroidx/preference/MiuiFontStylePreference;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
    .locals 4

    invoke-super {p0, p1}, Landroidx/preference/DialogPreference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V

    const v0, 0x1020010

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Landroidx/preference/MiuiFontStylePreference;->mSummary:Landroid/widget/TextView;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Landroidx/preference/MiuiFontStylePreference$2;

    invoke-direct {v1, p0}, Landroidx/preference/MiuiFontStylePreference$2;-><init>(Landroidx/preference/MiuiFontStylePreference;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method onItemClick(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Landroidx/preference/MiuiFontStylePreference;->mValue:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Landroidx/preference/MiuiFontStylePreference;->mValue:Ljava/lang/String;

    iget-object v0, p0, Landroidx/preference/MiuiFontStylePreference;->Helper:Landroidx/preference/XMiuiPreferenceHelper;

    invoke-virtual {v0, p1}, Landroidx/preference/XMiuiPreferenceHelper;->putStr(Ljava/lang/String;)V

    iget-object v0, p0, Landroidx/preference/MiuiFontStylePreference;->Helper:Landroidx/preference/XMiuiPreferenceHelper;

    invoke-virtual {v0}, Landroidx/preference/XMiuiPreferenceHelper;->sendIntent()V

    invoke-virtual {p0}, Landroidx/preference/MiuiFontStylePreference;->setSummary()V

    :cond_0
    return-void
.end method

.method public onMyBindViewHolder(Ljava/lang/Object;)V
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "itemView"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v1

    :goto_0
    if-eqz v0, :cond_0

    const v1, 0x1020010

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Landroidx/preference/MiuiFontStylePreference;->mSummary:Landroid/widget/TextView;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Landroidx/preference/MiuiFontStylePreference$1;

    invoke-direct {v2, p0}, Landroidx/preference/MiuiFontStylePreference$1;-><init>(Landroidx/preference/MiuiFontStylePreference;)V

    const-wide/16 v3, 0x64

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method public setSummary()V
    .locals 7

    iget-object v0, p0, Landroidx/preference/MiuiFontStylePreference;->Helper:Landroidx/preference/XMiuiPreferenceHelper;

    const-string v1, "Default"

    invoke-virtual {v0, v1}, Landroidx/preference/XMiuiPreferenceHelper;->getStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Landroidx/preference/MiuiFontStylePreference;->mSummary:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroidx/preference/MiuiFontStylePreference;->mSummary:Landroid/widget/TextView;

    invoke-static {v2}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Landroidx/preference/MiuiFontStylePreference;->mPath:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroidx/preference/MiuiFontStylePreference;->getTypeFase(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroidx/preference/MiuiFontStylePreference;->getKey()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_typefase"

    const-string v6, ""

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_typefasestyle"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Landroidx/preference/MiuiFontStylePreference;->mSummary:Landroid/widget/TextView;

    invoke-static {v3, v2}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v4, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    :cond_1
    :goto_0
    invoke-super {p0, v0}, Landroidx/preference/DialogPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method
