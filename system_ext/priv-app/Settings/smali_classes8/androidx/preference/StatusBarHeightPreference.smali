.class public Landroidx/preference/StatusBarHeightPreference;
.super Landroidx/preference/Preference;
.source "StatusBarHeightPreference.java"

# interfaces
.implements Lmiuix/settings/example/SeekBarDialog$OnProgressChangeListener;
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private dialogShowing:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroidx/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroidx/preference/StatusBarHeightPreference;->dialogShowing:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroidx/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroidx/preference/StatusBarHeightPreference;->dialogShowing:Z

    return-void
.end method


# virtual methods
.method protected onClick()V
    .locals 5

    invoke-super {p0}, Landroidx/preference/Preference;->onClick()V

    iget-boolean v1, p0, Landroidx/preference/StatusBarHeightPreference;->dialogShowing:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Landroidx/preference/StatusBarHeightPreference;->dialogShowing:Z

    new-instance v0, Lmiuix/settings/example/SeekBarDialog;

    invoke-virtual {p0}, Landroidx/preference/StatusBarHeightPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lmiuix/settings/example/SeekBarDialog;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x32

    invoke-virtual {v0, v1}, Lmiuix/settings/example/SeekBarDialog;->setMin(I)Lmiuix/settings/example/SeekBarDialog;

    move-result-object v1

    const/16 v2, 0xfa

    invoke-virtual {v1, v2}, Lmiuix/settings/example/SeekBarDialog;->setMax(I)Lmiuix/settings/example/SeekBarDialog;

    move-result-object v1

    invoke-virtual {p0}, Landroidx/preference/StatusBarHeightPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "custom_status_bar_height"

    const/16 v4, 0x63

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lmiuix/settings/example/SeekBarDialog;->setCurent(I)Lmiuix/settings/example/SeekBarDialog;

    move-result-object v1

    invoke-virtual {v1, p0}, Lmiuix/settings/example/SeekBarDialog;->setOnProgressChangeListener(Lmiuix/settings/example/SeekBarDialog$OnProgressChangeListener;)Lmiuix/settings/example/SeekBarDialog;

    move-result-object v1

    invoke-virtual {v1, p0}, Lmiuix/settings/example/SeekBarDialog;->setPositiveClickListener(Landroid/content/DialogInterface$OnClickListener;)Lmiuix/settings/example/SeekBarDialog;

    move-result-object v1

    const-string v2, "Statusbar size"

    invoke-virtual {v1, v2}, Lmiuix/settings/example/SeekBarDialog;->setTitle(Ljava/lang/String;)Lmiuix/settings/example/SeekBarDialog;

    move-result-object v1

    invoke-virtual {v1}, Lmiuix/settings/example/SeekBarDialog;->init()Lmiuix/settings/example/SeekBarDialog;

    move-result-object v1

    invoke-virtual {v1}, Lmiuix/settings/example/SeekBarDialog;->show()Lmiuix/appcompat/app/AlertDialog;

    invoke-static {}, Landroid/preference/CustomUpdater;->getInstance()Landroid/preference/CustomUpdater;

    move-result-object v1

    const-string v2, "custom_statusbar_dialog_open"

    invoke-virtual {v1, v2}, Landroid/preference/CustomUpdater;->beginChange(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 11

    invoke-static {}, Landroid/preference/CustomUpdater;->getInstance()Landroid/preference/CustomUpdater;

    move-result-object v6

    const-string v7, "custom_statusbar_dialog_close"

    invoke-virtual {v6, v7}, Landroid/preference/CustomUpdater;->beginChange(Ljava/lang/String;)V

    const/4 v6, 0x0

    iput-boolean v6, p0, Landroidx/preference/StatusBarHeightPreference;->dialogShowing:Z

    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Landroidx/preference/StatusBarHeightPreference;->getContext()Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    const-string v7, "sth.txt"

    invoke-direct {v1, v6, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_0
    new-instance v5, Ljava/io/FileWriter;

    const/4 v6, 0x0

    invoke-direct {v5, v1, v6}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v7, 0x0

    :try_start_1
    invoke-virtual {p0}, Landroidx/preference/StatusBarHeightPreference;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v8, "custom_status_bar_height"

    const/16 v9, 0x63

    invoke-static {v6, v8, v9}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v5, v4, v6, v8}, Ljava/io/FileWriter;->write(Ljava/lang/String;II)V

    invoke-virtual {v5}, Ljava/io/FileWriter;->flush()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v5, :cond_0

    if-eqz v7, :cond_1

    :try_start_2
    invoke-virtual {v5}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_0
    :goto_0
    const-string v6, "mount -o rw,remount /"

    const/4 v7, 0x1

    const/4 v8, 0x1

    invoke-static {v6, v7, v8}, Landroid/Utils/Shell;->execCommand(Ljava/lang/String;ZZ)Landroid/Utils/Shell$CommandResult;

    move-result-object v2

    const/4 v6, 0x2

    new-array v4, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "cp -p -f "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " /system/media/theme/default/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "chmod 0666 /system/media/theme/default/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-static {v4, v6, v7}, Landroid/Utils/Shell;->execCommand([Ljava/lang/String;ZZ)Landroid/Utils/Shell$CommandResult;

    move-result-object v3

    sget-object v6, Landroidx/preference/MiuiPreferenceHelper;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onClick: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v3, Landroid/Utils/Shell$CommandResult;->errorMsg:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    return-void

    :catch_0
    move-exception v6

    :try_start_3
    invoke-virtual {v7, v6}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v6, Landroidx/preference/MiuiPreferenceHelper;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onClick: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_1
    :try_start_4
    invoke-virtual {v5}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    :catch_2
    move-exception v6

    :try_start_5
    throw v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_0
    move-exception v7

    move-object v10, v7

    move-object v7, v6

    move-object v6, v10

    :goto_1
    if-eqz v5, :cond_2

    if-eqz v7, :cond_3

    :try_start_6
    invoke-virtual {v5}, Ljava/io/FileWriter;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    :cond_2
    :goto_2
    :try_start_7
    throw v6

    :catch_3
    move-exception v8

    invoke-virtual {v7, v8}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_3
    invoke-virtual {v5}, Ljava/io/FileWriter;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_2

    :catchall_1
    move-exception v6

    goto :goto_1
.end method

.method public onProgressChange(I)V
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/StatusBarHeightPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "custom_status_bar_height"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method public onStopTouch(I)V
    .locals 0

    return-void
.end method
