.class public Landroidx/preference/TilePreferenceCategory;
.super Landroidx/preference/PreferenceCategory;
.source "TilePreferenceCategory.java"

# interfaces
.implements Landroidx/preference/TileDropDownPreference$OnChangeListener;


# instance fields
.field private mAllEntries:[Ljava/lang/CharSequence;

.field private mAllEntryValues:[Ljava/lang/CharSequence;

.field private mEntries:[Ljava/lang/CharSequence;

.field private mEntryValues:[Ljava/lang/CharSequence;

.field private mLenght:I

.field private mPrefs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroidx/preference/TileDropDownPreference;",
            ">;"
        }
    .end annotation
.end field

.field private mSettingsValues:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Landroidx/preference/PreferenceCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroidx/preference/TilePreferenceCategory;->mPrefs:Ljava/util/ArrayList;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Landroidx/preference/TilePreferenceCategory;->mSettingsValues:[Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "big_tile_entries"

    invoke-direct {p0, v1}, Landroidx/preference/TilePreferenceCategory;->getArrayId(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroidx/preference/TilePreferenceCategory;->mAllEntryValues:[Ljava/lang/CharSequence;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "big_tile_title"

    invoke-direct {p0, v1}, Landroidx/preference/TilePreferenceCategory;->getArrayId(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroidx/preference/TilePreferenceCategory;->mAllEntries:[Ljava/lang/CharSequence;

    invoke-direct {p0}, Landroidx/preference/TilePreferenceCategory;->updateSettings()V

    invoke-direct {p0}, Landroidx/preference/TilePreferenceCategory;->init()V

    return-void
.end method

.method private checkSettings()V
    .locals 0

    return-void
.end method

.method private getArrayId(Ljava/lang/String;)I
    .locals 4

    invoke-virtual {p0}, Landroidx/preference/TilePreferenceCategory;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "array"

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method private getPrefKey(Landroidx/preference/Preference;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private init()V
    .locals 9

    iget v3, p0, Landroidx/preference/TilePreferenceCategory;->mLenght:I

    new-array v4, v3, [Ljava/lang/CharSequence;

    iput-object v4, p0, Landroidx/preference/TilePreferenceCategory;->mEntries:[Ljava/lang/CharSequence;

    new-array v4, v3, [Ljava/lang/CharSequence;

    iput-object v4, p0, Landroidx/preference/TilePreferenceCategory;->mEntryValues:[Ljava/lang/CharSequence;

    const/4 v1, 0x0

    const/4 v2, 0x1

    iget-object v5, p0, Landroidx/preference/TilePreferenceCategory;->mAllEntryValues:[Ljava/lang/CharSequence;

    array-length v6, v5

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v6, :cond_1

    aget-object v0, v5, v4

    iget-object v7, p0, Landroidx/preference/TilePreferenceCategory;->mSettingsValues:[Ljava/lang/String;

    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    if-ge v2, v3, :cond_0

    iget-object v7, p0, Landroidx/preference/TilePreferenceCategory;->mEntryValues:[Ljava/lang/CharSequence;

    aput-object v0, v7, v2

    iget-object v7, p0, Landroidx/preference/TilePreferenceCategory;->mEntries:[Ljava/lang/CharSequence;

    iget-object v8, p0, Landroidx/preference/TilePreferenceCategory;->mAllEntries:[Ljava/lang/CharSequence;

    aget-object v8, v8, v1

    aput-object v8, v7, v2

    add-int/lit8 v2, v2, 0x1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private setEntriesAndValues()V
    .locals 8

    const/4 v7, 0x0

    iget-object v4, p0, Landroidx/preference/TilePreferenceCategory;->mPrefs:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroidx/preference/TileDropDownPreference;

    iget-object v4, p0, Landroidx/preference/TilePreferenceCategory;->mEntryValues:[Ljava/lang/CharSequence;

    invoke-virtual {v4}, [Ljava/lang/CharSequence;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    iget-object v4, p0, Landroidx/preference/TilePreferenceCategory;->mEntries:[Ljava/lang/CharSequence;

    invoke-virtual {v4}, [Ljava/lang/CharSequence;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/CharSequence;

    iget-object v4, p0, Landroidx/preference/TilePreferenceCategory;->mSettingsValues:[Ljava/lang/String;

    invoke-direct {p0, v2}, Landroidx/preference/TilePreferenceCategory;->getPrefKey(Landroidx/preference/Preference;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    aget-object v3, v4, v6

    aput-object v3, v0, v7

    const-string/jumbo v4, "none"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string/jumbo v4, "none"

    :goto_1
    aput-object v4, v1, v7

    invoke-virtual {v2, v0}, Landroidx/preference/TileDropDownPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v1}, Landroidx/preference/TileDropDownPreference;->setEntries([Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v7}, Landroidx/preference/TileDropDownPreference;->setValueIndex(I)V

    invoke-virtual {v2, v3}, Landroidx/preference/TileDropDownPreference;->setValue(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v4, p0, Landroidx/preference/TilePreferenceCategory;->mAllEntries:[Ljava/lang/CharSequence;

    iget-object v6, p0, Landroidx/preference/TilePreferenceCategory;->mAllEntryValues:[Ljava/lang/CharSequence;

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v6

    aget-object v4, v4, v6

    goto :goto_1

    :cond_1
    return-void
.end method

.method private updateSettings()V
    .locals 7

    const/16 v6, 0x8

    const/4 v5, 0x2

    const-string v3, "big_tile_column"

    invoke-static {v3, v5}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;I)I

    move-result v0

    iget-object v3, p0, Landroidx/preference/TilePreferenceCategory;->mAllEntryValues:[Ljava/lang/CharSequence;

    array-length v3, v3

    mul-int/lit8 v4, v0, 0x2

    add-int/lit8 v4, v4, -0x1

    sub-int/2addr v3, v4

    iput v3, p0, Landroidx/preference/TilePreferenceCategory;->mLenght:I

    new-array v2, v6, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "cell"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "bt"

    aput-object v4, v2, v3

    const-string/jumbo v3, "mute"

    aput-object v3, v2, v5

    const/4 v3, 0x3

    const-string/jumbo v4, "sync"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "hotspot"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string/jumbo v4, "sync"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "screenbutton"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "nfc"

    aput-object v4, v2, v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v6, :cond_1

    iget-object v4, p0, Landroidx/preference/TilePreferenceCategory;->mSettingsValues:[Ljava/lang/String;

    mul-int/lit8 v3, v0, 0x2

    if-ge v1, v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "big_tile_position_"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aget-object v5, v2, v1

    invoke-static {v3, v5}, Landroid/preference/SettingsEliteHelper;->getStringofSettings(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_1
    aput-object v3, v4, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const-string/jumbo v3, "none"

    goto :goto_1

    :cond_1
    return-void
.end method


# virtual methods
.method public onDependencyChanged(Landroidx/preference/Preference;Z)V
    .locals 0

    invoke-direct {p0}, Landroidx/preference/TilePreferenceCategory;->updateSettings()V

    invoke-direct {p0}, Landroidx/preference/TilePreferenceCategory;->init()V

    invoke-direct {p0}, Landroidx/preference/TilePreferenceCategory;->setEntriesAndValues()V

    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)V
    .locals 3

    invoke-direct {p0, p1}, Landroidx/preference/TilePreferenceCategory;->getPrefKey(Landroidx/preference/Preference;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Landroidx/preference/TilePreferenceCategory;->mSettingsValues:[Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "big_tile_position_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    check-cast p2, Ljava/lang/String;

    invoke-static {v1, p2}, Landroid/preference/SettingsEliteHelper;->putStringinSettings(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Landroidx/preference/TilePreferenceCategory;->updateSettings()V

    invoke-direct {p0}, Landroidx/preference/TilePreferenceCategory;->init()V

    invoke-direct {p0}, Landroidx/preference/TilePreferenceCategory;->setEntriesAndValues()V

    invoke-static {}, Landroid/preference/CustomUpdater;->getInstance()Landroid/preference/CustomUpdater;

    move-result-object v1

    const-string v2, "big_tile_position"

    invoke-virtual {v1, v2}, Landroid/preference/CustomUpdater;->beginChange(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected onPrepareAddPreference(Landroidx/preference/Preference;)Z
    .locals 4

    move-object v1, p1

    check-cast v1, Landroidx/preference/TileDropDownPreference;

    invoke-super {p0, v1}, Landroidx/preference/PreferenceCategory;->onPrepareAddPreference(Landroidx/preference/Preference;)Z

    move-result v0

    iget-object v2, p0, Landroidx/preference/TilePreferenceCategory;->mPrefs:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, p0}, Landroidx/preference/TileDropDownPreference;->setOnChangeListener(Landroidx/preference/TileDropDownPreference$OnChangeListener;)V

    iget-object v2, p0, Landroidx/preference/TilePreferenceCategory;->mPrefs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_0

    invoke-direct {p0}, Landroidx/preference/TilePreferenceCategory;->setEntriesAndValues()V

    :cond_0
    return v0
.end method
