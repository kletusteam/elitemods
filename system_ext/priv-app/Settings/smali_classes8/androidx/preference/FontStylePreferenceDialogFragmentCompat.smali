.class public Landroidx/preference/FontStylePreferenceDialogFragmentCompat;
.super Landroidx/preference/PreferenceDialogFragmentCompat;
.source "FontStylePreferenceDialogFragmentCompat.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/preference/FontStylePreferenceDialogFragmentCompat$LoadFile;,
        Landroidx/preference/FontStylePreferenceDialogFragmentCompat$FileListAdapter;
    }
.end annotation


# static fields
.field static TAG:Ljava/lang/String;


# instance fields
.field private mAppListAdapter:Landroidx/preference/FontStylePreferenceDialogFragmentCompat$FileListAdapter;

.field private mListView:Landroid/widget/ListView;

.field private mLoadFile:Landroidx/preference/FontStylePreferenceDialogFragmentCompat$LoadFile;

.field private mProgressBar:Landroid/widget/LinearLayout;

.field private mValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "MiuiPreferenceHelper"

    sput-object v0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroidx/preference/PreferenceDialogFragmentCompat;-><init>()V

    return-void
.end method

.method static synthetic access$100(Landroidx/preference/FontStylePreferenceDialogFragmentCompat;)Landroidx/preference/FontStylePreferenceDialogFragmentCompat$FileListAdapter;
    .locals 1

    iget-object v0, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mAppListAdapter:Landroidx/preference/FontStylePreferenceDialogFragmentCompat$FileListAdapter;

    return-object v0
.end method

.method static synthetic access$102(Landroidx/preference/FontStylePreferenceDialogFragmentCompat;Landroidx/preference/FontStylePreferenceDialogFragmentCompat$FileListAdapter;)Landroidx/preference/FontStylePreferenceDialogFragmentCompat$FileListAdapter;
    .locals 0

    iput-object p1, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mAppListAdapter:Landroidx/preference/FontStylePreferenceDialogFragmentCompat$FileListAdapter;

    return-object p1
.end method

.method static synthetic access$300(Landroidx/preference/FontStylePreferenceDialogFragmentCompat;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mProgressBar:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$400(Landroidx/preference/FontStylePreferenceDialogFragmentCompat;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method private createList()V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat$LoadFile;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat$LoadFile;-><init>(Landroidx/preference/FontStylePreferenceDialogFragmentCompat;Landroidx/preference/FontStylePreferenceDialogFragmentCompat$1;)V

    iput-object v0, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mLoadFile:Landroidx/preference/FontStylePreferenceDialogFragmentCompat$LoadFile;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {p0}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->getFontStylePreference()Landroidx/preference/MiuiFontStylePreference;

    move-result-object v2

    invoke-virtual {v2}, Landroidx/preference/MiuiFontStylePreference;->getPath()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat$LoadFile;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v1, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mProgressBar:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method

.method public static newInstance(Ljava/lang/String;)Landroidx/preference/FontStylePreferenceDialogFragmentCompat;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;

    invoke-direct {v0}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(I)V

    const-string v2, "key"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method protected getFontStylePreference()Landroidx/preference/MiuiFontStylePreference;
    .locals 1

    invoke-virtual {p0}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->getPreference()Landroidx/preference/DialogPreference;

    move-result-object v0

    check-cast v0, Landroidx/preference/MiuiFontStylePreference;

    return-object v0
.end method

.method public getTypeFase(Ljava/lang/String;)Landroid/graphics/Typeface;
    .locals 1

    invoke-virtual {p0}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->getFontStylePreference()Landroidx/preference/MiuiFontStylePreference;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroidx/preference/MiuiFontStylePreference;->getTypeFase(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    return-object v0
.end method

.method public needInputMethod()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected notifyChange()V
    .locals 1

    iget-object v0, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mAppListAdapter:Landroidx/preference/FontStylePreferenceDialogFragmentCompat$FileListAdapter;

    invoke-virtual {v0}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat$FileListAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method protected onBindDialogView(Landroid/view/View;)V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroidx/preference/PreferenceDialogFragmentCompat;->onBindDialogView(Landroid/view/View;)V

    const v0, 0x102000a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mListView:Landroid/widget/ListView;

    iget-object v0, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    iget-object v0, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFadingEdgeLength(I)V

    iget-object v0, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x1080013

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setScrollingCacheEnabled(Z)V

    invoke-virtual {p0}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "progressContainer"

    const-string v2, "android"

    invoke-static {v0, v1, v2}, Landroid/Utils/Utils;->IDtoID(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mProgressBar:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->createList()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/preference/PreferenceDialogFragmentCompat;->onCreate(Landroid/os/Bundle;)V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->getFontStylePreference()Landroidx/preference/MiuiFontStylePreference;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/preference/MiuiFontStylePreference;->getValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mValue:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    const-string v0, "FontStylePreferenceDialogFragment.text"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mValue:Ljava/lang/String;

    goto :goto_0
.end method

.method public onDialogClosed(Z)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mValue:Ljava/lang/String;

    invoke-virtual {p0}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->getFontStylePreference()Landroidx/preference/MiuiFontStylePreference;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroidx/preference/MiuiFontStylePreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->getFontStylePreference()Landroidx/preference/MiuiFontStylePreference;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroidx/preference/MiuiFontStylePreference;->onItemClick(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->getFontStylePreference()Landroidx/preference/MiuiFontStylePreference;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/preference/MiuiFontStylePreference;->setSummary()V

    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroidx/preference/PreferenceDialogFragmentCompat;->onDismiss(Landroid/content/DialogInterface;)V

    iget-object v0, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mLoadFile:Landroidx/preference/FontStylePreferenceDialogFragmentCompat$LoadFile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mLoadFile:Landroidx/preference/FontStylePreferenceDialogFragmentCompat$LoadFile;

    invoke-virtual {v0}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat$LoadFile;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mLoadFile:Landroidx/preference/FontStylePreferenceDialogFragmentCompat$LoadFile;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat$LoadFile;->cancel(Z)Z

    iput-object v2, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mLoadFile:Landroidx/preference/FontStylePreferenceDialogFragmentCompat$LoadFile;

    :cond_0
    iput-object v2, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mAppListAdapter:Landroidx/preference/FontStylePreferenceDialogFragmentCompat$FileListAdapter;

    iput-object v2, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mListView:Landroid/widget/ListView;

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v1, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mAppListAdapter:Landroidx/preference/FontStylePreferenceDialogFragmentCompat$FileListAdapter;

    invoke-virtual {v1, p3}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat$FileListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "Default"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->getFontStylePreference()Landroidx/preference/MiuiFontStylePreference;

    move-result-object v2

    invoke-virtual {v2}, Landroidx/preference/MiuiFontStylePreference;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    iput-object v0, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mValue:Ljava/lang/String;

    invoke-virtual {p0}, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.method protected onPrepareDialogBuilder(Landroidx/appcompat/app/AlertDialog$Builder;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p1, v0, v0}, Landroidx/appcompat/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    invoke-super {p0, p1}, Landroidx/preference/PreferenceDialogFragmentCompat;->onPrepareDialogBuilder(Landroidx/appcompat/app/AlertDialog$Builder;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroidx/preference/PreferenceDialogFragmentCompat;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "FontStylePreferenceDialogFragment.text"

    iget-object v1, p0, Landroidx/preference/FontStylePreferenceDialogFragmentCompat;->mValue:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    return-void
.end method
