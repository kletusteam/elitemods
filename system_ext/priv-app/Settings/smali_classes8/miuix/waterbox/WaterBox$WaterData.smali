.class Lmiuix/waterbox/WaterBox$WaterData;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/waterbox/WaterBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "WaterData"
.end annotation


# instance fields
.field private edgeRot:F

.field private effectPer:F

.field private rot:F

.field private value:F

.field private waterAlpha:F


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, 0x42b40000    # 90.0f

    iput v0, p0, Lmiuix/waterbox/WaterBox$WaterData;->edgeRot:F

    iput v0, p0, Lmiuix/waterbox/WaterBox$WaterData;->rot:F

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lmiuix/waterbox/WaterBox$WaterData;->value:F

    iput v0, p0, Lmiuix/waterbox/WaterBox$WaterData;->waterAlpha:F

    return-void
.end method


# virtual methods
.method getEdgeRot()F
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iget p0, p0, Lmiuix/waterbox/WaterBox$WaterData;->edgeRot:F

    goto/32 :goto_1

    nop

    :goto_1
    return p0
.end method

.method getEffectPer()F
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iget p0, p0, Lmiuix/waterbox/WaterBox$WaterData;->effectPer:F

    goto/32 :goto_1

    nop

    :goto_1
    return p0
.end method

.method getRot()F
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iget p0, p0, Lmiuix/waterbox/WaterBox$WaterData;->rot:F

    goto/32 :goto_1

    nop

    :goto_1
    return p0
.end method

.method getValue()F
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iget p0, p0, Lmiuix/waterbox/WaterBox$WaterData;->value:F

    goto/32 :goto_1

    nop

    :goto_1
    return p0
.end method

.method getWaterAlpha()F
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iget p0, p0, Lmiuix/waterbox/WaterBox$WaterData;->waterAlpha:F

    goto/32 :goto_1

    nop

    :goto_1
    return p0
.end method

.method setEdgeRot(F)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iput p1, p0, Lmiuix/waterbox/WaterBox$WaterData;->edgeRot:F

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method setEffectPer(F)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iput p1, p0, Lmiuix/waterbox/WaterBox$WaterData;->effectPer:F

    goto/32 :goto_0

    nop
.end method

.method setRot(F)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iput p1, p0, Lmiuix/waterbox/WaterBox$WaterData;->rot:F

    goto/32 :goto_0

    nop
.end method

.method setValue(F)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iput p1, p0, Lmiuix/waterbox/WaterBox$WaterData;->value:F

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method setWaterAlpha(F)V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    cmpg-float v1, p1, v0

    goto/32 :goto_6

    nop

    :goto_1
    iput p1, p0, Lmiuix/waterbox/WaterBox$WaterData;->waterAlpha:F

    goto/32 :goto_5

    nop

    :goto_2
    const/4 v0, 0x0

    goto/32 :goto_0

    nop

    :goto_3
    move p1, v0

    :goto_4
    goto/32 :goto_1

    nop

    :goto_5
    return-void

    :goto_6
    if-ltz v1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop
.end method
