.class public Lsrc/com/android/settings/deviceinfo/aboutphone/MiuiBatteryStatus;
.super Ljava/lang/Object;


# instance fields
.field public chargeDeviceType:I

.field public chargeSpeed:I

.field public health:I

.field public level:I

.field public maxChargingWattage:I

.field public plugged:I

.field public status:I

.field public wireState:I


# direct methods
.method public constructor <init>(IIIIIIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lsrc/com/android/settings/deviceinfo/aboutphone/MiuiBatteryStatus;->status:I

    iput p2, p0, Lsrc/com/android/settings/deviceinfo/aboutphone/MiuiBatteryStatus;->plugged:I

    iput p3, p0, Lsrc/com/android/settings/deviceinfo/aboutphone/MiuiBatteryStatus;->level:I

    iput p4, p0, Lsrc/com/android/settings/deviceinfo/aboutphone/MiuiBatteryStatus;->wireState:I

    iput p5, p0, Lsrc/com/android/settings/deviceinfo/aboutphone/MiuiBatteryStatus;->chargeSpeed:I

    iput p6, p0, Lsrc/com/android/settings/deviceinfo/aboutphone/MiuiBatteryStatus;->chargeDeviceType:I

    iput p7, p0, Lsrc/com/android/settings/deviceinfo/aboutphone/MiuiBatteryStatus;->health:I

    iput p8, p0, Lsrc/com/android/settings/deviceinfo/aboutphone/MiuiBatteryStatus;->maxChargingWattage:I

    return-void
.end method

.method public static getMaxChargingWattage(Landroid/content/Intent;)I
    .locals 3

    const-string v0, "max_charging_current"

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string v2, "max_charging_voltage"

    invoke-virtual {p0, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p0

    if-gtz p0, :cond_0

    const p0, 0x4c4b40

    :cond_0
    if-lez v0, :cond_1

    div-int/lit16 v0, v0, 0x3e8

    div-int/lit16 p0, p0, 0x3e8

    mul-int v1, v0, p0

    :cond_1
    return v1
.end method
