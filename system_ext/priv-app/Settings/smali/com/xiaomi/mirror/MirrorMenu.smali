.class public Lcom/xiaomi/mirror/MirrorMenu;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/mirror/MirrorMenu$PcOpenBuilder;,
        Lcom/xiaomi/mirror/MirrorMenu$NewDisplayOpenBuilder;,
        Lcom/xiaomi/mirror/MirrorMenu$Builder;,
        Lcom/xiaomi/mirror/MirrorMenu$Callback;
    }
.end annotation


# static fields
.field static final TYPE_COMMON:I = 0x0

.field static final TYPE_NEW_DISPLAY_OPEN:I = 0x1

.field static final TYPE_PC_OPEN:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getLabel()Ljava/lang/CharSequence;
    .locals 1

    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Stub!"

    invoke-direct {p0, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method getListener()Lcom/xiaomi/mirror/MirrorMenu$Callback;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-direct {p0, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_2

    nop

    :goto_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    goto/32 :goto_3

    nop

    :goto_2
    throw p0

    :goto_3
    const-string v0, "Stub!"

    goto/32 :goto_0

    nop
.end method

.method getPendingIntent()Landroid/app/PendingIntent;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    goto/32 :goto_2

    nop

    :goto_1
    invoke-direct {p0, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_3

    nop

    :goto_2
    const-string v0, "Stub!"

    goto/32 :goto_1

    nop

    :goto_3
    throw p0
.end method

.method getType()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    goto/32 :goto_2

    nop

    :goto_1
    invoke-direct {p0, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_3

    nop

    :goto_2
    const-string v0, "Stub!"

    goto/32 :goto_1

    nop

    :goto_3
    throw p0
.end method

.method getUri()Landroid/net/Uri;
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-direct {p0, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_1

    nop

    :goto_1
    throw p0

    :goto_2
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    goto/32 :goto_3

    nop

    :goto_3
    const-string v0, "Stub!"

    goto/32 :goto_0

    nop
.end method

.method needCallRemote()Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    goto/32 :goto_2

    nop

    :goto_1
    throw p0

    :goto_2
    const-string v0, "Stub!"

    goto/32 :goto_3

    nop

    :goto_3
    invoke-direct {p0, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_1

    nop
.end method
