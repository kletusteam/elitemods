.class final Lcom/iqiyi/android/qigsaw/core/extension/SplitComponentInfoProvider;
.super Ljava/lang/Object;


# instance fields
.field private final splitNames:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/iqiyi/android/qigsaw/core/extension/SplitComponentInfoProvider;->splitNames:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method getSplitActivitiesMap()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    goto/32 :goto_b

    nop

    :goto_0
    const/4 v1, 0x0

    goto/32 :goto_8

    nop

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_10

    nop

    :goto_2
    new-instance v3, Ljava/util/ArrayList;

    goto/32 :goto_13

    nop

    :goto_3
    return-object v0

    :goto_4
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_5
    goto/32 :goto_d

    nop

    :goto_6
    if-nez v2, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_11

    nop

    :goto_7
    if-gtz v3, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_2

    nop

    :goto_8
    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    goto/32 :goto_c

    nop

    :goto_9
    if-nez v1, :cond_2

    goto/32 :goto_f

    :cond_2
    goto/32 :goto_1

    nop

    :goto_a
    invoke-static {v1}, Lcom/iqiyi/android/qigsaw/core/extension/ComponentInfoManager;->getSplitActivities(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_6

    nop

    :goto_b
    new-instance v0, Ljava/util/HashMap;

    goto/32 :goto_0

    nop

    :goto_c
    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/SplitComponentInfoProvider;->splitNames:Ljava/util/Set;

    goto/32 :goto_4

    nop

    :goto_d
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_9

    nop

    :goto_e
    goto :goto_5

    :goto_f
    goto/32 :goto_3

    nop

    :goto_10
    check-cast v1, Ljava/lang/String;

    goto/32 :goto_a

    nop

    :goto_11
    array-length v3, v2

    goto/32 :goto_7

    nop

    :goto_12
    invoke-static {v3, v2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    goto/32 :goto_14

    nop

    :goto_13
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_12

    nop

    :goto_14
    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_e

    nop
.end method

.method getSplitApplicationName(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-static {p1}, Lcom/iqiyi/android/qigsaw/core/extension/ComponentInfoManager;->getSplitApplication(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_1

    nop

    :goto_1
    return-object p0
.end method

.method getSplitReceivers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    goto/32 :goto_10

    nop

    :goto_0
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    goto/32 :goto_e

    nop

    :goto_2
    check-cast v1, Ljava/lang/String;

    goto/32 :goto_f

    nop

    :goto_3
    if-nez v1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_8

    nop

    :goto_4
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_d

    nop

    :goto_5
    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    goto/32 :goto_6

    nop

    :goto_6
    goto :goto_1

    :goto_7
    goto/32 :goto_c

    nop

    :goto_8
    array-length v2, v1

    goto/32 :goto_a

    nop

    :goto_9
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_2

    nop

    :goto_a
    if-gtz v2, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_5

    nop

    :goto_b
    if-nez v1, :cond_2

    goto/32 :goto_7

    :cond_2
    goto/32 :goto_9

    nop

    :goto_c
    return-object v0

    :goto_d
    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/SplitComponentInfoProvider;->splitNames:Ljava/util/Set;

    goto/32 :goto_0

    nop

    :goto_e
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_b

    nop

    :goto_f
    invoke-static {v1}, Lcom/iqiyi/android/qigsaw/core/extension/ComponentInfoManager;->getSplitReceivers(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_3

    nop

    :goto_10
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_4

    nop
.end method

.method getSplitServices()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    goto/32 :goto_4

    nop

    :goto_0
    if-nez v1, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_f

    nop

    :goto_1
    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/SplitComponentInfoProvider;->splitNames:Ljava/util/Set;

    goto/32 :goto_9

    nop

    :goto_2
    if-gtz v2, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_c

    nop

    :goto_3
    if-nez v1, :cond_2

    goto/32 :goto_6

    :cond_2
    goto/32 :goto_10

    nop

    :goto_4
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_e

    nop

    :goto_5
    goto :goto_a

    :goto_6
    goto/32 :goto_8

    nop

    :goto_7
    check-cast v1, Ljava/lang/String;

    goto/32 :goto_d

    nop

    :goto_8
    return-object v0

    :goto_9
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_a
    goto/32 :goto_b

    nop

    :goto_b
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_3

    nop

    :goto_c
    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    goto/32 :goto_5

    nop

    :goto_d
    invoke-static {v1}, Lcom/iqiyi/android/qigsaw/core/extension/ComponentInfoManager;->getSplitServices(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_e
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_1

    nop

    :goto_f
    array-length v2, v1

    goto/32 :goto_2

    nop

    :goto_10
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_7

    nop
.end method
