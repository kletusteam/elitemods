.class public abstract Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;
.super Landroid/content/ContentProvider;


# static fields
.field private static final NAME_INFIX:Ljava/lang/String; = "_Decorated_"


# instance fields
.field private providerInfo:Landroid/content/pm/ProviderInfo;

.field private realContentProvider:Landroid/content/ContentProvider;

.field private realContentProviderClassName:Ljava/lang/String;

.field private splitName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/content/ContentProviderOperation;",
            ">;)[",
            "Landroid/content/ContentProviderResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    iget-object v0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->splitName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->checkRealContentProviderInstallStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    invoke-virtual {p0, p1}, Landroid/content/ContentProvider;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-super {p0, p1}, Landroid/content/ContentProvider;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object p0

    return-object p0
.end method

.method public attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V
    .locals 2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "_Decorated_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProviderClassName:Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->splitName:Ljava/lang/String;

    invoke-super {p0, p1, p2}, Landroid/content/ContentProvider;->attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V

    new-instance p1, Landroid/content/pm/ProviderInfo;

    invoke-direct {p1, p2}, Landroid/content/pm/ProviderInfo;-><init>(Landroid/content/pm/ProviderInfo;)V

    iput-object p1, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->providerInfo:Landroid/content/pm/ProviderInfo;

    invoke-static {}, Lcom/iqiyi/android/qigsaw/core/extension/AABExtension;->getInstance()Lcom/iqiyi/android/qigsaw/core/extension/AABExtension;

    move-result-object p1

    iget-object p2, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->splitName:Ljava/lang/String;

    invoke-virtual {p1, p2, p0}, Lcom/iqiyi/android/qigsaw/core/extension/AABExtension;->put(Ljava/lang/String;Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;)V

    return-void
.end method

.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 1

    iget-object v0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->splitName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->checkRealContentProviderInstallStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    invoke-virtual {p0, p1, p2}, Landroid/content/ContentProvider;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result p0

    return p0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/content/ContentProvider;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result p0

    return p0
.end method

.method public call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->splitName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->checkRealContentProviderInstallStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    invoke-virtual {p0, p1, p2, p3}, Landroid/content/ContentProvider;->call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/content/ContentProvider;->call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object p0

    return-object p0
.end method

.method public canonicalize(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 1

    invoke-virtual {p0}, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->getRealContentProvider()Landroid/content/ContentProvider;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    invoke-virtual {p0, p1}, Landroid/content/ContentProvider;->canonicalize(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-super {p0, p1}, Landroid/content/ContentProvider;->canonicalize(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object p0

    return-object p0
.end method

.method protected abstract checkRealContentProviderInstallStatus(Ljava/lang/String;)Z
.end method

.method createAndActivateRealContentProvider(Ljava/lang/ClassLoader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/iqiyi/android/qigsaw/core/extension/AABExtensionException;
        }
    .end annotation

    goto/32 :goto_12

    nop

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_16

    nop

    :goto_1
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_15

    nop

    :goto_2
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_1

    nop

    :goto_3
    new-instance p1, Lcom/iqiyi/android/qigsaw/core/extension/AABExtensionException;

    goto/32 :goto_4

    nop

    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_17

    nop

    :goto_5
    invoke-direct {p0, v1}, Lcom/iqiyi/android/qigsaw/core/extension/AABExtensionException;-><init>(Ljava/lang/Throwable;)V

    goto/32 :goto_d

    nop

    :goto_6
    throw p1

    :goto_7
    const-string v1, "Unable to read real content-provider for "

    goto/32 :goto_0

    nop

    :goto_8
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1, v0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/ContentProvider;

    iput-object p1, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    invoke-virtual {p0}, Landroid/content/ContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->providerInfo:Landroid/content/pm/ProviderInfo;

    invoke-virtual {p1, v0, p0}, Landroid/content/ContentProvider;->attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_f

    nop

    :goto_9
    return-void

    :goto_a
    goto/32 :goto_c

    nop

    :goto_b
    if-nez v0, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_8

    nop

    :goto_c
    new-instance p0, Lcom/iqiyi/android/qigsaw/core/extension/AABExtensionException;

    goto/32 :goto_5

    nop

    :goto_d
    throw p0

    :goto_e
    goto/32 :goto_3

    nop

    :goto_f
    goto :goto_11

    :catch_0
    move-exception p0

    goto/32 :goto_10

    nop

    :goto_10
    move-object v1, p0

    :goto_11
    goto/32 :goto_14

    nop

    :goto_12
    iget-object v0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProviderClassName:Ljava/lang/String;

    goto/32 :goto_b

    nop

    :goto_13
    invoke-direct {p1, p0}, Lcom/iqiyi/android/qigsaw/core/extension/AABExtensionException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_6

    nop

    :goto_14
    if-eqz v1, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_9

    nop

    :goto_15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_13

    nop

    :goto_16
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    goto/32 :goto_2

    nop

    :goto_17
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_7

    nop
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    iget-object v0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->splitName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->checkRealContentProviderInstallStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    invoke-virtual {p0, p1, p2, p3}, Landroid/content/ContentProvider;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result p0

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method protected getRealContentProvider()Landroid/content/ContentProvider;
    .locals 0

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    return-object p0
.end method

.method public getStreamTypes(Landroid/net/Uri;Ljava/lang/String;)[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->splitName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->checkRealContentProviderInstallStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    invoke-virtual {p0, p1, p2}, Landroid/content/ContentProvider;->getStreamTypes(Landroid/net/Uri;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/content/ContentProvider;->getStreamTypes(Landroid/net/Uri;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->splitName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->checkRealContentProviderInstallStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    invoke-virtual {p0, p1}, Landroid/content/ContentProvider;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->splitName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->checkRealContentProviderInstallStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    invoke-virtual {p0, p1, p2}, Landroid/content/ContentProvider;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/content/ContentProvider;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->splitName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->checkRealContentProviderInstallStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    invoke-virtual {p0, p1}, Landroid/content/ContentProvider;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    :cond_0
    return-void
.end method

.method public onCreate()Z
    .locals 0

    const/4 p0, 0x1

    return p0
.end method

.method public onLowMemory()V
    .locals 0

    invoke-super {p0}, Landroid/content/ContentProvider;->onLowMemory()V

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/ContentProvider;->onLowMemory()V

    :cond_0
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 0

    invoke-super {p0, p1}, Landroid/content/ContentProvider;->onTrimMemory(I)V

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Landroid/content/ContentProvider;->onTrimMemory(I)V

    :cond_0
    return-void
.end method

.method public openAssetFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    iget-object v0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->splitName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->checkRealContentProviderInstallStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    invoke-virtual {p0, p1, p2}, Landroid/content/ContentProvider;->openAssetFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/content/ContentProvider;->openAssetFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object p0

    return-object p0
.end method

.method public openAssetFile(Landroid/net/Uri;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/content/res/AssetFileDescriptor;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    iget-object v0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->splitName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->checkRealContentProviderInstallStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    invoke-virtual {p0, p1, p2, p3}, Landroid/content/ContentProvider;->openAssetFile(Landroid/net/Uri;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/content/res/AssetFileDescriptor;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/content/ContentProvider;->openAssetFile(Landroid/net/Uri;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/content/res/AssetFileDescriptor;

    move-result-object p0

    return-object p0
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    iget-object v0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->splitName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->checkRealContentProviderInstallStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    invoke-virtual {p0, p1, p2}, Landroid/content/ContentProvider;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/content/ContentProvider;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object p0

    return-object p0
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/os/ParcelFileDescriptor;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    iget-object v0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->splitName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->checkRealContentProviderInstallStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    invoke-virtual {p0, p1, p2, p3}, Landroid/content/ContentProvider;->openFile(Landroid/net/Uri;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/os/ParcelFileDescriptor;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/content/ContentProvider;->openFile(Landroid/net/Uri;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/os/ParcelFileDescriptor;

    move-result-object p0

    return-object p0
.end method

.method public openPipeHelper(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Object;Landroid/content/ContentProvider$PipeDataWriter;)Landroid/os/ParcelFileDescriptor;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "TT;",
            "Landroid/content/ContentProvider$PipeDataWriter<",
            "TT;>;)",
            "Landroid/os/ParcelFileDescriptor;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    iget-object v0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->splitName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->checkRealContentProviderInstallStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentProvider;->openPipeHelper(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Object;Landroid/content/ContentProvider$PipeDataWriter;)Landroid/os/ParcelFileDescriptor;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-super/range {p0 .. p5}, Landroid/content/ContentProvider;->openPipeHelper(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Object;Landroid/content/ContentProvider$PipeDataWriter;)Landroid/os/ParcelFileDescriptor;

    move-result-object p0

    return-object p0
.end method

.method public openTypedAssetFile(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/res/AssetFileDescriptor;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    iget-object v0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->splitName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->checkRealContentProviderInstallStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    invoke-virtual {p0, p1, p2, p3}, Landroid/content/ContentProvider;->openTypedAssetFile(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/res/AssetFileDescriptor;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/content/ContentProvider;->openTypedAssetFile(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/res/AssetFileDescriptor;

    move-result-object p0

    return-object p0
.end method

.method public openTypedAssetFile(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/CancellationSignal;)Landroid/content/res/AssetFileDescriptor;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    iget-object v0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->splitName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->checkRealContentProviderInstallStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/content/ContentProvider;->openTypedAssetFile(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/CancellationSignal;)Landroid/content/res/AssetFileDescriptor;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/content/ContentProvider;->openTypedAssetFile(Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/CancellationSignal;)Landroid/content/res/AssetFileDescriptor;

    move-result-object p0

    return-object p0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Landroid/os/Bundle;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    .locals 1

    iget-object v0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->splitName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->checkRealContentProviderInstallStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/content/ContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Landroid/os/Bundle;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/content/ContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Landroid/os/Bundle;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object p0

    return-object p0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7

    iget-object v0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->splitName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->checkRealContentProviderInstallStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    .locals 8

    iget-object v0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->splitName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->checkRealContentProviderInstallStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v1 .. v7}, Landroid/content/ContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-super/range {p0 .. p6}, Landroid/content/ContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object p0

    return-object p0
.end method

.method public refresh(Landroid/net/Uri;Landroid/os/Bundle;Landroid/os/CancellationSignal;)Z
    .locals 1

    iget-object v0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->splitName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->checkRealContentProviderInstallStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    invoke-virtual {p0, p1, p2, p3}, Landroid/content/ContentProvider;->refresh(Landroid/net/Uri;Landroid/os/Bundle;Landroid/os/CancellationSignal;)Z

    move-result p0

    return p0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/content/ContentProvider;->refresh(Landroid/net/Uri;Landroid/os/Bundle;Landroid/os/CancellationSignal;)Z

    move-result p0

    return p0
.end method

.method public uncanonicalize(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->splitName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->checkRealContentProviderInstallStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    invoke-virtual {p0, p1}, Landroid/content/ContentProvider;->uncanonicalize(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-super {p0, p1}, Landroid/content/ContentProvider;->uncanonicalize(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object p0

    return-object p0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    iget-object v0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->splitName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->checkRealContentProviderInstallStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/extension/ContentProviderProxy;->realContentProvider:Landroid/content/ContentProvider;

    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/content/ContentProvider;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result p0

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method
