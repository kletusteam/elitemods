.class final Lcom/iqiyi/android/qigsaw/core/splitload/SplitApplicationLoaders;
.super Ljava/lang/Object;


# static fields
.field private static final sInstance:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Lcom/iqiyi/android/qigsaw/core/splitload/SplitApplicationLoaders;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final splitDexClassLoaders:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/iqiyi/android/qigsaw/core/splitload/SplitDexClassLoader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    sput-object v0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitApplicationLoaders;->sInstance:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitApplicationLoaders;->splitDexClassLoaders:Ljava/util/Set;

    return-void
.end method

.method public static getInstance()Lcom/iqiyi/android/qigsaw/core/splitload/SplitApplicationLoaders;
    .locals 2

    sget-object v0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitApplicationLoaders;->sInstance:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/iqiyi/android/qigsaw/core/splitload/SplitApplicationLoaders;

    invoke-direct {v1}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitApplicationLoaders;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitApplicationLoaders;

    return-object v0
.end method


# virtual methods
.method addClassLoader(Lcom/iqiyi/android/qigsaw/core/splitload/SplitDexClassLoader;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitApplicationLoaders;->splitDexClassLoaders:Ljava/util/Set;

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    invoke-interface {p0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/32 :goto_1

    nop
.end method

.method getClassLoader(Ljava/lang/String;)Lcom/iqiyi/android/qigsaw/core/splitload/SplitDexClassLoader;
    .locals 2

    goto/32 :goto_9

    nop

    :goto_0
    if-nez v1, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_2

    nop

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_2
    return-object v0

    :goto_3
    goto/32 :goto_b

    nop

    :goto_4
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_5
    goto/32 :goto_a

    nop

    :goto_6
    invoke-virtual {v0}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitDexClassLoader;->moduleName()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_d

    nop

    :goto_7
    return-object p0

    :goto_8
    check-cast v0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitDexClassLoader;

    goto/32 :goto_6

    nop

    :goto_9
    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitApplicationLoaders;->splitDexClassLoaders:Ljava/util/Set;

    goto/32 :goto_4

    nop

    :goto_a
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    goto/32 :goto_c

    nop

    :goto_b
    const/4 p0, 0x0

    goto/32 :goto_7

    nop

    :goto_c
    if-nez v0, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_1

    nop

    :goto_d
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto/32 :goto_0

    nop
.end method

.method getValidClassLoader(Ljava/lang/String;)Lcom/iqiyi/android/qigsaw/core/splitload/SplitDexClassLoader;
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {v0}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitDexClassLoader;->isValid()Z

    move-result v1

    goto/32 :goto_5

    nop

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    goto/32 :goto_6

    nop

    :goto_2
    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitApplicationLoaders;->splitDexClassLoaders:Ljava/util/Set;

    goto/32 :goto_a

    nop

    :goto_3
    if-nez v1, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_0

    nop

    :goto_4
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_d

    nop

    :goto_5
    if-nez v1, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_7

    nop

    :goto_6
    if-nez v0, :cond_2

    goto/32 :goto_8

    :cond_2
    goto/32 :goto_4

    nop

    :goto_7
    return-object v0

    :goto_8
    goto/32 :goto_c

    nop

    :goto_9
    invoke-virtual {v0}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitDexClassLoader;->moduleName()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_f

    nop

    :goto_a
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_b
    goto/32 :goto_1

    nop

    :goto_c
    const/4 p0, 0x0

    goto/32 :goto_e

    nop

    :goto_d
    check-cast v0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitDexClassLoader;

    goto/32 :goto_9

    nop

    :goto_e
    return-object p0

    :goto_f
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto/32 :goto_3

    nop
.end method

.method getValidClassLoaders()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/iqiyi/android/qigsaw/core/splitload/SplitDexClassLoader;",
            ">;"
        }
    .end annotation

    goto/32 :goto_5

    nop

    :goto_0
    goto :goto_e

    :goto_1
    goto/32 :goto_4

    nop

    :goto_2
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/32 :goto_0

    nop

    :goto_3
    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitApplicationLoaders;->splitDexClassLoaders:Ljava/util/Set;

    goto/32 :goto_d

    nop

    :goto_4
    return-object v0

    :goto_5
    new-instance v0, Ljava/util/HashSet;

    goto/32 :goto_6

    nop

    :goto_6
    iget-object v1, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitApplicationLoaders;->splitDexClassLoaders:Ljava/util/Set;

    goto/32 :goto_7

    nop

    :goto_7
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    goto/32 :goto_10

    nop

    :goto_8
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_9

    nop

    :goto_9
    if-nez v1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_a

    nop

    :goto_a
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_b

    nop

    :goto_b
    check-cast v1, Lcom/iqiyi/android/qigsaw/core/splitload/SplitDexClassLoader;

    goto/32 :goto_f

    nop

    :goto_c
    if-nez v2, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_2

    nop

    :goto_d
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_e
    goto/32 :goto_8

    nop

    :goto_f
    invoke-virtual {v1}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitDexClassLoader;->isValid()Z

    move-result v2

    goto/32 :goto_c

    nop

    :goto_10
    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    goto/32 :goto_3

    nop
.end method

.method getValidClassLoaders(Ljava/util/List;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Set<",
            "Lcom/iqiyi/android/qigsaw/core/splitload/SplitDexClassLoader;",
            ">;"
        }
    .end annotation

    goto/32 :goto_12

    nop

    :goto_0
    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    goto/32 :goto_13

    nop

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_5

    nop

    :goto_2
    if-nez v2, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_8

    nop

    :goto_3
    invoke-virtual {v1}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitDexClassLoader;->moduleName()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_0

    nop

    :goto_4
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_c

    nop

    :goto_5
    if-nez v1, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_4

    nop

    :goto_6
    goto :goto_10

    :goto_7
    goto/32 :goto_14

    nop

    :goto_8
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/32 :goto_6

    nop

    :goto_9
    invoke-virtual {v1}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitDexClassLoader;->isValid()Z

    move-result v2

    goto/32 :goto_2

    nop

    :goto_a
    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitApplicationLoaders;->splitDexClassLoaders:Ljava/util/Set;

    goto/32 :goto_f

    nop

    :goto_b
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    goto/32 :goto_11

    nop

    :goto_c
    check-cast v1, Lcom/iqiyi/android/qigsaw/core/splitload/SplitDexClassLoader;

    goto/32 :goto_3

    nop

    :goto_d
    const/4 p0, 0x0

    goto/32 :goto_15

    nop

    :goto_e
    new-instance v0, Ljava/util/HashSet;

    goto/32 :goto_b

    nop

    :goto_f
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_10
    goto/32 :goto_1

    nop

    :goto_11
    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    goto/32 :goto_a

    nop

    :goto_12
    if-eqz p1, :cond_2

    goto/32 :goto_16

    :cond_2
    goto/32 :goto_d

    nop

    :goto_13
    if-nez v2, :cond_3

    goto/32 :goto_10

    :cond_3
    goto/32 :goto_9

    nop

    :goto_14
    return-object v0

    :goto_15
    return-object p0

    :goto_16
    goto/32 :goto_e

    nop
.end method
