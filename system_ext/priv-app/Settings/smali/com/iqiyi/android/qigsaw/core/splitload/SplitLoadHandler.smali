.class final Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler$OnSplitLoadFinishListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SplitLoadHandler"


# instance fields
.field private final activator:Lcom/iqiyi/android/qigsaw/core/splitload/SplitActivator;

.field private final infoManager:Lcom/iqiyi/android/qigsaw/core/splitrequest/splitinfo/SplitInfoManager;

.field private final loadManager:Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;

.field private final mainHandler:Landroid/os/Handler;

.field private final mapper:Lcom/iqiyi/android/qigsaw/core/splitload/compat/NativePathMapper;

.field private final splitFileIntents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private final splitLoader:Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoaderWrapper;


# direct methods
.method constructor <init>(Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoaderWrapper;Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoaderWrapper;",
            "Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;",
            "Ljava/util/List<",
            "Landroid/content/Intent;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->splitLoader:Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoaderWrapper;

    iput-object p2, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->loadManager:Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;

    iput-object p3, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->splitFileIntents:Ljava/util/List;

    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p3

    invoke-direct {p1, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p1, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->mainHandler:Landroid/os/Handler;

    invoke-static {}, Lcom/iqiyi/android/qigsaw/core/splitrequest/splitinfo/SplitInfoManagerService;->getInstance()Lcom/iqiyi/android/qigsaw/core/splitrequest/splitinfo/SplitInfoManager;

    move-result-object p1

    iput-object p1, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->infoManager:Lcom/iqiyi/android/qigsaw/core/splitrequest/splitinfo/SplitInfoManager;

    new-instance p1, Lcom/iqiyi/android/qigsaw/core/splitload/SplitActivator;

    invoke-virtual {p2}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-direct {p1, p3}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitActivator;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->activator:Lcom/iqiyi/android/qigsaw/core/splitload/SplitActivator;

    new-instance p1, Lcom/iqiyi/android/qigsaw/core/splitload/compat/NativePathMapperImpl;

    invoke-virtual {p2}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/iqiyi/android/qigsaw/core/splitload/compat/NativePathMapperImpl;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->mapper:Lcom/iqiyi/android/qigsaw/core/splitload/compat/NativePathMapper;

    return-void
.end method

.method private activateSplit(Ljava/lang/String;Ljava/lang/String;Landroid/app/Application;Ljava/lang/ClassLoader;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadException;
        }
    .end annotation

    const-string v0, "SplitLoadHandler"

    const/4 v1, 0x0

    const/4 v2, 0x1

    :try_start_0
    iget-object v3, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->splitLoader:Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoaderWrapper;

    invoke-interface {v3, p2}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoaderWrapper;->loadResources(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    new-array v4, v2, [Ljava/lang/Object;

    aput-object p2, v4, v1

    const-string p2, "Failed to load %s resources"

    invoke-static {v0, v3, p2, v4}, Lcom/iqiyi/android/qigsaw/core/common/SplitLog;->printErrStackTrace(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    :try_start_1
    iget-object p2, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->activator:Lcom/iqiyi/android/qigsaw/core/splitload/SplitActivator;

    invoke-virtual {p2, p3}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitActivator;->attachSplitApplication(Landroid/app/Application;)V
    :try_end_1
    .catch Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadException; {:try_start_1 .. :try_end_1} :catch_3

    :try_start_2
    iget-object p2, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->activator:Lcom/iqiyi/android/qigsaw/core/splitload/SplitActivator;

    invoke-virtual {p2, p4, p1}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitActivator;->createAndActivateSplitContentProviders(Ljava/lang/ClassLoader;Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadException; {:try_start_2 .. :try_end_2} :catch_2

    :try_start_3
    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->activator:Lcom/iqiyi/android/qigsaw/core/splitload/SplitActivator;

    invoke-virtual {p0, p3}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitActivator;->invokeOnCreateForSplitApplication(Landroid/app/Application;)V
    :try_end_3
    .catch Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadException; {:try_start_3 .. :try_end_3} :catch_1

    return-void

    :catch_1
    move-exception p0

    new-array p2, v2, [Ljava/lang/Object;

    aput-object p1, p2, v1

    const-string p1, "Failed to invoke onCreate for %s application"

    invoke-static {v0, p0, p1, p2}, Lcom/iqiyi/android/qigsaw/core/common/SplitLog;->printErrStackTrace(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    throw p0

    :catch_2
    move-exception p0

    new-array p2, v2, [Ljava/lang/Object;

    aput-object p1, p2, v1

    const-string p1, "Failed to create %s content-provider "

    invoke-static {v0, p0, p1, p2}, Lcom/iqiyi/android/qigsaw/core/common/SplitLog;->printErrStackTrace(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    throw p0

    :catch_3
    move-exception p0

    new-array p2, v2, [Ljava/lang/Object;

    aput-object p1, p2, v1

    const-string p1, "Failed to attach %s application"

    invoke-static {v0, p0, p1, p2}, Lcom/iqiyi/android/qigsaw/core/common/SplitLog;->printErrStackTrace(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    throw p0
.end method

.method private checkSplitLoaded(Ljava/lang/String;)Z
    .locals 1

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->loadManager:Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;

    invoke-virtual {p0}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;->getLoadedSplits()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/iqiyi/android/qigsaw/core/splitload/Split;

    iget-object v0, v0, Lcom/iqiyi/android/qigsaw/core/splitload/Split;->splitName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_1
    const/4 p0, 0x0

    return p0
.end method

.method private loadSplits(Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler$OnSplitLoadFinishListener;)V
    .locals 22

    move-object/from16 v1, p0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-direct {v7, v5}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v6, Ljava/util/ArrayList;

    iget-object v0, v1, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->splitFileIntents:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, v1, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->splitFileIntents:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    const-string/jumbo v11, "splitName"

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iget-object v12, v1, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->infoManager:Lcom/iqiyi/android/qigsaw/core/splitrequest/splitinfo/SplitInfoManager;

    invoke-virtual/range {p0 .. p0}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-interface {v12, v13, v11}, Lcom/iqiyi/android/qigsaw/core/splitrequest/splitinfo/SplitInfoManager;->getSplitInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/iqiyi/android/qigsaw/core/splitrequest/splitinfo/SplitInfo;

    move-result-object v15

    const/4 v14, 0x1

    const-string v13, "SplitLoadHandler"

    if-nez v15, :cond_1

    new-array v0, v14, [Ljava/lang/Object;

    if-nez v11, :cond_0

    const-string v11, "null"

    :cond_0
    aput-object v11, v0, v5

    const-string v9, "Unable to get info for %s, just skip!"

    invoke-static {v13, v9, v0}, Lcom/iqiyi/android/qigsaw/core/common/SplitLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-instance v12, Lcom/iqiyi/android/qigsaw/core/splitreport/SplitBriefInfo;

    invoke-virtual {v15}, Lcom/iqiyi/android/qigsaw/core/splitrequest/splitinfo/SplitInfo;->getSplitName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v15}, Lcom/iqiyi/android/qigsaw/core/splitrequest/splitinfo/SplitInfo;->getSplitVersion()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v18, v8

    invoke-virtual {v15}, Lcom/iqiyi/android/qigsaw/core/splitrequest/splitinfo/SplitInfo;->isBuiltIn()Z

    move-result v8

    invoke-direct {v12, v5, v14, v8}, Lcom/iqiyi/android/qigsaw/core/splitreport/SplitBriefInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-direct {v1, v11}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->checkSplitLoaded(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v5, 0x1

    new-array v0, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v11, v0, v8

    const-string v5, "Split %s has been loaded!"

    invoke-static {v13, v5, v0}, Lcom/iqiyi/android/qigsaw/core/common/SplitLog;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v5, v8

    move-object/from16 v8, v18

    goto :goto_0

    :cond_2
    const/4 v5, 0x1

    const/4 v8, 0x0

    const-string v14, "apk"

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string v8, " is missing!"

    move-wide/from16 v19, v2

    const/16 v2, -0x64

    if-nez v14, :cond_3

    new-array v0, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v11, v0, v3

    const-string v3, "Failed to read split %s apk path"

    invoke-static {v13, v3, v0}, Lcom/iqiyi/android/qigsaw/core/common/SplitLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Lcom/iqiyi/android/qigsaw/core/splitreport/SplitLoadError;

    new-instance v3, Ljava/lang/Exception;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "split apk path "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v12, v2, v3}, Lcom/iqiyi/android/qigsaw/core/splitreport/SplitLoadError;-><init>(Lcom/iqiyi/android/qigsaw/core/splitreport/SplitBriefInfo;ILjava/lang/Throwable;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    move-object/from16 v8, v18

    move-wide/from16 v2, v19

    :goto_2
    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_3
    const-string v3, "dex-opt-dir"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15}, Lcom/iqiyi/android/qigsaw/core/splitrequest/splitinfo/SplitInfo;->hasDex()Z

    move-result v5

    if-eqz v5, :cond_4

    if-nez v3, :cond_4

    const/4 v5, 0x1

    new-array v0, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v11, v0, v3

    const-string v3, "Failed to %s get dex-opt-dir"

    invoke-static {v13, v3, v0}, Lcom/iqiyi/android/qigsaw/core/common/SplitLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Lcom/iqiyi/android/qigsaw/core/splitreport/SplitLoadError;

    new-instance v3, Ljava/lang/Exception;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "dex-opt-dir of "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v12, v2, v3}, Lcom/iqiyi/android/qigsaw/core/splitreport/SplitLoadError;-><init>(Lcom/iqiyi/android/qigsaw/core/splitreport/SplitBriefInfo;ILjava/lang/Throwable;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    const-string v5, "native-lib-dir"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v15, v2}, Lcom/iqiyi/android/qigsaw/core/splitrequest/splitinfo/SplitInfo;->getPrimaryLibData(Landroid/content/Context;)Lcom/iqiyi/android/qigsaw/core/splitrequest/splitinfo/SplitInfo$LibData;

    move-result-object v2

    if-eqz v2, :cond_5

    if-nez v5, :cond_5

    const-string v0, "Failed to get %s native-lib-dir"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5

    const/4 v3, 0x0

    :try_start_1
    aput-object v11, v2, v3
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    invoke-static {v13, v0, v2}, Lcom/iqiyi/android/qigsaw/core/common/SplitLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Lcom/iqiyi/android/qigsaw/core/splitreport/SplitLoadError;

    new-instance v2, Ljava/lang/Exception;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "native-lib-dir of "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    const/16 v3, -0x64

    invoke-direct {v0, v12, v3, v2}, Lcom/iqiyi/android/qigsaw/core/splitreport/SplitLoadError;-><init>(Lcom/iqiyi/android/qigsaw/core/splitreport/SplitBriefInfo;ILjava/lang/Throwable;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    goto/16 :goto_1

    :catch_0
    move-exception v0

    move v8, v3

    move-object v3, v12

    goto/16 :goto_6

    :cond_5
    const-string v2, "added-dex"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v2, 0x2

    new-array v8, v2, [Ljava/lang/Object;

    const/16 v17, 0x0

    aput-object v11, v8, v17

    const/16 v16, 0x1

    aput-object v5, v8, v16

    const-string/jumbo v2, "split name: %s, origin native path: %s"

    invoke-static {v13, v2, v8}, Lcom/iqiyi/android/qigsaw/core/common/SplitLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v2, v1, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->mapper:Lcom/iqiyi/android/qigsaw/core/splitload/compat/NativePathMapper;

    invoke-interface {v2, v11, v5}, Lcom/iqiyi/android/qigsaw/core/splitload/compat/NativePathMapper;->map(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v11, v5, v17

    aput-object v2, v5, v16

    const-string/jumbo v8, "split name: %s, mapped native path: %s"

    invoke-static {v13, v8, v5}, Lcom/iqiyi/android/qigsaw/core/common/SplitLog;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :try_start_3
    iget-object v5, v1, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->splitLoader:Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoaderWrapper;

    if-nez v3, :cond_6

    const/4 v8, 0x0

    goto :goto_3

    :cond_6
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :goto_3
    if-nez v2, :cond_7

    const/16 v17, 0x0

    goto :goto_4

    :cond_7
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v17, v3

    :goto_4
    invoke-virtual {v15}, Lcom/iqiyi/android/qigsaw/core/splitrequest/splitinfo/SplitInfo;->getDependencies()Ljava/util/List;

    move-result-object v2
    :try_end_3
    .catch Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadException; {:try_start_3 .. :try_end_3} :catch_4

    move-object v3, v12

    move-object v12, v5

    move-object v5, v13

    move-object v13, v11

    move-object/from16 v21, v7

    move-object v7, v14

    move-object v14, v0

    move-object v0, v15

    move-object v15, v8

    move-object/from16 v16, v17

    move-object/from16 v17, v2

    :try_start_4
    invoke-interface/range {v12 .. v17}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoaderWrapper;->loadCode(Ljava/lang/String;Ljava/util/List;Ljava/io/File;Ljava/io/File;Ljava/util/List;)Ljava/lang/ClassLoader;

    move-result-object v2
    :try_end_4
    .catch Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadException; {:try_start_4 .. :try_end_4} :catch_3

    :try_start_5
    iget-object v8, v1, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->activator:Lcom/iqiyi/android/qigsaw/core/splitload/SplitActivator;

    invoke-virtual {v8, v2, v11}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitActivator;->createSplitApplication(Ljava/lang/ClassLoader;Ljava/lang/String;)Landroid/app/Application;

    move-result-object v8
    :try_end_5
    .catch Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadException; {:try_start_5 .. :try_end_5} :catch_2

    :try_start_6
    invoke-direct {v1, v11, v7, v8, v2}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->activateSplit(Ljava/lang/String;Ljava/lang/String;Landroid/app/Application;Ljava/lang/ClassLoader;)V
    :try_end_6
    .catch Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadException; {:try_start_6 .. :try_end_6} :catch_1

    invoke-static {}, Lcom/iqiyi/android/qigsaw/core/splitrequest/splitinfo/SplitPathManager;->require()Lcom/iqiyi/android/qigsaw/core/splitrequest/splitinfo/SplitPathManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/iqiyi/android/qigsaw/core/splitrequest/splitinfo/SplitPathManager;->getSplitDir(Lcom/iqiyi/android/qigsaw/core/splitrequest/splitinfo/SplitInfo;)Ljava/io/File;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-virtual {v0, v12, v13}, Ljava/io/File;->setLastModified(J)Z

    move-result v0

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to set last modified time for "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v8, v2, [Ljava/lang/Object;

    invoke-static {v5, v0, v8}, Lcom/iqiyi/android/qigsaw/core/common/SplitLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    sub-long/2addr v12, v9

    invoke-virtual {v3, v12, v13}, Lcom/iqiyi/android/qigsaw/core/splitreport/SplitBriefInfo;->setTimeCost(J)Lcom/iqiyi/android/qigsaw/core/splitreport/SplitBriefInfo;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/iqiyi/android/qigsaw/core/splitload/Split;

    invoke-direct {v0, v11, v7}, Lcom/iqiyi/android/qigsaw/core/splitload/Split;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v8, v18

    move-wide/from16 v2, v19

    move-object/from16 v7, v21

    goto/16 :goto_2

    :catch_1
    move-exception v0

    move-object v5, v0

    new-instance v0, Lcom/iqiyi/android/qigsaw/core/splitreport/SplitLoadError;

    invoke-virtual {v5}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadException;->getErrorCode()I

    move-result v7

    invoke-virtual {v5}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    invoke-direct {v0, v3, v7, v5}, Lcom/iqiyi/android/qigsaw/core/splitreport/SplitLoadError;-><init>(Lcom/iqiyi/android/qigsaw/core/splitreport/SplitBriefInfo;ILjava/lang/Throwable;)V

    move-object/from16 v7, v21

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v1, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->splitLoader:Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoaderWrapper;

    invoke-interface {v0, v2}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoaderWrapper;->unloadCode(Ljava/lang/ClassLoader;)V

    goto/16 :goto_1

    :catch_2
    move-exception v0

    move-object/from16 v7, v21

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v11, v8, v9

    const-string v9, "Failed to create %s application "

    invoke-static {v5, v0, v9, v8}, Lcom/iqiyi/android/qigsaw/core/common/SplitLog;->printErrStackTrace(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v5, Lcom/iqiyi/android/qigsaw/core/splitreport/SplitLoadError;

    invoke-virtual {v0}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadException;->getErrorCode()I

    move-result v8

    invoke-virtual {v0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v5, v3, v8, v0}, Lcom/iqiyi/android/qigsaw/core/splitreport/SplitLoadError;-><init>(Lcom/iqiyi/android/qigsaw/core/splitreport/SplitBriefInfo;ILjava/lang/Throwable;)V

    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v1, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->splitLoader:Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoaderWrapper;

    invoke-interface {v0, v2}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoaderWrapper;->unloadCode(Ljava/lang/ClassLoader;)V

    goto/16 :goto_1

    :catch_3
    move-exception v0

    move-object/from16 v7, v21

    const/4 v8, 0x1

    goto :goto_5

    :catch_4
    move-exception v0

    move-object v3, v12

    move-object v5, v13

    move/from16 v8, v16

    :goto_5
    new-array v2, v8, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v11, v2, v8

    const-string v9, "Failed to load split %s code!"

    invoke-static {v5, v0, v9, v2}, Lcom/iqiyi/android/qigsaw/core/common/SplitLog;->printErrStackTrace(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v2, Lcom/iqiyi/android/qigsaw/core/splitreport/SplitLoadError;

    invoke-virtual {v0}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadException;->getErrorCode()I

    move-result v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v2, v3, v5, v0}, Lcom/iqiyi/android/qigsaw/core/splitreport/SplitLoadError;-><init>(Lcom/iqiyi/android/qigsaw/core/splitreport/SplitBriefInfo;ILjava/lang/Throwable;)V

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :catch_5
    move-exception v0

    move-object v3, v12

    const/4 v8, 0x0

    :goto_6
    new-instance v2, Lcom/iqiyi/android/qigsaw/core/splitreport/SplitLoadError;

    const/16 v5, -0x64

    invoke-direct {v2, v3, v5, v0}, Lcom/iqiyi/android/qigsaw/core/splitreport/SplitLoadError;-><init>(Lcom/iqiyi/android/qigsaw/core/splitreport/SplitBriefInfo;ILjava/lang/Throwable;)V

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_7
    move v5, v8

    move-object/from16 v8, v18

    move-wide/from16 v2, v19

    goto/16 :goto_0

    :cond_9
    move-wide/from16 v19, v2

    iget-object v0, v1, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->loadManager:Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;

    invoke-virtual {v0, v4}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;->putSplits(Ljava/util/Collection;)V

    if-eqz p1, :cond_a

    iget-object v0, v1, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->loadManager:Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;

    iget-object v8, v0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;->currentProcessName:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long v9, v0, v19

    move-object/from16 v5, p1

    invoke-interface/range {v5 .. v10}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler$OnSplitLoadFinishListener;->onLoadFinish(Ljava/util/List;Ljava/util/List;Ljava/lang/String;J)V

    :cond_a
    return-void
.end method


# virtual methods
.method final getContext()Landroid/content/Context;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->loadManager:Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {p0}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;->getContext()Landroid/content/Context;

    move-result-object p0

    goto/32 :goto_2

    nop

    :goto_2
    return-object p0
.end method

.method getMainHandler()Landroid/os/Handler;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->mainHandler:Landroid/os/Handler;

    goto/32 :goto_1

    nop

    :goto_1
    return-object p0
.end method

.method final loadSplitsSync(Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler$OnSplitLoadFinishListener;)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-direct {p0, p1}, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler;->loadSplits(Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadHandler$OnSplitLoadFinishListener;)V

    goto/32 :goto_0

    nop
.end method
