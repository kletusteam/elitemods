.class public abstract Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;
.super Ljava/lang/Object;


# static fields
.field protected static final TAG:Ljava/lang/String; = "SplitLoadManager"


# instance fields
.field private final context:Landroid/content/Context;

.field final currentProcessName:Ljava/lang/String;

.field private final loadedSplits:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/iqiyi/android/qigsaw/core/splitload/Split;",
            ">;"
        }
    .end annotation
.end field

.field private final splitLoadMode:I


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;->loadedSplits:Ljava/util/Set;

    iput-object p1, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;->currentProcessName:Ljava/lang/String;

    iput p3, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;->splitLoadMode:I

    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 0

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;->loadedSplits:Ljava/util/Set;

    invoke-interface {p0}, Ljava/util/Set;->clear()V

    return-void
.end method

.method public abstract createSplitLoadTask(Ljava/util/List;Lcom/iqiyi/android/qigsaw/core/splitload/listener/OnSplitLoadListener;)Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/content/Intent;",
            ">;",
            "Lcom/iqiyi/android/qigsaw/core/splitload/listener/OnSplitLoadListener;",
            ")",
            "Ljava/lang/Runnable;"
        }
    .end annotation
.end method

.method getContext()Landroid/content/Context;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;->context:Landroid/content/Context;

    goto/32 :goto_1

    nop

    :goto_1
    return-object p0
.end method

.method getLoadedSplitApkPaths()Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    goto/32 :goto_12

    nop

    :goto_0
    invoke-static {v2, v4, v3}, Lcom/iqiyi/android/qigsaw/core/common/SplitLog;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_18

    nop

    :goto_1
    if-nez v2, :cond_0

    goto/32 :goto_19

    :cond_0
    goto/32 :goto_8

    nop

    :goto_2
    aput-object v2, v3, v1

    goto/32 :goto_1c

    nop

    :goto_3
    iget-object v2, v2, Lcom/iqiyi/android/qigsaw/core/splitload/Split;->splitApkPath:Ljava/lang/String;

    goto/32 :goto_2

    nop

    :goto_4
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_5
    goto/32 :goto_f

    nop

    :goto_6
    const/4 v3, 0x1

    goto/32 :goto_a

    nop

    :goto_7
    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;->loadedSplits:Ljava/util/Set;

    goto/32 :goto_4

    nop

    :goto_8
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_16

    nop

    :goto_9
    iget-object v2, v2, Lcom/iqiyi/android/qigsaw/core/splitload/Split;->splitApkPath:Ljava/lang/String;

    goto/32 :goto_14

    nop

    :goto_a
    new-array v3, v3, [Ljava/lang/Object;

    goto/32 :goto_3

    nop

    :goto_b
    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    goto/32 :goto_7

    nop

    :goto_c
    return-object v0

    :goto_d
    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto/32 :goto_1b

    nop

    :goto_e
    if-nez v3, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_9

    nop

    :goto_f
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    goto/32 :goto_1

    nop

    :goto_10
    goto :goto_5

    :goto_11
    goto/32 :goto_6

    nop

    :goto_12
    new-instance v0, Ljava/util/HashSet;

    goto/32 :goto_13

    nop

    :goto_13
    const/4 v1, 0x0

    goto/32 :goto_b

    nop

    :goto_14
    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/32 :goto_10

    nop

    :goto_15
    iget-object v4, v2, Lcom/iqiyi/android/qigsaw/core/splitload/Split;->splitApkPath:Ljava/lang/String;

    goto/32 :goto_d

    nop

    :goto_16
    check-cast v2, Lcom/iqiyi/android/qigsaw/core/splitload/Split;

    goto/32 :goto_1a

    nop

    :goto_17
    const-string v4, "Split has been loaded, but its file %s is not exist!"

    goto/32 :goto_0

    nop

    :goto_18
    goto :goto_5

    :goto_19
    goto/32 :goto_c

    nop

    :goto_1a
    new-instance v3, Ljava/io/File;

    goto/32 :goto_15

    nop

    :goto_1b
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    goto/32 :goto_e

    nop

    :goto_1c
    const-string v2, "SplitLoadManager"

    goto/32 :goto_17

    nop
.end method

.method public getLoadedSplitNames()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;->loadedSplits:Ljava/util/Set;

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/iqiyi/android/qigsaw/core/splitload/Split;

    iget-object v1, v1, Lcom/iqiyi/android/qigsaw/core/splitload/Split;->splitName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method final getLoadedSplits()Ljava/util/Set;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/iqiyi/android/qigsaw/core/splitload/Split;",
            ">;"
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;->loadedSplits:Ljava/util/Set;

    goto/32 :goto_1

    nop

    :goto_1
    return-object p0
.end method

.method public abstract getResources(Landroid/content/res/Resources;)V
.end method

.method public abstract injectPathClassloader()V
.end method

.method public abstract loadInstalledSplits()V
.end method

.method public abstract preloadInstalledSplits(Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method final putSplits(Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/iqiyi/android/qigsaw/core/splitload/Split;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-interface {p0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto/32 :goto_0

    nop

    :goto_2
    iget-object p0, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;->loadedSplits:Ljava/util/Set;

    goto/32 :goto_1

    nop
.end method

.method splitLoadMode()I
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iget p0, p0, Lcom/iqiyi/android/qigsaw/core/splitload/SplitLoadManager;->splitLoadMode:I

    goto/32 :goto_1

    nop

    :goto_1
    return p0
.end method
