.class final Lcom/android/settingslib/bluetooth/BluetoothDeviceFilter$AudioFilter;
.super Lcom/android/settingslib/bluetooth/BluetoothDeviceFilter$ClassUuidFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settingslib/bluetooth/BluetoothDeviceFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "AudioFilter"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settingslib/bluetooth/BluetoothDeviceFilter$ClassUuidFilter;-><init>(Lcom/android/settingslib/bluetooth/BluetoothDeviceFilter$ClassUuidFilter-IA;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settingslib/bluetooth/BluetoothDeviceFilter$AudioFilter-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/BluetoothDeviceFilter$AudioFilter;-><init>()V

    return-void
.end method


# virtual methods
.method matches([Landroid/os/ParcelUuid;Landroid/bluetooth/BluetoothClass;)Z
    .locals 1

    goto/32 :goto_10

    nop

    :goto_0
    if-nez p1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_a

    nop

    :goto_1
    invoke-static {p2, v0}, Lcom/android/settingslib/bluetooth/BluetoothDeviceFilter;->-$$Nest$smdoesClassMatch(Landroid/bluetooth/BluetoothClass;I)Z

    move-result p1

    goto/32 :goto_c

    nop

    :goto_2
    return v0

    :goto_3
    goto/32 :goto_12

    nop

    :goto_4
    if-nez p2, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_1

    nop

    :goto_5
    if-nez p2, :cond_2

    goto/32 :goto_3

    :cond_2
    goto/32 :goto_2

    nop

    :goto_6
    return p0

    :goto_7
    if-nez p1, :cond_3

    goto/32 :goto_e

    :cond_3
    goto/32 :goto_8

    nop

    :goto_8
    return v0

    :goto_9
    goto/32 :goto_4

    nop

    :goto_a
    sget-object p2, Lcom/android/settingslib/bluetooth/A2dpProfile;->SINK_UUIDS:[Landroid/os/ParcelUuid;

    goto/32 :goto_f

    nop

    :goto_b
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_c
    if-eqz p1, :cond_4

    goto/32 :goto_14

    :cond_4
    goto/32 :goto_15

    nop

    :goto_d
    return v0

    :goto_e
    goto/32 :goto_6

    nop

    :goto_f
    invoke-static {p1, p2}, Landroid/bluetooth/BluetoothUuid;->containsAnyUuid([Landroid/os/ParcelUuid;[Landroid/os/ParcelUuid;)Z

    move-result p2

    goto/32 :goto_5

    nop

    :goto_10
    const/4 p0, 0x0

    goto/32 :goto_b

    nop

    :goto_11
    invoke-static {p1, p2}, Landroid/bluetooth/BluetoothUuid;->containsAnyUuid([Landroid/os/ParcelUuid;[Landroid/os/ParcelUuid;)Z

    move-result p1

    goto/32 :goto_7

    nop

    :goto_12
    sget-object p2, Lcom/android/settingslib/bluetooth/HeadsetProfile;->UUIDS:[Landroid/os/ParcelUuid;

    goto/32 :goto_11

    nop

    :goto_13
    if-nez p1, :cond_5

    goto/32 :goto_e

    :cond_5
    :goto_14
    goto/32 :goto_d

    nop

    :goto_15
    invoke-static {p2, p0}, Lcom/android/settingslib/bluetooth/BluetoothDeviceFilter;->-$$Nest$smdoesClassMatch(Landroid/bluetooth/BluetoothClass;I)Z

    move-result p1

    goto/32 :goto_13

    nop
.end method
