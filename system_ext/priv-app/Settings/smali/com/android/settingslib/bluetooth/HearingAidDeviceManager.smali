.class public Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;
.super Ljava/lang/Object;


# instance fields
.field private final mBtManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

.field private final mCachedDevices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/android/settingslib/bluetooth/LocalBluetoothManager;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/settingslib/bluetooth/LocalBluetoothManager;",
            "Ljava/util/List<",
            "Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->mBtManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    iput-object p2, p0, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->mCachedDevices:Ljava/util/List;

    return-void
.end method

.method private getCachedDevice(J)Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;
    .locals 4

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->mCachedDevices:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->mCachedDevices:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getHiSyncId()J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    return-object v1

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method private getHiSyncId(Landroid/bluetooth/BluetoothDevice;)J
    .locals 2

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->mBtManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/LocalBluetoothManager;->getProfileManager()Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;->getHearingAidProfile()Lcom/android/settingslib/bluetooth/HearingAidProfile;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/HearingAidProfile;->isProfileReady()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, p1}, Lcom/android/settingslib/bluetooth/HearingAidProfile;->getHiSyncId(Landroid/bluetooth/BluetoothDevice;)J

    move-result-wide p0

    return-wide p0

    :cond_0
    iget-object p0, p0, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->mBtManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/LocalBluetoothManager;->getCachedDeviceManager()Lcom/android/settingslib/bluetooth/CachedBluetoothDeviceManager;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDeviceManager;->getHisyncIdFromSharedPreferences(Landroid/bluetooth/BluetoothDevice;)J

    move-result-wide p0

    return-wide p0
.end method

.method private isValidHiSyncId(J)Z
    .locals 2

    const-wide/16 v0, 0x0

    cmp-long p0, p1, v0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private log(Ljava/lang/String;)V
    .locals 0

    const-string p0, "HearingAidDeviceManager"

    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method findMainDevice(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;
    .locals 4

    goto/32 :goto_d

    nop

    :goto_0
    if-nez v2, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_7

    nop

    :goto_1
    const/4 p0, 0x0

    goto/32 :goto_11

    nop

    :goto_2
    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getHiSyncId()J

    move-result-wide v2

    goto/32 :goto_8

    nop

    :goto_3
    invoke-virtual {v2, p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto/32 :goto_e

    nop

    :goto_4
    if-nez v2, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_3

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    goto/32 :goto_10

    nop

    :goto_7
    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getSubDevice()Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    move-result-object v2

    goto/32 :goto_4

    nop

    :goto_8
    invoke-direct {p0, v2, v3}, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->isValidHiSyncId(J)Z

    move-result v2

    goto/32 :goto_0

    nop

    :goto_9
    return-object v1

    :goto_a
    goto/32 :goto_1

    nop

    :goto_b
    if-nez v1, :cond_2

    goto/32 :goto_a

    :cond_2
    goto/32 :goto_f

    nop

    :goto_c
    check-cast v1, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    goto/32 :goto_2

    nop

    :goto_d
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->mCachedDevices:Ljava/util/List;

    goto/32 :goto_5

    nop

    :goto_e
    if-nez v2, :cond_3

    goto/32 :goto_6

    :cond_3
    goto/32 :goto_9

    nop

    :goto_f
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_c

    nop

    :goto_10
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_b

    nop

    :goto_11
    return-object p0
.end method

.method initHearingAidDeviceIfNeeded(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->isValidHiSyncId(J)Z

    move-result p0

    goto/32 :goto_4

    nop

    :goto_1
    invoke-virtual {p1, v0, v1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->setHiSyncId(J)V

    :goto_2
    goto/32 :goto_6

    nop

    :goto_3
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_4
    if-nez p0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_5
    invoke-direct {p0, v0}, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->getHiSyncId(Landroid/bluetooth/BluetoothDevice;)J

    move-result-wide v0

    goto/32 :goto_0

    nop

    :goto_6
    return-void
.end method

.method onHiSyncIdChanged(J)V
    .locals 7
    .annotation build Lcom/android/internal/annotations/VisibleForTesting;
    .end annotation

    goto/32 :goto_21

    nop

    :goto_0
    move-object v6, v3

    goto/32 :goto_1e

    nop

    :goto_1
    invoke-direct {p0, p1}, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->log(Ljava/lang/String;)V

    goto/32 :goto_f

    nop

    :goto_2
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_34

    nop

    :goto_3
    return-void

    :goto_4
    goto/16 :goto_2c

    :goto_5
    goto/32 :goto_10

    nop

    :goto_6
    iget-object v2, p0, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->mCachedDevices:Ljava/util/List;

    goto/32 :goto_20

    nop

    :goto_7
    const/4 v1, -0x1

    goto/32 :goto_2b

    nop

    :goto_8
    if-nez v4, :cond_0

    goto/32 :goto_28

    :cond_0
    goto/32 :goto_27

    nop

    :goto_9
    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/LocalBluetoothManager;->getEventManager()Lcom/android/settingslib/bluetooth/BluetoothEventManager;

    move-result-object p0

    goto/32 :goto_15

    nop

    :goto_a
    const-string v2, "onHiSyncIdChanged: removed from UI device ="

    goto/32 :goto_17

    nop

    :goto_b
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_1d

    nop

    :goto_c
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_32

    nop

    :goto_d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_1a

    nop

    :goto_e
    move-object v1, v0

    goto/32 :goto_26

    nop

    :goto_f
    iget-object p0, p0, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->mBtManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    goto/32 :goto_9

    nop

    :goto_10
    invoke-virtual {v3}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->isConnected()Z

    move-result v1

    goto/32 :goto_35

    nop

    :goto_11
    cmp-long v4, v4, p1

    goto/32 :goto_8

    nop

    :goto_12
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_13
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_4

    nop

    :goto_14
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto/32 :goto_1c

    nop

    :goto_15
    invoke-virtual {p0, v1}, Lcom/android/settingslib/bluetooth/BluetoothEventManager;->dispatchDeviceRemoved(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)V

    :goto_16
    goto/32 :goto_3

    nop

    :goto_17
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_d

    nop

    :goto_18
    iget-object v1, p0, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->mCachedDevices:Ljava/util/List;

    goto/32 :goto_c

    nop

    :goto_19
    invoke-virtual {v3}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getHiSyncId()J

    move-result-wide v4

    goto/32 :goto_11

    nop

    :goto_1a
    const-string v2, ", with hiSyncId="

    goto/32 :goto_2

    nop

    :goto_1b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_a

    nop

    :goto_1c
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_7

    nop

    :goto_1d
    check-cast v3, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    goto/32 :goto_19

    nop

    :goto_1e
    move-object v3, v1

    goto/32 :goto_24

    nop

    :goto_1f
    iget-object v3, p0, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->mCachedDevices:Ljava/util/List;

    goto/32 :goto_b

    nop

    :goto_20
    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto/32 :goto_30

    nop

    :goto_21
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->mCachedDevices:Ljava/util/List;

    goto/32 :goto_14

    nop

    :goto_22
    move v2, v0

    :goto_23
    goto/32 :goto_13

    nop

    :goto_24
    move-object v1, v6

    :goto_25
    goto/32 :goto_2d

    nop

    :goto_26
    move v0, v2

    goto/32 :goto_2e

    nop

    :goto_27
    goto :goto_23

    :goto_28
    goto/32 :goto_36

    nop

    :goto_29
    check-cast v0, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    goto/32 :goto_e

    nop

    :goto_2a
    if-gez v0, :cond_1

    goto/32 :goto_16

    :cond_1
    goto/32 :goto_1f

    nop

    :goto_2b
    move v2, v1

    :goto_2c
    goto/32 :goto_2a

    nop

    :goto_2d
    invoke-virtual {v3, v1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->setSubDevice(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)V

    goto/32 :goto_6

    nop

    :goto_2e
    goto :goto_25

    :goto_2f
    goto/32 :goto_18

    nop

    :goto_30
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_1b

    nop

    :goto_31
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->mCachedDevices:Ljava/util/List;

    goto/32 :goto_33

    nop

    :goto_32
    check-cast v1, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    goto/32 :goto_0

    nop

    :goto_33
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_29

    nop

    :goto_34
    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto/32 :goto_12

    nop

    :goto_35
    if-nez v1, :cond_2

    goto/32 :goto_2f

    :cond_2
    goto/32 :goto_31

    nop

    :goto_36
    if-eq v2, v1, :cond_3

    goto/32 :goto_5

    :cond_3
    goto/32 :goto_22

    nop
.end method

.method onProfileConnectionStateChangedIfProcessed(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;I)Z
    .locals 3

    goto/32 :goto_6

    nop

    :goto_0
    iget-object p2, p0, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->mBtManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    goto/32 :goto_d

    nop

    :goto_1
    return v0

    :goto_2
    goto/32 :goto_25

    nop

    :goto_3
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getSubDevice()Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    move-result-object p2

    goto/32 :goto_1d

    nop

    :goto_4
    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/LocalBluetoothManager;->getEventManager()Lcom/android/settingslib/bluetooth/BluetoothEventManager;

    move-result-object p0

    goto/32 :goto_2d

    nop

    :goto_5
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->switchSubDeviceContent()V

    goto/32 :goto_13

    nop

    :goto_6
    const/4 v0, 0x1

    goto/32 :goto_32

    nop

    :goto_7
    iget-object p0, p0, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->mBtManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    goto/32 :goto_4

    nop

    :goto_8
    if-nez p1, :cond_0

    goto/32 :goto_24

    :cond_0
    goto/32 :goto_b

    nop

    :goto_9
    return p0

    :goto_a
    if-ne p2, v1, :cond_1

    goto/32 :goto_15

    :cond_1
    goto/32 :goto_14

    nop

    :goto_b
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->isConnected()Z

    move-result p2

    goto/32 :goto_20

    nop

    :goto_c
    iget-object p2, p0, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->mBtManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    goto/32 :goto_11

    nop

    :goto_d
    invoke-virtual {p2}, Lcom/android/settingslib/bluetooth/LocalBluetoothManager;->getEventManager()Lcom/android/settingslib/bluetooth/BluetoothEventManager;

    move-result-object p2

    goto/32 :goto_2a

    nop

    :goto_e
    invoke-virtual {p0, v1, v2}, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->onHiSyncIdChanged(J)V

    goto/32 :goto_c

    nop

    :goto_f
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->refresh()V

    goto/32 :goto_26

    nop

    :goto_10
    if-nez v1, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_1

    nop

    :goto_11
    invoke-virtual {p2}, Lcom/android/settingslib/bluetooth/LocalBluetoothManager;->getCachedDeviceManager()Lcom/android/settingslib/bluetooth/CachedBluetoothDeviceManager;

    move-result-object p2

    goto/32 :goto_18

    nop

    :goto_12
    const/4 p0, 0x0

    goto/32 :goto_9

    nop

    :goto_13
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->refresh()V

    goto/32 :goto_7

    nop

    :goto_14
    goto :goto_24

    :goto_15
    goto/32 :goto_31

    nop

    :goto_16
    if-nez p2, :cond_3

    goto/32 :goto_24

    :cond_3
    goto/32 :goto_0

    nop

    :goto_17
    invoke-virtual {p0, p1}, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->findMainDevice(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    move-result-object p2

    goto/32 :goto_2e

    nop

    :goto_18
    invoke-virtual {p2, p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDeviceManager;->addDevicetoSharedPreferences(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)Z

    goto/32 :goto_2c

    nop

    :goto_19
    const/4 v1, 0x2

    goto/32 :goto_a

    nop

    :goto_1a
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->switchSubDeviceContent()V

    goto/32 :goto_f

    nop

    :goto_1b
    invoke-virtual {p2}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->refresh()V

    goto/32 :goto_33

    nop

    :goto_1c
    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/LocalBluetoothManager;->getEventManager()Lcom/android/settingslib/bluetooth/BluetoothEventManager;

    move-result-object p0

    goto/32 :goto_22

    nop

    :goto_1d
    if-nez p2, :cond_4

    goto/32 :goto_24

    :cond_4
    goto/32 :goto_28

    nop

    :goto_1e
    return v0

    :goto_1f
    goto/32 :goto_17

    nop

    :goto_20
    if-nez p2, :cond_5

    goto/32 :goto_30

    :cond_5
    goto/32 :goto_29

    nop

    :goto_21
    invoke-virtual {p2, p1}, Lcom/android/settingslib/bluetooth/BluetoothEventManager;->dispatchHearingAidRemoved(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)V

    goto/32 :goto_1a

    nop

    :goto_22
    invoke-virtual {p0, p1}, Lcom/android/settingslib/bluetooth/BluetoothEventManager;->dispatchHearingAidAdded(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)V

    goto/32 :goto_1e

    nop

    :goto_23
    return v0

    :goto_24
    goto/32 :goto_12

    nop

    :goto_25
    if-nez p2, :cond_6

    goto/32 :goto_34

    :cond_6
    goto/32 :goto_1b

    nop

    :goto_26
    iget-object p0, p0, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->mBtManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    goto/32 :goto_1c

    nop

    :goto_27
    invoke-virtual {p2}, Lcom/android/settingslib/bluetooth/LocalBluetoothManager;->getEventManager()Lcom/android/settingslib/bluetooth/BluetoothEventManager;

    move-result-object p2

    goto/32 :goto_21

    nop

    :goto_28
    invoke-virtual {p2}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->isConnected()Z

    move-result p2

    goto/32 :goto_16

    nop

    :goto_29
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->refresh()V

    goto/32 :goto_2f

    nop

    :goto_2a
    invoke-virtual {p2, p1}, Lcom/android/settingslib/bluetooth/BluetoothEventManager;->dispatchDeviceRemoved(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)V

    goto/32 :goto_5

    nop

    :goto_2b
    iget-object p2, p0, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->mBtManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    goto/32 :goto_27

    nop

    :goto_2c
    invoke-virtual {p0, p1}, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->findMainDevice(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    move-result-object p1

    goto/32 :goto_8

    nop

    :goto_2d
    invoke-virtual {p0, p1}, Lcom/android/settingslib/bluetooth/BluetoothEventManager;->dispatchDeviceAdded(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)V

    goto/32 :goto_23

    nop

    :goto_2e
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getUnpairing()Z

    move-result v1

    goto/32 :goto_10

    nop

    :goto_2f
    return v0

    :goto_30
    goto/32 :goto_2b

    nop

    :goto_31
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getHiSyncId()J

    move-result-wide v1

    goto/32 :goto_e

    nop

    :goto_32
    if-nez p2, :cond_7

    goto/32 :goto_1f

    :cond_7
    goto/32 :goto_19

    nop

    :goto_33
    return v0

    :goto_34
    goto/32 :goto_3

    nop
.end method

.method setSubDeviceIfNeeded(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)Z
    .locals 3

    goto/32 :goto_5

    nop

    :goto_0
    return p0

    :goto_1
    goto/32 :goto_a

    nop

    :goto_2
    if-nez p0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_9

    nop

    :goto_3
    if-nez v2, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_4

    nop

    :goto_4
    invoke-direct {p0, v0, v1}, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->getCachedDevice(J)Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    move-result-object p0

    goto/32 :goto_2

    nop

    :goto_5
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getHiSyncId()J

    move-result-wide v0

    goto/32 :goto_7

    nop

    :goto_6
    return p0

    :goto_7
    invoke-direct {p0, v0, v1}, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->isValidHiSyncId(J)Z

    move-result v2

    goto/32 :goto_3

    nop

    :goto_8
    const/4 p0, 0x1

    goto/32 :goto_0

    nop

    :goto_9
    invoke-virtual {p0, p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->setSubDevice(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)V

    goto/32 :goto_8

    nop

    :goto_a
    const/4 p0, 0x0

    goto/32 :goto_6

    nop
.end method

.method updateHearingAidsDevices()V
    .locals 6

    goto/32 :goto_4

    nop

    :goto_0
    invoke-virtual {v2}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    goto/32 :goto_6

    nop

    :goto_1
    invoke-virtual {v2, v3, v4}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->setHiSyncId(J)V

    goto/32 :goto_13

    nop

    :goto_2
    invoke-virtual {p0, v1, v2}, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->onHiSyncIdChanged(J)V

    goto/32 :goto_f

    nop

    :goto_3
    check-cast v2, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    goto/32 :goto_c

    nop

    :goto_4
    new-instance v0, Ljava/util/HashSet;

    goto/32 :goto_7

    nop

    :goto_5
    invoke-direct {p0, v3, v4}, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->isValidHiSyncId(J)Z

    move-result v3

    goto/32 :goto_e

    nop

    :goto_6
    invoke-direct {p0, v3}, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->getHiSyncId(Landroid/bluetooth/BluetoothDevice;)J

    move-result-wide v3

    goto/32 :goto_1f

    nop

    :goto_7
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    goto/32 :goto_1a

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_9
    goto/32 :goto_1b

    nop

    :goto_a
    goto :goto_18

    :goto_b
    goto/32 :goto_8

    nop

    :goto_c
    invoke-virtual {v2}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getHiSyncId()J

    move-result-wide v3

    goto/32 :goto_5

    nop

    :goto_d
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    goto/32 :goto_2

    nop

    :goto_e
    if-eqz v3, :cond_0

    goto/32 :goto_18

    :cond_0
    goto/32 :goto_0

    nop

    :goto_f
    goto :goto_9

    :goto_10
    goto/32 :goto_1c

    nop

    :goto_11
    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/32 :goto_a

    nop

    :goto_12
    if-nez v5, :cond_1

    goto/32 :goto_18

    :cond_1
    goto/32 :goto_1

    nop

    :goto_13
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto/32 :goto_11

    nop

    :goto_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    goto/32 :goto_16

    nop

    :goto_15
    check-cast v1, Ljava/lang/Long;

    goto/32 :goto_d

    nop

    :goto_16
    if-nez v2, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_19

    nop

    :goto_17
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_18
    goto/32 :goto_14

    nop

    :goto_19
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_3

    nop

    :goto_1a
    iget-object v1, p0, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->mCachedDevices:Ljava/util/List;

    goto/32 :goto_17

    nop

    :goto_1b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_1e

    nop

    :goto_1c
    return-void

    :goto_1d
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_15

    nop

    :goto_1e
    if-nez v1, :cond_3

    goto/32 :goto_10

    :cond_3
    goto/32 :goto_1d

    nop

    :goto_1f
    invoke-direct {p0, v3, v4}, Lcom/android/settingslib/bluetooth/HearingAidDeviceManager;->isValidHiSyncId(J)Z

    move-result v5

    goto/32 :goto_12

    nop
.end method
