.class public Lcom/android/settingslib/bluetooth/CsipDeviceManager;
.super Ljava/lang/Object;


# instance fields
.field private final MI_DEVICE_LE_SECOND:Ljava/lang/String;

.field private final MI_DEVICE_LE_STATE_OFF:I

.field private final MI_DEVICE_LE_STATE_ON:I

.field private final MI_DEVICE_LE_STATE_REMOVE:I

.field private final mBtManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

.field private final mCachedDevices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/android/settingslib/bluetooth/LocalBluetoothManager;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/settingslib/bluetooth/LocalBluetoothManager;",
            "Ljava/util/List<",
            "Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "LE_SECONDARY"

    iput-object v0, p0, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->MI_DEVICE_LE_SECOND:Ljava/lang/String;

    const/4 v0, 0x2

    iput v0, p0, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->MI_DEVICE_LE_STATE_REMOVE:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->MI_DEVICE_LE_STATE_ON:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->MI_DEVICE_LE_STATE_OFF:I

    iput-object p1, p0, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->mBtManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    iput-object p2, p0, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->mCachedDevices:Ljava/util/List;

    return-void
.end method

.method private getBaseGroupId(Landroid/bluetooth/BluetoothDevice;)I
    .locals 1

    iget-object p0, p0, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->mBtManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/LocalBluetoothManager;->getProfileManager()Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;

    move-result-object p0

    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/LocalBluetoothProfileManager;->getCsipSetCoordinatorProfile()Lcom/android/settingslib/bluetooth/CsipSetCoordinatorProfile;

    move-result-object p0

    const/4 v0, -0x1

    if-eqz p0, :cond_1

    invoke-virtual {p0, p1}, Lcom/android/settingslib/bluetooth/CsipSetCoordinatorProfile;->getGroupUuidMapByDevice(Landroid/bluetooth/BluetoothDevice;)Ljava/util/Map;

    move-result-object p0

    if-nez p0, :cond_0

    return v0

    :cond_0
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Map$Entry;

    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    return p0

    :cond_1
    return v0
.end method

.method private getCachedDevice(I)Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;
    .locals 3

    iget-object v0, p0, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->mCachedDevices:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->mCachedDevices:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    invoke-virtual {v1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getGroupId()I

    move-result v2

    if-ne v2, p1, :cond_0

    return-object v1

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method private isValidGroupId(I)Z
    .locals 0

    const/4 p0, -0x1

    if-eq p1, p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private log(Ljava/lang/String;)V
    .locals 0

    const-string p0, "CsipDeviceManager"

    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method findMainDevice(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;
    .locals 5

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_1d

    nop

    :goto_1
    if-nez v4, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_9

    nop

    :goto_2
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    goto/32 :goto_e

    nop

    :goto_3
    check-cast v4, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    goto/32 :goto_16

    nop

    :goto_4
    if-nez v4, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_19

    nop

    :goto_5
    invoke-virtual {v4, p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v4

    goto/32 :goto_4

    nop

    :goto_6
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_7
    goto/32 :goto_b

    nop

    :goto_8
    iget-object v1, p0, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->mCachedDevices:Ljava/util/List;

    goto/32 :goto_18

    nop

    :goto_9
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_3

    nop

    :goto_a
    invoke-virtual {v2}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getGroupId()I

    move-result v3

    goto/32 :goto_11

    nop

    :goto_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    goto/32 :goto_1

    nop

    :goto_c
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_d
    goto/32 :goto_1b

    nop

    :goto_e
    if-nez v4, :cond_2

    goto/32 :goto_10

    :cond_2
    goto/32 :goto_f

    nop

    :goto_f
    goto :goto_d

    :goto_10
    goto/32 :goto_6

    nop

    :goto_11
    invoke-direct {p0, v3}, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->isValidGroupId(I)Z

    move-result v3

    goto/32 :goto_17

    nop

    :goto_12
    if-nez v2, :cond_3

    goto/32 :goto_1a

    :cond_3
    goto/32 :goto_1c

    nop

    :goto_13
    invoke-virtual {v2}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getMemberDevice()Ljava/util/Set;

    move-result-object v3

    goto/32 :goto_2

    nop

    :goto_14
    return-object v0

    :goto_15
    check-cast v2, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    goto/32 :goto_a

    nop

    :goto_16
    if-nez v4, :cond_4

    goto/32 :goto_7

    :cond_4
    goto/32 :goto_5

    nop

    :goto_17
    if-nez v3, :cond_5

    goto/32 :goto_d

    :cond_5
    goto/32 :goto_13

    nop

    :goto_18
    if-eqz v1, :cond_6

    goto/32 :goto_1f

    :cond_6
    goto/32 :goto_1e

    nop

    :goto_19
    return-object v2

    :goto_1a
    goto/32 :goto_14

    nop

    :goto_1b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    goto/32 :goto_12

    nop

    :goto_1c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_15

    nop

    :goto_1d
    if-nez p1, :cond_7

    goto/32 :goto_1a

    :cond_7
    goto/32 :goto_8

    nop

    :goto_1e
    goto :goto_1a

    :goto_1f
    goto/32 :goto_c

    nop
.end method

.method initCsipDeviceIfNeeded(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)V
    .locals 3

    goto/32 :goto_d

    nop

    :goto_0
    invoke-direct {p0, v0}, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->getBaseGroupId(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    goto/32 :goto_3

    nop

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_9

    nop

    :goto_2
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_7

    nop

    :goto_3
    invoke-direct {p0, v0}, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->isValidGroupId(I)Z

    move-result v1

    goto/32 :goto_4

    nop

    :goto_4
    if-nez v1, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_6

    nop

    :goto_5
    invoke-direct {p0, v1}, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->log(Ljava/lang/String;)V

    goto/32 :goto_11

    nop

    :goto_6
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_c

    nop

    :goto_7
    const-string v2, " (group: "

    goto/32 :goto_10

    nop

    :goto_8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_2

    nop

    :goto_9
    const-string v2, ")"

    goto/32 :goto_b

    nop

    :goto_a
    return-void

    :goto_b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_f

    nop

    :goto_c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_e

    nop

    :goto_d
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_e
    const-string v2, "initCsipDeviceIfNeeded: "

    goto/32 :goto_8

    nop

    :goto_f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_5

    nop

    :goto_10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop

    :goto_11
    invoke-virtual {p1, v0}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->setGroupId(I)V

    :goto_12
    goto/32 :goto_a

    nop
.end method

.method public isExistedGroupId(I)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->getCachedDevice(I)Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method onGroupIdChanged(I)V
    .locals 6
    .annotation build Lcom/android/internal/annotations/VisibleForTesting;
    .end annotation

    goto/32 :goto_a

    nop

    :goto_0
    const-string p1, " primary="

    goto/32 :goto_d

    nop

    :goto_1
    invoke-virtual {v3}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v4

    goto/32 :goto_6

    nop

    :goto_2
    invoke-virtual {v4, v5}, Landroid/bluetooth/BluetoothDevice;->getSpecificCodecStatus(Ljava/lang/String;)I

    move-result v4

    goto/32 :goto_5

    nop

    :goto_3
    invoke-virtual {v3}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getGroupId()I

    move-result v4

    goto/32 :goto_31

    nop

    :goto_4
    const/4 v1, 0x0

    goto/32 :goto_7

    nop

    :goto_5
    const/4 v5, 0x2

    goto/32 :goto_2b

    nop

    :goto_6
    const-string v5, "LE_SECONDARY"

    goto/32 :goto_2

    nop

    :goto_7
    move-object v2, v1

    :goto_8
    goto/32 :goto_2f

    nop

    :goto_9
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_18

    nop

    :goto_a
    iget-object v0, p0, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->mCachedDevices:Ljava/util/List;

    goto/32 :goto_16

    nop

    :goto_b
    goto :goto_8

    :goto_c
    goto/32 :goto_17

    nop

    :goto_d
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_27

    nop

    :goto_e
    check-cast v3, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    goto/32 :goto_3

    nop

    :goto_f
    if-nez v1, :cond_0

    goto/32 :goto_21

    :cond_0
    goto/32 :goto_15

    nop

    :goto_10
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_e

    nop

    :goto_11
    move-object v2, v3

    :goto_12
    goto/32 :goto_f

    nop

    :goto_13
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_25

    nop

    :goto_14
    const-string v3, "onGroupIdChanged: removed from UI device ="

    goto/32 :goto_13

    nop

    :goto_15
    if-nez v2, :cond_1

    goto/32 :goto_21

    :cond_1
    goto/32 :goto_29

    nop

    :goto_16
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto/32 :goto_2a

    nop

    :goto_17
    return-void

    :goto_18
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_0

    nop

    :goto_19
    iget-object p0, p0, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->mBtManager:Lcom/android/settingslib/bluetooth/LocalBluetoothManager;

    goto/32 :goto_32

    nop

    :goto_1a
    const-string v3, ", with groupId="

    goto/32 :goto_9

    nop

    :goto_1b
    invoke-virtual {p0, v2}, Lcom/android/settingslib/bluetooth/BluetoothEventManager;->dispatchDeviceRemoved(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)V

    goto/32 :goto_20

    nop

    :goto_1c
    invoke-virtual {v1, v2}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->addMemberDevice(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)V

    goto/32 :goto_30

    nop

    :goto_1d
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_b

    nop

    :goto_1e
    goto :goto_12

    :goto_1f
    goto/32 :goto_11

    nop

    :goto_20
    goto :goto_c

    :goto_21
    goto/32 :goto_1d

    nop

    :goto_22
    goto :goto_21

    :goto_23
    goto/32 :goto_1

    nop

    :goto_24
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_14

    nop

    :goto_25
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_1a

    nop

    :goto_26
    invoke-interface {p1, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/32 :goto_19

    nop

    :goto_27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_2d

    nop

    :goto_28
    move-object v1, v3

    goto/32 :goto_1e

    nop

    :goto_29
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_24

    nop

    :goto_2a
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_4

    nop

    :goto_2b
    if-eq v4, v5, :cond_2

    goto/32 :goto_1f

    :cond_2
    goto/32 :goto_28

    nop

    :goto_2c
    iget-object v3, p0, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->mCachedDevices:Ljava/util/List;

    goto/32 :goto_10

    nop

    :goto_2d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_2e

    nop

    :goto_2e
    invoke-direct {p0, p1}, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->log(Ljava/lang/String;)V

    goto/32 :goto_1c

    nop

    :goto_2f
    if-gez v0, :cond_3

    goto/32 :goto_c

    :cond_3
    goto/32 :goto_2c

    nop

    :goto_30
    iget-object p1, p0, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->mCachedDevices:Ljava/util/List;

    goto/32 :goto_26

    nop

    :goto_31
    if-ne v4, p1, :cond_4

    goto/32 :goto_23

    :cond_4
    goto/32 :goto_22

    nop

    :goto_32
    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/LocalBluetoothManager;->getEventManager()Lcom/android/settingslib/bluetooth/BluetoothEventManager;

    move-result-object p0

    goto/32 :goto_1b

    nop
.end method

.method onProfileConnectionStateChangedIfProcessed(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;I)Z
    .locals 2

    goto/32 :goto_15

    nop

    :goto_0
    if-nez p1, :cond_0

    goto/32 :goto_21

    :cond_0
    goto/32 :goto_2

    nop

    :goto_1
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getMemberDevice()Ljava/util/Set;

    move-result-object p0

    goto/32 :goto_2e

    nop

    :goto_2
    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->refresh()V

    goto/32 :goto_20

    nop

    :goto_3
    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->refresh()V

    goto/32 :goto_24

    nop

    :goto_4
    check-cast p2, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    goto/32 :goto_16

    nop

    :goto_5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_30

    nop

    :goto_6
    const/4 v0, 0x1

    goto/32 :goto_18

    nop

    :goto_7
    const/4 p0, 0x0

    goto/32 :goto_1c

    nop

    :goto_8
    if-nez p2, :cond_1

    goto/32 :goto_2b

    :cond_1
    goto/32 :goto_2a

    nop

    :goto_9
    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->isConnected()Z

    move-result p1

    goto/32 :goto_0

    nop

    :goto_a
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_b
    goto/32 :goto_1a

    nop

    :goto_c
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->refresh()V

    goto/32 :goto_12

    nop

    :goto_d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_10

    nop

    :goto_e
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    goto/32 :goto_4

    nop

    :goto_f
    const/4 v1, 0x2

    goto/32 :goto_1d

    nop

    :goto_10
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_2f

    nop

    :goto_11
    if-nez p2, :cond_2

    goto/32 :goto_13

    :cond_2
    goto/32 :goto_e

    nop

    :goto_12
    return v0

    :goto_13
    goto/32 :goto_7

    nop

    :goto_14
    const-string v1, "onProfileConnectionStateChangedIfProcessed: "

    goto/32 :goto_d

    nop

    :goto_15
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_17

    nop

    :goto_16
    invoke-virtual {p2}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->isConnected()Z

    move-result p2

    goto/32 :goto_1e

    nop

    :goto_17
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_14

    nop

    :goto_18
    if-nez p2, :cond_3

    goto/32 :goto_28

    :cond_3
    goto/32 :goto_f

    nop

    :goto_19
    if-nez p0, :cond_4

    goto/32 :goto_25

    :cond_4
    goto/32 :goto_3

    nop

    :goto_1a
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    goto/32 :goto_11

    nop

    :goto_1b
    invoke-virtual {p0}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->refresh()V

    goto/32 :goto_27

    nop

    :goto_1c
    return p0

    :goto_1d
    if-ne p2, v1, :cond_5

    goto/32 :goto_2d

    :cond_5
    goto/32 :goto_2c

    nop

    :goto_1e
    if-nez p2, :cond_6

    goto/32 :goto_b

    :cond_6
    goto/32 :goto_c

    nop

    :goto_1f
    invoke-virtual {p0, p1}, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->findMainDevice(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    move-result-object p0

    goto/32 :goto_19

    nop

    :goto_20
    return v0

    :goto_21
    goto/32 :goto_1b

    nop

    :goto_22
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getGroupId()I

    move-result p2

    goto/32 :goto_23

    nop

    :goto_23
    invoke-virtual {p0, p2}, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->onGroupIdChanged(I)V

    goto/32 :goto_32

    nop

    :goto_24
    return v0

    :goto_25
    goto/32 :goto_1

    nop

    :goto_26
    invoke-direct {p0, v0}, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->log(Ljava/lang/String;)V

    goto/32 :goto_6

    nop

    :goto_27
    return v0

    :goto_28
    goto/32 :goto_1f

    nop

    :goto_29
    if-nez p0, :cond_7

    goto/32 :goto_13

    :cond_7
    goto/32 :goto_9

    nop

    :goto_2a
    goto/16 :goto_13

    :goto_2b
    goto/32 :goto_a

    nop

    :goto_2c
    goto/16 :goto_13

    :goto_2d
    goto/32 :goto_22

    nop

    :goto_2e
    invoke-interface {p0}, Ljava/util/Set;->isEmpty()Z

    move-result p2

    goto/32 :goto_8

    nop

    :goto_2f
    const-string v1, ", state: "

    goto/32 :goto_5

    nop

    :goto_30
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_31

    nop

    :goto_31
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_26

    nop

    :goto_32
    invoke-virtual {p0, p1}, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->findMainDevice(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    move-result-object p0

    goto/32 :goto_29

    nop
.end method

.method setMemberDeviceIfNeeded(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)Z
    .locals 3

    goto/32 :goto_e

    nop

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_5

    nop

    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_15

    nop

    :goto_2
    const-string/jumbo v2, "setMemberDeviceIfNeeded, main: "

    goto/32 :goto_6

    nop

    :goto_3
    invoke-direct {p0, v0}, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->getCachedDevice(I)Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    move-result-object v0

    goto/32 :goto_14

    nop

    :goto_4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_2

    nop

    :goto_5
    const-string v2, ", member: "

    goto/32 :goto_c

    nop

    :goto_6
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_0

    nop

    :goto_7
    if-nez v1, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_3

    nop

    :goto_8
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop

    :goto_9
    invoke-virtual {v0, p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->addMemberDevice(Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;)V

    goto/32 :goto_12

    nop

    :goto_a
    if-nez v0, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_9

    nop

    :goto_b
    const/4 p0, 0x0

    goto/32 :goto_13

    nop

    :goto_c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_8

    nop

    :goto_d
    invoke-virtual {p1, p0}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->setName(Ljava/lang/String;)V

    goto/32 :goto_11

    nop

    :goto_e
    invoke-virtual {p1}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getGroupId()I

    move-result v0

    goto/32 :goto_16

    nop

    :goto_f
    return p0

    :goto_10
    goto/32 :goto_b

    nop

    :goto_11
    const/4 p0, 0x1

    goto/32 :goto_f

    nop

    :goto_12
    invoke-virtual {v0}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getName()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_d

    nop

    :goto_13
    return p0

    :goto_14
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_4

    nop

    :goto_15
    invoke-direct {p0, v1}, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->log(Ljava/lang/String;)V

    goto/32 :goto_a

    nop

    :goto_16
    invoke-direct {p0, v0}, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->isValidGroupId(I)Z

    move-result v1

    goto/32 :goto_7

    nop
.end method

.method updateCsipDevices()V
    .locals 5

    goto/32 :goto_1a

    nop

    :goto_0
    invoke-virtual {v2}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    goto/32 :goto_1b

    nop

    :goto_1
    check-cast v2, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;

    goto/32 :goto_d

    nop

    :goto_2
    invoke-direct {p0, v3}, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->isValidGroupId(I)Z

    move-result v4

    goto/32 :goto_4

    nop

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_13

    nop

    :goto_4
    if-nez v4, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_5

    nop

    :goto_5
    invoke-virtual {v2, v3}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->setGroupId(I)V

    goto/32 :goto_6

    nop

    :goto_6
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/32 :goto_b

    nop

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_1

    nop

    :goto_8
    iget-object v1, p0, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->mCachedDevices:Ljava/util/List;

    goto/32 :goto_11

    nop

    :goto_9
    goto :goto_1e

    :goto_a
    goto/32 :goto_e

    nop

    :goto_b
    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/32 :goto_f

    nop

    :goto_c
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto/32 :goto_18

    nop

    :goto_d
    invoke-virtual {v2}, Lcom/android/settingslib/bluetooth/CachedBluetoothDevice;->getGroupId()I

    move-result v3

    goto/32 :goto_1f

    nop

    :goto_e
    return-void

    :goto_f
    goto :goto_12

    :goto_10
    goto/32 :goto_1d

    nop

    :goto_11
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_12
    goto/32 :goto_19

    nop

    :goto_13
    check-cast v1, Ljava/lang/Integer;

    goto/32 :goto_c

    nop

    :goto_14
    if-nez v1, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_3

    nop

    :goto_15
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    goto/32 :goto_8

    nop

    :goto_16
    if-eqz v3, :cond_2

    goto/32 :goto_12

    :cond_2
    goto/32 :goto_0

    nop

    :goto_17
    if-nez v2, :cond_3

    goto/32 :goto_10

    :cond_3
    goto/32 :goto_7

    nop

    :goto_18
    invoke-virtual {p0, v1}, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->onGroupIdChanged(I)V

    goto/32 :goto_9

    nop

    :goto_19
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    goto/32 :goto_17

    nop

    :goto_1a
    new-instance v0, Ljava/util/HashSet;

    goto/32 :goto_15

    nop

    :goto_1b
    invoke-direct {p0, v3}, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->getBaseGroupId(Landroid/bluetooth/BluetoothDevice;)I

    move-result v3

    goto/32 :goto_2

    nop

    :goto_1c
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_14

    nop

    :goto_1d
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1e
    goto/32 :goto_1c

    nop

    :goto_1f
    invoke-direct {p0, v3}, Lcom/android/settingslib/bluetooth/CsipDeviceManager;->isValidGroupId(I)Z

    move-result v3

    goto/32 :goto_16

    nop
.end method
