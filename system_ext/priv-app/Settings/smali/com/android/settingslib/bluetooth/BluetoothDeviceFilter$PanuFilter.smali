.class final Lcom/android/settingslib/bluetooth/BluetoothDeviceFilter$PanuFilter;
.super Lcom/android/settingslib/bluetooth/BluetoothDeviceFilter$ClassUuidFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settingslib/bluetooth/BluetoothDeviceFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "PanuFilter"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/settingslib/bluetooth/BluetoothDeviceFilter$ClassUuidFilter;-><init>(Lcom/android/settingslib/bluetooth/BluetoothDeviceFilter$ClassUuidFilter-IA;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/settingslib/bluetooth/BluetoothDeviceFilter$PanuFilter-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settingslib/bluetooth/BluetoothDeviceFilter$PanuFilter;-><init>()V

    return-void
.end method


# virtual methods
.method matches([Landroid/os/ParcelUuid;Landroid/bluetooth/BluetoothClass;)Z
    .locals 1

    goto/32 :goto_a

    nop

    :goto_0
    return p0

    :goto_1
    const/4 p0, 0x0

    :goto_2
    goto/32 :goto_0

    nop

    :goto_3
    if-nez p1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_b

    nop

    :goto_4
    const/4 p1, 0x4

    goto/32 :goto_8

    nop

    :goto_5
    return p0

    :goto_6
    goto/32 :goto_f

    nop

    :goto_7
    invoke-static {p1, v0}, Lcom/android/internal/util/ArrayUtils;->contains([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    goto/32 :goto_c

    nop

    :goto_8
    invoke-static {p2, p1}, Lcom/android/settingslib/bluetooth/BluetoothDeviceFilter;->-$$Nest$smdoesClassMatch(Landroid/bluetooth/BluetoothClass;I)Z

    move-result p1

    goto/32 :goto_9

    nop

    :goto_9
    if-nez p1, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_d

    nop

    :goto_a
    const/4 p0, 0x1

    goto/32 :goto_3

    nop

    :goto_b
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->PANU:Landroid/os/ParcelUuid;

    goto/32 :goto_7

    nop

    :goto_c
    if-nez p1, :cond_2

    goto/32 :goto_6

    :cond_2
    goto/32 :goto_5

    nop

    :goto_d
    goto :goto_2

    :goto_e
    goto/32 :goto_1

    nop

    :goto_f
    if-nez p2, :cond_3

    goto/32 :goto_e

    :cond_3
    goto/32 :goto_4

    nop
.end method
