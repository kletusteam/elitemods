.class public final Lcom/android/settingslib/R$id;
.super Ljava/lang/Object;


# static fields
.field public static final ad_service_instructions_link_text_view:I = 0x7f0a0075

.field public static final add_a_photo_icon:I = 0x7f0a0077

.field public static final additional_summary:I = 0x7f0a0081

.field public static final alert:I = 0x7f0a009b

.field public static final alert_icon:I = 0x7f0a009d

.field public static final alert_label:I = 0x7f0a009e

.field public static final alert_summary:I = 0x7f0a009f

.field public static final arrow_right:I = 0x7f0a0104

.field public static final avatar_grid:I = 0x7f0a0113

.field public static final avatar_image:I = 0x7f0a0114

.field public static final bottom_label_group:I = 0x7f0a0193

.field public static final bottom_label_space:I = 0x7f0a0194

.field public static final bubble_all:I = 0x7f0a01bb

.field public static final bubble_all_icon:I = 0x7f0a01bc

.field public static final bubble_all_label:I = 0x7f0a01bd

.field public static final bubble_none:I = 0x7f0a01bf

.field public static final bubble_none_icon:I = 0x7f0a01c0

.field public static final bubble_none_label:I = 0x7f0a01c1

.field public static final bubble_selected:I = 0x7f0a01c2

.field public static final bubble_selected_icon:I = 0x7f0a01c3

.field public static final bubble_selected_label:I = 0x7f0a01c4

.field public static final button:I = 0x7f0a01c9

.field public static final container:I = 0x7f0a0268

.field public static final dialog_subtitle:I = 0x7f0a0306

.field public static final dialog_title:I = 0x7f0a0308

.field public static final error_message:I = 0x7f0a0380

.field public static final fragment_container:I = 0x7f0a0409

.field public static final friction_icon:I = 0x7f0a041b

.field public static final glif_layout:I = 0x7f0a042c

.field public static final graph_label_group:I = 0x7f0a0435

.field public static final icon:I = 0x7f0a04ab

.field public static final icon_button:I = 0x7f0a04ac

.field public static final label:I = 0x7f0a0580

.field public static final label_bottom:I = 0x7f0a0583

.field public static final label_end:I = 0x7f0a0584

.field public static final label_group:I = 0x7f0a0586

.field public static final label_middle:I = 0x7f0a0588

.field public static final label_start:I = 0x7f0a0589

.field public static final label_top:I = 0x7f0a058a

.field public static final message:I = 0x7f0a0624

.field public static final negative_btn:I = 0x7f0a06f7

.field public static final neutral_btn:I = 0x7f0a0701

.field public static final positive_btn:I = 0x7f0a07b5

.field public static final preview_view:I = 0x7f0a07d1

.field public static final priority_group:I = 0x7f0a07d9

.field public static final restricted_icon:I = 0x7f0a0880

.field public static final settings_button:I = 0x7f0a0916

.field public static final settings_button_no_background:I = 0x7f0a0917

.field public static final silence:I = 0x7f0a0956

.field public static final silence_icon:I = 0x7f0a0957

.field public static final silence_label:I = 0x7f0a0958

.field public static final silence_summary:I = 0x7f0a0959

.field public static final space1:I = 0x7f0a0999

.field public static final space2:I = 0x7f0a099a

.field public static final spacer:I = 0x7f0a099b

.field public static final summary:I = 0x7f0a0a28

.field public static final switchWidget:I = 0x7f0a0a32

.field public static final text_right:I = 0x7f0a0a75

.field public static final two_target_divider:I = 0x7f0a0b0a

.field public static final usage_graph:I = 0x7f0a0b2d

.field public static final user_name:I = 0x7f0a0b40

.field public static final user_photo:I = 0x7f0a0b41

.field public static final zen_alarm_warning:I = 0x7f0a0bd3

.field public static final zen_duration_container:I = 0x7f0a0be7

.field public static final zen_radio_buttons:I = 0x7f0a0bf9

.field public static final zen_radio_buttons_content:I = 0x7f0a0bfa
