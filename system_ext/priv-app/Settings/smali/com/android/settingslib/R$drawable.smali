.class public final Lcom/android/settingslib/R$drawable;
.super Ljava/lang/Object;


# static fields
.field public static final avatar_choose_photo_circled:I = 0x7f0801bb

.field public static final avatar_take_photo_circled:I = 0x7f0801bd

.field public static final button_border_selected:I = 0x7f080252

.field public static final button_border_unselected:I = 0x7f080253

.field public static final ic_1x_mobiledata:I = 0x7f080606

.field public static final ic_3g_mobiledata:I = 0x7f080607

.field public static final ic_4g_lte_mobiledata:I = 0x7f080608

.field public static final ic_4g_lte_plus_mobiledata:I = 0x7f080609

.field public static final ic_4g_mobiledata:I = 0x7f08060a

.field public static final ic_4g_plus_mobiledata:I = 0x7f08060b

.field public static final ic_5g_e_mobiledata:I = 0x7f08060c

.field public static final ic_5g_mobiledata:I = 0x7f08060d

.field public static final ic_5g_plus_mobiledata:I = 0x7f08060e

.field public static final ic_5g_uwb_mobiledata:I = 0x7f080611

.field public static final ic_adv_audio:I = 0x7f080630

.field public static final ic_bt_bluetooth:I = 0x7f080691

.field public static final ic_bt_bluetooth_bonded:I = 0x7f080692

.field public static final ic_bt_cellphone:I = 0x7f080695

.field public static final ic_bt_cellphone_bonded:I = 0x7f080696

.field public static final ic_bt_digital_pen:I = 0x7f080698

.field public static final ic_bt_digital_pen_bonded:I = 0x7f080699

.field public static final ic_bt_headphones_a2dp:I = 0x7f08069a

.field public static final ic_bt_headphones_a2dp_bonded:I = 0x7f08069b

.field public static final ic_bt_headset_hfp:I = 0x7f08069c

.field public static final ic_bt_headset_hfp_bonded:I = 0x7f08069d

.field public static final ic_bt_hearing_aid:I = 0x7f08069e

.field public static final ic_bt_imaging:I = 0x7f08069f

.field public static final ic_bt_imaging_bonded:I = 0x7f0806a0

.field public static final ic_bt_keyboard_hid:I = 0x7f0806a1

.field public static final ic_bt_keyboard_hid_bonded:I = 0x7f0806a2

.field public static final ic_bt_laptop:I = 0x7f0806a3

.field public static final ic_bt_laptop_bonded:I = 0x7f0806a4

.field public static final ic_bt_misc_hid:I = 0x7f0806a8

.field public static final ic_bt_misc_hid_bonded:I = 0x7f0806a9

.field public static final ic_bt_network_pan:I = 0x7f0806aa

.field public static final ic_bt_network_pan_bonded:I = 0x7f0806ab

.field public static final ic_bt_pointing_hid:I = 0x7f0806ac

.field public static final ic_bt_pointing_hid_bonded:I = 0x7f0806ad

.field public static final ic_carrier_wifi:I = 0x7f0806bd

.field public static final ic_e_mobiledata:I = 0x7f0806fa

.field public static final ic_g_mobiledata:I = 0x7f080730

.field public static final ic_h_mobiledata:I = 0x7f080758

.field public static final ic_h_plus_mobiledata:I = 0x7f080759

.field public static final ic_headphone:I = 0x7f080771

.field public static final ic_help:I = 0x7f080774

.field public static final ic_info_outline_24dp:I = 0x7f080799

.field public static final ic_lockscreen_ime:I = 0x7f0807be

.field public static final ic_lte_mobiledata:I = 0x7f0807bf

.field public static final ic_lte_plus_mobiledata:I = 0x7f0807c0

.field public static final ic_media_display_device:I = 0x7f0807c8

.field public static final ic_media_group_device:I = 0x7f0807c9

.field public static final ic_media_speaker_device:I = 0x7f0807ce

.field public static final ic_mobile_call_strength_0:I = 0x7f0807e2

.field public static final ic_mobile_call_strength_1:I = 0x7f0807e3

.field public static final ic_mobile_call_strength_2:I = 0x7f0807e4

.field public static final ic_mobile_call_strength_3:I = 0x7f0807e5

.field public static final ic_mobile_call_strength_4:I = 0x7f0807e6

.field public static final ic_no_internet_wifi_signal_0:I = 0x7f08087f

.field public static final ic_no_internet_wifi_signal_1:I = 0x7f080880

.field public static final ic_no_internet_wifi_signal_2:I = 0x7f080881

.field public static final ic_no_internet_wifi_signal_3:I = 0x7f080882

.field public static final ic_no_internet_wifi_signal_4:I = 0x7f080883

.field public static final ic_show_x_wifi_signal_0:I = 0x7f080927

.field public static final ic_show_x_wifi_signal_1:I = 0x7f080928

.field public static final ic_show_x_wifi_signal_2:I = 0x7f080929

.field public static final ic_show_x_wifi_signal_3:I = 0x7f08092a

.field public static final ic_show_x_wifi_signal_4:I = 0x7f08092b

.field public static final ic_smartphone:I = 0x7f080935

.field public static final ic_system_update:I = 0x7f080965

.field public static final ic_vowifi:I = 0x7f0809ab

.field public static final ic_vowifi_calling:I = 0x7f0809ac

.field public static final ic_wifi_call_strength_0:I = 0x7f0809c6

.field public static final ic_wifi_call_strength_1:I = 0x7f0809c7

.field public static final ic_wifi_call_strength_2:I = 0x7f0809c8

.field public static final ic_wifi_call_strength_3:I = 0x7f0809c9

.field public static final ic_wifi_call_strength_4:I = 0x7f0809ca

.field public static final settings_input_antenna:I = 0x7f080f3e

.field public static final stat_sys_airplane_mode:I = 0x7f080fa3

.field public static final stylus_update_bg:I = 0x7f080fc4
