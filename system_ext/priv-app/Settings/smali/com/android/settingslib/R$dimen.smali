.class public final Lcom/android/settingslib/R$dimen;
.super Ljava/lang/Object;


# static fields
.field public static final bt_nearby_icon_size:I = 0x7f070134

.field public static final conversation_icon_size:I = 0x7f0701d9

.field public static final qrcode_preview_radius:I = 0x7f070a05

.field public static final restricted_icon_padding:I = 0x7f070a3f

.field public static final secondary_app_icon_size:I = 0x7f070a95

.field public static final signal_icon_size:I = 0x7f070ae4

.field public static final stylus_update_padding_hor:I = 0x7f070b9c

.field public static final stylus_update_padding_ver:I = 0x7f070b9d

.field public static final two_target_pref_medium_icon_size:I = 0x7f070cb3

.field public static final two_target_pref_small_icon_size:I = 0x7f070cb4

.field public static final usage_graph_divider_size:I = 0x7f070cbb

.field public static final usage_graph_dot_interval:I = 0x7f070cbc

.field public static final usage_graph_dot_size:I = 0x7f070cbd

.field public static final usage_graph_line_corner_radius:I = 0x7f070cc0

.field public static final usage_graph_line_width:I = 0x7f070cc1

.field public static final wifi_preference_badge_padding:I = 0x7f070d95
