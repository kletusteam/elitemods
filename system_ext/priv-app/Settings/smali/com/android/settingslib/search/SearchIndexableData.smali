.class public Lcom/android/settingslib/search/SearchIndexableData;
.super Ljava/lang/Object;


# instance fields
.field private final mSearchIndexProvider:Lcom/android/settingslib/search/Indexable$SearchIndexProvider;

.field private final mTargetClass:Ljava/lang/Class;


# virtual methods
.method public getSearchIndexProvider()Lcom/android/settingslib/search/Indexable$SearchIndexProvider;
    .locals 0

    iget-object p0, p0, Lcom/android/settingslib/search/SearchIndexableData;->mSearchIndexProvider:Lcom/android/settingslib/search/Indexable$SearchIndexProvider;

    return-object p0
.end method

.method public getTargetClass()Ljava/lang/Class;
    .locals 0

    iget-object p0, p0, Lcom/android/settingslib/search/SearchIndexableData;->mTargetClass:Ljava/lang/Class;

    return-object p0
.end method
