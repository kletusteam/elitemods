.class public Lcom/android/settingslib/wifi/WifiPasspointProvision;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settingslib/wifi/WifiPasspointProvision$PasspointR1ProvisioningCallback;
    }
.end annotation


# static fields
.field private static final RANDOM:Ljava/util/Random;

.field private static sInstance:Lcom/android/settingslib/wifi/WifiPasspointProvision;


# instance fields
.field private mConnection:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;

.field private mIPasspointKeyInterface:Lcom/miui/cloudservice/IPasspointKeyInterface;

.field private mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmIPasspointKeyInterface(Lcom/android/settingslib/wifi/WifiPasspointProvision;)Lcom/miui/cloudservice/IPasspointKeyInterface;
    .locals 0

    iget-object p0, p0, Lcom/android/settingslib/wifi/WifiPasspointProvision;->mIPasspointKeyInterface:Lcom/miui/cloudservice/IPasspointKeyInterface;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmIPasspointKeyInterface(Lcom/android/settingslib/wifi/WifiPasspointProvision;Lcom/miui/cloudservice/IPasspointKeyInterface;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/wifi/WifiPasspointProvision;->mIPasspointKeyInterface:Lcom/miui/cloudservice/IPasspointKeyInterface;

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/android/settingslib/wifi/WifiPasspointProvision;->RANDOM:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/settingslib/wifi/WifiPasspointProvision$1;

    invoke-direct {v0, p0}, Lcom/android/settingslib/wifi/WifiPasspointProvision$1;-><init>(Lcom/android/settingslib/wifi/WifiPasspointProvision;)V

    iput-object v0, p0, Lcom/android/settingslib/wifi/WifiPasspointProvision;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settingslib/wifi/WifiPasspointProvision;->mContext:Landroid/content/Context;

    const-string/jumbo v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/wifi/WifiManager;

    iput-object p1, p0, Lcom/android/settingslib/wifi/WifiPasspointProvision;->mWifiManager:Landroid/net/wifi/WifiManager;

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/settingslib/wifi/WifiPasspointProvision;
    .locals 1

    sget-object v0, Lcom/android/settingslib/wifi/WifiPasspointProvision;->sInstance:Lcom/android/settingslib/wifi/WifiPasspointProvision;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settingslib/wifi/WifiPasspointProvision;

    invoke-direct {v0, p0}, Lcom/android/settingslib/wifi/WifiPasspointProvision;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/settingslib/wifi/WifiPasspointProvision;->sInstance:Lcom/android/settingslib/wifi/WifiPasspointProvision;

    :cond_0
    sget-object p0, Lcom/android/settingslib/wifi/WifiPasspointProvision;->sInstance:Lcom/android/settingslib/wifi/WifiPasspointProvision;

    return-object p0
.end method

.method public static isPasspointR1Supported()Z
    .locals 3

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const-string/jumbo v0, "ro.vendor.net.enable_passpoint_r1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    move v1, v2

    :cond_1
    return v1
.end method


# virtual methods
.method public bindPasspointKeyService()V
    .locals 3

    iget-object v0, p0, Lcom/android/settingslib/wifi/WifiPasspointProvision;->mIPasspointKeyInterface:Lcom/miui/cloudservice/IPasspointKeyInterface;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.miui.cloudservice.PasspointService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.miui.cloudservice"

    const-string v2, "com.miui.cloudservice.alipay.provision.PasspointService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/settingslib/wifi/WifiPasspointProvision;->mContext:Landroid/content/Context;

    iget-object p0, p0, Lcom/android/settingslib/wifi/WifiPasspointProvision;->mConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p0, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method
