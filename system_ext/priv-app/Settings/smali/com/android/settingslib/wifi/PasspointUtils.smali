.class public Lcom/android/settingslib/wifi/PasspointUtils;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/wifitrackerlib/IPasspointUtils;


# static fields
.field private static volatile mInstance:Lcom/android/settingslib/wifi/PasspointUtils;


# instance fields
.field private final mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "MiuiWifiService"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/wifi/MiuiWifiManager;

    iput-object p1, p0, Lcom/android/settingslib/wifi/PasspointUtils;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/settingslib/wifi/PasspointUtils;
    .locals 2

    sget-object v0, Lcom/android/settingslib/wifi/PasspointUtils;->mInstance:Lcom/android/settingslib/wifi/PasspointUtils;

    if-nez v0, :cond_1

    const-class v0, Lcom/android/settingslib/wifi/PasspointUtils;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/android/settingslib/wifi/PasspointUtils;->mInstance:Lcom/android/settingslib/wifi/PasspointUtils;

    if-nez v1, :cond_0

    new-instance v1, Lcom/android/settingslib/wifi/PasspointUtils;

    invoke-direct {v1, p0}, Lcom/android/settingslib/wifi/PasspointUtils;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/android/settingslib/wifi/PasspointUtils;->mInstance:Lcom/android/settingslib/wifi/PasspointUtils;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    :cond_1
    :goto_0
    sget-object p0, Lcom/android/settingslib/wifi/PasspointUtils;->mInstance:Lcom/android/settingslib/wifi/PasspointUtils;

    return-object p0
.end method


# virtual methods
.method public getMatchingPasspointConfigsForPasspointR1Providers(Ljava/util/Set;)Ljava/util/Map;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Landroid/net/wifi/hotspot2/PasspointR1Provider;",
            ">;)",
            "Ljava/util/Map<",
            "Landroid/net/wifi/hotspot2/PasspointR1Provider;",
            "Landroid/net/wifi/hotspot2/PasspointConfiguration;",
            ">;"
        }
    .end annotation

    iget-object p0, p0, Lcom/android/settingslib/wifi/PasspointUtils;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Landroid/net/wifi/MiuiWifiManager;->getMatchingPasspointConfigsForPasspointR1Providers(Ljava/util/Set;)Ljava/util/Map;

    move-result-object p0

    return-object p0

    :cond_0
    new-instance p0, Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    return-object p0
.end method

.method public getMatchingPasspointR1Providers(Ljava/util/List;)Ljava/util/Map;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/net/wifi/ScanResult;",
            ">;)",
            "Ljava/util/Map<",
            "Landroid/net/wifi/hotspot2/PasspointR1Provider;",
            "Ljava/util/List<",
            "Landroid/net/wifi/ScanResult;",
            ">;>;"
        }
    .end annotation

    iget-object p0, p0, Lcom/android/settingslib/wifi/PasspointUtils;->mMiuiWifiManager:Landroid/net/wifi/MiuiWifiManager;

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Landroid/net/wifi/MiuiWifiManager;->getMatchingPasspointR1Providers(Ljava/util/List;)Ljava/util/Map;

    move-result-object p0

    return-object p0

    :cond_0
    new-instance p0, Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    return-object p0
.end method
