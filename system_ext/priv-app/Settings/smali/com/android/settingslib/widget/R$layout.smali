.class public final Lcom/android/settingslib/widget/R$layout;
.super Ljava/lang/Object;


# static fields
.field public static final illustration_preference:I = 0x7f0d019d

.field public static final layout_preference_frame:I = 0x7f0d01bd

.field public static final preference_app:I = 0x7f0d0303

.field public static final preference_footer:I = 0x7f0d0327

.field public static final preference_selector_with_widget:I = 0x7f0d0346

.field public static final preference_usage_progress_bar:I = 0x7f0d0351

.field public static final preference_widget_checkbox:I = 0x7f0d0357

.field public static final preference_widget_radiobutton:I = 0x7f0d0364

.field public static final settings_bar_chart:I = 0x7f0d03f8

.field public static final settings_bar_view:I = 0x7f0d03f9

.field public static final settings_spinner_dropdown_view:I = 0x7f0d0407

.field public static final settings_spinner_preference:I = 0x7f0d0408

.field public static final settings_spinner_view:I = 0x7f0d0409

.field public static final settingslib_action_buttons:I = 0x7f0d040d

.field public static final settingslib_banner_message:I = 0x7f0d040e

.field public static final settingslib_button_layout:I = 0x7f0d040f

.field public static final settingslib_main_switch_bar:I = 0x7f0d0412

.field public static final settingslib_main_switch_layout:I = 0x7f0d0413

.field public static final top_intro_preference:I = 0x7f0d04c2
