.class public final Lcom/android/settingslib/widget/R$id;
.super Ljava/lang/Object;


# static fields
.field public static final appendix:I = 0x7f0a00f6

.field public static final arrow_right:I = 0x7f0a0104

.field public static final background_view:I = 0x7f0a011f

.field public static final banner_dismiss_btn:I = 0x7f0a0127

.field public static final banner_icon:I = 0x7f0a0128

.field public static final banner_negative_btn:I = 0x7f0a0129

.field public static final banner_positive_btn:I = 0x7f0a012a

.field public static final banner_subtitle:I = 0x7f0a012b

.field public static final banner_summary:I = 0x7f0a012c

.field public static final banner_title:I = 0x7f0a012d

.field public static final bar_chart_details:I = 0x7f0a012f

.field public static final bar_chart_title:I = 0x7f0a0130

.field public static final bar_summary:I = 0x7f0a0131

.field public static final bar_title:I = 0x7f0a0132

.field public static final bar_view:I = 0x7f0a0133

.field public static final bar_view1:I = 0x7f0a0134

.field public static final bar_view2:I = 0x7f0a0135

.field public static final bar_view3:I = 0x7f0a0136

.field public static final bar_view4:I = 0x7f0a0137

.field public static final bottom_summary:I = 0x7f0a0196

.field public static final button1:I = 0x7f0a01ca

.field public static final button2:I = 0x7f0a01cb

.field public static final button3:I = 0x7f0a01cc

.field public static final button4:I = 0x7f0a01cd

.field public static final custom_content:I = 0x7f0a0299

.field public static final divider1:I = 0x7f0a031c

.field public static final divider2:I = 0x7f0a031d

.field public static final divider3:I = 0x7f0a031e

.field public static final frame:I = 0x7f0a040e

.field public static final icon_frame:I = 0x7f0a04b3

.field public static final icon_view:I = 0x7f0a04c6

.field public static final illustration_frame:I = 0x7f0a04d9

.field public static final lottie_view:I = 0x7f0a05eb

.field public static final middleground_layout:I = 0x7f0a0635

.field public static final radio_extra_widget:I = 0x7f0a0842

.field public static final radio_extra_widget_container:I = 0x7f0a0843

.field public static final restricted_icon:I = 0x7f0a0880

.field public static final selector_extra_widget:I = 0x7f0a090b

.field public static final selector_extra_widget_container:I = 0x7f0a090c

.field public static final settingslib_button:I = 0x7f0a0924

.field public static final settingslib_learn_more:I = 0x7f0a0925

.field public static final settingslib_main_switch_bar:I = 0x7f0a0926

.field public static final spinner:I = 0x7f0a09a2

.field public static final summary:I = 0x7f0a0a28

.field public static final summary_container:I = 0x7f0a0a2a

.field public static final switch_text:I = 0x7f0a0a35

.field public static final top_row:I = 0x7f0a0aa4

.field public static final total_summary:I = 0x7f0a0aa9

.field public static final two_target_divider:I = 0x7f0a0b0a

.field public static final usage_summary:I = 0x7f0a0b30
