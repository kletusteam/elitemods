.class Lcom/android/settingslib/users/AppRestrictionsHelper$Injector;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settingslib/users/AppRestrictionsHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Injector"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mUser:Landroid/os/UserHandle;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/os/UserHandle;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settingslib/users/AppRestrictionsHelper$Injector;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settingslib/users/AppRestrictionsHelper$Injector;->mUser:Landroid/os/UserHandle;

    return-void
.end method


# virtual methods
.method getContext()Landroid/content/Context;
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-object p0

    :goto_1
    iget-object p0, p0, Lcom/android/settingslib/users/AppRestrictionsHelper$Injector;->mContext:Landroid/content/Context;

    goto/32 :goto_0

    nop
.end method

.method getIPackageManager()Landroid/content/pm/IPackageManager;
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-object p0

    :goto_1
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object p0

    goto/32 :goto_0

    nop
.end method

.method getInputMethodList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;"
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    const-string v1, "input_method"

    goto/32 :goto_4

    nop

    :goto_1
    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->getInputMethodListAsUser(I)Ljava/util/List;

    move-result-object p0

    goto/32 :goto_3

    nop

    :goto_2
    invoke-virtual {p0}, Lcom/android/settingslib/users/AppRestrictionsHelper$Injector;->getContext()Landroid/content/Context;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_3
    return-object p0

    :goto_4
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_5
    invoke-virtual {p0}, Landroid/os/UserHandle;->getIdentifier()I

    move-result p0

    goto/32 :goto_1

    nop

    :goto_6
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    goto/32 :goto_7

    nop

    :goto_7
    iget-object p0, p0, Lcom/android/settingslib/users/AppRestrictionsHelper$Injector;->mUser:Landroid/os/UserHandle;

    goto/32 :goto_5

    nop
.end method

.method getPackageManager()Landroid/content/pm/PackageManager;
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    goto/32 :goto_2

    nop

    :goto_1
    iget-object p0, p0, Lcom/android/settingslib/users/AppRestrictionsHelper$Injector;->mContext:Landroid/content/Context;

    goto/32 :goto_0

    nop

    :goto_2
    return-object p0
.end method

.method getUser()Landroid/os/UserHandle;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iget-object p0, p0, Lcom/android/settingslib/users/AppRestrictionsHelper$Injector;->mUser:Landroid/os/UserHandle;

    goto/32 :goto_1

    nop

    :goto_1
    return-object p0
.end method

.method getUserManager()Landroid/os/UserManager;
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    const-class v0, Landroid/os/UserManager;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    goto/32 :goto_4

    nop

    :goto_2
    iget-object p0, p0, Lcom/android/settingslib/users/AppRestrictionsHelper$Injector;->mContext:Landroid/content/Context;

    goto/32 :goto_0

    nop

    :goto_3
    return-object p0

    :goto_4
    check-cast p0, Landroid/os/UserManager;

    goto/32 :goto_3

    nop
.end method
