.class public Lcom/android/settingslib/media/InfoMediaDevice;
.super Lcom/android/settingslib/media/MediaDevice;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/media/MediaRouter2Manager;Landroid/media/MediaRoute2Info;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/settingslib/media/MediaDevice;-><init>(Landroid/content/Context;Landroid/media/MediaRouter2Manager;Landroid/media/MediaRoute2Info;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/settingslib/media/MediaDevice;->initDeviceRecord()V

    return-void
.end method


# virtual methods
.method getDrawableResId()I
    .locals 1

    goto/32 :goto_e

    nop

    :goto_0
    sget p0, Lcom/android/settingslib/R$drawable;->ic_media_speaker_device:I

    goto/32 :goto_b

    nop

    :goto_1
    const/16 v0, 0x7d0

    goto/32 :goto_d

    nop

    :goto_2
    sget p0, Lcom/android/settingslib/R$drawable;->ic_media_group_device:I

    goto/32 :goto_3

    nop

    :goto_3
    goto :goto_a

    :goto_4
    goto/32 :goto_9

    nop

    :goto_5
    invoke-virtual {p0}, Landroid/media/MediaRoute2Info;->getType()I

    move-result p0

    goto/32 :goto_8

    nop

    :goto_6
    if-ne p0, v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_1

    nop

    :goto_7
    return p0

    :goto_8
    const/16 v0, 0x3e9

    goto/32 :goto_6

    nop

    :goto_9
    sget p0, Lcom/android/settingslib/R$drawable;->ic_media_display_device:I

    :goto_a
    goto/32 :goto_7

    nop

    :goto_b
    goto :goto_a

    :goto_c
    goto/32 :goto_2

    nop

    :goto_d
    if-ne p0, v0, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_0

    nop

    :goto_e
    iget-object p0, p0, Lcom/android/settingslib/media/MediaDevice;->mRouteInfo:Landroid/media/MediaRoute2Info;

    goto/32 :goto_5

    nop
.end method

.method getDrawableResIdByFeature()I
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    goto :goto_d

    :goto_1
    goto/32 :goto_3

    nop

    :goto_2
    const-string v0, "android.media.route.feature.REMOTE_GROUP_PLAYBACK"

    goto/32 :goto_10

    nop

    :goto_3
    const-string v0, "android.media.route.feature.REMOTE_VIDEO_PLAYBACK"

    goto/32 :goto_5

    nop

    :goto_4
    iget-object p0, p0, Lcom/android/settingslib/media/MediaDevice;->mRouteInfo:Landroid/media/MediaRoute2Info;

    goto/32 :goto_f

    nop

    :goto_5
    invoke-interface {p0, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p0

    goto/32 :goto_9

    nop

    :goto_6
    sget p0, Lcom/android/settingslib/R$drawable;->ic_media_group_device:I

    goto/32 :goto_0

    nop

    :goto_7
    goto :goto_d

    :goto_8
    goto/32 :goto_c

    nop

    :goto_9
    if-nez p0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_e

    nop

    :goto_a
    if-nez v0, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_6

    nop

    :goto_b
    return p0

    :goto_c
    sget p0, Lcom/android/settingslib/R$drawable;->ic_media_speaker_device:I

    :goto_d
    goto/32 :goto_b

    nop

    :goto_e
    sget p0, Lcom/android/settingslib/R$drawable;->ic_media_display_device:I

    goto/32 :goto_7

    nop

    :goto_f
    invoke-virtual {p0}, Landroid/media/MediaRoute2Info;->getFeatures()Ljava/util/List;

    move-result-object p0

    goto/32 :goto_2

    nop

    :goto_10
    invoke-interface {p0, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_a

    nop
.end method

.method public getId()Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/settingslib/media/MediaDevice;->mRouteInfo:Landroid/media/MediaRoute2Info;

    invoke-static {p0}, Lcom/android/settingslib/media/MediaDeviceUtils;->getId(Landroid/media/MediaRoute2Info;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public getName()Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/settingslib/media/MediaDevice;->mRouteInfo:Landroid/media/MediaRoute2Info;

    invoke-virtual {p0}, Landroid/media/MediaRoute2Info;->getName()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public isConnected()Z
    .locals 0

    const/4 p0, 0x1

    return p0
.end method
