.class public final Lcom/android/settingslib/R$color;
.super Ljava/lang/Object;


# static fields
.field public static final dark_mode_icon_color_single_tone:I = 0x7f060112

.field public static final disabled_text_color:I = 0x7f06014b

.field public static final important_conversation:I = 0x7f0601d7

.field public static final light_mode_icon_color_single_tone:I = 0x7f0601f9

.field public static final qr_background_color:I = 0x7f0606c6

.field public static final qr_corner_line_color:I = 0x7f0606ca

.field public static final qr_focused_corner_line_color:I = 0x7f0606cb

.field public static final usage_graph_dots:I = 0x7f060842
