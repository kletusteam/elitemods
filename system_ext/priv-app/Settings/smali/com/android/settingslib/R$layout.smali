.class public final Lcom/android/settingslib/R$layout;
.super Ljava/lang/Object;


# static fields
.field public static final access_point_friction_widget:I = 0x7f0d002f

.field public static final ad_service_instructions_layout:I = 0x7f0d0049

.field public static final avatar_item:I = 0x7f0d007f

.field public static final avatar_picker:I = 0x7f0d0080

.field public static final broadcast_dialog:I = 0x7f0d00aa

.field public static final bubble_preference:I = 0x7f0d00ad

.field public static final edit_user_info_dialog_content:I = 0x7f0d0140

.field public static final notif_importance_preference:I = 0x7f0d02c9

.field public static final notif_priority_conversation_preference:I = 0x7f0d02ca

.field public static final preference_access_point:I = 0x7f0d0301

.field public static final preference_two_target:I = 0x7f0d034e

.field public static final preference_widget_gear_optional_background:I = 0x7f0d035f

.field public static final preference_widget_primary_switch:I = 0x7f0d0363

.field public static final qrcode_scan_mode_activity:I = 0x7f0d03a2

.field public static final qrcode_scanner_fragment:I = 0x7f0d03a3

.field public static final restricted_icon:I = 0x7f0d03be

.field public static final usage_and_diagnostic_layout:I = 0x7f0d04d6

.field public static final usage_view:I = 0x7f0d04dd

.field public static final user_creation_progress_dialog:I = 0x7f0d04ee

.field public static final user_preference:I = 0x7f0d04f5

.field public static final user_select_item:I = 0x7f0d04f7

.field public static final zen_mode_condition:I = 0x7f0d0541

.field public static final zen_mode_duration_dialog:I = 0x7f0d0542

.field public static final zen_mode_radio_button:I = 0x7f0d0543

.field public static final zen_mode_turn_on_dialog_container:I = 0x7f0d0547
