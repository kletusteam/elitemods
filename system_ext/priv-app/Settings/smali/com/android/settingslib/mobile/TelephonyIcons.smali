.class public Lcom/android/settingslib/mobile/TelephonyIcons;
.super Ljava/lang/Object;


# static fields
.field public static final CARRIER_MERGED_WIFI:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final CARRIER_NETWORK_CHANGE:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final DATA_DISABLED:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final E:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final FIVE_G:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final FIVE_G_BASIC:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final FIVE_G_SA:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final FIVE_G_UWB:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final FLIGHT_MODE_ICON:I

.field public static final FOUR_G:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final FOUR_G_LTE:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final FOUR_G_LTE_PLUS:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final FOUR_G_PLUS:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final G:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final H:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final H_PLUS:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final ICON_1X:I

.field public static final ICON_3G:I

.field public static final ICON_4G:I

.field public static final ICON_4G_LTE:I

.field public static final ICON_4G_LTE_PLUS:I

.field public static final ICON_4G_PLUS:I

.field public static final ICON_5G:I

.field public static final ICON_5G_BASIC:I

.field public static final ICON_5G_E:I

.field public static final ICON_5G_PLUS:I

.field public static final ICON_5G_SA:I

.field public static final ICON_5G_UWB:I

.field public static final ICON_CWF:I

.field public static final ICON_E:I

.field public static final ICON_G:I

.field public static final ICON_H:I

.field public static final ICON_H_PLUS:I

.field public static final ICON_LTE:I

.field public static final ICON_LTE_PLUS:I

.field public static final ICON_NAME_TO_ICON:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/android/settingslib/SignalIcon$MobileIconGroup;",
            ">;"
        }
    .end annotation
.end field

.field public static final ICON_VOWIFI:I

.field public static final ICON_VOWIFI_CALLING:I

.field public static final LTE:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final LTE_CA_5G_E:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final LTE_PLUS:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final MOBILE_CALL_STRENGTH_ICONS:[I

.field public static final NOT_DEFAULT_DATA:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final NR_5G:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final NR_5G_PLUS:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final ONE_X:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final THREE_G:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final UNKNOWN:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final VOWIFI:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final VOWIFI_CALLING:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final WFC:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

.field public static final WIFI_CALL_STRENGTH_ICONS:[I


# direct methods
.method static constructor <clinit>()V
    .locals 164

    sget v0, Lcom/android/settingslib/R$drawable;->stat_sys_airplane_mode:I

    sput v0, Lcom/android/settingslib/mobile/TelephonyIcons;->FLIGHT_MODE_ICON:I

    sget v12, Lcom/android/settingslib/R$drawable;->ic_lte_mobiledata:I

    sput v12, Lcom/android/settingslib/mobile/TelephonyIcons;->ICON_LTE:I

    sget v13, Lcom/android/settingslib/R$drawable;->ic_lte_plus_mobiledata:I

    sput v13, Lcom/android/settingslib/mobile/TelephonyIcons;->ICON_LTE_PLUS:I

    sget v11, Lcom/android/settingslib/R$drawable;->ic_g_mobiledata:I

    sput v11, Lcom/android/settingslib/mobile/TelephonyIcons;->ICON_G:I

    sget v25, Lcom/android/settingslib/R$drawable;->ic_e_mobiledata:I

    sput v25, Lcom/android/settingslib/mobile/TelephonyIcons;->ICON_E:I

    sget v37, Lcom/android/settingslib/R$drawable;->ic_h_mobiledata:I

    sput v37, Lcom/android/settingslib/mobile/TelephonyIcons;->ICON_H:I

    sget v49, Lcom/android/settingslib/R$drawable;->ic_h_plus_mobiledata:I

    sput v49, Lcom/android/settingslib/mobile/TelephonyIcons;->ICON_H_PLUS:I

    sget v61, Lcom/android/settingslib/R$drawable;->ic_3g_mobiledata:I

    sput v61, Lcom/android/settingslib/mobile/TelephonyIcons;->ICON_3G:I

    sget v73, Lcom/android/settingslib/R$drawable;->ic_4g_mobiledata:I

    sput v73, Lcom/android/settingslib/mobile/TelephonyIcons;->ICON_4G:I

    sget v85, Lcom/android/settingslib/R$drawable;->ic_4g_plus_mobiledata:I

    sput v85, Lcom/android/settingslib/mobile/TelephonyIcons;->ICON_4G_PLUS:I

    sget v97, Lcom/android/settingslib/R$drawable;->ic_4g_lte_mobiledata:I

    sput v97, Lcom/android/settingslib/mobile/TelephonyIcons;->ICON_4G_LTE:I

    sget v109, Lcom/android/settingslib/R$drawable;->ic_4g_lte_plus_mobiledata:I

    sput v109, Lcom/android/settingslib/mobile/TelephonyIcons;->ICON_4G_LTE_PLUS:I

    sget v121, Lcom/android/settingslib/R$drawable;->ic_5g_e_mobiledata:I

    sput v121, Lcom/android/settingslib/mobile/TelephonyIcons;->ICON_5G_E:I

    sget v133, Lcom/android/settingslib/R$drawable;->ic_1x_mobiledata:I

    sput v133, Lcom/android/settingslib/mobile/TelephonyIcons;->ICON_1X:I

    sget v146, Lcom/android/settingslib/R$drawable;->ic_5g_mobiledata:I

    sput v146, Lcom/android/settingslib/mobile/TelephonyIcons;->ICON_5G:I

    sget v147, Lcom/android/settingslib/R$drawable;->ic_5g_plus_mobiledata:I

    sput v147, Lcom/android/settingslib/mobile/TelephonyIcons;->ICON_5G_PLUS:I

    sget v148, Lcom/android/settingslib/R$drawable;->ic_carrier_wifi:I

    sput v148, Lcom/android/settingslib/mobile/TelephonyIcons;->ICON_CWF:I

    sput v146, Lcom/android/settingslib/mobile/TelephonyIcons;->ICON_5G_SA:I

    sput v146, Lcom/android/settingslib/mobile/TelephonyIcons;->ICON_5G_BASIC:I

    sget v149, Lcom/android/settingslib/R$drawable;->ic_5g_uwb_mobiledata:I

    sput v149, Lcom/android/settingslib/mobile/TelephonyIcons;->ICON_5G_UWB:I

    sget v150, Lcom/android/settingslib/R$drawable;->ic_vowifi:I

    sput v150, Lcom/android/settingslib/mobile/TelephonyIcons;->ICON_VOWIFI:I

    sget v151, Lcom/android/settingslib/R$drawable;->ic_vowifi_calling:I

    sput v151, Lcom/android/settingslib/mobile/TelephonyIcons;->ICON_VOWIFI_CALLING:I

    new-instance v10, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    sget-object v152, Lcom/android/settingslib/AccessibilityContentDescriptions;->PHONE_SIGNAL_STRENGTH:[I

    const/16 v153, 0x0

    aget v143, v152, v153

    sget v144, Lcom/android/settingslib/R$string;->carrier_network_change_mode:I

    const-string v135, "CARRIER_NETWORK_CHANGE"

    const/16 v136, 0x0

    const/16 v137, 0x0

    const/16 v139, 0x0

    const/16 v140, 0x0

    const/16 v141, 0x0

    const/16 v142, 0x0

    const/16 v145, 0x0

    move-object/from16 v134, v10

    move-object/from16 v138, v152

    invoke-direct/range {v134 .. v145}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v10, Lcom/android/settingslib/mobile/TelephonyIcons;->CARRIER_NETWORK_CHANGE:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v9, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v59, v152, v153

    sget v60, Lcom/android/settingslib/R$string;->data_connection_3g:I

    const-string v51, "3G"

    const/16 v52, 0x0

    const/16 v53, 0x0

    const/16 v55, 0x0

    const/16 v56, 0x0

    const/16 v57, 0x0

    const/16 v58, 0x0

    move-object/from16 v50, v9

    move-object/from16 v54, v152

    invoke-direct/range {v50 .. v61}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v9, Lcom/android/settingslib/mobile/TelephonyIcons;->THREE_G:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v8, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v143, v152, v153

    const-string v135, "WFC"

    const/16 v144, 0x0

    move-object/from16 v134, v8

    invoke-direct/range {v134 .. v145}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v8, Lcom/android/settingslib/mobile/TelephonyIcons;->WFC:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v7, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v143, v152, v153

    const-string v135, "Unknown"

    move-object/from16 v134, v7

    invoke-direct/range {v134 .. v145}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v7, Lcom/android/settingslib/mobile/TelephonyIcons;->UNKNOWN:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v6, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v23, v152, v153

    sget v24, Lcom/android/settingslib/R$string;->data_connection_edge:I

    const-string v15, "E"

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object v14, v6

    move-object/from16 v18, v152

    invoke-direct/range {v14 .. v25}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v6, Lcom/android/settingslib/mobile/TelephonyIcons;->E:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v14, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v131, v152, v153

    sget v132, Lcom/android/settingslib/R$string;->data_connection_cdma:I

    const-string v123, "1X"

    const/16 v124, 0x0

    const/16 v125, 0x0

    const/16 v127, 0x0

    const/16 v128, 0x0

    const/16 v129, 0x0

    const/16 v130, 0x0

    move-object/from16 v122, v14

    move-object/from16 v126, v152

    invoke-direct/range {v122 .. v133}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v14, Lcom/android/settingslib/mobile/TelephonyIcons;->ONE_X:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v15, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v16, v152, v153

    sget v17, Lcom/android/settingslib/R$string;->data_connection_gprs:I

    const-string v1, "G"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v18, 0x0

    move-object v0, v15

    move-object/from16 v4, v152

    move-object/from16 v154, v6

    move/from16 v6, v18

    move-object/from16 v155, v7

    move/from16 v7, v19

    move-object/from16 v156, v8

    move/from16 v8, v20

    move-object/from16 v157, v9

    move/from16 v9, v16

    move-object/from16 v158, v10

    move/from16 v10, v17

    invoke-direct/range {v0 .. v11}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v15, Lcom/android/settingslib/mobile/TelephonyIcons;->G:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v0, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v35, v152, v153

    sget v36, Lcom/android/settingslib/R$string;->data_connection_3_5g:I

    const-string v27, "H"

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    move-object/from16 v26, v0

    move-object/from16 v30, v152

    invoke-direct/range {v26 .. v37}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v0, Lcom/android/settingslib/mobile/TelephonyIcons;->H:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v11, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v47, v152, v153

    sget v48, Lcom/android/settingslib/R$string;->data_connection_3_5g_plus:I

    const-string v39, "H+"

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    const/16 v46, 0x0

    move-object/from16 v38, v11

    move-object/from16 v42, v152

    invoke-direct/range {v38 .. v49}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v11, Lcom/android/settingslib/mobile/TelephonyIcons;->H_PLUS:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v10, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v71, v152, v153

    sget v72, Lcom/android/settingslib/R$string;->data_connection_4g:I

    const-string v63, "4G"

    const/16 v64, 0x0

    const/16 v65, 0x0

    const/16 v67, 0x0

    const/16 v68, 0x0

    const/16 v69, 0x0

    const/16 v70, 0x0

    move-object/from16 v62, v10

    move-object/from16 v66, v152

    invoke-direct/range {v62 .. v73}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v10, Lcom/android/settingslib/mobile/TelephonyIcons;->FOUR_G:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v9, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v83, v152, v153

    sget v84, Lcom/android/settingslib/R$string;->data_connection_4g_plus:I

    const-string v75, "4G+"

    const/16 v76, 0x0

    const/16 v77, 0x0

    const/16 v79, 0x0

    const/16 v80, 0x0

    const/16 v81, 0x0

    const/16 v82, 0x0

    move-object/from16 v74, v9

    move-object/from16 v78, v152

    invoke-direct/range {v74 .. v85}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v9, Lcom/android/settingslib/mobile/TelephonyIcons;->FOUR_G_PLUS:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v8, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v16, v152, v153

    sget v17, Lcom/android/settingslib/R$string;->data_connection_lte:I

    const-string v2, "LTE"

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, v8

    move-object/from16 v5, v152

    move-object/from16 v159, v8

    move/from16 v8, v18

    move-object/from16 v160, v9

    move/from16 v9, v19

    move-object/from16 v161, v10

    move/from16 v10, v16

    move-object/from16 v162, v11

    move/from16 v11, v17

    invoke-direct/range {v1 .. v12}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    move-object/from16 v12, v159

    sput-object v12, Lcom/android/settingslib/mobile/TelephonyIcons;->LTE:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v11, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v9, v152, v153

    sget v10, Lcom/android/settingslib/R$string;->data_connection_lte_plus:I

    const-string v1, "LTE+"

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    move-object v4, v0

    move-object v0, v11

    move-object/from16 v163, v4

    move-object/from16 v4, v152

    move-object v12, v11

    move v11, v13

    invoke-direct/range {v0 .. v11}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v12, Lcom/android/settingslib/mobile/TelephonyIcons;->LTE_PLUS:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v0, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v95, v152, v153

    sget v96, Lcom/android/settingslib/R$string;->data_connection_4g_lte:I

    const-string v87, "4G LTE"

    const/16 v88, 0x0

    const/16 v89, 0x0

    const/16 v91, 0x0

    const/16 v92, 0x0

    const/16 v93, 0x0

    const/16 v94, 0x0

    move-object/from16 v86, v0

    move-object/from16 v90, v152

    invoke-direct/range {v86 .. v97}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v0, Lcom/android/settingslib/mobile/TelephonyIcons;->FOUR_G_LTE:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v1, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v107, v152, v153

    sget v108, Lcom/android/settingslib/R$string;->data_connection_4g_lte_plus:I

    const-string v99, "4G LTE+"

    const/16 v100, 0x0

    const/16 v101, 0x0

    const/16 v103, 0x0

    const/16 v104, 0x0

    const/16 v105, 0x0

    const/16 v106, 0x0

    move-object/from16 v98, v1

    move-object/from16 v102, v152

    invoke-direct/range {v98 .. v109}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v1, Lcom/android/settingslib/mobile/TelephonyIcons;->FOUR_G_LTE_PLUS:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v2, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v119, v152, v153

    sget v120, Lcom/android/settingslib/R$string;->data_connection_5ge_html:I

    const-string v111, "5Ge"

    const/16 v112, 0x0

    const/16 v113, 0x0

    const/16 v115, 0x0

    const/16 v116, 0x0

    const/16 v117, 0x0

    const/16 v118, 0x0

    move-object/from16 v110, v2

    move-object/from16 v114, v152

    invoke-direct/range {v110 .. v121}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v2, Lcom/android/settingslib/mobile/TelephonyIcons;->LTE_CA_5G_E:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v3, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v143, v152, v153

    sget v4, Lcom/android/settingslib/R$string;->data_connection_5g:I

    const-string v135, "5G"

    move-object/from16 v134, v3

    move/from16 v144, v4

    move/from16 v145, v146

    invoke-direct/range {v134 .. v145}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v3, Lcom/android/settingslib/mobile/TelephonyIcons;->NR_5G:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v5, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v143, v152, v153

    sget v144, Lcom/android/settingslib/R$string;->data_connection_5g_plus:I

    const-string v135, "5G_PLUS"

    move-object/from16 v134, v5

    move/from16 v145, v147

    invoke-direct/range {v134 .. v145}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v5, Lcom/android/settingslib/mobile/TelephonyIcons;->NR_5G_PLUS:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v6, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v143, v152, v153

    sget v144, Lcom/android/settingslib/R$string;->cell_data_off_content_description:I

    const-string v135, "DataDisabled"

    const/16 v145, 0x0

    move-object/from16 v134, v6

    invoke-direct/range {v134 .. v145}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v6, Lcom/android/settingslib/mobile/TelephonyIcons;->DATA_DISABLED:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v7, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v143, v152, v153

    sget v144, Lcom/android/settingslib/R$string;->not_default_data_content_description:I

    const-string v135, "NotDefaultData"

    move-object/from16 v134, v7

    invoke-direct/range {v134 .. v145}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v7, Lcom/android/settingslib/mobile/TelephonyIcons;->NOT_DEFAULT_DATA:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v8, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v143, v152, v153

    sget v144, Lcom/android/settingslib/R$string;->data_connection_carrier_wifi:I

    const-string v135, "CWF"

    move-object/from16 v134, v8

    move/from16 v145, v148

    invoke-direct/range {v134 .. v145}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v8, Lcom/android/settingslib/mobile/TelephonyIcons;->CARRIER_MERGED_WIFI:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v8, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v143, v152, v153

    const-string v135, "5G"

    move-object/from16 v134, v8

    move/from16 v144, v4

    move/from16 v145, v146

    invoke-direct/range {v134 .. v145}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v8, Lcom/android/settingslib/mobile/TelephonyIcons;->FIVE_G:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v4, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v143, v152, v153

    sget v144, Lcom/android/settingslib/R$string;->data_connection_5g_basic:I

    const-string v135, "5GBasic"

    move-object/from16 v134, v4

    invoke-direct/range {v134 .. v145}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v4, Lcom/android/settingslib/mobile/TelephonyIcons;->FIVE_G_BASIC:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v4, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v143, v152, v153

    sget v144, Lcom/android/settingslib/R$string;->data_connection_5g_uwb:I

    const-string v135, "5GUWB"

    move-object/from16 v134, v4

    move/from16 v145, v149

    invoke-direct/range {v134 .. v145}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v4, Lcom/android/settingslib/mobile/TelephonyIcons;->FIVE_G_UWB:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v8, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v143, v152, v153

    sget v144, Lcom/android/settingslib/R$string;->data_connection_5g_sa:I

    const-string v135, "5GSA"

    move-object/from16 v134, v8

    move/from16 v145, v146

    invoke-direct/range {v134 .. v145}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v8, Lcom/android/settingslib/mobile/TelephonyIcons;->FIVE_G_SA:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v8, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v143, v152, v153

    const-string v135, "VoWIFI"

    const/16 v144, 0x0

    move-object/from16 v134, v8

    move/from16 v145, v150

    invoke-direct/range {v134 .. v145}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v8, Lcom/android/settingslib/mobile/TelephonyIcons;->VOWIFI:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v8, Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    aget v143, v152, v153

    const-string v135, "VoWIFICall"

    move-object/from16 v134, v8

    move/from16 v145, v151

    invoke-direct/range {v134 .. v145}, Lcom/android/settingslib/SignalIcon$MobileIconGroup;-><init>(Ljava/lang/String;[[I[[I[IIIIIIII)V

    sput-object v8, Lcom/android/settingslib/mobile/TelephonyIcons;->VOWIFI_CALLING:Lcom/android/settingslib/SignalIcon$MobileIconGroup;

    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    sput-object v8, Lcom/android/settingslib/mobile/TelephonyIcons;->ICON_NAME_TO_ICON:Ljava/util/Map;

    const-string v9, "carrier_network_change"

    move-object/from16 v10, v158

    invoke-interface {v8, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v9, "3g"

    move-object/from16 v10, v157

    invoke-interface {v8, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v9, "wfc"

    move-object/from16 v10, v156

    invoke-interface {v8, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v9, "unknown"

    move-object/from16 v10, v155

    invoke-interface {v8, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v9, "e"

    move-object/from16 v10, v154

    invoke-interface {v8, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v9, "1x"

    invoke-interface {v8, v9, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v9, "g"

    invoke-interface {v8, v9, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v9, "h"

    move-object/from16 v10, v163

    invoke-interface {v8, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v9, "h+"

    move-object/from16 v10, v162

    invoke-interface {v8, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v9, "4g"

    move-object/from16 v10, v161

    invoke-interface {v8, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v9, "4g+"

    move-object/from16 v10, v160

    invoke-interface {v8, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v9, "4glte"

    invoke-interface {v8, v9, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "4glte+"

    invoke-interface {v8, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "5ge"

    invoke-interface {v8, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "lte"

    move-object/from16 v1, v159

    invoke-interface {v8, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "lte+"

    invoke-interface {v8, v0, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "5g"

    invoke-interface {v8, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "5g_plus"

    invoke-interface {v8, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "5guwb"

    invoke-interface {v8, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "datadisable"

    invoke-interface {v8, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "notdefaultdata"

    invoke-interface {v8, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x5

    new-array v1, v0, [I

    sget v2, Lcom/android/settingslib/R$drawable;->ic_wifi_call_strength_0:I

    aput v2, v1, v153

    sget v2, Lcom/android/settingslib/R$drawable;->ic_wifi_call_strength_1:I

    const/4 v3, 0x1

    aput v2, v1, v3

    sget v2, Lcom/android/settingslib/R$drawable;->ic_wifi_call_strength_2:I

    const/4 v4, 0x2

    aput v2, v1, v4

    sget v2, Lcom/android/settingslib/R$drawable;->ic_wifi_call_strength_3:I

    const/4 v5, 0x3

    aput v2, v1, v5

    sget v2, Lcom/android/settingslib/R$drawable;->ic_wifi_call_strength_4:I

    const/4 v6, 0x4

    aput v2, v1, v6

    sput-object v1, Lcom/android/settingslib/mobile/TelephonyIcons;->WIFI_CALL_STRENGTH_ICONS:[I

    new-array v0, v0, [I

    sget v1, Lcom/android/settingslib/R$drawable;->ic_mobile_call_strength_0:I

    aput v1, v0, v153

    sget v1, Lcom/android/settingslib/R$drawable;->ic_mobile_call_strength_1:I

    aput v1, v0, v3

    sget v1, Lcom/android/settingslib/R$drawable;->ic_mobile_call_strength_2:I

    aput v1, v0, v4

    sget v1, Lcom/android/settingslib/R$drawable;->ic_mobile_call_strength_3:I

    aput v1, v0, v5

    sget v1, Lcom/android/settingslib/R$drawable;->ic_mobile_call_strength_4:I

    aput v1, v0, v6

    sput-object v0, Lcom/android/settingslib/mobile/TelephonyIcons;->MOBILE_CALL_STRENGTH_ICONS:[I

    return-void
.end method
