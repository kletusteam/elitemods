.class public final Lcom/android/settingslib/R$array;
.super Ljava/lang/Object;


# static fields
.field public static final avatar_image_descriptions:I = 0x7f03003c

.field public static final avatar_images:I = 0x7f03003d

.field public static final bluetooth_a2dp_codec_titles:I = 0x7f030069

.field public static final bluetooth_audio_active_device_summaries:I = 0x7f03006b

.field public static final bt_icon_bg_colors:I = 0x7f030079

.field public static final bt_icon_fg_colors:I = 0x7f03007a

.field public static final screen_resolution:I = 0x7f0301b1

.field public static final screen_resolution_format:I = 0x7f0301b2

.field public static final screen_resolution_text:I = 0x7f0301b3

.field public static final select_logd_size_lowram_titles:I = 0x7f0301ba

.field public static final select_logd_size_values:I = 0x7f0301bd

.field public static final select_logpersist_summaries:I = 0x7f0301bf

.field public static final select_logpersist_values:I = 0x7f0301c1

.field public static final wifi_status:I = 0x7f030256

.field public static final wifi_status_with_ssid:I = 0x7f030257
