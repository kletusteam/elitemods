.class public final Lcom/android/settingslib/R$string;
.super Ljava/lang/Object;


# static fields
.field public static final about_ad_service_instructions:I = 0x7f120044

.field public static final about_ad_service_instructions_for_global:I = 0x7f120045

.field public static final accessibility_data_one_bar:I = 0x7f1200a2

.field public static final accessibility_data_signal_full:I = 0x7f1200a3

.field public static final accessibility_data_three_bars:I = 0x7f1200a4

.field public static final accessibility_data_two_bars:I = 0x7f1200a5

.field public static final accessibility_ethernet_connected:I = 0x7f1200b5

.field public static final accessibility_ethernet_disconnected:I = 0x7f1200b6

.field public static final accessibility_no_calling:I = 0x7f1200e7

.field public static final accessibility_no_data:I = 0x7f1200e8

.field public static final accessibility_no_phone:I = 0x7f1200e9

.field public static final accessibility_no_wifi:I = 0x7f1200ec

.field public static final accessibility_phone_one_bar:I = 0x7f1200f2

.field public static final accessibility_phone_signal_full:I = 0x7f1200f3

.field public static final accessibility_phone_three_bars:I = 0x7f1200f4

.field public static final accessibility_phone_two_bars:I = 0x7f1200f5

.field public static final accessibility_wifi_one_bar:I = 0x7f120181

.field public static final accessibility_wifi_security_type_none:I = 0x7f120182

.field public static final accessibility_wifi_security_type_secured:I = 0x7f120183

.field public static final accessibility_wifi_signal_full:I = 0x7f120184

.field public static final accessibility_wifi_three_bars:I = 0x7f120185

.field public static final accessibility_wifi_two_bars:I = 0x7f120186

.field public static final accessibility_work_profile_app_description:I = 0x7f120188

.field public static final active_input_method_subtypes:I = 0x7f1201ab

.field public static final add_guest_failed:I = 0x7f120202

.field public static final add_user_failed:I = 0x7f12020b

.field public static final alarm_template:I = 0x7f120249

.field public static final alarm_template_far:I = 0x7f12024a

.field public static final app_link_open_always:I = 0x7f1202f3

.field public static final app_link_open_never:I = 0x7f1202f5

.field public static final battery_info_status_charging:I = 0x7f120476

.field public static final battery_info_status_charging_fast:I = 0x7f120478

.field public static final battery_info_status_charging_slow:I = 0x7f120479

.field public static final battery_info_status_charging_wireless:I = 0x7f12047b

.field public static final battery_info_status_discharging:I = 0x7f12047c

.field public static final battery_info_status_full:I = 0x7f12047d

.field public static final battery_info_status_full_charged:I = 0x7f12047e

.field public static final battery_info_status_not_charging:I = 0x7f120480

.field public static final battery_info_status_unknown:I = 0x7f120481

.field public static final bluetooth_a2dp_profile_summary_connected:I = 0x7f120545

.field public static final bluetooth_a2dp_profile_summary_use_for:I = 0x7f120546

.field public static final bluetooth_bc_profile_summary_connected:I = 0x7f12055e

.field public static final bluetooth_bc_profile_summary_use_for:I = 0x7f12055f

.field public static final bluetooth_connected:I = 0x7f1205ba

.field public static final bluetooth_connected_battery_level:I = 0x7f1205bb

.field public static final bluetooth_connected_no_a2dp:I = 0x7f1205bd

.field public static final bluetooth_connected_no_a2dp_battery_level:I = 0x7f1205be

.field public static final bluetooth_connected_no_headset:I = 0x7f1205bf

.field public static final bluetooth_connected_no_headset_battery_level:I = 0x7f1205c0

.field public static final bluetooth_connected_no_headset_no_a2dp:I = 0x7f1205c1

.field public static final bluetooth_connected_no_headset_no_a2dp_battery_level:I = 0x7f1205c2

.field public static final bluetooth_connecting:I = 0x7f1205c5

.field public static final bluetooth_disconnected:I = 0x7f1205fa

.field public static final bluetooth_disconnecting:I = 0x7f1205fb

.field public static final bluetooth_dun_profile_summary_connected:I = 0x7f120603

.field public static final bluetooth_dun_profile_summary_use_for:I = 0x7f120604

.field public static final bluetooth_headset_profile_summary_connected:I = 0x7f120624

.field public static final bluetooth_headset_profile_summary_use_for:I = 0x7f120625

.field public static final bluetooth_hearing_aid_profile_summary_connected:I = 0x7f120628

.field public static final bluetooth_hearing_aid_profile_summary_use_for:I = 0x7f120629

.field public static final bluetooth_hid_profile_summary_connected:I = 0x7f12062f

.field public static final bluetooth_hid_profile_summary_use_for:I = 0x7f120630

.field public static final bluetooth_le_audio_profile_summary_connected:I = 0x7f120638

.field public static final bluetooth_le_audio_profile_summary_use_for:I = 0x7f120639

.field public static final bluetooth_map_profile_summary_connected:I = 0x7f12063f

.field public static final bluetooth_map_profile_summary_use_for:I = 0x7f120640

.field public static final bluetooth_paired:I = 0x7f120660

.field public static final bluetooth_pairing:I = 0x7f120662

.field public static final bluetooth_pairing_device_down_error_message:I = 0x7f120667

.field public static final bluetooth_pairing_error_message:I = 0x7f12066c

.field public static final bluetooth_pairing_pin_error_message:I = 0x7f12066f

.field public static final bluetooth_pairing_rejected_error_message:I = 0x7f120671

.field public static final bluetooth_pan_nap_profile_summary_connected:I = 0x7f120677

.field public static final bluetooth_pan_profile_summary_use_for:I = 0x7f120678

.field public static final bluetooth_pan_user_profile_summary_connected:I = 0x7f120679

.field public static final bluetooth_profile_a2dp:I = 0x7f120693

.field public static final bluetooth_profile_a2dp_high_quality:I = 0x7f120694

.field public static final bluetooth_profile_a2dp_high_quality_unknown_codec:I = 0x7f120695

.field public static final bluetooth_profile_bc:I = 0x7f120696

.field public static final bluetooth_profile_broadcast:I = 0x7f120697

.field public static final bluetooth_profile_dun:I = 0x7f120699

.field public static final bluetooth_profile_headset:I = 0x7f12069a

.field public static final bluetooth_profile_hearing_aid:I = 0x7f12069b

.field public static final bluetooth_profile_hid:I = 0x7f12069c

.field public static final bluetooth_profile_le_audio:I = 0x7f12069d

.field public static final bluetooth_profile_map:I = 0x7f12069e

.field public static final bluetooth_profile_opp:I = 0x7f12069f

.field public static final bluetooth_profile_pan:I = 0x7f1206a0

.field public static final bluetooth_profile_pan_nap:I = 0x7f1206a1

.field public static final bluetooth_profile_pbap:I = 0x7f1206a2

.field public static final bluetooth_profile_pbap_summary:I = 0x7f1206a3

.field public static final bluetooth_profile_sap:I = 0x7f1206a4

.field public static final bluetooth_profile_vcp:I = 0x7f1206a5

.field public static final bluetooth_sap_profile_summary_connected:I = 0x7f1206ae

.field public static final bluetooth_sap_profile_summary_use_for:I = 0x7f1206af

.field public static final bluetooth_talkback_bluetooth:I = 0x7f1206f6

.field public static final bluetooth_talkback_computer:I = 0x7f1206f7

.field public static final bluetooth_talkback_group:I = 0x7f1206f8

.field public static final bluetooth_talkback_headphone:I = 0x7f1206f9

.field public static final bluetooth_talkback_headset:I = 0x7f1206fa

.field public static final bluetooth_talkback_imaging:I = 0x7f1206fb

.field public static final bluetooth_talkback_input_peripheral:I = 0x7f1206fc

.field public static final bluetooth_talkback_phone:I = 0x7f1206fd

.field public static final bluetooth_version_message:I = 0x7f120727

.field public static final bt_le_audio_qr_code_is_not_valid_format:I = 0x7f12078a

.field public static final bt_le_audio_scan_qr_code_scanner:I = 0x7f12078b

.field public static final cancel:I = 0x7f120814

.field public static final carrier_network_change_mode:I = 0x7f120840

.field public static final category_personal:I = 0x7f120850

.field public static final category_work:I = 0x7f120851

.field public static final cell_data_off_content_description:I = 0x7f12085c

.field public static final certinstaller_package:I = 0x7f120867

.field public static final charge_length_format:I = 0x7f120877

.field public static final choose_profile:I = 0x7f1208c9

.field public static final connected_via_app:I = 0x7f1209d6

.field public static final connected_via_network_scorer:I = 0x7f1209d8

.field public static final connected_via_network_scorer_default:I = 0x7f1209d9

.field public static final creating_new_guest_dialog_message:I = 0x7f120a0b

.field public static final creating_new_user_dialog_message:I = 0x7f120a0c

.field public static final data_connection_3_5g:I = 0x7f120a8f

.field public static final data_connection_3_5g_plus:I = 0x7f120a90

.field public static final data_connection_3g:I = 0x7f120a91

.field public static final data_connection_4g:I = 0x7f120a92

.field public static final data_connection_4g_lte:I = 0x7f120a93

.field public static final data_connection_4g_lte_plus:I = 0x7f120a94

.field public static final data_connection_4g_plus:I = 0x7f120a95

.field public static final data_connection_5g:I = 0x7f120a96

.field public static final data_connection_5g_basic:I = 0x7f120a97

.field public static final data_connection_5g_plus:I = 0x7f120a98

.field public static final data_connection_5g_sa:I = 0x7f120a99

.field public static final data_connection_5g_uwb:I = 0x7f120a9a

.field public static final data_connection_5ge_html:I = 0x7f120a9b

.field public static final data_connection_carrier_wifi:I = 0x7f120a9c

.field public static final data_connection_cdma:I = 0x7f120a9d

.field public static final data_connection_edge:I = 0x7f120a9e

.field public static final data_connection_gprs:I = 0x7f120a9f

.field public static final data_connection_lte:I = 0x7f120aa0

.field public static final data_connection_lte_plus:I = 0x7f120aa1

.field public static final data_usage_ota:I = 0x7f120af0

.field public static final data_usage_uninstalled_apps:I = 0x7f120b05

.field public static final data_usage_uninstalled_apps_users:I = 0x7f120b06

.field public static final default_user_icon_description:I = 0x7f120b63

.field public static final direct_boot_unaware_dialog_message:I = 0x7f120bfd

.field public static final disabled_by_admin:I = 0x7f120c10

.field public static final disabled_by_admin_summary_text:I = 0x7f120c11

.field public static final disabled_by_app_ops_text:I = 0x7f120c13

.field public static final done:I = 0x7f120c99

.field public static final enabled_by_admin:I = 0x7f120d5c

.field public static final failed_attempts_now_wiping_device:I = 0x7f120e39

.field public static final failed_attempts_now_wiping_dialog_dismiss:I = 0x7f120e3a

.field public static final failed_attempts_now_wiping_profile:I = 0x7f120e3b

.field public static final failed_attempts_now_wiping_user:I = 0x7f120e3c

.field public static final failed_to_open_app_settings_toast:I = 0x7f120e41

.field public static final guest_exit_guest:I = 0x7f121010

.field public static final guest_new_guest:I = 0x7f121011

.field public static final guest_remove_guest_confirm_button:I = 0x7f121012

.field public static final guest_remove_guest_dialog_title:I = 0x7f121013

.field public static final guest_reset_guest:I = 0x7f121014

.field public static final guest_reset_guest_confirm_button:I = 0x7f121015

.field public static final guest_reset_guest_dialog_title:I = 0x7f121016

.field public static final help_label:I = 0x7f1210b6

.field public static final ime_security_warning:I = 0x7f12114f

.field public static final ime_security_warning_global:I = 0x7f121150

.field public static final ime_security_warning_title_global:I = 0x7f121151

.field public static final ims_reg_status_not_registered:I = 0x7f121164

.field public static final ims_reg_status_registered:I = 0x7f121165

.field public static final loading_injected_setting_summary:I = 0x7f121342

.field public static final managed_user_title:I = 0x7f121506

.field public static final media_transfer_this_device_name:I = 0x7f121564

.field public static final media_transfer_wired_usb_device_name:I = 0x7f121567

.field public static final not_default_data_content_description:I = 0x7f121911

.field public static final notice_header:I = 0x7f121920

.field public static final notifications_sent_never:I = 0x7f1219c6

.field public static final oem_preferred_feedback_reporter:I = 0x7f1219d4

.field public static final okay:I = 0x7f1219de

.field public static final osu_completing_sign_up:I = 0x7f121a0b

.field public static final osu_connect_failed:I = 0x7f121a0c

.field public static final osu_opening_provider:I = 0x7f121a0d

.field public static final osu_sign_up_complete:I = 0x7f121a0e

.field public static final osu_sign_up_failed:I = 0x7f121a0f

.field public static final power_charging:I = 0x7f121af6

.field public static final power_charging_limited:I = 0x7f121af8

.field public static final power_discharging_duration:I = 0x7f121b00

.field public static final power_discharging_duration_enhanced:I = 0x7f121b01

.field public static final power_remaining_duration_only:I = 0x7f121b24

.field public static final power_remaining_duration_only_enhanced:I = 0x7f121b25

.field public static final power_remaining_duration_only_shutdown_imminent:I = 0x7f121b27

.field public static final power_remaining_duration_shutdown_imminent:I = 0x7f121b28

.field public static final power_remaining_less_than_duration:I = 0x7f121b29

.field public static final power_remaining_less_than_duration_only:I = 0x7f121b2a

.field public static final power_remaining_more_than_subtext:I = 0x7f121b2b

.field public static final power_remaining_only_more_than_subtext:I = 0x7f121b2c

.field public static final power_suggestion_battery_run_out:I = 0x7f121b30

.field public static final preference_summary_default_combination:I = 0x7f121b65

.field public static final private_dns_broken:I = 0x7f121c2d

.field public static final process_kernel_label:I = 0x7f121c42

.field public static final profile_info_settings_title:I = 0x7f121c59

.field public static final remaining_length_format:I = 0x7f121cd4

.field public static final running_process_item_user_label:I = 0x7f121d6d

.field public static final saved_network:I = 0x7f121d92

.field public static final screen_resolution_dialog_title:I = 0x7f121e1c

.field public static final screen_zoom_summary_custom:I = 0x7f121e42

.field public static final screen_zoom_summary_default:I = 0x7f121e43

.field public static final screen_zoom_summary_extremely_large:I = 0x7f121e44

.field public static final screen_zoom_summary_large:I = 0x7f121e45

.field public static final screen_zoom_summary_small:I = 0x7f121e46

.field public static final screen_zoom_summary_very_large:I = 0x7f121e47

.field public static final settings_package:I = 0x7f12206f

.field public static final speed_label_fast:I = 0x7f122220

.field public static final speed_label_okay:I = 0x7f122222

.field public static final speed_label_slow:I = 0x7f122223

.field public static final speed_label_very_fast:I = 0x7f122224

.field public static final status_unavailable:I = 0x7f122268

.field public static final string_KB:I = 0x7f1222f9

.field public static final string_KB_per_log_buffer:I = 0x7f1222fa

.field public static final string_MB:I = 0x7f1222fb

.field public static final string_MB_per_log_buffer:I = 0x7f1222fc

.field public static final string_off:I = 0x7f12230e

.field public static final string_offed:I = 0x7f12230f

.field public static final summary_empty:I = 0x7f122394

.field public static final summary_placeholder:I = 0x7f122396

.field public static final tap_to_sign_up:I = 0x7f1223fd

.field public static final tether_settings_title_all:I = 0x7f12245e

.field public static final tether_settings_title_bluetooth:I = 0x7f12245f

.field public static final tether_settings_title_usb:I = 0x7f122460

.field public static final tether_settings_title_usb_bluetooth:I = 0x7f122461

.field public static final tether_settings_title_wifi:I = 0x7f122462

.field public static final time_unit_just_now:I = 0x7f1224ac

.field public static final use_system_language_to_select_input_method_subtypes:I = 0x7f122674

.field public static final user_add_profile_item_summary:I = 0x7f12267b

.field public static final user_add_profile_item_title:I = 0x7f12267c

.field public static final user_add_user:I = 0x7f12267d

.field public static final user_add_user_item_summary:I = 0x7f12267e

.field public static final user_add_user_item_title:I = 0x7f12267f

.field public static final user_add_user_message_long:I = 0x7f122680

.field public static final user_add_user_message_short:I = 0x7f122681

.field public static final user_add_user_title:I = 0x7f122683

.field public static final user_add_user_type_title:I = 0x7f122684

.field public static final user_image_choose_photo:I = 0x7f1226b8

.field public static final user_image_take_photo:I = 0x7f1226ba

.field public static final user_info_settings_title:I = 0x7f1226bb

.field public static final user_need_lock_message:I = 0x7f1226bf

.field public static final user_new_profile_name:I = 0x7f1226c0

.field public static final user_new_user_name:I = 0x7f1226c1

.field public static final user_set_lock_button:I = 0x7f1226cb

.field public static final user_setup_button_setup_later:I = 0x7f1226cf

.field public static final user_setup_button_setup_now:I = 0x7f1226d0

.field public static final user_setup_dialog_message:I = 0x7f1226d1

.field public static final user_setup_dialog_title:I = 0x7f1226d2

.field public static final user_switch_to_user:I = 0x7f1226d8

.field public static final wifi_ap_unable_to_handle_new_sta:I = 0x7f122824

.field public static final wifi_check_password_try_again:I = 0x7f122869

.field public static final wifi_connected_low_quality:I = 0x7f12287a

.field public static final wifi_connected_no_internet:I = 0x7f12287b

.field public static final wifi_disabled_generic:I = 0x7f122891

.field public static final wifi_disabled_network_failure:I = 0x7f122893

.field public static final wifi_disabled_password_failure:I = 0x7f122894

.field public static final wifi_disconnected:I = 0x7f122897

.field public static final wifi_fail_to_scan:I = 0x7f1228ef

.field public static final wifi_limited_connection:I = 0x7f122926

.field public static final wifi_metered_label:I = 0x7f12293f

.field public static final wifi_no_internet:I = 0x7f12294a

.field public static final wifi_no_internet_no_reconnect:I = 0x7f12294b

.field public static final wifi_not_in_range:I = 0x7f122951

.field public static final wifi_passpoint_expired:I = 0x7f12296f

.field public static final wifi_remembered:I = 0x7f122983

.field public static final wifi_security_dpp:I = 0x7f12299e

.field public static final wifi_security_eap:I = 0x7f12299f

.field public static final wifi_security_eap_suiteb:I = 0x7f1229a0

.field public static final wifi_security_eap_wpa:I = 0x7f1229a1

.field public static final wifi_security_eap_wpa2_wpa3:I = 0x7f1229a2

.field public static final wifi_security_none:I = 0x7f1229a5

.field public static final wifi_security_none_owe:I = 0x7f1229a6

.field public static final wifi_security_owe:I = 0x7f1229a7

.field public static final wifi_security_psk_generic:I = 0x7f1229a9

.field public static final wifi_security_psk_sae:I = 0x7f1229aa

.field public static final wifi_security_sae:I = 0x7f1229ab

.field public static final wifi_security_short_dpp:I = 0x7f1229ae

.field public static final wifi_security_short_eap:I = 0x7f1229af

.field public static final wifi_security_short_eap_suiteb:I = 0x7f1229b0

.field public static final wifi_security_short_eap_wpa:I = 0x7f1229b1

.field public static final wifi_security_short_eap_wpa2_wpa3:I = 0x7f1229b2

.field public static final wifi_security_short_none_owe:I = 0x7f1229b3

.field public static final wifi_security_short_owe:I = 0x7f1229b4

.field public static final wifi_security_short_psk_generic:I = 0x7f1229b5

.field public static final wifi_security_short_psk_sae:I = 0x7f1229b6

.field public static final wifi_security_short_sae:I = 0x7f1229b7

.field public static final wifi_security_short_wep:I = 0x7f1229b8

.field public static final wifi_security_short_wpa:I = 0x7f1229b9

.field public static final wifi_security_short_wpa2:I = 0x7f1229ba

.field public static final wifi_security_short_wpa_wpa2:I = 0x7f1229bb

.field public static final wifi_security_wapi_cert:I = 0x7f1229bc

.field public static final wifi_security_wapi_psk:I = 0x7f1229bd

.field public static final wifi_security_wep:I = 0x7f1229be

.field public static final wifi_security_wpa:I = 0x7f1229bf

.field public static final wifi_security_wpa2:I = 0x7f1229c0

.field public static final wifi_security_wpa_wpa2:I = 0x7f1229c1

.field public static final wifi_status_no_internet:I = 0x7f122a0f

.field public static final wifi_status_sign_in_required:I = 0x7f122a10

.field public static final wifi_unmetered_label:I = 0x7f122a3e

.field public static final wifitrackerlib_wifi_connected_cannot_provide_internet:I = 0x7f122a85

.field public static final zen_alarm_warning:I = 0x7f122b05

.field public static final zen_alarm_warning_indef:I = 0x7f122b06

.field public static final zen_mode_duration_always_prompt_title:I = 0x7f122b68

.field public static final zen_mode_duration_settings_title:I = 0x7f122b69

.field public static final zen_mode_enable_dialog_turn_on:I = 0x7f122b6f

.field public static final zen_mode_forever:I = 0x7f122b7f

.field public static final zen_mode_settings_turn_on_dialog_title:I = 0x7f122bde
