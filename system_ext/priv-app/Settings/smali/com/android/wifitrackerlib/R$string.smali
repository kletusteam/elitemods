.class public final Lcom/android/wifitrackerlib/R$string;
.super Ljava/lang/Object;


# static fields
.field public static final wifi_connected_low_quality:I = 0x7f12287a

.field public static final wifi_security_wapi_cert:I = 0x7f1229bc

.field public static final wifi_security_wapi_psk:I = 0x7f1229bd

.field public static final wifitrackerlib_admin_restricted_network:I = 0x7f122a70

.field public static final wifitrackerlib_auto_connect_disable:I = 0x7f122a71

.field public static final wifitrackerlib_available_via_app:I = 0x7f122a72

.field public static final wifitrackerlib_connected_via_app:I = 0x7f122a74

.field public static final wifitrackerlib_help_url_imsi_protection:I = 0x7f122a75

.field public static final wifitrackerlib_imsi_protection_warning:I = 0x7f122a76

.field public static final wifitrackerlib_no_attribution_annotation_packages:I = 0x7f122a77

.field public static final wifitrackerlib_osu_completing_sign_up:I = 0x7f122a78

.field public static final wifitrackerlib_osu_connect_failed:I = 0x7f122a79

.field public static final wifitrackerlib_osu_opening_provider:I = 0x7f122a7a

.field public static final wifitrackerlib_osu_sign_up_complete:I = 0x7f122a7b

.field public static final wifitrackerlib_osu_sign_up_failed:I = 0x7f122a7c

.field public static final wifitrackerlib_private_dns_broken:I = 0x7f122a7d

.field public static final wifitrackerlib_saved_network:I = 0x7f122a7e

.field public static final wifitrackerlib_summary_separator:I = 0x7f122a80

.field public static final wifitrackerlib_tap_to_renew_subscription_and_connect:I = 0x7f122a81

.field public static final wifitrackerlib_tap_to_sign_up:I = 0x7f122a82

.field public static final wifitrackerlib_wifi_ap_unable_to_handle_new_sta:I = 0x7f122a83

.field public static final wifitrackerlib_wifi_check_password_try_again:I = 0x7f122a84

.field public static final wifitrackerlib_wifi_connected_cannot_provide_internet:I = 0x7f122a85

.field public static final wifitrackerlib_wifi_disabled_generic:I = 0x7f122a86

.field public static final wifitrackerlib_wifi_disabled_network_failure:I = 0x7f122a87

.field public static final wifitrackerlib_wifi_disabled_password_failure:I = 0x7f122a88

.field public static final wifitrackerlib_wifi_disconnected:I = 0x7f122a89

.field public static final wifitrackerlib_wifi_mbo_assoc_disallowed_cannot_connect:I = 0x7f122a8b

.field public static final wifitrackerlib_wifi_mbo_assoc_disallowed_max_num_sta_associated:I = 0x7f122a8c

.field public static final wifitrackerlib_wifi_mbo_oce_assoc_disallowed_insufficient_rssi:I = 0x7f122a8d

.field public static final wifitrackerlib_wifi_metered_label:I = 0x7f122a8e

.field public static final wifitrackerlib_wifi_network_not_found:I = 0x7f122a8f

.field public static final wifitrackerlib_wifi_no_internet:I = 0x7f122a90

.field public static final wifitrackerlib_wifi_no_internet_no_reconnect:I = 0x7f122a91

.field public static final wifitrackerlib_wifi_passpoint_expired:I = 0x7f122a92

.field public static final wifitrackerlib_wifi_poor_channel_conditions:I = 0x7f122a93

.field public static final wifitrackerlib_wifi_remembered:I = 0x7f122a94

.field public static final wifitrackerlib_wifi_security_eap_suiteb:I = 0x7f122a95

.field public static final wifitrackerlib_wifi_security_eap_wpa3:I = 0x7f122a96

.field public static final wifitrackerlib_wifi_security_eap_wpa_wpa2:I = 0x7f122a97

.field public static final wifitrackerlib_wifi_security_eap_wpa_wpa2_wpa3:I = 0x7f122a98

.field public static final wifitrackerlib_wifi_security_none:I = 0x7f122a99

.field public static final wifitrackerlib_wifi_security_owe:I = 0x7f122a9a

.field public static final wifitrackerlib_wifi_security_passpoint:I = 0x7f122a9b

.field public static final wifitrackerlib_wifi_security_sae:I = 0x7f122a9c

.field public static final wifitrackerlib_wifi_security_short_eap_suiteb:I = 0x7f122a9d

.field public static final wifitrackerlib_wifi_security_short_eap_wpa3:I = 0x7f122a9e

.field public static final wifitrackerlib_wifi_security_short_eap_wpa_wpa2:I = 0x7f122a9f

.field public static final wifitrackerlib_wifi_security_short_eap_wpa_wpa2_wpa3:I = 0x7f122aa0

.field public static final wifitrackerlib_wifi_security_short_owe:I = 0x7f122aa1

.field public static final wifitrackerlib_wifi_security_short_sae:I = 0x7f122aa2

.field public static final wifitrackerlib_wifi_security_short_wpa_wpa2:I = 0x7f122aa3

.field public static final wifitrackerlib_wifi_security_short_wpa_wpa2_wpa3:I = 0x7f122aa4

.field public static final wifitrackerlib_wifi_security_wep:I = 0x7f122aa5

.field public static final wifitrackerlib_wifi_security_wpa_wpa2:I = 0x7f122aa6

.field public static final wifitrackerlib_wifi_security_wpa_wpa2_wpa3:I = 0x7f122aa7

.field public static final wifitrackerlib_wifi_standard_11ac:I = 0x7f122aa8

.field public static final wifitrackerlib_wifi_standard_11ad:I = 0x7f122aa9

.field public static final wifitrackerlib_wifi_standard_11ax:I = 0x7f122aaa

.field public static final wifitrackerlib_wifi_standard_11be:I = 0x7f122aab

.field public static final wifitrackerlib_wifi_standard_11n:I = 0x7f122aac

.field public static final wifitrackerlib_wifi_standard_legacy:I = 0x7f122aad

.field public static final wifitrackerlib_wifi_standard_unknown:I = 0x7f122aae

.field public static final wifitrackerlib_wifi_unmetered_label:I = 0x7f122aaf

.field public static final wifitrackerlib_wifi_wont_autoconnect_for_now:I = 0x7f122ab0
