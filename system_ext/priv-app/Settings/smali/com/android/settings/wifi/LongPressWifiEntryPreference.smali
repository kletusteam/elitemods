.class public Lcom/android/settings/wifi/LongPressWifiEntryPreference;
.super Lcom/android/settings/wifi/MiuiWifiEntryPreference;


# instance fields
.field private final mFragment:Landroidx/fragment/app/Fragment;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/wifitrackerlib/WifiEntry;Landroidx/fragment/app/Fragment;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;-><init>(Landroid/content/Context;Lcom/android/wifitrackerlib/WifiEntry;)V

    iput-object p3, p0, Lcom/android/settings/wifi/LongPressWifiEntryPreference;->mFragment:Landroidx/fragment/app/Fragment;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/wifitrackerlib/WifiEntry;Landroidx/fragment/app/Fragment;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p4}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;-><init>(Landroid/content/Context;Lcom/android/wifitrackerlib/WifiEntry;Z)V

    iput-object p3, p0, Lcom/android/settings/wifi/LongPressWifiEntryPreference;->mFragment:Landroidx/fragment/app/Fragment;

    return-void
.end method
