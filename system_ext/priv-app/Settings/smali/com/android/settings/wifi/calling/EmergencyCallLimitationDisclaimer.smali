.class public Lcom/android/settings/wifi/calling/EmergencyCallLimitationDisclaimer;
.super Lcom/android/settings/wifi/calling/DisclaimerItem;


# static fields
.field static final KEY_HAS_AGREED_EMERGENCY_LIMITATION_DISCLAIMER:Ljava/lang/String; = "key_has_agreed_emergency_limitation_disclaimer"
    .annotation build Lcom/android/internal/annotations/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/wifi/calling/DisclaimerItem;-><init>(Landroid/content/Context;I)V

    return-void
.end method


# virtual methods
.method protected getMessageId()I
    .locals 0

    sget p0, Lcom/android/settings/R$string;->wfc_disclaimer_emergency_limitation_desc_text:I

    return p0
.end method

.method protected getName()Ljava/lang/String;
    .locals 0

    const-string p0, "EmergencyCallLimitationDisclaimer"

    return-object p0
.end method

.method protected getPrefKey()Ljava/lang/String;
    .locals 0

    const-string p0, "key_has_agreed_emergency_limitation_disclaimer"

    return-object p0
.end method

.method protected getTitleId()I
    .locals 0

    sget p0, Lcom/android/settings/R$string;->wfc_disclaimer_emergency_limitation_title_text:I

    return p0
.end method

.method shouldShow()Z
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {v0, v1}, Landroid/os/PersistableBundle;->getInt(Ljava/lang/String;)I

    move-result v0

    goto/32 :goto_4

    nop

    :goto_1
    invoke-virtual {p0}, Lcom/android/settings/wifi/calling/DisclaimerItem;->getCarrierConfig()Landroid/os/PersistableBundle;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_2
    const-string/jumbo v0, "shouldShow: false due to carrier config is default(-1)."

    goto/32 :goto_b

    nop

    :goto_3
    const-string v1, "emergency_notification_delay_int"

    goto/32 :goto_0

    nop

    :goto_4
    const/4 v1, -0x1

    goto/32 :goto_8

    nop

    :goto_5
    invoke-super {p0}, Lcom/android/settings/wifi/calling/DisclaimerItem;->shouldShow()Z

    move-result p0

    goto/32 :goto_9

    nop

    :goto_6
    return p0

    :goto_7
    goto/32 :goto_5

    nop

    :goto_8
    if-eq v0, v1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_2

    nop

    :goto_9
    return p0

    :goto_a
    const/4 p0, 0x0

    goto/32 :goto_6

    nop

    :goto_b
    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/calling/DisclaimerItem;->logd(Ljava/lang/String;)V

    goto/32 :goto_a

    nop
.end method
