.class public Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;
.super Lcom/android/settings/dashboard/DashboardFragment;

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions$FragmentListener;
    }
.end annotation


# static fields
.field public static final SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settingslib/search/Indexable$SearchIndexProvider;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDualSim:Landroidx/preference/Preference;

.field private mDualWifi:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

.field private mEnableWifiLinkTurbo:Landroidx/preference/CheckBoxPreference;

.field private mHandler:Landroid/os/Handler;

.field private mListSummaries:[Ljava/lang/String;

.field private mListValues:[Ljava/lang/String;

.field private mListener:Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions$FragmentListener;

.field private mMode:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

.field private mPrimaryEnabled:Z

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mSlaveEnabled:Z

.field private mSlaveWifiUtils:Lcom/android/settingslib/wifi/SlaveWifiUtils;

.field private mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmDualWifi(Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;)Lcom/android/settingslib/miuisettings/preference/ValuePreference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mDualWifi:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPrimaryEnabled(Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mPrimaryEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSlaveEnabled(Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mSlaveEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSlaveWifiUtils(Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;)Lcom/android/settingslib/wifi/SlaveWifiUtils;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mSlaveWifiUtils:Lcom/android/settingslib/wifi/SlaveWifiUtils;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmWifiManager(Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;)Landroid/net/wifi/WifiManager;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mWifiManager:Landroid/net/wifi/WifiManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmPrimaryEnabled(Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mPrimaryEnabled:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmSlaveEnabled(Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mSlaveEnabled:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateDualSimPref(Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->updateDualSimPref()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateDualWifiPref(Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->updateDualWifiPref()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions$3;

    invoke-direct {v0}, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions$3;-><init>()V

    sput-object v0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->SEARCH_INDEX_DATA_PROVIDER:Lcom/android/settingslib/search/Indexable$SearchIndexProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/android/settings/dashboard/DashboardFragment;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mPrimaryEnabled:Z

    iput-boolean v0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mSlaveEnabled:Z

    new-instance v0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions$1;-><init>(Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions$2;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions$2;-><init>(Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;)V

    iput-object v0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private initDualWifiAndDualSim()V
    .locals 2

    const-string v0, "dual_wifi"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    iput-object v0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mDualWifi:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/wifi/MiuiWifiAssistFeatureSupport;->isDualWifiSupported(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mDualWifi:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mDualWifi:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mDualWifi:Lcom/android/settingslib/miuisettings/preference/ValuePreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/settingslib/miuisettings/preference/ValuePreference;->setShowRightArrow(Z)V

    :cond_1
    :goto_0
    const-string v0, "button_smart_dual_sim_key"

    invoke-virtual {p0, v0}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mDualSim:Landroidx/preference/Preference;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/wifi/MiuiWifiAssistFeatureSupport;->isDualSimSupported(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mDualSim:Landroidx/preference/Preference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_2
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.net.wifi.WIFI_SLAVE_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.telephony.action.CARRIER_CONFIG_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mContext:Landroid/content/Context;

    iget-object p0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private showOrHideOption(Z)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mMode:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    invoke-direct {p0}, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->updateSwitchAndModeSummary()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object p0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mMode:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :goto_0
    return-void
.end method

.method private updateDualSimPref()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mDualSim:Landroidx/preference/Preference;

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {v0}, Lcom/android/settings/wifi/MiuiWifiAssistFeatureSupport;->isDualSimSupported(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object p0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mDualSim:Landroidx/preference/Preference;

    invoke-virtual {v0, p0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object p0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mDualSim:Landroidx/preference/Preference;

    invoke-virtual {v0, p0}, Landroidx/preference/PreferenceGroup;->addPreference(Landroidx/preference/Preference;)Z

    :cond_2
    :goto_0
    return-void
.end method

.method private updateDualWifiPref()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x101

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mHandler:Landroid/os/Handler;

    invoke-virtual {p0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method private updateSwitchAndModeSummary()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "link_turbo_mode"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mMode:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    iget-object v2, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mListValues:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lmiuix/preference/DropDownPreference;->setValue(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mMode:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    iget-object p0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mListSummaries:[Ljava/lang/String;

    aget-object p0, p0, v0

    invoke-virtual {v1, p0}, Landroidx/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method protected createPreferenceControllers(Landroid/content/Context;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/settingslib/core/AbstractPreferenceController;",
            ">;"
        }
    .end annotation

    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboController;

    invoke-direct {v0, p1}, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboController;-><init>(Landroid/content/Context;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/android/settings/wifi/WifiAssistantController;

    invoke-direct {v0, p1}, Lcom/android/settings/wifi/WifiAssistantController;-><init>(Landroid/content/Context;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method protected getLogTag()Ljava/lang/String;
    .locals 0

    const-string p0, "WifiLinkTurboOptions"

    return-object p0
.end method

.method protected getPreferenceScreenResId()I
    .locals 0

    sget p0, Lcom/android/settings/R$xml;->wifi_link_turbo_option:I

    return p0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/android/settings/dashboard/DashboardFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mContext:Landroid/content/Context;

    const-string/jumbo v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/wifi/WifiManager;

    iput-object p1, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mWifiManager:Landroid/net/wifi/WifiManager;

    iget-object p1, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/settingslib/wifi/SlaveWifiUtils;->getInstance(Landroid/content/Context;)Lcom/android/settingslib/wifi/SlaveWifiUtils;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mSlaveWifiUtils:Lcom/android/settingslib/wifi/SlaveWifiUtils;

    const-string p1, "enable_wifi_link_turbo"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mEnableWifiLinkTurbo:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-object p1, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mContext:Landroid/content/Context;

    instance-of v0, p1, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions$FragmentListener;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions$FragmentListener;

    iput-object v0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mListener:Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions$FragmentListener;

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/android/settings/R$array;->wifi_link_turbo_mode_values:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mListValues:[Ljava/lang/String;

    iget-object p1, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/android/settings/R$array;->wifi_link_turbo_mode_summary_entries:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mListSummaries:[Ljava/lang/String;

    const-string/jumbo p1, "wifi_link_turbo_mode"

    invoke-virtual {p0, p1}, Landroidx/preference/PreferenceFragmentCompat;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    iput-object p1, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mMode:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    iget-object p1, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mEnableWifiLinkTurbo:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mMode:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mMode:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    :cond_1
    invoke-direct {p0}, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->initDualWifiAndDualSim()V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/android/settingslib/core/lifecycle/ObservablePreferenceFragment;->onDestroy()V

    iget-object v0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    const-string v2, "enable_wifi_link_turbo"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->showOrHideOption(Z)V

    if-eqz p1, :cond_0

    iget-object p2, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mContext:Landroid/content/Context;

    invoke-static {p2, v0}, Lcom/android/settings/wifi/linkturbo/LinkTurboUtils;->setLinkTurboOptions(Landroid/content/Context;I)V

    iget-object p2, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mListener:Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions$FragmentListener;

    if-eqz p2, :cond_0

    invoke-interface {p2, v0}, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions$FragmentListener;->setLinkTurboOptionsCallback(I)V

    :cond_0
    iget-object p0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mListener:Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions$FragmentListener;

    if-eqz p0, :cond_1

    invoke-interface {p0, p1}, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions$FragmentListener;->enableWifiLinkTurboCallback(Z)V

    :cond_1
    const/4 p0, 0x1

    return p0

    :cond_2
    invoke-virtual {p1}, Landroidx/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object p1

    const-string/jumbo v1, "wifi_link_turbo_mode"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget-object p2, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mContext:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p2

    const-string v1, "link_turbo_mode"

    invoke-static {p2, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-direct {p0}, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->updateSwitchAndModeSummary()V

    :cond_3
    return v0
.end method

.method public onPreferenceTreeClick(Landroidx/preference/Preference;)Z
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mDualSim:Landroidx/preference/Preference;

    if-ne p1, v0, :cond_0

    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    const/high16 v0, 0x20000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v0, "com.android.phone"

    const-string v1, "com.android.phone.settings.MiuiConfigureMobileSettings"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "extra_from"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    const/4 p0, 0x1

    return p0

    :cond_0
    invoke-super {p0, p1}, Lcom/android/settings/dashboard/DashboardFragment;->onPreferenceTreeClick(Landroidx/preference/Preference;)Z

    move-result p0

    return p0
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/android/settings/dashboard/DashboardFragment;->onResume()V

    iget-object v0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboSettings;

    invoke-virtual {v0}, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboSettings;->isWifiLinkTurboEnabled()Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mEnableWifiLinkTurbo:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mMode:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroidx/preference/PreferenceFragmentCompat;->getPreferenceScreen()Landroidx/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mMode:Lcom/android/settingslib/miuisettings/preference/miuix/DropDownPreference;

    invoke-virtual {v0, v1}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->updateSwitchAndModeSummary()V

    iget-object v0, p0, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/settings/wifi/linkturbo/LinkTurboUtils;->setLinkTurboOptions(Landroid/content/Context;I)V

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->updateDualWifiPref()V

    invoke-direct {p0}, Lcom/android/settings/wifi/linkturbo/WifiLinkTurboOptions;->updateDualSimPref()V

    return-void
.end method
