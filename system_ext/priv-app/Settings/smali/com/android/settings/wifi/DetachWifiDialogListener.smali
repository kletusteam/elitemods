.class public Lcom/android/settings/wifi/DetachWifiDialogListener;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mActivity:Landroid/app/Activity;


# direct methods
.method static bridge synthetic -$$Nest$fgetmActivity(Lcom/android/settings/wifi/DetachWifiDialogListener;)Landroid/app/Activity;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/wifi/DetachWifiDialogListener;->mActivity:Landroid/app/Activity;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmActivity(Lcom/android/settings/wifi/DetachWifiDialogListener;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/DetachWifiDialogListener;->mActivity:Landroid/app/Activity;

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetTAG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/settings/wifi/DetachWifiDialogListener;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/settings/wifi/DetachWifiDialogListener;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/wifi/DetachWifiDialogListener;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/wifi/DetachWifiDialogListener;->mActivity:Landroid/app/Activity;

    return-void
.end method


# virtual methods
.method public clearOnDetach(Landroid/app/Dialog;)V
    .locals 1

    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    new-instance v0, Lcom/android/settings/wifi/DetachWifiDialogListener$1;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/DetachWifiDialogListener$1;-><init>(Lcom/android/settings/wifi/DetachWifiDialogListener;)V

    invoke-virtual {p1, v0}, Landroid/view/ViewTreeObserver;->addOnWindowAttachListener(Landroid/view/ViewTreeObserver$OnWindowAttachListener;)V

    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 0

    sget-object p0, Lcom/android/settings/wifi/DetachWifiDialogListener;->TAG:Ljava/lang/String;

    const-string p1, "Dialog onDismiss"

    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
