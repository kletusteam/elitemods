.class Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;->resortAccessPoint(Ljava/util/Collection;)Ljava/util/ArrayList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/android/settings/wifi/SavedAccessPointPreference;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings$2;->this$0:Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/android/settings/wifi/SavedAccessPointPreference;Lcom/android/settings/wifi/SavedAccessPointPreference;)I
    .locals 0

    instance-of p0, p1, Lcom/android/settings/wifi/SavedAccessPointPreference;

    if-nez p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    instance-of p0, p2, Lcom/android/settings/wifi/SavedAccessPointPreference;

    if-nez p0, :cond_1

    const/4 p0, -0x1

    return p0

    :cond_1
    invoke-virtual {p1}, Lcom/android/settings/wifi/SavedAccessPointPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object p0

    invoke-virtual {p0}, Lcom/android/wifitrackerlib/WifiEntry;->getSsid()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2}, Lcom/android/settings/wifi/SavedAccessPointPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object p1

    invoke-virtual {p1}, Lcom/android/wifitrackerlib/WifiEntry;->getSsid()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result p2

    if-nez p2, :cond_2

    invoke-virtual {p0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p2

    :cond_2
    return p2
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/android/settings/wifi/SavedAccessPointPreference;

    check-cast p2, Lcom/android/settings/wifi/SavedAccessPointPreference;

    invoke-virtual {p0, p1, p2}, Lcom/android/settings/wifi/MiuiSavedAccessPointsWifiSettings$2;->compare(Lcom/android/settings/wifi/SavedAccessPointPreference;Lcom/android/settings/wifi/SavedAccessPointPreference;)I

    move-result p0

    return p0
.end method
