.class public Lcom/android/settings/wifi/linkturbo/FormatBytesUtil;
.super Ljava/lang/Object;


# static fields
.field public static BString:Ljava/lang/String; = "B"

.field public static GBString:Ljava/lang/String; = "GB"

.field public static KBString:Ljava/lang/String; = "KB"

.field public static MBString:Ljava/lang/String; = "MB"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public static formatBytes(J)Ljava/lang/String;
    .locals 6

    const-wide/32 v0, 0x40000000

    cmp-long v0, p0, v0

    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    const/4 v3, 0x1

    if-ltz v0, :cond_0

    long-to-double p0, p0

    mul-double/2addr p0, v1

    const-wide/high16 v0, 0x41d0000000000000L    # 1.073741824E9

    div-double/2addr p0, v0

    sget-object v0, Lcom/android/settings/wifi/linkturbo/FormatBytesUtil;->GBString:Ljava/lang/String;

    const/4 v3, 0x2

    goto :goto_0

    :cond_0
    const-wide/32 v4, 0x100000

    cmp-long v0, p0, v4

    if-ltz v0, :cond_1

    long-to-double p0, p0

    mul-double/2addr p0, v1

    const-wide/high16 v0, 0x4130000000000000L    # 1048576.0

    div-double/2addr p0, v0

    sget-object v0, Lcom/android/settings/wifi/linkturbo/FormatBytesUtil;->MBString:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-wide/16 v4, 0x400

    cmp-long v0, p0, v4

    if-ltz v0, :cond_2

    long-to-double p0, p0

    mul-double/2addr p0, v1

    const-wide/high16 v0, 0x4090000000000000L    # 1024.0

    div-double/2addr p0, v0

    sget-object v0, Lcom/android/settings/wifi/linkturbo/FormatBytesUtil;->KBString:Ljava/lang/String;

    goto :goto_0

    :cond_2
    long-to-double p0, p0

    mul-double/2addr p0, v1

    sget-object v0, Lcom/android/settings/wifi/linkturbo/FormatBytesUtil;->BString:Ljava/lang/String;

    :goto_0
    invoke-static {p0, p1, v0, v3}, Lcom/android/settings/wifi/linkturbo/FormatBytesUtil;->textFormat(DLjava/lang/String;I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static textFormat(DLjava/lang/String;I)Ljava/lang/String;
    .locals 5

    const-wide v0, 0x408f3c0000000000L    # 999.5

    cmpl-double v0, p0, v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-gtz v0, :cond_2

    sget-object v0, Lcom/android/settings/wifi/linkturbo/FormatBytesUtil;->BString:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-wide v3, 0x4058e00000000000L    # 99.5

    cmpl-double v0, p0, v3

    if-lez v0, :cond_1

    new-array p3, v2, [Ljava/lang/Object;

    invoke-static {p0, p1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p0

    aput-object p0, p3, v1

    const-string p0, "%.01f"

    invoke-static {p0, p3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v3, 0x10

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "%.0"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 p3, 0x66

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    new-array v0, v2, [Ljava/lang/Object;

    invoke-static {p0, p1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p0

    aput-object p0, v0, v1

    invoke-static {p3, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    :cond_2
    :goto_0
    new-array p3, v2, [Ljava/lang/Object;

    double-to-int p0, p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, p3, v1

    const-string p0, "%d"

    invoke-static {p0, p3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    :goto_1
    if-eqz p2, :cond_3

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_3
    return-object p0
.end method
