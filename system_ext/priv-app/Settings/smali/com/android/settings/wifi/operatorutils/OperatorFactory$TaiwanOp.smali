.class Lcom/android/settings/wifi/operatorutils/OperatorFactory$TaiwanOp;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/wifi/operatorutils/OperatorFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TaiwanOp"
.end annotation


# static fields
.field private static final INSTANCE:Lcom/android/settings/wifi/operatorutils/operatorutilsimpl/TaiwanOperator;


# direct methods
.method static bridge synthetic -$$Nest$sfgetINSTANCE()Lcom/android/settings/wifi/operatorutils/operatorutilsimpl/TaiwanOperator;
    .locals 1

    sget-object v0, Lcom/android/settings/wifi/operatorutils/OperatorFactory$TaiwanOp;->INSTANCE:Lcom/android/settings/wifi/operatorutils/operatorutilsimpl/TaiwanOperator;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/android/settings/wifi/operatorutils/operatorutilsimpl/TaiwanOperator;

    invoke-static {}, Lcom/android/settings/wifi/operatorutils/OperatorFactory;->-$$Nest$sfgetmContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/settings/wifi/operatorutils/operatorutilsimpl/TaiwanOperator;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/settings/wifi/operatorutils/OperatorFactory$TaiwanOp;->INSTANCE:Lcom/android/settings/wifi/operatorutils/operatorutilsimpl/TaiwanOperator;

    return-void
.end method
