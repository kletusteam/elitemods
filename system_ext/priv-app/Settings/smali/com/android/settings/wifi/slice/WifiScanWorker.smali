.class public Lcom/android/settings/wifi/slice/WifiScanWorker;
.super Lcom/android/settings/slices/SliceBackgroundWorker;

# interfaces
.implements Lcom/android/wifitrackerlib/WifiPickerTracker$WifiPickerTrackerCallback;
.implements Landroidx/lifecycle/LifecycleOwner;
.implements Lcom/android/wifitrackerlib/WifiEntry$WifiEntryCallback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/settings/slices/SliceBackgroundWorker<",
        "Lcom/android/settings/wifi/slice/WifiSliceItem;",
        ">;",
        "Lcom/android/wifitrackerlib/WifiPickerTracker$WifiPickerTrackerCallback;",
        "Landroidx/lifecycle/LifecycleOwner;",
        "Lcom/android/wifitrackerlib/WifiEntry$WifiEntryCallback;"
    }
.end annotation


# instance fields
.field final mLifecycleRegistry:Landroidx/lifecycle/LifecycleRegistry;

.field protected mWifiPickerTracker:Lcom/android/wifitrackerlib/WifiPickerTracker;

.field protected mWifiPickerTrackerHelper:Lcom/android/settings/wifi/WifiPickerTrackerHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/slices/SliceBackgroundWorker;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    new-instance p2, Landroidx/lifecycle/LifecycleRegistry;

    invoke-direct {p2, p0}, Landroidx/lifecycle/LifecycleRegistry;-><init>(Landroidx/lifecycle/LifecycleOwner;)V

    iput-object p2, p0, Lcom/android/settings/wifi/slice/WifiScanWorker;->mLifecycleRegistry:Landroidx/lifecycle/LifecycleRegistry;

    new-instance v0, Lcom/android/settings/wifi/WifiPickerTrackerHelper;

    invoke-direct {v0, p2, p1, p0}, Lcom/android/settings/wifi/WifiPickerTrackerHelper;-><init>(Landroidx/lifecycle/Lifecycle;Landroid/content/Context;Lcom/android/wifitrackerlib/WifiPickerTracker$WifiPickerTrackerCallback;)V

    iput-object v0, p0, Lcom/android/settings/wifi/slice/WifiScanWorker;->mWifiPickerTrackerHelper:Lcom/android/settings/wifi/WifiPickerTrackerHelper;

    invoke-virtual {v0}, Lcom/android/settings/wifi/WifiPickerTrackerHelper;->getWifiPickerTracker()Lcom/android/wifitrackerlib/WifiPickerTracker;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/wifi/slice/WifiScanWorker;->mWifiPickerTracker:Lcom/android/wifitrackerlib/WifiPickerTracker;

    sget-object p0, Landroidx/lifecycle/Lifecycle$State;->INITIALIZED:Landroidx/lifecycle/Lifecycle$State;

    invoke-virtual {p2, p0}, Landroidx/lifecycle/LifecycleRegistry;->markState(Landroidx/lifecycle/Lifecycle$State;)V

    sget-object p0, Landroidx/lifecycle/Lifecycle$State;->CREATED:Landroidx/lifecycle/Lifecycle$State;

    invoke-virtual {p2, p0}, Landroidx/lifecycle/LifecycleRegistry;->markState(Landroidx/lifecycle/Lifecycle$State;)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object p0, p0, Lcom/android/settings/wifi/slice/WifiScanWorker;->mLifecycleRegistry:Landroidx/lifecycle/LifecycleRegistry;

    sget-object v0, Landroidx/lifecycle/Lifecycle$State;->DESTROYED:Landroidx/lifecycle/Lifecycle$State;

    invoke-virtual {p0, v0}, Landroidx/lifecycle/LifecycleRegistry;->markState(Landroidx/lifecycle/Lifecycle$State;)V

    return-void
.end method

.method public connectCarrierNetwork()V
    .locals 1

    iget-object p0, p0, Lcom/android/settings/wifi/slice/WifiScanWorker;->mWifiPickerTrackerHelper:Lcom/android/settings/wifi/WifiPickerTrackerHelper;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/WifiPickerTrackerHelper;->connectCarrierNetwork(Lcom/android/wifitrackerlib/WifiEntry$ConnectCallback;)Z

    return-void
.end method

.method protected getApRowCount()I
    .locals 0

    const/4 p0, 0x3

    return p0
.end method

.method public getLifecycle()Landroidx/lifecycle/Lifecycle;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/wifi/slice/WifiScanWorker;->mLifecycleRegistry:Landroidx/lifecycle/LifecycleRegistry;

    return-object p0
.end method

.method public getWifiEntry(Ljava/lang/String;)Lcom/android/wifitrackerlib/WifiEntry;
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/slice/WifiScanWorker;->mWifiPickerTracker:Lcom/android/wifitrackerlib/WifiPickerTracker;

    invoke-virtual {v0}, Lcom/android/wifitrackerlib/WifiPickerTracker;->getConnectedWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/wifitrackerlib/WifiEntry;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p0, p0, Lcom/android/settings/wifi/slice/WifiScanWorker;->mWifiPickerTracker:Lcom/android/wifitrackerlib/WifiPickerTracker;

    invoke-virtual {p0}, Lcom/android/wifitrackerlib/WifiPickerTracker;->getWifiEntries()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/wifitrackerlib/WifiEntry;

    invoke-virtual {v0}, Lcom/android/wifitrackerlib/WifiEntry;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public onNumSavedNetworksChanged()V
    .locals 0

    return-void
.end method

.method public onNumSavedSubscriptionsChanged()V
    .locals 0

    return-void
.end method

.method protected onSlicePinned()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/slice/WifiScanWorker;->mLifecycleRegistry:Landroidx/lifecycle/LifecycleRegistry;

    sget-object v1, Landroidx/lifecycle/Lifecycle$State;->STARTED:Landroidx/lifecycle/Lifecycle$State;

    invoke-virtual {v0, v1}, Landroidx/lifecycle/LifecycleRegistry;->markState(Landroidx/lifecycle/Lifecycle$State;)V

    iget-object v0, p0, Lcom/android/settings/wifi/slice/WifiScanWorker;->mLifecycleRegistry:Landroidx/lifecycle/LifecycleRegistry;

    sget-object v1, Landroidx/lifecycle/Lifecycle$State;->RESUMED:Landroidx/lifecycle/Lifecycle$State;

    invoke-virtual {v0, v1}, Landroidx/lifecycle/LifecycleRegistry;->markState(Landroidx/lifecycle/Lifecycle$State;)V

    invoke-virtual {p0}, Lcom/android/settings/wifi/slice/WifiScanWorker;->updateResults()V

    return-void
.end method

.method protected onSliceUnpinned()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/slice/WifiScanWorker;->mLifecycleRegistry:Landroidx/lifecycle/LifecycleRegistry;

    sget-object v1, Landroidx/lifecycle/Lifecycle$State;->STARTED:Landroidx/lifecycle/Lifecycle$State;

    invoke-virtual {v0, v1}, Landroidx/lifecycle/LifecycleRegistry;->markState(Landroidx/lifecycle/Lifecycle$State;)V

    iget-object p0, p0, Lcom/android/settings/wifi/slice/WifiScanWorker;->mLifecycleRegistry:Landroidx/lifecycle/LifecycleRegistry;

    sget-object v0, Landroidx/lifecycle/Lifecycle$State;->CREATED:Landroidx/lifecycle/Lifecycle$State;

    invoke-virtual {p0, v0}, Landroidx/lifecycle/LifecycleRegistry;->markState(Landroidx/lifecycle/Lifecycle$State;)V

    return-void
.end method

.method public onUpdated()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/wifi/slice/WifiScanWorker;->updateResults()V

    return-void
.end method

.method public onWifiEntriesChanged()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/wifi/slice/WifiScanWorker;->updateResults()V

    return-void
.end method

.method public onWifiStateChanged()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/settings/slices/SliceBackgroundWorker;->notifySliceChange()V

    return-void
.end method

.method public setCarrierNetworkEnabledIfNeeded(ZI)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/slice/WifiScanWorker;->mWifiPickerTrackerHelper:Lcom/android/settings/wifi/WifiPickerTrackerHelper;

    invoke-virtual {v0, p2}, Lcom/android/settings/wifi/WifiPickerTrackerHelper;->isCarrierNetworkProvisionEnabled(I)Z

    move-result p2

    if-nez p2, :cond_0

    iget-object p0, p0, Lcom/android/settings/wifi/slice/WifiScanWorker;->mWifiPickerTrackerHelper:Lcom/android/settings/wifi/WifiPickerTrackerHelper;

    invoke-virtual {p0, p1}, Lcom/android/settings/wifi/WifiPickerTrackerHelper;->setCarrierNetworkEnabled(Z)V

    :cond_0
    return-void
.end method

.method updateResults()V
    .locals 5

    goto/32 :goto_2d

    nop

    :goto_0
    if-nez v1, :cond_0

    goto/32 :goto_18

    :cond_0
    goto/32 :goto_b

    nop

    :goto_1
    invoke-virtual {p0}, Lcom/android/settings/slices/SliceBackgroundWorker;->getContext()Landroid/content/Context;

    move-result-object v3

    goto/32 :goto_1b

    nop

    :goto_2
    invoke-direct {v3, v4, v2}, Lcom/android/settings/wifi/slice/WifiSliceItem;-><init>(Landroid/content/Context;Lcom/android/wifitrackerlib/WifiEntry;)V

    goto/32 :goto_e

    nop

    :goto_3
    invoke-super {p0, v0}, Lcom/android/settings/slices/SliceBackgroundWorker;->updateResults(Ljava/util/List;)V

    goto/32 :goto_2f

    nop

    :goto_4
    invoke-virtual {v1}, Lcom/android/wifitrackerlib/WifiPickerTracker;->getWifiEntries()Ljava/util/List;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_5
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_12

    nop

    :goto_6
    goto/16 :goto_31

    :goto_7
    goto/32 :goto_21

    nop

    :goto_8
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    goto/32 :goto_c

    nop

    :goto_a
    iget-object v1, p0, Lcom/android/settings/wifi/slice/WifiScanWorker;->mWifiPickerTracker:Lcom/android/wifitrackerlib/WifiPickerTracker;

    goto/32 :goto_22

    nop

    :goto_b
    invoke-virtual {v1, p0}, Lcom/android/wifitrackerlib/WifiEntry;->setListener(Lcom/android/wifitrackerlib/WifiEntry$WifiEntryCallback;)V

    goto/32 :goto_13

    nop

    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    goto/32 :goto_23

    nop

    :goto_d
    if-ge v3, v4, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_6

    nop

    :goto_e
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_30

    nop

    :goto_f
    sget-object v1, Landroidx/lifecycle/Lifecycle$State;->RESUMED:Landroidx/lifecycle/Lifecycle$State;

    goto/32 :goto_29

    nop

    :goto_10
    goto :goto_25

    :goto_11
    goto/32 :goto_5

    nop

    :goto_12
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_a

    nop

    :goto_13
    new-instance v2, Lcom/android/settings/wifi/slice/WifiSliceItem;

    goto/32 :goto_1

    nop

    :goto_14
    invoke-virtual {p0}, Lcom/android/settings/slices/SliceBackgroundWorker;->getContext()Landroid/content/Context;

    move-result-object v4

    goto/32 :goto_2

    nop

    :goto_15
    check-cast v2, Lcom/android/wifitrackerlib/WifiEntry;

    goto/32 :goto_1f

    nop

    :goto_16
    iget-object v0, p0, Lcom/android/settings/wifi/slice/WifiScanWorker;->mLifecycleRegistry:Landroidx/lifecycle/LifecycleRegistry;

    goto/32 :goto_1a

    nop

    :goto_17
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_18
    goto/32 :goto_2a

    nop

    :goto_19
    invoke-virtual {v2, p0}, Lcom/android/wifitrackerlib/WifiEntry;->setListener(Lcom/android/wifitrackerlib/WifiEntry$WifiEntryCallback;)V

    goto/32 :goto_20

    nop

    :goto_1a
    invoke-virtual {v0}, Landroidx/lifecycle/LifecycleRegistry;->getCurrentState()Landroidx/lifecycle/Lifecycle$State;

    move-result-object v0

    goto/32 :goto_f

    nop

    :goto_1b
    invoke-direct {v2, v3, v1}, Lcom/android/settings/wifi/slice/WifiSliceItem;-><init>(Landroid/content/Context;Lcom/android/wifitrackerlib/WifiEntry;)V

    goto/32 :goto_17

    nop

    :goto_1c
    const/4 v4, -0x1

    goto/32 :goto_28

    nop

    :goto_1d
    const/4 v1, 0x3

    goto/32 :goto_1e

    nop

    :goto_1e
    if-eq v0, v1, :cond_2

    goto/32 :goto_25

    :cond_2
    goto/32 :goto_16

    nop

    :goto_1f
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    goto/32 :goto_2c

    nop

    :goto_20
    new-instance v3, Lcom/android/settings/wifi/slice/WifiSliceItem;

    goto/32 :goto_14

    nop

    :goto_21
    invoke-virtual {v2}, Lcom/android/wifitrackerlib/WifiEntry;->getLevel()I

    move-result v3

    goto/32 :goto_1c

    nop

    :goto_22
    invoke-virtual {v1}, Lcom/android/wifitrackerlib/WifiPickerTracker;->getConnectedWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_23
    if-nez v2, :cond_3

    goto/32 :goto_31

    :cond_3
    goto/32 :goto_26

    nop

    :goto_24
    return-void

    :goto_25
    goto/32 :goto_27

    nop

    :goto_26
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_15

    nop

    :goto_27
    const/4 v0, 0x0

    goto/32 :goto_3

    nop

    :goto_28
    if-ne v3, v4, :cond_4

    goto/32 :goto_9

    :cond_4
    goto/32 :goto_19

    nop

    :goto_29
    if-ne v0, v1, :cond_5

    goto/32 :goto_11

    :cond_5
    goto/32 :goto_10

    nop

    :goto_2a
    iget-object v1, p0, Lcom/android/settings/wifi/slice/WifiScanWorker;->mWifiPickerTracker:Lcom/android/wifitrackerlib/WifiPickerTracker;

    goto/32 :goto_4

    nop

    :goto_2b
    invoke-virtual {v0}, Lcom/android/wifitrackerlib/BaseWifiTracker;->getWifiState()I

    move-result v0

    goto/32 :goto_1d

    nop

    :goto_2c
    invoke-virtual {p0}, Lcom/android/settings/wifi/slice/WifiScanWorker;->getApRowCount()I

    move-result v4

    goto/32 :goto_d

    nop

    :goto_2d
    iget-object v0, p0, Lcom/android/settings/wifi/slice/WifiScanWorker;->mWifiPickerTracker:Lcom/android/wifitrackerlib/WifiPickerTracker;

    goto/32 :goto_2b

    nop

    :goto_2e
    invoke-super {p0, v0}, Lcom/android/settings/slices/SliceBackgroundWorker;->updateResults(Ljava/util/List;)V

    goto/32 :goto_24

    nop

    :goto_2f
    return-void

    :goto_30
    goto/16 :goto_9

    :goto_31
    goto/32 :goto_2e

    nop
.end method
