.class public Lcom/android/settings/wifi/WifiProvisionSettingsActivity;
.super Lmiuix/provision/ProvisionBaseActivity;


# static fields
.field private static HALF_ALPHA:F = 0.5f

.field private static NO_ALPHA:F = 1.0f


# instance fields
.field private mMiuiWifiSettingsInstance:Lmiuix/preference/PreferenceFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiuix/provision/ProvisionBaseActivity;-><init>()V

    return-void
.end method

.method public static fitNotchForFullScreen(Landroid/app/Activity;)V
    .locals 2

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->isFoldDevice()Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    const/4 v1, 0x1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p0

    invoke-virtual {p0, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public onBackAnimStart()V
    .locals 0

    invoke-super {p0}, Lmiuix/provision/ProvisionBaseActivity;->onBackAnimStart()V

    const-string/jumbo p0, "provision_wifi_page"

    invoke-static {p0}, Lcom/android/settingslib/util/MiStatInterfaceUtils;->trackPageEnd(Ljava/lang/String;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lmiuix/provision/ProvisionBaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/android/settings/wifi/WifiProvisionSettingsActivity;->fitNotchForFullScreen(Landroid/app/Activity;)V

    iget-object p1, p0, Lcom/android/settings/wifi/WifiProvisionSettingsActivity;->mMiuiWifiSettingsInstance:Lmiuix/preference/PreferenceFragment;

    if-nez p1, :cond_0

    new-instance p1, Lcom/android/settings/wifi/MiuiWifiSettings;

    invoke-direct {p1}, Lcom/android/settings/wifi/MiuiWifiSettings;-><init>()V

    iput-object p1, p0, Lcom/android/settings/wifi/WifiProvisionSettingsActivity;->mMiuiWifiSettingsInstance:Lmiuix/preference/PreferenceFragment;

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    sget v0, Lcom/android/settings/R$id;->provision_container:I

    iget-object v1, p0, Lcom/android/settings/wifi/WifiProvisionSettingsActivity;->mMiuiWifiSettingsInstance:Lmiuix/preference/PreferenceFragment;

    invoke-virtual {p1, v0, v1}, Landroidx/fragment/app/FragmentTransaction;->replace(ILandroidx/fragment/app/Fragment;)Landroidx/fragment/app/FragmentTransaction;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/fragment/app/FragmentManager;->executePendingTransactions()Z

    sget p1, Lcom/android/settings/R$drawable;->provision_wifi:I

    invoke-virtual {p0, p1}, Landroid/app/Activity;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p0, p1}, Lmiuix/provision/ProvisionBaseActivity;->setPreviewView(Landroid/graphics/drawable/Drawable;)V

    sget p1, Lcom/android/settings/R$string;->connect_to_internet:I

    invoke-virtual {p0, p1}, Lmiuix/provision/ProvisionBaseActivity;->setTitle(I)V

    return-void
.end method

.method public onNextAminStart()V
    .locals 1

    invoke-super {p0}, Lmiuix/provision/ProvisionBaseActivity;->onNextAminStart()V

    const-string/jumbo v0, "provision_wifi_page"

    invoke-static {v0}, Lcom/android/settingslib/util/MiStatInterfaceUtils;->trackPageEnd(Ljava/lang/String;)V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/settings/wifi/MiuiWifiSettings;->mIsDisableBack:Z

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    return-void
.end method

.method protected onNextButtonClick()V
    .locals 2

    const-string/jumbo p0, "provision_wifi_next"

    invoke-static {p0}, Lcom/android/settingslib/util/MiStatInterfaceUtils;->trackEvent(Ljava/lang/String;)V

    new-instance p0, Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    const-string/jumbo v0, "provision_wifi_state"

    const-string v1, "next"

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0, p0}, Lcom/android/settingslib/util/OneTrackInterfaceUtils;->track(Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method protected onSkipButtonClick()V
    .locals 3

    const-string/jumbo v0, "provision_wifi_page"

    invoke-static {v0}, Lcom/android/settingslib/util/MiStatInterfaceUtils;->trackPageEnd(Ljava/lang/String;)V

    const-string/jumbo v0, "provision_wifi_skip"

    invoke-static {v0}, Lcom/android/settingslib/util/MiStatInterfaceUtils;->trackEvent(Ljava/lang/String;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string/jumbo v1, "provision_wifi_state"

    const-string/jumbo v2, "skip"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1, v0}, Lcom/android/settingslib/util/OneTrackInterfaceUtils;->track(Ljava/lang/String;Ljava/util/Map;)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/AppCompatActivity;->finish()V

    return-void
.end method

.method public updateButtonState(Z)V
    .locals 2

    invoke-static {}, Lmiuix/provision/OobeUtil;->needFastAnimation()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/provision/ProvisionBaseActivity;->mBackBtn:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    sget v1, Lcom/android/settings/wifi/WifiProvisionSettingsActivity;->NO_ALPHA:F

    goto :goto_0

    :cond_0
    sget v1, Lcom/android/settings/wifi/WifiProvisionSettingsActivity;->HALF_ALPHA:F

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    iget-object p0, p0, Lmiuix/provision/ProvisionBaseActivity;->mBackBtn:Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1

    :cond_1
    invoke-super {p0, p1}, Lmiuix/provision/ProvisionBaseActivity;->updateButtonState(Z)V

    :goto_1
    return-void
.end method
