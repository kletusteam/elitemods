.class public Lcom/android/settings/wifi/MiuiWifiEntryPreference;
.super Lcom/android/settingslib/wifi/WifiEntryPreference;

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/wifi/MiuiWifiEntryPreference$ArrowClickListener;,
        Lcom/android/settings/wifi/MiuiWifiEntryPreference$LongClickListener;
    }
.end annotation


# static fields
.field static final BATTERY_LEVEL_CONNECTED:[I

.field private static final VENDOR_SPECIFIC_INFO_IOS:[B

.field static final WIFI_6_PIE:[I

.field private static sSuperComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/android/settings/wifi/MiuiWifiEntryPreference;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mArrowClickListener:Landroid/view/View$OnClickListener;

.field private mBatteryLevel:I

.field private mContext:Landroid/content/Context;

.field private mForSlaveWifi:Z

.field private mHasDetail:Z

.field private mHelper:Lmiuix/preference/ConnectPreferenceHelper;

.field private mIsConnected:Z

.field private mIsFreeWifi:Z

.field private mIsInProvision:Z

.field private mIsMeteredHint:Z

.field private mLongClickListener:Lcom/android/settings/wifi/MiuiWifiEntryPreference$LongClickListener;

.field private mRssiForCompare:I

.field private mShowArrow:Z

.field private mView:Landroid/view/View;


# direct methods
.method static bridge synthetic -$$Nest$fgetmRssiForCompare(Lcom/android/settings/wifi/MiuiWifiEntryPreference;)I
    .locals 0

    iget p0, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mRssiForCompare:I

    return p0
.end method

.method static constructor <clinit>()V
    .locals 9

    const/16 v0, 0x8

    new-array v1, v0, [B

    fill-array-data v1, :array_0

    sput-object v1, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->VENDOR_SPECIFIC_INFO_IOS:[B

    const/4 v1, 0x5

    new-array v2, v1, [I

    sget v3, Lcom/android/settings/R$drawable;->ic_wifi_6_signal_0:I

    const/4 v4, 0x0

    aput v3, v2, v4

    sget v3, Lcom/android/settings/R$drawable;->ic_wifi_6_signal_1:I

    const/4 v5, 0x1

    aput v3, v2, v5

    sget v3, Lcom/android/settings/R$drawable;->ic_wifi_6_signal_2:I

    const/4 v6, 0x2

    aput v3, v2, v6

    sget v3, Lcom/android/settings/R$drawable;->ic_wifi_6_signal_3:I

    const/4 v7, 0x3

    aput v3, v2, v7

    sget v3, Lcom/android/settings/R$drawable;->ic_wifi_6_signal_4:I

    const/4 v8, 0x4

    aput v3, v2, v8

    sput-object v2, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->WIFI_6_PIE:[I

    const/16 v2, 0xa

    new-array v2, v2, [I

    sget v3, Lcom/android/settings/R$drawable;->ap_battery_10_connected:I

    aput v3, v2, v4

    sget v3, Lcom/android/settings/R$drawable;->ap_battery_20_connected:I

    aput v3, v2, v5

    sget v3, Lcom/android/settings/R$drawable;->ap_battery_30_connected:I

    aput v3, v2, v6

    sget v3, Lcom/android/settings/R$drawable;->ap_battery_40_connected:I

    aput v3, v2, v7

    sget v3, Lcom/android/settings/R$drawable;->ap_battery_50_connected:I

    aput v3, v2, v8

    sget v3, Lcom/android/settings/R$drawable;->ap_battery_60_connected:I

    aput v3, v2, v1

    sget v1, Lcom/android/settings/R$drawable;->ap_battery_70_connected:I

    const/4 v3, 0x6

    aput v1, v2, v3

    sget v1, Lcom/android/settings/R$drawable;->ap_battery_80_connected:I

    const/4 v3, 0x7

    aput v1, v2, v3

    sget v1, Lcom/android/settings/R$drawable;->ap_battery_90_connected:I

    aput v1, v2, v0

    sget v0, Lcom/android/settings/R$drawable;->ap_battery_100_connected:I

    const/16 v1, 0x9

    aput v0, v2, v1

    sput-object v2, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->BATTERY_LEVEL_CONNECTED:[I

    new-instance v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference$1;

    invoke-direct {v0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference$1;-><init>()V

    sput-object v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->sSuperComparator:Ljava/util/Comparator;

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        0x17t
        -0xet
        0x6t
        0x1t
        0x1t
        0x3t
        0x1t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/wifitrackerlib/WifiEntry;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settingslib/wifi/WifiEntryPreference;-><init>(Landroid/content/Context;Lcom/android/wifitrackerlib/WifiEntry;)V

    const/4 p2, 0x1

    iput-boolean p2, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mHasDetail:Z

    iput-boolean p2, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mShowArrow:Z

    const/4 p2, 0x0

    iput-boolean p2, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mIsConnected:Z

    const/4 p2, -0x1

    iput p2, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mBatteryLevel:I

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/wifitrackerlib/WifiEntry;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/settingslib/wifi/WifiEntryPreference;-><init>(Landroid/content/Context;Lcom/android/wifitrackerlib/WifiEntry;Z)V

    const/4 p2, 0x1

    iput-boolean p2, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mHasDetail:Z

    iput-boolean p2, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mShowArrow:Z

    const/4 p2, 0x0

    iput-boolean p2, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mIsConnected:Z

    const/4 p2, -0x1

    iput p2, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mBatteryLevel:I

    iput-boolean p3, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mForSlaveWifi:Z

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->init(Landroid/content/Context;)V

    return-void
.end method

.method private deviceIsProvisioned(Landroid/content/Context;)Z
    .locals 1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string p1, "device_provisioned"

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private getBatteryLevel()I
    .locals 1

    iget p0, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mBatteryLevel:I

    const/4 v0, -0x1

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa

    div-int/2addr p0, v0

    if-ne p0, v0, :cond_1

    add-int/lit8 p0, p0, -0x1

    goto :goto_0

    :cond_0
    const/16 p0, 0x9

    :cond_1
    :goto_0
    return p0
.end method

.method public static getSuperComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator<",
            "Lcom/android/settings/wifi/MiuiWifiEntryPreference;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->sSuperComparator:Ljava/util/Comparator;

    return-object v0
.end method

.method private getWifiEntrySummary()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object p0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/wifitrackerlib/WifiEntry;->getSummary(Z)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private init(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->deviceIsProvisioned(Landroid/content/Context;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mIsInProvision:Z

    if-eqz p1, :cond_0

    sget p1, Lcom/android/settings/R$layout;->provision_accesspoint_preference:I

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setLayoutResource(I)V

    goto :goto_0

    :cond_0
    sget p1, Lcom/android/settings/R$layout;->accesspoint_preference:I

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setLayoutResource(I)V

    :goto_0
    sget p1, Lcom/android/settings/R$layout;->preference_widget_wifi_signal:I

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setWidgetLayoutResource(I)V

    invoke-virtual {p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object p1

    invoke-virtual {p1}, Lcom/android/wifitrackerlib/WifiEntry;->getTargetScanResults()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lcom/android/wifitrackerlib/Utils;->getBestScanResultByLevel(Ljava/util/List;)Landroid/net/wifi/ScanResult;

    move-result-object p1

    if-eqz p1, :cond_1

    iget p1, p1, Landroid/net/wifi/ScanResult;->level:I

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    iput p1, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mRssiForCompare:I

    return-void
.end method

.method private isPad()Z
    .locals 1

    sget-boolean p0, Lmiui/os/Build;->IS_TABLET:Z

    if-nez p0, :cond_1

    const/4 p0, 0x2

    const-string v0, "MiuiQuickHotspotTest"

    invoke-static {v0, p0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private isPasswordCanShare()Z
    .locals 2

    invoke-virtual {p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/wifitrackerlib/WifiEntry;->getSecurity()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/wifitrackerlib/WifiEntry;->getSecurity()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/settingslib/wifi/WifiEntryPreference;->mWifiEntry:Lcom/android/wifitrackerlib/WifiEntry;

    invoke-virtual {v0}, Lcom/android/wifitrackerlib/WifiEntry;->getSummary()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/wifi/WifiEntryPreference;->mWifiEntry:Lcom/android/wifitrackerlib/WifiEntry;

    invoke-virtual {v0}, Lcom/android/wifitrackerlib/WifiEntry;->canShare()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/wifi/WifiEntryPreference;->mWifiEntry:Lcom/android/wifitrackerlib/WifiEntry;

    invoke-virtual {v0}, Lcom/android/wifitrackerlib/WifiEntry;->getSummary()Ljava/lang/String;

    move-result-object v0

    iget-object p0, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    sget v1, Lcom/android/settingslib/R$string;->wifitrackerlib_wifi_connected_cannot_provide_internet:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_1

    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private isSmallScreenForFoldDevice(Landroid/content/Context;)Z
    .locals 0

    invoke-static {}, Lcom/android/settings/utils/SettingsFeatures;->isFoldDevice()Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-static {p1}, Lcom/android/settings/utils/SettingsFeatures;->isScreenLayoutLarge(Landroid/content/Context;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private isWifiConnected()Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mIsConnected:Z

    return p0
.end method

.method private startSignalConnectedAnimation()V
    .locals 4

    invoke-virtual {p0}, Landroidx/preference/Preference;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/wifitrackerlib/WifiEntry;->getScanResults()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->isMeteredHint(Ljava/util/Set;)Z

    move-result p0

    if-eqz p0, :cond_2

    instance-of p0, v0, Landroid/graphics/drawable/AnimatedVectorDrawable;

    if-eqz p0, :cond_5

    check-cast v0, Landroid/graphics/drawable/AnimatedVectorDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimatedVectorDrawable;->isRunning()Z

    move-result p0

    if-eqz p0, :cond_1

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimatedVectorDrawable;->stop()V

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimatedVectorDrawable;->reset()V

    :cond_1
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimatedVectorDrawable;->start()V

    goto :goto_1

    :cond_2
    instance-of p0, v0, Landroid/graphics/drawable/LayerDrawable;

    if-eqz p0, :cond_5

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/LayerDrawable;->getNumberOfLayers()I

    move-result p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p0, :cond_5

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->getId(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    instance-of v3, v2, Landroid/graphics/drawable/AnimatedVectorDrawable;

    if-eqz v3, :cond_4

    check-cast v2, Landroid/graphics/drawable/AnimatedVectorDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/AnimatedVectorDrawable;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Landroid/graphics/drawable/AnimatedVectorDrawable;->stop()V

    invoke-virtual {v2}, Landroid/graphics/drawable/AnimatedVectorDrawable;->reset()V

    :cond_3
    invoke-virtual {v2}, Landroid/graphics/drawable/AnimatedVectorDrawable;->start()V

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_5
    :goto_1
    return-void
.end method

.method private updateBatteryLevelInternal(I)V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mView:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->isMeteredHint()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    iput p1, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mBatteryLevel:I

    iget-object p1, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->encryption:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    sget v2, Lcom/android/settings/R$dimen;->ap_battery_image_width:I

    invoke-static {v1, v2}, Lcom/android/settings/MiuiUtils;->getDimenValue(Landroid/content/Context;I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    float-to-int v1, v1

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    sget v3, Lcom/android/settings/R$dimen;->ap_battery_image_high:I

    invoke-static {v2, v3}, Lcom/android/settings/MiuiUtils;->getDimenValue(Landroid/content/Context;I)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    float-to-int v0, v2

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v1, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->BATTERY_LEVEL_CONNECTED:[I

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->getBatteryLevel()I

    move-result v3

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mView:Landroid/view/View;

    const v1, 0x1020010

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$string;->ap_connected_battery_level:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mBatteryLevel:I

    invoke-static {v4}, Lcom/android/settingslib/Utils;->formatPercentage(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object p0, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v1, Lcom/android/settings/R$array;->wifi_status:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object p0

    const/4 v1, 0x5

    aget-object p0, p0, v1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private updateConnectAnimation()V
    .locals 5

    invoke-virtual {p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/wifitrackerlib/WifiEntry;->getNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object v0, v1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v0

    :goto_0
    invoke-virtual {p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/wifitrackerlib/WifiEntry;->getSlaveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v1

    :goto_1
    iget-object v2, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mHelper:Lmiuix/preference/ConnectPreferenceHelper;

    invoke-virtual {v2}, Lmiuix/preference/ConnectPreferenceHelper;->getConnectState()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "updateConnectAnimation* state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v4, ", slaveState: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v4, ", animationHelperState: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ", hashCode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MiuiWifiEntryPreference"

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v3, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-eq v0, v3, :cond_4

    if-ne v1, v3, :cond_2

    goto :goto_2

    :cond_2
    sget-object v3, Landroid/net/NetworkInfo$DetailedState;->AUTHENTICATING:Landroid/net/NetworkInfo$DetailedState;

    if-eq v0, v3, :cond_3

    sget-object v3, Landroid/net/NetworkInfo$DetailedState;->OBTAINING_IPADDR:Landroid/net/NetworkInfo$DetailedState;

    if-eq v0, v3, :cond_3

    sget-object v3, Landroid/net/NetworkInfo$DetailedState;->VERIFYING_POOR_LINK:Landroid/net/NetworkInfo$DetailedState;

    if-eq v0, v3, :cond_3

    sget-object v3, Landroid/net/NetworkInfo$DetailedState;->CAPTIVE_PORTAL_CHECK:Landroid/net/NetworkInfo$DetailedState;

    if-eq v0, v3, :cond_3

    sget-object v3, Landroid/net/NetworkInfo$DetailedState;->CONNECTING:Landroid/net/NetworkInfo$DetailedState;

    if-eq v0, v3, :cond_3

    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->AUTHENTICATING:Landroid/net/NetworkInfo$DetailedState;

    if-eq v1, v0, :cond_3

    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->OBTAINING_IPADDR:Landroid/net/NetworkInfo$DetailedState;

    if-eq v1, v0, :cond_3

    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->VERIFYING_POOR_LINK:Landroid/net/NetworkInfo$DetailedState;

    if-eq v1, v0, :cond_3

    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->CAPTIVE_PORTAL_CHECK:Landroid/net/NetworkInfo$DetailedState;

    if-eq v1, v0, :cond_3

    sget-object v0, Landroid/net/NetworkInfo$DetailedState;->CONNECTING:Landroid/net/NetworkInfo$DetailedState;

    if-ne v1, v0, :cond_5

    :cond_3
    const/4 v0, 0x2

    if-eq v2, v0, :cond_5

    iget-object p0, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mHelper:Lmiuix/preference/ConnectPreferenceHelper;

    invoke-virtual {p0, v0}, Lmiuix/preference/ConnectPreferenceHelper;->setConnectState(I)V

    goto :goto_3

    :cond_4
    :goto_2
    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->isWifiConnected()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    if-eq v2, v0, :cond_5

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mHelper:Lmiuix/preference/ConnectPreferenceHelper;

    invoke-virtual {v1, v0}, Lmiuix/preference/ConnectPreferenceHelper;->setConnectState(I)V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->startSignalConnectedAnimation()V

    :cond_5
    :goto_3
    return-void
.end method

.method private updateSignalLevel()V
    .locals 5

    invoke-virtual {p0}, Landroidx/preference/Preference;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object p0

    invoke-virtual {p0}, Lcom/android/wifitrackerlib/WifiEntry;->getLevel()I

    move-result p0

    const/4 v1, -0x1

    if-ne p0, v1, :cond_0

    return-void

    :cond_0
    instance-of v1, v0, Landroid/graphics/drawable/LayerDrawable;

    if-eqz v1, :cond_2

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/LayerDrawable;->getNumberOfLayers()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_0
    if-ltz v2, :cond_1

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/LayerDrawable;->getId(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/16 v4, 0xff

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_1
    :goto_1
    if-lt v1, p0, :cond_2

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->getId(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/16 v3, 0x3f

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_2
    return-void
.end method


# virtual methods
.method public getPrimaryWifiTitleForSlave()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/android/settingslib/wifi/WifiEntryPreference;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getFrequency()I

    move-result v3

    invoke-static {v3}, Lcom/android/settingslib/wifi/WifiUtils;->is24GHz(I)Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v1

    goto :goto_0

    :cond_0
    move v3, v2

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getFrequency()I

    move-result v0

    invoke-static {v0}, Lcom/android/settingslib/wifi/WifiUtils;->is5GHz(I)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move v1, v2

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/wifitrackerlib/WifiEntry;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v3, :cond_2

    iget-object p0, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    sget v1, Lcom/android/settings/R$string;->band_24G:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_3

    iget-object p0, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    sget v1, Lcom/android/settings/R$string;->band_5G:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    :goto_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public isMeteredHint()Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mIsMeteredHint:Z

    return p0
.end method

.method protected isMeteredHint(Ljava/util/Set;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Landroid/net/wifi/ScanResult;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/ScanResult;

    invoke-virtual {v1}, Landroid/net/wifi/ScanResult;->getInformationElements()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/net/wifi/ScanResult$InformationElement;

    if-eqz v1, :cond_0

    move v2, v0

    :goto_0
    array-length v3, v1

    if-ge v2, v3, :cond_0

    aget-object v3, v1, v2

    invoke-virtual {v3}, Landroid/net/wifi/ScanResult$InformationElement;->getId()I

    move-result v3

    const/16 v4, 0xdd

    if-ne v3, v4, :cond_2

    sget-object v3, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->VENDOR_SPECIFIC_INFO_IOS:[B

    array-length v4, v3

    new-array v4, v4, [B

    :try_start_0
    array-length v5, v3

    aget-object v6, v1, v2

    invoke-virtual {v6}, Landroid/net/wifi/ScanResult$InformationElement;->getBytes()Ljava/nio/ByteBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v6

    if-le v5, v6, :cond_1

    goto :goto_1

    :cond_1
    aget-object v5, v1, v2

    invoke-virtual {v5}, Landroid/net/wifi/ScanResult$InformationElement;->getBytes()Ljava/nio/ByteBuffer;

    move-result-object v5

    array-length v6, v3

    invoke-virtual {v5, v4, v0, v6}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    invoke-static {v4, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mIsMeteredHint:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v3

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    iput-boolean v0, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mIsMeteredHint:Z

    return v0
.end method

.method public isXiaomiRouter()Z
    .locals 0

    invoke-virtual {p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object p0

    invoke-virtual {p0}, Lcom/android/wifitrackerlib/WifiEntry;->getScanResults()Ljava/util/Set;

    move-result-object p0

    invoke-static {p0}, Lcom/android/settings/wifi/XiaomiRouterUtils;->isXiaomiRouter(Ljava/util/Set;)Z

    move-result p0

    return p0
.end method

.method public onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-super/range {p0 .. p1}, Lcom/android/settingslib/wifi/WifiEntryPreference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object v2

    iput-object v2, v0, Lcom/android/settingslib/wifi/WifiEntryPreference;->mWifiEntry:Lcom/android/wifitrackerlib/WifiEntry;

    iget-object v2, v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    iput-object v2, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mView:Landroid/view/View;

    iget-boolean v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mIsInProvision:Z

    if-eqz v3, :cond_0

    sget v3, Lcom/android/settings/R$drawable;->provision_list_item_background:I

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_0
    iget-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mHelper:Lmiuix/preference/ConnectPreferenceHelper;

    const/4 v4, 0x0

    if-nez v3, :cond_1

    new-instance v3, Lmiuix/preference/ConnectPreferenceHelper;

    invoke-virtual/range {p0 .. p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v3, v5, v0}, Lmiuix/preference/ConnectPreferenceHelper;-><init>(Landroid/content/Context;Landroidx/preference/Preference;)V

    iput-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mHelper:Lmiuix/preference/ConnectPreferenceHelper;

    invoke-virtual {v3, v4}, Lmiuix/preference/ConnectPreferenceHelper;->initConnectState(I)V

    :cond_1
    iget-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mHelper:Lmiuix/preference/ConnectPreferenceHelper;

    sget v5, Lcom/android/settings/R$id;->l_highlight:I

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v3, v1, v6}, Lmiuix/preference/ConnectPreferenceHelper;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;Landroid/view/View;)V

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v6, -0x2

    const/4 v7, -0x1

    invoke-direct {v3, v7, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v6, v0, Lcom/android/settingslib/wifi/WifiEntryPreference;->mWifiEntry:Lcom/android/wifitrackerlib/WifiEntry;

    invoke-virtual {v6}, Lcom/android/wifitrackerlib/WifiEntry;->getWifiInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v6

    iget-boolean v8, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mIsInProvision:Z

    const/4 v9, 0x0

    if-nez v8, :cond_4

    invoke-direct/range {p0 .. p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->isWifiConnected()Z

    move-result v8

    if-nez v8, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/wifitrackerlib/WifiEntry;->isNetworkRequest()Z

    move-result v8

    if-nez v8, :cond_3

    if-eqz v6, :cond_2

    invoke-virtual {v6}, Landroid/net/wifi/WifiInfo;->isPasspointAp()Z

    move-result v8

    if-eqz v8, :cond_2

    goto :goto_0

    :cond_2
    iget-object v8, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mHelper:Lmiuix/preference/ConnectPreferenceHelper;

    invoke-virtual {v8, v4}, Lmiuix/preference/ConnectPreferenceHelper;->setConnectState(I)V

    iget-object v8, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v10, Lcom/android/settings/R$dimen;->highlight_side_left_margin:I

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v8

    iget-object v10, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/android/settings/R$dimen;->highlight_side_right_margin:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v10

    invoke-virtual {v3, v8, v4, v10, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    sget v8, Lcom/android/settings/R$id;->cardview:I

    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    sget v3, Lcom/android/settings/R$drawable;->list_item_background:I

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_2

    :cond_3
    :goto_0
    iget-object v5, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v8, Lcom/android/settings/R$dimen;->highlight_side_left_margin:I

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    iget-object v8, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v10, Lcom/android/settings/R$dimen;->highlight_top_margin:I

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v8

    iget-object v10, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/android/settings/R$dimen;->highlight_side_right_margin:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v10

    invoke-virtual {v3, v5, v8, v10, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    sget v5, Lcom/android/settings/R$id;->cardview:I

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct/range {p0 .. p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->updateConnectAnimation()V

    goto :goto_2

    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/wifitrackerlib/WifiEntry;->isSaved()Z

    move-result v3

    if-nez v3, :cond_6

    if-eqz v6, :cond_5

    invoke-virtual {v6}, Landroid/net/wifi/WifiInfo;->isPasspointAp()Z

    move-result v3

    if-eqz v3, :cond_5

    goto :goto_1

    :cond_5
    iget-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mHelper:Lmiuix/preference/ConnectPreferenceHelper;

    invoke-virtual {v3, v4}, Lmiuix/preference/ConnectPreferenceHelper;->setConnectState(I)V

    goto :goto_2

    :cond_6
    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->updateConnectAnimation()V

    :goto_2
    sget v3, Lcom/android/settings/R$id;->preference_detail:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v8, Lcom/android/settings/R$string;->network_detail:I

    const/4 v10, 0x1

    new-array v11, v10, [Ljava/lang/Object;

    iget-object v12, v0, Lcom/android/settingslib/wifi/WifiEntryPreference;->mWifiEntry:Lcom/android/wifitrackerlib/WifiEntry;

    invoke-virtual {v12}, Lcom/android/wifitrackerlib/WifiEntry;->getTitle()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v4

    invoke-virtual {v5, v8, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-boolean v5, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mHasDetail:Z

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-boolean v5, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mHasDetail:Z

    if-eqz v5, :cond_7

    iget-object v5, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mArrowClickListener:Landroid/view/View$OnClickListener;

    goto :goto_3

    :cond_7
    move-object v5, v9

    :goto_3
    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-boolean v5, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mShowArrow:Z

    const/16 v8, 0x8

    if-eqz v5, :cond_8

    move v5, v4

    goto :goto_4

    :cond_8
    move v5, v8

    :goto_4
    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->isSlaveConnected()Z

    move-result v5

    if-eqz v5, :cond_9

    iget-boolean v5, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mForSlaveWifi:Z

    if-eqz v5, :cond_a

    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_b

    iget-boolean v5, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mForSlaveWifi:Z

    if-eqz v5, :cond_b

    :cond_a
    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_b
    const v3, 0x1020016

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckedTextView;

    iget-boolean v5, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mIsFreeWifi:Z

    if-eqz v5, :cond_c

    sget v5, Lcom/android/settings/R$drawable;->free_wifi_indicator:I

    goto :goto_5

    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->isXiaomiRouter()Z

    move-result v5

    if-eqz v5, :cond_d

    invoke-virtual/range {p0 .. p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/wifitrackerlib/WifiEntry;->getScanResults()Ljava/util/Set;

    move-result-object v5

    invoke-static {v5}, Lcom/android/settings/wifi/XiaomiRouterUtils;->getIndictorDrawableId(Ljava/util/Set;)I

    move-result v5

    invoke-direct/range {p0 .. p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->isWifiConnected()Z

    move-result v11

    if-eqz v11, :cond_e

    sget v5, Lcom/android/settings/R$drawable;->xiaomi_wifi_indicator_connected:I

    goto :goto_5

    :cond_d
    move v5, v4

    :cond_e
    :goto_5
    invoke-virtual {v3}, Landroid/widget/CheckedTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    sget v12, Lcom/android/settings/R$dimen;->wifi_title_compound_padding:I

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v11

    invoke-virtual {v3, v11}, Landroid/widget/CheckedTextView;->setCompoundDrawablePadding(I)V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v11

    if-ne v11, v10, :cond_f

    move v11, v10

    goto :goto_6

    :cond_f
    move v11, v4

    :goto_6
    if-nez v11, :cond_10

    invoke-virtual {v3, v4, v4, v5, v4}, Landroid/widget/CheckedTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_7

    :cond_10
    invoke-virtual {v3, v5, v4, v4, v4}, Landroid/widget/CheckedTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :goto_7
    sget v12, Lcom/android/settings/R$id;->wifi_band:I

    invoke-virtual {v2, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    if-nez v5, :cond_11

    invoke-virtual {v12, v4, v4, v4, v4}, Landroid/widget/ImageView;->setPadding(IIII)V

    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object v13

    invoke-virtual {v13}, Lcom/android/wifitrackerlib/WifiEntry;->getScanResults()Ljava/util/Set;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    move v14, v4

    move v15, v14

    :cond_12
    :goto_8
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_14

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/net/wifi/ScanResult;

    invoke-static/range {v16 .. v16}, Lcom/android/settingslib/wifi/WifiUtils;->is24GHz(Landroid/net/wifi/ScanResult;)Z

    move-result v17

    if-eqz v17, :cond_13

    move v15, v10

    goto :goto_8

    :cond_13
    invoke-static/range {v16 .. v16}, Lcom/android/settingslib/wifi/WifiUtils;->is5GHz(Landroid/net/wifi/ScanResult;)Z

    move-result v16

    if-eqz v16, :cond_12

    move v14, v10

    goto :goto_8

    :cond_14
    if-eqz v6, :cond_16

    invoke-virtual {v6}, Landroid/net/wifi/WifiInfo;->is24GHz()Z

    move-result v13

    if-eqz v13, :cond_15

    move v15, v10

    goto :goto_9

    :cond_15
    invoke-virtual {v6}, Landroid/net/wifi/WifiInfo;->is5GHz()Z

    move-result v6

    if-eqz v6, :cond_16

    move v14, v10

    :cond_16
    :goto_9
    iget-object v6, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    iget-object v13, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    sget v7, Lcom/android/settings/R$drawable;->band_wifi_5g:I

    invoke-virtual {v13, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v12, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    if-eqz v15, :cond_18

    if-eqz v14, :cond_18

    iget-object v13, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    sget v9, Lcom/android/settings/R$drawable;->band_wifi_24g:I

    invoke-virtual {v13, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    if-nez v5, :cond_1a

    invoke-virtual {v12, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v3}, Landroid/widget/CheckedTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    sget v10, Lcom/android/settings/R$dimen;->wifi_title_compound_padding:I

    invoke-virtual {v13, v10}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v10

    invoke-virtual {v3, v10}, Landroid/widget/CheckedTextView;->setCompoundDrawablePadding(I)V

    if-nez v11, :cond_17

    invoke-virtual {v3, v4, v4, v9, v4}, Landroid/widget/CheckedTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_a

    :cond_17
    invoke-virtual {v3, v9, v4, v4, v4}, Landroid/widget/CheckedTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_a

    :cond_18
    if-eqz v14, :cond_19

    invoke-virtual {v12, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_a

    :cond_19
    if-nez v14, :cond_1a

    invoke-virtual {v12, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_1a
    :goto_a
    const/high16 v9, 0x40a00000    # 5.0f

    const/high16 v10, 0x3f000000    # 0.5f

    if-eqz v15, :cond_1f

    if-eqz v14, :cond_1f

    if-eqz v5, :cond_23

    iget-object v7, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v13, Lcom/android/settings/R$drawable;->band_wifi_24g:I

    invoke-virtual {v7, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    iget-object v13, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    invoke-direct {v0, v13}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->isSmallScreenForFoldDevice(Landroid/content/Context;)Z

    move-result v13

    if-nez v13, :cond_1b

    goto :goto_b

    :cond_1b
    const/high16 v9, 0x40000000    # 2.0f

    :goto_b
    mul-float/2addr v6, v9

    add-float/2addr v6, v10

    float-to-int v6, v6

    add-int/2addr v7, v6

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v6

    const/4 v9, 0x1

    if-ne v6, v9, :cond_1c

    const/4 v6, 0x1

    goto :goto_c

    :cond_1c
    move v6, v4

    :goto_c
    if-eqz v6, :cond_1d

    move v9, v7

    goto :goto_d

    :cond_1d
    move v9, v4

    :goto_d
    if-eqz v6, :cond_1e

    move v7, v4

    :cond_1e
    invoke-virtual {v3, v9, v4, v7, v4}, Landroid/widget/CheckedTextView;->setPadding(IIII)V

    goto :goto_10

    :cond_1f
    if-eqz v14, :cond_23

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    mul-float/2addr v6, v9

    add-float/2addr v6, v10

    float-to-int v6, v6

    add-int/2addr v7, v6

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v6

    const/4 v9, 0x1

    if-ne v6, v9, :cond_20

    const/4 v6, 0x1

    goto :goto_e

    :cond_20
    move v6, v4

    :goto_e
    if-eqz v6, :cond_21

    move v9, v7

    goto :goto_f

    :cond_21
    move v9, v4

    :goto_f
    if-eqz v6, :cond_22

    move v7, v4

    :cond_22
    invoke-virtual {v3, v9, v4, v7, v4}, Landroid/widget/CheckedTextView;->setPadding(IIII)V

    :cond_23
    :goto_10
    iget-boolean v6, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mIsInProvision:Z

    if-eqz v6, :cond_24

    invoke-virtual/range {p0 .. p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/android/settings/R$dimen;->provision_list_left_padding:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v6

    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result v7

    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v9

    invoke-virtual {v2}, Landroid/view/View;->getPaddingBottom()I

    move-result v10

    invoke-virtual {v2, v6, v7, v9, v10}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_11

    :cond_24
    invoke-virtual/range {p0 .. p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/android/settings/R$dimen;->miuix_preference_item_icon_margin_end:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v6

    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result v7

    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v9

    invoke-virtual {v2}, Landroid/view/View;->getPaddingBottom()I

    move-result v10

    invoke-virtual {v2, v6, v7, v9, v10}, Landroid/view/View;->setPadding(IIII)V

    :goto_11
    const v6, 0x1020006

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    invoke-virtual/range {p0 .. p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/wifitrackerlib/WifiEntry;->getLevel()I

    move-result v7

    if-lez v7, :cond_25

    sget-object v9, Lcom/android/settingslib/wifi/WifiEntryPreference;->WIFI_CONNECTION_STRENGTH:[I

    array-length v10, v9

    if-gt v7, v10, :cond_25

    invoke-virtual/range {p0 .. p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v10

    const/4 v13, 0x1

    sub-int/2addr v7, v13

    aget v7, v9, v7

    invoke-virtual {v10, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_25
    sget v6, Lcom/android/settings/R$id;->encryption:I

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    invoke-virtual/range {p0 .. p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/wifitrackerlib/WifiEntry;->getSecurity()I

    move-result v7

    const/4 v9, 0x4

    if-eqz v7, :cond_26

    move v7, v4

    goto :goto_12

    :cond_26
    move v7, v9

    :goto_12
    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->isSlaveConnected()Z

    move-result v7

    if-eqz v7, :cond_27

    iget-boolean v7, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mForSlaveWifi:Z

    if-eqz v7, :cond_28

    :cond_27
    invoke-virtual/range {p0 .. p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->isConnected()Z

    move-result v7

    if-eqz v7, :cond_29

    iget-boolean v7, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mForSlaveWifi:Z

    if-eqz v7, :cond_29

    :cond_28
    invoke-virtual {v6, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_29
    invoke-direct/range {p0 .. p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->isWifiConnected()Z

    move-result v7

    if-eqz v7, :cond_2d

    if-eqz v15, :cond_2b

    if-eqz v14, :cond_2b

    iget-object v7, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v9, Lcom/android/settings/R$drawable;->band_wifi_24g_connected:I

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v12, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    if-nez v5, :cond_2c

    invoke-virtual {v12, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v3}, Landroid/widget/CheckedTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v7, Lcom/android/settings/R$dimen;->wifi_title_compound_padding:I

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/CheckedTextView;->setCompoundDrawablePadding(I)V

    if-nez v11, :cond_2a

    invoke-virtual {v3, v4, v4, v9, v4}, Landroid/widget/CheckedTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_13

    :cond_2a
    invoke-virtual {v3, v9, v4, v4, v4}, Landroid/widget/CheckedTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_13

    :cond_2b
    if-eqz v14, :cond_2c

    iget-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v5, Lcom/android/settings/R$drawable;->band_wifi_5g_connected:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v12, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_2c
    :goto_13
    iget-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    sget v5, Lcom/android/settings/R$drawable;->ic_wifi_encryption_connected:I

    invoke-virtual {v3, v5}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_14

    :cond_2d
    iget-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    sget v5, Lcom/android/settings/R$drawable;->ic_wifi_encryption:I

    invoke-virtual {v3, v5}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_14
    iget-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    const-string v5, "connectivity"

    invoke-virtual {v3, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/ConnectivityManager;

    const v5, 0x1020010

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-direct/range {p0 .. p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->getWifiEntrySummary()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Lcom/android/wifitrackerlib/BaseWifiTracker;->isVerboseLoggingEnabled()Z

    move-result v6

    invoke-virtual/range {p0 .. p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/wifitrackerlib/WifiEntry;->getConnectedState()I

    move-result v7

    const/16 v9, 0x10

    const/16 v10, 0x11

    const/4 v11, 0x2

    if-ne v7, v11, :cond_38

    iget-boolean v7, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mIsInProvision:Z

    if-nez v7, :cond_38

    iget-boolean v7, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mForSlaveWifi:Z

    if-nez v7, :cond_38

    iget-object v7, v0, Lcom/android/settingslib/wifi/WifiEntryPreference;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v7}, Landroid/net/wifi/WifiManager;->getCurrentNetwork()Landroid/net/Network;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/net/ConnectivityManager;->getNetworkCapabilities(Landroid/net/Network;)Landroid/net/NetworkCapabilities;

    move-result-object v3

    invoke-direct/range {p0 .. p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->isPasswordCanShare()Z

    move-result v7

    if-eqz v7, :cond_36

    if-eqz v3, :cond_2f

    invoke-virtual {v3, v10}, Landroid/net/NetworkCapabilities;->hasCapability(I)Z

    move-result v7

    if-eqz v7, :cond_2f

    if-eqz v6, :cond_2e

    invoke-direct/range {p0 .. p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->getWifiEntrySummary()Ljava/lang/String;

    move-result-object v3

    goto :goto_15

    :cond_2e
    iget-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    sget v7, Lcom/android/settings/R$string;->wifi_click_login_wlan:I

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_15
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_21

    :cond_2f
    if-eqz v3, :cond_31

    const/16 v7, 0x18

    invoke-virtual {v3, v7}, Landroid/net/NetworkCapabilities;->hasCapability(I)Z

    move-result v7

    if-eqz v7, :cond_31

    if-eqz v6, :cond_30

    invoke-direct/range {p0 .. p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->getWifiEntrySummary()Ljava/lang/String;

    move-result-object v3

    goto :goto_16

    :cond_30
    iget-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v7, Lcom/android/settings/R$array;->wifitrackerlib_wifi_status:I

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    sget-object v7, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v7}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    move-result v7

    aget-object v3, v3, v7

    :goto_16
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_21

    :cond_31
    if-eqz v3, :cond_33

    invoke-virtual {v3, v9}, Landroid/net/NetworkCapabilities;->hasCapability(I)Z

    move-result v3

    if-nez v3, :cond_33

    if-eqz v6, :cond_32

    invoke-direct/range {p0 .. p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->getWifiEntrySummary()Ljava/lang/String;

    move-result-object v3

    goto :goto_17

    :cond_32
    iget-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    sget v7, Lcom/android/settingslib/R$string;->wifitrackerlib_wifi_connected_cannot_provide_internet:I

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_17
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_21

    :cond_33
    if-nez v6, :cond_35

    iget-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/settingslib/wifi/WifiUtils;->isInMishow(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_35

    invoke-virtual/range {p0 .. p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/wifitrackerlib/WifiEntry;->isNetworkRequest()Z

    move-result v3

    if-eqz v3, :cond_34

    goto :goto_18

    :cond_34
    iget-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    sget v7, Lcom/android/settings/R$string;->wifi_click_share_wlan:I

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_19

    :cond_35
    :goto_18
    invoke-direct/range {p0 .. p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->getWifiEntrySummary()Ljava/lang/String;

    move-result-object v3

    :goto_19
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_21

    :cond_36
    if-eqz v3, :cond_43

    invoke-virtual {v3, v10}, Landroid/net/NetworkCapabilities;->hasCapability(I)Z

    move-result v3

    if-eqz v3, :cond_43

    if-eqz v6, :cond_37

    invoke-direct/range {p0 .. p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->getWifiEntrySummary()Ljava/lang/String;

    move-result-object v3

    goto :goto_1a

    :cond_37
    iget-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    sget v7, Lcom/android/settings/R$string;->wifi_click_login_wlan:I

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_1a
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_21

    :cond_38
    iget-object v7, v0, Lcom/android/settingslib/wifi/WifiEntryPreference;->mWifiEntry:Lcom/android/wifitrackerlib/WifiEntry;

    invoke-virtual {v7}, Lcom/android/wifitrackerlib/WifiEntry;->getSlaveConnectedState()I

    move-result v7

    if-ne v7, v11, :cond_43

    iget-boolean v7, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mIsInProvision:Z

    if-nez v7, :cond_43

    iget-boolean v7, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mForSlaveWifi:Z

    if-nez v7, :cond_3a

    if-eqz v6, :cond_39

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    sget v9, Lcom/android/settings/R$string;->dual_wifi_acceleration:I

    invoke-virtual {v7, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ", "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct/range {p0 .. p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->getWifiEntrySummary()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1b

    :cond_39
    iget-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    sget v7, Lcom/android/settings/R$string;->dual_wifi_acceleration:I

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_1b
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_21

    :cond_3a
    iget-object v7, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/android/settingslib/wifi/SlaveWifiUtils;->getInstance(Landroid/content/Context;)Lcom/android/settingslib/wifi/SlaveWifiUtils;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/settingslib/wifi/SlaveWifiUtils;->getSlaveWifiCurrentNetwork()Landroid/net/Network;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/net/ConnectivityManager;->getNetworkCapabilities(Landroid/net/Network;)Landroid/net/NetworkCapabilities;

    move-result-object v3

    invoke-direct/range {p0 .. p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->isPasswordCanShare()Z

    move-result v7

    if-eqz v7, :cond_41

    if-eqz v3, :cond_3c

    invoke-virtual {v3, v10}, Landroid/net/NetworkCapabilities;->hasCapability(I)Z

    move-result v7

    if-eqz v7, :cond_3c

    if-eqz v6, :cond_3b

    invoke-direct/range {p0 .. p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->getWifiEntrySummary()Ljava/lang/String;

    move-result-object v3

    goto :goto_1c

    :cond_3b
    iget-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    sget v7, Lcom/android/settings/R$string;->wifi_click_login_wlan:I

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_1c
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_21

    :cond_3c
    if-eqz v3, :cond_3e

    invoke-virtual {v3, v9}, Landroid/net/NetworkCapabilities;->hasCapability(I)Z

    move-result v3

    if-nez v3, :cond_3e

    if-eqz v6, :cond_3d

    invoke-direct/range {p0 .. p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->getWifiEntrySummary()Ljava/lang/String;

    move-result-object v3

    goto :goto_1d

    :cond_3d
    iget-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    sget v7, Lcom/android/settingslib/R$string;->wifitrackerlib_wifi_connected_cannot_provide_internet:I

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_1d
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_21

    :cond_3e
    if-nez v6, :cond_40

    iget-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/settingslib/wifi/WifiUtils;->isInMishow(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3f

    goto :goto_1e

    :cond_3f
    iget-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    sget v7, Lcom/android/settings/R$string;->wifi_click_share_wlan:I

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1f

    :cond_40
    :goto_1e
    invoke-direct/range {p0 .. p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->getWifiEntrySummary()Ljava/lang/String;

    move-result-object v3

    :goto_1f
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_21

    :cond_41
    if-eqz v3, :cond_43

    invoke-virtual {v3, v10}, Landroid/net/NetworkCapabilities;->hasCapability(I)Z

    move-result v3

    if-eqz v3, :cond_43

    if-eqz v6, :cond_42

    invoke-direct/range {p0 .. p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->getWifiEntrySummary()Ljava/lang/String;

    move-result-object v3

    goto :goto_20

    :cond_42
    iget-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    sget v7, Lcom/android/settings/R$string;->wifi_click_login_wlan:I

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_20
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_43
    :goto_21
    invoke-direct/range {p0 .. p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->isWifiConnected()Z

    move-result v3

    if-eqz v3, :cond_49

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v6, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    sget v7, Lcom/android/settingslib/R$string;->wifitrackerlib_wifi_connected_cannot_provide_internet:I

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_48

    invoke-virtual/range {p0 .. p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/wifitrackerlib/WifiEntry;->getConnectedState()I

    move-result v3

    const-wide/16 v6, 0x1b58

    if-ne v3, v11, :cond_44

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iget-object v3, v0, Lcom/android/settingslib/wifi/WifiEntryPreference;->mWifiEntry:Lcom/android/wifitrackerlib/WifiEntry;

    invoke-virtual {v3}, Lcom/android/wifitrackerlib/WifiEntry;->getConnectedTimeStamp()J

    move-result-wide v12

    sub-long/2addr v8, v12

    cmp-long v3, v8, v6

    if-ltz v3, :cond_45

    :cond_44
    invoke-virtual/range {p0 .. p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/wifitrackerlib/WifiEntry;->getSlaveConnectedState()I

    move-result v3

    if-ne v3, v11, :cond_48

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iget-object v3, v0, Lcom/android/settingslib/wifi/WifiEntryPreference;->mWifiEntry:Lcom/android/wifitrackerlib/WifiEntry;

    invoke-virtual {v3}, Lcom/android/wifitrackerlib/WifiEntry;->getSlaveConnectedTimeStamp()J

    move-result-wide v12

    sub-long/2addr v8, v12

    cmp-long v3, v8, v6

    if-gez v3, :cond_48

    :cond_45
    iget-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/settingslib/wifi/WifiUtils;->isInMishow(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_47

    iget-object v3, v0, Lcom/android/settingslib/wifi/WifiEntryPreference;->mWifiEntry:Lcom/android/wifitrackerlib/WifiEntry;

    invoke-virtual {v3}, Lcom/android/wifitrackerlib/WifiEntry;->getSecurity()I

    move-result v3

    if-eq v3, v11, :cond_46

    iget-object v3, v0, Lcom/android/settingslib/wifi/WifiEntryPreference;->mWifiEntry:Lcom/android/wifitrackerlib/WifiEntry;

    invoke-virtual {v3}, Lcom/android/wifitrackerlib/WifiEntry;->getSecurity()I

    move-result v3

    const/4 v6, 0x5

    if-ne v3, v6, :cond_47

    :cond_46
    iget-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    sget v6, Lcom/android/settings/R$string;->wifi_click_share_wlan:I

    invoke-virtual {v3, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_22

    :cond_47
    iget-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    sget v6, Lcom/android/settings/R$string;->wireless_connected:I

    invoke-virtual {v3, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_48
    :goto_22
    invoke-direct/range {p0 .. p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->getWifiEntrySummary()Ljava/lang/String;

    move-result-object v3

    iget-object v6, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    sget v7, Lcom/android/settingslib/R$string;->wifi_remembered:I

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4a

    iget-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/provider/MiuiSettings$System;->getDisableWifiAutoConnectSsid(Landroid/content/Context;)Ljava/util/HashSet;

    move-result-object v3

    if-eqz v3, :cond_4a

    invoke-virtual/range {p0 .. p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/wifitrackerlib/WifiEntry;->getWifiConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v6

    if-eqz v6, :cond_4a

    invoke-virtual/range {p0 .. p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/wifitrackerlib/WifiEntry;->getWifiConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v6

    iget-object v6, v6, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-interface {v3, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4a

    iget-object v3, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mContext:Landroid/content/Context;

    sget v6, Lcom/android/settings/R$string;->wifi_remembered_disabled_auto_connect:I

    invoke-virtual {v3, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_23

    :cond_49
    if-nez v6, :cond_4a

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_4a
    :goto_23
    invoke-virtual/range {p0 .. p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/wifitrackerlib/WifiEntry;->isNetworkRequest()Z

    move-result v3

    if-eqz v3, :cond_4b

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_4b
    invoke-direct/range {p0 .. p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->isPad()Z

    move-result v2

    if-eqz v2, :cond_4c

    iget v2, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mBatteryLevel:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_4c

    invoke-virtual/range {p0 .. p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_4c

    iget v2, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mBatteryLevel:I

    invoke-direct {v0, v2}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->updateBatteryLevelInternal(I)V

    :cond_4c
    iget-object v2, v0, Lcom/android/settingslib/wifi/WifiEntryPreference;->mWifiEntry:Lcom/android/wifitrackerlib/WifiEntry;

    invoke-virtual {v2}, Lcom/android/wifitrackerlib/WifiEntry;->getSlaveConnectedState()I

    move-result v2

    if-ne v2, v11, :cond_4d

    iget-boolean v2, v0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mIsInProvision:Z

    if-nez v2, :cond_4d

    iget-object v2, v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->setLongClickable(Z)V

    iget-object v1, v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto :goto_24

    :cond_4d
    iget-object v0, v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setLongClickable(Z)V

    :goto_24
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 0

    iget-object p1, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mLongClickListener:Lcom/android/settings/wifi/MiuiWifiEntryPreference$LongClickListener;

    if-eqz p1, :cond_0

    invoke-interface {p1, p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference$LongClickListener;->onPreferenceLongClick(Lcom/android/settings/wifi/MiuiWifiEntryPreference;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public setArrowClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mArrowClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public setLongClickListener(Lcom/android/settings/wifi/MiuiWifiEntryPreference$LongClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mLongClickListener:Lcom/android/settings/wifi/MiuiWifiEntryPreference$LongClickListener;

    return-void
.end method

.method public setWifiConnected(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mIsConnected:Z

    return-void
.end method

.method shouldEnabled()Z
    .locals 1

    goto/32 :goto_b

    nop

    :goto_0
    if-nez p0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop

    :goto_1
    invoke-virtual {p0}, Lcom/android/wifitrackerlib/WifiEntry;->canConnect()Z

    move-result v0

    goto/32 :goto_a

    nop

    :goto_2
    invoke-virtual {p0}, Lcom/android/wifitrackerlib/WifiEntry;->canDisconnect()Z

    move-result p0

    goto/32 :goto_0

    nop

    :goto_3
    const/4 v0, 0x1

    :goto_4
    goto/32 :goto_8

    nop

    :goto_5
    return p0

    :goto_6
    goto/32 :goto_1

    nop

    :goto_7
    const/4 p0, 0x0

    goto/32 :goto_5

    nop

    :goto_8
    return v0

    :goto_9
    if-eqz p0, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_7

    nop

    :goto_a
    if-eqz v0, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_2

    nop

    :goto_b
    invoke-virtual {p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object p0

    goto/32 :goto_9

    nop
.end method

.method public update(Lcom/android/wifitrackerlib/WifiEntry;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settingslib/wifi/WifiEntryPreference;->mWifiEntry:Lcom/android/wifitrackerlib/WifiEntry;

    invoke-virtual {p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->refresh()V

    invoke-virtual {p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->shouldEnabled()Z

    move-result p1

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setEnabled(Z)V

    return-void
.end method

.method public updateBatteryLevel(I)V
    .locals 1

    iget v0, p0, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->mBatteryLevel:I

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->updateBatteryLevelInternal(I)V

    return-void
.end method

.method protected updateIcon(ZII)V
    .locals 0

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object p1

    sget p2, Lcom/android/settings/R$drawable;->wifi_signal:I

    invoke-virtual {p1, p2}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p0}, Lcom/android/settingslib/wifi/WifiEntryPreference;->getWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object p2

    invoke-virtual {p2}, Lcom/android/wifitrackerlib/WifiEntry;->getScanResults()Ljava/util/Set;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->isMeteredHint(Ljava/util/Set;)Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object p1

    sget p2, Lcom/android/settings/R$drawable;->wifi_metered:I

    invoke-virtual {p1, p2}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget p2, p0, Lcom/android/settingslib/wifi/WifiEntryPreference;->mWifiStandard:I

    const/4 p3, 0x6

    if-ne p2, p3, :cond_1

    invoke-virtual {p0}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object p1

    sget p2, Lcom/android/settings/R$drawable;->wifi6_signal:I

    invoke-virtual {p1, p2}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    :cond_1
    :goto_0
    if-eqz p1, :cond_2

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    :cond_2
    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiWifiEntryPreference;->updateSignalLevel()V

    return-void
.end method
