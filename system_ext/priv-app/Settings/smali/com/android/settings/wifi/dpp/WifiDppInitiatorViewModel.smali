.class public Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;
.super Landroidx/lifecycle/AndroidViewModel;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel$EasyConnectDelegateCallback;
    }
.end annotation


# instance fields
.field private mBandArray:[I

.field private mEnrolleeSuccessNetworkId:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mIsWifiDppHandshaking:Z

.field private mStatusCode:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mTriedChannels:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "[I>;"
        }
    .end annotation
.end field

.field private mTriedSsid:Ljava/lang/String;


# direct methods
.method static bridge synthetic -$$Nest$fgetmEnrolleeSuccessNetworkId(Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;)Landroidx/lifecycle/MutableLiveData;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;->mEnrolleeSuccessNetworkId:Landroidx/lifecycle/MutableLiveData;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmStatusCode(Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;)Landroidx/lifecycle/MutableLiveData;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;->mStatusCode:Landroidx/lifecycle/MutableLiveData;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmBandArray(Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;[I)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;->mBandArray:[I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsWifiDppHandshaking(Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;->mIsWifiDppHandshaking:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmTriedChannels(Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;Landroid/util/SparseArray;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;->mTriedChannels:Landroid/util/SparseArray;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmTriedSsid(Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;->mTriedSsid:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;)V
    .locals 0

    invoke-direct {p0, p1}, Landroidx/lifecycle/AndroidViewModel;-><init>(Landroid/app/Application;)V

    return-void
.end method


# virtual methods
.method getBandArray()[I
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-object p0

    :goto_1
    iget-object p0, p0, Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;->mBandArray:[I

    goto/32 :goto_0

    nop
.end method

.method getEnrolleeSuccessNetworkId()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    goto/32 :goto_4

    nop

    :goto_0
    return-object p0

    :goto_1
    if-eqz v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_7

    nop

    :goto_2
    iput-object v0, p0, Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;->mEnrolleeSuccessNetworkId:Landroidx/lifecycle/MutableLiveData;

    :goto_3
    goto/32 :goto_5

    nop

    :goto_4
    iget-object v0, p0, Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;->mEnrolleeSuccessNetworkId:Landroidx/lifecycle/MutableLiveData;

    goto/32 :goto_1

    nop

    :goto_5
    iget-object p0, p0, Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;->mEnrolleeSuccessNetworkId:Landroidx/lifecycle/MutableLiveData;

    goto/32 :goto_0

    nop

    :goto_6
    invoke-direct {v0}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    goto/32 :goto_2

    nop

    :goto_7
    new-instance v0, Landroidx/lifecycle/MutableLiveData;

    goto/32 :goto_6

    nop
.end method

.method getStatusCode()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    new-instance v0, Landroidx/lifecycle/MutableLiveData;

    goto/32 :goto_7

    nop

    :goto_1
    iget-object v0, p0, Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;->mStatusCode:Landroidx/lifecycle/MutableLiveData;

    goto/32 :goto_3

    nop

    :goto_2
    iget-object p0, p0, Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;->mStatusCode:Landroidx/lifecycle/MutableLiveData;

    goto/32 :goto_4

    nop

    :goto_3
    if-eqz v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_0

    nop

    :goto_4
    return-object p0

    :goto_5
    iput-object v0, p0, Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;->mStatusCode:Landroidx/lifecycle/MutableLiveData;

    :goto_6
    goto/32 :goto_2

    nop

    :goto_7
    invoke-direct {v0}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    goto/32 :goto_5

    nop
.end method

.method getTriedChannels()Landroid/util/SparseArray;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray<",
            "[I>;"
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    iget-object p0, p0, Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;->mTriedChannels:Landroid/util/SparseArray;

    goto/32 :goto_1

    nop

    :goto_1
    return-object p0
.end method

.method getTriedSsid()Ljava/lang/String;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iget-object p0, p0, Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;->mTriedSsid:Ljava/lang/String;

    goto/32 :goto_1

    nop

    :goto_1
    return-object p0
.end method

.method isWifiDppHandshaking()Z
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return p0

    :goto_1
    iget-boolean p0, p0, Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;->mIsWifiDppHandshaking:Z

    goto/32 :goto_0

    nop
.end method

.method startEasyConnectAsConfiguratorInitiator(Ljava/lang/String;I)V
    .locals 7

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p0}, Landroidx/lifecycle/AndroidViewModel;->getApplication()Landroid/app/Application;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_d

    nop

    :goto_2
    const/4 v0, 0x1

    goto/32 :goto_e

    nop

    :goto_3
    invoke-virtual {v0}, Landroid/app/Application;->getMainExecutor()Ljava/util/concurrent/Executor;

    move-result-object v5

    goto/32 :goto_4

    nop

    :goto_4
    new-instance v6, Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel$EasyConnectDelegateCallback;

    goto/32 :goto_1

    nop

    :goto_5
    invoke-virtual {v0, v1}, Landroid/app/Application;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_f

    nop

    :goto_6
    invoke-virtual/range {v1 .. v6}, Landroid/net/wifi/WifiManager;->startEasyConnectAsConfiguratorInitiator(Ljava/lang/String;IILjava/util/concurrent/Executor;Landroid/net/wifi/EasyConnectStatusCallback;)V

    goto/32 :goto_9

    nop

    :goto_7
    move-object v2, p1

    goto/32 :goto_8

    nop

    :goto_8
    move v3, p2

    goto/32 :goto_6

    nop

    :goto_9
    return-void

    :goto_a
    check-cast v1, Landroid/net/wifi/WifiManager;

    goto/32 :goto_c

    nop

    :goto_b
    const-class v1, Landroid/net/wifi/WifiManager;

    goto/32 :goto_5

    nop

    :goto_c
    invoke-virtual {p0}, Landroidx/lifecycle/AndroidViewModel;->getApplication()Landroid/app/Application;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_d
    invoke-direct {v6, p0, v0}, Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel$EasyConnectDelegateCallback;-><init>(Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel$EasyConnectDelegateCallback-IA;)V

    goto/32 :goto_10

    nop

    :goto_e
    iput-boolean v0, p0, Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;->mIsWifiDppHandshaking:Z

    goto/32 :goto_0

    nop

    :goto_f
    move-object v1, v0

    goto/32 :goto_a

    nop

    :goto_10
    const/4 v4, 0x0

    goto/32 :goto_7

    nop
.end method

.method startEasyConnectAsEnrolleeInitiator(Ljava/lang/String;)V
    .locals 4

    goto/32 :goto_c

    nop

    :goto_0
    invoke-virtual {p0}, Landroidx/lifecycle/AndroidViewModel;->getApplication()Landroid/app/Application;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_1
    invoke-virtual {v0, v1}, Landroid/app/Application;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_2
    invoke-virtual {v1}, Landroid/app/Application;->getMainExecutor()Ljava/util/concurrent/Executor;

    move-result-object v1

    goto/32 :goto_7

    nop

    :goto_3
    return-void

    :goto_4
    check-cast v0, Landroid/net/wifi/WifiManager;

    goto/32 :goto_5

    nop

    :goto_5
    invoke-virtual {p0}, Landroidx/lifecycle/AndroidViewModel;->getApplication()Landroid/app/Application;

    move-result-object v1

    goto/32 :goto_2

    nop

    :goto_6
    const/4 v3, 0x0

    goto/32 :goto_a

    nop

    :goto_7
    new-instance v2, Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel$EasyConnectDelegateCallback;

    goto/32 :goto_6

    nop

    :goto_8
    iput-boolean v0, p0, Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;->mIsWifiDppHandshaking:Z

    goto/32 :goto_0

    nop

    :goto_9
    const-class v1, Landroid/net/wifi/WifiManager;

    goto/32 :goto_1

    nop

    :goto_a
    invoke-direct {v2, p0, v3}, Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel$EasyConnectDelegateCallback;-><init>(Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel;Lcom/android/settings/wifi/dpp/WifiDppInitiatorViewModel$EasyConnectDelegateCallback-IA;)V

    goto/32 :goto_b

    nop

    :goto_b
    invoke-virtual {v0, p1, v1, v2}, Landroid/net/wifi/WifiManager;->startEasyConnectAsEnrolleeInitiator(Ljava/lang/String;Ljava/util/concurrent/Executor;Landroid/net/wifi/EasyConnectStatusCallback;)V

    goto/32 :goto_3

    nop

    :goto_c
    const/4 v0, 0x1

    goto/32 :goto_8

    nop
.end method
