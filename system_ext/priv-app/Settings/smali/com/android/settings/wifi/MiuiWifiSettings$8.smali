.class Lcom/android/settings/wifi/MiuiWifiSettings$8;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/settings/wifi/MiuiWifiSettings;->initDppHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/wifi/MiuiWifiSettings;


# direct methods
.method constructor <init>(Lcom/android/settings/wifi/MiuiWifiSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/MiuiWifiSettings$8;->this$0:Lcom/android/settings/wifi/MiuiWifiSettings;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    const/16 v2, 0x5001

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings$8;->this$0:Lcom/android/settings/wifi/MiuiWifiSettings;

    invoke-static {v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->access$000(Lcom/android/settings/wifi/MiuiWifiSettings;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object p1, p0, Lcom/android/settings/wifi/MiuiWifiSettings$8;->this$0:Lcom/android/settings/wifi/MiuiWifiSettings;

    invoke-static {p1, v1}, Lcom/android/settings/wifi/MiuiWifiSettings;->access$102(Lcom/android/settings/wifi/MiuiWifiSettings;Z)Z

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiWifiSettings$8;->this$0:Lcom/android/settings/wifi/MiuiWifiSettings;

    const-class p0, Lcom/android/settings/wifi/dpp/MiuishowDppQrCodeFragment;

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x65

    const/4 v6, 0x0

    sget v7, Lcom/android/settings/R$string;->dpp_theme_title:I

    move-object v2, v3

    invoke-virtual/range {v2 .. v7}, Lcom/android/settings/SettingsPreferenceFragment;->startFragment(Landroidx/fragment/app/Fragment;Ljava/lang/String;ILandroid/os/Bundle;I)Z

    goto :goto_0

    :cond_0
    iget p1, p1, Landroid/os/Message;->what:I

    const/16 v0, 0x5002

    if-ne p1, v0, :cond_2

    iget-object p1, p0, Lcom/android/settings/wifi/MiuiWifiSettings$8;->this$0:Lcom/android/settings/wifi/MiuiWifiSettings;

    invoke-static {p1}, Lcom/android/settings/wifi/MiuiWifiSettings;->access$200(Lcom/android/settings/wifi/MiuiWifiSettings;)Z

    move-result p1

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/android/settings/wifi/MiuiWifiSettings$8;->this$0:Lcom/android/settings/wifi/MiuiWifiSettings;

    invoke-static {p1, v1}, Lcom/android/settings/wifi/MiuiWifiSettings;->access$302(Lcom/android/settings/wifi/MiuiWifiSettings;Z)Z

    iget-object p1, p0, Lcom/android/settings/wifi/MiuiWifiSettings$8;->this$0:Lcom/android/settings/wifi/MiuiWifiSettings;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/android/settings/wifi/MiuiWifiSettings$8;->this$0:Lcom/android/settings/wifi/MiuiWifiSettings;

    invoke-static {p1}, Lcom/android/settings/wifi/MiuiWifiSettings;->-$$Nest$fgetmWifiQrcode(Lcom/android/settings/wifi/MiuiWifiSettings;)Lcom/android/settings/wifi/dpp/WifiQrCode;

    move-result-object p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/android/settings/wifi/MiuiWifiSettings$8;->this$0:Lcom/android/settings/wifi/MiuiWifiSettings;

    invoke-static {p1}, Lcom/android/settings/wifi/MiuiWifiSettings;->-$$Nest$fgetmWifiNetworkConfig(Lcom/android/settings/wifi/MiuiWifiSettings;)Lcom/android/settings/wifi/dpp/WifiNetworkConfig;

    move-result-object p1

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    new-instance p1, Lcom/android/settings/wifi/dpp/MiuiWifiDppUtils;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings$8;->this$0:Lcom/android/settings/wifi/MiuiWifiSettings;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/android/settings/wifi/dpp/MiuiWifiDppUtils;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiWifiSettings$8;->this$0:Lcom/android/settings/wifi/MiuiWifiSettings;

    invoke-static {v0}, Lcom/android/settings/wifi/MiuiWifiSettings;->-$$Nest$fgetmWifiQrcode(Lcom/android/settings/wifi/MiuiWifiSettings;)Lcom/android/settings/wifi/dpp/WifiQrCode;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/settings/wifi/dpp/MiuiWifiDppUtils;->setWifiQrCode(Lcom/android/settings/wifi/dpp/WifiQrCode;)V

    iget-object p0, p0, Lcom/android/settings/wifi/MiuiWifiSettings$8;->this$0:Lcom/android/settings/wifi/MiuiWifiSettings;

    invoke-static {p0}, Lcom/android/settings/wifi/MiuiWifiSettings;->-$$Nest$fgetmWifiNetworkConfig(Lcom/android/settings/wifi/MiuiWifiSettings;)Lcom/android/settings/wifi/dpp/WifiNetworkConfig;

    move-result-object p0

    invoke-virtual {p1, p0}, Lcom/android/settings/wifi/dpp/MiuiWifiDppUtils;->setWifiNetworkConfig(Lcom/android/settings/wifi/dpp/WifiNetworkConfig;)V

    invoke-virtual {p1}, Lcom/android/settings/wifi/dpp/MiuiWifiDppUtils;->showWifiShareDialog()V

    nop

    :cond_2
    :goto_0
    return-void
.end method
