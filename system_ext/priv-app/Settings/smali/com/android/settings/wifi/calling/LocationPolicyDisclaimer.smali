.class Lcom/android/settings/wifi/calling/LocationPolicyDisclaimer;
.super Lcom/android/settings/wifi/calling/DisclaimerItem;


# static fields
.field static final KEY_HAS_AGREED_LOCATION_DISCLAIMER:Ljava/lang/String; = "key_has_agreed_location_disclaimer"
    .annotation build Lcom/android/internal/annotations/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/wifi/calling/DisclaimerItem;-><init>(Landroid/content/Context;I)V

    return-void
.end method


# virtual methods
.method protected getMessageId()I
    .locals 0

    sget p0, Lcom/android/settings/R$string;->wfc_disclaimer_location_desc_text:I

    return p0
.end method

.method protected getName()Ljava/lang/String;
    .locals 0

    const-string p0, "LocationPolicyDisclaimer"

    return-object p0
.end method

.method protected getPrefKey()Ljava/lang/String;
    .locals 0

    const-string p0, "key_has_agreed_location_disclaimer"

    return-object p0
.end method

.method protected getTitleId()I
    .locals 0

    sget p0, Lcom/android/settings/R$string;->wfc_disclaimer_location_title_text:I

    return p0
.end method

.method shouldShow()Z
    .locals 3

    goto/32 :goto_11

    nop

    :goto_0
    const/4 v2, 0x0

    goto/32 :goto_2

    nop

    :goto_1
    const-string v1, "carrier_default_wfc_ims_enabled_bool"

    goto/32 :goto_b

    nop

    :goto_2
    if-eqz v1, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_7

    nop

    :goto_3
    return p0

    :goto_4
    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/calling/DisclaimerItem;->logd(Ljava/lang/String;)V

    goto/32 :goto_9

    nop

    :goto_5
    invoke-super {p0}, Lcom/android/settings/wifi/calling/DisclaimerItem;->shouldShow()Z

    move-result p0

    goto/32 :goto_3

    nop

    :goto_6
    const-string/jumbo v1, "show_wfc_location_privacy_policy_bool"

    goto/32 :goto_c

    nop

    :goto_7
    const-string/jumbo v0, "shouldShow: false due to carrier config is false."

    goto/32 :goto_e

    nop

    :goto_8
    if-nez v0, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_d

    nop

    :goto_9
    return v2

    :goto_a
    goto/32 :goto_5

    nop

    :goto_b
    invoke-virtual {v0, v1}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    goto/32 :goto_8

    nop

    :goto_c
    invoke-virtual {v0, v1}, Landroid/os/PersistableBundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    goto/32 :goto_0

    nop

    :goto_d
    const-string/jumbo v0, "shouldShow: false due to WFC is on as default."

    goto/32 :goto_4

    nop

    :goto_e
    invoke-virtual {p0, v0}, Lcom/android/settings/wifi/calling/DisclaimerItem;->logd(Ljava/lang/String;)V

    goto/32 :goto_f

    nop

    :goto_f
    return v2

    :goto_10
    goto/32 :goto_1

    nop

    :goto_11
    invoke-virtual {p0}, Lcom/android/settings/wifi/calling/DisclaimerItem;->getCarrierConfig()Landroid/os/PersistableBundle;

    move-result-object v0

    goto/32 :goto_6

    nop
.end method
