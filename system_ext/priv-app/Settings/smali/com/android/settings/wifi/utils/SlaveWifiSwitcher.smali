.class public Lcom/android/settings/wifi/utils/SlaveWifiSwitcher;
.super Ljava/lang/Object;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDailog:Lmiuix/appcompat/app/AlertDialog;

.field private final mIsDbsDualWifiSupport:Z

.field private mTracker:Lcom/android/wifitrackerlib/WifiPickerTracker;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/wifitrackerlib/WifiPickerTracker;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/wifi/utils/SlaveWifiSwitcher;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/settings/wifi/utils/SlaveWifiSwitcher;->mTracker:Lcom/android/wifitrackerlib/WifiPickerTracker;

    invoke-static {p1}, Lcom/android/settingslib/wifi/SlaveWifiUtils;->getInstance(Landroid/content/Context;)Lcom/android/settingslib/wifi/SlaveWifiUtils;

    move-result-object p1

    invoke-virtual {p1}, Lcom/android/settingslib/wifi/SlaveWifiUtils;->supportDbsDualWifi()Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/settings/wifi/utils/SlaveWifiSwitcher;->mIsDbsDualWifiSupport:Z

    return-void
.end method


# virtual methods
.method public cancelDialogIfNeed()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/utils/SlaveWifiSwitcher;->mDailog:Lmiuix/appcompat/app/AlertDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/utils/SlaveWifiSwitcher;->mTracker:Lcom/android/wifitrackerlib/WifiPickerTracker;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/wifitrackerlib/WifiPickerTracker;->getSlaveConnectedWifiEntry()Lcom/android/wifitrackerlib/WifiEntry;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "SlaveWifiSwitcher"

    const-string v1, "cancel dialog, getSlaveConnectedWifiEntry is null."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p0, p0, Lcom/android/settings/wifi/utils/SlaveWifiSwitcher;->mDailog:Lmiuix/appcompat/app/AlertDialog;

    invoke-virtual {p0}, Landroid/app/Dialog;->cancel()V

    :cond_0
    return-void
.end method
