.class public Lcom/android/settings/wifi/linkturbo/ApkIconLoader;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/wifi/linkturbo/ApkIconLoader$LoaderThread;,
        Lcom/android/settings/wifi/linkturbo/ApkIconLoader$FileId;,
        Lcom/android/settings/wifi/linkturbo/ApkIconLoader$DrawableHolder;,
        Lcom/android/settings/wifi/linkturbo/ApkIconLoader$ImageHolder;
    }
.end annotation


# static fields
.field private static final mImageCache:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/android/settings/wifi/linkturbo/ApkIconLoader$ImageHolder;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mLoaderThread:Lcom/android/settings/wifi/linkturbo/ApkIconLoader$LoaderThread;

.field private mLoadingRequested:Z

.field private final mMainThreadHandler:Landroid/os/Handler;

.field private mPaused:Z

.field private final mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Landroid/widget/ImageView;",
            "Lcom/android/settings/wifi/linkturbo/ApkIconLoader$FileId;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/android/settings/wifi/linkturbo/ApkIconLoader;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMainThreadHandler(Lcom/android/settings/wifi/linkturbo/ApkIconLoader;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->mMainThreadHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPendingRequests(Lcom/android/settings/wifi/linkturbo/ApkIconLoader;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$sfgetmImageCache()Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1

    sget-object v0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->mImageCache:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->mImageCache:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->mMainThreadHandler:Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->mContext:Landroid/content/Context;

    return-void
.end method

.method private loadCachedIcon(Landroid/widget/ImageView;Ljava/lang/String;)Z
    .locals 1

    sget-object p0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->mImageCache:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p0, p2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader$ImageHolder;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/settings/wifi/linkturbo/ApkIconLoader$ImageHolder;->create()Lcom/android/settings/wifi/linkturbo/ApkIconLoader$ImageHolder;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget p0, v0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader$ImageHolder;->state:I

    const/4 p2, 0x2

    if-ne p0, p2, :cond_2

    invoke-virtual {v0}, Lcom/android/settings/wifi/linkturbo/ApkIconLoader$ImageHolder;->isNull()Z

    move-result p0

    const/4 p2, 0x1

    if-eqz p0, :cond_1

    return p2

    :cond_1
    invoke-virtual {v0, p1}, Lcom/android/settings/wifi/linkturbo/ApkIconLoader$ImageHolder;->setImageView(Landroid/widget/ImageView;)Z

    move-result p0

    if-eqz p0, :cond_2

    return p2

    :cond_2
    :goto_0
    const/4 p0, 0x0

    iput p0, v0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader$ImageHolder;->state:I

    return p0
.end method

.method private processLoadedIcons()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/settings/wifi/linkturbo/ApkIconLoader$FileId;

    iget-object v2, v2, Lcom/android/settings/wifi/linkturbo/ApkIconLoader$FileId;->mPkgName:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->loadCachedIcon(Landroid/widget/ImageView;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->requestLoading()V

    :cond_2
    return-void
.end method

.method private requestLoading()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->mLoadingRequested:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->mLoadingRequested:Z

    iget-object p0, p0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->mMainThreadHandler:Landroid/os/Handler;

    invoke-virtual {p0, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 3

    iget p1, p1, Landroid/os/Message;->what:I

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eq p1, v1, :cond_2

    const/4 v2, 0x2

    if-eq p1, v2, :cond_0

    return v0

    :cond_0
    iget-boolean p1, p0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->mPaused:Z

    if-nez p1, :cond_1

    invoke-direct {p0}, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->processLoadedIcons()V

    :cond_1
    return v1

    :cond_2
    iput-boolean v0, p0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->mLoadingRequested:Z

    iget-boolean p1, p0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->mPaused:Z

    if-nez p1, :cond_4

    iget-object p1, p0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->mLoaderThread:Lcom/android/settings/wifi/linkturbo/ApkIconLoader$LoaderThread;

    if-nez p1, :cond_3

    new-instance p1, Lcom/android/settings/wifi/linkturbo/ApkIconLoader$LoaderThread;

    invoke-direct {p1, p0}, Lcom/android/settings/wifi/linkturbo/ApkIconLoader$LoaderThread;-><init>(Lcom/android/settings/wifi/linkturbo/ApkIconLoader;)V

    iput-object p1, p0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->mLoaderThread:Lcom/android/settings/wifi/linkturbo/ApkIconLoader$LoaderThread;

    invoke-virtual {p1}, Landroid/os/HandlerThread;->start()V

    :cond_3
    iget-object p0, p0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->mLoaderThread:Lcom/android/settings/wifi/linkturbo/ApkIconLoader$LoaderThread;

    invoke-virtual {p0}, Lcom/android/settings/wifi/linkturbo/ApkIconLoader$LoaderThread;->requestLoading()V

    :cond_4
    return v1
.end method

.method public loadIcon(Landroid/widget/ImageView;Ljava/lang/String;)Z
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->loadCachedIcon(Landroid/widget/ImageView;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/android/settings/wifi/linkturbo/ApkIconLoader$FileId;

    invoke-direct {v1, p2}, Lcom/android/settings/wifi/linkturbo/ApkIconLoader$FileId;-><init>(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p2, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean p1, p0, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->mPaused:Z

    if-nez p1, :cond_1

    invoke-direct {p0}, Lcom/android/settings/wifi/linkturbo/ApkIconLoader;->requestLoading()V

    :cond_1
    :goto_0
    return v0
.end method
