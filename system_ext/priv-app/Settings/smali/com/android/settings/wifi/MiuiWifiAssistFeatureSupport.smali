.class public Lcom/android/settings/wifi/MiuiWifiAssistFeatureSupport;
.super Ljava/lang/Object;


# static fields
.field private static final IS_INTERNATIONAL_BUILD:Z

.field private static final IS_JP_KDDI_VERSION:Z

.field private static final IS_STABLE_VERSION:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    sput-boolean v0, Lcom/android/settings/wifi/MiuiWifiAssistFeatureSupport;->IS_INTERNATIONAL_BUILD:Z

    sget-boolean v0, Lmiui/os/Build;->IS_STABLE_VERSION:Z

    sput-boolean v0, Lcom/android/settings/wifi/MiuiWifiAssistFeatureSupport;->IS_STABLE_VERSION:Z

    sget-boolean v0, Lcom/android/settings/RegionUtils;->IS_JP_KDDI:Z

    sput-boolean v0, Lcom/android/settings/wifi/MiuiWifiAssistFeatureSupport;->IS_JP_KDDI_VERSION:Z

    return-void
.end method

.method public static getWifiWakeupStatus()I
    .locals 1

    sget-boolean v0, Lcom/android/settings/wifi/MiuiWifiAssistFeatureSupport;->IS_JP_KDDI_VERSION:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x3

    :goto_0
    return v0
.end method

.method public static isDualSimSupported(Landroid/content/Context;)Z
    .locals 1

    invoke-static {p0}, Lcom/android/settings/wifi/MiuiWifiAssistFeatureSupport;->isLinkTurbAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/android/settings/wifi/linkturbo/LinkTurboUtils;->shouldHideSmartDualSimButton(Landroid/content/Context;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isDualWifiSupported(Landroid/content/Context;)Z
    .locals 1

    invoke-static {p0}, Lcom/android/settingslib/wifi/SlaveWifiUtils;->getInstance(Landroid/content/Context;)Lcom/android/settingslib/wifi/SlaveWifiUtils;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/settingslib/wifi/SlaveWifiUtils;->isUiVisible(Landroid/content/Context;)Z

    move-result p0

    return p0
.end method

.method public static isLinkTurbAvailable(Landroid/content/Context;)Z
    .locals 0

    invoke-static {p0}, Lcom/android/settings/wifi/linkturbo/LinkTurboClient;->isLinkTurboSupported(Landroid/content/Context;)Z

    move-result p0

    return p0
.end method

.method public static isTrafficPriorityAvailable()Z
    .locals 2

    const-string/jumbo v0, "sys.net.support.netprio"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isWifiAssistAvailable(Landroid/content/Context;)Z
    .locals 4

    invoke-static {p0}, Lcom/android/settings/wifi/MiuiWifiAssistFeatureSupport;->isWifiAssistantAvailable(Landroid/content/Context;)Z

    move-result v0

    invoke-static {p0}, Lcom/android/settings/wifi/MiuiWifiAssistFeatureSupport;->isDualWifiSupported(Landroid/content/Context;)Z

    move-result v1

    invoke-static {p0}, Lcom/android/settings/wifi/MiuiWifiAssistFeatureSupport;->isDualSimSupported(Landroid/content/Context;)Z

    invoke-static {p0}, Lcom/android/settings/wifi/MiuiWifiAssistFeatureSupport;->isLinkTurbAvailable(Landroid/content/Context;)Z

    move-result p0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Judge whether the Wifi Assist Fragment is available { CanWifiAssistant : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v3, " , isDualWifiSupported : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v3, " , linkTurbAvailable : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v3, " }"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiWifiAssistFeatureSupport"

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_1

    if-nez v1, :cond_1

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const-string p0, "Wifi Assist Function is null, So the fragment should be hidden."

    invoke-static {v3, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p0, 0x0

    return p0

    :cond_1
    :goto_0
    const-string p0, "Wifi Assist Fragment is available!"

    invoke-static {v3, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p0, 0x1

    return p0
.end method

.method public static isWifiAssistantAvailable(Landroid/content/Context;)Z
    .locals 0

    if-eqz p0, :cond_1

    invoke-static {p0}, Lcom/android/settingslib/Utils;->isWifiOnly(Landroid/content/Context;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x1

    return p0

    :cond_1
    :goto_0
    const/4 p0, 0x0

    return p0
.end method

.method public static isWifiWakeupAvailable()Z
    .locals 1

    invoke-static {}, Lcom/android/settings/wifi/MiuiWifiAssistFeatureSupport;->getWifiWakeupStatus()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
