.class public Lcom/android/settings/wifi/passpoint/PasspointConfigureReceiver;
.super Landroid/content/BroadcastReceiver;


# static fields
.field private static mIsRegistered:Z

.field private static mPasspointConfigureReceiver:Lcom/android/settings/wifi/passpoint/PasspointConfigureReceiver;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private disablePasspointWifiReceiver(Landroid/content/Context;)V
    .locals 1

    sget-boolean v0, Lcom/android/settings/wifi/passpoint/PasspointConfigureReceiver;->mIsRegistered:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 p0, 0x0

    sput-boolean p0, Lcom/android/settings/wifi/passpoint/PasspointConfigureReceiver;->mIsRegistered:Z

    :cond_0
    return-void
.end method

.method public static enablePasspointWifiReceiver(Landroid/content/Context;)V
    .locals 2

    sget-object v0, Lcom/android/settings/wifi/passpoint/PasspointConfigureReceiver;->mPasspointConfigureReceiver:Lcom/android/settings/wifi/passpoint/PasspointConfigureReceiver;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/settings/wifi/passpoint/PasspointConfigureReceiver;

    invoke-direct {v0}, Lcom/android/settings/wifi/passpoint/PasspointConfigureReceiver;-><init>()V

    sput-object v0, Lcom/android/settings/wifi/passpoint/PasspointConfigureReceiver;->mPasspointConfigureReceiver:Lcom/android/settings/wifi/passpoint/PasspointConfigureReceiver;

    :cond_0
    sget-boolean v0, Lcom/android/settings/wifi/passpoint/PasspointConfigureReceiver;->mIsRegistered:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/settings/wifi/passpoint/PasspointConfigureReceiver;->mIsRegistered:Z

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    sget-object v1, Lcom/android/settings/wifi/passpoint/PasspointConfigureReceiver;->mPasspointConfigureReceiver:Lcom/android/settings/wifi/passpoint/PasspointConfigureReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_1
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "wifi"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    const-string v2, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result p2

    if-ne p2, v3, :cond_5

    invoke-static {p1}, Lcom/android/settings/wifi/passpoint/MiuiPasspointR1Utils;->removeAllUnregisteredConfig(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/passpoint/PasspointConfigureReceiver;->disablePasspointWifiReceiver(Landroid/content/Context;)V

    goto/16 :goto_2

    :cond_0
    const-string p0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    const-string p0, "networkInfo"

    invoke-virtual {p2, p0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Landroid/net/NetworkInfo;

    if-eqz p0, :cond_5

    sget-object p2, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object p0

    invoke-virtual {p2, p0}, Landroid/net/NetworkInfo$State;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object p0

    if-eqz p0, :cond_5

    invoke-virtual {p0}, Landroid/net/wifi/WifiInfo;->isPasspointAp()Z

    move-result p2

    if-eqz p2, :cond_5

    invoke-virtual {p0}, Landroid/net/wifi/WifiInfo;->isOsuAp()Z

    move-result p2

    if-nez p2, :cond_5

    invoke-virtual {p0}, Landroid/net/wifi/WifiInfo;->getPasspointProviderFriendlyName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0}, Landroid/net/wifi/WifiInfo;->getPasspointFqdn()Ljava/lang/String;

    move-result-object p0

    invoke-static {p1, p0}, Lcom/android/settings/wifi/passpoint/MiuiPasspointR1Utils;->getRegisterState(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1, p0, p2}, Lcom/android/settings/wifi/passpoint/MiuiPasspointR1Utils;->gotoLoginActivity(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_1
    new-instance p0, Landroid/content/Intent;

    const-string p2, "com.miui.wifi.passpoint.action.PASSPOINT_CONNECTED"

    invoke-direct {p0, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_2

    :cond_2
    const-string p0, "android.settings.wifi.PASSPOINT_LOGIN_RESULT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    const-string p0, "friendly_name"

    invoke-virtual {p2, p0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const-string/jumbo v0, "result"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p2

    if-eqz p0, :cond_3

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    const-string v0, "exands"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_3

    const-string p0, "exands.com"

    goto :goto_0

    :cond_3
    const-string p0, ""

    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_5

    if-nez p2, :cond_4

    move v0, v3

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    :goto_1
    invoke-static {p1, p0, v0}, Lcom/android/settings/wifi/passpoint/MiuiPasspointR1Utils;->saveRegisterState(Landroid/content/Context;Ljava/lang/String;Z)V

    if-ne p2, v3, :cond_5

    invoke-static {p1, p0}, Lcom/android/settings/wifi/passpoint/MiuiPasspointR1Utils;->removePasspointConfig(Landroid/content/Context;Ljava/lang/String;)V

    :cond_5
    :goto_2
    return-void
.end method
