.class public Lcom/android/settings/wifi/MiuiAddNetworkFragment;
.super Lcom/android/settings/BaseEditFragment;

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field static final UNDESIRED_CERTIFICATES:[Ljava/lang/String;


# instance fields
.field private mAccessPointSecurity:I

.field private mContext:Landroid/content/Context;

.field private mDoNotProvideEapUserCertString:Ljava/lang/String;

.field private mDoNotValidateEapServerString:Ljava/lang/String;

.field private mEapAnonymousView:Landroid/widget/TextView;

.field private mEapCaCertSpinner:Lmiuix/appcompat/widget/Spinner;

.field private mEapDomainView:Landroid/widget/TextView;

.field private mEapIdentityView:Landroid/widget/TextView;

.field private mEapMethodSpinner:Lmiuix/appcompat/widget/Spinner;

.field private mEapUserCertSpinner:Lmiuix/appcompat/widget/Spinner;

.field private mHiddenGbkSpinner:Lmiuix/appcompat/widget/Spinner;

.field private mHiddenGbkSpinnerParent:Landroid/widget/LinearLayout;

.field private mHiddenSettingsSpinner:Lmiuix/appcompat/widget/Spinner;

.field private mInstallCertsString:Ljava/lang/String;

.field private mIpFieldsSpinner:Lmiuix/appcompat/widget/Spinner;

.field private mIpFieldsSpinnerParent:Landroid/widget/LinearLayout;

.field private mLPrivacySettingsSpinner:Lmiuix/appcompat/widget/Spinner;

.field private mMaxSsidLength:I

.field private mMultipleCertSetString:Ljava/lang/String;

.field private mPasswordView:Landroid/widget/TextView;

.field private mPhase2Adapter:Landroid/widget/ArrayAdapter;

.field private mPhase2PeapAdapter:Landroid/widget/ArrayAdapter;

.field private mPhase2Spinner:Lmiuix/appcompat/widget/Spinner;

.field private mPhase2TtlsAdapter:Landroid/widget/ArrayAdapter;

.field private mPrivacySettingsSpinner:Lmiuix/appcompat/widget/Spinner;

.field private mProxySettingsSpinner:Lmiuix/appcompat/widget/Spinner;

.field private mProxySettingsSpinnerParent:Landroid/widget/LinearLayout;

.field private mSecuritySpinner:Lmiuix/appcompat/widget/Spinner;

.field private mSimCardSpinner:Lmiuix/appcompat/widget/Spinner;

.field private mSimDisplayNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSsidView:Landroid/widget/TextView;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mTextViewChangedHandler:Landroid/os/Handler;

.field private mUnspecifiedCertString:Ljava/lang/String;

.field private mUseSystemCertsString:Ljava/lang/String;

.field private mView:Landroid/view/View;

.field private mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method public static synthetic $r8$lambda$05GWwus4XwvdXp2nTk-P2nES1Yg(Ljava/lang/String;)Z
    .locals 0

    invoke-static {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->lambda$loadCertificates$2(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$Q9206LyPpzpvATsJPj89bwYCmQY(I)Z
    .locals 0

    invoke-static {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->lambda$getConfig$1(I)Z

    move-result p0

    return p0
.end method

.method public static synthetic $r8$lambda$eHClf9HBB33J6UaDaywXxNUQ_to(Landroid/view/View;)V
    .locals 0

    invoke-static {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->lambda$adaptSoftInput$0(Landroid/view/View;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$menableSubmitIfAppropriate(Lcom/android/settings/wifi/MiuiAddNetworkFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->enableSubmitIfAppropriate()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    const-string v0, "MacRandSecret"

    const-string v1, "MacRandSapSecret"

    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->UNDESIRED_CERTIFICATES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/BaseEditFragment;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mMaxSsidLength:I

    return-void
.end method

.method private adaptSoftInput(Landroid/app/Activity;)V
    .locals 2

    const/4 p0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "navigation_mode"

    invoke-static {v0, v1, p0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 p0, 0x1

    :cond_0
    if-eqz p0, :cond_1

    :try_start_0
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p0

    invoke-virtual {p0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p0

    invoke-virtual {p0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    new-instance v0, Lcom/android/settings/wifi/MiuiAddNetworkFragment$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment$$ExternalSyntheticLambda0;-><init>(Landroid/view/View;)V

    invoke-virtual {p1, v0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string p0, "MiuiAddNetworkFragment"

    const-string p1, "Resize decorView layout error!"

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void
.end method

.method private checkSsidLength()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSsidView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    array-length v1, v1

    iget v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mMaxSsidLength:I

    if-le v1, v2, :cond_1

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    array-length v1, v1

    iget v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mMaxSsidLength:I

    if-gt v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSsidView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSsidView:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p0

    check-cast p0, Landroid/text/Spannable;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-static {p0, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    :cond_1
    return-void
.end method

.method private enableSubmitIfAppropriate()V
    .locals 7

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSsidView:Landroid/widget/TextView;

    const/4 v1, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSsidView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eq v0, v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v3

    :goto_0
    if-eqz v0, :cond_7

    iget-object v4, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPasswordView:Landroid/widget/TextView;

    if-eqz v4, :cond_7

    iget v5, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mAccessPointSecurity:I

    const/4 v6, 0x5

    if-ne v5, v1, :cond_4

    invoke-virtual {v4}, Landroid/widget/TextView;->length()I

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    move v0, v3

    goto :goto_2

    :cond_1
    iget-object v4, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0xa

    if-eq v1, v5, :cond_2

    const/16 v5, 0x1a

    if-ne v1, v5, :cond_3

    :cond_2
    const-string v5, "[0-9A-Fa-f]*"

    invoke-virtual {v4, v5}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    :cond_3
    if-eq v1, v6, :cond_4

    const/16 v4, 0xd

    if-eq v1, v4, :cond_4

    goto :goto_1

    :cond_4
    :goto_2
    iget v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mAccessPointSecurity:I

    const/4 v4, 0x2

    if-eq v1, v4, :cond_5

    if-ne v1, v6, :cond_6

    :cond_5
    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->length()I

    move-result v1

    if-lt v1, v2, :cond_8

    :cond_6
    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->length()I

    move-result v1

    const/16 v2, 0x3f

    if-le v1, v2, :cond_7

    goto :goto_3

    :cond_7
    move v3, v0

    :cond_8
    :goto_3
    invoke-virtual {p0, v3}, Lcom/android/settings/BaseEditFragment;->onEditStateChange(Z)V

    return-void
.end method

.method private getAndroidKeystoreAliasLoader()Lcom/android/settings/utils/AndroidKeystoreAliasLoader;
    .locals 1

    new-instance p0, Lcom/android/settings/utils/AndroidKeystoreAliasLoader;

    const/16 v0, 0x66

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/settings/utils/AndroidKeystoreAliasLoader;-><init>(Ljava/lang/Integer;)V

    return-object p0
.end method

.method private getConfig()Landroid/net/wifi/WifiConfiguration;
    .locals 13

    new-instance v0, Landroid/net/wifi/WifiConfiguration;

    invoke-direct {v0}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSsidView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/settingslib/wifi/AccessPoint;->convertToQuotedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mHiddenSettingsSpinner:Lmiuix/appcompat/widget/Spinner;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    if-ne v1, v3, :cond_0

    goto :goto_0

    :cond_0
    move v1, v2

    goto :goto_1

    :cond_1
    :goto_0
    move v1, v3

    :goto_1
    iput-boolean v1, v0, Landroid/net/wifi/WifiConfiguration;->hiddenSSID:Z

    const-string v4, "MiuiAddNetworkFragment"

    if-eqz v1, :cond_5

    invoke-static {}, Lcom/android/wifitrackerlib/WifiEntry;->isGbkSsidSupported()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSsidView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mHiddenGbkSpinner:Lmiuix/appcompat/widget/Spinner;

    if-nez v5, :cond_3

    :cond_2
    move v5, v2

    goto :goto_2

    :cond_3
    invoke-virtual {v5}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v5

    if-ne v5, v3, :cond_2

    move v5, v3

    :goto_2
    invoke-virtual {v1}, Ljava/lang/String;->chars()Ljava/util/stream/IntStream;

    move-result-object v1

    new-instance v6, Lcom/android/settings/wifi/MiuiAddNetworkFragment$$ExternalSyntheticLambda1;

    invoke-direct {v6}, Lcom/android/settings/wifi/MiuiAddNetworkFragment$$ExternalSyntheticLambda1;-><init>()V

    invoke-interface {v1, v6}, Ljava/util/stream/IntStream;->allMatch(Ljava/util/function/IntPredicate;)Z

    move-result v1

    if-eqz v5, :cond_4

    if-nez v1, :cond_4

    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/settings/wifi/WifiUtils;->quotedStrToGbkHex(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getConfig SSID: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mAccessPointSecurity:I

    const/4 v5, 0x3

    if-eqz v1, :cond_21

    const/16 v6, 0x22

    if-eq v1, v3, :cond_1e

    const/4 v7, 0x2

    if-eq v1, v7, :cond_1b

    const/4 v8, 0x0

    const/4 v9, 0x5

    if-eq v1, v5, :cond_6

    if-eq v1, v9, :cond_1b

    return-object v8

    :cond_6
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v1, v7}, Ljava/util/BitSet;->set(I)V

    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v1, v5}, Ljava/util/BitSet;->set(I)V

    new-instance v1, Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-direct {v1}, Landroid/net/wifi/WifiEnterpriseConfig;-><init>()V

    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapMethodSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    iget-object v6, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPhase2Spinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v6}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v6

    iget-object v10, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v10, v1}, Landroid/net/wifi/WifiEnterpriseConfig;->setEapMethod(I)V

    const/4 v10, 0x6

    const/4 v11, 0x4

    if-eqz v1, :cond_8

    if-eq v1, v11, :cond_7

    if-eq v1, v9, :cond_7

    if-eq v1, v10, :cond_7

    iget-object v7, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v7, v6}, Landroid/net/wifi/WifiEnterpriseConfig;->setPhase2Method(I)V

    goto :goto_3

    :cond_7
    iget-object v6, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    invoke-static {v6}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v6

    iget-object v7, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSimCardSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v7}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoForSimSlotIndex(I)Landroid/telephony/SubscriptionInfo;

    move-result-object v6

    if-eqz v6, :cond_f

    invoke-virtual {v6}, Landroid/telephony/SubscriptionInfo;->getCarrierId()I

    move-result v7

    iput v7, v0, Landroid/net/wifi/WifiConfiguration;->carrierId:I

    invoke-virtual {v6}, Landroid/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v6

    iput v6, v0, Landroid/net/wifi/WifiConfiguration;->subscriptionId:I

    goto :goto_3

    :cond_8
    if-eqz v6, :cond_e

    if-eq v6, v3, :cond_d

    if-eq v6, v7, :cond_c

    if-eq v6, v5, :cond_b

    if-eq v6, v11, :cond_a

    if-eq v6, v9, :cond_9

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Unknown phase2 method"

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_9
    iget-object v6, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    const/4 v7, 0x7

    invoke-virtual {v6, v7}, Landroid/net/wifi/WifiEnterpriseConfig;->setPhase2Method(I)V

    goto :goto_3

    :cond_a
    iget-object v6, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v6, v10}, Landroid/net/wifi/WifiEnterpriseConfig;->setPhase2Method(I)V

    goto :goto_3

    :cond_b
    iget-object v6, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v6, v9}, Landroid/net/wifi/WifiEnterpriseConfig;->setPhase2Method(I)V

    goto :goto_3

    :cond_c
    iget-object v6, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v6, v11}, Landroid/net/wifi/WifiEnterpriseConfig;->setPhase2Method(I)V

    goto :goto_3

    :cond_d
    iget-object v6, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v6, v5}, Landroid/net/wifi/WifiEnterpriseConfig;->setPhase2Method(I)V

    goto :goto_3

    :cond_e
    iget-object v6, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v6, v2}, Landroid/net/wifi/WifiEnterpriseConfig;->setPhase2Method(I)V

    :cond_f
    :goto_3
    iget-object v6, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapCaCertSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v6}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iget-object v7, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v7, v8}, Landroid/net/wifi/WifiEnterpriseConfig;->setCaCertificateAliases([Ljava/lang/String;)V

    iget-object v7, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v7, v8}, Landroid/net/wifi/WifiEnterpriseConfig;->setCaPath(Ljava/lang/String;)V

    iget-object v7, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    iget-object v8, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapDomainView:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/net/wifi/WifiEnterpriseConfig;->setDomainSuffixMatch(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mUnspecifiedCertString:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_13

    iget-object v7, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mDoNotValidateEapServerString:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    goto :goto_4

    :cond_10
    iget-object v7, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mUseSystemCertsString:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_11

    iget-object v2, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    const-string v3, "/system/etc/security/cacerts"

    invoke-virtual {v2, v3}, Landroid/net/wifi/WifiEnterpriseConfig;->setCaPath(Ljava/lang/String;)V

    goto :goto_4

    :cond_11
    iget-object v7, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mMultipleCertSetString:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_12

    goto :goto_4

    :cond_12
    iget-object v7, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    new-array v3, v3, [Ljava/lang/String;

    aput-object v6, v3, v2

    invoke-virtual {v7, v3}, Landroid/net/wifi/WifiEnterpriseConfig;->setCaCertificateAliases([Ljava/lang/String;)V

    :cond_13
    :goto_4
    iget-object v2, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v2}, Landroid/net/wifi/WifiEnterpriseConfig;->getCaCertificateAliases()[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_14

    iget-object v2, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v2}, Landroid/net/wifi/WifiEnterpriseConfig;->getCaPath()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_14

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ca_cert ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v3}, Landroid/net/wifi/WifiEnterpriseConfig;->getCaCertificateAliases()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, ") and ca_path ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v3}, Landroid/net/wifi/WifiEnterpriseConfig;->getCaPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ") should not both be non-null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_14
    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapUserCertSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mUnspecifiedCertString:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const-string v4, ""

    if-nez v3, :cond_15

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mDoNotProvideEapUserCertString:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_16

    :cond_15
    move-object v2, v4

    :cond_16
    iget-object v3, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v3, v2}, Landroid/net/wifi/WifiEnterpriseConfig;->setClientCertificateAlias(Ljava/lang/String;)V

    if-eq v1, v11, :cond_19

    if-eq v1, v9, :cond_19

    if-ne v1, v10, :cond_17

    goto :goto_5

    :cond_17
    if-ne v1, v5, :cond_18

    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapIdentityView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiEnterpriseConfig;->setIdentity(Ljava/lang/String;)V

    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v1, v4}, Landroid/net/wifi/WifiEnterpriseConfig;->setAnonymousIdentity(Ljava/lang/String;)V

    goto :goto_6

    :cond_18
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapIdentityView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiEnterpriseConfig;->setIdentity(Ljava/lang/String;)V

    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapAnonymousView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiEnterpriseConfig;->setAnonymousIdentity(Ljava/lang/String;)V

    goto :goto_6

    :cond_19
    :goto_5
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v1, v4}, Landroid/net/wifi/WifiEnterpriseConfig;->setIdentity(Ljava/lang/String;)V

    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v1, v4}, Landroid/net/wifi/WifiEnterpriseConfig;->setAnonymousIdentity(Ljava/lang/String;)V

    :goto_6
    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->isShown()Z

    move-result v1

    if-eqz v1, :cond_1a

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->length()I

    move-result v1

    if-lez v1, :cond_22

    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiEnterpriseConfig;->setPassword(Ljava/lang/String;)V

    goto/16 :goto_8

    :cond_1a
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiEnterpriseConfig;->setPassword(Ljava/lang/String;)V

    goto/16 :goto_8

    :cond_1b
    if-ne v1, v7, :cond_1c

    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    goto :goto_7

    :cond_1c
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    iput-boolean v3, v0, Landroid/net/wifi/WifiConfiguration;->requirePmf:Z

    :goto_7
    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->length()I

    move-result v1

    if-eqz v1, :cond_22

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "[0-9A-Fa-f]{64}"

    invoke-virtual {v1, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1d

    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    goto :goto_8

    :cond_1d
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    goto :goto_8

    :cond_1e
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->length()I

    move-result v1

    if-eqz v1, :cond_22

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->length()I

    move-result v1

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0xa

    if-eq v1, v4, :cond_1f

    const/16 v4, 0x1a

    if-eq v1, v4, :cond_1f

    const/16 v4, 0x20

    if-ne v1, v4, :cond_20

    :cond_1f
    const-string v1, "[0-9A-Fa-f]*"

    invoke-virtual {v3, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_20

    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    aput-object v3, v1, v2

    goto :goto_8

    :cond_20
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    goto :goto_8

    :cond_21
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    :cond_22
    :goto_8
    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mLPrivacySettingsSpinner:Lmiuix/appcompat/widget/Spinner;

    if-eqz v1, :cond_23

    iget v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mAccessPointSecurity:I

    if-ne v2, v5, :cond_23

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result p0

    invoke-static {p0}, Lcom/android/settings/wifi/details2/WifiPrivacyPreferenceController2;->translatePrefValueToMacRandomizedValue(I)I

    move-result p0

    iput p0, v0, Landroid/net/wifi/WifiConfiguration;->macRandomizationSetting:I

    goto :goto_9

    :cond_23
    iget-object p0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPrivacySettingsSpinner:Lmiuix/appcompat/widget/Spinner;

    if-eqz p0, :cond_24

    invoke-virtual {p0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result p0

    invoke-static {p0}, Lcom/android/settings/wifi/details2/WifiPrivacyPreferenceController2;->translatePrefValueToMacRandomizedValue(I)I

    move-result p0

    iput p0, v0, Landroid/net/wifi/WifiConfiguration;->macRandomizationSetting:I

    :cond_24
    :goto_9
    return-object v0
.end method

.method private getSIMInfo()V
    .locals 7

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    iget-object v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimCount()I

    move-result v3

    if-ge v2, v3, :cond_1

    invoke-virtual {v0, v2}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoForSimSlotIndex(I)Landroid/telephony/SubscriptionInfo;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/telephony/SubscriptionInfo;->getDisplayName()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_0
    iget-object v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    sget v4, Lcom/android/settings/R$string;->sim_editor_title:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    add-int/lit8 v6, v2, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :goto_1
    iget-object v4, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSimDisplayNames:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private init()V
    .locals 8

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mAccessPointSecurity:I

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->ssid:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSsidView:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$integer;->wifi_max_ssid_length:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mMaxSsidLength:I

    if-lez v1, :cond_0

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSsidView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setMaxEms(I)V

    :cond_0
    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->type:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mTextViewChangedHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->password:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSimDisplayNames:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    sget v2, Lcom/android/settings/R$string;->wifi_unspecified:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mUnspecifiedCertString:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    sget v2, Lcom/android/settings/R$string;->wifi_multiple_cert_added:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mMultipleCertSetString:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    sget v2, Lcom/android/settings/R$string;->wifi_use_system_certs:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mUseSystemCertsString:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    sget v2, Lcom/android/settings/R$string;->wifi_do_not_provide_eap_user_cert:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mDoNotProvideEapUserCertString:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    sget v2, Lcom/android/settings/R$string;->wifi_do_not_validate_eap_server:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mDoNotValidateEapServerString:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    sget v2, Lcom/android/settings/R$string;->wifi_install_credentials:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mInstallCertsString:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->security:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/widget/Spinner;

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSecuritySpinner:Lmiuix/appcompat/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "wifi"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWpa3SaeSupported()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$array;->add_wifi_security_with_sae:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$array;->add_wifi_security:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-static {}, Lcom/android/settings/MiuiUtils;->getInstance()Lcom/android/settings/MiuiUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSecuritySpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v2, v3, v1, v4}, Lcom/android/settings/MiuiUtils;->setSpinnerAdapter(Landroid/content/Context;[Ljava/lang/String;Lmiuix/appcompat/widget/Spinner;)V

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSecuritySpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSecuritySpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSecuritySpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1}, Lmiuix/appcompat/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiuix/appcompat/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->sim_card:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/widget/Spinner;

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSimCardSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->phase2:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/widget/Spinner;

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPhase2Spinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1}, Lmiuix/appcompat/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiuix/appcompat/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->ca_cert:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/widget/Spinner;

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapCaCertSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1}, Lmiuix/appcompat/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiuix/appcompat/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->user_cert:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/widget/Spinner;

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapUserCertSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1}, Lmiuix/appcompat/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiuix/appcompat/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->identity:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapIdentityView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->anonymous:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapAnonymousView:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    sget v3, Lcom/android/settings/R$layout;->miuix_appcompat_simple_spinner_layout_integrated:I

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/android/settings/R$array;->wifi_peap_phase2_entries:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    const v5, 0x1020014

    invoke-direct {v1, v2, v3, v5, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPhase2PeapAdapter:Landroid/widget/ArrayAdapter;

    sget v2, Lcom/android/settings/R$layout;->miuix_appcompat_simple_spinner_dropdown_item:I

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/android/settings/R$array;->wifi_ttls_phase2_entries:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v4, v3, v5, v6}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPhase2TtlsAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->privacy_settings:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/widget/Spinner;

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPrivacySettingsSpinner:Lmiuix/appcompat/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->l_privacy_settings:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/widget/Spinner;

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mLPrivacySettingsSpinner:Lmiuix/appcompat/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$array;->wifi_privacy_entries:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/android/settings/MiuiUtils;->getInstance()Lcom/android/settings/MiuiUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mLPrivacySettingsSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v2, v3, v1, v4}, Lcom/android/settings/MiuiUtils;->setSpinnerAdapter(Landroid/content/Context;[Ljava/lang/String;Lmiuix/appcompat/widget/Spinner;)V

    invoke-static {}, Lcom/android/settings/MiuiUtils;->getInstance()Lcom/android/settings/MiuiUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPrivacySettingsSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v2, v3, v1, v4}, Lcom/android/settings/MiuiUtils;->setSpinnerAdapter(Landroid/content/Context;[Ljava/lang/String;Lmiuix/appcompat/widget/Spinner;)V

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->proxy_settings_fields:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mProxySettingsSpinnerParent:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->proxy_settings:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/widget/Spinner;

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mProxySettingsSpinner:Lmiuix/appcompat/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->hidden_settings:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/widget/Spinner;

    iput-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mHiddenSettingsSpinner:Lmiuix/appcompat/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/settings/R$array;->wifi_hidden_entries:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/android/settings/MiuiUtils;->getInstance()Lcom/android/settings/MiuiUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mHiddenSettingsSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v2, v3, v1, v4}, Lcom/android/settings/MiuiUtils;->setSpinnerAdapter(Landroid/content/Context;[Ljava/lang/String;Lmiuix/appcompat/widget/Spinner;)V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mHiddenSettingsSpinner:Lmiuix/appcompat/widget/Spinner;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setSelection(I)V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v3, Lcom/android/settings/R$id;->hidden_gbk_fields:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mHiddenGbkSpinnerParent:Landroid/widget/LinearLayout;

    invoke-static {}, Lcom/android/wifitrackerlib/WifiEntry;->isGbkSsidSupported()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {}, Lmiui/os/Build;->getRegion()Ljava/lang/String;

    move-result-object v2

    const-string v3, "CN"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mHiddenSettingsSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v2, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mHiddenGbkSpinnerParent:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v3, Lcom/android/settings/R$id;->hidden_gbk_settings:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lmiuix/appcompat/widget/Spinner;

    iput-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mHiddenGbkSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-static {}, Lcom/android/settings/MiuiUtils;->getInstance()Lcom/android/settings/MiuiUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mHiddenGbkSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v2, v3, v1, v4}, Lcom/android/settings/MiuiUtils;->setSpinnerAdapter(Landroid/content/Context;[Ljava/lang/String;Lmiuix/appcompat/widget/Spinner;)V

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mHiddenGbkSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mHiddenGbkSpinnerParent:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_1
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->ip_fields:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mIpFieldsSpinnerParent:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->ip_settings:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mIpFieldsSpinner:Lmiuix/appcompat/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settingslib/wifi/WifiUtils;->getDefaultWifiPrivacy(Landroid/content/Context;)I

    move-result v0

    invoke-static {v0}, Lcom/android/settings/wifi/details2/WifiPrivacyPreferenceController2;->translateMacRandomizedValueToPrefValue(I)I

    move-result v0

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPrivacySettingsSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mLPrivacySettingsSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->enableSubmitIfAppropriate()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/android/settings/R$color;->bg_spinner_parent:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getColor(I)I

    move-result v0

    invoke-static {}, Lcom/android/settings/MiuiUtils;->getInstance()Lcom/android/settings/MiuiUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v3, Lcom/android/settings/R$id;->method:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/MiuiUtils;->setSpinnerDisplayLocation(Lmiuix/appcompat/widget/Spinner;I)V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSecuritySpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/MiuiUtils;->setSpinnerDisplayLocation(Lmiuix/appcompat/widget/Spinner;I)V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSimCardSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/MiuiUtils;->setSpinnerDisplayLocation(Lmiuix/appcompat/widget/Spinner;I)V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPhase2Spinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/MiuiUtils;->setSpinnerDisplayLocation(Lmiuix/appcompat/widget/Spinner;I)V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapCaCertSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/MiuiUtils;->setSpinnerDisplayLocation(Lmiuix/appcompat/widget/Spinner;I)V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapUserCertSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/MiuiUtils;->setSpinnerDisplayLocation(Lmiuix/appcompat/widget/Spinner;I)V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPrivacySettingsSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/MiuiUtils;->setSpinnerDisplayLocation(Lmiuix/appcompat/widget/Spinner;I)V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mLPrivacySettingsSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/MiuiUtils;->setSpinnerDisplayLocation(Lmiuix/appcompat/widget/Spinner;I)V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mProxySettingsSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/MiuiUtils;->setSpinnerDisplayLocation(Lmiuix/appcompat/widget/Spinner;I)V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mIpFieldsSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/MiuiUtils;->setSpinnerDisplayLocation(Lmiuix/appcompat/widget/Spinner;I)V

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mHiddenSettingsSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1, v2, v0}, Lcom/android/settings/MiuiUtils;->setSpinnerDisplayLocation(Lmiuix/appcompat/widget/Spinner;I)V

    iget-object p0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mHiddenGbkSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1, p0, v0}, Lcom/android/settings/MiuiUtils;->setSpinnerDisplayLocation(Lmiuix/appcompat/widget/Spinner;I)V

    return-void
.end method

.method private static synthetic lambda$adaptSoftInput$0(Landroid/view/View;)V
    .locals 3

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    iget v0, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v0

    add-int/lit8 v1, v1, 0x50

    const/4 v0, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    if-eq v2, v1, :cond_1

    invoke-virtual {p0, v0, v0, v0, v1}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, v0, v0, v0, v0}, Landroid/view/View;->setPadding(IIII)V

    :cond_1
    :goto_0
    return-void
.end method

.method private static synthetic lambda$getConfig$1(I)Z
    .locals 1

    const/16 v0, 0x80

    if-ge p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static synthetic lambda$loadCertificates$2(Ljava/lang/String;)Z
    .locals 5

    sget-object v0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->UNDESIRED_CERTIFICATES:[Ljava/lang/String;

    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_1

    aget-object v4, v0, v3

    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    return v2

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x1

    return p0
.end method

.method private loadCertificates(Lmiuix/appcompat/widget/Spinner;Ljava/util/Collection;Ljava/lang/String;ZZ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiuix/appcompat/widget/Spinner;",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "ZZ)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/widget/Spinner;->getContext()Landroid/content/Context;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mUnspecifiedCertString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p4, :cond_0

    iget-object p4, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mMultipleCertSetString:Ljava/lang/String;

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz p5, :cond_1

    iget-object p4, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mUseSystemCertsString:Ljava/lang/String;

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object p4, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mInstallCertsString:Ljava/lang/String;

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result p4

    if-eqz p4, :cond_2

    invoke-interface {p2}, Ljava/util/Collection;->stream()Ljava/util/stream/Stream;

    move-result-object p2

    new-instance p4, Lcom/android/settings/wifi/MiuiAddNetworkFragment$$ExternalSyntheticLambda2;

    invoke-direct {p4}, Lcom/android/settings/wifi/MiuiAddNetworkFragment$$ExternalSyntheticLambda2;-><init>()V

    invoke-interface {p2, p4}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object p2

    invoke-static {}, Ljava/util/stream/Collectors;->toList()Ljava/util/stream/Collector;

    move-result-object p4

    invoke-interface {p2, p4}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/Collection;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_2
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_3

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-static {}, Lcom/android/settings/MiuiUtils;->getInstance()Lcom/android/settings/MiuiUtils;

    move-result-object p2

    iget-object p0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p3

    new-array p3, p3, [Ljava/lang/String;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p3

    check-cast p3, [Ljava/lang/String;

    invoke-virtual {p2, p0, p3, p1}, Lcom/android/settings/MiuiUtils;->setSpinnerAdapter(Landroid/content/Context;[Ljava/lang/String;Lmiuix/appcompat/widget/Spinner;)V

    return-void
.end method

.method private mapPositionToSecurityType(I)V
    .locals 1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const-string p1, "MiuiAddNetworkFragment"

    const-string v0, "Position out of range, unsupport security type."

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x0

    iput p1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mAccessPointSecurity:I

    goto :goto_0

    :cond_0
    const/4 p1, 0x5

    iput p1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mAccessPointSecurity:I

    goto :goto_0

    :cond_1
    iput p1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mAccessPointSecurity:I

    :goto_0
    return-void
.end method

.method private setAnonymousIdentInvisible()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->l_anonymous:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object p0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapAnonymousView:Landroid/widget/TextView;

    const-string v0, ""

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private setCaCertInvisible()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->l_ca_cert:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapCaCertSpinner:Lmiuix/appcompat/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mUnspecifiedCertString:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setSelection(Lmiuix/appcompat/widget/Spinner;Ljava/lang/String;)V

    return-void
.end method

.method private setDomainInvisible()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->l_domain:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object p0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapDomainView:Landroid/widget/TextView;

    const-string v0, ""

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private setIdentityInvisible()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->l_identity:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object p0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPhase2Spinner:Lmiuix/appcompat/widget/Spinner;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/Spinner;->setSelection(I)V

    return-void
.end method

.method private setPasswordEapInvisible()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPasswordView:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->l_password_layout:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private setPasswordEapVisible()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPasswordView:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->l_password_layout:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private setPasswordInVisible()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPasswordView:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->password_layout:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private setPasswordVisible()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPasswordView:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->password_layout:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private setPhase2Invisible()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->l_phase2:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object p0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPhase2Spinner:Lmiuix/appcompat/widget/Spinner;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/Spinner;->setSelection(I)V

    return-void
.end method

.method private setSelection(Lmiuix/appcompat/widget/Spinner;Ljava/lang/String;)V
    .locals 2

    if-eqz p2, :cond_1

    invoke-virtual {p1}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object p0

    check-cast p0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    invoke-virtual {p0, v0}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method private setSimCardInvisible()V
    .locals 1

    iget-object p0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->l_sim_card:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private setUserCertInvisible()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->l_user_cert:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapUserCertSpinner:Lmiuix/appcompat/widget/Spinner;

    iget-object v1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mUnspecifiedCertString:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setSelection(Lmiuix/appcompat/widget/Spinner;Ljava/lang/String;)V

    return-void
.end method

.method private showEapFieldsByMethod(I)V
    .locals 5

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->l_method:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->l_identity:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->l_domain:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->l_ca_cert:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setPasswordInVisible()V

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    invoke-static {}, Lcom/android/settings/MiuiUtils;->getInstance()Lcom/android/settings/MiuiUtils;

    move-result-object p1

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSimDisplayNames:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    iget-object v4, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSimCardSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {p1, v0, v3, v4}, Lcom/android/settings/MiuiUtils;->setSpinnerAdapter(Landroid/content/Context;[Ljava/lang/String;Lmiuix/appcompat/widget/Spinner;)V

    iget-object p1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->l_sim_card:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setPhase2Invisible()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setAnonymousIdentInvisible()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setCaCertInvisible()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setDomainInvisible()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setUserCertInvisible()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setIdentityInvisible()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setPasswordEapInvisible()V

    goto/16 :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setPhase2Invisible()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setCaCertInvisible()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setDomainInvisible()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setAnonymousIdentInvisible()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setUserCertInvisible()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setSimCardInvisible()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setPasswordEapVisible()V

    goto :goto_0

    :pswitch_2
    iget-object p1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPhase2Adapter:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPhase2TtlsAdapter:Landroid/widget/ArrayAdapter;

    if-eq p1, v0, :cond_0

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPhase2Adapter:Landroid/widget/ArrayAdapter;

    iget-object p1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPhase2Spinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {p1, v0}, Lmiuix/appcompat/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    :cond_0
    iget-object p1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->l_phase2:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->l_anonymous:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setUserCertInvisible()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setSimCardInvisible()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setPasswordEapVisible()V

    goto :goto_0

    :pswitch_3
    iget-object p1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->l_user_cert:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setPhase2Invisible()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setAnonymousIdentInvisible()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setPasswordEapInvisible()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setSimCardInvisible()V

    goto :goto_0

    :pswitch_4
    iget-object p1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPhase2Adapter:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPhase2PeapAdapter:Landroid/widget/ArrayAdapter;

    if-eq p1, v0, :cond_1

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPhase2Adapter:Landroid/widget/ArrayAdapter;

    iget-object p1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPhase2Spinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {p1, v0}, Lmiuix/appcompat/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    :cond_1
    iget-object p1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->l_phase2:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->l_anonymous:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->showPeapFields()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setUserCertInvisible()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setSimCardInvisible()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setPasswordEapVisible()V

    :goto_0
    iget-object p1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    const/16 v0, 0x8

    if-eq p1, v0, :cond_3

    iget-object p1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapCaCertSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {p1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mDoNotValidateEapServerString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mUnspecifiedCertString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setDomainInvisible()V

    :cond_3
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private showLPrivacySettingsFields(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isConnectedMacRandomizationSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->l_privacy_settings_fields:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method private showPeapFields()V
    .locals 2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPhase2Spinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->l_identity:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object p0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->l_anonymous:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    invoke-virtual {p0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapIdentityView:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->l_identity:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    return-void
.end method

.method private showPrivacySettingsFields(Z)V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isConnectedMacRandomizationSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v0, Lcom/android/settings/R$id;->privacy_settings_fields:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method private showSecurityFields()V
    .locals 14

    iget v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mAccessPointSecurity:I

    const/16 v1, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v4, Lcom/android/settings/R$id;->security_fields:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, v2}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->showPrivacySettingsFields(Z)V

    invoke-direct {p0, v3}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->showLPrivacySettingsFields(Z)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v4, Lcom/android/settings/R$id;->security_fields:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mAccessPointSecurity:I

    const/4 v4, 0x3

    if-eq v0, v4, :cond_1

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v4, Lcom/android/settings/R$id;->password:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->setPasswordVisible()V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v4, Lcom/android/settings/R$id;->eap:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, v2}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->showPrivacySettingsFields(Z)V

    invoke-direct {p0, v3}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->showLPrivacySettingsFields(Z)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->eap:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, v3}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->showPrivacySettingsFields(Z)V

    invoke-direct {p0, v2}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->showLPrivacySettingsFields(Z)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->l_password:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapMethodSpinner:Lmiuix/appcompat/widget/Spinner;

    if-nez v0, :cond_4

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->getSIMInfo()V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->method:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapMethodSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/settings/R$array;->wifi_eap_method:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/android/settings/MiuiUtils;->getInstance()Lcom/android/settings/MiuiUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapMethodSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1, v2, v0, v3}, Lcom/android/settings/MiuiUtils;->setSpinnerAdapter(Landroid/content/Context;[Ljava/lang/String;Lmiuix/appcompat/widget/Spinner;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settingslib/Utils;->isWifiOnly(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1110127

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/android/settings/R$array;->eap_method_without_sim_auth:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/android/settings/MiuiUtils;->getInstance()Lcom/android/settings/MiuiUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapMethodSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v1, v2, v0, v3}, Lcom/android/settings/MiuiUtils;->setSpinnerAdapter(Landroid/content/Context;[Ljava/lang/String;Lmiuix/appcompat/widget/Spinner;)V

    :cond_3
    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapMethodSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v0}, Lmiuix/appcompat/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiuix/appcompat/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v1, Lcom/android/settings/R$id;->phase2:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPhase2Spinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v0}, Lmiuix/appcompat/widget/Spinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmiuix/appcompat/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->ca_cert:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapCaCertSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->domain:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapDomainView:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->user_cert:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapUserCertSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->sim_card:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSimCardSpinner:Lmiuix/appcompat/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->identity:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapIdentityView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    sget v2, Lcom/android/settings/R$id;->anonymous:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapAnonymousView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSimCardSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->getAndroidKeystoreAliasLoader()Lcom/android/settings/utils/AndroidKeystoreAliasLoader;

    move-result-object v0

    iget-object v3, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapCaCertSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v0}, Lcom/android/settings/utils/AndroidKeystoreAliasLoader;->getCaCertAliases()Ljava/util/Collection;

    move-result-object v4

    iget-object v5, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mDoNotValidateEapServerString:Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->loadCertificates(Lmiuix/appcompat/widget/Spinner;Ljava/util/Collection;Ljava/lang/String;ZZ)V

    iget-object v9, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapUserCertSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v0}, Lcom/android/settings/utils/AndroidKeystoreAliasLoader;->getKeyCertAliases()Ljava/util/Collection;

    move-result-object v10

    iget-object v11, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mDoNotProvideEapUserCertString:Ljava/lang/String;

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v8, p0

    invoke-direct/range {v8 .. v13}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->loadCertificates(Lmiuix/appcompat/widget/Spinner;Ljava/util/Collection;Ljava/lang/String;ZZ)V

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/widget/Spinner;

    iput-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPhase2Spinner:Lmiuix/appcompat/widget/Spinner;

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapMethodSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->showEapFieldsByMethod(I)V

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->showEapFieldsByMethod(I)V

    :goto_0
    return-void
.end method

.method private startActivityForInstallCerts()V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.credentials.INSTALL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.android.certinstaller"

    const-string v3, "com.android.certinstaller.CertInstallerMain"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v1, "certificate_install_usage"

    const-string/jumbo v2, "wifi"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object p0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    iget-object p1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mTextViewChangedHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/settings/wifi/MiuiAddNetworkFragment$1;

    invoke-direct {v0, p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment$1;-><init>(Lcom/android/settings/wifi/MiuiAddNetworkFragment;)V

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget p1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mMaxSsidLength:I

    if-lez p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSsidView:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->checkSsidLength()V

    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    sget v0, Lcom/android/settings/R$string;->manually_add_network:I

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->init()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/android/settings/utils/SettingsFeatures;->isSplitTablet(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Landroid/view/Window;->setSoftInputMode(I)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->adaptSoftInput(Landroid/app/Activity;)V

    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result p1

    sget v0, Lcom/android/settings/R$id;->show_password:I

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getSelectionEnd()I

    move-result p1

    iget-object v0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPasswordView:Landroid/widget/TextView;

    if-eqz p2, :cond_0

    const/16 p2, 0x90

    goto :goto_0

    :cond_0
    const/16 p2, 0x80

    :goto_0
    or-int/lit8 p2, p2, 0x1

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setInputType(I)V

    if-ltz p1, :cond_1

    iget-object p0, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mPasswordView:Landroid/widget/TextView;

    check-cast p0, Landroid/widget/EditText;

    invoke-virtual {p0, p1}, Landroid/widget/EditText;->setSelection(I)V

    :cond_1
    return-void
.end method

.method public onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 0

    invoke-super {p0, p1, p2, p3}, Lcom/android/settings/BaseFragment;->onInflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    sget p0, Lcom/android/settings/R$layout;->wifi_add_network_layout:I

    const/4 p3, 0x0

    invoke-virtual {p1, p0, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object p2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSecuritySpinner:Lmiuix/appcompat/widget/Spinner;

    if-ne p1, p2, :cond_0

    invoke-direct {p0, p3}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mapPositionToSecurityType(I)V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->showSecurityFields()V

    goto :goto_2

    :cond_0
    iget-object p2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapMethodSpinner:Lmiuix/appcompat/widget/Spinner;

    if-eq p1, p2, :cond_4

    iget-object p2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mEapCaCertSpinner:Lmiuix/appcompat/widget/Spinner;

    if-ne p1, p2, :cond_1

    goto :goto_1

    :cond_1
    iget-object p2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mSimCardSpinner:Lmiuix/appcompat/widget/Spinner;

    if-ne p1, p2, :cond_2

    goto :goto_2

    :cond_2
    iget-object p2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mHiddenSettingsSpinner:Lmiuix/appcompat/widget/Spinner;

    if-ne p1, p2, :cond_6

    invoke-static {}, Lcom/android/wifitrackerlib/WifiEntry;->isGbkSsidSupported()Z

    move-result p1

    if-eqz p1, :cond_6

    iget-object p1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mHiddenGbkSpinnerParent:Landroid/widget/LinearLayout;

    iget-object p2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mHiddenSettingsSpinner:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {p2}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result p2

    if-nez p2, :cond_3

    const/16 p2, 0x8

    goto :goto_0

    :cond_3
    const/4 p2, 0x0

    :goto_0
    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_2

    :cond_4
    :goto_1
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mInstallCertsString:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->startActivityForInstallCerts()V

    :cond_5
    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->showSecurityFields()V

    :cond_6
    :goto_2
    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->enableSubmitIfAppropriate()V

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;)V"
        }
    .end annotation

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lmiuix/appcompat/app/Fragment;->onResume()V

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->enableSubmitIfAppropriate()V

    return-void
.end method

.method public onSave()V
    .locals 3

    invoke-direct {p0}, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->getConfig()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "config"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v1}, Lcom/android/settings/BaseEditFragment;->onSave(Landroid/os/Bundle;)V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/android/settings/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iput-object p1, p0, Lcom/android/settings/wifi/MiuiAddNetworkFragment;->mView:Landroid/view/View;

    const-string/jumbo p0, "wifi_add_network"

    const/4 p1, 0x0

    invoke-static {p0, p1}, Lcom/android/settingslib/util/OneTrackInterfaceUtils;->track(Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method
