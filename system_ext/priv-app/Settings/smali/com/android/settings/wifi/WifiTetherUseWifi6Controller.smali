.class public Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;
.super Ljava/lang/Object;

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/settingslib/core/lifecycle/LifecycleObserver;
.implements Lcom/android/settingslib/core/lifecycle/events/OnResume;
.implements Lcom/android/settingslib/core/lifecycle/events/OnPause;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/wifi/WifiTetherUseWifi6Controller$RestartWifiAp;
    }
.end annotation


# instance fields
.field private mCm:Landroid/net/ConnectivityManager;

.field private mContext:Landroid/content/Context;

.field private mRestartWifiAp:Lcom/android/settings/wifi/WifiTetherUseWifi6Controller$RestartWifiAp;

.field private mTetherUseWifi6:Landroidx/preference/CheckBoxPreference;

.field private mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmTetherUseWifi6(Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;)Landroidx/preference/CheckBoxPreference;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;->mTetherUseWifi6:Landroidx/preference/CheckBoxPreference;

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/settingslib/core/lifecycle/Lifecycle;Landroidx/preference/Preference;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;->mContext:Landroid/content/Context;

    check-cast p3, Landroidx/preference/CheckBoxPreference;

    iput-object p3, p0, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;->mTetherUseWifi6:Landroidx/preference/CheckBoxPreference;

    const-string p3, "connectivity"

    invoke-virtual {p1, p3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/ConnectivityManager;

    iput-object p1, p0, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;->mCm:Landroid/net/ConnectivityManager;

    iget-object p1, p0, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;->mContext:Landroid/content/Context;

    const-string/jumbo p3, "wifi"

    invoke-virtual {p1, p3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/wifi/WifiManager;

    iput-object p1, p0, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {p2, p0}, Lcom/android/settingslib/core/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    return-void
.end method


# virtual methods
.method public onPause()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;->mTetherUseWifi6:Landroidx/preference/CheckBoxPreference;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;->mRestartWifiAp:Lcom/android/settings/wifi/WifiTetherUseWifi6Controller$RestartWifiAp;

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/AsyncTask;->cancel(Z)Z

    iput-object v1, p0, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;->mRestartWifiAp:Lcom/android/settings/wifi/WifiTetherUseWifi6Controller$RestartWifiAp;

    :cond_1
    return-void
.end method

.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iget-object p2, p0, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;->mContext:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p2

    const-string v0, "hotspot_80211ax_support"

    const/4 v1, -0x2

    invoke-static {p2, v0, p1, v1}, Landroid/provider/MiuiSettings$System;->putBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    iget-object p1, p0, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;->mTetherUseWifi6:Landroidx/preference/CheckBoxPreference;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;->mCm:Landroid/net/ConnectivityManager;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {p1}, Landroid/net/wifi/WifiManager;->getWifiApState()I

    move-result p1

    const/16 p2, 0xd

    if-ne p1, p2, :cond_0

    iget-object p1, p0, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;->mTetherUseWifi6:Landroidx/preference/CheckBoxPreference;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroidx/preference/Preference;->setEnabled(Z)V

    iget-object p1, p0, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;->mCm:Landroid/net/ConnectivityManager;

    invoke-virtual {p1, p2}, Landroid/net/ConnectivityManager;->stopTethering(I)V

    new-instance p1, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller$RestartWifiAp;

    iget-object v0, p0, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;->mContext:Landroid/content/Context;

    invoke-direct {p1, p0, v0}, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller$RestartWifiAp;-><init>(Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;Landroid/content/Context;)V

    iput-object p1, p0, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;->mRestartWifiAp:Lcom/android/settings/wifi/WifiTetherUseWifi6Controller$RestartWifiAp;

    new-array p0, p2, [Ljava/lang/Void;

    invoke-virtual {p1, p0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    const/4 p0, 0x1

    return p0
.end method

.method public onResume()V
    .locals 1

    iget-object v0, p0, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;->mTetherUseWifi6:Landroidx/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;->updateState()V

    iget-object v0, p0, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;->mTetherUseWifi6:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroidx/preference/Preference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    :cond_0
    return-void
.end method

.method public updateState()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "hotspot_80211ax_support"

    const/4 v2, 0x0

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v0

    iget-object v1, p0, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;->mTetherUseWifi6:Landroidx/preference/CheckBoxPreference;

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroidx/preference/Preference;->setEnabled(Z)V

    iget-object p0, p0, Lcom/android/settings/wifi/WifiTetherUseWifi6Controller;->mTetherUseWifi6:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p0, v0}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    :cond_0
    return-void
.end method
