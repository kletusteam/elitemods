.class public Lcom/android/settings/wifi/WifiDetailInfoBean;
.super Ljava/lang/Object;


# instance fields
.field private iconNameId:I

.field private summary:Ljava/lang/String;

.field private titleId:I


# direct methods
.method public constructor <init>(IILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/settings/wifi/WifiDetailInfoBean;->iconNameId:I

    iput p2, p0, Lcom/android/settings/wifi/WifiDetailInfoBean;->titleId:I

    iput-object p3, p0, Lcom/android/settings/wifi/WifiDetailInfoBean;->summary:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getIconNameId()I
    .locals 0

    iget p0, p0, Lcom/android/settings/wifi/WifiDetailInfoBean;->iconNameId:I

    return p0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/wifi/WifiDetailInfoBean;->summary:Ljava/lang/String;

    return-object p0
.end method

.method public getTitleId()I
    .locals 0

    iget p0, p0, Lcom/android/settings/wifi/WifiDetailInfoBean;->titleId:I

    return p0
.end method
