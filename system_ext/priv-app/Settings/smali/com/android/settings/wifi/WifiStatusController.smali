.class public Lcom/android/settings/wifi/WifiStatusController;
.super Lcom/android/settings/BaseSettingsController;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mHasRegister:Z

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mNetworkConnected:Z

.field private mNwIntentFilter:Landroid/content/IntentFilter;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mStatusViewText:Ljava/lang/String;

.field private mVerbose:Z

.field private mWifiEnabled:Z

.field private mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmNetworkConnected(Lcom/android/settings/wifi/WifiStatusController;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/wifi/WifiStatusController;->mNetworkConnected:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmStatusViewText(Lcom/android/settings/wifi/WifiStatusController;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/wifi/WifiStatusController;->mStatusViewText:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmVerbose(Lcom/android/settings/wifi/WifiStatusController;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/wifi/WifiStatusController;->mVerbose:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmWifiEnabled(Lcom/android/settings/wifi/WifiStatusController;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/settings/wifi/WifiStatusController;->mWifiEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmNetworkConnected(Lcom/android/settings/wifi/WifiStatusController;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/wifi/WifiStatusController;->mNetworkConnected:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmStatusViewText(Lcom/android/settings/wifi/WifiStatusController;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wifi/WifiStatusController;->mStatusViewText:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmWifiEnabled(Lcom/android/settings/wifi/WifiStatusController;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/android/settings/wifi/WifiStatusController;->mWifiEnabled:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetWifiState(Lcom/android/settings/wifi/WifiStatusController;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/wifi/WifiStatusController;->getWifiState()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/TextView;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/android/settings/BaseSettingsController;-><init>(Landroid/content/Context;Landroid/widget/TextView;)V

    const-string p2, ""

    iput-object p2, p0, Lcom/android/settings/wifi/WifiStatusController;->mStatusViewText:Ljava/lang/String;

    new-instance p2, Lcom/android/settings/wifi/WifiStatusController$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p2, p0, v0}, Lcom/android/settings/wifi/WifiStatusController$1;-><init>(Lcom/android/settings/wifi/WifiStatusController;Landroid/os/Looper;)V

    iput-object p2, p0, Lcom/android/settings/wifi/WifiStatusController;->mHandler:Landroid/os/Handler;

    const/4 p2, 0x0

    iput-boolean p2, p0, Lcom/android/settings/wifi/WifiStatusController;->mWifiEnabled:Z

    iput-boolean p2, p0, Lcom/android/settings/wifi/WifiStatusController;->mNetworkConnected:Z

    new-instance p2, Lcom/android/settings/wifi/WifiStatusController$2;

    invoke-direct {p2, p0}, Lcom/android/settings/wifi/WifiStatusController$2;-><init>(Lcom/android/settings/wifi/WifiStatusController;)V

    iput-object p2, p0, Lcom/android/settings/wifi/WifiStatusController;->mReceiver:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/android/settings/wifi/WifiStatusController;->mContext:Landroid/content/Context;

    const-string/jumbo p2, "wifi"

    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/net/wifi/WifiManager;

    iput-object p1, p0, Lcom/android/settings/wifi/WifiStatusController;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {p1}, Landroid/net/wifi/WifiManager;->isVerboseLoggingEnabled()Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/settings/wifi/WifiStatusController;->mVerbose:Z

    new-instance p1, Landroid/content/IntentFilter;

    invoke-direct {p1}, Landroid/content/IntentFilter;-><init>()V

    iput-object p1, p0, Lcom/android/settings/wifi/WifiStatusController;->mIntentFilter:Landroid/content/IntentFilter;

    const-string p2, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {p1, p2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/settings/wifi/WifiStatusController;->mIntentFilter:Landroid/content/IntentFilter;

    const-string p2, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {p1, p2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance p1, Landroid/content/IntentFilter;

    invoke-direct {p1}, Landroid/content/IntentFilter;-><init>()V

    iput-object p1, p0, Lcom/android/settings/wifi/WifiStatusController;->mNwIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p1, p2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/android/settings/wifi/WifiStatusController;->wakeUpWifiTrackerInjector()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/wifi/WifiStatusController;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/BaseSettingsController;->mStatusView:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$100(Lcom/android/settings/wifi/WifiStatusController;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/BaseSettingsController;->mStatusView:Landroid/widget/TextView;

    return-object p0
.end method

.method private getWifiState()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/android/settings/BaseSettingsController;->mStatusView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string p0, ""

    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/WifiStatusController;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    const-string v1, "WifiStatusController"

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/settings/wifi/WifiStatusController;->mVerbose:Z

    if-eqz v0, :cond_1

    const-string v0, "WiFi is off."

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object p0, p0, Lcom/android/settings/wifi/WifiStatusController;->mContext:Landroid/content/Context;

    sget v0, Lcom/android/settings/R$string;->wireless_off:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_2
    iget-object v0, p0, Lcom/android/settings/wifi/WifiStatusController;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/settings/wifi/WifiStatusController;->mNwIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/android/settings/wifi/WifiStatusController;->mVerbose:Z

    if-eqz v0, :cond_3

    const-string v0, "Can\'t get network state intent"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object p0, p0, Lcom/android/settings/wifi/WifiStatusController;->mContext:Landroid/content/Context;

    sget v0, Lcom/android/settings/R$string;->wireless_on:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_4
    const-string v2, "networkInfo"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-nez v0, :cond_5

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/android/settings/wifi/WifiStatusController;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getIpAddress()I

    move-result v1

    if-eqz v1, :cond_9

    sget-boolean v1, Lcom/android/settings/utils/TabletUtils;->IS_TABLET:Z

    if-eqz v1, :cond_6

    iget-object p0, p0, Lcom/android/settings/wifi/WifiStatusController;->mContext:Landroid/content/Context;

    sget v0, Lcom/android/settings/R$string;->wireless_connected:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_6
    invoke-static {}, Lcom/android/wifitrackerlib/WifiEntry;->isGbkSsidSupported()Z

    move-result p0

    if-eqz p0, :cond_7

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/android/wifitrackerlib/Utils;->getReadableText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_7
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/android/settingslib/wifi/AccessPoint;->removeDoubleQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    :goto_0
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->isPasspointAp()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getPasspointProviderFriendlyName()Ljava/lang/String;

    move-result-object p0

    :cond_8
    return-object p0

    :cond_9
    iget-object p0, p0, Lcom/android/settings/wifi/WifiStatusController;->mContext:Landroid/content/Context;

    sget v0, Lcom/android/settings/R$string;->wireless_on:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_a
    :goto_1
    iget-boolean v0, p0, Lcom/android/settings/wifi/WifiStatusController;->mVerbose:Z

    if-eqz v0, :cond_b

    const-string v0, "WiFi is not connected."

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    iget-object p0, p0, Lcom/android/settings/wifi/WifiStatusController;->mContext:Landroid/content/Context;

    sget v0, Lcom/android/settings/R$string;->wireless_on:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private wakeUpWifiTrackerInjector()V
    .locals 5

    :try_start_0
    const-string v0, "com.android.wifitrackerlib.WifiTrackerInjector"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Class;

    const-class v3, Landroid/content/Context;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    new-array v1, v1, [Ljava/lang/Object;

    iget-object p0, p0, Lcom/android/settings/wifi/WifiStatusController;->mContext:Landroid/content/Context;

    aput-object p0, v1, v4

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string p0, "WifiStatusController"

    const-string v0, "Wake Up WifiTrackerInjector error."

    invoke-static {p0, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method


# virtual methods
.method public pause()V
    .locals 0

    return-void
.end method

.method public resume()V
    .locals 0

    return-void
.end method

.method public start()V
    .locals 3

    iget-object v0, p0, Lcom/android/settings/wifi/WifiStatusController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/wifi/WifiStatusController;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/android/settings/wifi/WifiStatusController;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/wifi/WifiStatusController;->mHasRegister:Z

    invoke-virtual {p0}, Lcom/android/settings/wifi/WifiStatusController;->updateStatus()V

    return-void
.end method

.method public stop()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/settings/wifi/WifiStatusController;->mHasRegister:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/settings/wifi/WifiStatusController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/wifi/WifiStatusController;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/wifi/WifiStatusController;->mHasRegister:Z

    :cond_0
    return-void
.end method

.method protected updateStatus()V
    .locals 4

    iget-object v0, p0, Lcom/android/settings/BaseSettingsController;->mStatusView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/settings/wifi/WifiStatusController;->mStatusViewText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wifi/WifiStatusController;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x101

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object p0, p0, Lcom/android/settings/wifi/WifiStatusController;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x64

    invoke-virtual {p0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_1
    return-void
.end method
