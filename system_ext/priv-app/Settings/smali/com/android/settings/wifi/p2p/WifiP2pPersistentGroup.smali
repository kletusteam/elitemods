.class public Lcom/android/settings/wifi/p2p/WifiP2pPersistentGroup;
.super Landroidx/preference/Preference;


# instance fields
.field public mGroup:Landroid/net/wifi/p2p/WifiP2pGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/wifi/p2p/WifiP2pGroup;)V
    .locals 0

    invoke-direct {p0, p1}, Landroidx/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/android/settings/wifi/p2p/WifiP2pPersistentGroup;->mGroup:Landroid/net/wifi/p2p/WifiP2pGroup;

    invoke-virtual {p2}, Landroid/net/wifi/p2p/WifiP2pGroup;->getNetworkName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method getGroupName()Ljava/lang/String;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iget-object p0, p0, Lcom/android/settings/wifi/p2p/WifiP2pPersistentGroup;->mGroup:Landroid/net/wifi/p2p/WifiP2pGroup;

    goto/32 :goto_2

    nop

    :goto_1
    return-object p0

    :goto_2
    invoke-virtual {p0}, Landroid/net/wifi/p2p/WifiP2pGroup;->getNetworkName()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_1

    nop
.end method

.method getNetworkId()I
    .locals 0

    goto/32 :goto_2

    nop

    :goto_0
    return p0

    :goto_1
    invoke-virtual {p0}, Landroid/net/wifi/p2p/WifiP2pGroup;->getNetworkId()I

    move-result p0

    goto/32 :goto_0

    nop

    :goto_2
    iget-object p0, p0, Lcom/android/settings/wifi/p2p/WifiP2pPersistentGroup;->mGroup:Landroid/net/wifi/p2p/WifiP2pGroup;

    goto/32 :goto_1

    nop
.end method
