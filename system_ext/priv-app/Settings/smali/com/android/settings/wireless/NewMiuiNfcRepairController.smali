.class public Lcom/android/settings/wireless/NewMiuiNfcRepairController;
.super Lcom/android/settingslib/core/AbstractPreferenceController;

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Landroidx/preference/Preference$OnPreferenceClickListener;
.implements Lcom/android/settingslib/core/lifecycle/LifecycleObserver;
.implements Lcom/android/settingslib/core/lifecycle/events/OnCreate;
.implements Lcom/android/settingslib/core/lifecycle/events/OnSaveInstanceState;
.implements Lcom/android/settingslib/core/lifecycle/events/OnResume;
.implements Lcom/android/settingslib/core/lifecycle/events/OnPause;
.implements Lcom/android/settingslib/core/lifecycle/events/OnDestroy;


# instance fields
.field mActionListener:Landroid/content/DialogInterface$OnClickListener;

.field private mDialog:Landroid/app/Dialog;

.field mHandler:Landroid/os/Handler;

.field mInquiryListener:Landroid/content/DialogInterface$OnClickListener;

.field mRebootListener:Landroid/content/DialogInterface$OnClickListener;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mRecovery:Z

.field private mRepairPref:Landroidx/preference/Preference;

.field private mStartTime:J

.field private mState:I

.field private mUiContext:Landroid/content/Context;

.field private mWhichBtn:I


# direct methods
.method public static synthetic $r8$lambda$-T3L7htgTLuca6lbsTSEVJH6Iqw(Lcom/android/settings/wireless/NewMiuiNfcRepairController;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->lambda$new$0(Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$1zOZiH6CiwLAsuDKRskuvnw5lfQ(Lcom/android/settings/wireless/NewMiuiNfcRepairController;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->lambda$new$2(Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic $r8$lambda$sgIpNPKNZSNGnII_u1POboh5LH4(Lcom/android/settings/wireless/NewMiuiNfcRepairController;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->lambda$new$1(Landroid/content/DialogInterface;I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmDialog(Lcom/android/settings/wireless/NewMiuiNfcRepairController;)Landroid/app/Dialog;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mDialog:Landroid/app/Dialog;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmReceiver(Lcom/android/settings/wireless/NewMiuiNfcRepairController;)Landroid/content/BroadcastReceiver;
    .locals 0

    iget-object p0, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mReceiver:Landroid/content/BroadcastReceiver;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmDialog(Lcom/android/settings/wireless/NewMiuiNfcRepairController;Landroid/app/Dialog;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mDialog:Landroid/app/Dialog;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmWhichBtn(Lcom/android/settings/wireless/NewMiuiNfcRepairController;I)V
    .locals 0

    iput p1, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mWhichBtn:I

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateState(Lcom/android/settings/wireless/NewMiuiNfcRepairController;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->updateState(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/settingslib/core/lifecycle/Lifecycle;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/android/settingslib/core/AbstractPreferenceController;-><init>(Landroid/content/Context;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mStartTime:J

    const/4 p1, 0x0

    iput p1, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mState:I

    new-instance p1, Lcom/android/settings/wireless/NewMiuiNfcRepairController$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p1, p0, v0}, Lcom/android/settings/wireless/NewMiuiNfcRepairController$1;-><init>(Lcom/android/settings/wireless/NewMiuiNfcRepairController;Landroid/os/Looper;)V

    iput-object p1, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mHandler:Landroid/os/Handler;

    new-instance p1, Lcom/android/settings/wireless/NewMiuiNfcRepairController$2;

    invoke-direct {p1, p0}, Lcom/android/settings/wireless/NewMiuiNfcRepairController$2;-><init>(Lcom/android/settings/wireless/NewMiuiNfcRepairController;)V

    iput-object p1, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mReceiver:Landroid/content/BroadcastReceiver;

    const/4 p1, -0x2

    iput p1, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mWhichBtn:I

    new-instance p1, Lcom/android/settings/wireless/NewMiuiNfcRepairController$$ExternalSyntheticLambda0;

    invoke-direct {p1, p0}, Lcom/android/settings/wireless/NewMiuiNfcRepairController$$ExternalSyntheticLambda0;-><init>(Lcom/android/settings/wireless/NewMiuiNfcRepairController;)V

    iput-object p1, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mRebootListener:Landroid/content/DialogInterface$OnClickListener;

    new-instance p1, Lcom/android/settings/wireless/NewMiuiNfcRepairController$$ExternalSyntheticLambda1;

    invoke-direct {p1, p0}, Lcom/android/settings/wireless/NewMiuiNfcRepairController$$ExternalSyntheticLambda1;-><init>(Lcom/android/settings/wireless/NewMiuiNfcRepairController;)V

    iput-object p1, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mActionListener:Landroid/content/DialogInterface$OnClickListener;

    new-instance p1, Lcom/android/settings/wireless/NewMiuiNfcRepairController$$ExternalSyntheticLambda2;

    invoke-direct {p1, p0}, Lcom/android/settings/wireless/NewMiuiNfcRepairController$$ExternalSyntheticLambda2;-><init>(Lcom/android/settings/wireless/NewMiuiNfcRepairController;)V

    iput-object p1, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mInquiryListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p2, p0}, Lcom/android/settingslib/core/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/wireless/NewMiuiNfcRepairController;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method private synthetic lambda$new$0(Landroid/content/DialogInterface;I)V
    .locals 0

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->updateState(I)V

    const/4 p1, -0x1

    if-ne p2, p1, :cond_0

    iput p2, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mWhichBtn:I

    invoke-virtual {p0}, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->rebootPhone()V

    :cond_0
    return-void
.end method

.method private synthetic lambda$new$1(Landroid/content/DialogInterface;I)V
    .locals 0

    const/4 p1, -0x1

    if-ne p2, p1, :cond_0

    iput p2, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mWhichBtn:I

    const/4 p1, 0x3

    invoke-direct {p0, p1}, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->updateState(I)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->updateState(I)V

    :goto_0
    return-void
.end method

.method private synthetic lambda$new$2(Landroid/content/DialogInterface;I)V
    .locals 0

    const/4 p1, -0x1

    if-ne p2, p1, :cond_0

    iput p2, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mWhichBtn:I

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->updateState(I)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->updateState(I)V

    :goto_0
    return-void
.end method

.method private updateState(I)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "updateState: old state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", new state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "NewMiuiNfcRepairController"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput p1, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mState:I

    return-void
.end method


# virtual methods
.method public displayPreference(Landroidx/preference/PreferenceScreen;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/android/settingslib/core/AbstractPreferenceController;->displayPreference(Landroidx/preference/PreferenceScreen;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "displayPreference: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "NewMiuiNfcRepairController"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "nfc_repair"

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mRepairPref:Landroidx/preference/Preference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/settings/utils/SettingsFeatures;->hasNfcRepairFeature(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object p0, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mRepairPref:Landroidx/preference/Preference;

    invoke-virtual {p1, p0}, Landroidx/preference/PreferenceGroup;->removePreference(Landroidx/preference/Preference;)Z

    return-void

    :cond_0
    iget-object p1, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mRepairPref:Landroidx/preference/Preference;

    invoke-virtual {p1}, Landroidx/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mUiContext:Landroid/content/Context;

    iget-object p1, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mRepairPref:Landroidx/preference/Preference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->setOnPreferenceClickListener(Landroidx/preference/Preference$OnPreferenceClickListener;)V

    iget-object p1, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/miui/enterprise/RestrictionsHelper;->hasNFCRestriction(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p0, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mRepairPref:Landroidx/preference/Preference;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroidx/preference/Preference;->setEnabled(Z)V

    :cond_1
    return-void
.end method

.method public getPreferenceKey()Ljava/lang/String;
    .locals 0

    const-string p0, "nfc_repair"

    return-object p0
.end method

.method public isAvailable()Z
    .locals 0

    iget-object p0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    invoke-static {p0}, Lcom/android/settings/utils/SettingsFeatures;->hasNfcRepairFeature(Landroid/content/Context;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    const/4 p0, 0x1

    return p0
.end method

.method nextDialogShow()V
    .locals 11

    goto/32 :goto_4b

    nop

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_a

    nop

    :goto_1
    invoke-direct {v7, v8}, Lmiuix/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    goto/32 :goto_3e

    nop

    :goto_2
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    goto/32 :goto_6

    nop

    :goto_3
    if-ne v0, v3, :cond_0

    goto/32 :goto_47

    :cond_0
    goto/32 :goto_4e

    nop

    :goto_4
    move v4, v1

    goto/32 :goto_53

    nop

    :goto_5
    iget v1, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mState:I

    goto/32 :goto_0

    nop

    :goto_6
    aput-object v9, v8, v5

    goto/32 :goto_23

    nop

    :goto_7
    const/high16 v1, 0x1040000

    goto/32 :goto_8

    nop

    :goto_8
    const/4 v2, 0x0

    goto/32 :goto_4d

    nop

    :goto_9
    iput-object v0, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mDialog:Landroid/app/Dialog;

    goto/32 :goto_42

    nop

    :goto_a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_2f

    nop

    :goto_b
    invoke-virtual {v0, v3}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/AlertDialog$Builder;

    :goto_c
    goto/32 :goto_49

    nop

    :goto_d
    invoke-virtual {p0}, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->repairNFC()V

    goto/32 :goto_5a

    nop

    :goto_e
    sget v5, Lcom/android/settings/R$string;->nfc_repair_btn_no:I

    goto/32 :goto_48

    nop

    :goto_f
    invoke-virtual {v7, v1, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_15

    nop

    :goto_10
    iput v0, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mWhichBtn:I

    goto/32 :goto_51

    nop

    :goto_11
    invoke-virtual {v0, v1, v2}, Lmiuix/appcompat/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    goto/32 :goto_54

    nop

    :goto_12
    sget v4, Lcom/android/settings/R$string;->nfc_repair_btn_reboot:I

    goto/32 :goto_17

    nop

    :goto_13
    sget v0, Lcom/android/settings/R$string;->nfc_repair_pass_title:I

    goto/32 :goto_29

    nop

    :goto_14
    sget v0, Lcom/android/settings/R$string;->nfc_repair_fail_title:I

    goto/32 :goto_5e

    nop

    :goto_15
    new-array v4, v6, [Ljava/lang/Object;

    goto/32 :goto_2d

    nop

    :goto_16
    sget v0, Lcom/android/settings/R$string;->nfc_repair_title:I

    goto/32 :goto_1c

    nop

    :goto_17
    sget v1, Lcom/android/settings/R$string;->nfc_repair_btn_reboot_later:I

    goto/32 :goto_1e

    nop

    :goto_18
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_33

    nop

    :goto_19
    iget-object v7, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    goto/32 :goto_5f

    nop

    :goto_1a
    invoke-virtual {v0, v6}, Lmiuix/appcompat/app/AlertDialog;->setCancelable(Z)V

    goto/32 :goto_40

    nop

    :goto_1b
    if-ne v0, v4, :cond_1

    goto/32 :goto_2b

    :cond_1
    goto/32 :goto_57

    nop

    :goto_1c
    sget v3, Lcom/android/settings/R$string;->nfc_repair_action_desc:I

    goto/32 :goto_64

    nop

    :goto_1d
    if-ne v0, v5, :cond_2

    goto/32 :goto_5d

    :cond_2
    goto/32 :goto_1b

    nop

    :goto_1e
    iget-object v5, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mRebootListener:Landroid/content/DialogInterface$OnClickListener;

    goto/32 :goto_27

    nop

    :goto_1f
    return-void

    :goto_20
    goto/32 :goto_14

    nop

    :goto_21
    const-string v1, "nextDialogShow: "

    goto/32 :goto_58

    nop

    :goto_22
    iget-object v8, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mUiContext:Landroid/content/Context;

    goto/32 :goto_1

    nop

    :goto_23
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/32 :goto_30

    nop

    :goto_24
    sget v4, Lcom/android/settings/R$string;->nfc_repair_btn_yes:I

    goto/32 :goto_e

    nop

    :goto_25
    goto/16 :goto_c

    :goto_26
    goto/32 :goto_b

    nop

    :goto_27
    goto :goto_39

    :goto_28
    goto/32 :goto_d

    nop

    :goto_29
    sget v3, Lcom/android/settings/R$string;->nfc_repair_pass_desc:I

    goto/32 :goto_12

    nop

    :goto_2a
    return-void

    :goto_2b
    goto/32 :goto_16

    nop

    :goto_2c
    sget v0, Lcom/android/settings/R$string;->nfc_repair_title:I

    goto/32 :goto_45

    nop

    :goto_2d
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_24

    nop

    :goto_2e
    sget v2, Lcom/android/settings/R$string;->nfc_repair_ongoing:I

    goto/32 :goto_52

    nop

    :goto_2f
    const-string v1, "NewMiuiNfcRepairController"

    goto/32 :goto_18

    nop

    :goto_30
    aput-object v3, v8, v4

    goto/32 :goto_f

    nop

    :goto_31
    const/4 v5, 0x1

    goto/32 :goto_4c

    nop

    :goto_32
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    goto/32 :goto_2e

    nop

    :goto_33
    iget v0, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mState:I

    goto/32 :goto_7

    nop

    :goto_34
    move v10, v4

    goto/32 :goto_4

    nop

    :goto_35
    invoke-virtual {v0, v5, v6}, Lmiuix/appcompat/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    goto/32 :goto_11

    nop

    :goto_36
    if-eqz v3, :cond_3

    goto/32 :goto_26

    :cond_3
    goto/32 :goto_37

    nop

    :goto_37
    invoke-virtual {v0, v4}, Lmiuix/appcompat/app/AlertDialog$Builder;->setMessage(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    goto/32 :goto_25

    nop

    :goto_38
    iget-object v5, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mActionListener:Landroid/content/DialogInterface$OnClickListener;

    :goto_39
    goto/32 :goto_66

    nop

    :goto_3a
    move v5, v10

    :goto_3b
    goto/32 :goto_55

    nop

    :goto_3c
    const/4 v3, 0x4

    goto/32 :goto_3

    nop

    :goto_3d
    move v5, v4

    goto/32 :goto_59

    nop

    :goto_3e
    invoke-virtual {v7, v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setTitle(I)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    goto/32 :goto_35

    nop

    :goto_3f
    aput-object v9, v8, v6

    goto/32 :goto_2

    nop

    :goto_40
    invoke-virtual {v0, v6}, Lmiuix/appcompat/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    goto/32 :goto_61

    nop

    :goto_41
    invoke-direct {v0, v1}, Lmiuix/appcompat/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    goto/32 :goto_1a

    nop

    :goto_42
    return-void

    :goto_43
    move-object v3, v2

    goto/32 :goto_5c

    nop

    :goto_44
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto/32 :goto_62

    nop

    :goto_45
    sget v1, Lcom/android/settings/R$string;->nfc_repair_confirm_desc:I

    goto/32 :goto_19

    nop

    :goto_46
    goto :goto_39

    :goto_47
    goto/32 :goto_13

    nop

    :goto_48
    iget-object v6, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mInquiryListener:Landroid/content/DialogInterface$OnClickListener;

    goto/32 :goto_34

    nop

    :goto_49
    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertDialog$Builder;->show()Lmiuix/appcompat/app/AlertDialog;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_4a
    sget v4, Lcom/android/settings/R$string;->nfc_repair_btn_retry:I

    goto/32 :goto_63

    nop

    :goto_4b
    const/4 v0, -0x2

    goto/32 :goto_10

    nop

    :goto_4c
    const/4 v6, 0x0

    goto/32 :goto_1d

    nop

    :goto_4d
    const/4 v3, 0x3

    goto/32 :goto_60

    nop

    :goto_4e
    const/4 v3, 0x5

    goto/32 :goto_56

    nop

    :goto_4f
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    goto/32 :goto_3f

    nop

    :goto_50
    invoke-virtual {v0, p0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto/32 :goto_44

    nop

    :goto_51
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_5b

    nop

    :goto_52
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    goto/32 :goto_65

    nop

    :goto_53
    move v1, v5

    goto/32 :goto_3a

    nop

    :goto_54
    invoke-virtual {v0, p0}, Lmiuix/appcompat/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Lmiuix/appcompat/app/AlertDialog$Builder;

    move-result-object v0

    goto/32 :goto_36

    nop

    :goto_55
    new-instance v7, Lmiuix/appcompat/app/AlertDialog$Builder;

    goto/32 :goto_22

    nop

    :goto_56
    if-ne v0, v3, :cond_4

    goto/32 :goto_20

    :cond_4
    goto/32 :goto_1f

    nop

    :goto_57
    if-ne v0, v3, :cond_5

    goto/32 :goto_28

    :cond_5
    goto/32 :goto_3c

    nop

    :goto_58
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_5

    nop

    :goto_59
    move v4, v3

    goto/32 :goto_43

    nop

    :goto_5a
    new-instance v0, Lmiuix/appcompat/app/ProgressDialog;

    goto/32 :goto_67

    nop

    :goto_5b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_21

    nop

    :goto_5c
    goto/16 :goto_3b

    :goto_5d
    goto/32 :goto_2c

    nop

    :goto_5e
    sget v3, Lcom/android/settings/R$string;->nfc_repair_fail_desc:I

    goto/32 :goto_4a

    nop

    :goto_5f
    new-array v8, v3, [Ljava/lang/Object;

    goto/32 :goto_4f

    nop

    :goto_60
    const/4 v4, 0x2

    goto/32 :goto_31

    nop

    :goto_61
    iget-object v1, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    goto/32 :goto_32

    nop

    :goto_62
    iput-object v0, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mDialog:Landroid/app/Dialog;

    goto/32 :goto_2a

    nop

    :goto_63
    iget-object v5, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mActionListener:Landroid/content/DialogInterface$OnClickListener;

    goto/32 :goto_46

    nop

    :goto_64
    sget v4, Lcom/android/settings/R$string;->nfc_repair_action_btn:I

    goto/32 :goto_38

    nop

    :goto_65
    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto/32 :goto_50

    nop

    :goto_66
    move-object v6, v5

    goto/32 :goto_3d

    nop

    :goto_67
    iget-object v1, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mUiContext:Landroid/content/Context;

    goto/32 :goto_41

    nop
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mRecovery:Z

    const/4 v0, 0x0

    const-string v1, "RepairState"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->updateState(I)V

    const-wide/16 v0, 0x0

    const-string v2, "RepairStart"

    invoke-virtual {p1, v2, v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mStartTime:J

    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onCreate: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p0, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mState:I

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "NewMiuiNfcRepairController"

    invoke-static {p1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onDestroy()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onDestroy: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "NewMiuiNfcRepairController"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    iget-object v0, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iput-object v1, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mDialog:Landroid/app/Dialog;

    :cond_1
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mDialog:Landroid/app/Dialog;

    iget p1, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mWhichBtn:I

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->updateState(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->nextDialogShow()V

    return-void
.end method

.method public onPause()V
    .locals 0

    return-void
.end method

.method public onPreferenceClick(Landroidx/preference/Preference;)Z
    .locals 2

    const-string p1, "NewMiuiNfcRepairController"

    const-string/jumbo v0, "trigger inquiry"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/android/settings/Utils;->isMonkeyRunning()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const-string p0, "Ingore, Monkey running..."

    invoke-static {p1, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_0
    invoke-direct {p0, v1}, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->updateState(I)V

    invoke-virtual {p0}, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->nextDialogShow()V

    return v1
.end method

.method public onResume()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mRecovery:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->nextDialogShow()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mRecovery:Z

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "onSaveInstanceState: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", StartTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mStartTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "NewMiuiNfcRepairController"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mState:I

    const-string v1, "RepairState"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-wide v0, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mStartTime:J

    const-string p0, "RepairStart"

    invoke-virtual {p1, p0, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    return-void
.end method

.method rebootPhone()V
    .locals 3

    goto/32 :goto_4

    nop

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_3

    nop

    :goto_1
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_a

    nop

    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop

    :goto_3
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_7

    nop

    :goto_4
    const-string v0, "NewMiuiNfcRepairController"

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "rebootPhone: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p0, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "power"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/os/PowerManager;

    const-string v1, "OneTouchRepair..."

    invoke-virtual {p0, v1}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_6

    nop

    :goto_5
    return-void

    :goto_6
    goto :goto_9

    :catch_0
    move-exception p0

    goto/32 :goto_2

    nop

    :goto_7
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_8

    nop

    :goto_8
    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_9
    goto/32 :goto_5

    nop

    :goto_a
    const-string v2, "RebootPhone fail: "

    goto/32 :goto_0

    nop
.end method

.method repairNFC()V
    .locals 10

    goto/32 :goto_10

    nop

    :goto_0
    const-wide/16 v2, 0x0

    goto/32 :goto_d

    nop

    :goto_1
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_9

    nop

    :goto_2
    iget v1, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mState:I

    goto/32 :goto_5

    nop

    :goto_3
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_0

    nop

    :goto_4
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_f

    nop

    :goto_5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_c

    nop

    :goto_6
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_b

    nop

    :goto_7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_8

    nop

    :goto_8
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_9
    const-string/jumbo v1, "repairNFC: "

    goto/32 :goto_a

    nop

    :goto_a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_2

    nop

    :goto_b
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_12

    nop

    :goto_c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_16

    nop

    :goto_d
    const/4 v0, 0x1

    :try_start_0
    new-instance v7, Landroid/content/IntentFilter;

    const-string v4, "com.android.nfc.action.repair.rsp"

    invoke-direct {v7, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mReceiver:Landroid/content/BroadcastReceiver;

    sget-object v6, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const-string v8, "android.permission.WRITE_SECURE_SETTINGS"

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    iget-boolean v4, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mRecovery:Z

    if-eqz v4, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mStartTime:J

    sub-long/2addr v2, v4

    goto :goto_e

    :cond_0
    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.android.nfc.action.repair.req"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v5, "com.android.nfc"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v5, p0, Lcom/android/settingslib/core/AbstractPreferenceController;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mStartTime:J

    :goto_e
    iget-object v4, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mHandler:Landroid/os/Handler;

    const-wide/16 v5, 0x7530

    sub-long/2addr v5, v2

    invoke-virtual {v4, v0, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_13

    nop

    :goto_f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_17

    nop

    :goto_10
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop

    :goto_11
    return-void

    :goto_12
    iget-object p0, p0, Lcom/android/settings/wireless/NewMiuiNfcRepairController;->mHandler:Landroid/os/Handler;

    goto/32 :goto_14

    nop

    :goto_13
    goto :goto_15

    :catch_0
    move-exception v2

    goto/32 :goto_4

    nop

    :goto_14
    invoke-virtual {p0, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_15
    goto/32 :goto_11

    nop

    :goto_16
    const-string v1, "NewMiuiNfcRepairController"

    goto/32 :goto_3

    nop

    :goto_17
    const-string v4, "RepairNFC fail: "

    goto/32 :goto_7

    nop
.end method
