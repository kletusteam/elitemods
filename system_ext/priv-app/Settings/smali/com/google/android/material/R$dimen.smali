.class public final Lcom/google/android/material/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final design_appbar_elevation:I = 0x7f070213

.field public static final design_bottom_navigation_active_item_max_width:I = 0x7f070214

.field public static final design_bottom_navigation_active_item_min_width:I = 0x7f070215

.field public static final design_bottom_navigation_item_max_width:I = 0x7f07021a

.field public static final design_bottom_navigation_item_min_width:I = 0x7f07021b

.field public static final design_bottom_navigation_margin:I = 0x7f07021d

.field public static final design_bottom_navigation_shadow_height:I = 0x7f07021e

.field public static final design_bottom_sheet_peek_height_min:I = 0x7f070222

.field public static final design_fab_size_mini:I = 0x7f070226

.field public static final design_fab_size_normal:I = 0x7f070227

.field public static final design_navigation_icon_size:I = 0x7f07022c

.field public static final design_navigation_separator_vertical_padding:I = 0x7f070232

.field public static final design_snackbar_padding_vertical:I = 0x7f07023b

.field public static final design_snackbar_padding_vertical_2lines:I = 0x7f07023c

.field public static final design_tab_scrollable_min_width:I = 0x7f07023f

.field public static final design_tab_text_size_2line:I = 0x7f070241

.field public static final design_textinput_caption_translate_y:I = 0x7f070242

.field public static final material_clock_hand_center_dot_radius:I = 0x7f0704cd

.field public static final material_clock_hand_padding:I = 0x7f0704ce

.field public static final material_clock_hand_stroke_width:I = 0x7f0704cf

.field public static final material_clock_size:I = 0x7f0704d5

.field public static final material_divider_thickness:I = 0x7f0704d9

.field public static final material_filled_edittext_font_1_3_padding_bottom:I = 0x7f0704de

.field public static final material_filled_edittext_font_1_3_padding_top:I = 0x7f0704df

.field public static final material_filled_edittext_font_2_0_padding_bottom:I = 0x7f0704e0

.field public static final material_filled_edittext_font_2_0_padding_top:I = 0x7f0704e1

.field public static final material_font_1_3_box_collapsed_padding_top:I = 0x7f0704e2

.field public static final material_font_2_0_box_collapsed_padding_top:I = 0x7f0704e3

.field public static final material_helper_text_default_padding_top:I = 0x7f0704e4

.field public static final material_helper_text_font_1_3_padding_horizontal:I = 0x7f0704e5

.field public static final material_helper_text_font_1_3_padding_top:I = 0x7f0704e6

.field public static final material_input_text_to_prefix_suffix_padding:I = 0x7f0704e7

.field public static final material_time_picker_minimum_screen_height:I = 0x7f0704ef

.field public static final material_time_picker_minimum_screen_width:I = 0x7f0704f0

.field public static final mtrl_badge_horizontal_edge_offset:I = 0x7f0707e2

.field public static final mtrl_badge_long_text_horizontal_padding:I = 0x7f0707e3

.field public static final mtrl_badge_radius:I = 0x7f0707e4

.field public static final mtrl_badge_text_horizontal_edge_offset:I = 0x7f0707e5

.field public static final mtrl_badge_toolbar_action_menu_item_horizontal_offset:I = 0x7f0707e7

.field public static final mtrl_badge_toolbar_action_menu_item_vertical_offset:I = 0x7f0707e8

.field public static final mtrl_badge_with_text_radius:I = 0x7f0707e9

.field public static final mtrl_bottomappbar_fabOffsetEndMode:I = 0x7f0707ea

.field public static final mtrl_bottomappbar_fab_bottom_margin:I = 0x7f0707eb

.field public static final mtrl_calendar_bottom_padding:I = 0x7f07080b

.field public static final mtrl_calendar_content_padding:I = 0x7f07080c

.field public static final mtrl_calendar_day_height:I = 0x7f07080e

.field public static final mtrl_calendar_day_width:I = 0x7f070812

.field public static final mtrl_calendar_days_of_week_height:I = 0x7f070813

.field public static final mtrl_calendar_dialog_background_inset:I = 0x7f070814

.field public static final mtrl_calendar_maximum_default_fullscreen_minor_axis:I = 0x7f07081f

.field public static final mtrl_calendar_month_horizontal_padding:I = 0x7f070820

.field public static final mtrl_calendar_month_vertical_padding:I = 0x7f070821

.field public static final mtrl_calendar_navigation_bottom_padding:I = 0x7f070822

.field public static final mtrl_calendar_navigation_height:I = 0x7f070823

.field public static final mtrl_calendar_navigation_top_padding:I = 0x7f070824

.field public static final mtrl_edittext_rectangle_top_offset:I = 0x7f07083a

.field public static final mtrl_exposed_dropdown_menu_popup_elevation:I = 0x7f07083b

.field public static final mtrl_exposed_dropdown_menu_popup_vertical_padding:I = 0x7f07083d

.field public static final mtrl_fab_min_touch_target:I = 0x7f070850

.field public static final mtrl_min_touch_target_size:I = 0x7f07085c

.field public static final mtrl_navigation_bar_item_default_icon_size:I = 0x7f07085d

.field public static final mtrl_navigation_bar_item_default_margin:I = 0x7f07085e

.field public static final mtrl_navigation_rail_icon_margin:I = 0x7f070869

.field public static final mtrl_navigation_rail_margin:I = 0x7f07086b

.field public static final mtrl_progress_circular_inset_medium:I = 0x7f070870

.field public static final mtrl_progress_circular_size_medium:I = 0x7f070875

.field public static final mtrl_progress_track_thickness:I = 0x7f07087b

.field public static final mtrl_shape_corner_size_small_component:I = 0x7f07087e

.field public static final mtrl_slider_label_padding:I = 0x7f070880

.field public static final mtrl_slider_thumb_radius:I = 0x7f070884

.field public static final mtrl_slider_track_side_padding:I = 0x7f070886

.field public static final mtrl_slider_track_top:I = 0x7f070887

.field public static final mtrl_slider_widget_height:I = 0x7f070888

.field public static final mtrl_snackbar_background_corner_radius:I = 0x7f07088a

.field public static final mtrl_switch_thumb_elevation:I = 0x7f07088f

.field public static final mtrl_textinput_box_label_cutout_padding:I = 0x7f070892

.field public static final mtrl_textinput_box_stroke_width_default:I = 0x7f070893

.field public static final mtrl_textinput_box_stroke_width_focused:I = 0x7f070894

.field public static final mtrl_textinput_counter_margin_start:I = 0x7f070895

.field public static final mtrl_tooltip_arrowSize:I = 0x7f07089a
