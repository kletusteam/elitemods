.class public final Lcom/google/android/material/R$id;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final cancel_button:I = 0x7f0a01f5

.field public static final circle_center:I = 0x7f0a0232

.field public static final confirm_button:I = 0x7f0a025c

.field public static final design_menu_item_action_area_stub:I = 0x7f0a02e0

.field public static final design_menu_item_text:I = 0x7f0a02e1

.field public static final material_clock_display:I = 0x7f0a0607

.field public static final material_clock_face:I = 0x7f0a0608

.field public static final material_clock_hand:I = 0x7f0a0609

.field public static final material_clock_period_pm_button:I = 0x7f0a060b

.field public static final material_clock_period_toggle:I = 0x7f0a060c

.field public static final material_hour_tv:I = 0x7f0a060e

.field public static final material_label:I = 0x7f0a060f

.field public static final material_minute_tv:I = 0x7f0a0611

.field public static final material_value_index:I = 0x7f0a0619

.field public static final month_grid:I = 0x7f0a067d

.field public static final month_navigation_fragment_toggle:I = 0x7f0a067f

.field public static final month_navigation_next:I = 0x7f0a0680

.field public static final month_navigation_previous:I = 0x7f0a0681

.field public static final month_title:I = 0x7f0a0682

.field public static final mtrl_anchor_parent:I = 0x7f0a06cb

.field public static final mtrl_calendar_day_selector_frame:I = 0x7f0a06cc

.field public static final mtrl_calendar_days_of_week:I = 0x7f0a06cd

.field public static final mtrl_calendar_frame:I = 0x7f0a06ce

.field public static final mtrl_calendar_main_pane:I = 0x7f0a06cf

.field public static final mtrl_calendar_months:I = 0x7f0a06d0

.field public static final mtrl_calendar_year_selector_frame:I = 0x7f0a06d3

.field public static final mtrl_card_checked_layer_id:I = 0x7f0a06d4

.field public static final mtrl_child_content_container:I = 0x7f0a06d5

.field public static final mtrl_internal_children_alpha_tag:I = 0x7f0a06d6

.field public static final mtrl_picker_header_selection_text:I = 0x7f0a06da

.field public static final mtrl_picker_header_toggle:I = 0x7f0a06dc

.field public static final mtrl_picker_text_input_date:I = 0x7f0a06dd

.field public static final mtrl_picker_text_input_range_end:I = 0x7f0a06de

.field public static final mtrl_picker_text_input_range_start:I = 0x7f0a06df

.field public static final mtrl_picker_title_text:I = 0x7f0a06e0

.field public static final navigation_bar_item_active_indicator_view:I = 0x7f0a06ee

.field public static final navigation_bar_item_icon_container:I = 0x7f0a06ef

.field public static final navigation_bar_item_icon_view:I = 0x7f0a06f0

.field public static final navigation_bar_item_labels_group:I = 0x7f0a06f1

.field public static final navigation_bar_item_large_label_view:I = 0x7f0a06f2

.field public static final navigation_bar_item_small_label_view:I = 0x7f0a06f3

.field public static final row_index_key:I = 0x7f0a08a8

.field public static final selection_type:I = 0x7f0a090a

.field public static final snackbar_action:I = 0x7f0a0977

.field public static final snackbar_text:I = 0x7f0a0978

.field public static final text_input_error_icon:I = 0x7f0a0a70

.field public static final textinput_counter:I = 0x7f0a0a7b

.field public static final textinput_error:I = 0x7f0a0a7c

.field public static final textinput_helper_text:I = 0x7f0a0a7d

.field public static final textinput_placeholder:I = 0x7f0a0a7e

.field public static final textinput_prefix_text:I = 0x7f0a0a7f

.field public static final textinput_suffix_text:I = 0x7f0a0a80

.field public static final view_offset_helper:I = 0x7f0a0b6e
