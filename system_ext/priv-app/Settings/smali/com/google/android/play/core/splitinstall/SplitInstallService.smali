.class final Lcom/google/android/play/core/splitinstall/SplitInstallService;
.super Ljava/lang/Object;


# static fields
.field static final playCore:Lcom/google/android/play/core/splitcompat/util/PlayCore;


# instance fields
.field private final mContext:Landroid/content/Context;

.field final mPackageName:Ljava/lang/String;

.field final mSplitRemoteManager:Lcom/google/android/play/core/remote/RemoteManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/play/core/remote/RemoteManager<",
            "Lcom/google/android/play/core/splitinstall/protocol/ISplitInstallServiceProxy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/play/core/splitcompat/util/PlayCore;

    const-class v1, Lcom/google/android/play/core/splitinstall/SplitInstallService;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/play/core/splitcompat/util/PlayCore;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/play/core/splitinstall/SplitInstallService;->playCore:Lcom/google/android/play/core/splitcompat/util/PlayCore;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/core/splitinstall/SplitInstallService;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v6, Lcom/google/android/play/core/splitinstall/OnBinderDiedListenerImpl;

    invoke-direct {v6, p0}, Lcom/google/android/play/core/splitinstall/OnBinderDiedListenerImpl;-><init>(Lcom/google/android/play/core/splitinstall/SplitInstallService;)V

    iput-object p1, p0, Lcom/google/android/play/core/splitinstall/SplitInstallService;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/play/core/splitinstall/SplitInstallService;->mPackageName:Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.iqiyi.android.play.core.splitinstall.BIND_SPLIT_INSTALL_SERVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    new-instance p2, Lcom/google/android/play/core/remote/RemoteManager;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/google/android/play/core/splitinstall/SplitInstallService;->playCore:Lcom/google/android/play/core/splitcompat/util/PlayCore;

    sget-object v5, Lcom/google/android/play/core/splitinstall/SplitRemoteImpl;->sInstance:Lcom/google/android/play/core/remote/IRemote;

    const-string v3, "SplitInstallService"

    move-object v0, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/play/core/remote/RemoteManager;-><init>(Landroid/content/Context;Lcom/google/android/play/core/splitcompat/util/PlayCore;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/play/core/remote/IRemote;Lcom/google/android/play/core/remote/OnBinderDiedListener;)V

    iput-object p2, p0, Lcom/google/android/play/core/splitinstall/SplitInstallService;->mSplitRemoteManager:Lcom/google/android/play/core/remote/RemoteManager;

    return-void
.end method

.method static wrapModuleNames(Ljava/util/Collection;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "module_name"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method static wrapVersionCode()Landroid/os/Bundle;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "playcore_version_code"

    const/16 v2, 0x271a

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object v0
.end method


# virtual methods
.method cancelInstall(I)Lcom/google/android/play/core/tasks/Task;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/android/play/core/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    goto/32 :goto_5

    nop

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/play/core/tasks/TaskWrapper;->getTask()Lcom/google/android/play/core/tasks/Task;

    move-result-object p0

    goto/32 :goto_6

    nop

    :goto_1
    invoke-virtual {v1, v2}, Lcom/google/android/play/core/remote/RemoteManager;->bindService(Lcom/google/android/play/core/remote/RemoteTask;)V

    goto/32 :goto_0

    nop

    :goto_2
    new-instance v2, Lcom/google/android/play/core/splitinstall/CancelInstallTask;

    goto/32 :goto_7

    nop

    :goto_3
    invoke-direct {v0}, Lcom/google/android/play/core/tasks/TaskWrapper;-><init>()V

    goto/32 :goto_9

    nop

    :goto_4
    new-array v1, v1, [Ljava/lang/Object;

    goto/32 :goto_d

    nop

    :goto_5
    sget-object v0, Lcom/google/android/play/core/splitinstall/SplitInstallService;->playCore:Lcom/google/android/play/core/splitcompat/util/PlayCore;

    goto/32 :goto_c

    nop

    :goto_6
    return-object p0

    :goto_7
    invoke-direct {v2, p0, v0, p1, v0}, Lcom/google/android/play/core/splitinstall/CancelInstallTask;-><init>(Lcom/google/android/play/core/splitinstall/SplitInstallService;Lcom/google/android/play/core/tasks/TaskWrapper;ILcom/google/android/play/core/tasks/TaskWrapper;)V

    goto/32 :goto_1

    nop

    :goto_8
    const-string v2, "cancelInstall(%d)"

    goto/32 :goto_e

    nop

    :goto_9
    iget-object v1, p0, Lcom/google/android/play/core/splitinstall/SplitInstallService;->mSplitRemoteManager:Lcom/google/android/play/core/remote/RemoteManager;

    goto/32 :goto_2

    nop

    :goto_a
    new-instance v0, Lcom/google/android/play/core/tasks/TaskWrapper;

    goto/32 :goto_3

    nop

    :goto_b
    aput-object v2, v1, v3

    goto/32 :goto_8

    nop

    :goto_c
    const/4 v1, 0x1

    goto/32 :goto_4

    nop

    :goto_d
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/32 :goto_f

    nop

    :goto_e
    invoke-virtual {v0, v2, v1}, Lcom/google/android/play/core/splitcompat/util/PlayCore;->info(Ljava/lang/String;[Ljava/lang/Object;)I

    goto/32 :goto_a

    nop

    :goto_f
    const/4 v3, 0x0

    goto/32 :goto_b

    nop
.end method

.method deferredInstall(Ljava/util/List;)Lcom/google/android/play/core/tasks/Task;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/play/core/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    sget-object v0, Lcom/google/android/play/core/splitinstall/SplitInstallService;->playCore:Lcom/google/android/play/core/splitcompat/util/PlayCore;

    goto/32 :goto_7

    nop

    :goto_1
    invoke-direct {v2, p0, v0, p1, v0}, Lcom/google/android/play/core/splitinstall/DeferredInstallTask;-><init>(Lcom/google/android/play/core/splitinstall/SplitInstallService;Lcom/google/android/play/core/tasks/TaskWrapper;Ljava/util/List;Lcom/google/android/play/core/tasks/TaskWrapper;)V

    goto/32 :goto_3

    nop

    :goto_2
    iget-object v1, p0, Lcom/google/android/play/core/splitinstall/SplitInstallService;->mSplitRemoteManager:Lcom/google/android/play/core/remote/RemoteManager;

    goto/32 :goto_e

    nop

    :goto_3
    invoke-virtual {v1, v2}, Lcom/google/android/play/core/remote/RemoteManager;->bindService(Lcom/google/android/play/core/remote/RemoteTask;)V

    goto/32 :goto_b

    nop

    :goto_4
    const/4 v2, 0x0

    goto/32 :goto_6

    nop

    :goto_5
    new-array v1, v1, [Ljava/lang/Object;

    goto/32 :goto_4

    nop

    :goto_6
    aput-object p1, v1, v2

    goto/32 :goto_c

    nop

    :goto_7
    const/4 v1, 0x1

    goto/32 :goto_5

    nop

    :goto_8
    invoke-direct {v0}, Lcom/google/android/play/core/tasks/TaskWrapper;-><init>()V

    goto/32 :goto_2

    nop

    :goto_9
    return-object p0

    :goto_a
    new-instance v0, Lcom/google/android/play/core/tasks/TaskWrapper;

    goto/32 :goto_8

    nop

    :goto_b
    invoke-virtual {v0}, Lcom/google/android/play/core/tasks/TaskWrapper;->getTask()Lcom/google/android/play/core/tasks/Task;

    move-result-object p0

    goto/32 :goto_9

    nop

    :goto_c
    const-string v2, "deferredInstall(%s)"

    goto/32 :goto_d

    nop

    :goto_d
    invoke-virtual {v0, v2, v1}, Lcom/google/android/play/core/splitcompat/util/PlayCore;->info(Ljava/lang/String;[Ljava/lang/Object;)I

    goto/32 :goto_a

    nop

    :goto_e
    new-instance v2, Lcom/google/android/play/core/splitinstall/DeferredInstallTask;

    goto/32 :goto_1

    nop
.end method

.method deferredUninstall(Ljava/util/List;)Lcom/google/android/play/core/tasks/Task;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/play/core/tasks/Task<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    goto/32 :goto_6

    nop

    :goto_0
    invoke-direct {v2, p0, v0, p1, v0}, Lcom/google/android/play/core/splitinstall/DeferredUninstallTask;-><init>(Lcom/google/android/play/core/splitinstall/SplitInstallService;Lcom/google/android/play/core/tasks/TaskWrapper;Ljava/util/List;Lcom/google/android/play/core/tasks/TaskWrapper;)V

    goto/32 :goto_b

    nop

    :goto_1
    new-instance v2, Lcom/google/android/play/core/splitinstall/DeferredUninstallTask;

    goto/32 :goto_0

    nop

    :goto_2
    const/4 v1, 0x1

    goto/32 :goto_7

    nop

    :goto_3
    invoke-direct {v0}, Lcom/google/android/play/core/tasks/TaskWrapper;-><init>()V

    goto/32 :goto_4

    nop

    :goto_4
    iget-object v1, p0, Lcom/google/android/play/core/splitinstall/SplitInstallService;->mSplitRemoteManager:Lcom/google/android/play/core/remote/RemoteManager;

    goto/32 :goto_1

    nop

    :goto_5
    aput-object p1, v1, v2

    goto/32 :goto_9

    nop

    :goto_6
    sget-object v0, Lcom/google/android/play/core/splitinstall/SplitInstallService;->playCore:Lcom/google/android/play/core/splitcompat/util/PlayCore;

    goto/32 :goto_2

    nop

    :goto_7
    new-array v1, v1, [Ljava/lang/Object;

    goto/32 :goto_e

    nop

    :goto_8
    new-instance v0, Lcom/google/android/play/core/tasks/TaskWrapper;

    goto/32 :goto_3

    nop

    :goto_9
    const-string v2, "deferredUninstall(%s)"

    goto/32 :goto_c

    nop

    :goto_a
    invoke-virtual {v0}, Lcom/google/android/play/core/tasks/TaskWrapper;->getTask()Lcom/google/android/play/core/tasks/Task;

    move-result-object p0

    goto/32 :goto_d

    nop

    :goto_b
    invoke-virtual {v1, v2}, Lcom/google/android/play/core/remote/RemoteManager;->bindService(Lcom/google/android/play/core/remote/RemoteTask;)V

    goto/32 :goto_a

    nop

    :goto_c
    invoke-virtual {v0, v2, v1}, Lcom/google/android/play/core/splitcompat/util/PlayCore;->info(Ljava/lang/String;[Ljava/lang/Object;)I

    goto/32 :goto_8

    nop

    :goto_d
    return-object p0

    :goto_e
    const/4 v2, 0x0

    goto/32 :goto_5

    nop
.end method

.method getSessionState(I)Lcom/google/android/play/core/tasks/Task;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/android/play/core/tasks/Task<",
            "Lcom/google/android/play/core/splitinstall/SplitInstallSessionState;",
            ">;"
        }
    .end annotation

    goto/32 :goto_f

    nop

    :goto_0
    const/4 v1, 0x1

    goto/32 :goto_9

    nop

    :goto_1
    invoke-direct {v2, p0, v0, p1, v0}, Lcom/google/android/play/core/splitinstall/GetSessionStateTask;-><init>(Lcom/google/android/play/core/splitinstall/SplitInstallService;Lcom/google/android/play/core/tasks/TaskWrapper;ILcom/google/android/play/core/tasks/TaskWrapper;)V

    goto/32 :goto_a

    nop

    :goto_2
    new-instance v0, Lcom/google/android/play/core/tasks/TaskWrapper;

    goto/32 :goto_e

    nop

    :goto_3
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/32 :goto_c

    nop

    :goto_4
    return-object p0

    :goto_5
    invoke-virtual {v0, v2, v1}, Lcom/google/android/play/core/splitcompat/util/PlayCore;->info(Ljava/lang/String;[Ljava/lang/Object;)I

    goto/32 :goto_2

    nop

    :goto_6
    aput-object v2, v1, v3

    goto/32 :goto_7

    nop

    :goto_7
    const-string v2, "getSessionState(%d)"

    goto/32 :goto_5

    nop

    :goto_8
    new-instance v2, Lcom/google/android/play/core/splitinstall/GetSessionStateTask;

    goto/32 :goto_1

    nop

    :goto_9
    new-array v1, v1, [Ljava/lang/Object;

    goto/32 :goto_3

    nop

    :goto_a
    invoke-virtual {v1, v2}, Lcom/google/android/play/core/remote/RemoteManager;->bindService(Lcom/google/android/play/core/remote/RemoteTask;)V

    goto/32 :goto_b

    nop

    :goto_b
    invoke-virtual {v0}, Lcom/google/android/play/core/tasks/TaskWrapper;->getTask()Lcom/google/android/play/core/tasks/Task;

    move-result-object p0

    goto/32 :goto_4

    nop

    :goto_c
    const/4 v3, 0x0

    goto/32 :goto_6

    nop

    :goto_d
    iget-object v1, p0, Lcom/google/android/play/core/splitinstall/SplitInstallService;->mSplitRemoteManager:Lcom/google/android/play/core/remote/RemoteManager;

    goto/32 :goto_8

    nop

    :goto_e
    invoke-direct {v0}, Lcom/google/android/play/core/tasks/TaskWrapper;-><init>()V

    goto/32 :goto_d

    nop

    :goto_f
    sget-object v0, Lcom/google/android/play/core/splitinstall/SplitInstallService;->playCore:Lcom/google/android/play/core/splitcompat/util/PlayCore;

    goto/32 :goto_0

    nop
.end method

.method getSessionStates()Lcom/google/android/play/core/tasks/Task;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/play/core/tasks/Task<",
            "Ljava/util/List<",
            "Lcom/google/android/play/core/splitinstall/SplitInstallSessionState;",
            ">;>;"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    invoke-direct {v2, p0, v0, v0}, Lcom/google/android/play/core/splitinstall/GetSessionStatesTask;-><init>(Lcom/google/android/play/core/splitinstall/SplitInstallService;Lcom/google/android/play/core/tasks/TaskWrapper;Lcom/google/android/play/core/tasks/TaskWrapper;)V

    goto/32 :goto_4

    nop

    :goto_1
    sget-object v0, Lcom/google/android/play/core/splitinstall/SplitInstallService;->playCore:Lcom/google/android/play/core/splitcompat/util/PlayCore;

    goto/32 :goto_b

    nop

    :goto_2
    new-array v1, v1, [Ljava/lang/Object;

    goto/32 :goto_9

    nop

    :goto_3
    new-instance v2, Lcom/google/android/play/core/splitinstall/GetSessionStatesTask;

    goto/32 :goto_0

    nop

    :goto_4
    invoke-virtual {v1, v2}, Lcom/google/android/play/core/remote/RemoteManager;->bindService(Lcom/google/android/play/core/remote/RemoteTask;)V

    goto/32 :goto_a

    nop

    :goto_5
    new-instance v0, Lcom/google/android/play/core/tasks/TaskWrapper;

    goto/32 :goto_7

    nop

    :goto_6
    invoke-virtual {v0, v2, v1}, Lcom/google/android/play/core/splitcompat/util/PlayCore;->info(Ljava/lang/String;[Ljava/lang/Object;)I

    goto/32 :goto_5

    nop

    :goto_7
    invoke-direct {v0}, Lcom/google/android/play/core/tasks/TaskWrapper;-><init>()V

    goto/32 :goto_8

    nop

    :goto_8
    iget-object v1, p0, Lcom/google/android/play/core/splitinstall/SplitInstallService;->mSplitRemoteManager:Lcom/google/android/play/core/remote/RemoteManager;

    goto/32 :goto_3

    nop

    :goto_9
    const-string v2, "getSessionStates"

    goto/32 :goto_6

    nop

    :goto_a
    invoke-virtual {v0}, Lcom/google/android/play/core/tasks/TaskWrapper;->getTask()Lcom/google/android/play/core/tasks/Task;

    move-result-object p0

    goto/32 :goto_c

    nop

    :goto_b
    const/4 v1, 0x0

    goto/32 :goto_2

    nop

    :goto_c
    return-object p0
.end method

.method onBinderDied()V
    .locals 3

    goto/32 :goto_15

    nop

    :goto_0
    const-string/jumbo v1, "session_id"

    goto/32 :goto_1c

    nop

    :goto_1
    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto/32 :goto_a

    nop

    :goto_2
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/32 :goto_11

    nop

    :goto_3
    const-string v1, "error_code"

    goto/32 :goto_5

    nop

    :goto_4
    const/high16 v0, 0x40000000    # 2.0f

    goto/32 :goto_1

    nop

    :goto_5
    const/16 v2, -0x9

    goto/32 :goto_2

    nop

    :goto_6
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/32 :goto_3

    nop

    :goto_7
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/32 :goto_18

    nop

    :goto_8
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto/32 :goto_e

    nop

    :goto_9
    const/4 v2, 0x6

    goto/32 :goto_6

    nop

    :goto_a
    const/high16 v0, 0x200000

    goto/32 :goto_13

    nop

    :goto_b
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    goto/32 :goto_4

    nop

    :goto_c
    new-array v1, v1, [Ljava/lang/Object;

    goto/32 :goto_16

    nop

    :goto_d
    invoke-virtual {p0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/32 :goto_10

    nop

    :goto_e
    const-string v2, "com.google.android.play.core.splitinstall.receiver.SplitInstallUpdateIntentService"

    goto/32 :goto_1a

    nop

    :goto_f
    invoke-virtual {v0, v2, v1}, Lcom/google/android/play/core/splitcompat/util/PlayCore;->info(Ljava/lang/String;[Ljava/lang/Object;)I

    goto/32 :goto_12

    nop

    :goto_10
    return-void

    :goto_11
    new-instance v1, Landroid/content/Intent;

    goto/32 :goto_14

    nop

    :goto_12
    new-instance v0, Landroid/os/Bundle;

    goto/32 :goto_1e

    nop

    :goto_13
    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto/32 :goto_17

    nop

    :goto_14
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    goto/32 :goto_1b

    nop

    :goto_15
    sget-object v0, Lcom/google/android/play/core/splitinstall/SplitInstallService;->playCore:Lcom/google/android/play/core/splitcompat/util/PlayCore;

    goto/32 :goto_19

    nop

    :goto_16
    const-string v2, "onBinderDied"

    goto/32 :goto_f

    nop

    :goto_17
    iget-object p0, p0, Lcom/google/android/play/core/splitinstall/SplitInstallService;->mContext:Landroid/content/Context;

    goto/32 :goto_d

    nop

    :goto_18
    const-string/jumbo v1, "status"

    goto/32 :goto_9

    nop

    :goto_19
    const/4 v1, 0x0

    goto/32 :goto_c

    nop

    :goto_1a
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto/32 :goto_1d

    nop

    :goto_1b
    iget-object v2, p0, Lcom/google/android/play/core/splitinstall/SplitInstallService;->mPackageName:Ljava/lang/String;

    goto/32 :goto_8

    nop

    :goto_1c
    const/4 v2, -0x1

    goto/32 :goto_7

    nop

    :goto_1d
    const-string/jumbo v2, "session_state"

    goto/32 :goto_b

    nop

    :goto_1e
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto/32 :goto_0

    nop
.end method

.method startInstall(Ljava/util/List;)Lcom/google/android/play/core/tasks/Task;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/play/core/tasks/Task<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    aput-object p1, v1, v2

    goto/32 :goto_9

    nop

    :goto_1
    sget-object v0, Lcom/google/android/play/core/splitinstall/SplitInstallService;->playCore:Lcom/google/android/play/core/splitcompat/util/PlayCore;

    goto/32 :goto_7

    nop

    :goto_2
    new-array v1, v1, [Ljava/lang/Object;

    goto/32 :goto_b

    nop

    :goto_3
    return-object p0

    :goto_4
    invoke-direct {v0}, Lcom/google/android/play/core/tasks/TaskWrapper;-><init>()V

    goto/32 :goto_a

    nop

    :goto_5
    invoke-virtual {v0, v2, v1}, Lcom/google/android/play/core/splitcompat/util/PlayCore;->info(Ljava/lang/String;[Ljava/lang/Object;)I

    goto/32 :goto_d

    nop

    :goto_6
    invoke-direct {v2, p0, v0, p1, v0}, Lcom/google/android/play/core/splitinstall/StartInstallTask;-><init>(Lcom/google/android/play/core/splitinstall/SplitInstallService;Lcom/google/android/play/core/tasks/TaskWrapper;Ljava/util/List;Lcom/google/android/play/core/tasks/TaskWrapper;)V

    goto/32 :goto_8

    nop

    :goto_7
    const/4 v1, 0x1

    goto/32 :goto_2

    nop

    :goto_8
    invoke-virtual {v1, v2}, Lcom/google/android/play/core/remote/RemoteManager;->bindService(Lcom/google/android/play/core/remote/RemoteTask;)V

    goto/32 :goto_e

    nop

    :goto_9
    const-string/jumbo v2, "startInstall(%s)"

    goto/32 :goto_5

    nop

    :goto_a
    iget-object v1, p0, Lcom/google/android/play/core/splitinstall/SplitInstallService;->mSplitRemoteManager:Lcom/google/android/play/core/remote/RemoteManager;

    goto/32 :goto_c

    nop

    :goto_b
    const/4 v2, 0x0

    goto/32 :goto_0

    nop

    :goto_c
    new-instance v2, Lcom/google/android/play/core/splitinstall/StartInstallTask;

    goto/32 :goto_6

    nop

    :goto_d
    new-instance v0, Lcom/google/android/play/core/tasks/TaskWrapper;

    goto/32 :goto_4

    nop

    :goto_e
    invoke-virtual {v0}, Lcom/google/android/play/core/tasks/TaskWrapper;->getTask()Lcom/google/android/play/core/tasks/Task;

    move-result-object p0

    goto/32 :goto_3

    nop
.end method
