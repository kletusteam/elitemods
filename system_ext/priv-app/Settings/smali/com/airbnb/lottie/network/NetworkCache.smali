.class Lcom/airbnb/lottie/network/NetworkCache;
.super Ljava/lang/Object;


# instance fields
.field private final appContext:Landroid/content/Context;

.field private final url:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/airbnb/lottie/network/NetworkCache;->appContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/airbnb/lottie/network/NetworkCache;->url:Ljava/lang/String;

    return-void
.end method

.method private static filenameForUrl(Ljava/lang/String;Lcom/airbnb/lottie/network/FileExtension;Z)Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "lottie_cache_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\\W+"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/airbnb/lottie/network/FileExtension;->tempExtension()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    iget-object p0, p1, Lcom/airbnb/lottie/network/FileExtension;->extension:Ljava/lang/String;

    :goto_0
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private getCachedFile(Ljava/lang/String;)Ljava/io/File;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/airbnb/lottie/network/NetworkCache;->appContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    sget-object v2, Lcom/airbnb/lottie/network/FileExtension;->JSON:Lcom/airbnb/lottie/network/FileExtension;

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, Lcom/airbnb/lottie/network/NetworkCache;->filenameForUrl(Ljava/lang/String;Lcom/airbnb/lottie/network/FileExtension;Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object p0, p0, Lcom/airbnb/lottie/network/NetworkCache;->appContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object p0

    sget-object v1, Lcom/airbnb/lottie/network/FileExtension;->ZIP:Lcom/airbnb/lottie/network/FileExtension;

    invoke-static {p1, v1, v3}, Lcom/airbnb/lottie/network/NetworkCache;->filenameForUrl(Ljava/lang/String;Lcom/airbnb/lottie/network/FileExtension;Z)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p0

    if-eqz p0, :cond_1

    return-object v0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method fetch()Landroidx/core/util/Pair;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/core/util/Pair<",
            "Lcom/airbnb/lottie/network/FileExtension;",
            "Ljava/io/InputStream;",
            ">;"
        }
    .end annotation

    goto/32 :goto_6

    nop

    :goto_0
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_3

    nop

    :goto_1
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_19

    nop

    :goto_2
    sget-object v0, Lcom/airbnb/lottie/network/FileExtension;->ZIP:Lcom/airbnb/lottie/network/FileExtension;

    goto/32 :goto_f

    nop

    :goto_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_e

    nop

    :goto_4
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop

    :goto_5
    if-eqz v1, :cond_0

    goto/32 :goto_15

    :cond_0
    goto/32 :goto_14

    nop

    :goto_6
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/airbnb/lottie/network/NetworkCache;->url:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/airbnb/lottie/network/NetworkCache;->getCachedFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_5

    nop

    :goto_7
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_0

    nop

    :goto_8
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_9
    const-string v3, ".zip"

    goto/32 :goto_13

    nop

    :goto_a
    sget-object v0, Lcom/airbnb/lottie/network/FileExtension;->JSON:Lcom/airbnb/lottie/network/FileExtension;

    :goto_b
    goto/32 :goto_4

    nop

    :goto_c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_12

    nop

    :goto_d
    invoke-direct {p0, v0, v2}, Landroidx/core/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/32 :goto_11

    nop

    :goto_e
    invoke-static {p0}, Lcom/airbnb/lottie/utils/Logger;->debug(Ljava/lang/String;)V

    goto/32 :goto_1b

    nop

    :goto_f
    goto :goto_b

    :goto_10
    goto/32 :goto_a

    nop

    :goto_11
    return-object p0

    :catch_0
    goto/32 :goto_1c

    nop

    :goto_12
    iget-object p0, p0, Lcom/airbnb/lottie/network/NetworkCache;->url:Ljava/lang/String;

    goto/32 :goto_18

    nop

    :goto_13
    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    goto/32 :goto_1a

    nop

    :goto_14
    return-object v0

    :goto_15
    :try_start_1
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/32 :goto_8

    nop

    :goto_16
    const-string p0, " at "

    goto/32 :goto_17

    nop

    :goto_17
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_7

    nop

    :goto_18
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_16

    nop

    :goto_19
    const-string v4, "Cache hit for "

    goto/32 :goto_c

    nop

    :goto_1a
    if-nez v0, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_2

    nop

    :goto_1b
    new-instance p0, Landroidx/core/util/Pair;

    goto/32 :goto_d

    nop

    :goto_1c
    return-object v0
.end method

.method renameTempFile(Lcom/airbnb/lottie/network/FileExtension;)V
    .locals 3

    goto/32 :goto_9

    nop

    :goto_0
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_12

    nop

    :goto_1
    const-string v0, " to "

    goto/32 :goto_10

    nop

    :goto_2
    invoke-direct {p1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto/32 :goto_c

    nop

    :goto_3
    invoke-direct {v0, p0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto/32 :goto_1a

    nop

    :goto_4
    if-eqz p0, :cond_0

    goto/32 :goto_13

    :cond_0
    goto/32 :goto_15

    nop

    :goto_5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_1f

    nop

    :goto_6
    invoke-static {v0, p1, v1}, Lcom/airbnb/lottie/network/NetworkCache;->filenameForUrl(Ljava/lang/String;Lcom/airbnb/lottie/network/FileExtension;Z)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_1c

    nop

    :goto_7
    new-instance p1, Ljava/io/File;

    goto/32 :goto_2

    nop

    :goto_8
    const-string v1, "Unable to rename cache file "

    goto/32 :goto_d

    nop

    :goto_9
    iget-object v0, p0, Lcom/airbnb/lottie/network/NetworkCache;->url:Ljava/lang/String;

    goto/32 :goto_19

    nop

    :goto_a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_23

    nop

    :goto_b
    const-string p1, "."

    goto/32 :goto_11

    nop

    :goto_c
    invoke-virtual {v0, p1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result p0

    goto/32 :goto_21

    nop

    :goto_d
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_18

    nop

    :goto_e
    invoke-virtual {p0, p1, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_7

    nop

    :goto_f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_5

    nop

    :goto_10
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_27

    nop

    :goto_11
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_0

    nop

    :goto_12
    invoke-static {p0}, Lcom/airbnb/lottie/utils/Logger;->warning(Ljava/lang/String;)V

    :goto_13
    goto/32 :goto_25

    nop

    :goto_14
    iget-object p0, p0, Lcom/airbnb/lottie/network/NetworkCache;->appContext:Landroid/content/Context;

    goto/32 :goto_1b

    nop

    :goto_15
    new-instance p0, Ljava/lang/StringBuilder;

    goto/32 :goto_17

    nop

    :goto_16
    const-string v1, ""

    goto/32 :goto_e

    nop

    :goto_17
    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_8

    nop

    :goto_18
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_26

    nop

    :goto_19
    const/4 v1, 0x1

    goto/32 :goto_6

    nop

    :goto_1a
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p0

    goto/32 :goto_1e

    nop

    :goto_1b
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object p0

    goto/32 :goto_3

    nop

    :goto_1c
    new-instance v0, Ljava/io/File;

    goto/32 :goto_14

    nop

    :goto_1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_20

    nop

    :goto_1e
    const-string p1, ".temp"

    goto/32 :goto_16

    nop

    :goto_1f
    invoke-static {v1}, Lcom/airbnb/lottie/utils/Logger;->debug(Ljava/lang/String;)V

    goto/32 :goto_4

    nop

    :goto_20
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_22

    nop

    :goto_21
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_a

    nop

    :goto_22
    const-string v2, ")"

    goto/32 :goto_f

    nop

    :goto_23
    const-string v2, "Copying temp file to real file ("

    goto/32 :goto_1d

    nop

    :goto_24
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_b

    nop

    :goto_25
    return-void

    :goto_26
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop

    :goto_27
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_24

    nop
.end method

.method writeTempCacheFile(Ljava/io/InputStream;Lcom/airbnb/lottie/network/FileExtension;)Ljava/io/File;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_8

    nop

    :goto_0
    throw p0

    :goto_1
    new-instance v0, Ljava/io/File;

    goto/32 :goto_a

    nop

    :goto_2
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    goto/32 :goto_0

    nop

    :goto_3
    const/4 v1, 0x1

    goto/32 :goto_4

    nop

    :goto_4
    invoke-static {v0, p2, v1}, Lcom/airbnb/lottie/network/NetworkCache;->filenameForUrl(Ljava/lang/String;Lcom/airbnb/lottie/network/FileExtension;Z)Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_1

    nop

    :goto_5
    const/16 p2, 0x400

    :try_start_0
    new-array p2, p2, [B

    :goto_6
    invoke-virtual {p1, p2}, Ljava/io/InputStream;->read([B)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {p0, p2, v2, v1}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_6

    :cond_0
    invoke-virtual {p0}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {p0}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto/32 :goto_7

    nop

    :goto_7
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    goto/32 :goto_b

    nop

    :goto_8
    iget-object v0, p0, Lcom/airbnb/lottie/network/NetworkCache;->url:Ljava/lang/String;

    goto/32 :goto_3

    nop

    :goto_9
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object p0

    goto/32 :goto_c

    nop

    :goto_a
    iget-object p0, p0, Lcom/airbnb/lottie/network/NetworkCache;->appContext:Landroid/content/Context;

    goto/32 :goto_9

    nop

    :goto_b
    return-object v0

    :catchall_0
    move-exception p2

    :try_start_2
    invoke-virtual {p0}, Ljava/io/OutputStream;->close()V

    throw p2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception p0

    goto/32 :goto_2

    nop

    :goto_c
    invoke-direct {v0, p0, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_3
    new-instance p0, Ljava/io/FileOutputStream;

    invoke-direct {p0, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto/32 :goto_5

    nop
.end method
