.class public Lcom/airbnb/lottie/animation/keyframe/IntegerKeyframeAnimation;
.super Lcom/airbnb/lottie/animation/keyframe/KeyframeAnimation;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/airbnb/lottie/animation/keyframe/KeyframeAnimation<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/value/Keyframe<",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/airbnb/lottie/animation/keyframe/KeyframeAnimation;-><init>(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public getIntValue()I
    .locals 2

    invoke-virtual {p0}, Lcom/airbnb/lottie/animation/keyframe/BaseKeyframeAnimation;->getCurrentKeyframe()Lcom/airbnb/lottie/value/Keyframe;

    move-result-object v0

    invoke-virtual {p0}, Lcom/airbnb/lottie/animation/keyframe/BaseKeyframeAnimation;->getInterpolatedCurrentKeyframeProgress()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/airbnb/lottie/animation/keyframe/IntegerKeyframeAnimation;->getIntValue(Lcom/airbnb/lottie/value/Keyframe;F)I

    move-result p0

    return p0
.end method

.method getIntValue(Lcom/airbnb/lottie/value/Keyframe;F)I
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/airbnb/lottie/value/Keyframe<",
            "Ljava/lang/Integer;",
            ">;F)I"
        }
    .end annotation

    goto/32 :goto_c

    nop

    :goto_0
    iget v2, p1, Lcom/airbnb/lottie/value/Keyframe;->startFrame:F

    goto/32 :goto_10

    nop

    :goto_1
    invoke-virtual {p1}, Lcom/airbnb/lottie/value/Keyframe;->getStartValueInt()I

    move-result p0

    goto/32 :goto_f

    nop

    :goto_2
    move v6, p2

    goto/32 :goto_a

    nop

    :goto_3
    iget-object v1, p0, Lcom/airbnb/lottie/animation/keyframe/BaseKeyframeAnimation;->valueCallback:Lcom/airbnb/lottie/value/LottieValueCallback;

    goto/32 :goto_16

    nop

    :goto_4
    const-string p1, "Missing values for keyframe."

    goto/32 :goto_15

    nop

    :goto_5
    iget-object v4, p1, Lcom/airbnb/lottie/value/Keyframe;->startValue:Ljava/lang/Object;

    goto/32 :goto_8

    nop

    :goto_6
    iget-object v0, p1, Lcom/airbnb/lottie/value/Keyframe;->endValue:Ljava/lang/Object;

    goto/32 :goto_12

    nop

    :goto_7
    new-instance p0, Ljava/lang/IllegalStateException;

    goto/32 :goto_4

    nop

    :goto_8
    iget-object v5, p1, Lcom/airbnb/lottie/value/Keyframe;->endValue:Ljava/lang/Object;

    goto/32 :goto_1b

    nop

    :goto_9
    throw p0

    :goto_a
    invoke-virtual/range {v1 .. v8}, Lcom/airbnb/lottie/value/LottieValueCallback;->getValueInternal(FFLjava/lang/Object;Ljava/lang/Object;FFF)Ljava/lang/Object;

    move-result-object p0

    goto/32 :goto_19

    nop

    :goto_b
    invoke-virtual {p0}, Lcom/airbnb/lottie/animation/keyframe/BaseKeyframeAnimation;->getProgress()F

    move-result v8

    goto/32 :goto_2

    nop

    :goto_c
    iget-object v0, p1, Lcom/airbnb/lottie/value/Keyframe;->startValue:Ljava/lang/Object;

    goto/32 :goto_1a

    nop

    :goto_d
    return p0

    :goto_e
    goto/32 :goto_7

    nop

    :goto_f
    invoke-virtual {p1}, Lcom/airbnb/lottie/value/Keyframe;->getEndValueInt()I

    move-result p1

    goto/32 :goto_14

    nop

    :goto_10
    iget-object v0, p1, Lcom/airbnb/lottie/value/Keyframe;->endFrame:Ljava/lang/Float;

    goto/32 :goto_13

    nop

    :goto_11
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    goto/32 :goto_17

    nop

    :goto_12
    if-nez v0, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_3

    nop

    :goto_13
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v3

    goto/32 :goto_5

    nop

    :goto_14
    invoke-static {p0, p1, p2}, Lcom/airbnb/lottie/utils/MiscUtils;->lerp(IIF)I

    move-result p0

    goto/32 :goto_d

    nop

    :goto_15
    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_9

    nop

    :goto_16
    if-nez v1, :cond_1

    goto/32 :goto_18

    :cond_1
    goto/32 :goto_0

    nop

    :goto_17
    return p0

    :goto_18
    goto/32 :goto_1

    nop

    :goto_19
    check-cast p0, Ljava/lang/Integer;

    goto/32 :goto_1c

    nop

    :goto_1a
    if-nez v0, :cond_2

    goto/32 :goto_e

    :cond_2
    goto/32 :goto_6

    nop

    :goto_1b
    invoke-virtual {p0}, Lcom/airbnb/lottie/animation/keyframe/BaseKeyframeAnimation;->getLinearCurrentKeyframeProgress()F

    move-result v7

    goto/32 :goto_b

    nop

    :goto_1c
    if-nez p0, :cond_3

    goto/32 :goto_18

    :cond_3
    goto/32 :goto_11

    nop
.end method

.method getValue(Lcom/airbnb/lottie/value/Keyframe;F)Ljava/lang/Integer;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/airbnb/lottie/value/Keyframe<",
            "Ljava/lang/Integer;",
            ">;F)",
            "Ljava/lang/Integer;"
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    return-object p0

    :goto_1
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    goto/32 :goto_0

    nop

    :goto_2
    invoke-virtual {p0, p1, p2}, Lcom/airbnb/lottie/animation/keyframe/IntegerKeyframeAnimation;->getIntValue(Lcom/airbnb/lottie/value/Keyframe;F)I

    move-result p0

    goto/32 :goto_1

    nop
.end method

.method bridge synthetic getValue(Lcom/airbnb/lottie/value/Keyframe;F)Ljava/lang/Object;
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-object p0

    :goto_1
    invoke-virtual {p0, p1, p2}, Lcom/airbnb/lottie/animation/keyframe/IntegerKeyframeAnimation;->getValue(Lcom/airbnb/lottie/value/Keyframe;F)Ljava/lang/Integer;

    move-result-object p0

    goto/32 :goto_0

    nop
.end method
