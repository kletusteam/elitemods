.class public final Landroidx/mediarouter/R$string;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final mr_cast_button_connected:I = 0x7f121786

.field public static final mr_cast_button_connecting:I = 0x7f121787

.field public static final mr_cast_button_disconnected:I = 0x7f121788

.field public static final mr_cast_dialog_title_view_placeholder:I = 0x7f121789

.field public static final mr_chooser_title:I = 0x7f12178b

.field public static final mr_controller_casting_screen:I = 0x7f12178d

.field public static final mr_controller_collapse_group:I = 0x7f12178f

.field public static final mr_controller_disconnect:I = 0x7f121790

.field public static final mr_controller_expand_group:I = 0x7f121791

.field public static final mr_controller_no_info_available:I = 0x7f121792

.field public static final mr_controller_no_media_selected:I = 0x7f121793

.field public static final mr_controller_pause:I = 0x7f121794

.field public static final mr_controller_play:I = 0x7f121795

.field public static final mr_controller_stop:I = 0x7f121796

.field public static final mr_controller_stop_casting:I = 0x7f121797

.field public static final mr_dialog_default_group_name:I = 0x7f121799

.field public static final mr_dialog_groupable_header:I = 0x7f12179a

.field public static final mr_dialog_transferable_header:I = 0x7f12179b

.field public static final mr_user_route_category_name:I = 0x7f12179d
