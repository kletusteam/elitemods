.class public final Landroidx/mediarouter/R$id;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final mr_art:I = 0x7f0a0688

.field public static final mr_cast_checkbox:I = 0x7f0a0689

.field public static final mr_cast_close_button:I = 0x7f0a068a

.field public static final mr_cast_group_icon:I = 0x7f0a068c

.field public static final mr_cast_group_name:I = 0x7f0a068d

.field public static final mr_cast_group_progress_bar:I = 0x7f0a068e

.field public static final mr_cast_header_name:I = 0x7f0a068f

.field public static final mr_cast_list:I = 0x7f0a0690

.field public static final mr_cast_meta_art:I = 0x7f0a0691

.field public static final mr_cast_meta_background:I = 0x7f0a0692

.field public static final mr_cast_meta_black_scrim:I = 0x7f0a0693

.field public static final mr_cast_meta_subtitle:I = 0x7f0a0694

.field public static final mr_cast_meta_title:I = 0x7f0a0695

.field public static final mr_cast_mute_button:I = 0x7f0a0696

.field public static final mr_cast_route_icon:I = 0x7f0a0697

.field public static final mr_cast_route_name:I = 0x7f0a0698

.field public static final mr_cast_route_progress_bar:I = 0x7f0a0699

.field public static final mr_cast_stop_button:I = 0x7f0a069a

.field public static final mr_cast_volume_layout:I = 0x7f0a069b

.field public static final mr_cast_volume_slider:I = 0x7f0a069c

.field public static final mr_chooser_list:I = 0x7f0a069d

.field public static final mr_chooser_route_desc:I = 0x7f0a069e

.field public static final mr_chooser_route_icon:I = 0x7f0a069f

.field public static final mr_chooser_route_name:I = 0x7f0a06a0

.field public static final mr_chooser_route_progress_bar:I = 0x7f0a06a1

.field public static final mr_chooser_title:I = 0x7f0a06a2

.field public static final mr_close:I = 0x7f0a06a3

.field public static final mr_control_divider:I = 0x7f0a06a4

.field public static final mr_control_playback_ctrl:I = 0x7f0a06a5

.field public static final mr_control_subtitle:I = 0x7f0a06a6

.field public static final mr_control_title:I = 0x7f0a06a7

.field public static final mr_control_title_container:I = 0x7f0a06a8

.field public static final mr_custom_control:I = 0x7f0a06a9

.field public static final mr_default_control:I = 0x7f0a06aa

.field public static final mr_dialog_area:I = 0x7f0a06ab

.field public static final mr_expandable_area:I = 0x7f0a06ac

.field public static final mr_group_expand_collapse:I = 0x7f0a06ad

.field public static final mr_group_volume_route_name:I = 0x7f0a06ae

.field public static final mr_media_main_control:I = 0x7f0a06af

.field public static final mr_name:I = 0x7f0a06b0

.field public static final mr_picker_close_button:I = 0x7f0a06b1

.field public static final mr_picker_header_name:I = 0x7f0a06b2

.field public static final mr_picker_list:I = 0x7f0a06b3

.field public static final mr_picker_route_icon:I = 0x7f0a06b4

.field public static final mr_picker_route_name:I = 0x7f0a06b5

.field public static final mr_picker_route_progress_bar:I = 0x7f0a06b6

.field public static final mr_playback_control:I = 0x7f0a06b7

.field public static final mr_volume_control:I = 0x7f0a06b9

.field public static final mr_volume_group_list:I = 0x7f0a06ba

.field public static final mr_volume_item_icon:I = 0x7f0a06bb

.field public static final mr_volume_slider:I = 0x7f0a06bc

.field public static final volume_item_container:I = 0x7f0a0b81
