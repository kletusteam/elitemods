.class public Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;
.super Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator;


# static fields
.field public static sAttachedListener:Landroid/view/View$OnAttachStateChangeListener;

.field public static sSpeedConfig:Lmiuix/animation/base/AnimConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator$1;

    invoke-direct {v0}, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator$1;-><init>()V

    sput-object v0, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->sAttachedListener:Landroid/view/View$OnAttachStateChangeListener;

    new-instance v0, Lmiuix/animation/base/AnimConfig;

    invoke-direct {v0}, Lmiuix/animation/base/AnimConfig;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiuix/animation/base/AnimConfig;->setFromSpeed(F)Lmiuix/animation/base/AnimConfig;

    move-result-object v0

    sput-object v0, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->sSpeedConfig:Lmiuix/animation/base/AnimConfig;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator;-><init>()V

    return-void
.end method


# virtual methods
.method animateAddImpl(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 8

    goto/32 :goto_11

    nop

    :goto_0
    const/high16 v5, 0x3f800000    # 1.0f

    goto/32 :goto_1d

    nop

    :goto_1
    aput-object v6, v2, v7

    goto/32 :goto_2

    nop

    :goto_2
    invoke-interface {v1, v2}, Lmiuix/animation/IStateStyle;->to([Ljava/lang/Object;)Lmiuix/animation/IStateStyle;

    goto/32 :goto_8

    nop

    :goto_3
    new-array v2, v2, [Ljava/lang/Object;

    goto/32 :goto_1b

    nop

    :goto_4
    return-void

    :goto_5
    aput-object v2, v1, v3

    goto/32 :goto_17

    nop

    :goto_6
    aput-object v5, v2, v0

    goto/32 :goto_e

    nop

    :goto_7
    const/4 v0, 0x1

    goto/32 :goto_15

    nop

    :goto_8
    new-array v1, v0, [Landroid/view/View;

    goto/32 :goto_1f

    nop

    :goto_9
    const/4 v2, 0x3

    goto/32 :goto_3

    nop

    :goto_a
    invoke-interface {v1}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_b
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_19

    nop

    :goto_c
    new-instance v3, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator$4;

    goto/32 :goto_18

    nop

    :goto_d
    invoke-virtual {v2, v3, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/32 :goto_4

    nop

    :goto_e
    sget-object v6, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->sSpeedConfig:Lmiuix/animation/base/AnimConfig;

    goto/32 :goto_12

    nop

    :goto_f
    aput-object v5, v2, v0

    goto/32 :goto_20

    nop

    :goto_10
    invoke-interface {v1}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v1

    goto/32 :goto_14

    nop

    :goto_11
    invoke-virtual {p0, p1}, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator;->notifyAddStarting(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_7

    nop

    :goto_12
    const/4 v7, 0x2

    goto/32 :goto_1

    nop

    :goto_13
    aput-object v4, v2, v3

    goto/32 :goto_0

    nop

    :goto_14
    new-array v2, v7, [Ljava/lang/Object;

    goto/32 :goto_1c

    nop

    :goto_15
    new-array v1, v0, [Landroid/view/View;

    goto/32 :goto_b

    nop

    :goto_16
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_c

    nop

    :goto_17
    invoke-static {v1}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v1

    goto/32 :goto_10

    nop

    :goto_18
    invoke-direct {v3, p0, p1}, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator$4;-><init>(Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_d

    nop

    :goto_19
    const/4 v3, 0x0

    goto/32 :goto_1a

    nop

    :goto_1a
    aput-object v2, v1, v3

    goto/32 :goto_1e

    nop

    :goto_1b
    sget-object v4, Lmiuix/animation/property/ViewProperty;->ALPHA:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_13

    nop

    :goto_1c
    aput-object v4, v2, v3

    goto/32 :goto_f

    nop

    :goto_1d
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    goto/32 :goto_6

    nop

    :goto_1e
    invoke-static {v1}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v1

    goto/32 :goto_a

    nop

    :goto_1f
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_5

    nop

    :goto_20
    invoke-interface {v1, v2}, Lmiuix/animation/IStateStyle;->predictDuration([Ljava/lang/Object;)J

    move-result-wide v0

    goto/32 :goto_16

    nop
.end method

.method animateChangeImpl(Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;)V
    .locals 16

    goto/32 :goto_53

    nop

    :goto_0
    invoke-interface {v1, v2}, Lmiuix/animation/IStateStyle;->to([Ljava/lang/Object;)Lmiuix/animation/IStateStyle;

    goto/32 :goto_40

    nop

    :goto_1
    invoke-static {v9}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v9

    goto/32 :goto_2e

    nop

    :goto_2
    invoke-static {v12}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v12

    goto/32 :goto_3b

    nop

    :goto_3
    aput-object v14, v13, v11

    goto/32 :goto_9

    nop

    :goto_4
    iget v14, v1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->fromX:I

    goto/32 :goto_50

    nop

    :goto_5
    aput-object v3, v1, v11

    goto/32 :goto_8

    nop

    :goto_6
    new-instance v1, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator$5;

    goto/32 :goto_3f

    nop

    :goto_7
    invoke-virtual {v0, v5, v11}, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator;->notifyChangeStarting(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Z)V

    goto/32 :goto_2d

    nop

    :goto_8
    invoke-static {v1}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v1

    goto/32 :goto_68

    nop

    :goto_9
    iget v15, v1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->toX:I

    goto/32 :goto_69

    nop

    :goto_a
    if-nez v5, :cond_0

    goto/32 :goto_62

    :cond_0
    goto/32 :goto_61

    nop

    :goto_b
    aput-object v6, v2, v11

    goto/32 :goto_48

    nop

    :goto_c
    aput-object v6, v13, v10

    goto/32 :goto_22

    nop

    :goto_d
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto/32 :goto_64

    nop

    :goto_e
    aput-object v14, v12, v11

    goto/32 :goto_2b

    nop

    :goto_f
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto/32 :goto_c

    nop

    :goto_10
    sub-int/2addr v6, v1

    goto/32 :goto_4c

    nop

    :goto_11
    aput-object v3, v1, v11

    goto/32 :goto_2f

    nop

    :goto_12
    iget v9, v1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->fromY:I

    goto/32 :goto_18

    nop

    :goto_13
    aput-object v9, v2, v6

    goto/32 :goto_d

    nop

    :goto_14
    if-nez v3, :cond_1

    goto/32 :goto_4b

    :cond_1
    goto/32 :goto_7

    nop

    :goto_15
    sub-int/2addr v15, v6

    goto/32 :goto_f

    nop

    :goto_16
    invoke-direct {v6, v0, v4, v5}, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator$6;-><init>(Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_4a

    nop

    :goto_17
    const/4 v7, 0x4

    goto/32 :goto_36

    nop

    :goto_18
    sub-int/2addr v15, v9

    goto/32 :goto_51

    nop

    :goto_19
    new-array v12, v10, [Landroid/view/View;

    goto/32 :goto_1d

    nop

    :goto_1a
    const/4 v2, 0x5

    goto/32 :goto_43

    nop

    :goto_1b
    aput-object v12, v2, v8

    goto/32 :goto_35

    nop

    :goto_1c
    invoke-interface {v9, v12}, Lmiuix/animation/IStateStyle;->predictDuration([Ljava/lang/Object;)J

    move-result-wide v12

    goto/32 :goto_6

    nop

    :goto_1d
    aput-object v4, v12, v11

    goto/32 :goto_2

    nop

    :goto_1e
    sget-object v12, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->sAttachedListener:Landroid/view/View$OnAttachStateChangeListener;

    goto/32 :goto_5a

    nop

    :goto_1f
    sget-object v9, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->sSpeedConfig:Lmiuix/animation/base/AnimConfig;

    goto/32 :goto_47

    nop

    :goto_20
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    goto/32 :goto_59

    nop

    :goto_21
    const/4 v12, 0x2

    goto/32 :goto_38

    nop

    :goto_22
    sget-object v6, Lmiuix/animation/property/ViewProperty;->TRANSLATION_Y:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_54

    nop

    :goto_23
    aput-object v6, v2, v11

    goto/32 :goto_39

    nop

    :goto_24
    aput-object v12, v2, v7

    goto/32 :goto_0

    nop

    :goto_25
    sget-object v14, Lmiuix/animation/property/ViewProperty;->TRANSLATION_X:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_3

    nop

    :goto_26
    aput-object v1, v12, v8

    goto/32 :goto_1c

    nop

    :goto_27
    invoke-interface {v1}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v1

    goto/32 :goto_1a

    nop

    :goto_28
    invoke-virtual {v4, v1, v12, v13}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_29
    goto/32 :goto_14

    nop

    :goto_2a
    if-nez v4, :cond_2

    goto/32 :goto_29

    :cond_2
    goto/32 :goto_58

    nop

    :goto_2b
    iget v13, v1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->toX:I

    goto/32 :goto_4

    nop

    :goto_2c
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    goto/32 :goto_1b

    nop

    :goto_2d
    new-array v1, v10, [Landroid/view/View;

    goto/32 :goto_11

    nop

    :goto_2e
    invoke-interface {v9}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v9

    goto/32 :goto_5d

    nop

    :goto_2f
    invoke-static {v1}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v1

    goto/32 :goto_27

    nop

    :goto_30
    goto/16 :goto_4f

    :goto_31
    goto/32 :goto_4e

    nop

    :goto_32
    new-array v2, v7, [Ljava/lang/Object;

    goto/32 :goto_b

    nop

    :goto_33
    sget-object v6, Lmiuix/animation/property/ViewProperty;->TRANSLATION_X:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_23

    nop

    :goto_34
    sget-object v9, Lmiuix/animation/property/ViewProperty;->TRANSLATION_Y:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_21

    nop

    :goto_35
    sget-object v12, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->sSpeedConfig:Lmiuix/animation/base/AnimConfig;

    goto/32 :goto_24

    nop

    :goto_36
    const/4 v8, 0x3

    goto/32 :goto_3e

    nop

    :goto_37
    if-eqz v2, :cond_3

    goto/32 :goto_31

    :cond_3
    goto/32 :goto_41

    nop

    :goto_38
    aput-object v9, v2, v12

    goto/32 :goto_2c

    nop

    :goto_39
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    goto/32 :goto_67

    nop

    :goto_3a
    iget v6, v1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->toY:I

    goto/32 :goto_3d

    nop

    :goto_3b
    invoke-interface {v12}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v12

    goto/32 :goto_42

    nop

    :goto_3c
    const/4 v11, 0x0

    goto/32 :goto_2a

    nop

    :goto_3d
    iget v1, v1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->fromY:I

    goto/32 :goto_10

    nop

    :goto_3e
    const/4 v9, 0x2

    goto/32 :goto_44

    nop

    :goto_3f
    invoke-direct {v1, v0, v4, v2}, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator$5;-><init>(Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_28

    nop

    :goto_40
    new-array v1, v10, [Landroid/view/View;

    goto/32 :goto_5

    nop

    :goto_41
    move-object v4, v3

    goto/32 :goto_30

    nop

    :goto_42
    new-array v13, v6, [Ljava/lang/Object;

    goto/32 :goto_25

    nop

    :goto_43
    new-array v2, v2, [Ljava/lang/Object;

    goto/32 :goto_33

    nop

    :goto_44
    const/4 v10, 0x1

    goto/32 :goto_3c

    nop

    :goto_45
    new-array v9, v10, [Landroid/view/View;

    goto/32 :goto_63

    nop

    :goto_46
    iget-object v2, v1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->oldHolder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_66

    nop

    :goto_47
    aput-object v9, v13, v7

    goto/32 :goto_6a

    nop

    :goto_48
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto/32 :goto_49

    nop

    :goto_49
    aput-object v6, v2, v10

    goto/32 :goto_52

    nop

    :goto_4a
    invoke-virtual {v3, v6, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_4b
    goto/32 :goto_55

    nop

    :goto_4c
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_26

    nop

    :goto_4d
    aput-object v9, v13, v8

    goto/32 :goto_1f

    nop

    :goto_4e
    iget-object v4, v2, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    :goto_4f
    goto/32 :goto_56

    nop

    :goto_50
    sub-int/2addr v13, v14

    goto/32 :goto_20

    nop

    :goto_51
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    goto/32 :goto_4d

    nop

    :goto_52
    const/4 v6, 0x2

    goto/32 :goto_13

    nop

    :goto_53
    move-object/from16 v0, p0

    goto/32 :goto_5e

    nop

    :goto_54
    aput-object v6, v13, v9

    goto/32 :goto_5b

    nop

    :goto_55
    return-void

    :goto_56
    iget-object v5, v1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->newHolder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_a

    nop

    :goto_57
    invoke-interface {v1, v2}, Lmiuix/animation/IStateStyle;->predictDuration([Ljava/lang/Object;)J

    move-result-wide v1

    goto/32 :goto_5c

    nop

    :goto_58
    invoke-virtual {v0, v2, v10}, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator;->notifyChangeStarting(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Z)V

    goto/32 :goto_1e

    nop

    :goto_59
    aput-object v13, v12, v10

    goto/32 :goto_5f

    nop

    :goto_5a
    invoke-virtual {v4, v12}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    goto/32 :goto_19

    nop

    :goto_5b
    iget v15, v1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->toY:I

    goto/32 :goto_12

    nop

    :goto_5c
    new-instance v6, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator$6;

    goto/32 :goto_16

    nop

    :goto_5d
    new-array v12, v7, [Ljava/lang/Object;

    goto/32 :goto_e

    nop

    :goto_5e
    move-object/from16 v1, p1

    goto/32 :goto_46

    nop

    :goto_5f
    const/4 v13, 0x2

    goto/32 :goto_60

    nop

    :goto_60
    aput-object v6, v12, v13

    goto/32 :goto_3a

    nop

    :goto_61
    iget-object v3, v5, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    :goto_62
    goto/32 :goto_65

    nop

    :goto_63
    aput-object v4, v9, v11

    goto/32 :goto_1

    nop

    :goto_64
    aput-object v6, v2, v8

    goto/32 :goto_57

    nop

    :goto_65
    const/4 v6, 0x5

    goto/32 :goto_17

    nop

    :goto_66
    const/4 v3, 0x0

    goto/32 :goto_37

    nop

    :goto_67
    aput-object v9, v2, v10

    goto/32 :goto_34

    nop

    :goto_68
    invoke-interface {v1}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v1

    goto/32 :goto_32

    nop

    :goto_69
    iget v6, v1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->fromX:I

    goto/32 :goto_15

    nop

    :goto_6a
    invoke-interface {v12, v13}, Lmiuix/animation/IStateStyle;->to([Ljava/lang/Object;)Lmiuix/animation/IStateStyle;

    goto/32 :goto_45

    nop
.end method

.method animateMoveImpl(Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;)V
    .locals 12

    goto/32 :goto_20

    nop

    :goto_0
    aput-object v6, v3, v4

    goto/32 :goto_8

    nop

    :goto_1
    aput-object v7, v3, v8

    goto/32 :goto_b

    nop

    :goto_2
    new-instance v3, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator$3;

    goto/32 :goto_1c

    nop

    :goto_3
    sget-object v10, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->sSpeedConfig:Lmiuix/animation/base/AnimConfig;

    goto/32 :goto_26

    nop

    :goto_4
    aput-object v3, v2, v4

    goto/32 :goto_1e

    nop

    :goto_5
    sget-object v6, Lmiuix/animation/property/ViewProperty;->TRANSLATION_X:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_0

    nop

    :goto_6
    new-array v2, v1, [Landroid/view/View;

    goto/32 :goto_a

    nop

    :goto_7
    new-array v2, v1, [Landroid/view/View;

    goto/32 :goto_19

    nop

    :goto_8
    aput-object v5, v3, v1

    goto/32 :goto_1d

    nop

    :goto_9
    aput-object v3, v2, v4

    goto/32 :goto_27

    nop

    :goto_a
    iget-object v3, v0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_15

    nop

    :goto_b
    const/4 v9, 0x3

    goto/32 :goto_11

    nop

    :goto_c
    iget-object v0, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;->holder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_13

    nop

    :goto_d
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_2

    nop

    :goto_e
    invoke-interface {v2}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v2

    goto/32 :goto_1b

    nop

    :goto_f
    aput-object v6, v3, v4

    goto/32 :goto_17

    nop

    :goto_10
    new-array v3, v11, [Ljava/lang/Object;

    goto/32 :goto_f

    nop

    :goto_11
    aput-object v5, v3, v9

    goto/32 :goto_3

    nop

    :goto_12
    invoke-interface {v2, v3}, Lmiuix/animation/IStateStyle;->to([Ljava/lang/Object;)Lmiuix/animation/IStateStyle;

    goto/32 :goto_7

    nop

    :goto_13
    const/4 v1, 0x1

    goto/32 :goto_6

    nop

    :goto_14
    invoke-interface {v2, v3}, Lmiuix/animation/IStateStyle;->predictDuration([Ljava/lang/Object;)J

    move-result-wide v1

    goto/32 :goto_22

    nop

    :goto_15
    const/4 v4, 0x0

    goto/32 :goto_2a

    nop

    :goto_16
    aput-object v7, v3, v8

    goto/32 :goto_29

    nop

    :goto_17
    aput-object v5, v3, v1

    goto/32 :goto_16

    nop

    :goto_18
    new-array v3, v3, [Ljava/lang/Object;

    goto/32 :goto_5

    nop

    :goto_19
    iget-object v3, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;->holder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_1f

    nop

    :goto_1a
    aput-object v10, v3, v11

    goto/32 :goto_12

    nop

    :goto_1b
    const/4 v3, 0x5

    goto/32 :goto_18

    nop

    :goto_1c
    invoke-direct {v3, p0, v0}, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator$3;-><init>(Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_23

    nop

    :goto_1d
    sget-object v7, Lmiuix/animation/property/ViewProperty;->TRANSLATION_Y:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_25

    nop

    :goto_1e
    invoke-static {v2}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v2

    goto/32 :goto_e

    nop

    :goto_1f
    iget-object v3, v3, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_9

    nop

    :goto_20
    iget-object v0, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;->holder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_21

    nop

    :goto_21
    invoke-virtual {p0, v0}, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator;->notifyMoveStarting(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_c

    nop

    :goto_22
    iget-object p1, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;->holder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_d

    nop

    :goto_23
    invoke-virtual {p1, v3, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/32 :goto_24

    nop

    :goto_24
    return-void

    :goto_25
    const/4 v8, 0x2

    goto/32 :goto_1

    nop

    :goto_26
    const/4 v11, 0x4

    goto/32 :goto_1a

    nop

    :goto_27
    invoke-static {v2}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v2

    goto/32 :goto_28

    nop

    :goto_28
    invoke-interface {v2}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v2

    goto/32 :goto_10

    nop

    :goto_29
    aput-object v5, v3, v9

    goto/32 :goto_14

    nop

    :goto_2a
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto/32 :goto_4

    nop
.end method

.method animateRemoveImpl(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 8

    goto/32 :goto_15

    nop

    :goto_0
    const/4 v7, 0x2

    goto/32 :goto_6

    nop

    :goto_1
    const/4 v2, 0x3

    goto/32 :goto_8

    nop

    :goto_2
    aput-object v4, v2, v3

    goto/32 :goto_e

    nop

    :goto_3
    aput-object v5, v2, v0

    goto/32 :goto_1f

    nop

    :goto_4
    sget-object v6, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->sSpeedConfig:Lmiuix/animation/base/AnimConfig;

    goto/32 :goto_0

    nop

    :goto_5
    new-array v1, v0, [Landroid/view/View;

    goto/32 :goto_10

    nop

    :goto_6
    aput-object v6, v2, v7

    goto/32 :goto_1a

    nop

    :goto_7
    aput-object v2, v1, v3

    goto/32 :goto_9

    nop

    :goto_8
    new-array v2, v2, [Ljava/lang/Object;

    goto/32 :goto_d

    nop

    :goto_9
    invoke-static {v1}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v1

    goto/32 :goto_1c

    nop

    :goto_a
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    goto/32 :goto_18

    nop

    :goto_b
    invoke-virtual {v2, v3, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/32 :goto_12

    nop

    :goto_c
    new-instance v3, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator$2;

    goto/32 :goto_1b

    nop

    :goto_d
    sget-object v4, Lmiuix/animation/property/ViewProperty;->ALPHA:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_2

    nop

    :goto_e
    const/4 v5, 0x0

    goto/32 :goto_a

    nop

    :goto_f
    aput-object v4, v2, v3

    goto/32 :goto_3

    nop

    :goto_10
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_1e

    nop

    :goto_11
    sget-object v1, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->sAttachedListener:Landroid/view/View$OnAttachStateChangeListener;

    goto/32 :goto_23

    nop

    :goto_12
    return-void

    :goto_13
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_c

    nop

    :goto_14
    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_11

    nop

    :goto_15
    invoke-virtual {p0, p1}, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator;->notifyRemoveStarting(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_14

    nop

    :goto_16
    invoke-interface {v1}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_17
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_7

    nop

    :goto_18
    aput-object v5, v2, v0

    goto/32 :goto_4

    nop

    :goto_19
    new-array v2, v7, [Ljava/lang/Object;

    goto/32 :goto_f

    nop

    :goto_1a
    invoke-interface {v1, v2}, Lmiuix/animation/IStateStyle;->to([Ljava/lang/Object;)Lmiuix/animation/IStateStyle;

    goto/32 :goto_1d

    nop

    :goto_1b
    invoke-direct {v3, p0, p1}, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator$2;-><init>(Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_b

    nop

    :goto_1c
    invoke-interface {v1}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v1

    goto/32 :goto_19

    nop

    :goto_1d
    new-array v1, v0, [Landroid/view/View;

    goto/32 :goto_17

    nop

    :goto_1e
    const/4 v3, 0x0

    goto/32 :goto_20

    nop

    :goto_1f
    invoke-interface {v1, v2}, Lmiuix/animation/IStateStyle;->predictDuration([Ljava/lang/Object;)J

    move-result-wide v0

    goto/32 :goto_13

    nop

    :goto_20
    aput-object v2, v1, v3

    goto/32 :goto_22

    nop

    :goto_21
    const/4 v0, 0x1

    goto/32 :goto_5

    nop

    :goto_22
    invoke-static {v1}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v1

    goto/32 :goto_16

    nop

    :goto_23
    invoke-virtual {v0, v1}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    goto/32 :goto_21

    nop
.end method

.method public getMoveDuration()J
    .locals 2

    const-wide/16 v0, 0x12c

    return-wide v0
.end method

.method public getRemoveDuration()J
    .locals 2

    const-wide/16 v0, 0x12c

    return-wide v0
.end method

.method prepareAdd(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1}, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->resetAnimation(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_1

    nop

    :goto_1
    iget-object p0, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_4

    nop

    :goto_2
    return-void

    :goto_3
    invoke-virtual {p0, p1}, Landroid/view/View;->setAlpha(F)V

    goto/32 :goto_2

    nop

    :goto_4
    const/4 p1, 0x0

    goto/32 :goto_3

    nop
.end method

.method prepareChange(Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;)V
    .locals 5

    goto/32 :goto_20

    nop

    :goto_0
    int-to-float v2, v2

    goto/32 :goto_19

    nop

    :goto_1
    invoke-virtual {v1}, Landroid/view/View;->getTranslationY()F

    move-result v1

    goto/32 :goto_c

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_10

    nop

    :goto_3
    invoke-virtual {p0, v2}, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->resetAnimation(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_21

    nop

    :goto_4
    iget-object v1, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->oldHolder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_13

    nop

    :goto_5
    invoke-virtual {p0, v0}, Landroid/view/View;->setTranslationX(F)V

    goto/32 :goto_1d

    nop

    :goto_6
    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v0

    goto/32 :goto_4

    nop

    :goto_7
    invoke-virtual {p0, p1}, Landroid/view/View;->setTranslationY(F)V

    :goto_8
    goto/32 :goto_27

    nop

    :goto_9
    iget-object p0, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->newHolder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_15

    nop

    :goto_a
    iget-object p0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_24

    nop

    :goto_b
    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    goto/32 :goto_1b

    nop

    :goto_c
    iget-object v2, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->oldHolder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_3

    nop

    :goto_d
    sub-float/2addr v3, v1

    goto/32 :goto_11

    nop

    :goto_e
    iget-object v0, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->oldHolder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_1f

    nop

    :goto_f
    iget v4, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->fromY:I

    goto/32 :goto_12

    nop

    :goto_10
    invoke-virtual {p0, v0}, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->resetAnimation(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_9

    nop

    :goto_11
    float-to-int v3, v3

    goto/32 :goto_18

    nop

    :goto_12
    sub-int/2addr v3, v4

    goto/32 :goto_16

    nop

    :goto_13
    iget-object v1, v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_1

    nop

    :goto_14
    int-to-float p1, p1

    goto/32 :goto_7

    nop

    :goto_15
    iget-object p0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_1e

    nop

    :goto_16
    int-to-float v3, v3

    goto/32 :goto_d

    nop

    :goto_17
    float-to-int v2, v2

    goto/32 :goto_22

    nop

    :goto_18
    iget-object v4, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->oldHolder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_26

    nop

    :goto_19
    sub-float/2addr v2, v0

    goto/32 :goto_17

    nop

    :goto_1a
    iget-object v0, v0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_6

    nop

    :goto_1b
    iget-object v0, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->newHolder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_2

    nop

    :goto_1c
    iget v3, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->fromX:I

    goto/32 :goto_28

    nop

    :goto_1d
    iget-object p0, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->newHolder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_a

    nop

    :goto_1e
    neg-int v0, v2

    goto/32 :goto_23

    nop

    :goto_1f
    iget-object v0, v0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_b

    nop

    :goto_20
    iget-object v0, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->oldHolder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_1a

    nop

    :goto_21
    iget v2, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->toX:I

    goto/32 :goto_1c

    nop

    :goto_22
    iget v3, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->toY:I

    goto/32 :goto_f

    nop

    :goto_23
    int-to-float v0, v0

    goto/32 :goto_5

    nop

    :goto_24
    neg-int p1, v3

    goto/32 :goto_14

    nop

    :goto_25
    invoke-virtual {v4, v0}, Landroid/view/View;->setTranslationX(F)V

    goto/32 :goto_e

    nop

    :goto_26
    iget-object v4, v4, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_25

    nop

    :goto_27
    return-void

    :goto_28
    sub-int/2addr v2, v3

    goto/32 :goto_0

    nop
.end method

.method prepareMove(Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;)V
    .locals 2

    goto/32 :goto_a

    nop

    :goto_0
    iget-object p0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_3

    nop

    :goto_1
    iget p1, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;->toY:I

    goto/32 :goto_c

    nop

    :goto_2
    iget v1, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;->toX:I

    goto/32 :goto_4

    nop

    :goto_3
    iget v0, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;->fromY:I

    goto/32 :goto_1

    nop

    :goto_4
    sub-int/2addr v0, v1

    goto/32 :goto_e

    nop

    :goto_5
    return-void

    :goto_6
    iget-object p0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_8

    nop

    :goto_7
    invoke-virtual {p0, v0}, Landroid/view/View;->setTranslationX(F)V

    goto/32 :goto_9

    nop

    :goto_8
    iget v0, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;->fromX:I

    goto/32 :goto_2

    nop

    :goto_9
    iget-object p0, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;->holder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_0

    nop

    :goto_a
    iget-object p0, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;->holder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_6

    nop

    :goto_b
    invoke-virtual {p0, p1}, Landroid/view/View;->setTranslationY(F)V

    goto/32 :goto_5

    nop

    :goto_c
    sub-int/2addr v0, p1

    goto/32 :goto_d

    nop

    :goto_d
    int-to-float p1, v0

    goto/32 :goto_b

    nop

    :goto_e
    int-to-float v0, v0

    goto/32 :goto_7

    nop
.end method

.method resetAnimation(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 4

    goto/32 :goto_15

    nop

    :goto_0
    sget-object v3, Lmiuix/animation/property/ViewProperty;->TRANSLATION_X:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_6

    nop

    :goto_1
    invoke-static {p0}, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator;->resetAnimation(Landroid/view/View;)V

    :goto_2
    goto/32 :goto_e

    nop

    :goto_3
    invoke-interface {v0, v1}, Lmiuix/animation/ICancelableStyle;->end([Ljava/lang/Object;)V

    goto/32 :goto_c

    nop

    :goto_4
    aput-object v2, v1, p0

    goto/32 :goto_b

    nop

    :goto_5
    new-array v0, p0, [Landroid/view/View;

    goto/32 :goto_11

    nop

    :goto_6
    aput-object v3, v1, v2

    goto/32 :goto_14

    nop

    :goto_7
    const/4 p0, 0x1

    goto/32 :goto_5

    nop

    :goto_8
    const/4 v2, 0x0

    goto/32 :goto_9

    nop

    :goto_9
    aput-object v1, v0, v2

    goto/32 :goto_f

    nop

    :goto_a
    const/4 v1, 0x3

    goto/32 :goto_10

    nop

    :goto_b
    const/4 p0, 0x2

    goto/32 :goto_13

    nop

    :goto_c
    iget-object p0, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_1

    nop

    :goto_d
    aput-object v2, v1, p0

    goto/32 :goto_3

    nop

    :goto_e
    return-void

    :goto_f
    invoke-static {v0}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v0

    goto/32 :goto_12

    nop

    :goto_10
    new-array v1, v1, [Ljava/lang/Object;

    goto/32 :goto_0

    nop

    :goto_11
    iget-object v1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_8

    nop

    :goto_12
    invoke-interface {v0}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v0

    goto/32 :goto_a

    nop

    :goto_13
    sget-object v2, Lmiuix/animation/property/ViewProperty;->ALPHA:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_d

    nop

    :goto_14
    sget-object v2, Lmiuix/animation/property/ViewProperty;->TRANSLATION_Y:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_4

    nop

    :goto_15
    if-nez p1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_7

    nop
.end method
