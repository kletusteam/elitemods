.class public Lmiuix/springback/view/SpringBackLayoutHelper;
.super Ljava/lang/Object;


# instance fields
.field mActivePointerId:I

.field mInitialDownX:F

.field mInitialDownY:F

.field mScrollOrientation:I

.field private mTarget:Landroid/view/ViewGroup;

.field mTargetScrollOrientation:I

.field private mTouchSlop:I


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mActivePointerId:I

    iput-object p1, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mTarget:Landroid/view/ViewGroup;

    iput p2, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mTargetScrollOrientation:I

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result p1

    iput p1, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mTouchSlop:I

    return-void
.end method


# virtual methods
.method checkOrientation(Landroid/view/MotionEvent;)V
    .locals 5

    goto/32 :goto_12

    nop

    :goto_0
    if-ne v0, v2, :cond_0

    goto/32 :goto_1d

    :cond_0
    goto/32 :goto_2e

    nop

    :goto_1
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    goto/32 :goto_25

    nop

    :goto_2
    const/4 v2, 0x1

    goto/32 :goto_0

    nop

    :goto_3
    goto :goto_7

    :goto_4
    goto/32 :goto_6

    nop

    :goto_5
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    goto/32 :goto_d

    nop

    :goto_6
    move v2, v3

    :goto_7
    goto/32 :goto_38

    nop

    :goto_8
    if-gtz v0, :cond_1

    goto/32 :goto_27

    :cond_1
    :goto_9
    goto/32 :goto_15

    nop

    :goto_a
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    goto/32 :goto_17

    nop

    :goto_b
    iput v0, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mActivePointerId:I

    goto/32 :goto_1

    nop

    :goto_c
    if-ne v0, v3, :cond_2

    goto/32 :goto_22

    :cond_2
    goto/32 :goto_3e

    nop

    :goto_d
    iget v4, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mTouchSlop:I

    goto/32 :goto_1e

    nop

    :goto_e
    iget v0, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mActivePointerId:I

    goto/32 :goto_3a

    nop

    :goto_f
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result p1

    goto/32 :goto_3c

    nop

    :goto_10
    sub-float/2addr v1, v0

    goto/32 :goto_31

    nop

    :goto_11
    sub-float/2addr p1, v0

    goto/32 :goto_5

    nop

    :goto_12
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    goto/32 :goto_2d

    nop

    :goto_13
    cmpl-float v0, v0, v4

    goto/32 :goto_8

    nop

    :goto_14
    if-lez v0, :cond_3

    goto/32 :goto_9

    :cond_3
    goto/32 :goto_24

    nop

    :goto_15
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    goto/32 :goto_16

    nop

    :goto_16
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    goto/32 :goto_41

    nop

    :goto_17
    if-ltz v0, :cond_4

    goto/32 :goto_2a

    :cond_4
    goto/32 :goto_29

    nop

    :goto_18
    goto :goto_27

    :goto_19
    goto/32 :goto_1a

    nop

    :goto_1a
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    goto/32 :goto_b

    nop

    :goto_1b
    if-nez v0, :cond_5

    goto/32 :goto_19

    :cond_5
    goto/32 :goto_2

    nop

    :goto_1c
    goto :goto_27

    :goto_1d
    goto/32 :goto_39

    nop

    :goto_1e
    int-to-float v4, v4

    goto/32 :goto_30

    nop

    :goto_1f
    return-void

    :goto_20
    goto/32 :goto_a

    nop

    :goto_21
    goto :goto_27

    :goto_22
    goto/32 :goto_e

    nop

    :goto_23
    if-ne v0, p1, :cond_6

    goto/32 :goto_1d

    :cond_6
    goto/32 :goto_21

    nop

    :goto_24
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    goto/32 :goto_37

    nop

    :goto_25
    if-ltz v0, :cond_7

    goto/32 :goto_2c

    :cond_7
    goto/32 :goto_2b

    nop

    :goto_26
    iput v1, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mScrollOrientation:I

    :goto_27
    goto/32 :goto_34

    nop

    :goto_28
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    goto/32 :goto_18

    nop

    :goto_29
    return-void

    :goto_2a
    goto/32 :goto_2f

    nop

    :goto_2b
    return-void

    :goto_2c
    goto/32 :goto_36

    nop

    :goto_2d
    const/4 v1, 0x0

    goto/32 :goto_1b

    nop

    :goto_2e
    const/4 v3, 0x2

    goto/32 :goto_c

    nop

    :goto_2f
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    goto/32 :goto_f

    nop

    :goto_30
    cmpl-float v0, v0, v4

    goto/32 :goto_14

    nop

    :goto_31
    iget v0, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mInitialDownX:F

    goto/32 :goto_11

    nop

    :goto_32
    if-gtz p1, :cond_8

    goto/32 :goto_4

    :cond_8
    goto/32 :goto_3

    nop

    :goto_33
    iget-object p0, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mTarget:Landroid/view/ViewGroup;

    goto/32 :goto_28

    nop

    :goto_34
    return-void

    :goto_35
    iput p1, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mInitialDownX:F

    goto/32 :goto_26

    nop

    :goto_36
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    goto/32 :goto_3f

    nop

    :goto_37
    iget v4, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mTouchSlop:I

    goto/32 :goto_40

    nop

    :goto_38
    iput v2, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mScrollOrientation:I

    goto/32 :goto_1c

    nop

    :goto_39
    iput v1, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mScrollOrientation:I

    goto/32 :goto_33

    nop

    :goto_3a
    const/4 v1, -0x1

    goto/32 :goto_3b

    nop

    :goto_3b
    if-eq v0, v1, :cond_9

    goto/32 :goto_20

    :cond_9
    goto/32 :goto_1f

    nop

    :goto_3c
    iget v0, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mInitialDownY:F

    goto/32 :goto_10

    nop

    :goto_3d
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result p1

    goto/32 :goto_35

    nop

    :goto_3e
    const/4 p1, 0x3

    goto/32 :goto_23

    nop

    :goto_3f
    iput v2, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mInitialDownY:F

    goto/32 :goto_3d

    nop

    :goto_40
    int-to-float v4, v4

    goto/32 :goto_13

    nop

    :goto_41
    cmpl-float p1, p1, v0

    goto/32 :goto_32

    nop
.end method

.method public isTouchInTarget(Landroid/view/MotionEvent;)Z
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v1

    if-ltz v1, :cond_0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result p1

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    iget-object v3, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mTarget:Landroid/view/ViewGroup;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getLocationInWindow([I)V

    aget v0, v1, v0

    const/4 v3, 0x1

    aget v1, v1, v3

    iget-object v3, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mTarget:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    add-int/2addr v3, v1

    iget-object p0, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mTarget:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getWidth()I

    move-result p0

    add-int/2addr p0, v0

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v0, v1, p0, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    float-to-int p0, p1

    float-to-int p1, v2

    invoke-virtual {v4, p0, p1}, Landroid/graphics/Rect;->contains(II)Z

    move-result p0

    return p0

    :cond_0
    return v0

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method
