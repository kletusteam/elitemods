.class Lmiuix/core/util/Utf8TextUtils$CharRange;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/core/util/Utf8TextUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CharRange"
.end annotation


# instance fields
.field length:I

.field start:I


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lmiuix/core/util/Utf8TextUtils$CharRange;->start:I

    iput v0, p0, Lmiuix/core/util/Utf8TextUtils$CharRange;->length:I

    return-void
.end method

.method constructor <init>(II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lmiuix/core/util/Utf8TextUtils$CharRange;->start:I

    iput p2, p0, Lmiuix/core/util/Utf8TextUtils$CharRange;->length:I

    return-void
.end method


# virtual methods
.method getEndIndex()I
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    iget v0, p0, Lmiuix/core/util/Utf8TextUtils$CharRange;->start:I

    goto/32 :goto_2

    nop

    :goto_2
    iget p0, p0, Lmiuix/core/util/Utf8TextUtils$CharRange;->length:I

    goto/32 :goto_3

    nop

    :goto_3
    add-int/2addr v0, p0

    goto/32 :goto_0

    nop
.end method

.method isValid()Z
    .locals 1

    goto/32 :goto_8

    nop

    :goto_0
    goto :goto_3

    :goto_1
    goto/32 :goto_2

    nop

    :goto_2
    const/4 p0, 0x0

    :goto_3
    goto/32 :goto_4

    nop

    :goto_4
    return p0

    :goto_5
    const/4 p0, 0x1

    goto/32 :goto_0

    nop

    :goto_6
    if-gez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_9

    nop

    :goto_7
    if-gtz p0, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_5

    nop

    :goto_8
    iget v0, p0, Lmiuix/core/util/Utf8TextUtils$CharRange;->start:I

    goto/32 :goto_6

    nop

    :goto_9
    iget p0, p0, Lmiuix/core/util/Utf8TextUtils$CharRange;->length:I

    goto/32 :goto_7

    nop
.end method
