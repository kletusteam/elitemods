.class public Lmiuix/provision/TitleLayoutHolder;
.super Ljava/lang/Object;


# instance fields
.field adjusted:Z

.field private titleLayout:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiuix/provision/TitleLayoutHolder;->titleLayout:Landroid/view/View;

    iput-boolean p2, p0, Lmiuix/provision/TitleLayoutHolder;->adjusted:Z

    return-void
.end method

.method public static adjustPaddingTop(Lmiuix/provision/TitleLayoutHolder;I)V
    .locals 5

    if-eqz p0, :cond_1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lmiuix/provision/TitleLayoutHolder;->getTitleLayout()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lmiuix/provision/TitleLayoutHolder;->isAdjusted()Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    add-int/2addr p1, v1

    invoke-virtual {v0}, Landroid/view/View;->getPaddingBottom()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v0, v2, p1, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lmiuix/provision/TitleLayoutHolder;->setAdjusted(Z)V

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public getTitleLayout()Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lmiuix/provision/TitleLayoutHolder;->titleLayout:Landroid/view/View;

    return-object p0
.end method

.method public isAdjusted()Z
    .locals 0

    iget-boolean p0, p0, Lmiuix/provision/TitleLayoutHolder;->adjusted:Z

    return p0
.end method

.method public setAdjusted(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/provision/TitleLayoutHolder;->adjusted:Z

    return-void
.end method
