.class public Lmiuix/view/CompatViewMethod;
.super Ljava/lang/Object;


# direct methods
.method public static setActivityTranslucent(Landroid/app/Activity;Z)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/app/Activity;->setTranslucent(Z)Z

    return-void
.end method

.method public static setForceDarkAllowed(Landroid/view/View;Z)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/view/View;->setForceDarkAllowed(Z)V

    return-void
.end method
