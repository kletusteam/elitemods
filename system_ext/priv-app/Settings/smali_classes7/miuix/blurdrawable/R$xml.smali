.class public final Lmiuix/blurdrawable/R$xml;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/blurdrawable/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "xml"
.end annotation


# static fields
.field public static final about_header:I = 0x7f150000

.field public static final about_legal:I = 0x7f150001

.field public static final accelerometer_header:I = 0x7f150002

.field public static final access_control:I = 0x7f150003

.field public static final access_control_set_app:I = 0x7f150004

.field public static final accessibility_audio_adjustment:I = 0x7f150005

.field public static final accessibility_autoclick_settings:I = 0x7f150006

.field public static final accessibility_button_settings:I = 0x7f150007

.field public static final accessibility_color_and_motion:I = 0x7f150008

.field public static final accessibility_color_inversion_settings:I = 0x7f150009

.field public static final accessibility_control_timeout_settings:I = 0x7f15000a

.field public static final accessibility_daltonizer_settings:I = 0x7f15000b

.field public static final accessibility_magnification_service_settings:I = 0x7f15000c

.field public static final accessibility_magnification_settings:I = 0x7f15000d

.field public static final accessibility_settings:I = 0x7f15000e

.field public static final accessibility_settings_for_setup_wizard:I = 0x7f15000f

.field public static final accessibility_settings_general:I = 0x7f150010

.field public static final accessibility_settings_hearing:I = 0x7f150011

.field public static final accessibility_settings_physical:I = 0x7f150012

.field public static final accessibility_settings_search:I = 0x7f150013

.field public static final accessibility_settings_visual:I = 0x7f150014

.field public static final accessibility_shortcut_service_settings:I = 0x7f150015

.field public static final accessibility_shortcut_settings:I = 0x7f150016

.field public static final accessibility_shortcuts_settings:I = 0x7f150017

.field public static final accessibility_system_controls:I = 0x7f150018

.field public static final accessibility_tap_assistance:I = 0x7f150019

.field public static final accessibility_text_reading_options:I = 0x7f15001a

.field public static final accessibility_vibration_intensity_settings:I = 0x7f15001b

.field public static final accessibility_vibration_settings:I = 0x7f15001c

.field public static final accessibilitymenu_service:I = 0x7f15001d

.field public static final accessibilitymenu_service_pad:I = 0x7f15001e

.field public static final account_settings:I = 0x7f15001f

.field public static final account_sync_settings:I = 0x7f150020

.field public static final account_type_settings:I = 0x7f150021

.field public static final accounts_dashboard_settings:I = 0x7f150022

.field public static final accounts_dashboard_settings_header:I = 0x7f150023

.field public static final accounts_personal_dashboard_settings:I = 0x7f150024

.field public static final accounts_work_dashboard_settings:I = 0x7f150025

.field public static final ad_service_settings:I = 0x7f150026

.field public static final adaptive_connectivity_settings:I = 0x7f150027

.field public static final adb_device_details_fragment:I = 0x7f150028

.field public static final adb_wireless_settings:I = 0x7f150029

.field public static final add_account_header:I = 0x7f15002a

.field public static final add_account_settings:I = 0x7f15002b

.field public static final agps_settings:I = 0x7f15002c

.field public static final ai_settings:I = 0x7f15002d

.field public static final airplane_mode_header:I = 0x7f15002e

.field public static final alarms_and_reminders:I = 0x7f15002f

.field public static final all_tether_prefs:I = 0x7f150030

.field public static final ambient_display_settings:I = 0x7f150031

.field public static final android_beam_header:I = 0x7f150032

.field public static final aod_and_lockscreen_settings:I = 0x7f150033

.field public static final aod_fake_settings:I = 0x7f150034

.field public static final apn_editor:I = 0x7f150035

.field public static final apn_settings:I = 0x7f150036

.field public static final app_and_notification:I = 0x7f150037

.field public static final app_bubble_notification_settings:I = 0x7f150038

.field public static final app_channels_bypassing_dnd_settings:I = 0x7f150039

.field public static final app_copier:I = 0x7f15003a

.field public static final app_data_usage:I = 0x7f15003b

.field public static final app_default_settings:I = 0x7f15003c

.field public static final app_info_settings:I = 0x7f15003d

.field public static final app_launch_settings:I = 0x7f15003e

.field public static final app_list_disclosure_settings:I = 0x7f15003f

.field public static final app_locale_details:I = 0x7f150040

.field public static final app_memory_settings:I = 0x7f150041

.field public static final app_notification_settings:I = 0x7f150042

.field public static final app_ops_permissions_details:I = 0x7f150043

.field public static final app_restrictions:I = 0x7f150044

.field public static final app_storage_settings:I = 0x7f150045

.field public static final applications_settings:I = 0x7f150046

.field public static final apps:I = 0x7f150047

.field public static final assist_gesture_settings:I = 0x7f150048

.field public static final auto_brightness_detail:I = 0x7f150049

.field public static final auto_rotate_settings:I = 0x7f15004a

.field public static final automatic_storage_management_settings:I = 0x7f15004b

.field public static final automatic_zen_rule_settings:I = 0x7f15004c

.field public static final automation_rules_settings:I = 0x7f15004d

.field public static final autorun:I = 0x7f15004e

.field public static final available_virtual_keyboard:I = 0x7f15004f

.field public static final backtap_settings_fragment:I = 0x7f150050

.field public static final backup_settings:I = 0x7f150051

.field public static final battery_indicator_header:I = 0x7f150052

.field public static final battery_saver_schedule_settings:I = 0x7f150053

.field public static final battery_saver_settings:I = 0x7f150054

.field public static final battery_settings:I = 0x7f150055

.field public static final bcast_source_info_details_fragment:I = 0x7f150056

.field public static final beauty_camera_settings:I = 0x7f150057

.field public static final billing_cycle:I = 0x7f150058

.field public static final bleAudioSourceInfo:I = 0x7f150059

.field public static final bluetoothShareBroadcast:I = 0x7f15005a

.field public static final bluetooth_advanced_settings:I = 0x7f15005b

.field public static final bluetooth_broadcast_audio_settings:I = 0x7f15005c

.field public static final bluetooth_broadcast_pin_config:I = 0x7f15005d

.field public static final bluetooth_device_advanced:I = 0x7f15005e

.field public static final bluetooth_device_blackfile:I = 0x7f15005f

.field public static final bluetooth_device_details_fragment:I = 0x7f150060

.field public static final bluetooth_find_broadcasts_fragment:I = 0x7f150061

.field public static final bluetooth_group_details_fragment:I = 0x7f150062

.field public static final bluetooth_header:I = 0x7f150063

.field public static final bluetooth_pairing_detail:I = 0x7f150064

.field public static final bluetooth_screen:I = 0x7f150065

.field public static final bluetooth_search_bcast_sources:I = 0x7f150066

.field public static final bluetooth_settings:I = 0x7f150067

.field public static final bluetooth_settings_obsolete:I = 0x7f150068

.field public static final bluetooth_virtualdevice_unpaire_device:I = 0x7f150069

.field public static final brightness_header:I = 0x7f15006a

.field public static final brightness_settings:I = 0x7f15006b

.field public static final bubble_notification_settings:I = 0x7f15006c

.field public static final bug_report_handler_settings:I = 0x7f15006d

.field public static final button_navigation_settings:I = 0x7f15006e

.field public static final captioning_appearance:I = 0x7f15006f

.field public static final captioning_more_options:I = 0x7f150070

.field public static final captioning_settings:I = 0x7f150071

.field public static final change_wifi_state_details:I = 0x7f150072

.field public static final channel_notification_settings:I = 0x7f150073

.field public static final choose_network:I = 0x7f150074

.field public static final classic_protection_mode:I = 0x7f150075

.field public static final close_lid_display_settings:I = 0x7f150076

.field public static final color_game_led:I = 0x7f150077

.field public static final color_game_led_custom_settings:I = 0x7f150078

.field public static final color_lamp:I = 0x7f150079

.field public static final color_led:I = 0x7f15007a

.field public static final color_mode_settings:I = 0x7f15007b

.field public static final common_issue_headers:I = 0x7f15007c

.field public static final configure_notification_settings:I = 0x7f15007d

.field public static final connected_devices:I = 0x7f15007e

.field public static final connected_devices_advanced:I = 0x7f15007f

.field public static final control_center_settings:I = 0x7f150080

.field public static final conversation_list_settings:I = 0x7f150081

.field public static final conversation_notification_settings:I = 0x7f150082

.field public static final create_shortcut:I = 0x7f150083

.field public static final credential_management_app_fragment:I = 0x7f150084

.field public static final current_dream_settings:I = 0x7f150085

.field public static final dark_mode_settings:I = 0x7f150086

.field public static final dark_ui_settings:I = 0x7f150087

.field public static final data_saver:I = 0x7f150088

.field public static final data_usage:I = 0x7f150089

.field public static final data_usage_cellular:I = 0x7f15008a

.field public static final data_usage_ethernet:I = 0x7f15008b

.field public static final data_usage_list:I = 0x7f15008c

.field public static final data_usage_metered_prefs:I = 0x7f15008d

.field public static final data_usage_wifi:I = 0x7f15008e

.field public static final date_time_header:I = 0x7f15008f

.field public static final date_time_prefs:I = 0x7f150090

.field public static final default_assist_settings:I = 0x7f150091

.field public static final default_autofill_settings:I = 0x7f150092

.field public static final default_browser_settings:I = 0x7f150093

.field public static final default_emergency_settings:I = 0x7f150094

.field public static final default_home_settings:I = 0x7f150095

.field public static final default_input_method_settings:I = 0x7f150096

.field public static final default_phone_settings:I = 0x7f150097

.field public static final default_sms_settings:I = 0x7f150098

.field public static final default_voice_settings:I = 0x7f150099

.field public static final development_settings:I = 0x7f15009a

.field public static final development_tile_settings:I = 0x7f15009b

.field public static final device_admin_settings:I = 0x7f15009c

.field public static final device_info_legal:I = 0x7f15009d

.field public static final device_info_memory:I = 0x7f15009e

.field public static final device_info_phone_status:I = 0x7f15009f

.field public static final device_info_settings:I = 0x7f1500a0

.field public static final device_info_sim_status:I = 0x7f1500a1

.field public static final device_info_status:I = 0x7f1500a2

.field public static final device_info_storage_volume:I = 0x7f1500a3

.field public static final device_picker:I = 0x7f1500a4

.field public static final device_state_auto_rotate_settings:I = 0x7f1500a5

.field public static final display_settings:I = 0x7f1500a6

.field public static final dnd_alarm_content:I = 0x7f1500a7

.field public static final dnd_mode_detail_settings:I = 0x7f1500a8

.field public static final dnd_mode_settings:I = 0x7f1500a9

.field public static final dnd_quiet_main:I = 0x7f1500aa

.field public static final dndm_custom_rule_item:I = 0x7f1500ab

.field public static final dndm_custom_vip_item:I = 0x7f1500ac

.field public static final dndm_fragment:I = 0x7f1500ad

.field public static final dndm_time_settings:I = 0x7f1500ae

.field public static final dndm_vip_call_settings_fragment:I = 0x7f1500af

.field public static final double_tap_power_settings:I = 0x7f1500b0

.field public static final double_tap_screen_settings:I = 0x7f1500b1

.field public static final double_twist_gesture_settings:I = 0x7f1500b2

.field public static final draw_overlay_permissions_details:I = 0x7f1500b3

.field public static final dream_fragment_overview:I = 0x7f1500b4

.field public static final edge_mode_guide:I = 0x7f1500b5

.field public static final edge_mode_settings:I = 0x7f1500b6

.field public static final edge_settings_select_fragment:I = 0x7f1500b7

.field public static final emergency_gesture_settings:I = 0x7f1500b8

.field public static final emergency_settings:I = 0x7f1500b9

.field public static final enable_bluetooth_recode:I = 0x7f1500ba

.field public static final encryption_and_credential:I = 0x7f1500bb

.field public static final encryption_settings:I = 0x7f1500bc

.field public static final enterprise_privacy_settings:I = 0x7f1500bd

.field public static final enterprise_set_default_apps_settings:I = 0x7f1500be

.field public static final expand_card_actions_settings:I = 0x7f1500bf

.field public static final external_ram_settings:I = 0x7f1500c0

.field public static final external_ram_settings_global:I = 0x7f1500c1

.field public static final external_sources_details:I = 0x7f1500c2

.field public static final face_data_manage:I = 0x7f1500c3

.field public static final face_unlock_type:I = 0x7f1500c4

.field public static final feature_flags_settings:I = 0x7f1500c5

.field public static final file_paths:I = 0x7f1500c6

.field public static final financed_privacy_settings:I = 0x7f1500c7

.field public static final fingerprint_manage:I = 0x7f1500c8

.field public static final firmware_version:I = 0x7f1500c9

.field public static final fiveg_nrca_setting:I = 0x7f1500ca

.field public static final fiveg_vonr_setting:I = 0x7f1500cb

.field public static final flashback_settings:I = 0x7f1500cc

.field public static final fold_screen_settings:I = 0x7f1500cd

.field public static final font_header:I = 0x7f1500ce

.field public static final font_settings:I = 0x7f1500cf

.field public static final font_settings2:I = 0x7f1500d0

.field public static final font_size_settings:I = 0x7f1500d1

.field public static final freeform_settings:I = 0x7f1500d2

.field public static final full_keyboard_settings:I = 0x7f1500d3

.field public static final full_screen_resolution_settings:I = 0x7f1500d4

.field public static final fullscreen_display_settings:I = 0x7f1500d5

.field public static final functionindex:I = 0x7f1500d6

.field public static final gesture_navigation_settings:I = 0x7f1500d7

.field public static final gesture_shortcut_settings:I = 0x7f1500d8

.field public static final gesture_shortcut_settings_select:I = 0x7f1500d9

.field public static final gestures:I = 0x7f1500da

.field public static final graphics_driver_settings:I = 0x7f1500db

.field public static final gxzw_quick_open:I = 0x7f1500dc

.field public static final handy_mode_settings:I = 0x7f1500dd

.field public static final haptic_settings:I = 0x7f1500de

.field public static final hardware_info:I = 0x7f1500df

.field public static final headsetAntiLostLayout:I = 0x7f1500e0

.field public static final headsetLayout:I = 0x7f1500e1

.field public static final headset_key_config:I = 0x7f1500e2

.field public static final headset_key_config_TWS01:I = 0x7f1500e3

.field public static final headset_key_config_tws_k77s:I = 0x7f1500e4

.field public static final headset_key_press_config:I = 0x7f1500e5

.field public static final high_power_details:I = 0x7f1500e6

.field public static final http_invoke_app_settings:I = 0x7f1500e7

.field public static final inapp_notification_settings:I = 0x7f1500e8

.field public static final infinity_display_settings:I = 0x7f1500e9

.field public static final input_method_clipboard_settings:I = 0x7f1500ea

.field public static final input_method_cloud_paste:I = 0x7f1500eb

.field public static final input_method_item_preference_layout:I = 0x7f1500ec

.field public static final input_methods_prefs:I = 0x7f1500ed

.field public static final input_methods_subtype:I = 0x7f1500ee

.field public static final install_certificate_from_storage:I = 0x7f1500ef

.field public static final installed_accessible_services:I = 0x7f1500f0

.field public static final installed_app_details_fragment:I = 0x7f1500f1

.field public static final installed_app_details_fragment_global:I = 0x7f1500f2

.field public static final installed_app_launch_settings:I = 0x7f1500f3

.field public static final interact_across_profiles:I = 0x7f1500f4

.field public static final interact_across_profiles_permissions_details:I = 0x7f1500f5

.field public static final items_multiple_carrier:I = 0x7f1500f6

.field public static final keep_screen_on_after_folding:I = 0x7f1500f7

.field public static final key_header:I = 0x7f1500f8

.field public static final key_settings:I = 0x7f1500f9

.field public static final key_settings_preview:I = 0x7f1500fa

.field public static final key_settings_select_fragment:I = 0x7f1500fb

.field public static final keyboard_layout_picker_fragment:I = 0x7f1500fc

.field public static final keyguard_advanced_settings:I = 0x7f1500fd

.field public static final keyshortcut_settings:I = 0x7f1500fe

.field public static final knock_settings_quick_feature_select_fragment:I = 0x7f1500ff

.field public static final language_and_input:I = 0x7f150100

.field public static final language_header:I = 0x7f150101

.field public static final language_settings:I = 0x7f150102

.field public static final large_screen_keyboard_fold:I = 0x7f150103

.field public static final large_screen_keyboard_pad:I = 0x7f150104

.field public static final led_header:I = 0x7f150105

.field public static final led_settings:I = 0x7f150106

.field public static final lite_font_settings:I = 0x7f150107

.field public static final locale_picker:I = 0x7f150108

.field public static final location_recent_access_see_all:I = 0x7f150109

.field public static final location_recent_app_settings:I = 0x7f15010a

.field public static final location_recent_requests_header:I = 0x7f15010b

.field public static final location_recent_requests_see_all:I = 0x7f15010c

.field public static final location_services:I = 0x7f15010d

.field public static final location_services_bluetooth_scanning:I = 0x7f15010e

.field public static final location_services_header:I = 0x7f15010f

.field public static final location_services_wifi_scanning:I = 0x7f150110

.field public static final location_services_workprofile:I = 0x7f150111

.field public static final location_settings:I = 0x7f150112

.field public static final location_settings_header:I = 0x7f150113

.field public static final location_settings_personal:I = 0x7f150114

.field public static final location_settings_workprofile:I = 0x7f150115

.field public static final lock_screen_actions_settings:I = 0x7f150116

.field public static final lock_secure_after_timeout_settings:I = 0x7f150117

.field public static final lock_settings_profile:I = 0x7f150118

.field public static final manage_accounts_settings:I = 0x7f150119

.field public static final manage_accounts_settings_miui:I = 0x7f15011a

.field public static final manage_assist:I = 0x7f15011b

.field public static final manage_domain_url_settings:I = 0x7f15011c

.field public static final manage_external_storage_permission_details:I = 0x7f15011d

.field public static final manage_voice:I = 0x7f15011e

.field public static final managed_profile_settings:I = 0x7f15011f

.field public static final master_clear:I = 0x7f150120

.field public static final media_controls_settings:I = 0x7f150121

.field public static final media_management_apps:I = 0x7f150122

.field public static final mi_transfer_history:I = 0x7f150123

.field public static final micloud_header:I = 0x7f150124

.field public static final mimoney_header:I = 0x7f150125

.field public static final miui_app_default_settings:I = 0x7f150126

.field public static final miui_app_notification_settings:I = 0x7f150127

.field public static final miui_bubble_notification_settings:I = 0x7f150128

.field public static final miui_channel_notification_settings:I = 0x7f150129

.field public static final miui_lab_settings:I = 0x7f15012a

.field public static final miui_location_settings:I = 0x7f15012b

.field public static final miui_nfc_detail:I = 0x7f15012c

.field public static final miui_physical_keyboard_settings:I = 0x7f15012d

.field public static final miui_provision_setup_security_settings_picker:I = 0x7f15012e

.field public static final miui_reset_network:I = 0x7f15012f

.field public static final miui_reset_network_noSim:I = 0x7f150130

.field public static final miui_settings_panel:I = 0x7f150131

.field public static final miui_setup_security_settings_picker:I = 0x7f150132

.field public static final miui_sos_settings:I = 0x7f150133

.field public static final miui_stylus_ota_pref:I = 0x7f150134

.field public static final miui_stylus_settings:I = 0x7f150135

.field public static final miui_tether_block_list:I = 0x7f150136

.field public static final miui_tether_devices:I = 0x7f150137

.field public static final miui_tether_prefs:I = 0x7f150138

.field public static final miui_volume_settings_panel:I = 0x7f150139

.field public static final miui_wifi_dpp_network_list:I = 0x7f15013a

.field public static final mobile_network_header:I = 0x7f15013b

.field public static final mobile_network_list:I = 0x7f15013c

.field public static final mobile_network_settings:I = 0x7f15013d

.field public static final mobile_network_settings_v2:I = 0x7f15013e

.field public static final modify_instruction_privacy_password:I = 0x7f15013f

.field public static final module_licenses:I = 0x7f150140

.field public static final monochrome_mode_apps:I = 0x7f150141

.field public static final monochrome_mode_settings:I = 0x7f150142

.field public static final multi_sim_ringtone_settings:I = 0x7f150143

.field public static final my_device_detail_settings:I = 0x7f150144

.field public static final my_device_info:I = 0x7f150145

.field public static final my_device_settings:I = 0x7f150146

.field public static final network_and_internet:I = 0x7f150147

.field public static final network_provider_calls_sms:I = 0x7f150148

.field public static final network_provider_internet:I = 0x7f150149

.field public static final network_provider_settings:I = 0x7f15014a

.field public static final network_provider_sims_list:I = 0x7f15014b

.field public static final network_scorer_picker_prefs:I = 0x7f15014c

.field public static final new_dndm_time_settings:I = 0x7f15014d

.field public static final nfc_and_payment_settings:I = 0x7f15014e

.field public static final nfc_default_payment_settings:I = 0x7f15014f

.field public static final nfc_payment_settings:I = 0x7f150150

.field public static final night_display_settings:I = 0x7f150151

.field public static final notch_status_bar_settings:I = 0x7f150152

.field public static final notification_access_bridged_apps_settings:I = 0x7f150153

.field public static final notification_access_permission_details:I = 0x7f150154

.field public static final notification_access_settings:I = 0x7f150155

.field public static final notification_assistant_settings:I = 0x7f150156

.field public static final notification_control_center_settings:I = 0x7f150157

.field public static final notification_group_settings:I = 0x7f150158

.field public static final notification_importance:I = 0x7f150159

.field public static final notification_settings:I = 0x7f15015a

.field public static final old_paper_mode_settings:I = 0x7f15015b

.field public static final one_handed_settings:I = 0x7f15015c

.field public static final open_supported_links:I = 0x7f15015d

.field public static final otg_settings:I = 0x7f15015e

.field public static final other_personal_settings:I = 0x7f15015f

.field public static final other_sound_settings:I = 0x7f150160

.field public static final other_special_function_settings:I = 0x7f150161

.field public static final paper_mode_apps:I = 0x7f150162

.field public static final paper_mode_settings:I = 0x7f150163

.field public static final paper_protection_mode:I = 0x7f150164

.field public static final permission_info:I = 0x7f150165

.field public static final physical_keyboard_settings:I = 0x7f150166

.field public static final pick_up_gesture_settings:I = 0x7f150167

.field public static final picture_in_picture_permissions_details:I = 0x7f150168

.field public static final picture_in_picture_settings:I = 0x7f150169

.field public static final placeholder_preference_screen:I = 0x7f15016a

.field public static final placeholder_prefs:I = 0x7f15016b

.field public static final platform_compat_settings:I = 0x7f15016c

.field public static final popupcamera_function_settings:I = 0x7f15016d

.field public static final power_menu_settings:I = 0x7f15016e

.field public static final power_mode_header:I = 0x7f15016f

.field public static final power_usage_advanced:I = 0x7f150170

.field public static final power_usage_detail:I = 0x7f150171

.field public static final power_usage_detail_legacy:I = 0x7f150172

.field public static final power_usage_summary:I = 0x7f150173

.field public static final pre_install_app:I = 0x7f150174

.field public static final prefer_vonr_list:I = 0x7f150175

.field public static final preference_bt_device_blacklist:I = 0x7f150176

.field public static final preference_bt_recode:I = 0x7f150177

.field public static final preferred_app_settings:I = 0x7f150178

.field public static final premium_sms_settings:I = 0x7f150179

.field public static final prevent_ringing_gesture_settings:I = 0x7f15017a

.field public static final previously_connected_devices:I = 0x7f15017b

.field public static final previously_connected_group_devices:I = 0x7f15017c

.field public static final print_job_settings:I = 0x7f15017d

.field public static final print_settings:I = 0x7f15017e

.field public static final priority_storage:I = 0x7f15017f

.field public static final privacy_advanced_settings:I = 0x7f150180

.field public static final privacy_controls_settings:I = 0x7f150181

.field public static final privacy_dashboard_settings:I = 0x7f150182

.field public static final privacy_password_setting:I = 0x7f150183

.field public static final privacy_settings:I = 0x7f150184

.field public static final process_stats_summary:I = 0x7f150185

.field public static final process_stats_ui:I = 0x7f150186

.field public static final profile_challenge_settings:I = 0x7f150187

.field public static final reduce_bright_colors_settings:I = 0x7f150188

.field public static final reset_dashboard_fragment:I = 0x7f150189

.field public static final restricted_apps_detail:I = 0x7f15018a

.field public static final ringer_volume_header:I = 0x7f15018b

.field public static final ringtone_header:I = 0x7f15018c

.field public static final saved_wifi:I = 0x7f15018d

.field public static final screen_color_settings:I = 0x7f15018e

.field public static final screen_effect_settings:I = 0x7f15018f

.field public static final screen_enhance_engine_ai_settings:I = 0x7f150190

.field public static final screen_enhance_engine_memc_settings:I = 0x7f150191

.field public static final screen_enhance_engine_s2h_settings:I = 0x7f150192

.field public static final screen_enhance_engine_settings:I = 0x7f150193

.field public static final screen_enhance_engine_sr_settings:I = 0x7f150194

.field public static final screen_lock_settings:I = 0x7f150195

.field public static final screen_pinning_settings:I = 0x7f150196

.field public static final screen_projection_examples:I = 0x7f150197

.field public static final screen_projection_settings:I = 0x7f150198

.field public static final screen_resolution_settings:I = 0x7f150199

.field public static final screen_timeout_header:I = 0x7f15019a

.field public static final screen_timeout_settings:I = 0x7f15019b

.field public static final screenshot_settings:I = 0x7f15019c

.field public static final searchable:I = 0x7f15019d

.field public static final security_advanced_settings:I = 0x7f15019e

.field public static final security_dashboard_settings:I = 0x7f15019f

.field public static final security_keyboard_settings:I = 0x7f1501a0

.field public static final security_lockscreen_settings:I = 0x7f1501a1

.field public static final security_privacy_settings:I = 0x7f1501a2

.field public static final security_settings:I = 0x7f1501a3

.field public static final security_settings_biometric_weak:I = 0x7f1501a4

.field public static final security_settings_bluetooth:I = 0x7f1501a5

.field public static final security_settings_bluetooth_add_device:I = 0x7f1501a6

.field public static final security_settings_chooser:I = 0x7f1501a7

.field public static final security_settings_combined_biometric:I = 0x7f1501a8

.field public static final security_settings_combined_biometric_profile:I = 0x7f1501a9

.field public static final security_settings_common:I = 0x7f1501aa

.field public static final security_settings_debug_log:I = 0x7f1501ab

.field public static final security_settings_encrypted:I = 0x7f1501ac

.field public static final security_settings_encryption:I = 0x7f1501ad

.field public static final security_settings_face:I = 0x7f1501ae

.field public static final security_settings_fingerprint:I = 0x7f1501af

.field public static final security_settings_lockscreen:I = 0x7f1501b0

.field public static final security_settings_lockscreen_profile:I = 0x7f1501b1

.field public static final security_settings_password:I = 0x7f1501b2

.field public static final security_settings_password_profile:I = 0x7f1501b3

.field public static final security_settings_pattern:I = 0x7f1501b4

.field public static final security_settings_pattern_profile:I = 0x7f1501b5

.field public static final security_settings_picker:I = 0x7f1501b6

.field public static final security_settings_pin:I = 0x7f1501b7

.field public static final security_settings_pin_profile:I = 0x7f1501b8

.field public static final security_settings_screen_password:I = 0x7f1501b9

.field public static final security_settings_unencrypted:I = 0x7f1501ba

.field public static final security_settings_unification:I = 0x7f1501bb

.field public static final security_settings_unlock_common:I = 0x7f1501bc

.field public static final security_settings_unlock_fingerprint:I = 0x7f1501bd

.field public static final security_status_settings:I = 0x7f1501be

.field public static final settings_haptic_settings:I = 0x7f1501bf

.field public static final settings_headers:I = 0x7f1501c0

.field public static final setup_security_settings_picker:I = 0x7f1501c1

.field public static final shortcuts:I = 0x7f1501c2

.field public static final shortcuts_pad:I = 0x7f1501c3

.field public static final shoulderkey_settings:I = 0x7f1501c4

.field public static final shoulderkey_shortcut:I = 0x7f1501c5

.field public static final shoulderkey_sound:I = 0x7f1501c6

.field public static final silent_settings:I = 0x7f1501c7

.field public static final sim_lock_settings:I = 0x7f1501c8

.field public static final single_choice_list_item_2:I = 0x7f1501c9

.field public static final slave_wifi_settings:I = 0x7f1501ca

.field public static final smart_battery_detail:I = 0x7f1501cb

.field public static final smart_forwarding_mdn_handler:I = 0x7f1501cc

.field public static final smart_forwarding_mdn_handler_header:I = 0x7f1501cd

.field public static final smart_forwarding_switch:I = 0x7f1501ce

.field public static final smartcover_mode_settings:I = 0x7f1501cf

.field public static final sms_received_header:I = 0x7f1501d0

.field public static final sound_haptic_settings:I = 0x7f1501d1

.field public static final sound_header:I = 0x7f1501d2

.field public static final sound_settings:I = 0x7f1501d3

.field public static final sound_work_settings:I = 0x7f1501d4

.field public static final spatial_audio_settings:I = 0x7f1501d5

.field public static final speaker_settings:I = 0x7f1501d6

.field public static final special_access:I = 0x7f1501d7

.field public static final spellchecker_prefs:I = 0x7f1501d8

.field public static final standalone_badge:I = 0x7f1501d9

.field public static final standalone_badge_gravity_bottom_end:I = 0x7f1501da

.field public static final standalone_badge_gravity_bottom_start:I = 0x7f1501db

.field public static final standalone_badge_gravity_top_start:I = 0x7f1501dc

.field public static final standalone_badge_offset:I = 0x7f1501dd

.field public static final status_bar_settings:I = 0x7f1501de

.field public static final status_imei:I = 0x7f1501df

.field public static final status_sim:I = 0x7f1501e0

.field public static final storage_category_fragment:I = 0x7f1501e1

.field public static final storage_dashboard_fragment:I = 0x7f1501e2

.field public static final storage_dashboard_header_fragment:I = 0x7f1501e3

.field public static final swipe_to_notification_settings:I = 0x7f1501e4

.field public static final sync_header:I = 0x7f1501e5

.field public static final sync_settings:I = 0x7f1501e6

.field public static final system_app_settings:I = 0x7f1501e7

.field public static final system_dashboard_fragment:I = 0x7f1501e8

.field public static final system_navigation_gesture_settings:I = 0x7f1501e9

.field public static final tablet_screen_settings:I = 0x7f1501ea

.field public static final tap_screen_gesture_settings:I = 0x7f1501eb

.field public static final testing_settings:I = 0x7f1501ec

.field public static final testing_wifi_settings:I = 0x7f1501ed

.field public static final tether_header:I = 0x7f1501ee

.field public static final tether_prefs:I = 0x7f1501ef

.field public static final time_zone_prefs:I = 0x7f1501f0

.field public static final timezones:I = 0x7f1501f1

.field public static final top_level_settings:I = 0x7f1501f2

.field public static final transcode_settings:I = 0x7f1501f3

.field public static final trust_agent_settings:I = 0x7f1501f4

.field public static final tts_engine_picker:I = 0x7f1501f5

.field public static final tts_settings:I = 0x7f1501f6

.field public static final turn_screen_on_permissions_details:I = 0x7f1501f7

.field public static final turn_screen_on_settings:I = 0x7f1501f8

.field public static final unlock_set_header:I = 0x7f1501f9

.field public static final unrestricted_data_access_settings:I = 0x7f1501fa

.field public static final upgraded_app_notification_settings:I = 0x7f1501fb

.field public static final uplmn_editor:I = 0x7f1501fc

.field public static final uplmn_settings:I = 0x7f1501fd

.field public static final usage_access_settings:I = 0x7f1501fe

.field public static final usage_and_diagnostics:I = 0x7f1501ff

.field public static final usagestats_set_time:I = 0x7f150200

.field public static final usb_default_fragment:I = 0x7f150201

.field public static final usb_details_fragment:I = 0x7f150202

.field public static final usb_settings:I = 0x7f150203

.field public static final user_details_settings:I = 0x7f150204

.field public static final user_dictionary_list_fragment:I = 0x7f150205

.field public static final user_settings:I = 0x7f150206

.field public static final user_timeout_to_user_zero_settings:I = 0x7f150207

.field public static final vibrate_for_calls_settings:I = 0x7f150208

.field public static final voice_input_settings:I = 0x7f150209

.field public static final vpn_app_management:I = 0x7f15020a

.field public static final vpn_header:I = 0x7f15020b

.field public static final vpn_settings2:I = 0x7f15020c

.field public static final vr_display_settings:I = 0x7f15020d

.field public static final vr_listeners_settings:I = 0x7f15020e

.field public static final wakeup_xiaoai_settings:I = 0x7f15020f

.field public static final wallpaper_header:I = 0x7f150210

.field public static final wallpaper_settings:I = 0x7f150211

.field public static final webview_app_settings:I = 0x7f150212

.field public static final when_to_dream_settings:I = 0x7f150213

.field public static final wifi_access_points:I = 0x7f150214

.field public static final wifi_access_points_for_wifi_setup_xl:I = 0x7f150215

.field public static final wifi_advanced_settings:I = 0x7f150216

.field public static final wifi_assistant_options:I = 0x7f150217

.field public static final wifi_calling_settings:I = 0x7f150218

.field public static final wifi_configure_settings:I = 0x7f150219

.field public static final wifi_detail_bottom:I = 0x7f15021a

.field public static final wifi_display_saved_access_points2:I = 0x7f15021b

.field public static final wifi_display_settings:I = 0x7f15021c

.field public static final wifi_dpp_network_list:I = 0x7f15021d

.field public static final wifi_header:I = 0x7f15021e

.field public static final wifi_link_turbo_option:I = 0x7f15021f

.field public static final wifi_network_details_fragment2:I = 0x7f150220

.field public static final wifi_p2p_settings:I = 0x7f150221

.field public static final wifi_protection_settings:I = 0x7f150222

.field public static final wifi_settings:I = 0x7f150223

.field public static final wifi_settings2:I = 0x7f150224

.field public static final wifi_tether_settings:I = 0x7f150225

.field public static final wireless_settings:I = 0x7f150226

.field public static final write_system_settings_permissions_details:I = 0x7f150227

.field public static final zen_access_permission_details:I = 0x7f150228

.field public static final zen_access_settings:I = 0x7f150229

.field public static final zen_mode_automation_settings:I = 0x7f15022a

.field public static final zen_mode_block_settings:I = 0x7f15022b

.field public static final zen_mode_bypassing_apps:I = 0x7f15022c

.field public static final zen_mode_calls_settings:I = 0x7f15022d

.field public static final zen_mode_conversations_settings:I = 0x7f15022e

.field public static final zen_mode_custom_rule_calls_settings:I = 0x7f15022f

.field public static final zen_mode_custom_rule_configuration:I = 0x7f150230

.field public static final zen_mode_custom_rule_messages_settings:I = 0x7f150231

.field public static final zen_mode_custom_rule_settings:I = 0x7f150232

.field public static final zen_mode_event_rule_settings:I = 0x7f150233

.field public static final zen_mode_messages_settings:I = 0x7f150234

.field public static final zen_mode_people_settings:I = 0x7f150235

.field public static final zen_mode_restrict_notifications_settings:I = 0x7f150236

.field public static final zen_mode_schedule_rule_settings:I = 0x7f150237

.field public static final zen_mode_settings:I = 0x7f150238

.field public static final zen_mode_sound_vibration_settings:I = 0x7f150239


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
