.class public final Lmiuix/blurdrawable/R$anim;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/blurdrawable/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "anim"
.end annotation


# static fields
.field public static final abc_fade_in:I = 0x7f010000

.field public static final abc_fade_out:I = 0x7f010001

.field public static final abc_grow_fade_in_from_bottom:I = 0x7f010002

.field public static final abc_popup_enter:I = 0x7f010003

.field public static final abc_popup_exit:I = 0x7f010004

.field public static final abc_shrink_fade_out_from_bottom:I = 0x7f010005

.field public static final abc_slide_in_bottom:I = 0x7f010006

.field public static final abc_slide_in_top:I = 0x7f010007

.field public static final abc_slide_out_bottom:I = 0x7f010008

.field public static final abc_slide_out_top:I = 0x7f010009

.field public static final abc_tooltip_enter:I = 0x7f01000a

.field public static final abc_tooltip_exit:I = 0x7f01000b

.field public static final accelerate_quart:I = 0x7f01000c

.field public static final accelerate_sextic:I = 0x7f01000d

.field public static final action_bar_fade_in:I = 0x7f01000e

.field public static final action_bar_fade_out:I = 0x7f01000f

.field public static final action_menu_in:I = 0x7f010010

.field public static final action_menu_item_in:I = 0x7f010011

.field public static final activity_open_enter:I = 0x7f010012

.field public static final activity_open_exit:I = 0x7f010013

.field public static final activity_translate_out:I = 0x7f010014

.field public static final btn_checkbox_to_checked_box_inner_merged_animation:I = 0x7f010015

.field public static final btn_checkbox_to_checked_box_outer_merged_animation:I = 0x7f010016

.field public static final btn_checkbox_to_checked_icon_null_animation:I = 0x7f010017

.field public static final btn_checkbox_to_unchecked_box_inner_merged_animation:I = 0x7f010018

.field public static final btn_checkbox_to_unchecked_check_path_merged_animation:I = 0x7f010019

.field public static final btn_checkbox_to_unchecked_icon_null_animation:I = 0x7f01001a

.field public static final btn_radio_to_off_mtrl_dot_group_animation:I = 0x7f01001b

.field public static final btn_radio_to_off_mtrl_ring_outer_animation:I = 0x7f01001c

.field public static final btn_radio_to_off_mtrl_ring_outer_path_animation:I = 0x7f01001d

.field public static final btn_radio_to_on_mtrl_dot_group_animation:I = 0x7f01001e

.field public static final btn_radio_to_on_mtrl_ring_outer_animation:I = 0x7f01001f

.field public static final btn_radio_to_on_mtrl_ring_outer_path_animation:I = 0x7f010020

.field public static final confirm_credential_biometric_transition_exit:I = 0x7f010021

.field public static final confirm_credential_close_enter:I = 0x7f010022

.field public static final confirm_credential_close_exit:I = 0x7f010023

.field public static final confirm_credential_open_enter:I = 0x7f010024

.field public static final confirm_credential_open_exit:I = 0x7f010025

.field public static final cubic_bezier_stylus:I = 0x7f010026

.field public static final decelerate_12_degree:I = 0x7f010027

.field public static final decelerate_cubic:I = 0x7f010028

.field public static final decelerate_quart:I = 0x7f010029

.field public static final decelerate_sextic:I = 0x7f01002a

.field public static final decelerate_square:I = 0x7f01002b

.field public static final design_bottom_sheet_slide_in:I = 0x7f01002c

.field public static final design_bottom_sheet_slide_out:I = 0x7f01002d

.field public static final design_snackbar_in:I = 0x7f01002e

.field public static final design_snackbar_out:I = 0x7f01002f

.field public static final dialog_anim_noanim:I = 0x7f010030

.field public static final dialog_anim_pop_in_center:I = 0x7f010031

.field public static final dialog_anim_pop_out_center:I = 0x7f010032

.field public static final dialog_anim_slide_in_bottom:I = 0x7f010033

.field public static final dialog_anim_slide_out_bottom:I = 0x7f010034

.field public static final drop_down_popup_enter:I = 0x7f010035

.field public static final drop_down_popup_exit:I = 0x7f010036

.field public static final enrollment_fingerprint_background_1_path_animation:I = 0x7f010037

.field public static final enrollment_fingerprint_background_2_path_animation:I = 0x7f010038

.field public static final enrollment_fingerprint_background_5_path_animation:I = 0x7f010039

.field public static final enrollment_fingerprint_background_6_path_animation:I = 0x7f01003a

.field public static final enrollment_fingerprint_background_7_path_animation:I = 0x7f01003b

.field public static final enrollment_fingerprint_isolated_ridge_1_path_animation:I = 0x7f01003c

.field public static final enrollment_fingerprint_isolated_ridge_2_path_animation:I = 0x7f01003d

.field public static final enrollment_fingerprint_isolated_ridge_5_path_animation:I = 0x7f01003e

.field public static final enrollment_fingerprint_isolated_ridge_6_path_animation:I = 0x7f01003f

.field public static final enrollment_fingerprint_isolated_ridge_7_path_animation:I = 0x7f010040

.field public static final fod_finger_appear:I = 0x7f010041

.field public static final fragment_fast_out_extra_slow_in:I = 0x7f010042

.field public static final fullscreen_dialog_exit:I = 0x7f010043

.field public static final immersion_menu_enter:I = 0x7f010044

.field public static final immersion_menu_exit:I = 0x7f010045

.field public static final item_animation_fade_in:I = 0x7f010046

.field public static final layout_animation_fade_in:I = 0x7f010047

.field public static final miuix_appcompat_floating_window_anim_in_full_screen:I = 0x7f010048

.field public static final miuix_appcompat_floating_window_anim_out_full_screen:I = 0x7f010049

.field public static final miuix_appcompat_floating_window_enter_anim:I = 0x7f01004a

.field public static final miuix_appcompat_floating_window_enter_anim_auto_dpi:I = 0x7f01004b

.field public static final miuix_appcompat_floating_window_enter_anim_auto_dpi_land:I = 0x7f01004c

.field public static final miuix_appcompat_floating_window_enter_anim_land:I = 0x7f01004d

.field public static final miuix_appcompat_floating_window_enter_anim_normal_rom_enter:I = 0x7f01004e

.field public static final miuix_appcompat_floating_window_enter_anim_normal_rom_enter_alpha:I = 0x7f01004f

.field public static final miuix_appcompat_floating_window_enter_anim_normal_rom_exit:I = 0x7f010050

.field public static final miuix_appcompat_floating_window_enter_anim_normal_rom_exit_alpha:I = 0x7f010051

.field public static final miuix_appcompat_floating_window_exit_anim:I = 0x7f010052

.field public static final miuix_appcompat_floating_window_exit_anim_auto_dpi:I = 0x7f010053

.field public static final miuix_appcompat_floating_window_exit_anim_auto_dpi_land:I = 0x7f010054

.field public static final miuix_appcompat_floating_window_exit_anim_land:I = 0x7f010055

.field public static final miuix_appcompat_floating_window_exit_anim_normal_rom_enter:I = 0x7f010056

.field public static final miuix_appcompat_floating_window_exit_anim_normal_rom_exit:I = 0x7f010057

.field public static final mtrl_bottom_sheet_slide_in:I = 0x7f010058

.field public static final mtrl_bottom_sheet_slide_out:I = 0x7f010059

.field public static final mtrl_card_lowers_interpolator:I = 0x7f01005a

.field public static final progress_indeterminate_horizontal_rect1_copy:I = 0x7f01005b

.field public static final progress_indeterminate_horizontal_rect2_copy:I = 0x7f01005c

.field public static final quint_decelerate_interpolator:I = 0x7f01005d

.field public static final recents_quick_switch_left_enter:I = 0x7f01005e

.field public static final recents_quick_switch_left_exit:I = 0x7f01005f

.field public static final slide_in_left:I = 0x7f010060

.field public static final slide_in_right:I = 0x7f010061

.field public static final slide_out_left:I = 0x7f010062

.field public static final slide_out_right:I = 0x7f010063

.field public static final stylus_battery_anim:I = 0x7f010064

.field public static final stylus_battery_in:I = 0x7f010065

.field public static final stylus_battery_out:I = 0x7f010066

.field public static final stylus_connecting_progress_anim:I = 0x7f010067

.field public static final stylus_window_in:I = 0x7f010068

.field public static final stylus_window_out:I = 0x7f010069

.field public static final sud_pre_p_activity_close_enter:I = 0x7f01006a

.field public static final sud_pre_p_activity_close_exit:I = 0x7f01006b

.field public static final sud_pre_p_activity_open_enter:I = 0x7f01006c

.field public static final sud_pre_p_activity_open_exit:I = 0x7f01006d

.field public static final sud_slide_back_in:I = 0x7f01006e

.field public static final sud_slide_back_out:I = 0x7f01006f

.field public static final sud_slide_next_in:I = 0x7f010070

.field public static final sud_slide_next_out:I = 0x7f010071

.field public static final sud_stay:I = 0x7f010072

.field public static final virus_scan_scanning:I = 0x7f010073


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
