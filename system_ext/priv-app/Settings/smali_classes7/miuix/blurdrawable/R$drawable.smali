.class public final Lmiuix/blurdrawable/R$drawable;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/blurdrawable/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final TWS01_list_Black:I = 0x7f0800ac

.field public static final TWS01_list_Gray:I = 0x7f0800ad

.field public static final TWS01_list_Yellow:I = 0x7f0800ae

.field public static final Transparent_OFF:I = 0x7f0800af

.field public static final Transparent_ON:I = 0x7f0800b0

.field public static final aab_brightness:I = 0x7f0800b1

.field public static final abc_ab_share_pack_mtrl_alpha:I = 0x7f0800b2

.field public static final abc_action_bar_item_background_material:I = 0x7f0800b3

.field public static final abc_btn_borderless_material:I = 0x7f0800b4

.field public static final abc_btn_check_material:I = 0x7f0800b5

.field public static final abc_btn_check_material_anim:I = 0x7f0800b6

.field public static final abc_btn_check_to_on_mtrl_000:I = 0x7f0800b7

.field public static final abc_btn_check_to_on_mtrl_015:I = 0x7f0800b8

.field public static final abc_btn_colored_material:I = 0x7f0800b9

.field public static final abc_btn_default_mtrl_shape:I = 0x7f0800ba

.field public static final abc_btn_radio_material:I = 0x7f0800bb

.field public static final abc_btn_radio_material_anim:I = 0x7f0800bc

.field public static final abc_btn_radio_to_on_mtrl_000:I = 0x7f0800bd

.field public static final abc_btn_radio_to_on_mtrl_015:I = 0x7f0800be

.field public static final abc_btn_switch_to_on_mtrl_00001:I = 0x7f0800bf

.field public static final abc_btn_switch_to_on_mtrl_00012:I = 0x7f0800c0

.field public static final abc_cab_background_internal_bg:I = 0x7f0800c1

.field public static final abc_cab_background_top_material:I = 0x7f0800c2

.field public static final abc_cab_background_top_mtrl_alpha:I = 0x7f0800c3

.field public static final abc_control_background_material:I = 0x7f0800c4

.field public static final abc_dialog_material_background:I = 0x7f0800c5

.field public static final abc_edit_text_material:I = 0x7f0800c6

.field public static final abc_ic_ab_back_material:I = 0x7f0800c7

.field public static final abc_ic_arrow_drop_right_black_24dp:I = 0x7f0800c8

.field public static final abc_ic_arrow_forward:I = 0x7f0800c9

.field public static final abc_ic_clear_material:I = 0x7f0800ca

.field public static final abc_ic_commit_search_api_mtrl_alpha:I = 0x7f0800cb

.field public static final abc_ic_go_search_api_material:I = 0x7f0800cc

.field public static final abc_ic_menu_copy_mtrl_am_alpha:I = 0x7f0800cd

.field public static final abc_ic_menu_cut_mtrl_alpha:I = 0x7f0800ce

.field public static final abc_ic_menu_overflow_material:I = 0x7f0800cf

.field public static final abc_ic_menu_paste_mtrl_am_alpha:I = 0x7f0800d0

.field public static final abc_ic_menu_selectall_mtrl_alpha:I = 0x7f0800d1

.field public static final abc_ic_menu_share_mtrl_alpha:I = 0x7f0800d2

.field public static final abc_ic_permission:I = 0x7f0800d3

.field public static final abc_ic_search_api_material:I = 0x7f0800d4

.field public static final abc_ic_slice_send:I = 0x7f0800d5

.field public static final abc_ic_star_black_16dp:I = 0x7f0800d6

.field public static final abc_ic_star_black_36dp:I = 0x7f0800d7

.field public static final abc_ic_star_black_48dp:I = 0x7f0800d8

.field public static final abc_ic_star_half_black_16dp:I = 0x7f0800d9

.field public static final abc_ic_star_half_black_36dp:I = 0x7f0800da

.field public static final abc_ic_star_half_black_48dp:I = 0x7f0800db

.field public static final abc_ic_voice_search_api_material:I = 0x7f0800dc

.field public static final abc_item_background_holo_dark:I = 0x7f0800dd

.field public static final abc_item_background_holo_light:I = 0x7f0800de

.field public static final abc_list_divider_material:I = 0x7f0800df

.field public static final abc_list_divider_mtrl_alpha:I = 0x7f0800e0

.field public static final abc_list_focused_holo:I = 0x7f0800e1

.field public static final abc_list_longpressed_holo:I = 0x7f0800e2

.field public static final abc_list_pressed_holo_dark:I = 0x7f0800e3

.field public static final abc_list_pressed_holo_light:I = 0x7f0800e4

.field public static final abc_list_selector_background_transition_holo_dark:I = 0x7f0800e5

.field public static final abc_list_selector_background_transition_holo_light:I = 0x7f0800e6

.field public static final abc_list_selector_disabled_holo_dark:I = 0x7f0800e7

.field public static final abc_list_selector_disabled_holo_light:I = 0x7f0800e8

.field public static final abc_list_selector_holo_dark:I = 0x7f0800e9

.field public static final abc_list_selector_holo_light:I = 0x7f0800ea

.field public static final abc_menu_hardkey_panel_mtrl_mult:I = 0x7f0800eb

.field public static final abc_popup_background_mtrl_mult:I = 0x7f0800ec

.field public static final abc_ratingbar_indicator_material:I = 0x7f0800ed

.field public static final abc_ratingbar_material:I = 0x7f0800ee

.field public static final abc_ratingbar_small_material:I = 0x7f0800ef

.field public static final abc_scrubber_control_off_mtrl_alpha:I = 0x7f0800f0

.field public static final abc_scrubber_control_to_pressed_mtrl_000:I = 0x7f0800f1

.field public static final abc_scrubber_control_to_pressed_mtrl_005:I = 0x7f0800f2

.field public static final abc_scrubber_primary_mtrl_alpha:I = 0x7f0800f3

.field public static final abc_scrubber_track_mtrl_alpha:I = 0x7f0800f4

.field public static final abc_seekbar_thumb_material:I = 0x7f0800f5

.field public static final abc_seekbar_tick_mark_material:I = 0x7f0800f6

.field public static final abc_seekbar_track_material:I = 0x7f0800f7

.field public static final abc_slice_gradient:I = 0x7f0800f8

.field public static final abc_slice_remote_input_bg:I = 0x7f0800f9

.field public static final abc_slice_ripple_drawable:I = 0x7f0800fa

.field public static final abc_slice_see_more_bg:I = 0x7f0800fb

.field public static final abc_spinner_mtrl_am_alpha:I = 0x7f0800fc

.field public static final abc_spinner_textfield_background_material:I = 0x7f0800fd

.field public static final abc_star_black_48dp:I = 0x7f0800fe

.field public static final abc_star_half_black_48dp:I = 0x7f0800ff

.field public static final abc_switch_thumb_material:I = 0x7f080100

.field public static final abc_switch_track_mtrl_alpha:I = 0x7f080101

.field public static final abc_tab_indicator_material:I = 0x7f080102

.field public static final abc_tab_indicator_mtrl_alpha:I = 0x7f080103

.field public static final abc_text_cursor_material:I = 0x7f080104

.field public static final abc_text_select_handle_left_mtrl:I = 0x7f080105

.field public static final abc_text_select_handle_left_mtrl_dark:I = 0x7f080106

.field public static final abc_text_select_handle_left_mtrl_light:I = 0x7f080107

.field public static final abc_text_select_handle_middle_mtrl:I = 0x7f080108

.field public static final abc_text_select_handle_middle_mtrl_dark:I = 0x7f080109

.field public static final abc_text_select_handle_middle_mtrl_light:I = 0x7f08010a

.field public static final abc_text_select_handle_right_mtrl:I = 0x7f08010b

.field public static final abc_text_select_handle_right_mtrl_dark:I = 0x7f08010c

.field public static final abc_text_select_handle_right_mtrl_light:I = 0x7f08010d

.field public static final abc_textfield_activated_mtrl_alpha:I = 0x7f08010e

.field public static final abc_textfield_default_mtrl_alpha:I = 0x7f08010f

.field public static final abc_textfield_search_activated_mtrl_alpha:I = 0x7f080110

.field public static final abc_textfield_search_default_mtrl_alpha:I = 0x7f080111

.field public static final abc_textfield_search_material:I = 0x7f080112

.field public static final abc_vector_test:I = 0x7f080113

.field public static final abunation_list_0000010000:I = 0x7f080114

.field public static final accelerometer_settings:I = 0x7f080115

.field public static final access_control_settings:I = 0x7f080116

.field public static final accessibility_auto_added_qs_tooltip_illustration:I = 0x7f080117

.field public static final accessibility_button_navigation:I = 0x7f080118

.field public static final accessibility_button_navigation_miui:I = 0x7f080119

.field public static final accessibility_button_preview_base:I = 0x7f08011a

.field public static final accessibility_button_preview_large_floating_menu:I = 0x7f08011b

.field public static final accessibility_button_preview_small_floating_menu:I = 0x7f08011c

.field public static final accessibility_button_preview_three_finger:I = 0x7f08011d

.field public static final accessibility_button_preview_two_finger:I = 0x7f08011e

.field public static final accessibility_captions_banner:I = 0x7f08011f

.field public static final accessibility_color_inversion_banner:I = 0x7f080120

.field public static final accessibility_color_inversion_banner_pad_horizontal:I = 0x7f080121

.field public static final accessibility_color_inversion_banner_pad_vertical:I = 0x7f080122

.field public static final accessibility_color_inversion_banner_phone:I = 0x7f080123

.field public static final accessibility_dwell:I = 0x7f080124

.field public static final accessibility_magnification_banner:I = 0x7f080125

.field public static final accessibility_qs_tooltip_background:I = 0x7f080126

.field public static final accessibility_qs_tooltip_illustration:I = 0x7f080127

.field public static final accessibility_selecttospeak_pad_horizontal:I = 0x7f080128

.field public static final accessibility_selecttospeak_pad_vertical:I = 0x7f080129

.field public static final accessibility_shortcut_type_hardware:I = 0x7f08012a

.field public static final accessibility_shortcut_type_hardware_pad_horizontal:I = 0x7f08012b

.field public static final accessibility_shortcut_type_hardware_pad_vertical:I = 0x7f08012c

.field public static final accessibility_shortcut_type_software:I = 0x7f08012d

.field public static final accessibility_shortcut_type_software_floating:I = 0x7f08012e

.field public static final accessibility_shortcut_type_software_floating_pad:I = 0x7f08012f

.field public static final accessibility_shortcut_type_software_gesture:I = 0x7f080130

.field public static final accessibility_shortcut_type_software_gesture_pad_horizontal:I = 0x7f080131

.field public static final accessibility_shortcut_type_software_gesture_pad_vertical:I = 0x7f080132

.field public static final accessibility_shortcut_type_software_gesture_talkback:I = 0x7f080133

.field public static final accessibility_shortcut_type_software_pad:I = 0x7f080134

.field public static final accessibility_shortcut_type_triple_tap:I = 0x7f080135

.field public static final accessibility_shortcut_type_triple_tap_pad_horizontal:I = 0x7f080136

.field public static final accessibility_shortcut_type_triple_tap_pad_vertical:I = 0x7f080137

.field public static final accessibility_talkback_pad_horizontal:I = 0x7f080138

.field public static final accessibility_talkback_pad_vertical:I = 0x7f080139

.field public static final accessibility_talkback_phone:I = 0x7f08013a

.field public static final accessibility_text_reading_preview:I = 0x7f08013b

.field public static final account_unlogin_tip:I = 0x7f08013c

.field public static final action_bar_back:I = 0x7f08013d

.field public static final action_bar_bg_light:I = 0x7f08013e

.field public static final action_bar_dropshadow:I = 0x7f08013f

.field public static final action_bar_empty_logo:I = 0x7f080140

.field public static final action_bar_second_space:I = 0x7f080141

.field public static final action_bar_title_bg:I = 0x7f080142

.field public static final action_bar_transparent_bg:I = 0x7f080143

.field public static final action_bottom_add_vip_disable:I = 0x7f080144

.field public static final action_bottom_add_vip_normal:I = 0x7f080145

.field public static final action_bottom_add_vip_pressed:I = 0x7f080146

.field public static final action_button_auto_start:I = 0x7f080147

.field public static final action_button_auto_start_normal:I = 0x7f080148

.field public static final action_button_auto_start_pressed:I = 0x7f080149

.field public static final action_button_clear:I = 0x7f08014a

.field public static final action_button_clear_disable:I = 0x7f08014b

.field public static final action_button_clear_normal:I = 0x7f08014c

.field public static final action_button_clear_pressed:I = 0x7f08014d

.field public static final action_button_delete:I = 0x7f08014e

.field public static final action_button_discard:I = 0x7f08014f

.field public static final action_button_edit:I = 0x7f080150

.field public static final action_button_edit_rules:I = 0x7f080151

.field public static final action_button_new:I = 0x7f080152

.field public static final action_button_refresh:I = 0x7f080153

.field public static final action_button_refresh_disable_light:I = 0x7f080154

.field public static final action_button_refresh_normal_anim:I = 0x7f080155

.field public static final action_button_refresh_normal_light:I = 0x7f080156

.field public static final action_button_refresh_pressed_light:I = 0x7f080157

.field public static final action_button_reset_volume:I = 0x7f080158

.field public static final action_button_reset_volume_disable:I = 0x7f080159

.field public static final action_button_reset_volume_normal:I = 0x7f08015a

.field public static final action_button_reset_volume_pressed:I = 0x7f08015b

.field public static final action_button_search:I = 0x7f08015c

.field public static final action_button_search_disable:I = 0x7f08015d

.field public static final action_button_search_normal:I = 0x7f08015e

.field public static final action_button_search_normal_anim:I = 0x7f08015f

.field public static final action_button_search_pressed:I = 0x7f080160

.field public static final action_button_setting:I = 0x7f080161

.field public static final action_button_show_process:I = 0x7f080162

.field public static final action_button_show_process_disable:I = 0x7f080163

.field public static final action_button_show_process_normal:I = 0x7f080164

.field public static final action_button_show_process_pressed:I = 0x7f080165

.field public static final action_button_stop:I = 0x7f080166

.field public static final action_button_stop_disable:I = 0x7f080167

.field public static final action_button_stop_normal:I = 0x7f080168

.field public static final action_button_stop_pressed:I = 0x7f080169

.field public static final action_button_wifi_direct:I = 0x7f08016a

.field public static final action_button_wifi_direct_disable:I = 0x7f08016b

.field public static final action_button_wifi_direct_normal:I = 0x7f08016c

.field public static final action_button_wifi_direct_pressed:I = 0x7f08016d

.field public static final action_mode_title_button_cancel:I = 0x7f08016e

.field public static final action_mode_title_button_confirm:I = 0x7f08016f

.field public static final action_mode_title_button_deselect_all:I = 0x7f080170

.field public static final action_mode_title_button_select_all:I = 0x7f080171

.field public static final action_sync:I = 0x7f080172

.field public static final action_sync_anim:I = 0x7f080173

.field public static final actionbar_haptic_text_selector:I = 0x7f080174

.field public static final actionbar_text_selctor:I = 0x7f080175

.field public static final actionbar_text_selector:I = 0x7f080176

.field public static final adaptive_icon_drawable_wrapper:I = 0x7f080177

.field public static final adaptive_sleep:I = 0x7f080178

.field public static final add_a_photo_circled:I = 0x7f080179

.field public static final ai_button_icon:I = 0x7f08017a

.field public static final ai_gesture_header:I = 0x7f08017b

.field public static final ai_settings_back:I = 0x7f08017c

.field public static final ai_settings_back_normal:I = 0x7f08017d

.field public static final ai_settings_back_pressed:I = 0x7f08017e

.field public static final ai_settings_picker:I = 0x7f08017f

.field public static final ai_settings_sub_item_bg:I = 0x7f080180

.field public static final ai_settings_sub_none_item_bg:I = 0x7f080181

.field public static final airplane_mode:I = 0x7f080182

.field public static final alarm_volume_icon:I = 0x7f080183

.field public static final all_fingerprint_finish:I = 0x7f080184

.field public static final android_beam_sample:I = 0x7f080185

.field public static final android_beam_settings:I = 0x7f080186

.field public static final ap_battery_10:I = 0x7f080187

.field public static final ap_battery_100:I = 0x7f080188

.field public static final ap_battery_100_connected:I = 0x7f080189

.field public static final ap_battery_10_connected:I = 0x7f08018a

.field public static final ap_battery_20:I = 0x7f08018b

.field public static final ap_battery_20_connected:I = 0x7f08018c

.field public static final ap_battery_30:I = 0x7f08018d

.field public static final ap_battery_30_connected:I = 0x7f08018e

.field public static final ap_battery_40:I = 0x7f08018f

.field public static final ap_battery_40_connected:I = 0x7f080190

.field public static final ap_battery_50:I = 0x7f080191

.field public static final ap_battery_50_connected:I = 0x7f080192

.field public static final ap_battery_60:I = 0x7f080193

.field public static final ap_battery_60_connected:I = 0x7f080194

.field public static final ap_battery_70:I = 0x7f080195

.field public static final ap_battery_70_connected:I = 0x7f080196

.field public static final ap_battery_80:I = 0x7f080197

.field public static final ap_battery_80_connected:I = 0x7f080198

.field public static final ap_battery_90:I = 0x7f080199

.field public static final ap_battery_90_connected:I = 0x7f08019a

.field public static final app_filter_spinner_background:I = 0x7f08019b

.field public static final app_gauge:I = 0x7f08019c

.field public static final app_usage_arrow_selector:I = 0x7f08019d

.field public static final app_usage_widget_divide:I = 0x7f08019e

.field public static final appwidget_bg_holo:I = 0x7f08019f

.field public static final appwidget_inner_focused_c_holo:I = 0x7f0801a0

.field public static final appwidget_inner_focused_l_holo:I = 0x7f0801a1

.field public static final appwidget_inner_focused_r_holo:I = 0x7f0801a2

.field public static final appwidget_inner_pressed_c_holo:I = 0x7f0801a3

.field public static final appwidget_inner_pressed_l_holo:I = 0x7f0801a4

.field public static final appwidget_inner_pressed_r_holo:I = 0x7f0801a5

.field public static final appwidget_item_bg_normal:I = 0x7f0801a6

.field public static final appwidget_item_bg_pressed:I = 0x7f0801a7

.field public static final appwidget_settings_divider_holo:I = 0x7f0801a8

.field public static final appwidget_settings_ind_mid_c_holo:I = 0x7f0801a9

.field public static final appwidget_settings_ind_mid_l_holo:I = 0x7f0801aa

.field public static final appwidget_settings_ind_mid_r_holo:I = 0x7f0801ab

.field public static final appwidget_settings_ind_off_c_holo:I = 0x7f0801ac

.field public static final appwidget_settings_ind_off_l_holo:I = 0x7f0801ad

.field public static final appwidget_settings_ind_off_r_holo:I = 0x7f0801ae

.field public static final appwidget_settings_ind_on_c_holo:I = 0x7f0801af

.field public static final appwidget_settings_ind_on_l_holo:I = 0x7f0801b0

.field public static final appwidget_settings_ind_on_r_holo:I = 0x7f0801b1

.field public static final arrow_app_timer:I = 0x7f0801b2

.field public static final arrow_right:I = 0x7f0801b3

.field public static final arrow_right_light:I = 0x7f0801b4

.field public static final arrow_right_pressed:I = 0x7f0801b5

.field public static final auth_dialog_enterprise:I = 0x7f0801b6

.field public static final auto_awesome_battery:I = 0x7f0801b7

.field public static final auto_rule_first_item_bg:I = 0x7f0801b8

.field public static final auto_rule_last_item_bg:I = 0x7f0801b9

.field public static final auto_rule_middle_item_bg:I = 0x7f0801ba

.field public static final avatar_choose_photo_circled:I = 0x7f0801bb

.field public static final avatar_selector:I = 0x7f0801bc

.field public static final avatar_take_photo_circled:I = 0x7f0801bd

.field public static final avd_hide_password:I = 0x7f0801be

.field public static final avd_show_password:I = 0x7f0801bf

.field public static final back_finger_print:I = 0x7f0801c0

.field public static final back_image_exit:I = 0x7f0801c1

.field public static final background_round_rect:I = 0x7f0801c2

.field public static final backup_image:I = 0x7f0801c3

.field public static final band_wifi_24g:I = 0x7f0801c4

.field public static final band_wifi_24g_connected:I = 0x7f0801c5

.field public static final band_wifi_5g:I = 0x7f0801c6

.field public static final band_wifi_5g_connected:I = 0x7f0801c7

.field public static final battery10:I = 0x7f0801c8

.field public static final battery100:I = 0x7f0801c9

.field public static final battery100_charge:I = 0x7f0801ca

.field public static final battery10_charge:I = 0x7f0801cb

.field public static final battery20:I = 0x7f0801cc

.field public static final battery20_charge:I = 0x7f0801cd

.field public static final battery30:I = 0x7f0801ce

.field public static final battery30_charge:I = 0x7f0801cf

.field public static final battery40:I = 0x7f0801d0

.field public static final battery40_charge:I = 0x7f0801d1

.field public static final battery50:I = 0x7f0801d2

.field public static final battery50_charge:I = 0x7f0801d3

.field public static final battery60:I = 0x7f0801d4

.field public static final battery60_charge:I = 0x7f0801d5

.field public static final battery70:I = 0x7f0801d6

.field public static final battery70_charge:I = 0x7f0801d7

.field public static final battery80:I = 0x7f0801d8

.field public static final battery80_charge:I = 0x7f0801d9

.field public static final battery90:I = 0x7f0801da

.field public static final battery90_charge:I = 0x7f0801db

.field public static final battery_indicator_graphic:I = 0x7f0801dc

.field public static final battery_indicator_percent_in:I = 0x7f0801dd

.field public static final battery_indicator_percent_out:I = 0x7f0801de

.field public static final battery_indicator_settings:I = 0x7f0801df

.field public static final battery_indicator_top:I = 0x7f0801e0

.field public static final bg_btn_normal:I = 0x7f0801e1

.field public static final bg_btn_suggest:I = 0x7f0801e2

.field public static final bg_control_center_legacy:I = 0x7f0801e3

.field public static final bg_control_center_legacy_international:I = 0x7f0801e4

.field public static final bg_control_center_modern:I = 0x7f0801e5

.field public static final bg_control_center_modern_international:I = 0x7f0801e6

.field public static final bg_header_horizontal_tile:I = 0x7f0801e7

.field public static final bg_internal_storage_header:I = 0x7f0801e8

.field public static final bg_portable_storage_header:I = 0x7f0801e9

.field public static final bg_screen_effect:I = 0x7f0801ea

.field public static final bg_screen_effect_selected:I = 0x7f0801eb

.field public static final bg_screen_effect_unselected:I = 0x7f0801ec

.field public static final bg_screen_zoom_icons:I = 0x7f0801ed

.field public static final bg_screen_zoom_icons_horizon:I = 0x7f0801ee

.field public static final bg_setup_header:I = 0x7f0801ef

.field public static final bg_tile_teal:I = 0x7f0801f0

.field public static final blank_screen_icon:I = 0x7f0801f1

.field public static final bluetooth_broadcast_dialog_done:I = 0x7f0801f2

.field public static final bluetooth_device_light:I = 0x7f0801f3

.field public static final bluetooth_device_unlock_confirmed:I = 0x7f0801f4

.field public static final bluetooth_device_unlock_confirmed_for_huami_watch:I = 0x7f0801f5

.field public static final bluetooth_device_unlock_confirmed_for_miband2:I = 0x7f0801f6

.field public static final bluetooth_device_unlock_confirmed_for_midongband:I = 0x7f0801f7

.field public static final bluetooth_device_unlock_confirmed_for_midongwatchlite:I = 0x7f0801f8

.field public static final bluetooth_device_unlock_default:I = 0x7f0801f9

.field public static final bluetooth_device_unlock_default_for_huami_watch:I = 0x7f0801fa

.field public static final bluetooth_device_unlock_default_for_miband2:I = 0x7f0801fb

.field public static final bluetooth_device_unlock_default_for_midongband:I = 0x7f0801fc

.field public static final bluetooth_device_unlock_default_for_midongwatchlite:I = 0x7f0801fd

.field public static final bootloader_lock:I = 0x7f0801fe

.field public static final bootloader_unlock:I = 0x7f0801ff

.field public static final borderlayout_bg:I = 0x7f080200

.field public static final bottom_bar_bg:I = 0x7f080201

.field public static final bottom_native_layout_sos:I = 0x7f080202

.field public static final brightness_settings:I = 0x7f080203

.field public static final broadcast_button_outline:I = 0x7f080204

.field public static final broadcast_dialog_btn_bg:I = 0x7f080205

.field public static final bt_codec_textview_background:I = 0x7f080206

.field public static final bt_headset_find_detail:I = 0x7f080207

.field public static final bt_headset_fitness_default:I = 0x7f080208

.field public static final bt_refresh:I = 0x7f080209

.field public static final bt_refresh_background:I = 0x7f08020a

.field public static final btn_bg:I = 0x7f08020b

.field public static final btn_bg_dialog_light:I = 0x7f08020c

.field public static final btn_bg_dialog_single_normal_light:I = 0x7f08020d

.field public static final btn_bg_dialog_single_pressed_light:I = 0x7f08020e

.field public static final btn_bg_normal_pressed:I = 0x7f08020f

.field public static final btn_bg_normal_selector:I = 0x7f080210

.field public static final btn_bg_suggest_pressed:I = 0x7f080211

.field public static final btn_bg_suggest_selector:I = 0x7f080212

.field public static final btn_bg_warn_dark:I = 0x7f080213

.field public static final btn_bg_warn_single_disable_dark:I = 0x7f080214

.field public static final btn_bg_warn_single_normal_dark:I = 0x7f080215

.field public static final btn_bg_warn_single_pressed_dark:I = 0x7f080216

.field public static final btn_checkbox_checked_mtrl:I = 0x7f080217

.field public static final btn_checkbox_checked_to_unchecked_mtrl_animation:I = 0x7f080218

.field public static final btn_checkbox_unchecked_mtrl:I = 0x7f080219

.field public static final btn_checkbox_unchecked_to_checked_mtrl_animation:I = 0x7f08021a

.field public static final btn_haptic_main_play:I = 0x7f08021b

.field public static final btn_inline_detail:I = 0x7f08021c

.field public static final btn_inline_detail_normal_light:I = 0x7f08021d

.field public static final btn_inline_detail_normal_light_drawable:I = 0x7f08021e

.field public static final btn_picker_bg_middle:I = 0x7f08021f

.field public static final btn_picker_review_pause_n:I = 0x7f080220

.field public static final btn_picker_review_pause_p:I = 0x7f080221

.field public static final btn_picker_review_play_n:I = 0x7f080222

.field public static final btn_picker_review_play_p:I = 0x7f080223

.field public static final btn_radio_off_mtrl:I = 0x7f080224

.field public static final btn_radio_off_to_on_mtrl_animation:I = 0x7f080225

.field public static final btn_radio_on_mtrl:I = 0x7f080226

.field public static final btn_radio_on_to_off_mtrl_animation:I = 0x7f080227

.field public static final btn_text_color_high_light:I = 0x7f080228

.field public static final btn_text_color_normal:I = 0x7f080229

.field public static final bubble_left:I = 0x7f08022a

.field public static final bubble_right:I = 0x7f08022b

.field public static final button_accessibility_menu_assistant_normal:I = 0x7f08022c

.field public static final button_accessibility_menu_assistant_pressed:I = 0x7f08022d

.field public static final button_accessibility_menu_assistant_selector:I = 0x7f08022e

.field public static final button_accessibility_menu_google_assistant_normal:I = 0x7f08022f

.field public static final button_accessibility_menu_google_assistant_pressed:I = 0x7f080230

.field public static final button_accessibility_menu_google_assistant_selector:I = 0x7f080231

.field public static final button_accessibility_menu_lock_settings_normal:I = 0x7f080232

.field public static final button_accessibility_menu_lock_settings_pressed:I = 0x7f080233

.field public static final button_accessibility_menu_lock_settings_selector:I = 0x7f080234

.field public static final button_accessibility_menu_notifications_normal:I = 0x7f080235

.field public static final button_accessibility_menu_notifications_pressed:I = 0x7f080236

.field public static final button_accessibility_menu_notifications_selector:I = 0x7f080237

.field public static final button_accessibility_menu_power_normal:I = 0x7f080238

.field public static final button_accessibility_menu_power_pressed:I = 0x7f080239

.field public static final button_accessibility_menu_power_settings_selector:I = 0x7f08023a

.field public static final button_accessibility_menu_quick_settings_normal:I = 0x7f08023b

.field public static final button_accessibility_menu_quick_settings_pressed:I = 0x7f08023c

.field public static final button_accessibility_menu_quick_settings_selector:I = 0x7f08023d

.field public static final button_accessibility_menu_screenshot_normal:I = 0x7f08023e

.field public static final button_accessibility_menu_screenshot_pressed:I = 0x7f08023f

.field public static final button_accessibility_menu_screenshot_selector:I = 0x7f080240

.field public static final button_accessibility_menu_settings_accessibility_normal:I = 0x7f080241

.field public static final button_accessibility_menu_settings_accessibility_pressed:I = 0x7f080242

.field public static final button_accessibility_menu_settings_accessibility_selector:I = 0x7f080243

.field public static final button_accessibility_menu_view_carousel_normal:I = 0x7f080244

.field public static final button_accessibility_menu_view_carousel_pressed:I = 0x7f080245

.field public static final button_accessibility_menu_view_carousel_selector:I = 0x7f080246

.field public static final button_accessibility_menu_volume_settings_normal:I = 0x7f080247

.field public static final button_accessibility_menu_volume_settings_pressed:I = 0x7f080248

.field public static final button_accessibility_menu_volume_settings_selector:I = 0x7f080249

.field public static final button_boot_left_disable:I = 0x7f08024a

.field public static final button_boot_left_normal:I = 0x7f08024b

.field public static final button_boot_left_pressed:I = 0x7f08024c

.field public static final button_boot_right_disable:I = 0x7f08024d

.field public static final button_boot_right_normal:I = 0x7f08024e

.field public static final button_boot_right_pressed:I = 0x7f08024f

.field public static final button_bootloader_left_selector:I = 0x7f080250

.field public static final button_bootloader_right_selector:I = 0x7f080251

.field public static final button_border_selected:I = 0x7f080252

.field public static final button_border_unselected:I = 0x7f080253

.field public static final button_ripple_radius:I = 0x7f080254

.field public static final button_selected_background_normal:I = 0x7f080255

.field public static final button_text_enable_normal_bg:I = 0x7f080256

.field public static final button_text_not_enable_normal_bg:I = 0x7f080257

.field public static final caption_background:I = 0x7f080258

.field public static final card_checked_corner:I = 0x7f080259

.field public static final card_icon_default:I = 0x7f08025a

.field public static final card_list_background:I = 0x7f08025b

.field public static final card_shape_corner:I = 0x7f08025c

.field public static final card_view_corner:I = 0x7f08025d

.field public static final category_bar_empty:I = 0x7f08025e

.field public static final category_bar_mask:I = 0x7f08025f

.field public static final checkbox_selected_background_normal:I = 0x7f080260

.field public static final choose_unlock_item_background:I = 0x7f080261

.field public static final choose_unlock_item_background_selected:I = 0x7f080262

.field public static final choose_unlock_item_bg_normal:I = 0x7f080263

.field public static final choose_unlock_item_bg_pressed:I = 0x7f080264

.field public static final choose_unlock_item_bg_selected:I = 0x7f080265

.field public static final choose_unlock_item_bg_selected_pressed:I = 0x7f080266

.field public static final circle:I = 0x7f080267

.field public static final circle_outline:I = 0x7f080268

.field public static final clear_all_data_text_bg_color:I = 0x7f080269

.field public static final clear_all_data_text_color:I = 0x7f08026a

.field public static final clickbottom01:I = 0x7f08026b

.field public static final clickbottom02:I = 0x7f08026c

.field public static final clickbottom03:I = 0x7f08026d

.field public static final clickbottom04:I = 0x7f08026e

.field public static final clickbottom05:I = 0x7f08026f

.field public static final clickbottom06:I = 0x7f080270

.field public static final clickbottom07:I = 0x7f080271

.field public static final clickbottom08:I = 0x7f080272

.field public static final clock_center:I = 0x7f080273

.field public static final clock_face:I = 0x7f080274

.field public static final clock_hour:I = 0x7f080275

.field public static final clock_minutes:I = 0x7f080276

.field public static final clock_overlay:I = 0x7f080277

.field public static final closeAnc_OFF:I = 0x7f080278

.field public static final closeAnc_ON:I = 0x7f080279

.field public static final close_icon:I = 0x7f08027a

.field public static final cloud_icon:I = 0x7f08027b

.field public static final color_bar_progress:I = 0x7f08027c

.field public static final color_mode_preview:I = 0x7f08027d

.field public static final color_mode_preview1:I = 0x7f08027e

.field public static final color_mode_preview2:I = 0x7f08027f

.field public static final color_mode_preview3:I = 0x7f080280

.field public static final color_preview:I = 0x7f080281

.field public static final color_thumb_icon:I = 0x7f080282

.field public static final com_android_thememanager:I = 0x7f080283

.field public static final com_miui_backup:I = 0x7f080284

.field public static final confirm_fingerprint_img:I = 0x7f080285

.field public static final confirm_password_use_fingerprint_btn_bg:I = 0x7f080286

.field public static final contextual_card_background:I = 0x7f080287

.field public static final conversation_message_icon:I = 0x7f080288

.field public static final conversation_message_text_info_view_background:I = 0x7f080289

.field public static final core_scan_gesture_back:I = 0x7f08028a

.field public static final core_scan_gesture_broadside:I = 0x7f08028b

.field public static final core_scan_gesture_font:I = 0x7f08028c

.field public static final core_scan_gesture_gxzw:I = 0x7f08028d

.field public static final core_scan_gesture_pad_broadside:I = 0x7f08028e

.field public static final credentials_image_21051182c:I = 0x7f08028f

.field public static final credentials_image_21051182g:I = 0x7f080290

.field public static final credentials_image_21051182g_india_20:I = 0x7f080291

.field public static final credentials_image_21061110ag:I = 0x7f080292

.field public static final credentials_image_21061119ag:I = 0x7f080293

.field public static final credentials_image_21061119al:I = 0x7f080294

.field public static final credentials_image_21061119bi_india_14:I = 0x7f080295

.field public static final credentials_image_21061119bi_india_15:I = 0x7f080296

.field public static final credentials_image_21061119bi_india_7:I = 0x7f080297

.field public static final credentials_image_21061119dg:I = 0x7f080298

.field public static final credentials_image_2106118c:I = 0x7f080299

.field public static final credentials_image_2107113sg:I = 0x7f08029a

.field public static final credentials_image_2107113si_india_2:I = 0x7f08029b

.field public static final credentials_image_2107113si_india_3:I = 0x7f08029c

.field public static final credentials_image_2107113sr:I = 0x7f08029d

.field public static final credentials_image_2107119dc:I = 0x7f08029e

.field public static final credentials_image_21081111rg:I = 0x7f08029f

.field public static final credentials_image_2109106a1i_india_2:I = 0x7f0802a0

.field public static final credentials_image_21091116ac:I = 0x7f0802a1

.field public static final credentials_image_21091116ag:I = 0x7f0802a2

.field public static final credentials_image_21091116ai_india_15:I = 0x7f0802a3

.field public static final credentials_image_21091116c:I = 0x7f0802a4

.field public static final credentials_image_21091116i_india_2:I = 0x7f0802a5

.field public static final credentials_image_21091116i_india_3:I = 0x7f0802a6

.field public static final credentials_image_21091116uc:I = 0x7f0802a7

.field public static final credentials_image_21091116ug:I = 0x7f0802a8

.field public static final credentials_image_21091116ui_india_2:I = 0x7f0802a9

.field public static final credentials_image_21091116ui_india_3:I = 0x7f0802aa

.field public static final credentials_image_2109119bc:I = 0x7f0802ab

.field public static final credentials_image_2109119dg:I = 0x7f0802ac

.field public static final credentials_image_2109119di_india_2:I = 0x7f0802ad

.field public static final credentials_image_2109119di_india_3:I = 0x7f0802ae

.field public static final credentials_image_211033mi_india_15:I = 0x7f0802af

.field public static final credentials_image_211033mi_india_2:I = 0x7f0802b0

.field public static final credentials_image_211033mi_india_3:I = 0x7f0802b1

.field public static final credentials_image_211033mi_india_7:I = 0x7f0802b2

.field public static final credentials_image_21121119sc:I = 0x7f0802b3

.field public static final credentials_image_21121119sg:I = 0x7f0802b4

.field public static final credentials_image_21121119vl:I = 0x7f0802b5

.field public static final credentials_image_21121210c:I = 0x7f0802b6

.field public static final credentials_image_21121210g:I = 0x7f0802b7

.field public static final credentials_image_2112123ac:I = 0x7f0802b8

.field public static final credentials_image_2112123ag:I = 0x7f0802b9

.field public static final credentials_image_22011119ti_india_14:I = 0x7f0802ba

.field public static final credentials_image_22011119ti_india_16:I = 0x7f0802bb

.field public static final credentials_image_22011119ti_india_7:I = 0x7f0802bc

.field public static final credentials_image_22011119uy:I = 0x7f0802bd

.field public static final credentials_image_2201116pg:I = 0x7f0802be

.field public static final credentials_image_2201116pi_india_14:I = 0x7f0802bf

.field public static final credentials_image_2201116pi_india_17:I = 0x7f0802c0

.field public static final credentials_image_2201116pi_india_2:I = 0x7f0802c1

.field public static final credentials_image_2201116pi_india_7:I = 0x7f0802c2

.field public static final credentials_image_2201116sc:I = 0x7f0802c3

.field public static final credentials_image_2201116sg:I = 0x7f0802c4

.field public static final credentials_image_2201116si_india_14:I = 0x7f0802c5

.field public static final credentials_image_2201116si_india_17:I = 0x7f0802c6

.field public static final credentials_image_2201116si_india_2:I = 0x7f0802c7

.field public static final credentials_image_2201116si_india_7:I = 0x7f0802c8

.field public static final credentials_image_2201116sr:I = 0x7f0802c9

.field public static final credentials_image_2201116tg:I = 0x7f0802ca

.field public static final credentials_image_2201116ti_india_14:I = 0x7f0802cb

.field public static final credentials_image_2201116ti_india_17:I = 0x7f0802cc

.field public static final credentials_image_2201116ti_india_2:I = 0x7f0802cd

.field public static final credentials_image_2201116ti_india_7:I = 0x7f0802ce

.field public static final credentials_image_2201117pg:I = 0x7f0802cf

.field public static final credentials_image_2201117pi_india_2:I = 0x7f0802d0

.field public static final credentials_image_2201117pi_india_3:I = 0x7f0802d1

.field public static final credentials_image_2201117sg:I = 0x7f0802d2

.field public static final credentials_image_2201117si_india_2:I = 0x7f0802d3

.field public static final credentials_image_2201117si_india_3:I = 0x7f0802d4

.field public static final credentials_image_2201117sl:I = 0x7f0802d5

.field public static final credentials_image_2201117sy:I = 0x7f0802d6

.field public static final credentials_image_2201117tg:I = 0x7f0802d7

.field public static final credentials_image_2201117ti_india_15:I = 0x7f0802d8

.field public static final credentials_image_2201117ti_india_2:I = 0x7f0802d9

.field public static final credentials_image_2201117ti_india_3:I = 0x7f0802da

.field public static final credentials_image_2201117tl:I = 0x7f0802db

.field public static final credentials_image_2201117ty:I = 0x7f0802dc

.field public static final credentials_image_22011211c:I = 0x7f0802dd

.field public static final credentials_image_22011211g:I = 0x7f0802de

.field public static final credentials_image_2201122c:I = 0x7f0802df

.field public static final credentials_image_2201122g:I = 0x7f0802e0

.field public static final credentials_image_2201122g_india_2:I = 0x7f0802e1

.field public static final credentials_image_2201122g_india_3:I = 0x7f0802e2

.field public static final credentials_image_2201123c:I = 0x7f0802e3

.field public static final credentials_image_2201123g:I = 0x7f0802e4

.field public static final credentials_image_22021119kr:I = 0x7f0802e5

.field public static final credentials_image_22021211rc:I = 0x7f0802e6

.field public static final credentials_image_22021211rg:I = 0x7f0802e7

.field public static final credentials_image_22021211ri_india_15:I = 0x7f0802e8

.field public static final credentials_image_220233l2c:I = 0x7f0802e9

.field public static final credentials_image_220233l2g:I = 0x7f0802ea

.field public static final credentials_image_220233l2i_india_14:I = 0x7f0802eb

.field public static final credentials_image_220233l2i_india_15:I = 0x7f0802ec

.field public static final credentials_image_220233l2i_india_7:I = 0x7f0802ed

.field public static final credentials_image_22031116ai_india_15:I = 0x7f0802ee

.field public static final credentials_image_22031116bg:I = 0x7f0802ef

.field public static final credentials_image_2203121c:I = 0x7f0802f0

.field public static final credentials_image_2203129g:I = 0x7f0802f1

.field public static final credentials_image_220333qag:I = 0x7f0802f2

.field public static final credentials_image_220333qbi_india_17:I = 0x7f0802f3

.field public static final credentials_image_220333qbi_india_2:I = 0x7f0802f4

.field public static final credentials_image_220333qbi_india_3:I = 0x7f0802f5

.field public static final credentials_image_220333ql:I = 0x7f0802f6

.field public static final credentials_image_220333qny:I = 0x7f0802f7

.field public static final credentials_image_220333qpg:I = 0x7f0802f8

.field public static final credentials_image_22041211ac:I = 0x7f0802f9

.field public static final credentials_image_22041216c:I = 0x7f0802fa

.field public static final credentials_image_22041216g:I = 0x7f0802fb

.field public static final credentials_image_22041216i_india_2:I = 0x7f0802fc

.field public static final credentials_image_22041216i_india_3:I = 0x7f0802fd

.field public static final credentials_image_22041216uc:I = 0x7f0802fe

.field public static final credentials_image_22041219c:I = 0x7f0802ff

.field public static final credentials_image_22041219g:I = 0x7f080300

.field public static final credentials_image_22041219i_india_2:I = 0x7f080301

.field public static final credentials_image_22041219i_india_3:I = 0x7f080302

.field public static final credentials_image_22041219i_india_7:I = 0x7f080303

.field public static final credentials_image_22041219ny:I = 0x7f080304

.field public static final credentials_image_22041219pg:I = 0x7f080305

.field public static final credentials_image_22041219pi_india_2:I = 0x7f080306

.field public static final credentials_image_22041219pi_india_3:I = 0x7f080307

.field public static final credentials_image_22061218c:I = 0x7f080308

.field public static final credentials_image_2206122sc:I = 0x7f080309

.field public static final credentials_image_2206123sc:I = 0x7f08030a

.field public static final credentials_image_22071212ag:I = 0x7f08030b

.field public static final credentials_image_2207122mc:I = 0x7f08030c

.field public static final credentials_image_22081212c:I = 0x7f08030d

.field public static final credentials_image_22081212r:I = 0x7f08030e

.field public static final credentials_image_22081212ug:I = 0x7f08030f

.field public static final credentials_image_22081212ug_art:I = 0x7f080310

.field public static final credentials_image_22081281ac:I = 0x7f080311

.field public static final credentials_image_2209129sc:I = 0x7f080312

.field public static final credentials_image_2210129sg:I = 0x7f080313

.field public static final credentials_image_a001xm:I = 0x7f080314

.field public static final credentials_image_a101xm:I = 0x7f080315

.field public static final credentials_image_a201xm:I = 0x7f080316

.field public static final credentials_image_cm:I = 0x7f080317

.field public static final credentials_image_ct:I = 0x7f080318

.field public static final credentials_image_default:I = 0x7f080319

.field public static final credentials_image_m1803d5xa:I = 0x7f08031a

.field public static final credentials_image_m1803d5xe:I = 0x7f08031b

.field public static final credentials_image_m1803e1a:I = 0x7f08031c

.field public static final credentials_image_m1803e1a_global:I = 0x7f08031d

.field public static final credentials_image_m1803e6e:I = 0x7f08031e

.field public static final credentials_image_m1803e6g:I = 0x7f08031f

.field public static final credentials_image_m1803e6h:I = 0x7f080320

.field public static final credentials_image_m1803e6i_india_2:I = 0x7f080321

.field public static final credentials_image_m1803e7sg:I = 0x7f080322

.field public static final credentials_image_m1803e7sh:I = 0x7f080323

.field public static final credentials_image_m1804d2se:I = 0x7f080324

.field public static final credentials_image_m1804e4a:I = 0x7f080325

.field public static final credentials_image_m1805d1se:I = 0x7f080326

.field public static final credentials_image_m1805d1si_india_2:I = 0x7f080327

.field public static final credentials_image_m1805d1si_india_3:I = 0x7f080328

.field public static final credentials_image_m1805e10a:I = 0x7f080329

.field public static final credentials_image_m1805e10a__india_2:I = 0x7f08032a

.field public static final credentials_image_m1805e10a_india_2:I = 0x7f08032b

.field public static final credentials_image_m1805e10a_india_3:I = 0x7f08032c

.field public static final credentials_image_m1805e2a:I = 0x7f08032d

.field public static final credentials_image_m1806e7tg:I = 0x7f08032e

.field public static final credentials_image_m1806e7th:I = 0x7f08032f

.field public static final credentials_image_m1806e7ti_india_2:I = 0x7f080330

.field public static final credentials_image_m1806e7ti_india_3:I = 0x7f080331

.field public static final credentials_image_m1807e8a:I = 0x7f080332

.field public static final credentials_image_m1807e8s:I = 0x7f080333

.field public static final credentials_image_m1808d2te:I = 0x7f080334

.field public static final credentials_image_m1808d2tg:I = 0x7f080335

.field public static final credentials_image_m1810e5a:I = 0x7f080336

.field public static final credentials_image_m1810e5a_global:I = 0x7f080337

.field public static final credentials_image_m1810e5e:I = 0x7f080338

.field public static final credentials_image_m1810e5gg:I = 0x7f080339

.field public static final credentials_image_m1810f6g:I = 0x7f08033a

.field public static final credentials_image_m1810f6i_india_2:I = 0x7f08033b

.field public static final credentials_image_m1810f6i_india_3:I = 0x7f08033c

.field public static final credentials_image_m1810f6i_india_8:I = 0x7f08033d

.field public static final credentials_image_m1810f6le:I = 0x7f08033e

.field public static final credentials_image_m1810f6lg:I = 0x7f08033f

.field public static final credentials_image_m1810f6lh:I = 0x7f080340

.field public static final credentials_image_m1810f6li_india_2:I = 0x7f080341

.field public static final credentials_image_m1810f6li_india_3:I = 0x7f080342

.field public static final credentials_image_m1901f7be:I = 0x7f080343

.field public static final credentials_image_m1901f7e:I = 0x7f080344

.field public static final credentials_image_m1901f7g:I = 0x7f080345

.field public static final credentials_image_m1901f7h:I = 0x7f080346

.field public static final credentials_image_m1901f7i_india_14:I = 0x7f080347

.field public static final credentials_image_m1901f7i_india_2:I = 0x7f080348

.field public static final credentials_image_m1901f7i_india_3:I = 0x7f080349

.field public static final credentials_image_m1901f7i_india_8:I = 0x7f08034a

.field public static final credentials_image_m1901f7s_india_2:I = 0x7f08034b

.field public static final credentials_image_m1901f7s_india_3:I = 0x7f08034c

.field public static final credentials_image_m1902f1a:I = 0x7f08034d

.field public static final credentials_image_m1902f1g:I = 0x7f08034e

.field public static final credentials_image_m1903c3ee:I = 0x7f08034f

.field public static final credentials_image_m1903c3eg:I = 0x7f080350

.field public static final credentials_image_m1903c3eh:I = 0x7f080351

.field public static final credentials_image_m1903c3ei_india_14:I = 0x7f080352

.field public static final credentials_image_m1903c3ei_india_2:I = 0x7f080353

.field public static final credentials_image_m1903c3ei_india_3:I = 0x7f080354

.field public static final credentials_image_m1903c3ei_india_8:I = 0x7f080355

.field public static final credentials_image_m1903f10a:I = 0x7f080356

.field public static final credentials_image_m1903f10g:I = 0x7f080357

.field public static final credentials_image_m1903f10i_india_2:I = 0x7f080358

.field public static final credentials_image_m1903f10i_india_3:I = 0x7f080359

.field public static final credentials_image_m1903f11a:I = 0x7f08035a

.field public static final credentials_image_m1903f11g:I = 0x7f08035b

.field public static final credentials_image_m1903f11i_india_2:I = 0x7f08035c

.field public static final credentials_image_m1903f11i_india_3:I = 0x7f08035d

.field public static final credentials_image_m1903f2a:I = 0x7f08035e

.field public static final credentials_image_m1903f2g:I = 0x7f08035f

.field public static final credentials_image_m1904f3bc:I = 0x7f080360

.field public static final credentials_image_m1904f3bg:I = 0x7f080361

.field public static final credentials_image_m1904f3bt:I = 0x7f080362

.field public static final credentials_image_m1906f9sc:I = 0x7f080363

.field public static final credentials_image_m1906g7e:I = 0x7f080364

.field public static final credentials_image_m1906g7g:I = 0x7f080365

.field public static final credentials_image_m1906g7i_india_2:I = 0x7f080366

.field public static final credentials_image_m1906g7i_india_3:I = 0x7f080367

.field public static final credentials_image_m1908c3ie:I = 0x7f080368

.field public static final credentials_image_m1908c3ig:I = 0x7f080369

.field public static final credentials_image_m1908c3ih:I = 0x7f08036a

.field public static final credentials_image_m1908c3ii_india_1:I = 0x7f08036b

.field public static final credentials_image_m1908c3ii_india_2:I = 0x7f08036c

.field public static final credentials_image_m1908c3ii_india_3:I = 0x7f08036d

.field public static final credentials_image_m1908c3ii_india_4:I = 0x7f08036e

.field public static final credentials_image_m1908c3je:I = 0x7f08036f

.field public static final credentials_image_m1908c3jg:I = 0x7f080370

.field public static final credentials_image_m1908c3jg_pa2:I = 0x7f080371

.field public static final credentials_image_m1908c3jgg:I = 0x7f080372

.field public static final credentials_image_m1908c3jh:I = 0x7f080373

.field public static final credentials_image_m1908c3ji_india_14:I = 0x7f080374

.field public static final credentials_image_m1908c3ji_india_2:I = 0x7f080375

.field public static final credentials_image_m1908c3ji_india_3:I = 0x7f080376

.field public static final credentials_image_m1908c3ji_india_6:I = 0x7f080377

.field public static final credentials_image_m1908c3ke:I = 0x7f080378

.field public static final credentials_image_m1908c3kg:I = 0x7f080379

.field public static final credentials_image_m1908c3kh:I = 0x7f08037a

.field public static final credentials_image_m1908c3ki_india_2:I = 0x7f08037b

.field public static final credentials_image_m1908c3ki_india_3:I = 0x7f08037c

.field public static final credentials_image_m1908c3xg:I = 0x7f08037d

.field public static final credentials_image_m1908f1xe:I = 0x7f08037e

.field public static final credentials_image_m1910f4e:I = 0x7f08037f

.field public static final credentials_image_m1910f4g:I = 0x7f080380

.field public static final credentials_image_m1910f4s:I = 0x7f080381

.field public static final credentials_image_m1912g7be:I = 0x7f080382

.field public static final credentials_image_m1912g7bi_india_14:I = 0x7f080383

.field public static final credentials_image_m1912g7bi_india_2:I = 0x7f080384

.field public static final credentials_image_m1912g7bi_india_3:I = 0x7f080385

.field public static final credentials_image_m2001c3k3i:I = 0x7f080386

.field public static final credentials_image_m2001c3k3i_india_11:I = 0x7f080387

.field public static final credentials_image_m2001c3k3i_india_15:I = 0x7f080388

.field public static final credentials_image_m2001c3k3i_india_2:I = 0x7f080389

.field public static final credentials_image_m2001c3k3i_india_3:I = 0x7f08038a

.field public static final credentials_image_m2001c3k3i_india_4:I = 0x7f08038b

.field public static final credentials_image_m2001g7ae:I = 0x7f08038c

.field public static final credentials_image_m2001g7ai_india_2:I = 0x7f08038d

.field public static final credentials_image_m2001g7ai_india_3:I = 0x7f08038e

.field public static final credentials_image_m2001g7ai_india_4:I = 0x7f08038f

.field public static final credentials_image_m2001j11c:I = 0x7f080390

.field public static final credentials_image_m2001j11c_cm:I = 0x7f080391

.field public static final credentials_image_m2001j11c_ct:I = 0x7f080392

.field public static final credentials_image_m2001j11e:I = 0x7f080393

.field public static final credentials_image_m2001j11e_cm:I = 0x7f080394

.field public static final credentials_image_m2001j11e_ct:I = 0x7f080395

.field public static final credentials_image_m2001j11i_india_2:I = 0x7f080396

.field public static final credentials_image_m2001j11i_india_3:I = 0x7f080397

.field public static final credentials_image_m2001j1c:I = 0x7f080398

.field public static final credentials_image_m2001j1g:I = 0x7f080399

.field public static final credentials_image_m2001j2c:I = 0x7f08039a

.field public static final credentials_image_m2001j2g:I = 0x7f08039b

.field public static final credentials_image_m2001j2i_india_1:I = 0x7f08039c

.field public static final credentials_image_m2002f4lg:I = 0x7f08039d

.field public static final credentials_image_m2002f4li_india_2:I = 0x7f08039e

.field public static final credentials_image_m2002f4li_india_3:I = 0x7f08039f

.field public static final credentials_image_m2002j9e:I = 0x7f0803a0

.field public static final credentials_image_m2002j9g:I = 0x7f0803a1

.field public static final credentials_image_m2002j9g_qorvo:I = 0x7f0803a2

.field public static final credentials_image_m2002j9r:I = 0x7f0803a3

.field public static final credentials_image_m2002j9s:I = 0x7f0803a4

.field public static final credentials_image_m2003j15sc:I = 0x7f0803a5

.field public static final credentials_image_m2003j15sg:I = 0x7f0803a6

.field public static final credentials_image_m2003j15si_india_11:I = 0x7f0803a7

.field public static final credentials_image_m2003j15si_india_12:I = 0x7f0803a8

.field public static final credentials_image_m2003j15si_india_17:I = 0x7f0803a9

.field public static final credentials_image_m2003j15si_india_2:I = 0x7f0803aa

.field public static final credentials_image_m2003j15si_india_3:I = 0x7f0803ab

.field public static final credentials_image_m2003j15ss:I = 0x7f0803ac

.field public static final credentials_image_m2003j6a1g:I = 0x7f0803ad

.field public static final credentials_image_m2003j6a1i_india_1:I = 0x7f0803ae

.field public static final credentials_image_m2003j6a1i_india_2:I = 0x7f0803af

.field public static final credentials_image_m2003j6a1i_india_3:I = 0x7f0803b0

.field public static final credentials_image_m2003j6a1r:I = 0x7f0803b1

.field public static final credentials_image_m2003j6b1i_india_2:I = 0x7f0803b2

.field public static final credentials_image_m2003j6b1i_india_3:I = 0x7f0803b3

.field public static final credentials_image_m2003j6b2g:I = 0x7f0803b4

.field public static final credentials_image_m2003j6ci_india_14:I = 0x7f0803b5

.field public static final credentials_image_m2004j11g:I = 0x7f0803b6

.field public static final credentials_image_m2004j19ag:I = 0x7f0803b7

.field public static final credentials_image_m2004j19c:I = 0x7f0803b8

.field public static final credentials_image_m2004j19g:I = 0x7f0803b9

.field public static final credentials_image_m2004j19i_india_11:I = 0x7f0803ba

.field public static final credentials_image_m2004j19i_india_14:I = 0x7f0803bb

.field public static final credentials_image_m2004j19i_india_2:I = 0x7f0803bc

.field public static final credentials_image_m2004j19i_india_3:I = 0x7f0803bd

.field public static final credentials_image_m2004j19i_india_7:I = 0x7f0803be

.field public static final credentials_image_m2004j19i_india_9:I = 0x7f0803bf

.field public static final credentials_image_m2004j19pi_india_14:I = 0x7f0803c0

.field public static final credentials_image_m2004j19pi_india_7:I = 0x7f0803c1

.field public static final credentials_image_m2004j19pi_india_9:I = 0x7f0803c2

.field public static final credentials_image_m2004j7ac:I = 0x7f0803c3

.field public static final credentials_image_m2004j7bc:I = 0x7f0803c4

.field public static final credentials_image_m2006c3lc:I = 0x7f0803c5

.field public static final credentials_image_m2006c3lg:I = 0x7f0803c6

.field public static final credentials_image_m2006c3li_india_11:I = 0x7f0803c7

.field public static final credentials_image_m2006c3li_india_14:I = 0x7f0803c8

.field public static final credentials_image_m2006c3li_india_15:I = 0x7f0803c9

.field public static final credentials_image_m2006c3li_india_2:I = 0x7f0803ca

.field public static final credentials_image_m2006c3li_india_3:I = 0x7f0803cb

.field public static final credentials_image_m2006c3li_india_4:I = 0x7f0803cc

.field public static final credentials_image_m2006c3li_india_6:I = 0x7f0803cd

.field public static final credentials_image_m2006c3li_india_7:I = 0x7f0803ce

.field public static final credentials_image_m2006c3lii_india_15:I = 0x7f0803cf

.field public static final credentials_image_m2006c3lii_india_2:I = 0x7f0803d0

.field public static final credentials_image_m2006c3lii_india_3:I = 0x7f0803d1

.field public static final credentials_image_m2006c3lii_india_4:I = 0x7f0803d2

.field public static final credentials_image_m2006c3lii_india_6:I = 0x7f0803d3

.field public static final credentials_image_m2006c3lii_india_9:I = 0x7f0803d4

.field public static final credentials_image_m2006c3lvg:I = 0x7f0803d5

.field public static final credentials_image_m2006c3mg:I = 0x7f0803d6

.field public static final credentials_image_m2006c3mi_india_2:I = 0x7f0803d7

.field public static final credentials_image_m2006c3mi_india_3:I = 0x7f0803d8

.field public static final credentials_image_m2006c3mi_india_4:I = 0x7f0803d9

.field public static final credentials_image_m2006c3mi_india_6:I = 0x7f0803da

.field public static final credentials_image_m2006c3mi_india_9:I = 0x7f0803db

.field public static final credentials_image_m2006c3mii_india_15:I = 0x7f0803dc

.field public static final credentials_image_m2006c3mii_india_2:I = 0x7f0803dd

.field public static final credentials_image_m2006c3mii_india_3:I = 0x7f0803de

.field public static final credentials_image_m2006c3mii_india_4:I = 0x7f0803df

.field public static final credentials_image_m2006c3mii_india_7:I = 0x7f0803e0

.field public static final credentials_image_m2006c3mii_india_9:I = 0x7f0803e1

.field public static final credentials_image_m2006c3mng:I = 0x7f0803e2

.field public static final credentials_image_m2006c3mt:I = 0x7f0803e3

.field public static final credentials_image_m2006j10c:I = 0x7f0803e4

.field public static final credentials_image_m2007j17c:I = 0x7f0803e5

.field public static final credentials_image_m2007j17g:I = 0x7f0803e6

.field public static final credentials_image_m2007j17i_india_2:I = 0x7f0803e7

.field public static final credentials_image_m2007j1sc:I = 0x7f0803e8

.field public static final credentials_image_m2007j20cg:I = 0x7f0803e9

.field public static final credentials_image_m2007j20ci_india_2:I = 0x7f0803ea

.field public static final credentials_image_m2007j20ci_india_3:I = 0x7f0803eb

.field public static final credentials_image_m2007j20ct:I = 0x7f0803ec

.field public static final credentials_image_m2007j22c:I = 0x7f0803ed

.field public static final credentials_image_m2007j22g:I = 0x7f0803ee

.field public static final credentials_image_m2007j3sc:I = 0x7f0803ef

.field public static final credentials_image_m2007j3sg:I = 0x7f0803f0

.field public static final credentials_image_m2007j3si_india_1:I = 0x7f0803f1

.field public static final credentials_image_m2007j3si_india_2:I = 0x7f0803f2

.field public static final credentials_image_m2007j3si_india_3:I = 0x7f0803f3

.field public static final credentials_image_m2007j3sp_india_1:I = 0x7f0803f4

.field public static final credentials_image_m2007j3sp_india_2:I = 0x7f0803f5

.field public static final credentials_image_m2007j3sp_india_3:I = 0x7f0803f6

.field public static final credentials_image_m2007j3sy:I = 0x7f0803f7

.field public static final credentials_image_m2010j19cg:I = 0x7f0803f8

.field public static final credentials_image_m2010j19ci_india_14:I = 0x7f0803f9

.field public static final credentials_image_m2010j19ct:I = 0x7f0803fa

.field public static final credentials_image_m2010j19sc:I = 0x7f0803fb

.field public static final credentials_image_m2010j19sg:I = 0x7f0803fc

.field public static final credentials_image_m2010j19si_india_14:I = 0x7f0803fd

.field public static final credentials_image_m2010j19si_india_2:I = 0x7f0803fe

.field public static final credentials_image_m2010j19sl:I = 0x7f0803ff

.field public static final credentials_image_m2010j19sr:I = 0x7f080400

.field public static final credentials_image_m2010j19st:I = 0x7f080401

.field public static final credentials_image_m2010j19sy:I = 0x7f080402

.field public static final credentials_image_m2011j18c:I = 0x7f080403

.field public static final credentials_image_m2011k2c:I = 0x7f080404

.field public static final credentials_image_m2011k2g:I = 0x7f080405

.field public static final credentials_image_m2012k10c:I = 0x7f080406

.field public static final credentials_image_m2012k11ac:I = 0x7f080407

.field public static final credentials_image_m2012k11ag:I = 0x7f080408

.field public static final credentials_image_m2012k11ai_india_2:I = 0x7f080409

.field public static final credentials_image_m2012k11ai_india_3:I = 0x7f08040a

.field public static final credentials_image_m2012k11c:I = 0x7f08040b

.field public static final credentials_image_m2012k11g:I = 0x7f08040c

.field public static final credentials_image_m2012k11i_india_2:I = 0x7f08040d

.field public static final credentials_image_m2012k11i_india_3:I = 0x7f08040e

.field public static final credentials_image_m2101k6c:I = 0x7f08040f

.field public static final credentials_image_m2101k6g:I = 0x7f080410

.field public static final credentials_image_m2101k6i_india_17:I = 0x7f080411

.field public static final credentials_image_m2101k6i_india_2:I = 0x7f080412

.field public static final credentials_image_m2101k6i_india_3:I = 0x7f080413

.field public static final credentials_image_m2101k6p_india_17:I = 0x7f080414

.field public static final credentials_image_m2101k6p_india_2:I = 0x7f080415

.field public static final credentials_image_m2101k6p_india_3:I = 0x7f080416

.field public static final credentials_image_m2101k6r:I = 0x7f080417

.field public static final credentials_image_m2101k7ag:I = 0x7f080418

.field public static final credentials_image_m2101k7ai_india_2:I = 0x7f080419

.field public static final credentials_image_m2101k7ai_india_3:I = 0x7f08041a

.field public static final credentials_image_m2101k7bg:I = 0x7f08041b

.field public static final credentials_image_m2101k7bi_india_15:I = 0x7f08041c

.field public static final credentials_image_m2101k7bi_india_2:I = 0x7f08041d

.field public static final credentials_image_m2101k7bi_india_3:I = 0x7f08041e

.field public static final credentials_image_m2101k7bl:I = 0x7f08041f

.field public static final credentials_image_m2101k7bny:I = 0x7f080420

.field public static final credentials_image_m2101k7br:I = 0x7f080421

.field public static final credentials_image_m2101k9ag:I = 0x7f080422

.field public static final credentials_image_m2101k9ai_india_14:I = 0x7f080423

.field public static final credentials_image_m2101k9c:I = 0x7f080424

.field public static final credentials_image_m2101k9g:I = 0x7f080425

.field public static final credentials_image_m2101k9r:I = 0x7f080426

.field public static final credentials_image_m2102j20sg:I = 0x7f080427

.field public static final credentials_image_m2102j20si_india_2:I = 0x7f080428

.field public static final credentials_image_m2102j20si_india_3:I = 0x7f080429

.field public static final credentials_image_m2102j2sc:I = 0x7f08042a

.field public static final credentials_image_m2102k1ac:I = 0x7f08042b

.field public static final credentials_image_m2102k1c:I = 0x7f08042c

.field public static final credentials_image_m2102k1g:I = 0x7f08042d

.field public static final credentials_image_m2102k1g_india_1:I = 0x7f08042e

.field public static final credentials_image_m2103k19c:I = 0x7f08042f

.field public static final credentials_image_m2103k19g:I = 0x7f080430

.field public static final credentials_image_m2103k19i_india_2:I = 0x7f080431

.field public static final credentials_image_m2103k19i_india_3:I = 0x7f080432

.field public static final credentials_image_m2103k19pg:I = 0x7f080433

.field public static final credentials_image_m2103k19pi_india_2:I = 0x7f080434

.field public static final credentials_image_m2103k19pi_india_3:I = 0x7f080435

.field public static final credentials_image_m2103k19py:I = 0x7f080436

.field public static final credentials_image_m2103k19y:I = 0x7f080437

.field public static final credentials_image_m2104k10ac:I = 0x7f080438

.field public static final credentials_image_m2104k10i_india_2:I = 0x7f080439

.field public static final credentials_image_m2104k10i_india_3:I = 0x7f08043a

.field public static final credentials_image_m2105k81ac:I = 0x7f08043b

.field public static final credentials_image_m2105k81c:I = 0x7f08043c

.field public static final credentials_image_mce16:I = 0x7f08043d

.field public static final credentials_image_mce8:I = 0x7f08043e

.field public static final credentials_image_mde5:I = 0x7f08043f

.field public static final credentials_image_mde5s:I = 0x7f080440

.field public static final credentials_image_mee7s:I = 0x7f080441

.field public static final credentials_image_mei7s_india_2:I = 0x7f080442

.field public static final credentials_image_mei7s_india_6:I = 0x7f080443

.field public static final credentials_image_xig01:I = 0x7f080444

.field public static final credentials_image_xig02:I = 0x7f080445

.field public static final crypt_button_text_color:I = 0x7f080446

.field public static final crypt_keeper_progress_style:I = 0x7f080447

.field public static final custom_sound_text_selector:I = 0x7f080448

.field public static final dark_theme:I = 0x7f080449

.field public static final data_grid_border:I = 0x7f08044a

.field public static final data_grid_primary:I = 0x7f08044b

.field public static final data_grid_secondary:I = 0x7f08044c

.field public static final data_sweep_left_activated:I = 0x7f08044d

.field public static final data_sweep_left_default:I = 0x7f08044e

.field public static final data_sweep_limit:I = 0x7f08044f

.field public static final data_sweep_limit_default:I = 0x7f080450

.field public static final data_sweep_right_activated:I = 0x7f080451

.field public static final data_sweep_right_default:I = 0x7f080452

.field public static final data_sweep_warning:I = 0x7f080453

.field public static final data_sweep_warning_default:I = 0x7f080454

.field public static final data_usage_sweep_background:I = 0x7f080455

.field public static final default_headset:I = 0x7f080456

.field public static final default_preference_background:I = 0x7f080457

.field public static final deferred_setup_corner:I = 0x7f080458

.field public static final deferred_setup_corner_error:I = 0x7f080459

.field public static final deferred_setup_corner_guide:I = 0x7f08045a

.field public static final deferred_setup_corner_warning:I = 0x7f08045b

.field public static final demo_input_bg:I = 0x7f08045c

.field public static final design_fab_background:I = 0x7f08045d

.field public static final design_ic_visibility:I = 0x7f08045e

.field public static final design_ic_visibility_off:I = 0x7f08045f

.field public static final design_password_eye:I = 0x7f080460

.field public static final design_snackbar_background:I = 0x7f080461

.field public static final detail_background:I = 0x7f080462

.field public static final device_card_back_ground:I = 0x7f080463

.field public static final device_description_android:I = 0x7f080464

.field public static final device_description_battery:I = 0x7f080465

.field public static final device_description_camera:I = 0x7f080466

.field public static final device_description_cpu:I = 0x7f080467

.field public static final device_description_double_camera:I = 0x7f080468

.field public static final device_description_model:I = 0x7f080469

.field public static final device_description_ram:I = 0x7f08046a

.field public static final device_description_resolution:I = 0x7f08046b

.field public static final device_description_screen:I = 0x7f08046c

.field public static final device_description_snapdragon_cpu:I = 0x7f08046d

.field public static final device_info_content_bg:I = 0x7f08046e

.field public static final device_update_signal:I = 0x7f08046f

.field public static final device_update_signal1:I = 0x7f080470

.field public static final dialog_button_bar_bg:I = 0x7f080471

.field public static final dialog_meta:I = 0x7f080472

.field public static final display_top_image:I = 0x7f080473

.field public static final dndm_menu_add_vip:I = 0x7f080474

.field public static final dndm_stat_notify:I = 0x7f080475

.field public static final dndm_stat_notify_large:I = 0x7f080476

.field public static final dotted_line_480px:I = 0x7f080477

.field public static final double_click_power_key:I = 0x7f080478

.field public static final double_click_volume_down_when_lock:I = 0x7f080479

.field public static final double_g_v:I = 0x7f08047a

.field public static final dpp_back:I = 0x7f08047b

.field public static final dpp_qr_code:I = 0x7f08047c

.field public static final dpp_share_loading:I = 0x7f08047d

.field public static final drag_handle:I = 0x7f08047e

.field public static final drawable_app_usage_channel_select:I = 0x7f08047f

.field public static final drawable_app_usage_channel_unselect:I = 0x7f080480

.field public static final dream_default_preview_icon:I = 0x7f080481

.field public static final dream_preview_icon:I = 0x7f080482

.field public static final dream_preview_rounded_bg:I = 0x7f080483

.field public static final dynamic_item_icon:I = 0x7f080484

.field public static final edge_guide_hand:I = 0x7f080485

.field public static final edge_scan_gesture_font:I = 0x7f080486

.field public static final edit_device_name_bg:I = 0x7f080487

.field public static final edit_text_bg_spinner:I = 0x7f080488

.field public static final emergency_contacts_list_item_bg:I = 0x7f080489

.field public static final emergency_contacts_list_item_divider_bg:I = 0x7f08048a

.field public static final emergency_sos_icon:I = 0x7f08048b

.field public static final empty:I = 0x7f08048c

.field public static final empty_icon:I = 0x7f08048d

.field public static final enable_local_backup_bg:I = 0x7f08048e

.field public static final encroid_progress:I = 0x7f08048f

.field public static final encroid_resignin:I = 0x7f080490

.field public static final encroid_waiting:I = 0x7f080491

.field public static final enrollment_fingerprint_isolated:I = 0x7f080492

.field public static final enrollment_fingerprint_isolated_animation:I = 0x7f080493

.field public static final eq_config_bg:I = 0x7f080494

.field public static final eq_config_bg_normal:I = 0x7f080495

.field public static final eq_config_bg_pressed:I = 0x7f080496

.field public static final eq_progress_bg:I = 0x7f080497

.field public static final eq_thumb:I = 0x7f080498

.field public static final eq_thumb_normal:I = 0x7f080499

.field public static final eq_thumb_pressed:I = 0x7f08049a

.field public static final eq_title_bar_home:I = 0x7f08049b

.field public static final eq_title_bar_home_n:I = 0x7f08049c

.field public static final eq_title_bar_home_p:I = 0x7f08049d

.field public static final external_ram_notification:I = 0x7f08049e

.field public static final extra_dim_banner:I = 0x7f08049f

.field public static final face_detail_info_image:I = 0x7f0804a0

.field public static final face_enroll_icon_large:I = 0x7f0804a1

.field public static final face_enroll_intro_illustration:I = 0x7f0804a2

.field public static final face_enroll_name_delete:I = 0x7f0804a3

.field public static final face_input:I = 0x7f0804a4

.field public static final face_input_back:I = 0x7f0804a5

.field public static final face_unlock_input_cancel_img:I = 0x7f0804a6

.field public static final factory_settings:I = 0x7f0804a7

.field public static final fallback_home_unprovisioned_background:I = 0x7f0804a8

.field public static final fallback_progress:I = 0x7f0804a9

.field public static final fallback_progress_anim:I = 0x7f0804aa

.field public static final fc_01010402_3000:I = 0x7f0804ab

.field public static final fc_01010402_3001:I = 0x7f0804ac

.field public static final fc_01010402_30010:I = 0x7f0804ad

.field public static final fc_01010402_30011:I = 0x7f0804ae

.field public static final fc_01010402_30012:I = 0x7f0804af

.field public static final fc_01010402_30013:I = 0x7f0804b0

.field public static final fc_01010402_30014:I = 0x7f0804b1

.field public static final fc_01010402_30015:I = 0x7f0804b2

.field public static final fc_01010402_30016:I = 0x7f0804b3

.field public static final fc_01010402_30017:I = 0x7f0804b4

.field public static final fc_01010402_30018:I = 0x7f0804b5

.field public static final fc_01010402_30019:I = 0x7f0804b6

.field public static final fc_01010402_3002:I = 0x7f0804b7

.field public static final fc_01010402_30020:I = 0x7f0804b8

.field public static final fc_01010402_30021:I = 0x7f0804b9

.field public static final fc_01010402_30022:I = 0x7f0804ba

.field public static final fc_01010402_30023:I = 0x7f0804bb

.field public static final fc_01010402_30024:I = 0x7f0804bc

.field public static final fc_01010402_30025:I = 0x7f0804bd

.field public static final fc_01010402_30026:I = 0x7f0804be

.field public static final fc_01010402_30027:I = 0x7f0804bf

.field public static final fc_01010402_30028:I = 0x7f0804c0

.field public static final fc_01010402_30029:I = 0x7f0804c1

.field public static final fc_01010402_3003:I = 0x7f0804c2

.field public static final fc_01010402_30030:I = 0x7f0804c3

.field public static final fc_01010402_30031:I = 0x7f0804c4

.field public static final fc_01010402_30032:I = 0x7f0804c5

.field public static final fc_01010402_30033:I = 0x7f0804c6

.field public static final fc_01010402_30034:I = 0x7f0804c7

.field public static final fc_01010402_30035:I = 0x7f0804c8

.field public static final fc_01010402_30036:I = 0x7f0804c9

.field public static final fc_01010402_30037:I = 0x7f0804ca

.field public static final fc_01010402_30038:I = 0x7f0804cb

.field public static final fc_01010402_30039:I = 0x7f0804cc

.field public static final fc_01010402_3004:I = 0x7f0804cd

.field public static final fc_01010402_30040:I = 0x7f0804ce

.field public static final fc_01010402_30041:I = 0x7f0804cf

.field public static final fc_01010402_30042:I = 0x7f0804d0

.field public static final fc_01010402_30043:I = 0x7f0804d1

.field public static final fc_01010402_3005:I = 0x7f0804d2

.field public static final fc_01010402_3006:I = 0x7f0804d3

.field public static final fc_01010402_3007:I = 0x7f0804d4

.field public static final fc_01010402_3008:I = 0x7f0804d5

.field public static final fc_01010402_3009:I = 0x7f0804d6

.field public static final fc_01010600_3000:I = 0x7f0804d7

.field public static final fc_01010600_3001:I = 0x7f0804d8

.field public static final fc_01010600_30010:I = 0x7f0804d9

.field public static final fc_01010600_30011:I = 0x7f0804da

.field public static final fc_01010600_30012:I = 0x7f0804db

.field public static final fc_01010600_30013:I = 0x7f0804dc

.field public static final fc_01010600_30014:I = 0x7f0804dd

.field public static final fc_01010600_30015:I = 0x7f0804de

.field public static final fc_01010600_30016:I = 0x7f0804df

.field public static final fc_01010600_30017:I = 0x7f0804e0

.field public static final fc_01010600_30018:I = 0x7f0804e1

.field public static final fc_01010600_30019:I = 0x7f0804e2

.field public static final fc_01010600_3002:I = 0x7f0804e3

.field public static final fc_01010600_30020:I = 0x7f0804e4

.field public static final fc_01010600_30021:I = 0x7f0804e5

.field public static final fc_01010600_30022:I = 0x7f0804e6

.field public static final fc_01010600_30023:I = 0x7f0804e7

.field public static final fc_01010600_30024:I = 0x7f0804e8

.field public static final fc_01010600_30025:I = 0x7f0804e9

.field public static final fc_01010600_30026:I = 0x7f0804ea

.field public static final fc_01010600_30027:I = 0x7f0804eb

.field public static final fc_01010600_30028:I = 0x7f0804ec

.field public static final fc_01010600_30029:I = 0x7f0804ed

.field public static final fc_01010600_3003:I = 0x7f0804ee

.field public static final fc_01010600_30030:I = 0x7f0804ef

.field public static final fc_01010600_30031:I = 0x7f0804f0

.field public static final fc_01010600_30032:I = 0x7f0804f1

.field public static final fc_01010600_30033:I = 0x7f0804f2

.field public static final fc_01010600_30034:I = 0x7f0804f3

.field public static final fc_01010600_30035:I = 0x7f0804f4

.field public static final fc_01010600_30036:I = 0x7f0804f5

.field public static final fc_01010600_30037:I = 0x7f0804f6

.field public static final fc_01010600_30038:I = 0x7f0804f7

.field public static final fc_01010600_30039:I = 0x7f0804f8

.field public static final fc_01010600_3004:I = 0x7f0804f9

.field public static final fc_01010600_30040:I = 0x7f0804fa

.field public static final fc_01010600_30041:I = 0x7f0804fb

.field public static final fc_01010600_30042:I = 0x7f0804fc

.field public static final fc_01010600_30043:I = 0x7f0804fd

.field public static final fc_01010600_3005:I = 0x7f0804fe

.field public static final fc_01010600_3006:I = 0x7f0804ff

.field public static final fc_01010600_3007:I = 0x7f080500

.field public static final fc_01010600_3008:I = 0x7f080501

.field public static final fc_01010600_3009:I = 0x7f080502

.field public static final fhd_image:I = 0x7f080503

.field public static final file_icon_3gpp:I = 0x7f080504

.field public static final file_icon_aac:I = 0x7f080505

.field public static final file_icon_amr:I = 0x7f080506

.field public static final file_icon_ape:I = 0x7f080507

.field public static final file_icon_apk:I = 0x7f080508

.field public static final file_icon_audio:I = 0x7f080509

.field public static final file_icon_default:I = 0x7f08050a

.field public static final file_icon_doc:I = 0x7f08050b

.field public static final file_icon_dps:I = 0x7f08050c

.field public static final file_icon_dpt:I = 0x7f08050d

.field public static final file_icon_et:I = 0x7f08050e

.field public static final file_icon_ett:I = 0x7f08050f

.field public static final file_icon_flac:I = 0x7f080510

.field public static final file_icon_html:I = 0x7f080511

.field public static final file_icon_m4a:I = 0x7f080512

.field public static final file_icon_mid:I = 0x7f080513

.field public static final file_icon_mp3:I = 0x7f080514

.field public static final file_icon_ogg:I = 0x7f080515

.field public static final file_icon_pdf:I = 0x7f080516

.field public static final file_icon_picture:I = 0x7f080517

.field public static final file_icon_pps:I = 0x7f080518

.field public static final file_icon_ppt:I = 0x7f080519

.field public static final file_icon_rar:I = 0x7f08051a

.field public static final file_icon_theme:I = 0x7f08051b

.field public static final file_icon_txt:I = 0x7f08051c

.field public static final file_icon_vcf:I = 0x7f08051d

.field public static final file_icon_video:I = 0x7f08051e

.field public static final file_icon_wav:I = 0x7f08051f

.field public static final file_icon_wma:I = 0x7f080520

.field public static final file_icon_wps:I = 0x7f080521

.field public static final file_icon_wpt:I = 0x7f080522

.field public static final file_icon_xls:I = 0x7f080523

.field public static final file_icon_xml:I = 0x7f080524

.field public static final file_icon_zip:I = 0x7f080525

.field public static final fingerprint_detail_image:I = 0x7f080526

.field public static final fingerprint_detail_info_image:I = 0x7f080527

.field public static final fingerprint_enroll_finish:I = 0x7f080528

.field public static final fingerprint_enroll_introduction:I = 0x7f080529

.field public static final fingerprint_icon:I = 0x7f08052a

.field public static final fingerprint_sensor_location:I = 0x7f08052b

.field public static final flashback_guide:I = 0x7f08052c

.field public static final flashback_icon:I = 0x7f08052d

.field public static final flip_camera_for_selfie:I = 0x7f08052e

.field public static final font_setting_icon_apps:I = 0x7f08052f

.field public static final font_setting_icon_camera:I = 0x7f080530

.field public static final font_setting_icon_camera_g:I = 0x7f080531

.field public static final font_setting_icon_gallery:I = 0x7f080532

.field public static final font_setting_icon_gallery2:I = 0x7f080533

.field public static final font_setting_icon_gallery_g:I = 0x7f080534

.field public static final font_setting_icon_games:I = 0x7f080535

.field public static final font_setting_icon_google_g:I = 0x7f080536

.field public static final font_setting_icon_milife:I = 0x7f080537

.field public static final font_setting_icon_music:I = 0x7f080538

.field public static final font_setting_icon_note:I = 0x7f080539

.field public static final font_setting_icon_phone:I = 0x7f08053a

.field public static final font_setting_icon_playstore_g:I = 0x7f08053b

.field public static final font_setting_icon_searchbar:I = 0x7f08053c

.field public static final font_setting_icon_security:I = 0x7f08053d

.field public static final font_setting_icon_security_g:I = 0x7f08053e

.field public static final font_setting_icon_settings:I = 0x7f08053f

.field public static final font_setting_icon_settings_g:I = 0x7f080540

.field public static final font_setting_icon_theme:I = 0x7f080541

.field public static final font_setting_icon_theme_g:I = 0x7f080542

.field public static final font_setting_icon_tools:I = 0x7f080543

.field public static final font_setting_icon_tools_g:I = 0x7f080544

.field public static final font_setting_icon_video:I = 0x7f080545

.field public static final font_setting_icon_weather:I = 0x7f080546

.field public static final font_setting_icon_weather2:I = 0x7f080547

.field public static final font_settings_text_view_background:I = 0x7f080548

.field public static final font_size_settings_seekbar:I = 0x7f080549

.field public static final font_size_settings_seekbar_tick:I = 0x7f08054a

.field public static final font_view_call_background:I = 0x7f08054b

.field public static final forget_password_button_bg:I = 0x7f08054c

.field public static final fp_app_indicator:I = 0x7f08054d

.field public static final fp_illustration:I = 0x7f08054e

.field public static final fp_illustration_enrollment:I = 0x7f08054f

.field public static final fp_illustration_enrollment_animation:I = 0x7f080550

.field public static final free_wifi_indicator:I = 0x7f080551

.field public static final free_wifi_login_bg:I = 0x7f080552

.field public static final free_wifi_login_btn:I = 0x7f080553

.field public static final free_wifi_login_btn_n:I = 0x7f080554

.field public static final free_wifi_login_btn_p:I = 0x7f080555

.field public static final free_wifi_login_fail_bg:I = 0x7f080556

.field public static final free_wifi_login_fail_signal_1:I = 0x7f080557

.field public static final free_wifi_login_fail_signal_2:I = 0x7f080558

.field public static final free_wifi_login_fail_signal_3:I = 0x7f080559

.field public static final free_wifi_login_fail_signal_4:I = 0x7f08055a

.field public static final free_wifi_login_signal:I = 0x7f08055b

.field public static final free_wifi_login_signal_1:I = 0x7f08055c

.field public static final free_wifi_login_signal_2:I = 0x7f08055d

.field public static final free_wifi_login_signal_3:I = 0x7f08055e

.field public static final free_wifi_login_signal_4:I = 0x7f08055f

.field public static final free_wifi_login_signal_5:I = 0x7f080560

.field public static final free_wifi_speed_bg:I = 0x7f080561

.field public static final free_wifi_speed_indicator:I = 0x7f080562

.field public static final freeform_guide_drop_down_to_fullscreen:I = 0x7f080563

.field public static final freeform_guide_move:I = 0x7f080564

.field public static final freeform_guide_move_to_small_freeform_window:I = 0x7f080565

.field public static final freeform_guide_notification_drop_down:I = 0x7f080566

.field public static final freeform_guide_slide_to_small_freeform:I = 0x7f080567

.field public static final freeform_guide_slide_to_small_freeform_global:I = 0x7f080568

.field public static final freeform_guide_slide_up_to_close:I = 0x7f080569

.field public static final freeform_guide_to_sidehide:I = 0x7f08056a

.field public static final gesture_ambient_lift:I = 0x7f08056b

.field public static final gesture_ambient_tap:I = 0x7f08056c

.field public static final gesture_ambient_tap_screen:I = 0x7f08056d

.field public static final gesture_ambient_wake_lock_screen:I = 0x7f08056e

.field public static final gesture_ambient_wake_screen:I = 0x7f08056f

.field public static final gesture_assist:I = 0x7f080570

.field public static final gesture_double_tap:I = 0x7f080571

.field public static final gesture_fingerprint_swipe:I = 0x7f080572

.field public static final gesture_prevent_ringing:I = 0x7f080573

.field public static final gesture_twist:I = 0x7f080574

.field public static final gmscore_icon:I = 0x7f080575

.field public static final google_n:I = 0x7f080576

.field public static final gps_settings:I = 0x7f080577

.field public static final guard_icon:I = 0x7f080578

.field public static final guide_button_share:I = 0x7f080579

.field public static final guide_dot_focused:I = 0x7f08057a

.field public static final guide_dot_normal:I = 0x7f08057b

.field public static final guide_exit:I = 0x7f08057c

.field public static final gxzw_anim_aurora_normal:I = 0x7f08057d

.field public static final gxzw_anim_aurora_select:I = 0x7f08057e

.field public static final gxzw_anim_drawable:I = 0x7f08057f

.field public static final gxzw_anim_light_normal:I = 0x7f080580

.field public static final gxzw_anim_light_select:I = 0x7f080581

.field public static final gxzw_anim_pulse_normal:I = 0x7f080582

.field public static final gxzw_anim_pulse_select:I = 0x7f080583

.field public static final gxzw_anim_rhythm_normal:I = 0x7f080584

.field public static final gxzw_anim_rhythm_select:I = 0x7f080585

.field public static final gxzw_anim_star_normal:I = 0x7f080586

.field public static final gxzw_anim_star_select:I = 0x7f080587

.field public static final gxzw_aurora_recognizing_anim:I = 0x7f080588

.field public static final gxzw_light_recognizing_anim:I = 0x7f080589

.field public static final gxzw_none_recognizing_anim:I = 0x7f08058a

.field public static final gxzw_none_recognizing_anim_bg:I = 0x7f08058b

.field public static final gxzw_pulse_recognizing_anim:I = 0x7f08058c

.field public static final gxzw_recognizing_anim_bg:I = 0x7f08058d

.field public static final gxzw_selected_rectangle:I = 0x7f08058e

.field public static final gxzw_star_recognizing_anim:I = 0x7f08058f

.field public static final half_rounded_left_bk:I = 0x7f080590

.field public static final half_rounded_right_bk:I = 0x7f080591

.field public static final handwriting_input_icon:I = 0x7f080592

.field public static final handymode_guide_bg:I = 0x7f080593

.field public static final haptic_demo_background:I = 0x7f080594

.field public static final haptic_motor_icon:I = 0x7f080595

.field public static final haptic_play_button:I = 0x7f080596

.field public static final haptic_stop_button:I = 0x7f080597

.field public static final headset0000:I = 0x7f080598

.field public static final headset0001:I = 0x7f080599

.field public static final headset0002:I = 0x7f08059a

.field public static final headset0003:I = 0x7f08059b

.field public static final headset0004:I = 0x7f08059c

.field public static final headset0005:I = 0x7f08059d

.field public static final headset0006:I = 0x7f08059e

.field public static final headset0007:I = 0x7f08059f

.field public static final headset0008:I = 0x7f0805a0

.field public static final headset0009:I = 0x7f0805a1

.field public static final headset0010:I = 0x7f0805a2

.field public static final headset0011:I = 0x7f0805a3

.field public static final headset0012:I = 0x7f0805a4

.field public static final headset0013:I = 0x7f0805a5

.field public static final headset0014:I = 0x7f0805a6

.field public static final headset0015:I = 0x7f0805a7

.field public static final headset0016:I = 0x7f0805a8

.field public static final headset0017:I = 0x7f0805a9

.field public static final headset0018:I = 0x7f0805aa

.field public static final headset0019:I = 0x7f0805ab

.field public static final headset0020:I = 0x7f0805ac

.field public static final headset0021:I = 0x7f0805ad

.field public static final headset0022:I = 0x7f0805ae

.field public static final headset0023:I = 0x7f0805af

.field public static final headset0024:I = 0x7f0805b0

.field public static final headset0025:I = 0x7f0805b1

.field public static final headset0026:I = 0x7f0805b2

.field public static final headset0027:I = 0x7f0805b3

.field public static final headset0028:I = 0x7f0805b4

.field public static final headset0029:I = 0x7f0805b5

.field public static final headset0030:I = 0x7f0805b6

.field public static final headset0031:I = 0x7f0805b7

.field public static final headset0032:I = 0x7f0805b8

.field public static final headset0033:I = 0x7f0805b9

.field public static final headset0034:I = 0x7f0805ba

.field public static final headset0035:I = 0x7f0805bb

.field public static final headset0036:I = 0x7f0805bc

.field public static final headset0037:I = 0x7f0805bd

.field public static final headset0038:I = 0x7f0805be

.field public static final headset0039:I = 0x7f0805bf

.field public static final headset0040:I = 0x7f0805c0

.field public static final headset0041:I = 0x7f0805c1

.field public static final headset0042:I = 0x7f0805c2

.field public static final headset0043:I = 0x7f0805c3

.field public static final headset0044:I = 0x7f0805c4

.field public static final headset0045:I = 0x7f0805c5

.field public static final headset0046:I = 0x7f0805c6

.field public static final headset_TWS01S:I = 0x7f0805c7

.field public static final headset_TWS01_Black:I = 0x7f0805c8

.field public static final headset_TWS01_Gray:I = 0x7f0805c9

.field public static final headset_TWS01_Yellow:I = 0x7f0805ca

.field public static final headset_TWS02_black:I = 0x7f0805cb

.field public static final headset_TWS200:I = 0x7f0805cc

.field public static final headset_TWS_k77s:I = 0x7f0805cd

.field public static final headset_connect_blue:I = 0x7f0805ce

.field public static final headset_default_k73_black:I = 0x7f0805cf

.field public static final headset_default_k73_green:I = 0x7f0805d0

.field public static final headset_default_k73_lblue:I = 0x7f0805d1

.field public static final headset_default_k73_white:I = 0x7f0805d2

.field public static final headset_default_k73a_black_enc:I = 0x7f0805d3

.field public static final headset_default_k73a_green_enc:I = 0x7f0805d4

.field public static final headset_default_k73a_white_enc:I = 0x7f0805d5

.field public static final headset_default_k75_black_enc:I = 0x7f0805d6

.field public static final headset_default_k75_white_enc:I = 0x7f0805d7

.field public static final headset_device_icon:I = 0x7f0805d8

.field public static final headset_disconnect_red:I = 0x7f0805d9

.field public static final headset_earphone_both:I = 0x7f0805da

.field public static final headset_earphone_left:I = 0x7f0805db

.field public static final headset_earphone_right:I = 0x7f0805dc

.field public static final headset_find_device_TWS002_black:I = 0x7f0805dd

.field public static final headset_find_device_TWS01S:I = 0x7f0805de

.field public static final headset_find_device_TWS200:I = 0x7f0805df

.field public static final headset_find_device_k73_black:I = 0x7f0805e0

.field public static final headset_find_device_k73_green:I = 0x7f0805e1

.field public static final headset_find_device_k73_lblue:I = 0x7f0805e2

.field public static final headset_find_device_k73_white:I = 0x7f0805e3

.field public static final headset_find_device_k75_black_enc:I = 0x7f0805e4

.field public static final headset_find_device_k75_white_enc:I = 0x7f0805e5

.field public static final headset_fitness_TWS01S:I = 0x7f0805e6

.field public static final headset_fitness_TWS02_black:I = 0x7f0805e7

.field public static final headset_fitness_TWS200:I = 0x7f0805e8

.field public static final headset_fitness_icon:I = 0x7f0805e9

.field public static final headset_fitness_icon_Black:I = 0x7f0805ea

.field public static final headset_fitness_icon_Gray:I = 0x7f0805eb

.field public static final headset_fitness_icon_Yellow:I = 0x7f0805ec

.field public static final headset_fitness_k73_black:I = 0x7f0805ed

.field public static final headset_fitness_k73_green:I = 0x7f0805ee

.field public static final headset_fitness_k73_lblue:I = 0x7f0805ef

.field public static final headset_fitness_k73_white:I = 0x7f0805f0

.field public static final headset_fitness_k73a_black_enc:I = 0x7f0805f1

.field public static final headset_fitness_k73a_green_enc:I = 0x7f0805f2

.field public static final headset_fitness_k73a_white_enc:I = 0x7f0805f3

.field public static final headset_fitness_k75_black_enc:I = 0x7f0805f4

.field public static final headset_fitness_k75_white_enc:I = 0x7f0805f5

.field public static final headset_fitness_left:I = 0x7f0805f6

.field public static final headset_fitness_left_not_ok:I = 0x7f0805f7

.field public static final headset_fitness_left_ok:I = 0x7f0805f8

.field public static final headset_fitness_right:I = 0x7f0805f9

.field public static final headset_fitness_right_not_ok:I = 0x7f0805fa

.field public static final headset_fitness_right_ok:I = 0x7f0805fb

.field public static final headset_k77s_white:I = 0x7f0805fc

.field public static final headset_play_bell:I = 0x7f0805fd

.field public static final headsetrename:I = 0x7f0805fe

.field public static final high_performance_notif:I = 0x7f0805ff

.field public static final high_performance_notif_small:I = 0x7f080600

.field public static final holo_ab_home_up_divider:I = 0x7f080601

.field public static final home:I = 0x7f080602

.field public static final homepage_app_bar_background:I = 0x7f080603

.field public static final homepage_highlighted_item_background:I = 0x7f080604

.field public static final homepage_selectable_item_background:I = 0x7f080605

.field public static final ic_1x_mobiledata:I = 0x7f080606

.field public static final ic_3g_mobiledata:I = 0x7f080607

.field public static final ic_4g_lte_mobiledata:I = 0x7f080608

.field public static final ic_4g_lte_plus_mobiledata:I = 0x7f080609

.field public static final ic_4g_mobiledata:I = 0x7f08060a

.field public static final ic_4g_plus_mobiledata:I = 0x7f08060b

.field public static final ic_5g_e_mobiledata:I = 0x7f08060c

.field public static final ic_5g_mobiledata:I = 0x7f08060d

.field public static final ic_5g_plus_mobiledata:I = 0x7f08060e

.field public static final ic_5g_uc_mobiledata:I = 0x7f08060f

.field public static final ic_5g_uw_mobiledata:I = 0x7f080610

.field public static final ic_5g_uwb_mobiledata:I = 0x7f080611

.field public static final ic_a_big:I = 0x7f080612

.field public static final ic_a_small:I = 0x7f080613

.field public static final ic_about_settings:I = 0x7f080614

.field public static final ic_accelerometer_settings:I = 0x7f080615

.field public static final ic_accessibility_animation:I = 0x7f080616

.field public static final ic_accessibility_generic:I = 0x7f080617

.field public static final ic_accessibility_illustration_colorblind:I = 0x7f080618

.field public static final ic_accessibility_magnification:I = 0x7f080619

.field public static final ic_accessibility_new:I = 0x7f08061a

.field public static final ic_accessibility_page_indicator:I = 0x7f08061b

.field public static final ic_accessibility_settings:I = 0x7f08061c

.field public static final ic_accessibility_suggestion:I = 0x7f08061d

.field public static final ic_accessibility_visibility:I = 0x7f08061e

.field public static final ic_account_autostar:I = 0x7f08061f

.field public static final ic_account_autostart:I = 0x7f080620

.field public static final ic_account_avatar:I = 0x7f080621

.field public static final ic_account_circle:I = 0x7f080622

.field public static final ic_account_circle_24dp:I = 0x7f080623

.field public static final ic_account_circle_filled:I = 0x7f080624

.field public static final ic_account_circle_outline:I = 0x7f080625

.field public static final ic_account_list:I = 0x7f080626

.field public static final ic_account_settings:I = 0x7f080627

.field public static final ic_adaptive_font_download:I = 0x7f080628

.field public static final ic_add_24dp:I = 0x7f080629

.field public static final ic_add_40dp:I = 0x7f08062a

.field public static final ic_add_a_photo:I = 0x7f08062b

.field public static final ic_add_account_settings:I = 0x7f08062c

.field public static final ic_add_blue_24dp:I = 0x7f08062d

.field public static final ic_add_supervised_user:I = 0x7f08062e

.field public static final ic_add_to_home:I = 0x7f08062f

.field public static final ic_adv_audio:I = 0x7f080630

.field public static final ic_ai:I = 0x7f080631

.field public static final ic_ai_preloading:I = 0x7f080632

.field public static final ic_aiasst_call_screen:I = 0x7f080633

.field public static final ic_airplane_mode_settings:I = 0x7f080634

.field public static final ic_airplane_safe_networks_24dp:I = 0x7f080635

.field public static final ic_airplanemode_active:I = 0x7f080636

.field public static final ic_alarms_ring:I = 0x7f080637

.field public static final ic_alert_red:I = 0x7f080638

.field public static final ic_alexa_widget_icon:I = 0x7f080639

.field public static final ic_analytics_grey:I = 0x7f08063a

.field public static final ic_android:I = 0x7f08063b

.field public static final ic_android_beam_settings:I = 0x7f08063c

.field public static final ic_app_browser:I = 0x7f08063d

.field public static final ic_app_camera:I = 0x7f08063e

.field public static final ic_app_lock:I = 0x7f08063f

.field public static final ic_app_mms:I = 0x7f080640

.field public static final ic_app_permission:I = 0x7f080641

.field public static final ic_app_phone:I = 0x7f080642

.field public static final ic_app_photo:I = 0x7f080643

.field public static final ic_app_timer:I = 0x7f080644

.field public static final ic_app_timer_noti:I = 0x7f080645

.field public static final ic_app_weather:I = 0x7f080646

.field public static final ic_apps:I = 0x7f080647

.field public static final ic_apps_alt:I = 0x7f080648

.field public static final ic_apps_filter_settings_24dp:I = 0x7f080649

.field public static final ic_appwidget_settings_bluetooth_off_holo:I = 0x7f08064a

.field public static final ic_appwidget_settings_bluetooth_on_holo:I = 0x7f08064b

.field public static final ic_appwidget_settings_brightness_auto_holo:I = 0x7f08064c

.field public static final ic_appwidget_settings_brightness_full_holo:I = 0x7f08064d

.field public static final ic_appwidget_settings_brightness_half_holo:I = 0x7f08064e

.field public static final ic_appwidget_settings_brightness_off_holo:I = 0x7f08064f

.field public static final ic_appwidget_settings_gps_off_holo:I = 0x7f080650

.field public static final ic_appwidget_settings_gps_on_holo:I = 0x7f080651

.field public static final ic_appwidget_settings_location_off_holo:I = 0x7f080652

.field public static final ic_appwidget_settings_location_on_holo:I = 0x7f080653

.field public static final ic_appwidget_settings_location_saving_holo:I = 0x7f080654

.field public static final ic_appwidget_settings_sync_off_holo:I = 0x7f080655

.field public static final ic_appwidget_settings_sync_on_holo:I = 0x7f080656

.field public static final ic_appwidget_settings_wifi_off_holo:I = 0x7f080657

.field public static final ic_appwidget_settings_wifi_on_holo:I = 0x7f080658

.field public static final ic_arrow_back:I = 0x7f080659

.field public static final ic_arrow_detail_normal:I = 0x7f08065a

.field public static final ic_arrow_detail_selected:I = 0x7f08065b

.field public static final ic_arrow_down:I = 0x7f08065c

.field public static final ic_arrow_down_24dp:I = 0x7f08065d

.field public static final ic_arrow_downward:I = 0x7f08065e

.field public static final ic_arrow_forward:I = 0x7f08065f

.field public static final ic_attach_money_black_24dp:I = 0x7f080660

.field public static final ic_audio_adjustment:I = 0x7f080661

.field public static final ic_audio_alarm:I = 0x7f080662

.field public static final ic_audio_alarm_mute:I = 0x7f080663

.field public static final ic_audio_bt:I = 0x7f080664

.field public static final ic_audio_bt_mute:I = 0x7f080665

.field public static final ic_audio_description:I = 0x7f080666

.field public static final ic_audio_headset:I = 0x7f080667

.field public static final ic_audio_headset_mute:I = 0x7f080668

.field public static final ic_audio_media:I = 0x7f080669

.field public static final ic_audio_media_mute:I = 0x7f08066a

.field public static final ic_audio_notification:I = 0x7f08066b

.field public static final ic_audio_notification_mute:I = 0x7f08066c

.field public static final ic_audio_phone:I = 0x7f08066d

.field public static final ic_audio_ring_notif:I = 0x7f08066e

.field public static final ic_audio_ring_notif_mute:I = 0x7f08066f

.field public static final ic_audio_ring_notif_vibrate:I = 0x7f080670

.field public static final ic_audio_vol:I = 0x7f080671

.field public static final ic_audio_vol_mute:I = 0x7f080672

.field public static final ic_audiotrack_dark:I = 0x7f080673

.field public static final ic_audiotrack_light:I = 0x7f080674

.field public static final ic_auto_wifi:I = 0x7f080675

.field public static final ic_avatar_choose_photo:I = 0x7f080676

.field public static final ic_avatar_take_photo:I = 0x7f080677

.field public static final ic_battery_alert_24dp:I = 0x7f080678

.field public static final ic_battery_charging_full:I = 0x7f080679

.field public static final ic_battery_circle:I = 0x7f08067a

.field public static final ic_battery_low:I = 0x7f08067b

.field public static final ic_battery_saver_accent_24dp:I = 0x7f08067c

.field public static final ic_battery_settings:I = 0x7f08067d

.field public static final ic_battery_status_bad_24dp:I = 0x7f08067e

.field public static final ic_battery_status_good_24dp:I = 0x7f08067f

.field public static final ic_battery_status_maybe_24dp:I = 0x7f080680

.field public static final ic_ble_bracelet:I = 0x7f080681

.field public static final ic_ble_bracelet_bonded:I = 0x7f080682

.field public static final ic_ble_mikey:I = 0x7f080683

.field public static final ic_ble_mikey_bonded:I = 0x7f080684

.field public static final ic_ble_mivr_controller:I = 0x7f080685

.field public static final ic_ble_mivr_controller_bonded:I = 0x7f080686

.field public static final ic_ble_unknown:I = 0x7f080687

.field public static final ic_bluetooth_disabled:I = 0x7f080688

.field public static final ic_bluetooth_settings:I = 0x7f080689

.field public static final ic_bluetooth_unlock:I = 0x7f08068a

.field public static final ic_bluetooth_unlock_checked:I = 0x7f08068b

.field public static final ic_brightness_settings:I = 0x7f08068c

.field public static final ic_bt_audio_video_cra_audio:I = 0x7f08068d

.field public static final ic_bt_audio_video_hifi_audio:I = 0x7f08068e

.field public static final ic_bt_audio_video_microphone:I = 0x7f08068f

.field public static final ic_bt_audio_video_vcr:I = 0x7f080690

.field public static final ic_bt_bluetooth:I = 0x7f080691

.field public static final ic_bt_bluetooth_bonded:I = 0x7f080692

.field public static final ic_bt_broadcast:I = 0x7f080693

.field public static final ic_bt_broadcast_icon:I = 0x7f080694

.field public static final ic_bt_cellphone:I = 0x7f080695

.field public static final ic_bt_cellphone_bonded:I = 0x7f080696

.field public static final ic_bt_config:I = 0x7f080697

.field public static final ic_bt_digital_pen:I = 0x7f080698

.field public static final ic_bt_digital_pen_bonded:I = 0x7f080699

.field public static final ic_bt_headphones_a2dp:I = 0x7f08069a

.field public static final ic_bt_headphones_a2dp_bonded:I = 0x7f08069b

.field public static final ic_bt_headset_hfp:I = 0x7f08069c

.field public static final ic_bt_headset_hfp_bonded:I = 0x7f08069d

.field public static final ic_bt_hearing_aid:I = 0x7f08069e

.field public static final ic_bt_imaging:I = 0x7f08069f

.field public static final ic_bt_imaging_bonded:I = 0x7f0806a0

.field public static final ic_bt_keyboard_hid:I = 0x7f0806a1

.field public static final ic_bt_keyboard_hid_bonded:I = 0x7f0806a2

.field public static final ic_bt_laptop:I = 0x7f0806a3

.field public static final ic_bt_laptop_bonded:I = 0x7f0806a4

.field public static final ic_bt_ldac:I = 0x7f0806a5

.field public static final ic_bt_le_audio:I = 0x7f0806a6

.field public static final ic_bt_low_latency:I = 0x7f0806a7

.field public static final ic_bt_misc_hid:I = 0x7f0806a8

.field public static final ic_bt_misc_hid_bonded:I = 0x7f0806a9

.field public static final ic_bt_network_pan:I = 0x7f0806aa

.field public static final ic_bt_network_pan_bonded:I = 0x7f0806ab

.field public static final ic_bt_pointing_hid:I = 0x7f0806ac

.field public static final ic_bt_pointing_hid_bonded:I = 0x7f0806ad

.field public static final ic_bt_rarely_used:I = 0x7f0806ae

.field public static final ic_bt_tv:I = 0x7f0806af

.field public static final ic_bt_tv_bonded:I = 0x7f0806b0

.field public static final ic_bt_unknown:I = 0x7f0806b1

.field public static final ic_btn_next:I = 0x7f0806b2

.field public static final ic_bubble_all:I = 0x7f0806b3

.field public static final ic_bubble_none:I = 0x7f0806b4

.field public static final ic_bubble_selected:I = 0x7f0806b5

.field public static final ic_bugreport_settings:I = 0x7f0806b6

.field public static final ic_calendar_ring:I = 0x7f0806b7

.field public static final ic_call_24dp:I = 0x7f0806b8

.field public static final ic_call_grey_24dp:I = 0x7f0806b9

.field public static final ic_calls_sms:I = 0x7f0806ba

.field public static final ic_cancel:I = 0x7f0806bb

.field public static final ic_captioning:I = 0x7f0806bc

.field public static final ic_carrier_wifi:I = 0x7f0806bd

.field public static final ic_cast_24dp:I = 0x7f0806be

.field public static final ic_cellular_1_bar:I = 0x7f0806bf

.field public static final ic_cellular_off:I = 0x7f0806c0

.field public static final ic_check_box_anim:I = 0x7f0806c1

.field public static final ic_check_box_blue_24dp:I = 0x7f0806c2

.field public static final ic_check_box_outline_24dp:I = 0x7f0806c3

.field public static final ic_check_circle_green:I = 0x7f0806c4

.field public static final ic_check_green_24dp:I = 0x7f0806c5

.field public static final ic_checked_checkbox:I = 0x7f0806c6

.field public static final ic_checkmark:I = 0x7f0806c7

.field public static final ic_chevron_right_24dp:I = 0x7f0806c8

.field public static final ic_children_mode_settings:I = 0x7f0806c9

.field public static final ic_clear:I = 0x7f0806ca

.field public static final ic_clear_account_data:I = 0x7f0806cb

.field public static final ic_clear_app_data:I = 0x7f0806cc

.field public static final ic_clear_backup_data:I = 0x7f0806cd

.field public static final ic_clear_contact_data:I = 0x7f0806ce

.field public static final ic_clear_other_data:I = 0x7f0806cf

.field public static final ic_clear_sdcard_data:I = 0x7f0806d0

.field public static final ic_clock_black_24dp:I = 0x7f0806d1

.field public static final ic_color_and_motion:I = 0x7f0806d2

.field public static final ic_color_arrow_left_lt:I = 0x7f0806d3

.field public static final ic_color_arrow_right_lt:I = 0x7f0806d4

.field public static final ic_color_cicle:I = 0x7f0806d5

.field public static final ic_color_inversion:I = 0x7f0806d6

.field public static final ic_color_page_indicator_focused:I = 0x7f0806d7

.field public static final ic_color_page_indicator_unfocused:I = 0x7f0806d8

.field public static final ic_computer:I = 0x7f0806d9

.field public static final ic_contact_list_picture:I = 0x7f0806da

.field public static final ic_contact_photo_bg:I = 0x7f0806db

.field public static final ic_contact_photo_fg:I = 0x7f0806dc

.field public static final ic_contact_photo_mask:I = 0x7f0806dd

.field public static final ic_content_copy_grey600_24dp:I = 0x7f0806de

.field public static final ic_control_center_legacy:I = 0x7f0806df

.field public static final ic_control_center_legacy_international:I = 0x7f0806e0

.field public static final ic_control_center_modern:I = 0x7f0806e1

.field public static final ic_control_center_modern_international:I = 0x7f0806e2

.field public static final ic_create_bubble:I = 0x7f0806e3

.field public static final ic_daltonizer:I = 0x7f0806e4

.field public static final ic_dark_ui:I = 0x7f0806e5

.field public static final ic_data_saver:I = 0x7f0806e6

.field public static final ic_date_time_settings:I = 0x7f0806e7

.field public static final ic_delete:I = 0x7f0806e8

.field public static final ic_delete_accent:I = 0x7f0806e9

.field public static final ic_delete_x:I = 0x7f0806ea

.field public static final ic_demote_conversation:I = 0x7f0806eb

.field public static final ic_development_settings:I = 0x7f0806ec

.field public static final ic_device_connection:I = 0x7f0806ed

.field public static final ic_device_locked_24dp:I = 0x7f0806ee

.field public static final ic_devices_check_circle_green_32dp:I = 0x7f0806ef

.field public static final ic_devices_other:I = 0x7f0806f0

.field public static final ic_devices_other_32dp:I = 0x7f0806f1

.field public static final ic_dialog_close_dark:I = 0x7f0806f2

.field public static final ic_dialog_close_light:I = 0x7f0806f3

.field public static final ic_display_settings:I = 0x7f0806f4

.field public static final ic_do_not_disturb_mode_settings:I = 0x7f0806f5

.field public static final ic_do_not_disturb_on_24dp:I = 0x7f0806f6

.field public static final ic_download_for_offline:I = 0x7f0806f7

.field public static final ic_dream_check_circle:I = 0x7f0806f8

.field public static final ic_drive_mode:I = 0x7f0806f9

.field public static final ic_e_mobiledata:I = 0x7f0806fa

.field public static final ic_earbuds_advanced:I = 0x7f0806fb

.field public static final ic_edge_mode_settings:I = 0x7f0806fc

.field public static final ic_eject_24dp:I = 0x7f0806fd

.field public static final ic_embedded_spinner:I = 0x7f0806fe

.field public static final ic_embedded_spinner_select:I = 0x7f0806ff

.field public static final ic_emergency:I = 0x7f080700

.field public static final ic_emergency_gesture_24dp:I = 0x7f080701

.field public static final ic_enable_taplus:I = 0x7f080702

.field public static final ic_enable_video_pip:I = 0x7f080703

.field public static final ic_enhanced_connectivity:I = 0x7f080704

.field public static final ic_enterprise:I = 0x7f080705

.field public static final ic_event:I = 0x7f080706

.field public static final ic_event_white:I = 0x7f080707

.field public static final ic_expand:I = 0x7f080708

.field public static final ic_expand_less:I = 0x7f080709

.field public static final ic_expand_more_inverse:I = 0x7f08070a

.field public static final ic_face_24dp:I = 0x7f08070b

.field public static final ic_face_enroll_introduction_detail:I = 0x7f08070c

.field public static final ic_face_enroll_introduction_glasses:I = 0x7f08070d

.field public static final ic_face_enroll_introduction_people:I = 0x7f08070e

.field public static final ic_face_enroll_introduction_visibility:I = 0x7f08070f

.field public static final ic_face_header:I = 0x7f080710

.field public static final ic_face_unlock:I = 0x7f080711

.field public static final ic_face_unlock_checked:I = 0x7f080712

.field public static final ic_feedback_24dp:I = 0x7f080713

.field public static final ic_feedback_settings:I = 0x7f080714

.field public static final ic_find_device_disabled:I = 0x7f080715

.field public static final ic_find_device_enabled:I = 0x7f080716

.field public static final ic_find_in_page_24px:I = 0x7f080717

.field public static final ic_finger_unlock:I = 0x7f080718

.field public static final ic_finger_unlock_checked:I = 0x7f080719

.field public static final ic_fingerprint:I = 0x7f08071a

.field public static final ic_fingerprint_24dp:I = 0x7f08071b

.field public static final ic_fingerprint_header:I = 0x7f08071c

.field public static final ic_fingerprint_introduction_shield_24dp:I = 0x7f08071d

.field public static final ic_fingerprint_list_icon:I = 0x7f08071e

.field public static final ic_fingerprint_success:I = 0x7f08071f

.field public static final ic_float_notification:I = 0x7f080720

.field public static final ic_fold_screen_settings:I = 0x7f080721

.field public static final ic_folder_vd_theme_24:I = 0x7f080722

.field public static final ic_font_download:I = 0x7f080723

.field public static final ic_font_settings:I = 0x7f080724

.field public static final ic_font_size:I = 0x7f080725

.field public static final ic_font_size_16dp:I = 0x7f080726

.field public static final ic_font_size_24dp:I = 0x7f080727

.field public static final ic_force_bold:I = 0x7f080728

.field public static final ic_four_spinner:I = 0x7f080729

.field public static final ic_four_spinner_select:I = 0x7f08072a

.field public static final ic_frequency_antenna:I = 0x7f08072b

.field public static final ic_friction_lock_closed:I = 0x7f08072c

.field public static final ic_friction_money:I = 0x7f08072d

.field public static final ic_full_spinner:I = 0x7f08072e

.field public static final ic_full_spinner_select:I = 0x7f08072f

.field public static final ic_g_mobiledata:I = 0x7f080730

.field public static final ic_gesture_play_button:I = 0x7f080731

.field public static final ic_google_settings:I = 0x7f080732

.field public static final ic_google_wellbeing:I = 0x7f080733

.field public static final ic_gps_settings:I = 0x7f080734

.field public static final ic_gray_scale_24dp:I = 0x7f080735

.field public static final ic_grayedout_printer:I = 0x7f080736

.field public static final ic_group_collapse_00:I = 0x7f080737

.field public static final ic_group_collapse_01:I = 0x7f080738

.field public static final ic_group_collapse_02:I = 0x7f080739

.field public static final ic_group_collapse_03:I = 0x7f08073a

.field public static final ic_group_collapse_04:I = 0x7f08073b

.field public static final ic_group_collapse_05:I = 0x7f08073c

.field public static final ic_group_collapse_06:I = 0x7f08073d

.field public static final ic_group_collapse_07:I = 0x7f08073e

.field public static final ic_group_collapse_08:I = 0x7f08073f

.field public static final ic_group_collapse_09:I = 0x7f080740

.field public static final ic_group_collapse_10:I = 0x7f080741

.field public static final ic_group_collapse_11:I = 0x7f080742

.field public static final ic_group_collapse_12:I = 0x7f080743

.field public static final ic_group_collapse_13:I = 0x7f080744

.field public static final ic_group_collapse_14:I = 0x7f080745

.field public static final ic_group_collapse_15:I = 0x7f080746

.field public static final ic_group_expand_00:I = 0x7f080747

.field public static final ic_group_expand_01:I = 0x7f080748

.field public static final ic_group_expand_02:I = 0x7f080749

.field public static final ic_group_expand_03:I = 0x7f08074a

.field public static final ic_group_expand_04:I = 0x7f08074b

.field public static final ic_group_expand_05:I = 0x7f08074c

.field public static final ic_group_expand_06:I = 0x7f08074d

.field public static final ic_group_expand_07:I = 0x7f08074e

.field public static final ic_group_expand_08:I = 0x7f08074f

.field public static final ic_group_expand_09:I = 0x7f080750

.field public static final ic_group_expand_10:I = 0x7f080751

.field public static final ic_group_expand_11:I = 0x7f080752

.field public static final ic_group_expand_12:I = 0x7f080753

.field public static final ic_group_expand_13:I = 0x7f080754

.field public static final ic_group_expand_14:I = 0x7f080755

.field public static final ic_group_expand_15:I = 0x7f080756

.field public static final ic_guarantee:I = 0x7f080757

.field public static final ic_h_mobiledata:I = 0x7f080758

.field public static final ic_h_plus_mobiledata:I = 0x7f080759

.field public static final ic_hand_gesture:I = 0x7f08075a

.field public static final ic_haptic:I = 0x7f08075b

.field public static final ic_haptic_feedback:I = 0x7f08075c

.field public static final ic_haptic_interesting:I = 0x7f08075d

.field public static final ic_haptic_limit:I = 0x7f08075e

.field public static final ic_haptic_video_bg_01:I = 0x7f08075f

.field public static final ic_haptic_video_bg_02:I = 0x7f080760

.field public static final ic_haptic_video_bg_03:I = 0x7f080761

.field public static final ic_haptic_video_bg_04:I = 0x7f080762

.field public static final ic_haptic_video_bg_05:I = 0x7f080763

.field public static final ic_haptic_video_bg_06:I = 0x7f080764

.field public static final ic_haptic_video_bg_07:I = 0x7f080765

.field public static final ic_haptic_video_bg_08:I = 0x7f080766

.field public static final ic_haptic_video_bg_09:I = 0x7f080767

.field public static final ic_haptic_video_bg_10:I = 0x7f080768

.field public static final ic_haptic_video_bg_11:I = 0x7f080769

.field public static final ic_haptic_video_bg_12:I = 0x7f08076a

.field public static final ic_haptic_video_bg_13:I = 0x7f08076b

.field public static final ic_haptic_video_bg_14:I = 0x7f08076c

.field public static final ic_haptic_video_bg_15:I = 0x7f08076d

.field public static final ic_haptic_video_bg_16:I = 0x7f08076e

.field public static final ic_hdmi_3d:I = 0x7f08076f

.field public static final ic_head:I = 0x7f080770

.field public static final ic_headphone:I = 0x7f080771

.field public static final ic_hearing:I = 0x7f080772

.field public static final ic_hearing_aid:I = 0x7f080773

.field public static final ic_help:I = 0x7f080774

.field public static final ic_help_actionbar:I = 0x7f080775

.field public static final ic_help_outline_32:I = 0x7f080776

.field public static final ic_history:I = 0x7f080777

.field public static final ic_homepage_about:I = 0x7f080778

.field public static final ic_homepage_accessibility:I = 0x7f080779

.field public static final ic_homepage_accounts:I = 0x7f08077a

.field public static final ic_homepage_apps:I = 0x7f08077b

.field public static final ic_homepage_battery:I = 0x7f08077c

.field public static final ic_homepage_connected_device:I = 0x7f08077d

.field public static final ic_homepage_data_usage:I = 0x7f08077e

.field public static final ic_homepage_display:I = 0x7f08077f

.field public static final ic_homepage_emergency:I = 0x7f080780

.field public static final ic_homepage_location:I = 0x7f080781

.field public static final ic_homepage_network:I = 0x7f080782

.field public static final ic_homepage_night_display:I = 0x7f080783

.field public static final ic_homepage_notification:I = 0x7f080784

.field public static final ic_homepage_privacy:I = 0x7f080785

.field public static final ic_homepage_search:I = 0x7f080786

.field public static final ic_homepage_security:I = 0x7f080787

.field public static final ic_homepage_sound:I = 0x7f080788

.field public static final ic_homepage_storage:I = 0x7f080789

.field public static final ic_homepage_support:I = 0x7f08078a

.field public static final ic_homepage_system_dashboard:I = 0x7f08078b

.field public static final ic_homepage_vpn:I = 0x7f08078c

.field public static final ic_homepage_wallpaper:I = 0x7f08078d

.field public static final ic_homepage_wifi_tethering:I = 0x7f08078e

.field public static final ic_hotspot:I = 0x7f08078f

.field public static final ic_illustration_adaptive_connectivity:I = 0x7f080790

.field public static final ic_illustration_fullscreen:I = 0x7f080791

.field public static final ic_illustration_switch:I = 0x7f080792

.field public static final ic_illustration_window:I = 0x7f080793

.field public static final ic_important_outline:I = 0x7f080794

.field public static final ic_infinity_display_settings:I = 0x7f080795

.field public static final ic_info:I = 0x7f080796

.field public static final ic_info_details:I = 0x7f080797

.field public static final ic_info_outline_24:I = 0x7f080798

.field public static final ic_info_outline_24dp:I = 0x7f080799

.field public static final ic_info_selector:I = 0x7f08079a

.field public static final ic_instant_app_badge:I = 0x7f08079b

.field public static final ic_jeejen_launcher:I = 0x7f08079c

.field public static final ic_key_settings:I = 0x7f08079d

.field public static final ic_keyboard_arrow_down:I = 0x7f08079e

.field public static final ic_keyboard_black_24dp:I = 0x7f08079f

.field public static final ic_lab_gesture:I = 0x7f0807a0

.field public static final ic_label:I = 0x7f0807a1

.field public static final ic_language_settings:I = 0x7f0807a2

.field public static final ic_launcher:I = 0x7f0807a3

.field public static final ic_launcher_anti_spam:I = 0x7f0807a4

.field public static final ic_launcher_background:I = 0x7f0807a5

.field public static final ic_launcher_license_manage:I = 0x7f0807a6

.field public static final ic_launcher_network_assistant:I = 0x7f0807a7

.field public static final ic_launcher_power_optimize:I = 0x7f0807a8

.field public static final ic_launcher_quick_clean:I = 0x7f0807a9

.field public static final ic_launcher_rubbish_clean:I = 0x7f0807aa

.field public static final ic_launcher_settings:I = 0x7f0807ab

.field public static final ic_launcher_virus_scan:I = 0x7f0807ac

.field public static final ic_led_settings:I = 0x7f0807ad

.field public static final ic_lightbulb_outline_24:I = 0x7f0807ae

.field public static final ic_link_24dp:I = 0x7f0807af

.field public static final ic_list_sync_anim:I = 0x7f0807b0

.field public static final ic_live_caption:I = 0x7f0807b1

.field public static final ic_live_help:I = 0x7f0807b2

.field public static final ic_local_movies:I = 0x7f0807b3

.field public static final ic_local_phone_24_lib:I = 0x7f0807b4

.field public static final ic_location_info_settings:I = 0x7f0807b5

.field public static final ic_location_settings:I = 0x7f0807b6

.field public static final ic_lock:I = 0x7f0807b7

.field public static final ic_lock_closed:I = 0x7f0807b8

.field public static final ic_lock_list_icon:I = 0x7f0807b9

.field public static final ic_lock_none:I = 0x7f0807ba

.field public static final ic_lock_pin:I = 0x7f0807bb

.field public static final ic_lock_screen_notification:I = 0x7f0807bc

.field public static final ic_lock_swipe:I = 0x7f0807bd

.field public static final ic_lockscreen_ime:I = 0x7f0807be

.field public static final ic_lte_mobiledata:I = 0x7f0807bf

.field public static final ic_lte_plus_mobiledata:I = 0x7f0807c0

.field public static final ic_m3_chip_check:I = 0x7f0807c1

.field public static final ic_m3_chip_checked_circle:I = 0x7f0807c2

.field public static final ic_m3_chip_close:I = 0x7f0807c3

.field public static final ic_main_video_play:I = 0x7f0807c4

.field public static final ic_main_video_play_bg:I = 0x7f0807c5

.field public static final ic_main_video_stop:I = 0x7f0807c6

.field public static final ic_mall_card_arrow_right:I = 0x7f0807c7

.field public static final ic_media_display_device:I = 0x7f0807c8

.field public static final ic_media_group_device:I = 0x7f0807c9

.field public static final ic_media_pause_dark:I = 0x7f0807ca

.field public static final ic_media_pause_light:I = 0x7f0807cb

.field public static final ic_media_play_dark:I = 0x7f0807cc

.field public static final ic_media_play_light:I = 0x7f0807cd

.field public static final ic_media_speaker_device:I = 0x7f0807ce

.field public static final ic_media_stop_dark:I = 0x7f0807cf

.field public static final ic_media_stop_light:I = 0x7f0807d0

.field public static final ic_media_stream:I = 0x7f0807d1

.field public static final ic_media_stream_off:I = 0x7f0807d2

.field public static final ic_menu_3d_globe:I = 0x7f0807d3

.field public static final ic_menu_add:I = 0x7f0807d4

.field public static final ic_menu_add_activated_tint:I = 0x7f0807d5

.field public static final ic_menu_delete:I = 0x7f0807d6

.field public static final ic_menu_delete_holo_dark:I = 0x7f0807d7

.field public static final ic_menu_refresh_holo_dark:I = 0x7f0807d8

.field public static final ic_menu_scan_network:I = 0x7f0807d9

.field public static final ic_mi_service:I = 0x7f0807da

.field public static final ic_mi_upi_settings:I = 0x7f0807db

.field public static final ic_mimoney_settings:I = 0x7f0807dc

.field public static final ic_mipay_account_indonesia:I = 0x7f0807dd

.field public static final ic_miracast_wifi:I = 0x7f0807de

.field public static final ic_miui_home_settings:I = 0x7f0807df

.field public static final ic_miui_lab_settings:I = 0x7f0807e0

.field public static final ic_mobile:I = 0x7f0807e1

.field public static final ic_mobile_call_strength_0:I = 0x7f0807e2

.field public static final ic_mobile_call_strength_1:I = 0x7f0807e3

.field public static final ic_mobile_call_strength_2:I = 0x7f0807e4

.field public static final ic_mobile_call_strength_3:I = 0x7f0807e5

.field public static final ic_mobile_call_strength_4:I = 0x7f0807e6

.field public static final ic_mobile_network_settings:I = 0x7f0807e7

.field public static final ic_mode_edit:I = 0x7f0807e8

.field public static final ic_more_applications_settings:I = 0x7f0807e9

.field public static final ic_mr_button_connected_00_dark:I = 0x7f0807ea

.field public static final ic_mr_button_connected_00_light:I = 0x7f0807eb

.field public static final ic_mr_button_connected_01_dark:I = 0x7f0807ec

.field public static final ic_mr_button_connected_01_light:I = 0x7f0807ed

.field public static final ic_mr_button_connected_02_dark:I = 0x7f0807ee

.field public static final ic_mr_button_connected_02_light:I = 0x7f0807ef

.field public static final ic_mr_button_connected_03_dark:I = 0x7f0807f0

.field public static final ic_mr_button_connected_03_light:I = 0x7f0807f1

.field public static final ic_mr_button_connected_04_dark:I = 0x7f0807f2

.field public static final ic_mr_button_connected_04_light:I = 0x7f0807f3

.field public static final ic_mr_button_connected_05_dark:I = 0x7f0807f4

.field public static final ic_mr_button_connected_05_light:I = 0x7f0807f5

.field public static final ic_mr_button_connected_06_dark:I = 0x7f0807f6

.field public static final ic_mr_button_connected_06_light:I = 0x7f0807f7

.field public static final ic_mr_button_connected_07_dark:I = 0x7f0807f8

.field public static final ic_mr_button_connected_07_light:I = 0x7f0807f9

.field public static final ic_mr_button_connected_08_dark:I = 0x7f0807fa

.field public static final ic_mr_button_connected_08_light:I = 0x7f0807fb

.field public static final ic_mr_button_connected_09_dark:I = 0x7f0807fc

.field public static final ic_mr_button_connected_09_light:I = 0x7f0807fd

.field public static final ic_mr_button_connected_10_dark:I = 0x7f0807fe

.field public static final ic_mr_button_connected_10_light:I = 0x7f0807ff

.field public static final ic_mr_button_connected_11_dark:I = 0x7f080800

.field public static final ic_mr_button_connected_11_light:I = 0x7f080801

.field public static final ic_mr_button_connected_12_dark:I = 0x7f080802

.field public static final ic_mr_button_connected_12_light:I = 0x7f080803

.field public static final ic_mr_button_connected_13_dark:I = 0x7f080804

.field public static final ic_mr_button_connected_13_light:I = 0x7f080805

.field public static final ic_mr_button_connected_14_dark:I = 0x7f080806

.field public static final ic_mr_button_connected_14_light:I = 0x7f080807

.field public static final ic_mr_button_connected_15_dark:I = 0x7f080808

.field public static final ic_mr_button_connected_15_light:I = 0x7f080809

.field public static final ic_mr_button_connected_16_dark:I = 0x7f08080a

.field public static final ic_mr_button_connected_16_light:I = 0x7f08080b

.field public static final ic_mr_button_connected_17_dark:I = 0x7f08080c

.field public static final ic_mr_button_connected_17_light:I = 0x7f08080d

.field public static final ic_mr_button_connected_18_dark:I = 0x7f08080e

.field public static final ic_mr_button_connected_18_light:I = 0x7f08080f

.field public static final ic_mr_button_connected_19_dark:I = 0x7f080810

.field public static final ic_mr_button_connected_19_light:I = 0x7f080811

.field public static final ic_mr_button_connected_20_dark:I = 0x7f080812

.field public static final ic_mr_button_connected_20_light:I = 0x7f080813

.field public static final ic_mr_button_connected_21_dark:I = 0x7f080814

.field public static final ic_mr_button_connected_21_light:I = 0x7f080815

.field public static final ic_mr_button_connected_22_dark:I = 0x7f080816

.field public static final ic_mr_button_connected_22_light:I = 0x7f080817

.field public static final ic_mr_button_connected_23_dark:I = 0x7f080818

.field public static final ic_mr_button_connected_23_light:I = 0x7f080819

.field public static final ic_mr_button_connected_24_dark:I = 0x7f08081a

.field public static final ic_mr_button_connected_24_light:I = 0x7f08081b

.field public static final ic_mr_button_connected_25_dark:I = 0x7f08081c

.field public static final ic_mr_button_connected_25_light:I = 0x7f08081d

.field public static final ic_mr_button_connected_26_dark:I = 0x7f08081e

.field public static final ic_mr_button_connected_26_light:I = 0x7f08081f

.field public static final ic_mr_button_connected_27_dark:I = 0x7f080820

.field public static final ic_mr_button_connected_27_light:I = 0x7f080821

.field public static final ic_mr_button_connected_28_dark:I = 0x7f080822

.field public static final ic_mr_button_connected_28_light:I = 0x7f080823

.field public static final ic_mr_button_connected_29_dark:I = 0x7f080824

.field public static final ic_mr_button_connected_29_light:I = 0x7f080825

.field public static final ic_mr_button_connected_30_dark:I = 0x7f080826

.field public static final ic_mr_button_connected_30_light:I = 0x7f080827

.field public static final ic_mr_button_connecting_00_dark:I = 0x7f080828

.field public static final ic_mr_button_connecting_00_light:I = 0x7f080829

.field public static final ic_mr_button_connecting_01_dark:I = 0x7f08082a

.field public static final ic_mr_button_connecting_01_light:I = 0x7f08082b

.field public static final ic_mr_button_connecting_02_dark:I = 0x7f08082c

.field public static final ic_mr_button_connecting_02_light:I = 0x7f08082d

.field public static final ic_mr_button_connecting_03_dark:I = 0x7f08082e

.field public static final ic_mr_button_connecting_03_light:I = 0x7f08082f

.field public static final ic_mr_button_connecting_04_dark:I = 0x7f080830

.field public static final ic_mr_button_connecting_04_light:I = 0x7f080831

.field public static final ic_mr_button_connecting_05_dark:I = 0x7f080832

.field public static final ic_mr_button_connecting_05_light:I = 0x7f080833

.field public static final ic_mr_button_connecting_06_dark:I = 0x7f080834

.field public static final ic_mr_button_connecting_06_light:I = 0x7f080835

.field public static final ic_mr_button_connecting_07_dark:I = 0x7f080836

.field public static final ic_mr_button_connecting_07_light:I = 0x7f080837

.field public static final ic_mr_button_connecting_08_dark:I = 0x7f080838

.field public static final ic_mr_button_connecting_08_light:I = 0x7f080839

.field public static final ic_mr_button_connecting_09_dark:I = 0x7f08083a

.field public static final ic_mr_button_connecting_09_light:I = 0x7f08083b

.field public static final ic_mr_button_connecting_10_dark:I = 0x7f08083c

.field public static final ic_mr_button_connecting_10_light:I = 0x7f08083d

.field public static final ic_mr_button_connecting_11_dark:I = 0x7f08083e

.field public static final ic_mr_button_connecting_11_light:I = 0x7f08083f

.field public static final ic_mr_button_connecting_12_dark:I = 0x7f080840

.field public static final ic_mr_button_connecting_12_light:I = 0x7f080841

.field public static final ic_mr_button_connecting_13_dark:I = 0x7f080842

.field public static final ic_mr_button_connecting_13_light:I = 0x7f080843

.field public static final ic_mr_button_connecting_14_dark:I = 0x7f080844

.field public static final ic_mr_button_connecting_14_light:I = 0x7f080845

.field public static final ic_mr_button_connecting_15_dark:I = 0x7f080846

.field public static final ic_mr_button_connecting_15_light:I = 0x7f080847

.field public static final ic_mr_button_connecting_16_dark:I = 0x7f080848

.field public static final ic_mr_button_connecting_16_light:I = 0x7f080849

.field public static final ic_mr_button_connecting_17_dark:I = 0x7f08084a

.field public static final ic_mr_button_connecting_17_light:I = 0x7f08084b

.field public static final ic_mr_button_connecting_18_dark:I = 0x7f08084c

.field public static final ic_mr_button_connecting_18_light:I = 0x7f08084d

.field public static final ic_mr_button_connecting_19_dark:I = 0x7f08084e

.field public static final ic_mr_button_connecting_19_light:I = 0x7f08084f

.field public static final ic_mr_button_connecting_20_dark:I = 0x7f080850

.field public static final ic_mr_button_connecting_20_light:I = 0x7f080851

.field public static final ic_mr_button_connecting_21_dark:I = 0x7f080852

.field public static final ic_mr_button_connecting_21_light:I = 0x7f080853

.field public static final ic_mr_button_connecting_22_dark:I = 0x7f080854

.field public static final ic_mr_button_connecting_22_light:I = 0x7f080855

.field public static final ic_mr_button_connecting_23_dark:I = 0x7f080856

.field public static final ic_mr_button_connecting_23_light:I = 0x7f080857

.field public static final ic_mr_button_connecting_24_dark:I = 0x7f080858

.field public static final ic_mr_button_connecting_24_light:I = 0x7f080859

.field public static final ic_mr_button_connecting_25_dark:I = 0x7f08085a

.field public static final ic_mr_button_connecting_25_light:I = 0x7f08085b

.field public static final ic_mr_button_connecting_26_dark:I = 0x7f08085c

.field public static final ic_mr_button_connecting_26_light:I = 0x7f08085d

.field public static final ic_mr_button_connecting_27_dark:I = 0x7f08085e

.field public static final ic_mr_button_connecting_27_light:I = 0x7f08085f

.field public static final ic_mr_button_connecting_28_dark:I = 0x7f080860

.field public static final ic_mr_button_connecting_28_light:I = 0x7f080861

.field public static final ic_mr_button_connecting_29_dark:I = 0x7f080862

.field public static final ic_mr_button_connecting_29_light:I = 0x7f080863

.field public static final ic_mr_button_connecting_30_dark:I = 0x7f080864

.field public static final ic_mr_button_connecting_30_light:I = 0x7f080865

.field public static final ic_mr_button_disabled_dark:I = 0x7f080866

.field public static final ic_mr_button_disabled_light:I = 0x7f080867

.field public static final ic_mr_button_disconnected_dark:I = 0x7f080868

.field public static final ic_mr_button_disconnected_light:I = 0x7f080869

.field public static final ic_mr_button_grey:I = 0x7f08086a

.field public static final ic_mtrl_checked_circle:I = 0x7f08086b

.field public static final ic_mtrl_chip_checked_black:I = 0x7f08086c

.field public static final ic_mtrl_chip_checked_circle:I = 0x7f08086d

.field public static final ic_mtrl_chip_close_circle:I = 0x7f08086e

.field public static final ic_multiple_users:I = 0x7f08086f

.field public static final ic_my_device:I = 0x7f080870

.field public static final ic_network_cell:I = 0x7f080871

.field public static final ic_network_ip_icon:I = 0x7f080872

.field public static final ic_network_security_icon:I = 0x7f080873

.field public static final ic_network_signal_0:I = 0x7f080874

.field public static final ic_network_signal_1:I = 0x7f080875

.field public static final ic_network_signal_2:I = 0x7f080876

.field public static final ic_network_signal_3:I = 0x7f080877

.field public static final ic_network_signal_4:I = 0x7f080878

.field public static final ic_network_signal_blue:I = 0x7f080879

.field public static final ic_nfc:I = 0x7f08087a

.field public static final ic_nine_spinner:I = 0x7f08087b

.field public static final ic_nine_spinner_select:I = 0x7f08087c

.field public static final ic_no_internet_available:I = 0x7f08087d

.field public static final ic_no_internet_unavailable:I = 0x7f08087e

.field public static final ic_no_internet_wifi_signal_0:I = 0x7f08087f

.field public static final ic_no_internet_wifi_signal_1:I = 0x7f080880

.field public static final ic_no_internet_wifi_signal_2:I = 0x7f080881

.field public static final ic_no_internet_wifi_signal_3:I = 0x7f080882

.field public static final ic_no_internet_wifi_signal_4:I = 0x7f080883

.field public static final ic_notification_alert:I = 0x7f080884

.field public static final ic_notification_badge:I = 0x7f080885

.field public static final ic_notification_block:I = 0x7f080886

.field public static final ic_notification_center:I = 0x7f080887

.field public static final ic_notification_dot:I = 0x7f080888

.field public static final ic_notification_min:I = 0x7f080889

.field public static final ic_notification_peek:I = 0x7f08088a

.field public static final ic_notification_ring:I = 0x7f08088b

.field public static final ic_notification_silence:I = 0x7f08088c

.field public static final ic_notification_vis_override:I = 0x7f08088d

.field public static final ic_notifications:I = 0x7f08088e

.field public static final ic_notifications_alert:I = 0x7f08088f

.field public static final ic_notifications_off_24dp:I = 0x7f080890

.field public static final ic_notifications_white:I = 0x7f080891

.field public static final ic_oldman_mode_settings:I = 0x7f080892

.field public static final ic_ongoing_notification:I = 0x7f080893

.field public static final ic_open_wifi_autoconnect:I = 0x7f080894

.field public static final ic_open_wifi_notifications:I = 0x7f080895

.field public static final ic_ota_update_available:I = 0x7f080896

.field public static final ic_ota_update_current:I = 0x7f080897

.field public static final ic_ota_update_none:I = 0x7f080898

.field public static final ic_ota_update_stale:I = 0x7f080899

.field public static final ic_other_advanced_settings:I = 0x7f08089a

.field public static final ic_package_verifier_disabled:I = 0x7f08089b

.field public static final ic_package_verifier_enabled:I = 0x7f08089c

.field public static final ic_package_verifier_removed:I = 0x7f08089d

.field public static final ic_page_layout_settings:I = 0x7f08089e

.field public static final ic_pan_tool_18dp:I = 0x7f08089f

.field public static final ic_paper_mode:I = 0x7f0808a0

.field public static final ic_partial_system_update_current:I = 0x7f0808a1

.field public static final ic_partial_system_update_stale:I = 0x7f0808a2

.field public static final ic_password:I = 0x7f0808a3

.field public static final ic_password_unlock:I = 0x7f0808a4

.field public static final ic_password_unlock_checked:I = 0x7f0808a5

.field public static final ic_pattern:I = 0x7f0808a6

.field public static final ic_performance_notification:I = 0x7f0808a7

.field public static final ic_perm_device_information_green_24dp:I = 0x7f0808a8

.field public static final ic_perm_device_information_red_24dp:I = 0x7f0808a9

.field public static final ic_person:I = 0x7f0808aa

.field public static final ic_phone:I = 0x7f0808ab

.field public static final ic_phone_info:I = 0x7f0808ac

.field public static final ic_phone_ring:I = 0x7f0808ad

.field public static final ic_photo_library:I = 0x7f0808ae

.field public static final ic_pin:I = 0x7f0808af

.field public static final ic_portable_wlan_hotspot_settings:I = 0x7f0808b0

.field public static final ic_power_mode:I = 0x7f0808b1

.field public static final ic_power_mode_settings:I = 0x7f0808b2

.field public static final ic_power_system:I = 0x7f0808b3

.field public static final ic_preference_location:I = 0x7f0808b4

.field public static final ic_printer_search:I = 0x7f0808b5

.field public static final ic_privacy_protection:I = 0x7f0808b6

.field public static final ic_privacy_settings:I = 0x7f0808b7

.field public static final ic_privacy_shield_24dp:I = 0x7f0808b8

.field public static final ic_promote_conversation:I = 0x7f0808b9

.field public static final ic_provision_arrow_back_normal:I = 0x7f0808ba

.field public static final ic_provision_arrow_back_pressed:I = 0x7f0808bb

.field public static final ic_provision_arrow_next_disabled:I = 0x7f0808bc

.field public static final ic_provision_arrow_next_normal:I = 0x7f0808bd

.field public static final ic_provision_arrow_next_pressed:I = 0x7f0808be

.field public static final ic_qr_code_scanner:I = 0x7f0808bf

.field public static final ic_qrcode_24dp:I = 0x7f0808c0

.field public static final ic_qrcode_32dp:I = 0x7f0808c1

.field public static final ic_quiet_mode_settings:I = 0x7f0808c2

.field public static final ic_radio_button_checked_black_24dp:I = 0x7f0808c3

.field public static final ic_radio_button_unchecked_black_24dp:I = 0x7f0808c4

.field public static final ic_rear_display:I = 0x7f0808c5

.field public static final ic_redo_24:I = 0x7f0808c6

.field public static final ic_reduce_bright_colors:I = 0x7f0808c7

.field public static final ic_remove_24dp:I = 0x7f0808c8

.field public static final ic_remove_circle:I = 0x7f0808c9

.field public static final ic_repair_24dp:I = 0x7f0808ca

.field public static final ic_resolver_settings:I = 0x7f0808cb

.field public static final ic_restore:I = 0x7f0808cc

.field public static final ic_ringer_volume_settings:I = 0x7f0808cd

.field public static final ic_ringtone_notification:I = 0x7f0808ce

.field public static final ic_ringtone_settings:I = 0x7f0808cf

.field public static final ic_router_icon:I = 0x7f0808d0

.field public static final ic_rtt_settings:I = 0x7f0808d1

.field public static final ic_scan_24dp:I = 0x7f0808d2

.field public static final ic_scan_32dp:I = 0x7f0808d3

.field public static final ic_screen_timeout_settings:I = 0x7f0808d4

.field public static final ic_screen_zoom:I = 0x7f0808d5

.field public static final ic_sd_card:I = 0x7f0808d6

.field public static final ic_search_24dp:I = 0x7f0808d7

.field public static final ic_second_space:I = 0x7f0808d8

.field public static final ic_security_center_settings:I = 0x7f0808d9

.field public static final ic_security_lock_24dp:I = 0x7f0808da

.field public static final ic_security_settings_settings:I = 0x7f0808db

.field public static final ic_security_status:I = 0x7f0808dc

.field public static final ic_setting:I = 0x7f0808dd

.field public static final ic_settings_24dp:I = 0x7f0808de

.field public static final ic_settings_about:I = 0x7f0808df

.field public static final ic_settings_accent:I = 0x7f0808e0

.field public static final ic_settings_accessibility:I = 0x7f0808e1

.field public static final ic_settings_accounts:I = 0x7f0808e2

.field public static final ic_settings_adaptive_sleep:I = 0x7f0808e3

.field public static final ic_settings_all:I = 0x7f0808e4

.field public static final ic_settings_aod:I = 0x7f0808e5

.field public static final ic_settings_applications:I = 0x7f0808e6

.field public static final ic_settings_backup:I = 0x7f0808e7

.field public static final ic_settings_backup_restore:I = 0x7f0808e8

.field public static final ic_settings_battery:I = 0x7f0808e9

.field public static final ic_settings_battery_white:I = 0x7f0808ea

.field public static final ic_settings_camera:I = 0x7f0808eb

.field public static final ic_settings_cell_standby:I = 0x7f0808ec

.field public static final ic_settings_close:I = 0x7f0808ed

.field public static final ic_settings_data_usage:I = 0x7f0808ee

.field public static final ic_settings_date_time:I = 0x7f0808ef

.field public static final ic_settings_delete:I = 0x7f0808f0

.field public static final ic_settings_development:I = 0x7f0808f1

.field public static final ic_settings_development_alpha:I = 0x7f0808f2

.field public static final ic_settings_disable:I = 0x7f0808f3

.field public static final ic_settings_display:I = 0x7f0808f4

.field public static final ic_settings_display_white:I = 0x7f0808f5

.field public static final ic_settings_dualsim:I = 0x7f0808f6

.field public static final ic_settings_emergency:I = 0x7f0808f7

.field public static final ic_settings_enable:I = 0x7f0808f8

.field public static final ic_settings_ethernet:I = 0x7f0808f9

.field public static final ic_settings_expand_less:I = 0x7f0808fa

.field public static final ic_settings_expand_more:I = 0x7f0808fb

.field public static final ic_settings_force_stop:I = 0x7f0808fc

.field public static final ic_settings_gestures:I = 0x7f0808fd

.field public static final ic_settings_home:I = 0x7f0808fe

.field public static final ic_settings_install:I = 0x7f0808ff

.field public static final ic_settings_language:I = 0x7f080900

.field public static final ic_settings_location:I = 0x7f080901

.field public static final ic_settings_memory:I = 0x7f080902

.field public static final ic_settings_multiuser:I = 0x7f080903

.field public static final ic_settings_nfc_payment:I = 0x7f080904

.field public static final ic_settings_nfc_payment_am_alpha:I = 0x7f080905

.field public static final ic_settings_night_display:I = 0x7f080906

.field public static final ic_settings_night_display_white:I = 0x7f080907

.field public static final ic_settings_open:I = 0x7f080908

.field public static final ic_settings_personal:I = 0x7f080909

.field public static final ic_settings_phone_idle:I = 0x7f08090a

.field public static final ic_settings_print:I = 0x7f08090b

.field public static final ic_settings_privacy:I = 0x7f08090c

.field public static final ic_settings_safety_center:I = 0x7f08090d

.field public static final ic_settings_safety_emergency:I = 0x7f08090e

.field public static final ic_settings_security:I = 0x7f08090f

.field public static final ic_settings_security_white:I = 0x7f080910

.field public static final ic_settings_sign_in:I = 0x7f080911

.field public static final ic_settings_sim:I = 0x7f080912

.field public static final ic_settings_sound:I = 0x7f080913

.field public static final ic_settings_sync:I = 0x7f080914

.field public static final ic_settings_sync_disabled:I = 0x7f080915

.field public static final ic_settings_sync_failed:I = 0x7f080916

.field public static final ic_settings_system_dashboard_white:I = 0x7f080917

.field public static final ic_settings_voice_calls:I = 0x7f080918

.field public static final ic_settings_wallpaper_white:I = 0x7f080919

.field public static final ic_settings_widget_background:I = 0x7f08091a

.field public static final ic_settings_wireless:I = 0x7f08091b

.field public static final ic_settings_wireless_white:I = 0x7f08091c

.field public static final ic_setup_hint:I = 0x7f08091d

.field public static final ic_setup_hint_blue:I = 0x7f08091e

.field public static final ic_setup_hint_red:I = 0x7f08091f

.field public static final ic_setup_hint_yellow:I = 0x7f080920

.field public static final ic_shortcut_battery:I = 0x7f080921

.field public static final ic_shortcut_data_usage:I = 0x7f080922

.field public static final ic_shortcut_display:I = 0x7f080923

.field public static final ic_shortcut_sound:I = 0x7f080924

.field public static final ic_shortcut_wifi:I = 0x7f080925

.field public static final ic_shortcut_wireless:I = 0x7f080926

.field public static final ic_show_x_wifi_signal_0:I = 0x7f080927

.field public static final ic_show_x_wifi_signal_1:I = 0x7f080928

.field public static final ic_show_x_wifi_signal_2:I = 0x7f080929

.field public static final ic_show_x_wifi_signal_3:I = 0x7f08092a

.field public static final ic_show_x_wifi_signal_4:I = 0x7f08092b

.field public static final ic_signal_flashlight:I = 0x7f08092c

.field public static final ic_signal_strength_4g:I = 0x7f08092d

.field public static final ic_signal_strength_zero_bar_no_internet:I = 0x7f08092e

.field public static final ic_signal_workmode_enable:I = 0x7f08092f

.field public static final ic_sim_alert:I = 0x7f080930

.field public static final ic_sim_card:I = 0x7f080931

.field public static final ic_sim_card_alert_white_48dp:I = 0x7f080932

.field public static final ic_sim_sd:I = 0x7f080933

.field public static final ic_skip:I = 0x7f080934

.field public static final ic_smartphone:I = 0x7f080935

.field public static final ic_sms:I = 0x7f080936

.field public static final ic_sms_received_settings:I = 0x7f080937

.field public static final ic_snooze:I = 0x7f080938

.field public static final ic_sos_call_icon:I = 0x7f080939

.field public static final ic_sos_phone_icon:I = 0x7f08093a

.field public static final ic_sound:I = 0x7f08093b

.field public static final ic_sound_settings:I = 0x7f08093c

.field public static final ic_speaker_group_black_24dp:I = 0x7f08093d

.field public static final ic_storage:I = 0x7f08093e

.field public static final ic_storage_apps:I = 0x7f08093f

.field public static final ic_storage_grey:I = 0x7f080940

.field public static final ic_storage_settings:I = 0x7f080941

.field public static final ic_storage_white:I = 0x7f080942

.field public static final ic_storage_wizard_external:I = 0x7f080943

.field public static final ic_storage_wizard_internal:I = 0x7f080944

.field public static final ic_stylus_and_keyboard_settings:I = 0x7f080945

.field public static final ic_stylus_in_charging:I = 0x7f080946

.field public static final ic_stylus_settings:I = 0x7f080947

.field public static final ic_subnet_mask_icon:I = 0x7f080948

.field public static final ic_suggested_notifications:I = 0x7f080949

.field public static final ic_suggestion_close_button:I = 0x7f08094a

.field public static final ic_suggestion_dnd:I = 0x7f08094b

.field public static final ic_suggestion_fingerprint:I = 0x7f08094c

.field public static final ic_suggestion_night_display:I = 0x7f08094d

.field public static final ic_suggestion_security:I = 0x7f08094e

.field public static final ic_suggestion_wireless:I = 0x7f08094f

.field public static final ic_support_24dp:I = 0x7f080950

.field public static final ic_swap:I = 0x7f080951

.field public static final ic_swap_horiz:I = 0x7f080952

.field public static final ic_swap_horiz_blue:I = 0x7f080953

.field public static final ic_swap_horiz_grey:I = 0x7f080954

.field public static final ic_sync:I = 0x7f080955

.field public static final ic_sync_anim_holo:I = 0x7f080956

.field public static final ic_sync_error:I = 0x7f080957

.field public static final ic_sync_error_holo:I = 0x7f080958

.field public static final ic_sync_green:I = 0x7f080959

.field public static final ic_sync_grey:I = 0x7f08095a

.field public static final ic_sync_grey_holo:I = 0x7f08095b

.field public static final ic_sync_problem_24dp:I = 0x7f08095c

.field public static final ic_sync_red:I = 0x7f08095d

.field public static final ic_sync_red_holo:I = 0x7f08095e

.field public static final ic_sync_settings:I = 0x7f08095f

.field public static final ic_sysbar_quicksettings:I = 0x7f080960

.field public static final ic_system_app_settings:I = 0x7f080961

.field public static final ic_system_app_settings_new:I = 0x7f080962

.field public static final ic_system_apps_updater:I = 0x7f080963

.field public static final ic_system_controls:I = 0x7f080964

.field public static final ic_system_update:I = 0x7f080965

.field public static final ic_tab_selected_all:I = 0x7f080966

.field public static final ic_tab_selected_download:I = 0x7f080967

.field public static final ic_tab_selected_running:I = 0x7f080968

.field public static final ic_tab_selected_sdcard:I = 0x7f080969

.field public static final ic_tab_unselected_all:I = 0x7f08096a

.field public static final ic_tab_unselected_download:I = 0x7f08096b

.field public static final ic_tab_unselected_running:I = 0x7f08096c

.field public static final ic_tab_unselected_sdcard:I = 0x7f08096d

.field public static final ic_tablet_screen_settings:I = 0x7f08096e

.field public static final ic_tap_assistance:I = 0x7f08096f

.field public static final ic_telephone_ring:I = 0x7f080970

.field public static final ic_tether_settings:I = 0x7f080971

.field public static final ic_text_dot:I = 0x7f080972

.field public static final ic_theme:I = 0x7f080973

.field public static final ic_time:I = 0x7f080974

.field public static final ic_timelapse:I = 0x7f080975

.field public static final ic_timelapse_white:I = 0x7f080976

.field public static final ic_today:I = 0x7f080977

.field public static final ic_translate_24dp:I = 0x7f080978

.field public static final ic_trash_can:I = 0x7f080979

.field public static final ic_unchecked_checkbox:I = 0x7f08097a

.field public static final ic_undo_24:I = 0x7f08097b

.field public static final ic_union:I = 0x7f08097c

.field public static final ic_union_disable:I = 0x7f08097d

.field public static final ic_unlock_password_icon:I = 0x7f08097e

.field public static final ic_unlock_password_icon_selected:I = 0x7f08097f

.field public static final ic_unlock_pattern_icon:I = 0x7f080980

.field public static final ic_unlock_pattern_icon_selected:I = 0x7f080981

.field public static final ic_unlock_pin_icon:I = 0x7f080982

.field public static final ic_unlock_pin_icon_selected:I = 0x7f080983

.field public static final ic_unlock_set_settings:I = 0x7f080984

.field public static final ic_updater_settings:I = 0x7f080985

.field public static final ic_updater_settings_new:I = 0x7f080986

.field public static final ic_upload:I = 0x7f080987

.field public static final ic_usb:I = 0x7f080988

.field public static final ic_usb_MIDI:I = 0x7f080989

.field public static final ic_usb_charging_only:I = 0x7f08098a

.field public static final ic_usb_charging_only_small:I = 0x7f08098b

.field public static final ic_usb_disable:I = 0x7f08098c

.field public static final ic_usb_enable:I = 0x7f08098d

.field public static final ic_usb_midi:I = 0x7f08098e

.field public static final ic_usb_mtp:I = 0x7f08098f

.field public static final ic_usb_mtp_small:I = 0x7f080990

.field public static final ic_usb_ptp:I = 0x7f080991

.field public static final ic_usb_ptp_small:I = 0x7f080992

.field public static final ic_usb_reverse_charging:I = 0x7f080993

.field public static final ic_userguide:I = 0x7f080994

.field public static final ic_vibration:I = 0x7f080995

.field public static final ic_videogame_vd_theme_24:I = 0x7f080996

.field public static final ic_vip_service_settings:I = 0x7f080997

.field public static final ic_visibility_18dp:I = 0x7f080998

.field public static final ic_voice_access:I = 0x7f080999

.field public static final ic_voice_access_normal:I = 0x7f08099a

.field public static final ic_voice_access_selected:I = 0x7f08099b

.field public static final ic_voip_assistant_screen:I = 0x7f08099c

.field public static final ic_vol_mute:I = 0x7f08099d

.field public static final ic_vol_type_speaker_dark:I = 0x7f08099e

.field public static final ic_vol_type_speaker_group_dark:I = 0x7f08099f

.field public static final ic_vol_type_speaker_group_light:I = 0x7f0809a0

.field public static final ic_vol_type_speaker_light:I = 0x7f0809a1

.field public static final ic_vol_type_tv_dark:I = 0x7f0809a2

.field public static final ic_vol_type_tv_light:I = 0x7f0809a3

.field public static final ic_vol_unmute:I = 0x7f0809a4

.field public static final ic_volume_media_bt:I = 0x7f0809a5

.field public static final ic_volume_remote:I = 0x7f0809a6

.field public static final ic_volume_remote_mute:I = 0x7f0809a7

.field public static final ic_volume_ringer_mute:I = 0x7f0809a8

.field public static final ic_volume_ringer_vibrate:I = 0x7f0809a9

.field public static final ic_volume_up_24dp:I = 0x7f0809aa

.field public static final ic_vowifi:I = 0x7f0809ab

.field public static final ic_vowifi_calling:I = 0x7f0809ac

.field public static final ic_vpn:I = 0x7f0809ad

.field public static final ic_vpn_key:I = 0x7f0809ae

.field public static final ic_vpn_settings:I = 0x7f0809af

.field public static final ic_wallpaper:I = 0x7f0809b0

.field public static final ic_wallpaper_settings:I = 0x7f0809b1

.field public static final ic_warning:I = 0x7f0809b2

.field public static final ic_warning_24dp:I = 0x7f0809b3

.field public static final ic_warning_circle_24dp:I = 0x7f0809b4

.field public static final ic_warning_smallicon:I = 0x7f0809b5

.field public static final ic_wechat:I = 0x7f0809b6

.field public static final ic_wifi_4_signal_0:I = 0x7f0809b7

.field public static final ic_wifi_4_signal_1:I = 0x7f0809b8

.field public static final ic_wifi_4_signal_2:I = 0x7f0809b9

.field public static final ic_wifi_4_signal_3:I = 0x7f0809ba

.field public static final ic_wifi_4_signal_4:I = 0x7f0809bb

.field public static final ic_wifi_5_signal_0:I = 0x7f0809bc

.field public static final ic_wifi_5_signal_1:I = 0x7f0809bd

.field public static final ic_wifi_5_signal_2:I = 0x7f0809be

.field public static final ic_wifi_5_signal_3:I = 0x7f0809bf

.field public static final ic_wifi_5_signal_4:I = 0x7f0809c0

.field public static final ic_wifi_6_signal_0:I = 0x7f0809c1

.field public static final ic_wifi_6_signal_1:I = 0x7f0809c2

.field public static final ic_wifi_6_signal_2:I = 0x7f0809c3

.field public static final ic_wifi_6_signal_3:I = 0x7f0809c4

.field public static final ic_wifi_6_signal_4:I = 0x7f0809c5

.field public static final ic_wifi_call_strength_0:I = 0x7f0809c6

.field public static final ic_wifi_call_strength_1:I = 0x7f0809c7

.field public static final ic_wifi_call_strength_2:I = 0x7f0809c8

.field public static final ic_wifi_call_strength_3:I = 0x7f0809c9

.field public static final ic_wifi_call_strength_4:I = 0x7f0809ca

.field public static final ic_wifi_encryption:I = 0x7f0809cb

.field public static final ic_wifi_encryption_connected:I = 0x7f0809cc

.field public static final ic_wifi_generation_icon:I = 0x7f0809cd

.field public static final ic_wifi_lock_signal_1_dark:I = 0x7f0809ce

.field public static final ic_wifi_lock_signal_2_dark:I = 0x7f0809cf

.field public static final ic_wifi_lock_signal_3_dark:I = 0x7f0809d0

.field public static final ic_wifi_lock_signal_4_dark:I = 0x7f0809d1

.field public static final ic_wifi_more_settings:I = 0x7f0809d2

.field public static final ic_wifi_off:I = 0x7f0809d3

.field public static final ic_wifi_privacy_24dp:I = 0x7f0809d4

.field public static final ic_wifi_settings:I = 0x7f0809d5

.field public static final ic_wifi_signal_0:I = 0x7f0809d6

.field public static final ic_wifi_signal_1:I = 0x7f0809d7

.field public static final ic_wifi_signal_1_dark:I = 0x7f0809d8

.field public static final ic_wifi_signal_2:I = 0x7f0809d9

.field public static final ic_wifi_signal_2_dark:I = 0x7f0809da

.field public static final ic_wifi_signal_3:I = 0x7f0809db

.field public static final ic_wifi_signal_3_dark:I = 0x7f0809dc

.field public static final ic_wifi_signal_4:I = 0x7f0809dd

.field public static final ic_wifi_signal_4_32dp:I = 0x7f0809de

.field public static final ic_wifi_signal_4_dark:I = 0x7f0809df

.field public static final ic_wifi_signal_lock:I = 0x7f0809e0

.field public static final ic_wifi_speed_icon:I = 0x7f0809e1

.field public static final ic_wifi_status_icon:I = 0x7f0809e2

.field public static final ic_wifi_strength_icon:I = 0x7f0809e3

.field public static final ic_wifi_tethering:I = 0x7f0809e4

.field public static final ic_work_app_badge:I = 0x7f0809e5

.field public static final ic_xiaoai_off:I = 0x7f0809e6

.field public static final ic_xiaoai_open:I = 0x7f0809e7

.field public static final ic_xspace:I = 0x7f0809e8

.field public static final ic_zoom_bigger:I = 0x7f0809e9

.field public static final ic_zoom_smaller:I = 0x7f0809ea

.field public static final icon_app_lock:I = 0x7f0809eb

.field public static final icon_app_lock_cetus:I = 0x7f0809ec

.field public static final icon_danger_warning:I = 0x7f0809ed

.field public static final icon_edit_normal_light:I = 0x7f0809ee

.field public static final icon_edit_pressed_light:I = 0x7f0809ef

.field public static final icon_global_back:I = 0x7f0809f0

.field public static final icon_global_back_n:I = 0x7f0809f1

.field public static final icon_global_back_p:I = 0x7f0809f2

.field public static final icon_global_next:I = 0x7f0809f3

.field public static final icon_global_next_d:I = 0x7f0809f4

.field public static final icon_global_next_n:I = 0x7f0809f5

.field public static final icon_global_next_p:I = 0x7f0809f6

.field public static final icon_list_empty:I = 0x7f0809f7

.field public static final icon_list_empty2:I = 0x7f0809f8

.field public static final illustration_accessibility_button:I = 0x7f0809f9

.field public static final illustration_accessibility_gesture_three_finger:I = 0x7f0809fa

.field public static final illustration_accessibility_gesture_two_finger:I = 0x7f0809fb

.field public static final illustration_horizontal:I = 0x7f0809fc

.field public static final illustration_tile:I = 0x7f0809fd

.field public static final image_dark_mode:I = 0x7f0809fe

.field public static final image_dark_mode_top:I = 0x7f0809ff

.field public static final image_dark_mode_top_new:I = 0x7f080a00

.field public static final image_dark_top:I = 0x7f080a01

.field public static final image_light_mode:I = 0x7f080a02

.field public static final image_light_mode_top:I = 0x7f080a03

.field public static final image_light_mode_top_new:I = 0x7f080a04

.field public static final image_miui_nfc:I = 0x7f080a05

.field public static final img_main_video_bg:I = 0x7f080a06

.field public static final imitate_real_keyboard:I = 0x7f080a07

.field public static final immersion_window_bg:I = 0x7f080a08

.field public static final input_method_clipboard_function:I = 0x7f080a09

.field public static final input_method_clipboard_function_selected:I = 0x7f080a0a

.field public static final input_method_clipboard_function_selector:I = 0x7f080a0b

.field public static final input_method_clipboard_settings_bg:I = 0x7f080a0c

.field public static final input_method_clipboard_settings_bg_dark:I = 0x7f080a0d

.field public static final input_method_clipboard_settings_bg_light:I = 0x7f080a0e

.field public static final input_method_clipboard_settings_bubble_dark:I = 0x7f080a0f

.field public static final input_method_clipboard_settings_bubble_light:I = 0x7f080a10

.field public static final input_method_clipboard_settings_cloud_bubble:I = 0x7f080a11

.field public static final input_method_clipboard_settings_red_point:I = 0x7f080a12

.field public static final input_method_item_selector:I = 0x7f080a13

.field public static final input_method_keyboard_select_image_bg:I = 0x7f080a14

.field public static final input_method_no_function:I = 0x7f080a15

.field public static final input_method_no_function_selected:I = 0x7f080a16

.field public static final input_method_no_function_selector:I = 0x7f080a17

.field public static final input_method_switch_function:I = 0x7f080a18

.field public static final input_method_switch_function_selected:I = 0x7f080a19

.field public static final input_method_switch_function_selector:I = 0x7f080a1a

.field public static final input_method_switch_keyboard_language:I = 0x7f080a1b

.field public static final input_method_switch_keyboard_language_selected:I = 0x7f080a1c

.field public static final input_method_switch_keyboard_language_selector:I = 0x7f080a1d

.field public static final input_method_switch_keyboard_type:I = 0x7f080a1e

.field public static final input_method_switch_keyboard_type_selected:I = 0x7f080a1f

.field public static final input_method_switch_keyboard_type_selector:I = 0x7f080a20

.field public static final input_method_voice_function:I = 0x7f080a21

.field public static final input_method_voice_function_selected:I = 0x7f080a22

.field public static final input_method_voice_function_selector:I = 0x7f080a23

.field public static final input_method_xiaoai_function:I = 0x7f080a24

.field public static final input_method_xiaoai_function_selected:I = 0x7f080a25

.field public static final input_method_xiaoai_function_selector:I = 0x7f080a26

.field public static final input_selected_background_normal:I = 0x7f080a27

.field public static final issuse_icon:I = 0x7f080a28

.field public static final issuse_icon_dark:I = 0x7f080a29

.field public static final item_drag_move:I = 0x7f080a2a

.field public static final jeejen_btn_bg:I = 0x7f080a2b

.field public static final jeejen_logo:I = 0x7f080a2c

.field public static final keep_screen_on_after_folding:I = 0x7f080a2d

.field public static final key_combination_power_volume_down:I = 0x7f080a2e

.field public static final keyboard_high_dark:I = 0x7f080a2f

.field public static final keyboard_high_light:I = 0x7f080a30

.field public static final keyboard_layout_dialog_list_item_background:I = 0x7f080a31

.field public static final keyboard_normal_dark:I = 0x7f080a32

.field public static final keyboard_normal_light:I = 0x7f080a33

.field public static final keyboard_select:I = 0x7f080a34

.field public static final keyboard_settings:I = 0x7f080a35

.field public static final keyboard_settings_disable:I = 0x7f080a36

.field public static final keyboard_settings_image_high:I = 0x7f080a37

.field public static final keyboard_settings_image_normal:I = 0x7f080a38

.field public static final keyboard_show_icon:I = 0x7f080a39

.field public static final keyguard_choose_password_show_selected:I = 0x7f080a3a

.field public static final keyguard_not_show_password:I = 0x7f080a3b

.field public static final keyguard_setup_btn_text_color_high_light:I = 0x7f080a3c

.field public static final keyguard_setup_btn_text_color_normal:I = 0x7f080a3d

.field public static final keyguard_setup_choose_password_btn_bg:I = 0x7f080a3e

.field public static final keyguard_show_password:I = 0x7f080a3f

.field public static final keysettings_camera:I = 0x7f080a40

.field public static final keysettings_common_no_power:I = 0x7f080a41

.field public static final keysettings_common_power:I = 0x7f080a42

.field public static final keysettings_launch_recents:I = 0x7f080a43

.field public static final keysettings_launcher:I = 0x7f080a44

.field public static final keysettings_mipay:I = 0x7f080a45

.field public static final keysettings_partial_screen_shot:I = 0x7f080a46

.field public static final keysettings_screen_shot:I = 0x7f080a47

.field public static final keysettings_show_menu:I = 0x7f080a48

.field public static final keysettings_split_screen:I = 0x7f080a49

.field public static final keysettings_turn_on_torch:I = 0x7f080a4a

.field public static final keysettings_voice_assistant:I = 0x7f080a4b

.field public static final launcher_settings:I = 0x7f080a4c

.field public static final led_color_preview:I = 0x7f080a4d

.field public static final led_settings:I = 0x7f080a4e

.field public static final left_right_hand_switch:I = 0x7f080a4f

.field public static final left_rounded_ripple:I = 0x7f080a50

.field public static final light_dark_mode_inner:I = 0x7f080a51

.field public static final light_dark_mode_outer:I = 0x7f080a52

.field public static final line:I = 0x7f080a53

.field public static final line_drawable:I = 0x7f080a54

.field public static final list_item_background:I = 0x7f080a55

.field public static final list_item_bg_dialog:I = 0x7f080a56

.field public static final list_item_bg_light:I = 0x7f080a57

.field public static final list_item_bg_normal:I = 0x7f080a58

.field public static final list_item_bg_pressed:I = 0x7f080a59

.field public static final list_view_item_group_header_bg_light:I = 0x7f080a5a

.field public static final lock_anim_0:I = 0x7f080a5b

.field public static final lock_anim_1:I = 0x7f080a5c

.field public static final lock_anim_10:I = 0x7f080a5d

.field public static final lock_anim_11:I = 0x7f080a5e

.field public static final lock_anim_12:I = 0x7f080a5f

.field public static final lock_anim_13:I = 0x7f080a60

.field public static final lock_anim_14:I = 0x7f080a61

.field public static final lock_anim_2:I = 0x7f080a62

.field public static final lock_anim_3:I = 0x7f080a63

.field public static final lock_anim_4:I = 0x7f080a64

.field public static final lock_anim_5:I = 0x7f080a65

.field public static final lock_anim_6:I = 0x7f080a66

.field public static final lock_anim_7:I = 0x7f080a67

.field public static final lock_anim_8:I = 0x7f080a68

.field public static final lock_anim_9:I = 0x7f080a69

.field public static final lock_pattern_background:I = 0x7f080a6a

.field public static final lock_pattern_background_texture:I = 0x7f080a6b

.field public static final lock_pattern_code_lock_error_holo:I = 0x7f080a6c

.field public static final lock_pattern_code_lock_touched_holo:I = 0x7f080a6d

.field public static final lock_pattern_code_lock_white:I = 0x7f080a6e

.field public static final lock_pattern_indicator_code_lock_drag_direction_red_up:I = 0x7f080a6f

.field public static final lock_pattern_indicator_code_lock_point_area_default_holo:I = 0x7f080a70

.field public static final lock_pattern_indicator_code_lock_point_area_green_holo:I = 0x7f080a71

.field public static final lock_pattern_indicator_code_lock_point_area_red_holo:I = 0x7f080a72

.field public static final logo_miui_lab:I = 0x7f080a73

.field public static final long_press_power_key:I = 0x7f080a74

.field public static final longpressbottom01:I = 0x7f080a75

.field public static final longpressbottom02:I = 0x7f080a76

.field public static final longpressbottom03:I = 0x7f080a77

.field public static final longpressbottom04:I = 0x7f080a78

.field public static final longpressbottom05:I = 0x7f080a79

.field public static final longpressbottom06:I = 0x7f080a7a

.field public static final longpressbottom07:I = 0x7f080a7b

.field public static final longpressbottom08:I = 0x7f080a7c

.field public static final longpressbottom09:I = 0x7f080a7d

.field public static final longpressbottom10:I = 0x7f080a7e

.field public static final longpressbottom11:I = 0x7f080a7f

.field public static final m3_appbar_background:I = 0x7f080a80

.field public static final m3_popupmenu_background_overlay:I = 0x7f080a81

.field public static final m3_radiobutton_ripple:I = 0x7f080a82

.field public static final m3_selection_control_ripple:I = 0x7f080a83

.field public static final m3_tabs_background:I = 0x7f080a84

.field public static final m3_tabs_line_indicator:I = 0x7f080a85

.field public static final m3_tabs_rounded_line_indicator:I = 0x7f080a86

.field public static final m3_tabs_transparent_background:I = 0x7f080a87

.field public static final magic_icon_list_empty:I = 0x7f080a88

.field public static final map_wizard:I = 0x7f080a89

.field public static final material_cursor_drawable:I = 0x7f080a8a

.field public static final material_ic_calendar_black_24dp:I = 0x7f080a8b

.field public static final material_ic_clear_black_24dp:I = 0x7f080a8c

.field public static final material_ic_edit_black_24dp:I = 0x7f080a8d

.field public static final material_ic_keyboard_arrow_left_black_24dp:I = 0x7f080a8e

.field public static final material_ic_keyboard_arrow_next_black_24dp:I = 0x7f080a8f

.field public static final material_ic_keyboard_arrow_previous_black_24dp:I = 0x7f080a90

.field public static final material_ic_keyboard_arrow_right_black_24dp:I = 0x7f080a91

.field public static final material_ic_menu_arrow_down_black_24dp:I = 0x7f080a92

.field public static final material_ic_menu_arrow_up_black_24dp:I = 0x7f080a93

.field public static final media_volume_icon:I = 0x7f080a94

.field public static final memory_application:I = 0x7f080a95

.field public static final memory_audio:I = 0x7f080a96

.field public static final memory_bar_application:I = 0x7f080a97

.field public static final memory_bar_audio:I = 0x7f080a98

.field public static final memory_bar_cache:I = 0x7f080a99

.field public static final memory_bar_dcim:I = 0x7f080a9a

.field public static final memory_bar_empty:I = 0x7f080a9b

.field public static final memory_bar_mask:I = 0x7f080a9c

.field public static final memory_bar_other:I = 0x7f080a9d

.field public static final memory_bar_second_space:I = 0x7f080a9e

.field public static final memory_bar_system_rom:I = 0x7f080a9f

.field public static final memory_cache:I = 0x7f080aa0

.field public static final memory_dcim:I = 0x7f080aa1

.field public static final memory_other:I = 0x7f080aa2

.field public static final memory_second_space:I = 0x7f080aa3

.field public static final memory_system_rom:I = 0x7f080aa4

.field public static final meta:I = 0x7f080aa5

.field public static final miheadset_key_config_close:I = 0x7f080aa6

.field public static final miheadset_key_openanc_icon:I = 0x7f080aa7

.field public static final miheadset_openanc:I = 0x7f080aa8

.field public static final miheadset_transparent:I = 0x7f080aa9

.field public static final miui_beta_version_logo:I = 0x7f080aaa

.field public static final miui_face_enroll_success_name_edit:I = 0x7f080aab

.field public static final miui_face_input_detect_left_bottom:I = 0x7f080aac

.field public static final miui_face_input_detect_left_top:I = 0x7f080aad

.field public static final miui_face_input_detect_right_bottom:I = 0x7f080aae

.field public static final miui_face_input_detect_right_top:I = 0x7f080aaf

.field public static final miui_face_suggestion_detect_left_bottom:I = 0x7f080ab0

.field public static final miui_face_suggestion_detect_left_top:I = 0x7f080ab1

.field public static final miui_face_suggestion_detect_right_bottom:I = 0x7f080ab2

.field public static final miui_face_suggestion_detect_right_top:I = 0x7f080ab3

.field public static final miui_finger_enroll_back_image:I = 0x7f080ab4

.field public static final miui_keyguard_forget_password_mi:I = 0x7f080ab5

.field public static final miui_keyguard_forget_password_poco:I = 0x7f080ab6

.field public static final miui_logo:I = 0x7f080ab7

.field public static final miui_logo_alpha:I = 0x7f080ab8

.field public static final miui_version_logo:I = 0x7f080ab9

.field public static final miui_wizard:I = 0x7f080aba

.field public static final miuix_appcompat_action_bar_back_dark:I = 0x7f080abb

.field public static final miuix_appcompat_action_bar_back_light:I = 0x7f080abc

.field public static final miuix_appcompat_action_bar_bg_dark:I = 0x7f080abd

.field public static final miuix_appcompat_action_bar_bg_light:I = 0x7f080abe

.field public static final miuix_appcompat_action_bar_bg_navigation_dark:I = 0x7f080abf

.field public static final miuix_appcompat_action_bar_bg_navigation_light:I = 0x7f080ac0

.field public static final miuix_appcompat_action_bar_embeded_tabs_bg_dark:I = 0x7f080ac1

.field public static final miuix_appcompat_action_bar_embeded_tabs_bg_light:I = 0x7f080ac2

.field public static final miuix_appcompat_action_bar_split_bg_dark:I = 0x7f080ac3

.field public static final miuix_appcompat_action_bar_split_bg_expanded_dark:I = 0x7f080ac4

.field public static final miuix_appcompat_action_bar_split_bg_expanded_light:I = 0x7f080ac5

.field public static final miuix_appcompat_action_bar_split_bg_light:I = 0x7f080ac6

.field public static final miuix_appcompat_action_bar_split_bg_navigation_dark:I = 0x7f080ac7

.field public static final miuix_appcompat_action_bar_split_bg_navigation_light:I = 0x7f080ac8

.field public static final miuix_appcompat_action_bar_stack_bg_dark:I = 0x7f080ac9

.field public static final miuix_appcompat_action_bar_stack_bg_light:I = 0x7f080aca

.field public static final miuix_appcompat_action_bar_subtitle_bg_land:I = 0x7f080acb

.field public static final miuix_appcompat_action_bar_tab_bg:I = 0x7f080acc

.field public static final miuix_appcompat_action_bar_title_stack_bg_dark:I = 0x7f080acd

.field public static final miuix_appcompat_action_bar_title_stack_bg_light:I = 0x7f080ace

.field public static final miuix_appcompat_action_button_add_secret_dark:I = 0x7f080acf

.field public static final miuix_appcompat_action_button_add_secret_disable_dark:I = 0x7f080ad0

.field public static final miuix_appcompat_action_button_add_secret_disable_light:I = 0x7f080ad1

.field public static final miuix_appcompat_action_button_add_secret_light:I = 0x7f080ad2

.field public static final miuix_appcompat_action_button_add_secret_normal_dark:I = 0x7f080ad3

.field public static final miuix_appcompat_action_button_add_secret_normal_light:I = 0x7f080ad4

.field public static final miuix_appcompat_action_button_bg:I = 0x7f080ad5

.field public static final miuix_appcompat_action_button_blacklist_dark:I = 0x7f080ad6

.field public static final miuix_appcompat_action_button_blacklist_disable_dark:I = 0x7f080ad7

.field public static final miuix_appcompat_action_button_blacklist_disable_light:I = 0x7f080ad8

.field public static final miuix_appcompat_action_button_blacklist_light:I = 0x7f080ad9

.field public static final miuix_appcompat_action_button_blacklist_normal_dark:I = 0x7f080ada

.field public static final miuix_appcompat_action_button_blacklist_normal_light:I = 0x7f080adb

.field public static final miuix_appcompat_action_button_copy_dark:I = 0x7f080adc

.field public static final miuix_appcompat_action_button_copy_disable_dark:I = 0x7f080add

.field public static final miuix_appcompat_action_button_copy_disable_light:I = 0x7f080ade

.field public static final miuix_appcompat_action_button_copy_light:I = 0x7f080adf

.field public static final miuix_appcompat_action_button_copy_normal_dark:I = 0x7f080ae0

.field public static final miuix_appcompat_action_button_copy_normal_light:I = 0x7f080ae1

.field public static final miuix_appcompat_action_button_cut_dark:I = 0x7f080ae2

.field public static final miuix_appcompat_action_button_cut_disable_dark:I = 0x7f080ae3

.field public static final miuix_appcompat_action_button_cut_disable_light:I = 0x7f080ae4

.field public static final miuix_appcompat_action_button_cut_light:I = 0x7f080ae5

.field public static final miuix_appcompat_action_button_cut_normal_dark:I = 0x7f080ae6

.field public static final miuix_appcompat_action_button_cut_normal_light:I = 0x7f080ae7

.field public static final miuix_appcompat_action_button_delete_dark:I = 0x7f080ae8

.field public static final miuix_appcompat_action_button_delete_disable_dark:I = 0x7f080ae9

.field public static final miuix_appcompat_action_button_delete_disable_light:I = 0x7f080aea

.field public static final miuix_appcompat_action_button_delete_light:I = 0x7f080aeb

.field public static final miuix_appcompat_action_button_delete_normal_dark:I = 0x7f080aec

.field public static final miuix_appcompat_action_button_delete_normal_light:I = 0x7f080aed

.field public static final miuix_appcompat_action_button_discard_dark:I = 0x7f080aee

.field public static final miuix_appcompat_action_button_discard_disable_dark:I = 0x7f080aef

.field public static final miuix_appcompat_action_button_discard_disable_light:I = 0x7f080af0

.field public static final miuix_appcompat_action_button_discard_light:I = 0x7f080af1

.field public static final miuix_appcompat_action_button_discard_normal_dark:I = 0x7f080af2

.field public static final miuix_appcompat_action_button_discard_normal_light:I = 0x7f080af3

.field public static final miuix_appcompat_action_button_edit_dark:I = 0x7f080af4

.field public static final miuix_appcompat_action_button_edit_disable_dark:I = 0x7f080af5

.field public static final miuix_appcompat_action_button_edit_disable_light:I = 0x7f080af6

.field public static final miuix_appcompat_action_button_edit_light:I = 0x7f080af7

.field public static final miuix_appcompat_action_button_edit_message_dark:I = 0x7f080af8

.field public static final miuix_appcompat_action_button_edit_message_disable_dark:I = 0x7f080af9

.field public static final miuix_appcompat_action_button_edit_message_disable_light:I = 0x7f080afa

.field public static final miuix_appcompat_action_button_edit_message_light:I = 0x7f080afb

.field public static final miuix_appcompat_action_button_edit_message_normal_dark:I = 0x7f080afc

.field public static final miuix_appcompat_action_button_edit_message_normal_light:I = 0x7f080afd

.field public static final miuix_appcompat_action_button_edit_normal_dark:I = 0x7f080afe

.field public static final miuix_appcompat_action_button_edit_normal_light:I = 0x7f080aff

.field public static final miuix_appcompat_action_button_favorite_dark:I = 0x7f080b00

.field public static final miuix_appcompat_action_button_favorite_disable_dark:I = 0x7f080b01

.field public static final miuix_appcompat_action_button_favorite_disable_light:I = 0x7f080b02

.field public static final miuix_appcompat_action_button_favorite_light:I = 0x7f080b03

.field public static final miuix_appcompat_action_button_favorite_normal_dark:I = 0x7f080b04

.field public static final miuix_appcompat_action_button_favorite_normal_light:I = 0x7f080b05

.field public static final miuix_appcompat_action_button_main_bg:I = 0x7f080b06

.field public static final miuix_appcompat_action_button_main_delete:I = 0x7f080b07

.field public static final miuix_appcompat_action_button_main_edit:I = 0x7f080b08

.field public static final miuix_appcompat_action_button_main_new:I = 0x7f080b09

.field public static final miuix_appcompat_action_button_main_new_light:I = 0x7f080b0a

.field public static final miuix_appcompat_action_button_main_new_normal_light:I = 0x7f080b0b

.field public static final miuix_appcompat_action_button_main_new_pressed_light:I = 0x7f080b0c

.field public static final miuix_appcompat_action_button_move_dark:I = 0x7f080b0d

.field public static final miuix_appcompat_action_button_move_disable_dark:I = 0x7f080b0e

.field public static final miuix_appcompat_action_button_move_disable_light:I = 0x7f080b0f

.field public static final miuix_appcompat_action_button_move_light:I = 0x7f080b10

.field public static final miuix_appcompat_action_button_move_normal_dark:I = 0x7f080b11

.field public static final miuix_appcompat_action_button_move_normal_light:I = 0x7f080b12

.field public static final miuix_appcompat_action_button_new_dark:I = 0x7f080b13

.field public static final miuix_appcompat_action_button_new_disable_dark:I = 0x7f080b14

.field public static final miuix_appcompat_action_button_new_disable_light:I = 0x7f080b15

.field public static final miuix_appcompat_action_button_new_light:I = 0x7f080b16

.field public static final miuix_appcompat_action_button_new_normal_dark:I = 0x7f080b17

.field public static final miuix_appcompat_action_button_new_normal_light:I = 0x7f080b18

.field public static final miuix_appcompat_action_button_paste_dark:I = 0x7f080b19

.field public static final miuix_appcompat_action_button_paste_disable_dark:I = 0x7f080b1a

.field public static final miuix_appcompat_action_button_paste_disable_light:I = 0x7f080b1b

.field public static final miuix_appcompat_action_button_paste_light:I = 0x7f080b1c

.field public static final miuix_appcompat_action_button_paste_normal_dark:I = 0x7f080b1d

.field public static final miuix_appcompat_action_button_paste_normal_light:I = 0x7f080b1e

.field public static final miuix_appcompat_action_button_pause_dark:I = 0x7f080b1f

.field public static final miuix_appcompat_action_button_pause_disable_dark:I = 0x7f080b20

.field public static final miuix_appcompat_action_button_pause_disable_light:I = 0x7f080b21

.field public static final miuix_appcompat_action_button_pause_light:I = 0x7f080b22

.field public static final miuix_appcompat_action_button_pause_normal_dark:I = 0x7f080b23

.field public static final miuix_appcompat_action_button_pause_normal_light:I = 0x7f080b24

.field public static final miuix_appcompat_action_button_refresh_dark:I = 0x7f080b25

.field public static final miuix_appcompat_action_button_refresh_disable_dark:I = 0x7f080b26

.field public static final miuix_appcompat_action_button_refresh_disable_light:I = 0x7f080b27

.field public static final miuix_appcompat_action_button_refresh_light:I = 0x7f080b28

.field public static final miuix_appcompat_action_button_refresh_normal_dark:I = 0x7f080b29

.field public static final miuix_appcompat_action_button_refresh_normal_light:I = 0x7f080b2a

.field public static final miuix_appcompat_action_button_remove_blacklist_dark:I = 0x7f080b2b

.field public static final miuix_appcompat_action_button_remove_blacklist_disable_dark:I = 0x7f080b2c

.field public static final miuix_appcompat_action_button_remove_blacklist_disable_light:I = 0x7f080b2d

.field public static final miuix_appcompat_action_button_remove_blacklist_light:I = 0x7f080b2e

.field public static final miuix_appcompat_action_button_remove_blacklist_normal_dark:I = 0x7f080b2f

.field public static final miuix_appcompat_action_button_remove_blacklist_normal_light:I = 0x7f080b30

.field public static final miuix_appcompat_action_button_remove_secret_dark:I = 0x7f080b31

.field public static final miuix_appcompat_action_button_remove_secret_disable_dark:I = 0x7f080b32

.field public static final miuix_appcompat_action_button_remove_secret_disable_light:I = 0x7f080b33

.field public static final miuix_appcompat_action_button_remove_secret_light:I = 0x7f080b34

.field public static final miuix_appcompat_action_button_remove_secret_normal_dark:I = 0x7f080b35

.field public static final miuix_appcompat_action_button_remove_secret_normal_light:I = 0x7f080b36

.field public static final miuix_appcompat_action_button_rename_dark:I = 0x7f080b37

.field public static final miuix_appcompat_action_button_rename_disable_dark:I = 0x7f080b38

.field public static final miuix_appcompat_action_button_rename_disable_light:I = 0x7f080b39

.field public static final miuix_appcompat_action_button_rename_light:I = 0x7f080b3a

.field public static final miuix_appcompat_action_button_rename_normal_dark:I = 0x7f080b3b

.field public static final miuix_appcompat_action_button_rename_normal_light:I = 0x7f080b3c

.field public static final miuix_appcompat_action_button_save_dark:I = 0x7f080b3d

.field public static final miuix_appcompat_action_button_save_disable_dark:I = 0x7f080b3e

.field public static final miuix_appcompat_action_button_save_disable_light:I = 0x7f080b3f

.field public static final miuix_appcompat_action_button_save_light:I = 0x7f080b40

.field public static final miuix_appcompat_action_button_save_normal_dark:I = 0x7f080b41

.field public static final miuix_appcompat_action_button_save_normal_light:I = 0x7f080b42

.field public static final miuix_appcompat_action_button_search_dark:I = 0x7f080b43

.field public static final miuix_appcompat_action_button_search_disable_dark:I = 0x7f080b44

.field public static final miuix_appcompat_action_button_search_disable_light:I = 0x7f080b45

.field public static final miuix_appcompat_action_button_search_light:I = 0x7f080b46

.field public static final miuix_appcompat_action_button_search_normal_dark:I = 0x7f080b47

.field public static final miuix_appcompat_action_button_search_normal_light:I = 0x7f080b48

.field public static final miuix_appcompat_action_button_send_dark:I = 0x7f080b49

.field public static final miuix_appcompat_action_button_send_disable_dark:I = 0x7f080b4a

.field public static final miuix_appcompat_action_button_send_disable_light:I = 0x7f080b4b

.field public static final miuix_appcompat_action_button_send_light:I = 0x7f080b4c

.field public static final miuix_appcompat_action_button_send_normal_dark:I = 0x7f080b4d

.field public static final miuix_appcompat_action_button_send_normal_light:I = 0x7f080b4e

.field public static final miuix_appcompat_action_button_setting_dark:I = 0x7f080b4f

.field public static final miuix_appcompat_action_button_setting_disable_dark:I = 0x7f080b50

.field public static final miuix_appcompat_action_button_setting_disable_light:I = 0x7f080b51

.field public static final miuix_appcompat_action_button_setting_light:I = 0x7f080b52

.field public static final miuix_appcompat_action_button_setting_normal_dark:I = 0x7f080b53

.field public static final miuix_appcompat_action_button_setting_normal_light:I = 0x7f080b54

.field public static final miuix_appcompat_action_button_share_dark:I = 0x7f080b55

.field public static final miuix_appcompat_action_button_share_disable_dark:I = 0x7f080b56

.field public static final miuix_appcompat_action_button_share_disable_light:I = 0x7f080b57

.field public static final miuix_appcompat_action_button_share_light:I = 0x7f080b58

.field public static final miuix_appcompat_action_button_share_normal_dark:I = 0x7f080b59

.field public static final miuix_appcompat_action_button_share_normal_light:I = 0x7f080b5a

.field public static final miuix_appcompat_action_button_start_dark:I = 0x7f080b5b

.field public static final miuix_appcompat_action_button_start_disable_dark:I = 0x7f080b5c

.field public static final miuix_appcompat_action_button_start_disable_light:I = 0x7f080b5d

.field public static final miuix_appcompat_action_button_start_light:I = 0x7f080b5e

.field public static final miuix_appcompat_action_button_start_normal_dark:I = 0x7f080b5f

.field public static final miuix_appcompat_action_button_start_normal_light:I = 0x7f080b60

.field public static final miuix_appcompat_action_button_stick_dark:I = 0x7f080b61

.field public static final miuix_appcompat_action_button_stick_disable_dark:I = 0x7f080b62

.field public static final miuix_appcompat_action_button_stick_disable_light:I = 0x7f080b63

.field public static final miuix_appcompat_action_button_stick_light:I = 0x7f080b64

.field public static final miuix_appcompat_action_button_stick_normal_dark:I = 0x7f080b65

.field public static final miuix_appcompat_action_button_stick_normal_light:I = 0x7f080b66

.field public static final miuix_appcompat_action_button_unfavorite_dark:I = 0x7f080b67

.field public static final miuix_appcompat_action_button_unfavorite_disable_dark:I = 0x7f080b68

.field public static final miuix_appcompat_action_button_unfavorite_disable_light:I = 0x7f080b69

.field public static final miuix_appcompat_action_button_unfavorite_light:I = 0x7f080b6a

.field public static final miuix_appcompat_action_button_unfavorite_normal_dark:I = 0x7f080b6b

.field public static final miuix_appcompat_action_button_unfavorite_normal_light:I = 0x7f080b6c

.field public static final miuix_appcompat_action_button_unstick_dark:I = 0x7f080b6d

.field public static final miuix_appcompat_action_button_unstick_disable_dark:I = 0x7f080b6e

.field public static final miuix_appcompat_action_button_unstick_disable_light:I = 0x7f080b6f

.field public static final miuix_appcompat_action_button_unstick_light:I = 0x7f080b70

.field public static final miuix_appcompat_action_button_unstick_normal_dark:I = 0x7f080b71

.field public static final miuix_appcompat_action_button_unstick_normal_light:I = 0x7f080b72

.field public static final miuix_appcompat_action_button_update_dark:I = 0x7f080b73

.field public static final miuix_appcompat_action_button_update_disable_dark:I = 0x7f080b74

.field public static final miuix_appcompat_action_button_update_disable_light:I = 0x7f080b75

.field public static final miuix_appcompat_action_button_update_light:I = 0x7f080b76

.field public static final miuix_appcompat_action_button_update_normal_dark:I = 0x7f080b77

.field public static final miuix_appcompat_action_button_update_normal_light:I = 0x7f080b78

.field public static final miuix_appcompat_action_mode_bg_dark:I = 0x7f080b79

.field public static final miuix_appcompat_action_mode_bg_light:I = 0x7f080b7a

.field public static final miuix_appcompat_action_mode_bg_navigation_dark:I = 0x7f080b7b

.field public static final miuix_appcompat_action_mode_bg_navigation_light:I = 0x7f080b7c

.field public static final miuix_appcompat_action_mode_button_bg_dark:I = 0x7f080b7d

.field public static final miuix_appcompat_action_mode_button_bg_light:I = 0x7f080b7e

.field public static final miuix_appcompat_action_mode_button_bg_single_disable_dark:I = 0x7f080b7f

.field public static final miuix_appcompat_action_mode_button_bg_single_disable_light:I = 0x7f080b80

.field public static final miuix_appcompat_action_mode_button_bg_single_normal_dark:I = 0x7f080b81

.field public static final miuix_appcompat_action_mode_button_bg_single_normal_light:I = 0x7f080b82

.field public static final miuix_appcompat_action_mode_button_bg_single_pressed_dark:I = 0x7f080b83

.field public static final miuix_appcompat_action_mode_button_bg_single_pressed_light:I = 0x7f080b84

.field public static final miuix_appcompat_action_mode_button_bg_single_selected_dark:I = 0x7f080b85

.field public static final miuix_appcompat_action_mode_button_bg_single_selected_light:I = 0x7f080b86

.field public static final miuix_appcompat_action_mode_button_bg_single_selected_pressed_dark:I = 0x7f080b87

.field public static final miuix_appcompat_action_mode_button_bg_single_selected_pressed_light:I = 0x7f080b88

.field public static final miuix_appcompat_action_mode_button_more_dark:I = 0x7f080b89

.field public static final miuix_appcompat_action_mode_button_more_disable_dark:I = 0x7f080b8a

.field public static final miuix_appcompat_action_mode_button_more_disable_light:I = 0x7f080b8b

.field public static final miuix_appcompat_action_mode_button_more_light:I = 0x7f080b8c

.field public static final miuix_appcompat_action_mode_button_more_normal_dark:I = 0x7f080b8d

.field public static final miuix_appcompat_action_mode_button_more_normal_light:I = 0x7f080b8e

.field public static final miuix_appcompat_action_mode_button_more_selected_dark:I = 0x7f080b8f

.field public static final miuix_appcompat_action_mode_button_more_selected_light:I = 0x7f080b90

.field public static final miuix_appcompat_action_mode_immersion_close_dark:I = 0x7f080b91

.field public static final miuix_appcompat_action_mode_immersion_close_disable_dark:I = 0x7f080b92

.field public static final miuix_appcompat_action_mode_immersion_close_disable_light:I = 0x7f080b93

.field public static final miuix_appcompat_action_mode_immersion_close_light:I = 0x7f080b94

.field public static final miuix_appcompat_action_mode_immersion_close_normal_dark:I = 0x7f080b95

.field public static final miuix_appcompat_action_mode_immersion_close_normal_light:I = 0x7f080b96

.field public static final miuix_appcompat_action_mode_immersion_done_dark:I = 0x7f080b97

.field public static final miuix_appcompat_action_mode_immersion_done_disable_dark:I = 0x7f080b98

.field public static final miuix_appcompat_action_mode_immersion_done_disable_light:I = 0x7f080b99

.field public static final miuix_appcompat_action_mode_immersion_done_light:I = 0x7f080b9a

.field public static final miuix_appcompat_action_mode_immersion_done_normal_dark:I = 0x7f080b9b

.field public static final miuix_appcompat_action_mode_immersion_done_normal_light:I = 0x7f080b9c

.field public static final miuix_appcompat_action_mode_immersion_more_dark:I = 0x7f080b9d

.field public static final miuix_appcompat_action_mode_immersion_more_disable_dark:I = 0x7f080b9e

.field public static final miuix_appcompat_action_mode_immersion_more_disable_light:I = 0x7f080b9f

.field public static final miuix_appcompat_action_mode_immersion_more_light:I = 0x7f080ba0

.field public static final miuix_appcompat_action_mode_immersion_more_normal_dark:I = 0x7f080ba1

.field public static final miuix_appcompat_action_mode_immersion_more_normal_light:I = 0x7f080ba2

.field public static final miuix_appcompat_action_mode_split_bg_navigation_dark:I = 0x7f080ba3

.field public static final miuix_appcompat_action_mode_split_bg_navigation_light:I = 0x7f080ba4

.field public static final miuix_appcompat_action_mode_title_button_bg_dark:I = 0x7f080ba5

.field public static final miuix_appcompat_action_mode_title_button_bg_disable_dark:I = 0x7f080ba6

.field public static final miuix_appcompat_action_mode_title_button_bg_disable_light:I = 0x7f080ba7

.field public static final miuix_appcompat_action_mode_title_button_bg_light:I = 0x7f080ba8

.field public static final miuix_appcompat_action_mode_title_button_bg_normal_dark:I = 0x7f080ba9

.field public static final miuix_appcompat_action_mode_title_button_bg_normal_light:I = 0x7f080baa

.field public static final miuix_appcompat_action_mode_title_button_bg_pressed_dark:I = 0x7f080bab

.field public static final miuix_appcompat_action_mode_title_button_bg_pressed_light:I = 0x7f080bac

.field public static final miuix_appcompat_action_mode_title_button_cancel_dark:I = 0x7f080bad

.field public static final miuix_appcompat_action_mode_title_button_cancel_disable_dark:I = 0x7f080bae

.field public static final miuix_appcompat_action_mode_title_button_cancel_disable_light:I = 0x7f080baf

.field public static final miuix_appcompat_action_mode_title_button_cancel_light:I = 0x7f080bb0

.field public static final miuix_appcompat_action_mode_title_button_cancel_normal_dark:I = 0x7f080bb1

.field public static final miuix_appcompat_action_mode_title_button_cancel_normal_light:I = 0x7f080bb2

.field public static final miuix_appcompat_action_mode_title_button_confirm_dark:I = 0x7f080bb3

.field public static final miuix_appcompat_action_mode_title_button_confirm_disable_dark:I = 0x7f080bb4

.field public static final miuix_appcompat_action_mode_title_button_confirm_disable_light:I = 0x7f080bb5

.field public static final miuix_appcompat_action_mode_title_button_confirm_light:I = 0x7f080bb6

.field public static final miuix_appcompat_action_mode_title_button_confirm_normal_dark:I = 0x7f080bb7

.field public static final miuix_appcompat_action_mode_title_button_confirm_normal_light:I = 0x7f080bb8

.field public static final miuix_appcompat_action_mode_title_button_delete_dark:I = 0x7f080bb9

.field public static final miuix_appcompat_action_mode_title_button_delete_disable_dark:I = 0x7f080bba

.field public static final miuix_appcompat_action_mode_title_button_delete_disable_light:I = 0x7f080bbb

.field public static final miuix_appcompat_action_mode_title_button_delete_light:I = 0x7f080bbc

.field public static final miuix_appcompat_action_mode_title_button_delete_normal_dark:I = 0x7f080bbd

.field public static final miuix_appcompat_action_mode_title_button_delete_normal_light:I = 0x7f080bbe

.field public static final miuix_appcompat_action_mode_title_button_deselect_all_dark:I = 0x7f080bbf

.field public static final miuix_appcompat_action_mode_title_button_deselect_all_disable_dark:I = 0x7f080bc0

.field public static final miuix_appcompat_action_mode_title_button_deselect_all_disable_light:I = 0x7f080bc1

.field public static final miuix_appcompat_action_mode_title_button_deselect_all_light:I = 0x7f080bc2

.field public static final miuix_appcompat_action_mode_title_button_deselect_all_normal_dark:I = 0x7f080bc3

.field public static final miuix_appcompat_action_mode_title_button_deselect_all_normal_light:I = 0x7f080bc4

.field public static final miuix_appcompat_action_mode_title_button_select_all_dark:I = 0x7f080bc5

.field public static final miuix_appcompat_action_mode_title_button_select_all_disable_dark:I = 0x7f080bc6

.field public static final miuix_appcompat_action_mode_title_button_select_all_disable_light:I = 0x7f080bc7

.field public static final miuix_appcompat_action_mode_title_button_select_all_light:I = 0x7f080bc8

.field public static final miuix_appcompat_action_mode_title_button_select_all_normal_dark:I = 0x7f080bc9

.field public static final miuix_appcompat_action_mode_title_button_select_all_normal_light:I = 0x7f080bca

.field public static final miuix_appcompat_action_mode_title_default_button_bg_dark:I = 0x7f080bcb

.field public static final miuix_appcompat_action_mode_title_default_button_bg_disable_dark:I = 0x7f080bcc

.field public static final miuix_appcompat_action_mode_title_default_button_bg_disable_light:I = 0x7f080bcd

.field public static final miuix_appcompat_action_mode_title_default_button_bg_light:I = 0x7f080bce

.field public static final miuix_appcompat_action_mode_title_default_button_bg_normal_dark:I = 0x7f080bcf

.field public static final miuix_appcompat_action_mode_title_default_button_bg_normal_light:I = 0x7f080bd0

.field public static final miuix_appcompat_action_mode_title_default_button_bg_pressed_dark:I = 0x7f080bd1

.field public static final miuix_appcompat_action_mode_title_default_button_bg_pressed_light:I = 0x7f080bd2

.field public static final miuix_appcompat_action_tab_badge:I = 0x7f080bd3

.field public static final miuix_appcompat_alphabet_indexer_bg:I = 0x7f080bd4

.field public static final miuix_appcompat_alphabet_indexer_bg_dark:I = 0x7f080bd5

.field public static final miuix_appcompat_alphabet_indexer_overlay_dark:I = 0x7f080bd6

.field public static final miuix_appcompat_alphabet_indexer_overlay_light:I = 0x7f080bd7

.field public static final miuix_appcompat_arrow_filter_sort_tab_view_dark:I = 0x7f080bd8

.field public static final miuix_appcompat_arrow_filter_sort_tab_view_light:I = 0x7f080bd9

.field public static final miuix_appcompat_arrow_popup_bg_dark:I = 0x7f080bda

.field public static final miuix_appcompat_arrow_popup_bg_light:I = 0x7f080bdb

.field public static final miuix_appcompat_arrow_popup_bottom_left_dark:I = 0x7f080bdc

.field public static final miuix_appcompat_arrow_popup_bottom_left_light:I = 0x7f080bdd

.field public static final miuix_appcompat_arrow_popup_bottom_middle_dark:I = 0x7f080bde

.field public static final miuix_appcompat_arrow_popup_bottom_middle_light:I = 0x7f080bdf

.field public static final miuix_appcompat_arrow_popup_bottom_right_dark:I = 0x7f080be0

.field public static final miuix_appcompat_arrow_popup_bottom_right_light:I = 0x7f080be1

.field public static final miuix_appcompat_arrow_popup_left_dark:I = 0x7f080be2

.field public static final miuix_appcompat_arrow_popup_left_light:I = 0x7f080be3

.field public static final miuix_appcompat_arrow_popup_right_dark:I = 0x7f080be4

.field public static final miuix_appcompat_arrow_popup_right_light:I = 0x7f080be5

.field public static final miuix_appcompat_arrow_popup_top_left_dark:I = 0x7f080be6

.field public static final miuix_appcompat_arrow_popup_top_left_light:I = 0x7f080be7

.field public static final miuix_appcompat_arrow_popup_top_middle_dark:I = 0x7f080be8

.field public static final miuix_appcompat_arrow_popup_top_middle_light:I = 0x7f080be9

.field public static final miuix_appcompat_arrow_popup_top_right_dark:I = 0x7f080bea

.field public static final miuix_appcompat_arrow_popup_top_right_light:I = 0x7f080beb

.field public static final miuix_appcompat_arrow_right:I = 0x7f080bec

.field public static final miuix_appcompat_arrow_up_down:I = 0x7f080bed

.field public static final miuix_appcompat_arrow_up_down_integrated:I = 0x7f080bee

.field public static final miuix_appcompat_auto_complete_text_input_background:I = 0x7f080bef

.field public static final miuix_appcompat_auto_complete_text_popup_background:I = 0x7f080bf0

.field public static final miuix_appcompat_btn_bg_dialog_foreground:I = 0x7f080bf1

.field public static final miuix_appcompat_btn_bg_dialog_foreground_dark:I = 0x7f080bf2

.field public static final miuix_appcompat_btn_bg_dialog_foreground_dark_p1:I = 0x7f080bf3

.field public static final miuix_appcompat_btn_bg_dialog_foreground_dark_p2:I = 0x7f080bf4

.field public static final miuix_appcompat_btn_bg_dialog_foreground_dark_p3:I = 0x7f080bf5

.field public static final miuix_appcompat_btn_bg_dialog_foreground_light:I = 0x7f080bf6

.field public static final miuix_appcompat_btn_bg_dialog_foreground_light_p1:I = 0x7f080bf7

.field public static final miuix_appcompat_btn_bg_dialog_foreground_light_p2:I = 0x7f080bf8

.field public static final miuix_appcompat_btn_bg_dialog_foreground_light_p3:I = 0x7f080bf9

.field public static final miuix_appcompat_btn_bg_dialog_selector_dark:I = 0x7f080bfa

.field public static final miuix_appcompat_btn_bg_dialog_selector_dark_no_anim:I = 0x7f080bfb

.field public static final miuix_appcompat_btn_bg_dialog_selector_dark_p1:I = 0x7f080bfc

.field public static final miuix_appcompat_btn_bg_dialog_selector_dark_p2:I = 0x7f080bfd

.field public static final miuix_appcompat_btn_bg_dialog_selector_dark_p3:I = 0x7f080bfe

.field public static final miuix_appcompat_btn_bg_dialog_selector_dark_press:I = 0x7f080bff

.field public static final miuix_appcompat_btn_bg_dialog_selector_light:I = 0x7f080c00

.field public static final miuix_appcompat_btn_bg_dialog_selector_light_no_anim:I = 0x7f080c01

.field public static final miuix_appcompat_btn_bg_dialog_selector_light_p1:I = 0x7f080c02

.field public static final miuix_appcompat_btn_bg_dialog_selector_light_p2:I = 0x7f080c03

.field public static final miuix_appcompat_btn_bg_dialog_selector_light_p3:I = 0x7f080c04

.field public static final miuix_appcompat_btn_bg_dialog_selector_light_press:I = 0x7f080c05

.field public static final miuix_appcompat_btn_bg_dialog_selector_primary_dark:I = 0x7f080c06

.field public static final miuix_appcompat_btn_bg_dialog_selector_primary_dark_no_anim:I = 0x7f080c07

.field public static final miuix_appcompat_btn_bg_dialog_selector_primary_dark_p1:I = 0x7f080c08

.field public static final miuix_appcompat_btn_bg_dialog_selector_primary_dark_p2:I = 0x7f080c09

.field public static final miuix_appcompat_btn_bg_dialog_selector_primary_dark_p3:I = 0x7f080c0a

.field public static final miuix_appcompat_btn_bg_dialog_selector_primary_dark_press:I = 0x7f080c0b

.field public static final miuix_appcompat_btn_bg_dialog_selector_primary_light:I = 0x7f080c0c

.field public static final miuix_appcompat_btn_bg_dialog_selector_primary_light_no_anim:I = 0x7f080c0d

.field public static final miuix_appcompat_btn_bg_dialog_selector_primary_light_p1:I = 0x7f080c0e

.field public static final miuix_appcompat_btn_bg_dialog_selector_primary_light_p2:I = 0x7f080c0f

.field public static final miuix_appcompat_btn_bg_dialog_selector_primary_light_p3:I = 0x7f080c10

.field public static final miuix_appcompat_btn_bg_dialog_selector_primary_light_press:I = 0x7f080c11

.field public static final miuix_appcompat_btn_bg_rect_dark:I = 0x7f080c12

.field public static final miuix_appcompat_btn_bg_rect_light:I = 0x7f080c13

.field public static final miuix_appcompat_btn_bg_rect_normal_dark:I = 0x7f080c14

.field public static final miuix_appcompat_btn_bg_rect_normal_light:I = 0x7f080c15

.field public static final miuix_appcompat_btn_bg_rect_pressed_dark:I = 0x7f080c16

.field public static final miuix_appcompat_btn_bg_rect_pressed_light:I = 0x7f080c17

.field public static final miuix_appcompat_btn_bg_warn_dark:I = 0x7f080c18

.field public static final miuix_appcompat_btn_bg_warn_light:I = 0x7f080c19

.field public static final miuix_appcompat_btn_bg_warn_single_disable_dark:I = 0x7f080c1a

.field public static final miuix_appcompat_btn_bg_warn_single_disable_light:I = 0x7f080c1b

.field public static final miuix_appcompat_btn_bg_warn_single_normal_dark:I = 0x7f080c1c

.field public static final miuix_appcompat_btn_bg_warn_single_normal_light:I = 0x7f080c1d

.field public static final miuix_appcompat_btn_bg_warn_single_pressed_dark:I = 0x7f080c1e

.field public static final miuix_appcompat_btn_bg_warn_single_pressed_light:I = 0x7f080c1f

.field public static final miuix_appcompat_btn_checkbox_dark:I = 0x7f080c20

.field public static final miuix_appcompat_btn_checkbox_dialog_dark:I = 0x7f080c21

.field public static final miuix_appcompat_btn_checkbox_dialog_light:I = 0x7f080c22

.field public static final miuix_appcompat_btn_checkbox_dialog_off_disabled_dark:I = 0x7f080c23

.field public static final miuix_appcompat_btn_checkbox_dialog_off_disabled_light:I = 0x7f080c24

.field public static final miuix_appcompat_btn_checkbox_dialog_off_normal_dark:I = 0x7f080c25

.field public static final miuix_appcompat_btn_checkbox_dialog_off_normal_light:I = 0x7f080c26

.field public static final miuix_appcompat_btn_checkbox_dialog_on_disabled_dark:I = 0x7f080c27

.field public static final miuix_appcompat_btn_checkbox_dialog_on_disabled_light:I = 0x7f080c28

.field public static final miuix_appcompat_btn_checkbox_dialog_on_normal_dark:I = 0x7f080c29

.field public static final miuix_appcompat_btn_checkbox_dialog_on_normal_light:I = 0x7f080c2a

.field public static final miuix_appcompat_btn_checkbox_expand_dark:I = 0x7f080c2b

.field public static final miuix_appcompat_btn_checkbox_expand_light:I = 0x7f080c2c

.field public static final miuix_appcompat_btn_checkbox_light:I = 0x7f080c2d

.field public static final miuix_appcompat_btn_checkbox_off_disabled_dark:I = 0x7f080c2e

.field public static final miuix_appcompat_btn_checkbox_off_disabled_light:I = 0x7f080c2f

.field public static final miuix_appcompat_btn_checkbox_off_normal_dark:I = 0x7f080c30

.field public static final miuix_appcompat_btn_checkbox_off_normal_light:I = 0x7f080c31

.field public static final miuix_appcompat_btn_checkbox_off_to_on_dark:I = 0x7f080c32

.field public static final miuix_appcompat_btn_checkbox_off_to_on_light:I = 0x7f080c33

.field public static final miuix_appcompat_btn_checkbox_on_disabled_dark:I = 0x7f080c34

.field public static final miuix_appcompat_btn_checkbox_on_disabled_light:I = 0x7f080c35

.field public static final miuix_appcompat_btn_checkbox_on_normal_dark:I = 0x7f080c36

.field public static final miuix_appcompat_btn_checkbox_on_normal_light:I = 0x7f080c37

.field public static final miuix_appcompat_btn_checkbox_on_to_off_dark:I = 0x7f080c38

.field public static final miuix_appcompat_btn_checkbox_on_to_off_light:I = 0x7f080c39

.field public static final miuix_appcompat_btn_inline_delete_dark:I = 0x7f080c3a

.field public static final miuix_appcompat_btn_inline_delete_light:I = 0x7f080c3b

.field public static final miuix_appcompat_btn_inline_delete_normal_dark:I = 0x7f080c3c

.field public static final miuix_appcompat_btn_inline_delete_normal_light:I = 0x7f080c3d

.field public static final miuix_appcompat_btn_inline_delete_pressed_dark:I = 0x7f080c3e

.field public static final miuix_appcompat_btn_inline_delete_pressed_light:I = 0x7f080c3f

.field public static final miuix_appcompat_btn_inline_detail_dark:I = 0x7f080c40

.field public static final miuix_appcompat_btn_inline_detail_disable_dark:I = 0x7f080c41

.field public static final miuix_appcompat_btn_inline_detail_disable_light:I = 0x7f080c42

.field public static final miuix_appcompat_btn_inline_detail_light:I = 0x7f080c43

.field public static final miuix_appcompat_btn_inline_detail_normal_dark:I = 0x7f080c44

.field public static final miuix_appcompat_btn_inline_detail_normal_light:I = 0x7f080c45

.field public static final miuix_appcompat_btn_inline_detail_pressed_dark:I = 0x7f080c46

.field public static final miuix_appcompat_btn_inline_detail_pressed_light:I = 0x7f080c47

.field public static final miuix_appcompat_btn_inline_expand_dark:I = 0x7f080c48

.field public static final miuix_appcompat_btn_inline_expand_disable_dark:I = 0x7f080c49

.field public static final miuix_appcompat_btn_inline_expand_disable_light:I = 0x7f080c4a

.field public static final miuix_appcompat_btn_inline_expand_light:I = 0x7f080c4b

.field public static final miuix_appcompat_btn_inline_expand_normal_dark:I = 0x7f080c4c

.field public static final miuix_appcompat_btn_inline_expand_normal_light:I = 0x7f080c4d

.field public static final miuix_appcompat_btn_inline_expand_pressed_dark:I = 0x7f080c4e

.field public static final miuix_appcompat_btn_inline_expand_pressed_light:I = 0x7f080c4f

.field public static final miuix_appcompat_btn_inline_shrink_dark:I = 0x7f080c50

.field public static final miuix_appcompat_btn_inline_shrink_disable_dark:I = 0x7f080c51

.field public static final miuix_appcompat_btn_inline_shrink_disable_light:I = 0x7f080c52

.field public static final miuix_appcompat_btn_inline_shrink_light:I = 0x7f080c53

.field public static final miuix_appcompat_btn_inline_shrink_normal_dark:I = 0x7f080c54

.field public static final miuix_appcompat_btn_inline_shrink_normal_light:I = 0x7f080c55

.field public static final miuix_appcompat_btn_inline_shrink_pressed_dark:I = 0x7f080c56

.field public static final miuix_appcompat_btn_inline_shrink_pressed_light:I = 0x7f080c57

.field public static final miuix_appcompat_btn_radio_arrow:I = 0x7f080c58

.field public static final miuix_appcompat_btn_radio_arrow_off:I = 0x7f080c59

.field public static final miuix_appcompat_btn_radio_dark:I = 0x7f080c5a

.field public static final miuix_appcompat_btn_radio_light:I = 0x7f080c5b

.field public static final miuix_appcompat_btn_radio_off_disabled_dark:I = 0x7f080c5c

.field public static final miuix_appcompat_btn_radio_off_disabled_light:I = 0x7f080c5d

.field public static final miuix_appcompat_btn_radio_off_normal_dark:I = 0x7f080c5e

.field public static final miuix_appcompat_btn_radio_off_normal_light:I = 0x7f080c5f

.field public static final miuix_appcompat_btn_radio_off_to_on_dark:I = 0x7f080c60

.field public static final miuix_appcompat_btn_radio_off_to_on_light:I = 0x7f080c61

.field public static final miuix_appcompat_btn_radio_on_dark:I = 0x7f080c62

.field public static final miuix_appcompat_btn_radio_on_disabled_dark:I = 0x7f080c63

.field public static final miuix_appcompat_btn_radio_on_disabled_light:I = 0x7f080c64

.field public static final miuix_appcompat_btn_radio_on_light:I = 0x7f080c65

.field public static final miuix_appcompat_btn_radio_on_normal_dark:I = 0x7f080c66

.field public static final miuix_appcompat_btn_radio_on_normal_light:I = 0x7f080c67

.field public static final miuix_appcompat_btn_radio_on_to_off_dark:I = 0x7f080c68

.field public static final miuix_appcompat_btn_radio_on_to_off_light:I = 0x7f080c69

.field public static final miuix_appcompat_context_menu_separate_item_bg_dark:I = 0x7f080c6a

.field public static final miuix_appcompat_context_menu_separate_item_bg_light:I = 0x7f080c6b

.field public static final miuix_appcompat_default_btn_radio_arrow_on:I = 0x7f080c6c

.field public static final miuix_appcompat_default_ic_invisible_dark:I = 0x7f080c6d

.field public static final miuix_appcompat_default_ic_invisible_light:I = 0x7f080c6e

.field public static final miuix_appcompat_default_ic_visible_dark:I = 0x7f080c6f

.field public static final miuix_appcompat_default_ic_visible_light:I = 0x7f080c70

.field public static final miuix_appcompat_dialog_bg_dark:I = 0x7f080c71

.field public static final miuix_appcompat_dialog_bg_dark_common:I = 0x7f080c72

.field public static final miuix_appcompat_dialog_bg_dark_smooth:I = 0x7f080c73

.field public static final miuix_appcompat_dialog_bg_light:I = 0x7f080c74

.field public static final miuix_appcompat_dialog_bg_light_common:I = 0x7f080c75

.field public static final miuix_appcompat_dialog_bg_light_smooth:I = 0x7f080c76

.field public static final miuix_appcompat_dialog_default_btn_bg_dark:I = 0x7f080c77

.field public static final miuix_appcompat_dialog_default_btn_bg_dark_common:I = 0x7f080c78

.field public static final miuix_appcompat_dialog_default_btn_bg_dark_smooth:I = 0x7f080c79

.field public static final miuix_appcompat_dialog_default_btn_bg_light:I = 0x7f080c7a

.field public static final miuix_appcompat_dialog_default_btn_bg_light_common:I = 0x7f080c7b

.field public static final miuix_appcompat_dialog_default_btn_bg_light_smooth:I = 0x7f080c7c

.field public static final miuix_appcompat_dialog_default_btn_bg_primary_dark:I = 0x7f080c7d

.field public static final miuix_appcompat_dialog_default_btn_bg_primary_dark_common:I = 0x7f080c7e

.field public static final miuix_appcompat_dialog_default_btn_bg_primary_dark_smooth:I = 0x7f080c7f

.field public static final miuix_appcompat_dialog_default_btn_bg_primary_light:I = 0x7f080c80

.field public static final miuix_appcompat_dialog_default_btn_bg_primary_light_common:I = 0x7f080c81

.field public static final miuix_appcompat_dialog_default_btn_bg_primary_light_smooth:I = 0x7f080c82

.field public static final miuix_appcompat_dialog_round_bg_dark:I = 0x7f080c83

.field public static final miuix_appcompat_dialog_round_bg_dark_common:I = 0x7f080c84

.field public static final miuix_appcompat_dialog_round_bg_dark_smooth:I = 0x7f080c85

.field public static final miuix_appcompat_dialog_round_bg_light:I = 0x7f080c86

.field public static final miuix_appcompat_dialog_round_bg_light_common:I = 0x7f080c87

.field public static final miuix_appcompat_dialog_round_bg_light_smooth:I = 0x7f080c88

.field public static final miuix_appcompat_dialog_title_bg_dark:I = 0x7f080c89

.field public static final miuix_appcompat_dialog_title_bg_light:I = 0x7f080c8a

.field public static final miuix_appcompat_divider_line_dark:I = 0x7f080c8b

.field public static final miuix_appcompat_divider_line_light:I = 0x7f080c8c

.field public static final miuix_appcompat_dropdown_indicator_down:I = 0x7f080c8d

.field public static final miuix_appcompat_dropdown_indicator_down_n:I = 0x7f080c8e

.field public static final miuix_appcompat_dropdown_indicator_down_p:I = 0x7f080c8f

.field public static final miuix_appcompat_dropdown_listview_bg_dark:I = 0x7f080c90

.field public static final miuix_appcompat_dropdown_listview_bg_light:I = 0x7f080c91

.field public static final miuix_appcompat_edit_text_bg_dark:I = 0x7f080c92

.field public static final miuix_appcompat_edit_text_bg_dialog_dark:I = 0x7f080c93

.field public static final miuix_appcompat_edit_text_bg_dialog_light:I = 0x7f080c94

.field public static final miuix_appcompat_edit_text_bg_light:I = 0x7f080c95

.field public static final miuix_appcompat_edit_text_clear_btn_dark:I = 0x7f080c96

.field public static final miuix_appcompat_edit_text_clear_btn_light:I = 0x7f080c97

.field public static final miuix_appcompat_edit_text_clear_btn_on_dark:I = 0x7f080c98

.field public static final miuix_appcompat_edit_text_clear_btn_on_light:I = 0x7f080c99

.field public static final miuix_appcompat_edit_text_search_bg_dark:I = 0x7f080c9a

.field public static final miuix_appcompat_edit_text_search_bg_light:I = 0x7f080c9b

.field public static final miuix_appcompat_edit_text_search_clear_btn_dark:I = 0x7f080c9c

.field public static final miuix_appcompat_edit_text_search_clear_btn_light:I = 0x7f080c9d

.field public static final miuix_appcompat_edit_text_search_clear_btn_on_dark:I = 0x7f080c9e

.field public static final miuix_appcompat_edit_text_search_clear_btn_on_light:I = 0x7f080c9f

.field public static final miuix_appcompat_edit_text_search_dark:I = 0x7f080ca0

.field public static final miuix_appcompat_edit_text_search_light:I = 0x7f080ca1

.field public static final miuix_appcompat_edit_text_search_normal_dark:I = 0x7f080ca2

.field public static final miuix_appcompat_edit_text_search_normal_light:I = 0x7f080ca3

.field public static final miuix_appcompat_expander_close_dark:I = 0x7f080ca4

.field public static final miuix_appcompat_expander_close_light:I = 0x7f080ca5

.field public static final miuix_appcompat_expander_close_normal_dark:I = 0x7f080ca6

.field public static final miuix_appcompat_expander_close_normal_light:I = 0x7f080ca7

.field public static final miuix_appcompat_expander_close_pressed_dark:I = 0x7f080ca8

.field public static final miuix_appcompat_expander_close_pressed_light:I = 0x7f080ca9

.field public static final miuix_appcompat_expander_open_dark:I = 0x7f080caa

.field public static final miuix_appcompat_expander_open_light:I = 0x7f080cab

.field public static final miuix_appcompat_expander_open_normal_dark:I = 0x7f080cac

.field public static final miuix_appcompat_expander_open_normal_light:I = 0x7f080cad

.field public static final miuix_appcompat_expander_open_pressed_dark:I = 0x7f080cae

.field public static final miuix_appcompat_expander_open_pressed_light:I = 0x7f080caf

.field public static final miuix_appcompat_fab_empty_holder:I = 0x7f080cb0

.field public static final miuix_appcompat_filter_sort_hover_bg:I = 0x7f080cb1

.field public static final miuix_appcompat_filter_sort_tab_view_bg_dark:I = 0x7f080cb2

.field public static final miuix_appcompat_filter_sort_tab_view_bg_light:I = 0x7f080cb3

.field public static final miuix_appcompat_filter_sort_tab_view_bg_normal:I = 0x7f080cb4

.field public static final miuix_appcompat_filter_sort_view_bg_dark:I = 0x7f080cb5

.field public static final miuix_appcompat_filter_sort_view_bg_light:I = 0x7f080cb6

.field public static final miuix_appcompat_floating_action_bar_split_bg_dark:I = 0x7f080cb7

.field public static final miuix_appcompat_floating_action_bar_split_bg_expanded_dark:I = 0x7f080cb8

.field public static final miuix_appcompat_floating_action_bar_split_bg_expanded_light:I = 0x7f080cb9

.field public static final miuix_appcompat_floating_action_bar_split_bg_light:I = 0x7f080cba

.field public static final miuix_appcompat_floating_action_mode_bg_dark:I = 0x7f080cbb

.field public static final miuix_appcompat_floating_action_mode_bg_light:I = 0x7f080cbc

.field public static final miuix_appcompat_floating_list_menu_bg_dark:I = 0x7f080cbd

.field public static final miuix_appcompat_floating_list_menu_bg_light:I = 0x7f080cbe

.field public static final miuix_appcompat_floating_window_bg_dark:I = 0x7f080cbf

.field public static final miuix_appcompat_floating_window_bg_light:I = 0x7f080cc0

.field public static final miuix_appcompat_ic_edit_text_search:I = 0x7f080cc1

.field public static final miuix_appcompat_ic_edit_text_search_dark:I = 0x7f080cc2

.field public static final miuix_appcompat_ic_edit_text_search_dark_with_padding:I = 0x7f080cc3

.field public static final miuix_appcompat_ic_edit_text_search_with_padding:I = 0x7f080cc4

.field public static final miuix_appcompat_ic_message_view_close_error_light:I = 0x7f080cc5

.field public static final miuix_appcompat_ic_message_view_close_guide_light:I = 0x7f080cc6

.field public static final miuix_appcompat_ic_message_view_close_warning_light:I = 0x7f080cc7

.field public static final miuix_appcompat_ic_visibility_selector_dark:I = 0x7f080cc8

.field public static final miuix_appcompat_ic_visibility_selector_light:I = 0x7f080cc9

.field public static final miuix_appcompat_icon_favorite_dark:I = 0x7f080cca

.field public static final miuix_appcompat_icon_favorite_light:I = 0x7f080ccb

.field public static final miuix_appcompat_icon_favorite_normal_dark:I = 0x7f080ccc

.field public static final miuix_appcompat_icon_favorite_normal_light:I = 0x7f080ccd

.field public static final miuix_appcompat_icon_favorite_pressed_dark:I = 0x7f080cce

.field public static final miuix_appcompat_icon_favorite_pressed_light:I = 0x7f080ccf

.field public static final miuix_appcompat_icon_info_dark:I = 0x7f080cd0

.field public static final miuix_appcompat_icon_info_disable_dark:I = 0x7f080cd1

.field public static final miuix_appcompat_icon_info_disable_light:I = 0x7f080cd2

.field public static final miuix_appcompat_icon_info_light:I = 0x7f080cd3

.field public static final miuix_appcompat_icon_info_normal_dark:I = 0x7f080cd4

.field public static final miuix_appcompat_icon_info_normal_light:I = 0x7f080cd5

.field public static final miuix_appcompat_icon_personal_dark:I = 0x7f080cd6

.field public static final miuix_appcompat_icon_personal_disable_dark:I = 0x7f080cd7

.field public static final miuix_appcompat_icon_personal_disable_light:I = 0x7f080cd8

.field public static final miuix_appcompat_icon_personal_light:I = 0x7f080cd9

.field public static final miuix_appcompat_icon_personal_normal_dark:I = 0x7f080cda

.field public static final miuix_appcompat_icon_personal_normal_light:I = 0x7f080cdb

.field public static final miuix_appcompat_icon_search_dark:I = 0x7f080cdc

.field public static final miuix_appcompat_icon_search_disable_dark:I = 0x7f080cdd

.field public static final miuix_appcompat_icon_search_disable_light:I = 0x7f080cde

.field public static final miuix_appcompat_icon_search_light:I = 0x7f080cdf

.field public static final miuix_appcompat_icon_search_normal_dark:I = 0x7f080ce0

.field public static final miuix_appcompat_icon_search_normal_light:I = 0x7f080ce1

.field public static final miuix_appcompat_icon_settings_dark:I = 0x7f080ce2

.field public static final miuix_appcompat_icon_settings_disable_dark:I = 0x7f080ce3

.field public static final miuix_appcompat_icon_settings_disable_light:I = 0x7f080ce4

.field public static final miuix_appcompat_icon_settings_light:I = 0x7f080ce5

.field public static final miuix_appcompat_icon_settings_normal_dark:I = 0x7f080ce6

.field public static final miuix_appcompat_icon_settings_normal_light:I = 0x7f080ce7

.field public static final miuix_appcompat_immersion_item_bg_dark:I = 0x7f080ce8

.field public static final miuix_appcompat_immersion_item_bg_first_dark:I = 0x7f080ce9

.field public static final miuix_appcompat_immersion_item_bg_first_light:I = 0x7f080cea

.field public static final miuix_appcompat_immersion_item_bg_last_dark:I = 0x7f080ceb

.field public static final miuix_appcompat_immersion_item_bg_last_light:I = 0x7f080cec

.field public static final miuix_appcompat_immersion_item_bg_light:I = 0x7f080ced

.field public static final miuix_appcompat_immersion_item_bg_middle_dark:I = 0x7f080cee

.field public static final miuix_appcompat_immersion_item_bg_middle_light:I = 0x7f080cef

.field public static final miuix_appcompat_immersion_item_bg_single_dark:I = 0x7f080cf0

.field public static final miuix_appcompat_immersion_item_bg_single_light:I = 0x7f080cf1

.field public static final miuix_appcompat_immersion_window_bg_dark:I = 0x7f080cf2

.field public static final miuix_appcompat_immersion_window_bg_light:I = 0x7f080cf3

.field public static final miuix_appcompat_list_item_bg_dark:I = 0x7f080cf4

.field public static final miuix_appcompat_list_item_bg_dialog_dark:I = 0x7f080cf5

.field public static final miuix_appcompat_list_item_bg_dialog_light:I = 0x7f080cf6

.field public static final miuix_appcompat_list_item_bg_dropdown_popup_dark:I = 0x7f080cf7

.field public static final miuix_appcompat_list_item_bg_dropdown_popup_first_dark:I = 0x7f080cf8

.field public static final miuix_appcompat_list_item_bg_dropdown_popup_first_light:I = 0x7f080cf9

.field public static final miuix_appcompat_list_item_bg_dropdown_popup_last_dark:I = 0x7f080cfa

.field public static final miuix_appcompat_list_item_bg_dropdown_popup_last_light:I = 0x7f080cfb

.field public static final miuix_appcompat_list_item_bg_dropdown_popup_light:I = 0x7f080cfc

.field public static final miuix_appcompat_list_item_bg_dropdown_popup_middle_dark:I = 0x7f080cfd

.field public static final miuix_appcompat_list_item_bg_dropdown_popup_middle_light:I = 0x7f080cfe

.field public static final miuix_appcompat_list_item_bg_dropdown_popup_single_dark:I = 0x7f080cff

.field public static final miuix_appcompat_list_item_bg_dropdown_popup_single_light:I = 0x7f080d00

.field public static final miuix_appcompat_list_item_bg_light:I = 0x7f080d01

.field public static final miuix_appcompat_list_item_bg_multichoice_dark:I = 0x7f080d02

.field public static final miuix_appcompat_list_item_bg_multichoice_light:I = 0x7f080d03

.field public static final miuix_appcompat_list_item_bg_normal_dark:I = 0x7f080d04

.field public static final miuix_appcompat_list_item_bg_normal_light:I = 0x7f080d05

.field public static final miuix_appcompat_list_item_bg_pressed_dark:I = 0x7f080d06

.field public static final miuix_appcompat_list_item_bg_pressed_light:I = 0x7f080d07

.field public static final miuix_appcompat_list_item_bg_selected_dark:I = 0x7f080d08

.field public static final miuix_appcompat_list_item_bg_selected_light:I = 0x7f080d09

.field public static final miuix_appcompat_list_menu_bg_dark:I = 0x7f080d0a

.field public static final miuix_appcompat_list_menu_bg_light:I = 0x7f080d0b

.field public static final miuix_appcompat_list_menu_item_bg_dark:I = 0x7f080d0c

.field public static final miuix_appcompat_list_menu_item_bg_light:I = 0x7f080d0d

.field public static final miuix_appcompat_list_popup_first_item_bg_normal_dark:I = 0x7f080d0e

.field public static final miuix_appcompat_list_popup_first_item_bg_normal_light:I = 0x7f080d0f

.field public static final miuix_appcompat_list_popup_first_item_bg_pressed_dark:I = 0x7f080d10

.field public static final miuix_appcompat_list_popup_first_item_bg_pressed_light:I = 0x7f080d11

.field public static final miuix_appcompat_list_popup_item_bg_dark:I = 0x7f080d12

.field public static final miuix_appcompat_list_popup_item_bg_light:I = 0x7f080d13

.field public static final miuix_appcompat_list_popup_last_item_bg_normal_dark:I = 0x7f080d14

.field public static final miuix_appcompat_list_popup_last_item_bg_normal_light:I = 0x7f080d15

.field public static final miuix_appcompat_list_popup_last_item_bg_pressed_dark:I = 0x7f080d16

.field public static final miuix_appcompat_list_popup_last_item_bg_pressed_light:I = 0x7f080d17

.field public static final miuix_appcompat_list_popup_middle_item_bg_normal_dark:I = 0x7f080d18

.field public static final miuix_appcompat_list_popup_middle_item_bg_normal_light:I = 0x7f080d19

.field public static final miuix_appcompat_list_popup_middle_item_bg_pressed_dark:I = 0x7f080d1a

.field public static final miuix_appcompat_list_popup_middle_item_bg_pressed_light:I = 0x7f080d1b

.field public static final miuix_appcompat_list_popup_single_item_bg_normal_dark:I = 0x7f080d1c

.field public static final miuix_appcompat_list_popup_single_item_bg_normal_light:I = 0x7f080d1d

.field public static final miuix_appcompat_list_popup_single_item_bg_pressed_dark:I = 0x7f080d1e

.field public static final miuix_appcompat_list_popup_single_item_bg_pressed_light:I = 0x7f080d1f

.field public static final miuix_appcompat_list_small_item_bg_light:I = 0x7f080d20

.field public static final miuix_appcompat_list_small_item_first_bg_normal_light:I = 0x7f080d21

.field public static final miuix_appcompat_list_small_item_first_bg_pressed_light:I = 0x7f080d22

.field public static final miuix_appcompat_list_small_item_last_bg_normal_light:I = 0x7f080d23

.field public static final miuix_appcompat_list_small_item_last_bg_pressed_light:I = 0x7f080d24

.field public static final miuix_appcompat_list_small_item_middle_bg_normal_light:I = 0x7f080d25

.field public static final miuix_appcompat_list_small_item_middle_bg_pressed_light:I = 0x7f080d26

.field public static final miuix_appcompat_list_small_item_single_bg_normal_light:I = 0x7f080d27

.field public static final miuix_appcompat_list_small_item_single_bg_pressed_light:I = 0x7f080d28

.field public static final miuix_appcompat_list_view_item_group_header_bg_dark:I = 0x7f080d29

.field public static final miuix_appcompat_list_view_item_group_header_bg_light:I = 0x7f080d2a

.field public static final miuix_appcompat_message_view_bg_error_dark:I = 0x7f080d2b

.field public static final miuix_appcompat_message_view_bg_error_light:I = 0x7f080d2c

.field public static final miuix_appcompat_message_view_bg_guide_dark:I = 0x7f080d2d

.field public static final miuix_appcompat_message_view_bg_guide_light:I = 0x7f080d2e

.field public static final miuix_appcompat_message_view_bg_warning_dark:I = 0x7f080d2f

.field public static final miuix_appcompat_message_view_bg_warning_light:I = 0x7f080d30

.field public static final miuix_appcompat_number_picker_bg:I = 0x7f080d31

.field public static final miuix_appcompat_number_picker_bg_dark:I = 0x7f080d32

.field public static final miuix_appcompat_number_picker_bg_normal:I = 0x7f080d33

.field public static final miuix_appcompat_number_picker_bg_normal_dark:I = 0x7f080d34

.field public static final miuix_appcompat_number_picker_big_bg:I = 0x7f080d35

.field public static final miuix_appcompat_number_picker_big_bg_dark:I = 0x7f080d36

.field public static final miuix_appcompat_number_picker_big_bg_normal:I = 0x7f080d37

.field public static final miuix_appcompat_number_picker_big_bg_normal_dark:I = 0x7f080d38

.field public static final miuix_appcompat_popup_mask_1:I = 0x7f080d39

.field public static final miuix_appcompat_popup_mask_2:I = 0x7f080d3a

.field public static final miuix_appcompat_popup_mask_3:I = 0x7f080d3b

.field public static final miuix_appcompat_popup_mask_4:I = 0x7f080d3c

.field public static final miuix_appcompat_progressbar_horizontal_bg_dark:I = 0x7f080d3d

.field public static final miuix_appcompat_progressbar_horizontal_bg_light:I = 0x7f080d3e

.field public static final miuix_appcompat_progressbar_horizontal_dark:I = 0x7f080d3f

.field public static final miuix_appcompat_progressbar_horizontal_light:I = 0x7f080d40

.field public static final miuix_appcompat_progressbar_horizontal_primary_dark:I = 0x7f080d41

.field public static final miuix_appcompat_progressbar_horizontal_primary_light:I = 0x7f080d42

.field public static final miuix_appcompat_progressbar_indeterminate_bg_dark:I = 0x7f080d43

.field public static final miuix_appcompat_progressbar_indeterminate_bg_light:I = 0x7f080d44

.field public static final miuix_appcompat_progressbar_indeterminate_circle_dark:I = 0x7f080d45

.field public static final miuix_appcompat_progressbar_indeterminate_circle_light:I = 0x7f080d46

.field public static final miuix_appcompat_progressbar_indeterminate_dark:I = 0x7f080d47

.field public static final miuix_appcompat_progressbar_indeterminate_light:I = 0x7f080d48

.field public static final miuix_appcompat_ratingbar:I = 0x7f080d49

.field public static final miuix_appcompat_ratingbar_indicator:I = 0x7f080d4a

.field public static final miuix_appcompat_ratingbar_indicator_large:I = 0x7f080d4b

.field public static final miuix_appcompat_ratingbar_indicator_small:I = 0x7f080d4c

.field public static final miuix_appcompat_ratingbar_star_large_off:I = 0x7f080d4d

.field public static final miuix_appcompat_ratingbar_star_large_on:I = 0x7f080d4e

.field public static final miuix_appcompat_ratingbar_star_off:I = 0x7f080d4f

.field public static final miuix_appcompat_ratingbar_star_on:I = 0x7f080d50

.field public static final miuix_appcompat_ratingbar_star_small_off:I = 0x7f080d51

.field public static final miuix_appcompat_ratingbar_star_small_on:I = 0x7f080d52

.field public static final miuix_appcompat_scrollbar_thumb_vertical_dark:I = 0x7f080d53

.field public static final miuix_appcompat_scrollbar_thumb_vertical_light:I = 0x7f080d54

.field public static final miuix_appcompat_search_mode_bg_dark:I = 0x7f080d55

.field public static final miuix_appcompat_search_mode_bg_light:I = 0x7f080d56

.field public static final miuix_appcompat_search_mode_edit_text_bg_dark:I = 0x7f080d57

.field public static final miuix_appcompat_search_mode_edit_text_bg_light:I = 0x7f080d58

.field public static final miuix_appcompat_search_mode_input_bg_dark:I = 0x7f080d59

.field public static final miuix_appcompat_search_mode_input_bg_light:I = 0x7f080d5a

.field public static final miuix_appcompat_search_mode_trans_bg:I = 0x7f080d5b

.field public static final miuix_appcompat_seekbar_progress_bg_dark:I = 0x7f080d5c

.field public static final miuix_appcompat_seekbar_progress_bg_light:I = 0x7f080d5d

.field public static final miuix_appcompat_seekbar_progress_dark:I = 0x7f080d5e

.field public static final miuix_appcompat_seekbar_progress_light:I = 0x7f080d5f

.field public static final miuix_appcompat_settings_window_bg_dark:I = 0x7f080d60

.field public static final miuix_appcompat_settings_window_bg_light:I = 0x7f080d61

.field public static final miuix_appcompat_sliding_btn_bar_off_dark:I = 0x7f080d62

.field public static final miuix_appcompat_sliding_btn_bar_off_light:I = 0x7f080d63

.field public static final miuix_appcompat_sliding_btn_bar_on_dark:I = 0x7f080d64

.field public static final miuix_appcompat_sliding_btn_bar_on_light:I = 0x7f080d65

.field public static final miuix_appcompat_sliding_btn_bg_dark:I = 0x7f080d66

.field public static final miuix_appcompat_sliding_btn_bg_light:I = 0x7f080d67

.field public static final miuix_appcompat_sliding_btn_frame_dark:I = 0x7f080d68

.field public static final miuix_appcompat_sliding_btn_frame_light:I = 0x7f080d69

.field public static final miuix_appcompat_sliding_btn_slider_off_dark:I = 0x7f080d6a

.field public static final miuix_appcompat_sliding_btn_slider_off_light:I = 0x7f080d6b

.field public static final miuix_appcompat_sliding_btn_slider_off_normal_dark:I = 0x7f080d6c

.field public static final miuix_appcompat_sliding_btn_slider_off_normal_light:I = 0x7f080d6d

.field public static final miuix_appcompat_sliding_btn_slider_off_pressed_dark:I = 0x7f080d6e

.field public static final miuix_appcompat_sliding_btn_slider_off_pressed_light:I = 0x7f080d6f

.field public static final miuix_appcompat_sliding_btn_slider_on_dark:I = 0x7f080d70

.field public static final miuix_appcompat_sliding_btn_slider_on_light:I = 0x7f080d71

.field public static final miuix_appcompat_sliding_btn_slider_on_normal_dark:I = 0x7f080d72

.field public static final miuix_appcompat_sliding_btn_slider_on_normal_light:I = 0x7f080d73

.field public static final miuix_appcompat_sliding_btn_slider_on_pressed_dark:I = 0x7f080d74

.field public static final miuix_appcompat_sliding_btn_slider_on_pressed_light:I = 0x7f080d75

.field public static final miuix_appcompat_sliding_btn_slider_shadow:I = 0x7f080d76

.field public static final miuix_appcompat_sliding_btn_slider_stroke_light:I = 0x7f080d77

.field public static final miuix_appcompat_spinner_bg:I = 0x7f080d78

.field public static final miuix_appcompat_spinner_bg_dark:I = 0x7f080d79

.field public static final miuix_appcompat_spinner_popup_item_bg_dark:I = 0x7f080d7a

.field public static final miuix_appcompat_spinner_popup_item_bg_light:I = 0x7f080d7b

.field public static final miuix_appcompat_text_cursor_dark:I = 0x7f080d7c

.field public static final miuix_appcompat_text_cursor_light:I = 0x7f080d7d

.field public static final miuix_appcompat_text_select_handle_left:I = 0x7f080d7e

.field public static final miuix_appcompat_text_select_handle_left_dark:I = 0x7f080d7f

.field public static final miuix_appcompat_text_select_handle_left_light:I = 0x7f080d80

.field public static final miuix_appcompat_text_select_handle_left_mtrl_alpha:I = 0x7f080d81

.field public static final miuix_appcompat_text_select_handle_middle:I = 0x7f080d82

.field public static final miuix_appcompat_text_select_handle_middle_dark:I = 0x7f080d83

.field public static final miuix_appcompat_text_select_handle_middle_light:I = 0x7f080d84

.field public static final miuix_appcompat_text_select_handle_middle_mtrl_alpha:I = 0x7f080d85

.field public static final miuix_appcompat_text_select_handle_right:I = 0x7f080d86

.field public static final miuix_appcompat_text_select_handle_right_dark:I = 0x7f080d87

.field public static final miuix_appcompat_text_select_handle_right_light:I = 0x7f080d88

.field public static final miuix_appcompat_text_select_handle_right_mtrl_alpha:I = 0x7f080d89

.field public static final miuix_appcompat_window_bg:I = 0x7f080d8a

.field public static final miuix_appcompat_window_bg_dark:I = 0x7f080d8b

.field public static final miuix_appcompat_window_bg_light:I = 0x7f080d8c

.field public static final miuix_appcompat_window_bg_navigation_dark:I = 0x7f080d8d

.field public static final miuix_appcompat_window_bg_navigation_light:I = 0x7f080d8e

.field public static final miuix_appcompat_window_bg_secondary_dark:I = 0x7f080d8f

.field public static final miuix_appcompat_window_bg_secondary_light:I = 0x7f080d90

.field public static final miuix_appcompat_window_content_mask_navigation:I = 0x7f080d91

.field public static final miuix_appcompat_window_content_mask_oled:I = 0x7f080d92

.field public static final miuix_appcompat_window_dialog_bg_dark:I = 0x7f080d93

.field public static final miuix_appcompat_window_dialog_bg_light:I = 0x7f080d94

.field public static final miuix_appcompat_window_search_mask:I = 0x7f080d95

.field public static final miuix_appcompat_window_search_mask_navigation:I = 0x7f080d96

.field public static final miuix_ic_omit:I = 0x7f080d97

.field public static final miuix_ic_omit_selected:I = 0x7f080d98

.field public static final miuix_ic_preference_connected_bg:I = 0x7f080d99

.field public static final miuix_ic_preference_disconnected_bg:I = 0x7f080d9a

.field public static final miuix_preference_btn_radio_arrow:I = 0x7f080d9b

.field public static final miuix_preference_btn_radio_arrow_off:I = 0x7f080d9c

.field public static final miuix_preference_btn_radio_arrow_on:I = 0x7f080d9d

.field public static final miuix_preference_category_background_dark:I = 0x7f080d9e

.field public static final miuix_preference_category_background_dark_0:I = 0x7f080d9f

.field public static final miuix_preference_category_background_dark_1:I = 0x7f080da0

.field public static final miuix_preference_category_background_dark_2:I = 0x7f080da1

.field public static final miuix_preference_category_background_light:I = 0x7f080da2

.field public static final miuix_preference_category_background_light_0:I = 0x7f080da3

.field public static final miuix_preference_category_background_light_1:I = 0x7f080da4

.field public static final miuix_preference_category_background_light_2:I = 0x7f080da5

.field public static final miuix_preference_category_bg_dark:I = 0x7f080da6

.field public static final miuix_preference_category_bg_first_dark:I = 0x7f080da7

.field public static final miuix_preference_category_bg_first_light:I = 0x7f080da8

.field public static final miuix_preference_category_bg_first_no_title_dark:I = 0x7f080da9

.field public static final miuix_preference_category_bg_first_no_title_light:I = 0x7f080daa

.field public static final miuix_preference_category_bg_light:I = 0x7f080dab

.field public static final miuix_preference_category_bg_no_title_dark:I = 0x7f080dac

.field public static final miuix_preference_category_bg_no_title_light:I = 0x7f080dad

.field public static final miuix_preference_first_item_bg_dark:I = 0x7f080dae

.field public static final miuix_preference_first_item_bg_dark_normal:I = 0x7f080daf

.field public static final miuix_preference_first_item_bg_light:I = 0x7f080db0

.field public static final miuix_preference_first_item_bg_light_normal:I = 0x7f080db1

.field public static final miuix_preference_first_item_radio_bg_dark:I = 0x7f080db2

.field public static final miuix_preference_first_item_radio_bg_dark_normal:I = 0x7f080db3

.field public static final miuix_preference_first_item_radio_bg_light:I = 0x7f080db4

.field public static final miuix_preference_first_item_radio_bg_light_normal:I = 0x7f080db5

.field public static final miuix_preference_first_item_radio_two_state_bg:I = 0x7f080db6

.field public static final miuix_preference_first_item_radio_two_state_bg_normal:I = 0x7f080db7

.field public static final miuix_preference_first_item_radio_two_state_bg_pressed:I = 0x7f080db8

.field public static final miuix_preference_first_last_divider_line_dark:I = 0x7f080db9

.field public static final miuix_preference_first_last_divider_line_light:I = 0x7f080dba

.field public static final miuix_preference_ic_bg_connect:I = 0x7f080dbb

.field public static final miuix_preference_ic_connecting_translation:I = 0x7f080dbc

.field public static final miuix_preference_ic_detail_connected:I = 0x7f080dbd

.field public static final miuix_preference_ic_detail_disconnected_dark:I = 0x7f080dbe

.field public static final miuix_preference_ic_detail_disconnected_light:I = 0x7f080dbf

.field public static final miuix_preference_item_background_dark:I = 0x7f080dc0

.field public static final miuix_preference_item_background_light:I = 0x7f080dc1

.field public static final miuix_preference_item_radio_background_dark:I = 0x7f080dc2

.field public static final miuix_preference_item_radio_background_light:I = 0x7f080dc3

.field public static final miuix_preference_item_radio_two_state_background:I = 0x7f080dc4

.field public static final miuix_preference_item_radio_two_state_background_0:I = 0x7f080dc5

.field public static final miuix_preference_item_radio_two_state_background_1:I = 0x7f080dc6

.field public static final miuix_preference_item_radio_two_state_background_2:I = 0x7f080dc7

.field public static final miuix_preference_item_seekbar_background:I = 0x7f080dc8

.field public static final miuix_preference_last_item_bg_dark:I = 0x7f080dc9

.field public static final miuix_preference_last_item_bg_dark_normal:I = 0x7f080dca

.field public static final miuix_preference_last_item_bg_light:I = 0x7f080dcb

.field public static final miuix_preference_last_item_bg_light_normal:I = 0x7f080dcc

.field public static final miuix_preference_last_item_radio_bg_dark:I = 0x7f080dcd

.field public static final miuix_preference_last_item_radio_bg_dark_normal:I = 0x7f080dce

.field public static final miuix_preference_last_item_radio_bg_light:I = 0x7f080dcf

.field public static final miuix_preference_last_item_radio_bg_light_normal:I = 0x7f080dd0

.field public static final miuix_preference_last_item_radio_two_state_bg:I = 0x7f080dd1

.field public static final miuix_preference_last_item_radio_two_state_bg_normal:I = 0x7f080dd2

.field public static final miuix_preference_last_item_radio_two_state_bg_pressed:I = 0x7f080dd3

.field public static final miuix_preference_middle_item_bg_dark:I = 0x7f080dd4

.field public static final miuix_preference_middle_item_bg_dark_normal:I = 0x7f080dd5

.field public static final miuix_preference_middle_item_bg_light:I = 0x7f080dd6

.field public static final miuix_preference_middle_item_bg_light_normal:I = 0x7f080dd7

.field public static final miuix_preference_middle_item_radio_bg_dark:I = 0x7f080dd8

.field public static final miuix_preference_middle_item_radio_bg_dark_normal:I = 0x7f080dd9

.field public static final miuix_preference_middle_item_radio_bg_light:I = 0x7f080dda

.field public static final miuix_preference_middle_item_radio_bg_light_normal:I = 0x7f080ddb

.field public static final miuix_preference_middle_item_radio_two_state_bg:I = 0x7f080ddc

.field public static final miuix_preference_middle_item_radio_two_state_bg_normal:I = 0x7f080ddd

.field public static final miuix_preference_middle_item_radio_two_state_bg_pressed:I = 0x7f080dde

.field public static final miuix_preference_navigation_item_background:I = 0x7f080ddf

.field public static final miuix_preference_navigation_item_bg:I = 0x7f080de0

.field public static final miuix_preference_navigation_item_bg_normal:I = 0x7f080de1

.field public static final miuix_preference_navigation_item_bg_pressed:I = 0x7f080de2

.field public static final miuix_preference_navigation_item_bg_selected:I = 0x7f080de3

.field public static final miuix_preference_navigation_item_first_bg:I = 0x7f080de4

.field public static final miuix_preference_navigation_item_last_bg:I = 0x7f080de5

.field public static final miuix_preference_navigation_item_middle_bg:I = 0x7f080de6

.field public static final miuix_preference_seekbar_item_bg:I = 0x7f080de7

.field public static final miuix_preference_seekbar_item_bg_normal:I = 0x7f080de8

.field public static final miuix_preference_seekbar_item_first_bg:I = 0x7f080de9

.field public static final miuix_preference_seekbar_item_last_bg:I = 0x7f080dea

.field public static final miuix_preference_seekbar_item_middle_bg:I = 0x7f080deb

.field public static final miuix_preference_single_item_bg_dark:I = 0x7f080dec

.field public static final miuix_preference_single_item_bg_dark_normal:I = 0x7f080ded

.field public static final miuix_preference_single_item_bg_light:I = 0x7f080dee

.field public static final miuix_preference_single_item_bg_light_normal:I = 0x7f080def

.field public static final miuix_preference_single_item_radio_bg_dark:I = 0x7f080df0

.field public static final miuix_preference_single_item_radio_bg_dark_normal:I = 0x7f080df1

.field public static final miuix_preference_single_item_radio_bg_light:I = 0x7f080df2

.field public static final miuix_preference_single_item_radio_bg_light_normal:I = 0x7f080df3

.field public static final miuix_preference_single_item_radio_two_state_bg:I = 0x7f080df4

.field public static final miuix_preference_single_item_radio_two_state_bg_normal:I = 0x7f080df5

.field public static final miuix_preference_single_item_radio_two_state_bg_pressed:I = 0x7f080df6

.field public static final miuix_sbl_loading_progress_bg_light:I = 0x7f080df7

.field public static final miuix_sbl_loading_progress_circle_light:I = 0x7f080df8

.field public static final miuix_sbl_loading_progress_circle_light_svg:I = 0x7f080df9

.field public static final miuix_sbl_simple_indicator_locked_body_blue:I = 0x7f080dfa

.field public static final miuix_sbl_simple_indicator_locked_body_gray:I = 0x7f080dfb

.field public static final miuix_sbl_simple_indicator_locked_header_blue:I = 0x7f080dfc

.field public static final miuix_sbl_simple_indicator_locked_header_gray:I = 0x7f080dfd

.field public static final miuix_sbl_tracking_progress_circle_bg:I = 0x7f080dfe

.field public static final miuix_sbl_tracking_progress_ellipse_bg:I = 0x7f080dff

.field public static final miuix_stretchable_widget_state_collapse:I = 0x7f080e00

.field public static final miuix_stretchable_widget_state_expand:I = 0x7f080e01

.field public static final mobile_network_settings:I = 0x7f080e02

.field public static final moment_smalldot:I = 0x7f080e03

.field public static final mr_button_connected_dark:I = 0x7f080e04

.field public static final mr_button_connected_light:I = 0x7f080e05

.field public static final mr_button_connecting_dark:I = 0x7f080e06

.field public static final mr_button_connecting_light:I = 0x7f080e07

.field public static final mr_button_dark:I = 0x7f080e08

.field public static final mr_button_dark_static:I = 0x7f080e09

.field public static final mr_button_light:I = 0x7f080e0a

.field public static final mr_button_light_static:I = 0x7f080e0b

.field public static final mr_cast_checkbox:I = 0x7f080e0c

.field public static final mr_cast_group_seekbar_track:I = 0x7f080e0d

.field public static final mr_cast_mute_button:I = 0x7f080e0e

.field public static final mr_cast_route_seekbar_track:I = 0x7f080e0f

.field public static final mr_cast_stop:I = 0x7f080e10

.field public static final mr_cast_thumb:I = 0x7f080e11

.field public static final mr_dialog_close_dark:I = 0x7f080e12

.field public static final mr_dialog_close_light:I = 0x7f080e13

.field public static final mr_dialog_material_background_dark:I = 0x7f080e14

.field public static final mr_dialog_material_background_light:I = 0x7f080e15

.field public static final mr_group_collapse:I = 0x7f080e16

.field public static final mr_group_expand:I = 0x7f080e17

.field public static final mr_media_pause_dark:I = 0x7f080e18

.field public static final mr_media_pause_light:I = 0x7f080e19

.field public static final mr_media_play_dark:I = 0x7f080e1a

.field public static final mr_media_play_light:I = 0x7f080e1b

.field public static final mr_media_stop_dark:I = 0x7f080e1c

.field public static final mr_media_stop_light:I = 0x7f080e1d

.field public static final mr_vol_type_audiotrack_dark:I = 0x7f080e1e

.field public static final mr_vol_type_audiotrack_light:I = 0x7f080e1f

.field public static final msd_install_driver_download_icon_mac:I = 0x7f080e20

.field public static final msd_install_driver_download_icon_xp:I = 0x7f080e21

.field public static final msd_install_driver_magnifier:I = 0x7f080e22

.field public static final msd_install_driver_setup_xp:I = 0x7f080e23

.field public static final msd_install_error_dot:I = 0x7f080e24

.field public static final msd_pc_info:I = 0x7f080e25

.field public static final msg_bubble_incoming:I = 0x7f080e26

.field public static final msg_bubble_outgoing:I = 0x7f080e27

.field public static final mtrl_dialog_background:I = 0x7f080e28

.field public static final mtrl_dropdown_arrow:I = 0x7f080e29

.field public static final mtrl_ic_arrow_drop_down:I = 0x7f080e2a

.field public static final mtrl_ic_arrow_drop_up:I = 0x7f080e2b

.field public static final mtrl_ic_cancel:I = 0x7f080e2c

.field public static final mtrl_ic_error:I = 0x7f080e2d

.field public static final mtrl_navigation_bar_item_background:I = 0x7f080e2e

.field public static final mtrl_popupmenu_background:I = 0x7f080e2f

.field public static final mtrl_popupmenu_background_overlay:I = 0x7f080e30

.field public static final mtrl_tabs_default_indicator:I = 0x7f080e31

.field public static final multi_gradient_bg:I = 0x7f080e32

.field public static final multi_window_cvw:I = 0x7f080e33

.field public static final music_ic_headset_settings:I = 0x7f080e34

.field public static final mute_drawable:I = 0x7f080e35

.field public static final mute_icon_selector:I = 0x7f080e36

.field public static final mute_media_drawable:I = 0x7f080e37

.field public static final mute_media_icon_selector:I = 0x7f080e38

.field public static final mute_media_selected_drawable:I = 0x7f080e39

.field public static final mute_selected_drawable:I = 0x7f080e3a

.field public static final nav_divider:I = 0x7f080e3b

.field public static final navigation_bar_guide_app:I = 0x7f080e3c

.field public static final navigation_bar_guide_appswitch:I = 0x7f080e3d

.field public static final navigation_bar_guide_back:I = 0x7f080e3e

.field public static final navigation_bar_guide_home:I = 0x7f080e3f

.field public static final navigation_bar_guide_new_appswitch:I = 0x7f080e40

.field public static final navigation_bar_guide_new_appswitch_hide_gesture_line:I = 0x7f080e41

.field public static final navigation_bar_guide_recent:I = 0x7f080e42

.field public static final navigation_empty_icon:I = 0x7f080e43

.field public static final navigation_gesture_line_bg:I = 0x7f080e44

.field public static final navigation_picker_full_screen:I = 0x7f080e45

.field public static final navigation_picker_virtual_keys:I = 0x7f080e46

.field public static final new_fingerprint_cancel_black:I = 0x7f080e47

.field public static final new_fingerprint_cancel_white:I = 0x7f080e48

.field public static final new_fingerprint_finish:I = 0x7f080e49

.field public static final new_version_button:I = 0x7f080e4a

.field public static final nfc_detection_point:I = 0x7f080e4b

.field public static final nfc_how_it_works:I = 0x7f080e4c

.field public static final nfc_payment_empty_state:I = 0x7f080e4d

.field public static final no_action_selected_background_normal:I = 0x7f080e4e

.field public static final notch_style_show:I = 0x7f080e4f

.field public static final notch_style_status_bar_inside:I = 0x7f080e50

.field public static final notch_style_status_bar_outside:I = 0x7f080e51

.field public static final notification_action_background:I = 0x7f080e52

.field public static final notification_action_reboot_icon:I = 0x7f080e53

.field public static final notification_bg:I = 0x7f080e54

.field public static final notification_bg_low:I = 0x7f080e55

.field public static final notification_bg_low_normal:I = 0x7f080e56

.field public static final notification_bg_low_pressed:I = 0x7f080e57

.field public static final notification_bg_normal:I = 0x7f080e58

.field public static final notification_bg_normal_pressed:I = 0x7f080e59

.field public static final notification_icon_background:I = 0x7f080e5a

.field public static final notification_template_icon_bg:I = 0x7f080e5b

.field public static final notification_template_icon_low_bg:I = 0x7f080e5c

.field public static final notification_tile_bg:I = 0x7f080e5d

.field public static final notify_icon:I = 0x7f080e5e

.field public static final notify_panel_notification_icon_bg:I = 0x7f080e5f

.field public static final oaid_anonymous_icon:I = 0x7f080e60

.field public static final one_handed_guideline:I = 0x7f080e61

.field public static final openAnc_OFF:I = 0x7f080e62

.field public static final openAnc_ON:I = 0x7f080e63

.field public static final open_network_connect_notif:I = 0x7f080e64

.field public static final open_network_connect_notif_small:I = 0x7f080e65

.field public static final pad_left_frame_line_bg:I = 0x7f080e66

.field public static final pad_right_frame_line_bg:I = 0x7f080e67

.field public static final page_layout_seekpoint:I = 0x7f080e68

.field public static final page_layout_seekpoint_blue:I = 0x7f080e69

.field public static final page_layout_seekpoint_orange:I = 0x7f080e6a

.field public static final panel_bg:I = 0x7f080e6b

.field public static final paper_eyecare_image:I = 0x7f080e6c

.field public static final passport_progressbar_indeterminate_bg_light:I = 0x7f080e6d

.field public static final passport_progressbar_indeterminate_circle_light:I = 0x7f080e6e

.field public static final passport_verification_def_dialog_bg:I = 0x7f080e6f

.field public static final password_entry_bg:I = 0x7f080e70

.field public static final password_field_default:I = 0x7f080e71

.field public static final password_guard_icon:I = 0x7f080e72

.field public static final phone_led_color:I = 0x7f080e73

.field public static final phone_model:I = 0x7f080e74

.field public static final picker_selected_background_normal:I = 0x7f080e75

.field public static final points3_bg:I = 0x7f080e76

.field public static final popup_background:I = 0x7f080e77

.field public static final popup_background_view:I = 0x7f080e78

.field public static final popup_cangmen_up:I = 0x7f080e79

.field public static final popup_chilun_up:I = 0x7f080e7a

.field public static final popup_jijia_up:I = 0x7f080e7b

.field public static final popup_led_background_view:I = 0x7f080e7c

.field public static final popup_led_color1:I = 0x7f080e7d

.field public static final popup_led_color2:I = 0x7f080e7e

.field public static final popup_led_color3:I = 0x7f080e7f

.field public static final popup_led_color4:I = 0x7f080e80

.field public static final popup_led_color5:I = 0x7f080e81

.field public static final popup_mofa_up:I = 0x7f080e82

.field public static final popup_muqin_up:I = 0x7f080e83

.field public static final popup_yingyan_up:I = 0x7f080e84

.field public static final power_guide_bk_bottom:I = 0x7f080e85

.field public static final power_guide_bk_top:I = 0x7f080e86

.field public static final power_guide_force_off:I = 0x7f080e87

.field public static final power_guide_icon:I = 0x7f080e88

.field public static final power_guide_off:I = 0x7f080e89

.field public static final powerclick01:I = 0x7f080e8a

.field public static final powerclick02:I = 0x7f080e8b

.field public static final powerclick03:I = 0x7f080e8c

.field public static final powerclick04:I = 0x7f080e8d

.field public static final powerclick05:I = 0x7f080e8e

.field public static final powerclick06:I = 0x7f080e8f

.field public static final powerclick07:I = 0x7f080e90

.field public static final preference_background:I = 0x7f080e91

.field public static final preference_card_end_bg:I = 0x7f080e92

.field public static final preference_card_end_bg_rtl:I = 0x7f080e93

.field public static final preference_card_head_bg:I = 0x7f080e94

.field public static final preference_card_head_bg_rtl:I = 0x7f080e95

.field public static final preference_category_background_no_title:I = 0x7f080e96

.field public static final preference_divider_bg:I = 0x7f080e97

.field public static final preference_highlight:I = 0x7f080e98

.field public static final preference_image_click_bg:I = 0x7f080e99

.field public static final preference_list_divider_material:I = 0x7f080e9a

.field public static final preference_system_app_new_end_bg:I = 0x7f080e9b

.field public static final preference_system_app_new_end_bg_rtl:I = 0x7f080e9c

.field public static final preference_system_app_new_head_bg:I = 0x7f080e9d

.field public static final preference_system_app_new_head_bg_rtl:I = 0x7f080e9e

.field public static final preview:I = 0x7f080e9f

.field public static final preview_seek_bar_outline:I = 0x7f080ea0

.field public static final privacy_password_down:I = 0x7f080ea1

.field public static final privacy_password_file:I = 0x7f080ea2

.field public static final privacy_password_gallery:I = 0x7f080ea3

.field public static final privacy_password_mms:I = 0x7f080ea4

.field public static final privacy_password_notes:I = 0x7f080ea5

.field public static final privacypassword_back_icon:I = 0x7f080ea6

.field public static final privacypassword_setting_icon:I = 0x7f080ea7

.field public static final progress_card_shape_corner:I = 0x7f080ea8

.field public static final progress_indeterminate_horizontal_material_trimmed:I = 0x7f080ea9

.field public static final projection_characteristic_off_screen:I = 0x7f080eaa

.field public static final projection_characteristic_privacy_protect:I = 0x7f080eab

.field public static final projection_characteristic_small_window:I = 0x7f080eac

.field public static final projection_example_document:I = 0x7f080ead

.field public static final projection_example_gallery:I = 0x7f080eae

.field public static final projection_example_game:I = 0x7f080eaf

.field public static final protection_background:I = 0x7f080eb0

.field public static final provision_action_bar_back:I = 0x7f080eb1

.field public static final provision_action_bar_back_n:I = 0x7f080eb2

.field public static final provision_action_bar_back_p:I = 0x7f080eb3

.field public static final provision_arrow_back:I = 0x7f080eb4

.field public static final provision_arrow_next:I = 0x7f080eb5

.field public static final provision_arrow_next_disabled:I = 0x7f080eb6

.field public static final provision_arrow_right_n:I = 0x7f080eb7

.field public static final provision_arrow_right_p:I = 0x7f080eb8

.field public static final provision_bg:I = 0x7f080eb9

.field public static final provision_btn_back_color:I = 0x7f080eba

.field public static final provision_btn_background:I = 0x7f080ebb

.field public static final provision_btn_bg:I = 0x7f080ebc

.field public static final provision_btn_checkbox_checked_bg_light:I = 0x7f080ebd

.field public static final provision_btn_delete:I = 0x7f080ebe

.field public static final provision_btn_delete_n:I = 0x7f080ebf

.field public static final provision_btn_delete_p:I = 0x7f080ec0

.field public static final provision_btn_next:I = 0x7f080ec1

.field public static final provision_btn_next_color:I = 0x7f080ec2

.field public static final provision_btn_next_color_low_light:I = 0x7f080ec3

.field public static final provision_btn_next_empty:I = 0x7f080ec4

.field public static final provision_btn_next_n:I = 0x7f080ec5

.field public static final provision_btn_next_p:I = 0x7f080ec6

.field public static final provision_btn_next_skip:I = 0x7f080ec7

.field public static final provision_btn_radio:I = 0x7f080ec8

.field public static final provision_btn_radio_off:I = 0x7f080ec9

.field public static final provision_btn_radio_on:I = 0x7f080eca

.field public static final provision_btn_skip_color:I = 0x7f080ecb

.field public static final provision_checkbox:I = 0x7f080ecc

.field public static final provision_checkbox_off_d:I = 0x7f080ecd

.field public static final provision_checkbox_off_n:I = 0x7f080ece

.field public static final provision_checkbox_on_d:I = 0x7f080ecf

.field public static final provision_checkbox_on_n:I = 0x7f080ed0

.field public static final provision_free_wifi_indicator:I = 0x7f080ed1

.field public static final provision_ic_wifi_lock_signal_0:I = 0x7f080ed2

.field public static final provision_ic_wifi_lock_signal_1:I = 0x7f080ed3

.field public static final provision_ic_wifi_lock_signal_2:I = 0x7f080ed4

.field public static final provision_ic_wifi_lock_signal_3:I = 0x7f080ed5

.field public static final provision_ic_wifi_lock_signal_4:I = 0x7f080ed6

.field public static final provision_ic_wifi_signal_0:I = 0x7f080ed7

.field public static final provision_ic_wifi_signal_1:I = 0x7f080ed8

.field public static final provision_ic_wifi_signal_2:I = 0x7f080ed9

.field public static final provision_ic_wifi_signal_3:I = 0x7f080eda

.field public static final provision_ic_wifi_signal_4:I = 0x7f080edb

.field public static final provision_input_item_bg:I = 0x7f080edc

.field public static final provision_item_selection_bg:I = 0x7f080edd

.field public static final provision_item_selection_n:I = 0x7f080ede

.field public static final provision_item_selection_s:I = 0x7f080edf

.field public static final provision_list_divider:I = 0x7f080ee0

.field public static final provision_list_item_background:I = 0x7f080ee1

.field public static final provision_list_item_bg:I = 0x7f080ee2

.field public static final provision_list_item_corner_bg:I = 0x7f080ee3

.field public static final provision_list_item_first_bg_normal:I = 0x7f080ee4

.field public static final provision_list_item_first_bg_pressed:I = 0x7f080ee5

.field public static final provision_list_item_first_bg_selected:I = 0x7f080ee6

.field public static final provision_list_item_last_bg_light:I = 0x7f080ee7

.field public static final provision_list_item_last_bg_normal:I = 0x7f080ee8

.field public static final provision_list_item_last_bg_pressed:I = 0x7f080ee9

.field public static final provision_list_item_last_bg_selected:I = 0x7f080eea

.field public static final provision_list_item_middle_bg_light:I = 0x7f080eeb

.field public static final provision_list_item_middle_bg_normal:I = 0x7f080eec

.field public static final provision_list_item_middle_bg_normal_:I = 0x7f080eed

.field public static final provision_list_item_middle_bg_pressed:I = 0x7f080eee

.field public static final provision_list_item_middle_bg_selected:I = 0x7f080eef

.field public static final provision_list_item_single_bg_normal:I = 0x7f080ef0

.field public static final provision_list_item_single_bg_pressed:I = 0x7f080ef1

.field public static final provision_list_item_single_bg_selected:I = 0x7f080ef2

.field public static final provision_logo_wifi:I = 0x7f080ef3

.field public static final provision_logo_zonepicker:I = 0x7f080ef4

.field public static final provision_other_ap_list_item_background:I = 0x7f080ef5

.field public static final provision_other_ap_list_item_corner_bg:I = 0x7f080ef6

.field public static final provision_spinner_bg:I = 0x7f080ef7

.field public static final provision_spinner_bg_n:I = 0x7f080ef8

.field public static final provision_spinner_bg_p:I = 0x7f080ef9

.field public static final provision_wifi:I = 0x7f080efa

.field public static final qhd_image:I = 0x7f080efb

.field public static final quickly_open_camera:I = 0x7f080efc

.field public static final radio_button_check:I = 0x7f080efd

.field public static final radio_preference_background_corner:I = 0x7f080efe

.field public static final recommend_text_selector:I = 0x7f080eff

.field public static final red_dot:I = 0x7f080f00

.field public static final regulatory_info:I = 0x7f080f01

.field public static final right_rounded_ripple:I = 0x7f080f02

.field public static final ring_progress:I = 0x7f080f03

.field public static final ring_volume_icon:I = 0x7f080f04

.field public static final ringer_volume_settings:I = 0x7f080f05

.field public static final ringtone_settings:I = 0x7f080f06

.field public static final rounded_bg:I = 0x7f080f07

.field public static final rounded_bk:I = 0x7f080f08

.field public static final rounded_ripple:I = 0x7f080f09

.field public static final scan_output_01:I = 0x7f080f0a

.field public static final scan_output_02:I = 0x7f080f0b

.field public static final scan_output_03:I = 0x7f080f0c

.field public static final scan_output_04:I = 0x7f080f0d

.field public static final scan_output_05:I = 0x7f080f0e

.field public static final scan_output_06:I = 0x7f080f0f

.field public static final scan_output_07:I = 0x7f080f10

.field public static final scan_output_08:I = 0x7f080f11

.field public static final scan_output_09:I = 0x7f080f12

.field public static final scan_output_10:I = 0x7f080f13

.field public static final scan_output_11:I = 0x7f080f14

.field public static final scan_output_12:I = 0x7f080f15

.field public static final scan_output_13:I = 0x7f080f16

.field public static final scan_output_14:I = 0x7f080f17

.field public static final scan_output_15:I = 0x7f080f18

.field public static final scan_output_16:I = 0x7f080f19

.field public static final scan_output_17:I = 0x7f080f1a

.field public static final scan_output_18:I = 0x7f080f1b

.field public static final scan_output_19:I = 0x7f080f1c

.field public static final scan_output_20:I = 0x7f080f1d

.field public static final scan_output_background_black:I = 0x7f080f1e

.field public static final scan_output_background_white:I = 0x7f080f1f

.field public static final screen_color_degree:I = 0x7f080f20

.field public static final screen_color_preview:I = 0x7f080f21

.field public static final screen_enhance_engine_ai_image:I = 0x7f080f22

.field public static final screen_enhance_engine_view_count_circle:I = 0x7f080f23

.field public static final screen_resolution_1080p:I = 0x7f080f24

.field public static final screen_resolution_1440p:I = 0x7f080f25

.field public static final screen_timeout_settings:I = 0x7f080f26

.field public static final screen_view_arrow_left:I = 0x7f080f27

.field public static final screen_view_arrow_left_gray:I = 0x7f080f28

.field public static final screen_view_arrow_right:I = 0x7f080f29

.field public static final screen_view_arrow_right_gray:I = 0x7f080f2a

.field public static final screen_view_seek_point_highlight:I = 0x7f080f2b

.field public static final screen_view_seek_point_highlight_white:I = 0x7f080f2c

.field public static final screen_view_seek_point_normal:I = 0x7f080f2d

.field public static final screen_view_seek_point_normal_white:I = 0x7f080f2e

.field public static final screen_view_slide_bar:I = 0x7f080f2f

.field public static final screen_view_slide_bar_bg:I = 0x7f080f30

.field public static final search_bar_selected_background:I = 0x7f080f31

.field public static final search_history_item_bg:I = 0x7f080f32

.field public static final search_list_empty:I = 0x7f080f33

.field public static final security_input_num_order_icon:I = 0x7f080f34

.field public static final security_input_num_random_order_icon:I = 0x7f080f35

.field public static final seekbar_bg:I = 0x7f080f36

.field public static final seekbar_demo_bg:I = 0x7f080f37

.field public static final seekbar_selected_background_normal:I = 0x7f080f38

.field public static final selectable_card:I = 0x7f080f39

.field public static final selected:I = 0x7f080f3a

.field public static final selector_gxzw_anim_text_color:I = 0x7f080f3b

.field public static final separate_keyboard_fold:I = 0x7f080f3c

.field public static final separate_keyboard_pad:I = 0x7f080f3d

.field public static final settings_input_antenna:I = 0x7f080f3e

.field public static final settings_notify_bg:I = 0x7f080f3f

.field public static final settings_panel_background:I = 0x7f080f40

.field public static final settings_panel_rounded_top_corner_background:I = 0x7f080f41

.field public static final settingslib_arrow_drop_down:I = 0x7f080f42

.field public static final settingslib_btn_colored_material:I = 0x7f080f43

.field public static final settingslib_card_background:I = 0x7f080f44

.field public static final settingslib_ic_cross:I = 0x7f080f45

.field public static final settingslib_ic_info:I = 0x7f080f46

.field public static final settingslib_ic_info_outline_24:I = 0x7f080f47

.field public static final settingslib_progress_horizontal:I = 0x7f080f48

.field public static final settingslib_spinner_background:I = 0x7f080f49

.field public static final settingslib_spinner_dropdown_background:I = 0x7f080f4a

.field public static final settingslib_switch_bar_bg_disabled:I = 0x7f080f4b

.field public static final settingslib_switch_bar_bg_off:I = 0x7f080f4c

.field public static final settingslib_switch_bar_bg_on:I = 0x7f080f4d

.field public static final settingslib_switch_thumb:I = 0x7f080f4e

.field public static final settingslib_switch_track:I = 0x7f080f4f

.field public static final settingslib_tabs_background:I = 0x7f080f50

.field public static final settingslib_tabs_indicator_background:I = 0x7f080f51

.field public static final setup_list_item_background:I = 0x7f080f52

.field public static final setup_list_item_corner_bg:I = 0x7f080f53

.field public static final setup_new_fingerprint_cancel_black:I = 0x7f080f54

.field public static final setup_new_fingerprint_cancel_white:I = 0x7f080f55

.field public static final shape_corner:I = 0x7f080f56

.field public static final shape_corner_matrix:I = 0x7f080f57

.field public static final shape_gxzw_anim_preview_bg:I = 0x7f080f58

.field public static final shape_notification_button_normal:I = 0x7f080f59

.field public static final shortcut_accessibility:I = 0x7f080f5a

.field public static final shortcut_application:I = 0x7f080f5b

.field public static final shortcut_base:I = 0x7f080f5c

.field public static final shortcut_battery:I = 0x7f080f5d

.field public static final shortcut_bluetooth:I = 0x7f080f5e

.field public static final shortcut_display:I = 0x7f080f5f

.field public static final shortcut_location:I = 0x7f080f60

.field public static final shortcut_notification:I = 0x7f080f61

.field public static final shortcut_power_usage:I = 0x7f080f62

.field public static final shortcut_sound:I = 0x7f080f63

.field public static final shortcut_status_bar:I = 0x7f080f64

.field public static final shortcut_tether:I = 0x7f080f65

.field public static final shortcut_vpn:I = 0x7f080f66

.field public static final shortcut_wifi:I = 0x7f080f67

.field public static final shortcut_wifi_display:I = 0x7f080f68

.field public static final shortcut_zone:I = 0x7f080f69

.field public static final shoulder_key_shortcut_guide:I = 0x7f080f6a

.field public static final shoulder_key_shortcut_guide_poco:I = 0x7f080f6b

.field public static final shoulderkey_indicator_image:I = 0x7f080f6c

.field public static final shoulderkey_sound_bullet_image:I = 0x7f080f6d

.field public static final shoulderkey_sound_engine_image:I = 0x7f080f6e

.field public static final shoulderkey_sound_feature_image:I = 0x7f080f6f

.field public static final shoulderkey_sound_sword_image:I = 0x7f080f70

.field public static final signal_strength_1x:I = 0x7f080f71

.field public static final signal_strength_3g:I = 0x7f080f72

.field public static final signal_strength_5g:I = 0x7f080f73

.field public static final signal_strength_g:I = 0x7f080f74

.field public static final signal_strength_lte:I = 0x7f080f75

.field public static final silent_mode_settings:I = 0x7f080f76

.field public static final silent_zen_mode_button_bg:I = 0x7f080f77

.field public static final silent_zen_mode_selected_bg:I = 0x7f080f78

.field public static final sim_confirm_dialog_btn_outline:I = 0x7f080f79

.field public static final sim_confirm_dialog_rounded_bg:I = 0x7f080f7a

.field public static final sim_progress_dialog_rounded_bg:I = 0x7f080f7b

.field public static final sim_slot_1_icon:I = 0x7f080f7c

.field public static final sim_slot_2_icon:I = 0x7f080f7d

.field public static final slider_icon:I = 0x7f080f7e

.field public static final slider_online_sound_alpha:I = 0x7f080f7f

.field public static final slider_online_sound_bg:I = 0x7f080f80

.field public static final sliding_panel_visualization_bg:I = 0x7f080f81

.field public static final sliding_panel_visualization_dot_bar:I = 0x7f080f82

.field public static final sliding_panel_visualization_shadow_dot_bar:I = 0x7f080f83

.field public static final small_sim_icon_slot_1:I = 0x7f080f84

.field public static final small_sim_icon_slot_2:I = 0x7f080f85

.field public static final smartcover_lattice_normal:I = 0x7f080f86

.field public static final smartcover_lattice_selected:I = 0x7f080f87

.field public static final smartcover_normal_normal:I = 0x7f080f88

.field public static final smartcover_normal_selected:I = 0x7f080f89

.field public static final smartcover_small_window_normal:I = 0x7f080f8a

.field public static final smartcover_small_window_selected:I = 0x7f080f8b

.field public static final smartcover_transparent_normal:I = 0x7f080f8c

.field public static final smartcover_transparent_selected:I = 0x7f080f8d

.field public static final sms_received_settings:I = 0x7f080f8e

.field public static final sos_cancel_button:I = 0x7f080f8f

.field public static final sos_common_bg:I = 0x7f080f90

.field public static final sos_direction_back:I = 0x7f080f91

.field public static final sos_el_icon:I = 0x7f080f92

.field public static final sos_exit_dialog_el_bg:I = 0x7f080f93

.field public static final sos_location_bg:I = 0x7f080f94

.field public static final sos_normal_item_selector:I = 0x7f080f95

.field public static final sos_notification_icon:I = 0x7f080f96

.field public static final sos_player_pause:I = 0x7f080f97

.field public static final sos_player_play:I = 0x7f080f98

.field public static final sos_status_bar_view_bg:I = 0x7f080f99

.field public static final sos_wifi:I = 0x7f080f9a

.field public static final sound_seekbar_disable_style:I = 0x7f080f9b

.field public static final sound_seekbar_style:I = 0x7f080f9c

.field public static final sound_speaker_harman:I = 0x7f080f9d

.field public static final sound_speaker_icon:I = 0x7f080f9e

.field public static final spinner_bg_edit:I = 0x7f080f9f

.field public static final spinner_default_holo_dark_am_no_underline:I = 0x7f080fa0

.field public static final square_bk:I = 0x7f080fa1

.field public static final square_ripple:I = 0x7f080fa2

.field public static final stat_sys_airplane_mode:I = 0x7f080fa3

.field public static final stat_sys_phone_call:I = 0x7f080fa4

.field public static final stat_sys_sos:I = 0x7f080fa5

.field public static final status_bar_screen_key_back:I = 0x7f080fa6

.field public static final status_bar_screen_key_home:I = 0x7f080fa7

.field public static final status_bar_screen_key_menu:I = 0x7f080fa8

.field public static final status_bar_screen_key_recent_apps:I = 0x7f080fa9

.field public static final storage_button_style:I = 0x7f080faa

.field public static final storage_button_style_normal:I = 0x7f080fab

.field public static final storage_button_style_pressed:I = 0x7f080fac

.field public static final storage_progressbar_style:I = 0x7f080fad

.field public static final storage_wizard_sd_png:I = 0x7f080fae

.field public static final stylus_battery_bg:I = 0x7f080faf

.field public static final stylus_blue_point:I = 0x7f080fb0

.field public static final stylus_connecting_progress:I = 0x7f080fb1

.field public static final stylus_edit_show_bg:I = 0x7f080fb2

.field public static final stylus_function_arrow:I = 0x7f080fb3

.field public static final stylus_green_end:I = 0x7f080fb4

.field public static final stylus_green_middle:I = 0x7f080fb5

.field public static final stylus_green_start:I = 0x7f080fb6

.field public static final stylus_hand_writing:I = 0x7f080fb7

.field public static final stylus_ic_pencil_black:I = 0x7f080fb8

.field public static final stylus_instruction_bg:I = 0x7f080fb9

.field public static final stylus_ota_progress:I = 0x7f080fba

.field public static final stylus_ota_progress_bar:I = 0x7f080fbb

.field public static final stylus_ota_progress_bg:I = 0x7f080fbc

.field public static final stylus_pen_empty:I = 0x7f080fbd

.field public static final stylus_red_end:I = 0x7f080fbe

.field public static final stylus_red_middle:I = 0x7f080fbf

.field public static final stylus_red_start:I = 0x7f080fc0

.field public static final stylus_screen_shot:I = 0x7f080fc1

.field public static final stylus_shortcut_actions_icon:I = 0x7f080fc2

.field public static final stylus_show_icon:I = 0x7f080fc3

.field public static final stylus_update_bg:I = 0x7f080fc4

.field public static final sud_autofilled_highlight_mn_bg:I = 0x7f080fc5

.field public static final sud_card_bg:I = 0x7f080fc6

.field public static final sud_card_bg_dark:I = 0x7f080fc7

.field public static final sud_card_bg_light:I = 0x7f080fc8

.field public static final sud_dialog_background_dark:I = 0x7f080fc9

.field public static final sud_dialog_background_light:I = 0x7f080fca

.field public static final sud_edit_text_bg:I = 0x7f080fcb

.field public static final sud_edit_text_bg_shape:I = 0x7f080fcc

.field public static final sud_fourcolor_progress_bar:I = 0x7f080fcd

.field public static final sud_ic_expand:I = 0x7f080fce

.field public static final sud_ic_expand_less:I = 0x7f080fcf

.field public static final sud_ic_expand_more:I = 0x7f080fd0

.field public static final sud_layout_background:I = 0x7f080fd1

.field public static final sud_navbar_btn_bg:I = 0x7f080fd2

.field public static final sud_navbar_btn_bg_dark:I = 0x7f080fd3

.field public static final sud_navbar_btn_bg_light:I = 0x7f080fd4

.field public static final sud_navbar_ic_back:I = 0x7f080fd5

.field public static final sud_navbar_ic_down_arrow:I = 0x7f080fd6

.field public static final sud_navbar_ic_left_arrow:I = 0x7f080fd7

.field public static final sud_navbar_ic_more:I = 0x7f080fd8

.field public static final sud_navbar_ic_next:I = 0x7f080fd9

.field public static final sud_navbar_ic_right_arrow:I = 0x7f080fda

.field public static final sud_scroll_bar_dark:I = 0x7f080fdb

.field public static final sud_scroll_bar_light:I = 0x7f080fdc

.field public static final sud_switch_thumb_off:I = 0x7f080fdd

.field public static final sud_switch_thumb_on:I = 0x7f080fde

.field public static final sud_switch_thumb_selector:I = 0x7f080fdf

.field public static final sud_switch_track_off_background:I = 0x7f080fe0

.field public static final sud_switch_track_on_background:I = 0x7f080fe1

.field public static final sud_switch_track_selector:I = 0x7f080fe2

.field public static final sun_brightness_icon:I = 0x7f080fe3

.field public static final switchbar_background:I = 0x7f080fe4

.field public static final symbol_fast_input:I = 0x7f080fe5

.field public static final sync_icon:I = 0x7f080fe6

.field public static final sync_wifi:I = 0x7f080fe7

.field public static final system_nav_2_button:I = 0x7f080fe8

.field public static final system_nav_3_button:I = 0x7f080fe9

.field public static final system_nav_fully_gestural:I = 0x7f080fea

.field public static final tab_bg:I = 0x7f080feb

.field public static final tab_bg_first_normal:I = 0x7f080fec

.field public static final tab_bg_first_pressed:I = 0x7f080fed

.field public static final tab_bg_first_selected:I = 0x7f080fee

.field public static final tab_bg_last_normal:I = 0x7f080fef

.field public static final tab_bg_last_pressed:I = 0x7f080ff0

.field public static final tab_bg_last_selected:I = 0x7f080ff1

.field public static final tab_bg_middle_normal:I = 0x7f080ff2

.field public static final tab_bg_middle_pressed:I = 0x7f080ff3

.field public static final tab_bg_middle_selected:I = 0x7f080ff4

.field public static final tab_bg_single_normal:I = 0x7f080ff5

.field public static final tab_bg_single_pressed:I = 0x7f080ff6

.field public static final tab_bg_single_selected:I = 0x7f080ff7

.field public static final tab_selected_background_normal:I = 0x7f080ff8

.field public static final tablet_provision_btn_background:I = 0x7f080ff9

.field public static final tabs_indicator_background:I = 0x7f080ffa

.field public static final tapandpay_emptystate:I = 0x7f080ffb

.field public static final temperature_icon:I = 0x7f080ffc

.field public static final test_custom_background:I = 0x7f080ffd

.field public static final test_level_drawable:I = 0x7f080ffe

.field public static final tether_no_connected_device:I = 0x7f080fff

.field public static final tether_settings:I = 0x7f081000

.field public static final textIcon_thick:I = 0x7f081001

.field public static final textIcon_thin:I = 0x7f081002

.field public static final texture_icon:I = 0x7f081003

.field public static final thought_bubble:I = 0x7f081004

.field public static final three_fingers_long_press:I = 0x7f081005

.field public static final three_gesture_down:I = 0x7f081006

.field public static final three_gesture_long_press:I = 0x7f081007

.field public static final threedrop01:I = 0x7f081008

.field public static final threedrop02:I = 0x7f081009

.field public static final threedrop03:I = 0x7f08100a

.field public static final threedrop04:I = 0x7f08100b

.field public static final threedrop05:I = 0x7f08100c

.field public static final threedrop06:I = 0x7f08100d

.field public static final threedrop07:I = 0x7f08100e

.field public static final thumb:I = 0x7f08100f

.field public static final thumb_bar:I = 0x7f081010

.field public static final thumb_drawable:I = 0x7f081011

.field public static final thumb_icon:I = 0x7f081012

.field public static final tile_icon_animation_speed:I = 0x7f081013

.field public static final tile_icon_debugging_wireless:I = 0x7f081014

.field public static final tile_icon_force_rtl:I = 0x7f081015

.field public static final tile_icon_graphics:I = 0x7f081016

.field public static final tile_icon_sensors_off:I = 0x7f081017

.field public static final tile_icon_show_layout:I = 0x7f081018

.field public static final tile_icon_show_taps:I = 0x7f081019

.field public static final tile_icon_winscope_trace:I = 0x7f08101a

.field public static final timer_seekbar_progress:I = 0x7f08101b

.field public static final timer_seekbar_progress_bg:I = 0x7f08101c

.field public static final timer_seekbar_progress_primary:I = 0x7f08101d

.field public static final tooltip_frame_dark:I = 0x7f08101e

.field public static final tooltip_frame_light:I = 0x7f08101f

.field public static final top_logo:I = 0x7f081020

.field public static final top_logo_large:I = 0x7f081021

.field public static final transparency:I = 0x7f081022

.field public static final transparency_tileable:I = 0x7f081023

.field public static final tv_shape_circle:I = 0x7f081024

.field public static final type_approved_logo:I = 0x7f081025

.field public static final unlock_01_bind:I = 0x7f081026

.field public static final unlock_01_granted:I = 0x7f081027

.field public static final unlock_01_ungranted:I = 0x7f081028

.field public static final unlock_03_bind:I = 0x7f081029

.field public static final unlock_03_granted:I = 0x7f08102a

.field public static final unlock_03_ungranted:I = 0x7f08102b

.field public static final unlock_04_bind:I = 0x7f08102c

.field public static final unlock_04_granted:I = 0x7f08102d

.field public static final unlock_04_ungranted:I = 0x7f08102e

.field public static final unlock_set_settings:I = 0x7f08102f

.field public static final update_notification:I = 0x7f081030

.field public static final updater_notification:I = 0x7f081031

.field public static final usage_state_channel_selector:I = 0x7f081032

.field public static final usagestats_app_limit_bg:I = 0x7f081033

.field public static final usagestats_app_limit_bg2:I = 0x7f081034

.field public static final usb_background_selected_view:I = 0x7f081035

.field public static final usb_background_view:I = 0x7f081036

.field public static final user_avatar_bg:I = 0x7f081037

.field public static final user_fold_diagram:I = 0x7f081038

.field public static final user_select_background:I = 0x7f081039

.field public static final vector_drawable_progress_indeterminate_horizontal_trimmed:I = 0x7f08103a

.field public static final verfication_progressbar_circle:I = 0x7f08103b

.field public static final verifaction_progressbar_indeterminate_light:I = 0x7f08103c

.field public static final video_checkbox_back_ground:I = 0x7f08103d

.field public static final view_background:I = 0x7f08103e

.field public static final virus_scan_arrow:I = 0x7f08103f

.field public static final virus_scan_icon:I = 0x7f081040

.field public static final virus_scan_shield:I = 0x7f081041

.field public static final visual_checkbox_border_layout_bg:I = 0x7f081042

.field public static final voice_access_tile:I = 0x7f081043

.field public static final volume_dialog_button_background_outline:I = 0x7f081044

.field public static final volume_dialog_button_background_solid:I = 0x7f081045

.field public static final volume_seekbar_thumb:I = 0x7f081046

.field public static final volume_seekbar_thumb_n:I = 0x7f081047

.field public static final volume_seekbar_thumb_p:I = 0x7f081048

.field public static final vpn_settings:I = 0x7f081049

.field public static final wallet_icon:I = 0x7f08104a

.field public static final wallpaper_settings:I = 0x7f08104b

.field public static final warning_dialog_highlight_bg:I = 0x7f08104c

.field public static final wifi6_animation_1:I = 0x7f08104d

.field public static final wifi6_animation_2:I = 0x7f08104e

.field public static final wifi6_animation_3:I = 0x7f08104f

.field public static final wifi6_animation_4:I = 0x7f081050

.field public static final wifi6_signal:I = 0x7f081051

.field public static final wifi_animation_1:I = 0x7f081052

.field public static final wifi_animation_2:I = 0x7f081053

.field public static final wifi_animation_3:I = 0x7f081054

.field public static final wifi_animation_4:I = 0x7f081055

.field public static final wifi_button_normal:I = 0x7f081056

.field public static final wifi_button_pressed:I = 0x7f081057

.field public static final wifi_button_selector:I = 0x7f081058

.field public static final wifi_dpp_error:I = 0x7f081059

.field public static final wifi_dpp_success:I = 0x7f08105a

.field public static final wifi_friction:I = 0x7f08105b

.field public static final wifi_metered:I = 0x7f08105c

.field public static final wifi_not_show_password:I = 0x7f08105d

.field public static final wifi_refresh:I = 0x7f08105e

.field public static final wifi_refresh_background:I = 0x7f08105f

.field public static final wifi_scan_with_qr_code:I = 0x7f081060

.field public static final wifi_show_password:I = 0x7f081061

.field public static final wifi_signal:I = 0x7f081062

.field public static final wifi_wps:I = 0x7f081063

.field public static final wireless_more:I = 0x7f081064

.field public static final word_photo_bg_dark:I = 0x7f081065

.field public static final word_photo_bg_light:I = 0x7f081066

.field public static final work_challenge_background:I = 0x7f081067

.field public static final xiaoai:I = 0x7f081068

.field public static final xiaoai_volume_icon:I = 0x7f081069

.field public static final xiaomi_account:I = 0x7f08106a

.field public static final xiaomi_wifi_dianping_indicator:I = 0x7f08106b

.field public static final xiaomi_wifi_indicator:I = 0x7f08106c

.field public static final xiaomi_wifi_indicator_connected:I = 0x7f08106d

.field public static final xiaomi_wifi_meituan_indicator:I = 0x7f08106e

.field public static final xiaomi_wifi_nuomi_indicator:I = 0x7f08106f

.field public static final xiaomi_wifi_wechat_indicator:I = 0x7f081070

.field public static final zen_always_drawable:I = 0x7f081071

.field public static final zen_always_icon_selector:I = 0x7f081072

.field public static final zen_always_selected_drawable:I = 0x7f081073

.field public static final zen_calls_any:I = 0x7f081074

.field public static final zen_calls_contacts:I = 0x7f081075

.field public static final zen_calls_none:I = 0x7f081076

.field public static final zen_calls_starred:I = 0x7f081077

.field public static final zen_lockscreen_drawable:I = 0x7f081078

.field public static final zen_lockscreen_icon_selector:I = 0x7f081079

.field public static final zen_lockscreen_selected_drawable:I = 0x7f08107a

.field public static final zen_messages_any:I = 0x7f08107b

.field public static final zen_messages_contacts:I = 0x7f08107c

.field public static final zen_messages_none:I = 0x7f08107d

.field public static final zen_messages_starred:I = 0x7f08107e


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
