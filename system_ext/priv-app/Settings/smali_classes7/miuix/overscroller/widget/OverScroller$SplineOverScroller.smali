.class Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/overscroller/widget/OverScroller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SplineOverScroller"
.end annotation


# static fields
.field private static DECELERATION_RATE:F

.field private static final SPLINE_POSITION:[F

.field private static final SPLINE_TIME:[F


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrVelocity:D

.field private mCurrentPosition:D

.field private mDeceleration:F

.field private mDuration:I

.field private mFinal:D

.field private mFinished:Z

.field private mFlingFriction:F

.field private mLastStep:Z

.field private mOriginStart:D

.field private mPhysicalCoeff:F

.field private mSpringOperator:Lmiuix/animation/physics/SpringOperator;

.field private mSpringParams:[D

.field private mStart:D

.field private mStartTime:J

.field private mState:I

.field private mVelocity:D


# direct methods
.method static constructor <clinit>()V
    .locals 19

    const-wide v0, 0x3fe8f5c28f5c28f6L    # 0.78

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    const-wide v2, 0x3feccccccccccccdL    # 0.9

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->DECELERATION_RATE:F

    const/16 v0, 0x65

    new-array v1, v0, [F

    sput-object v1, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->SPLINE_POSITION:[F

    new-array v0, v0, [F

    sput-object v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->SPLINE_TIME:[F

    const/4 v0, 0x0

    const/4 v1, 0x0

    move v2, v1

    move v1, v0

    :goto_0
    const/16 v3, 0x64

    const/high16 v4, 0x3f800000    # 1.0f

    if-ge v2, v3, :cond_4

    int-to-float v3, v2

    const/high16 v5, 0x42c80000    # 100.0f

    div-float v5, v3, v5

    move v3, v4

    :goto_1
    sub-float v6, v3, v0

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    add-float/2addr v6, v0

    const/high16 v8, 0x40400000    # 3.0f

    mul-float v9, v6, v8

    sub-float v10, v4, v6

    mul-float/2addr v9, v10

    const v11, 0x3e333333    # 0.175f

    mul-float v12, v10, v11

    const v13, 0x3eb33334    # 0.35000002f

    mul-float v14, v6, v13

    add-float/2addr v12, v14

    mul-float/2addr v12, v9

    mul-float v14, v6, v6

    mul-float/2addr v14, v6

    add-float/2addr v12, v14

    sub-float v15, v12, v5

    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v15

    move/from16 v16, v12

    float-to-double v11, v15

    const-wide v17, 0x3ee4f8b588e368f1L    # 1.0E-5

    cmpg-double v11, v11, v17

    if-gez v11, :cond_2

    sget-object v3, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->SPLINE_POSITION:[F

    const/high16 v11, 0x3f000000    # 0.5f

    mul-float/2addr v10, v11

    add-float/2addr v10, v6

    mul-float/2addr v9, v10

    add-float/2addr v9, v14

    aput v9, v3, v2

    move v3, v4

    :goto_2
    sub-float v6, v3, v1

    div-float/2addr v6, v7

    add-float/2addr v6, v1

    mul-float v9, v6, v8

    sub-float v10, v4, v6

    mul-float/2addr v9, v10

    mul-float v12, v10, v11

    add-float/2addr v12, v6

    mul-float/2addr v12, v9

    mul-float v14, v6, v6

    mul-float/2addr v14, v6

    add-float/2addr v12, v14

    sub-float v15, v12, v5

    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v15

    float-to-double v7, v15

    cmpg-double v7, v7, v17

    if-gez v7, :cond_0

    sget-object v3, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->SPLINE_TIME:[F

    const v7, 0x3e333333    # 0.175f

    mul-float/2addr v10, v7

    mul-float/2addr v6, v13

    add-float/2addr v10, v6

    mul-float/2addr v9, v10

    add-float/2addr v9, v14

    aput v9, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const v7, 0x3e333333    # 0.175f

    cmpl-float v8, v12, v5

    if-lez v8, :cond_1

    move v3, v6

    goto :goto_3

    :cond_1
    move v1, v6

    :goto_3
    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v8, 0x40400000    # 3.0f

    goto :goto_2

    :cond_2
    cmpl-float v7, v16, v5

    if-lez v7, :cond_3

    move v3, v6

    goto :goto_1

    :cond_3
    move v0, v6

    goto :goto_1

    :cond_4
    sget-object v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->SPLINE_POSITION:[F

    sget-object v1, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->SPLINE_TIME:[F

    aput v4, v1, v3

    aput v4, v0, v3

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v0

    iput v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFlingFriction:F

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mState:I

    iput-object p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mContext:Landroid/content/Context;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinished:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iget p1, p1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v0, 0x43200000    # 160.0f

    mul-float/2addr p1, v0

    const v0, 0x43c10b3d

    mul-float/2addr p1, v0

    const v0, 0x3f570a3d    # 0.84f

    mul-float/2addr p1, v0

    iput p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mPhysicalCoeff:F

    return-void
.end method

.method static synthetic access$000(Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;)Z
    .locals 0

    iget-boolean p0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinished:Z

    return p0
.end method

.method static synthetic access$100(Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;)D
    .locals 2

    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    return-wide v0
.end method

.method static synthetic access$200(Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;)D
    .locals 2

    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrVelocity:D

    return-wide v0
.end method

.method static synthetic access$300(Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;)D
    .locals 2

    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    return-wide v0
.end method

.method static synthetic access$400(Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;)D
    .locals 2

    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    return-wide v0
.end method

.method static synthetic access$500(Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;)I
    .locals 0

    iget p0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDuration:I

    return p0
.end method

.method static synthetic access$600(Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;)J
    .locals 2

    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStartTime:J

    return-wide v0
.end method


# virtual methods
.method computeScrollOffset()Z
    .locals 20

    goto/32 :goto_23

    nop

    :goto_0
    const-wide/16 v10, 0x0

    goto/32 :goto_38

    nop

    :goto_1
    sub-long v6, v4, v6

    goto/32 :goto_1b

    nop

    :goto_2
    aput-wide v4, v1, v3

    goto/32 :goto_3e

    nop

    :goto_3
    aget-wide v15, v1, v3

    goto/32 :goto_34

    nop

    :goto_4
    mul-double/2addr v8, v1

    goto/32 :goto_32

    nop

    :goto_5
    iget-wide v11, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrVelocity:D

    goto/32 :goto_27

    nop

    :goto_6
    iget-wide v1, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_28

    nop

    :goto_7
    if-nez v1, :cond_0

    goto/32 :goto_2b

    :cond_0
    goto/32 :goto_18

    nop

    :goto_8
    move-wide v8, v6

    :goto_9
    goto/32 :goto_1e

    nop

    :goto_a
    if-nez v1, :cond_1

    goto/32 :goto_22

    :cond_1
    goto/32 :goto_3c

    nop

    :goto_b
    iget-boolean v1, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mLastStep:Z

    goto/32 :goto_3d

    nop

    :goto_c
    const/high16 v6, 0x447a0000    # 1000.0f

    goto/32 :goto_2c

    nop

    :goto_d
    iput-wide v1, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    :goto_e
    goto/32 :goto_2a

    nop

    :goto_f
    goto :goto_9

    :goto_10
    goto/32 :goto_8

    nop

    :goto_11
    return v3

    :goto_12
    goto/32 :goto_1c

    nop

    :goto_13
    if-eqz v1, :cond_2

    goto/32 :goto_10

    :cond_2
    goto/32 :goto_f

    nop

    :goto_14
    aput-wide v4, v1, v2

    goto/32 :goto_39

    nop

    :goto_15
    new-array v1, v1, [D

    goto/32 :goto_3b

    nop

    :goto_16
    iget-wide v1, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    goto/32 :goto_d

    nop

    :goto_17
    float-to-double v6, v1

    goto/32 :goto_2f

    nop

    :goto_18
    iget-boolean v1, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinished:Z

    goto/32 :goto_2e

    nop

    :goto_19
    goto :goto_2b

    :goto_1a
    goto/32 :goto_b

    nop

    :goto_1b
    long-to-float v1, v6

    goto/32 :goto_c

    nop

    :goto_1c
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v4

    goto/32 :goto_2d

    nop

    :goto_1d
    iget-object v1, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mSpringOperator:Lmiuix/animation/physics/SpringOperator;

    goto/32 :goto_26

    nop

    :goto_1e
    iput-wide v4, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStartTime:J

    goto/32 :goto_37

    nop

    :goto_1f
    iput-wide v4, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    goto/32 :goto_30

    nop

    :goto_20
    aget-wide v13, v1, v2

    goto/32 :goto_3

    nop

    :goto_21
    goto :goto_e

    :goto_22
    goto/32 :goto_16

    nop

    :goto_23
    move-object/from16 v0, p0

    goto/32 :goto_1d

    nop

    :goto_24
    invoke-virtual/range {v10 .. v19}, Lmiuix/animation/physics/SpringOperator;->updateVelocity(DDDD[D)D

    move-result-wide v1

    goto/32 :goto_31

    nop

    :goto_25
    iput-boolean v3, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinished:Z

    goto/32 :goto_33

    nop

    :goto_26
    const/4 v2, 0x0

    goto/32 :goto_7

    nop

    :goto_27
    iget-object v1, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mSpringParams:[D

    goto/32 :goto_20

    nop

    :goto_28
    invoke-virtual {v0, v4, v5, v1, v2}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->isAtEquilibrium(DD)Z

    move-result v1

    goto/32 :goto_a

    nop

    :goto_29
    if-nez v1, :cond_3

    goto/32 :goto_12

    :cond_3
    goto/32 :goto_25

    nop

    :goto_2a
    return v3

    :goto_2b
    goto/32 :goto_3f

    nop

    :goto_2c
    div-float/2addr v1, v6

    goto/32 :goto_17

    nop

    :goto_2d
    iget-wide v6, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStartTime:J

    goto/32 :goto_1

    nop

    :goto_2e
    if-nez v1, :cond_4

    goto/32 :goto_1a

    :cond_4
    goto/32 :goto_19

    nop

    :goto_2f
    const-wide v8, 0x3f90624de0000000L    # 0.01600000075995922

    goto/32 :goto_36

    nop

    :goto_30
    iput-wide v1, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrVelocity:D

    goto/32 :goto_6

    nop

    :goto_31
    iget-wide v4, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    goto/32 :goto_4

    nop

    :goto_32
    add-double/2addr v4, v8

    goto/32 :goto_1f

    nop

    :goto_33
    iget-wide v1, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_3a

    nop

    :goto_34
    const/4 v1, 0x2

    goto/32 :goto_15

    nop

    :goto_35
    move-object/from16 v19, v1

    goto/32 :goto_24

    nop

    :goto_36
    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(DD)D

    move-result-wide v6

    goto/32 :goto_0

    nop

    :goto_37
    iget-object v10, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mSpringOperator:Lmiuix/animation/physics/SpringOperator;

    goto/32 :goto_5

    nop

    :goto_38
    cmpl-double v1, v6, v10

    goto/32 :goto_13

    nop

    :goto_39
    iget-wide v4, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    goto/32 :goto_2

    nop

    :goto_3a
    iput-wide v1, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    goto/32 :goto_11

    nop

    :goto_3b
    iget-wide v4, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_14

    nop

    :goto_3c
    iput-boolean v3, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mLastStep:Z

    goto/32 :goto_21

    nop

    :goto_3d
    const/4 v3, 0x1

    goto/32 :goto_29

    nop

    :goto_3e
    move-wide/from16 v17, v8

    goto/32 :goto_35

    nop

    :goto_3f
    return v2
.end method

.method continueWhenFinished()Z
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    throw p0

    :goto_1
    const/4 p0, 0x0

    goto/32 :goto_0

    nop
.end method

.method finish()V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    const/4 p0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    throw p0
.end method

.method fling(IIIII)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    const/4 p0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    throw p0
.end method

.method final getCurrVelocity()F
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    return p0

    :goto_1
    double-to-float p0, v0

    goto/32 :goto_0

    nop

    :goto_2
    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrVelocity:D

    goto/32 :goto_1

    nop
.end method

.method final getCurrentPosition()I
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    goto/32 :goto_2

    nop

    :goto_1
    return p0

    :goto_2
    double-to-int p0, v0

    goto/32 :goto_1

    nop
.end method

.method final getFinal()I
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    double-to-int p0, v0

    goto/32 :goto_2

    nop

    :goto_1
    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_0

    nop

    :goto_2
    return p0
.end method

.method final getStart()I
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    return p0

    :goto_1
    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    goto/32 :goto_2

    nop

    :goto_2
    double-to-int p0, v0

    goto/32 :goto_0

    nop
.end method

.method final getState()I
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iget p0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mState:I

    goto/32 :goto_1

    nop

    :goto_1
    return p0
.end method

.method isAtEquilibrium(DD)Z
    .locals 0

    goto/32 :goto_2

    nop

    :goto_0
    const/4 p0, 0x0

    :goto_1
    goto/32 :goto_5

    nop

    :goto_2
    sub-double/2addr p1, p3

    goto/32 :goto_9

    nop

    :goto_3
    goto :goto_1

    :goto_4
    goto/32 :goto_0

    nop

    :goto_5
    return p0

    :goto_6
    if-ltz p0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_a

    nop

    :goto_7
    cmpg-double p0, p0, p2

    goto/32 :goto_6

    nop

    :goto_8
    const-wide/high16 p2, 0x3ff0000000000000L    # 1.0

    goto/32 :goto_7

    nop

    :goto_9
    invoke-static {p1, p2}, Ljava/lang/Math;->abs(D)D

    move-result-wide p0

    goto/32 :goto_8

    nop

    :goto_a
    const/4 p0, 0x1

    goto/32 :goto_3

    nop
.end method

.method final isFinished()Z
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iget-boolean p0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinished:Z

    goto/32 :goto_1

    nop

    :goto_1
    return p0
.end method

.method notifyEdgeReached(III)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    const/4 p0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    throw p0
.end method

.method final setCurrVelocity(F)V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrVelocity:D

    goto/32 :goto_2

    nop

    :goto_1
    float-to-double v0, p1

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method final setCurrentPosition(I)V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    goto/32 :goto_2

    nop

    :goto_1
    int-to-double v0, p1

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method final setDuration(I)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iput p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDuration:I

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method final setFinal(I)V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_2

    nop

    :goto_1
    int-to-double v0, p1

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method setFinalPosition(I)V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    const/4 p1, 0x0

    goto/32 :goto_4

    nop

    :goto_1
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_0

    nop

    :goto_2
    return-void

    :goto_3
    int-to-double v0, p1

    goto/32 :goto_1

    nop

    :goto_4
    iput-boolean p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinished:Z

    goto/32 :goto_2

    nop
.end method

.method final setFinished(Z)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iput-boolean p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinished:Z

    goto/32 :goto_0

    nop
.end method

.method final setStart(I)V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    int-to-double v0, p1

    goto/32 :goto_0

    nop
.end method

.method final setStartTime(J)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iput-wide p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStartTime:J

    goto/32 :goto_0

    nop
.end method

.method final setState(I)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iput p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mState:I

    goto/32 :goto_0

    nop
.end method

.method springback(III)Z
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    throw p0

    :goto_1
    const/4 p0, 0x0

    goto/32 :goto_0

    nop
.end method

.method startScroll(III)V
    .locals 2

    goto/32 :goto_e

    nop

    :goto_0
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    goto/32 :goto_d

    nop

    :goto_1
    iput-wide p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mVelocity:D

    goto/32 :goto_2

    nop

    :goto_2
    return-void

    :goto_3
    iput-boolean v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinished:Z

    goto/32 :goto_4

    nop

    :goto_4
    int-to-double v0, p1

    goto/32 :goto_f

    nop

    :goto_5
    iput-wide p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStartTime:J

    goto/32 :goto_b

    nop

    :goto_6
    const-wide/16 p1, 0x0

    goto/32 :goto_1

    nop

    :goto_7
    iput-wide p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_8

    nop

    :goto_8
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide p1

    goto/32 :goto_5

    nop

    :goto_9
    const/4 p1, 0x0

    goto/32 :goto_c

    nop

    :goto_a
    int-to-double p1, p1

    goto/32 :goto_7

    nop

    :goto_b
    iput p3, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDuration:I

    goto/32 :goto_9

    nop

    :goto_c
    iput p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    goto/32 :goto_6

    nop

    :goto_d
    add-int/2addr p1, p2

    goto/32 :goto_a

    nop

    :goto_e
    const/4 v0, 0x0

    goto/32 :goto_3

    nop

    :goto_f
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    goto/32 :goto_0

    nop
.end method

.method startScrollByFling(FII)V
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    new-array p0, p2, [F

    fill-array-data p0, :array_0

    goto/32 :goto_15

    nop

    :goto_1
    iput-object p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mSpringOperator:Lmiuix/animation/physics/SpringOperator;

    goto/32 :goto_13

    nop

    :goto_2
    iput-boolean v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinished:Z

    goto/32 :goto_12

    nop

    :goto_3
    new-array p3, p2, [D

    goto/32 :goto_16

    nop

    :goto_4
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mOriginStart:D

    goto/32 :goto_19

    nop

    :goto_5
    const/4 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_6
    add-float/2addr p1, p2

    goto/32 :goto_9

    nop

    :goto_7
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    goto/32 :goto_8

    nop

    :goto_8
    int-to-float p2, p2

    goto/32 :goto_6

    nop

    :goto_9
    float-to-double p1, p1

    goto/32 :goto_11

    nop

    :goto_a
    int-to-double p1, p3

    goto/32 :goto_17

    nop

    :goto_b
    iput-wide p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStartTime:J

    goto/32 :goto_a

    nop

    :goto_c
    iput-wide p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrVelocity:D

    goto/32 :goto_14

    nop

    :goto_d
    return-void

    nop

    :array_0
    .array-data 4
        0x3f7d70a4    # 0.99f
        0x3ecccccd    # 0.4f
    .end array-data

    :goto_e
    float-to-double v0, p1

    goto/32 :goto_4

    nop

    :goto_f
    invoke-direct {p1}, Lmiuix/animation/physics/SpringOperator;-><init>()V

    goto/32 :goto_1

    nop

    :goto_10
    invoke-virtual {p0, v0}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->setState(I)V

    goto/32 :goto_e

    nop

    :goto_11
    iput-wide p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_18

    nop

    :goto_12
    iput-boolean v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mLastStep:Z

    goto/32 :goto_10

    nop

    :goto_13
    const/4 p2, 0x2

    goto/32 :goto_3

    nop

    :goto_14
    new-instance p1, Lmiuix/animation/physics/SpringOperator;

    goto/32 :goto_f

    nop

    :goto_15
    invoke-virtual {p1, p0, p3}, Lmiuix/animation/physics/SpringOperator;->getParameters([F[D)V

    goto/32 :goto_d

    nop

    :goto_16
    iput-object p3, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mSpringParams:[D

    goto/32 :goto_0

    nop

    :goto_17
    iput-wide p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mVelocity:D

    goto/32 :goto_c

    nop

    :goto_18
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide p1

    goto/32 :goto_b

    nop

    :goto_19
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    goto/32 :goto_7

    nop
.end method

.method update()Z
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    const/4 p0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    throw p0
.end method

.method updateScroll(F)V
    .locals 6

    goto/32 :goto_9

    nop

    :goto_0
    long-to-double v2, v2

    goto/32 :goto_2

    nop

    :goto_1
    iget-wide v4, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_5

    nop

    :goto_2
    add-double/2addr v0, v2

    goto/32 :goto_6

    nop

    :goto_3
    mul-double/2addr v2, v4

    goto/32 :goto_7

    nop

    :goto_4
    float-to-double v2, p1

    goto/32 :goto_1

    nop

    :goto_5
    sub-double/2addr v4, v0

    goto/32 :goto_3

    nop

    :goto_6
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    goto/32 :goto_8

    nop

    :goto_7
    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    goto/32 :goto_0

    nop

    :goto_8
    return-void

    :goto_9
    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    goto/32 :goto_4

    nop
.end method
