.class public final Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;
.super Ljava/lang/Object;


# instance fields
.field private mDampedFreq:D

.field mDampingRatio:D

.field private mFinalPosition:D

.field private mGammaMinus:D

.field private mGammaPlus:D

.field private mInitialized:Z

.field private final mMassState:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;

.field mNaturalFreq:D

.field mTimeRatio:D

.field private mValueThreshold:D

.field private mVelocityThreshold:D


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide v0, 0x4097700000000000L    # 1500.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    iput-wide v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mNaturalFreq:D

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    iput-wide v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mDampingRatio:D

    const-wide v0, 0x408f400000000000L    # 1000.0

    iput-wide v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mTimeRatio:D

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mInitialized:Z

    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mFinalPosition:D

    new-instance v0, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;

    invoke-direct {v0}, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;-><init>()V

    iput-object v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mMassState:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;

    return-void
.end method

.method private init()V
    .locals 8

    iget-boolean v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mInitialized:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-wide v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mFinalPosition:D

    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_3

    iget-wide v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mDampingRatio:D

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v4, v0, v2

    if-lez v4, :cond_1

    neg-double v4, v0

    iget-wide v6, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mNaturalFreq:D

    mul-double/2addr v4, v6

    mul-double/2addr v0, v0

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    mul-double/2addr v6, v0

    add-double/2addr v4, v6

    iput-wide v4, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mGammaPlus:D

    iget-wide v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mDampingRatio:D

    neg-double v4, v0

    iget-wide v6, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mNaturalFreq:D

    mul-double/2addr v4, v6

    mul-double/2addr v0, v0

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    mul-double/2addr v6, v0

    sub-double/2addr v4, v6

    iput-wide v4, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mGammaMinus:D

    goto :goto_0

    :cond_1
    const-wide/16 v4, 0x0

    cmpl-double v4, v0, v4

    if-ltz v4, :cond_2

    cmpg-double v4, v0, v2

    if-gez v4, :cond_2

    iget-wide v4, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mNaturalFreq:D

    mul-double/2addr v0, v0

    sub-double/2addr v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    mul-double/2addr v4, v0

    iput-wide v4, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mDampedFreq:D

    :cond_2
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mInitialized:Z

    return-void

    :cond_3
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Error: Final position of the spring must be set before the animation starts"

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public getFinalPosition()F
    .locals 2

    iget-wide v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mFinalPosition:D

    double-to-float p0, v0

    return p0
.end method

.method public isAtEquilibrium(FF)Z
    .locals 4

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result p2

    float-to-double v0, p2

    iget-wide v2, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mVelocityThreshold:D

    cmpg-double p2, v0, v2

    if-gez p2, :cond_0

    invoke-virtual {p0}, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->getFinalPosition()F

    move-result p2

    sub-float/2addr p1, p2

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    float-to-double p1, p1

    iget-wide v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mValueThreshold:D

    cmpg-double p0, p1, v0

    if-gez p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public setDampingRatio(F)Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;
    .locals 2

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_0

    float-to-double v0, p1

    iput-wide v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mDampingRatio:D

    const/4 p1, 0x0

    iput-boolean p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mInitialized:Z

    return-object p0

    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Damping ratio must be non-negative"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public setFinalPosition(F)Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;
    .locals 2

    float-to-double v0, p1

    iput-wide v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mFinalPosition:D

    return-object p0
.end method

.method public setStiffness(F)Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;
    .locals 2

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-lez v0, :cond_0

    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    iput-wide v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mNaturalFreq:D

    const/4 p1, 0x0

    iput-boolean p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mInitialized:Z

    return-object p0

    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Spring stiffness constant must be positive."

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public setTimeRatio(D)Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;
    .locals 0

    iput-wide p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mTimeRatio:D

    return-object p0
.end method

.method setValueThreshold(D)V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    iput-wide p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mValueThreshold:D

    goto/32 :goto_1

    nop

    :goto_1
    const-wide v0, 0x404f400000000000L    # 62.5

    goto/32 :goto_5

    nop

    :goto_2
    return-void

    :goto_3
    invoke-static {p1, p2}, Ljava/lang/Math;->abs(D)D

    move-result-wide p1

    goto/32 :goto_0

    nop

    :goto_4
    iput-wide p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mVelocityThreshold:D

    goto/32 :goto_2

    nop

    :goto_5
    mul-double/2addr p1, v0

    goto/32 :goto_4

    nop
.end method

.method updateValues(DDJ)Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;
    .locals 16

    goto/32 :goto_e

    nop

    :goto_0
    mul-double/2addr v12, v1

    goto/32 :goto_71

    nop

    :goto_1
    iput v0, v1, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;->mVelocity:F

    goto/32 :goto_21

    nop

    :goto_2
    mul-double/2addr v3, v1

    goto/32 :goto_27

    nop

    :goto_3
    mul-double/2addr v12, v1

    goto/32 :goto_2d

    nop

    :goto_4
    invoke-static {v10, v11, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    goto/32 :goto_3c

    nop

    :goto_5
    iget-wide v5, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mGammaMinus:D

    goto/32 :goto_3b

    nop

    :goto_6
    if-ltz v1, :cond_0

    goto/32 :goto_1e

    :cond_0
    goto/32 :goto_36

    nop

    :goto_7
    iput v0, v1, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;->mValue:F

    goto/32 :goto_1f

    nop

    :goto_8
    neg-double v12, v12

    goto/32 :goto_3f

    nop

    :goto_9
    double-to-float v0, v5

    goto/32 :goto_7

    nop

    :goto_a
    invoke-direct/range {p0 .. p0}, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->init()V

    goto/32 :goto_6e

    nop

    :goto_b
    add-double v7, p3, v7

    goto/32 :goto_63

    nop

    :goto_c
    mul-double/2addr v3, v5

    goto/32 :goto_57

    nop

    :goto_d
    mul-double/2addr v5, v1

    goto/32 :goto_4

    nop

    :goto_e
    move-object/from16 v0, p0

    goto/32 :goto_a

    nop

    :goto_f
    div-double/2addr v7, v12

    goto/32 :goto_20

    nop

    :goto_10
    add-double/2addr v5, v12

    goto/32 :goto_6f

    nop

    :goto_11
    div-double/2addr v7, v14

    goto/32 :goto_86

    nop

    :goto_12
    neg-double v9, v10

    goto/32 :goto_64

    nop

    :goto_13
    const-wide v10, 0x4005bf0a8b145769L    # Math.E

    goto/32 :goto_67

    nop

    :goto_14
    invoke-static {v11, v12}, Ljava/lang/Math;->sin(D)D

    move-result-wide v3

    goto/32 :goto_49

    nop

    :goto_15
    iget-wide v11, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mDampedFreq:D

    goto/32 :goto_1c

    nop

    :goto_16
    invoke-static {v10, v11, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    goto/32 :goto_60

    nop

    :goto_17
    cmpg-double v1, v1, v3

    goto/32 :goto_5e

    nop

    :goto_18
    cmpl-double v9, v5, v7

    goto/32 :goto_13

    nop

    :goto_19
    long-to-double v1, v1

    goto/32 :goto_58

    nop

    :goto_1a
    invoke-static {v5, v6}, Ljava/lang/Math;->abs(D)D

    move-result-wide v1

    goto/32 :goto_4e

    nop

    :goto_1b
    mul-double/2addr v3, v12

    goto/32 :goto_3

    nop

    :goto_1c
    move-wide/from16 p1, v5

    goto/32 :goto_7f

    nop

    :goto_1d
    move-wide v7, v5

    :goto_1e
    goto/32 :goto_2b

    nop

    :goto_1f
    double-to-float v0, v7

    goto/32 :goto_1

    nop

    :goto_20
    iget-wide v12, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mNaturalFreq:D

    goto/32 :goto_54

    nop

    :goto_21
    return-object v1

    :goto_22
    iget-wide v12, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mGammaPlus:D

    goto/32 :goto_1b

    nop

    :goto_23
    mul-double/2addr v14, v3

    goto/32 :goto_81

    nop

    :goto_24
    add-double/2addr v7, v3

    goto/32 :goto_7d

    nop

    :goto_25
    mul-double/2addr v5, v12

    goto/32 :goto_65

    nop

    :goto_26
    mul-double/2addr v7, v12

    goto/32 :goto_22

    nop

    :goto_27
    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v1

    goto/32 :goto_5c

    nop

    :goto_28
    neg-double v5, v5

    goto/32 :goto_25

    nop

    :goto_29
    add-double/2addr v12, v14

    goto/32 :goto_68

    nop

    :goto_2a
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    goto/32 :goto_73

    nop

    :goto_2b
    iget-object v1, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mMassState:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;

    goto/32 :goto_45

    nop

    :goto_2c
    iget-wide v5, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mNaturalFreq:D

    goto/32 :goto_33

    nop

    :goto_2d
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    goto/32 :goto_5a

    nop

    :goto_2e
    neg-double v5, v5

    goto/32 :goto_d

    nop

    :goto_2f
    mul-double/2addr v5, v3

    goto/32 :goto_80

    nop

    :goto_30
    mul-double/2addr v14, v1

    goto/32 :goto_37

    nop

    :goto_31
    cmpl-double v9, v5, v7

    goto/32 :goto_4d

    nop

    :goto_32
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    goto/32 :goto_4f

    nop

    :goto_33
    mul-double v7, v5, v3

    goto/32 :goto_b

    nop

    :goto_34
    mul-double/2addr v5, v1

    goto/32 :goto_16

    nop

    :goto_35
    sub-double v3, p1, v3

    goto/32 :goto_40

    nop

    :goto_36
    move-wide v5, v2

    goto/32 :goto_1d

    nop

    :goto_37
    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    goto/32 :goto_51

    nop

    :goto_38
    mul-double/2addr v9, v1

    goto/32 :goto_79

    nop

    :goto_39
    mul-double/2addr v14, v5

    goto/32 :goto_7a

    nop

    :goto_3a
    mul-double/2addr v9, v5

    goto/32 :goto_7b

    nop

    :goto_3b
    mul-double v7, v5, v3

    goto/32 :goto_7c

    nop

    :goto_3c
    mul-double/2addr v5, v3

    goto/32 :goto_47

    nop

    :goto_3d
    add-double/2addr v5, v2

    goto/32 :goto_9

    nop

    :goto_3e
    neg-double v14, v12

    goto/32 :goto_39

    nop

    :goto_3f
    mul-double/2addr v12, v1

    goto/32 :goto_2a

    nop

    :goto_40
    iget-wide v5, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mDampingRatio:D

    goto/32 :goto_46

    nop

    :goto_41
    iget-wide v3, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mDampedFreq:D

    goto/32 :goto_6b

    nop

    :goto_42
    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    goto/32 :goto_44

    nop

    :goto_43
    mul-double/2addr v12, v3

    goto/32 :goto_10

    nop

    :goto_44
    mul-double/2addr v12, v3

    goto/32 :goto_52

    nop

    :goto_45
    iget-wide v2, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mFinalPosition:D

    goto/32 :goto_3d

    nop

    :goto_46
    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    goto/32 :goto_18

    nop

    :goto_47
    iget-wide v12, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mNaturalFreq:D

    goto/32 :goto_66

    nop

    :goto_48
    mul-double/2addr v12, v1

    goto/32 :goto_32

    nop

    :goto_49
    mul-double/2addr v5, v3

    goto/32 :goto_41

    nop

    :goto_4a
    mul-double/2addr v7, v14

    goto/32 :goto_28

    nop

    :goto_4b
    add-double/2addr v7, v3

    goto/32 :goto_6c

    nop

    :goto_4c
    iget-wide v12, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mDampedFreq:D

    goto/32 :goto_50

    nop

    :goto_4d
    if-eqz v9, :cond_1

    goto/32 :goto_6d

    :cond_1
    goto/32 :goto_2c

    nop

    :goto_4e
    const-wide v3, 0x3fe3333340000000L    # 0.6000000238418579

    goto/32 :goto_17

    nop

    :goto_4f
    mul-double/2addr v3, v12

    goto/32 :goto_70

    nop

    :goto_50
    mul-double/2addr v12, v1

    goto/32 :goto_42

    nop

    :goto_51
    mul-double/2addr v14, v7

    goto/32 :goto_29

    nop

    :goto_52
    iget-wide v14, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mDampedFreq:D

    goto/32 :goto_30

    nop

    :goto_53
    mul-double/2addr v7, v12

    goto/32 :goto_5f

    nop

    :goto_54
    mul-double v14, v5, v12

    goto/32 :goto_23

    nop

    :goto_55
    sub-double v12, v5, v12

    goto/32 :goto_83

    nop

    :goto_56
    sub-double v14, v5, v12

    goto/32 :goto_11

    nop

    :goto_57
    sub-double v3, v3, p3

    goto/32 :goto_55

    nop

    :goto_58
    iget-wide v3, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mTimeRatio:D

    goto/32 :goto_72

    nop

    :goto_59
    mul-double/2addr v14, v10

    goto/32 :goto_12

    nop

    :goto_5a
    mul-double/2addr v3, v1

    goto/32 :goto_24

    nop

    :goto_5b
    mul-double/2addr v3, v14

    goto/32 :goto_8

    nop

    :goto_5c
    mul-double/2addr v7, v1

    goto/32 :goto_76

    nop

    :goto_5d
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    goto/32 :goto_26

    nop

    :goto_5e
    const-wide/16 v2, 0x0

    goto/32 :goto_6

    nop

    :goto_5f
    mul-double/2addr v12, v1

    goto/32 :goto_5d

    nop

    :goto_60
    mul-double/2addr v5, v7

    goto/32 :goto_62

    nop

    :goto_61
    iget-wide v12, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mNaturalFreq:D

    goto/32 :goto_3e

    nop

    :goto_62
    iget-wide v12, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mGammaPlus:D

    goto/32 :goto_0

    nop

    :goto_63
    mul-double v12, v7, v1

    goto/32 :goto_75

    nop

    :goto_64
    mul-double/2addr v9, v12

    goto/32 :goto_38

    nop

    :goto_65
    mul-double/2addr v5, v1

    goto/32 :goto_85

    nop

    :goto_66
    neg-double v12, v12

    goto/32 :goto_48

    nop

    :goto_67
    if-gtz v9, :cond_2

    goto/32 :goto_7e

    :cond_2
    goto/32 :goto_5

    nop

    :goto_68
    mul-double/2addr v5, v12

    goto/32 :goto_61

    nop

    :goto_69
    move-wide/from16 v5, p1

    :goto_6a
    goto/32 :goto_1a

    nop

    :goto_6b
    mul-double/2addr v7, v3

    goto/32 :goto_2

    nop

    :goto_6c
    goto :goto_6a

    :goto_6d
    goto/32 :goto_74

    nop

    :goto_6e
    move-wide/from16 v1, p5

    goto/32 :goto_19

    nop

    :goto_6f
    iget-wide v12, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mGammaMinus:D

    goto/32 :goto_53

    nop

    :goto_70
    iget-wide v12, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mNaturalFreq:D

    goto/32 :goto_82

    nop

    :goto_71
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    goto/32 :goto_43

    nop

    :goto_72
    div-double/2addr v1, v3

    goto/32 :goto_78

    nop

    :goto_73
    mul-double/2addr v7, v1

    goto/32 :goto_4b

    nop

    :goto_74
    iget-wide v12, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mDampedFreq:D

    goto/32 :goto_f

    nop

    :goto_75
    add-double/2addr v3, v12

    goto/32 :goto_2e

    nop

    :goto_76
    add-double/2addr v5, v7

    goto/32 :goto_3a

    nop

    :goto_77
    iget-wide v12, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mGammaPlus:D

    goto/32 :goto_56

    nop

    :goto_78
    iget-wide v3, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mFinalPosition:D

    goto/32 :goto_35

    nop

    :goto_79
    const-wide v11, 0x4005bf0a8b145769L    # Math.E

    goto/32 :goto_84

    nop

    :goto_7a
    iget-wide v10, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->mDampingRatio:D

    goto/32 :goto_59

    nop

    :goto_7b
    add-double v7, v14, v9

    goto/32 :goto_69

    nop

    :goto_7c
    sub-double v7, v7, p3

    goto/32 :goto_77

    nop

    :goto_7d
    goto/16 :goto_6a

    :goto_7e
    goto/32 :goto_31

    nop

    :goto_7f
    neg-double v5, v11

    goto/32 :goto_2f

    nop

    :goto_80
    mul-double/2addr v11, v1

    goto/32 :goto_14

    nop

    :goto_81
    add-double v14, v14, p3

    goto/32 :goto_4a

    nop

    :goto_82
    neg-double v14, v12

    goto/32 :goto_5b

    nop

    :goto_83
    div-double/2addr v3, v12

    goto/32 :goto_34

    nop

    :goto_84
    invoke-static {v11, v12, v9, v10}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v9

    goto/32 :goto_15

    nop

    :goto_85
    invoke-static {v10, v11, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    goto/32 :goto_4c

    nop

    :goto_86
    sub-double v7, v3, v7

    goto/32 :goto_c

    nop
.end method
