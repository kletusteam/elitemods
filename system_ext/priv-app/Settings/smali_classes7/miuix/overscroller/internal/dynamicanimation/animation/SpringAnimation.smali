.class public final Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;
.super Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation<",
        "Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;",
        ">;"
    }
.end annotation


# instance fields
.field private mEndRequested:Z

.field private mPendingPosition:F

.field private mSpring:Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;


# direct methods
.method public constructor <init>(Lmiuix/overscroller/internal/dynamicanimation/animation/FloatValueHolder;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;-><init>(Lmiuix/overscroller/internal/dynamicanimation/animation/FloatValueHolder;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->mSpring:Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;

    const p1, 0x7f7fffff    # Float.MAX_VALUE

    iput p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->mPendingPosition:F

    const/4 p1, 0x0

    iput-boolean p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->mEndRequested:Z

    return-void
.end method

.method private sanityCheck()V
    .locals 4

    iget-object v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->mSpring:Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->getFinalPosition()F

    move-result v0

    float-to-double v0, v0

    iget v2, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->mMaxValue:F

    float-to-double v2, v2

    cmpl-double v2, v0, v2

    if-gtz v2, :cond_1

    iget p0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->mMinValue:F

    float-to-double v2, p0

    cmpg-double p0, v0, v2

    if-ltz p0, :cond_0

    return-void

    :cond_0
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Final position of the spring cannot be less than the min value."

    invoke-direct {p0, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Final position of the spring cannot be greater than the max value."

    invoke-direct {p0, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_2
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Incomplete SpringAnimation: Either final position or a spring force needs to be set."

    invoke-direct {p0, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public getSpring()Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;
    .locals 0

    iget-object p0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->mSpring:Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;

    return-object p0
.end method

.method isAtEquilibrium(FF)Z
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return p0

    :goto_1
    iget-object p0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->mSpring:Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {p0, p1, p2}, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->isAtEquilibrium(FF)Z

    move-result p0

    goto/32 :goto_0

    nop
.end method

.method public setSpring(Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;)Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;
    .locals 0

    iput-object p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->mSpring:Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;

    return-object p0
.end method

.method setValueThreshold(F)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-void
.end method

.method public start(Z)V
    .locals 3

    invoke-direct {p0}, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->sanityCheck()V

    iget-object v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->mSpring:Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;

    invoke-virtual {p0}, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->getValueThreshold()F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->setValueThreshold(D)V

    invoke-super {p0, p1}, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->start(Z)V

    return-void
.end method

.method updateValueAndVelocity(J)Z
    .locals 20

    goto/32 :goto_a

    nop

    :goto_0
    return v3

    :goto_1
    invoke-virtual/range {v13 .. v19}, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->updateValues(DDJ)Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;

    move-result-object v1

    goto/32 :goto_43

    nop

    :goto_2
    iget v5, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->mMinValue:F

    goto/32 :goto_46

    nop

    :goto_3
    iput v1, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->mValue:F

    goto/32 :goto_41

    nop

    :goto_4
    return v2

    :goto_5
    goto/32 :goto_1c

    nop

    :goto_6
    if-nez v6, :cond_0

    goto/32 :goto_1a

    :cond_0
    goto/32 :goto_18

    nop

    :goto_7
    cmpl-float v6, v1, v5

    goto/32 :goto_6

    nop

    :goto_8
    const-wide/16 v11, 0x2

    goto/32 :goto_34

    nop

    :goto_9
    float-to-double v5, v1

    goto/32 :goto_1b

    nop

    :goto_a
    move-object/from16 v0, p0

    goto/32 :goto_17

    nop

    :goto_b
    invoke-virtual {v1}, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->getFinalPosition()F

    move-result v1

    goto/32 :goto_24

    nop

    :goto_c
    invoke-virtual/range {v13 .. v19}, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->updateValues(DDJ)Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;

    move-result-object v1

    goto/32 :goto_32

    nop

    :goto_d
    invoke-virtual {v6, v7}, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->setFinalPosition(F)Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;

    goto/32 :goto_39

    nop

    :goto_e
    iget v5, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->mMaxValue:F

    goto/32 :goto_16

    nop

    :goto_f
    const/4 v2, 0x1

    goto/32 :goto_4b

    nop

    :goto_10
    iget-object v1, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->mSpring:Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;

    goto/32 :goto_2f

    nop

    :goto_11
    iput-boolean v3, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->mEndRequested:Z

    goto/32 :goto_4

    nop

    :goto_12
    return v2

    :goto_13
    goto/32 :goto_0

    nop

    :goto_14
    const v5, 0x7f7fffff    # Float.MAX_VALUE

    goto/32 :goto_23

    nop

    :goto_15
    move-wide/from16 v11, v18

    goto/32 :goto_40

    nop

    :goto_16
    invoke-static {v1, v5}, Ljava/lang/Math;->min(FF)F

    move-result v1

    goto/32 :goto_3

    nop

    :goto_17
    iget-boolean v1, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->mEndRequested:Z

    goto/32 :goto_f

    nop

    :goto_18
    iget-object v6, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->mSpring:Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;

    goto/32 :goto_27

    nop

    :goto_19
    iput v5, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->mPendingPosition:F

    :goto_1a
    goto/32 :goto_37

    nop

    :goto_1b
    move-wide/from16 v16, v5

    goto/32 :goto_1e

    nop

    :goto_1c
    iget v1, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->mPendingPosition:F

    goto/32 :goto_25

    nop

    :goto_1d
    iput v1, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->mValue:F

    goto/32 :goto_e

    nop

    :goto_1e
    move-wide/from16 v18, p1

    goto/32 :goto_1

    nop

    :goto_1f
    iget-object v13, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->mSpring:Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;

    goto/32 :goto_26

    nop

    :goto_20
    iget v1, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->mValue:F

    goto/32 :goto_4c

    nop

    :goto_21
    float-to-double v14, v1

    goto/32 :goto_28

    nop

    :goto_22
    iget v1, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->mValue:F

    goto/32 :goto_21

    nop

    :goto_23
    if-nez v1, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_2d

    nop

    :goto_24
    iput v1, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->mValue:F

    goto/32 :goto_33

    nop

    :goto_25
    cmpl-float v1, v1, v5

    goto/32 :goto_4d

    nop

    :goto_26
    iget v5, v1, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;->mValue:F

    goto/32 :goto_3a

    nop

    :goto_27
    invoke-virtual {v6, v1}, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->setFinalPosition(F)Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;

    goto/32 :goto_19

    nop

    :goto_28
    iget v1, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->mVelocity:F

    goto/32 :goto_9

    nop

    :goto_29
    float-to-double v9, v1

    goto/32 :goto_8

    nop

    :goto_2a
    iput v1, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->mVelocity:F

    :goto_2b
    goto/32 :goto_3b

    nop

    :goto_2c
    iget v7, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->mPendingPosition:F

    goto/32 :goto_d

    nop

    :goto_2d
    iget v1, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->mPendingPosition:F

    goto/32 :goto_7

    nop

    :goto_2e
    if-nez v1, :cond_2

    goto/32 :goto_13

    :cond_2
    goto/32 :goto_49

    nop

    :goto_2f
    invoke-virtual {v1}, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->getFinalPosition()F

    goto/32 :goto_3d

    nop

    :goto_30
    iput v5, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->mValue:F

    goto/32 :goto_3c

    nop

    :goto_31
    iget-object v6, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->mSpring:Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;

    goto/32 :goto_2c

    nop

    :goto_32
    iget v5, v1, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;->mValue:F

    goto/32 :goto_47

    nop

    :goto_33
    iput v4, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->mVelocity:F

    goto/32 :goto_12

    nop

    :goto_34
    div-long v18, p1, v11

    goto/32 :goto_15

    nop

    :goto_35
    goto :goto_2b

    :goto_36
    goto/32 :goto_38

    nop

    :goto_37
    iget-object v1, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->mSpring:Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;

    goto/32 :goto_4e

    nop

    :goto_38
    iget-object v13, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->mSpring:Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;

    goto/32 :goto_22

    nop

    :goto_39
    iput v5, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->mPendingPosition:F

    goto/32 :goto_1f

    nop

    :goto_3a
    float-to-double v14, v5

    goto/32 :goto_45

    nop

    :goto_3b
    iget v1, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->mValue:F

    goto/32 :goto_2

    nop

    :goto_3c
    iget v1, v1, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;->mVelocity:F

    goto/32 :goto_2a

    nop

    :goto_3d
    iget-object v6, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->mSpring:Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;

    goto/32 :goto_20

    nop

    :goto_3e
    iput v1, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->mValue:F

    goto/32 :goto_48

    nop

    :goto_3f
    move-wide/from16 v16, v5

    goto/32 :goto_c

    nop

    :goto_40
    invoke-virtual/range {v6 .. v12}, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->updateValues(DDJ)Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;

    move-result-object v1

    goto/32 :goto_31

    nop

    :goto_41
    iget v5, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->mVelocity:F

    goto/32 :goto_44

    nop

    :goto_42
    iget v1, v1, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;->mVelocity:F

    goto/32 :goto_4f

    nop

    :goto_43
    iget v5, v1, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;->mValue:F

    goto/32 :goto_30

    nop

    :goto_44
    invoke-virtual {v0, v1, v5}, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->isAtEquilibrium(FF)Z

    move-result v1

    goto/32 :goto_2e

    nop

    :goto_45
    iget v1, v1, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;->mVelocity:F

    goto/32 :goto_50

    nop

    :goto_46
    invoke-static {v1, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    goto/32 :goto_1d

    nop

    :goto_47
    iput v5, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->mValue:F

    goto/32 :goto_42

    nop

    :goto_48
    iput v4, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->mVelocity:F

    goto/32 :goto_11

    nop

    :goto_49
    iget-object v1, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringAnimation;->mSpring:Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;

    goto/32 :goto_b

    nop

    :goto_4a
    iget v1, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->mVelocity:F

    goto/32 :goto_29

    nop

    :goto_4b
    const/4 v3, 0x0

    goto/32 :goto_51

    nop

    :goto_4c
    float-to-double v7, v1

    goto/32 :goto_4a

    nop

    :goto_4d
    if-nez v1, :cond_3

    goto/32 :goto_36

    :cond_3
    goto/32 :goto_10

    nop

    :goto_4e
    invoke-virtual {v1}, Lmiuix/overscroller/internal/dynamicanimation/animation/SpringForce;->getFinalPosition()F

    move-result v1

    goto/32 :goto_3e

    nop

    :goto_4f
    iput v1, v0, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->mVelocity:F

    goto/32 :goto_35

    nop

    :goto_50
    float-to-double v5, v1

    goto/32 :goto_3f

    nop

    :goto_51
    const/4 v4, 0x0

    goto/32 :goto_14

    nop
.end method
