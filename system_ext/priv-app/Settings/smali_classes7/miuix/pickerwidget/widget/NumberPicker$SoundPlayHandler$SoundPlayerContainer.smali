.class Lmiuix/pickerwidget/widget/NumberPicker$SoundPlayHandler$SoundPlayerContainer;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/pickerwidget/widget/NumberPicker$SoundPlayHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SoundPlayerContainer"
.end annotation


# instance fields
.field private mPrevPlayTime:J

.field private mRefs:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSoundId:I

.field private mSoundPlayer:Landroid/media/SoundPool;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroidx/collection/ArraySet;

    invoke-direct {v0}, Landroidx/collection/ArraySet;-><init>()V

    iput-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker$SoundPlayHandler$SoundPlayerContainer;->mRefs:Ljava/util/Set;

    return-void
.end method

.method synthetic constructor <init>(Lmiuix/pickerwidget/widget/NumberPicker$1;)V
    .locals 0

    invoke-direct {p0}, Lmiuix/pickerwidget/widget/NumberPicker$SoundPlayHandler$SoundPlayerContainer;-><init>()V

    return-void
.end method


# virtual methods
.method init(Landroid/content/Context;I)V
    .locals 3

    goto/32 :goto_d

    nop

    :goto_0
    return-void

    :goto_1
    sget v1, Lmiuix/pickerwidget/R$raw;->number_picker_value_change:I

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v0, p1, v1, v2}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result p1

    goto/32 :goto_a

    nop

    :goto_3
    iput-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker$SoundPlayHandler$SoundPlayerContainer;->mSoundPlayer:Landroid/media/SoundPool;

    goto/32 :goto_1

    nop

    :goto_4
    new-instance v0, Landroid/media/SoundPool;

    goto/32 :goto_7

    nop

    :goto_5
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto/32 :goto_e

    nop

    :goto_6
    iget-object p0, p0, Lmiuix/pickerwidget/widget/NumberPicker$SoundPlayHandler$SoundPlayerContainer;->mRefs:Ljava/util/Set;

    goto/32 :goto_5

    nop

    :goto_7
    const/4 v1, 0x0

    goto/32 :goto_9

    nop

    :goto_8
    invoke-direct {v0, v2, v2, v1}, Landroid/media/SoundPool;-><init>(III)V

    goto/32 :goto_3

    nop

    :goto_9
    const/4 v2, 0x1

    goto/32 :goto_8

    nop

    :goto_a
    iput p1, p0, Lmiuix/pickerwidget/widget/NumberPicker$SoundPlayHandler$SoundPlayerContainer;->mSoundId:I

    :goto_b
    goto/32 :goto_6

    nop

    :goto_c
    if-eqz v0, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_4

    nop

    :goto_d
    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker$SoundPlayHandler$SoundPlayerContainer;->mSoundPlayer:Landroid/media/SoundPool;

    goto/32 :goto_c

    nop

    :goto_e
    invoke-interface {p0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/32 :goto_0

    nop
.end method

.method play()V
    .locals 9

    goto/32 :goto_b

    nop

    :goto_0
    const/4 v6, 0x0

    goto/32 :goto_c

    nop

    :goto_1
    return-void

    :goto_2
    iget-object v2, p0, Lmiuix/pickerwidget/widget/NumberPicker$SoundPlayHandler$SoundPlayerContainer;->mSoundPlayer:Landroid/media/SoundPool;

    goto/32 :goto_11

    nop

    :goto_3
    invoke-virtual/range {v2 .. v8}, Landroid/media/SoundPool;->play(IFFIIF)I

    goto/32 :goto_5

    nop

    :goto_4
    if-gtz v3, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_7

    nop

    :goto_5
    iput-wide v0, p0, Lmiuix/pickerwidget/widget/NumberPicker$SoundPlayHandler$SoundPlayerContainer;->mPrevPlayTime:J

    :goto_6
    goto/32 :goto_1

    nop

    :goto_7
    iget v3, p0, Lmiuix/pickerwidget/widget/NumberPicker$SoundPlayHandler$SoundPlayerContainer;->mSoundId:I

    goto/32 :goto_a

    nop

    :goto_8
    const-wide/16 v5, 0x32

    goto/32 :goto_f

    nop

    :goto_9
    sub-long v3, v0, v3

    goto/32 :goto_8

    nop

    :goto_a
    const/high16 v4, 0x3f800000    # 1.0f

    goto/32 :goto_e

    nop

    :goto_b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    goto/32 :goto_2

    nop

    :goto_c
    const/4 v7, 0x0

    goto/32 :goto_10

    nop

    :goto_d
    iget-wide v3, p0, Lmiuix/pickerwidget/widget/NumberPicker$SoundPlayHandler$SoundPlayerContainer;->mPrevPlayTime:J

    goto/32 :goto_9

    nop

    :goto_e
    const/high16 v5, 0x3f800000    # 1.0f

    goto/32 :goto_0

    nop

    :goto_f
    cmp-long v3, v3, v5

    goto/32 :goto_4

    nop

    :goto_10
    const/high16 v8, 0x3f800000    # 1.0f

    goto/32 :goto_3

    nop

    :goto_11
    if-nez v2, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_d

    nop
.end method

.method release(I)V
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto/32 :goto_2

    nop

    :goto_1
    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker$SoundPlayHandler$SoundPlayerContainer;->mSoundPlayer:Landroid/media/SoundPool;

    goto/32 :goto_6

    nop

    :goto_2
    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result p1

    goto/32 :goto_c

    nop

    :goto_3
    if-nez p1, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_1

    nop

    :goto_4
    iget-object v0, p0, Lmiuix/pickerwidget/widget/NumberPicker$SoundPlayHandler$SoundPlayerContainer;->mRefs:Ljava/util/Set;

    goto/32 :goto_0

    nop

    :goto_5
    iget-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker$SoundPlayHandler$SoundPlayerContainer;->mRefs:Ljava/util/Set;

    goto/32 :goto_d

    nop

    :goto_6
    if-nez p1, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_9

    nop

    :goto_7
    const/4 p1, 0x0

    goto/32 :goto_a

    nop

    :goto_8
    return-void

    :goto_9
    invoke-virtual {p1}, Landroid/media/SoundPool;->release()V

    goto/32 :goto_7

    nop

    :goto_a
    iput-object p1, p0, Lmiuix/pickerwidget/widget/NumberPicker$SoundPlayHandler$SoundPlayerContainer;->mSoundPlayer:Landroid/media/SoundPool;

    :goto_b
    goto/32 :goto_8

    nop

    :goto_c
    if-nez p1, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_5

    nop

    :goto_d
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result p1

    goto/32 :goto_3

    nop
.end method
