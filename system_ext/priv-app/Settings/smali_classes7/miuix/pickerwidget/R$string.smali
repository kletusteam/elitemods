.class public final Lmiuix/pickerwidget/R$string;
.super Ljava/lang/Object;


# static fields
.field public static final chinese_month:I = 0x7f1208aa

.field public static final date_picker_label_day:I = 0x7f120b15

.field public static final date_picker_label_month:I = 0x7f120b16

.field public static final date_picker_label_year:I = 0x7f120b17

.field public static final empty:I = 0x7f120d2a

.field public static final fmt_chinese_date:I = 0x7f120eee

.field public static final fmt_date:I = 0x7f120eef

.field public static final fmt_date_day:I = 0x7f120ef0

.field public static final fmt_date_long_month:I = 0x7f120ef1

.field public static final fmt_date_long_month_day:I = 0x7f120ef2

.field public static final fmt_date_long_year_month:I = 0x7f120ef3

.field public static final fmt_date_long_year_month_day:I = 0x7f120ef4

.field public static final fmt_date_numeric_day:I = 0x7f120ef5

.field public static final fmt_date_numeric_month:I = 0x7f120ef6

.field public static final fmt_date_numeric_month_day:I = 0x7f120ef7

.field public static final fmt_date_numeric_year:I = 0x7f120ef8

.field public static final fmt_date_numeric_year_month:I = 0x7f120ef9

.field public static final fmt_date_numeric_year_month_day:I = 0x7f120efa

.field public static final fmt_date_short_month:I = 0x7f120efb

.field public static final fmt_date_short_month_day:I = 0x7f120efc

.field public static final fmt_date_short_year_month:I = 0x7f120efd

.field public static final fmt_date_short_year_month_day:I = 0x7f120efe

.field public static final fmt_date_time:I = 0x7f120eff

.field public static final fmt_date_time_timezone:I = 0x7f120f00

.field public static final fmt_date_timezone:I = 0x7f120f01

.field public static final fmt_date_year:I = 0x7f120f02

.field public static final fmt_time:I = 0x7f120f03

.field public static final fmt_time_12hour:I = 0x7f120f04

.field public static final fmt_time_12hour_minute:I = 0x7f120f05

.field public static final fmt_time_12hour_minute_pm:I = 0x7f120f06

.field public static final fmt_time_12hour_minute_second:I = 0x7f120f07

.field public static final fmt_time_12hour_minute_second_millis:I = 0x7f120f08

.field public static final fmt_time_12hour_minute_second_millis_pm:I = 0x7f120f09

.field public static final fmt_time_12hour_minute_second_pm:I = 0x7f120f0a

.field public static final fmt_time_12hour_pm:I = 0x7f120f0b

.field public static final fmt_time_24hour:I = 0x7f120f0c

.field public static final fmt_time_24hour_minute:I = 0x7f120f0d

.field public static final fmt_time_24hour_minute_second:I = 0x7f120f0e

.field public static final fmt_time_24hour_minute_second_millis:I = 0x7f120f0f

.field public static final fmt_time_millis:I = 0x7f120f10

.field public static final fmt_time_minute:I = 0x7f120f11

.field public static final fmt_time_minute_second:I = 0x7f120f12

.field public static final fmt_time_minute_second_millis:I = 0x7f120f13

.field public static final fmt_time_second:I = 0x7f120f14

.field public static final fmt_time_second_millis:I = 0x7f120f15

.field public static final fmt_time_timezone:I = 0x7f120f16

.field public static final fmt_timezone:I = 0x7f120f17

.field public static final fmt_weekday:I = 0x7f120f18

.field public static final fmt_weekday_date:I = 0x7f120f19

.field public static final fmt_weekday_date_time:I = 0x7f120f1a

.field public static final fmt_weekday_date_time_timezone:I = 0x7f120f1b

.field public static final fmt_weekday_date_timezone:I = 0x7f120f1c

.field public static final fmt_weekday_long:I = 0x7f120f1d

.field public static final fmt_weekday_short:I = 0x7f120f1e

.field public static final fmt_weekday_time:I = 0x7f120f1f

.field public static final fmt_weekday_time_timezone:I = 0x7f120f20

.field public static final fmt_weekday_timezone:I = 0x7f120f21

.field public static final miuix_access_state_desc:I = 0x7f1216ed
