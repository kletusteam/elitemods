.class public Lmiuix/animation/internal/AnimTask;
.super Lmiuix/animation/utils/LinkNode;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiuix/animation/utils/LinkNode<",
        "Lmiuix/animation/internal/AnimTask;",
        ">;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# static fields
.field public static final sTaskCount:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public final animStats:Lmiuix/animation/internal/AnimStats;

.field public volatile deltaT:J

.field public volatile info:Lmiuix/animation/internal/TransitionInfo;

.field public volatile startPos:I

.field public volatile toPage:Z

.field public volatile totalT:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lmiuix/animation/internal/AnimTask;->sTaskCount:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiuix/animation/utils/LinkNode;-><init>()V

    new-instance v0, Lmiuix/animation/internal/AnimStats;

    invoke-direct {v0}, Lmiuix/animation/internal/AnimStats;-><init>()V

    iput-object v0, p0, Lmiuix/animation/internal/AnimTask;->animStats:Lmiuix/animation/internal/AnimStats;

    return-void
.end method

.method public static isRunning(B)Z
    .locals 2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v1, 0x2

    if-ne p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0
.end method


# virtual methods
.method public getAnimCount()I
    .locals 0

    iget-object p0, p0, Lmiuix/animation/internal/AnimTask;->animStats:Lmiuix/animation/internal/AnimStats;

    iget p0, p0, Lmiuix/animation/internal/AnimStats;->animCount:I

    return p0
.end method

.method public getTotalAnimCount()I
    .locals 2

    const/4 v0, 0x0

    :goto_0
    if-eqz p0, :cond_0

    iget-object v1, p0, Lmiuix/animation/internal/AnimTask;->animStats:Lmiuix/animation/internal/AnimStats;

    iget v1, v1, Lmiuix/animation/internal/AnimStats;->animCount:I

    add-int/2addr v0, v1

    iget-object p0, p0, Lmiuix/animation/utils/LinkNode;->next:Lmiuix/animation/utils/LinkNode;

    check-cast p0, Lmiuix/animation/internal/AnimTask;

    goto :goto_0

    :cond_0
    return v0
.end method

.method public run()V
    .locals 7

    :try_start_0
    iget-wide v1, p0, Lmiuix/animation/internal/AnimTask;->totalT:J

    iget-wide v3, p0, Lmiuix/animation/internal/AnimTask;->deltaT:J

    const/4 v5, 0x1

    iget-boolean v6, p0, Lmiuix/animation/internal/AnimTask;->toPage:Z

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lmiuix/animation/internal/AnimRunnerTask;->doAnimationFrame(Lmiuix/animation/internal/AnimTask;JJZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    const-string v0, "miuix_anim"

    const-string v1, "doAnimationFrame failed"

    invoke-static {v0, v1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    sget-object p0, Lmiuix/animation/internal/AnimTask;->sTaskCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result p0

    if-nez p0, :cond_0

    sget-object p0, Lmiuix/animation/internal/AnimRunner;->sRunnerHandler:Lmiuix/animation/internal/RunnerHandler;

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method public setup(II)V
    .locals 1

    iget-object v0, p0, Lmiuix/animation/internal/AnimTask;->animStats:Lmiuix/animation/internal/AnimStats;

    invoke-virtual {v0}, Lmiuix/animation/internal/AnimStats;->clear()V

    iget-object v0, p0, Lmiuix/animation/internal/AnimTask;->animStats:Lmiuix/animation/internal/AnimStats;

    iput p2, v0, Lmiuix/animation/internal/AnimStats;->animCount:I

    iput p1, p0, Lmiuix/animation/internal/AnimTask;->startPos:I

    return-void
.end method

.method public start(JJZ)V
    .locals 0

    iput-wide p1, p0, Lmiuix/animation/internal/AnimTask;->totalT:J

    iput-wide p3, p0, Lmiuix/animation/internal/AnimTask;->deltaT:J

    iput-boolean p5, p0, Lmiuix/animation/internal/AnimTask;->toPage:Z

    invoke-static {p0}, Lmiuix/animation/internal/ThreadPoolUtil;->post(Ljava/lang/Runnable;)V

    return-void
.end method

.method updateAnimStats()V
    .locals 6

    goto/32 :goto_23

    nop

    :goto_0
    iget-object v2, p0, Lmiuix/animation/internal/AnimTask;->animStats:Lmiuix/animation/internal/AnimStats;

    goto/32 :goto_11

    nop

    :goto_1
    iput v3, v2, Lmiuix/animation/internal/AnimStats;->endCount:I

    goto/32 :goto_26

    nop

    :goto_2
    iget-byte v3, v3, Lmiuix/animation/internal/AnimInfo;->op:B

    goto/32 :goto_25

    nop

    :goto_3
    iget-byte v3, v3, Lmiuix/animation/internal/AnimInfo;->op:B

    goto/32 :goto_20

    nop

    :goto_4
    goto :goto_1d

    :goto_5
    goto/32 :goto_d

    nop

    :goto_6
    goto :goto_1d

    :goto_7
    goto/32 :goto_36

    nop

    :goto_8
    iget-object v2, p0, Lmiuix/animation/internal/AnimTask;->animStats:Lmiuix/animation/internal/AnimStats;

    goto/32 :goto_1f

    nop

    :goto_9
    iget-object v2, v2, Lmiuix/animation/listener/UpdateInfo;->animInfo:Lmiuix/animation/internal/AnimInfo;

    goto/32 :goto_2f

    nop

    :goto_a
    add-int/2addr v3, v4

    goto/32 :goto_12

    nop

    :goto_b
    if-ne v2, v3, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_4

    nop

    :goto_c
    iget v5, v3, Lmiuix/animation/internal/AnimStats;->initCount:I

    goto/32 :goto_1b

    nop

    :goto_d
    iget-object v2, p0, Lmiuix/animation/internal/AnimTask;->animStats:Lmiuix/animation/internal/AnimStats;

    goto/32 :goto_2a

    nop

    :goto_e
    goto :goto_1d

    :goto_f
    goto/32 :goto_21

    nop

    :goto_10
    if-nez v3, :cond_1

    goto/32 :goto_27

    :cond_1
    goto/32 :goto_38

    nop

    :goto_11
    iget v3, v2, Lmiuix/animation/internal/AnimStats;->cancelCount:I

    goto/32 :goto_2e

    nop

    :goto_12
    iput v3, v2, Lmiuix/animation/internal/AnimStats;->failCount:I

    goto/32 :goto_3c

    nop

    :goto_13
    add-int/2addr v3, v4

    goto/32 :goto_1c

    nop

    :goto_14
    add-int/2addr v3, v4

    goto/32 :goto_1

    nop

    :goto_15
    iget v3, v2, Lmiuix/animation/internal/AnimStats;->endCount:I

    goto/32 :goto_14

    nop

    :goto_16
    const/4 v3, 0x4

    goto/32 :goto_37

    nop

    :goto_17
    iget v2, v2, Lmiuix/animation/internal/AnimStats;->animCount:I

    goto/32 :goto_3a

    nop

    :goto_18
    const/4 v3, 0x5

    goto/32 :goto_b

    nop

    :goto_19
    iput v5, v3, Lmiuix/animation/internal/AnimStats;->initCount:I

    goto/32 :goto_9

    nop

    :goto_1a
    iget-object v2, p0, Lmiuix/animation/internal/AnimTask;->animStats:Lmiuix/animation/internal/AnimStats;

    goto/32 :goto_17

    nop

    :goto_1b
    add-int/2addr v5, v4

    goto/32 :goto_19

    nop

    :goto_1c
    iput v3, v2, Lmiuix/animation/internal/AnimStats;->startCount:I

    :goto_1d
    goto/32 :goto_24

    nop

    :goto_1e
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_32

    nop

    :goto_1f
    iget v3, v2, Lmiuix/animation/internal/AnimStats;->startCount:I

    goto/32 :goto_13

    nop

    :goto_20
    if-eq v3, v4, :cond_2

    goto/32 :goto_29

    :cond_2
    goto/32 :goto_28

    nop

    :goto_21
    iget-object v2, p0, Lmiuix/animation/internal/AnimTask;->animStats:Lmiuix/animation/internal/AnimStats;

    goto/32 :goto_15

    nop

    :goto_22
    if-lt v0, v1, :cond_3

    goto/32 :goto_31

    :cond_3
    goto/32 :goto_2b

    nop

    :goto_23
    iget v0, p0, Lmiuix/animation/internal/AnimTask;->startPos:I

    goto/32 :goto_3e

    nop

    :goto_24
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_30

    nop

    :goto_25
    const/4 v4, 0x1

    goto/32 :goto_10

    nop

    :goto_26
    goto :goto_1d

    :goto_27
    goto/32 :goto_8

    nop

    :goto_28
    goto :goto_27

    :goto_29
    goto/32 :goto_2d

    nop

    :goto_2a
    iget v3, v2, Lmiuix/animation/internal/AnimStats;->failCount:I

    goto/32 :goto_a

    nop

    :goto_2b
    iget-object v2, p0, Lmiuix/animation/internal/AnimTask;->info:Lmiuix/animation/internal/TransitionInfo;

    goto/32 :goto_33

    nop

    :goto_2c
    return-void

    :goto_2d
    iget-object v3, p0, Lmiuix/animation/internal/AnimTask;->animStats:Lmiuix/animation/internal/AnimStats;

    goto/32 :goto_c

    nop

    :goto_2e
    add-int/2addr v3, v4

    goto/32 :goto_3f

    nop

    :goto_2f
    iget-byte v2, v2, Lmiuix/animation/internal/AnimInfo;->op:B

    goto/32 :goto_35

    nop

    :goto_30
    goto :goto_3b

    :goto_31
    goto/32 :goto_2c

    nop

    :goto_32
    check-cast v2, Lmiuix/animation/listener/UpdateInfo;

    goto/32 :goto_34

    nop

    :goto_33
    iget-object v2, v2, Lmiuix/animation/internal/TransitionInfo;->updateList:Ljava/util/List;

    goto/32 :goto_1e

    nop

    :goto_34
    if-eqz v2, :cond_4

    goto/32 :goto_7

    :cond_4
    goto/32 :goto_6

    nop

    :goto_35
    const/4 v3, 0x3

    goto/32 :goto_39

    nop

    :goto_36
    iget-object v3, v2, Lmiuix/animation/listener/UpdateInfo;->animInfo:Lmiuix/animation/internal/AnimInfo;

    goto/32 :goto_2

    nop

    :goto_37
    if-ne v2, v3, :cond_5

    goto/32 :goto_3d

    :cond_5
    goto/32 :goto_18

    nop

    :goto_38
    iget-object v3, v2, Lmiuix/animation/listener/UpdateInfo;->animInfo:Lmiuix/animation/internal/AnimInfo;

    goto/32 :goto_3

    nop

    :goto_39
    if-ne v2, v3, :cond_6

    goto/32 :goto_f

    :cond_6
    goto/32 :goto_16

    nop

    :goto_3a
    add-int/2addr v1, v2

    :goto_3b
    goto/32 :goto_22

    nop

    :goto_3c
    goto/16 :goto_1d

    :goto_3d
    goto/32 :goto_0

    nop

    :goto_3e
    iget v1, p0, Lmiuix/animation/internal/AnimTask;->startPos:I

    goto/32 :goto_1a

    nop

    :goto_3f
    iput v3, v2, Lmiuix/animation/internal/AnimStats;->cancelCount:I

    goto/32 :goto_e

    nop
.end method
