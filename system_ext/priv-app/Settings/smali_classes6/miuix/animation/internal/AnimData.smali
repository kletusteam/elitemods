.class public Lmiuix/animation/internal/AnimData;
.super Ljava/lang/Object;


# instance fields
.field public delay:J

.field public ease:Lmiuix/animation/utils/EaseManager$EaseStyle;

.field public frameCount:I

.field public initTime:J

.field public isCompleted:Z

.field justEnd:Z

.field logEnabled:Z

.field public op:B

.field public progress:D

.field public property:Lmiuix/animation/property/FloatProperty;

.field public startTime:J

.field public startValue:D

.field public targetValue:D

.field public tintMode:I

.field public value:D

.field public velocity:D


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lmiuix/animation/internal/AnimData;->tintMode:I

    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lmiuix/animation/internal/AnimData;->startValue:D

    iput-wide v0, p0, Lmiuix/animation/internal/AnimData;->targetValue:D

    iput-wide v0, p0, Lmiuix/animation/internal/AnimData;->value:D

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_3

    nop

    :goto_1
    iput-object v0, p0, Lmiuix/animation/internal/AnimData;->ease:Lmiuix/animation/utils/EaseManager$EaseStyle;

    goto/32 :goto_2

    nop

    :goto_2
    return-void

    :goto_3
    iput-object v0, p0, Lmiuix/animation/internal/AnimData;->property:Lmiuix/animation/property/FloatProperty;

    goto/32 :goto_1

    nop
.end method

.method from(Lmiuix/animation/listener/UpdateInfo;Lmiuix/animation/base/AnimConfig;Lmiuix/animation/base/AnimSpecialConfig;)V
    .locals 2

    goto/32 :goto_16

    nop

    :goto_0
    iput p1, p0, Lmiuix/animation/internal/AnimData;->tintMode:I

    goto/32 :goto_22

    nop

    :goto_1
    iput-wide v0, p0, Lmiuix/animation/internal/AnimData;->initTime:J

    goto/32 :goto_1d

    nop

    :goto_2
    iget-object p1, p1, Lmiuix/animation/listener/UpdateInfo;->animInfo:Lmiuix/animation/internal/AnimInfo;

    goto/32 :goto_4

    nop

    :goto_3
    return-void

    :goto_4
    iget-boolean p1, p1, Lmiuix/animation/internal/AnimInfo;->justEnd:Z

    goto/32 :goto_b

    nop

    :goto_5
    iget-object v0, p1, Lmiuix/animation/listener/UpdateInfo;->animInfo:Lmiuix/animation/internal/AnimInfo;

    goto/32 :goto_20

    nop

    :goto_6
    iget-wide v0, v0, Lmiuix/animation/internal/AnimInfo;->startTime:J

    goto/32 :goto_1b

    nop

    :goto_7
    iget-wide v0, v0, Lmiuix/animation/internal/AnimInfo;->value:D

    goto/32 :goto_17

    nop

    :goto_8
    iput-wide v0, p0, Lmiuix/animation/internal/AnimData;->startValue:D

    goto/32 :goto_f

    nop

    :goto_9
    invoke-static {p2, p3}, Lmiuix/animation/internal/AnimConfigUtils;->getDelay(Lmiuix/animation/base/AnimConfig;Lmiuix/animation/base/AnimSpecialConfig;)J

    move-result-wide p1

    goto/32 :goto_e

    nop

    :goto_a
    iget-object v0, p1, Lmiuix/animation/listener/UpdateInfo;->animInfo:Lmiuix/animation/internal/AnimInfo;

    goto/32 :goto_15

    nop

    :goto_b
    iput-boolean p1, p0, Lmiuix/animation/internal/AnimData;->justEnd:Z

    goto/32 :goto_d

    nop

    :goto_c
    iput-wide v0, p0, Lmiuix/animation/internal/AnimData;->progress:D

    goto/32 :goto_24

    nop

    :goto_d
    invoke-static {p2, p3}, Lmiuix/animation/internal/AnimConfigUtils;->getTintMode(Lmiuix/animation/base/AnimConfig;Lmiuix/animation/base/AnimSpecialConfig;)I

    move-result p1

    goto/32 :goto_0

    nop

    :goto_e
    iput-wide p1, p0, Lmiuix/animation/internal/AnimData;->delay:J

    goto/32 :goto_3

    nop

    :goto_f
    iget-object v0, p1, Lmiuix/animation/listener/UpdateInfo;->animInfo:Lmiuix/animation/internal/AnimInfo;

    goto/32 :goto_11

    nop

    :goto_10
    iput-boolean v0, p0, Lmiuix/animation/internal/AnimData;->isCompleted:Z

    goto/32 :goto_2

    nop

    :goto_11
    iget-wide v0, v0, Lmiuix/animation/internal/AnimInfo;->targetValue:D

    goto/32 :goto_18

    nop

    :goto_12
    iget v0, p1, Lmiuix/animation/listener/UpdateInfo;->frameCount:I

    goto/32 :goto_19

    nop

    :goto_13
    iput-object p1, p0, Lmiuix/animation/internal/AnimData;->ease:Lmiuix/animation/utils/EaseManager$EaseStyle;

    goto/32 :goto_9

    nop

    :goto_14
    iget-boolean v0, p1, Lmiuix/animation/listener/UpdateInfo;->isCompleted:Z

    goto/32 :goto_10

    nop

    :goto_15
    iget-wide v0, v0, Lmiuix/animation/internal/AnimInfo;->initTime:J

    goto/32 :goto_1

    nop

    :goto_16
    iget-object v0, p1, Lmiuix/animation/listener/UpdateInfo;->property:Lmiuix/animation/property/FloatProperty;

    goto/32 :goto_1f

    nop

    :goto_17
    iput-wide v0, p0, Lmiuix/animation/internal/AnimData;->value:D

    goto/32 :goto_14

    nop

    :goto_18
    iput-wide v0, p0, Lmiuix/animation/internal/AnimData;->targetValue:D

    goto/32 :goto_21

    nop

    :goto_19
    iput v0, p0, Lmiuix/animation/internal/AnimData;->frameCount:I

    goto/32 :goto_5

    nop

    :goto_1a
    iget-wide v0, v0, Lmiuix/animation/internal/AnimInfo;->startValue:D

    goto/32 :goto_8

    nop

    :goto_1b
    iput-wide v0, p0, Lmiuix/animation/internal/AnimData;->startTime:J

    goto/32 :goto_1c

    nop

    :goto_1c
    iget-object v0, p1, Lmiuix/animation/listener/UpdateInfo;->animInfo:Lmiuix/animation/internal/AnimInfo;

    goto/32 :goto_23

    nop

    :goto_1d
    iget-object v0, p1, Lmiuix/animation/listener/UpdateInfo;->animInfo:Lmiuix/animation/internal/AnimInfo;

    goto/32 :goto_6

    nop

    :goto_1e
    iget-wide v0, p1, Lmiuix/animation/listener/UpdateInfo;->velocity:D

    goto/32 :goto_25

    nop

    :goto_1f
    iput-object v0, p0, Lmiuix/animation/internal/AnimData;->property:Lmiuix/animation/property/FloatProperty;

    goto/32 :goto_1e

    nop

    :goto_20
    iget-byte v0, v0, Lmiuix/animation/internal/AnimInfo;->op:B

    goto/32 :goto_26

    nop

    :goto_21
    iget-object v0, p1, Lmiuix/animation/listener/UpdateInfo;->animInfo:Lmiuix/animation/internal/AnimInfo;

    goto/32 :goto_7

    nop

    :goto_22
    invoke-static {p2, p3}, Lmiuix/animation/internal/AnimConfigUtils;->getEase(Lmiuix/animation/base/AnimConfig;Lmiuix/animation/base/AnimSpecialConfig;)Lmiuix/animation/utils/EaseManager$EaseStyle;

    move-result-object p1

    goto/32 :goto_13

    nop

    :goto_23
    iget-wide v0, v0, Lmiuix/animation/internal/AnimInfo;->progress:D

    goto/32 :goto_c

    nop

    :goto_24
    iget-object v0, p1, Lmiuix/animation/listener/UpdateInfo;->animInfo:Lmiuix/animation/internal/AnimInfo;

    goto/32 :goto_1a

    nop

    :goto_25
    iput-wide v0, p0, Lmiuix/animation/internal/AnimData;->velocity:D

    goto/32 :goto_12

    nop

    :goto_26
    iput-byte v0, p0, Lmiuix/animation/internal/AnimData;->op:B

    goto/32 :goto_a

    nop
.end method

.method reset()V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    iput v0, p0, Lmiuix/animation/internal/AnimData;->frameCount:I

    goto/32 :goto_2

    nop

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_4

    nop

    :goto_2
    iput-boolean v0, p0, Lmiuix/animation/internal/AnimData;->justEnd:Z

    goto/32 :goto_3

    nop

    :goto_3
    return-void

    :goto_4
    iput-boolean v0, p0, Lmiuix/animation/internal/AnimData;->isCompleted:Z

    goto/32 :goto_0

    nop
.end method

.method public setOp(B)V
    .locals 1

    iput-byte p1, p0, Lmiuix/animation/internal/AnimData;->op:B

    if-eqz p1, :cond_1

    const/4 v0, 0x2

    if-le p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    iput-boolean p1, p0, Lmiuix/animation/internal/AnimData;->isCompleted:Z

    return-void
.end method

.method to(Lmiuix/animation/listener/UpdateInfo;)V
    .locals 3

    goto/32 :goto_16

    nop

    :goto_0
    iget-wide v1, p0, Lmiuix/animation/internal/AnimData;->initTime:J

    goto/32 :goto_25

    nop

    :goto_1
    iget-object v0, p1, Lmiuix/animation/listener/UpdateInfo;->animInfo:Lmiuix/animation/internal/AnimInfo;

    goto/32 :goto_24

    nop

    :goto_2
    invoke-virtual {p0}, Lmiuix/animation/internal/AnimData;->clear()V

    goto/32 :goto_15

    nop

    :goto_3
    iget-object v0, p1, Lmiuix/animation/listener/UpdateInfo;->animInfo:Lmiuix/animation/internal/AnimInfo;

    goto/32 :goto_6

    nop

    :goto_4
    iget v1, p0, Lmiuix/animation/internal/AnimData;->tintMode:I

    goto/32 :goto_22

    nop

    :goto_5
    iget-object v0, p1, Lmiuix/animation/listener/UpdateInfo;->animInfo:Lmiuix/animation/internal/AnimInfo;

    goto/32 :goto_c

    nop

    :goto_6
    iget-byte v1, p0, Lmiuix/animation/internal/AnimData;->op:B

    goto/32 :goto_a

    nop

    :goto_7
    iget-boolean v0, p0, Lmiuix/animation/internal/AnimData;->justEnd:Z

    goto/32 :goto_1a

    nop

    :goto_8
    iget-object v0, p1, Lmiuix/animation/listener/UpdateInfo;->animInfo:Lmiuix/animation/internal/AnimInfo;

    goto/32 :goto_13

    nop

    :goto_9
    iput-boolean v0, p1, Lmiuix/animation/listener/UpdateInfo;->isCompleted:Z

    goto/32 :goto_8

    nop

    :goto_a
    iput-byte v1, v0, Lmiuix/animation/internal/AnimInfo;->op:B

    goto/32 :goto_d

    nop

    :goto_b
    iput-wide v1, v0, Lmiuix/animation/internal/AnimInfo;->value:D

    goto/32 :goto_12

    nop

    :goto_c
    iget-wide v1, p0, Lmiuix/animation/internal/AnimData;->progress:D

    goto/32 :goto_1c

    nop

    :goto_d
    iget-object v0, p1, Lmiuix/animation/listener/UpdateInfo;->animInfo:Lmiuix/animation/internal/AnimInfo;

    goto/32 :goto_10

    nop

    :goto_e
    iget-wide v1, p0, Lmiuix/animation/internal/AnimData;->startTime:J

    goto/32 :goto_1d

    nop

    :goto_f
    iget-object v0, p1, Lmiuix/animation/listener/UpdateInfo;->animInfo:Lmiuix/animation/internal/AnimInfo;

    goto/32 :goto_0

    nop

    :goto_10
    iget-wide v1, p0, Lmiuix/animation/internal/AnimData;->delay:J

    goto/32 :goto_14

    nop

    :goto_11
    iput-wide v1, v0, Lmiuix/animation/internal/AnimInfo;->targetValue:D

    goto/32 :goto_17

    nop

    :goto_12
    iget-wide v0, p0, Lmiuix/animation/internal/AnimData;->velocity:D

    goto/32 :goto_19

    nop

    :goto_13
    iget-wide v1, p0, Lmiuix/animation/internal/AnimData;->value:D

    goto/32 :goto_b

    nop

    :goto_14
    iput-wide v1, v0, Lmiuix/animation/internal/AnimInfo;->delay:J

    goto/32 :goto_1e

    nop

    :goto_15
    return-void

    :goto_16
    iget v0, p0, Lmiuix/animation/internal/AnimData;->frameCount:I

    goto/32 :goto_1f

    nop

    :goto_17
    iget-boolean v0, p0, Lmiuix/animation/internal/AnimData;->isCompleted:Z

    goto/32 :goto_9

    nop

    :goto_18
    iget-wide v1, p0, Lmiuix/animation/internal/AnimData;->targetValue:D

    goto/32 :goto_11

    nop

    :goto_19
    iput-wide v0, p1, Lmiuix/animation/listener/UpdateInfo;->velocity:D

    goto/32 :goto_1b

    nop

    :goto_1a
    iput-boolean v0, p1, Lmiuix/animation/internal/AnimInfo;->justEnd:Z

    goto/32 :goto_2

    nop

    :goto_1b
    iget-object p1, p1, Lmiuix/animation/listener/UpdateInfo;->animInfo:Lmiuix/animation/internal/AnimInfo;

    goto/32 :goto_7

    nop

    :goto_1c
    iput-wide v1, v0, Lmiuix/animation/internal/AnimInfo;->progress:D

    goto/32 :goto_1

    nop

    :goto_1d
    iput-wide v1, v0, Lmiuix/animation/internal/AnimInfo;->startTime:J

    goto/32 :goto_5

    nop

    :goto_1e
    iget-object v0, p1, Lmiuix/animation/listener/UpdateInfo;->animInfo:Lmiuix/animation/internal/AnimInfo;

    goto/32 :goto_4

    nop

    :goto_1f
    iput v0, p1, Lmiuix/animation/listener/UpdateInfo;->frameCount:I

    goto/32 :goto_3

    nop

    :goto_20
    iput-wide v1, v0, Lmiuix/animation/internal/AnimInfo;->startValue:D

    goto/32 :goto_21

    nop

    :goto_21
    iget-object v0, p1, Lmiuix/animation/listener/UpdateInfo;->animInfo:Lmiuix/animation/internal/AnimInfo;

    goto/32 :goto_18

    nop

    :goto_22
    iput v1, v0, Lmiuix/animation/internal/AnimInfo;->tintMode:I

    goto/32 :goto_f

    nop

    :goto_23
    iget-object v0, p1, Lmiuix/animation/listener/UpdateInfo;->animInfo:Lmiuix/animation/internal/AnimInfo;

    goto/32 :goto_e

    nop

    :goto_24
    iget-wide v1, p0, Lmiuix/animation/internal/AnimData;->startValue:D

    goto/32 :goto_20

    nop

    :goto_25
    iput-wide v1, v0, Lmiuix/animation/internal/AnimInfo;->initTime:J

    goto/32 :goto_23

    nop
.end method
