.class public final Lmiuix/animation/physics/SpringAnimation;
.super Lmiuix/animation/physics/DynamicAnimation;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiuix/animation/physics/DynamicAnimation<",
        "Lmiuix/animation/physics/SpringAnimation;",
        ">;"
    }
.end annotation


# instance fields
.field private mEndRequested:Z

.field private mPendingPosition:F

.field private mSpring:Lmiuix/animation/physics/SpringForce;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lmiuix/animation/property/FloatProperty;F)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">(TK;",
            "Lmiuix/animation/property/FloatProperty<",
            "TK;>;F)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lmiuix/animation/physics/DynamicAnimation;-><init>(Ljava/lang/Object;Lmiuix/animation/property/FloatProperty;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    const p1, 0x7f7fffff    # Float.MAX_VALUE

    iput p1, p0, Lmiuix/animation/physics/SpringAnimation;->mPendingPosition:F

    const/4 p1, 0x0

    iput-boolean p1, p0, Lmiuix/animation/physics/SpringAnimation;->mEndRequested:Z

    new-instance p1, Lmiuix/animation/physics/SpringForce;

    invoke-direct {p1, p3}, Lmiuix/animation/physics/SpringForce;-><init>(F)V

    iput-object p1, p0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    return-void
.end method

.method private sanityCheck()V
    .locals 4

    iget-object v0, p0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lmiuix/animation/physics/SpringForce;->getFinalPosition()F

    move-result v0

    float-to-double v0, v0

    iget v2, p0, Lmiuix/animation/physics/DynamicAnimation;->mMaxValue:F

    float-to-double v2, v2

    cmpl-double v2, v0, v2

    if-gtz v2, :cond_1

    iget p0, p0, Lmiuix/animation/physics/DynamicAnimation;->mMinValue:F

    float-to-double v2, p0

    cmpg-double p0, v0, v2

    if-ltz p0, :cond_0

    return-void

    :cond_0
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Final position of the spring cannot be less than the min value."

    invoke-direct {p0, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Final position of the spring cannot be greater than the max value."

    invoke-direct {p0, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_2
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Incomplete SpringAnimation: Either final position or a spring force needs to be set."

    invoke-direct {p0, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public canSkipToEnd()Z
    .locals 4

    iget-object p0, p0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    iget-wide v0, p0, Lmiuix/animation/physics/SpringForce;->mDampingRatio:D

    const-wide/16 v2, 0x0

    cmpl-double p0, v0, v2

    if-lez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public getSpring()Lmiuix/animation/physics/SpringForce;
    .locals 0

    iget-object p0, p0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    return-object p0
.end method

.method isAtEquilibrium(FF)Z
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return p0

    :goto_1
    iget-object p0, p0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {p0, p1, p2}, Lmiuix/animation/physics/SpringForce;->isAtEquilibrium(FF)Z

    move-result p0

    goto/32 :goto_0

    nop
.end method

.method setValueThreshold(F)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-void
.end method

.method public skipToEnd()V
    .locals 1

    invoke-virtual {p0}, Lmiuix/animation/physics/SpringAnimation;->canSkipToEnd()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lmiuix/animation/physics/DynamicAnimation;->getAnimationHandler()Lmiuix/animation/physics/AnimationHandler;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/animation/physics/AnimationHandler;->isCurrentThread()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lmiuix/animation/physics/DynamicAnimation;->mRunning:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/animation/physics/SpringAnimation;->mEndRequested:Z

    :cond_0
    return-void

    :cond_1
    new-instance p0, Landroid/util/AndroidRuntimeException;

    const-string v0, "Animations may only be started on the same thread as the animation handler"

    invoke-direct {p0, v0}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_2
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Spring animations can only come to an end when there is damping"

    invoke-direct {p0, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public start()V
    .locals 3

    invoke-direct {p0}, Lmiuix/animation/physics/SpringAnimation;->sanityCheck()V

    iget-object v0, p0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    invoke-virtual {p0}, Lmiuix/animation/physics/DynamicAnimation;->getValueThreshold()F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Lmiuix/animation/physics/SpringForce;->setValueThreshold(D)V

    invoke-super {p0}, Lmiuix/animation/physics/DynamicAnimation;->start()V

    return-void
.end method

.method updateValueAndVelocity(J)Z
    .locals 20

    goto/32 :goto_9

    nop

    :goto_0
    invoke-virtual {v6, v7}, Lmiuix/animation/physics/SpringForce;->setFinalPosition(F)Lmiuix/animation/physics/SpringForce;

    goto/32 :goto_39

    nop

    :goto_1
    iget v1, v0, Lmiuix/animation/physics/DynamicAnimation;->mVelocity:F

    goto/32 :goto_29

    nop

    :goto_2
    iput-boolean v3, v0, Lmiuix/animation/physics/SpringAnimation;->mEndRequested:Z

    goto/32 :goto_32

    nop

    :goto_3
    iget v5, v0, Lmiuix/animation/physics/DynamicAnimation;->mMinValue:F

    goto/32 :goto_47

    nop

    :goto_4
    const/4 v3, 0x0

    goto/32 :goto_4b

    nop

    :goto_5
    invoke-virtual/range {v6 .. v12}, Lmiuix/animation/physics/SpringForce;->updateValues(DDJ)Lmiuix/animation/physics/DynamicAnimation$MassState;

    move-result-object v1

    goto/32 :goto_1d

    nop

    :goto_6
    iput v1, v0, Lmiuix/animation/physics/DynamicAnimation;->mValue:F

    goto/32 :goto_3d

    nop

    :goto_7
    iput v5, v0, Lmiuix/animation/physics/DynamicAnimation;->mValue:F

    goto/32 :goto_40

    nop

    :goto_8
    iget-object v6, v0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    goto/32 :goto_50

    nop

    :goto_9
    move-object/from16 v0, p0

    goto/32 :goto_41

    nop

    :goto_a
    iput v5, v0, Lmiuix/animation/physics/SpringAnimation;->mPendingPosition:F

    :goto_b
    goto/32 :goto_3f

    nop

    :goto_c
    invoke-virtual/range {v13 .. v19}, Lmiuix/animation/physics/SpringForce;->updateValues(DDJ)Lmiuix/animation/physics/DynamicAnimation$MassState;

    move-result-object v1

    goto/32 :goto_45

    nop

    :goto_d
    iget v1, v0, Lmiuix/animation/physics/DynamicAnimation;->mValue:F

    goto/32 :goto_11

    nop

    :goto_e
    float-to-double v14, v5

    goto/32 :goto_48

    nop

    :goto_f
    iget-object v13, v0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    goto/32 :goto_4c

    nop

    :goto_10
    iget-object v1, v0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    goto/32 :goto_42

    nop

    :goto_11
    float-to-double v14, v1

    goto/32 :goto_35

    nop

    :goto_12
    if-nez v6, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_17

    nop

    :goto_13
    iget v7, v0, Lmiuix/animation/physics/SpringAnimation;->mPendingPosition:F

    goto/32 :goto_0

    nop

    :goto_14
    iput v4, v0, Lmiuix/animation/physics/DynamicAnimation;->mVelocity:F

    goto/32 :goto_3a

    nop

    :goto_15
    goto :goto_27

    :goto_16
    goto/32 :goto_2a

    nop

    :goto_17
    iget-object v6, v0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    goto/32 :goto_46

    nop

    :goto_18
    const-wide/16 v11, 0x2

    goto/32 :goto_20

    nop

    :goto_19
    iget v1, v0, Lmiuix/animation/physics/SpringAnimation;->mPendingPosition:F

    goto/32 :goto_3e

    nop

    :goto_1a
    if-nez v1, :cond_1

    goto/32 :goto_16

    :cond_1
    goto/32 :goto_2c

    nop

    :goto_1b
    const/4 v2, 0x1

    goto/32 :goto_4

    nop

    :goto_1c
    iget v1, v0, Lmiuix/animation/physics/SpringAnimation;->mPendingPosition:F

    goto/32 :goto_30

    nop

    :goto_1d
    iget-object v6, v0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    goto/32 :goto_13

    nop

    :goto_1e
    return v3

    :goto_1f
    iput v1, v0, Lmiuix/animation/physics/DynamicAnimation;->mValue:F

    goto/32 :goto_36

    nop

    :goto_20
    div-long v18, p1, v11

    goto/32 :goto_3c

    nop

    :goto_21
    if-nez v1, :cond_2

    goto/32 :goto_3b

    :cond_2
    goto/32 :goto_10

    nop

    :goto_22
    invoke-virtual/range {v13 .. v19}, Lmiuix/animation/physics/SpringForce;->updateValues(DDJ)Lmiuix/animation/physics/DynamicAnimation$MassState;

    move-result-object v1

    goto/32 :goto_23

    nop

    :goto_23
    iget v5, v1, Lmiuix/animation/physics/DynamicAnimation$MassState;->mValue:F

    goto/32 :goto_7

    nop

    :goto_24
    invoke-static {v1, v5}, Ljava/lang/Math;->min(FF)F

    move-result v1

    goto/32 :goto_6

    nop

    :goto_25
    iget v1, v1, Lmiuix/animation/physics/DynamicAnimation$MassState;->mVelocity:F

    goto/32 :goto_4f

    nop

    :goto_26
    iput v1, v0, Lmiuix/animation/physics/DynamicAnimation;->mVelocity:F

    :goto_27
    goto/32 :goto_49

    nop

    :goto_28
    const v5, 0x7f7fffff    # Float.MAX_VALUE

    goto/32 :goto_4e

    nop

    :goto_29
    float-to-double v9, v1

    goto/32 :goto_18

    nop

    :goto_2a
    iget-object v13, v0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    goto/32 :goto_d

    nop

    :goto_2b
    iput v1, v0, Lmiuix/animation/physics/DynamicAnimation;->mValue:F

    goto/32 :goto_37

    nop

    :goto_2c
    iget-object v1, v0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    goto/32 :goto_2d

    nop

    :goto_2d
    invoke-virtual {v1}, Lmiuix/animation/physics/SpringForce;->getFinalPosition()F

    goto/32 :goto_8

    nop

    :goto_2e
    iput v1, v0, Lmiuix/animation/physics/DynamicAnimation;->mValue:F

    goto/32 :goto_14

    nop

    :goto_2f
    iput v5, v0, Lmiuix/animation/physics/DynamicAnimation;->mValue:F

    goto/32 :goto_25

    nop

    :goto_30
    cmpl-float v6, v1, v5

    goto/32 :goto_12

    nop

    :goto_31
    float-to-double v5, v1

    goto/32 :goto_44

    nop

    :goto_32
    return v2

    :goto_33
    goto/32 :goto_19

    nop

    :goto_34
    float-to-double v7, v1

    goto/32 :goto_1

    nop

    :goto_35
    iget v1, v0, Lmiuix/animation/physics/DynamicAnimation;->mVelocity:F

    goto/32 :goto_31

    nop

    :goto_36
    iget v5, v0, Lmiuix/animation/physics/DynamicAnimation;->mMaxValue:F

    goto/32 :goto_24

    nop

    :goto_37
    iput v4, v0, Lmiuix/animation/physics/DynamicAnimation;->mVelocity:F

    goto/32 :goto_2

    nop

    :goto_38
    invoke-virtual {v0, v1, v5}, Lmiuix/animation/physics/SpringAnimation;->isAtEquilibrium(FF)Z

    move-result v1

    goto/32 :goto_21

    nop

    :goto_39
    iput v5, v0, Lmiuix/animation/physics/SpringAnimation;->mPendingPosition:F

    goto/32 :goto_f

    nop

    :goto_3a
    return v2

    :goto_3b
    goto/32 :goto_1e

    nop

    :goto_3c
    move-wide/from16 v11, v18

    goto/32 :goto_5

    nop

    :goto_3d
    iget v5, v0, Lmiuix/animation/physics/DynamicAnimation;->mVelocity:F

    goto/32 :goto_38

    nop

    :goto_3e
    cmpl-float v1, v1, v5

    goto/32 :goto_1a

    nop

    :goto_3f
    iget-object v1, v0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    goto/32 :goto_51

    nop

    :goto_40
    iget v1, v1, Lmiuix/animation/physics/DynamicAnimation$MassState;->mVelocity:F

    goto/32 :goto_26

    nop

    :goto_41
    iget-boolean v1, v0, Lmiuix/animation/physics/SpringAnimation;->mEndRequested:Z

    goto/32 :goto_1b

    nop

    :goto_42
    invoke-virtual {v1}, Lmiuix/animation/physics/SpringForce;->getFinalPosition()F

    move-result v1

    goto/32 :goto_2e

    nop

    :goto_43
    move-wide/from16 v18, p1

    goto/32 :goto_22

    nop

    :goto_44
    move-wide/from16 v16, v5

    goto/32 :goto_43

    nop

    :goto_45
    iget v5, v1, Lmiuix/animation/physics/DynamicAnimation$MassState;->mValue:F

    goto/32 :goto_2f

    nop

    :goto_46
    invoke-virtual {v6, v1}, Lmiuix/animation/physics/SpringForce;->setFinalPosition(F)Lmiuix/animation/physics/SpringForce;

    goto/32 :goto_a

    nop

    :goto_47
    invoke-static {v1, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    goto/32 :goto_1f

    nop

    :goto_48
    iget v1, v1, Lmiuix/animation/physics/DynamicAnimation$MassState;->mVelocity:F

    goto/32 :goto_4a

    nop

    :goto_49
    iget v1, v0, Lmiuix/animation/physics/DynamicAnimation;->mValue:F

    goto/32 :goto_3

    nop

    :goto_4a
    float-to-double v5, v1

    goto/32 :goto_4d

    nop

    :goto_4b
    const/4 v4, 0x0

    goto/32 :goto_28

    nop

    :goto_4c
    iget v5, v1, Lmiuix/animation/physics/DynamicAnimation$MassState;->mValue:F

    goto/32 :goto_e

    nop

    :goto_4d
    move-wide/from16 v16, v5

    goto/32 :goto_c

    nop

    :goto_4e
    if-nez v1, :cond_3

    goto/32 :goto_33

    :cond_3
    goto/32 :goto_1c

    nop

    :goto_4f
    iput v1, v0, Lmiuix/animation/physics/DynamicAnimation;->mVelocity:F

    goto/32 :goto_15

    nop

    :goto_50
    iget v1, v0, Lmiuix/animation/physics/DynamicAnimation;->mValue:F

    goto/32 :goto_34

    nop

    :goto_51
    invoke-virtual {v1}, Lmiuix/animation/physics/SpringForce;->getFinalPosition()F

    move-result v1

    goto/32 :goto_2b

    nop
.end method
