.class Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;
.super Ljava/lang/Object;

# interfaces
.implements Lmiuix/appcompat/app/ActionBar$FragmentViewPagerChangeListener;


# instance fields
.field mBaseItem:I

.field mBaseItemUpdated:Z

.field mIncomingPosition:I

.field mListView:Landroid/view/ViewGroup;

.field mPagerAdapter:Lmiuix/appcompat/internal/app/widget/DynamicFragmentPagerAdapter;

.field mScrollBasePosition:I

.field mViewPager:Lmiuix/viewpager/widget/ViewPager;

.field sList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field sRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Lmiuix/viewpager/widget/ViewPager;Lmiuix/appcompat/internal/app/widget/DynamicFragmentPagerAdapter;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->sRect:Landroid/graphics/Rect;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->sList:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mBaseItem:I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mBaseItemUpdated:Z

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mScrollBasePosition:I

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mIncomingPosition:I

    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mListView:Landroid/view/ViewGroup;

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mViewPager:Lmiuix/viewpager/widget/ViewPager;

    iput-object p2, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mPagerAdapter:Lmiuix/appcompat/internal/app/widget/DynamicFragmentPagerAdapter;

    return-void
.end method


# virtual methods
.method clearTranslation(Landroid/view/ViewGroup;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    check-cast p1, Landroid/view/View;

    goto/32 :goto_f

    nop

    :goto_1
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->sList:Ljava/util/ArrayList;

    goto/32 :goto_b

    nop

    :goto_2
    goto :goto_a

    :goto_3
    goto/32 :goto_6

    nop

    :goto_4
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    goto/32 :goto_c

    nop

    :goto_5
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_0

    nop

    :goto_6
    return-void

    :goto_7
    if-nez p1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_5

    nop

    :goto_8
    iget-object p0, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->sList:Ljava/util/ArrayList;

    goto/32 :goto_9

    nop

    :goto_9
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_a
    goto/32 :goto_10

    nop

    :goto_b
    invoke-virtual {p0, p1, v0}, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->fillList(Landroid/view/ViewGroup;Ljava/util/ArrayList;)V

    goto/32 :goto_d

    nop

    :goto_c
    if-eqz p1, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_8

    nop

    :goto_d
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->sList:Ljava/util/ArrayList;

    goto/32 :goto_4

    nop

    :goto_e
    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    goto/32 :goto_2

    nop

    :goto_f
    const/4 v0, 0x0

    goto/32 :goto_e

    nop

    :goto_10
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    goto/32 :goto_7

    nop
.end method

.method clearTranslation(Ljava/util/ArrayList;Landroid/view/ViewGroup;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;",
            "Landroid/view/ViewGroup;",
            ")V"
        }
    .end annotation

    goto/32 :goto_a

    nop

    :goto_0
    invoke-virtual {p2, p1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    goto/32 :goto_f

    nop

    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v0

    goto/32 :goto_c

    nop

    :goto_2
    return-void

    :goto_3
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_7

    nop

    :goto_4
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    goto/32 :goto_5

    nop

    :goto_5
    if-nez p1, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_3

    nop

    :goto_6
    cmpl-float v0, v0, v1

    goto/32 :goto_9

    nop

    :goto_7
    check-cast p1, Landroid/view/View;

    goto/32 :goto_0

    nop

    :goto_8
    if-eq v0, v1, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_1

    nop

    :goto_9
    if-nez v0, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_10

    nop

    :goto_a
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_b
    goto/32 :goto_4

    nop

    :goto_c
    const/4 v1, 0x0

    goto/32 :goto_6

    nop

    :goto_d
    goto :goto_b

    :goto_e
    goto/32 :goto_2

    nop

    :goto_f
    const/4 v1, -0x1

    goto/32 :goto_8

    nop

    :goto_10
    invoke-virtual {p1, v1}, Landroid/view/View;->setTranslationX(F)V

    goto/32 :goto_d

    nop
.end method

.method computOffset(IIIF)I
    .locals 0

    goto/32 :goto_18

    nop

    :goto_0
    float-to-int p0, p0

    goto/32 :goto_a

    nop

    :goto_1
    int-to-float p2, p2

    goto/32 :goto_2

    nop

    :goto_2
    mul-float/2addr p1, p2

    goto/32 :goto_5

    nop

    :goto_3
    div-int/2addr p1, p3

    goto/32 :goto_12

    nop

    :goto_4
    const p3, 0x3f666666    # 0.9f

    goto/32 :goto_6

    nop

    :goto_5
    add-float/2addr p0, p1

    goto/32 :goto_16

    nop

    :goto_6
    div-float/2addr p4, p3

    goto/32 :goto_17

    nop

    :goto_7
    const/4 p0, 0x0

    :goto_8
    goto/32 :goto_e

    nop

    :goto_9
    cmpl-float p1, p0, p1

    goto/32 :goto_14

    nop

    :goto_a
    goto :goto_8

    :goto_b
    goto/32 :goto_7

    nop

    :goto_c
    move p1, p2

    :goto_d
    goto/32 :goto_11

    nop

    :goto_e
    return p0

    :goto_f
    int-to-float p0, p1

    goto/32 :goto_10

    nop

    :goto_10
    const p1, 0x3dcccccd    # 0.1f

    goto/32 :goto_4

    nop

    :goto_11
    mul-float/2addr p4, p4

    goto/32 :goto_f

    nop

    :goto_12
    goto :goto_d

    :goto_13
    goto/32 :goto_c

    nop

    :goto_14
    if-gtz p1, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_0

    nop

    :goto_15
    mul-int/2addr p1, p2

    goto/32 :goto_3

    nop

    :goto_16
    const/4 p1, 0x0

    goto/32 :goto_9

    nop

    :goto_17
    sub-float/2addr p1, p4

    goto/32 :goto_1

    nop

    :goto_18
    if-lt p1, p3, :cond_1

    goto/32 :goto_13

    :cond_1
    goto/32 :goto_15

    nop
.end method

.method fillList(Landroid/view/ViewGroup;Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_3

    nop

    :goto_0
    if-lt v0, p0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    goto/32 :goto_12

    nop

    :goto_2
    if-nez p0, :cond_1

    goto/32 :goto_17

    :cond_1
    goto/32 :goto_16

    nop

    :goto_3
    invoke-virtual {p0, p2, p1}, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->clearTranslation(Ljava/util/ArrayList;Landroid/view/ViewGroup;)V

    goto/32 :goto_9

    nop

    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_5

    nop

    :goto_5
    goto :goto_b

    :goto_6
    goto/32 :goto_d

    nop

    :goto_7
    if-gtz v2, :cond_2

    goto/32 :goto_14

    :cond_2
    :goto_8
    goto/32 :goto_13

    nop

    :goto_9
    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    goto/32 :goto_10

    nop

    :goto_a
    const/4 v0, 0x0

    :goto_b
    goto/32 :goto_0

    nop

    :goto_c
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v2

    goto/32 :goto_7

    nop

    :goto_d
    return-void

    :goto_e
    const/16 v3, 0x8

    goto/32 :goto_15

    nop

    :goto_f
    invoke-virtual {p0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result p0

    goto/32 :goto_2

    nop

    :goto_10
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->sRect:Landroid/graphics/Rect;

    goto/32 :goto_18

    nop

    :goto_11
    iget-object p0, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->sRect:Landroid/graphics/Rect;

    goto/32 :goto_f

    nop

    :goto_12
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v2

    goto/32 :goto_e

    nop

    :goto_13
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_14
    goto/32 :goto_4

    nop

    :goto_15
    if-eq v2, v3, :cond_3

    goto/32 :goto_8

    :cond_3
    goto/32 :goto_c

    nop

    :goto_16
    return-void

    :goto_17
    goto/32 :goto_19

    nop

    :goto_18
    invoke-static {p1, v0}, Lmiuix/internal/util/ViewUtils;->getContentRect(Landroid/view/View;Landroid/graphics/Rect;)V

    goto/32 :goto_11

    nop

    :goto_19
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p0

    goto/32 :goto_a

    nop
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0

    if-nez p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mViewPager:Lmiuix/viewpager/widget/ViewPager;

    invoke-virtual {p1}, Landroidx/viewpager/widget/OriginalViewPager;->getCurrentItem()I

    move-result p1

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mBaseItem:I

    const/4 p1, 0x1

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mBaseItemUpdated:Z

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mListView:Landroid/view/ViewGroup;

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->clearTranslation(Landroid/view/ViewGroup;)V

    :cond_0
    return-void
.end method

.method public onPageScrolled(IFZZ)V
    .locals 7

    const/4 p3, 0x0

    cmpl-float p3, p2, p3

    const/4 p4, 0x1

    if-nez p3, :cond_0

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mBaseItem:I

    iput-boolean p4, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mBaseItemUpdated:Z

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mListView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->clearTranslation(Landroid/view/ViewGroup;)V

    :cond_0
    iget v0, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mScrollBasePosition:I

    if-eq v0, p1, :cond_3

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mBaseItem:I

    if-ge v0, p1, :cond_1

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mBaseItem:I

    goto :goto_0

    :cond_1
    add-int/lit8 v1, p1, 0x1

    if-le v0, v1, :cond_2

    iput v1, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mBaseItem:I

    :cond_2
    :goto_0
    iput p1, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mScrollBasePosition:I

    iput-boolean p4, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mBaseItemUpdated:Z

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mListView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->clearTranslation(Landroid/view/ViewGroup;)V

    :cond_3
    if-lez p3, :cond_8

    iget-boolean p3, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mBaseItemUpdated:Z

    const/4 v0, 0x0

    if-eqz p3, :cond_5

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mBaseItemUpdated:Z

    iget p3, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mBaseItem:I

    if-ne p3, p1, :cond_4

    iget-object p3, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mPagerAdapter:Lmiuix/appcompat/internal/app/widget/DynamicFragmentPagerAdapter;

    invoke-virtual {p3}, Lmiuix/appcompat/internal/app/widget/DynamicFragmentPagerAdapter;->getCount()I

    move-result p3

    sub-int/2addr p3, p4

    if-ge p1, p3, :cond_4

    add-int/lit8 p3, p1, 0x1

    iput p3, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mIncomingPosition:I

    goto :goto_1

    :cond_4
    iput p1, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mIncomingPosition:I

    :goto_1
    iget-object p3, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mPagerAdapter:Lmiuix/appcompat/internal/app/widget/DynamicFragmentPagerAdapter;

    iget v1, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mIncomingPosition:I

    invoke-virtual {p3, v1, v0}, Lmiuix/appcompat/internal/app/widget/DynamicFragmentPagerAdapter;->getFragment(IZ)Landroidx/fragment/app/Fragment;

    move-result-object p3

    const/4 v1, 0x0

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mListView:Landroid/view/ViewGroup;

    if-eqz p3, :cond_5

    invoke-virtual {p3}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {p3}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    move-result-object p3

    const v1, 0x102000a

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    instance-of v1, p3, Landroid/view/ViewGroup;

    if-eqz v1, :cond_5

    check-cast p3, Landroid/view/ViewGroup;

    iput-object p3, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mListView:Landroid/view/ViewGroup;

    :cond_5
    iget p3, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mIncomingPosition:I

    if-ne p3, p1, :cond_6

    const/high16 p3, 0x3f800000    # 1.0f

    sub-float p2, p3, p2

    :cond_6
    move v5, p2

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mListView:Landroid/view/ViewGroup;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v3

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mListView:Landroid/view/ViewGroup;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    iget p2, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->mIncomingPosition:I

    if-eq p2, p1, :cond_7

    move v6, p4

    goto :goto_2

    :cond_7
    move v6, v0

    :goto_2
    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->translateView(Landroid/view/ViewGroup;IIFZ)V

    :cond_8
    return-void
.end method

.method public onPageSelected(I)V
    .locals 0

    return-void
.end method

.method translateView(Landroid/view/ViewGroup;IIFZ)V
    .locals 6

    goto/32 :goto_18

    nop

    :goto_0
    invoke-virtual {p0, p1, v0}, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->fillList(Landroid/view/ViewGroup;Ljava/util/ArrayList;)V

    goto/32 :goto_23

    nop

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    goto/32 :goto_21

    nop

    :goto_2
    if-nez p5, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_f

    nop

    :goto_3
    int-to-float v4, v0

    goto/32 :goto_d

    nop

    :goto_4
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->sList:Ljava/util/ArrayList;

    goto/32 :goto_1a

    nop

    :goto_5
    return-void

    :goto_6
    const v1, 0x7fffffff

    goto/32 :goto_1b

    nop

    :goto_7
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v0

    goto/32 :goto_1c

    nop

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_e

    nop

    :goto_9
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result p1

    goto/32 :goto_6

    nop

    :goto_a
    goto :goto_13

    :goto_b
    goto/32 :goto_5

    nop

    :goto_c
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v4

    goto/32 :goto_1d

    nop

    :goto_d
    invoke-virtual {v3, v4}, Landroid/view/View;->setTranslationX(F)V

    goto/32 :goto_a

    nop

    :goto_e
    check-cast v3, Landroid/view/View;

    goto/32 :goto_c

    nop

    :goto_f
    goto :goto_20

    :goto_10
    goto/32 :goto_1f

    nop

    :goto_11
    check-cast p1, Landroid/view/View;

    goto/32 :goto_9

    nop

    :goto_12
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_13
    goto/32 :goto_1

    nop

    :goto_14
    invoke-virtual {p0, v1, p2, p3, p4}, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->computOffset(IIIF)I

    move-result v1

    goto/32 :goto_2

    nop

    :goto_15
    if-eqz p1, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_4

    nop

    :goto_16
    move v0, v5

    :goto_17
    goto/32 :goto_3

    nop

    :goto_18
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->sList:Ljava/util/ArrayList;

    goto/32 :goto_0

    nop

    :goto_19
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p1

    goto/32 :goto_15

    nop

    :goto_1a
    const/4 v0, 0x0

    goto/32 :goto_24

    nop

    :goto_1b
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->sList:Ljava/util/ArrayList;

    goto/32 :goto_12

    nop

    :goto_1c
    sub-int v1, v0, p1

    goto/32 :goto_14

    nop

    :goto_1d
    if-ne v1, v4, :cond_2

    goto/32 :goto_17

    :cond_2
    goto/32 :goto_7

    nop

    :goto_1e
    move v1, v0

    goto/32 :goto_16

    nop

    :goto_1f
    neg-int v1, v1

    :goto_20
    goto/32 :goto_22

    nop

    :goto_21
    if-nez v3, :cond_3

    goto/32 :goto_b

    :cond_3
    goto/32 :goto_8

    nop

    :goto_22
    move v5, v1

    goto/32 :goto_1e

    nop

    :goto_23
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/ViewPagerScrollEffect;->sList:Ljava/util/ArrayList;

    goto/32 :goto_19

    nop

    :goto_24
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_11

    nop
.end method
