.class public final Lmiuix/appcompat/R$id;
.super Ljava/lang/Object;


# static fields
.field public static final action_bar:I = 0x7f0a0045

.field public static final action_bar_collapse_container:I = 0x7f0a0048

.field public static final action_bar_collapse_tab_container:I = 0x7f0a0049

.field public static final action_bar_container:I = 0x7f0a004a

.field public static final action_bar_expand_container:I = 0x7f0a004b

.field public static final action_bar_movable_container:I = 0x7f0a004c

.field public static final action_bar_movable_tab_container:I = 0x7f0a004d

.field public static final action_bar_overlay_bg:I = 0x7f0a004e

.field public static final action_bar_overlay_floating_root:I = 0x7f0a004f

.field public static final action_bar_overlay_layout:I = 0x7f0a0050

.field public static final action_bar_subtitle:I = 0x7f0a0053

.field public static final action_bar_subtitle_expand:I = 0x7f0a0054

.field public static final action_bar_title:I = 0x7f0a0055

.field public static final action_bar_title_expand:I = 0x7f0a0056

.field public static final action_context_bar:I = 0x7f0a005a

.field public static final action_context_bar_vs:I = 0x7f0a005b

.field public static final action_menu_item_child_icon:I = 0x7f0a0066

.field public static final action_menu_item_child_text:I = 0x7f0a0067

.field public static final action_menu_presenter:I = 0x7f0a0068

.field public static final alertTitle:I = 0x7f0a009c

.field public static final buttonGroup:I = 0x7f0a01cf

.field public static final buttonPanel:I = 0x7f0a01d0

.field public static final checkbox_stub:I = 0x7f0a0224

.field public static final comment:I = 0x7f0a0255

.field public static final contentPanel:I = 0x7f0a026c

.field public static final contentView:I = 0x7f0a026d

.field public static final content_mask:I = 0x7f0a0271

.field public static final content_mask_vs:I = 0x7f0a0272

.field public static final customPanel:I = 0x7f0a0297

.field public static final datePicker:I = 0x7f0a02b5

.field public static final datePickerLunar:I = 0x7f0a02b7

.field public static final dialog_dim_bg:I = 0x7f0a02ff

.field public static final dialog_root_view:I = 0x7f0a0304

.field public static final expanded_menu:I = 0x7f0a0390

.field public static final home:I = 0x7f0a0493

.field public static final lunarModePanel:I = 0x7f0a05f0

.field public static final message:I = 0x7f0a0624

.field public static final miuix_appcompat_floating_window_index:I = 0x7f0a0677

.field public static final more:I = 0x7f0a0684

.field public static final progress_circular:I = 0x7f0a0802

.field public static final progress_percent:I = 0x7f0a0805

.field public static final searchActionMode_customFrameLayout:I = 0x7f0a08d3

.field public static final search_container:I = 0x7f0a08dc

.field public static final search_mask:I = 0x7f0a08e7

.field public static final search_mask_vs:I = 0x7f0a08e8

.field public static final search_text_cancel:I = 0x7f0a08ee

.field public static final shortcut:I = 0x7f0a093f

.field public static final sliding_drawer_handle:I = 0x7f0a0971

.field public static final spinner_dropdown_container:I = 0x7f0a09a3

.field public static final split_action_bar:I = 0x7f0a09a7

.field public static final split_action_bar_vs:I = 0x7f0a09a8

.field public static final tag_popup_menu_item:I = 0x7f0a0a50

.field public static final tag_spinner_dropdown_view:I = 0x7f0a0a52

.field public static final tag_spinner_dropdown_view_double_line:I = 0x7f0a0a53

.field public static final timePicker:I = 0x7f0a0a86

.field public static final title:I = 0x7f0a0a8e

.field public static final titleDividerNoCustom:I = 0x7f0a0a90

.field public static final topPanel:I = 0x7f0a0a9f

.field public static final up:I = 0x7f0a0b1e

.field public static final view_pager:I = 0x7f0a0b6f
