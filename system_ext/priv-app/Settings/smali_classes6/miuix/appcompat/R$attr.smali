.class public final Lmiuix/appcompat/R$attr;
.super Ljava/lang/Object;


# static fields
.field public static final actionBarEmbedTabs:I = 0x7f04000d

.field public static final actionBarIndeterminateProgressStyle:I = 0x7f040013

.field public static final actionBarMovableLayoutStyle:I = 0x7f040016

.field public static final actionBarPaddingEnd:I = 0x7f040019

.field public static final actionBarPaddingStart:I = 0x7f04001a

.field public static final actionBarSplitMaxPercentageHeight:I = 0x7f04002b

.field public static final actionBarTabBadgeIcon:I = 0x7f040033

.field public static final actionBarTabTextExpandStyle:I = 0x7f040038

.field public static final actionBarTabTextSecondaryExpandStyle:I = 0x7f040039

.field public static final actionBarTabTextSecondaryStyle:I = 0x7f04003a

.field public static final actionBarTightTitle:I = 0x7f04003d

.field public static final actionBarTitleEnableEllipsis:I = 0x7f04003f

.field public static final actionModeOverflowButtonStyle:I = 0x7f040055

.field public static final collapseSubtitleTheme:I = 0x7f040165

.field public static final collapseTitleTheme:I = 0x7f040166

.field public static final contextMenuSeparateItemBackground:I = 0x7f0401c0

.field public static final dialogListPreferredItemHeight:I = 0x7f040206

.field public static final expandBackground:I = 0x7f040267

.field public static final expandSubtitleTheme:I = 0x7f04026a

.field public static final expandTitleTheme:I = 0x7f04026c

.field public static final immersionWindowBackground:I = 0x7f04031a

.field public static final miuiAlertDialogTheme:I = 0x7f040433

.field public static final miuiSpinnerStyle:I = 0x7f040436

.field public static final miuixAppcompatFloatingWindowBorderColor:I = 0x7f04043e

.field public static final popupWindowElevation:I = 0x7f0404ba

.field public static final popupWindowShadowAlpha:I = 0x7f0404bc

.field public static final splitActionBarOverlayHeight:I = 0x7f04058c

.field public static final startingWindowOverlay:I = 0x7f04059d

.field public static final state_first_h:I = 0x7f0405a6

.field public static final state_first_v:I = 0x7f0405a7

.field public static final state_last_h:I = 0x7f0405a8

.field public static final state_last_v:I = 0x7f0405a9

.field public static final state_middle_h:I = 0x7f0405ad

.field public static final state_middle_v:I = 0x7f0405ae

.field public static final state_single_h:I = 0x7f0405b2

.field public static final windowActionBar:I = 0x7f04074a

.field public static final windowActionBarMovable:I = 0x7f04074b

.field public static final windowExtraPaddingHorizontal:I = 0x7f040750

.field public static final windowExtraPaddingHorizontalEnable:I = 0x7f040751

.field public static final windowFixedHeightMajor:I = 0x7f040752

.field public static final windowFixedHeightMinor:I = 0x7f040753

.field public static final windowFixedWidthMajor:I = 0x7f040754

.field public static final windowFixedWidthMinor:I = 0x7f040755

.field public static final windowMaxHeightMajor:I = 0x7f040758

.field public static final windowMaxHeightMinor:I = 0x7f040759

.field public static final windowMaxWidthMajor:I = 0x7f04075a

.field public static final windowMaxWidthMinor:I = 0x7f04075b

.field public static final windowTranslucentStatus:I = 0x7f040761
