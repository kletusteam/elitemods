.class public final Lmiuix/appcompat/R$dimen;
.super Ljava/lang/Object;


# static fields
.field public static final action_bar_subtitle_bottom_margin:I = 0x7f070086

.field public static final action_bar_subtitle_top_margin:I = 0x7f070087

.field public static final fake_landscape_screen_minor_size:I = 0x7f070298

.field public static final miuix_appcompat_action_bar_stacked_tab_max_width:I = 0x7f0705ac

.field public static final miuix_appcompat_action_bar_subtitle_bottom_padding:I = 0x7f0705b1

.field public static final miuix_appcompat_action_bar_subtitle_collapse_padding_vertical:I = 0x7f0705b2

.field public static final miuix_appcompat_action_bar_subtitle_start_margin:I = 0x7f0705b3

.field public static final miuix_appcompat_action_bar_tab_expand_margin:I = 0x7f0705b6

.field public static final miuix_appcompat_action_bar_tab_expand_text_size:I = 0x7f0705b7

.field public static final miuix_appcompat_action_bar_tab_expand_text_size_1:I = 0x7f0705b8

.field public static final miuix_appcompat_action_bar_tab_expand_text_size_2:I = 0x7f0705b9

.field public static final miuix_appcompat_action_bar_tab_secondary_margin:I = 0x7f0705bc

.field public static final miuix_appcompat_action_bar_title_bottom_padding:I = 0x7f0705be

.field public static final miuix_appcompat_action_bar_title_collapse_padding_vertical:I = 0x7f0705bf

.field public static final miuix_appcompat_action_bar_title_horizontal_padding:I = 0x7f0705c1

.field public static final miuix_appcompat_action_bar_title_tab_horizontal_padding:I = 0x7f0705c4

.field public static final miuix_appcompat_action_bar_title_top_padding:I = 0x7f0705c5

.field public static final miuix_appcompat_action_button_bg_bottom_padding:I = 0x7f0705c6

.field public static final miuix_appcompat_action_button_bg_top_padding:I = 0x7f0705c7

.field public static final miuix_appcompat_action_button_gap:I = 0x7f0705c8

.field public static final miuix_appcompat_action_button_gap_big_wide:I = 0x7f0705c9

.field public static final miuix_appcompat_action_button_gap_normal_wide:I = 0x7f0705ca

.field public static final miuix_appcompat_action_button_gap_small_wide:I = 0x7f0705cb

.field public static final miuix_appcompat_action_button_gap_tiny_wide:I = 0x7f0705cc

.field public static final miuix_appcompat_action_button_max_width:I = 0x7f0705d0

.field public static final miuix_appcompat_context_menu_separate_item_margin_top:I = 0x7f07060d

.field public static final miuix_appcompat_context_menu_window_margin_screen:I = 0x7f070610

.field public static final miuix_appcompat_context_menu_window_margin_statusbar:I = 0x7f070611

.field public static final miuix_appcompat_dialog_bottom_margin:I = 0x7f07061b

.field public static final miuix_appcompat_dialog_btn_margin_horizontal:I = 0x7f07061c

.field public static final miuix_appcompat_dialog_btn_margin_vertical:I = 0x7f07061d

.field public static final miuix_appcompat_dialog_button_panel_horizontal_margin:I = 0x7f070620

.field public static final miuix_appcompat_dialog_ime_margin:I = 0x7f07063b

.field public static final miuix_appcompat_dialog_max_width:I = 0x7f070646

.field public static final miuix_appcompat_dialog_max_width_land:I = 0x7f070647

.field public static final miuix_appcompat_dialog_message_line_height:I = 0x7f07064b

.field public static final miuix_appcompat_dialog_width_margin:I = 0x7f07065f

.field public static final miuix_appcompat_drop_down_menu_padding_large:I = 0x7f070663

.field public static final miuix_appcompat_drop_down_menu_padding_single_item:I = 0x7f070664

.field public static final miuix_appcompat_drop_down_menu_padding_small:I = 0x7f070665

.field public static final miuix_appcompat_floating_window_background_border_width:I = 0x7f070686

.field public static final miuix_appcompat_floating_window_background_radius:I = 0x7f070687

.field public static final miuix_appcompat_floating_window_top_offset:I = 0x7f070691

.field public static final miuix_appcompat_immersion_menu_background_radius:I = 0x7f07069f

.field public static final miuix_appcompat_list_menu_dialog_maximum_height:I = 0x7f0706a6

.field public static final miuix_appcompat_list_menu_dialog_maximum_width:I = 0x7f0706a7

.field public static final miuix_appcompat_list_menu_dialog_minimum_width:I = 0x7f0706a8

.field public static final miuix_appcompat_menu_popup_max_height:I = 0x7f0706b1

.field public static final miuix_appcompat_radio_button_drawable_padding:I = 0x7f0706e3

.field public static final miuix_appcompat_search_mode_bg_padding:I = 0x7f0706f5

.field public static final miuix_appcompat_search_mode_bg_padding_top:I = 0x7f0706f7

.field public static final miuix_appcompat_search_view_default_height:I = 0x7f0706fa

.field public static final miuix_appcompat_small_text_size:I = 0x7f07070c

.field public static final miuix_appcompat_spinner_magin_screen_horizontal:I = 0x7f07071c

.field public static final miuix_appcompat_spinner_magin_screen_vertical:I = 0x7f07071d

.field public static final miuix_appcompat_spinner_max_height:I = 0x7f07071e

.field public static final miuix_appcompat_spinner_max_width:I = 0x7f07071f

.field public static final miuix_appcompat_subtitle_text_size:I = 0x7f070729

.field public static final miuix_appcompat_window_extra_padding_horizontal_huge:I = 0x7f070730

.field public static final miuix_appcompat_window_extra_padding_horizontal_large:I = 0x7f070731

.field public static final miuix_appcompat_window_extra_padding_horizontal_small:I = 0x7f070732
